; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/extend.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/extend.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }

; Function Attrs: nounwind
define hidden void @av1_copy_and_extend_frame(%struct.yv12_buffer_config* %src, %struct.yv12_buffer_config* %dst) #0 {
entry:
  %src.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst.addr = alloca %struct.yv12_buffer_config*, align 4
  %et_y = alloca i32, align 4
  %el_y = alloca i32, align 4
  %er_y = alloca i32, align 4
  %eb_y = alloca i32, align 4
  %uv_width_subsampling = alloca i32, align 4
  %uv_height_subsampling = alloca i32, align 4
  %et_uv = alloca i32, align 4
  %el_uv = alloca i32, align 4
  %eb_uv = alloca i32, align 4
  %er_uv = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %src, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %dst, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %0 = bitcast i32* %et_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %border = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 12
  %2 = load i32, i32* %border, align 4, !tbaa !6
  store i32 %2, i32* %et_y, align 4, !tbaa !10
  %3 = bitcast i32* %el_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %border1 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %4, i32 0, i32 12
  %5 = load i32, i32* %border1, align 4, !tbaa !6
  store i32 %5, i32* %el_y, align 4, !tbaa !10
  %6 = bitcast i32* %er_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 0
  %9 = bitcast %union.anon* %8 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %9, i32 0, i32 0
  %10 = load i32, i32* %y_width, align 4, !tbaa !11
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %border2 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 12
  %12 = load i32, i32* %border2, align 4, !tbaa !6
  %add = add nsw i32 %10, %12
  %13 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %14 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %13, i32 0, i32 0
  %15 = bitcast %union.anon* %14 to %struct.anon*
  %y_width3 = getelementptr inbounds %struct.anon, %struct.anon* %15, i32 0, i32 0
  %16 = load i32, i32* %y_width3, align 4, !tbaa !11
  %add4 = add nsw i32 %16, 63
  %and = and i32 %add4, -64
  %cmp = icmp sgt i32 %add, %and
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %18 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 0
  %19 = bitcast %union.anon* %18 to %struct.anon*
  %y_width5 = getelementptr inbounds %struct.anon, %struct.anon* %19, i32 0, i32 0
  %20 = load i32, i32* %y_width5, align 4, !tbaa !11
  %21 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %border6 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %21, i32 0, i32 12
  %22 = load i32, i32* %border6, align 4, !tbaa !6
  %add7 = add nsw i32 %20, %22
  br label %cond.end

cond.false:                                       ; preds = %entry
  %23 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %24 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %23, i32 0, i32 0
  %25 = bitcast %union.anon* %24 to %struct.anon*
  %y_width8 = getelementptr inbounds %struct.anon, %struct.anon* %25, i32 0, i32 0
  %26 = load i32, i32* %y_width8, align 4, !tbaa !11
  %add9 = add nsw i32 %26, 63
  %and10 = and i32 %add9, -64
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add7, %cond.true ], [ %and10, %cond.false ]
  %27 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %28 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %27, i32 0, i32 2
  %29 = bitcast %union.anon.2* %28 to %struct.anon.3*
  %y_crop_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %29, i32 0, i32 0
  %30 = load i32, i32* %y_crop_width, align 4, !tbaa !11
  %sub = sub nsw i32 %cond, %30
  store i32 %sub, i32* %er_y, align 4, !tbaa !10
  %31 = bitcast i32* %eb_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %33 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %32, i32 0, i32 1
  %34 = bitcast %union.anon.0* %33 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %34, i32 0, i32 0
  %35 = load i32, i32* %y_height, align 4, !tbaa !11
  %36 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %border11 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %36, i32 0, i32 12
  %37 = load i32, i32* %border11, align 4, !tbaa !6
  %add12 = add nsw i32 %35, %37
  %38 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %39 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %38, i32 0, i32 1
  %40 = bitcast %union.anon.0* %39 to %struct.anon.1*
  %y_height13 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %40, i32 0, i32 0
  %41 = load i32, i32* %y_height13, align 4, !tbaa !11
  %add14 = add nsw i32 %41, 63
  %and15 = and i32 %add14, -64
  %cmp16 = icmp sgt i32 %add12, %and15
  br i1 %cmp16, label %cond.true17, label %cond.false21

cond.true17:                                      ; preds = %cond.end
  %42 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %43 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %42, i32 0, i32 1
  %44 = bitcast %union.anon.0* %43 to %struct.anon.1*
  %y_height18 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %44, i32 0, i32 0
  %45 = load i32, i32* %y_height18, align 4, !tbaa !11
  %46 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %border19 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %46, i32 0, i32 12
  %47 = load i32, i32* %border19, align 4, !tbaa !6
  %add20 = add nsw i32 %45, %47
  br label %cond.end25

cond.false21:                                     ; preds = %cond.end
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %49 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 1
  %50 = bitcast %union.anon.0* %49 to %struct.anon.1*
  %y_height22 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %50, i32 0, i32 0
  %51 = load i32, i32* %y_height22, align 4, !tbaa !11
  %add23 = add nsw i32 %51, 63
  %and24 = and i32 %add23, -64
  br label %cond.end25

cond.end25:                                       ; preds = %cond.false21, %cond.true17
  %cond26 = phi i32 [ %add20, %cond.true17 ], [ %and24, %cond.false21 ]
  %52 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %53 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %52, i32 0, i32 3
  %54 = bitcast %union.anon.4* %53 to %struct.anon.5*
  %y_crop_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %54, i32 0, i32 0
  %55 = load i32, i32* %y_crop_height, align 4, !tbaa !11
  %sub27 = sub nsw i32 %cond26, %55
  store i32 %sub27, i32* %eb_y, align 4, !tbaa !10
  %56 = bitcast i32* %uv_width_subsampling to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #4
  %57 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %58 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %57, i32 0, i32 0
  %59 = bitcast %union.anon* %58 to %struct.anon*
  %uv_width = getelementptr inbounds %struct.anon, %struct.anon* %59, i32 0, i32 1
  %60 = load i32, i32* %uv_width, align 4, !tbaa !11
  %61 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %62 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %61, i32 0, i32 0
  %63 = bitcast %union.anon* %62 to %struct.anon*
  %y_width28 = getelementptr inbounds %struct.anon, %struct.anon* %63, i32 0, i32 0
  %64 = load i32, i32* %y_width28, align 4, !tbaa !11
  %cmp29 = icmp ne i32 %60, %64
  %conv = zext i1 %cmp29 to i32
  store i32 %conv, i32* %uv_width_subsampling, align 4, !tbaa !10
  %65 = bitcast i32* %uv_height_subsampling to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #4
  %66 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %67 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %66, i32 0, i32 1
  %68 = bitcast %union.anon.0* %67 to %struct.anon.1*
  %uv_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %68, i32 0, i32 1
  %69 = load i32, i32* %uv_height, align 4, !tbaa !11
  %70 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %71 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %70, i32 0, i32 1
  %72 = bitcast %union.anon.0* %71 to %struct.anon.1*
  %y_height30 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %72, i32 0, i32 0
  %73 = load i32, i32* %y_height30, align 4, !tbaa !11
  %cmp31 = icmp ne i32 %69, %73
  %conv32 = zext i1 %cmp31 to i32
  store i32 %conv32, i32* %uv_height_subsampling, align 4, !tbaa !10
  %74 = bitcast i32* %et_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #4
  %75 = load i32, i32* %et_y, align 4, !tbaa !10
  %76 = load i32, i32* %uv_height_subsampling, align 4, !tbaa !10
  %shr = ashr i32 %75, %76
  store i32 %shr, i32* %et_uv, align 4, !tbaa !10
  %77 = bitcast i32* %el_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #4
  %78 = load i32, i32* %el_y, align 4, !tbaa !10
  %79 = load i32, i32* %uv_width_subsampling, align 4, !tbaa !10
  %shr33 = ashr i32 %78, %79
  store i32 %shr33, i32* %el_uv, align 4, !tbaa !10
  %80 = bitcast i32* %eb_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #4
  %81 = load i32, i32* %eb_y, align 4, !tbaa !10
  %82 = load i32, i32* %uv_height_subsampling, align 4, !tbaa !10
  %shr34 = ashr i32 %81, %82
  store i32 %shr34, i32* %eb_uv, align 4, !tbaa !10
  %83 = bitcast i32* %er_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #4
  %84 = load i32, i32* %er_y, align 4, !tbaa !10
  %85 = load i32, i32* %uv_width_subsampling, align 4, !tbaa !10
  %shr35 = ashr i32 %84, %85
  store i32 %shr35, i32* %er_uv, align 4, !tbaa !10
  %86 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %86, i32 0, i32 26
  %87 = load i32, i32* %flags, align 4, !tbaa !12
  %and36 = and i32 %87, 8
  %tobool = icmp ne i32 %and36, 0
  br i1 %tobool, label %if.then, label %if.end55

if.then:                                          ; preds = %cond.end25
  %88 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %89 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %88, i32 0, i32 5
  %90 = bitcast %union.anon.8* %89 to %struct.anon.9*
  %y_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %90, i32 0, i32 0
  %91 = load i8*, i8** %y_buffer, align 4, !tbaa !11
  %92 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %93 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %92, i32 0, i32 4
  %94 = bitcast %union.anon.6* %93 to %struct.anon.7*
  %y_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %94, i32 0, i32 0
  %95 = load i32, i32* %y_stride, align 4, !tbaa !11
  %96 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %97 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %96, i32 0, i32 5
  %98 = bitcast %union.anon.8* %97 to %struct.anon.9*
  %y_buffer37 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %98, i32 0, i32 0
  %99 = load i8*, i8** %y_buffer37, align 4, !tbaa !11
  %100 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %101 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %100, i32 0, i32 4
  %102 = bitcast %union.anon.6* %101 to %struct.anon.7*
  %y_stride38 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %102, i32 0, i32 0
  %103 = load i32, i32* %y_stride38, align 4, !tbaa !11
  %104 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %105 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %104, i32 0, i32 2
  %106 = bitcast %union.anon.2* %105 to %struct.anon.3*
  %y_crop_width39 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %106, i32 0, i32 0
  %107 = load i32, i32* %y_crop_width39, align 4, !tbaa !11
  %108 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %109 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %108, i32 0, i32 3
  %110 = bitcast %union.anon.4* %109 to %struct.anon.5*
  %y_crop_height40 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %110, i32 0, i32 0
  %111 = load i32, i32* %y_crop_height40, align 4, !tbaa !11
  %112 = load i32, i32* %et_y, align 4, !tbaa !10
  %113 = load i32, i32* %el_y, align 4, !tbaa !10
  %114 = load i32, i32* %eb_y, align 4, !tbaa !10
  %115 = load i32, i32* %er_y, align 4, !tbaa !10
  call void @highbd_copy_and_extend_plane(i8* %91, i32 %95, i8* %99, i32 %103, i32 %107, i32 %111, i32 %112, i32 %113, i32 %114, i32 %115)
  %116 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %117 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %116, i32 0, i32 5
  %118 = bitcast %union.anon.8* %117 to %struct.anon.9*
  %u_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %118, i32 0, i32 1
  %119 = load i8*, i8** %u_buffer, align 4, !tbaa !11
  %tobool41 = icmp ne i8* %119, null
  br i1 %tobool41, label %if.then42, label %if.end

if.then42:                                        ; preds = %if.then
  %120 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %121 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %120, i32 0, i32 5
  %122 = bitcast %union.anon.8* %121 to %struct.anon.9*
  %u_buffer43 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %122, i32 0, i32 1
  %123 = load i8*, i8** %u_buffer43, align 4, !tbaa !11
  %124 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %125 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %124, i32 0, i32 4
  %126 = bitcast %union.anon.6* %125 to %struct.anon.7*
  %uv_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %126, i32 0, i32 1
  %127 = load i32, i32* %uv_stride, align 4, !tbaa !11
  %128 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %129 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %128, i32 0, i32 5
  %130 = bitcast %union.anon.8* %129 to %struct.anon.9*
  %u_buffer44 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %130, i32 0, i32 1
  %131 = load i8*, i8** %u_buffer44, align 4, !tbaa !11
  %132 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %133 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %132, i32 0, i32 4
  %134 = bitcast %union.anon.6* %133 to %struct.anon.7*
  %uv_stride45 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %134, i32 0, i32 1
  %135 = load i32, i32* %uv_stride45, align 4, !tbaa !11
  %136 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %137 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %136, i32 0, i32 2
  %138 = bitcast %union.anon.2* %137 to %struct.anon.3*
  %uv_crop_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %138, i32 0, i32 1
  %139 = load i32, i32* %uv_crop_width, align 4, !tbaa !11
  %140 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %141 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %140, i32 0, i32 3
  %142 = bitcast %union.anon.4* %141 to %struct.anon.5*
  %uv_crop_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %142, i32 0, i32 1
  %143 = load i32, i32* %uv_crop_height, align 4, !tbaa !11
  %144 = load i32, i32* %et_uv, align 4, !tbaa !10
  %145 = load i32, i32* %el_uv, align 4, !tbaa !10
  %146 = load i32, i32* %eb_uv, align 4, !tbaa !10
  %147 = load i32, i32* %er_uv, align 4, !tbaa !10
  call void @highbd_copy_and_extend_plane(i8* %123, i32 %127, i8* %131, i32 %135, i32 %139, i32 %143, i32 %144, i32 %145, i32 %146, i32 %147)
  br label %if.end

if.end:                                           ; preds = %if.then42, %if.then
  %148 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %149 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %148, i32 0, i32 5
  %150 = bitcast %union.anon.8* %149 to %struct.anon.9*
  %v_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %150, i32 0, i32 2
  %151 = load i8*, i8** %v_buffer, align 4, !tbaa !11
  %tobool46 = icmp ne i8* %151, null
  br i1 %tobool46, label %if.then47, label %if.end54

if.then47:                                        ; preds = %if.end
  %152 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %153 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %152, i32 0, i32 5
  %154 = bitcast %union.anon.8* %153 to %struct.anon.9*
  %v_buffer48 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %154, i32 0, i32 2
  %155 = load i8*, i8** %v_buffer48, align 4, !tbaa !11
  %156 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %157 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %156, i32 0, i32 4
  %158 = bitcast %union.anon.6* %157 to %struct.anon.7*
  %uv_stride49 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %158, i32 0, i32 1
  %159 = load i32, i32* %uv_stride49, align 4, !tbaa !11
  %160 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %161 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %160, i32 0, i32 5
  %162 = bitcast %union.anon.8* %161 to %struct.anon.9*
  %v_buffer50 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %162, i32 0, i32 2
  %163 = load i8*, i8** %v_buffer50, align 4, !tbaa !11
  %164 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %165 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %164, i32 0, i32 4
  %166 = bitcast %union.anon.6* %165 to %struct.anon.7*
  %uv_stride51 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %166, i32 0, i32 1
  %167 = load i32, i32* %uv_stride51, align 4, !tbaa !11
  %168 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %169 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %168, i32 0, i32 2
  %170 = bitcast %union.anon.2* %169 to %struct.anon.3*
  %uv_crop_width52 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %170, i32 0, i32 1
  %171 = load i32, i32* %uv_crop_width52, align 4, !tbaa !11
  %172 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %173 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %172, i32 0, i32 3
  %174 = bitcast %union.anon.4* %173 to %struct.anon.5*
  %uv_crop_height53 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %174, i32 0, i32 1
  %175 = load i32, i32* %uv_crop_height53, align 4, !tbaa !11
  %176 = load i32, i32* %et_uv, align 4, !tbaa !10
  %177 = load i32, i32* %el_uv, align 4, !tbaa !10
  %178 = load i32, i32* %eb_uv, align 4, !tbaa !10
  %179 = load i32, i32* %er_uv, align 4, !tbaa !10
  call void @highbd_copy_and_extend_plane(i8* %155, i32 %159, i8* %163, i32 %167, i32 %171, i32 %175, i32 %176, i32 %177, i32 %178, i32 %179)
  br label %if.end54

if.end54:                                         ; preds = %if.then47, %if.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end55:                                         ; preds = %cond.end25
  %180 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %181 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %180, i32 0, i32 5
  %182 = bitcast %union.anon.8* %181 to %struct.anon.9*
  %y_buffer56 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %182, i32 0, i32 0
  %183 = load i8*, i8** %y_buffer56, align 4, !tbaa !11
  %184 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %185 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %184, i32 0, i32 4
  %186 = bitcast %union.anon.6* %185 to %struct.anon.7*
  %y_stride57 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %186, i32 0, i32 0
  %187 = load i32, i32* %y_stride57, align 4, !tbaa !11
  %188 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %189 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %188, i32 0, i32 5
  %190 = bitcast %union.anon.8* %189 to %struct.anon.9*
  %y_buffer58 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %190, i32 0, i32 0
  %191 = load i8*, i8** %y_buffer58, align 4, !tbaa !11
  %192 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %193 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %192, i32 0, i32 4
  %194 = bitcast %union.anon.6* %193 to %struct.anon.7*
  %y_stride59 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %194, i32 0, i32 0
  %195 = load i32, i32* %y_stride59, align 4, !tbaa !11
  %196 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %197 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %196, i32 0, i32 2
  %198 = bitcast %union.anon.2* %197 to %struct.anon.3*
  %y_crop_width60 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %198, i32 0, i32 0
  %199 = load i32, i32* %y_crop_width60, align 4, !tbaa !11
  %200 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %201 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %200, i32 0, i32 3
  %202 = bitcast %union.anon.4* %201 to %struct.anon.5*
  %y_crop_height61 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %202, i32 0, i32 0
  %203 = load i32, i32* %y_crop_height61, align 4, !tbaa !11
  %204 = load i32, i32* %et_y, align 4, !tbaa !10
  %205 = load i32, i32* %el_y, align 4, !tbaa !10
  %206 = load i32, i32* %eb_y, align 4, !tbaa !10
  %207 = load i32, i32* %er_y, align 4, !tbaa !10
  call void @copy_and_extend_plane(i8* %183, i32 %187, i8* %191, i32 %195, i32 %199, i32 %203, i32 %204, i32 %205, i32 %206, i32 %207)
  %208 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %209 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %208, i32 0, i32 5
  %210 = bitcast %union.anon.8* %209 to %struct.anon.9*
  %u_buffer62 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %210, i32 0, i32 1
  %211 = load i8*, i8** %u_buffer62, align 4, !tbaa !11
  %tobool63 = icmp ne i8* %211, null
  br i1 %tobool63, label %if.then64, label %if.end71

if.then64:                                        ; preds = %if.end55
  %212 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %213 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %212, i32 0, i32 5
  %214 = bitcast %union.anon.8* %213 to %struct.anon.9*
  %u_buffer65 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %214, i32 0, i32 1
  %215 = load i8*, i8** %u_buffer65, align 4, !tbaa !11
  %216 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %217 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %216, i32 0, i32 4
  %218 = bitcast %union.anon.6* %217 to %struct.anon.7*
  %uv_stride66 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %218, i32 0, i32 1
  %219 = load i32, i32* %uv_stride66, align 4, !tbaa !11
  %220 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %221 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %220, i32 0, i32 5
  %222 = bitcast %union.anon.8* %221 to %struct.anon.9*
  %u_buffer67 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %222, i32 0, i32 1
  %223 = load i8*, i8** %u_buffer67, align 4, !tbaa !11
  %224 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %225 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %224, i32 0, i32 4
  %226 = bitcast %union.anon.6* %225 to %struct.anon.7*
  %uv_stride68 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %226, i32 0, i32 1
  %227 = load i32, i32* %uv_stride68, align 4, !tbaa !11
  %228 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %229 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %228, i32 0, i32 2
  %230 = bitcast %union.anon.2* %229 to %struct.anon.3*
  %uv_crop_width69 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %230, i32 0, i32 1
  %231 = load i32, i32* %uv_crop_width69, align 4, !tbaa !11
  %232 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %233 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %232, i32 0, i32 3
  %234 = bitcast %union.anon.4* %233 to %struct.anon.5*
  %uv_crop_height70 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %234, i32 0, i32 1
  %235 = load i32, i32* %uv_crop_height70, align 4, !tbaa !11
  %236 = load i32, i32* %et_uv, align 4, !tbaa !10
  %237 = load i32, i32* %el_uv, align 4, !tbaa !10
  %238 = load i32, i32* %eb_uv, align 4, !tbaa !10
  %239 = load i32, i32* %er_uv, align 4, !tbaa !10
  call void @copy_and_extend_plane(i8* %215, i32 %219, i8* %223, i32 %227, i32 %231, i32 %235, i32 %236, i32 %237, i32 %238, i32 %239)
  br label %if.end71

if.end71:                                         ; preds = %if.then64, %if.end55
  %240 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %241 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %240, i32 0, i32 5
  %242 = bitcast %union.anon.8* %241 to %struct.anon.9*
  %v_buffer72 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %242, i32 0, i32 2
  %243 = load i8*, i8** %v_buffer72, align 4, !tbaa !11
  %tobool73 = icmp ne i8* %243, null
  br i1 %tobool73, label %if.then74, label %if.end81

if.then74:                                        ; preds = %if.end71
  %244 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %245 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %244, i32 0, i32 5
  %246 = bitcast %union.anon.8* %245 to %struct.anon.9*
  %v_buffer75 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %246, i32 0, i32 2
  %247 = load i8*, i8** %v_buffer75, align 4, !tbaa !11
  %248 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %249 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %248, i32 0, i32 4
  %250 = bitcast %union.anon.6* %249 to %struct.anon.7*
  %uv_stride76 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %250, i32 0, i32 1
  %251 = load i32, i32* %uv_stride76, align 4, !tbaa !11
  %252 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %253 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %252, i32 0, i32 5
  %254 = bitcast %union.anon.8* %253 to %struct.anon.9*
  %v_buffer77 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %254, i32 0, i32 2
  %255 = load i8*, i8** %v_buffer77, align 4, !tbaa !11
  %256 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %257 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %256, i32 0, i32 4
  %258 = bitcast %union.anon.6* %257 to %struct.anon.7*
  %uv_stride78 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %258, i32 0, i32 1
  %259 = load i32, i32* %uv_stride78, align 4, !tbaa !11
  %260 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %261 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %260, i32 0, i32 2
  %262 = bitcast %union.anon.2* %261 to %struct.anon.3*
  %uv_crop_width79 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %262, i32 0, i32 1
  %263 = load i32, i32* %uv_crop_width79, align 4, !tbaa !11
  %264 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %265 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %264, i32 0, i32 3
  %266 = bitcast %union.anon.4* %265 to %struct.anon.5*
  %uv_crop_height80 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %266, i32 0, i32 1
  %267 = load i32, i32* %uv_crop_height80, align 4, !tbaa !11
  %268 = load i32, i32* %et_uv, align 4, !tbaa !10
  %269 = load i32, i32* %el_uv, align 4, !tbaa !10
  %270 = load i32, i32* %eb_uv, align 4, !tbaa !10
  %271 = load i32, i32* %er_uv, align 4, !tbaa !10
  call void @copy_and_extend_plane(i8* %247, i32 %251, i8* %255, i32 %259, i32 %263, i32 %267, i32 %268, i32 %269, i32 %270, i32 %271)
  br label %if.end81

if.end81:                                         ; preds = %if.then74, %if.end71
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end81, %if.end54
  %272 = bitcast i32* %er_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #4
  %273 = bitcast i32* %eb_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #4
  %274 = bitcast i32* %el_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #4
  %275 = bitcast i32* %et_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #4
  %276 = bitcast i32* %uv_height_subsampling to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #4
  %277 = bitcast i32* %uv_width_subsampling to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #4
  %278 = bitcast i32* %eb_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #4
  %279 = bitcast i32* %er_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #4
  %280 = bitcast i32* %el_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #4
  %281 = bitcast i32* %et_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @highbd_copy_and_extend_plane(i8* %src8, i32 %src_pitch, i8* %dst8, i32 %dst_pitch, i32 %w, i32 %h, i32 %extend_top, i32 %extend_left, i32 %extend_bottom, i32 %extend_right) #0 {
entry:
  %src8.addr = alloca i8*, align 4
  %src_pitch.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_pitch.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %extend_top.addr = alloca i32, align 4
  %extend_left.addr = alloca i32, align 4
  %extend_bottom.addr = alloca i32, align 4
  %extend_right.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %linesize = alloca i32, align 4
  %src = alloca i16*, align 4
  %dst = alloca i16*, align 4
  %src_ptr1 = alloca i16*, align 4
  %src_ptr2 = alloca i16*, align 4
  %dst_ptr1 = alloca i16*, align 4
  %dst_ptr2 = alloca i16*, align 4
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %src_pitch, i32* %src_pitch.addr, align 4, !tbaa !10
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_pitch, i32* %dst_pitch.addr, align 4, !tbaa !10
  store i32 %w, i32* %w.addr, align 4, !tbaa !10
  store i32 %h, i32* %h.addr, align 4, !tbaa !10
  store i32 %extend_top, i32* %extend_top.addr, align 4, !tbaa !10
  store i32 %extend_left, i32* %extend_left.addr, align 4, !tbaa !10
  store i32 %extend_bottom, i32* %extend_bottom.addr, align 4, !tbaa !10
  store i32 %extend_right, i32* %extend_right.addr, align 4, !tbaa !10
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %linesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i16** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  store i16* %5, i16** %src, align 4, !tbaa !2
  %6 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %8 = ptrtoint i8* %7 to i32
  %shl1 = shl i32 %8, 1
  %9 = inttoptr i32 %shl1 to i16*
  store i16* %9, i16** %dst, align 4, !tbaa !2
  %10 = bitcast i16** %src_ptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load i16*, i16** %src, align 4, !tbaa !2
  store i16* %11, i16** %src_ptr1, align 4, !tbaa !2
  %12 = bitcast i16** %src_ptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i16*, i16** %src, align 4, !tbaa !2
  %14 = load i32, i32* %w.addr, align 4, !tbaa !10
  %add.ptr = getelementptr inbounds i16, i16* %13, i32 %14
  %add.ptr2 = getelementptr inbounds i16, i16* %add.ptr, i32 -1
  store i16* %add.ptr2, i16** %src_ptr2, align 4, !tbaa !2
  %15 = bitcast i16** %dst_ptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load i16*, i16** %dst, align 4, !tbaa !2
  %17 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg = sub i32 0, %17
  %add.ptr3 = getelementptr inbounds i16, i16* %16, i32 %idx.neg
  store i16* %add.ptr3, i16** %dst_ptr1, align 4, !tbaa !2
  %18 = bitcast i16** %dst_ptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load i16*, i16** %dst, align 4, !tbaa !2
  %20 = load i32, i32* %w.addr, align 4, !tbaa !10
  %add.ptr4 = getelementptr inbounds i16, i16* %19, i32 %20
  store i16* %add.ptr4, i16** %dst_ptr2, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %21 = load i32, i32* %i, align 4, !tbaa !10
  %22 = load i32, i32* %h.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %21, %22
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load i16*, i16** %dst_ptr1, align 4, !tbaa !2
  %24 = bitcast i16* %23 to i8*
  %25 = load i16*, i16** %src_ptr1, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %25, i32 0
  %26 = load i16, i16* %arrayidx, align 2, !tbaa !13
  %conv = zext i16 %26 to i32
  %27 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %call = call i8* @aom_memset16(i8* %24, i32 %conv, i32 %27)
  %28 = load i16*, i16** %dst_ptr1, align 4, !tbaa !2
  %29 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %add.ptr5 = getelementptr inbounds i16, i16* %28, i32 %29
  %30 = bitcast i16* %add.ptr5 to i8*
  %31 = load i16*, i16** %src_ptr1, align 4, !tbaa !2
  %32 = bitcast i16* %31 to i8*
  %33 = load i32, i32* %w.addr, align 4, !tbaa !10
  %mul = mul i32 %33, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %30, i8* align 2 %32, i32 %mul, i1 false)
  %34 = load i16*, i16** %dst_ptr2, align 4, !tbaa !2
  %35 = bitcast i16* %34 to i8*
  %36 = load i16*, i16** %src_ptr2, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %36, i32 0
  %37 = load i16, i16* %arrayidx6, align 2, !tbaa !13
  %conv7 = zext i16 %37 to i32
  %38 = load i32, i32* %extend_right.addr, align 4, !tbaa !10
  %call8 = call i8* @aom_memset16(i8* %35, i32 %conv7, i32 %38)
  %39 = load i32, i32* %src_pitch.addr, align 4, !tbaa !10
  %40 = load i16*, i16** %src_ptr1, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i16, i16* %40, i32 %39
  store i16* %add.ptr9, i16** %src_ptr1, align 4, !tbaa !2
  %41 = load i32, i32* %src_pitch.addr, align 4, !tbaa !10
  %42 = load i16*, i16** %src_ptr2, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i16, i16* %42, i32 %41
  store i16* %add.ptr10, i16** %src_ptr2, align 4, !tbaa !2
  %43 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %44 = load i16*, i16** %dst_ptr1, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i16, i16* %44, i32 %43
  store i16* %add.ptr11, i16** %dst_ptr1, align 4, !tbaa !2
  %45 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %46 = load i16*, i16** %dst_ptr2, align 4, !tbaa !2
  %add.ptr12 = getelementptr inbounds i16, i16* %46, i32 %45
  store i16* %add.ptr12, i16** %dst_ptr2, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %47 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %48 = load i16*, i16** %dst, align 4, !tbaa !2
  %49 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg13 = sub i32 0, %49
  %add.ptr14 = getelementptr inbounds i16, i16* %48, i32 %idx.neg13
  store i16* %add.ptr14, i16** %src_ptr1, align 4, !tbaa !2
  %50 = load i16*, i16** %dst, align 4, !tbaa !2
  %51 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %52 = load i32, i32* %h.addr, align 4, !tbaa !10
  %sub = sub nsw i32 %52, 1
  %mul15 = mul nsw i32 %51, %sub
  %add.ptr16 = getelementptr inbounds i16, i16* %50, i32 %mul15
  %53 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg17 = sub i32 0, %53
  %add.ptr18 = getelementptr inbounds i16, i16* %add.ptr16, i32 %idx.neg17
  store i16* %add.ptr18, i16** %src_ptr2, align 4, !tbaa !2
  %54 = load i16*, i16** %dst, align 4, !tbaa !2
  %55 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %56 = load i32, i32* %extend_top.addr, align 4, !tbaa !10
  %sub19 = sub nsw i32 0, %56
  %mul20 = mul nsw i32 %55, %sub19
  %add.ptr21 = getelementptr inbounds i16, i16* %54, i32 %mul20
  %57 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg22 = sub i32 0, %57
  %add.ptr23 = getelementptr inbounds i16, i16* %add.ptr21, i32 %idx.neg22
  store i16* %add.ptr23, i16** %dst_ptr1, align 4, !tbaa !2
  %58 = load i16*, i16** %dst, align 4, !tbaa !2
  %59 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %60 = load i32, i32* %h.addr, align 4, !tbaa !10
  %mul24 = mul nsw i32 %59, %60
  %add.ptr25 = getelementptr inbounds i16, i16* %58, i32 %mul24
  %61 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg26 = sub i32 0, %61
  %add.ptr27 = getelementptr inbounds i16, i16* %add.ptr25, i32 %idx.neg26
  store i16* %add.ptr27, i16** %dst_ptr2, align 4, !tbaa !2
  %62 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %63 = load i32, i32* %extend_right.addr, align 4, !tbaa !10
  %add = add nsw i32 %62, %63
  %64 = load i32, i32* %w.addr, align 4, !tbaa !10
  %add28 = add nsw i32 %add, %64
  store i32 %add28, i32* %linesize, align 4, !tbaa !10
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc35, %for.end
  %65 = load i32, i32* %i, align 4, !tbaa !10
  %66 = load i32, i32* %extend_top.addr, align 4, !tbaa !10
  %cmp30 = icmp slt i32 %65, %66
  br i1 %cmp30, label %for.body32, label %for.end37

for.body32:                                       ; preds = %for.cond29
  %67 = load i16*, i16** %dst_ptr1, align 4, !tbaa !2
  %68 = bitcast i16* %67 to i8*
  %69 = load i16*, i16** %src_ptr1, align 4, !tbaa !2
  %70 = bitcast i16* %69 to i8*
  %71 = load i32, i32* %linesize, align 4, !tbaa !10
  %mul33 = mul i32 %71, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %68, i8* align 2 %70, i32 %mul33, i1 false)
  %72 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %73 = load i16*, i16** %dst_ptr1, align 4, !tbaa !2
  %add.ptr34 = getelementptr inbounds i16, i16* %73, i32 %72
  store i16* %add.ptr34, i16** %dst_ptr1, align 4, !tbaa !2
  br label %for.inc35

for.inc35:                                        ; preds = %for.body32
  %74 = load i32, i32* %i, align 4, !tbaa !10
  %inc36 = add nsw i32 %74, 1
  store i32 %inc36, i32* %i, align 4, !tbaa !10
  br label %for.cond29

for.end37:                                        ; preds = %for.cond29
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc44, %for.end37
  %75 = load i32, i32* %i, align 4, !tbaa !10
  %76 = load i32, i32* %extend_bottom.addr, align 4, !tbaa !10
  %cmp39 = icmp slt i32 %75, %76
  br i1 %cmp39, label %for.body41, label %for.end46

for.body41:                                       ; preds = %for.cond38
  %77 = load i16*, i16** %dst_ptr2, align 4, !tbaa !2
  %78 = bitcast i16* %77 to i8*
  %79 = load i16*, i16** %src_ptr2, align 4, !tbaa !2
  %80 = bitcast i16* %79 to i8*
  %81 = load i32, i32* %linesize, align 4, !tbaa !10
  %mul42 = mul i32 %81, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %78, i8* align 2 %80, i32 %mul42, i1 false)
  %82 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %83 = load i16*, i16** %dst_ptr2, align 4, !tbaa !2
  %add.ptr43 = getelementptr inbounds i16, i16* %83, i32 %82
  store i16* %add.ptr43, i16** %dst_ptr2, align 4, !tbaa !2
  br label %for.inc44

for.inc44:                                        ; preds = %for.body41
  %84 = load i32, i32* %i, align 4, !tbaa !10
  %inc45 = add nsw i32 %84, 1
  store i32 %inc45, i32* %i, align 4, !tbaa !10
  br label %for.cond38

for.end46:                                        ; preds = %for.cond38
  %85 = bitcast i16** %dst_ptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i16** %dst_ptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i16** %src_ptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i16** %src_ptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i16** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i32* %linesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  ret void
}

; Function Attrs: nounwind
define internal void @copy_and_extend_plane(i8* %src, i32 %src_pitch, i8* %dst, i32 %dst_pitch, i32 %w, i32 %h, i32 %extend_top, i32 %extend_left, i32 %extend_bottom, i32 %extend_right) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_pitch.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_pitch.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %extend_top.addr = alloca i32, align 4
  %extend_left.addr = alloca i32, align 4
  %extend_bottom.addr = alloca i32, align 4
  %extend_right.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %linesize = alloca i32, align 4
  %src_ptr1 = alloca i8*, align 4
  %src_ptr2 = alloca i8*, align 4
  %dst_ptr1 = alloca i8*, align 4
  %dst_ptr2 = alloca i8*, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_pitch, i32* %src_pitch.addr, align 4, !tbaa !10
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_pitch, i32* %dst_pitch.addr, align 4, !tbaa !10
  store i32 %w, i32* %w.addr, align 4, !tbaa !10
  store i32 %h, i32* %h.addr, align 4, !tbaa !10
  store i32 %extend_top, i32* %extend_top.addr, align 4, !tbaa !10
  store i32 %extend_left, i32* %extend_left.addr, align 4, !tbaa !10
  store i32 %extend_bottom, i32* %extend_bottom.addr, align 4, !tbaa !10
  store i32 %extend_right, i32* %extend_right.addr, align 4, !tbaa !10
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %linesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %src_ptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i8*, i8** %src.addr, align 4, !tbaa !2
  store i8* %3, i8** %src_ptr1, align 4, !tbaa !2
  %4 = bitcast i8** %src_ptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %6 = load i32, i32* %w.addr, align 4, !tbaa !10
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %6
  %add.ptr1 = getelementptr inbounds i8, i8* %add.ptr, i32 -1
  store i8* %add.ptr1, i8** %src_ptr2, align 4, !tbaa !2
  %7 = bitcast i8** %dst_ptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %9 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg = sub i32 0, %9
  %add.ptr2 = getelementptr inbounds i8, i8* %8, i32 %idx.neg
  store i8* %add.ptr2, i8** %dst_ptr1, align 4, !tbaa !2
  %10 = bitcast i8** %dst_ptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %12 = load i32, i32* %w.addr, align 4, !tbaa !10
  %add.ptr3 = getelementptr inbounds i8, i8* %11, i32 %12
  store i8* %add.ptr3, i8** %dst_ptr2, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load i32, i32* %i, align 4, !tbaa !10
  %14 = load i32, i32* %h.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %13, %14
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i8*, i8** %dst_ptr1, align 4, !tbaa !2
  %16 = load i8*, i8** %src_ptr1, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %16, i32 0
  %17 = load i8, i8* %arrayidx, align 1, !tbaa !11
  %conv = zext i8 %17 to i32
  %18 = trunc i32 %conv to i8
  %19 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  call void @llvm.memset.p0i8.i32(i8* align 1 %15, i8 %18, i32 %19, i1 false)
  %20 = load i8*, i8** %dst_ptr1, align 4, !tbaa !2
  %21 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %add.ptr4 = getelementptr inbounds i8, i8* %20, i32 %21
  %22 = load i8*, i8** %src_ptr1, align 4, !tbaa !2
  %23 = load i32, i32* %w.addr, align 4, !tbaa !10
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr4, i8* align 1 %22, i32 %23, i1 false)
  %24 = load i8*, i8** %dst_ptr2, align 4, !tbaa !2
  %25 = load i8*, i8** %src_ptr2, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %25, i32 0
  %26 = load i8, i8* %arrayidx5, align 1, !tbaa !11
  %conv6 = zext i8 %26 to i32
  %27 = trunc i32 %conv6 to i8
  %28 = load i32, i32* %extend_right.addr, align 4, !tbaa !10
  call void @llvm.memset.p0i8.i32(i8* align 1 %24, i8 %27, i32 %28, i1 false)
  %29 = load i32, i32* %src_pitch.addr, align 4, !tbaa !10
  %30 = load i8*, i8** %src_ptr1, align 4, !tbaa !2
  %add.ptr7 = getelementptr inbounds i8, i8* %30, i32 %29
  store i8* %add.ptr7, i8** %src_ptr1, align 4, !tbaa !2
  %31 = load i32, i32* %src_pitch.addr, align 4, !tbaa !10
  %32 = load i8*, i8** %src_ptr2, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds i8, i8* %32, i32 %31
  store i8* %add.ptr8, i8** %src_ptr2, align 4, !tbaa !2
  %33 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %34 = load i8*, i8** %dst_ptr1, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i8, i8* %34, i32 %33
  store i8* %add.ptr9, i8** %dst_ptr1, align 4, !tbaa !2
  %35 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %36 = load i8*, i8** %dst_ptr2, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i8, i8* %36, i32 %35
  store i8* %add.ptr10, i8** %dst_ptr2, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %37 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %38 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %39 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg11 = sub i32 0, %39
  %add.ptr12 = getelementptr inbounds i8, i8* %38, i32 %idx.neg11
  store i8* %add.ptr12, i8** %src_ptr1, align 4, !tbaa !2
  %40 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %41 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %42 = load i32, i32* %h.addr, align 4, !tbaa !10
  %sub = sub nsw i32 %42, 1
  %mul = mul nsw i32 %41, %sub
  %add.ptr13 = getelementptr inbounds i8, i8* %40, i32 %mul
  %43 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg14 = sub i32 0, %43
  %add.ptr15 = getelementptr inbounds i8, i8* %add.ptr13, i32 %idx.neg14
  store i8* %add.ptr15, i8** %src_ptr2, align 4, !tbaa !2
  %44 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %45 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %46 = load i32, i32* %extend_top.addr, align 4, !tbaa !10
  %sub16 = sub nsw i32 0, %46
  %mul17 = mul nsw i32 %45, %sub16
  %add.ptr18 = getelementptr inbounds i8, i8* %44, i32 %mul17
  %47 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg19 = sub i32 0, %47
  %add.ptr20 = getelementptr inbounds i8, i8* %add.ptr18, i32 %idx.neg19
  store i8* %add.ptr20, i8** %dst_ptr1, align 4, !tbaa !2
  %48 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %49 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %50 = load i32, i32* %h.addr, align 4, !tbaa !10
  %mul21 = mul nsw i32 %49, %50
  %add.ptr22 = getelementptr inbounds i8, i8* %48, i32 %mul21
  %51 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %idx.neg23 = sub i32 0, %51
  %add.ptr24 = getelementptr inbounds i8, i8* %add.ptr22, i32 %idx.neg23
  store i8* %add.ptr24, i8** %dst_ptr2, align 4, !tbaa !2
  %52 = load i32, i32* %extend_left.addr, align 4, !tbaa !10
  %53 = load i32, i32* %extend_right.addr, align 4, !tbaa !10
  %add = add nsw i32 %52, %53
  %54 = load i32, i32* %w.addr, align 4, !tbaa !10
  %add25 = add nsw i32 %add, %54
  store i32 %add25, i32* %linesize, align 4, !tbaa !10
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc31, %for.end
  %55 = load i32, i32* %i, align 4, !tbaa !10
  %56 = load i32, i32* %extend_top.addr, align 4, !tbaa !10
  %cmp27 = icmp slt i32 %55, %56
  br i1 %cmp27, label %for.body29, label %for.end33

for.body29:                                       ; preds = %for.cond26
  %57 = load i8*, i8** %dst_ptr1, align 4, !tbaa !2
  %58 = load i8*, i8** %src_ptr1, align 4, !tbaa !2
  %59 = load i32, i32* %linesize, align 4, !tbaa !10
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %57, i8* align 1 %58, i32 %59, i1 false)
  %60 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %61 = load i8*, i8** %dst_ptr1, align 4, !tbaa !2
  %add.ptr30 = getelementptr inbounds i8, i8* %61, i32 %60
  store i8* %add.ptr30, i8** %dst_ptr1, align 4, !tbaa !2
  br label %for.inc31

for.inc31:                                        ; preds = %for.body29
  %62 = load i32, i32* %i, align 4, !tbaa !10
  %inc32 = add nsw i32 %62, 1
  store i32 %inc32, i32* %i, align 4, !tbaa !10
  br label %for.cond26

for.end33:                                        ; preds = %for.cond26
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc39, %for.end33
  %63 = load i32, i32* %i, align 4, !tbaa !10
  %64 = load i32, i32* %extend_bottom.addr, align 4, !tbaa !10
  %cmp35 = icmp slt i32 %63, %64
  br i1 %cmp35, label %for.body37, label %for.end41

for.body37:                                       ; preds = %for.cond34
  %65 = load i8*, i8** %dst_ptr2, align 4, !tbaa !2
  %66 = load i8*, i8** %src_ptr2, align 4, !tbaa !2
  %67 = load i32, i32* %linesize, align 4, !tbaa !10
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %65, i8* align 1 %66, i32 %67, i1 false)
  %68 = load i32, i32* %dst_pitch.addr, align 4, !tbaa !10
  %69 = load i8*, i8** %dst_ptr2, align 4, !tbaa !2
  %add.ptr38 = getelementptr inbounds i8, i8* %69, i32 %68
  store i8* %add.ptr38, i8** %dst_ptr2, align 4, !tbaa !2
  br label %for.inc39

for.inc39:                                        ; preds = %for.body37
  %70 = load i32, i32* %i, align 4, !tbaa !10
  %inc40 = add nsw i32 %70, 1
  store i32 %inc40, i32* %i, align 4, !tbaa !10
  br label %for.cond34

for.end41:                                        ; preds = %for.cond34
  %71 = bitcast i8** %dst_ptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  %72 = bitcast i8** %dst_ptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #4
  %73 = bitcast i8** %src_ptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  %74 = bitcast i8** %src_ptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  %75 = bitcast i32* %linesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  %76 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @aom_memset16(i8*, i32, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 84}
!7 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !8, i64 52, !4, i64 56, !3, i64 68, !8, i64 72, !3, i64 76, !9, i64 80, !8, i64 84, !9, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !8, i64 128, !8, i64 132, !8, i64 136, !8, i64 140, !3, i64 144}
!8 = !{!"int", !4, i64 0}
!9 = !{!"long", !4, i64 0}
!10 = !{!8, !8, i64 0}
!11 = !{!4, !4, i64 0}
!12 = !{!7, !8, i64 140}
!13 = !{!14, !14, i64 0}
!14 = !{!"short", !4, i64 0}
