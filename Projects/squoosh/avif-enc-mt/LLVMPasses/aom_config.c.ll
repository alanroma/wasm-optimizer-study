; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc-mt/ext/aom/build.libavif/config/aom_config.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc-mt/ext/aom/build.libavif/config/aom_config.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@.str = private unnamed_addr constant [420 x i8] c"cmake ../../../../libaom -G \22Unix Makefiles\22 -DCMAKE_TOOLCHAIN_FILE=\22../../../../../../../../../../../../../../home/alan/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake\22 -DCONFIG_AV1_DECODER=0 -DCONFIG_RUNTIME_CPU_DETECT=0 -DCONFIG_WEBM_IO=0 -DCONFIG_ACCOUNTING=1 -DCONFIG_INSPECTION=0 -DCONFIG_AV1_HIGHBITDEPTH=0 -DENABLE_CCACHE=0 -DENABLE_DOCS=0 -DENABLE_EXAMPLES=0 -DENABLE_TESTS=0 -DENABLE_TOOLS=0\00", align 1

; Function Attrs: nounwind
define hidden i8* @aom_codec_build_config() #0 {
entry:
  ret i8* getelementptr inbounds ([420 x i8], [420 x i8]* @.str, i32 0, i32 0)
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
