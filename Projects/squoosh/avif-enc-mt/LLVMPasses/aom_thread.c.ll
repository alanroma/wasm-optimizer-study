; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_util/aom_thread.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_util/aom_thread.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.AVxWorkerInterface = type { void (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)* }
%struct.AVxWorker = type { %struct.AVxWorkerImpl*, i32, i8*, i32 (i8*, i8*)*, i8*, i8*, i32 }
%struct.AVxWorkerImpl = type { %struct.pthread_mutex_t, %struct.pthread_cond_t, %struct.__pthread* }
%struct.pthread_mutex_t = type { %union.anon }
%union.anon = type { [7 x i32] }
%struct.pthread_cond_t = type { %union.anon.0 }
%union.anon.0 = type { [12 x i32] }
%struct.__pthread = type opaque
%struct.pthread_mutexattr_t = type { i32 }
%struct.pthread_condattr_t = type { i32 }
%struct.pthread_attr_t = type { %union.anon.1 }
%union.anon.1 = type { [11 x i32] }

@g_worker_interface = internal global %struct.AVxWorkerInterface { void (%struct.AVxWorker*)* @init, i32 (%struct.AVxWorker*)* @reset, i32 (%struct.AVxWorker*)* @sync, void (%struct.AVxWorker*)* @launch, void (%struct.AVxWorker*)* @execute, void (%struct.AVxWorker*)* @end }, align 4

; Function Attrs: nounwind
define hidden i32 @aom_set_worker_interface(%struct.AVxWorkerInterface* %winterface) #0 {
entry:
  %retval = alloca i32, align 4
  %winterface.addr = alloca %struct.AVxWorkerInterface*, align 4
  store %struct.AVxWorkerInterface* %winterface, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.AVxWorkerInterface* %0, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %init = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %1, i32 0, i32 0
  %2 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %init, align 4, !tbaa !6
  %cmp1 = icmp eq void (%struct.AVxWorker*)* %2, null
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %3 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %reset = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %3, i32 0, i32 1
  %4 = load i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)** %reset, align 4, !tbaa !8
  %cmp3 = icmp eq i32 (%struct.AVxWorker*)* %4, null
  br i1 %cmp3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %5 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %5, i32 0, i32 2
  %6 = load i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)** %sync, align 4, !tbaa !9
  %cmp5 = icmp eq i32 (%struct.AVxWorker*)* %6, null
  br i1 %cmp5, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %lor.lhs.false4
  %7 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %launch = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %7, i32 0, i32 3
  %8 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %launch, align 4, !tbaa !10
  %cmp7 = icmp eq void (%struct.AVxWorker*)* %8, null
  br i1 %cmp7, label %if.then, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %lor.lhs.false6
  %9 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %execute = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %9, i32 0, i32 4
  %10 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %execute, align 4, !tbaa !11
  %cmp9 = icmp eq void (%struct.AVxWorker*)* %10, null
  br i1 %cmp9, label %if.then, label %lor.lhs.false10

lor.lhs.false10:                                  ; preds = %lor.lhs.false8
  %11 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %end = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %11, i32 0, i32 5
  %12 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %end, align 4, !tbaa !12
  %cmp11 = icmp eq void (%struct.AVxWorker*)* %12, null
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false10, %lor.lhs.false8, %lor.lhs.false6, %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false10
  %13 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %14 = bitcast %struct.AVxWorkerInterface* %13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 bitcast (%struct.AVxWorkerInterface* @g_worker_interface to i8*), i8* align 4 %14, i32 24, i1 false), !tbaa.struct !13
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden %struct.AVxWorkerInterface* @aom_get_worker_interface() #0 {
entry:
  ret %struct.AVxWorkerInterface* @g_worker_interface
}

; Function Attrs: nounwind
define internal void @init(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %1 = bitcast %struct.AVxWorker* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 28, i1 false)
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %2, i32 0, i32 1
  store i32 0, i32* %status_, align 4, !tbaa !14
  ret void
}

; Function Attrs: nounwind
define internal i32 @reset(%struct.AVxWorker* %worker) #0 {
entry:
  %retval = alloca i32, align 4
  %worker.addr = alloca %struct.AVxWorker*, align 4
  %ok = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ok to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 1, i32* %ok, align 4, !tbaa !17
  %1 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %had_error = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %1, i32 0, i32 6
  store i32 0, i32* %had_error, align 4, !tbaa !18
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %2, i32 0, i32 1
  %3 = load i32, i32* %status_, align 4, !tbaa !14
  %cmp = icmp ult i32 %3, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call = call i8* @aom_calloc(i32 1, i32 80)
  %4 = bitcast i8* %call to %struct.AVxWorkerImpl*
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %5, i32 0, i32 0
  store %struct.AVxWorkerImpl* %4, %struct.AVxWorkerImpl** %impl_, align 4, !tbaa !19
  %6 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %6, i32 0, i32 0
  %7 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_1, align 4, !tbaa !19
  %cmp2 = icmp eq %struct.AVxWorkerImpl* %7, null
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_4 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %8, i32 0, i32 0
  %9 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_4, align 4, !tbaa !19
  %mutex_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %9, i32 0, i32 0
  %call5 = call i32 @pthread_mutex_init(%struct.pthread_mutex_t* %mutex_, %struct.pthread_mutexattr_t* null)
  %tobool = icmp ne i32 %call5, 0
  br i1 %tobool, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  br label %Error

if.end7:                                          ; preds = %if.end
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_8 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %10, i32 0, i32 0
  %11 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_8, align 4, !tbaa !19
  %condition_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %11, i32 0, i32 1
  %call9 = call i32 @pthread_cond_init(%struct.pthread_cond_t* %condition_, %struct.pthread_condattr_t* null)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.then11, label %if.end15

if.then11:                                        ; preds = %if.end7
  %12 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_12 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %12, i32 0, i32 0
  %13 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_12, align 4, !tbaa !19
  %mutex_13 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %13, i32 0, i32 0
  %call14 = call i32 @pthread_mutex_destroy(%struct.pthread_mutex_t* %mutex_13)
  br label %Error

if.end15:                                         ; preds = %if.end7
  %14 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_16 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %14, i32 0, i32 0
  %15 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_16, align 4, !tbaa !19
  %mutex_17 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %15, i32 0, i32 0
  %call18 = call i32 @pthread_mutex_lock(%struct.pthread_mutex_t* %mutex_17)
  %16 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_19 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %16, i32 0, i32 0
  %17 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_19, align 4, !tbaa !19
  %thread_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %17, i32 0, i32 2
  %18 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %19 = bitcast %struct.AVxWorker* %18 to i8*
  %call20 = call i32 @pthread_create(%struct.__pthread** %thread_, %struct.pthread_attr_t* null, i8* (i8*)* @thread_loop, i8* %19)
  %tobool21 = icmp ne i32 %call20, 0
  %lnot = xor i1 %tobool21, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %ok, align 4, !tbaa !17
  %20 = load i32, i32* %ok, align 4, !tbaa !17
  %tobool22 = icmp ne i32 %20, 0
  br i1 %tobool22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.end15
  %21 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_24 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %21, i32 0, i32 1
  store i32 1, i32* %status_24, align 4, !tbaa !14
  br label %if.end25

if.end25:                                         ; preds = %if.then23, %if.end15
  %22 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_26 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %22, i32 0, i32 0
  %23 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_26, align 4, !tbaa !19
  %mutex_27 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %23, i32 0, i32 0
  %call28 = call i32 @pthread_mutex_unlock(%struct.pthread_mutex_t* %mutex_27)
  %24 = load i32, i32* %ok, align 4, !tbaa !17
  %tobool29 = icmp ne i32 %24, 0
  br i1 %tobool29, label %if.end39, label %if.then30

if.then30:                                        ; preds = %if.end25
  %25 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_31 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %25, i32 0, i32 0
  %26 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_31, align 4, !tbaa !19
  %mutex_32 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %26, i32 0, i32 0
  %call33 = call i32 @pthread_mutex_destroy(%struct.pthread_mutex_t* %mutex_32)
  %27 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_34 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %27, i32 0, i32 0
  %28 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_34, align 4, !tbaa !19
  %condition_35 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %28, i32 0, i32 1
  %call36 = call i32 @pthread_cond_destroy(%struct.pthread_cond_t* %condition_35)
  br label %Error

Error:                                            ; preds = %if.then30, %if.then11, %if.then6
  %29 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_37 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %29, i32 0, i32 0
  %30 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_37, align 4, !tbaa !19
  %31 = bitcast %struct.AVxWorkerImpl* %30 to i8*
  call void @aom_free(i8* %31)
  %32 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_38 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %32, i32 0, i32 0
  store %struct.AVxWorkerImpl* null, %struct.AVxWorkerImpl** %impl_38, align 4, !tbaa !19
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end39:                                         ; preds = %if.end25
  br label %if.end45

if.else:                                          ; preds = %entry
  %33 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_40 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %33, i32 0, i32 1
  %34 = load i32, i32* %status_40, align 4, !tbaa !14
  %cmp41 = icmp ugt i32 %34, 1
  br i1 %cmp41, label %if.then42, label %if.end44

if.then42:                                        ; preds = %if.else
  %35 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %call43 = call i32 @sync(%struct.AVxWorker* %35)
  store i32 %call43, i32* %ok, align 4, !tbaa !17
  br label %if.end44

if.end44:                                         ; preds = %if.then42, %if.else
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.end39
  %36 = load i32, i32* %ok, align 4, !tbaa !17
  store i32 %36, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end45, %Error, %if.then3
  %37 = bitcast i32* %ok to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %38 = load i32, i32* %retval, align 4
  ret i32 %38
}

; Function Attrs: nounwind
define internal i32 @sync(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  call void @change_state(%struct.AVxWorker* %0, i32 1)
  %1 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %had_error = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %1, i32 0, i32 6
  %2 = load i32, i32* %had_error, align 4, !tbaa !18
  %tobool = icmp ne i32 %2, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  ret i32 %lnot.ext
}

; Function Attrs: nounwind
define internal void @launch(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  call void @change_state(%struct.AVxWorker* %0, i32 2)
  ret void
}

; Function Attrs: nounwind
define internal void @execute(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %hook = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %0, i32 0, i32 3
  %1 = load i32 (i8*, i8*)*, i32 (i8*, i8*)** %hook, align 4, !tbaa !20
  %cmp = icmp ne i32 (i8*, i8*)* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %hook1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %2, i32 0, i32 3
  %3 = load i32 (i8*, i8*)*, i32 (i8*, i8*)** %hook1, align 4, !tbaa !20
  %4 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %4, i32 0, i32 4
  %5 = load i8*, i8** %data1, align 4, !tbaa !21
  %6 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %data2 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %6, i32 0, i32 5
  %7 = load i8*, i8** %data2, align 4, !tbaa !22
  %call = call i32 %3(i8* %5, i8* %7)
  %tobool = icmp ne i32 %call, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %had_error = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %8, i32 0, i32 6
  %9 = load i32, i32* %had_error, align 4, !tbaa !18
  %or = or i32 %9, %lnot.ext
  store i32 %or, i32* %had_error, align 4, !tbaa !18
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define internal void @end(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %0, i32 0, i32 0
  %1 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_, align 4, !tbaa !19
  %cmp = icmp ne %struct.AVxWorkerImpl* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  call void @change_state(%struct.AVxWorker* %2, i32 0)
  %3 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %3, i32 0, i32 0
  %4 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_1, align 4, !tbaa !19
  %thread_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %4, i32 0, i32 2
  %5 = load %struct.__pthread*, %struct.__pthread** %thread_, align 4, !tbaa !23
  %call = call i32 @pthread_join(%struct.__pthread* %5, i8** null)
  %6 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_2 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %6, i32 0, i32 0
  %7 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_2, align 4, !tbaa !19
  %mutex_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %7, i32 0, i32 0
  %call3 = call i32 @pthread_mutex_destroy(%struct.pthread_mutex_t* %mutex_)
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_4 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %8, i32 0, i32 0
  %9 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_4, align 4, !tbaa !19
  %condition_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %9, i32 0, i32 1
  %call5 = call i32 @pthread_cond_destroy(%struct.pthread_cond_t* %condition_)
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_6 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %10, i32 0, i32 0
  %11 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_6, align 4, !tbaa !19
  %12 = bitcast %struct.AVxWorkerImpl* %11 to i8*
  call void @aom_free(i8* %12)
  %13 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_7 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %13, i32 0, i32 0
  store %struct.AVxWorkerImpl* null, %struct.AVxWorkerImpl** %impl_7, align 4, !tbaa !19
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @aom_calloc(i32, i32) #3

declare i32 @pthread_mutex_init(%struct.pthread_mutex_t*, %struct.pthread_mutexattr_t*) #3

declare i32 @pthread_cond_init(%struct.pthread_cond_t*, %struct.pthread_condattr_t*) #3

declare i32 @pthread_mutex_destroy(%struct.pthread_mutex_t*) #3

declare i32 @pthread_mutex_lock(%struct.pthread_mutex_t*) #3

declare i32 @pthread_create(%struct.__pthread**, %struct.pthread_attr_t*, i8* (i8*)*, i8*) #3

; Function Attrs: nounwind
define internal i8* @thread_loop(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %done = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.AVxWorker*
  store %struct.AVxWorker* %2, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %3 = bitcast i32* %done to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 0, i32* %done, align 4, !tbaa !17
  br label %while.cond

while.cond:                                       ; preds = %if.end13, %entry
  %4 = load i32, i32* %done, align 4, !tbaa !17
  %tobool = icmp ne i32 %4, 0
  %lnot = xor i1 %tobool, true
  br i1 %lnot, label %while.body, label %while.end20

while.body:                                       ; preds = %while.cond
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %impl_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %5, i32 0, i32 0
  %6 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_, align 4, !tbaa !19
  %mutex_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %6, i32 0, i32 0
  %call = call i32 @pthread_mutex_lock(%struct.pthread_mutex_t* %mutex_)
  br label %while.cond1

while.cond1:                                      ; preds = %while.body2, %while.body
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %status_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %7, i32 0, i32 1
  %8 = load i32, i32* %status_, align 4, !tbaa !14
  %cmp = icmp eq i32 %8, 1
  br i1 %cmp, label %while.body2, label %while.end

while.body2:                                      ; preds = %while.cond1
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %impl_3 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 0
  %10 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_3, align 4, !tbaa !19
  %condition_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %10, i32 0, i32 1
  %11 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %impl_4 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %11, i32 0, i32 0
  %12 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_4, align 4, !tbaa !19
  %mutex_5 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %12, i32 0, i32 0
  %call6 = call i32 @pthread_cond_wait(%struct.pthread_cond_t* %condition_, %struct.pthread_mutex_t* %mutex_5)
  br label %while.cond1

while.end:                                        ; preds = %while.cond1
  %13 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %status_7 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %13, i32 0, i32 1
  %14 = load i32, i32* %status_7, align 4, !tbaa !14
  %cmp8 = icmp eq i32 %14, 2
  br i1 %cmp8, label %if.then, label %if.else

if.then:                                          ; preds = %while.end
  %15 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  call void @execute(%struct.AVxWorker* %15)
  %16 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %status_9 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %16, i32 0, i32 1
  store i32 1, i32* %status_9, align 4, !tbaa !14
  br label %if.end13

if.else:                                          ; preds = %while.end
  %17 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %status_10 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %17, i32 0, i32 1
  %18 = load i32, i32* %status_10, align 4, !tbaa !14
  %cmp11 = icmp eq i32 %18, 0
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.else
  store i32 1, i32* %done, align 4, !tbaa !17
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end, %if.then
  %19 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %impl_14 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %19, i32 0, i32 0
  %20 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_14, align 4, !tbaa !19
  %condition_15 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %20, i32 0, i32 1
  %call16 = call i32 @pthread_cond_signal(%struct.pthread_cond_t* %condition_15)
  %21 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %impl_17 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %21, i32 0, i32 0
  %22 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_17, align 4, !tbaa !19
  %mutex_18 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %22, i32 0, i32 0
  %call19 = call i32 @pthread_mutex_unlock(%struct.pthread_mutex_t* %mutex_18)
  br label %while.cond

while.end20:                                      ; preds = %while.cond
  %23 = bitcast i32* %done to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %24 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  ret i8* null
}

declare i32 @pthread_mutex_unlock(%struct.pthread_mutex_t*) #3

declare i32 @pthread_cond_destroy(%struct.pthread_cond_t*) #3

declare void @aom_free(i8*) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare i32 @pthread_cond_wait(%struct.pthread_cond_t*, %struct.pthread_mutex_t*) #3

declare i32 @pthread_cond_signal(%struct.pthread_cond_t*) #3

; Function Attrs: nounwind
define internal void @change_state(%struct.AVxWorker* %worker, i32 %new_status) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  %new_status.addr = alloca i32, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  store i32 %new_status, i32* %new_status.addr, align 4, !tbaa !26
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %0, i32 0, i32 0
  %1 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_, align 4, !tbaa !19
  %cmp = icmp eq %struct.AVxWorkerImpl* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %2, i32 0, i32 0
  %3 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_1, align 4, !tbaa !19
  %mutex_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %3, i32 0, i32 0
  %call = call i32 @pthread_mutex_lock(%struct.pthread_mutex_t* %mutex_)
  %4 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %4, i32 0, i32 1
  %5 = load i32, i32* %status_, align 4, !tbaa !14
  %cmp2 = icmp uge i32 %5, 1
  br i1 %cmp2, label %if.then3, label %if.end17

if.then3:                                         ; preds = %if.end
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then3
  %6 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_4 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %6, i32 0, i32 1
  %7 = load i32, i32* %status_4, align 4, !tbaa !14
  %cmp5 = icmp ne i32 %7, 1
  br i1 %cmp5, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_6 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %8, i32 0, i32 0
  %9 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_6, align 4, !tbaa !19
  %condition_ = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %9, i32 0, i32 1
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_7 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %10, i32 0, i32 0
  %11 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_7, align 4, !tbaa !19
  %mutex_8 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %11, i32 0, i32 0
  %call9 = call i32 @pthread_cond_wait(%struct.pthread_cond_t* %condition_, %struct.pthread_mutex_t* %mutex_8)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %12 = load i32, i32* %new_status.addr, align 4, !tbaa !26
  %cmp10 = icmp ne i32 %12, 1
  br i1 %cmp10, label %if.then11, label %if.end16

if.then11:                                        ; preds = %while.end
  %13 = load i32, i32* %new_status.addr, align 4, !tbaa !26
  %14 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_12 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %14, i32 0, i32 1
  store i32 %13, i32* %status_12, align 4, !tbaa !14
  %15 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_13 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %15, i32 0, i32 0
  %16 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_13, align 4, !tbaa !19
  %condition_14 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %16, i32 0, i32 1
  %call15 = call i32 @pthread_cond_signal(%struct.pthread_cond_t* %condition_14)
  br label %if.end16

if.end16:                                         ; preds = %if.then11, %while.end
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.end
  %17 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %impl_18 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %17, i32 0, i32 0
  %18 = load %struct.AVxWorkerImpl*, %struct.AVxWorkerImpl** %impl_18, align 4, !tbaa !19
  %mutex_19 = getelementptr inbounds %struct.AVxWorkerImpl, %struct.AVxWorkerImpl* %18, i32 0, i32 0
  %call20 = call i32 @pthread_mutex_unlock(%struct.pthread_mutex_t* %mutex_19)
  br label %return

return:                                           ; preds = %if.end17, %if.then
  ret void
}

declare i32 @pthread_join(%struct.__pthread*, i8**) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 0}
!7 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20}
!8 = !{!7, !3, i64 4}
!9 = !{!7, !3, i64 8}
!10 = !{!7, !3, i64 12}
!11 = !{!7, !3, i64 16}
!12 = !{!7, !3, i64 20}
!13 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 16, i64 4, !2, i64 20, i64 4, !2}
!14 = !{!15, !4, i64 4}
!15 = !{!"", !3, i64 0, !4, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !16, i64 24}
!16 = !{!"int", !4, i64 0}
!17 = !{!16, !16, i64 0}
!18 = !{!15, !16, i64 24}
!19 = !{!15, !3, i64 0}
!20 = !{!15, !3, i64 12}
!21 = !{!15, !3, i64 16}
!22 = !{!15, !3, i64 20}
!23 = !{!24, !3, i64 76}
!24 = !{!"AVxWorkerImpl", !25, i64 0, !25, i64 28, !3, i64 76}
!25 = !{!"", !4, i64 0}
!26 = !{!4, !4, i64 0}
