; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/palette.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/palette.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }

; Function Attrs: nounwind
define hidden void @av1_calc_indices_dim1(i32* %data, i32* %centroids, i8* %indices, i32 %n, i32 %k) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %centroids.addr = alloca i32*, align 4
  %indices.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %k.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %min_dist = alloca i32, align 4
  %j = alloca i32, align 4
  %this_dist = alloca i32, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  store i32* %centroids, i32** %centroids.addr, align 4, !tbaa !2
  store i8* %indices, i8** %indices.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %k, i32* %k.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end14

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %min_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul nsw i32 %6, 1
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %mul
  %7 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %call = call i32 @calc_dist_dim1(i32* %add.ptr, i32* %7)
  store i32 %call, i32* %min_dist, align 4, !tbaa !6
  %8 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  store i8 0, i8* %arrayidx, align 1, !tbaa !8
  %10 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store i32 1, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %12 = load i32, i32* %k.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %11, %12
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %14 = bitcast i32* %this_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %mul5 = mul nsw i32 %16, 1
  %add.ptr6 = getelementptr inbounds i32, i32* %15, i32 %mul5
  %17 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %mul7 = mul nsw i32 %18, 1
  %add.ptr8 = getelementptr inbounds i32, i32* %17, i32 %mul7
  %call9 = call i32 @calc_dist_dim1(i32* %add.ptr6, i32* %add.ptr8)
  store i32 %call9, i32* %this_dist, align 4, !tbaa !6
  %19 = load i32, i32* %this_dist, align 4, !tbaa !6
  %20 = load i32, i32* %min_dist, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %19, %20
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %for.body4
  %21 = load i32, i32* %this_dist, align 4, !tbaa !6
  store i32 %21, i32* %min_dist, align 4, !tbaa !6
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %conv = trunc i32 %22 to i8
  %23 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds i8, i8* %23, i32 %24
  store i8 %conv, i8* %arrayidx11, align 1, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body4
  %25 = bitcast i32* %this_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %27 = bitcast i32* %min_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #6
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %inc13 = add nsw i32 %28, 1
  store i32 %inc13, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end14:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @calc_dist_dim1(i32* %p1, i32* %p2) #0 {
entry:
  %p1.addr = alloca i32*, align 4
  %p2.addr = alloca i32*, align 4
  %dist = alloca i32, align 4
  %i = alloca i32, align 4
  %diff = alloca i32, align 4
  store i32* %p1, i32** %p1.addr, align 4, !tbaa !2
  store i32* %p2, i32** %p2.addr, align 4, !tbaa !2
  %0 = bitcast i32* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %dist, align 4, !tbaa !6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 1
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32*, i32** %p1.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  %7 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %8 = load i32*, i32** %p2.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %sub = sub nsw i32 %7, %10
  store i32 %sub, i32* %diff, align 4, !tbaa !6
  %11 = load i32, i32* %diff, align 4, !tbaa !6
  %12 = load i32, i32* %diff, align 4, !tbaa !6
  %mul = mul nsw i32 %11, %12
  %13 = load i32, i32* %dist, align 4, !tbaa !6
  %add = add nsw i32 %13, %mul
  store i32 %add, i32* %dist, align 4, !tbaa !6
  %14 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %16 = load i32, i32* %dist, align 4, !tbaa !6
  %17 = bitcast i32* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret i32 %16
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_k_means_dim1(i32* %data, i32* %centroids, i8* %indices, i32 %n, i32 %k, i32 %max_itr) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %centroids.addr = alloca i32*, align 4
  %indices.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %k.addr = alloca i32, align 4
  %max_itr.addr = alloca i32, align 4
  %pre_centroids = alloca [16 x i32], align 16
  %pre_indices = alloca [16384 x i8], align 16
  %this_dist = alloca i64, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pre_dist = alloca i64, align 8
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  store i32* %centroids, i32** %centroids.addr, align 4, !tbaa !2
  store i8* %indices, i8** %indices.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %k, i32* %k.addr, align 4, !tbaa !6
  store i32 %max_itr, i32* %max_itr.addr, align 4, !tbaa !6
  %0 = bitcast [16 x i32]* %pre_centroids to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #6
  %1 = bitcast [16384 x i8]* %pre_indices to i8*
  call void @llvm.lifetime.start.p0i8(i64 16384, i8* %1) #6
  %2 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %3 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %5 = load i32, i32* %n.addr, align 4, !tbaa !6
  %6 = load i32, i32* %k.addr, align 4, !tbaa !6
  call void @av1_calc_indices_dim1(i32* %2, i32* %3, i8* %4, i32 %5, i32 %6)
  %7 = bitcast i64* %this_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #6
  %8 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %9 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %11 = load i32, i32* %n.addr, align 4, !tbaa !6
  %12 = load i32, i32* %k.addr, align 4, !tbaa !6
  %call = call i64 @calc_total_dist_dim1(i32* %8, i32* %9, i8* %10, i32 %11, i32 %12)
  store i64 %call, i64* %this_dist, align 8, !tbaa !9
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %max_itr.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %14, %15
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

for.body:                                         ; preds = %for.cond
  %16 = bitcast i64* %pre_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %16) #6
  %17 = load i64, i64* %this_dist, align 8, !tbaa !9
  store i64 %17, i64* %pre_dist, align 8, !tbaa !9
  %arraydecay = getelementptr inbounds [16 x i32], [16 x i32]* %pre_centroids, i32 0, i32 0
  %18 = bitcast i32* %arraydecay to i8*
  %19 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %20 = bitcast i32* %19 to i8*
  %21 = load i32, i32* %k.addr, align 4, !tbaa !6
  %mul = mul i32 4, %21
  %mul1 = mul i32 %mul, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %18, i8* align 4 %20, i32 %mul1, i1 false)
  %arraydecay2 = getelementptr inbounds [16384 x i8], [16384 x i8]* %pre_indices, i32 0, i32 0
  %22 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %23 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul3 = mul i32 1, %23
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay2, i8* align 1 %22, i32 %mul3, i1 false)
  %24 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %25 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %26 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %27 = load i32, i32* %n.addr, align 4, !tbaa !6
  %28 = load i32, i32* %k.addr, align 4, !tbaa !6
  call void @calc_centroids_dim1(i32* %24, i32* %25, i8* %26, i32 %27, i32 %28)
  %29 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %30 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %31 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %32 = load i32, i32* %n.addr, align 4, !tbaa !6
  %33 = load i32, i32* %k.addr, align 4, !tbaa !6
  call void @av1_calc_indices_dim1(i32* %29, i32* %30, i8* %31, i32 %32, i32 %33)
  %34 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %35 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %36 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %37 = load i32, i32* %n.addr, align 4, !tbaa !6
  %38 = load i32, i32* %k.addr, align 4, !tbaa !6
  %call4 = call i64 @calc_total_dist_dim1(i32* %34, i32* %35, i8* %36, i32 %37, i32 %38)
  store i64 %call4, i64* %this_dist, align 8, !tbaa !9
  %39 = load i64, i64* %this_dist, align 8, !tbaa !9
  %40 = load i64, i64* %pre_dist, align 8, !tbaa !9
  %cmp5 = icmp sgt i64 %39, %40
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %41 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %42 = bitcast i32* %41 to i8*
  %arraydecay6 = getelementptr inbounds [16 x i32], [16 x i32]* %pre_centroids, i32 0, i32 0
  %43 = bitcast i32* %arraydecay6 to i8*
  %44 = load i32, i32* %k.addr, align 4, !tbaa !6
  %mul7 = mul i32 4, %44
  %mul8 = mul i32 %mul7, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 16 %43, i32 %mul8, i1 false)
  %45 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %arraydecay9 = getelementptr inbounds [16384 x i8], [16384 x i8]* %pre_indices, i32 0, i32 0
  %46 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul10 = mul i32 1, %46
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %45, i8* align 16 %arraydecay9, i32 %mul10, i1 false)
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %47 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %48 = bitcast i32* %47 to i8*
  %arraydecay11 = getelementptr inbounds [16 x i32], [16 x i32]* %pre_centroids, i32 0, i32 0
  %49 = bitcast i32* %arraydecay11 to i8*
  %50 = load i32, i32* %k.addr, align 4, !tbaa !6
  %mul12 = mul i32 4, %50
  %mul13 = mul i32 %mul12, 1
  %call14 = call i32 @memcmp(i8* %48, i8* %49, i32 %mul13)
  %tobool = icmp ne i32 %call14, 0
  br i1 %tobool, label %if.end16, label %if.then15

if.then15:                                        ; preds = %if.end
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.then15, %if.then
  %51 = bitcast i64* %pre_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %51) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup17 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %52 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup17:                                        ; preds = %cleanup, %for.cond.cleanup
  %53 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  br label %for.end

for.end:                                          ; preds = %cleanup17
  %54 = bitcast i64* %this_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %54) #6
  %55 = bitcast [16384 x i8]* %pre_indices to i8*
  call void @llvm.lifetime.end.p0i8(i64 16384, i8* %55) #6
  %56 = bitcast [16 x i32]* %pre_centroids to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %56) #6
  ret void
}

; Function Attrs: nounwind
define internal i64 @calc_total_dist_dim1(i32* %data, i32* %centroids, i8* %indices, i32 %n, i32 %k) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %centroids.addr = alloca i32*, align 4
  %indices.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %k.addr = alloca i32, align 4
  %dist = alloca i64, align 8
  %i = alloca i32, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  store i32* %centroids, i32** %centroids.addr, align 4, !tbaa !2
  store i8* %indices, i8** %indices.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %k, i32* %k.addr, align 4, !tbaa !6
  %0 = bitcast i64* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  store i64 0, i64* %dist, align 8, !tbaa !9
  %1 = load i32, i32* %k.addr, align 4, !tbaa !6
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul nsw i32 %7, 1
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %mul
  %8 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %11 to i32
  %mul1 = mul nsw i32 %conv, 1
  %add.ptr2 = getelementptr inbounds i32, i32* %8, i32 %mul1
  %call = call i32 @calc_dist_dim1(i32* %add.ptr, i32* %add.ptr2)
  %conv3 = sext i32 %call to i64
  %12 = load i64, i64* %dist, align 8, !tbaa !9
  %add = add nsw i64 %12, %conv3
  store i64 %add, i64* %dist, align 8, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = load i64, i64* %dist, align 8, !tbaa !9
  %15 = bitcast i64* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #6
  ret i64 %14
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal void @calc_centroids_dim1(i32* %data, i32* %centroids, i8* %indices, i32 %n, i32 %k) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %centroids.addr = alloca i32*, align 4
  %indices.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %k.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %count = alloca [8 x i32], align 16
  %rand_state = alloca i32, align 4
  %index = alloca i32, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  store i32* %centroids, i32** %centroids.addr, align 4, !tbaa !2
  store i8* %indices, i8** %indices.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %k, i32* %k.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast [8 x i32]* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %2) #6
  %3 = bitcast [8 x i32]* %count to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %3, i8 0, i32 32, i1 false)
  %4 = bitcast i32* %rand_state to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 0
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %6, i32* %rand_state, align 4, !tbaa !6
  %7 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %8 = bitcast i32* %7 to i8*
  %9 = load i32, i32* %k.addr, align 4, !tbaa !6
  %mul = mul i32 4, %9
  %mul1 = mul i32 %mul, 1
  call void @llvm.memset.p0i8.i32(i8* align 4 %8, i8 0, i32 %mul1, i1 false)
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8, i8* %13, i32 %14
  %15 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv = zext i8 %15 to i32
  store i32 %conv, i32* %index, align 4, !tbaa !6
  %16 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [8 x i32], [8 x i32]* %count, i32 0, i32 %16
  %17 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %arrayidx3, align 4, !tbaa !6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %18, 1
  br i1 %cmp5, label %for.body7, label %for.end

for.body7:                                        ; preds = %for.cond4
  %19 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %mul8 = mul nsw i32 %20, 1
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul8, %21
  %arrayidx9 = getelementptr inbounds i32, i32* %19, i32 %add
  %22 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %23 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %24 = load i32, i32* %index, align 4, !tbaa !6
  %mul10 = mul nsw i32 %24, 1
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %add11 = add nsw i32 %mul10, %25
  %arrayidx12 = getelementptr inbounds i32, i32* %23, i32 %add11
  %26 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %add13 = add nsw i32 %26, %22
  store i32 %add13, i32* %arrayidx12, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body7
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %inc14 = add nsw i32 %27, 1
  store i32 %inc14, i32* %j, align 4, !tbaa !6
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %28 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %inc16 = add nsw i32 %29, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc44, %for.end17
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %31 = load i32, i32* %k.addr, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %30, %31
  br i1 %cmp19, label %for.body21, label %for.end46

for.body21:                                       ; preds = %for.cond18
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [8 x i32], [8 x i32]* %count, i32 0, i32 %32
  %33 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  %cmp23 = icmp eq i32 %33, 0
  br i1 %cmp23, label %if.then, label %if.else

if.then:                                          ; preds = %for.body21
  %34 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %mul25 = mul nsw i32 %35, 1
  %add.ptr = getelementptr inbounds i32, i32* %34, i32 %mul25
  %36 = bitcast i32* %add.ptr to i8*
  %37 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %call = call i32 @lcg_rand16(i32* %rand_state)
  %38 = load i32, i32* %n.addr, align 4, !tbaa !6
  %rem = urem i32 %call, %38
  %mul26 = mul i32 %rem, 1
  %add.ptr27 = getelementptr inbounds i32, i32* %37, i32 %mul26
  %39 = bitcast i32* %add.ptr27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %39, i32 4, i1 false)
  br label %if.end

if.else:                                          ; preds = %for.body21
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc41, %if.else
  %40 = load i32, i32* %j, align 4, !tbaa !6
  %cmp29 = icmp slt i32 %40, 1
  br i1 %cmp29, label %for.body31, label %for.end43

for.body31:                                       ; preds = %for.cond28
  %41 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %mul32 = mul nsw i32 %42, 1
  %43 = load i32, i32* %j, align 4, !tbaa !6
  %add33 = add nsw i32 %mul32, %43
  %arrayidx34 = getelementptr inbounds i32, i32* %41, i32 %add33
  %44 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds [8 x i32], [8 x i32]* %count, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %shr = ashr i32 %46, 1
  %add36 = add nsw i32 %44, %shr
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [8 x i32], [8 x i32]* %count, i32 0, i32 %47
  %48 = load i32, i32* %arrayidx37, align 4, !tbaa !6
  %div = sdiv i32 %add36, %48
  %49 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %mul38 = mul nsw i32 %50, 1
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %add39 = add nsw i32 %mul38, %51
  %arrayidx40 = getelementptr inbounds i32, i32* %49, i32 %add39
  store i32 %div, i32* %arrayidx40, align 4, !tbaa !6
  br label %for.inc41

for.inc41:                                        ; preds = %for.body31
  %52 = load i32, i32* %j, align 4, !tbaa !6
  %inc42 = add nsw i32 %52, 1
  store i32 %inc42, i32* %j, align 4, !tbaa !6
  br label %for.cond28

for.end43:                                        ; preds = %for.cond28
  br label %if.end

if.end:                                           ; preds = %for.end43, %if.then
  br label %for.inc44

for.inc44:                                        ; preds = %if.end
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %inc45 = add nsw i32 %53, 1
  store i32 %inc45, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.end46:                                        ; preds = %for.cond18
  %54 = bitcast i32* %rand_state to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast [8 x i32]* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %55) #6
  %56 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  %57 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  ret void
}

declare i32 @memcmp(i8*, i8*, i32) #2

; Function Attrs: nounwind
define hidden void @av1_calc_indices_dim2(i32* %data, i32* %centroids, i8* %indices, i32 %n, i32 %k) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %centroids.addr = alloca i32*, align 4
  %indices.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %k.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %min_dist = alloca i32, align 4
  %j = alloca i32, align 4
  %this_dist = alloca i32, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  store i32* %centroids, i32** %centroids.addr, align 4, !tbaa !2
  store i8* %indices, i8** %indices.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %k, i32* %k.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end14

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %min_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul nsw i32 %6, 2
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %mul
  %7 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %call = call i32 @calc_dist_dim2(i32* %add.ptr, i32* %7)
  store i32 %call, i32* %min_dist, align 4, !tbaa !6
  %8 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  store i8 0, i8* %arrayidx, align 1, !tbaa !8
  %10 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store i32 1, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %12 = load i32, i32* %k.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %11, %12
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %14 = bitcast i32* %this_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %mul5 = mul nsw i32 %16, 2
  %add.ptr6 = getelementptr inbounds i32, i32* %15, i32 %mul5
  %17 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %mul7 = mul nsw i32 %18, 2
  %add.ptr8 = getelementptr inbounds i32, i32* %17, i32 %mul7
  %call9 = call i32 @calc_dist_dim2(i32* %add.ptr6, i32* %add.ptr8)
  store i32 %call9, i32* %this_dist, align 4, !tbaa !6
  %19 = load i32, i32* %this_dist, align 4, !tbaa !6
  %20 = load i32, i32* %min_dist, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %19, %20
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %for.body4
  %21 = load i32, i32* %this_dist, align 4, !tbaa !6
  store i32 %21, i32* %min_dist, align 4, !tbaa !6
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %conv = trunc i32 %22 to i8
  %23 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds i8, i8* %23, i32 %24
  store i8 %conv, i8* %arrayidx11, align 1, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body4
  %25 = bitcast i32* %this_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %27 = bitcast i32* %min_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #6
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %inc13 = add nsw i32 %28, 1
  store i32 %inc13, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end14:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal i32 @calc_dist_dim2(i32* %p1, i32* %p2) #0 {
entry:
  %p1.addr = alloca i32*, align 4
  %p2.addr = alloca i32*, align 4
  %dist = alloca i32, align 4
  %i = alloca i32, align 4
  %diff = alloca i32, align 4
  store i32* %p1, i32** %p1.addr, align 4, !tbaa !2
  store i32* %p2, i32** %p2.addr, align 4, !tbaa !2
  %0 = bitcast i32* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %dist, align 4, !tbaa !6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32*, i32** %p1.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  %7 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %8 = load i32*, i32** %p2.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %sub = sub nsw i32 %7, %10
  store i32 %sub, i32* %diff, align 4, !tbaa !6
  %11 = load i32, i32* %diff, align 4, !tbaa !6
  %12 = load i32, i32* %diff, align 4, !tbaa !6
  %mul = mul nsw i32 %11, %12
  %13 = load i32, i32* %dist, align 4, !tbaa !6
  %add = add nsw i32 %13, %mul
  store i32 %add, i32* %dist, align 4, !tbaa !6
  %14 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %16 = load i32, i32* %dist, align 4, !tbaa !6
  %17 = bitcast i32* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret i32 %16
}

; Function Attrs: nounwind
define hidden void @av1_k_means_dim2(i32* %data, i32* %centroids, i8* %indices, i32 %n, i32 %k, i32 %max_itr) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %centroids.addr = alloca i32*, align 4
  %indices.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %k.addr = alloca i32, align 4
  %max_itr.addr = alloca i32, align 4
  %pre_centroids = alloca [16 x i32], align 16
  %pre_indices = alloca [16384 x i8], align 16
  %this_dist = alloca i64, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pre_dist = alloca i64, align 8
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  store i32* %centroids, i32** %centroids.addr, align 4, !tbaa !2
  store i8* %indices, i8** %indices.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %k, i32* %k.addr, align 4, !tbaa !6
  store i32 %max_itr, i32* %max_itr.addr, align 4, !tbaa !6
  %0 = bitcast [16 x i32]* %pre_centroids to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #6
  %1 = bitcast [16384 x i8]* %pre_indices to i8*
  call void @llvm.lifetime.start.p0i8(i64 16384, i8* %1) #6
  %2 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %3 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %5 = load i32, i32* %n.addr, align 4, !tbaa !6
  %6 = load i32, i32* %k.addr, align 4, !tbaa !6
  call void @av1_calc_indices_dim2(i32* %2, i32* %3, i8* %4, i32 %5, i32 %6)
  %7 = bitcast i64* %this_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #6
  %8 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %9 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %11 = load i32, i32* %n.addr, align 4, !tbaa !6
  %12 = load i32, i32* %k.addr, align 4, !tbaa !6
  %call = call i64 @calc_total_dist_dim2(i32* %8, i32* %9, i8* %10, i32 %11, i32 %12)
  store i64 %call, i64* %this_dist, align 8, !tbaa !9
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %max_itr.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %14, %15
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

for.body:                                         ; preds = %for.cond
  %16 = bitcast i64* %pre_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %16) #6
  %17 = load i64, i64* %this_dist, align 8, !tbaa !9
  store i64 %17, i64* %pre_dist, align 8, !tbaa !9
  %arraydecay = getelementptr inbounds [16 x i32], [16 x i32]* %pre_centroids, i32 0, i32 0
  %18 = bitcast i32* %arraydecay to i8*
  %19 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %20 = bitcast i32* %19 to i8*
  %21 = load i32, i32* %k.addr, align 4, !tbaa !6
  %mul = mul i32 4, %21
  %mul1 = mul i32 %mul, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %18, i8* align 4 %20, i32 %mul1, i1 false)
  %arraydecay2 = getelementptr inbounds [16384 x i8], [16384 x i8]* %pre_indices, i32 0, i32 0
  %22 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %23 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul3 = mul i32 1, %23
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay2, i8* align 1 %22, i32 %mul3, i1 false)
  %24 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %25 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %26 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %27 = load i32, i32* %n.addr, align 4, !tbaa !6
  %28 = load i32, i32* %k.addr, align 4, !tbaa !6
  call void @calc_centroids_dim2(i32* %24, i32* %25, i8* %26, i32 %27, i32 %28)
  %29 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %30 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %31 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %32 = load i32, i32* %n.addr, align 4, !tbaa !6
  %33 = load i32, i32* %k.addr, align 4, !tbaa !6
  call void @av1_calc_indices_dim2(i32* %29, i32* %30, i8* %31, i32 %32, i32 %33)
  %34 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %35 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %36 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %37 = load i32, i32* %n.addr, align 4, !tbaa !6
  %38 = load i32, i32* %k.addr, align 4, !tbaa !6
  %call4 = call i64 @calc_total_dist_dim2(i32* %34, i32* %35, i8* %36, i32 %37, i32 %38)
  store i64 %call4, i64* %this_dist, align 8, !tbaa !9
  %39 = load i64, i64* %this_dist, align 8, !tbaa !9
  %40 = load i64, i64* %pre_dist, align 8, !tbaa !9
  %cmp5 = icmp sgt i64 %39, %40
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %41 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %42 = bitcast i32* %41 to i8*
  %arraydecay6 = getelementptr inbounds [16 x i32], [16 x i32]* %pre_centroids, i32 0, i32 0
  %43 = bitcast i32* %arraydecay6 to i8*
  %44 = load i32, i32* %k.addr, align 4, !tbaa !6
  %mul7 = mul i32 4, %44
  %mul8 = mul i32 %mul7, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 16 %43, i32 %mul8, i1 false)
  %45 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %arraydecay9 = getelementptr inbounds [16384 x i8], [16384 x i8]* %pre_indices, i32 0, i32 0
  %46 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul10 = mul i32 1, %46
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %45, i8* align 16 %arraydecay9, i32 %mul10, i1 false)
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %47 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %48 = bitcast i32* %47 to i8*
  %arraydecay11 = getelementptr inbounds [16 x i32], [16 x i32]* %pre_centroids, i32 0, i32 0
  %49 = bitcast i32* %arraydecay11 to i8*
  %50 = load i32, i32* %k.addr, align 4, !tbaa !6
  %mul12 = mul i32 4, %50
  %mul13 = mul i32 %mul12, 2
  %call14 = call i32 @memcmp(i8* %48, i8* %49, i32 %mul13)
  %tobool = icmp ne i32 %call14, 0
  br i1 %tobool, label %if.end16, label %if.then15

if.then15:                                        ; preds = %if.end
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.then15, %if.then
  %51 = bitcast i64* %pre_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %51) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup17 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %52 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup17:                                        ; preds = %cleanup, %for.cond.cleanup
  %53 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  br label %for.end

for.end:                                          ; preds = %cleanup17
  %54 = bitcast i64* %this_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %54) #6
  %55 = bitcast [16384 x i8]* %pre_indices to i8*
  call void @llvm.lifetime.end.p0i8(i64 16384, i8* %55) #6
  %56 = bitcast [16 x i32]* %pre_centroids to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %56) #6
  ret void
}

; Function Attrs: nounwind
define internal i64 @calc_total_dist_dim2(i32* %data, i32* %centroids, i8* %indices, i32 %n, i32 %k) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %centroids.addr = alloca i32*, align 4
  %indices.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %k.addr = alloca i32, align 4
  %dist = alloca i64, align 8
  %i = alloca i32, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  store i32* %centroids, i32** %centroids.addr, align 4, !tbaa !2
  store i8* %indices, i8** %indices.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %k, i32* %k.addr, align 4, !tbaa !6
  %0 = bitcast i64* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  store i64 0, i64* %dist, align 8, !tbaa !9
  %1 = load i32, i32* %k.addr, align 4, !tbaa !6
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul nsw i32 %7, 2
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %mul
  %8 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %11 to i32
  %mul1 = mul nsw i32 %conv, 2
  %add.ptr2 = getelementptr inbounds i32, i32* %8, i32 %mul1
  %call = call i32 @calc_dist_dim2(i32* %add.ptr, i32* %add.ptr2)
  %conv3 = sext i32 %call to i64
  %12 = load i64, i64* %dist, align 8, !tbaa !9
  %add = add nsw i64 %12, %conv3
  store i64 %add, i64* %dist, align 8, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = load i64, i64* %dist, align 8, !tbaa !9
  %15 = bitcast i64* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #6
  ret i64 %14
}

; Function Attrs: nounwind
define internal void @calc_centroids_dim2(i32* %data, i32* %centroids, i8* %indices, i32 %n, i32 %k) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %centroids.addr = alloca i32*, align 4
  %indices.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %k.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %count = alloca [8 x i32], align 16
  %rand_state = alloca i32, align 4
  %index = alloca i32, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  store i32* %centroids, i32** %centroids.addr, align 4, !tbaa !2
  store i8* %indices, i8** %indices.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %k, i32* %k.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast [8 x i32]* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %2) #6
  %3 = bitcast [8 x i32]* %count to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %3, i8 0, i32 32, i1 false)
  %4 = bitcast i32* %rand_state to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 0
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %6, i32* %rand_state, align 4, !tbaa !6
  %7 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %8 = bitcast i32* %7 to i8*
  %9 = load i32, i32* %k.addr, align 4, !tbaa !6
  %mul = mul i32 4, %9
  %mul1 = mul i32 %mul, 2
  call void @llvm.memset.p0i8.i32(i8* align 4 %8, i8 0, i32 %mul1, i1 false)
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i8*, i8** %indices.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8, i8* %13, i32 %14
  %15 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv = zext i8 %15 to i32
  store i32 %conv, i32* %index, align 4, !tbaa !6
  %16 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [8 x i32], [8 x i32]* %count, i32 0, i32 %16
  %17 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %arrayidx3, align 4, !tbaa !6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %18, 2
  br i1 %cmp5, label %for.body7, label %for.end

for.body7:                                        ; preds = %for.cond4
  %19 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %mul8 = mul nsw i32 %20, 2
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul8, %21
  %arrayidx9 = getelementptr inbounds i32, i32* %19, i32 %add
  %22 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %23 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %24 = load i32, i32* %index, align 4, !tbaa !6
  %mul10 = mul nsw i32 %24, 2
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %add11 = add nsw i32 %mul10, %25
  %arrayidx12 = getelementptr inbounds i32, i32* %23, i32 %add11
  %26 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %add13 = add nsw i32 %26, %22
  store i32 %add13, i32* %arrayidx12, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body7
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %inc14 = add nsw i32 %27, 1
  store i32 %inc14, i32* %j, align 4, !tbaa !6
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %28 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %inc16 = add nsw i32 %29, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc44, %for.end17
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %31 = load i32, i32* %k.addr, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %30, %31
  br i1 %cmp19, label %for.body21, label %for.end46

for.body21:                                       ; preds = %for.cond18
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [8 x i32], [8 x i32]* %count, i32 0, i32 %32
  %33 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  %cmp23 = icmp eq i32 %33, 0
  br i1 %cmp23, label %if.then, label %if.else

if.then:                                          ; preds = %for.body21
  %34 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %mul25 = mul nsw i32 %35, 2
  %add.ptr = getelementptr inbounds i32, i32* %34, i32 %mul25
  %36 = bitcast i32* %add.ptr to i8*
  %37 = load i32*, i32** %data.addr, align 4, !tbaa !2
  %call = call i32 @lcg_rand16(i32* %rand_state)
  %38 = load i32, i32* %n.addr, align 4, !tbaa !6
  %rem = urem i32 %call, %38
  %mul26 = mul i32 %rem, 2
  %add.ptr27 = getelementptr inbounds i32, i32* %37, i32 %mul26
  %39 = bitcast i32* %add.ptr27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %39, i32 8, i1 false)
  br label %if.end

if.else:                                          ; preds = %for.body21
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc41, %if.else
  %40 = load i32, i32* %j, align 4, !tbaa !6
  %cmp29 = icmp slt i32 %40, 2
  br i1 %cmp29, label %for.body31, label %for.end43

for.body31:                                       ; preds = %for.cond28
  %41 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %mul32 = mul nsw i32 %42, 2
  %43 = load i32, i32* %j, align 4, !tbaa !6
  %add33 = add nsw i32 %mul32, %43
  %arrayidx34 = getelementptr inbounds i32, i32* %41, i32 %add33
  %44 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds [8 x i32], [8 x i32]* %count, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %shr = ashr i32 %46, 1
  %add36 = add nsw i32 %44, %shr
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [8 x i32], [8 x i32]* %count, i32 0, i32 %47
  %48 = load i32, i32* %arrayidx37, align 4, !tbaa !6
  %div = sdiv i32 %add36, %48
  %49 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %mul38 = mul nsw i32 %50, 2
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %add39 = add nsw i32 %mul38, %51
  %arrayidx40 = getelementptr inbounds i32, i32* %49, i32 %add39
  store i32 %div, i32* %arrayidx40, align 4, !tbaa !6
  br label %for.inc41

for.inc41:                                        ; preds = %for.body31
  %52 = load i32, i32* %j, align 4, !tbaa !6
  %inc42 = add nsw i32 %52, 1
  store i32 %inc42, i32* %j, align 4, !tbaa !6
  br label %for.cond28

for.end43:                                        ; preds = %for.cond28
  br label %if.end

if.end:                                           ; preds = %for.end43, %if.then
  br label %for.inc44

for.inc44:                                        ; preds = %if.end
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %inc45 = add nsw i32 %53, 1
  store i32 %inc45, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.end46:                                        ; preds = %for.cond18
  %54 = bitcast i32* %rand_state to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast [8 x i32]* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %55) #6
  %56 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  %57 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_remove_duplicates(i32* %centroids, i32 %num_centroids) #0 {
entry:
  %centroids.addr = alloca i32*, align 4
  %num_centroids.addr = alloca i32, align 4
  %num_unique = alloca i32, align 4
  %i = alloca i32, align 4
  store i32* %centroids, i32** %centroids.addr, align 4, !tbaa !2
  store i32 %num_centroids, i32* %num_centroids.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_unique to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %3 = bitcast i32* %2 to i8*
  %4 = load i32, i32* %num_centroids.addr, align 4, !tbaa !6
  call void @qsort(i8* %3, i32 %4, i32 4, i32 (i8*, i8*)* @int_comparer)
  store i32 1, i32* %num_unique, align 4, !tbaa !6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %num_centroids.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %10 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %sub = sub nsw i32 %11, 1
  %arrayidx1 = getelementptr inbounds i32, i32* %10, i32 %sub
  %12 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %cmp2 = icmp ne i32 %9, %12
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %13 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds i32, i32* %13, i32 %14
  %15 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %16 = load i32*, i32** %centroids.addr, align 4, !tbaa !2
  %17 = load i32, i32* %num_unique, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %num_unique, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds i32, i32* %16, i32 %17
  store i32 %15, i32* %arrayidx4, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %inc5 = add nsw i32 %18, 1
  store i32 %inc5, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %19 = load i32, i32* %num_unique, align 4, !tbaa !6
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = bitcast i32* %num_unique to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  ret i32 %19
}

declare void @qsort(i8*, i32, i32, i32 (i8*, i8*)*) #2

; Function Attrs: nounwind
define internal i32 @int_comparer(i8* %a, i8* %b) #0 {
entry:
  %a.addr = alloca i8*, align 4
  %b.addr = alloca i8*, align 4
  store i8* %a, i8** %a.addr, align 4, !tbaa !2
  store i8* %b, i8** %b.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %1 = bitcast i8* %0 to i32*
  %2 = load i32, i32* %1, align 4, !tbaa !6
  %3 = load i8*, i8** %b.addr, align 4, !tbaa !2
  %4 = bitcast i8* %3 to i32*
  %5 = load i32, i32* %4, align 4, !tbaa !6
  %sub = sub nsw i32 %2, %5
  ret i32 %sub
}

; Function Attrs: nounwind
define hidden i32 @av1_index_color_cache(i16* %color_cache, i32 %n_cache, i16* %colors, i32 %n_colors, i8* %cache_color_found, i32* %out_cache_colors) #0 {
entry:
  %retval = alloca i32, align 4
  %color_cache.addr = alloca i16*, align 4
  %n_cache.addr = alloca i32, align 4
  %colors.addr = alloca i16*, align 4
  %n_colors.addr = alloca i32, align 4
  %cache_color_found.addr = alloca i8*, align 4
  %out_cache_colors.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %n_in_cache = alloca i32, align 4
  %in_cache_flags = alloca [8 x i32], align 16
  %i3 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %j34 = alloca i32, align 4
  %i35 = alloca i32, align 4
  store i16* %color_cache, i16** %color_cache.addr, align 4, !tbaa !2
  store i32 %n_cache, i32* %n_cache.addr, align 4, !tbaa !6
  store i16* %colors, i16** %colors.addr, align 4, !tbaa !2
  store i32 %n_colors, i32* %n_colors.addr, align 4, !tbaa !6
  store i8* %cache_color_found, i8** %cache_color_found.addr, align 4, !tbaa !2
  store i32* %out_cache_colors, i32** %out_cache_colors.addr, align 4, !tbaa !2
  %0 = load i32, i32* %n_cache.addr, align 4, !tbaa !6
  %cmp = icmp sle i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %n_colors.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %2, %3
  br i1 %cmp1, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i16*, i16** %colors.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 %6
  %7 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %7 to i32
  %8 = load i32*, i32** %out_cache_colors.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  store i32 %conv, i32* %arrayidx2, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %11 = load i32, i32* %n_colors.addr, align 4, !tbaa !6
  store i32 %11, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %12 = load i8*, i8** %cache_color_found.addr, align 4, !tbaa !2
  %13 = load i32, i32* %n_cache.addr, align 4, !tbaa !6
  %mul = mul i32 %13, 1
  call void @llvm.memset.p0i8.i32(i8* align 1 %12, i8 0, i32 %mul, i1 false)
  %14 = bitcast i32* %n_in_cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  store i32 0, i32* %n_in_cache, align 4, !tbaa !6
  %15 = bitcast [8 x i32]* %in_cache_flags to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %15) #6
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %in_cache_flags, i32 0, i32 0
  %16 = bitcast i32* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %16, i8 0, i32 32, i1 false)
  %17 = bitcast i32* %i3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  store i32 0, i32* %i3, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc30, %if.end
  %18 = load i32, i32* %i3, align 4, !tbaa !6
  %19 = load i32, i32* %n_cache.addr, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %18, %19
  br i1 %cmp5, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond4
  %20 = load i32, i32* %n_in_cache, align 4, !tbaa !6
  %21 = load i32, i32* %n_colors.addr, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %20, %21
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond4
  %22 = phi i1 [ false, %for.cond4 ], [ %cmp7, %land.rhs ]
  br i1 %22, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %land.end
  store i32 5, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %i3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  br label %for.end33

for.body10:                                       ; preds = %land.end
  %24 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc27, %for.body10
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %26 = load i32, i32* %n_colors.addr, align 4, !tbaa !6
  %cmp12 = icmp slt i32 %25, %26
  br i1 %cmp12, label %for.body15, label %for.cond.cleanup14

for.cond.cleanup14:                               ; preds = %for.cond11
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body15:                                       ; preds = %for.cond11
  %27 = load i16*, i16** %colors.addr, align 4, !tbaa !2
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds i16, i16* %27, i32 %28
  %29 = load i16, i16* %arrayidx16, align 2, !tbaa !11
  %conv17 = zext i16 %29 to i32
  %30 = load i16*, i16** %color_cache.addr, align 4, !tbaa !2
  %31 = load i32, i32* %i3, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds i16, i16* %30, i32 %31
  %32 = load i16, i16* %arrayidx18, align 2, !tbaa !11
  %conv19 = zext i16 %32 to i32
  %cmp20 = icmp eq i32 %conv17, %conv19
  br i1 %cmp20, label %if.then22, label %if.end26

if.then22:                                        ; preds = %for.body15
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds [8 x i32], [8 x i32]* %in_cache_flags, i32 0, i32 %33
  store i32 1, i32* %arrayidx23, align 4, !tbaa !6
  %34 = load i8*, i8** %cache_color_found.addr, align 4, !tbaa !2
  %35 = load i32, i32* %i3, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds i8, i8* %34, i32 %35
  store i8 1, i8* %arrayidx24, align 1, !tbaa !8
  %36 = load i32, i32* %n_in_cache, align 4, !tbaa !6
  %inc25 = add nsw i32 %36, 1
  store i32 %inc25, i32* %n_in_cache, align 4, !tbaa !6
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end26:                                         ; preds = %for.body15
  br label %for.inc27

for.inc27:                                        ; preds = %if.end26
  %37 = load i32, i32* %j, align 4, !tbaa !6
  %inc28 = add nsw i32 %37, 1
  store i32 %inc28, i32* %j, align 4, !tbaa !6
  br label %for.cond11

cleanup:                                          ; preds = %if.then22, %for.cond.cleanup14
  %38 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  br label %for.end29

for.end29:                                        ; preds = %cleanup
  br label %for.inc30

for.inc30:                                        ; preds = %for.end29
  %39 = load i32, i32* %i3, align 4, !tbaa !6
  %inc31 = add nsw i32 %39, 1
  store i32 %inc31, i32* %i3, align 4, !tbaa !6
  br label %for.cond4

for.end33:                                        ; preds = %for.cond.cleanup9
  %40 = bitcast i32* %j34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  store i32 0, i32* %j34, align 4, !tbaa !6
  %41 = bitcast i32* %i35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  store i32 0, i32* %i35, align 4, !tbaa !6
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc48, %for.end33
  %42 = load i32, i32* %i35, align 4, !tbaa !6
  %43 = load i32, i32* %n_colors.addr, align 4, !tbaa !6
  %cmp37 = icmp slt i32 %42, %43
  br i1 %cmp37, label %for.body40, label %for.cond.cleanup39

for.cond.cleanup39:                               ; preds = %for.cond36
  store i32 11, i32* %cleanup.dest.slot, align 4
  %44 = bitcast i32* %i35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  br label %for.end51

for.body40:                                       ; preds = %for.cond36
  %45 = load i32, i32* %i35, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds [8 x i32], [8 x i32]* %in_cache_flags, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx41, align 4, !tbaa !6
  %tobool = icmp ne i32 %46, 0
  br i1 %tobool, label %if.end47, label %if.then42

if.then42:                                        ; preds = %for.body40
  %47 = load i16*, i16** %colors.addr, align 4, !tbaa !2
  %48 = load i32, i32* %i35, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds i16, i16* %47, i32 %48
  %49 = load i16, i16* %arrayidx43, align 2, !tbaa !11
  %conv44 = zext i16 %49 to i32
  %50 = load i32*, i32** %out_cache_colors.addr, align 4, !tbaa !2
  %51 = load i32, i32* %j34, align 4, !tbaa !6
  %inc45 = add nsw i32 %51, 1
  store i32 %inc45, i32* %j34, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds i32, i32* %50, i32 %51
  store i32 %conv44, i32* %arrayidx46, align 4, !tbaa !6
  br label %if.end47

if.end47:                                         ; preds = %if.then42, %for.body40
  br label %for.inc48

for.inc48:                                        ; preds = %if.end47
  %52 = load i32, i32* %i35, align 4, !tbaa !6
  %inc49 = add nsw i32 %52, 1
  store i32 %inc49, i32* %i35, align 4, !tbaa !6
  br label %for.cond36

for.end51:                                        ; preds = %for.cond.cleanup39
  %53 = load i32, i32* %j34, align 4, !tbaa !6
  store i32 %53, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %54 = bitcast i32* %j34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast [8 x i32]* %in_cache_flags to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %55) #6
  %56 = bitcast i32* %n_in_cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  br label %return

return:                                           ; preds = %for.end51, %for.end
  %57 = load i32, i32* %retval, align 4
  ret i32 %57
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define hidden i32 @av1_get_palette_delta_bits_v(%struct.PALETTE_MODE_INFO* %pmi, i32 %bit_depth, i32* %zero_count, i32* %min_bits) #0 {
entry:
  %pmi.addr = alloca %struct.PALETTE_MODE_INFO*, align 4
  %bit_depth.addr = alloca i32, align 4
  %zero_count.addr = alloca i32*, align 4
  %min_bits.addr = alloca i32*, align 4
  %n = alloca i32, align 4
  %max_val = alloca i32, align 4
  %max_d = alloca i32, align 4
  %i = alloca i32, align 4
  %delta = alloca i32, align 4
  %v = alloca i32, align 4
  %d = alloca i32, align 4
  store %struct.PALETTE_MODE_INFO* %pmi, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32* %zero_count, i32** %zero_count.addr, align 4, !tbaa !2
  store i32* %min_bits, i32** %min_bits.addr, align 4, !tbaa !2
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 1
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %n, align 4, !tbaa !6
  %3 = bitcast i32* %max_val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %shl = shl i32 1, %4
  store i32 %shl, i32* %max_val, align 4, !tbaa !6
  %5 = bitcast i32* %max_d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store i32 0, i32* %max_d, align 4, !tbaa !6
  %6 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %6, 4
  %7 = load i32*, i32** %min_bits.addr, align 4, !tbaa !2
  store i32 %sub, i32* %7, align 4, !tbaa !6
  %8 = load i32*, i32** %zero_count.addr, align 4, !tbaa !2
  store i32 0, i32* %8, align 4, !tbaa !6
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %n, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  %palette_colors = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %14, i32 0, i32 0
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 16, %15
  %arrayidx2 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors, i32 0, i32 %add
  %16 = load i16, i16* %arrayidx2, align 2, !tbaa !11
  %conv3 = zext i16 %16 to i32
  %17 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  %palette_colors4 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %17, i32 0, i32 0
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %add5 = add nsw i32 16, %18
  %sub6 = sub nsw i32 %add5, 1
  %arrayidx7 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors4, i32 0, i32 %sub6
  %19 = load i16, i16* %arrayidx7, align 2, !tbaa !11
  %conv8 = zext i16 %19 to i32
  %sub9 = sub nsw i32 %conv3, %conv8
  store i32 %sub9, i32* %delta, align 4, !tbaa !6
  %20 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load i32, i32* %delta, align 4, !tbaa !6
  %call = call i32 @abs(i32 %21) #7
  store i32 %call, i32* %v, align 4, !tbaa !6
  %22 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load i32, i32* %v, align 4, !tbaa !6
  %24 = load i32, i32* %max_val, align 4, !tbaa !6
  %25 = load i32, i32* %v, align 4, !tbaa !6
  %sub10 = sub nsw i32 %24, %25
  %cmp11 = icmp slt i32 %23, %sub10
  br i1 %cmp11, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %26 = load i32, i32* %v, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %27 = load i32, i32* %max_val, align 4, !tbaa !6
  %28 = load i32, i32* %v, align 4, !tbaa !6
  %sub13 = sub nsw i32 %27, %28
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %26, %cond.true ], [ %sub13, %cond.false ]
  store i32 %cond, i32* %d, align 4, !tbaa !6
  %29 = load i32, i32* %d, align 4, !tbaa !6
  %30 = load i32, i32* %max_d, align 4, !tbaa !6
  %cmp14 = icmp sgt i32 %29, %30
  br i1 %cmp14, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %31 = load i32, i32* %d, align 4, !tbaa !6
  store i32 %31, i32* %max_d, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %32 = load i32, i32* %d, align 4, !tbaa !6
  %cmp16 = icmp eq i32 %32, 0
  br i1 %cmp16, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end
  %33 = load i32*, i32** %zero_count.addr, align 4, !tbaa !2
  %34 = load i32, i32* %33, align 4, !tbaa !6
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %33, align 4, !tbaa !6
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end
  %35 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  %37 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end19
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %inc20 = add nsw i32 %38, 1
  store i32 %inc20, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %39 = load i32, i32* %max_d, align 4, !tbaa !6
  %add21 = add nsw i32 %39, 1
  %call22 = call i32 @av1_ceil_log2(i32 %add21)
  %40 = load i32*, i32** %min_bits.addr, align 4, !tbaa !2
  %41 = load i32, i32* %40, align 4, !tbaa !6
  %cmp23 = icmp sgt i32 %call22, %41
  br i1 %cmp23, label %cond.true25, label %cond.false28

cond.true25:                                      ; preds = %for.end
  %42 = load i32, i32* %max_d, align 4, !tbaa !6
  %add26 = add nsw i32 %42, 1
  %call27 = call i32 @av1_ceil_log2(i32 %add26)
  br label %cond.end29

cond.false28:                                     ; preds = %for.end
  %43 = load i32*, i32** %min_bits.addr, align 4, !tbaa !2
  %44 = load i32, i32* %43, align 4, !tbaa !6
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false28, %cond.true25
  %cond30 = phi i32 [ %call27, %cond.true25 ], [ %44, %cond.false28 ]
  %45 = bitcast i32* %max_d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  %46 = bitcast i32* %max_val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  %47 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  ret i32 %cond30
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #4

; Function Attrs: inlinehint nounwind
define internal i32 @av1_ceil_log2(i32 %n) #5 {
entry:
  %retval = alloca i32, align 4
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %p = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 1, i32* %i, align 4, !tbaa !6
  %2 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 2, i32* %p, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %3 = load i32, i32* %p, align 4, !tbaa !6
  %4 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %3, %4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %p, align 4, !tbaa !6
  %shl = shl i32 %6, 1
  store i32 %shl, i32* %p, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %7, i32* %retval, align 4
  %8 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  br label %return

return:                                           ; preds = %while.end, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define hidden i32 @av1_palette_color_cost_y(%struct.PALETTE_MODE_INFO* %pmi, i16* %color_cache, i32 %n_cache, i32 %bit_depth) #0 {
entry:
  %pmi.addr = alloca %struct.PALETTE_MODE_INFO*, align 4
  %color_cache.addr = alloca i16*, align 4
  %n_cache.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %n = alloca i32, align 4
  %out_cache_colors = alloca [8 x i32], align 16
  %cache_color_found = alloca [16 x i8], align 16
  %n_out_cache = alloca i32, align 4
  %total_bits = alloca i32, align 4
  store %struct.PALETTE_MODE_INFO* %pmi, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  store i16* %color_cache, i16** %color_cache.addr, align 4, !tbaa !2
  store i32 %n_cache, i32* %n_cache.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 2, !tbaa !8
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %n, align 4, !tbaa !6
  %3 = bitcast [8 x i32]* %out_cache_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %3) #6
  %4 = bitcast [16 x i8]* %cache_color_found to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #6
  %5 = bitcast i32* %n_out_cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i16*, i16** %color_cache.addr, align 4, !tbaa !2
  %7 = load i32, i32* %n_cache.addr, align 4, !tbaa !6
  %8 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  %palette_colors = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %8, i32 0, i32 0
  %arraydecay = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors, i32 0, i32 0
  %9 = load i32, i32* %n, align 4, !tbaa !6
  %arraydecay1 = getelementptr inbounds [16 x i8], [16 x i8]* %cache_color_found, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [8 x i32], [8 x i32]* %out_cache_colors, i32 0, i32 0
  %call = call i32 @av1_index_color_cache(i16* %6, i32 %7, i16* %arraydecay, i32 %9, i8* %arraydecay1, i32* %arraydecay2)
  store i32 %call, i32* %n_out_cache, align 4, !tbaa !6
  %10 = bitcast i32* %total_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load i32, i32* %n_cache.addr, align 4, !tbaa !6
  %arraydecay3 = getelementptr inbounds [8 x i32], [8 x i32]* %out_cache_colors, i32 0, i32 0
  %12 = load i32, i32* %n_out_cache, align 4, !tbaa !6
  %13 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %call4 = call i32 @delta_encode_cost(i32* %arraydecay3, i32 %12, i32 %13, i32 1)
  %add = add nsw i32 %11, %call4
  store i32 %add, i32* %total_bits, align 4, !tbaa !6
  %14 = load i32, i32* %total_bits, align 4, !tbaa !6
  %mul = mul nsw i32 %14, 512
  %15 = bitcast i32* %total_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast i32* %n_out_cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast [16 x i8]* %cache_color_found to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #6
  %18 = bitcast [8 x i32]* %out_cache_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %18) #6
  %19 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  ret i32 %mul
}

; Function Attrs: nounwind
define internal i32 @delta_encode_cost(i32* %colors, i32 %num, i32 %bit_depth, i32 %min_val) #0 {
entry:
  %retval = alloca i32, align 4
  %colors.addr = alloca i32*, align 4
  %num.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %min_val.addr = alloca i32, align 4
  %bits_cost = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %max_delta = alloca i32, align 4
  %deltas = alloca [8 x i32], align 16
  %min_bits = alloca i32, align 4
  %i = alloca i32, align 4
  %delta = alloca i32, align 4
  %bits_per_delta = alloca i32, align 4
  %range = alloca i32, align 4
  %i22 = alloca i32, align 4
  store i32* %colors, i32** %colors.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %min_val, i32* %min_val.addr, align 4, !tbaa !6
  %0 = load i32, i32* %num.addr, align 4, !tbaa !6
  %cmp = icmp sle i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %bits_cost to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %2, i32* %bits_cost, align 4, !tbaa !6
  %3 = load i32, i32* %num.addr, align 4, !tbaa !6
  %cmp1 = icmp eq i32 %3, 1
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  %4 = load i32, i32* %bits_cost, align 4, !tbaa !6
  store i32 %4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %5 = load i32, i32* %bits_cost, align 4, !tbaa !6
  %add = add nsw i32 %5, 2
  store i32 %add, i32* %bits_cost, align 4, !tbaa !6
  %6 = bitcast i32* %max_delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 0, i32* %max_delta, align 4, !tbaa !6
  %7 = bitcast [8 x i32]* %deltas to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %7) #6
  %8 = bitcast i32* %min_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %9, 3
  store i32 %sub, i32* %min_bits, align 4, !tbaa !6
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end3
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %num.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %11, %12
  br i1 %cmp4, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32*, i32** %colors.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 %16
  %17 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %18 = load i32*, i32** %colors.addr, align 4, !tbaa !2
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %sub5 = sub nsw i32 %19, 1
  %arrayidx6 = getelementptr inbounds i32, i32* %18, i32 %sub5
  %20 = load i32, i32* %arrayidx6, align 4, !tbaa !6
  %sub7 = sub nsw i32 %17, %20
  store i32 %sub7, i32* %delta, align 4, !tbaa !6
  %21 = load i32, i32* %delta, align 4, !tbaa !6
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %sub8 = sub nsw i32 %22, 1
  %arrayidx9 = getelementptr inbounds [8 x i32], [8 x i32]* %deltas, i32 0, i32 %sub8
  store i32 %21, i32* %arrayidx9, align 4, !tbaa !6
  %23 = load i32, i32* %delta, align 4, !tbaa !6
  %24 = load i32, i32* %max_delta, align 4, !tbaa !6
  %cmp10 = icmp sgt i32 %23, %24
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %for.body
  %25 = load i32, i32* %delta, align 4, !tbaa !6
  store i32 %25, i32* %max_delta, align 4, !tbaa !6
  br label %if.end12

if.end12:                                         ; preds = %if.then11, %for.body
  %26 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end12
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %28 = bitcast i32* %bits_per_delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %29 = load i32, i32* %max_delta, align 4, !tbaa !6
  %add13 = add nsw i32 %29, 1
  %30 = load i32, i32* %min_val.addr, align 4, !tbaa !6
  %sub14 = sub nsw i32 %add13, %30
  %call = call i32 @av1_ceil_log2(i32 %sub14)
  %31 = load i32, i32* %min_bits, align 4, !tbaa !6
  %cmp15 = icmp sgt i32 %call, %31
  br i1 %cmp15, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %32 = load i32, i32* %max_delta, align 4, !tbaa !6
  %add16 = add nsw i32 %32, 1
  %33 = load i32, i32* %min_val.addr, align 4, !tbaa !6
  %sub17 = sub nsw i32 %add16, %33
  %call18 = call i32 @av1_ceil_log2(i32 %sub17)
  br label %cond.end

cond.false:                                       ; preds = %for.end
  %34 = load i32, i32* %min_bits, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call18, %cond.true ], [ %34, %cond.false ]
  store i32 %cond, i32* %bits_per_delta, align 4, !tbaa !6
  %35 = bitcast i32* %range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %36 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %shl = shl i32 1, %36
  %37 = load i32*, i32** %colors.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %37, i32 0
  %38 = load i32, i32* %arrayidx19, align 4, !tbaa !6
  %sub20 = sub nsw i32 %shl, %38
  %39 = load i32, i32* %min_val.addr, align 4, !tbaa !6
  %sub21 = sub nsw i32 %sub20, %39
  store i32 %sub21, i32* %range, align 4, !tbaa !6
  %40 = bitcast i32* %i22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  store i32 0, i32* %i22, align 4, !tbaa !6
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc38, %cond.end
  %41 = load i32, i32* %i22, align 4, !tbaa !6
  %42 = load i32, i32* %num.addr, align 4, !tbaa !6
  %sub24 = sub nsw i32 %42, 1
  %cmp25 = icmp slt i32 %41, %sub24
  br i1 %cmp25, label %for.body27, label %for.cond.cleanup26

for.cond.cleanup26:                               ; preds = %for.cond23
  store i32 5, i32* %cleanup.dest.slot, align 4
  %43 = bitcast i32* %i22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  br label %for.end40

for.body27:                                       ; preds = %for.cond23
  %44 = load i32, i32* %bits_per_delta, align 4, !tbaa !6
  %45 = load i32, i32* %bits_cost, align 4, !tbaa !6
  %add28 = add nsw i32 %45, %44
  store i32 %add28, i32* %bits_cost, align 4, !tbaa !6
  %46 = load i32, i32* %i22, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [8 x i32], [8 x i32]* %deltas, i32 0, i32 %46
  %47 = load i32, i32* %arrayidx29, align 4, !tbaa !6
  %48 = load i32, i32* %range, align 4, !tbaa !6
  %sub30 = sub nsw i32 %48, %47
  store i32 %sub30, i32* %range, align 4, !tbaa !6
  %49 = load i32, i32* %bits_per_delta, align 4, !tbaa !6
  %50 = load i32, i32* %range, align 4, !tbaa !6
  %call31 = call i32 @av1_ceil_log2(i32 %50)
  %cmp32 = icmp slt i32 %49, %call31
  br i1 %cmp32, label %cond.true33, label %cond.false34

cond.true33:                                      ; preds = %for.body27
  %51 = load i32, i32* %bits_per_delta, align 4, !tbaa !6
  br label %cond.end36

cond.false34:                                     ; preds = %for.body27
  %52 = load i32, i32* %range, align 4, !tbaa !6
  %call35 = call i32 @av1_ceil_log2(i32 %52)
  br label %cond.end36

cond.end36:                                       ; preds = %cond.false34, %cond.true33
  %cond37 = phi i32 [ %51, %cond.true33 ], [ %call35, %cond.false34 ]
  store i32 %cond37, i32* %bits_per_delta, align 4, !tbaa !6
  br label %for.inc38

for.inc38:                                        ; preds = %cond.end36
  %53 = load i32, i32* %i22, align 4, !tbaa !6
  %inc39 = add nsw i32 %53, 1
  store i32 %inc39, i32* %i22, align 4, !tbaa !6
  br label %for.cond23

for.end40:                                        ; preds = %for.cond.cleanup26
  %54 = load i32, i32* %bits_cost, align 4, !tbaa !6
  store i32 %54, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %55 = bitcast i32* %range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #6
  %56 = bitcast i32* %bits_per_delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  %57 = bitcast i32* %min_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  %58 = bitcast [8 x i32]* %deltas to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %58) #6
  %59 = bitcast i32* %max_delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  br label %cleanup

cleanup:                                          ; preds = %for.end40, %if.then2
  %60 = bitcast i32* %bits_cost to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %61 = load i32, i32* %retval, align 4
  ret i32 %61
}

; Function Attrs: nounwind
define hidden i32 @av1_palette_color_cost_uv(%struct.PALETTE_MODE_INFO* %pmi, i16* %color_cache, i32 %n_cache, i32 %bit_depth) #0 {
entry:
  %pmi.addr = alloca %struct.PALETTE_MODE_INFO*, align 4
  %color_cache.addr = alloca i16*, align 4
  %n_cache.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %n = alloca i32, align 4
  %total_bits = alloca i32, align 4
  %out_cache_colors = alloca [8 x i32], align 16
  %cache_color_found = alloca [16 x i8], align 16
  %n_out_cache = alloca i32, align 4
  %zero_count = alloca i32, align 4
  %min_bits_v = alloca i32, align 4
  %bits_v = alloca i32, align 4
  %bits_using_delta = alloca i32, align 4
  %bits_using_raw = alloca i32, align 4
  store %struct.PALETTE_MODE_INFO* %pmi, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  store i16* %color_cache, i16** %color_cache.addr, align 4, !tbaa !2
  store i32 %n_cache, i32* %n_cache.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 1
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %n, align 4, !tbaa !6
  %3 = bitcast i32* %total_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store i32 0, i32* %total_bits, align 4, !tbaa !6
  %4 = bitcast [8 x i32]* %out_cache_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %4) #6
  %5 = bitcast [16 x i8]* %cache_color_found to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #6
  %6 = bitcast i32* %n_out_cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i16*, i16** %color_cache.addr, align 4, !tbaa !2
  %8 = load i32, i32* %n_cache.addr, align 4, !tbaa !6
  %9 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  %palette_colors = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %9, i32 0, i32 0
  %arraydecay = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors, i32 0, i32 0
  %add.ptr = getelementptr inbounds i16, i16* %arraydecay, i32 8
  %10 = load i32, i32* %n, align 4, !tbaa !6
  %arraydecay1 = getelementptr inbounds [16 x i8], [16 x i8]* %cache_color_found, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [8 x i32], [8 x i32]* %out_cache_colors, i32 0, i32 0
  %call = call i32 @av1_index_color_cache(i16* %7, i32 %8, i16* %add.ptr, i32 %10, i8* %arraydecay1, i32* %arraydecay2)
  store i32 %call, i32* %n_out_cache, align 4, !tbaa !6
  %11 = load i32, i32* %n_cache.addr, align 4, !tbaa !6
  %arraydecay3 = getelementptr inbounds [8 x i32], [8 x i32]* %out_cache_colors, i32 0, i32 0
  %12 = load i32, i32* %n_out_cache, align 4, !tbaa !6
  %13 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %call4 = call i32 @delta_encode_cost(i32* %arraydecay3, i32 %12, i32 %13, i32 0)
  %add = add nsw i32 %11, %call4
  %14 = load i32, i32* %total_bits, align 4, !tbaa !6
  %add5 = add nsw i32 %14, %add
  store i32 %add5, i32* %total_bits, align 4, !tbaa !6
  %15 = bitcast i32* %zero_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  store i32 0, i32* %zero_count, align 4, !tbaa !6
  %16 = bitcast i32* %min_bits_v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  store i32 0, i32* %min_bits_v, align 4, !tbaa !6
  %17 = bitcast i32* %bits_v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !2
  %19 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %call6 = call i32 @av1_get_palette_delta_bits_v(%struct.PALETTE_MODE_INFO* %18, i32 %19, i32* %zero_count, i32* %min_bits_v)
  store i32 %call6, i32* %bits_v, align 4, !tbaa !6
  %20 = bitcast i32* %bits_using_delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %add7 = add nsw i32 2, %21
  %22 = load i32, i32* %bits_v, align 4, !tbaa !6
  %add8 = add nsw i32 %22, 1
  %23 = load i32, i32* %n, align 4, !tbaa !6
  %sub = sub nsw i32 %23, 1
  %mul = mul nsw i32 %add8, %sub
  %add9 = add nsw i32 %add7, %mul
  %24 = load i32, i32* %zero_count, align 4, !tbaa !6
  %sub10 = sub nsw i32 %add9, %24
  store i32 %sub10, i32* %bits_using_delta, align 4, !tbaa !6
  %25 = bitcast i32* %bits_using_raw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %27 = load i32, i32* %n, align 4, !tbaa !6
  %mul11 = mul nsw i32 %26, %27
  store i32 %mul11, i32* %bits_using_raw, align 4, !tbaa !6
  %28 = load i32, i32* %bits_using_delta, align 4, !tbaa !6
  %29 = load i32, i32* %bits_using_raw, align 4, !tbaa !6
  %cmp = icmp slt i32 %28, %29
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %30 = load i32, i32* %bits_using_delta, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %31 = load i32, i32* %bits_using_raw, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %30, %cond.true ], [ %31, %cond.false ]
  %add13 = add nsw i32 1, %cond
  %32 = load i32, i32* %total_bits, align 4, !tbaa !6
  %add14 = add nsw i32 %32, %add13
  store i32 %add14, i32* %total_bits, align 4, !tbaa !6
  %33 = load i32, i32* %total_bits, align 4, !tbaa !6
  %mul15 = mul nsw i32 %33, 512
  %34 = bitcast i32* %bits_using_raw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast i32* %bits_using_delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = bitcast i32* %bits_v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  %37 = bitcast i32* %min_bits_v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  %38 = bitcast i32* %zero_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  %39 = bitcast i32* %n_out_cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  %40 = bitcast [16 x i8]* %cache_color_found to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #6
  %41 = bitcast [8 x i32]* %out_cache_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %41) #6
  %42 = bitcast i32* %total_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  %43 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  ret i32 %mul15
}

; Function Attrs: inlinehint nounwind
define internal i32 @lcg_rand16(i32* %state) #5 {
entry:
  %state.addr = alloca i32*, align 4
  store i32* %state, i32** %state.addr, align 4, !tbaa !2
  %0 = load i32*, i32** %state.addr, align 4, !tbaa !2
  %1 = load i32, i32* %0, align 4, !tbaa !6
  %conv = zext i32 %1 to i64
  %mul = mul i64 %conv, 1103515245
  %add = add i64 %mul, 12345
  %conv1 = trunc i64 %add to i32
  %2 = load i32*, i32** %state.addr, align 4, !tbaa !2
  store i32 %conv1, i32* %2, align 4, !tbaa !6
  %3 = load i32*, i32** %state.addr, align 4, !tbaa !2
  %4 = load i32, i32* %3, align 4, !tbaa !6
  %div = udiv i32 %4, 65536
  %rem = urem i32 %div, 32768
  ret i32 %rem
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"long long", !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"short", !4, i64 0}
