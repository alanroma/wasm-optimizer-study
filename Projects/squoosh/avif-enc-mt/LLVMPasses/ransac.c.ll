; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/ransac.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/ransac.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.MotionModel = type { [8 x double], i32*, i32 }
%struct.RANSAC_MOTION = type { i32, double, i32* }

@is_collinear3.collinear_eps = internal constant double 1.000000e-03, align 8

; Function Attrs: nounwind
define hidden i32 (i32*, i32, i32*, %struct.MotionModel*, i32)* @av1_get_ransac_type(i8 zeroext %type) #0 {
entry:
  %retval = alloca i32 (i32*, i32, i32*, %struct.MotionModel*, i32)*, align 4
  %type.addr = alloca i8, align 1
  store i8 %type, i8* %type.addr, align 1, !tbaa !2
  %0 = load i8, i8* %type.addr, align 1, !tbaa !2
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 3, label %sw.bb
    i32 2, label %sw.bb1
    i32 1, label %sw.bb2
  ]

sw.bb:                                            ; preds = %entry
  store i32 (i32*, i32, i32*, %struct.MotionModel*, i32)* @ransac_affine, i32 (i32*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  store i32 (i32*, i32, i32*, %struct.MotionModel*, i32)* @ransac_rotzoom, i32 (i32*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i32 (i32*, i32, i32*, %struct.MotionModel*, i32)* @ransac_translation, i32 (i32*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  store i32 (i32*, i32, i32*, %struct.MotionModel*, i32)* null, i32 (i32*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i32 (i32*, i32, i32*, %struct.MotionModel*, i32)*, i32 (i32*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  ret i32 (i32*, i32, i32*, %struct.MotionModel*, i32)* %1
}

; Function Attrs: nounwind
define internal i32 @ransac_affine(i32* %matched_points, i32 %npoints, i32* %num_inliers_by_motion, %struct.MotionModel* %params_by_motion, i32 %num_desired_motions) #0 {
entry:
  %matched_points.addr = alloca i32*, align 4
  %npoints.addr = alloca i32, align 4
  %num_inliers_by_motion.addr = alloca i32*, align 4
  %params_by_motion.addr = alloca %struct.MotionModel*, align 4
  %num_desired_motions.addr = alloca i32, align 4
  store i32* %matched_points, i32** %matched_points.addr, align 4, !tbaa !5
  store i32 %npoints, i32* %npoints.addr, align 4, !tbaa !7
  store i32* %num_inliers_by_motion, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  store %struct.MotionModel* %params_by_motion, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  store i32 %num_desired_motions, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %0 = load i32*, i32** %matched_points.addr, align 4, !tbaa !5
  %1 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %2 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %3 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %4 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %call = call i32 @ransac(i32* %0, i32 %1, i32* %2, %struct.MotionModel* %3, i32 %4, i32 3, i32 (double*)* @is_degenerate_affine, i32 (i32, double*, double*, double*)* @find_affine, void (double*, double*, double*, i32, i32, i32)* @project_points_double_affine)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @ransac_rotzoom(i32* %matched_points, i32 %npoints, i32* %num_inliers_by_motion, %struct.MotionModel* %params_by_motion, i32 %num_desired_motions) #0 {
entry:
  %matched_points.addr = alloca i32*, align 4
  %npoints.addr = alloca i32, align 4
  %num_inliers_by_motion.addr = alloca i32*, align 4
  %params_by_motion.addr = alloca %struct.MotionModel*, align 4
  %num_desired_motions.addr = alloca i32, align 4
  store i32* %matched_points, i32** %matched_points.addr, align 4, !tbaa !5
  store i32 %npoints, i32* %npoints.addr, align 4, !tbaa !7
  store i32* %num_inliers_by_motion, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  store %struct.MotionModel* %params_by_motion, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  store i32 %num_desired_motions, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %0 = load i32*, i32** %matched_points.addr, align 4, !tbaa !5
  %1 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %2 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %3 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %4 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %call = call i32 @ransac(i32* %0, i32 %1, i32* %2, %struct.MotionModel* %3, i32 %4, i32 3, i32 (double*)* @is_degenerate_affine, i32 (i32, double*, double*, double*)* @find_rotzoom, void (double*, double*, double*, i32, i32, i32)* @project_points_double_rotzoom)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @ransac_translation(i32* %matched_points, i32 %npoints, i32* %num_inliers_by_motion, %struct.MotionModel* %params_by_motion, i32 %num_desired_motions) #0 {
entry:
  %matched_points.addr = alloca i32*, align 4
  %npoints.addr = alloca i32, align 4
  %num_inliers_by_motion.addr = alloca i32*, align 4
  %params_by_motion.addr = alloca %struct.MotionModel*, align 4
  %num_desired_motions.addr = alloca i32, align 4
  store i32* %matched_points, i32** %matched_points.addr, align 4, !tbaa !5
  store i32 %npoints, i32* %npoints.addr, align 4, !tbaa !7
  store i32* %num_inliers_by_motion, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  store %struct.MotionModel* %params_by_motion, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  store i32 %num_desired_motions, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %0 = load i32*, i32** %matched_points.addr, align 4, !tbaa !5
  %1 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %2 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %3 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %4 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %call = call i32 @ransac(i32* %0, i32 %1, i32* %2, %struct.MotionModel* %3, i32 %4, i32 3, i32 (double*)* @is_degenerate_translation, i32 (i32, double*, double*, double*)* @find_translation, void (double*, double*, double*, i32, i32, i32)* @project_points_double_translation)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 (double*, i32, i32*, %struct.MotionModel*, i32)* @av1_get_ransac_double_prec_type(i8 zeroext %type) #0 {
entry:
  %retval = alloca i32 (double*, i32, i32*, %struct.MotionModel*, i32)*, align 4
  %type.addr = alloca i8, align 1
  store i8 %type, i8* %type.addr, align 1, !tbaa !2
  %0 = load i8, i8* %type.addr, align 1, !tbaa !2
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 3, label %sw.bb
    i32 2, label %sw.bb1
    i32 1, label %sw.bb2
  ]

sw.bb:                                            ; preds = %entry
  store i32 (double*, i32, i32*, %struct.MotionModel*, i32)* @ransac_affine_double_prec, i32 (double*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  store i32 (double*, i32, i32*, %struct.MotionModel*, i32)* @ransac_rotzoom_double_prec, i32 (double*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i32 (double*, i32, i32*, %struct.MotionModel*, i32)* @ransac_translation_double_prec, i32 (double*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  store i32 (double*, i32, i32*, %struct.MotionModel*, i32)* null, i32 (double*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i32 (double*, i32, i32*, %struct.MotionModel*, i32)*, i32 (double*, i32, i32*, %struct.MotionModel*, i32)** %retval, align 4
  ret i32 (double*, i32, i32*, %struct.MotionModel*, i32)* %1
}

; Function Attrs: nounwind
define internal i32 @ransac_affine_double_prec(double* %matched_points, i32 %npoints, i32* %num_inliers_by_motion, %struct.MotionModel* %params_by_motion, i32 %num_desired_motions) #0 {
entry:
  %matched_points.addr = alloca double*, align 4
  %npoints.addr = alloca i32, align 4
  %num_inliers_by_motion.addr = alloca i32*, align 4
  %params_by_motion.addr = alloca %struct.MotionModel*, align 4
  %num_desired_motions.addr = alloca i32, align 4
  store double* %matched_points, double** %matched_points.addr, align 4, !tbaa !5
  store i32 %npoints, i32* %npoints.addr, align 4, !tbaa !7
  store i32* %num_inliers_by_motion, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  store %struct.MotionModel* %params_by_motion, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  store i32 %num_desired_motions, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %0 = load double*, double** %matched_points.addr, align 4, !tbaa !5
  %1 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %2 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %3 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %4 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %call = call i32 @ransac_double_prec(double* %0, i32 %1, i32* %2, %struct.MotionModel* %3, i32 %4, i32 3, i32 (double*)* @is_degenerate_affine, i32 (i32, double*, double*, double*)* @find_affine, void (double*, double*, double*, i32, i32, i32)* @project_points_double_affine)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @ransac_rotzoom_double_prec(double* %matched_points, i32 %npoints, i32* %num_inliers_by_motion, %struct.MotionModel* %params_by_motion, i32 %num_desired_motions) #0 {
entry:
  %matched_points.addr = alloca double*, align 4
  %npoints.addr = alloca i32, align 4
  %num_inliers_by_motion.addr = alloca i32*, align 4
  %params_by_motion.addr = alloca %struct.MotionModel*, align 4
  %num_desired_motions.addr = alloca i32, align 4
  store double* %matched_points, double** %matched_points.addr, align 4, !tbaa !5
  store i32 %npoints, i32* %npoints.addr, align 4, !tbaa !7
  store i32* %num_inliers_by_motion, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  store %struct.MotionModel* %params_by_motion, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  store i32 %num_desired_motions, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %0 = load double*, double** %matched_points.addr, align 4, !tbaa !5
  %1 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %2 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %3 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %4 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %call = call i32 @ransac_double_prec(double* %0, i32 %1, i32* %2, %struct.MotionModel* %3, i32 %4, i32 3, i32 (double*)* @is_degenerate_affine, i32 (i32, double*, double*, double*)* @find_rotzoom, void (double*, double*, double*, i32, i32, i32)* @project_points_double_rotzoom)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @ransac_translation_double_prec(double* %matched_points, i32 %npoints, i32* %num_inliers_by_motion, %struct.MotionModel* %params_by_motion, i32 %num_desired_motions) #0 {
entry:
  %matched_points.addr = alloca double*, align 4
  %npoints.addr = alloca i32, align 4
  %num_inliers_by_motion.addr = alloca i32*, align 4
  %params_by_motion.addr = alloca %struct.MotionModel*, align 4
  %num_desired_motions.addr = alloca i32, align 4
  store double* %matched_points, double** %matched_points.addr, align 4, !tbaa !5
  store i32 %npoints, i32* %npoints.addr, align 4, !tbaa !7
  store i32* %num_inliers_by_motion, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  store %struct.MotionModel* %params_by_motion, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  store i32 %num_desired_motions, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %0 = load double*, double** %matched_points.addr, align 4, !tbaa !5
  %1 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %2 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %3 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %4 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %call = call i32 @ransac_double_prec(double* %0, i32 %1, i32* %2, %struct.MotionModel* %3, i32 %4, i32 3, i32 (double*)* @is_degenerate_translation, i32 (i32, double*, double*, double*)* @find_translation, void (double*, double*, double*, i32, i32, i32)* @project_points_double_translation)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @ransac(i32* %matched_points, i32 %npoints, i32* %num_inliers_by_motion, %struct.MotionModel* %params_by_motion, i32 %num_desired_motions, i32 %minpts, i32 (double*)* %is_degenerate, i32 (i32, double*, double*, double*)* %find_transformation, void (double*, double*, double*, i32, i32, i32)* %projectpoints) #0 {
entry:
  %retval = alloca i32, align 4
  %matched_points.addr = alloca i32*, align 4
  %npoints.addr = alloca i32, align 4
  %num_inliers_by_motion.addr = alloca i32*, align 4
  %params_by_motion.addr = alloca %struct.MotionModel*, align 4
  %num_desired_motions.addr = alloca i32, align 4
  %minpts.addr = alloca i32, align 4
  %is_degenerate.addr = alloca i32 (double*)*, align 4
  %find_transformation.addr = alloca i32 (i32, double*, double*, double*)*, align 4
  %projectpoints.addr = alloca void (double*, double*, double*, i32, i32, i32)*, align 4
  %trial_count = alloca i32, align 4
  %i = alloca i32, align 4
  %ret_val = alloca i32, align 4
  %seed = alloca i32, align 4
  %indices = alloca [4 x i32], align 16
  %points1 = alloca double*, align 4
  %points2 = alloca double*, align 4
  %corners1 = alloca double*, align 4
  %corners2 = alloca double*, align 4
  %image1_coord = alloca double*, align 4
  %motions = alloca %struct.RANSAC_MOTION*, align 4
  %worst_kept_motion = alloca %struct.RANSAC_MOTION*, align 4
  %current_motion = alloca %struct.RANSAC_MOTION, align 8
  %params_this_motion = alloca [9 x double], align 16
  %cnp1 = alloca double*, align 4
  %cnp2 = alloca double*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %sum_distance = alloca double, align 8
  %sum_distance_squared = alloca double, align 8
  %degenerate = alloca i32, align 4
  %num_degenerate_iter = alloca i32, align 4
  %dx = alloca double, align 8
  %dy = alloca double, align 8
  %distance = alloca double, align 8
  %mean_distance = alloca double, align 8
  store i32* %matched_points, i32** %matched_points.addr, align 4, !tbaa !5
  store i32 %npoints, i32* %npoints.addr, align 4, !tbaa !7
  store i32* %num_inliers_by_motion, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  store %struct.MotionModel* %params_by_motion, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  store i32 %num_desired_motions, i32* %num_desired_motions.addr, align 4, !tbaa !7
  store i32 %minpts, i32* %minpts.addr, align 4, !tbaa !7
  store i32 (double*)* %is_degenerate, i32 (double*)** %is_degenerate.addr, align 4, !tbaa !5
  store i32 (i32, double*, double*, double*)* %find_transformation, i32 (i32, double*, double*, double*)** %find_transformation.addr, align 4, !tbaa !5
  store void (double*, double*, double*, i32, i32, i32)* %projectpoints, void (double*, double*, double*, i32, i32, i32)** %projectpoints.addr, align 4, !tbaa !5
  %0 = bitcast i32* %trial_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %trial_count, align 4, !tbaa !7
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !7
  %2 = bitcast i32* %ret_val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 0, i32* %ret_val, align 4, !tbaa !7
  %3 = bitcast i32* %seed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  store i32 %4, i32* %seed, align 4, !tbaa !7
  %5 = bitcast [4 x i32]* %indices to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #6
  %6 = bitcast [4 x i32]* %indices to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %6, i8 0, i32 16, i1 false)
  %7 = bitcast double** %points1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = bitcast double** %points2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = bitcast double** %corners1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = bitcast double** %corners2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = bitcast double** %image1_coord to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = bitcast %struct.RANSAC_MOTION** %motions to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = bitcast %struct.RANSAC_MOTION** %worst_kept_motion to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store %struct.RANSAC_MOTION* null, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %14 = bitcast %struct.RANSAC_MOTION* %current_motion to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %14) #6
  %15 = bitcast [9 x double]* %params_this_motion to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %15) #6
  %16 = bitcast double** %cnp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = bitcast double** %cnp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %18 = load i32, i32* %i, align 4, !tbaa !7
  %19 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %18, %19
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %20 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %21 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i32, i32* %20, i32 %21
  store i32 0, i32* %arrayidx, align 4, !tbaa !7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %23 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %24 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %mul = mul nsw i32 %24, 5
  %cmp1 = icmp slt i32 %23, %mul
  br i1 %cmp1, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %25 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %cmp2 = icmp eq i32 %25, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup216

if.end:                                           ; preds = %lor.lhs.false
  %26 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul3 = mul i32 8, %26
  %mul4 = mul i32 %mul3, 2
  %call = call i8* @aom_malloc(i32 %mul4)
  %27 = bitcast i8* %call to double*
  store double* %27, double** %points1, align 4, !tbaa !5
  %28 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul5 = mul i32 8, %28
  %mul6 = mul i32 %mul5, 2
  %call7 = call i8* @aom_malloc(i32 %mul6)
  %29 = bitcast i8* %call7 to double*
  store double* %29, double** %points2, align 4, !tbaa !5
  %30 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul8 = mul i32 8, %30
  %mul9 = mul i32 %mul8, 2
  %call10 = call i8* @aom_malloc(i32 %mul9)
  %31 = bitcast i8* %call10 to double*
  store double* %31, double** %corners1, align 4, !tbaa !5
  %32 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul11 = mul i32 8, %32
  %mul12 = mul i32 %mul11, 2
  %call13 = call i8* @aom_malloc(i32 %mul12)
  %33 = bitcast i8* %call13 to double*
  store double* %33, double** %corners2, align 4, !tbaa !5
  %34 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul14 = mul i32 8, %34
  %mul15 = mul i32 %mul14, 2
  %call16 = call i8* @aom_malloc(i32 %mul15)
  %35 = bitcast i8* %call16 to double*
  store double* %35, double** %image1_coord, align 4, !tbaa !5
  %36 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %mul17 = mul i32 24, %36
  %call18 = call i8* @aom_malloc(i32 %mul17)
  %37 = bitcast i8* %call18 to %struct.RANSAC_MOTION*
  store %struct.RANSAC_MOTION* %37, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc25, %if.end
  %38 = load i32, i32* %i, align 4, !tbaa !7
  %39 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp20 = icmp slt i32 %38, %39
  br i1 %cmp20, label %for.body21, label %for.end27

for.body21:                                       ; preds = %for.cond19
  %40 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul22 = mul i32 4, %40
  %call23 = call i8* @aom_malloc(i32 %mul22)
  %41 = bitcast i8* %call23 to i32*
  %42 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %43 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx24 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %42, i32 %43
  %inlier_indices = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx24, i32 0, i32 2
  store i32* %41, i32** %inlier_indices, align 8, !tbaa !9
  %44 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %45 = load i32, i32* %i, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %44, i32 %45
  %46 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  call void @clear_motion(%struct.RANSAC_MOTION* %add.ptr, i32 %46)
  br label %for.inc25

for.inc25:                                        ; preds = %for.body21
  %47 = load i32, i32* %i, align 4, !tbaa !7
  %inc26 = add nsw i32 %47, 1
  store i32 %inc26, i32* %i, align 4, !tbaa !7
  br label %for.cond19

for.end27:                                        ; preds = %for.cond19
  %48 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul28 = mul i32 4, %48
  %call29 = call i8* @aom_malloc(i32 %mul28)
  %49 = bitcast i8* %call29 to i32*
  %inlier_indices30 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  store i32* %49, i32** %inlier_indices30, align 8, !tbaa !9
  %50 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  call void @clear_motion(%struct.RANSAC_MOTION* %current_motion, i32 %50)
  %51 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  store %struct.RANSAC_MOTION* %51, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %52 = load double*, double** %points1, align 4, !tbaa !5
  %tobool = icmp ne double* %52, null
  br i1 %tobool, label %land.lhs.true, label %if.then43

land.lhs.true:                                    ; preds = %for.end27
  %53 = load double*, double** %points2, align 4, !tbaa !5
  %tobool31 = icmp ne double* %53, null
  br i1 %tobool31, label %land.lhs.true32, label %if.then43

land.lhs.true32:                                  ; preds = %land.lhs.true
  %54 = load double*, double** %corners1, align 4, !tbaa !5
  %tobool33 = icmp ne double* %54, null
  br i1 %tobool33, label %land.lhs.true34, label %if.then43

land.lhs.true34:                                  ; preds = %land.lhs.true32
  %55 = load double*, double** %corners2, align 4, !tbaa !5
  %tobool35 = icmp ne double* %55, null
  br i1 %tobool35, label %land.lhs.true36, label %if.then43

land.lhs.true36:                                  ; preds = %land.lhs.true34
  %56 = load double*, double** %image1_coord, align 4, !tbaa !5
  %tobool37 = icmp ne double* %56, null
  br i1 %tobool37, label %land.lhs.true38, label %if.then43

land.lhs.true38:                                  ; preds = %land.lhs.true36
  %57 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %tobool39 = icmp ne %struct.RANSAC_MOTION* %57, null
  br i1 %tobool39, label %land.lhs.true40, label %if.then43

land.lhs.true40:                                  ; preds = %land.lhs.true38
  %inlier_indices41 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  %58 = load i32*, i32** %inlier_indices41, align 8, !tbaa !9
  %tobool42 = icmp ne i32* %58, null
  br i1 %tobool42, label %if.end44, label %if.then43

if.then43:                                        ; preds = %land.lhs.true40, %land.lhs.true38, %land.lhs.true36, %land.lhs.true34, %land.lhs.true32, %land.lhs.true, %for.end27
  store i32 1, i32* %ret_val, align 4, !tbaa !7
  br label %finish_ransac

if.end44:                                         ; preds = %land.lhs.true40
  %59 = load double*, double** %corners1, align 4, !tbaa !5
  store double* %59, double** %cnp1, align 4, !tbaa !5
  %60 = load double*, double** %corners2, align 4, !tbaa !5
  store double* %60, double** %cnp2, align 4, !tbaa !5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond45

for.cond45:                                       ; preds = %for.inc58, %if.end44
  %61 = load i32, i32* %i, align 4, !tbaa !7
  %62 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %cmp46 = icmp slt i32 %61, %62
  br i1 %cmp46, label %for.body47, label %for.end60

for.body47:                                       ; preds = %for.cond45
  %63 = load i32*, i32** %matched_points.addr, align 4, !tbaa !5
  %incdec.ptr = getelementptr inbounds i32, i32* %63, i32 1
  store i32* %incdec.ptr, i32** %matched_points.addr, align 4, !tbaa !5
  %64 = load i32, i32* %63, align 4, !tbaa !7
  %conv = sitofp i32 %64 to double
  %65 = load double*, double** %cnp1, align 4, !tbaa !5
  %incdec.ptr48 = getelementptr inbounds double, double* %65, i32 1
  store double* %incdec.ptr48, double** %cnp1, align 4, !tbaa !5
  store double %conv, double* %65, align 8, !tbaa !12
  %66 = load i32*, i32** %matched_points.addr, align 4, !tbaa !5
  %incdec.ptr49 = getelementptr inbounds i32, i32* %66, i32 1
  store i32* %incdec.ptr49, i32** %matched_points.addr, align 4, !tbaa !5
  %67 = load i32, i32* %66, align 4, !tbaa !7
  %conv50 = sitofp i32 %67 to double
  %68 = load double*, double** %cnp1, align 4, !tbaa !5
  %incdec.ptr51 = getelementptr inbounds double, double* %68, i32 1
  store double* %incdec.ptr51, double** %cnp1, align 4, !tbaa !5
  store double %conv50, double* %68, align 8, !tbaa !12
  %69 = load i32*, i32** %matched_points.addr, align 4, !tbaa !5
  %incdec.ptr52 = getelementptr inbounds i32, i32* %69, i32 1
  store i32* %incdec.ptr52, i32** %matched_points.addr, align 4, !tbaa !5
  %70 = load i32, i32* %69, align 4, !tbaa !7
  %conv53 = sitofp i32 %70 to double
  %71 = load double*, double** %cnp2, align 4, !tbaa !5
  %incdec.ptr54 = getelementptr inbounds double, double* %71, i32 1
  store double* %incdec.ptr54, double** %cnp2, align 4, !tbaa !5
  store double %conv53, double* %71, align 8, !tbaa !12
  %72 = load i32*, i32** %matched_points.addr, align 4, !tbaa !5
  %incdec.ptr55 = getelementptr inbounds i32, i32* %72, i32 1
  store i32* %incdec.ptr55, i32** %matched_points.addr, align 4, !tbaa !5
  %73 = load i32, i32* %72, align 4, !tbaa !7
  %conv56 = sitofp i32 %73 to double
  %74 = load double*, double** %cnp2, align 4, !tbaa !5
  %incdec.ptr57 = getelementptr inbounds double, double* %74, i32 1
  store double* %incdec.ptr57, double** %cnp2, align 4, !tbaa !5
  store double %conv56, double* %74, align 8, !tbaa !12
  br label %for.inc58

for.inc58:                                        ; preds = %for.body47
  %75 = load i32, i32* %i, align 4, !tbaa !7
  %inc59 = add nsw i32 %75, 1
  store i32 %inc59, i32* %i, align 4, !tbaa !7
  br label %for.cond45

for.end60:                                        ; preds = %for.cond45
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %cleanup, %for.end60
  %76 = load i32, i32* %trial_count, align 4, !tbaa !7
  %cmp61 = icmp sgt i32 20, %76
  br i1 %cmp61, label %while.body, label %while.end168

while.body:                                       ; preds = %while.cond
  %77 = bitcast double* %sum_distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %77) #6
  store double 0.000000e+00, double* %sum_distance, align 8, !tbaa !12
  %78 = bitcast double* %sum_distance_squared to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %78) #6
  store double 0.000000e+00, double* %sum_distance_squared, align 8, !tbaa !12
  %79 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  call void @clear_motion(%struct.RANSAC_MOTION* %current_motion, i32 %79)
  %80 = bitcast i32* %degenerate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #6
  store i32 1, i32* %degenerate, align 4, !tbaa !7
  %81 = bitcast i32* %num_degenerate_iter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #6
  store i32 0, i32* %num_degenerate_iter, align 4, !tbaa !7
  br label %while.cond63

while.cond63:                                     ; preds = %if.end77, %while.body
  %82 = load i32, i32* %degenerate, align 4, !tbaa !7
  %tobool64 = icmp ne i32 %82, 0
  br i1 %tobool64, label %while.body65, label %while.end

while.body65:                                     ; preds = %while.cond63
  %83 = load i32, i32* %num_degenerate_iter, align 4, !tbaa !7
  %inc66 = add nsw i32 %83, 1
  store i32 %inc66, i32* %num_degenerate_iter, align 4, !tbaa !7
  %84 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %85 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %indices, i32 0, i32 0
  %call67 = call i32 @get_rand_indices(i32 %84, i32 %85, i32* %arraydecay, i32* %seed)
  %tobool68 = icmp ne i32 %call67, 0
  br i1 %tobool68, label %if.end70, label %if.then69

if.then69:                                        ; preds = %while.body65
  store i32 1, i32* %ret_val, align 4, !tbaa !7
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end70:                                         ; preds = %while.body65
  %86 = load double*, double** %points1, align 4, !tbaa !5
  %87 = load double*, double** %corners1, align 4, !tbaa !5
  %arraydecay71 = getelementptr inbounds [4 x i32], [4 x i32]* %indices, i32 0, i32 0
  %88 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  call void @copy_points_at_indices(double* %86, double* %87, i32* %arraydecay71, i32 %88)
  %89 = load double*, double** %points2, align 4, !tbaa !5
  %90 = load double*, double** %corners2, align 4, !tbaa !5
  %arraydecay72 = getelementptr inbounds [4 x i32], [4 x i32]* %indices, i32 0, i32 0
  %91 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  call void @copy_points_at_indices(double* %89, double* %90, i32* %arraydecay72, i32 %91)
  %92 = load i32 (double*)*, i32 (double*)** %is_degenerate.addr, align 4, !tbaa !5
  %93 = load double*, double** %points1, align 4, !tbaa !5
  %call73 = call i32 %92(double* %93)
  store i32 %call73, i32* %degenerate, align 4, !tbaa !7
  %94 = load i32, i32* %num_degenerate_iter, align 4, !tbaa !7
  %cmp74 = icmp sgt i32 %94, 10
  br i1 %cmp74, label %if.then76, label %if.end77

if.then76:                                        ; preds = %if.end70
  store i32 1, i32* %ret_val, align 4, !tbaa !7
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end77:                                         ; preds = %if.end70
  br label %while.cond63

while.end:                                        ; preds = %while.cond63
  %95 = load i32 (i32, double*, double*, double*)*, i32 (i32, double*, double*, double*)** %find_transformation.addr, align 4, !tbaa !5
  %96 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %97 = load double*, double** %points1, align 4, !tbaa !5
  %98 = load double*, double** %points2, align 4, !tbaa !5
  %arraydecay78 = getelementptr inbounds [9 x double], [9 x double]* %params_this_motion, i32 0, i32 0
  %call79 = call i32 %95(i32 %96, double* %97, double* %98, double* %arraydecay78)
  %tobool80 = icmp ne i32 %call79, 0
  br i1 %tobool80, label %if.then81, label %if.end83

if.then81:                                        ; preds = %while.end
  %99 = load i32, i32* %trial_count, align 4, !tbaa !7
  %inc82 = add nsw i32 %99, 1
  store i32 %inc82, i32* %trial_count, align 4, !tbaa !7
  store i32 12, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end83:                                         ; preds = %while.end
  %100 = load void (double*, double*, double*, i32, i32, i32)*, void (double*, double*, double*, i32, i32, i32)** %projectpoints.addr, align 4, !tbaa !5
  %arraydecay84 = getelementptr inbounds [9 x double], [9 x double]* %params_this_motion, i32 0, i32 0
  %101 = load double*, double** %corners1, align 4, !tbaa !5
  %102 = load double*, double** %image1_coord, align 4, !tbaa !5
  %103 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  call void %100(double* %arraydecay84, double* %101, double* %102, i32 %103, i32 2, i32 2)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond85

for.cond85:                                       ; preds = %for.inc112, %if.end83
  %104 = load i32, i32* %i, align 4, !tbaa !7
  %105 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %cmp86 = icmp slt i32 %104, %105
  br i1 %cmp86, label %for.body88, label %for.end114

for.body88:                                       ; preds = %for.cond85
  %106 = bitcast double* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %106) #6
  %107 = load double*, double** %image1_coord, align 4, !tbaa !5
  %108 = load i32, i32* %i, align 4, !tbaa !7
  %mul89 = mul nsw i32 %108, 2
  %arrayidx90 = getelementptr inbounds double, double* %107, i32 %mul89
  %109 = load double, double* %arrayidx90, align 8, !tbaa !12
  %110 = load double*, double** %corners2, align 4, !tbaa !5
  %111 = load i32, i32* %i, align 4, !tbaa !7
  %mul91 = mul nsw i32 %111, 2
  %arrayidx92 = getelementptr inbounds double, double* %110, i32 %mul91
  %112 = load double, double* %arrayidx92, align 8, !tbaa !12
  %sub = fsub double %109, %112
  store double %sub, double* %dx, align 8, !tbaa !12
  %113 = bitcast double* %dy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %113) #6
  %114 = load double*, double** %image1_coord, align 4, !tbaa !5
  %115 = load i32, i32* %i, align 4, !tbaa !7
  %mul93 = mul nsw i32 %115, 2
  %add = add nsw i32 %mul93, 1
  %arrayidx94 = getelementptr inbounds double, double* %114, i32 %add
  %116 = load double, double* %arrayidx94, align 8, !tbaa !12
  %117 = load double*, double** %corners2, align 4, !tbaa !5
  %118 = load i32, i32* %i, align 4, !tbaa !7
  %mul95 = mul nsw i32 %118, 2
  %add96 = add nsw i32 %mul95, 1
  %arrayidx97 = getelementptr inbounds double, double* %117, i32 %add96
  %119 = load double, double* %arrayidx97, align 8, !tbaa !12
  %sub98 = fsub double %116, %119
  store double %sub98, double* %dy, align 8, !tbaa !12
  %120 = bitcast double* %distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %120) #6
  %121 = load double, double* %dx, align 8, !tbaa !12
  %122 = load double, double* %dx, align 8, !tbaa !12
  %mul99 = fmul double %121, %122
  %123 = load double, double* %dy, align 8, !tbaa !12
  %124 = load double, double* %dy, align 8, !tbaa !12
  %mul100 = fmul double %123, %124
  %add101 = fadd double %mul99, %mul100
  %125 = call double @llvm.sqrt.f64(double %add101)
  store double %125, double* %distance, align 8, !tbaa !12
  %126 = load double, double* %distance, align 8, !tbaa !12
  %cmp102 = fcmp olt double %126, 1.250000e+00
  br i1 %cmp102, label %if.then104, label %if.end111

if.then104:                                       ; preds = %for.body88
  %127 = load i32, i32* %i, align 4, !tbaa !7
  %inlier_indices105 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  %128 = load i32*, i32** %inlier_indices105, align 8, !tbaa !9
  %num_inliers = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %129 = load i32, i32* %num_inliers, align 8, !tbaa !13
  %inc106 = add nsw i32 %129, 1
  store i32 %inc106, i32* %num_inliers, align 8, !tbaa !13
  %arrayidx107 = getelementptr inbounds i32, i32* %128, i32 %129
  store i32 %127, i32* %arrayidx107, align 4, !tbaa !7
  %130 = load double, double* %distance, align 8, !tbaa !12
  %131 = load double, double* %sum_distance, align 8, !tbaa !12
  %add108 = fadd double %131, %130
  store double %add108, double* %sum_distance, align 8, !tbaa !12
  %132 = load double, double* %distance, align 8, !tbaa !12
  %133 = load double, double* %distance, align 8, !tbaa !12
  %mul109 = fmul double %132, %133
  %134 = load double, double* %sum_distance_squared, align 8, !tbaa !12
  %add110 = fadd double %134, %mul109
  store double %add110, double* %sum_distance_squared, align 8, !tbaa !12
  br label %if.end111

if.end111:                                        ; preds = %if.then104, %for.body88
  %135 = bitcast double* %distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %135) #6
  %136 = bitcast double* %dy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %136) #6
  %137 = bitcast double* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %137) #6
  br label %for.inc112

for.inc112:                                       ; preds = %if.end111
  %138 = load i32, i32* %i, align 4, !tbaa !7
  %inc113 = add nsw i32 %138, 1
  store i32 %inc113, i32* %i, align 4, !tbaa !7
  br label %for.cond85

for.end114:                                       ; preds = %for.cond85
  %num_inliers115 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %139 = load i32, i32* %num_inliers115, align 8, !tbaa !13
  %140 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %num_inliers116 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %140, i32 0, i32 0
  %141 = load i32, i32* %num_inliers116, align 8, !tbaa !13
  %cmp117 = icmp sge i32 %139, %141
  br i1 %cmp117, label %land.lhs.true119, label %if.end163

land.lhs.true119:                                 ; preds = %for.end114
  %num_inliers120 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %142 = load i32, i32* %num_inliers120, align 8, !tbaa !13
  %cmp121 = icmp sgt i32 %142, 1
  br i1 %cmp121, label %if.then123, label %if.end163

if.then123:                                       ; preds = %land.lhs.true119
  %143 = bitcast double* %mean_distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %143) #6
  %144 = load double, double* %sum_distance, align 8, !tbaa !12
  %num_inliers124 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %145 = load i32, i32* %num_inliers124, align 8, !tbaa !13
  %conv125 = sitofp i32 %145 to double
  %div = fdiv double %144, %conv125
  store double %div, double* %mean_distance, align 8, !tbaa !12
  %146 = load double, double* %sum_distance_squared, align 8, !tbaa !12
  %num_inliers126 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %147 = load i32, i32* %num_inliers126, align 8, !tbaa !13
  %conv127 = sitofp i32 %147 to double
  %sub128 = fsub double %conv127, 1.000000e+00
  %div129 = fdiv double %146, %sub128
  %148 = load double, double* %mean_distance, align 8, !tbaa !12
  %149 = load double, double* %mean_distance, align 8, !tbaa !12
  %mul130 = fmul double %148, %149
  %num_inliers131 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %150 = load i32, i32* %num_inliers131, align 8, !tbaa !13
  %conv132 = sitofp i32 %150 to double
  %mul133 = fmul double %mul130, %conv132
  %num_inliers134 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %151 = load i32, i32* %num_inliers134, align 8, !tbaa !13
  %conv135 = sitofp i32 %151 to double
  %sub136 = fsub double %conv135, 1.000000e+00
  %div137 = fdiv double %mul133, %sub136
  %sub138 = fsub double %div129, %div137
  %variance = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 1
  store double %sub138, double* %variance, align 8, !tbaa !14
  %152 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %call139 = call i32 @is_better_motion(%struct.RANSAC_MOTION* %current_motion, %struct.RANSAC_MOTION* %152)
  %tobool140 = icmp ne i32 %call139, 0
  br i1 %tobool140, label %if.then141, label %if.end162

if.then141:                                       ; preds = %if.then123
  %num_inliers142 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %153 = load i32, i32* %num_inliers142, align 8, !tbaa !13
  %154 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %num_inliers143 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %154, i32 0, i32 0
  store i32 %153, i32* %num_inliers143, align 8, !tbaa !13
  %variance144 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 1
  %155 = load double, double* %variance144, align 8, !tbaa !14
  %156 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %variance145 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %156, i32 0, i32 1
  store double %155, double* %variance145, align 8, !tbaa !14
  %157 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %inlier_indices146 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %157, i32 0, i32 2
  %158 = load i32*, i32** %inlier_indices146, align 8, !tbaa !9
  %159 = bitcast i32* %158 to i8*
  %inlier_indices147 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  %160 = load i32*, i32** %inlier_indices147, align 8, !tbaa !9
  %161 = bitcast i32* %160 to i8*
  %162 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul148 = mul i32 4, %162
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %159, i8* align 4 %161, i32 %mul148, i1 false)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond149

for.cond149:                                      ; preds = %for.inc159, %if.then141
  %163 = load i32, i32* %i, align 4, !tbaa !7
  %164 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp150 = icmp slt i32 %163, %164
  br i1 %cmp150, label %for.body152, label %for.end161

for.body152:                                      ; preds = %for.cond149
  %165 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %166 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %167 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx153 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %166, i32 %167
  %call154 = call i32 @is_better_motion(%struct.RANSAC_MOTION* %165, %struct.RANSAC_MOTION* %arrayidx153)
  %tobool155 = icmp ne i32 %call154, 0
  br i1 %tobool155, label %if.then156, label %if.end158

if.then156:                                       ; preds = %for.body152
  %168 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %169 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx157 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %168, i32 %169
  store %struct.RANSAC_MOTION* %arrayidx157, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  br label %if.end158

if.end158:                                        ; preds = %if.then156, %for.body152
  br label %for.inc159

for.inc159:                                       ; preds = %if.end158
  %170 = load i32, i32* %i, align 4, !tbaa !7
  %inc160 = add nsw i32 %170, 1
  store i32 %inc160, i32* %i, align 4, !tbaa !7
  br label %for.cond149

for.end161:                                       ; preds = %for.cond149
  br label %if.end162

if.end162:                                        ; preds = %for.end161, %if.then123
  %171 = bitcast double* %mean_distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %171) #6
  br label %if.end163

if.end163:                                        ; preds = %if.end162, %land.lhs.true119, %for.end114
  %172 = load i32, i32* %trial_count, align 4, !tbaa !7
  %inc164 = add nsw i32 %172, 1
  store i32 %inc164, i32* %trial_count, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then76, %if.then69, %if.end163, %if.then81
  %173 = bitcast i32* %num_degenerate_iter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #6
  %174 = bitcast i32* %degenerate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #6
  %175 = bitcast double* %sum_distance_squared to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %175) #6
  %176 = bitcast double* %sum_distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %176) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup216 [
    i32 0, label %cleanup.cont
    i32 12, label %while.cond
    i32 8, label %finish_ransac
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end168:                                     ; preds = %while.cond
  %177 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %178 = bitcast %struct.RANSAC_MOTION* %177 to i8*
  %179 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  call void @qsort(i8* %178, i32 %179, i32 24, i32 (i8*, i8*)* @compare_motions)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond169

for.cond169:                                      ; preds = %for.inc203, %while.end168
  %180 = load i32, i32* %i, align 4, !tbaa !7
  %181 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp170 = icmp slt i32 %180, %181
  br i1 %cmp170, label %for.body172, label %for.end205

for.body172:                                      ; preds = %for.cond169
  %182 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %183 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx173 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %182, i32 %183
  %num_inliers174 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx173, i32 0, i32 0
  %184 = load i32, i32* %num_inliers174, align 8, !tbaa !13
  %185 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %cmp175 = icmp sge i32 %184, %185
  br i1 %cmp175, label %if.then177, label %if.end202

if.then177:                                       ; preds = %for.body172
  %186 = load double*, double** %points1, align 4, !tbaa !5
  %187 = load double*, double** %corners1, align 4, !tbaa !5
  %188 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %189 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx178 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %188, i32 %189
  %inlier_indices179 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx178, i32 0, i32 2
  %190 = load i32*, i32** %inlier_indices179, align 8, !tbaa !9
  %191 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %192 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx180 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %191, i32 %192
  %num_inliers181 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx180, i32 0, i32 0
  %193 = load i32, i32* %num_inliers181, align 8, !tbaa !13
  call void @copy_points_at_indices(double* %186, double* %187, i32* %190, i32 %193)
  %194 = load double*, double** %points2, align 4, !tbaa !5
  %195 = load double*, double** %corners2, align 4, !tbaa !5
  %196 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %197 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx182 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %196, i32 %197
  %inlier_indices183 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx182, i32 0, i32 2
  %198 = load i32*, i32** %inlier_indices183, align 8, !tbaa !9
  %199 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %200 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx184 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %199, i32 %200
  %num_inliers185 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx184, i32 0, i32 0
  %201 = load i32, i32* %num_inliers185, align 8, !tbaa !13
  call void @copy_points_at_indices(double* %194, double* %195, i32* %198, i32 %201)
  %202 = load i32 (i32, double*, double*, double*)*, i32 (i32, double*, double*, double*)** %find_transformation.addr, align 4, !tbaa !5
  %203 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %204 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx186 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %203, i32 %204
  %num_inliers187 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx186, i32 0, i32 0
  %205 = load i32, i32* %num_inliers187, align 8, !tbaa !13
  %206 = load double*, double** %points1, align 4, !tbaa !5
  %207 = load double*, double** %points2, align 4, !tbaa !5
  %208 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %209 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx188 = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %208, i32 %209
  %params = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %arrayidx188, i32 0, i32 0
  %arraydecay189 = getelementptr inbounds [8 x double], [8 x double]* %params, i32 0, i32 0
  %call190 = call i32 %202(i32 %205, double* %206, double* %207, double* %arraydecay189)
  %210 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %211 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx191 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %210, i32 %211
  %num_inliers192 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx191, i32 0, i32 0
  %212 = load i32, i32* %num_inliers192, align 8, !tbaa !13
  %213 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %214 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx193 = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %213, i32 %214
  %num_inliers194 = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %arrayidx193, i32 0, i32 2
  store i32 %212, i32* %num_inliers194, align 4, !tbaa !15
  %215 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %216 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx195 = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %215, i32 %216
  %inliers = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %arrayidx195, i32 0, i32 1
  %217 = load i32*, i32** %inliers, align 8, !tbaa !17
  %218 = bitcast i32* %217 to i8*
  %219 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %220 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx196 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %219, i32 %220
  %inlier_indices197 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx196, i32 0, i32 2
  %221 = load i32*, i32** %inlier_indices197, align 8, !tbaa !9
  %222 = bitcast i32* %221 to i8*
  %223 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul198 = mul i32 4, %223
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %218, i8* align 4 %222, i32 %mul198, i1 false)
  %224 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %225 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx199 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %224, i32 %225
  %num_inliers200 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx199, i32 0, i32 0
  %226 = load i32, i32* %num_inliers200, align 8, !tbaa !13
  %227 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %228 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx201 = getelementptr inbounds i32, i32* %227, i32 %228
  store i32 %226, i32* %arrayidx201, align 4, !tbaa !7
  br label %if.end202

if.end202:                                        ; preds = %if.then177, %for.body172
  br label %for.inc203

for.inc203:                                       ; preds = %if.end202
  %229 = load i32, i32* %i, align 4, !tbaa !7
  %inc204 = add nsw i32 %229, 1
  store i32 %inc204, i32* %i, align 4, !tbaa !7
  br label %for.cond169

for.end205:                                       ; preds = %for.cond169
  br label %finish_ransac

finish_ransac:                                    ; preds = %for.end205, %cleanup, %if.then43
  %230 = load double*, double** %points1, align 4, !tbaa !5
  %231 = bitcast double* %230 to i8*
  call void @aom_free(i8* %231)
  %232 = load double*, double** %points2, align 4, !tbaa !5
  %233 = bitcast double* %232 to i8*
  call void @aom_free(i8* %233)
  %234 = load double*, double** %corners1, align 4, !tbaa !5
  %235 = bitcast double* %234 to i8*
  call void @aom_free(i8* %235)
  %236 = load double*, double** %corners2, align 4, !tbaa !5
  %237 = bitcast double* %236 to i8*
  call void @aom_free(i8* %237)
  %238 = load double*, double** %image1_coord, align 4, !tbaa !5
  %239 = bitcast double* %238 to i8*
  call void @aom_free(i8* %239)
  %inlier_indices206 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  %240 = load i32*, i32** %inlier_indices206, align 8, !tbaa !9
  %241 = bitcast i32* %240 to i8*
  call void @aom_free(i8* %241)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond207

for.cond207:                                      ; preds = %for.inc213, %finish_ransac
  %242 = load i32, i32* %i, align 4, !tbaa !7
  %243 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp208 = icmp slt i32 %242, %243
  br i1 %cmp208, label %for.body210, label %for.end215

for.body210:                                      ; preds = %for.cond207
  %244 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %245 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx211 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %244, i32 %245
  %inlier_indices212 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx211, i32 0, i32 2
  %246 = load i32*, i32** %inlier_indices212, align 8, !tbaa !9
  %247 = bitcast i32* %246 to i8*
  call void @aom_free(i8* %247)
  br label %for.inc213

for.inc213:                                       ; preds = %for.body210
  %248 = load i32, i32* %i, align 4, !tbaa !7
  %inc214 = add nsw i32 %248, 1
  store i32 %inc214, i32* %i, align 4, !tbaa !7
  br label %for.cond207

for.end215:                                       ; preds = %for.cond207
  %249 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %250 = bitcast %struct.RANSAC_MOTION* %249 to i8*
  call void @aom_free(i8* %250)
  %251 = load i32, i32* %ret_val, align 4, !tbaa !7
  store i32 %251, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup216

cleanup216:                                       ; preds = %for.end215, %cleanup, %if.then
  %252 = bitcast double** %cnp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #6
  %253 = bitcast double** %cnp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #6
  %254 = bitcast [9 x double]* %params_this_motion to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %254) #6
  %255 = bitcast %struct.RANSAC_MOTION* %current_motion to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %255) #6
  %256 = bitcast %struct.RANSAC_MOTION** %worst_kept_motion to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #6
  %257 = bitcast %struct.RANSAC_MOTION** %motions to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #6
  %258 = bitcast double** %image1_coord to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #6
  %259 = bitcast double** %corners2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #6
  %260 = bitcast double** %corners1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #6
  %261 = bitcast double** %points2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #6
  %262 = bitcast double** %points1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #6
  %263 = bitcast [4 x i32]* %indices to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %263) #6
  %264 = bitcast i32* %seed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #6
  %265 = bitcast i32* %ret_val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #6
  %266 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #6
  %267 = bitcast i32* %trial_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #6
  %268 = load i32, i32* %retval, align 4
  ret i32 %268
}

; Function Attrs: nounwind
define internal i32 @is_degenerate_affine(double* %p) #0 {
entry:
  %p.addr = alloca double*, align 4
  store double* %p, double** %p.addr, align 4, !tbaa !5
  %0 = load double*, double** %p.addr, align 4, !tbaa !5
  %1 = load double*, double** %p.addr, align 4, !tbaa !5
  %add.ptr = getelementptr inbounds double, double* %1, i32 2
  %2 = load double*, double** %p.addr, align 4, !tbaa !5
  %add.ptr1 = getelementptr inbounds double, double* %2, i32 4
  %call = call i32 @is_collinear3(double* %0, double* %add.ptr, double* %add.ptr1)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @find_affine(i32 %np, double* %pts1, double* %pts2, double* %mat) #0 {
entry:
  %retval = alloca i32, align 4
  %np.addr = alloca i32, align 4
  %pts1.addr = alloca double*, align 4
  %pts2.addr = alloca double*, align 4
  %mat.addr = alloca double*, align 4
  %np2 = alloca i32, align 4
  %a = alloca double*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %b = alloca double*, align 4
  %temp = alloca double*, align 4
  %i = alloca i32, align 4
  %sx = alloca double, align 8
  %sy = alloca double, align 8
  %dx = alloca double, align 8
  %dy = alloca double, align 8
  %T1 = alloca [9 x double], align 16
  %T2 = alloca [9 x double], align 16
  store i32 %np, i32* %np.addr, align 4, !tbaa !7
  store double* %pts1, double** %pts1.addr, align 4, !tbaa !5
  store double* %pts2, double** %pts2.addr, align 4, !tbaa !5
  store double* %mat, double** %mat.addr, align 4, !tbaa !5
  %0 = bitcast i32* %np2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %np.addr, align 4, !tbaa !7
  %mul = mul nsw i32 %1, 2
  store i32 %mul, i32* %np2, align 4, !tbaa !7
  %2 = bitcast double** %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %np2, align 4, !tbaa !7
  %mul1 = mul nsw i32 %3, 7
  %add = add nsw i32 %mul1, 42
  %mul2 = mul i32 8, %add
  %call = call i8* @aom_malloc(i32 %mul2)
  %4 = bitcast i8* %call to double*
  store double* %4, double** %a, align 4, !tbaa !5
  %5 = load double*, double** %a, align 4, !tbaa !5
  %cmp = icmp eq double* %5, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup81

if.end:                                           ; preds = %entry
  %6 = bitcast double** %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load double*, double** %a, align 4, !tbaa !5
  %8 = load i32, i32* %np2, align 4, !tbaa !7
  %mul3 = mul nsw i32 %8, 6
  %add.ptr = getelementptr inbounds double, double* %7, i32 %mul3
  store double* %add.ptr, double** %b, align 4, !tbaa !5
  %9 = bitcast double** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load double*, double** %b, align 4, !tbaa !5
  %11 = load i32, i32* %np2, align 4, !tbaa !7
  %add.ptr4 = getelementptr inbounds double, double* %10, i32 %11
  store double* %add.ptr4, double** %temp, align 4, !tbaa !5
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = bitcast double* %sx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #6
  %14 = bitcast double* %sy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %14) #6
  %15 = bitcast double* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %15) #6
  %16 = bitcast double* %dy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %16) #6
  %17 = bitcast [9 x double]* %T1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %17) #6
  %18 = bitcast [9 x double]* %T2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %18) #6
  %19 = load double*, double** %pts1.addr, align 4, !tbaa !5
  %20 = load i32, i32* %np.addr, align 4, !tbaa !7
  %arraydecay = getelementptr inbounds [9 x double], [9 x double]* %T1, i32 0, i32 0
  call void @normalize_homography(double* %19, i32 %20, double* %arraydecay)
  %21 = load double*, double** %pts2.addr, align 4, !tbaa !5
  %22 = load i32, i32* %np.addr, align 4, !tbaa !7
  %arraydecay5 = getelementptr inbounds [9 x double], [9 x double]* %T2, i32 0, i32 0
  call void @normalize_homography(double* %21, i32 %22, double* %arraydecay5)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %23 = load i32, i32* %i, align 4, !tbaa !7
  %24 = load i32, i32* %np.addr, align 4, !tbaa !7
  %cmp6 = icmp slt i32 %23, %24
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %25 = load double*, double** %pts2.addr, align 4, !tbaa !5
  %incdec.ptr = getelementptr inbounds double, double* %25, i32 1
  store double* %incdec.ptr, double** %pts2.addr, align 4, !tbaa !5
  %26 = load double, double* %25, align 8, !tbaa !12
  store double %26, double* %dx, align 8, !tbaa !12
  %27 = load double*, double** %pts2.addr, align 4, !tbaa !5
  %incdec.ptr7 = getelementptr inbounds double, double* %27, i32 1
  store double* %incdec.ptr7, double** %pts2.addr, align 4, !tbaa !5
  %28 = load double, double* %27, align 8, !tbaa !12
  store double %28, double* %dy, align 8, !tbaa !12
  %29 = load double*, double** %pts1.addr, align 4, !tbaa !5
  %incdec.ptr8 = getelementptr inbounds double, double* %29, i32 1
  store double* %incdec.ptr8, double** %pts1.addr, align 4, !tbaa !5
  %30 = load double, double* %29, align 8, !tbaa !12
  store double %30, double* %sx, align 8, !tbaa !12
  %31 = load double*, double** %pts1.addr, align 4, !tbaa !5
  %incdec.ptr9 = getelementptr inbounds double, double* %31, i32 1
  store double* %incdec.ptr9, double** %pts1.addr, align 4, !tbaa !5
  %32 = load double, double* %31, align 8, !tbaa !12
  store double %32, double* %sy, align 8, !tbaa !12
  %33 = load double, double* %sx, align 8, !tbaa !12
  %34 = load double*, double** %a, align 4, !tbaa !5
  %35 = load i32, i32* %i, align 4, !tbaa !7
  %mul10 = mul nsw i32 %35, 2
  %mul11 = mul nsw i32 %mul10, 6
  %add12 = add nsw i32 %mul11, 0
  %arrayidx = getelementptr inbounds double, double* %34, i32 %add12
  store double %33, double* %arrayidx, align 8, !tbaa !12
  %36 = load double, double* %sy, align 8, !tbaa !12
  %37 = load double*, double** %a, align 4, !tbaa !5
  %38 = load i32, i32* %i, align 4, !tbaa !7
  %mul13 = mul nsw i32 %38, 2
  %mul14 = mul nsw i32 %mul13, 6
  %add15 = add nsw i32 %mul14, 1
  %arrayidx16 = getelementptr inbounds double, double* %37, i32 %add15
  store double %36, double* %arrayidx16, align 8, !tbaa !12
  %39 = load double*, double** %a, align 4, !tbaa !5
  %40 = load i32, i32* %i, align 4, !tbaa !7
  %mul17 = mul nsw i32 %40, 2
  %mul18 = mul nsw i32 %mul17, 6
  %add19 = add nsw i32 %mul18, 2
  %arrayidx20 = getelementptr inbounds double, double* %39, i32 %add19
  store double 0.000000e+00, double* %arrayidx20, align 8, !tbaa !12
  %41 = load double*, double** %a, align 4, !tbaa !5
  %42 = load i32, i32* %i, align 4, !tbaa !7
  %mul21 = mul nsw i32 %42, 2
  %mul22 = mul nsw i32 %mul21, 6
  %add23 = add nsw i32 %mul22, 3
  %arrayidx24 = getelementptr inbounds double, double* %41, i32 %add23
  store double 0.000000e+00, double* %arrayidx24, align 8, !tbaa !12
  %43 = load double*, double** %a, align 4, !tbaa !5
  %44 = load i32, i32* %i, align 4, !tbaa !7
  %mul25 = mul nsw i32 %44, 2
  %mul26 = mul nsw i32 %mul25, 6
  %add27 = add nsw i32 %mul26, 4
  %arrayidx28 = getelementptr inbounds double, double* %43, i32 %add27
  store double 1.000000e+00, double* %arrayidx28, align 8, !tbaa !12
  %45 = load double*, double** %a, align 4, !tbaa !5
  %46 = load i32, i32* %i, align 4, !tbaa !7
  %mul29 = mul nsw i32 %46, 2
  %mul30 = mul nsw i32 %mul29, 6
  %add31 = add nsw i32 %mul30, 5
  %arrayidx32 = getelementptr inbounds double, double* %45, i32 %add31
  store double 0.000000e+00, double* %arrayidx32, align 8, !tbaa !12
  %47 = load double*, double** %a, align 4, !tbaa !5
  %48 = load i32, i32* %i, align 4, !tbaa !7
  %mul33 = mul nsw i32 %48, 2
  %add34 = add nsw i32 %mul33, 1
  %mul35 = mul nsw i32 %add34, 6
  %add36 = add nsw i32 %mul35, 0
  %arrayidx37 = getelementptr inbounds double, double* %47, i32 %add36
  store double 0.000000e+00, double* %arrayidx37, align 8, !tbaa !12
  %49 = load double*, double** %a, align 4, !tbaa !5
  %50 = load i32, i32* %i, align 4, !tbaa !7
  %mul38 = mul nsw i32 %50, 2
  %add39 = add nsw i32 %mul38, 1
  %mul40 = mul nsw i32 %add39, 6
  %add41 = add nsw i32 %mul40, 1
  %arrayidx42 = getelementptr inbounds double, double* %49, i32 %add41
  store double 0.000000e+00, double* %arrayidx42, align 8, !tbaa !12
  %51 = load double, double* %sx, align 8, !tbaa !12
  %52 = load double*, double** %a, align 4, !tbaa !5
  %53 = load i32, i32* %i, align 4, !tbaa !7
  %mul43 = mul nsw i32 %53, 2
  %add44 = add nsw i32 %mul43, 1
  %mul45 = mul nsw i32 %add44, 6
  %add46 = add nsw i32 %mul45, 2
  %arrayidx47 = getelementptr inbounds double, double* %52, i32 %add46
  store double %51, double* %arrayidx47, align 8, !tbaa !12
  %54 = load double, double* %sy, align 8, !tbaa !12
  %55 = load double*, double** %a, align 4, !tbaa !5
  %56 = load i32, i32* %i, align 4, !tbaa !7
  %mul48 = mul nsw i32 %56, 2
  %add49 = add nsw i32 %mul48, 1
  %mul50 = mul nsw i32 %add49, 6
  %add51 = add nsw i32 %mul50, 3
  %arrayidx52 = getelementptr inbounds double, double* %55, i32 %add51
  store double %54, double* %arrayidx52, align 8, !tbaa !12
  %57 = load double*, double** %a, align 4, !tbaa !5
  %58 = load i32, i32* %i, align 4, !tbaa !7
  %mul53 = mul nsw i32 %58, 2
  %add54 = add nsw i32 %mul53, 1
  %mul55 = mul nsw i32 %add54, 6
  %add56 = add nsw i32 %mul55, 4
  %arrayidx57 = getelementptr inbounds double, double* %57, i32 %add56
  store double 0.000000e+00, double* %arrayidx57, align 8, !tbaa !12
  %59 = load double*, double** %a, align 4, !tbaa !5
  %60 = load i32, i32* %i, align 4, !tbaa !7
  %mul58 = mul nsw i32 %60, 2
  %add59 = add nsw i32 %mul58, 1
  %mul60 = mul nsw i32 %add59, 6
  %add61 = add nsw i32 %mul60, 5
  %arrayidx62 = getelementptr inbounds double, double* %59, i32 %add61
  store double 1.000000e+00, double* %arrayidx62, align 8, !tbaa !12
  %61 = load double, double* %dx, align 8, !tbaa !12
  %62 = load double*, double** %b, align 4, !tbaa !5
  %63 = load i32, i32* %i, align 4, !tbaa !7
  %mul63 = mul nsw i32 2, %63
  %arrayidx64 = getelementptr inbounds double, double* %62, i32 %mul63
  store double %61, double* %arrayidx64, align 8, !tbaa !12
  %64 = load double, double* %dy, align 8, !tbaa !12
  %65 = load double*, double** %b, align 4, !tbaa !5
  %66 = load i32, i32* %i, align 4, !tbaa !7
  %mul65 = mul nsw i32 2, %66
  %add66 = add nsw i32 %mul65, 1
  %arrayidx67 = getelementptr inbounds double, double* %65, i32 %add66
  store double %64, double* %arrayidx67, align 8, !tbaa !12
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %67 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %67, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %68 = load double*, double** %a, align 4, !tbaa !5
  %69 = load i32, i32* %np2, align 4, !tbaa !7
  %70 = load double*, double** %b, align 4, !tbaa !5
  %71 = load double*, double** %temp, align 4, !tbaa !5
  %72 = load double*, double** %mat.addr, align 4, !tbaa !5
  %call68 = call i32 @least_squares(i32 6, double* %68, i32 %69, i32 6, double* %70, double* %71, double* %72)
  %tobool = icmp ne i32 %call68, 0
  br i1 %tobool, label %if.end70, label %if.then69

if.then69:                                        ; preds = %for.end
  %73 = load double*, double** %a, align 4, !tbaa !5
  %74 = bitcast double* %73 to i8*
  call void @aom_free(i8* %74)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end70:                                         ; preds = %for.end
  %75 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arraydecay71 = getelementptr inbounds [9 x double], [9 x double]* %T1, i32 0, i32 0
  %arraydecay72 = getelementptr inbounds [9 x double], [9 x double]* %T2, i32 0, i32 0
  call void @denormalize_affine_reorder(double* %75, double* %arraydecay71, double* %arraydecay72)
  %76 = load double*, double** %a, align 4, !tbaa !5
  %77 = bitcast double* %76 to i8*
  call void @aom_free(i8* %77)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end70, %if.then69
  %78 = bitcast [9 x double]* %T2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %78) #6
  %79 = bitcast [9 x double]* %T1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %79) #6
  %80 = bitcast double* %dy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %80) #6
  %81 = bitcast double* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %81) #6
  %82 = bitcast double* %sy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %82) #6
  %83 = bitcast double* %sx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %83) #6
  %84 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #6
  %85 = bitcast double** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  %86 = bitcast double** %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #6
  br label %cleanup81

cleanup81:                                        ; preds = %cleanup, %if.then
  %87 = bitcast double** %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #6
  %88 = bitcast i32* %np2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #6
  %89 = load i32, i32* %retval, align 4
  ret i32 %89
}

; Function Attrs: nounwind
define internal void @project_points_double_affine(double* %mat, double* %points, double* %proj, i32 %n, i32 %stride_points, i32 %stride_proj) #0 {
entry:
  %mat.addr = alloca double*, align 4
  %points.addr = alloca double*, align 4
  %proj.addr = alloca double*, align 4
  %n.addr = alloca i32, align 4
  %stride_points.addr = alloca i32, align 4
  %stride_proj.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %x = alloca double, align 8
  %y = alloca double, align 8
  store double* %mat, double** %mat.addr, align 4, !tbaa !5
  store double* %points, double** %points.addr, align 4, !tbaa !5
  store double* %proj, double** %proj.addr, align 4, !tbaa !5
  store i32 %n, i32* %n.addr, align 4, !tbaa !7
  store i32 %stride_points, i32* %stride_points.addr, align 4, !tbaa !7
  store i32 %stride_proj, i32* %stride_proj.addr, align 4, !tbaa !7
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !7
  %2 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast double* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #6
  %4 = load double*, double** %points.addr, align 4, !tbaa !5
  %incdec.ptr = getelementptr inbounds double, double* %4, i32 1
  store double* %incdec.ptr, double** %points.addr, align 4, !tbaa !5
  %5 = load double, double* %4, align 8, !tbaa !12
  store double %5, double* %x, align 8, !tbaa !12
  %6 = bitcast double* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #6
  %7 = load double*, double** %points.addr, align 4, !tbaa !5
  %incdec.ptr1 = getelementptr inbounds double, double* %7, i32 1
  store double* %incdec.ptr1, double** %points.addr, align 4, !tbaa !5
  %8 = load double, double* %7, align 8, !tbaa !12
  store double %8, double* %y, align 8, !tbaa !12
  %9 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %9, i32 2
  %10 = load double, double* %arrayidx, align 8, !tbaa !12
  %11 = load double, double* %x, align 8, !tbaa !12
  %mul = fmul double %10, %11
  %12 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds double, double* %12, i32 3
  %13 = load double, double* %arrayidx2, align 8, !tbaa !12
  %14 = load double, double* %y, align 8, !tbaa !12
  %mul3 = fmul double %13, %14
  %add = fadd double %mul, %mul3
  %15 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx4 = getelementptr inbounds double, double* %15, i32 0
  %16 = load double, double* %arrayidx4, align 8, !tbaa !12
  %add5 = fadd double %add, %16
  %17 = load double*, double** %proj.addr, align 4, !tbaa !5
  %incdec.ptr6 = getelementptr inbounds double, double* %17, i32 1
  store double* %incdec.ptr6, double** %proj.addr, align 4, !tbaa !5
  store double %add5, double* %17, align 8, !tbaa !12
  %18 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx7 = getelementptr inbounds double, double* %18, i32 4
  %19 = load double, double* %arrayidx7, align 8, !tbaa !12
  %20 = load double, double* %x, align 8, !tbaa !12
  %mul8 = fmul double %19, %20
  %21 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx9 = getelementptr inbounds double, double* %21, i32 5
  %22 = load double, double* %arrayidx9, align 8, !tbaa !12
  %23 = load double, double* %y, align 8, !tbaa !12
  %mul10 = fmul double %22, %23
  %add11 = fadd double %mul8, %mul10
  %24 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx12 = getelementptr inbounds double, double* %24, i32 1
  %25 = load double, double* %arrayidx12, align 8, !tbaa !12
  %add13 = fadd double %add11, %25
  %26 = load double*, double** %proj.addr, align 4, !tbaa !5
  %incdec.ptr14 = getelementptr inbounds double, double* %26, i32 1
  store double* %incdec.ptr14, double** %proj.addr, align 4, !tbaa !5
  store double %add13, double* %26, align 8, !tbaa !12
  %27 = load i32, i32* %stride_points.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %27, 2
  %28 = load double*, double** %points.addr, align 4, !tbaa !5
  %add.ptr = getelementptr inbounds double, double* %28, i32 %sub
  store double* %add.ptr, double** %points.addr, align 4, !tbaa !5
  %29 = load i32, i32* %stride_proj.addr, align 4, !tbaa !7
  %sub15 = sub nsw i32 %29, 2
  %30 = load double*, double** %proj.addr, align 4, !tbaa !5
  %add.ptr16 = getelementptr inbounds double, double* %30, i32 %sub15
  store double* %add.ptr16, double** %proj.addr, align 4, !tbaa !5
  %31 = bitcast double* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %31) #6
  %32 = bitcast double* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %32) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %33 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %34 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare i8* @aom_malloc(i32) #3

; Function Attrs: nounwind
define internal void @clear_motion(%struct.RANSAC_MOTION* %motion, i32 %num_points) #0 {
entry:
  %motion.addr = alloca %struct.RANSAC_MOTION*, align 4
  %num_points.addr = alloca i32, align 4
  store %struct.RANSAC_MOTION* %motion, %struct.RANSAC_MOTION** %motion.addr, align 4, !tbaa !5
  store i32 %num_points, i32* %num_points.addr, align 4, !tbaa !7
  %0 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion.addr, align 4, !tbaa !5
  %num_inliers = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %0, i32 0, i32 0
  store i32 0, i32* %num_inliers, align 8, !tbaa !13
  %1 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion.addr, align 4, !tbaa !5
  %variance = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %1, i32 0, i32 1
  store double 1.000000e+12, double* %variance, align 8, !tbaa !14
  %2 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion.addr, align 4, !tbaa !5
  %inlier_indices = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %2, i32 0, i32 2
  %3 = load i32*, i32** %inlier_indices, align 8, !tbaa !9
  %4 = bitcast i32* %3 to i8*
  %5 = load i32, i32* %num_points.addr, align 4, !tbaa !7
  %mul = mul i32 4, %5
  call void @llvm.memset.p0i8.i32(i8* align 4 %4, i8 0, i32 %mul, i1 false)
  ret void
}

; Function Attrs: nounwind
define internal i32 @get_rand_indices(i32 %npoints, i32 %minpts, i32* %indices, i32* %seed) #0 {
entry:
  %retval = alloca i32, align 4
  %npoints.addr = alloca i32, align 4
  %minpts.addr = alloca i32, align 4
  %indices.addr = alloca i32*, align 4
  %seed.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %ptr = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %index = alloca i32, align 4
  store i32 %npoints, i32* %npoints.addr, align 4, !tbaa !7
  store i32 %minpts, i32* %minpts.addr, align 4, !tbaa !7
  store i32* %indices, i32** %indices.addr, align 4, !tbaa !5
  store i32* %seed, i32** %seed.addr, align 4, !tbaa !5
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32*, i32** %seed.addr, align 4, !tbaa !5
  %call = call i32 @lcg_rand16(i32* %3)
  %4 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %rem = urem i32 %call, %4
  store i32 %rem, i32* %ptr, align 4, !tbaa !7
  %5 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %6 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %cmp = icmp sgt i32 %5, %6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %7 = load i32, i32* %ptr, align 4, !tbaa !7
  %8 = load i32*, i32** %indices.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 0
  store i32 %7, i32* %arrayidx, align 4, !tbaa !7
  %9 = load i32, i32* %ptr, align 4, !tbaa !7
  %10 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %10, 1
  %cmp1 = icmp eq i32 %9, %sub
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %11 = load i32, i32* %ptr, align 4, !tbaa !7
  %add = add nsw i32 %11, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %add, %cond.false ]
  store i32 %cond, i32* %ptr, align 4, !tbaa !7
  store i32 1, i32* %i, align 4, !tbaa !7
  br label %while.cond

while.cond:                                       ; preds = %while.end, %cond.end
  %12 = load i32, i32* %i, align 4, !tbaa !7
  %13 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %cmp2 = icmp slt i32 %12, %13
  br i1 %cmp2, label %while.body, label %while.end24

while.body:                                       ; preds = %while.cond
  %14 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32*, i32** %seed.addr, align 4, !tbaa !5
  %call3 = call i32 @lcg_rand16(i32* %15)
  %16 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %rem4 = urem i32 %call3, %16
  store i32 %rem4, i32* %index, align 4, !tbaa !7
  br label %while.cond5

while.cond5:                                      ; preds = %if.end21, %while.body
  %17 = load i32, i32* %index, align 4, !tbaa !7
  %tobool = icmp ne i32 %17, 0
  br i1 %tobool, label %while.body6, label %while.end

while.body6:                                      ; preds = %while.cond5
  %18 = load i32, i32* %ptr, align 4, !tbaa !7
  %19 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %sub7 = sub nsw i32 %19, 1
  %cmp8 = icmp eq i32 %18, %sub7
  br i1 %cmp8, label %cond.true9, label %cond.false10

cond.true9:                                       ; preds = %while.body6
  br label %cond.end12

cond.false10:                                     ; preds = %while.body6
  %20 = load i32, i32* %ptr, align 4, !tbaa !7
  %add11 = add nsw i32 %20, 1
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false10, %cond.true9
  %cond13 = phi i32 [ 0, %cond.true9 ], [ %add11, %cond.false10 ]
  store i32 %cond13, i32* %ptr, align 4, !tbaa !7
  store i32 0, i32* %j, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end12
  %21 = load i32, i32* %j, align 4, !tbaa !7
  %22 = load i32, i32* %i, align 4, !tbaa !7
  %cmp14 = icmp slt i32 %21, %22
  br i1 %cmp14, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load i32*, i32** %indices.addr, align 4, !tbaa !5
  %24 = load i32, i32* %j, align 4, !tbaa !7
  %arrayidx15 = getelementptr inbounds i32, i32* %23, i32 %24
  %25 = load i32, i32* %arrayidx15, align 4, !tbaa !7
  %26 = load i32, i32* %ptr, align 4, !tbaa !7
  %cmp16 = icmp eq i32 %25, %26
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.body
  br label %for.end

if.end18:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end18
  %27 = load i32, i32* %j, align 4, !tbaa !7
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %j, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %if.then17, %for.cond
  %28 = load i32, i32* %j, align 4, !tbaa !7
  %29 = load i32, i32* %i, align 4, !tbaa !7
  %cmp19 = icmp eq i32 %28, %29
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %for.end
  %30 = load i32, i32* %index, align 4, !tbaa !7
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %index, align 4, !tbaa !7
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %for.end
  br label %while.cond5

while.end:                                        ; preds = %while.cond5
  %31 = load i32, i32* %ptr, align 4, !tbaa !7
  %32 = load i32*, i32** %indices.addr, align 4, !tbaa !5
  %33 = load i32, i32* %i, align 4, !tbaa !7
  %inc22 = add nsw i32 %33, 1
  store i32 %inc22, i32* %i, align 4, !tbaa !7
  %arrayidx23 = getelementptr inbounds i32, i32* %32, i32 %33
  store i32 %31, i32* %arrayidx23, align 4, !tbaa !7
  %34 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  br label %while.cond

while.end24:                                      ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end24, %if.then
  %35 = bitcast i32* %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  %37 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  %38 = load i32, i32* %retval, align 4
  ret i32 %38
}

; Function Attrs: nounwind
define internal void @copy_points_at_indices(double* %dest, double* %src, i32* %indices, i32 %num_points) #0 {
entry:
  %dest.addr = alloca double*, align 4
  %src.addr = alloca double*, align 4
  %indices.addr = alloca i32*, align 4
  %num_points.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %index = alloca i32, align 4
  store double* %dest, double** %dest.addr, align 4, !tbaa !5
  store double* %src, double** %src.addr, align 4, !tbaa !5
  store i32* %indices, i32** %indices.addr, align 4, !tbaa !5
  store i32 %num_points, i32* %num_points.addr, align 4, !tbaa !7
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !7
  %2 = load i32, i32* %num_points.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32*, i32** %indices.addr, align 4, !tbaa !5
  %6 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  %7 = load i32, i32* %arrayidx, align 4, !tbaa !7
  store i32 %7, i32* %index, align 4, !tbaa !7
  %8 = load double*, double** %src.addr, align 4, !tbaa !5
  %9 = load i32, i32* %index, align 4, !tbaa !7
  %mul = mul nsw i32 %9, 2
  %arrayidx1 = getelementptr inbounds double, double* %8, i32 %mul
  %10 = load double, double* %arrayidx1, align 8, !tbaa !12
  %11 = load double*, double** %dest.addr, align 4, !tbaa !5
  %12 = load i32, i32* %i, align 4, !tbaa !7
  %mul2 = mul nsw i32 %12, 2
  %arrayidx3 = getelementptr inbounds double, double* %11, i32 %mul2
  store double %10, double* %arrayidx3, align 8, !tbaa !12
  %13 = load double*, double** %src.addr, align 4, !tbaa !5
  %14 = load i32, i32* %index, align 4, !tbaa !7
  %mul4 = mul nsw i32 %14, 2
  %add = add nsw i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds double, double* %13, i32 %add
  %15 = load double, double* %arrayidx5, align 8, !tbaa !12
  %16 = load double*, double** %dest.addr, align 4, !tbaa !5
  %17 = load i32, i32* %i, align 4, !tbaa !7
  %mul6 = mul nsw i32 %17, 2
  %add7 = add nsw i32 %mul6, 1
  %arrayidx8 = getelementptr inbounds double, double* %16, i32 %add7
  store double %15, double* %arrayidx8, align 8, !tbaa !12
  %18 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sqrt.f64(double) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @is_better_motion(%struct.RANSAC_MOTION* %motion_a, %struct.RANSAC_MOTION* %motion_b) #0 {
entry:
  %motion_a.addr = alloca %struct.RANSAC_MOTION*, align 4
  %motion_b.addr = alloca %struct.RANSAC_MOTION*, align 4
  store %struct.RANSAC_MOTION* %motion_a, %struct.RANSAC_MOTION** %motion_a.addr, align 4, !tbaa !5
  store %struct.RANSAC_MOTION* %motion_b, %struct.RANSAC_MOTION** %motion_b.addr, align 4, !tbaa !5
  %0 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_a.addr, align 4, !tbaa !5
  %1 = bitcast %struct.RANSAC_MOTION* %0 to i8*
  %2 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_b.addr, align 4, !tbaa !5
  %3 = bitcast %struct.RANSAC_MOTION* %2 to i8*
  %call = call i32 @compare_motions(i8* %1, i8* %3)
  %cmp = icmp slt i32 %call, 0
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare void @qsort(i8*, i32, i32, i32 (i8*, i8*)*) #3

; Function Attrs: nounwind
define internal i32 @compare_motions(i8* %arg_a, i8* %arg_b) #0 {
entry:
  %retval = alloca i32, align 4
  %arg_a.addr = alloca i8*, align 4
  %arg_b.addr = alloca i8*, align 4
  %motion_a = alloca %struct.RANSAC_MOTION*, align 4
  %motion_b = alloca %struct.RANSAC_MOTION*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %arg_a, i8** %arg_a.addr, align 4, !tbaa !5
  store i8* %arg_b, i8** %arg_b.addr, align 4, !tbaa !5
  %0 = bitcast %struct.RANSAC_MOTION** %motion_a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %arg_a.addr, align 4, !tbaa !5
  %2 = bitcast i8* %1 to %struct.RANSAC_MOTION*
  store %struct.RANSAC_MOTION* %2, %struct.RANSAC_MOTION** %motion_a, align 4, !tbaa !5
  %3 = bitcast %struct.RANSAC_MOTION** %motion_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8*, i8** %arg_b.addr, align 4, !tbaa !5
  %5 = bitcast i8* %4 to %struct.RANSAC_MOTION*
  store %struct.RANSAC_MOTION* %5, %struct.RANSAC_MOTION** %motion_b, align 4, !tbaa !5
  %6 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_a, align 4, !tbaa !5
  %num_inliers = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %6, i32 0, i32 0
  %7 = load i32, i32* %num_inliers, align 8, !tbaa !13
  %8 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_b, align 4, !tbaa !5
  %num_inliers1 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %8, i32 0, i32 0
  %9 = load i32, i32* %num_inliers1, align 8, !tbaa !13
  %cmp = icmp sgt i32 %7, %9
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %10 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_a, align 4, !tbaa !5
  %num_inliers2 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %10, i32 0, i32 0
  %11 = load i32, i32* %num_inliers2, align 8, !tbaa !13
  %12 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_b, align 4, !tbaa !5
  %num_inliers3 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %12, i32 0, i32 0
  %13 = load i32, i32* %num_inliers3, align 8, !tbaa !13
  %cmp4 = icmp slt i32 %11, %13
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.end
  %14 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_a, align 4, !tbaa !5
  %variance = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %14, i32 0, i32 1
  %15 = load double, double* %variance, align 8, !tbaa !14
  %16 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_b, align 4, !tbaa !5
  %variance7 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %16, i32 0, i32 1
  %17 = load double, double* %variance7, align 8, !tbaa !14
  %cmp8 = fcmp olt double %15, %17
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end6
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %if.end6
  %18 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_a, align 4, !tbaa !5
  %variance11 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %18, i32 0, i32 1
  %19 = load double, double* %variance11, align 8, !tbaa !14
  %20 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motion_b, align 4, !tbaa !5
  %variance12 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %20, i32 0, i32 1
  %21 = load double, double* %variance12, align 8, !tbaa !14
  %cmp13 = fcmp ogt double %19, %21
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end10
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %if.end10
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end15, %if.then14, %if.then9, %if.then5, %if.then
  %22 = bitcast %struct.RANSAC_MOTION** %motion_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast %struct.RANSAC_MOTION** %motion_a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

declare void @aom_free(i8*) #3

; Function Attrs: inlinehint nounwind
define internal i32 @lcg_rand16(i32* %state) #5 {
entry:
  %state.addr = alloca i32*, align 4
  store i32* %state, i32** %state.addr, align 4, !tbaa !5
  %0 = load i32*, i32** %state.addr, align 4, !tbaa !5
  %1 = load i32, i32* %0, align 4, !tbaa !7
  %conv = zext i32 %1 to i64
  %mul = mul i64 %conv, 1103515245
  %add = add i64 %mul, 12345
  %conv1 = trunc i64 %add to i32
  %2 = load i32*, i32** %state.addr, align 4, !tbaa !5
  store i32 %conv1, i32* %2, align 4, !tbaa !7
  %3 = load i32*, i32** %state.addr, align 4, !tbaa !5
  %4 = load i32, i32* %3, align 4, !tbaa !7
  %div = udiv i32 %4, 65536
  %rem = urem i32 %div, 32768
  ret i32 %rem
}

; Function Attrs: nounwind
define internal i32 @is_collinear3(double* %p1, double* %p2, double* %p3) #0 {
entry:
  %p1.addr = alloca double*, align 4
  %p2.addr = alloca double*, align 4
  %p3.addr = alloca double*, align 4
  %v = alloca double, align 8
  store double* %p1, double** %p1.addr, align 4, !tbaa !5
  store double* %p2, double** %p2.addr, align 4, !tbaa !5
  store double* %p3, double** %p3.addr, align 4, !tbaa !5
  %0 = bitcast double* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = load double*, double** %p2.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %1, i32 0
  %2 = load double, double* %arrayidx, align 8, !tbaa !12
  %3 = load double*, double** %p1.addr, align 4, !tbaa !5
  %arrayidx1 = getelementptr inbounds double, double* %3, i32 0
  %4 = load double, double* %arrayidx1, align 8, !tbaa !12
  %sub = fsub double %2, %4
  %5 = load double*, double** %p3.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds double, double* %5, i32 1
  %6 = load double, double* %arrayidx2, align 8, !tbaa !12
  %7 = load double*, double** %p1.addr, align 4, !tbaa !5
  %arrayidx3 = getelementptr inbounds double, double* %7, i32 1
  %8 = load double, double* %arrayidx3, align 8, !tbaa !12
  %sub4 = fsub double %6, %8
  %mul = fmul double %sub, %sub4
  %9 = load double*, double** %p2.addr, align 4, !tbaa !5
  %arrayidx5 = getelementptr inbounds double, double* %9, i32 1
  %10 = load double, double* %arrayidx5, align 8, !tbaa !12
  %11 = load double*, double** %p1.addr, align 4, !tbaa !5
  %arrayidx6 = getelementptr inbounds double, double* %11, i32 1
  %12 = load double, double* %arrayidx6, align 8, !tbaa !12
  %sub7 = fsub double %10, %12
  %13 = load double*, double** %p3.addr, align 4, !tbaa !5
  %arrayidx8 = getelementptr inbounds double, double* %13, i32 0
  %14 = load double, double* %arrayidx8, align 8, !tbaa !12
  %15 = load double*, double** %p1.addr, align 4, !tbaa !5
  %arrayidx9 = getelementptr inbounds double, double* %15, i32 0
  %16 = load double, double* %arrayidx9, align 8, !tbaa !12
  %sub10 = fsub double %14, %16
  %mul11 = fmul double %sub7, %sub10
  %sub12 = fsub double %mul, %mul11
  store double %sub12, double* %v, align 8, !tbaa !12
  %17 = load double, double* %v, align 8, !tbaa !12
  %18 = call double @llvm.fabs.f64(double %17)
  %cmp = fcmp olt double %18, 1.000000e-03
  %conv = zext i1 %cmp to i32
  %19 = bitcast double* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %19) #6
  ret i32 %conv
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #4

; Function Attrs: nounwind
define internal void @normalize_homography(double* %pts, i32 %n, double* %T) #0 {
entry:
  %pts.addr = alloca double*, align 4
  %n.addr = alloca i32, align 4
  %T.addr = alloca double*, align 4
  %p = alloca double*, align 4
  %mean = alloca [2 x double], align 16
  %msqe = alloca double, align 8
  %scale = alloca double, align 8
  %i = alloca i32, align 4
  store double* %pts, double** %pts.addr, align 4, !tbaa !5
  store i32 %n, i32* %n.addr, align 4, !tbaa !7
  store double* %T, double** %T.addr, align 4, !tbaa !5
  %0 = bitcast double** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load double*, double** %pts.addr, align 4, !tbaa !5
  store double* %1, double** %p, align 4, !tbaa !5
  %2 = bitcast [2 x double]* %mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #6
  %3 = bitcast [2 x double]* %mean to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %3, i8 0, i32 16, i1 false)
  %4 = bitcast double* %msqe to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #6
  store double 0.000000e+00, double* %msqe, align 8, !tbaa !12
  %5 = bitcast double* %scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #6
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !7
  %8 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %7, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %9, i32 0
  %10 = load double, double* %arrayidx, align 8, !tbaa !12
  %arrayidx1 = getelementptr inbounds [2 x double], [2 x double]* %mean, i32 0, i32 0
  %11 = load double, double* %arrayidx1, align 16, !tbaa !12
  %add = fadd double %11, %10
  store double %add, double* %arrayidx1, align 16, !tbaa !12
  %12 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds double, double* %12, i32 1
  %13 = load double, double* %arrayidx2, align 8, !tbaa !12
  %arrayidx3 = getelementptr inbounds [2 x double], [2 x double]* %mean, i32 0, i32 1
  %14 = load double, double* %arrayidx3, align 8, !tbaa !12
  %add4 = fadd double %14, %13
  store double %add4, double* %arrayidx3, align 8, !tbaa !12
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  %16 = load double*, double** %p, align 4, !tbaa !5
  %add.ptr = getelementptr inbounds double, double* %16, i32 2
  store double* %add.ptr, double** %p, align 4, !tbaa !5
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %17 = load i32, i32* %n.addr, align 4, !tbaa !7
  %conv = sitofp i32 %17 to double
  %arrayidx5 = getelementptr inbounds [2 x double], [2 x double]* %mean, i32 0, i32 0
  %18 = load double, double* %arrayidx5, align 16, !tbaa !12
  %div = fdiv double %18, %conv
  store double %div, double* %arrayidx5, align 16, !tbaa !12
  %19 = load i32, i32* %n.addr, align 4, !tbaa !7
  %conv6 = sitofp i32 %19 to double
  %arrayidx7 = getelementptr inbounds [2 x double], [2 x double]* %mean, i32 0, i32 1
  %20 = load double, double* %arrayidx7, align 8, !tbaa !12
  %div8 = fdiv double %20, %conv6
  store double %div8, double* %arrayidx7, align 8, !tbaa !12
  %21 = load double*, double** %pts.addr, align 4, !tbaa !5
  store double* %21, double** %p, align 4, !tbaa !5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc25, %for.end
  %22 = load i32, i32* %i, align 4, !tbaa !7
  %23 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp10 = icmp slt i32 %22, %23
  br i1 %cmp10, label %for.body12, label %for.end28

for.body12:                                       ; preds = %for.cond9
  %arrayidx13 = getelementptr inbounds [2 x double], [2 x double]* %mean, i32 0, i32 0
  %24 = load double, double* %arrayidx13, align 16, !tbaa !12
  %25 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx14 = getelementptr inbounds double, double* %25, i32 0
  %26 = load double, double* %arrayidx14, align 8, !tbaa !12
  %sub = fsub double %26, %24
  store double %sub, double* %arrayidx14, align 8, !tbaa !12
  %arrayidx15 = getelementptr inbounds [2 x double], [2 x double]* %mean, i32 0, i32 1
  %27 = load double, double* %arrayidx15, align 8, !tbaa !12
  %28 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx16 = getelementptr inbounds double, double* %28, i32 1
  %29 = load double, double* %arrayidx16, align 8, !tbaa !12
  %sub17 = fsub double %29, %27
  store double %sub17, double* %arrayidx16, align 8, !tbaa !12
  %30 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx18 = getelementptr inbounds double, double* %30, i32 0
  %31 = load double, double* %arrayidx18, align 8, !tbaa !12
  %32 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx19 = getelementptr inbounds double, double* %32, i32 0
  %33 = load double, double* %arrayidx19, align 8, !tbaa !12
  %mul = fmul double %31, %33
  %34 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx20 = getelementptr inbounds double, double* %34, i32 1
  %35 = load double, double* %arrayidx20, align 8, !tbaa !12
  %36 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx21 = getelementptr inbounds double, double* %36, i32 1
  %37 = load double, double* %arrayidx21, align 8, !tbaa !12
  %mul22 = fmul double %35, %37
  %add23 = fadd double %mul, %mul22
  %38 = call double @llvm.sqrt.f64(double %add23)
  %39 = load double, double* %msqe, align 8, !tbaa !12
  %add24 = fadd double %39, %38
  store double %add24, double* %msqe, align 8, !tbaa !12
  br label %for.inc25

for.inc25:                                        ; preds = %for.body12
  %40 = load i32, i32* %i, align 4, !tbaa !7
  %inc26 = add nsw i32 %40, 1
  store i32 %inc26, i32* %i, align 4, !tbaa !7
  %41 = load double*, double** %p, align 4, !tbaa !5
  %add.ptr27 = getelementptr inbounds double, double* %41, i32 2
  store double* %add.ptr27, double** %p, align 4, !tbaa !5
  br label %for.cond9

for.end28:                                        ; preds = %for.cond9
  %42 = load i32, i32* %n.addr, align 4, !tbaa !7
  %conv29 = sitofp i32 %42 to double
  %43 = load double, double* %msqe, align 8, !tbaa !12
  %div30 = fdiv double %43, %conv29
  store double %div30, double* %msqe, align 8, !tbaa !12
  %44 = load double, double* %msqe, align 8, !tbaa !12
  %cmp31 = fcmp oeq double %44, 0.000000e+00
  br i1 %cmp31, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end28
  br label %cond.end

cond.false:                                       ; preds = %for.end28
  %45 = call double @llvm.sqrt.f64(double 2.000000e+00)
  %46 = load double, double* %msqe, align 8, !tbaa !12
  %div33 = fdiv double %45, %46
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ 1.000000e+00, %cond.true ], [ %div33, %cond.false ]
  store double %cond, double* %scale, align 8, !tbaa !12
  %47 = load double, double* %scale, align 8, !tbaa !12
  %48 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx34 = getelementptr inbounds double, double* %48, i32 0
  store double %47, double* %arrayidx34, align 8, !tbaa !12
  %49 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx35 = getelementptr inbounds double, double* %49, i32 1
  store double 0.000000e+00, double* %arrayidx35, align 8, !tbaa !12
  %50 = load double, double* %scale, align 8, !tbaa !12
  %fneg = fneg double %50
  %arrayidx36 = getelementptr inbounds [2 x double], [2 x double]* %mean, i32 0, i32 0
  %51 = load double, double* %arrayidx36, align 16, !tbaa !12
  %mul37 = fmul double %fneg, %51
  %52 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx38 = getelementptr inbounds double, double* %52, i32 2
  store double %mul37, double* %arrayidx38, align 8, !tbaa !12
  %53 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx39 = getelementptr inbounds double, double* %53, i32 3
  store double 0.000000e+00, double* %arrayidx39, align 8, !tbaa !12
  %54 = load double, double* %scale, align 8, !tbaa !12
  %55 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx40 = getelementptr inbounds double, double* %55, i32 4
  store double %54, double* %arrayidx40, align 8, !tbaa !12
  %56 = load double, double* %scale, align 8, !tbaa !12
  %fneg41 = fneg double %56
  %arrayidx42 = getelementptr inbounds [2 x double], [2 x double]* %mean, i32 0, i32 1
  %57 = load double, double* %arrayidx42, align 8, !tbaa !12
  %mul43 = fmul double %fneg41, %57
  %58 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx44 = getelementptr inbounds double, double* %58, i32 5
  store double %mul43, double* %arrayidx44, align 8, !tbaa !12
  %59 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx45 = getelementptr inbounds double, double* %59, i32 6
  store double 0.000000e+00, double* %arrayidx45, align 8, !tbaa !12
  %60 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx46 = getelementptr inbounds double, double* %60, i32 7
  store double 0.000000e+00, double* %arrayidx46, align 8, !tbaa !12
  %61 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx47 = getelementptr inbounds double, double* %61, i32 8
  store double 1.000000e+00, double* %arrayidx47, align 8, !tbaa !12
  %62 = load double*, double** %pts.addr, align 4, !tbaa !5
  store double* %62, double** %p, align 4, !tbaa !5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc56, %cond.end
  %63 = load i32, i32* %i, align 4, !tbaa !7
  %64 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp49 = icmp slt i32 %63, %64
  br i1 %cmp49, label %for.body51, label %for.end59

for.body51:                                       ; preds = %for.cond48
  %65 = load double, double* %scale, align 8, !tbaa !12
  %66 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx52 = getelementptr inbounds double, double* %66, i32 0
  %67 = load double, double* %arrayidx52, align 8, !tbaa !12
  %mul53 = fmul double %67, %65
  store double %mul53, double* %arrayidx52, align 8, !tbaa !12
  %68 = load double, double* %scale, align 8, !tbaa !12
  %69 = load double*, double** %p, align 4, !tbaa !5
  %arrayidx54 = getelementptr inbounds double, double* %69, i32 1
  %70 = load double, double* %arrayidx54, align 8, !tbaa !12
  %mul55 = fmul double %70, %68
  store double %mul55, double* %arrayidx54, align 8, !tbaa !12
  br label %for.inc56

for.inc56:                                        ; preds = %for.body51
  %71 = load i32, i32* %i, align 4, !tbaa !7
  %inc57 = add nsw i32 %71, 1
  store i32 %inc57, i32* %i, align 4, !tbaa !7
  %72 = load double*, double** %p, align 4, !tbaa !5
  %add.ptr58 = getelementptr inbounds double, double* %72, i32 2
  store double* %add.ptr58, double** %p, align 4, !tbaa !5
  br label %for.cond48

for.end59:                                        ; preds = %for.cond48
  %73 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #6
  %74 = bitcast double* %scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %74) #6
  %75 = bitcast double* %msqe to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %75) #6
  %76 = bitcast [2 x double]* %mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %76) #6
  %77 = bitcast double** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @least_squares(i32 %n, double* %A, i32 %rows, i32 %stride, double* %b, double* %scratch, double* %x) #5 {
entry:
  %n.addr = alloca i32, align 4
  %A.addr = alloca double*, align 4
  %rows.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %b.addr = alloca double*, align 4
  %scratch.addr = alloca double*, align 4
  %x.addr = alloca double*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %scratch_ = alloca double*, align 4
  %AtA = alloca double*, align 4
  %Atb = alloca double*, align 4
  %ret = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !7
  store double* %A, double** %A.addr, align 4, !tbaa !5
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !7
  store double* %b, double** %b.addr, align 4, !tbaa !5
  store double* %scratch, double** %scratch.addr, align 4, !tbaa !5
  store double* %x, double** %x.addr, align 4, !tbaa !5
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast double** %scratch_ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store double* null, double** %scratch_, align 4, !tbaa !5
  %4 = bitcast double** %AtA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = bitcast double** %Atb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load double*, double** %scratch.addr, align 4, !tbaa !5
  %tobool = icmp ne double* %6, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %7 = load i32, i32* %n.addr, align 4, !tbaa !7
  %mul = mul i32 8, %7
  %8 = load i32, i32* %n.addr, align 4, !tbaa !7
  %add = add nsw i32 %8, 1
  %mul1 = mul i32 %mul, %add
  %call = call i8* @aom_malloc(i32 %mul1)
  %9 = bitcast i8* %call to double*
  store double* %9, double** %scratch_, align 4, !tbaa !5
  %10 = load double*, double** %scratch_, align 4, !tbaa !5
  store double* %10, double** %scratch.addr, align 4, !tbaa !5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load double*, double** %scratch.addr, align 4, !tbaa !5
  store double* %11, double** %AtA, align 4, !tbaa !5
  %12 = load double*, double** %scratch.addr, align 4, !tbaa !5
  %13 = load i32, i32* %n.addr, align 4, !tbaa !7
  %14 = load i32, i32* %n.addr, align 4, !tbaa !7
  %mul2 = mul nsw i32 %13, %14
  %add.ptr = getelementptr inbounds double, double* %12, i32 %mul2
  store double* %add.ptr, double** %Atb, align 4, !tbaa !5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc45, %if.end
  %15 = load i32, i32* %i, align 4, !tbaa !7
  %16 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %15, %16
  br i1 %cmp, label %for.body, label %for.end47

for.body:                                         ; preds = %for.cond
  %17 = load i32, i32* %i, align 4, !tbaa !7
  store i32 %17, i32* %j, align 4, !tbaa !7
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc28, %for.body
  %18 = load i32, i32* %j, align 4, !tbaa !7
  %19 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp4 = icmp slt i32 %18, %19
  br i1 %cmp4, label %for.body5, label %for.end30

for.body5:                                        ; preds = %for.cond3
  %20 = load double*, double** %AtA, align 4, !tbaa !5
  %21 = load i32, i32* %i, align 4, !tbaa !7
  %22 = load i32, i32* %n.addr, align 4, !tbaa !7
  %mul6 = mul nsw i32 %21, %22
  %23 = load i32, i32* %j, align 4, !tbaa !7
  %add7 = add nsw i32 %mul6, %23
  %arrayidx = getelementptr inbounds double, double* %20, i32 %add7
  store double 0.000000e+00, double* %arrayidx, align 8, !tbaa !12
  store i32 0, i32* %k, align 4, !tbaa !7
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body5
  %24 = load i32, i32* %k, align 4, !tbaa !7
  %25 = load i32, i32* %rows.addr, align 4, !tbaa !7
  %cmp9 = icmp slt i32 %24, %25
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %26 = load double*, double** %A.addr, align 4, !tbaa !5
  %27 = load i32, i32* %k, align 4, !tbaa !7
  %28 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul11 = mul nsw i32 %27, %28
  %29 = load i32, i32* %i, align 4, !tbaa !7
  %add12 = add nsw i32 %mul11, %29
  %arrayidx13 = getelementptr inbounds double, double* %26, i32 %add12
  %30 = load double, double* %arrayidx13, align 8, !tbaa !12
  %31 = load double*, double** %A.addr, align 4, !tbaa !5
  %32 = load i32, i32* %k, align 4, !tbaa !7
  %33 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul14 = mul nsw i32 %32, %33
  %34 = load i32, i32* %j, align 4, !tbaa !7
  %add15 = add nsw i32 %mul14, %34
  %arrayidx16 = getelementptr inbounds double, double* %31, i32 %add15
  %35 = load double, double* %arrayidx16, align 8, !tbaa !12
  %mul17 = fmul double %30, %35
  %36 = load double*, double** %AtA, align 4, !tbaa !5
  %37 = load i32, i32* %i, align 4, !tbaa !7
  %38 = load i32, i32* %n.addr, align 4, !tbaa !7
  %mul18 = mul nsw i32 %37, %38
  %39 = load i32, i32* %j, align 4, !tbaa !7
  %add19 = add nsw i32 %mul18, %39
  %arrayidx20 = getelementptr inbounds double, double* %36, i32 %add19
  %40 = load double, double* %arrayidx20, align 8, !tbaa !12
  %add21 = fadd double %40, %mul17
  store double %add21, double* %arrayidx20, align 8, !tbaa !12
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %41 = load i32, i32* %k, align 4, !tbaa !7
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %k, align 4, !tbaa !7
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %42 = load double*, double** %AtA, align 4, !tbaa !5
  %43 = load i32, i32* %i, align 4, !tbaa !7
  %44 = load i32, i32* %n.addr, align 4, !tbaa !7
  %mul22 = mul nsw i32 %43, %44
  %45 = load i32, i32* %j, align 4, !tbaa !7
  %add23 = add nsw i32 %mul22, %45
  %arrayidx24 = getelementptr inbounds double, double* %42, i32 %add23
  %46 = load double, double* %arrayidx24, align 8, !tbaa !12
  %47 = load double*, double** %AtA, align 4, !tbaa !5
  %48 = load i32, i32* %j, align 4, !tbaa !7
  %49 = load i32, i32* %n.addr, align 4, !tbaa !7
  %mul25 = mul nsw i32 %48, %49
  %50 = load i32, i32* %i, align 4, !tbaa !7
  %add26 = add nsw i32 %mul25, %50
  %arrayidx27 = getelementptr inbounds double, double* %47, i32 %add26
  store double %46, double* %arrayidx27, align 8, !tbaa !12
  br label %for.inc28

for.inc28:                                        ; preds = %for.end
  %51 = load i32, i32* %j, align 4, !tbaa !7
  %inc29 = add nsw i32 %51, 1
  store i32 %inc29, i32* %j, align 4, !tbaa !7
  br label %for.cond3

for.end30:                                        ; preds = %for.cond3
  %52 = load double*, double** %Atb, align 4, !tbaa !5
  %53 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx31 = getelementptr inbounds double, double* %52, i32 %53
  store double 0.000000e+00, double* %arrayidx31, align 8, !tbaa !12
  store i32 0, i32* %k, align 4, !tbaa !7
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc42, %for.end30
  %54 = load i32, i32* %k, align 4, !tbaa !7
  %55 = load i32, i32* %rows.addr, align 4, !tbaa !7
  %cmp33 = icmp slt i32 %54, %55
  br i1 %cmp33, label %for.body34, label %for.end44

for.body34:                                       ; preds = %for.cond32
  %56 = load double*, double** %A.addr, align 4, !tbaa !5
  %57 = load i32, i32* %k, align 4, !tbaa !7
  %58 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul35 = mul nsw i32 %57, %58
  %59 = load i32, i32* %i, align 4, !tbaa !7
  %add36 = add nsw i32 %mul35, %59
  %arrayidx37 = getelementptr inbounds double, double* %56, i32 %add36
  %60 = load double, double* %arrayidx37, align 8, !tbaa !12
  %61 = load double*, double** %b.addr, align 4, !tbaa !5
  %62 = load i32, i32* %k, align 4, !tbaa !7
  %arrayidx38 = getelementptr inbounds double, double* %61, i32 %62
  %63 = load double, double* %arrayidx38, align 8, !tbaa !12
  %mul39 = fmul double %60, %63
  %64 = load double*, double** %Atb, align 4, !tbaa !5
  %65 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx40 = getelementptr inbounds double, double* %64, i32 %65
  %66 = load double, double* %arrayidx40, align 8, !tbaa !12
  %add41 = fadd double %66, %mul39
  store double %add41, double* %arrayidx40, align 8, !tbaa !12
  br label %for.inc42

for.inc42:                                        ; preds = %for.body34
  %67 = load i32, i32* %k, align 4, !tbaa !7
  %inc43 = add nsw i32 %67, 1
  store i32 %inc43, i32* %k, align 4, !tbaa !7
  br label %for.cond32

for.end44:                                        ; preds = %for.cond32
  br label %for.inc45

for.inc45:                                        ; preds = %for.end44
  %68 = load i32, i32* %i, align 4, !tbaa !7
  %inc46 = add nsw i32 %68, 1
  store i32 %inc46, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end47:                                        ; preds = %for.cond
  %69 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #6
  %70 = load i32, i32* %n.addr, align 4, !tbaa !7
  %71 = load double*, double** %AtA, align 4, !tbaa !5
  %72 = load i32, i32* %n.addr, align 4, !tbaa !7
  %73 = load double*, double** %Atb, align 4, !tbaa !5
  %74 = load double*, double** %x.addr, align 4, !tbaa !5
  %call48 = call i32 @linsolve(i32 %70, double* %71, i32 %72, double* %73, double* %74)
  store i32 %call48, i32* %ret, align 4, !tbaa !7
  %75 = load double*, double** %scratch_, align 4, !tbaa !5
  %tobool49 = icmp ne double* %75, null
  br i1 %tobool49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %for.end47
  %76 = load double*, double** %scratch_, align 4, !tbaa !5
  %77 = bitcast double* %76 to i8*
  call void @aom_free(i8* %77)
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %for.end47
  %78 = load i32, i32* %ret, align 4, !tbaa !7
  %79 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #6
  %80 = bitcast double** %Atb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #6
  %81 = bitcast double** %AtA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #6
  %82 = bitcast double** %scratch_ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #6
  %83 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #6
  %84 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #6
  %85 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  ret i32 %78
}

; Function Attrs: nounwind
define internal void @denormalize_affine_reorder(double* %params, double* %T1, double* %T2) #0 {
entry:
  %params.addr = alloca double*, align 4
  %T1.addr = alloca double*, align 4
  %T2.addr = alloca double*, align 4
  %params_denorm = alloca [9 x double], align 16
  store double* %params, double** %params.addr, align 4, !tbaa !5
  store double* %T1, double** %T1.addr, align 4, !tbaa !5
  store double* %T2, double** %T2.addr, align 4, !tbaa !5
  %0 = bitcast [9 x double]* %params_denorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %0) #6
  %1 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %1, i32 0
  %2 = load double, double* %arrayidx, align 8, !tbaa !12
  %arrayidx1 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 0
  store double %2, double* %arrayidx1, align 16, !tbaa !12
  %3 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds double, double* %3, i32 1
  %4 = load double, double* %arrayidx2, align 8, !tbaa !12
  %arrayidx3 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 1
  store double %4, double* %arrayidx3, align 8, !tbaa !12
  %5 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx4 = getelementptr inbounds double, double* %5, i32 4
  %6 = load double, double* %arrayidx4, align 8, !tbaa !12
  %arrayidx5 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 2
  store double %6, double* %arrayidx5, align 16, !tbaa !12
  %7 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx6 = getelementptr inbounds double, double* %7, i32 2
  %8 = load double, double* %arrayidx6, align 8, !tbaa !12
  %arrayidx7 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 3
  store double %8, double* %arrayidx7, align 8, !tbaa !12
  %9 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx8 = getelementptr inbounds double, double* %9, i32 3
  %10 = load double, double* %arrayidx8, align 8, !tbaa !12
  %arrayidx9 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 4
  store double %10, double* %arrayidx9, align 16, !tbaa !12
  %11 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx10 = getelementptr inbounds double, double* %11, i32 5
  %12 = load double, double* %arrayidx10, align 8, !tbaa !12
  %arrayidx11 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 5
  store double %12, double* %arrayidx11, align 8, !tbaa !12
  %arrayidx12 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 7
  store double 0.000000e+00, double* %arrayidx12, align 8, !tbaa !12
  %arrayidx13 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 6
  store double 0.000000e+00, double* %arrayidx13, align 16, !tbaa !12
  %arrayidx14 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 8
  store double 1.000000e+00, double* %arrayidx14, align 16, !tbaa !12
  %arraydecay = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 0
  %13 = load double*, double** %T1.addr, align 4, !tbaa !5
  %14 = load double*, double** %T2.addr, align 4, !tbaa !5
  call void @denormalize_homography(double* %arraydecay, double* %13, double* %14)
  %arrayidx15 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 2
  %15 = load double, double* %arrayidx15, align 16, !tbaa !12
  %16 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx16 = getelementptr inbounds double, double* %16, i32 0
  store double %15, double* %arrayidx16, align 8, !tbaa !12
  %arrayidx17 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 5
  %17 = load double, double* %arrayidx17, align 8, !tbaa !12
  %18 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx18 = getelementptr inbounds double, double* %18, i32 1
  store double %17, double* %arrayidx18, align 8, !tbaa !12
  %arrayidx19 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 0
  %19 = load double, double* %arrayidx19, align 16, !tbaa !12
  %20 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx20 = getelementptr inbounds double, double* %20, i32 2
  store double %19, double* %arrayidx20, align 8, !tbaa !12
  %arrayidx21 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 1
  %21 = load double, double* %arrayidx21, align 8, !tbaa !12
  %22 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx22 = getelementptr inbounds double, double* %22, i32 3
  store double %21, double* %arrayidx22, align 8, !tbaa !12
  %arrayidx23 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 3
  %23 = load double, double* %arrayidx23, align 8, !tbaa !12
  %24 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx24 = getelementptr inbounds double, double* %24, i32 4
  store double %23, double* %arrayidx24, align 8, !tbaa !12
  %arrayidx25 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 4
  %25 = load double, double* %arrayidx25, align 16, !tbaa !12
  %26 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx26 = getelementptr inbounds double, double* %26, i32 5
  store double %25, double* %arrayidx26, align 8, !tbaa !12
  %27 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx27 = getelementptr inbounds double, double* %27, i32 7
  store double 0.000000e+00, double* %arrayidx27, align 8, !tbaa !12
  %28 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx28 = getelementptr inbounds double, double* %28, i32 6
  store double 0.000000e+00, double* %arrayidx28, align 8, !tbaa !12
  %29 = bitcast [9 x double]* %params_denorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %29) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @linsolve(i32 %n, double* %A, i32 %stride, double* %b, double* %x) #5 {
entry:
  %retval = alloca i32, align 4
  %n.addr = alloca i32, align 4
  %A.addr = alloca double*, align 4
  %stride.addr = alloca i32, align 4
  %b.addr = alloca double*, align 4
  %x.addr = alloca double*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %c = alloca double, align 8
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !7
  store double* %A, double** %A.addr, align 4, !tbaa !5
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !7
  store double* %b, double** %b.addr, align 4, !tbaa !5
  store double* %x, double** %x.addr, align 4, !tbaa !5
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast double* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #6
  store i32 0, i32* %k, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc75, %entry
  %4 = load i32, i32* %k, align 4, !tbaa !7
  %5 = load i32, i32* %n.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %5, 1
  %cmp = icmp slt i32 %4, %sub
  br i1 %cmp, label %for.body, label %for.end77

for.body:                                         ; preds = %for.cond
  %6 = load i32, i32* %n.addr, align 4, !tbaa !7
  %sub1 = sub nsw i32 %6, 1
  store i32 %sub1, i32* %i, align 4, !tbaa !7
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc33, %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !7
  %8 = load i32, i32* %k, align 4, !tbaa !7
  %cmp3 = icmp sgt i32 %7, %8
  br i1 %cmp3, label %for.body4, label %for.end34

for.body4:                                        ; preds = %for.cond2
  %9 = load double*, double** %A.addr, align 4, !tbaa !5
  %10 = load i32, i32* %i, align 4, !tbaa !7
  %sub5 = sub nsw i32 %10, 1
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul = mul nsw i32 %sub5, %11
  %12 = load i32, i32* %k, align 4, !tbaa !7
  %add = add nsw i32 %mul, %12
  %arrayidx = getelementptr inbounds double, double* %9, i32 %add
  %13 = load double, double* %arrayidx, align 8, !tbaa !12
  %14 = call double @llvm.fabs.f64(double %13)
  %15 = load double*, double** %A.addr, align 4, !tbaa !5
  %16 = load i32, i32* %i, align 4, !tbaa !7
  %17 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul6 = mul nsw i32 %16, %17
  %18 = load i32, i32* %k, align 4, !tbaa !7
  %add7 = add nsw i32 %mul6, %18
  %arrayidx8 = getelementptr inbounds double, double* %15, i32 %add7
  %19 = load double, double* %arrayidx8, align 8, !tbaa !12
  %20 = call double @llvm.fabs.f64(double %19)
  %cmp9 = fcmp olt double %14, %20
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body4
  store i32 0, i32* %j, align 4, !tbaa !7
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %if.then
  %21 = load i32, i32* %j, align 4, !tbaa !7
  %22 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp11 = icmp slt i32 %21, %22
  br i1 %cmp11, label %for.body12, label %for.end

for.body12:                                       ; preds = %for.cond10
  %23 = load double*, double** %A.addr, align 4, !tbaa !5
  %24 = load i32, i32* %i, align 4, !tbaa !7
  %25 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul13 = mul nsw i32 %24, %25
  %26 = load i32, i32* %j, align 4, !tbaa !7
  %add14 = add nsw i32 %mul13, %26
  %arrayidx15 = getelementptr inbounds double, double* %23, i32 %add14
  %27 = load double, double* %arrayidx15, align 8, !tbaa !12
  store double %27, double* %c, align 8, !tbaa !12
  %28 = load double*, double** %A.addr, align 4, !tbaa !5
  %29 = load i32, i32* %i, align 4, !tbaa !7
  %sub16 = sub nsw i32 %29, 1
  %30 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul17 = mul nsw i32 %sub16, %30
  %31 = load i32, i32* %j, align 4, !tbaa !7
  %add18 = add nsw i32 %mul17, %31
  %arrayidx19 = getelementptr inbounds double, double* %28, i32 %add18
  %32 = load double, double* %arrayidx19, align 8, !tbaa !12
  %33 = load double*, double** %A.addr, align 4, !tbaa !5
  %34 = load i32, i32* %i, align 4, !tbaa !7
  %35 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul20 = mul nsw i32 %34, %35
  %36 = load i32, i32* %j, align 4, !tbaa !7
  %add21 = add nsw i32 %mul20, %36
  %arrayidx22 = getelementptr inbounds double, double* %33, i32 %add21
  store double %32, double* %arrayidx22, align 8, !tbaa !12
  %37 = load double, double* %c, align 8, !tbaa !12
  %38 = load double*, double** %A.addr, align 4, !tbaa !5
  %39 = load i32, i32* %i, align 4, !tbaa !7
  %sub23 = sub nsw i32 %39, 1
  %40 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul24 = mul nsw i32 %sub23, %40
  %41 = load i32, i32* %j, align 4, !tbaa !7
  %add25 = add nsw i32 %mul24, %41
  %arrayidx26 = getelementptr inbounds double, double* %38, i32 %add25
  store double %37, double* %arrayidx26, align 8, !tbaa !12
  br label %for.inc

for.inc:                                          ; preds = %for.body12
  %42 = load i32, i32* %j, align 4, !tbaa !7
  %inc = add nsw i32 %42, 1
  store i32 %inc, i32* %j, align 4, !tbaa !7
  br label %for.cond10

for.end:                                          ; preds = %for.cond10
  %43 = load double*, double** %b.addr, align 4, !tbaa !5
  %44 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx27 = getelementptr inbounds double, double* %43, i32 %44
  %45 = load double, double* %arrayidx27, align 8, !tbaa !12
  store double %45, double* %c, align 8, !tbaa !12
  %46 = load double*, double** %b.addr, align 4, !tbaa !5
  %47 = load i32, i32* %i, align 4, !tbaa !7
  %sub28 = sub nsw i32 %47, 1
  %arrayidx29 = getelementptr inbounds double, double* %46, i32 %sub28
  %48 = load double, double* %arrayidx29, align 8, !tbaa !12
  %49 = load double*, double** %b.addr, align 4, !tbaa !5
  %50 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx30 = getelementptr inbounds double, double* %49, i32 %50
  store double %48, double* %arrayidx30, align 8, !tbaa !12
  %51 = load double, double* %c, align 8, !tbaa !12
  %52 = load double*, double** %b.addr, align 4, !tbaa !5
  %53 = load i32, i32* %i, align 4, !tbaa !7
  %sub31 = sub nsw i32 %53, 1
  %arrayidx32 = getelementptr inbounds double, double* %52, i32 %sub31
  store double %51, double* %arrayidx32, align 8, !tbaa !12
  br label %if.end

if.end:                                           ; preds = %for.end, %for.body4
  br label %for.inc33

for.inc33:                                        ; preds = %if.end
  %54 = load i32, i32* %i, align 4, !tbaa !7
  %dec = add nsw i32 %54, -1
  store i32 %dec, i32* %i, align 4, !tbaa !7
  br label %for.cond2

for.end34:                                        ; preds = %for.cond2
  %55 = load i32, i32* %k, align 4, !tbaa !7
  store i32 %55, i32* %i, align 4, !tbaa !7
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc72, %for.end34
  %56 = load i32, i32* %i, align 4, !tbaa !7
  %57 = load i32, i32* %n.addr, align 4, !tbaa !7
  %sub36 = sub nsw i32 %57, 1
  %cmp37 = icmp slt i32 %56, %sub36
  br i1 %cmp37, label %for.body38, label %for.end74

for.body38:                                       ; preds = %for.cond35
  %58 = load double*, double** %A.addr, align 4, !tbaa !5
  %59 = load i32, i32* %k, align 4, !tbaa !7
  %60 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul39 = mul nsw i32 %59, %60
  %61 = load i32, i32* %k, align 4, !tbaa !7
  %add40 = add nsw i32 %mul39, %61
  %arrayidx41 = getelementptr inbounds double, double* %58, i32 %add40
  %62 = load double, double* %arrayidx41, align 8, !tbaa !12
  %63 = call double @llvm.fabs.f64(double %62)
  %cmp42 = fcmp olt double %63, 0x3C9CD2B297D889BC
  br i1 %cmp42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %for.body38
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end44:                                         ; preds = %for.body38
  %64 = load double*, double** %A.addr, align 4, !tbaa !5
  %65 = load i32, i32* %i, align 4, !tbaa !7
  %add45 = add nsw i32 %65, 1
  %66 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul46 = mul nsw i32 %add45, %66
  %67 = load i32, i32* %k, align 4, !tbaa !7
  %add47 = add nsw i32 %mul46, %67
  %arrayidx48 = getelementptr inbounds double, double* %64, i32 %add47
  %68 = load double, double* %arrayidx48, align 8, !tbaa !12
  %69 = load double*, double** %A.addr, align 4, !tbaa !5
  %70 = load i32, i32* %k, align 4, !tbaa !7
  %71 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul49 = mul nsw i32 %70, %71
  %72 = load i32, i32* %k, align 4, !tbaa !7
  %add50 = add nsw i32 %mul49, %72
  %arrayidx51 = getelementptr inbounds double, double* %69, i32 %add50
  %73 = load double, double* %arrayidx51, align 8, !tbaa !12
  %div = fdiv double %68, %73
  store double %div, double* %c, align 8, !tbaa !12
  store i32 0, i32* %j, align 4, !tbaa !7
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc64, %if.end44
  %74 = load i32, i32* %j, align 4, !tbaa !7
  %75 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp53 = icmp slt i32 %74, %75
  br i1 %cmp53, label %for.body54, label %for.end66

for.body54:                                       ; preds = %for.cond52
  %76 = load double, double* %c, align 8, !tbaa !12
  %77 = load double*, double** %A.addr, align 4, !tbaa !5
  %78 = load i32, i32* %k, align 4, !tbaa !7
  %79 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul55 = mul nsw i32 %78, %79
  %80 = load i32, i32* %j, align 4, !tbaa !7
  %add56 = add nsw i32 %mul55, %80
  %arrayidx57 = getelementptr inbounds double, double* %77, i32 %add56
  %81 = load double, double* %arrayidx57, align 8, !tbaa !12
  %mul58 = fmul double %76, %81
  %82 = load double*, double** %A.addr, align 4, !tbaa !5
  %83 = load i32, i32* %i, align 4, !tbaa !7
  %add59 = add nsw i32 %83, 1
  %84 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul60 = mul nsw i32 %add59, %84
  %85 = load i32, i32* %j, align 4, !tbaa !7
  %add61 = add nsw i32 %mul60, %85
  %arrayidx62 = getelementptr inbounds double, double* %82, i32 %add61
  %86 = load double, double* %arrayidx62, align 8, !tbaa !12
  %sub63 = fsub double %86, %mul58
  store double %sub63, double* %arrayidx62, align 8, !tbaa !12
  br label %for.inc64

for.inc64:                                        ; preds = %for.body54
  %87 = load i32, i32* %j, align 4, !tbaa !7
  %inc65 = add nsw i32 %87, 1
  store i32 %inc65, i32* %j, align 4, !tbaa !7
  br label %for.cond52

for.end66:                                        ; preds = %for.cond52
  %88 = load double, double* %c, align 8, !tbaa !12
  %89 = load double*, double** %b.addr, align 4, !tbaa !5
  %90 = load i32, i32* %k, align 4, !tbaa !7
  %arrayidx67 = getelementptr inbounds double, double* %89, i32 %90
  %91 = load double, double* %arrayidx67, align 8, !tbaa !12
  %mul68 = fmul double %88, %91
  %92 = load double*, double** %b.addr, align 4, !tbaa !5
  %93 = load i32, i32* %i, align 4, !tbaa !7
  %add69 = add nsw i32 %93, 1
  %arrayidx70 = getelementptr inbounds double, double* %92, i32 %add69
  %94 = load double, double* %arrayidx70, align 8, !tbaa !12
  %sub71 = fsub double %94, %mul68
  store double %sub71, double* %arrayidx70, align 8, !tbaa !12
  br label %for.inc72

for.inc72:                                        ; preds = %for.end66
  %95 = load i32, i32* %i, align 4, !tbaa !7
  %inc73 = add nsw i32 %95, 1
  store i32 %inc73, i32* %i, align 4, !tbaa !7
  br label %for.cond35

for.end74:                                        ; preds = %for.cond35
  br label %for.inc75

for.inc75:                                        ; preds = %for.end74
  %96 = load i32, i32* %k, align 4, !tbaa !7
  %inc76 = add nsw i32 %96, 1
  store i32 %inc76, i32* %k, align 4, !tbaa !7
  br label %for.cond

for.end77:                                        ; preds = %for.cond
  %97 = load i32, i32* %n.addr, align 4, !tbaa !7
  %sub78 = sub nsw i32 %97, 1
  store i32 %sub78, i32* %i, align 4, !tbaa !7
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc109, %for.end77
  %98 = load i32, i32* %i, align 4, !tbaa !7
  %cmp80 = icmp sge i32 %98, 0
  br i1 %cmp80, label %for.body81, label %for.end111

for.body81:                                       ; preds = %for.cond79
  %99 = load double*, double** %A.addr, align 4, !tbaa !5
  %100 = load i32, i32* %i, align 4, !tbaa !7
  %101 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul82 = mul nsw i32 %100, %101
  %102 = load i32, i32* %i, align 4, !tbaa !7
  %add83 = add nsw i32 %mul82, %102
  %arrayidx84 = getelementptr inbounds double, double* %99, i32 %add83
  %103 = load double, double* %arrayidx84, align 8, !tbaa !12
  %104 = call double @llvm.fabs.f64(double %103)
  %cmp85 = fcmp olt double %104, 0x3C9CD2B297D889BC
  br i1 %cmp85, label %if.then86, label %if.end87

if.then86:                                        ; preds = %for.body81
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end87:                                         ; preds = %for.body81
  store double 0.000000e+00, double* %c, align 8, !tbaa !12
  %105 = load i32, i32* %i, align 4, !tbaa !7
  %add88 = add nsw i32 %105, 1
  store i32 %add88, i32* %j, align 4, !tbaa !7
  br label %for.cond89

for.cond89:                                       ; preds = %for.inc99, %if.end87
  %106 = load i32, i32* %j, align 4, !tbaa !7
  %107 = load i32, i32* %n.addr, align 4, !tbaa !7
  %sub90 = sub nsw i32 %107, 1
  %cmp91 = icmp sle i32 %106, %sub90
  br i1 %cmp91, label %for.body92, label %for.end101

for.body92:                                       ; preds = %for.cond89
  %108 = load double*, double** %A.addr, align 4, !tbaa !5
  %109 = load i32, i32* %i, align 4, !tbaa !7
  %110 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul93 = mul nsw i32 %109, %110
  %111 = load i32, i32* %j, align 4, !tbaa !7
  %add94 = add nsw i32 %mul93, %111
  %arrayidx95 = getelementptr inbounds double, double* %108, i32 %add94
  %112 = load double, double* %arrayidx95, align 8, !tbaa !12
  %113 = load double*, double** %x.addr, align 4, !tbaa !5
  %114 = load i32, i32* %j, align 4, !tbaa !7
  %arrayidx96 = getelementptr inbounds double, double* %113, i32 %114
  %115 = load double, double* %arrayidx96, align 8, !tbaa !12
  %mul97 = fmul double %112, %115
  %116 = load double, double* %c, align 8, !tbaa !12
  %add98 = fadd double %116, %mul97
  store double %add98, double* %c, align 8, !tbaa !12
  br label %for.inc99

for.inc99:                                        ; preds = %for.body92
  %117 = load i32, i32* %j, align 4, !tbaa !7
  %inc100 = add nsw i32 %117, 1
  store i32 %inc100, i32* %j, align 4, !tbaa !7
  br label %for.cond89

for.end101:                                       ; preds = %for.cond89
  %118 = load double*, double** %b.addr, align 4, !tbaa !5
  %119 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx102 = getelementptr inbounds double, double* %118, i32 %119
  %120 = load double, double* %arrayidx102, align 8, !tbaa !12
  %121 = load double, double* %c, align 8, !tbaa !12
  %sub103 = fsub double %120, %121
  %122 = load double*, double** %A.addr, align 4, !tbaa !5
  %123 = load i32, i32* %i, align 4, !tbaa !7
  %124 = load i32, i32* %stride.addr, align 4, !tbaa !7
  %mul104 = mul nsw i32 %123, %124
  %125 = load i32, i32* %i, align 4, !tbaa !7
  %add105 = add nsw i32 %mul104, %125
  %arrayidx106 = getelementptr inbounds double, double* %122, i32 %add105
  %126 = load double, double* %arrayidx106, align 8, !tbaa !12
  %div107 = fdiv double %sub103, %126
  %127 = load double*, double** %x.addr, align 4, !tbaa !5
  %128 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx108 = getelementptr inbounds double, double* %127, i32 %128
  store double %div107, double* %arrayidx108, align 8, !tbaa !12
  br label %for.inc109

for.inc109:                                       ; preds = %for.end101
  %129 = load i32, i32* %i, align 4, !tbaa !7
  %dec110 = add nsw i32 %129, -1
  store i32 %dec110, i32* %i, align 4, !tbaa !7
  br label %for.cond79

for.end111:                                       ; preds = %for.cond79
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end111, %if.then86, %if.then43
  %130 = bitcast double* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %130) #6
  %131 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #6
  %132 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #6
  %133 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #6
  %134 = load i32, i32* %retval, align 4
  ret i32 %134
}

; Function Attrs: nounwind
define internal void @denormalize_homography(double* %params, double* %T1, double* %T2) #0 {
entry:
  %params.addr = alloca double*, align 4
  %T1.addr = alloca double*, align 4
  %T2.addr = alloca double*, align 4
  %iT2 = alloca [9 x double], align 16
  %params2 = alloca [9 x double], align 16
  store double* %params, double** %params.addr, align 4, !tbaa !5
  store double* %T1, double** %T1.addr, align 4, !tbaa !5
  store double* %T2, double** %T2.addr, align 4, !tbaa !5
  %0 = bitcast [9 x double]* %iT2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %0) #6
  %1 = bitcast [9 x double]* %params2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %1) #6
  %2 = load double*, double** %T2.addr, align 4, !tbaa !5
  %arraydecay = getelementptr inbounds [9 x double], [9 x double]* %iT2, i32 0, i32 0
  call void @invnormalize_mat(double* %2, double* %arraydecay)
  %3 = load double*, double** %params.addr, align 4, !tbaa !5
  %4 = load double*, double** %T1.addr, align 4, !tbaa !5
  %arraydecay1 = getelementptr inbounds [9 x double], [9 x double]* %params2, i32 0, i32 0
  call void @multiply_mat(double* %3, double* %4, double* %arraydecay1, i32 3, i32 3, i32 3)
  %arraydecay2 = getelementptr inbounds [9 x double], [9 x double]* %iT2, i32 0, i32 0
  %arraydecay3 = getelementptr inbounds [9 x double], [9 x double]* %params2, i32 0, i32 0
  %5 = load double*, double** %params.addr, align 4, !tbaa !5
  call void @multiply_mat(double* %arraydecay2, double* %arraydecay3, double* %5, i32 3, i32 3, i32 3)
  %6 = bitcast [9 x double]* %params2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %6) #6
  %7 = bitcast [9 x double]* %iT2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %7) #6
  ret void
}

; Function Attrs: nounwind
define internal void @invnormalize_mat(double* %T, double* %iT) #0 {
entry:
  %T.addr = alloca double*, align 4
  %iT.addr = alloca double*, align 4
  %is = alloca double, align 8
  %m0 = alloca double, align 8
  %m1 = alloca double, align 8
  store double* %T, double** %T.addr, align 4, !tbaa !5
  store double* %iT, double** %iT.addr, align 4, !tbaa !5
  %0 = bitcast double* %is to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %1, i32 0
  %2 = load double, double* %arrayidx, align 8, !tbaa !12
  %div = fdiv double 1.000000e+00, %2
  store double %div, double* %is, align 8, !tbaa !12
  %3 = bitcast double* %m0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #6
  %4 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx1 = getelementptr inbounds double, double* %4, i32 2
  %5 = load double, double* %arrayidx1, align 8, !tbaa !12
  %fneg = fneg double %5
  %6 = load double, double* %is, align 8, !tbaa !12
  %mul = fmul double %fneg, %6
  store double %mul, double* %m0, align 8, !tbaa !12
  %7 = bitcast double* %m1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #6
  %8 = load double*, double** %T.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds double, double* %8, i32 5
  %9 = load double, double* %arrayidx2, align 8, !tbaa !12
  %fneg3 = fneg double %9
  %10 = load double, double* %is, align 8, !tbaa !12
  %mul4 = fmul double %fneg3, %10
  store double %mul4, double* %m1, align 8, !tbaa !12
  %11 = load double, double* %is, align 8, !tbaa !12
  %12 = load double*, double** %iT.addr, align 4, !tbaa !5
  %arrayidx5 = getelementptr inbounds double, double* %12, i32 0
  store double %11, double* %arrayidx5, align 8, !tbaa !12
  %13 = load double*, double** %iT.addr, align 4, !tbaa !5
  %arrayidx6 = getelementptr inbounds double, double* %13, i32 1
  store double 0.000000e+00, double* %arrayidx6, align 8, !tbaa !12
  %14 = load double, double* %m0, align 8, !tbaa !12
  %15 = load double*, double** %iT.addr, align 4, !tbaa !5
  %arrayidx7 = getelementptr inbounds double, double* %15, i32 2
  store double %14, double* %arrayidx7, align 8, !tbaa !12
  %16 = load double*, double** %iT.addr, align 4, !tbaa !5
  %arrayidx8 = getelementptr inbounds double, double* %16, i32 3
  store double 0.000000e+00, double* %arrayidx8, align 8, !tbaa !12
  %17 = load double, double* %is, align 8, !tbaa !12
  %18 = load double*, double** %iT.addr, align 4, !tbaa !5
  %arrayidx9 = getelementptr inbounds double, double* %18, i32 4
  store double %17, double* %arrayidx9, align 8, !tbaa !12
  %19 = load double, double* %m1, align 8, !tbaa !12
  %20 = load double*, double** %iT.addr, align 4, !tbaa !5
  %arrayidx10 = getelementptr inbounds double, double* %20, i32 5
  store double %19, double* %arrayidx10, align 8, !tbaa !12
  %21 = load double*, double** %iT.addr, align 4, !tbaa !5
  %arrayidx11 = getelementptr inbounds double, double* %21, i32 6
  store double 0.000000e+00, double* %arrayidx11, align 8, !tbaa !12
  %22 = load double*, double** %iT.addr, align 4, !tbaa !5
  %arrayidx12 = getelementptr inbounds double, double* %22, i32 7
  store double 0.000000e+00, double* %arrayidx12, align 8, !tbaa !12
  %23 = load double*, double** %iT.addr, align 4, !tbaa !5
  %arrayidx13 = getelementptr inbounds double, double* %23, i32 8
  store double 1.000000e+00, double* %arrayidx13, align 8, !tbaa !12
  %24 = bitcast double* %m1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %24) #6
  %25 = bitcast double* %m0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %25) #6
  %26 = bitcast double* %is to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %26) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @multiply_mat(double* %m1, double* %m2, double* %res, i32 %m1_rows, i32 %inner_dim, i32 %m2_cols) #5 {
entry:
  %m1.addr = alloca double*, align 4
  %m2.addr = alloca double*, align 4
  %res.addr = alloca double*, align 4
  %m1_rows.addr = alloca i32, align 4
  %inner_dim.addr = alloca i32, align 4
  %m2_cols.addr = alloca i32, align 4
  %sum = alloca double, align 8
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %inner = alloca i32, align 4
  store double* %m1, double** %m1.addr, align 4, !tbaa !5
  store double* %m2, double** %m2.addr, align 4, !tbaa !5
  store double* %res, double** %res.addr, align 4, !tbaa !5
  store i32 %m1_rows, i32* %m1_rows.addr, align 4, !tbaa !7
  store i32 %inner_dim, i32* %inner_dim.addr, align 4, !tbaa !7
  store i32 %m2_cols, i32* %m2_cols.addr, align 4, !tbaa !7
  %0 = bitcast double* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %inner to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store i32 0, i32* %row, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %4 = load i32, i32* %row, align 4, !tbaa !7
  %5 = load i32, i32* %m1_rows.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %col, align 4, !tbaa !7
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc12, %for.body
  %6 = load i32, i32* %col, align 4, !tbaa !7
  %7 = load i32, i32* %m2_cols.addr, align 4, !tbaa !7
  %cmp2 = icmp slt i32 %6, %7
  br i1 %cmp2, label %for.body3, label %for.end14

for.body3:                                        ; preds = %for.cond1
  store double 0.000000e+00, double* %sum, align 8, !tbaa !12
  store i32 0, i32* %inner, align 4, !tbaa !7
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body3
  %8 = load i32, i32* %inner, align 4, !tbaa !7
  %9 = load i32, i32* %inner_dim.addr, align 4, !tbaa !7
  %cmp5 = icmp slt i32 %8, %9
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %10 = load double*, double** %m1.addr, align 4, !tbaa !5
  %11 = load i32, i32* %row, align 4, !tbaa !7
  %12 = load i32, i32* %inner_dim.addr, align 4, !tbaa !7
  %mul = mul nsw i32 %11, %12
  %13 = load i32, i32* %inner, align 4, !tbaa !7
  %add = add nsw i32 %mul, %13
  %arrayidx = getelementptr inbounds double, double* %10, i32 %add
  %14 = load double, double* %arrayidx, align 8, !tbaa !12
  %15 = load double*, double** %m2.addr, align 4, !tbaa !5
  %16 = load i32, i32* %inner, align 4, !tbaa !7
  %17 = load i32, i32* %m2_cols.addr, align 4, !tbaa !7
  %mul7 = mul nsw i32 %16, %17
  %18 = load i32, i32* %col, align 4, !tbaa !7
  %add8 = add nsw i32 %mul7, %18
  %arrayidx9 = getelementptr inbounds double, double* %15, i32 %add8
  %19 = load double, double* %arrayidx9, align 8, !tbaa !12
  %mul10 = fmul double %14, %19
  %20 = load double, double* %sum, align 8, !tbaa !12
  %add11 = fadd double %20, %mul10
  store double %add11, double* %sum, align 8, !tbaa !12
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %21 = load i32, i32* %inner, align 4, !tbaa !7
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %inner, align 4, !tbaa !7
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %22 = load double, double* %sum, align 8, !tbaa !12
  %23 = load double*, double** %res.addr, align 4, !tbaa !5
  %incdec.ptr = getelementptr inbounds double, double* %23, i32 1
  store double* %incdec.ptr, double** %res.addr, align 4, !tbaa !5
  store double %22, double* %23, align 8, !tbaa !12
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %24 = load i32, i32* %col, align 4, !tbaa !7
  %inc13 = add nsw i32 %24, 1
  store i32 %inc13, i32* %col, align 4, !tbaa !7
  br label %for.cond1

for.end14:                                        ; preds = %for.cond1
  br label %for.inc15

for.inc15:                                        ; preds = %for.end14
  %25 = load i32, i32* %row, align 4, !tbaa !7
  %inc16 = add nsw i32 %25, 1
  store i32 %inc16, i32* %row, align 4, !tbaa !7
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  %26 = bitcast i32* %inner to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  %27 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #6
  %28 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  %29 = bitcast double* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %29) #6
  ret void
}

; Function Attrs: nounwind
define internal i32 @find_rotzoom(i32 %np, double* %pts1, double* %pts2, double* %mat) #0 {
entry:
  %retval = alloca i32, align 4
  %np.addr = alloca i32, align 4
  %pts1.addr = alloca double*, align 4
  %pts2.addr = alloca double*, align 4
  %mat.addr = alloca double*, align 4
  %np2 = alloca i32, align 4
  %a = alloca double*, align 4
  %b = alloca double*, align 4
  %temp = alloca double*, align 4
  %i = alloca i32, align 4
  %sx = alloca double, align 8
  %sy = alloca double, align 8
  %dx = alloca double, align 8
  %dy = alloca double, align 8
  %T1 = alloca [9 x double], align 16
  %T2 = alloca [9 x double], align 16
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %np, i32* %np.addr, align 4, !tbaa !7
  store double* %pts1, double** %pts1.addr, align 4, !tbaa !5
  store double* %pts2, double** %pts2.addr, align 4, !tbaa !5
  store double* %mat, double** %mat.addr, align 4, !tbaa !5
  %0 = bitcast i32* %np2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %np.addr, align 4, !tbaa !7
  %mul = mul nsw i32 %1, 2
  store i32 %mul, i32* %np2, align 4, !tbaa !7
  %2 = bitcast double** %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %np2, align 4, !tbaa !7
  %mul1 = mul nsw i32 %3, 5
  %add = add nsw i32 %mul1, 20
  %mul2 = mul i32 8, %add
  %call = call i8* @aom_malloc(i32 %mul2)
  %4 = bitcast i8* %call to double*
  store double* %4, double** %a, align 4, !tbaa !5
  %5 = bitcast double** %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load double*, double** %a, align 4, !tbaa !5
  %7 = load i32, i32* %np2, align 4, !tbaa !7
  %mul3 = mul nsw i32 %7, 4
  %add.ptr = getelementptr inbounds double, double* %6, i32 %mul3
  store double* %add.ptr, double** %b, align 4, !tbaa !5
  %8 = bitcast double** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load double*, double** %b, align 4, !tbaa !5
  %10 = load i32, i32* %np2, align 4, !tbaa !7
  %add.ptr4 = getelementptr inbounds double, double* %9, i32 %10
  store double* %add.ptr4, double** %temp, align 4, !tbaa !5
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = bitcast double* %sx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %12) #6
  %13 = bitcast double* %sy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #6
  %14 = bitcast double* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %14) #6
  %15 = bitcast double* %dy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %15) #6
  %16 = bitcast [9 x double]* %T1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %16) #6
  %17 = bitcast [9 x double]* %T2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %17) #6
  %18 = load double*, double** %pts1.addr, align 4, !tbaa !5
  %19 = load i32, i32* %np.addr, align 4, !tbaa !7
  %arraydecay = getelementptr inbounds [9 x double], [9 x double]* %T1, i32 0, i32 0
  call void @normalize_homography(double* %18, i32 %19, double* %arraydecay)
  %20 = load double*, double** %pts2.addr, align 4, !tbaa !5
  %21 = load i32, i32* %np.addr, align 4, !tbaa !7
  %arraydecay5 = getelementptr inbounds [9 x double], [9 x double]* %T2, i32 0, i32 0
  call void @normalize_homography(double* %20, i32 %21, double* %arraydecay5)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %22 = load i32, i32* %i, align 4, !tbaa !7
  %23 = load i32, i32* %np.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %22, %23
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load double*, double** %pts2.addr, align 4, !tbaa !5
  %incdec.ptr = getelementptr inbounds double, double* %24, i32 1
  store double* %incdec.ptr, double** %pts2.addr, align 4, !tbaa !5
  %25 = load double, double* %24, align 8, !tbaa !12
  store double %25, double* %dx, align 8, !tbaa !12
  %26 = load double*, double** %pts2.addr, align 4, !tbaa !5
  %incdec.ptr6 = getelementptr inbounds double, double* %26, i32 1
  store double* %incdec.ptr6, double** %pts2.addr, align 4, !tbaa !5
  %27 = load double, double* %26, align 8, !tbaa !12
  store double %27, double* %dy, align 8, !tbaa !12
  %28 = load double*, double** %pts1.addr, align 4, !tbaa !5
  %incdec.ptr7 = getelementptr inbounds double, double* %28, i32 1
  store double* %incdec.ptr7, double** %pts1.addr, align 4, !tbaa !5
  %29 = load double, double* %28, align 8, !tbaa !12
  store double %29, double* %sx, align 8, !tbaa !12
  %30 = load double*, double** %pts1.addr, align 4, !tbaa !5
  %incdec.ptr8 = getelementptr inbounds double, double* %30, i32 1
  store double* %incdec.ptr8, double** %pts1.addr, align 4, !tbaa !5
  %31 = load double, double* %30, align 8, !tbaa !12
  store double %31, double* %sy, align 8, !tbaa !12
  %32 = load double, double* %sx, align 8, !tbaa !12
  %33 = load double*, double** %a, align 4, !tbaa !5
  %34 = load i32, i32* %i, align 4, !tbaa !7
  %mul9 = mul nsw i32 %34, 2
  %mul10 = mul nsw i32 %mul9, 4
  %add11 = add nsw i32 %mul10, 0
  %arrayidx = getelementptr inbounds double, double* %33, i32 %add11
  store double %32, double* %arrayidx, align 8, !tbaa !12
  %35 = load double, double* %sy, align 8, !tbaa !12
  %36 = load double*, double** %a, align 4, !tbaa !5
  %37 = load i32, i32* %i, align 4, !tbaa !7
  %mul12 = mul nsw i32 %37, 2
  %mul13 = mul nsw i32 %mul12, 4
  %add14 = add nsw i32 %mul13, 1
  %arrayidx15 = getelementptr inbounds double, double* %36, i32 %add14
  store double %35, double* %arrayidx15, align 8, !tbaa !12
  %38 = load double*, double** %a, align 4, !tbaa !5
  %39 = load i32, i32* %i, align 4, !tbaa !7
  %mul16 = mul nsw i32 %39, 2
  %mul17 = mul nsw i32 %mul16, 4
  %add18 = add nsw i32 %mul17, 2
  %arrayidx19 = getelementptr inbounds double, double* %38, i32 %add18
  store double 1.000000e+00, double* %arrayidx19, align 8, !tbaa !12
  %40 = load double*, double** %a, align 4, !tbaa !5
  %41 = load i32, i32* %i, align 4, !tbaa !7
  %mul20 = mul nsw i32 %41, 2
  %mul21 = mul nsw i32 %mul20, 4
  %add22 = add nsw i32 %mul21, 3
  %arrayidx23 = getelementptr inbounds double, double* %40, i32 %add22
  store double 0.000000e+00, double* %arrayidx23, align 8, !tbaa !12
  %42 = load double, double* %sy, align 8, !tbaa !12
  %43 = load double*, double** %a, align 4, !tbaa !5
  %44 = load i32, i32* %i, align 4, !tbaa !7
  %mul24 = mul nsw i32 %44, 2
  %add25 = add nsw i32 %mul24, 1
  %mul26 = mul nsw i32 %add25, 4
  %add27 = add nsw i32 %mul26, 0
  %arrayidx28 = getelementptr inbounds double, double* %43, i32 %add27
  store double %42, double* %arrayidx28, align 8, !tbaa !12
  %45 = load double, double* %sx, align 8, !tbaa !12
  %fneg = fneg double %45
  %46 = load double*, double** %a, align 4, !tbaa !5
  %47 = load i32, i32* %i, align 4, !tbaa !7
  %mul29 = mul nsw i32 %47, 2
  %add30 = add nsw i32 %mul29, 1
  %mul31 = mul nsw i32 %add30, 4
  %add32 = add nsw i32 %mul31, 1
  %arrayidx33 = getelementptr inbounds double, double* %46, i32 %add32
  store double %fneg, double* %arrayidx33, align 8, !tbaa !12
  %48 = load double*, double** %a, align 4, !tbaa !5
  %49 = load i32, i32* %i, align 4, !tbaa !7
  %mul34 = mul nsw i32 %49, 2
  %add35 = add nsw i32 %mul34, 1
  %mul36 = mul nsw i32 %add35, 4
  %add37 = add nsw i32 %mul36, 2
  %arrayidx38 = getelementptr inbounds double, double* %48, i32 %add37
  store double 0.000000e+00, double* %arrayidx38, align 8, !tbaa !12
  %50 = load double*, double** %a, align 4, !tbaa !5
  %51 = load i32, i32* %i, align 4, !tbaa !7
  %mul39 = mul nsw i32 %51, 2
  %add40 = add nsw i32 %mul39, 1
  %mul41 = mul nsw i32 %add40, 4
  %add42 = add nsw i32 %mul41, 3
  %arrayidx43 = getelementptr inbounds double, double* %50, i32 %add42
  store double 1.000000e+00, double* %arrayidx43, align 8, !tbaa !12
  %52 = load double, double* %dx, align 8, !tbaa !12
  %53 = load double*, double** %b, align 4, !tbaa !5
  %54 = load i32, i32* %i, align 4, !tbaa !7
  %mul44 = mul nsw i32 2, %54
  %arrayidx45 = getelementptr inbounds double, double* %53, i32 %mul44
  store double %52, double* %arrayidx45, align 8, !tbaa !12
  %55 = load double, double* %dy, align 8, !tbaa !12
  %56 = load double*, double** %b, align 4, !tbaa !5
  %57 = load i32, i32* %i, align 4, !tbaa !7
  %mul46 = mul nsw i32 2, %57
  %add47 = add nsw i32 %mul46, 1
  %arrayidx48 = getelementptr inbounds double, double* %56, i32 %add47
  store double %55, double* %arrayidx48, align 8, !tbaa !12
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %58 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %59 = load double*, double** %a, align 4, !tbaa !5
  %60 = load i32, i32* %np2, align 4, !tbaa !7
  %61 = load double*, double** %b, align 4, !tbaa !5
  %62 = load double*, double** %temp, align 4, !tbaa !5
  %63 = load double*, double** %mat.addr, align 4, !tbaa !5
  %call49 = call i32 @least_squares(i32 4, double* %59, i32 %60, i32 4, double* %61, double* %62, double* %63)
  %tobool = icmp ne i32 %call49, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.end
  %64 = load double*, double** %a, align 4, !tbaa !5
  %65 = bitcast double* %64 to i8*
  call void @aom_free(i8* %65)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.end
  %66 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arraydecay50 = getelementptr inbounds [9 x double], [9 x double]* %T1, i32 0, i32 0
  %arraydecay51 = getelementptr inbounds [9 x double], [9 x double]* %T2, i32 0, i32 0
  call void @denormalize_rotzoom_reorder(double* %66, double* %arraydecay50, double* %arraydecay51)
  %67 = load double*, double** %a, align 4, !tbaa !5
  %68 = bitcast double* %67 to i8*
  call void @aom_free(i8* %68)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %69 = bitcast [9 x double]* %T2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %69) #6
  %70 = bitcast [9 x double]* %T1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %70) #6
  %71 = bitcast double* %dy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %71) #6
  %72 = bitcast double* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %72) #6
  %73 = bitcast double* %sy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %73) #6
  %74 = bitcast double* %sx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %74) #6
  %75 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #6
  %76 = bitcast double** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  %77 = bitcast double** %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #6
  %78 = bitcast double** %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #6
  %79 = bitcast i32* %np2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #6
  %80 = load i32, i32* %retval, align 4
  ret i32 %80
}

; Function Attrs: nounwind
define internal void @project_points_double_rotzoom(double* %mat, double* %points, double* %proj, i32 %n, i32 %stride_points, i32 %stride_proj) #0 {
entry:
  %mat.addr = alloca double*, align 4
  %points.addr = alloca double*, align 4
  %proj.addr = alloca double*, align 4
  %n.addr = alloca i32, align 4
  %stride_points.addr = alloca i32, align 4
  %stride_proj.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %x = alloca double, align 8
  %y = alloca double, align 8
  store double* %mat, double** %mat.addr, align 4, !tbaa !5
  store double* %points, double** %points.addr, align 4, !tbaa !5
  store double* %proj, double** %proj.addr, align 4, !tbaa !5
  store i32 %n, i32* %n.addr, align 4, !tbaa !7
  store i32 %stride_points, i32* %stride_points.addr, align 4, !tbaa !7
  store i32 %stride_proj, i32* %stride_proj.addr, align 4, !tbaa !7
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !7
  %2 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast double* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #6
  %4 = load double*, double** %points.addr, align 4, !tbaa !5
  %incdec.ptr = getelementptr inbounds double, double* %4, i32 1
  store double* %incdec.ptr, double** %points.addr, align 4, !tbaa !5
  %5 = load double, double* %4, align 8, !tbaa !12
  store double %5, double* %x, align 8, !tbaa !12
  %6 = bitcast double* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #6
  %7 = load double*, double** %points.addr, align 4, !tbaa !5
  %incdec.ptr1 = getelementptr inbounds double, double* %7, i32 1
  store double* %incdec.ptr1, double** %points.addr, align 4, !tbaa !5
  %8 = load double, double* %7, align 8, !tbaa !12
  store double %8, double* %y, align 8, !tbaa !12
  %9 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %9, i32 2
  %10 = load double, double* %arrayidx, align 8, !tbaa !12
  %11 = load double, double* %x, align 8, !tbaa !12
  %mul = fmul double %10, %11
  %12 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds double, double* %12, i32 3
  %13 = load double, double* %arrayidx2, align 8, !tbaa !12
  %14 = load double, double* %y, align 8, !tbaa !12
  %mul3 = fmul double %13, %14
  %add = fadd double %mul, %mul3
  %15 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx4 = getelementptr inbounds double, double* %15, i32 0
  %16 = load double, double* %arrayidx4, align 8, !tbaa !12
  %add5 = fadd double %add, %16
  %17 = load double*, double** %proj.addr, align 4, !tbaa !5
  %incdec.ptr6 = getelementptr inbounds double, double* %17, i32 1
  store double* %incdec.ptr6, double** %proj.addr, align 4, !tbaa !5
  store double %add5, double* %17, align 8, !tbaa !12
  %18 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx7 = getelementptr inbounds double, double* %18, i32 3
  %19 = load double, double* %arrayidx7, align 8, !tbaa !12
  %fneg = fneg double %19
  %20 = load double, double* %x, align 8, !tbaa !12
  %mul8 = fmul double %fneg, %20
  %21 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx9 = getelementptr inbounds double, double* %21, i32 2
  %22 = load double, double* %arrayidx9, align 8, !tbaa !12
  %23 = load double, double* %y, align 8, !tbaa !12
  %mul10 = fmul double %22, %23
  %add11 = fadd double %mul8, %mul10
  %24 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx12 = getelementptr inbounds double, double* %24, i32 1
  %25 = load double, double* %arrayidx12, align 8, !tbaa !12
  %add13 = fadd double %add11, %25
  %26 = load double*, double** %proj.addr, align 4, !tbaa !5
  %incdec.ptr14 = getelementptr inbounds double, double* %26, i32 1
  store double* %incdec.ptr14, double** %proj.addr, align 4, !tbaa !5
  store double %add13, double* %26, align 8, !tbaa !12
  %27 = load i32, i32* %stride_points.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %27, 2
  %28 = load double*, double** %points.addr, align 4, !tbaa !5
  %add.ptr = getelementptr inbounds double, double* %28, i32 %sub
  store double* %add.ptr, double** %points.addr, align 4, !tbaa !5
  %29 = load i32, i32* %stride_proj.addr, align 4, !tbaa !7
  %sub15 = sub nsw i32 %29, 2
  %30 = load double*, double** %proj.addr, align 4, !tbaa !5
  %add.ptr16 = getelementptr inbounds double, double* %30, i32 %sub15
  store double* %add.ptr16, double** %proj.addr, align 4, !tbaa !5
  %31 = bitcast double* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %31) #6
  %32 = bitcast double* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %32) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %33 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %34 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  ret void
}

; Function Attrs: nounwind
define internal void @denormalize_rotzoom_reorder(double* %params, double* %T1, double* %T2) #0 {
entry:
  %params.addr = alloca double*, align 4
  %T1.addr = alloca double*, align 4
  %T2.addr = alloca double*, align 4
  %params_denorm = alloca [9 x double], align 16
  store double* %params, double** %params.addr, align 4, !tbaa !5
  store double* %T1, double** %T1.addr, align 4, !tbaa !5
  store double* %T2, double** %T2.addr, align 4, !tbaa !5
  %0 = bitcast [9 x double]* %params_denorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %0) #6
  %1 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %1, i32 0
  %2 = load double, double* %arrayidx, align 8, !tbaa !12
  %arrayidx1 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 0
  store double %2, double* %arrayidx1, align 16, !tbaa !12
  %3 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds double, double* %3, i32 1
  %4 = load double, double* %arrayidx2, align 8, !tbaa !12
  %arrayidx3 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 1
  store double %4, double* %arrayidx3, align 8, !tbaa !12
  %5 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx4 = getelementptr inbounds double, double* %5, i32 2
  %6 = load double, double* %arrayidx4, align 8, !tbaa !12
  %arrayidx5 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 2
  store double %6, double* %arrayidx5, align 16, !tbaa !12
  %7 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx6 = getelementptr inbounds double, double* %7, i32 1
  %8 = load double, double* %arrayidx6, align 8, !tbaa !12
  %fneg = fneg double %8
  %arrayidx7 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 3
  store double %fneg, double* %arrayidx7, align 8, !tbaa !12
  %9 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx8 = getelementptr inbounds double, double* %9, i32 0
  %10 = load double, double* %arrayidx8, align 8, !tbaa !12
  %arrayidx9 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 4
  store double %10, double* %arrayidx9, align 16, !tbaa !12
  %11 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx10 = getelementptr inbounds double, double* %11, i32 3
  %12 = load double, double* %arrayidx10, align 8, !tbaa !12
  %arrayidx11 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 5
  store double %12, double* %arrayidx11, align 8, !tbaa !12
  %arrayidx12 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 7
  store double 0.000000e+00, double* %arrayidx12, align 8, !tbaa !12
  %arrayidx13 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 6
  store double 0.000000e+00, double* %arrayidx13, align 16, !tbaa !12
  %arrayidx14 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 8
  store double 1.000000e+00, double* %arrayidx14, align 16, !tbaa !12
  %arraydecay = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 0
  %13 = load double*, double** %T1.addr, align 4, !tbaa !5
  %14 = load double*, double** %T2.addr, align 4, !tbaa !5
  call void @denormalize_homography(double* %arraydecay, double* %13, double* %14)
  %arrayidx15 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 2
  %15 = load double, double* %arrayidx15, align 16, !tbaa !12
  %16 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx16 = getelementptr inbounds double, double* %16, i32 0
  store double %15, double* %arrayidx16, align 8, !tbaa !12
  %arrayidx17 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 5
  %17 = load double, double* %arrayidx17, align 8, !tbaa !12
  %18 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx18 = getelementptr inbounds double, double* %18, i32 1
  store double %17, double* %arrayidx18, align 8, !tbaa !12
  %arrayidx19 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 0
  %19 = load double, double* %arrayidx19, align 16, !tbaa !12
  %20 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx20 = getelementptr inbounds double, double* %20, i32 2
  store double %19, double* %arrayidx20, align 8, !tbaa !12
  %arrayidx21 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 1
  %21 = load double, double* %arrayidx21, align 8, !tbaa !12
  %22 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx22 = getelementptr inbounds double, double* %22, i32 3
  store double %21, double* %arrayidx22, align 8, !tbaa !12
  %23 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx23 = getelementptr inbounds double, double* %23, i32 3
  %24 = load double, double* %arrayidx23, align 8, !tbaa !12
  %fneg24 = fneg double %24
  %25 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx25 = getelementptr inbounds double, double* %25, i32 4
  store double %fneg24, double* %arrayidx25, align 8, !tbaa !12
  %26 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx26 = getelementptr inbounds double, double* %26, i32 2
  %27 = load double, double* %arrayidx26, align 8, !tbaa !12
  %28 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx27 = getelementptr inbounds double, double* %28, i32 5
  store double %27, double* %arrayidx27, align 8, !tbaa !12
  %29 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx28 = getelementptr inbounds double, double* %29, i32 7
  store double 0.000000e+00, double* %arrayidx28, align 8, !tbaa !12
  %30 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx29 = getelementptr inbounds double, double* %30, i32 6
  store double 0.000000e+00, double* %arrayidx29, align 8, !tbaa !12
  %31 = bitcast [9 x double]* %params_denorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %31) #6
  ret void
}

; Function Attrs: nounwind
define internal i32 @is_degenerate_translation(double* %p) #0 {
entry:
  %p.addr = alloca double*, align 4
  store double* %p, double** %p.addr, align 4, !tbaa !5
  %0 = load double*, double** %p.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %0, i32 0
  %1 = load double, double* %arrayidx, align 8, !tbaa !12
  %2 = load double*, double** %p.addr, align 4, !tbaa !5
  %arrayidx1 = getelementptr inbounds double, double* %2, i32 2
  %3 = load double, double* %arrayidx1, align 8, !tbaa !12
  %sub = fsub double %1, %3
  %4 = load double*, double** %p.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds double, double* %4, i32 0
  %5 = load double, double* %arrayidx2, align 8, !tbaa !12
  %6 = load double*, double** %p.addr, align 4, !tbaa !5
  %arrayidx3 = getelementptr inbounds double, double* %6, i32 2
  %7 = load double, double* %arrayidx3, align 8, !tbaa !12
  %sub4 = fsub double %5, %7
  %mul = fmul double %sub, %sub4
  %8 = load double*, double** %p.addr, align 4, !tbaa !5
  %arrayidx5 = getelementptr inbounds double, double* %8, i32 1
  %9 = load double, double* %arrayidx5, align 8, !tbaa !12
  %10 = load double*, double** %p.addr, align 4, !tbaa !5
  %arrayidx6 = getelementptr inbounds double, double* %10, i32 3
  %11 = load double, double* %arrayidx6, align 8, !tbaa !12
  %sub7 = fsub double %9, %11
  %12 = load double*, double** %p.addr, align 4, !tbaa !5
  %arrayidx8 = getelementptr inbounds double, double* %12, i32 1
  %13 = load double, double* %arrayidx8, align 8, !tbaa !12
  %14 = load double*, double** %p.addr, align 4, !tbaa !5
  %arrayidx9 = getelementptr inbounds double, double* %14, i32 3
  %15 = load double, double* %arrayidx9, align 8, !tbaa !12
  %sub10 = fsub double %13, %15
  %mul11 = fmul double %sub7, %sub10
  %add = fadd double %mul, %mul11
  %cmp = fcmp ole double %add, 2.000000e+00
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define internal i32 @find_translation(i32 %np, double* %pts1, double* %pts2, double* %mat) #0 {
entry:
  %np.addr = alloca i32, align 4
  %pts1.addr = alloca double*, align 4
  %pts2.addr = alloca double*, align 4
  %mat.addr = alloca double*, align 4
  %i = alloca i32, align 4
  %sx = alloca double, align 8
  %sy = alloca double, align 8
  %dx = alloca double, align 8
  %dy = alloca double, align 8
  %sumx = alloca double, align 8
  %sumy = alloca double, align 8
  %T1 = alloca [9 x double], align 16
  %T2 = alloca [9 x double], align 16
  store i32 %np, i32* %np.addr, align 4, !tbaa !7
  store double* %pts1, double** %pts1.addr, align 4, !tbaa !5
  store double* %pts2, double** %pts2.addr, align 4, !tbaa !5
  store double* %mat, double** %mat.addr, align 4, !tbaa !5
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast double* %sx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #6
  %2 = bitcast double* %sy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #6
  %3 = bitcast double* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #6
  %4 = bitcast double* %dy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #6
  %5 = bitcast double* %sumx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #6
  %6 = bitcast double* %sumy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #6
  %7 = bitcast [9 x double]* %T1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %7) #6
  %8 = bitcast [9 x double]* %T2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %8) #6
  %9 = load double*, double** %pts1.addr, align 4, !tbaa !5
  %10 = load i32, i32* %np.addr, align 4, !tbaa !7
  %arraydecay = getelementptr inbounds [9 x double], [9 x double]* %T1, i32 0, i32 0
  call void @normalize_homography(double* %9, i32 %10, double* %arraydecay)
  %11 = load double*, double** %pts2.addr, align 4, !tbaa !5
  %12 = load i32, i32* %np.addr, align 4, !tbaa !7
  %arraydecay1 = getelementptr inbounds [9 x double], [9 x double]* %T2, i32 0, i32 0
  call void @normalize_homography(double* %11, i32 %12, double* %arraydecay1)
  store double 0.000000e+00, double* %sumx, align 8, !tbaa !12
  store double 0.000000e+00, double* %sumy, align 8, !tbaa !12
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load i32, i32* %i, align 4, !tbaa !7
  %14 = load i32, i32* %np.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %13, %14
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load double*, double** %pts2.addr, align 4, !tbaa !5
  %incdec.ptr = getelementptr inbounds double, double* %15, i32 1
  store double* %incdec.ptr, double** %pts2.addr, align 4, !tbaa !5
  %16 = load double, double* %15, align 8, !tbaa !12
  store double %16, double* %dx, align 8, !tbaa !12
  %17 = load double*, double** %pts2.addr, align 4, !tbaa !5
  %incdec.ptr2 = getelementptr inbounds double, double* %17, i32 1
  store double* %incdec.ptr2, double** %pts2.addr, align 4, !tbaa !5
  %18 = load double, double* %17, align 8, !tbaa !12
  store double %18, double* %dy, align 8, !tbaa !12
  %19 = load double*, double** %pts1.addr, align 4, !tbaa !5
  %incdec.ptr3 = getelementptr inbounds double, double* %19, i32 1
  store double* %incdec.ptr3, double** %pts1.addr, align 4, !tbaa !5
  %20 = load double, double* %19, align 8, !tbaa !12
  store double %20, double* %sx, align 8, !tbaa !12
  %21 = load double*, double** %pts1.addr, align 4, !tbaa !5
  %incdec.ptr4 = getelementptr inbounds double, double* %21, i32 1
  store double* %incdec.ptr4, double** %pts1.addr, align 4, !tbaa !5
  %22 = load double, double* %21, align 8, !tbaa !12
  store double %22, double* %sy, align 8, !tbaa !12
  %23 = load double, double* %dx, align 8, !tbaa !12
  %24 = load double, double* %sx, align 8, !tbaa !12
  %sub = fsub double %23, %24
  %25 = load double, double* %sumx, align 8, !tbaa !12
  %add = fadd double %25, %sub
  store double %add, double* %sumx, align 8, !tbaa !12
  %26 = load double, double* %dy, align 8, !tbaa !12
  %27 = load double, double* %sy, align 8, !tbaa !12
  %sub5 = fsub double %26, %27
  %28 = load double, double* %sumy, align 8, !tbaa !12
  %add6 = fadd double %28, %sub5
  store double %add6, double* %sumy, align 8, !tbaa !12
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %30 = load double, double* %sumx, align 8, !tbaa !12
  %31 = load i32, i32* %np.addr, align 4, !tbaa !7
  %conv = sitofp i32 %31 to double
  %div = fdiv double %30, %conv
  %32 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %32, i32 0
  store double %div, double* %arrayidx, align 8, !tbaa !12
  %33 = load double, double* %sumy, align 8, !tbaa !12
  %34 = load i32, i32* %np.addr, align 4, !tbaa !7
  %conv7 = sitofp i32 %34 to double
  %div8 = fdiv double %33, %conv7
  %35 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx9 = getelementptr inbounds double, double* %35, i32 1
  store double %div8, double* %arrayidx9, align 8, !tbaa !12
  %36 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arraydecay10 = getelementptr inbounds [9 x double], [9 x double]* %T1, i32 0, i32 0
  %arraydecay11 = getelementptr inbounds [9 x double], [9 x double]* %T2, i32 0, i32 0
  call void @denormalize_translation_reorder(double* %36, double* %arraydecay10, double* %arraydecay11)
  %37 = bitcast [9 x double]* %T2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %37) #6
  %38 = bitcast [9 x double]* %T1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %38) #6
  %39 = bitcast double* %sumy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %39) #6
  %40 = bitcast double* %sumx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %40) #6
  %41 = bitcast double* %dy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %41) #6
  %42 = bitcast double* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %42) #6
  %43 = bitcast double* %sy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %43) #6
  %44 = bitcast double* %sx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %44) #6
  %45 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  ret i32 0
}

; Function Attrs: nounwind
define internal void @project_points_double_translation(double* %mat, double* %points, double* %proj, i32 %n, i32 %stride_points, i32 %stride_proj) #0 {
entry:
  %mat.addr = alloca double*, align 4
  %points.addr = alloca double*, align 4
  %proj.addr = alloca double*, align 4
  %n.addr = alloca i32, align 4
  %stride_points.addr = alloca i32, align 4
  %stride_proj.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %x = alloca double, align 8
  %y = alloca double, align 8
  store double* %mat, double** %mat.addr, align 4, !tbaa !5
  store double* %points, double** %points.addr, align 4, !tbaa !5
  store double* %proj, double** %proj.addr, align 4, !tbaa !5
  store i32 %n, i32* %n.addr, align 4, !tbaa !7
  store i32 %stride_points, i32* %stride_points.addr, align 4, !tbaa !7
  store i32 %stride_proj, i32* %stride_proj.addr, align 4, !tbaa !7
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !7
  %2 = load i32, i32* %n.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast double* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #6
  %4 = load double*, double** %points.addr, align 4, !tbaa !5
  %incdec.ptr = getelementptr inbounds double, double* %4, i32 1
  store double* %incdec.ptr, double** %points.addr, align 4, !tbaa !5
  %5 = load double, double* %4, align 8, !tbaa !12
  store double %5, double* %x, align 8, !tbaa !12
  %6 = bitcast double* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #6
  %7 = load double*, double** %points.addr, align 4, !tbaa !5
  %incdec.ptr1 = getelementptr inbounds double, double* %7, i32 1
  store double* %incdec.ptr1, double** %points.addr, align 4, !tbaa !5
  %8 = load double, double* %7, align 8, !tbaa !12
  store double %8, double* %y, align 8, !tbaa !12
  %9 = load double, double* %x, align 8, !tbaa !12
  %10 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds double, double* %10, i32 0
  %11 = load double, double* %arrayidx, align 8, !tbaa !12
  %add = fadd double %9, %11
  %12 = load double*, double** %proj.addr, align 4, !tbaa !5
  %incdec.ptr2 = getelementptr inbounds double, double* %12, i32 1
  store double* %incdec.ptr2, double** %proj.addr, align 4, !tbaa !5
  store double %add, double* %12, align 8, !tbaa !12
  %13 = load double, double* %y, align 8, !tbaa !12
  %14 = load double*, double** %mat.addr, align 4, !tbaa !5
  %arrayidx3 = getelementptr inbounds double, double* %14, i32 1
  %15 = load double, double* %arrayidx3, align 8, !tbaa !12
  %add4 = fadd double %13, %15
  %16 = load double*, double** %proj.addr, align 4, !tbaa !5
  %incdec.ptr5 = getelementptr inbounds double, double* %16, i32 1
  store double* %incdec.ptr5, double** %proj.addr, align 4, !tbaa !5
  store double %add4, double* %16, align 8, !tbaa !12
  %17 = load i32, i32* %stride_points.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %17, 2
  %18 = load double*, double** %points.addr, align 4, !tbaa !5
  %add.ptr = getelementptr inbounds double, double* %18, i32 %sub
  store double* %add.ptr, double** %points.addr, align 4, !tbaa !5
  %19 = load i32, i32* %stride_proj.addr, align 4, !tbaa !7
  %sub6 = sub nsw i32 %19, 2
  %20 = load double*, double** %proj.addr, align 4, !tbaa !5
  %add.ptr7 = getelementptr inbounds double, double* %20, i32 %sub6
  store double* %add.ptr7, double** %proj.addr, align 4, !tbaa !5
  %21 = bitcast double* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %21) #6
  %22 = bitcast double* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  ret void
}

; Function Attrs: nounwind
define internal void @denormalize_translation_reorder(double* %params, double* %T1, double* %T2) #0 {
entry:
  %params.addr = alloca double*, align 4
  %T1.addr = alloca double*, align 4
  %T2.addr = alloca double*, align 4
  %params_denorm = alloca [9 x double], align 16
  store double* %params, double** %params.addr, align 4, !tbaa !5
  store double* %T1, double** %T1.addr, align 4, !tbaa !5
  store double* %T2, double** %T2.addr, align 4, !tbaa !5
  %0 = bitcast [9 x double]* %params_denorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %0) #6
  %arrayidx = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 0
  store double 1.000000e+00, double* %arrayidx, align 16, !tbaa !12
  %arrayidx1 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 1
  store double 0.000000e+00, double* %arrayidx1, align 8, !tbaa !12
  %1 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds double, double* %1, i32 0
  %2 = load double, double* %arrayidx2, align 8, !tbaa !12
  %arrayidx3 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 2
  store double %2, double* %arrayidx3, align 16, !tbaa !12
  %arrayidx4 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 3
  store double 0.000000e+00, double* %arrayidx4, align 8, !tbaa !12
  %arrayidx5 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 4
  store double 1.000000e+00, double* %arrayidx5, align 16, !tbaa !12
  %3 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx6 = getelementptr inbounds double, double* %3, i32 1
  %4 = load double, double* %arrayidx6, align 8, !tbaa !12
  %arrayidx7 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 5
  store double %4, double* %arrayidx7, align 8, !tbaa !12
  %arrayidx8 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 7
  store double 0.000000e+00, double* %arrayidx8, align 8, !tbaa !12
  %arrayidx9 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 6
  store double 0.000000e+00, double* %arrayidx9, align 16, !tbaa !12
  %arrayidx10 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 8
  store double 1.000000e+00, double* %arrayidx10, align 16, !tbaa !12
  %arraydecay = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 0
  %5 = load double*, double** %T1.addr, align 4, !tbaa !5
  %6 = load double*, double** %T2.addr, align 4, !tbaa !5
  call void @denormalize_homography(double* %arraydecay, double* %5, double* %6)
  %arrayidx11 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 2
  %7 = load double, double* %arrayidx11, align 16, !tbaa !12
  %8 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx12 = getelementptr inbounds double, double* %8, i32 0
  store double %7, double* %arrayidx12, align 8, !tbaa !12
  %arrayidx13 = getelementptr inbounds [9 x double], [9 x double]* %params_denorm, i32 0, i32 5
  %9 = load double, double* %arrayidx13, align 8, !tbaa !12
  %10 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx14 = getelementptr inbounds double, double* %10, i32 1
  store double %9, double* %arrayidx14, align 8, !tbaa !12
  %11 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx15 = getelementptr inbounds double, double* %11, i32 5
  store double 1.000000e+00, double* %arrayidx15, align 8, !tbaa !12
  %12 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx16 = getelementptr inbounds double, double* %12, i32 2
  store double 1.000000e+00, double* %arrayidx16, align 8, !tbaa !12
  %13 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx17 = getelementptr inbounds double, double* %13, i32 4
  store double 0.000000e+00, double* %arrayidx17, align 8, !tbaa !12
  %14 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx18 = getelementptr inbounds double, double* %14, i32 3
  store double 0.000000e+00, double* %arrayidx18, align 8, !tbaa !12
  %15 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx19 = getelementptr inbounds double, double* %15, i32 7
  store double 0.000000e+00, double* %arrayidx19, align 8, !tbaa !12
  %16 = load double*, double** %params.addr, align 4, !tbaa !5
  %arrayidx20 = getelementptr inbounds double, double* %16, i32 6
  store double 0.000000e+00, double* %arrayidx20, align 8, !tbaa !12
  %17 = bitcast [9 x double]* %params_denorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %17) #6
  ret void
}

; Function Attrs: nounwind
define internal i32 @ransac_double_prec(double* %matched_points, i32 %npoints, i32* %num_inliers_by_motion, %struct.MotionModel* %params_by_motion, i32 %num_desired_motions, i32 %minpts, i32 (double*)* %is_degenerate, i32 (i32, double*, double*, double*)* %find_transformation, void (double*, double*, double*, i32, i32, i32)* %projectpoints) #0 {
entry:
  %retval = alloca i32, align 4
  %matched_points.addr = alloca double*, align 4
  %npoints.addr = alloca i32, align 4
  %num_inliers_by_motion.addr = alloca i32*, align 4
  %params_by_motion.addr = alloca %struct.MotionModel*, align 4
  %num_desired_motions.addr = alloca i32, align 4
  %minpts.addr = alloca i32, align 4
  %is_degenerate.addr = alloca i32 (double*)*, align 4
  %find_transformation.addr = alloca i32 (i32, double*, double*, double*)*, align 4
  %projectpoints.addr = alloca void (double*, double*, double*, i32, i32, i32)*, align 4
  %trial_count = alloca i32, align 4
  %i = alloca i32, align 4
  %ret_val = alloca i32, align 4
  %seed = alloca i32, align 4
  %indices = alloca [4 x i32], align 16
  %points1 = alloca double*, align 4
  %points2 = alloca double*, align 4
  %corners1 = alloca double*, align 4
  %corners2 = alloca double*, align 4
  %image1_coord = alloca double*, align 4
  %motions = alloca %struct.RANSAC_MOTION*, align 4
  %worst_kept_motion = alloca %struct.RANSAC_MOTION*, align 4
  %current_motion = alloca %struct.RANSAC_MOTION, align 8
  %params_this_motion = alloca [9 x double], align 16
  %cnp1 = alloca double*, align 4
  %cnp2 = alloca double*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %sum_distance = alloca double, align 8
  %sum_distance_squared = alloca double, align 8
  %degenerate = alloca i32, align 4
  %num_degenerate_iter = alloca i32, align 4
  %dx = alloca double, align 8
  %dy = alloca double, align 8
  %distance = alloca double, align 8
  %mean_distance = alloca double, align 8
  store double* %matched_points, double** %matched_points.addr, align 4, !tbaa !5
  store i32 %npoints, i32* %npoints.addr, align 4, !tbaa !7
  store i32* %num_inliers_by_motion, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  store %struct.MotionModel* %params_by_motion, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  store i32 %num_desired_motions, i32* %num_desired_motions.addr, align 4, !tbaa !7
  store i32 %minpts, i32* %minpts.addr, align 4, !tbaa !7
  store i32 (double*)* %is_degenerate, i32 (double*)** %is_degenerate.addr, align 4, !tbaa !5
  store i32 (i32, double*, double*, double*)* %find_transformation, i32 (i32, double*, double*, double*)** %find_transformation.addr, align 4, !tbaa !5
  store void (double*, double*, double*, i32, i32, i32)* %projectpoints, void (double*, double*, double*, i32, i32, i32)** %projectpoints.addr, align 4, !tbaa !5
  %0 = bitcast i32* %trial_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %trial_count, align 4, !tbaa !7
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !7
  %2 = bitcast i32* %ret_val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 0, i32* %ret_val, align 4, !tbaa !7
  %3 = bitcast i32* %seed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  store i32 %4, i32* %seed, align 4, !tbaa !7
  %5 = bitcast [4 x i32]* %indices to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #6
  %6 = bitcast [4 x i32]* %indices to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %6, i8 0, i32 16, i1 false)
  %7 = bitcast double** %points1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = bitcast double** %points2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = bitcast double** %corners1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = bitcast double** %corners2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = bitcast double** %image1_coord to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = bitcast %struct.RANSAC_MOTION** %motions to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = bitcast %struct.RANSAC_MOTION** %worst_kept_motion to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store %struct.RANSAC_MOTION* null, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %14 = bitcast %struct.RANSAC_MOTION* %current_motion to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %14) #6
  %15 = bitcast [9 x double]* %params_this_motion to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %15) #6
  %16 = bitcast double** %cnp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = bitcast double** %cnp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %18 = load i32, i32* %i, align 4, !tbaa !7
  %19 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %18, %19
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %20 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %21 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i32, i32* %20, i32 %21
  store i32 0, i32* %arrayidx, align 4, !tbaa !7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %23 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %24 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %mul = mul nsw i32 %24, 5
  %cmp1 = icmp slt i32 %23, %mul
  br i1 %cmp1, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %25 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %cmp2 = icmp eq i32 %25, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup202

if.end:                                           ; preds = %lor.lhs.false
  %26 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul3 = mul i32 8, %26
  %mul4 = mul i32 %mul3, 2
  %call = call i8* @aom_malloc(i32 %mul4)
  %27 = bitcast i8* %call to double*
  store double* %27, double** %points1, align 4, !tbaa !5
  %28 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul5 = mul i32 8, %28
  %mul6 = mul i32 %mul5, 2
  %call7 = call i8* @aom_malloc(i32 %mul6)
  %29 = bitcast i8* %call7 to double*
  store double* %29, double** %points2, align 4, !tbaa !5
  %30 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul8 = mul i32 8, %30
  %mul9 = mul i32 %mul8, 2
  %call10 = call i8* @aom_malloc(i32 %mul9)
  %31 = bitcast i8* %call10 to double*
  store double* %31, double** %corners1, align 4, !tbaa !5
  %32 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul11 = mul i32 8, %32
  %mul12 = mul i32 %mul11, 2
  %call13 = call i8* @aom_malloc(i32 %mul12)
  %33 = bitcast i8* %call13 to double*
  store double* %33, double** %corners2, align 4, !tbaa !5
  %34 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul14 = mul i32 8, %34
  %mul15 = mul i32 %mul14, 2
  %call16 = call i8* @aom_malloc(i32 %mul15)
  %35 = bitcast i8* %call16 to double*
  store double* %35, double** %image1_coord, align 4, !tbaa !5
  %36 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %mul17 = mul i32 24, %36
  %call18 = call i8* @aom_malloc(i32 %mul17)
  %37 = bitcast i8* %call18 to %struct.RANSAC_MOTION*
  store %struct.RANSAC_MOTION* %37, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc25, %if.end
  %38 = load i32, i32* %i, align 4, !tbaa !7
  %39 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp20 = icmp slt i32 %38, %39
  br i1 %cmp20, label %for.body21, label %for.end27

for.body21:                                       ; preds = %for.cond19
  %40 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul22 = mul i32 4, %40
  %call23 = call i8* @aom_malloc(i32 %mul22)
  %41 = bitcast i8* %call23 to i32*
  %42 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %43 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx24 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %42, i32 %43
  %inlier_indices = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx24, i32 0, i32 2
  store i32* %41, i32** %inlier_indices, align 8, !tbaa !9
  %44 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %45 = load i32, i32* %i, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %44, i32 %45
  %46 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  call void @clear_motion(%struct.RANSAC_MOTION* %add.ptr, i32 %46)
  br label %for.inc25

for.inc25:                                        ; preds = %for.body21
  %47 = load i32, i32* %i, align 4, !tbaa !7
  %inc26 = add nsw i32 %47, 1
  store i32 %inc26, i32* %i, align 4, !tbaa !7
  br label %for.cond19

for.end27:                                        ; preds = %for.cond19
  %48 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul28 = mul i32 4, %48
  %call29 = call i8* @aom_malloc(i32 %mul28)
  %49 = bitcast i8* %call29 to i32*
  %inlier_indices30 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  store i32* %49, i32** %inlier_indices30, align 8, !tbaa !9
  %50 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  call void @clear_motion(%struct.RANSAC_MOTION* %current_motion, i32 %50)
  %51 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  store %struct.RANSAC_MOTION* %51, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %52 = load double*, double** %points1, align 4, !tbaa !5
  %tobool = icmp ne double* %52, null
  br i1 %tobool, label %land.lhs.true, label %if.then43

land.lhs.true:                                    ; preds = %for.end27
  %53 = load double*, double** %points2, align 4, !tbaa !5
  %tobool31 = icmp ne double* %53, null
  br i1 %tobool31, label %land.lhs.true32, label %if.then43

land.lhs.true32:                                  ; preds = %land.lhs.true
  %54 = load double*, double** %corners1, align 4, !tbaa !5
  %tobool33 = icmp ne double* %54, null
  br i1 %tobool33, label %land.lhs.true34, label %if.then43

land.lhs.true34:                                  ; preds = %land.lhs.true32
  %55 = load double*, double** %corners2, align 4, !tbaa !5
  %tobool35 = icmp ne double* %55, null
  br i1 %tobool35, label %land.lhs.true36, label %if.then43

land.lhs.true36:                                  ; preds = %land.lhs.true34
  %56 = load double*, double** %image1_coord, align 4, !tbaa !5
  %tobool37 = icmp ne double* %56, null
  br i1 %tobool37, label %land.lhs.true38, label %if.then43

land.lhs.true38:                                  ; preds = %land.lhs.true36
  %57 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %tobool39 = icmp ne %struct.RANSAC_MOTION* %57, null
  br i1 %tobool39, label %land.lhs.true40, label %if.then43

land.lhs.true40:                                  ; preds = %land.lhs.true38
  %inlier_indices41 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  %58 = load i32*, i32** %inlier_indices41, align 8, !tbaa !9
  %tobool42 = icmp ne i32* %58, null
  br i1 %tobool42, label %if.end44, label %if.then43

if.then43:                                        ; preds = %land.lhs.true40, %land.lhs.true38, %land.lhs.true36, %land.lhs.true34, %land.lhs.true32, %land.lhs.true, %for.end27
  store i32 1, i32* %ret_val, align 4, !tbaa !7
  br label %finish_ransac

if.end44:                                         ; preds = %land.lhs.true40
  %59 = load double*, double** %corners1, align 4, !tbaa !5
  store double* %59, double** %cnp1, align 4, !tbaa !5
  %60 = load double*, double** %corners2, align 4, !tbaa !5
  store double* %60, double** %cnp2, align 4, !tbaa !5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond45

for.cond45:                                       ; preds = %for.inc55, %if.end44
  %61 = load i32, i32* %i, align 4, !tbaa !7
  %62 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %cmp46 = icmp slt i32 %61, %62
  br i1 %cmp46, label %for.body47, label %for.end57

for.body47:                                       ; preds = %for.cond45
  %63 = load double*, double** %matched_points.addr, align 4, !tbaa !5
  %incdec.ptr = getelementptr inbounds double, double* %63, i32 1
  store double* %incdec.ptr, double** %matched_points.addr, align 4, !tbaa !5
  %64 = load double, double* %63, align 8, !tbaa !12
  %65 = load double*, double** %cnp1, align 4, !tbaa !5
  %incdec.ptr48 = getelementptr inbounds double, double* %65, i32 1
  store double* %incdec.ptr48, double** %cnp1, align 4, !tbaa !5
  store double %64, double* %65, align 8, !tbaa !12
  %66 = load double*, double** %matched_points.addr, align 4, !tbaa !5
  %incdec.ptr49 = getelementptr inbounds double, double* %66, i32 1
  store double* %incdec.ptr49, double** %matched_points.addr, align 4, !tbaa !5
  %67 = load double, double* %66, align 8, !tbaa !12
  %68 = load double*, double** %cnp1, align 4, !tbaa !5
  %incdec.ptr50 = getelementptr inbounds double, double* %68, i32 1
  store double* %incdec.ptr50, double** %cnp1, align 4, !tbaa !5
  store double %67, double* %68, align 8, !tbaa !12
  %69 = load double*, double** %matched_points.addr, align 4, !tbaa !5
  %incdec.ptr51 = getelementptr inbounds double, double* %69, i32 1
  store double* %incdec.ptr51, double** %matched_points.addr, align 4, !tbaa !5
  %70 = load double, double* %69, align 8, !tbaa !12
  %71 = load double*, double** %cnp2, align 4, !tbaa !5
  %incdec.ptr52 = getelementptr inbounds double, double* %71, i32 1
  store double* %incdec.ptr52, double** %cnp2, align 4, !tbaa !5
  store double %70, double* %71, align 8, !tbaa !12
  %72 = load double*, double** %matched_points.addr, align 4, !tbaa !5
  %incdec.ptr53 = getelementptr inbounds double, double* %72, i32 1
  store double* %incdec.ptr53, double** %matched_points.addr, align 4, !tbaa !5
  %73 = load double, double* %72, align 8, !tbaa !12
  %74 = load double*, double** %cnp2, align 4, !tbaa !5
  %incdec.ptr54 = getelementptr inbounds double, double* %74, i32 1
  store double* %incdec.ptr54, double** %cnp2, align 4, !tbaa !5
  store double %73, double* %74, align 8, !tbaa !12
  br label %for.inc55

for.inc55:                                        ; preds = %for.body47
  %75 = load i32, i32* %i, align 4, !tbaa !7
  %inc56 = add nsw i32 %75, 1
  store i32 %inc56, i32* %i, align 4, !tbaa !7
  br label %for.cond45

for.end57:                                        ; preds = %for.cond45
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %cleanup, %for.end57
  %76 = load i32, i32* %trial_count, align 4, !tbaa !7
  %cmp58 = icmp sgt i32 20, %76
  br i1 %cmp58, label %while.body, label %while.end158

while.body:                                       ; preds = %while.cond
  %77 = bitcast double* %sum_distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %77) #6
  store double 0.000000e+00, double* %sum_distance, align 8, !tbaa !12
  %78 = bitcast double* %sum_distance_squared to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %78) #6
  store double 0.000000e+00, double* %sum_distance_squared, align 8, !tbaa !12
  %79 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  call void @clear_motion(%struct.RANSAC_MOTION* %current_motion, i32 %79)
  %80 = bitcast i32* %degenerate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #6
  store i32 1, i32* %degenerate, align 4, !tbaa !7
  %81 = bitcast i32* %num_degenerate_iter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #6
  store i32 0, i32* %num_degenerate_iter, align 4, !tbaa !7
  br label %while.cond59

while.cond59:                                     ; preds = %if.end72, %while.body
  %82 = load i32, i32* %degenerate, align 4, !tbaa !7
  %tobool60 = icmp ne i32 %82, 0
  br i1 %tobool60, label %while.body61, label %while.end

while.body61:                                     ; preds = %while.cond59
  %83 = load i32, i32* %num_degenerate_iter, align 4, !tbaa !7
  %inc62 = add nsw i32 %83, 1
  store i32 %inc62, i32* %num_degenerate_iter, align 4, !tbaa !7
  %84 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %85 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %indices, i32 0, i32 0
  %call63 = call i32 @get_rand_indices(i32 %84, i32 %85, i32* %arraydecay, i32* %seed)
  %tobool64 = icmp ne i32 %call63, 0
  br i1 %tobool64, label %if.end66, label %if.then65

if.then65:                                        ; preds = %while.body61
  store i32 1, i32* %ret_val, align 4, !tbaa !7
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end66:                                         ; preds = %while.body61
  %86 = load double*, double** %points1, align 4, !tbaa !5
  %87 = load double*, double** %corners1, align 4, !tbaa !5
  %arraydecay67 = getelementptr inbounds [4 x i32], [4 x i32]* %indices, i32 0, i32 0
  %88 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  call void @copy_points_at_indices(double* %86, double* %87, i32* %arraydecay67, i32 %88)
  %89 = load double*, double** %points2, align 4, !tbaa !5
  %90 = load double*, double** %corners2, align 4, !tbaa !5
  %arraydecay68 = getelementptr inbounds [4 x i32], [4 x i32]* %indices, i32 0, i32 0
  %91 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  call void @copy_points_at_indices(double* %89, double* %90, i32* %arraydecay68, i32 %91)
  %92 = load i32 (double*)*, i32 (double*)** %is_degenerate.addr, align 4, !tbaa !5
  %93 = load double*, double** %points1, align 4, !tbaa !5
  %call69 = call i32 %92(double* %93)
  store i32 %call69, i32* %degenerate, align 4, !tbaa !7
  %94 = load i32, i32* %num_degenerate_iter, align 4, !tbaa !7
  %cmp70 = icmp sgt i32 %94, 10
  br i1 %cmp70, label %if.then71, label %if.end72

if.then71:                                        ; preds = %if.end66
  store i32 1, i32* %ret_val, align 4, !tbaa !7
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end72:                                         ; preds = %if.end66
  br label %while.cond59

while.end:                                        ; preds = %while.cond59
  %95 = load i32 (i32, double*, double*, double*)*, i32 (i32, double*, double*, double*)** %find_transformation.addr, align 4, !tbaa !5
  %96 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %97 = load double*, double** %points1, align 4, !tbaa !5
  %98 = load double*, double** %points2, align 4, !tbaa !5
  %arraydecay73 = getelementptr inbounds [9 x double], [9 x double]* %params_this_motion, i32 0, i32 0
  %call74 = call i32 %95(i32 %96, double* %97, double* %98, double* %arraydecay73)
  %tobool75 = icmp ne i32 %call74, 0
  br i1 %tobool75, label %if.then76, label %if.end78

if.then76:                                        ; preds = %while.end
  %99 = load i32, i32* %trial_count, align 4, !tbaa !7
  %inc77 = add nsw i32 %99, 1
  store i32 %inc77, i32* %trial_count, align 4, !tbaa !7
  store i32 12, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end78:                                         ; preds = %while.end
  %100 = load void (double*, double*, double*, i32, i32, i32)*, void (double*, double*, double*, i32, i32, i32)** %projectpoints.addr, align 4, !tbaa !5
  %arraydecay79 = getelementptr inbounds [9 x double], [9 x double]* %params_this_motion, i32 0, i32 0
  %101 = load double*, double** %corners1, align 4, !tbaa !5
  %102 = load double*, double** %image1_coord, align 4, !tbaa !5
  %103 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  call void %100(double* %arraydecay79, double* %101, double* %102, i32 %103, i32 2, i32 2)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond80

for.cond80:                                       ; preds = %for.inc105, %if.end78
  %104 = load i32, i32* %i, align 4, !tbaa !7
  %105 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %cmp81 = icmp slt i32 %104, %105
  br i1 %cmp81, label %for.body82, label %for.end107

for.body82:                                       ; preds = %for.cond80
  %106 = bitcast double* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %106) #6
  %107 = load double*, double** %image1_coord, align 4, !tbaa !5
  %108 = load i32, i32* %i, align 4, !tbaa !7
  %mul83 = mul nsw i32 %108, 2
  %arrayidx84 = getelementptr inbounds double, double* %107, i32 %mul83
  %109 = load double, double* %arrayidx84, align 8, !tbaa !12
  %110 = load double*, double** %corners2, align 4, !tbaa !5
  %111 = load i32, i32* %i, align 4, !tbaa !7
  %mul85 = mul nsw i32 %111, 2
  %arrayidx86 = getelementptr inbounds double, double* %110, i32 %mul85
  %112 = load double, double* %arrayidx86, align 8, !tbaa !12
  %sub = fsub double %109, %112
  store double %sub, double* %dx, align 8, !tbaa !12
  %113 = bitcast double* %dy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %113) #6
  %114 = load double*, double** %image1_coord, align 4, !tbaa !5
  %115 = load i32, i32* %i, align 4, !tbaa !7
  %mul87 = mul nsw i32 %115, 2
  %add = add nsw i32 %mul87, 1
  %arrayidx88 = getelementptr inbounds double, double* %114, i32 %add
  %116 = load double, double* %arrayidx88, align 8, !tbaa !12
  %117 = load double*, double** %corners2, align 4, !tbaa !5
  %118 = load i32, i32* %i, align 4, !tbaa !7
  %mul89 = mul nsw i32 %118, 2
  %add90 = add nsw i32 %mul89, 1
  %arrayidx91 = getelementptr inbounds double, double* %117, i32 %add90
  %119 = load double, double* %arrayidx91, align 8, !tbaa !12
  %sub92 = fsub double %116, %119
  store double %sub92, double* %dy, align 8, !tbaa !12
  %120 = bitcast double* %distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %120) #6
  %121 = load double, double* %dx, align 8, !tbaa !12
  %122 = load double, double* %dx, align 8, !tbaa !12
  %mul93 = fmul double %121, %122
  %123 = load double, double* %dy, align 8, !tbaa !12
  %124 = load double, double* %dy, align 8, !tbaa !12
  %mul94 = fmul double %123, %124
  %add95 = fadd double %mul93, %mul94
  %125 = call double @llvm.sqrt.f64(double %add95)
  store double %125, double* %distance, align 8, !tbaa !12
  %126 = load double, double* %distance, align 8, !tbaa !12
  %cmp96 = fcmp olt double %126, 1.250000e+00
  br i1 %cmp96, label %if.then97, label %if.end104

if.then97:                                        ; preds = %for.body82
  %127 = load i32, i32* %i, align 4, !tbaa !7
  %inlier_indices98 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  %128 = load i32*, i32** %inlier_indices98, align 8, !tbaa !9
  %num_inliers = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %129 = load i32, i32* %num_inliers, align 8, !tbaa !13
  %inc99 = add nsw i32 %129, 1
  store i32 %inc99, i32* %num_inliers, align 8, !tbaa !13
  %arrayidx100 = getelementptr inbounds i32, i32* %128, i32 %129
  store i32 %127, i32* %arrayidx100, align 4, !tbaa !7
  %130 = load double, double* %distance, align 8, !tbaa !12
  %131 = load double, double* %sum_distance, align 8, !tbaa !12
  %add101 = fadd double %131, %130
  store double %add101, double* %sum_distance, align 8, !tbaa !12
  %132 = load double, double* %distance, align 8, !tbaa !12
  %133 = load double, double* %distance, align 8, !tbaa !12
  %mul102 = fmul double %132, %133
  %134 = load double, double* %sum_distance_squared, align 8, !tbaa !12
  %add103 = fadd double %134, %mul102
  store double %add103, double* %sum_distance_squared, align 8, !tbaa !12
  br label %if.end104

if.end104:                                        ; preds = %if.then97, %for.body82
  %135 = bitcast double* %distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %135) #6
  %136 = bitcast double* %dy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %136) #6
  %137 = bitcast double* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %137) #6
  br label %for.inc105

for.inc105:                                       ; preds = %if.end104
  %138 = load i32, i32* %i, align 4, !tbaa !7
  %inc106 = add nsw i32 %138, 1
  store i32 %inc106, i32* %i, align 4, !tbaa !7
  br label %for.cond80

for.end107:                                       ; preds = %for.cond80
  %num_inliers108 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %139 = load i32, i32* %num_inliers108, align 8, !tbaa !13
  %140 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %num_inliers109 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %140, i32 0, i32 0
  %141 = load i32, i32* %num_inliers109, align 8, !tbaa !13
  %cmp110 = icmp sge i32 %139, %141
  br i1 %cmp110, label %land.lhs.true111, label %if.end153

land.lhs.true111:                                 ; preds = %for.end107
  %num_inliers112 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %142 = load i32, i32* %num_inliers112, align 8, !tbaa !13
  %cmp113 = icmp sgt i32 %142, 1
  br i1 %cmp113, label %if.then114, label %if.end153

if.then114:                                       ; preds = %land.lhs.true111
  %143 = bitcast double* %mean_distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %143) #6
  %144 = load double, double* %sum_distance, align 8, !tbaa !12
  %num_inliers115 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %145 = load i32, i32* %num_inliers115, align 8, !tbaa !13
  %conv = sitofp i32 %145 to double
  %div = fdiv double %144, %conv
  store double %div, double* %mean_distance, align 8, !tbaa !12
  %146 = load double, double* %sum_distance_squared, align 8, !tbaa !12
  %num_inliers116 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %147 = load i32, i32* %num_inliers116, align 8, !tbaa !13
  %conv117 = sitofp i32 %147 to double
  %sub118 = fsub double %conv117, 1.000000e+00
  %div119 = fdiv double %146, %sub118
  %148 = load double, double* %mean_distance, align 8, !tbaa !12
  %149 = load double, double* %mean_distance, align 8, !tbaa !12
  %mul120 = fmul double %148, %149
  %num_inliers121 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %150 = load i32, i32* %num_inliers121, align 8, !tbaa !13
  %conv122 = sitofp i32 %150 to double
  %mul123 = fmul double %mul120, %conv122
  %num_inliers124 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %151 = load i32, i32* %num_inliers124, align 8, !tbaa !13
  %conv125 = sitofp i32 %151 to double
  %sub126 = fsub double %conv125, 1.000000e+00
  %div127 = fdiv double %mul123, %sub126
  %sub128 = fsub double %div119, %div127
  %variance = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 1
  store double %sub128, double* %variance, align 8, !tbaa !14
  %152 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %call129 = call i32 @is_better_motion(%struct.RANSAC_MOTION* %current_motion, %struct.RANSAC_MOTION* %152)
  %tobool130 = icmp ne i32 %call129, 0
  br i1 %tobool130, label %if.then131, label %if.end152

if.then131:                                       ; preds = %if.then114
  %num_inliers132 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 0
  %153 = load i32, i32* %num_inliers132, align 8, !tbaa !13
  %154 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %num_inliers133 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %154, i32 0, i32 0
  store i32 %153, i32* %num_inliers133, align 8, !tbaa !13
  %variance134 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 1
  %155 = load double, double* %variance134, align 8, !tbaa !14
  %156 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %variance135 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %156, i32 0, i32 1
  store double %155, double* %variance135, align 8, !tbaa !14
  %157 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %inlier_indices136 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %157, i32 0, i32 2
  %158 = load i32*, i32** %inlier_indices136, align 8, !tbaa !9
  %159 = bitcast i32* %158 to i8*
  %inlier_indices137 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  %160 = load i32*, i32** %inlier_indices137, align 8, !tbaa !9
  %161 = bitcast i32* %160 to i8*
  %162 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul138 = mul i32 4, %162
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %159, i8* align 4 %161, i32 %mul138, i1 false)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond139

for.cond139:                                      ; preds = %for.inc149, %if.then131
  %163 = load i32, i32* %i, align 4, !tbaa !7
  %164 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp140 = icmp slt i32 %163, %164
  br i1 %cmp140, label %for.body142, label %for.end151

for.body142:                                      ; preds = %for.cond139
  %165 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  %166 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %167 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx143 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %166, i32 %167
  %call144 = call i32 @is_better_motion(%struct.RANSAC_MOTION* %165, %struct.RANSAC_MOTION* %arrayidx143)
  %tobool145 = icmp ne i32 %call144, 0
  br i1 %tobool145, label %if.then146, label %if.end148

if.then146:                                       ; preds = %for.body142
  %168 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %169 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx147 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %168, i32 %169
  store %struct.RANSAC_MOTION* %arrayidx147, %struct.RANSAC_MOTION** %worst_kept_motion, align 4, !tbaa !5
  br label %if.end148

if.end148:                                        ; preds = %if.then146, %for.body142
  br label %for.inc149

for.inc149:                                       ; preds = %if.end148
  %170 = load i32, i32* %i, align 4, !tbaa !7
  %inc150 = add nsw i32 %170, 1
  store i32 %inc150, i32* %i, align 4, !tbaa !7
  br label %for.cond139

for.end151:                                       ; preds = %for.cond139
  br label %if.end152

if.end152:                                        ; preds = %for.end151, %if.then114
  %171 = bitcast double* %mean_distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %171) #6
  br label %if.end153

if.end153:                                        ; preds = %if.end152, %land.lhs.true111, %for.end107
  %172 = load i32, i32* %trial_count, align 4, !tbaa !7
  %inc154 = add nsw i32 %172, 1
  store i32 %inc154, i32* %trial_count, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then71, %if.then65, %if.end153, %if.then76
  %173 = bitcast i32* %num_degenerate_iter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #6
  %174 = bitcast i32* %degenerate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #6
  %175 = bitcast double* %sum_distance_squared to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %175) #6
  %176 = bitcast double* %sum_distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %176) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup202 [
    i32 0, label %cleanup.cont
    i32 12, label %while.cond
    i32 8, label %finish_ransac
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end158:                                     ; preds = %while.cond
  %177 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %178 = bitcast %struct.RANSAC_MOTION* %177 to i8*
  %179 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  call void @qsort(i8* %178, i32 %179, i32 24, i32 (i8*, i8*)* @compare_motions)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond159

for.cond159:                                      ; preds = %for.inc189, %while.end158
  %180 = load i32, i32* %i, align 4, !tbaa !7
  %181 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp160 = icmp slt i32 %180, %181
  br i1 %cmp160, label %for.body162, label %for.end191

for.body162:                                      ; preds = %for.cond159
  %182 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %183 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx163 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %182, i32 %183
  %num_inliers164 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx163, i32 0, i32 0
  %184 = load i32, i32* %num_inliers164, align 8, !tbaa !13
  %185 = load i32, i32* %minpts.addr, align 4, !tbaa !7
  %cmp165 = icmp sge i32 %184, %185
  br i1 %cmp165, label %if.then167, label %if.end185

if.then167:                                       ; preds = %for.body162
  %186 = load double*, double** %points1, align 4, !tbaa !5
  %187 = load double*, double** %corners1, align 4, !tbaa !5
  %188 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %189 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx168 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %188, i32 %189
  %inlier_indices169 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx168, i32 0, i32 2
  %190 = load i32*, i32** %inlier_indices169, align 8, !tbaa !9
  %191 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %192 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx170 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %191, i32 %192
  %num_inliers171 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx170, i32 0, i32 0
  %193 = load i32, i32* %num_inliers171, align 8, !tbaa !13
  call void @copy_points_at_indices(double* %186, double* %187, i32* %190, i32 %193)
  %194 = load double*, double** %points2, align 4, !tbaa !5
  %195 = load double*, double** %corners2, align 4, !tbaa !5
  %196 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %197 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx172 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %196, i32 %197
  %inlier_indices173 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx172, i32 0, i32 2
  %198 = load i32*, i32** %inlier_indices173, align 8, !tbaa !9
  %199 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %200 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx174 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %199, i32 %200
  %num_inliers175 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx174, i32 0, i32 0
  %201 = load i32, i32* %num_inliers175, align 8, !tbaa !13
  call void @copy_points_at_indices(double* %194, double* %195, i32* %198, i32 %201)
  %202 = load i32 (i32, double*, double*, double*)*, i32 (i32, double*, double*, double*)** %find_transformation.addr, align 4, !tbaa !5
  %203 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %204 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx176 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %203, i32 %204
  %num_inliers177 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx176, i32 0, i32 0
  %205 = load i32, i32* %num_inliers177, align 8, !tbaa !13
  %206 = load double*, double** %points1, align 4, !tbaa !5
  %207 = load double*, double** %points2, align 4, !tbaa !5
  %208 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %209 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx178 = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %208, i32 %209
  %params = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %arrayidx178, i32 0, i32 0
  %arraydecay179 = getelementptr inbounds [8 x double], [8 x double]* %params, i32 0, i32 0
  %call180 = call i32 %202(i32 %205, double* %206, double* %207, double* %arraydecay179)
  %210 = load %struct.MotionModel*, %struct.MotionModel** %params_by_motion.addr, align 4, !tbaa !5
  %211 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx181 = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %210, i32 %211
  %inliers = getelementptr inbounds %struct.MotionModel, %struct.MotionModel* %arrayidx181, i32 0, i32 1
  %212 = load i32*, i32** %inliers, align 8, !tbaa !17
  %213 = bitcast i32* %212 to i8*
  %214 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %215 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx182 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %214, i32 %215
  %inlier_indices183 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx182, i32 0, i32 2
  %216 = load i32*, i32** %inlier_indices183, align 8, !tbaa !9
  %217 = bitcast i32* %216 to i8*
  %218 = load i32, i32* %npoints.addr, align 4, !tbaa !7
  %mul184 = mul i32 4, %218
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %213, i8* align 4 %217, i32 %mul184, i1 false)
  br label %if.end185

if.end185:                                        ; preds = %if.then167, %for.body162
  %219 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %220 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx186 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %219, i32 %220
  %num_inliers187 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx186, i32 0, i32 0
  %221 = load i32, i32* %num_inliers187, align 8, !tbaa !13
  %222 = load i32*, i32** %num_inliers_by_motion.addr, align 4, !tbaa !5
  %223 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx188 = getelementptr inbounds i32, i32* %222, i32 %223
  store i32 %221, i32* %arrayidx188, align 4, !tbaa !7
  br label %for.inc189

for.inc189:                                       ; preds = %if.end185
  %224 = load i32, i32* %i, align 4, !tbaa !7
  %inc190 = add nsw i32 %224, 1
  store i32 %inc190, i32* %i, align 4, !tbaa !7
  br label %for.cond159

for.end191:                                       ; preds = %for.cond159
  br label %finish_ransac

finish_ransac:                                    ; preds = %for.end191, %cleanup, %if.then43
  %225 = load double*, double** %points1, align 4, !tbaa !5
  %226 = bitcast double* %225 to i8*
  call void @aom_free(i8* %226)
  %227 = load double*, double** %points2, align 4, !tbaa !5
  %228 = bitcast double* %227 to i8*
  call void @aom_free(i8* %228)
  %229 = load double*, double** %corners1, align 4, !tbaa !5
  %230 = bitcast double* %229 to i8*
  call void @aom_free(i8* %230)
  %231 = load double*, double** %corners2, align 4, !tbaa !5
  %232 = bitcast double* %231 to i8*
  call void @aom_free(i8* %232)
  %233 = load double*, double** %image1_coord, align 4, !tbaa !5
  %234 = bitcast double* %233 to i8*
  call void @aom_free(i8* %234)
  %inlier_indices192 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %current_motion, i32 0, i32 2
  %235 = load i32*, i32** %inlier_indices192, align 8, !tbaa !9
  %236 = bitcast i32* %235 to i8*
  call void @aom_free(i8* %236)
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond193

for.cond193:                                      ; preds = %for.inc199, %finish_ransac
  %237 = load i32, i32* %i, align 4, !tbaa !7
  %238 = load i32, i32* %num_desired_motions.addr, align 4, !tbaa !7
  %cmp194 = icmp slt i32 %237, %238
  br i1 %cmp194, label %for.body196, label %for.end201

for.body196:                                      ; preds = %for.cond193
  %239 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %240 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx197 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %239, i32 %240
  %inlier_indices198 = getelementptr inbounds %struct.RANSAC_MOTION, %struct.RANSAC_MOTION* %arrayidx197, i32 0, i32 2
  %241 = load i32*, i32** %inlier_indices198, align 8, !tbaa !9
  %242 = bitcast i32* %241 to i8*
  call void @aom_free(i8* %242)
  br label %for.inc199

for.inc199:                                       ; preds = %for.body196
  %243 = load i32, i32* %i, align 4, !tbaa !7
  %inc200 = add nsw i32 %243, 1
  store i32 %inc200, i32* %i, align 4, !tbaa !7
  br label %for.cond193

for.end201:                                       ; preds = %for.cond193
  %244 = load %struct.RANSAC_MOTION*, %struct.RANSAC_MOTION** %motions, align 4, !tbaa !5
  %245 = bitcast %struct.RANSAC_MOTION* %244 to i8*
  call void @aom_free(i8* %245)
  %246 = load i32, i32* %ret_val, align 4, !tbaa !7
  store i32 %246, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup202

cleanup202:                                       ; preds = %for.end201, %cleanup, %if.then
  %247 = bitcast double** %cnp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #6
  %248 = bitcast double** %cnp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #6
  %249 = bitcast [9 x double]* %params_this_motion to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %249) #6
  %250 = bitcast %struct.RANSAC_MOTION* %current_motion to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %250) #6
  %251 = bitcast %struct.RANSAC_MOTION** %worst_kept_motion to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #6
  %252 = bitcast %struct.RANSAC_MOTION** %motions to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #6
  %253 = bitcast double** %image1_coord to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #6
  %254 = bitcast double** %corners2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #6
  %255 = bitcast double** %corners1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #6
  %256 = bitcast double** %points2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #6
  %257 = bitcast double** %points1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #6
  %258 = bitcast [4 x i32]* %indices to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %258) #6
  %259 = bitcast i32* %seed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #6
  %260 = bitcast i32* %ret_val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #6
  %261 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #6
  %262 = bitcast i32* %trial_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #6
  %263 = load i32, i32* %retval, align 4
  ret i32 %263
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"any pointer", !3, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"int", !3, i64 0}
!9 = !{!10, !6, i64 16}
!10 = !{!"", !8, i64 0, !11, i64 8, !6, i64 16}
!11 = !{!"double", !3, i64 0}
!12 = !{!11, !11, i64 0}
!13 = !{!10, !8, i64 0}
!14 = !{!10, !11, i64 8}
!15 = !{!16, !8, i64 68}
!16 = !{!"", !3, i64 0, !6, i64 64, !8, i64 68}
!17 = !{!16, !6, i64 64}
