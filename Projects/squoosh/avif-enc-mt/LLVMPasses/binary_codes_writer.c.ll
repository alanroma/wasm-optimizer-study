; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/binary_codes_writer.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/binary_codes_writer.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_writer = type { i32, i8*, %struct.od_ec_enc, i8 }
%struct.od_ec_enc = type { i8*, i32, i16*, i32, i32, i32, i16, i16, i32 }

; Function Attrs: nounwind
define hidden void @aom_write_primitive_symmetric(%struct.aom_writer* %w, i16 signext %v, i32 %abs_bits) #0 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %v.addr = alloca i16, align 2
  %abs_bits.addr = alloca i32, align 4
  %x = alloca i32, align 4
  %s = alloca i32, align 4
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  store i32 %abs_bits, i32* %abs_bits.addr, align 4, !tbaa !8
  %0 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv = sext i16 %0 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  call void @aom_write_bit(%struct.aom_writer* %1, i32 0)
  br label %if.end

if.else:                                          ; preds = %entry
  %2 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv2 = sext i16 %3 to i32
  %call = call i32 @abs(i32 %conv2) #7
  store i32 %call, i32* %x, align 4, !tbaa !8
  %4 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv3 = sext i16 %5 to i32
  %cmp4 = icmp slt i32 %conv3, 0
  %conv5 = zext i1 %cmp4 to i32
  store i32 %conv5, i32* %s, align 4, !tbaa !8
  %6 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  call void @aom_write_bit(%struct.aom_writer* %6, i32 1)
  %7 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %8 = load i32, i32* %s, align 4, !tbaa !8
  call void @aom_write_bit(%struct.aom_writer* %7, i32 %8)
  %9 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %10 = load i32, i32* %x, align 4, !tbaa !8
  %sub = sub nsw i32 %10, 1
  %11 = load i32, i32* %abs_bits.addr, align 4, !tbaa !8
  call void @aom_write_literal(%struct.aom_writer* %9, i32 %sub, i32 %11)
  %12 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @aom_write_bit(%struct.aom_writer* %w, i32 %bit) #1 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %bit.addr = alloca i32, align 4
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  store i32 %bit, i32* %bit.addr, align 4, !tbaa !8
  %0 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %1 = load i32, i32* %bit.addr, align 4, !tbaa !8
  call void @aom_write(%struct.aom_writer* %0, i32 %1, i32 128)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #3

; Function Attrs: inlinehint nounwind
define internal void @aom_write_literal(%struct.aom_writer* %w, i32 %data, i32 %bits) #1 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %data.addr = alloca i32, align 4
  %bits.addr = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  store i32 %data, i32* %data.addr, align 4, !tbaa !8
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !8
  %0 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %bits.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %1, 1
  store i32 %sub, i32* %bit, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %bit, align 4, !tbaa !8
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %4 = load i32, i32* %data.addr, align 4, !tbaa !8
  %5 = load i32, i32* %bit, align 4, !tbaa !8
  %shr = ashr i32 %4, %5
  %and = and i32 1, %shr
  call void @aom_write_bit(%struct.aom_writer* %3, i32 %and)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %bit, align 4, !tbaa !8
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %bit, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden i32 @aom_count_primitive_symmetric(i16 signext %v, i32 %abs_bits) #0 {
entry:
  %v.addr = alloca i16, align 2
  %abs_bits.addr = alloca i32, align 4
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  store i32 %abs_bits, i32* %abs_bits.addr, align 4, !tbaa !8
  %0 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv = sext i16 %0 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %abs_bits.addr, align 4, !tbaa !8
  %add = add i32 %1, 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %add, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define hidden void @aom_write_primitive_quniform(%struct.aom_writer* %w, i16 zeroext %n, i16 zeroext %v) #0 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %n.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  %l = alloca i32, align 4
  %m = alloca i32, align 4
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %cmp = icmp sle i32 %conv, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv2 = zext i16 %2 to i32
  %call = call i32 @get_msb(i32 %conv2)
  %add = add nsw i32 %call, 1
  store i32 %add, i32* %l, align 4, !tbaa !8
  %3 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %l, align 4, !tbaa !8
  %shl = shl i32 1, %4
  %5 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv3 = zext i16 %5 to i32
  %sub = sub nsw i32 %shl, %conv3
  store i32 %sub, i32* %m, align 4, !tbaa !8
  %6 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv4 = zext i16 %6 to i32
  %7 = load i32, i32* %m, align 4, !tbaa !8
  %cmp5 = icmp slt i32 %conv4, %7
  br i1 %cmp5, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end
  %8 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %9 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv8 = zext i16 %9 to i32
  %10 = load i32, i32* %l, align 4, !tbaa !8
  %sub9 = sub nsw i32 %10, 1
  call void @aom_write_literal(%struct.aom_writer* %8, i32 %conv8, i32 %sub9)
  br label %if.end16

if.else:                                          ; preds = %if.end
  %11 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %12 = load i32, i32* %m, align 4, !tbaa !8
  %13 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv10 = zext i16 %13 to i32
  %14 = load i32, i32* %m, align 4, !tbaa !8
  %sub11 = sub nsw i32 %conv10, %14
  %shr = ashr i32 %sub11, 1
  %add12 = add nsw i32 %12, %shr
  %15 = load i32, i32* %l, align 4, !tbaa !8
  %sub13 = sub nsw i32 %15, 1
  call void @aom_write_literal(%struct.aom_writer* %11, i32 %add12, i32 %sub13)
  %16 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %17 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv14 = zext i16 %17 to i32
  %18 = load i32, i32* %m, align 4, !tbaa !8
  %sub15 = sub nsw i32 %conv14, %18
  %and = and i32 %sub15, 1
  call void @aom_write_bit(%struct.aom_writer* %16, i32 %and)
  br label %if.end16

if.end16:                                         ; preds = %if.else, %if.then7
  %19 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  br label %return

return:                                           ; preds = %if.end16, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #1 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: nounwind
define hidden i32 @aom_count_primitive_quniform(i16 zeroext %n, i16 zeroext %v) #0 {
entry:
  %retval = alloca i32, align 4
  %n.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  %l = alloca i32, align 4
  %m = alloca i32, align 4
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %cmp = icmp sle i32 %conv, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv2 = zext i16 %2 to i32
  %call = call i32 @get_msb(i32 %conv2)
  %add = add nsw i32 %call, 1
  store i32 %add, i32* %l, align 4, !tbaa !8
  %3 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %l, align 4, !tbaa !8
  %shl = shl i32 1, %4
  %5 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv3 = zext i16 %5 to i32
  %sub = sub nsw i32 %shl, %conv3
  store i32 %sub, i32* %m, align 4, !tbaa !8
  %6 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv4 = zext i16 %6 to i32
  %7 = load i32, i32* %m, align 4, !tbaa !8
  %cmp5 = icmp slt i32 %conv4, %7
  br i1 %cmp5, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %8 = load i32, i32* %l, align 4, !tbaa !8
  %sub7 = sub nsw i32 %8, 1
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %9 = load i32, i32* %l, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub7, %cond.true ], [ %9, %cond.false ]
  store i32 %cond, i32* %retval, align 4
  %10 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: nounwind
define hidden void @aom_write_primitive_subexpfin(%struct.aom_writer* %w, i16 zeroext %n, i16 zeroext %k, i16 zeroext %v) #0 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  %i = alloca i32, align 4
  %mk = alloca i32, align 4
  %b = alloca i32, align 4
  %a = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %t = alloca i32, align 4
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %k, i16* %k.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !8
  %1 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %mk, align 4, !tbaa !8
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont26, %entry
  br label %while.body

while.body:                                       ; preds = %while.cond
  %2 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %i, align 4, !tbaa !8
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %4 = load i16, i16* %k.addr, align 2, !tbaa !6
  %conv = zext i16 %4 to i32
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %add = add nsw i32 %conv, %5
  %sub = sub nsw i32 %add, 1
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %6 = load i16, i16* %k.addr, align 2, !tbaa !6
  %conv1 = zext i16 %6 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ %conv1, %cond.false ]
  store i32 %cond, i32* %b, align 4, !tbaa !8
  %7 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i32, i32* %b, align 4, !tbaa !8
  %shl = shl i32 1, %8
  store i32 %shl, i32* %a, align 4, !tbaa !8
  %9 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv2 = zext i16 %9 to i32
  %10 = load i32, i32* %mk, align 4, !tbaa !8
  %11 = load i32, i32* %a, align 4, !tbaa !8
  %mul = mul nsw i32 3, %11
  %add3 = add nsw i32 %10, %mul
  %cmp = icmp sle i32 %conv2, %add3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %12 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %13 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv5 = zext i16 %13 to i32
  %14 = load i32, i32* %mk, align 4, !tbaa !8
  %sub6 = sub nsw i32 %conv5, %14
  %conv7 = trunc i32 %sub6 to i16
  %15 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv8 = zext i16 %15 to i32
  %16 = load i32, i32* %mk, align 4, !tbaa !8
  %sub9 = sub nsw i32 %conv8, %16
  %conv10 = trunc i32 %sub9 to i16
  call void @aom_write_primitive_quniform(%struct.aom_writer* %12, i16 zeroext %conv7, i16 zeroext %conv10)
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

if.else:                                          ; preds = %cond.end
  %17 = bitcast i32* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv11 = zext i16 %18 to i32
  %19 = load i32, i32* %mk, align 4, !tbaa !8
  %20 = load i32, i32* %a, align 4, !tbaa !8
  %add12 = add nsw i32 %19, %20
  %cmp13 = icmp sge i32 %conv11, %add12
  %conv14 = zext i1 %cmp13 to i32
  store i32 %conv14, i32* %t, align 4, !tbaa !8
  %21 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %22 = load i32, i32* %t, align 4, !tbaa !8
  call void @aom_write_bit(%struct.aom_writer* %21, i32 %22)
  %23 = load i32, i32* %t, align 4, !tbaa !8
  %tobool15 = icmp ne i32 %23, 0
  br i1 %tobool15, label %if.then16, label %if.else19

if.then16:                                        ; preds = %if.else
  %24 = load i32, i32* %i, align 4, !tbaa !8
  %add17 = add nsw i32 %24, 1
  store i32 %add17, i32* %i, align 4, !tbaa !8
  %25 = load i32, i32* %a, align 4, !tbaa !8
  %26 = load i32, i32* %mk, align 4, !tbaa !8
  %add18 = add nsw i32 %26, %25
  store i32 %add18, i32* %mk, align 4, !tbaa !8
  br label %if.end

if.else19:                                        ; preds = %if.else
  %27 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %28 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv20 = zext i16 %28 to i32
  %29 = load i32, i32* %mk, align 4, !tbaa !8
  %sub21 = sub nsw i32 %conv20, %29
  %30 = load i32, i32* %b, align 4, !tbaa !8
  call void @aom_write_literal(%struct.aom_writer* %27, i32 %sub21, i32 %30)
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then16
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else19
  %31 = bitcast i32* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup23 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end22

if.end22:                                         ; preds = %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.end22, %cleanup, %if.then
  %32 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  %33 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %cleanup.dest25 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest25, label %unreachable [
    i32 0, label %cleanup.cont26
    i32 3, label %while.end
  ]

cleanup.cont26:                                   ; preds = %cleanup23
  br label %while.cond

while.end:                                        ; preds = %cleanup23
  %34 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  ret void

unreachable:                                      ; preds = %cleanup23
  unreachable
}

; Function Attrs: nounwind
define hidden i32 @aom_count_primitive_subexpfin(i16 zeroext %n, i16 zeroext %k, i16 zeroext %v) #0 {
entry:
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  %count = alloca i32, align 4
  %i = alloca i32, align 4
  %mk = alloca i32, align 4
  %b = alloca i32, align 4
  %a = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %t = alloca i32, align 4
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %k, i16* %k.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %count, align 4, !tbaa !8
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !8
  %2 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 0, i32* %mk, align 4, !tbaa !8
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont26, %entry
  br label %while.body

while.body:                                       ; preds = %while.cond
  %3 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %i, align 4, !tbaa !8
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %5 = load i16, i16* %k.addr, align 2, !tbaa !6
  %conv = zext i16 %5 to i32
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %add = add nsw i32 %conv, %6
  %sub = sub nsw i32 %add, 1
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %7 = load i16, i16* %k.addr, align 2, !tbaa !6
  %conv1 = zext i16 %7 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ %conv1, %cond.false ]
  store i32 %cond, i32* %b, align 4, !tbaa !8
  %8 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %b, align 4, !tbaa !8
  %shl = shl i32 1, %9
  store i32 %shl, i32* %a, align 4, !tbaa !8
  %10 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv2 = zext i16 %10 to i32
  %11 = load i32, i32* %mk, align 4, !tbaa !8
  %12 = load i32, i32* %a, align 4, !tbaa !8
  %mul = mul nsw i32 3, %12
  %add3 = add nsw i32 %11, %mul
  %cmp = icmp sle i32 %conv2, %add3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %13 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv5 = zext i16 %13 to i32
  %14 = load i32, i32* %mk, align 4, !tbaa !8
  %sub6 = sub nsw i32 %conv5, %14
  %conv7 = trunc i32 %sub6 to i16
  %15 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv8 = zext i16 %15 to i32
  %16 = load i32, i32* %mk, align 4, !tbaa !8
  %sub9 = sub nsw i32 %conv8, %16
  %conv10 = trunc i32 %sub9 to i16
  %call = call i32 @aom_count_primitive_quniform(i16 zeroext %conv7, i16 zeroext %conv10)
  %17 = load i32, i32* %count, align 4, !tbaa !8
  %add11 = add nsw i32 %17, %call
  store i32 %add11, i32* %count, align 4, !tbaa !8
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

if.else:                                          ; preds = %cond.end
  %18 = bitcast i32* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv12 = zext i16 %19 to i32
  %20 = load i32, i32* %mk, align 4, !tbaa !8
  %21 = load i32, i32* %a, align 4, !tbaa !8
  %add13 = add nsw i32 %20, %21
  %cmp14 = icmp sge i32 %conv12, %add13
  %conv15 = zext i1 %cmp14 to i32
  store i32 %conv15, i32* %t, align 4, !tbaa !8
  %22 = load i32, i32* %count, align 4, !tbaa !8
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %count, align 4, !tbaa !8
  %23 = load i32, i32* %t, align 4, !tbaa !8
  %tobool16 = icmp ne i32 %23, 0
  br i1 %tobool16, label %if.then17, label %if.else20

if.then17:                                        ; preds = %if.else
  %24 = load i32, i32* %i, align 4, !tbaa !8
  %add18 = add nsw i32 %24, 1
  store i32 %add18, i32* %i, align 4, !tbaa !8
  %25 = load i32, i32* %a, align 4, !tbaa !8
  %26 = load i32, i32* %mk, align 4, !tbaa !8
  %add19 = add nsw i32 %26, %25
  store i32 %add19, i32* %mk, align 4, !tbaa !8
  br label %if.end

if.else20:                                        ; preds = %if.else
  %27 = load i32, i32* %b, align 4, !tbaa !8
  %28 = load i32, i32* %count, align 4, !tbaa !8
  %add21 = add nsw i32 %28, %27
  store i32 %add21, i32* %count, align 4, !tbaa !8
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then17
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else20
  %29 = bitcast i32* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup23 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end22

if.end22:                                         ; preds = %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.end22, %cleanup, %if.then
  %30 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  %31 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  %cleanup.dest25 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest25, label %unreachable [
    i32 0, label %cleanup.cont26
    i32 3, label %while.end
  ]

cleanup.cont26:                                   ; preds = %cleanup23
  br label %while.cond

while.end:                                        ; preds = %cleanup23
  %32 = load i32, i32* %count, align 4, !tbaa !8
  store i32 1, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  ret i32 %32

unreachable:                                      ; preds = %cleanup23
  unreachable
}

; Function Attrs: nounwind
define hidden void @aom_write_primitive_refsubexpfin(%struct.aom_writer* %w, i16 zeroext %n, i16 zeroext %k, i16 zeroext %ref, i16 zeroext %v) #0 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %ref.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %k, i16* %k.addr, align 2, !tbaa !6
  store i16 %ref, i16* %ref.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %1 = load i16, i16* %n.addr, align 2, !tbaa !6
  %2 = load i16, i16* %k.addr, align 2, !tbaa !6
  %3 = load i16, i16* %n.addr, align 2, !tbaa !6
  %4 = load i16, i16* %ref.addr, align 2, !tbaa !6
  %5 = load i16, i16* %v.addr, align 2, !tbaa !6
  %call = call zeroext i16 @recenter_finite_nonneg(i16 zeroext %3, i16 zeroext %4, i16 zeroext %5)
  call void @aom_write_primitive_subexpfin(%struct.aom_writer* %0, i16 zeroext %1, i16 zeroext %2, i16 zeroext %call)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @recenter_finite_nonneg(i16 zeroext %n, i16 zeroext %r, i16 zeroext %v) #1 {
entry:
  %retval = alloca i16, align 2
  %n.addr = alloca i16, align 2
  %r.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %r, i16* %r.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %shl = shl i32 %conv, 1
  %1 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv1 = zext i16 %1 to i32
  %cmp = icmp sle i32 %shl, %conv1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i16, i16* %r.addr, align 2, !tbaa !6
  %3 = load i16, i16* %v.addr, align 2, !tbaa !6
  %call = call zeroext i16 @recenter_nonneg(i16 zeroext %2, i16 zeroext %3)
  store i16 %call, i16* %retval, align 2
  br label %return

if.else:                                          ; preds = %entry
  %4 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv3 = zext i16 %4 to i32
  %sub = sub nsw i32 %conv3, 1
  %5 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv4 = zext i16 %5 to i32
  %sub5 = sub nsw i32 %sub, %conv4
  %conv6 = trunc i32 %sub5 to i16
  %6 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv7 = zext i16 %6 to i32
  %sub8 = sub nsw i32 %conv7, 1
  %7 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv9 = zext i16 %7 to i32
  %sub10 = sub nsw i32 %sub8, %conv9
  %conv11 = trunc i32 %sub10 to i16
  %call12 = call zeroext i16 @recenter_nonneg(i16 zeroext %conv6, i16 zeroext %conv11)
  store i16 %call12, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.else, %if.then
  %8 = load i16, i16* %retval, align 2
  ret i16 %8
}

; Function Attrs: nounwind
define hidden void @aom_write_signed_primitive_refsubexpfin(%struct.aom_writer* %w, i16 zeroext %n, i16 zeroext %k, i16 signext %ref, i16 signext %v) #0 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %ref.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  %scaled_n = alloca i16, align 2
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %k, i16* %k.addr, align 2, !tbaa !6
  store i16 %ref, i16* %ref.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %sub = sub nsw i32 %conv, 1
  %1 = load i16, i16* %ref.addr, align 2, !tbaa !6
  %conv1 = sext i16 %1 to i32
  %add = add nsw i32 %conv1, %sub
  %conv2 = trunc i32 %add to i16
  store i16 %conv2, i16* %ref.addr, align 2, !tbaa !6
  %2 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv3 = zext i16 %2 to i32
  %sub4 = sub nsw i32 %conv3, 1
  %3 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv5 = sext i16 %3 to i32
  %add6 = add nsw i32 %conv5, %sub4
  %conv7 = trunc i32 %add6 to i16
  store i16 %conv7, i16* %v.addr, align 2, !tbaa !6
  %4 = bitcast i16* %scaled_n to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #6
  %5 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv8 = zext i16 %5 to i32
  %shl = shl i32 %conv8, 1
  %sub9 = sub nsw i32 %shl, 1
  %conv10 = trunc i32 %sub9 to i16
  store i16 %conv10, i16* %scaled_n, align 2, !tbaa !6
  %6 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %7 = load i16, i16* %scaled_n, align 2, !tbaa !6
  %8 = load i16, i16* %k.addr, align 2, !tbaa !6
  %9 = load i16, i16* %ref.addr, align 2, !tbaa !6
  %10 = load i16, i16* %v.addr, align 2, !tbaa !6
  call void @aom_write_primitive_refsubexpfin(%struct.aom_writer* %6, i16 zeroext %7, i16 zeroext %8, i16 zeroext %9, i16 zeroext %10)
  %11 = bitcast i16* %scaled_n to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %11) #6
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_count_primitive_refsubexpfin(i16 zeroext %n, i16 zeroext %k, i16 zeroext %ref, i16 zeroext %v) #0 {
entry:
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %ref.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %k, i16* %k.addr, align 2, !tbaa !6
  store i16 %ref, i16* %ref.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load i16, i16* %n.addr, align 2, !tbaa !6
  %1 = load i16, i16* %k.addr, align 2, !tbaa !6
  %2 = load i16, i16* %n.addr, align 2, !tbaa !6
  %3 = load i16, i16* %ref.addr, align 2, !tbaa !6
  %4 = load i16, i16* %v.addr, align 2, !tbaa !6
  %call = call zeroext i16 @recenter_finite_nonneg(i16 zeroext %2, i16 zeroext %3, i16 zeroext %4)
  %call1 = call i32 @aom_count_primitive_subexpfin(i16 zeroext %0, i16 zeroext %1, i16 zeroext %call)
  ret i32 %call1
}

; Function Attrs: nounwind
define hidden i32 @aom_count_signed_primitive_refsubexpfin(i16 zeroext %n, i16 zeroext %k, i16 signext %ref, i16 signext %v) #0 {
entry:
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %ref.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  %scaled_n = alloca i16, align 2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %k, i16* %k.addr, align 2, !tbaa !6
  store i16 %ref, i16* %ref.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %sub = sub nsw i32 %conv, 1
  %1 = load i16, i16* %ref.addr, align 2, !tbaa !6
  %conv1 = sext i16 %1 to i32
  %add = add nsw i32 %conv1, %sub
  %conv2 = trunc i32 %add to i16
  store i16 %conv2, i16* %ref.addr, align 2, !tbaa !6
  %2 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv3 = zext i16 %2 to i32
  %sub4 = sub nsw i32 %conv3, 1
  %3 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv5 = sext i16 %3 to i32
  %add6 = add nsw i32 %conv5, %sub4
  %conv7 = trunc i32 %add6 to i16
  store i16 %conv7, i16* %v.addr, align 2, !tbaa !6
  %4 = bitcast i16* %scaled_n to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #6
  %5 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv8 = zext i16 %5 to i32
  %shl = shl i32 %conv8, 1
  %sub9 = sub nsw i32 %shl, 1
  %conv10 = trunc i32 %sub9 to i16
  store i16 %conv10, i16* %scaled_n, align 2, !tbaa !6
  %6 = load i16, i16* %scaled_n, align 2, !tbaa !6
  %7 = load i16, i16* %k.addr, align 2, !tbaa !6
  %8 = load i16, i16* %ref.addr, align 2, !tbaa !6
  %9 = load i16, i16* %v.addr, align 2, !tbaa !6
  %call = call i32 @aom_count_primitive_refsubexpfin(i16 zeroext %6, i16 zeroext %7, i16 zeroext %8, i16 zeroext %9)
  %10 = bitcast i16* %scaled_n to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %10) #6
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define internal void @aom_write(%struct.aom_writer* %w, i32 %bit, i32 %probability) #1 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %bit.addr = alloca i32, align 4
  %probability.addr = alloca i32, align 4
  %p = alloca i32, align 4
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  store i32 %bit, i32* %bit.addr, align 4, !tbaa !8
  store i32 %probability, i32* %probability.addr, align 4, !tbaa !8
  %0 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %probability.addr, align 4, !tbaa !8
  %shl = shl i32 %1, 15
  %sub = sub nsw i32 8388607, %shl
  %2 = load i32, i32* %probability.addr, align 4, !tbaa !8
  %add = add nsw i32 %sub, %2
  %shr = ashr i32 %add, 8
  store i32 %shr, i32* %p, align 4, !tbaa !8
  %3 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_writer, %struct.aom_writer* %3, i32 0, i32 2
  %4 = load i32, i32* %bit.addr, align 4, !tbaa !8
  %5 = load i32, i32* %p, align 4, !tbaa !8
  call void @od_ec_encode_bool_q15(%struct.od_ec_enc* %ec, i32 %4, i32 %5)
  %6 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret void
}

declare void @od_ec_encode_bool_q15(%struct.od_ec_enc*, i32, i32) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #5

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @recenter_nonneg(i16 zeroext %r, i16 zeroext %v) #1 {
entry:
  %retval = alloca i16, align 2
  %r.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store i16 %r, i16* %r.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %1 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv1 = zext i16 %1 to i32
  %shl = shl i32 %conv1, 1
  %cmp = icmp sgt i32 %conv, %shl
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i16, i16* %v.addr, align 2, !tbaa !6
  store i16 %2, i16* %retval, align 2
  br label %return

if.else:                                          ; preds = %entry
  %3 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv3 = zext i16 %3 to i32
  %4 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv4 = zext i16 %4 to i32
  %cmp5 = icmp sge i32 %conv3, %conv4
  br i1 %cmp5, label %if.then7, label %if.else12

if.then7:                                         ; preds = %if.else
  %5 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv8 = zext i16 %5 to i32
  %6 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv9 = zext i16 %6 to i32
  %sub = sub nsw i32 %conv8, %conv9
  %shl10 = shl i32 %sub, 1
  %conv11 = trunc i32 %shl10 to i16
  store i16 %conv11, i16* %retval, align 2
  br label %return

if.else12:                                        ; preds = %if.else
  %7 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv13 = zext i16 %7 to i32
  %8 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv14 = zext i16 %8 to i32
  %sub15 = sub nsw i32 %conv13, %conv14
  %shl16 = shl i32 %sub15, 1
  %sub17 = sub nsw i32 %shl16, 1
  %conv18 = trunc i32 %sub17 to i16
  store i16 %conv18, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.else12, %if.then7, %if.then
  %9 = load i16, i16* %retval, align 2
  ret i16 %9
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind }
attributes #7 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"short", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
