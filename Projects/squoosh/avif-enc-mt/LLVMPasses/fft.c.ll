; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/fft.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/fft.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @aom_fft_2d_gen(float* %input, float* %temp, float* %output, i32 %n, void (float*, float*, i32)* %tform, void (float*, float*, i32)* %transpose, void (float*, float*, i32)* %unpack, i32 %vec_size) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %tform.addr = alloca void (float*, float*, i32)*, align 4
  %transpose.addr = alloca void (float*, float*, i32)*, align 4
  %unpack.addr = alloca void (float*, float*, i32)*, align 4
  %vec_size.addr = alloca i32, align 4
  %x = alloca i32, align 4
  %x2 = alloca i32, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store void (float*, float*, i32)* %tform, void (float*, float*, i32)** %tform.addr, align 4, !tbaa !2
  store void (float*, float*, i32)* %transpose, void (float*, float*, i32)** %transpose.addr, align 4, !tbaa !2
  store void (float*, float*, i32)* %unpack, void (float*, float*, i32)** %unpack.addr, align 4, !tbaa !2
  store i32 %vec_size, i32* %vec_size.addr, align 4, !tbaa !6
  %0 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %x, align 4, !tbaa !6
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load void (float*, float*, i32)*, void (float*, float*, i32)** %tform.addr, align 4, !tbaa !2
  %5 = load float*, float** %input.addr, align 4, !tbaa !2
  %6 = load i32, i32* %x, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds float, float* %5, i32 %6
  %7 = load float*, float** %output.addr, align 4, !tbaa !2
  %8 = load i32, i32* %x, align 4, !tbaa !6
  %add.ptr1 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %4(float* %add.ptr, float* %add.ptr1, i32 %9)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %vec_size.addr, align 4, !tbaa !6
  %11 = load i32, i32* %x, align 4, !tbaa !6
  %add = add nsw i32 %11, %10
  store i32 %add, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %12 = load void (float*, float*, i32)*, void (float*, float*, i32)** %transpose.addr, align 4, !tbaa !2
  %13 = load float*, float** %output.addr, align 4, !tbaa !2
  %14 = load float*, float** %temp.addr, align 4, !tbaa !2
  %15 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %12(float* %13, float* %14, i32 %15)
  %16 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  store i32 0, i32* %x2, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc9, %for.end
  %17 = load i32, i32* %x2, align 4, !tbaa !6
  %18 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %17, %18
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond3
  %19 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  br label %for.end11

for.body6:                                        ; preds = %for.cond3
  %20 = load void (float*, float*, i32)*, void (float*, float*, i32)** %tform.addr, align 4, !tbaa !2
  %21 = load float*, float** %temp.addr, align 4, !tbaa !2
  %22 = load i32, i32* %x2, align 4, !tbaa !6
  %add.ptr7 = getelementptr inbounds float, float* %21, i32 %22
  %23 = load float*, float** %output.addr, align 4, !tbaa !2
  %24 = load i32, i32* %x2, align 4, !tbaa !6
  %add.ptr8 = getelementptr inbounds float, float* %23, i32 %24
  %25 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %20(float* %add.ptr7, float* %add.ptr8, i32 %25)
  br label %for.inc9

for.inc9:                                         ; preds = %for.body6
  %26 = load i32, i32* %vec_size.addr, align 4, !tbaa !6
  %27 = load i32, i32* %x2, align 4, !tbaa !6
  %add10 = add nsw i32 %27, %26
  store i32 %add10, i32* %x2, align 4, !tbaa !6
  br label %for.cond3

for.end11:                                        ; preds = %for.cond.cleanup5
  %28 = load void (float*, float*, i32)*, void (float*, float*, i32)** %transpose.addr, align 4, !tbaa !2
  %29 = load float*, float** %output.addr, align 4, !tbaa !2
  %30 = load float*, float** %temp.addr, align 4, !tbaa !2
  %31 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %28(float* %29, float* %30, i32 %31)
  %32 = load void (float*, float*, i32)*, void (float*, float*, i32)** %unpack.addr, align 4, !tbaa !2
  %33 = load float*, float** %temp.addr, align 4, !tbaa !2
  %34 = load float*, float** %output.addr, align 4, !tbaa !2
  %35 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %32(float* %33, float* %34, i32 %35)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_fft1d_2_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load float*, float** %input.addr, align 4, !tbaa !2
  %2 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %2
  %add.ptr = getelementptr inbounds float, float* %1, i32 %mul
  %3 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %3, float* %i0, align 4, !tbaa !8
  %4 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load float*, float** %input.addr, align 4, !tbaa !2
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %6
  %add.ptr2 = getelementptr inbounds float, float* %5, i32 %mul1
  %7 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %7, float* %i1, align 4, !tbaa !8
  %8 = load float*, float** %output.addr, align 4, !tbaa !2
  %9 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 0, %9
  %add.ptr4 = getelementptr inbounds float, float* %8, i32 %mul3
  %10 = load float, float* %i0, align 4, !tbaa !8
  %11 = load float, float* %i1, align 4, !tbaa !8
  %add = fadd float %10, %11
  call void @store_float(float* %add.ptr4, float %add)
  %12 = load float*, float** %output.addr, align 4, !tbaa !2
  %13 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 1, %13
  %add.ptr6 = getelementptr inbounds float, float* %12, i32 %mul5
  %14 = load float, float* %i0, align 4, !tbaa !8
  %15 = load float, float* %i1, align 4, !tbaa !8
  %sub = fsub float %14, %15
  call void @store_float(float* %add.ptr6, float %sub)
  %16 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %17 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @store_float(float* %output, float %input) #2 {
entry:
  %output.addr = alloca float*, align 4
  %input.addr = alloca float, align 4
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store float %input, float* %input.addr, align 4, !tbaa !8
  %0 = load float, float* %input.addr, align 4, !tbaa !8
  %1 = load float*, float** %output.addr, align 4, !tbaa !2
  store float %0, float* %1, align 4, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_fft1d_4_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %kWeight0 = alloca float, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  %i2 = alloca float, align 4
  %i3 = alloca float, align 4
  %w0 = alloca float, align 4
  %w1 = alloca float, align 4
  %w2 = alloca float, align 4
  %w3 = alloca float, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store float 0.000000e+00, float* %kWeight0, align 4, !tbaa !8
  %1 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load float*, float** %input.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %3
  %add.ptr = getelementptr inbounds float, float* %2, i32 %mul
  %4 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %4, float* %i0, align 4, !tbaa !8
  %5 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load float*, float** %input.addr, align 4, !tbaa !2
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %7
  %add.ptr2 = getelementptr inbounds float, float* %6, i32 %mul1
  %8 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %8, float* %i1, align 4, !tbaa !8
  %9 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load float*, float** %input.addr, align 4, !tbaa !2
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 2, %11
  %add.ptr4 = getelementptr inbounds float, float* %10, i32 %mul3
  %12 = load float, float* %add.ptr4, align 4, !tbaa !8
  store float %12, float* %i2, align 4, !tbaa !8
  %13 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = load float*, float** %input.addr, align 4, !tbaa !2
  %15 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %15
  %add.ptr6 = getelementptr inbounds float, float* %14, i32 %mul5
  %16 = load float, float* %add.ptr6, align 4, !tbaa !8
  store float %16, float* %i3, align 4, !tbaa !8
  %17 = bitcast float* %w0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load float, float* %i0, align 4, !tbaa !8
  %19 = load float, float* %i2, align 4, !tbaa !8
  %call = call float @add_float(float %18, float %19)
  store float %call, float* %w0, align 4, !tbaa !8
  %20 = bitcast float* %w1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load float, float* %i0, align 4, !tbaa !8
  %22 = load float, float* %i2, align 4, !tbaa !8
  %call7 = call float @sub_float(float %21, float %22)
  store float %call7, float* %w1, align 4, !tbaa !8
  %23 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = load float, float* %i1, align 4, !tbaa !8
  %25 = load float, float* %i3, align 4, !tbaa !8
  %call8 = call float @add_float(float %24, float %25)
  store float %call8, float* %w2, align 4, !tbaa !8
  %26 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load float, float* %i1, align 4, !tbaa !8
  %28 = load float, float* %i3, align 4, !tbaa !8
  %call9 = call float @sub_float(float %27, float %28)
  store float %call9, float* %w3, align 4, !tbaa !8
  %29 = load float*, float** %output.addr, align 4, !tbaa !2
  %30 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 0, %30
  %add.ptr11 = getelementptr inbounds float, float* %29, i32 %mul10
  %31 = load float, float* %w0, align 4, !tbaa !8
  %32 = load float, float* %w2, align 4, !tbaa !8
  %call12 = call float @add_float(float %31, float %32)
  call void @store_float(float* %add.ptr11, float %call12)
  %33 = load float*, float** %output.addr, align 4, !tbaa !2
  %34 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 1, %34
  %add.ptr14 = getelementptr inbounds float, float* %33, i32 %mul13
  %35 = load float, float* %w1, align 4, !tbaa !8
  call void @store_float(float* %add.ptr14, float %35)
  %36 = load float*, float** %output.addr, align 4, !tbaa !2
  %37 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 2, %37
  %add.ptr16 = getelementptr inbounds float, float* %36, i32 %mul15
  %38 = load float, float* %w0, align 4, !tbaa !8
  %39 = load float, float* %w2, align 4, !tbaa !8
  %call17 = call float @sub_float(float %38, float %39)
  call void @store_float(float* %add.ptr16, float %call17)
  %40 = load float*, float** %output.addr, align 4, !tbaa !2
  %41 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 3, %41
  %add.ptr19 = getelementptr inbounds float, float* %40, i32 %mul18
  %42 = load float, float* %w3, align 4, !tbaa !8
  %call20 = call float @sub_float(float 0.000000e+00, float %42)
  call void @store_float(float* %add.ptr19, float %call20)
  %43 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  %44 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  %45 = bitcast float* %w1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  %46 = bitcast float* %w0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  %49 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #3
  %50 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  ret void
}

; Function Attrs: inlinehint nounwind
define internal float @add_float(float %a, float %b) #2 {
entry:
  %a.addr = alloca float, align 4
  %b.addr = alloca float, align 4
  store float %a, float* %a.addr, align 4, !tbaa !8
  store float %b, float* %b.addr, align 4, !tbaa !8
  %0 = load float, float* %a.addr, align 4, !tbaa !8
  %1 = load float, float* %b.addr, align 4, !tbaa !8
  %add = fadd float %0, %1
  ret float %add
}

; Function Attrs: inlinehint nounwind
define internal float @sub_float(float %a, float %b) #2 {
entry:
  %a.addr = alloca float, align 4
  %b.addr = alloca float, align 4
  store float %a, float* %a.addr, align 4, !tbaa !8
  store float %b, float* %b.addr, align 4, !tbaa !8
  %0 = load float, float* %a.addr, align 4, !tbaa !8
  %1 = load float, float* %b.addr, align 4, !tbaa !8
  %sub = fsub float %0, %1
  ret float %sub
}

; Function Attrs: nounwind
define hidden void @aom_fft1d_8_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %kWeight0 = alloca float, align 4
  %kWeight2 = alloca float, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  %i2 = alloca float, align 4
  %i3 = alloca float, align 4
  %i4 = alloca float, align 4
  %i5 = alloca float, align 4
  %i6 = alloca float, align 4
  %i7 = alloca float, align 4
  %w0 = alloca float, align 4
  %w1 = alloca float, align 4
  %w2 = alloca float, align 4
  %w3 = alloca float, align 4
  %w4 = alloca float, align 4
  %w5 = alloca float, align 4
  %w7 = alloca float, align 4
  %w8 = alloca float, align 4
  %w9 = alloca float, align 4
  %w10 = alloca float, align 4
  %w11 = alloca float, align 4
  %w12 = alloca float, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store float 0.000000e+00, float* %kWeight0, align 4, !tbaa !8
  %1 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store float 0x3FE6A09EE0000000, float* %kWeight2, align 4, !tbaa !8
  %2 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load float*, float** %input.addr, align 4, !tbaa !2
  %4 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %4
  %add.ptr = getelementptr inbounds float, float* %3, i32 %mul
  %5 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %5, float* %i0, align 4, !tbaa !8
  %6 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load float*, float** %input.addr, align 4, !tbaa !2
  %8 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %8
  %add.ptr2 = getelementptr inbounds float, float* %7, i32 %mul1
  %9 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %9, float* %i1, align 4, !tbaa !8
  %10 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load float*, float** %input.addr, align 4, !tbaa !2
  %12 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 2, %12
  %add.ptr4 = getelementptr inbounds float, float* %11, i32 %mul3
  %13 = load float, float* %add.ptr4, align 4, !tbaa !8
  store float %13, float* %i2, align 4, !tbaa !8
  %14 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load float*, float** %input.addr, align 4, !tbaa !2
  %16 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %16
  %add.ptr6 = getelementptr inbounds float, float* %15, i32 %mul5
  %17 = load float, float* %add.ptr6, align 4, !tbaa !8
  store float %17, float* %i3, align 4, !tbaa !8
  %18 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = load float*, float** %input.addr, align 4, !tbaa !2
  %20 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 4, %20
  %add.ptr8 = getelementptr inbounds float, float* %19, i32 %mul7
  %21 = load float, float* %add.ptr8, align 4, !tbaa !8
  store float %21, float* %i4, align 4, !tbaa !8
  %22 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = load float*, float** %input.addr, align 4, !tbaa !2
  %24 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul9 = mul nsw i32 5, %24
  %add.ptr10 = getelementptr inbounds float, float* %23, i32 %mul9
  %25 = load float, float* %add.ptr10, align 4, !tbaa !8
  store float %25, float* %i5, align 4, !tbaa !8
  %26 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load float*, float** %input.addr, align 4, !tbaa !2
  %28 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 6, %28
  %add.ptr12 = getelementptr inbounds float, float* %27, i32 %mul11
  %29 = load float, float* %add.ptr12, align 4, !tbaa !8
  store float %29, float* %i6, align 4, !tbaa !8
  %30 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  %31 = load float*, float** %input.addr, align 4, !tbaa !2
  %32 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 7, %32
  %add.ptr14 = getelementptr inbounds float, float* %31, i32 %mul13
  %33 = load float, float* %add.ptr14, align 4, !tbaa !8
  store float %33, float* %i7, align 4, !tbaa !8
  %34 = bitcast float* %w0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #3
  %35 = load float, float* %i0, align 4, !tbaa !8
  %36 = load float, float* %i4, align 4, !tbaa !8
  %call = call float @add_float(float %35, float %36)
  store float %call, float* %w0, align 4, !tbaa !8
  %37 = bitcast float* %w1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #3
  %38 = load float, float* %i0, align 4, !tbaa !8
  %39 = load float, float* %i4, align 4, !tbaa !8
  %call15 = call float @sub_float(float %38, float %39)
  store float %call15, float* %w1, align 4, !tbaa !8
  %40 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load float, float* %i2, align 4, !tbaa !8
  %42 = load float, float* %i6, align 4, !tbaa !8
  %call16 = call float @add_float(float %41, float %42)
  store float %call16, float* %w2, align 4, !tbaa !8
  %43 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #3
  %44 = load float, float* %i2, align 4, !tbaa !8
  %45 = load float, float* %i6, align 4, !tbaa !8
  %call17 = call float @sub_float(float %44, float %45)
  store float %call17, float* %w3, align 4, !tbaa !8
  %46 = bitcast float* %w4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #3
  %47 = load float, float* %w0, align 4, !tbaa !8
  %48 = load float, float* %w2, align 4, !tbaa !8
  %call18 = call float @add_float(float %47, float %48)
  store float %call18, float* %w4, align 4, !tbaa !8
  %49 = bitcast float* %w5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #3
  %50 = load float, float* %w0, align 4, !tbaa !8
  %51 = load float, float* %w2, align 4, !tbaa !8
  %call19 = call float @sub_float(float %50, float %51)
  store float %call19, float* %w5, align 4, !tbaa !8
  %52 = bitcast float* %w7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  %53 = load float, float* %i1, align 4, !tbaa !8
  %54 = load float, float* %i5, align 4, !tbaa !8
  %call20 = call float @add_float(float %53, float %54)
  store float %call20, float* %w7, align 4, !tbaa !8
  %55 = bitcast float* %w8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #3
  %56 = load float, float* %i1, align 4, !tbaa !8
  %57 = load float, float* %i5, align 4, !tbaa !8
  %call21 = call float @sub_float(float %56, float %57)
  store float %call21, float* %w8, align 4, !tbaa !8
  %58 = bitcast float* %w9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #3
  %59 = load float, float* %i3, align 4, !tbaa !8
  %60 = load float, float* %i7, align 4, !tbaa !8
  %call22 = call float @add_float(float %59, float %60)
  store float %call22, float* %w9, align 4, !tbaa !8
  %61 = bitcast float* %w10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #3
  %62 = load float, float* %i3, align 4, !tbaa !8
  %63 = load float, float* %i7, align 4, !tbaa !8
  %call23 = call float @sub_float(float %62, float %63)
  store float %call23, float* %w10, align 4, !tbaa !8
  %64 = bitcast float* %w11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  %65 = load float, float* %w7, align 4, !tbaa !8
  %66 = load float, float* %w9, align 4, !tbaa !8
  %call24 = call float @add_float(float %65, float %66)
  store float %call24, float* %w11, align 4, !tbaa !8
  %67 = bitcast float* %w12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #3
  %68 = load float, float* %w7, align 4, !tbaa !8
  %69 = load float, float* %w9, align 4, !tbaa !8
  %call25 = call float @sub_float(float %68, float %69)
  store float %call25, float* %w12, align 4, !tbaa !8
  %70 = load float*, float** %output.addr, align 4, !tbaa !2
  %71 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 0, %71
  %add.ptr27 = getelementptr inbounds float, float* %70, i32 %mul26
  %72 = load float, float* %w4, align 4, !tbaa !8
  %73 = load float, float* %w11, align 4, !tbaa !8
  %call28 = call float @add_float(float %72, float %73)
  call void @store_float(float* %add.ptr27, float %call28)
  %74 = load float*, float** %output.addr, align 4, !tbaa !2
  %75 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 1, %75
  %add.ptr30 = getelementptr inbounds float, float* %74, i32 %mul29
  %76 = load float, float* %w1, align 4, !tbaa !8
  %77 = load float, float* %w8, align 4, !tbaa !8
  %78 = load float, float* %w10, align 4, !tbaa !8
  %call31 = call float @sub_float(float %77, float %78)
  %call32 = call float @mul_float(float 0x3FE6A09EE0000000, float %call31)
  %call33 = call float @add_float(float %76, float %call32)
  call void @store_float(float* %add.ptr30, float %call33)
  %79 = load float*, float** %output.addr, align 4, !tbaa !2
  %80 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul34 = mul nsw i32 2, %80
  %add.ptr35 = getelementptr inbounds float, float* %79, i32 %mul34
  %81 = load float, float* %w5, align 4, !tbaa !8
  call void @store_float(float* %add.ptr35, float %81)
  %82 = load float*, float** %output.addr, align 4, !tbaa !2
  %83 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul36 = mul nsw i32 3, %83
  %add.ptr37 = getelementptr inbounds float, float* %82, i32 %mul36
  %84 = load float, float* %w1, align 4, !tbaa !8
  %85 = load float, float* %w8, align 4, !tbaa !8
  %86 = load float, float* %w10, align 4, !tbaa !8
  %call38 = call float @sub_float(float %85, float %86)
  %call39 = call float @mul_float(float 0x3FE6A09EE0000000, float %call38)
  %call40 = call float @sub_float(float %84, float %call39)
  call void @store_float(float* %add.ptr37, float %call40)
  %87 = load float*, float** %output.addr, align 4, !tbaa !2
  %88 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul41 = mul nsw i32 4, %88
  %add.ptr42 = getelementptr inbounds float, float* %87, i32 %mul41
  %89 = load float, float* %w4, align 4, !tbaa !8
  %90 = load float, float* %w11, align 4, !tbaa !8
  %call43 = call float @sub_float(float %89, float %90)
  call void @store_float(float* %add.ptr42, float %call43)
  %91 = load float*, float** %output.addr, align 4, !tbaa !2
  %92 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul44 = mul nsw i32 5, %92
  %add.ptr45 = getelementptr inbounds float, float* %91, i32 %mul44
  %93 = load float, float* %w3, align 4, !tbaa !8
  %call46 = call float @sub_float(float 0.000000e+00, float %93)
  %94 = load float, float* %w10, align 4, !tbaa !8
  %95 = load float, float* %w8, align 4, !tbaa !8
  %call47 = call float @add_float(float %94, float %95)
  %call48 = call float @mul_float(float 0x3FE6A09EE0000000, float %call47)
  %call49 = call float @sub_float(float %call46, float %call48)
  call void @store_float(float* %add.ptr45, float %call49)
  %96 = load float*, float** %output.addr, align 4, !tbaa !2
  %97 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul50 = mul nsw i32 6, %97
  %add.ptr51 = getelementptr inbounds float, float* %96, i32 %mul50
  %98 = load float, float* %w12, align 4, !tbaa !8
  %call52 = call float @sub_float(float 0.000000e+00, float %98)
  call void @store_float(float* %add.ptr51, float %call52)
  %99 = load float*, float** %output.addr, align 4, !tbaa !2
  %100 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul53 = mul nsw i32 7, %100
  %add.ptr54 = getelementptr inbounds float, float* %99, i32 %mul53
  %101 = load float, float* %w3, align 4, !tbaa !8
  %102 = load float, float* %w10, align 4, !tbaa !8
  %103 = load float, float* %w8, align 4, !tbaa !8
  %call55 = call float @add_float(float %102, float %103)
  %call56 = call float @mul_float(float 0x3FE6A09EE0000000, float %call55)
  %call57 = call float @sub_float(float %101, float %call56)
  call void @store_float(float* %add.ptr54, float %call57)
  %104 = bitcast float* %w12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #3
  %105 = bitcast float* %w11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #3
  %106 = bitcast float* %w10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #3
  %107 = bitcast float* %w9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  %108 = bitcast float* %w8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast float* %w7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast float* %w5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast float* %w4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %112 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #3
  %113 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #3
  %114 = bitcast float* %w1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #3
  %115 = bitcast float* %w0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #3
  %116 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  %117 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  %119 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #3
  %120 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #3
  %121 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #3
  %122 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #3
  %123 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %125 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  ret void
}

; Function Attrs: inlinehint nounwind
define internal float @mul_float(float %a, float %b) #2 {
entry:
  %a.addr = alloca float, align 4
  %b.addr = alloca float, align 4
  store float %a, float* %a.addr, align 4, !tbaa !8
  store float %b, float* %b.addr, align 4, !tbaa !8
  %0 = load float, float* %a.addr, align 4, !tbaa !8
  %1 = load float, float* %b.addr, align 4, !tbaa !8
  %mul = fmul float %0, %1
  ret float %mul
}

; Function Attrs: nounwind
define hidden void @aom_fft1d_16_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %kWeight0 = alloca float, align 4
  %kWeight2 = alloca float, align 4
  %kWeight3 = alloca float, align 4
  %kWeight4 = alloca float, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  %i2 = alloca float, align 4
  %i3 = alloca float, align 4
  %i4 = alloca float, align 4
  %i5 = alloca float, align 4
  %i6 = alloca float, align 4
  %i7 = alloca float, align 4
  %i8 = alloca float, align 4
  %i9 = alloca float, align 4
  %i10 = alloca float, align 4
  %i11 = alloca float, align 4
  %i12 = alloca float, align 4
  %i13 = alloca float, align 4
  %i14 = alloca float, align 4
  %i15 = alloca float, align 4
  %w0 = alloca float, align 4
  %w1 = alloca float, align 4
  %w2 = alloca float, align 4
  %w3 = alloca float, align 4
  %w4 = alloca float, align 4
  %w5 = alloca float, align 4
  %w7 = alloca float, align 4
  %w8 = alloca float, align 4
  %w9 = alloca float, align 4
  %w10 = alloca float, align 4
  %w11 = alloca float, align 4
  %w12 = alloca float, align 4
  %w14 = alloca float, align 4
  %w15 = alloca float, align 4
  %w16 = alloca [2 x float], align 4
  %w18 = alloca [2 x float], align 4
  %w19 = alloca float, align 4
  %w20 = alloca float, align 4
  %w21 = alloca float, align 4
  %w22 = alloca float, align 4
  %w23 = alloca float, align 4
  %w24 = alloca float, align 4
  %w26 = alloca float, align 4
  %w27 = alloca float, align 4
  %w28 = alloca float, align 4
  %w29 = alloca float, align 4
  %w30 = alloca float, align 4
  %w31 = alloca float, align 4
  %w33 = alloca float, align 4
  %w34 = alloca float, align 4
  %w35 = alloca [2 x float], align 4
  %w37 = alloca [2 x float], align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store float 0.000000e+00, float* %kWeight0, align 4, !tbaa !8
  %1 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store float 0x3FE6A09EE0000000, float* %kWeight2, align 4, !tbaa !8
  %2 = bitcast float* %kWeight3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store float 0x3FED906CC0000000, float* %kWeight3, align 4, !tbaa !8
  %3 = bitcast float* %kWeight4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store float 0x3FD87DE0E0000000, float* %kWeight4, align 4, !tbaa !8
  %4 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load float*, float** %input.addr, align 4, !tbaa !2
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %6
  %add.ptr = getelementptr inbounds float, float* %5, i32 %mul
  %7 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %7, float* %i0, align 4, !tbaa !8
  %8 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load float*, float** %input.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %10
  %add.ptr2 = getelementptr inbounds float, float* %9, i32 %mul1
  %11 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %11, float* %i1, align 4, !tbaa !8
  %12 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load float*, float** %input.addr, align 4, !tbaa !2
  %14 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 2, %14
  %add.ptr4 = getelementptr inbounds float, float* %13, i32 %mul3
  %15 = load float, float* %add.ptr4, align 4, !tbaa !8
  store float %15, float* %i2, align 4, !tbaa !8
  %16 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load float*, float** %input.addr, align 4, !tbaa !2
  %18 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %18
  %add.ptr6 = getelementptr inbounds float, float* %17, i32 %mul5
  %19 = load float, float* %add.ptr6, align 4, !tbaa !8
  store float %19, float* %i3, align 4, !tbaa !8
  %20 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load float*, float** %input.addr, align 4, !tbaa !2
  %22 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 4, %22
  %add.ptr8 = getelementptr inbounds float, float* %21, i32 %mul7
  %23 = load float, float* %add.ptr8, align 4, !tbaa !8
  store float %23, float* %i4, align 4, !tbaa !8
  %24 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load float*, float** %input.addr, align 4, !tbaa !2
  %26 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul9 = mul nsw i32 5, %26
  %add.ptr10 = getelementptr inbounds float, float* %25, i32 %mul9
  %27 = load float, float* %add.ptr10, align 4, !tbaa !8
  store float %27, float* %i5, align 4, !tbaa !8
  %28 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load float*, float** %input.addr, align 4, !tbaa !2
  %30 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 6, %30
  %add.ptr12 = getelementptr inbounds float, float* %29, i32 %mul11
  %31 = load float, float* %add.ptr12, align 4, !tbaa !8
  store float %31, float* %i6, align 4, !tbaa !8
  %32 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load float*, float** %input.addr, align 4, !tbaa !2
  %34 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 7, %34
  %add.ptr14 = getelementptr inbounds float, float* %33, i32 %mul13
  %35 = load float, float* %add.ptr14, align 4, !tbaa !8
  store float %35, float* %i7, align 4, !tbaa !8
  %36 = bitcast float* %i8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #3
  %37 = load float*, float** %input.addr, align 4, !tbaa !2
  %38 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 8, %38
  %add.ptr16 = getelementptr inbounds float, float* %37, i32 %mul15
  %39 = load float, float* %add.ptr16, align 4, !tbaa !8
  store float %39, float* %i8, align 4, !tbaa !8
  %40 = bitcast float* %i9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load float*, float** %input.addr, align 4, !tbaa !2
  %42 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 9, %42
  %add.ptr18 = getelementptr inbounds float, float* %41, i32 %mul17
  %43 = load float, float* %add.ptr18, align 4, !tbaa !8
  store float %43, float* %i9, align 4, !tbaa !8
  %44 = bitcast float* %i10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load float*, float** %input.addr, align 4, !tbaa !2
  %46 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul19 = mul nsw i32 10, %46
  %add.ptr20 = getelementptr inbounds float, float* %45, i32 %mul19
  %47 = load float, float* %add.ptr20, align 4, !tbaa !8
  store float %47, float* %i10, align 4, !tbaa !8
  %48 = bitcast float* %i11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #3
  %49 = load float*, float** %input.addr, align 4, !tbaa !2
  %50 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul21 = mul nsw i32 11, %50
  %add.ptr22 = getelementptr inbounds float, float* %49, i32 %mul21
  %51 = load float, float* %add.ptr22, align 4, !tbaa !8
  store float %51, float* %i11, align 4, !tbaa !8
  %52 = bitcast float* %i12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  %53 = load float*, float** %input.addr, align 4, !tbaa !2
  %54 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 12, %54
  %add.ptr24 = getelementptr inbounds float, float* %53, i32 %mul23
  %55 = load float, float* %add.ptr24, align 4, !tbaa !8
  store float %55, float* %i12, align 4, !tbaa !8
  %56 = bitcast float* %i13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #3
  %57 = load float*, float** %input.addr, align 4, !tbaa !2
  %58 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 13, %58
  %add.ptr26 = getelementptr inbounds float, float* %57, i32 %mul25
  %59 = load float, float* %add.ptr26, align 4, !tbaa !8
  store float %59, float* %i13, align 4, !tbaa !8
  %60 = bitcast float* %i14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #3
  %61 = load float*, float** %input.addr, align 4, !tbaa !2
  %62 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul27 = mul nsw i32 14, %62
  %add.ptr28 = getelementptr inbounds float, float* %61, i32 %mul27
  %63 = load float, float* %add.ptr28, align 4, !tbaa !8
  store float %63, float* %i14, align 4, !tbaa !8
  %64 = bitcast float* %i15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  %65 = load float*, float** %input.addr, align 4, !tbaa !2
  %66 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 15, %66
  %add.ptr30 = getelementptr inbounds float, float* %65, i32 %mul29
  %67 = load float, float* %add.ptr30, align 4, !tbaa !8
  store float %67, float* %i15, align 4, !tbaa !8
  %68 = bitcast float* %w0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #3
  %69 = load float, float* %i0, align 4, !tbaa !8
  %70 = load float, float* %i8, align 4, !tbaa !8
  %call = call float @add_float(float %69, float %70)
  store float %call, float* %w0, align 4, !tbaa !8
  %71 = bitcast float* %w1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #3
  %72 = load float, float* %i0, align 4, !tbaa !8
  %73 = load float, float* %i8, align 4, !tbaa !8
  %call31 = call float @sub_float(float %72, float %73)
  store float %call31, float* %w1, align 4, !tbaa !8
  %74 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #3
  %75 = load float, float* %i4, align 4, !tbaa !8
  %76 = load float, float* %i12, align 4, !tbaa !8
  %call32 = call float @add_float(float %75, float %76)
  store float %call32, float* %w2, align 4, !tbaa !8
  %77 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #3
  %78 = load float, float* %i4, align 4, !tbaa !8
  %79 = load float, float* %i12, align 4, !tbaa !8
  %call33 = call float @sub_float(float %78, float %79)
  store float %call33, float* %w3, align 4, !tbaa !8
  %80 = bitcast float* %w4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #3
  %81 = load float, float* %w0, align 4, !tbaa !8
  %82 = load float, float* %w2, align 4, !tbaa !8
  %call34 = call float @add_float(float %81, float %82)
  store float %call34, float* %w4, align 4, !tbaa !8
  %83 = bitcast float* %w5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #3
  %84 = load float, float* %w0, align 4, !tbaa !8
  %85 = load float, float* %w2, align 4, !tbaa !8
  %call35 = call float @sub_float(float %84, float %85)
  store float %call35, float* %w5, align 4, !tbaa !8
  %86 = bitcast float* %w7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #3
  %87 = load float, float* %i2, align 4, !tbaa !8
  %88 = load float, float* %i10, align 4, !tbaa !8
  %call36 = call float @add_float(float %87, float %88)
  store float %call36, float* %w7, align 4, !tbaa !8
  %89 = bitcast float* %w8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #3
  %90 = load float, float* %i2, align 4, !tbaa !8
  %91 = load float, float* %i10, align 4, !tbaa !8
  %call37 = call float @sub_float(float %90, float %91)
  store float %call37, float* %w8, align 4, !tbaa !8
  %92 = bitcast float* %w9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #3
  %93 = load float, float* %i6, align 4, !tbaa !8
  %94 = load float, float* %i14, align 4, !tbaa !8
  %call38 = call float @add_float(float %93, float %94)
  store float %call38, float* %w9, align 4, !tbaa !8
  %95 = bitcast float* %w10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #3
  %96 = load float, float* %i6, align 4, !tbaa !8
  %97 = load float, float* %i14, align 4, !tbaa !8
  %call39 = call float @sub_float(float %96, float %97)
  store float %call39, float* %w10, align 4, !tbaa !8
  %98 = bitcast float* %w11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #3
  %99 = load float, float* %w7, align 4, !tbaa !8
  %100 = load float, float* %w9, align 4, !tbaa !8
  %call40 = call float @add_float(float %99, float %100)
  store float %call40, float* %w11, align 4, !tbaa !8
  %101 = bitcast float* %w12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #3
  %102 = load float, float* %w7, align 4, !tbaa !8
  %103 = load float, float* %w9, align 4, !tbaa !8
  %call41 = call float @sub_float(float %102, float %103)
  store float %call41, float* %w12, align 4, !tbaa !8
  %104 = bitcast float* %w14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #3
  %105 = load float, float* %w4, align 4, !tbaa !8
  %106 = load float, float* %w11, align 4, !tbaa !8
  %call42 = call float @add_float(float %105, float %106)
  store float %call42, float* %w14, align 4, !tbaa !8
  %107 = bitcast float* %w15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #3
  %108 = load float, float* %w4, align 4, !tbaa !8
  %109 = load float, float* %w11, align 4, !tbaa !8
  %call43 = call float @sub_float(float %108, float %109)
  store float %call43, float* %w15, align 4, !tbaa !8
  %110 = bitcast [2 x float]* %w16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %110) #3
  %arrayinit.begin = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %111 = load float, float* %w1, align 4, !tbaa !8
  %112 = load float, float* %w8, align 4, !tbaa !8
  %113 = load float, float* %w10, align 4, !tbaa !8
  %call44 = call float @sub_float(float %112, float %113)
  %call45 = call float @mul_float(float 0x3FE6A09EE0000000, float %call44)
  %call46 = call float @add_float(float %111, float %call45)
  store float %call46, float* %arrayinit.begin, align 4, !tbaa !8
  %arrayinit.element = getelementptr inbounds float, float* %arrayinit.begin, i32 1
  %114 = load float, float* %w3, align 4, !tbaa !8
  %call47 = call float @sub_float(float 0.000000e+00, float %114)
  %115 = load float, float* %w10, align 4, !tbaa !8
  %116 = load float, float* %w8, align 4, !tbaa !8
  %call48 = call float @add_float(float %115, float %116)
  %call49 = call float @mul_float(float 0x3FE6A09EE0000000, float %call48)
  %call50 = call float @sub_float(float %call47, float %call49)
  store float %call50, float* %arrayinit.element, align 4, !tbaa !8
  %117 = bitcast [2 x float]* %w18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %117) #3
  %arrayinit.begin51 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %118 = load float, float* %w1, align 4, !tbaa !8
  %119 = load float, float* %w8, align 4, !tbaa !8
  %120 = load float, float* %w10, align 4, !tbaa !8
  %call52 = call float @sub_float(float %119, float %120)
  %call53 = call float @mul_float(float 0x3FE6A09EE0000000, float %call52)
  %call54 = call float @sub_float(float %118, float %call53)
  store float %call54, float* %arrayinit.begin51, align 4, !tbaa !8
  %arrayinit.element55 = getelementptr inbounds float, float* %arrayinit.begin51, i32 1
  %121 = load float, float* %w3, align 4, !tbaa !8
  %122 = load float, float* %w10, align 4, !tbaa !8
  %123 = load float, float* %w8, align 4, !tbaa !8
  %call56 = call float @add_float(float %122, float %123)
  %call57 = call float @mul_float(float 0x3FE6A09EE0000000, float %call56)
  %call58 = call float @sub_float(float %121, float %call57)
  store float %call58, float* %arrayinit.element55, align 4, !tbaa !8
  %124 = bitcast float* %w19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #3
  %125 = load float, float* %i1, align 4, !tbaa !8
  %126 = load float, float* %i9, align 4, !tbaa !8
  %call59 = call float @add_float(float %125, float %126)
  store float %call59, float* %w19, align 4, !tbaa !8
  %127 = bitcast float* %w20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %127) #3
  %128 = load float, float* %i1, align 4, !tbaa !8
  %129 = load float, float* %i9, align 4, !tbaa !8
  %call60 = call float @sub_float(float %128, float %129)
  store float %call60, float* %w20, align 4, !tbaa !8
  %130 = bitcast float* %w21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %130) #3
  %131 = load float, float* %i5, align 4, !tbaa !8
  %132 = load float, float* %i13, align 4, !tbaa !8
  %call61 = call float @add_float(float %131, float %132)
  store float %call61, float* %w21, align 4, !tbaa !8
  %133 = bitcast float* %w22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %133) #3
  %134 = load float, float* %i5, align 4, !tbaa !8
  %135 = load float, float* %i13, align 4, !tbaa !8
  %call62 = call float @sub_float(float %134, float %135)
  store float %call62, float* %w22, align 4, !tbaa !8
  %136 = bitcast float* %w23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #3
  %137 = load float, float* %w19, align 4, !tbaa !8
  %138 = load float, float* %w21, align 4, !tbaa !8
  %call63 = call float @add_float(float %137, float %138)
  store float %call63, float* %w23, align 4, !tbaa !8
  %139 = bitcast float* %w24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #3
  %140 = load float, float* %w19, align 4, !tbaa !8
  %141 = load float, float* %w21, align 4, !tbaa !8
  %call64 = call float @sub_float(float %140, float %141)
  store float %call64, float* %w24, align 4, !tbaa !8
  %142 = bitcast float* %w26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #3
  %143 = load float, float* %i3, align 4, !tbaa !8
  %144 = load float, float* %i11, align 4, !tbaa !8
  %call65 = call float @add_float(float %143, float %144)
  store float %call65, float* %w26, align 4, !tbaa !8
  %145 = bitcast float* %w27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #3
  %146 = load float, float* %i3, align 4, !tbaa !8
  %147 = load float, float* %i11, align 4, !tbaa !8
  %call66 = call float @sub_float(float %146, float %147)
  store float %call66, float* %w27, align 4, !tbaa !8
  %148 = bitcast float* %w28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #3
  %149 = load float, float* %i7, align 4, !tbaa !8
  %150 = load float, float* %i15, align 4, !tbaa !8
  %call67 = call float @add_float(float %149, float %150)
  store float %call67, float* %w28, align 4, !tbaa !8
  %151 = bitcast float* %w29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %151) #3
  %152 = load float, float* %i7, align 4, !tbaa !8
  %153 = load float, float* %i15, align 4, !tbaa !8
  %call68 = call float @sub_float(float %152, float %153)
  store float %call68, float* %w29, align 4, !tbaa !8
  %154 = bitcast float* %w30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %154) #3
  %155 = load float, float* %w26, align 4, !tbaa !8
  %156 = load float, float* %w28, align 4, !tbaa !8
  %call69 = call float @add_float(float %155, float %156)
  store float %call69, float* %w30, align 4, !tbaa !8
  %157 = bitcast float* %w31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #3
  %158 = load float, float* %w26, align 4, !tbaa !8
  %159 = load float, float* %w28, align 4, !tbaa !8
  %call70 = call float @sub_float(float %158, float %159)
  store float %call70, float* %w31, align 4, !tbaa !8
  %160 = bitcast float* %w33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #3
  %161 = load float, float* %w23, align 4, !tbaa !8
  %162 = load float, float* %w30, align 4, !tbaa !8
  %call71 = call float @add_float(float %161, float %162)
  store float %call71, float* %w33, align 4, !tbaa !8
  %163 = bitcast float* %w34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %163) #3
  %164 = load float, float* %w23, align 4, !tbaa !8
  %165 = load float, float* %w30, align 4, !tbaa !8
  %call72 = call float @sub_float(float %164, float %165)
  store float %call72, float* %w34, align 4, !tbaa !8
  %166 = bitcast [2 x float]* %w35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %166) #3
  %arrayinit.begin73 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %167 = load float, float* %w20, align 4, !tbaa !8
  %168 = load float, float* %w27, align 4, !tbaa !8
  %169 = load float, float* %w29, align 4, !tbaa !8
  %call74 = call float @sub_float(float %168, float %169)
  %call75 = call float @mul_float(float 0x3FE6A09EE0000000, float %call74)
  %call76 = call float @add_float(float %167, float %call75)
  store float %call76, float* %arrayinit.begin73, align 4, !tbaa !8
  %arrayinit.element77 = getelementptr inbounds float, float* %arrayinit.begin73, i32 1
  %170 = load float, float* %w22, align 4, !tbaa !8
  %call78 = call float @sub_float(float 0.000000e+00, float %170)
  %171 = load float, float* %w29, align 4, !tbaa !8
  %172 = load float, float* %w27, align 4, !tbaa !8
  %call79 = call float @add_float(float %171, float %172)
  %call80 = call float @mul_float(float 0x3FE6A09EE0000000, float %call79)
  %call81 = call float @sub_float(float %call78, float %call80)
  store float %call81, float* %arrayinit.element77, align 4, !tbaa !8
  %173 = bitcast [2 x float]* %w37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %173) #3
  %arrayinit.begin82 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %174 = load float, float* %w20, align 4, !tbaa !8
  %175 = load float, float* %w27, align 4, !tbaa !8
  %176 = load float, float* %w29, align 4, !tbaa !8
  %call83 = call float @sub_float(float %175, float %176)
  %call84 = call float @mul_float(float 0x3FE6A09EE0000000, float %call83)
  %call85 = call float @sub_float(float %174, float %call84)
  store float %call85, float* %arrayinit.begin82, align 4, !tbaa !8
  %arrayinit.element86 = getelementptr inbounds float, float* %arrayinit.begin82, i32 1
  %177 = load float, float* %w22, align 4, !tbaa !8
  %178 = load float, float* %w29, align 4, !tbaa !8
  %179 = load float, float* %w27, align 4, !tbaa !8
  %call87 = call float @add_float(float %178, float %179)
  %call88 = call float @mul_float(float 0x3FE6A09EE0000000, float %call87)
  %call89 = call float @sub_float(float %177, float %call88)
  store float %call89, float* %arrayinit.element86, align 4, !tbaa !8
  %180 = load float*, float** %output.addr, align 4, !tbaa !2
  %181 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul90 = mul nsw i32 0, %181
  %add.ptr91 = getelementptr inbounds float, float* %180, i32 %mul90
  %182 = load float, float* %w14, align 4, !tbaa !8
  %183 = load float, float* %w33, align 4, !tbaa !8
  %call92 = call float @add_float(float %182, float %183)
  call void @store_float(float* %add.ptr91, float %call92)
  %184 = load float*, float** %output.addr, align 4, !tbaa !2
  %185 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul93 = mul nsw i32 1, %185
  %add.ptr94 = getelementptr inbounds float, float* %184, i32 %mul93
  %arrayidx = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %186 = load float, float* %arrayidx, align 4, !tbaa !8
  %arrayidx95 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %187 = load float, float* %arrayidx95, align 4, !tbaa !8
  %call96 = call float @mul_float(float 0x3FED906CC0000000, float %187)
  %arrayidx97 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %188 = load float, float* %arrayidx97, align 4, !tbaa !8
  %call98 = call float @mul_float(float 0x3FD87DE0E0000000, float %188)
  %call99 = call float @add_float(float %call96, float %call98)
  %call100 = call float @add_float(float %186, float %call99)
  call void @store_float(float* %add.ptr94, float %call100)
  %189 = load float*, float** %output.addr, align 4, !tbaa !2
  %190 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul101 = mul nsw i32 2, %190
  %add.ptr102 = getelementptr inbounds float, float* %189, i32 %mul101
  %191 = load float, float* %w5, align 4, !tbaa !8
  %192 = load float, float* %w24, align 4, !tbaa !8
  %193 = load float, float* %w31, align 4, !tbaa !8
  %call103 = call float @sub_float(float %192, float %193)
  %call104 = call float @mul_float(float 0x3FE6A09EE0000000, float %call103)
  %call105 = call float @add_float(float %191, float %call104)
  call void @store_float(float* %add.ptr102, float %call105)
  %194 = load float*, float** %output.addr, align 4, !tbaa !2
  %195 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul106 = mul nsw i32 3, %195
  %add.ptr107 = getelementptr inbounds float, float* %194, i32 %mul106
  %arrayidx108 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %196 = load float, float* %arrayidx108, align 4, !tbaa !8
  %arrayidx109 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %197 = load float, float* %arrayidx109, align 4, !tbaa !8
  %call110 = call float @mul_float(float 0x3FD87DE0E0000000, float %197)
  %arrayidx111 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %198 = load float, float* %arrayidx111, align 4, !tbaa !8
  %call112 = call float @mul_float(float 0x3FED906CC0000000, float %198)
  %call113 = call float @add_float(float %call110, float %call112)
  %call114 = call float @add_float(float %196, float %call113)
  call void @store_float(float* %add.ptr107, float %call114)
  %199 = load float*, float** %output.addr, align 4, !tbaa !2
  %200 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul115 = mul nsw i32 4, %200
  %add.ptr116 = getelementptr inbounds float, float* %199, i32 %mul115
  %201 = load float, float* %w15, align 4, !tbaa !8
  call void @store_float(float* %add.ptr116, float %201)
  %202 = load float*, float** %output.addr, align 4, !tbaa !2
  %203 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul117 = mul nsw i32 5, %203
  %add.ptr118 = getelementptr inbounds float, float* %202, i32 %mul117
  %arrayidx119 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %204 = load float, float* %arrayidx119, align 4, !tbaa !8
  %arrayidx120 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %205 = load float, float* %arrayidx120, align 4, !tbaa !8
  %call121 = call float @mul_float(float 0x3FD87DE0E0000000, float %205)
  %call122 = call float @sub_float(float 0.000000e+00, float %call121)
  %arrayidx123 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %206 = load float, float* %arrayidx123, align 4, !tbaa !8
  %call124 = call float @mul_float(float 0x3FED906CC0000000, float %206)
  %call125 = call float @sub_float(float %call122, float %call124)
  %call126 = call float @add_float(float %204, float %call125)
  call void @store_float(float* %add.ptr118, float %call126)
  %207 = load float*, float** %output.addr, align 4, !tbaa !2
  %208 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul127 = mul nsw i32 6, %208
  %add.ptr128 = getelementptr inbounds float, float* %207, i32 %mul127
  %209 = load float, float* %w5, align 4, !tbaa !8
  %210 = load float, float* %w24, align 4, !tbaa !8
  %211 = load float, float* %w31, align 4, !tbaa !8
  %call129 = call float @sub_float(float %210, float %211)
  %call130 = call float @mul_float(float 0x3FE6A09EE0000000, float %call129)
  %call131 = call float @sub_float(float %209, float %call130)
  call void @store_float(float* %add.ptr128, float %call131)
  %212 = load float*, float** %output.addr, align 4, !tbaa !2
  %213 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul132 = mul nsw i32 7, %213
  %add.ptr133 = getelementptr inbounds float, float* %212, i32 %mul132
  %arrayidx134 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %214 = load float, float* %arrayidx134, align 4, !tbaa !8
  %arrayidx135 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %215 = load float, float* %arrayidx135, align 4, !tbaa !8
  %call136 = call float @mul_float(float 0x3FED906CC0000000, float %215)
  %call137 = call float @sub_float(float 0.000000e+00, float %call136)
  %arrayidx138 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %216 = load float, float* %arrayidx138, align 4, !tbaa !8
  %call139 = call float @mul_float(float 0x3FD87DE0E0000000, float %216)
  %call140 = call float @sub_float(float %call137, float %call139)
  %call141 = call float @add_float(float %214, float %call140)
  call void @store_float(float* %add.ptr133, float %call141)
  %217 = load float*, float** %output.addr, align 4, !tbaa !2
  %218 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul142 = mul nsw i32 8, %218
  %add.ptr143 = getelementptr inbounds float, float* %217, i32 %mul142
  %219 = load float, float* %w14, align 4, !tbaa !8
  %220 = load float, float* %w33, align 4, !tbaa !8
  %call144 = call float @sub_float(float %219, float %220)
  call void @store_float(float* %add.ptr143, float %call144)
  %221 = load float*, float** %output.addr, align 4, !tbaa !2
  %222 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul145 = mul nsw i32 9, %222
  %add.ptr146 = getelementptr inbounds float, float* %221, i32 %mul145
  %arrayidx147 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 1
  %223 = load float, float* %arrayidx147, align 4, !tbaa !8
  %arrayidx148 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %224 = load float, float* %arrayidx148, align 4, !tbaa !8
  %call149 = call float @mul_float(float 0x3FED906CC0000000, float %224)
  %arrayidx150 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %225 = load float, float* %arrayidx150, align 4, !tbaa !8
  %call151 = call float @mul_float(float 0x3FD87DE0E0000000, float %225)
  %call152 = call float @sub_float(float %call149, float %call151)
  %call153 = call float @add_float(float %223, float %call152)
  call void @store_float(float* %add.ptr146, float %call153)
  %226 = load float*, float** %output.addr, align 4, !tbaa !2
  %227 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul154 = mul nsw i32 10, %227
  %add.ptr155 = getelementptr inbounds float, float* %226, i32 %mul154
  %228 = load float, float* %w12, align 4, !tbaa !8
  %call156 = call float @sub_float(float 0.000000e+00, float %228)
  %229 = load float, float* %w31, align 4, !tbaa !8
  %230 = load float, float* %w24, align 4, !tbaa !8
  %call157 = call float @add_float(float %229, float %230)
  %call158 = call float @mul_float(float 0x3FE6A09EE0000000, float %call157)
  %call159 = call float @sub_float(float %call156, float %call158)
  call void @store_float(float* %add.ptr155, float %call159)
  %231 = load float*, float** %output.addr, align 4, !tbaa !2
  %232 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul160 = mul nsw i32 11, %232
  %add.ptr161 = getelementptr inbounds float, float* %231, i32 %mul160
  %arrayidx162 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 1
  %233 = load float, float* %arrayidx162, align 4, !tbaa !8
  %arrayidx163 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %234 = load float, float* %arrayidx163, align 4, !tbaa !8
  %call164 = call float @mul_float(float 0x3FD87DE0E0000000, float %234)
  %arrayidx165 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %235 = load float, float* %arrayidx165, align 4, !tbaa !8
  %call166 = call float @mul_float(float 0x3FED906CC0000000, float %235)
  %call167 = call float @sub_float(float %call164, float %call166)
  %call168 = call float @add_float(float %233, float %call167)
  call void @store_float(float* %add.ptr161, float %call168)
  %236 = load float*, float** %output.addr, align 4, !tbaa !2
  %237 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul169 = mul nsw i32 12, %237
  %add.ptr170 = getelementptr inbounds float, float* %236, i32 %mul169
  %238 = load float, float* %w34, align 4, !tbaa !8
  %call171 = call float @sub_float(float 0.000000e+00, float %238)
  call void @store_float(float* %add.ptr170, float %call171)
  %239 = load float*, float** %output.addr, align 4, !tbaa !2
  %240 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul172 = mul nsw i32 13, %240
  %add.ptr173 = getelementptr inbounds float, float* %239, i32 %mul172
  %arrayidx174 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 1
  %241 = load float, float* %arrayidx174, align 4, !tbaa !8
  %call175 = call float @sub_float(float 0.000000e+00, float %241)
  %arrayidx176 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %242 = load float, float* %arrayidx176, align 4, !tbaa !8
  %call177 = call float @mul_float(float 0x3FED906CC0000000, float %242)
  %arrayidx178 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %243 = load float, float* %arrayidx178, align 4, !tbaa !8
  %call179 = call float @mul_float(float 0x3FD87DE0E0000000, float %243)
  %call180 = call float @sub_float(float %call177, float %call179)
  %call181 = call float @sub_float(float %call175, float %call180)
  call void @store_float(float* %add.ptr173, float %call181)
  %244 = load float*, float** %output.addr, align 4, !tbaa !2
  %245 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul182 = mul nsw i32 14, %245
  %add.ptr183 = getelementptr inbounds float, float* %244, i32 %mul182
  %246 = load float, float* %w12, align 4, !tbaa !8
  %247 = load float, float* %w31, align 4, !tbaa !8
  %248 = load float, float* %w24, align 4, !tbaa !8
  %call184 = call float @add_float(float %247, float %248)
  %call185 = call float @mul_float(float 0x3FE6A09EE0000000, float %call184)
  %call186 = call float @sub_float(float %246, float %call185)
  call void @store_float(float* %add.ptr183, float %call186)
  %249 = load float*, float** %output.addr, align 4, !tbaa !2
  %250 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul187 = mul nsw i32 15, %250
  %add.ptr188 = getelementptr inbounds float, float* %249, i32 %mul187
  %arrayidx189 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 1
  %251 = load float, float* %arrayidx189, align 4, !tbaa !8
  %call190 = call float @sub_float(float 0.000000e+00, float %251)
  %arrayidx191 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %252 = load float, float* %arrayidx191, align 4, !tbaa !8
  %call192 = call float @mul_float(float 0x3FD87DE0E0000000, float %252)
  %arrayidx193 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %253 = load float, float* %arrayidx193, align 4, !tbaa !8
  %call194 = call float @mul_float(float 0x3FED906CC0000000, float %253)
  %call195 = call float @sub_float(float %call192, float %call194)
  %call196 = call float @sub_float(float %call190, float %call195)
  call void @store_float(float* %add.ptr188, float %call196)
  %254 = bitcast [2 x float]* %w37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %254) #3
  %255 = bitcast [2 x float]* %w35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %255) #3
  %256 = bitcast float* %w34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #3
  %257 = bitcast float* %w33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #3
  %258 = bitcast float* %w31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #3
  %259 = bitcast float* %w30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #3
  %260 = bitcast float* %w29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #3
  %261 = bitcast float* %w28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #3
  %262 = bitcast float* %w27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #3
  %263 = bitcast float* %w26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #3
  %264 = bitcast float* %w24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #3
  %265 = bitcast float* %w23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #3
  %266 = bitcast float* %w22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #3
  %267 = bitcast float* %w21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #3
  %268 = bitcast float* %w20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #3
  %269 = bitcast float* %w19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #3
  %270 = bitcast [2 x float]* %w18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %270) #3
  %271 = bitcast [2 x float]* %w16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %271) #3
  %272 = bitcast float* %w15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #3
  %273 = bitcast float* %w14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #3
  %274 = bitcast float* %w12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #3
  %275 = bitcast float* %w11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #3
  %276 = bitcast float* %w10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #3
  %277 = bitcast float* %w9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #3
  %278 = bitcast float* %w8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #3
  %279 = bitcast float* %w7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #3
  %280 = bitcast float* %w5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #3
  %281 = bitcast float* %w4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #3
  %282 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #3
  %283 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %283) #3
  %284 = bitcast float* %w1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #3
  %285 = bitcast float* %w0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %285) #3
  %286 = bitcast float* %i15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %286) #3
  %287 = bitcast float* %i14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #3
  %288 = bitcast float* %i13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %288) #3
  %289 = bitcast float* %i12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %289) #3
  %290 = bitcast float* %i11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %290) #3
  %291 = bitcast float* %i10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %291) #3
  %292 = bitcast float* %i9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %292) #3
  %293 = bitcast float* %i8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #3
  %294 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %294) #3
  %295 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %295) #3
  %296 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #3
  %297 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %297) #3
  %298 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #3
  %299 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %299) #3
  %300 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %300) #3
  %301 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %301) #3
  %302 = bitcast float* %kWeight4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %302) #3
  %303 = bitcast float* %kWeight3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %303) #3
  %304 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %304) #3
  %305 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %305) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_fft1d_32_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %kWeight0 = alloca float, align 4
  %kWeight2 = alloca float, align 4
  %kWeight3 = alloca float, align 4
  %kWeight4 = alloca float, align 4
  %kWeight5 = alloca float, align 4
  %kWeight6 = alloca float, align 4
  %kWeight7 = alloca float, align 4
  %kWeight8 = alloca float, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  %i2 = alloca float, align 4
  %i3 = alloca float, align 4
  %i4 = alloca float, align 4
  %i5 = alloca float, align 4
  %i6 = alloca float, align 4
  %i7 = alloca float, align 4
  %i8 = alloca float, align 4
  %i9 = alloca float, align 4
  %i10 = alloca float, align 4
  %i11 = alloca float, align 4
  %i12 = alloca float, align 4
  %i13 = alloca float, align 4
  %i14 = alloca float, align 4
  %i15 = alloca float, align 4
  %i16 = alloca float, align 4
  %i17 = alloca float, align 4
  %i18 = alloca float, align 4
  %i19 = alloca float, align 4
  %i20 = alloca float, align 4
  %i21 = alloca float, align 4
  %i22 = alloca float, align 4
  %i23 = alloca float, align 4
  %i24 = alloca float, align 4
  %i25 = alloca float, align 4
  %i26 = alloca float, align 4
  %i27 = alloca float, align 4
  %i28 = alloca float, align 4
  %i29 = alloca float, align 4
  %i30 = alloca float, align 4
  %i31 = alloca float, align 4
  %w0 = alloca float, align 4
  %w1 = alloca float, align 4
  %w2 = alloca float, align 4
  %w3 = alloca float, align 4
  %w4 = alloca float, align 4
  %w5 = alloca float, align 4
  %w7 = alloca float, align 4
  %w8 = alloca float, align 4
  %w9 = alloca float, align 4
  %w10 = alloca float, align 4
  %w11 = alloca float, align 4
  %w12 = alloca float, align 4
  %w14 = alloca float, align 4
  %w15 = alloca float, align 4
  %w16 = alloca [2 x float], align 4
  %w18 = alloca [2 x float], align 4
  %w19 = alloca float, align 4
  %w20 = alloca float, align 4
  %w21 = alloca float, align 4
  %w22 = alloca float, align 4
  %w23 = alloca float, align 4
  %w24 = alloca float, align 4
  %w26 = alloca float, align 4
  %w27 = alloca float, align 4
  %w28 = alloca float, align 4
  %w29 = alloca float, align 4
  %w30 = alloca float, align 4
  %w31 = alloca float, align 4
  %w33 = alloca float, align 4
  %w34 = alloca float, align 4
  %w35 = alloca [2 x float], align 4
  %w37 = alloca [2 x float], align 4
  %w38 = alloca float, align 4
  %w39 = alloca float, align 4
  %w40 = alloca [2 x float], align 4
  %w41 = alloca [2 x float], align 4
  %w42 = alloca [2 x float], align 4
  %w44 = alloca [2 x float], align 4
  %w45 = alloca [2 x float], align 4
  %w46 = alloca [2 x float], align 4
  %w47 = alloca float, align 4
  %w48 = alloca float, align 4
  %w49 = alloca float, align 4
  %w50 = alloca float, align 4
  %w51 = alloca float, align 4
  %w52 = alloca float, align 4
  %w54 = alloca float, align 4
  %w55 = alloca float, align 4
  %w56 = alloca float, align 4
  %w57 = alloca float, align 4
  %w58 = alloca float, align 4
  %w59 = alloca float, align 4
  %w61 = alloca float, align 4
  %w62 = alloca float, align 4
  %w63 = alloca [2 x float], align 4
  %w65 = alloca [2 x float], align 4
  %w66 = alloca float, align 4
  %w67 = alloca float, align 4
  %w68 = alloca float, align 4
  %w69 = alloca float, align 4
  %w70 = alloca float, align 4
  %w71 = alloca float, align 4
  %w73 = alloca float, align 4
  %w74 = alloca float, align 4
  %w75 = alloca float, align 4
  %w76 = alloca float, align 4
  %w77 = alloca float, align 4
  %w78 = alloca float, align 4
  %w80 = alloca float, align 4
  %w81 = alloca float, align 4
  %w82 = alloca [2 x float], align 4
  %w84 = alloca [2 x float], align 4
  %w85 = alloca float, align 4
  %w86 = alloca float, align 4
  %w87 = alloca [2 x float], align 4
  %w88 = alloca [2 x float], align 4
  %w89 = alloca [2 x float], align 4
  %w91 = alloca [2 x float], align 4
  %w92 = alloca [2 x float], align 4
  %w93 = alloca [2 x float], align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store float 0.000000e+00, float* %kWeight0, align 4, !tbaa !8
  %1 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store float 0x3FE6A09EE0000000, float* %kWeight2, align 4, !tbaa !8
  %2 = bitcast float* %kWeight3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store float 0x3FED906CC0000000, float* %kWeight3, align 4, !tbaa !8
  %3 = bitcast float* %kWeight4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store float 0x3FD87DE0E0000000, float* %kWeight4, align 4, !tbaa !8
  %4 = bitcast float* %kWeight5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  store float 0x3FEF629740000000, float* %kWeight5, align 4, !tbaa !8
  %5 = bitcast float* %kWeight6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  store float 0x3FC8F8B580000000, float* %kWeight6, align 4, !tbaa !8
  %6 = bitcast float* %kWeight7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  store float 0x3FEA9B6700000000, float* %kWeight7, align 4, !tbaa !8
  %7 = bitcast float* %kWeight8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  store float 0x3FE1C73AC0000000, float* %kWeight8, align 4, !tbaa !8
  %8 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load float*, float** %input.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %10
  %add.ptr = getelementptr inbounds float, float* %9, i32 %mul
  %11 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %11, float* %i0, align 4, !tbaa !8
  %12 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load float*, float** %input.addr, align 4, !tbaa !2
  %14 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %14
  %add.ptr2 = getelementptr inbounds float, float* %13, i32 %mul1
  %15 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %15, float* %i1, align 4, !tbaa !8
  %16 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load float*, float** %input.addr, align 4, !tbaa !2
  %18 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 2, %18
  %add.ptr4 = getelementptr inbounds float, float* %17, i32 %mul3
  %19 = load float, float* %add.ptr4, align 4, !tbaa !8
  store float %19, float* %i2, align 4, !tbaa !8
  %20 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load float*, float** %input.addr, align 4, !tbaa !2
  %22 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %22
  %add.ptr6 = getelementptr inbounds float, float* %21, i32 %mul5
  %23 = load float, float* %add.ptr6, align 4, !tbaa !8
  store float %23, float* %i3, align 4, !tbaa !8
  %24 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load float*, float** %input.addr, align 4, !tbaa !2
  %26 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 4, %26
  %add.ptr8 = getelementptr inbounds float, float* %25, i32 %mul7
  %27 = load float, float* %add.ptr8, align 4, !tbaa !8
  store float %27, float* %i4, align 4, !tbaa !8
  %28 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load float*, float** %input.addr, align 4, !tbaa !2
  %30 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul9 = mul nsw i32 5, %30
  %add.ptr10 = getelementptr inbounds float, float* %29, i32 %mul9
  %31 = load float, float* %add.ptr10, align 4, !tbaa !8
  store float %31, float* %i5, align 4, !tbaa !8
  %32 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load float*, float** %input.addr, align 4, !tbaa !2
  %34 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 6, %34
  %add.ptr12 = getelementptr inbounds float, float* %33, i32 %mul11
  %35 = load float, float* %add.ptr12, align 4, !tbaa !8
  store float %35, float* %i6, align 4, !tbaa !8
  %36 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #3
  %37 = load float*, float** %input.addr, align 4, !tbaa !2
  %38 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 7, %38
  %add.ptr14 = getelementptr inbounds float, float* %37, i32 %mul13
  %39 = load float, float* %add.ptr14, align 4, !tbaa !8
  store float %39, float* %i7, align 4, !tbaa !8
  %40 = bitcast float* %i8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load float*, float** %input.addr, align 4, !tbaa !2
  %42 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 8, %42
  %add.ptr16 = getelementptr inbounds float, float* %41, i32 %mul15
  %43 = load float, float* %add.ptr16, align 4, !tbaa !8
  store float %43, float* %i8, align 4, !tbaa !8
  %44 = bitcast float* %i9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load float*, float** %input.addr, align 4, !tbaa !2
  %46 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 9, %46
  %add.ptr18 = getelementptr inbounds float, float* %45, i32 %mul17
  %47 = load float, float* %add.ptr18, align 4, !tbaa !8
  store float %47, float* %i9, align 4, !tbaa !8
  %48 = bitcast float* %i10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #3
  %49 = load float*, float** %input.addr, align 4, !tbaa !2
  %50 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul19 = mul nsw i32 10, %50
  %add.ptr20 = getelementptr inbounds float, float* %49, i32 %mul19
  %51 = load float, float* %add.ptr20, align 4, !tbaa !8
  store float %51, float* %i10, align 4, !tbaa !8
  %52 = bitcast float* %i11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  %53 = load float*, float** %input.addr, align 4, !tbaa !2
  %54 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul21 = mul nsw i32 11, %54
  %add.ptr22 = getelementptr inbounds float, float* %53, i32 %mul21
  %55 = load float, float* %add.ptr22, align 4, !tbaa !8
  store float %55, float* %i11, align 4, !tbaa !8
  %56 = bitcast float* %i12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #3
  %57 = load float*, float** %input.addr, align 4, !tbaa !2
  %58 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 12, %58
  %add.ptr24 = getelementptr inbounds float, float* %57, i32 %mul23
  %59 = load float, float* %add.ptr24, align 4, !tbaa !8
  store float %59, float* %i12, align 4, !tbaa !8
  %60 = bitcast float* %i13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #3
  %61 = load float*, float** %input.addr, align 4, !tbaa !2
  %62 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 13, %62
  %add.ptr26 = getelementptr inbounds float, float* %61, i32 %mul25
  %63 = load float, float* %add.ptr26, align 4, !tbaa !8
  store float %63, float* %i13, align 4, !tbaa !8
  %64 = bitcast float* %i14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  %65 = load float*, float** %input.addr, align 4, !tbaa !2
  %66 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul27 = mul nsw i32 14, %66
  %add.ptr28 = getelementptr inbounds float, float* %65, i32 %mul27
  %67 = load float, float* %add.ptr28, align 4, !tbaa !8
  store float %67, float* %i14, align 4, !tbaa !8
  %68 = bitcast float* %i15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #3
  %69 = load float*, float** %input.addr, align 4, !tbaa !2
  %70 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 15, %70
  %add.ptr30 = getelementptr inbounds float, float* %69, i32 %mul29
  %71 = load float, float* %add.ptr30, align 4, !tbaa !8
  store float %71, float* %i15, align 4, !tbaa !8
  %72 = bitcast float* %i16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #3
  %73 = load float*, float** %input.addr, align 4, !tbaa !2
  %74 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul31 = mul nsw i32 16, %74
  %add.ptr32 = getelementptr inbounds float, float* %73, i32 %mul31
  %75 = load float, float* %add.ptr32, align 4, !tbaa !8
  store float %75, float* %i16, align 4, !tbaa !8
  %76 = bitcast float* %i17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #3
  %77 = load float*, float** %input.addr, align 4, !tbaa !2
  %78 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul33 = mul nsw i32 17, %78
  %add.ptr34 = getelementptr inbounds float, float* %77, i32 %mul33
  %79 = load float, float* %add.ptr34, align 4, !tbaa !8
  store float %79, float* %i17, align 4, !tbaa !8
  %80 = bitcast float* %i18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #3
  %81 = load float*, float** %input.addr, align 4, !tbaa !2
  %82 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul35 = mul nsw i32 18, %82
  %add.ptr36 = getelementptr inbounds float, float* %81, i32 %mul35
  %83 = load float, float* %add.ptr36, align 4, !tbaa !8
  store float %83, float* %i18, align 4, !tbaa !8
  %84 = bitcast float* %i19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #3
  %85 = load float*, float** %input.addr, align 4, !tbaa !2
  %86 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul37 = mul nsw i32 19, %86
  %add.ptr38 = getelementptr inbounds float, float* %85, i32 %mul37
  %87 = load float, float* %add.ptr38, align 4, !tbaa !8
  store float %87, float* %i19, align 4, !tbaa !8
  %88 = bitcast float* %i20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #3
  %89 = load float*, float** %input.addr, align 4, !tbaa !2
  %90 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul39 = mul nsw i32 20, %90
  %add.ptr40 = getelementptr inbounds float, float* %89, i32 %mul39
  %91 = load float, float* %add.ptr40, align 4, !tbaa !8
  store float %91, float* %i20, align 4, !tbaa !8
  %92 = bitcast float* %i21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #3
  %93 = load float*, float** %input.addr, align 4, !tbaa !2
  %94 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul41 = mul nsw i32 21, %94
  %add.ptr42 = getelementptr inbounds float, float* %93, i32 %mul41
  %95 = load float, float* %add.ptr42, align 4, !tbaa !8
  store float %95, float* %i21, align 4, !tbaa !8
  %96 = bitcast float* %i22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #3
  %97 = load float*, float** %input.addr, align 4, !tbaa !2
  %98 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul43 = mul nsw i32 22, %98
  %add.ptr44 = getelementptr inbounds float, float* %97, i32 %mul43
  %99 = load float, float* %add.ptr44, align 4, !tbaa !8
  store float %99, float* %i22, align 4, !tbaa !8
  %100 = bitcast float* %i23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #3
  %101 = load float*, float** %input.addr, align 4, !tbaa !2
  %102 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul45 = mul nsw i32 23, %102
  %add.ptr46 = getelementptr inbounds float, float* %101, i32 %mul45
  %103 = load float, float* %add.ptr46, align 4, !tbaa !8
  store float %103, float* %i23, align 4, !tbaa !8
  %104 = bitcast float* %i24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #3
  %105 = load float*, float** %input.addr, align 4, !tbaa !2
  %106 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul47 = mul nsw i32 24, %106
  %add.ptr48 = getelementptr inbounds float, float* %105, i32 %mul47
  %107 = load float, float* %add.ptr48, align 4, !tbaa !8
  store float %107, float* %i24, align 4, !tbaa !8
  %108 = bitcast float* %i25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #3
  %109 = load float*, float** %input.addr, align 4, !tbaa !2
  %110 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul49 = mul nsw i32 25, %110
  %add.ptr50 = getelementptr inbounds float, float* %109, i32 %mul49
  %111 = load float, float* %add.ptr50, align 4, !tbaa !8
  store float %111, float* %i25, align 4, !tbaa !8
  %112 = bitcast float* %i26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #3
  %113 = load float*, float** %input.addr, align 4, !tbaa !2
  %114 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul51 = mul nsw i32 26, %114
  %add.ptr52 = getelementptr inbounds float, float* %113, i32 %mul51
  %115 = load float, float* %add.ptr52, align 4, !tbaa !8
  store float %115, float* %i26, align 4, !tbaa !8
  %116 = bitcast float* %i27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #3
  %117 = load float*, float** %input.addr, align 4, !tbaa !2
  %118 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul53 = mul nsw i32 27, %118
  %add.ptr54 = getelementptr inbounds float, float* %117, i32 %mul53
  %119 = load float, float* %add.ptr54, align 4, !tbaa !8
  store float %119, float* %i27, align 4, !tbaa !8
  %120 = bitcast float* %i28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #3
  %121 = load float*, float** %input.addr, align 4, !tbaa !2
  %122 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul55 = mul nsw i32 28, %122
  %add.ptr56 = getelementptr inbounds float, float* %121, i32 %mul55
  %123 = load float, float* %add.ptr56, align 4, !tbaa !8
  store float %123, float* %i28, align 4, !tbaa !8
  %124 = bitcast float* %i29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #3
  %125 = load float*, float** %input.addr, align 4, !tbaa !2
  %126 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul57 = mul nsw i32 29, %126
  %add.ptr58 = getelementptr inbounds float, float* %125, i32 %mul57
  %127 = load float, float* %add.ptr58, align 4, !tbaa !8
  store float %127, float* %i29, align 4, !tbaa !8
  %128 = bitcast float* %i30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #3
  %129 = load float*, float** %input.addr, align 4, !tbaa !2
  %130 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul59 = mul nsw i32 30, %130
  %add.ptr60 = getelementptr inbounds float, float* %129, i32 %mul59
  %131 = load float, float* %add.ptr60, align 4, !tbaa !8
  store float %131, float* %i30, align 4, !tbaa !8
  %132 = bitcast float* %i31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #3
  %133 = load float*, float** %input.addr, align 4, !tbaa !2
  %134 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul61 = mul nsw i32 31, %134
  %add.ptr62 = getelementptr inbounds float, float* %133, i32 %mul61
  %135 = load float, float* %add.ptr62, align 4, !tbaa !8
  store float %135, float* %i31, align 4, !tbaa !8
  %136 = bitcast float* %w0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #3
  %137 = load float, float* %i0, align 4, !tbaa !8
  %138 = load float, float* %i16, align 4, !tbaa !8
  %call = call float @add_float(float %137, float %138)
  store float %call, float* %w0, align 4, !tbaa !8
  %139 = bitcast float* %w1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #3
  %140 = load float, float* %i0, align 4, !tbaa !8
  %141 = load float, float* %i16, align 4, !tbaa !8
  %call63 = call float @sub_float(float %140, float %141)
  store float %call63, float* %w1, align 4, !tbaa !8
  %142 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #3
  %143 = load float, float* %i8, align 4, !tbaa !8
  %144 = load float, float* %i24, align 4, !tbaa !8
  %call64 = call float @add_float(float %143, float %144)
  store float %call64, float* %w2, align 4, !tbaa !8
  %145 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #3
  %146 = load float, float* %i8, align 4, !tbaa !8
  %147 = load float, float* %i24, align 4, !tbaa !8
  %call65 = call float @sub_float(float %146, float %147)
  store float %call65, float* %w3, align 4, !tbaa !8
  %148 = bitcast float* %w4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #3
  %149 = load float, float* %w0, align 4, !tbaa !8
  %150 = load float, float* %w2, align 4, !tbaa !8
  %call66 = call float @add_float(float %149, float %150)
  store float %call66, float* %w4, align 4, !tbaa !8
  %151 = bitcast float* %w5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %151) #3
  %152 = load float, float* %w0, align 4, !tbaa !8
  %153 = load float, float* %w2, align 4, !tbaa !8
  %call67 = call float @sub_float(float %152, float %153)
  store float %call67, float* %w5, align 4, !tbaa !8
  %154 = bitcast float* %w7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %154) #3
  %155 = load float, float* %i4, align 4, !tbaa !8
  %156 = load float, float* %i20, align 4, !tbaa !8
  %call68 = call float @add_float(float %155, float %156)
  store float %call68, float* %w7, align 4, !tbaa !8
  %157 = bitcast float* %w8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #3
  %158 = load float, float* %i4, align 4, !tbaa !8
  %159 = load float, float* %i20, align 4, !tbaa !8
  %call69 = call float @sub_float(float %158, float %159)
  store float %call69, float* %w8, align 4, !tbaa !8
  %160 = bitcast float* %w9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #3
  %161 = load float, float* %i12, align 4, !tbaa !8
  %162 = load float, float* %i28, align 4, !tbaa !8
  %call70 = call float @add_float(float %161, float %162)
  store float %call70, float* %w9, align 4, !tbaa !8
  %163 = bitcast float* %w10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %163) #3
  %164 = load float, float* %i12, align 4, !tbaa !8
  %165 = load float, float* %i28, align 4, !tbaa !8
  %call71 = call float @sub_float(float %164, float %165)
  store float %call71, float* %w10, align 4, !tbaa !8
  %166 = bitcast float* %w11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %166) #3
  %167 = load float, float* %w7, align 4, !tbaa !8
  %168 = load float, float* %w9, align 4, !tbaa !8
  %call72 = call float @add_float(float %167, float %168)
  store float %call72, float* %w11, align 4, !tbaa !8
  %169 = bitcast float* %w12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %169) #3
  %170 = load float, float* %w7, align 4, !tbaa !8
  %171 = load float, float* %w9, align 4, !tbaa !8
  %call73 = call float @sub_float(float %170, float %171)
  store float %call73, float* %w12, align 4, !tbaa !8
  %172 = bitcast float* %w14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %172) #3
  %173 = load float, float* %w4, align 4, !tbaa !8
  %174 = load float, float* %w11, align 4, !tbaa !8
  %call74 = call float @add_float(float %173, float %174)
  store float %call74, float* %w14, align 4, !tbaa !8
  %175 = bitcast float* %w15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %175) #3
  %176 = load float, float* %w4, align 4, !tbaa !8
  %177 = load float, float* %w11, align 4, !tbaa !8
  %call75 = call float @sub_float(float %176, float %177)
  store float %call75, float* %w15, align 4, !tbaa !8
  %178 = bitcast [2 x float]* %w16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %178) #3
  %arrayinit.begin = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %179 = load float, float* %w1, align 4, !tbaa !8
  %180 = load float, float* %w8, align 4, !tbaa !8
  %181 = load float, float* %w10, align 4, !tbaa !8
  %call76 = call float @sub_float(float %180, float %181)
  %call77 = call float @mul_float(float 0x3FE6A09EE0000000, float %call76)
  %call78 = call float @add_float(float %179, float %call77)
  store float %call78, float* %arrayinit.begin, align 4, !tbaa !8
  %arrayinit.element = getelementptr inbounds float, float* %arrayinit.begin, i32 1
  %182 = load float, float* %w3, align 4, !tbaa !8
  %call79 = call float @sub_float(float 0.000000e+00, float %182)
  %183 = load float, float* %w10, align 4, !tbaa !8
  %184 = load float, float* %w8, align 4, !tbaa !8
  %call80 = call float @add_float(float %183, float %184)
  %call81 = call float @mul_float(float 0x3FE6A09EE0000000, float %call80)
  %call82 = call float @sub_float(float %call79, float %call81)
  store float %call82, float* %arrayinit.element, align 4, !tbaa !8
  %185 = bitcast [2 x float]* %w18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %185) #3
  %arrayinit.begin83 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %186 = load float, float* %w1, align 4, !tbaa !8
  %187 = load float, float* %w8, align 4, !tbaa !8
  %188 = load float, float* %w10, align 4, !tbaa !8
  %call84 = call float @sub_float(float %187, float %188)
  %call85 = call float @mul_float(float 0x3FE6A09EE0000000, float %call84)
  %call86 = call float @sub_float(float %186, float %call85)
  store float %call86, float* %arrayinit.begin83, align 4, !tbaa !8
  %arrayinit.element87 = getelementptr inbounds float, float* %arrayinit.begin83, i32 1
  %189 = load float, float* %w3, align 4, !tbaa !8
  %190 = load float, float* %w10, align 4, !tbaa !8
  %191 = load float, float* %w8, align 4, !tbaa !8
  %call88 = call float @add_float(float %190, float %191)
  %call89 = call float @mul_float(float 0x3FE6A09EE0000000, float %call88)
  %call90 = call float @sub_float(float %189, float %call89)
  store float %call90, float* %arrayinit.element87, align 4, !tbaa !8
  %192 = bitcast float* %w19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %192) #3
  %193 = load float, float* %i2, align 4, !tbaa !8
  %194 = load float, float* %i18, align 4, !tbaa !8
  %call91 = call float @add_float(float %193, float %194)
  store float %call91, float* %w19, align 4, !tbaa !8
  %195 = bitcast float* %w20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %195) #3
  %196 = load float, float* %i2, align 4, !tbaa !8
  %197 = load float, float* %i18, align 4, !tbaa !8
  %call92 = call float @sub_float(float %196, float %197)
  store float %call92, float* %w20, align 4, !tbaa !8
  %198 = bitcast float* %w21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #3
  %199 = load float, float* %i10, align 4, !tbaa !8
  %200 = load float, float* %i26, align 4, !tbaa !8
  %call93 = call float @add_float(float %199, float %200)
  store float %call93, float* %w21, align 4, !tbaa !8
  %201 = bitcast float* %w22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %201) #3
  %202 = load float, float* %i10, align 4, !tbaa !8
  %203 = load float, float* %i26, align 4, !tbaa !8
  %call94 = call float @sub_float(float %202, float %203)
  store float %call94, float* %w22, align 4, !tbaa !8
  %204 = bitcast float* %w23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %204) #3
  %205 = load float, float* %w19, align 4, !tbaa !8
  %206 = load float, float* %w21, align 4, !tbaa !8
  %call95 = call float @add_float(float %205, float %206)
  store float %call95, float* %w23, align 4, !tbaa !8
  %207 = bitcast float* %w24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %207) #3
  %208 = load float, float* %w19, align 4, !tbaa !8
  %209 = load float, float* %w21, align 4, !tbaa !8
  %call96 = call float @sub_float(float %208, float %209)
  store float %call96, float* %w24, align 4, !tbaa !8
  %210 = bitcast float* %w26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %210) #3
  %211 = load float, float* %i6, align 4, !tbaa !8
  %212 = load float, float* %i22, align 4, !tbaa !8
  %call97 = call float @add_float(float %211, float %212)
  store float %call97, float* %w26, align 4, !tbaa !8
  %213 = bitcast float* %w27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %213) #3
  %214 = load float, float* %i6, align 4, !tbaa !8
  %215 = load float, float* %i22, align 4, !tbaa !8
  %call98 = call float @sub_float(float %214, float %215)
  store float %call98, float* %w27, align 4, !tbaa !8
  %216 = bitcast float* %w28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %216) #3
  %217 = load float, float* %i14, align 4, !tbaa !8
  %218 = load float, float* %i30, align 4, !tbaa !8
  %call99 = call float @add_float(float %217, float %218)
  store float %call99, float* %w28, align 4, !tbaa !8
  %219 = bitcast float* %w29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %219) #3
  %220 = load float, float* %i14, align 4, !tbaa !8
  %221 = load float, float* %i30, align 4, !tbaa !8
  %call100 = call float @sub_float(float %220, float %221)
  store float %call100, float* %w29, align 4, !tbaa !8
  %222 = bitcast float* %w30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %222) #3
  %223 = load float, float* %w26, align 4, !tbaa !8
  %224 = load float, float* %w28, align 4, !tbaa !8
  %call101 = call float @add_float(float %223, float %224)
  store float %call101, float* %w30, align 4, !tbaa !8
  %225 = bitcast float* %w31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %225) #3
  %226 = load float, float* %w26, align 4, !tbaa !8
  %227 = load float, float* %w28, align 4, !tbaa !8
  %call102 = call float @sub_float(float %226, float %227)
  store float %call102, float* %w31, align 4, !tbaa !8
  %228 = bitcast float* %w33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %228) #3
  %229 = load float, float* %w23, align 4, !tbaa !8
  %230 = load float, float* %w30, align 4, !tbaa !8
  %call103 = call float @add_float(float %229, float %230)
  store float %call103, float* %w33, align 4, !tbaa !8
  %231 = bitcast float* %w34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %231) #3
  %232 = load float, float* %w23, align 4, !tbaa !8
  %233 = load float, float* %w30, align 4, !tbaa !8
  %call104 = call float @sub_float(float %232, float %233)
  store float %call104, float* %w34, align 4, !tbaa !8
  %234 = bitcast [2 x float]* %w35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %234) #3
  %arrayinit.begin105 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %235 = load float, float* %w20, align 4, !tbaa !8
  %236 = load float, float* %w27, align 4, !tbaa !8
  %237 = load float, float* %w29, align 4, !tbaa !8
  %call106 = call float @sub_float(float %236, float %237)
  %call107 = call float @mul_float(float 0x3FE6A09EE0000000, float %call106)
  %call108 = call float @add_float(float %235, float %call107)
  store float %call108, float* %arrayinit.begin105, align 4, !tbaa !8
  %arrayinit.element109 = getelementptr inbounds float, float* %arrayinit.begin105, i32 1
  %238 = load float, float* %w22, align 4, !tbaa !8
  %call110 = call float @sub_float(float 0.000000e+00, float %238)
  %239 = load float, float* %w29, align 4, !tbaa !8
  %240 = load float, float* %w27, align 4, !tbaa !8
  %call111 = call float @add_float(float %239, float %240)
  %call112 = call float @mul_float(float 0x3FE6A09EE0000000, float %call111)
  %call113 = call float @sub_float(float %call110, float %call112)
  store float %call113, float* %arrayinit.element109, align 4, !tbaa !8
  %241 = bitcast [2 x float]* %w37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %241) #3
  %arrayinit.begin114 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %242 = load float, float* %w20, align 4, !tbaa !8
  %243 = load float, float* %w27, align 4, !tbaa !8
  %244 = load float, float* %w29, align 4, !tbaa !8
  %call115 = call float @sub_float(float %243, float %244)
  %call116 = call float @mul_float(float 0x3FE6A09EE0000000, float %call115)
  %call117 = call float @sub_float(float %242, float %call116)
  store float %call117, float* %arrayinit.begin114, align 4, !tbaa !8
  %arrayinit.element118 = getelementptr inbounds float, float* %arrayinit.begin114, i32 1
  %245 = load float, float* %w22, align 4, !tbaa !8
  %246 = load float, float* %w29, align 4, !tbaa !8
  %247 = load float, float* %w27, align 4, !tbaa !8
  %call119 = call float @add_float(float %246, float %247)
  %call120 = call float @mul_float(float 0x3FE6A09EE0000000, float %call119)
  %call121 = call float @sub_float(float %245, float %call120)
  store float %call121, float* %arrayinit.element118, align 4, !tbaa !8
  %248 = bitcast float* %w38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %248) #3
  %249 = load float, float* %w14, align 4, !tbaa !8
  %250 = load float, float* %w33, align 4, !tbaa !8
  %call122 = call float @add_float(float %249, float %250)
  store float %call122, float* %w38, align 4, !tbaa !8
  %251 = bitcast float* %w39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %251) #3
  %252 = load float, float* %w14, align 4, !tbaa !8
  %253 = load float, float* %w33, align 4, !tbaa !8
  %call123 = call float @sub_float(float %252, float %253)
  store float %call123, float* %w39, align 4, !tbaa !8
  %254 = bitcast [2 x float]* %w40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %254) #3
  %arrayinit.begin124 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %255 = load float, float* %arrayidx, align 4, !tbaa !8
  %arrayidx125 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %256 = load float, float* %arrayidx125, align 4, !tbaa !8
  %call126 = call float @mul_float(float 0x3FED906CC0000000, float %256)
  %arrayidx127 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %257 = load float, float* %arrayidx127, align 4, !tbaa !8
  %call128 = call float @mul_float(float 0x3FD87DE0E0000000, float %257)
  %call129 = call float @add_float(float %call126, float %call128)
  %call130 = call float @add_float(float %255, float %call129)
  store float %call130, float* %arrayinit.begin124, align 4, !tbaa !8
  %arrayinit.element131 = getelementptr inbounds float, float* %arrayinit.begin124, i32 1
  %arrayidx132 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 1
  %258 = load float, float* %arrayidx132, align 4, !tbaa !8
  %arrayidx133 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %259 = load float, float* %arrayidx133, align 4, !tbaa !8
  %call134 = call float @mul_float(float 0x3FED906CC0000000, float %259)
  %arrayidx135 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %260 = load float, float* %arrayidx135, align 4, !tbaa !8
  %call136 = call float @mul_float(float 0x3FD87DE0E0000000, float %260)
  %call137 = call float @sub_float(float %call134, float %call136)
  %call138 = call float @add_float(float %258, float %call137)
  store float %call138, float* %arrayinit.element131, align 4, !tbaa !8
  %261 = bitcast [2 x float]* %w41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %261) #3
  %arrayinit.begin139 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 0
  %262 = load float, float* %w5, align 4, !tbaa !8
  %263 = load float, float* %w24, align 4, !tbaa !8
  %264 = load float, float* %w31, align 4, !tbaa !8
  %call140 = call float @sub_float(float %263, float %264)
  %call141 = call float @mul_float(float 0x3FE6A09EE0000000, float %call140)
  %call142 = call float @add_float(float %262, float %call141)
  store float %call142, float* %arrayinit.begin139, align 4, !tbaa !8
  %arrayinit.element143 = getelementptr inbounds float, float* %arrayinit.begin139, i32 1
  %265 = load float, float* %w12, align 4, !tbaa !8
  %call144 = call float @sub_float(float 0.000000e+00, float %265)
  %266 = load float, float* %w31, align 4, !tbaa !8
  %267 = load float, float* %w24, align 4, !tbaa !8
  %call145 = call float @add_float(float %266, float %267)
  %call146 = call float @mul_float(float 0x3FE6A09EE0000000, float %call145)
  %call147 = call float @sub_float(float %call144, float %call146)
  store float %call147, float* %arrayinit.element143, align 4, !tbaa !8
  %268 = bitcast [2 x float]* %w42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %268) #3
  %arrayinit.begin148 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 0
  %arrayidx149 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %269 = load float, float* %arrayidx149, align 4, !tbaa !8
  %arrayidx150 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %270 = load float, float* %arrayidx150, align 4, !tbaa !8
  %call151 = call float @mul_float(float 0x3FD87DE0E0000000, float %270)
  %arrayidx152 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %271 = load float, float* %arrayidx152, align 4, !tbaa !8
  %call153 = call float @mul_float(float 0x3FED906CC0000000, float %271)
  %call154 = call float @add_float(float %call151, float %call153)
  %call155 = call float @add_float(float %269, float %call154)
  store float %call155, float* %arrayinit.begin148, align 4, !tbaa !8
  %arrayinit.element156 = getelementptr inbounds float, float* %arrayinit.begin148, i32 1
  %arrayidx157 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 1
  %272 = load float, float* %arrayidx157, align 4, !tbaa !8
  %arrayidx158 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %273 = load float, float* %arrayidx158, align 4, !tbaa !8
  %call159 = call float @mul_float(float 0x3FD87DE0E0000000, float %273)
  %arrayidx160 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %274 = load float, float* %arrayidx160, align 4, !tbaa !8
  %call161 = call float @mul_float(float 0x3FED906CC0000000, float %274)
  %call162 = call float @sub_float(float %call159, float %call161)
  %call163 = call float @add_float(float %272, float %call162)
  store float %call163, float* %arrayinit.element156, align 4, !tbaa !8
  %275 = bitcast [2 x float]* %w44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %275) #3
  %arrayinit.begin164 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %arrayidx165 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %276 = load float, float* %arrayidx165, align 4, !tbaa !8
  %arrayidx166 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %277 = load float, float* %arrayidx166, align 4, !tbaa !8
  %call167 = call float @mul_float(float 0x3FD87DE0E0000000, float %277)
  %call168 = call float @sub_float(float 0.000000e+00, float %call167)
  %arrayidx169 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %278 = load float, float* %arrayidx169, align 4, !tbaa !8
  %call170 = call float @mul_float(float 0x3FED906CC0000000, float %278)
  %call171 = call float @sub_float(float %call168, float %call170)
  %call172 = call float @add_float(float %276, float %call171)
  store float %call172, float* %arrayinit.begin164, align 4, !tbaa !8
  %arrayinit.element173 = getelementptr inbounds float, float* %arrayinit.begin164, i32 1
  %arrayidx174 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 1
  %279 = load float, float* %arrayidx174, align 4, !tbaa !8
  %call175 = call float @sub_float(float 0.000000e+00, float %279)
  %arrayidx176 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %280 = load float, float* %arrayidx176, align 4, !tbaa !8
  %call177 = call float @mul_float(float 0x3FED906CC0000000, float %280)
  %arrayidx178 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %281 = load float, float* %arrayidx178, align 4, !tbaa !8
  %call179 = call float @mul_float(float 0x3FD87DE0E0000000, float %281)
  %call180 = call float @sub_float(float %call177, float %call179)
  %call181 = call float @sub_float(float %call175, float %call180)
  store float %call181, float* %arrayinit.element173, align 4, !tbaa !8
  %282 = bitcast [2 x float]* %w45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %282) #3
  %arrayinit.begin182 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %283 = load float, float* %w5, align 4, !tbaa !8
  %284 = load float, float* %w24, align 4, !tbaa !8
  %285 = load float, float* %w31, align 4, !tbaa !8
  %call183 = call float @sub_float(float %284, float %285)
  %call184 = call float @mul_float(float 0x3FE6A09EE0000000, float %call183)
  %call185 = call float @sub_float(float %283, float %call184)
  store float %call185, float* %arrayinit.begin182, align 4, !tbaa !8
  %arrayinit.element186 = getelementptr inbounds float, float* %arrayinit.begin182, i32 1
  %286 = load float, float* %w12, align 4, !tbaa !8
  %287 = load float, float* %w31, align 4, !tbaa !8
  %288 = load float, float* %w24, align 4, !tbaa !8
  %call187 = call float @add_float(float %287, float %288)
  %call188 = call float @mul_float(float 0x3FE6A09EE0000000, float %call187)
  %call189 = call float @sub_float(float %286, float %call188)
  store float %call189, float* %arrayinit.element186, align 4, !tbaa !8
  %289 = bitcast [2 x float]* %w46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %289) #3
  %arrayinit.begin190 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 0
  %arrayidx191 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %290 = load float, float* %arrayidx191, align 4, !tbaa !8
  %arrayidx192 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %291 = load float, float* %arrayidx192, align 4, !tbaa !8
  %call193 = call float @mul_float(float 0x3FED906CC0000000, float %291)
  %call194 = call float @sub_float(float 0.000000e+00, float %call193)
  %arrayidx195 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %292 = load float, float* %arrayidx195, align 4, !tbaa !8
  %call196 = call float @mul_float(float 0x3FD87DE0E0000000, float %292)
  %call197 = call float @sub_float(float %call194, float %call196)
  %call198 = call float @add_float(float %290, float %call197)
  store float %call198, float* %arrayinit.begin190, align 4, !tbaa !8
  %arrayinit.element199 = getelementptr inbounds float, float* %arrayinit.begin190, i32 1
  %arrayidx200 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 1
  %293 = load float, float* %arrayidx200, align 4, !tbaa !8
  %call201 = call float @sub_float(float 0.000000e+00, float %293)
  %arrayidx202 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %294 = load float, float* %arrayidx202, align 4, !tbaa !8
  %call203 = call float @mul_float(float 0x3FD87DE0E0000000, float %294)
  %arrayidx204 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %295 = load float, float* %arrayidx204, align 4, !tbaa !8
  %call205 = call float @mul_float(float 0x3FED906CC0000000, float %295)
  %call206 = call float @sub_float(float %call203, float %call205)
  %call207 = call float @sub_float(float %call201, float %call206)
  store float %call207, float* %arrayinit.element199, align 4, !tbaa !8
  %296 = bitcast float* %w47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %296) #3
  %297 = load float, float* %i1, align 4, !tbaa !8
  %298 = load float, float* %i17, align 4, !tbaa !8
  %call208 = call float @add_float(float %297, float %298)
  store float %call208, float* %w47, align 4, !tbaa !8
  %299 = bitcast float* %w48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %299) #3
  %300 = load float, float* %i1, align 4, !tbaa !8
  %301 = load float, float* %i17, align 4, !tbaa !8
  %call209 = call float @sub_float(float %300, float %301)
  store float %call209, float* %w48, align 4, !tbaa !8
  %302 = bitcast float* %w49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %302) #3
  %303 = load float, float* %i9, align 4, !tbaa !8
  %304 = load float, float* %i25, align 4, !tbaa !8
  %call210 = call float @add_float(float %303, float %304)
  store float %call210, float* %w49, align 4, !tbaa !8
  %305 = bitcast float* %w50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %305) #3
  %306 = load float, float* %i9, align 4, !tbaa !8
  %307 = load float, float* %i25, align 4, !tbaa !8
  %call211 = call float @sub_float(float %306, float %307)
  store float %call211, float* %w50, align 4, !tbaa !8
  %308 = bitcast float* %w51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %308) #3
  %309 = load float, float* %w47, align 4, !tbaa !8
  %310 = load float, float* %w49, align 4, !tbaa !8
  %call212 = call float @add_float(float %309, float %310)
  store float %call212, float* %w51, align 4, !tbaa !8
  %311 = bitcast float* %w52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %311) #3
  %312 = load float, float* %w47, align 4, !tbaa !8
  %313 = load float, float* %w49, align 4, !tbaa !8
  %call213 = call float @sub_float(float %312, float %313)
  store float %call213, float* %w52, align 4, !tbaa !8
  %314 = bitcast float* %w54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %314) #3
  %315 = load float, float* %i5, align 4, !tbaa !8
  %316 = load float, float* %i21, align 4, !tbaa !8
  %call214 = call float @add_float(float %315, float %316)
  store float %call214, float* %w54, align 4, !tbaa !8
  %317 = bitcast float* %w55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %317) #3
  %318 = load float, float* %i5, align 4, !tbaa !8
  %319 = load float, float* %i21, align 4, !tbaa !8
  %call215 = call float @sub_float(float %318, float %319)
  store float %call215, float* %w55, align 4, !tbaa !8
  %320 = bitcast float* %w56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %320) #3
  %321 = load float, float* %i13, align 4, !tbaa !8
  %322 = load float, float* %i29, align 4, !tbaa !8
  %call216 = call float @add_float(float %321, float %322)
  store float %call216, float* %w56, align 4, !tbaa !8
  %323 = bitcast float* %w57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %323) #3
  %324 = load float, float* %i13, align 4, !tbaa !8
  %325 = load float, float* %i29, align 4, !tbaa !8
  %call217 = call float @sub_float(float %324, float %325)
  store float %call217, float* %w57, align 4, !tbaa !8
  %326 = bitcast float* %w58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %326) #3
  %327 = load float, float* %w54, align 4, !tbaa !8
  %328 = load float, float* %w56, align 4, !tbaa !8
  %call218 = call float @add_float(float %327, float %328)
  store float %call218, float* %w58, align 4, !tbaa !8
  %329 = bitcast float* %w59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %329) #3
  %330 = load float, float* %w54, align 4, !tbaa !8
  %331 = load float, float* %w56, align 4, !tbaa !8
  %call219 = call float @sub_float(float %330, float %331)
  store float %call219, float* %w59, align 4, !tbaa !8
  %332 = bitcast float* %w61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %332) #3
  %333 = load float, float* %w51, align 4, !tbaa !8
  %334 = load float, float* %w58, align 4, !tbaa !8
  %call220 = call float @add_float(float %333, float %334)
  store float %call220, float* %w61, align 4, !tbaa !8
  %335 = bitcast float* %w62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %335) #3
  %336 = load float, float* %w51, align 4, !tbaa !8
  %337 = load float, float* %w58, align 4, !tbaa !8
  %call221 = call float @sub_float(float %336, float %337)
  store float %call221, float* %w62, align 4, !tbaa !8
  %338 = bitcast [2 x float]* %w63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %338) #3
  %arrayinit.begin222 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 0
  %339 = load float, float* %w48, align 4, !tbaa !8
  %340 = load float, float* %w55, align 4, !tbaa !8
  %341 = load float, float* %w57, align 4, !tbaa !8
  %call223 = call float @sub_float(float %340, float %341)
  %call224 = call float @mul_float(float 0x3FE6A09EE0000000, float %call223)
  %call225 = call float @add_float(float %339, float %call224)
  store float %call225, float* %arrayinit.begin222, align 4, !tbaa !8
  %arrayinit.element226 = getelementptr inbounds float, float* %arrayinit.begin222, i32 1
  %342 = load float, float* %w50, align 4, !tbaa !8
  %call227 = call float @sub_float(float 0.000000e+00, float %342)
  %343 = load float, float* %w57, align 4, !tbaa !8
  %344 = load float, float* %w55, align 4, !tbaa !8
  %call228 = call float @add_float(float %343, float %344)
  %call229 = call float @mul_float(float 0x3FE6A09EE0000000, float %call228)
  %call230 = call float @sub_float(float %call227, float %call229)
  store float %call230, float* %arrayinit.element226, align 4, !tbaa !8
  %345 = bitcast [2 x float]* %w65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %345) #3
  %arrayinit.begin231 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 0
  %346 = load float, float* %w48, align 4, !tbaa !8
  %347 = load float, float* %w55, align 4, !tbaa !8
  %348 = load float, float* %w57, align 4, !tbaa !8
  %call232 = call float @sub_float(float %347, float %348)
  %call233 = call float @mul_float(float 0x3FE6A09EE0000000, float %call232)
  %call234 = call float @sub_float(float %346, float %call233)
  store float %call234, float* %arrayinit.begin231, align 4, !tbaa !8
  %arrayinit.element235 = getelementptr inbounds float, float* %arrayinit.begin231, i32 1
  %349 = load float, float* %w50, align 4, !tbaa !8
  %350 = load float, float* %w57, align 4, !tbaa !8
  %351 = load float, float* %w55, align 4, !tbaa !8
  %call236 = call float @add_float(float %350, float %351)
  %call237 = call float @mul_float(float 0x3FE6A09EE0000000, float %call236)
  %call238 = call float @sub_float(float %349, float %call237)
  store float %call238, float* %arrayinit.element235, align 4, !tbaa !8
  %352 = bitcast float* %w66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %352) #3
  %353 = load float, float* %i3, align 4, !tbaa !8
  %354 = load float, float* %i19, align 4, !tbaa !8
  %call239 = call float @add_float(float %353, float %354)
  store float %call239, float* %w66, align 4, !tbaa !8
  %355 = bitcast float* %w67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %355) #3
  %356 = load float, float* %i3, align 4, !tbaa !8
  %357 = load float, float* %i19, align 4, !tbaa !8
  %call240 = call float @sub_float(float %356, float %357)
  store float %call240, float* %w67, align 4, !tbaa !8
  %358 = bitcast float* %w68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %358) #3
  %359 = load float, float* %i11, align 4, !tbaa !8
  %360 = load float, float* %i27, align 4, !tbaa !8
  %call241 = call float @add_float(float %359, float %360)
  store float %call241, float* %w68, align 4, !tbaa !8
  %361 = bitcast float* %w69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %361) #3
  %362 = load float, float* %i11, align 4, !tbaa !8
  %363 = load float, float* %i27, align 4, !tbaa !8
  %call242 = call float @sub_float(float %362, float %363)
  store float %call242, float* %w69, align 4, !tbaa !8
  %364 = bitcast float* %w70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %364) #3
  %365 = load float, float* %w66, align 4, !tbaa !8
  %366 = load float, float* %w68, align 4, !tbaa !8
  %call243 = call float @add_float(float %365, float %366)
  store float %call243, float* %w70, align 4, !tbaa !8
  %367 = bitcast float* %w71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %367) #3
  %368 = load float, float* %w66, align 4, !tbaa !8
  %369 = load float, float* %w68, align 4, !tbaa !8
  %call244 = call float @sub_float(float %368, float %369)
  store float %call244, float* %w71, align 4, !tbaa !8
  %370 = bitcast float* %w73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %370) #3
  %371 = load float, float* %i7, align 4, !tbaa !8
  %372 = load float, float* %i23, align 4, !tbaa !8
  %call245 = call float @add_float(float %371, float %372)
  store float %call245, float* %w73, align 4, !tbaa !8
  %373 = bitcast float* %w74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %373) #3
  %374 = load float, float* %i7, align 4, !tbaa !8
  %375 = load float, float* %i23, align 4, !tbaa !8
  %call246 = call float @sub_float(float %374, float %375)
  store float %call246, float* %w74, align 4, !tbaa !8
  %376 = bitcast float* %w75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %376) #3
  %377 = load float, float* %i15, align 4, !tbaa !8
  %378 = load float, float* %i31, align 4, !tbaa !8
  %call247 = call float @add_float(float %377, float %378)
  store float %call247, float* %w75, align 4, !tbaa !8
  %379 = bitcast float* %w76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %379) #3
  %380 = load float, float* %i15, align 4, !tbaa !8
  %381 = load float, float* %i31, align 4, !tbaa !8
  %call248 = call float @sub_float(float %380, float %381)
  store float %call248, float* %w76, align 4, !tbaa !8
  %382 = bitcast float* %w77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %382) #3
  %383 = load float, float* %w73, align 4, !tbaa !8
  %384 = load float, float* %w75, align 4, !tbaa !8
  %call249 = call float @add_float(float %383, float %384)
  store float %call249, float* %w77, align 4, !tbaa !8
  %385 = bitcast float* %w78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %385) #3
  %386 = load float, float* %w73, align 4, !tbaa !8
  %387 = load float, float* %w75, align 4, !tbaa !8
  %call250 = call float @sub_float(float %386, float %387)
  store float %call250, float* %w78, align 4, !tbaa !8
  %388 = bitcast float* %w80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %388) #3
  %389 = load float, float* %w70, align 4, !tbaa !8
  %390 = load float, float* %w77, align 4, !tbaa !8
  %call251 = call float @add_float(float %389, float %390)
  store float %call251, float* %w80, align 4, !tbaa !8
  %391 = bitcast float* %w81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %391) #3
  %392 = load float, float* %w70, align 4, !tbaa !8
  %393 = load float, float* %w77, align 4, !tbaa !8
  %call252 = call float @sub_float(float %392, float %393)
  store float %call252, float* %w81, align 4, !tbaa !8
  %394 = bitcast [2 x float]* %w82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %394) #3
  %arrayinit.begin253 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 0
  %395 = load float, float* %w67, align 4, !tbaa !8
  %396 = load float, float* %w74, align 4, !tbaa !8
  %397 = load float, float* %w76, align 4, !tbaa !8
  %call254 = call float @sub_float(float %396, float %397)
  %call255 = call float @mul_float(float 0x3FE6A09EE0000000, float %call254)
  %call256 = call float @add_float(float %395, float %call255)
  store float %call256, float* %arrayinit.begin253, align 4, !tbaa !8
  %arrayinit.element257 = getelementptr inbounds float, float* %arrayinit.begin253, i32 1
  %398 = load float, float* %w69, align 4, !tbaa !8
  %call258 = call float @sub_float(float 0.000000e+00, float %398)
  %399 = load float, float* %w76, align 4, !tbaa !8
  %400 = load float, float* %w74, align 4, !tbaa !8
  %call259 = call float @add_float(float %399, float %400)
  %call260 = call float @mul_float(float 0x3FE6A09EE0000000, float %call259)
  %call261 = call float @sub_float(float %call258, float %call260)
  store float %call261, float* %arrayinit.element257, align 4, !tbaa !8
  %401 = bitcast [2 x float]* %w84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %401) #3
  %arrayinit.begin262 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 0
  %402 = load float, float* %w67, align 4, !tbaa !8
  %403 = load float, float* %w74, align 4, !tbaa !8
  %404 = load float, float* %w76, align 4, !tbaa !8
  %call263 = call float @sub_float(float %403, float %404)
  %call264 = call float @mul_float(float 0x3FE6A09EE0000000, float %call263)
  %call265 = call float @sub_float(float %402, float %call264)
  store float %call265, float* %arrayinit.begin262, align 4, !tbaa !8
  %arrayinit.element266 = getelementptr inbounds float, float* %arrayinit.begin262, i32 1
  %405 = load float, float* %w69, align 4, !tbaa !8
  %406 = load float, float* %w76, align 4, !tbaa !8
  %407 = load float, float* %w74, align 4, !tbaa !8
  %call267 = call float @add_float(float %406, float %407)
  %call268 = call float @mul_float(float 0x3FE6A09EE0000000, float %call267)
  %call269 = call float @sub_float(float %405, float %call268)
  store float %call269, float* %arrayinit.element266, align 4, !tbaa !8
  %408 = bitcast float* %w85 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %408) #3
  %409 = load float, float* %w61, align 4, !tbaa !8
  %410 = load float, float* %w80, align 4, !tbaa !8
  %call270 = call float @add_float(float %409, float %410)
  store float %call270, float* %w85, align 4, !tbaa !8
  %411 = bitcast float* %w86 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %411) #3
  %412 = load float, float* %w61, align 4, !tbaa !8
  %413 = load float, float* %w80, align 4, !tbaa !8
  %call271 = call float @sub_float(float %412, float %413)
  store float %call271, float* %w86, align 4, !tbaa !8
  %414 = bitcast [2 x float]* %w87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %414) #3
  %arrayinit.begin272 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 0
  %arrayidx273 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 0
  %415 = load float, float* %arrayidx273, align 4, !tbaa !8
  %arrayidx274 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 0
  %416 = load float, float* %arrayidx274, align 4, !tbaa !8
  %call275 = call float @mul_float(float 0x3FED906CC0000000, float %416)
  %arrayidx276 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 1
  %417 = load float, float* %arrayidx276, align 4, !tbaa !8
  %call277 = call float @mul_float(float 0x3FD87DE0E0000000, float %417)
  %call278 = call float @add_float(float %call275, float %call277)
  %call279 = call float @add_float(float %415, float %call278)
  store float %call279, float* %arrayinit.begin272, align 4, !tbaa !8
  %arrayinit.element280 = getelementptr inbounds float, float* %arrayinit.begin272, i32 1
  %arrayidx281 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 1
  %418 = load float, float* %arrayidx281, align 4, !tbaa !8
  %arrayidx282 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 1
  %419 = load float, float* %arrayidx282, align 4, !tbaa !8
  %call283 = call float @mul_float(float 0x3FED906CC0000000, float %419)
  %arrayidx284 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 0
  %420 = load float, float* %arrayidx284, align 4, !tbaa !8
  %call285 = call float @mul_float(float 0x3FD87DE0E0000000, float %420)
  %call286 = call float @sub_float(float %call283, float %call285)
  %call287 = call float @add_float(float %418, float %call286)
  store float %call287, float* %arrayinit.element280, align 4, !tbaa !8
  %421 = bitcast [2 x float]* %w88 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %421) #3
  %arrayinit.begin288 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 0
  %422 = load float, float* %w52, align 4, !tbaa !8
  %423 = load float, float* %w71, align 4, !tbaa !8
  %424 = load float, float* %w78, align 4, !tbaa !8
  %call289 = call float @sub_float(float %423, float %424)
  %call290 = call float @mul_float(float 0x3FE6A09EE0000000, float %call289)
  %call291 = call float @add_float(float %422, float %call290)
  store float %call291, float* %arrayinit.begin288, align 4, !tbaa !8
  %arrayinit.element292 = getelementptr inbounds float, float* %arrayinit.begin288, i32 1
  %425 = load float, float* %w59, align 4, !tbaa !8
  %call293 = call float @sub_float(float 0.000000e+00, float %425)
  %426 = load float, float* %w78, align 4, !tbaa !8
  %427 = load float, float* %w71, align 4, !tbaa !8
  %call294 = call float @add_float(float %426, float %427)
  %call295 = call float @mul_float(float 0x3FE6A09EE0000000, float %call294)
  %call296 = call float @sub_float(float %call293, float %call295)
  store float %call296, float* %arrayinit.element292, align 4, !tbaa !8
  %428 = bitcast [2 x float]* %w89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %428) #3
  %arrayinit.begin297 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 0
  %arrayidx298 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 0
  %429 = load float, float* %arrayidx298, align 4, !tbaa !8
  %arrayidx299 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 0
  %430 = load float, float* %arrayidx299, align 4, !tbaa !8
  %call300 = call float @mul_float(float 0x3FD87DE0E0000000, float %430)
  %arrayidx301 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 1
  %431 = load float, float* %arrayidx301, align 4, !tbaa !8
  %call302 = call float @mul_float(float 0x3FED906CC0000000, float %431)
  %call303 = call float @add_float(float %call300, float %call302)
  %call304 = call float @add_float(float %429, float %call303)
  store float %call304, float* %arrayinit.begin297, align 4, !tbaa !8
  %arrayinit.element305 = getelementptr inbounds float, float* %arrayinit.begin297, i32 1
  %arrayidx306 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 1
  %432 = load float, float* %arrayidx306, align 4, !tbaa !8
  %arrayidx307 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 1
  %433 = load float, float* %arrayidx307, align 4, !tbaa !8
  %call308 = call float @mul_float(float 0x3FD87DE0E0000000, float %433)
  %arrayidx309 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 0
  %434 = load float, float* %arrayidx309, align 4, !tbaa !8
  %call310 = call float @mul_float(float 0x3FED906CC0000000, float %434)
  %call311 = call float @sub_float(float %call308, float %call310)
  %call312 = call float @add_float(float %432, float %call311)
  store float %call312, float* %arrayinit.element305, align 4, !tbaa !8
  %435 = bitcast [2 x float]* %w91 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %435) #3
  %arrayinit.begin313 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 0
  %arrayidx314 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 0
  %436 = load float, float* %arrayidx314, align 4, !tbaa !8
  %arrayidx315 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 0
  %437 = load float, float* %arrayidx315, align 4, !tbaa !8
  %call316 = call float @mul_float(float 0x3FD87DE0E0000000, float %437)
  %call317 = call float @sub_float(float 0.000000e+00, float %call316)
  %arrayidx318 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 1
  %438 = load float, float* %arrayidx318, align 4, !tbaa !8
  %call319 = call float @mul_float(float 0x3FED906CC0000000, float %438)
  %call320 = call float @sub_float(float %call317, float %call319)
  %call321 = call float @add_float(float %436, float %call320)
  store float %call321, float* %arrayinit.begin313, align 4, !tbaa !8
  %arrayinit.element322 = getelementptr inbounds float, float* %arrayinit.begin313, i32 1
  %arrayidx323 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 1
  %439 = load float, float* %arrayidx323, align 4, !tbaa !8
  %call324 = call float @sub_float(float 0.000000e+00, float %439)
  %arrayidx325 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 0
  %440 = load float, float* %arrayidx325, align 4, !tbaa !8
  %call326 = call float @mul_float(float 0x3FED906CC0000000, float %440)
  %arrayidx327 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 1
  %441 = load float, float* %arrayidx327, align 4, !tbaa !8
  %call328 = call float @mul_float(float 0x3FD87DE0E0000000, float %441)
  %call329 = call float @sub_float(float %call326, float %call328)
  %call330 = call float @sub_float(float %call324, float %call329)
  store float %call330, float* %arrayinit.element322, align 4, !tbaa !8
  %442 = bitcast [2 x float]* %w92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %442) #3
  %arrayinit.begin331 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 0
  %443 = load float, float* %w52, align 4, !tbaa !8
  %444 = load float, float* %w71, align 4, !tbaa !8
  %445 = load float, float* %w78, align 4, !tbaa !8
  %call332 = call float @sub_float(float %444, float %445)
  %call333 = call float @mul_float(float 0x3FE6A09EE0000000, float %call332)
  %call334 = call float @sub_float(float %443, float %call333)
  store float %call334, float* %arrayinit.begin331, align 4, !tbaa !8
  %arrayinit.element335 = getelementptr inbounds float, float* %arrayinit.begin331, i32 1
  %446 = load float, float* %w59, align 4, !tbaa !8
  %447 = load float, float* %w78, align 4, !tbaa !8
  %448 = load float, float* %w71, align 4, !tbaa !8
  %call336 = call float @add_float(float %447, float %448)
  %call337 = call float @mul_float(float 0x3FE6A09EE0000000, float %call336)
  %call338 = call float @sub_float(float %446, float %call337)
  store float %call338, float* %arrayinit.element335, align 4, !tbaa !8
  %449 = bitcast [2 x float]* %w93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %449) #3
  %arrayinit.begin339 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 0
  %arrayidx340 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 0
  %450 = load float, float* %arrayidx340, align 4, !tbaa !8
  %arrayidx341 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 0
  %451 = load float, float* %arrayidx341, align 4, !tbaa !8
  %call342 = call float @mul_float(float 0x3FED906CC0000000, float %451)
  %call343 = call float @sub_float(float 0.000000e+00, float %call342)
  %arrayidx344 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 1
  %452 = load float, float* %arrayidx344, align 4, !tbaa !8
  %call345 = call float @mul_float(float 0x3FD87DE0E0000000, float %452)
  %call346 = call float @sub_float(float %call343, float %call345)
  %call347 = call float @add_float(float %450, float %call346)
  store float %call347, float* %arrayinit.begin339, align 4, !tbaa !8
  %arrayinit.element348 = getelementptr inbounds float, float* %arrayinit.begin339, i32 1
  %arrayidx349 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 1
  %453 = load float, float* %arrayidx349, align 4, !tbaa !8
  %call350 = call float @sub_float(float 0.000000e+00, float %453)
  %arrayidx351 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 0
  %454 = load float, float* %arrayidx351, align 4, !tbaa !8
  %call352 = call float @mul_float(float 0x3FD87DE0E0000000, float %454)
  %arrayidx353 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 1
  %455 = load float, float* %arrayidx353, align 4, !tbaa !8
  %call354 = call float @mul_float(float 0x3FED906CC0000000, float %455)
  %call355 = call float @sub_float(float %call352, float %call354)
  %call356 = call float @sub_float(float %call350, float %call355)
  store float %call356, float* %arrayinit.element348, align 4, !tbaa !8
  %456 = load float*, float** %output.addr, align 4, !tbaa !2
  %457 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul357 = mul nsw i32 0, %457
  %add.ptr358 = getelementptr inbounds float, float* %456, i32 %mul357
  %458 = load float, float* %w38, align 4, !tbaa !8
  %459 = load float, float* %w85, align 4, !tbaa !8
  %call359 = call float @add_float(float %458, float %459)
  call void @store_float(float* %add.ptr358, float %call359)
  %460 = load float*, float** %output.addr, align 4, !tbaa !2
  %461 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul360 = mul nsw i32 1, %461
  %add.ptr361 = getelementptr inbounds float, float* %460, i32 %mul360
  %arrayidx362 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 0
  %462 = load float, float* %arrayidx362, align 4, !tbaa !8
  %arrayidx363 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 0
  %463 = load float, float* %arrayidx363, align 4, !tbaa !8
  %call364 = call float @mul_float(float 0x3FEF629740000000, float %463)
  %arrayidx365 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 1
  %464 = load float, float* %arrayidx365, align 4, !tbaa !8
  %call366 = call float @mul_float(float 0x3FC8F8B580000000, float %464)
  %call367 = call float @add_float(float %call364, float %call366)
  %call368 = call float @add_float(float %462, float %call367)
  call void @store_float(float* %add.ptr361, float %call368)
  %465 = load float*, float** %output.addr, align 4, !tbaa !2
  %466 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul369 = mul nsw i32 2, %466
  %add.ptr370 = getelementptr inbounds float, float* %465, i32 %mul369
  %arrayidx371 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 0
  %467 = load float, float* %arrayidx371, align 4, !tbaa !8
  %arrayidx372 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 0
  %468 = load float, float* %arrayidx372, align 4, !tbaa !8
  %call373 = call float @mul_float(float 0x3FED906CC0000000, float %468)
  %arrayidx374 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 1
  %469 = load float, float* %arrayidx374, align 4, !tbaa !8
  %call375 = call float @mul_float(float 0x3FD87DE0E0000000, float %469)
  %call376 = call float @add_float(float %call373, float %call375)
  %call377 = call float @add_float(float %467, float %call376)
  call void @store_float(float* %add.ptr370, float %call377)
  %470 = load float*, float** %output.addr, align 4, !tbaa !2
  %471 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul378 = mul nsw i32 3, %471
  %add.ptr379 = getelementptr inbounds float, float* %470, i32 %mul378
  %arrayidx380 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 0
  %472 = load float, float* %arrayidx380, align 4, !tbaa !8
  %arrayidx381 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 0
  %473 = load float, float* %arrayidx381, align 4, !tbaa !8
  %call382 = call float @mul_float(float 0x3FEA9B6700000000, float %473)
  %arrayidx383 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 1
  %474 = load float, float* %arrayidx383, align 4, !tbaa !8
  %call384 = call float @mul_float(float 0x3FE1C73AC0000000, float %474)
  %call385 = call float @add_float(float %call382, float %call384)
  %call386 = call float @add_float(float %472, float %call385)
  call void @store_float(float* %add.ptr379, float %call386)
  %475 = load float*, float** %output.addr, align 4, !tbaa !2
  %476 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul387 = mul nsw i32 4, %476
  %add.ptr388 = getelementptr inbounds float, float* %475, i32 %mul387
  %477 = load float, float* %w15, align 4, !tbaa !8
  %478 = load float, float* %w62, align 4, !tbaa !8
  %479 = load float, float* %w81, align 4, !tbaa !8
  %call389 = call float @sub_float(float %478, float %479)
  %call390 = call float @mul_float(float 0x3FE6A09EE0000000, float %call389)
  %call391 = call float @add_float(float %477, float %call390)
  call void @store_float(float* %add.ptr388, float %call391)
  %480 = load float*, float** %output.addr, align 4, !tbaa !2
  %481 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul392 = mul nsw i32 5, %481
  %add.ptr393 = getelementptr inbounds float, float* %480, i32 %mul392
  %arrayidx394 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %482 = load float, float* %arrayidx394, align 4, !tbaa !8
  %arrayidx395 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 0
  %483 = load float, float* %arrayidx395, align 4, !tbaa !8
  %call396 = call float @mul_float(float 0x3FE1C73AC0000000, float %483)
  %arrayidx397 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 1
  %484 = load float, float* %arrayidx397, align 4, !tbaa !8
  %call398 = call float @mul_float(float 0x3FEA9B6700000000, float %484)
  %call399 = call float @add_float(float %call396, float %call398)
  %call400 = call float @add_float(float %482, float %call399)
  call void @store_float(float* %add.ptr393, float %call400)
  %485 = load float*, float** %output.addr, align 4, !tbaa !2
  %486 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul401 = mul nsw i32 6, %486
  %add.ptr402 = getelementptr inbounds float, float* %485, i32 %mul401
  %arrayidx403 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %487 = load float, float* %arrayidx403, align 4, !tbaa !8
  %arrayidx404 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 0
  %488 = load float, float* %arrayidx404, align 4, !tbaa !8
  %call405 = call float @mul_float(float 0x3FD87DE0E0000000, float %488)
  %arrayidx406 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 1
  %489 = load float, float* %arrayidx406, align 4, !tbaa !8
  %call407 = call float @mul_float(float 0x3FED906CC0000000, float %489)
  %call408 = call float @add_float(float %call405, float %call407)
  %call409 = call float @add_float(float %487, float %call408)
  call void @store_float(float* %add.ptr402, float %call409)
  %490 = load float*, float** %output.addr, align 4, !tbaa !2
  %491 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul410 = mul nsw i32 7, %491
  %add.ptr411 = getelementptr inbounds float, float* %490, i32 %mul410
  %arrayidx412 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 0
  %492 = load float, float* %arrayidx412, align 4, !tbaa !8
  %arrayidx413 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 0
  %493 = load float, float* %arrayidx413, align 4, !tbaa !8
  %call414 = call float @mul_float(float 0x3FC8F8B580000000, float %493)
  %arrayidx415 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 1
  %494 = load float, float* %arrayidx415, align 4, !tbaa !8
  %call416 = call float @mul_float(float 0x3FEF629740000000, float %494)
  %call417 = call float @add_float(float %call414, float %call416)
  %call418 = call float @add_float(float %492, float %call417)
  call void @store_float(float* %add.ptr411, float %call418)
  %495 = load float*, float** %output.addr, align 4, !tbaa !2
  %496 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul419 = mul nsw i32 8, %496
  %add.ptr420 = getelementptr inbounds float, float* %495, i32 %mul419
  %497 = load float, float* %w39, align 4, !tbaa !8
  call void @store_float(float* %add.ptr420, float %497)
  %498 = load float*, float** %output.addr, align 4, !tbaa !2
  %499 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul421 = mul nsw i32 9, %499
  %add.ptr422 = getelementptr inbounds float, float* %498, i32 %mul421
  %arrayidx423 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 0
  %500 = load float, float* %arrayidx423, align 4, !tbaa !8
  %arrayidx424 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 0
  %501 = load float, float* %arrayidx424, align 4, !tbaa !8
  %call425 = call float @mul_float(float 0x3FC8F8B580000000, float %501)
  %call426 = call float @sub_float(float 0.000000e+00, float %call425)
  %arrayidx427 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 1
  %502 = load float, float* %arrayidx427, align 4, !tbaa !8
  %call428 = call float @mul_float(float 0x3FEF629740000000, float %502)
  %call429 = call float @sub_float(float %call426, float %call428)
  %call430 = call float @add_float(float %500, float %call429)
  call void @store_float(float* %add.ptr422, float %call430)
  %503 = load float*, float** %output.addr, align 4, !tbaa !2
  %504 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul431 = mul nsw i32 10, %504
  %add.ptr432 = getelementptr inbounds float, float* %503, i32 %mul431
  %arrayidx433 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %505 = load float, float* %arrayidx433, align 4, !tbaa !8
  %arrayidx434 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 0
  %506 = load float, float* %arrayidx434, align 4, !tbaa !8
  %call435 = call float @mul_float(float 0x3FD87DE0E0000000, float %506)
  %call436 = call float @sub_float(float 0.000000e+00, float %call435)
  %arrayidx437 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 1
  %507 = load float, float* %arrayidx437, align 4, !tbaa !8
  %call438 = call float @mul_float(float 0x3FED906CC0000000, float %507)
  %call439 = call float @sub_float(float %call436, float %call438)
  %call440 = call float @add_float(float %505, float %call439)
  call void @store_float(float* %add.ptr432, float %call440)
  %508 = load float*, float** %output.addr, align 4, !tbaa !2
  %509 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul441 = mul nsw i32 11, %509
  %add.ptr442 = getelementptr inbounds float, float* %508, i32 %mul441
  %arrayidx443 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %510 = load float, float* %arrayidx443, align 4, !tbaa !8
  %arrayidx444 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 0
  %511 = load float, float* %arrayidx444, align 4, !tbaa !8
  %call445 = call float @mul_float(float 0x3FE1C73AC0000000, float %511)
  %call446 = call float @sub_float(float 0.000000e+00, float %call445)
  %arrayidx447 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 1
  %512 = load float, float* %arrayidx447, align 4, !tbaa !8
  %call448 = call float @mul_float(float 0x3FEA9B6700000000, float %512)
  %call449 = call float @sub_float(float %call446, float %call448)
  %call450 = call float @add_float(float %510, float %call449)
  call void @store_float(float* %add.ptr442, float %call450)
  %513 = load float*, float** %output.addr, align 4, !tbaa !2
  %514 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul451 = mul nsw i32 12, %514
  %add.ptr452 = getelementptr inbounds float, float* %513, i32 %mul451
  %515 = load float, float* %w15, align 4, !tbaa !8
  %516 = load float, float* %w62, align 4, !tbaa !8
  %517 = load float, float* %w81, align 4, !tbaa !8
  %call453 = call float @sub_float(float %516, float %517)
  %call454 = call float @mul_float(float 0x3FE6A09EE0000000, float %call453)
  %call455 = call float @sub_float(float %515, float %call454)
  call void @store_float(float* %add.ptr452, float %call455)
  %518 = load float*, float** %output.addr, align 4, !tbaa !2
  %519 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul456 = mul nsw i32 13, %519
  %add.ptr457 = getelementptr inbounds float, float* %518, i32 %mul456
  %arrayidx458 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 0
  %520 = load float, float* %arrayidx458, align 4, !tbaa !8
  %arrayidx459 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 0
  %521 = load float, float* %arrayidx459, align 4, !tbaa !8
  %call460 = call float @mul_float(float 0x3FEA9B6700000000, float %521)
  %call461 = call float @sub_float(float 0.000000e+00, float %call460)
  %arrayidx462 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 1
  %522 = load float, float* %arrayidx462, align 4, !tbaa !8
  %call463 = call float @mul_float(float 0x3FE1C73AC0000000, float %522)
  %call464 = call float @sub_float(float %call461, float %call463)
  %call465 = call float @add_float(float %520, float %call464)
  call void @store_float(float* %add.ptr457, float %call465)
  %523 = load float*, float** %output.addr, align 4, !tbaa !2
  %524 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul466 = mul nsw i32 14, %524
  %add.ptr467 = getelementptr inbounds float, float* %523, i32 %mul466
  %arrayidx468 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 0
  %525 = load float, float* %arrayidx468, align 4, !tbaa !8
  %arrayidx469 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 0
  %526 = load float, float* %arrayidx469, align 4, !tbaa !8
  %call470 = call float @mul_float(float 0x3FED906CC0000000, float %526)
  %call471 = call float @sub_float(float 0.000000e+00, float %call470)
  %arrayidx472 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 1
  %527 = load float, float* %arrayidx472, align 4, !tbaa !8
  %call473 = call float @mul_float(float 0x3FD87DE0E0000000, float %527)
  %call474 = call float @sub_float(float %call471, float %call473)
  %call475 = call float @add_float(float %525, float %call474)
  call void @store_float(float* %add.ptr467, float %call475)
  %528 = load float*, float** %output.addr, align 4, !tbaa !2
  %529 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul476 = mul nsw i32 15, %529
  %add.ptr477 = getelementptr inbounds float, float* %528, i32 %mul476
  %arrayidx478 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 0
  %530 = load float, float* %arrayidx478, align 4, !tbaa !8
  %arrayidx479 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 0
  %531 = load float, float* %arrayidx479, align 4, !tbaa !8
  %call480 = call float @mul_float(float 0x3FEF629740000000, float %531)
  %call481 = call float @sub_float(float 0.000000e+00, float %call480)
  %arrayidx482 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 1
  %532 = load float, float* %arrayidx482, align 4, !tbaa !8
  %call483 = call float @mul_float(float 0x3FC8F8B580000000, float %532)
  %call484 = call float @sub_float(float %call481, float %call483)
  %call485 = call float @add_float(float %530, float %call484)
  call void @store_float(float* %add.ptr477, float %call485)
  %533 = load float*, float** %output.addr, align 4, !tbaa !2
  %534 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul486 = mul nsw i32 16, %534
  %add.ptr487 = getelementptr inbounds float, float* %533, i32 %mul486
  %535 = load float, float* %w38, align 4, !tbaa !8
  %536 = load float, float* %w85, align 4, !tbaa !8
  %call488 = call float @sub_float(float %535, float %536)
  call void @store_float(float* %add.ptr487, float %call488)
  %537 = load float*, float** %output.addr, align 4, !tbaa !2
  %538 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul489 = mul nsw i32 17, %538
  %add.ptr490 = getelementptr inbounds float, float* %537, i32 %mul489
  %arrayidx491 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 1
  %539 = load float, float* %arrayidx491, align 4, !tbaa !8
  %arrayidx492 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 1
  %540 = load float, float* %arrayidx492, align 4, !tbaa !8
  %call493 = call float @mul_float(float 0x3FEF629740000000, float %540)
  %arrayidx494 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 0
  %541 = load float, float* %arrayidx494, align 4, !tbaa !8
  %call495 = call float @mul_float(float 0x3FC8F8B580000000, float %541)
  %call496 = call float @sub_float(float %call493, float %call495)
  %call497 = call float @add_float(float %539, float %call496)
  call void @store_float(float* %add.ptr490, float %call497)
  %542 = load float*, float** %output.addr, align 4, !tbaa !2
  %543 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul498 = mul nsw i32 18, %543
  %add.ptr499 = getelementptr inbounds float, float* %542, i32 %mul498
  %arrayidx500 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 1
  %544 = load float, float* %arrayidx500, align 4, !tbaa !8
  %arrayidx501 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 1
  %545 = load float, float* %arrayidx501, align 4, !tbaa !8
  %call502 = call float @mul_float(float 0x3FED906CC0000000, float %545)
  %arrayidx503 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 0
  %546 = load float, float* %arrayidx503, align 4, !tbaa !8
  %call504 = call float @mul_float(float 0x3FD87DE0E0000000, float %546)
  %call505 = call float @sub_float(float %call502, float %call504)
  %call506 = call float @add_float(float %544, float %call505)
  call void @store_float(float* %add.ptr499, float %call506)
  %547 = load float*, float** %output.addr, align 4, !tbaa !2
  %548 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul507 = mul nsw i32 19, %548
  %add.ptr508 = getelementptr inbounds float, float* %547, i32 %mul507
  %arrayidx509 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 1
  %549 = load float, float* %arrayidx509, align 4, !tbaa !8
  %arrayidx510 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 1
  %550 = load float, float* %arrayidx510, align 4, !tbaa !8
  %call511 = call float @mul_float(float 0x3FEA9B6700000000, float %550)
  %arrayidx512 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 0
  %551 = load float, float* %arrayidx512, align 4, !tbaa !8
  %call513 = call float @mul_float(float 0x3FE1C73AC0000000, float %551)
  %call514 = call float @sub_float(float %call511, float %call513)
  %call515 = call float @add_float(float %549, float %call514)
  call void @store_float(float* %add.ptr508, float %call515)
  %552 = load float*, float** %output.addr, align 4, !tbaa !2
  %553 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul516 = mul nsw i32 20, %553
  %add.ptr517 = getelementptr inbounds float, float* %552, i32 %mul516
  %554 = load float, float* %w34, align 4, !tbaa !8
  %call518 = call float @sub_float(float 0.000000e+00, float %554)
  %555 = load float, float* %w81, align 4, !tbaa !8
  %556 = load float, float* %w62, align 4, !tbaa !8
  %call519 = call float @add_float(float %555, float %556)
  %call520 = call float @mul_float(float 0x3FE6A09EE0000000, float %call519)
  %call521 = call float @sub_float(float %call518, float %call520)
  call void @store_float(float* %add.ptr517, float %call521)
  %557 = load float*, float** %output.addr, align 4, !tbaa !2
  %558 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul522 = mul nsw i32 21, %558
  %add.ptr523 = getelementptr inbounds float, float* %557, i32 %mul522
  %arrayidx524 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 1
  %559 = load float, float* %arrayidx524, align 4, !tbaa !8
  %arrayidx525 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 1
  %560 = load float, float* %arrayidx525, align 4, !tbaa !8
  %call526 = call float @mul_float(float 0x3FE1C73AC0000000, float %560)
  %arrayidx527 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 0
  %561 = load float, float* %arrayidx527, align 4, !tbaa !8
  %call528 = call float @mul_float(float 0x3FEA9B6700000000, float %561)
  %call529 = call float @sub_float(float %call526, float %call528)
  %call530 = call float @add_float(float %559, float %call529)
  call void @store_float(float* %add.ptr523, float %call530)
  %562 = load float*, float** %output.addr, align 4, !tbaa !2
  %563 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul531 = mul nsw i32 22, %563
  %add.ptr532 = getelementptr inbounds float, float* %562, i32 %mul531
  %arrayidx533 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 1
  %564 = load float, float* %arrayidx533, align 4, !tbaa !8
  %arrayidx534 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 1
  %565 = load float, float* %arrayidx534, align 4, !tbaa !8
  %call535 = call float @mul_float(float 0x3FD87DE0E0000000, float %565)
  %arrayidx536 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 0
  %566 = load float, float* %arrayidx536, align 4, !tbaa !8
  %call537 = call float @mul_float(float 0x3FED906CC0000000, float %566)
  %call538 = call float @sub_float(float %call535, float %call537)
  %call539 = call float @add_float(float %564, float %call538)
  call void @store_float(float* %add.ptr532, float %call539)
  %567 = load float*, float** %output.addr, align 4, !tbaa !2
  %568 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul540 = mul nsw i32 23, %568
  %add.ptr541 = getelementptr inbounds float, float* %567, i32 %mul540
  %arrayidx542 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 1
  %569 = load float, float* %arrayidx542, align 4, !tbaa !8
  %arrayidx543 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 1
  %570 = load float, float* %arrayidx543, align 4, !tbaa !8
  %call544 = call float @mul_float(float 0x3FC8F8B580000000, float %570)
  %arrayidx545 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 0
  %571 = load float, float* %arrayidx545, align 4, !tbaa !8
  %call546 = call float @mul_float(float 0x3FEF629740000000, float %571)
  %call547 = call float @sub_float(float %call544, float %call546)
  %call548 = call float @add_float(float %569, float %call547)
  call void @store_float(float* %add.ptr541, float %call548)
  %572 = load float*, float** %output.addr, align 4, !tbaa !2
  %573 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul549 = mul nsw i32 24, %573
  %add.ptr550 = getelementptr inbounds float, float* %572, i32 %mul549
  %574 = load float, float* %w86, align 4, !tbaa !8
  %call551 = call float @sub_float(float 0.000000e+00, float %574)
  call void @store_float(float* %add.ptr550, float %call551)
  %575 = load float*, float** %output.addr, align 4, !tbaa !2
  %576 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul552 = mul nsw i32 25, %576
  %add.ptr553 = getelementptr inbounds float, float* %575, i32 %mul552
  %arrayidx554 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 1
  %577 = load float, float* %arrayidx554, align 4, !tbaa !8
  %call555 = call float @sub_float(float 0.000000e+00, float %577)
  %arrayidx556 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 0
  %578 = load float, float* %arrayidx556, align 4, !tbaa !8
  %call557 = call float @mul_float(float 0x3FEF629740000000, float %578)
  %arrayidx558 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 1
  %579 = load float, float* %arrayidx558, align 4, !tbaa !8
  %call559 = call float @mul_float(float 0x3FC8F8B580000000, float %579)
  %call560 = call float @sub_float(float %call557, float %call559)
  %call561 = call float @sub_float(float %call555, float %call560)
  call void @store_float(float* %add.ptr553, float %call561)
  %580 = load float*, float** %output.addr, align 4, !tbaa !2
  %581 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul562 = mul nsw i32 26, %581
  %add.ptr563 = getelementptr inbounds float, float* %580, i32 %mul562
  %arrayidx564 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 1
  %582 = load float, float* %arrayidx564, align 4, !tbaa !8
  %call565 = call float @sub_float(float 0.000000e+00, float %582)
  %arrayidx566 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 0
  %583 = load float, float* %arrayidx566, align 4, !tbaa !8
  %call567 = call float @mul_float(float 0x3FED906CC0000000, float %583)
  %arrayidx568 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 1
  %584 = load float, float* %arrayidx568, align 4, !tbaa !8
  %call569 = call float @mul_float(float 0x3FD87DE0E0000000, float %584)
  %call570 = call float @sub_float(float %call567, float %call569)
  %call571 = call float @sub_float(float %call565, float %call570)
  call void @store_float(float* %add.ptr563, float %call571)
  %585 = load float*, float** %output.addr, align 4, !tbaa !2
  %586 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul572 = mul nsw i32 27, %586
  %add.ptr573 = getelementptr inbounds float, float* %585, i32 %mul572
  %arrayidx574 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 1
  %587 = load float, float* %arrayidx574, align 4, !tbaa !8
  %call575 = call float @sub_float(float 0.000000e+00, float %587)
  %arrayidx576 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 0
  %588 = load float, float* %arrayidx576, align 4, !tbaa !8
  %call577 = call float @mul_float(float 0x3FEA9B6700000000, float %588)
  %arrayidx578 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 1
  %589 = load float, float* %arrayidx578, align 4, !tbaa !8
  %call579 = call float @mul_float(float 0x3FE1C73AC0000000, float %589)
  %call580 = call float @sub_float(float %call577, float %call579)
  %call581 = call float @sub_float(float %call575, float %call580)
  call void @store_float(float* %add.ptr573, float %call581)
  %590 = load float*, float** %output.addr, align 4, !tbaa !2
  %591 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul582 = mul nsw i32 28, %591
  %add.ptr583 = getelementptr inbounds float, float* %590, i32 %mul582
  %592 = load float, float* %w34, align 4, !tbaa !8
  %593 = load float, float* %w81, align 4, !tbaa !8
  %594 = load float, float* %w62, align 4, !tbaa !8
  %call584 = call float @add_float(float %593, float %594)
  %call585 = call float @mul_float(float 0x3FE6A09EE0000000, float %call584)
  %call586 = call float @sub_float(float %592, float %call585)
  call void @store_float(float* %add.ptr583, float %call586)
  %595 = load float*, float** %output.addr, align 4, !tbaa !2
  %596 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul587 = mul nsw i32 29, %596
  %add.ptr588 = getelementptr inbounds float, float* %595, i32 %mul587
  %arrayidx589 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 1
  %597 = load float, float* %arrayidx589, align 4, !tbaa !8
  %call590 = call float @sub_float(float 0.000000e+00, float %597)
  %arrayidx591 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 0
  %598 = load float, float* %arrayidx591, align 4, !tbaa !8
  %call592 = call float @mul_float(float 0x3FE1C73AC0000000, float %598)
  %arrayidx593 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 1
  %599 = load float, float* %arrayidx593, align 4, !tbaa !8
  %call594 = call float @mul_float(float 0x3FEA9B6700000000, float %599)
  %call595 = call float @sub_float(float %call592, float %call594)
  %call596 = call float @sub_float(float %call590, float %call595)
  call void @store_float(float* %add.ptr588, float %call596)
  %600 = load float*, float** %output.addr, align 4, !tbaa !2
  %601 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul597 = mul nsw i32 30, %601
  %add.ptr598 = getelementptr inbounds float, float* %600, i32 %mul597
  %arrayidx599 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 1
  %602 = load float, float* %arrayidx599, align 4, !tbaa !8
  %call600 = call float @sub_float(float 0.000000e+00, float %602)
  %arrayidx601 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 0
  %603 = load float, float* %arrayidx601, align 4, !tbaa !8
  %call602 = call float @mul_float(float 0x3FD87DE0E0000000, float %603)
  %arrayidx603 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 1
  %604 = load float, float* %arrayidx603, align 4, !tbaa !8
  %call604 = call float @mul_float(float 0x3FED906CC0000000, float %604)
  %call605 = call float @sub_float(float %call602, float %call604)
  %call606 = call float @sub_float(float %call600, float %call605)
  call void @store_float(float* %add.ptr598, float %call606)
  %605 = load float*, float** %output.addr, align 4, !tbaa !2
  %606 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul607 = mul nsw i32 31, %606
  %add.ptr608 = getelementptr inbounds float, float* %605, i32 %mul607
  %arrayidx609 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 1
  %607 = load float, float* %arrayidx609, align 4, !tbaa !8
  %call610 = call float @sub_float(float 0.000000e+00, float %607)
  %arrayidx611 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 0
  %608 = load float, float* %arrayidx611, align 4, !tbaa !8
  %call612 = call float @mul_float(float 0x3FC8F8B580000000, float %608)
  %arrayidx613 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 1
  %609 = load float, float* %arrayidx613, align 4, !tbaa !8
  %call614 = call float @mul_float(float 0x3FEF629740000000, float %609)
  %call615 = call float @sub_float(float %call612, float %call614)
  %call616 = call float @sub_float(float %call610, float %call615)
  call void @store_float(float* %add.ptr608, float %call616)
  %610 = bitcast [2 x float]* %w93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %610) #3
  %611 = bitcast [2 x float]* %w92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %611) #3
  %612 = bitcast [2 x float]* %w91 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %612) #3
  %613 = bitcast [2 x float]* %w89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %613) #3
  %614 = bitcast [2 x float]* %w88 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %614) #3
  %615 = bitcast [2 x float]* %w87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %615) #3
  %616 = bitcast float* %w86 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %616) #3
  %617 = bitcast float* %w85 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %617) #3
  %618 = bitcast [2 x float]* %w84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %618) #3
  %619 = bitcast [2 x float]* %w82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %619) #3
  %620 = bitcast float* %w81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %620) #3
  %621 = bitcast float* %w80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %621) #3
  %622 = bitcast float* %w78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %622) #3
  %623 = bitcast float* %w77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %623) #3
  %624 = bitcast float* %w76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %624) #3
  %625 = bitcast float* %w75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %625) #3
  %626 = bitcast float* %w74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %626) #3
  %627 = bitcast float* %w73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %627) #3
  %628 = bitcast float* %w71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %628) #3
  %629 = bitcast float* %w70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %629) #3
  %630 = bitcast float* %w69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %630) #3
  %631 = bitcast float* %w68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %631) #3
  %632 = bitcast float* %w67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %632) #3
  %633 = bitcast float* %w66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %633) #3
  %634 = bitcast [2 x float]* %w65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %634) #3
  %635 = bitcast [2 x float]* %w63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %635) #3
  %636 = bitcast float* %w62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %636) #3
  %637 = bitcast float* %w61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %637) #3
  %638 = bitcast float* %w59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %638) #3
  %639 = bitcast float* %w58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %639) #3
  %640 = bitcast float* %w57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %640) #3
  %641 = bitcast float* %w56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %641) #3
  %642 = bitcast float* %w55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %642) #3
  %643 = bitcast float* %w54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %643) #3
  %644 = bitcast float* %w52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %644) #3
  %645 = bitcast float* %w51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %645) #3
  %646 = bitcast float* %w50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %646) #3
  %647 = bitcast float* %w49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %647) #3
  %648 = bitcast float* %w48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %648) #3
  %649 = bitcast float* %w47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %649) #3
  %650 = bitcast [2 x float]* %w46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %650) #3
  %651 = bitcast [2 x float]* %w45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %651) #3
  %652 = bitcast [2 x float]* %w44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %652) #3
  %653 = bitcast [2 x float]* %w42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %653) #3
  %654 = bitcast [2 x float]* %w41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %654) #3
  %655 = bitcast [2 x float]* %w40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %655) #3
  %656 = bitcast float* %w39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %656) #3
  %657 = bitcast float* %w38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %657) #3
  %658 = bitcast [2 x float]* %w37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %658) #3
  %659 = bitcast [2 x float]* %w35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %659) #3
  %660 = bitcast float* %w34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %660) #3
  %661 = bitcast float* %w33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %661) #3
  %662 = bitcast float* %w31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %662) #3
  %663 = bitcast float* %w30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %663) #3
  %664 = bitcast float* %w29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %664) #3
  %665 = bitcast float* %w28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %665) #3
  %666 = bitcast float* %w27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %666) #3
  %667 = bitcast float* %w26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %667) #3
  %668 = bitcast float* %w24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %668) #3
  %669 = bitcast float* %w23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %669) #3
  %670 = bitcast float* %w22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %670) #3
  %671 = bitcast float* %w21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %671) #3
  %672 = bitcast float* %w20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %672) #3
  %673 = bitcast float* %w19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %673) #3
  %674 = bitcast [2 x float]* %w18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %674) #3
  %675 = bitcast [2 x float]* %w16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %675) #3
  %676 = bitcast float* %w15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %676) #3
  %677 = bitcast float* %w14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %677) #3
  %678 = bitcast float* %w12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %678) #3
  %679 = bitcast float* %w11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %679) #3
  %680 = bitcast float* %w10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %680) #3
  %681 = bitcast float* %w9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %681) #3
  %682 = bitcast float* %w8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %682) #3
  %683 = bitcast float* %w7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %683) #3
  %684 = bitcast float* %w5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %684) #3
  %685 = bitcast float* %w4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %685) #3
  %686 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %686) #3
  %687 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %687) #3
  %688 = bitcast float* %w1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %688) #3
  %689 = bitcast float* %w0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %689) #3
  %690 = bitcast float* %i31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %690) #3
  %691 = bitcast float* %i30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %691) #3
  %692 = bitcast float* %i29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %692) #3
  %693 = bitcast float* %i28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %693) #3
  %694 = bitcast float* %i27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %694) #3
  %695 = bitcast float* %i26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %695) #3
  %696 = bitcast float* %i25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %696) #3
  %697 = bitcast float* %i24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %697) #3
  %698 = bitcast float* %i23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %698) #3
  %699 = bitcast float* %i22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %699) #3
  %700 = bitcast float* %i21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %700) #3
  %701 = bitcast float* %i20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %701) #3
  %702 = bitcast float* %i19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %702) #3
  %703 = bitcast float* %i18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %703) #3
  %704 = bitcast float* %i17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %704) #3
  %705 = bitcast float* %i16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %705) #3
  %706 = bitcast float* %i15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %706) #3
  %707 = bitcast float* %i14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %707) #3
  %708 = bitcast float* %i13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %708) #3
  %709 = bitcast float* %i12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %709) #3
  %710 = bitcast float* %i11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %710) #3
  %711 = bitcast float* %i10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %711) #3
  %712 = bitcast float* %i9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %712) #3
  %713 = bitcast float* %i8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %713) #3
  %714 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %714) #3
  %715 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %715) #3
  %716 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %716) #3
  %717 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %717) #3
  %718 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %718) #3
  %719 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %719) #3
  %720 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %720) #3
  %721 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %721) #3
  %722 = bitcast float* %kWeight8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %722) #3
  %723 = bitcast float* %kWeight7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %723) #3
  %724 = bitcast float* %kWeight6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %724) #3
  %725 = bitcast float* %kWeight5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %725) #3
  %726 = bitcast float* %kWeight4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %726) #3
  %727 = bitcast float* %kWeight3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %727) #3
  %728 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %728) #3
  %729 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %729) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_fft2x2_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_fft_2d_gen(float* %0, float* %1, float* %2, i32 2, void (float*, float*, i32)* @aom_fft1d_2_float, void (float*, float*, i32)* @simple_transpose, void (float*, float*, i32)* @unpack_2d_output, i32 1)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @simple_transpose(float* %A, float* %B, i32 %n) #2 {
entry:
  %A.addr = alloca float*, align 4
  %B.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  store float* %A, float** %A.addr, align 4, !tbaa !2
  store float* %B, float** %B.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc8, %entry
  %1 = load i32, i32* %y, align 4, !tbaa !6
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  br label %for.end10

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %x, align 4, !tbaa !6
  %6 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #3
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load float*, float** %A.addr, align 4, !tbaa !2
  %9 = load i32, i32* %x, align 4, !tbaa !6
  %10 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %9, %10
  %11 = load i32, i32* %y, align 4, !tbaa !6
  %add = add nsw i32 %mul, %11
  %arrayidx = getelementptr inbounds float, float* %8, i32 %add
  %12 = load float, float* %arrayidx, align 4, !tbaa !8
  %13 = load float*, float** %B.addr, align 4, !tbaa !2
  %14 = load i32, i32* %y, align 4, !tbaa !6
  %15 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 %14, %15
  %16 = load i32, i32* %x, align 4, !tbaa !6
  %add6 = add nsw i32 %mul5, %16
  %arrayidx7 = getelementptr inbounds float, float* %13, i32 %add6
  store float %12, float* %arrayidx7, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %17 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc8

for.inc8:                                         ; preds = %for.end
  %18 = load i32, i32* %y, align 4, !tbaa !6
  %inc9 = add nsw i32 %18, 1
  store i32 %inc9, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end10:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @unpack_2d_output(float* %col_fft, float* %output, i32 %n) #2 {
entry:
  %col_fft.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %y2 = alloca i32, align 4
  %y_extra = alloca i32, align 4
  %x = alloca i32, align 4
  %x2 = alloca i32, align 4
  %x_extra = alloca i32, align 4
  store float* %col_fft, float** %col_fft.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc92, %entry
  %1 = load i32, i32* %y, align 4, !tbaa !6
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div = sdiv i32 %2, 2
  %cmp = icmp sle i32 %1, %div
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  br label %for.end94

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %y2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i32, i32* %y, align 4, !tbaa !6
  %6 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div1 = sdiv i32 %6, 2
  %add = add nsw i32 %5, %div1
  store i32 %add, i32* %y2, align 4, !tbaa !6
  %7 = bitcast i32* %y_extra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load i32, i32* %y2, align 4, !tbaa !6
  %9 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div2 = sdiv i32 %9, 2
  %cmp3 = icmp sgt i32 %8, %div2
  br i1 %cmp3, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.body
  %10 = load i32, i32* %y2, align 4, !tbaa !6
  %11 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %10, %11
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.body
  %12 = phi i1 [ false, %for.body ], [ %cmp4, %land.rhs ]
  %land.ext = zext i1 %12 to i32
  store i32 %land.ext, i32* %y_extra, align 4, !tbaa !6
  %13 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %land.end
  %14 = load i32, i32* %x, align 4, !tbaa !6
  %15 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div6 = sdiv i32 %15, 2
  %cmp7 = icmp sle i32 %14, %div6
  br i1 %cmp7, label %for.body9, label %for.cond.cleanup8

for.cond.cleanup8:                                ; preds = %for.cond5
  store i32 5, i32* %cleanup.dest.slot, align 4
  %16 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  br label %for.end

for.body9:                                        ; preds = %for.cond5
  %17 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load i32, i32* %x, align 4, !tbaa !6
  %19 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div10 = sdiv i32 %19, 2
  %add11 = add nsw i32 %18, %div10
  store i32 %add11, i32* %x2, align 4, !tbaa !6
  %20 = bitcast i32* %x_extra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load i32, i32* %x2, align 4, !tbaa !6
  %22 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div12 = sdiv i32 %22, 2
  %cmp13 = icmp sgt i32 %21, %div12
  br i1 %cmp13, label %land.rhs14, label %land.end16

land.rhs14:                                       ; preds = %for.body9
  %23 = load i32, i32* %x2, align 4, !tbaa !6
  %24 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp15 = icmp slt i32 %23, %24
  br label %land.end16

land.end16:                                       ; preds = %land.rhs14, %for.body9
  %25 = phi i1 [ false, %for.body9 ], [ %cmp15, %land.rhs14 ]
  %land.ext17 = zext i1 %25 to i32
  store i32 %land.ext17, i32* %x_extra, align 4, !tbaa !6
  %26 = load float*, float** %col_fft.addr, align 4, !tbaa !2
  %27 = load i32, i32* %y, align 4, !tbaa !6
  %28 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %27, %28
  %29 = load i32, i32* %x, align 4, !tbaa !6
  %add18 = add nsw i32 %mul, %29
  %arrayidx = getelementptr inbounds float, float* %26, i32 %add18
  %30 = load float, float* %arrayidx, align 4, !tbaa !8
  %31 = load i32, i32* %x_extra, align 4, !tbaa !6
  %tobool = icmp ne i32 %31, 0
  br i1 %tobool, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %land.end16
  %32 = load i32, i32* %y_extra, align 4, !tbaa !6
  %tobool19 = icmp ne i32 %32, 0
  br i1 %tobool19, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %33 = load float*, float** %col_fft.addr, align 4, !tbaa !2
  %34 = load i32, i32* %y2, align 4, !tbaa !6
  %35 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 %34, %35
  %36 = load i32, i32* %x2, align 4, !tbaa !6
  %add21 = add nsw i32 %mul20, %36
  %arrayidx22 = getelementptr inbounds float, float* %33, i32 %add21
  %37 = load float, float* %arrayidx22, align 4, !tbaa !8
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %land.end16
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %37, %cond.true ], [ 0.000000e+00, %cond.false ]
  %sub = fsub float %30, %cond
  %38 = load float*, float** %output.addr, align 4, !tbaa !2
  %39 = load i32, i32* %y, align 4, !tbaa !6
  %40 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 %39, %40
  %41 = load i32, i32* %x, align 4, !tbaa !6
  %add24 = add nsw i32 %mul23, %41
  %mul25 = mul nsw i32 2, %add24
  %arrayidx26 = getelementptr inbounds float, float* %38, i32 %mul25
  store float %sub, float* %arrayidx26, align 4, !tbaa !8
  %42 = load i32, i32* %y_extra, align 4, !tbaa !6
  %tobool27 = icmp ne i32 %42, 0
  br i1 %tobool27, label %cond.true28, label %cond.false32

cond.true28:                                      ; preds = %cond.end
  %43 = load float*, float** %col_fft.addr, align 4, !tbaa !2
  %44 = load i32, i32* %y2, align 4, !tbaa !6
  %45 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 %44, %45
  %46 = load i32, i32* %x, align 4, !tbaa !6
  %add30 = add nsw i32 %mul29, %46
  %arrayidx31 = getelementptr inbounds float, float* %43, i32 %add30
  %47 = load float, float* %arrayidx31, align 4, !tbaa !8
  br label %cond.end33

cond.false32:                                     ; preds = %cond.end
  br label %cond.end33

cond.end33:                                       ; preds = %cond.false32, %cond.true28
  %cond34 = phi float [ %47, %cond.true28 ], [ 0.000000e+00, %cond.false32 ]
  %48 = load i32, i32* %x_extra, align 4, !tbaa !6
  %tobool35 = icmp ne i32 %48, 0
  br i1 %tobool35, label %cond.true36, label %cond.false40

cond.true36:                                      ; preds = %cond.end33
  %49 = load float*, float** %col_fft.addr, align 4, !tbaa !2
  %50 = load i32, i32* %y, align 4, !tbaa !6
  %51 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul37 = mul nsw i32 %50, %51
  %52 = load i32, i32* %x2, align 4, !tbaa !6
  %add38 = add nsw i32 %mul37, %52
  %arrayidx39 = getelementptr inbounds float, float* %49, i32 %add38
  %53 = load float, float* %arrayidx39, align 4, !tbaa !8
  br label %cond.end41

cond.false40:                                     ; preds = %cond.end33
  br label %cond.end41

cond.end41:                                       ; preds = %cond.false40, %cond.true36
  %cond42 = phi float [ %53, %cond.true36 ], [ 0.000000e+00, %cond.false40 ]
  %add43 = fadd float %cond34, %cond42
  %54 = load float*, float** %output.addr, align 4, !tbaa !2
  %55 = load i32, i32* %y, align 4, !tbaa !6
  %56 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul44 = mul nsw i32 %55, %56
  %57 = load i32, i32* %x, align 4, !tbaa !6
  %add45 = add nsw i32 %mul44, %57
  %mul46 = mul nsw i32 2, %add45
  %add47 = add nsw i32 %mul46, 1
  %arrayidx48 = getelementptr inbounds float, float* %54, i32 %add47
  store float %add43, float* %arrayidx48, align 4, !tbaa !8
  %58 = load i32, i32* %y_extra, align 4, !tbaa !6
  %tobool49 = icmp ne i32 %58, 0
  br i1 %tobool49, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end41
  %59 = load float*, float** %col_fft.addr, align 4, !tbaa !2
  %60 = load i32, i32* %y, align 4, !tbaa !6
  %61 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul50 = mul nsw i32 %60, %61
  %62 = load i32, i32* %x, align 4, !tbaa !6
  %add51 = add nsw i32 %mul50, %62
  %arrayidx52 = getelementptr inbounds float, float* %59, i32 %add51
  %63 = load float, float* %arrayidx52, align 4, !tbaa !8
  %64 = load i32, i32* %x_extra, align 4, !tbaa !6
  %tobool53 = icmp ne i32 %64, 0
  br i1 %tobool53, label %land.lhs.true54, label %cond.false60

land.lhs.true54:                                  ; preds = %if.then
  %65 = load i32, i32* %y_extra, align 4, !tbaa !6
  %tobool55 = icmp ne i32 %65, 0
  br i1 %tobool55, label %cond.true56, label %cond.false60

cond.true56:                                      ; preds = %land.lhs.true54
  %66 = load float*, float** %col_fft.addr, align 4, !tbaa !2
  %67 = load i32, i32* %y2, align 4, !tbaa !6
  %68 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul57 = mul nsw i32 %67, %68
  %69 = load i32, i32* %x2, align 4, !tbaa !6
  %add58 = add nsw i32 %mul57, %69
  %arrayidx59 = getelementptr inbounds float, float* %66, i32 %add58
  %70 = load float, float* %arrayidx59, align 4, !tbaa !8
  br label %cond.end61

cond.false60:                                     ; preds = %land.lhs.true54, %if.then
  br label %cond.end61

cond.end61:                                       ; preds = %cond.false60, %cond.true56
  %cond62 = phi float [ %70, %cond.true56 ], [ 0.000000e+00, %cond.false60 ]
  %add63 = fadd float %63, %cond62
  %71 = load float*, float** %output.addr, align 4, !tbaa !2
  %72 = load i32, i32* %n.addr, align 4, !tbaa !6
  %73 = load i32, i32* %y, align 4, !tbaa !6
  %sub64 = sub nsw i32 %72, %73
  %74 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul65 = mul nsw i32 %sub64, %74
  %75 = load i32, i32* %x, align 4, !tbaa !6
  %add66 = add nsw i32 %mul65, %75
  %mul67 = mul nsw i32 2, %add66
  %arrayidx68 = getelementptr inbounds float, float* %71, i32 %mul67
  store float %add63, float* %arrayidx68, align 4, !tbaa !8
  %76 = load i32, i32* %y_extra, align 4, !tbaa !6
  %tobool69 = icmp ne i32 %76, 0
  br i1 %tobool69, label %cond.true70, label %cond.false74

cond.true70:                                      ; preds = %cond.end61
  %77 = load float*, float** %col_fft.addr, align 4, !tbaa !2
  %78 = load i32, i32* %y2, align 4, !tbaa !6
  %79 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul71 = mul nsw i32 %78, %79
  %80 = load i32, i32* %x, align 4, !tbaa !6
  %add72 = add nsw i32 %mul71, %80
  %arrayidx73 = getelementptr inbounds float, float* %77, i32 %add72
  %81 = load float, float* %arrayidx73, align 4, !tbaa !8
  br label %cond.end75

cond.false74:                                     ; preds = %cond.end61
  br label %cond.end75

cond.end75:                                       ; preds = %cond.false74, %cond.true70
  %cond76 = phi float [ %81, %cond.true70 ], [ 0.000000e+00, %cond.false74 ]
  %fneg = fneg float %cond76
  %82 = load i32, i32* %x_extra, align 4, !tbaa !6
  %tobool77 = icmp ne i32 %82, 0
  br i1 %tobool77, label %cond.true78, label %cond.false82

cond.true78:                                      ; preds = %cond.end75
  %83 = load float*, float** %col_fft.addr, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !6
  %85 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul79 = mul nsw i32 %84, %85
  %86 = load i32, i32* %x2, align 4, !tbaa !6
  %add80 = add nsw i32 %mul79, %86
  %arrayidx81 = getelementptr inbounds float, float* %83, i32 %add80
  %87 = load float, float* %arrayidx81, align 4, !tbaa !8
  br label %cond.end83

cond.false82:                                     ; preds = %cond.end75
  br label %cond.end83

cond.end83:                                       ; preds = %cond.false82, %cond.true78
  %cond84 = phi float [ %87, %cond.true78 ], [ 0.000000e+00, %cond.false82 ]
  %add85 = fadd float %fneg, %cond84
  %88 = load float*, float** %output.addr, align 4, !tbaa !2
  %89 = load i32, i32* %n.addr, align 4, !tbaa !6
  %90 = load i32, i32* %y, align 4, !tbaa !6
  %sub86 = sub nsw i32 %89, %90
  %91 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul87 = mul nsw i32 %sub86, %91
  %92 = load i32, i32* %x, align 4, !tbaa !6
  %add88 = add nsw i32 %mul87, %92
  %mul89 = mul nsw i32 2, %add88
  %add90 = add nsw i32 %mul89, 1
  %arrayidx91 = getelementptr inbounds float, float* %88, i32 %add90
  store float %add85, float* %arrayidx91, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %cond.end83, %cond.end41
  %93 = bitcast i32* %x_extra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #3
  %94 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #3
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %95 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %95, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond5

for.end:                                          ; preds = %for.cond.cleanup8
  %96 = bitcast i32* %y_extra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #3
  %97 = bitcast i32* %y2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #3
  br label %for.inc92

for.inc92:                                        ; preds = %for.end
  %98 = load i32, i32* %y, align 4, !tbaa !6
  %inc93 = add nsw i32 %98, 1
  store i32 %inc93, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end94:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_fft4x4_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_fft_2d_gen(float* %0, float* %1, float* %2, i32 4, void (float*, float*, i32)* @aom_fft1d_4_float, void (float*, float*, i32)* @simple_transpose, void (float*, float*, i32)* @unpack_2d_output, i32 1)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_fft8x8_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_fft_2d_gen(float* %0, float* %1, float* %2, i32 8, void (float*, float*, i32)* @aom_fft1d_8_float, void (float*, float*, i32)* @simple_transpose, void (float*, float*, i32)* @unpack_2d_output, i32 1)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_fft16x16_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_fft_2d_gen(float* %0, float* %1, float* %2, i32 16, void (float*, float*, i32)* @aom_fft1d_16_float, void (float*, float*, i32)* @simple_transpose, void (float*, float*, i32)* @unpack_2d_output, i32 1)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_fft32x32_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_fft_2d_gen(float* %0, float* %1, float* %2, i32 32, void (float*, float*, i32)* @aom_fft1d_32_float, void (float*, float*, i32)* @simple_transpose, void (float*, float*, i32)* @unpack_2d_output, i32 1)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft_2d_gen(float* %input, float* %temp, float* %output, i32 %n, void (float*, float*, i32)* %fft_single, void (float*, float*, i32)* %fft_multi, void (float*, float*, i32)* %ifft_multi, void (float*, float*, i32)* %transpose, i32 %vec_size) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %fft_single.addr = alloca void (float*, float*, i32)*, align 4
  %fft_multi.addr = alloca void (float*, float*, i32)*, align 4
  %ifft_multi.addr = alloca void (float*, float*, i32)*, align 4
  %transpose.addr = alloca void (float*, float*, i32)*, align 4
  %vec_size.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %y11 = alloca i32, align 4
  %i = alloca i32, align 4
  %y47 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %x68 = alloca i32, align 4
  %y90 = alloca i32, align 4
  %y100 = alloca i32, align 4
  %x111 = alloca i32, align 4
  %y129 = alloca i32, align 4
  %x135 = alloca i32, align 4
  %x166 = alloca i32, align 4
  %x193 = alloca i32, align 4
  %x227 = alloca i32, align 4
  %y259 = alloca i32, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store void (float*, float*, i32)* %fft_single, void (float*, float*, i32)** %fft_single.addr, align 4, !tbaa !2
  store void (float*, float*, i32)* %fft_multi, void (float*, float*, i32)** %fft_multi.addr, align 4, !tbaa !2
  store void (float*, float*, i32)* %ifft_multi, void (float*, float*, i32)** %ifft_multi.addr, align 4, !tbaa !2
  store void (float*, float*, i32)* %transpose, void (float*, float*, i32)** %transpose.addr, align 4, !tbaa !2
  store i32 %vec_size, i32* %vec_size.addr, align 4, !tbaa !6
  %0 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %y, align 4, !tbaa !6
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div = sdiv i32 %2, 2
  %cmp = icmp sle i32 %1, %div
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %input.addr, align 4, !tbaa !2
  %5 = load i32, i32* %y, align 4, !tbaa !6
  %mul = mul nsw i32 2, %5
  %6 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 %mul, %6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %mul1
  %7 = load float, float* %arrayidx, align 4, !tbaa !8
  %8 = load float*, float** %output.addr, align 4, !tbaa !2
  %9 = load i32, i32* %y, align 4, !tbaa !6
  %10 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 %9, %10
  %arrayidx3 = getelementptr inbounds float, float* %8, i32 %mul2
  store float %7, float* %arrayidx3, align 4, !tbaa !8
  %11 = load float*, float** %input.addr, align 4, !tbaa !2
  %12 = load i32, i32* %y, align 4, !tbaa !6
  %13 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 %12, %13
  %14 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div5 = sdiv i32 %14, 2
  %add = add nsw i32 %mul4, %div5
  %mul6 = mul nsw i32 2, %add
  %arrayidx7 = getelementptr inbounds float, float* %11, i32 %mul6
  %15 = load float, float* %arrayidx7, align 4, !tbaa !8
  %16 = load float*, float** %output.addr, align 4, !tbaa !2
  %17 = load i32, i32* %y, align 4, !tbaa !6
  %18 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul8 = mul nsw i32 %17, %18
  %add9 = add nsw i32 %mul8, 1
  %arrayidx10 = getelementptr inbounds float, float* %16, i32 %add9
  store float %15, float* %arrayidx10, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %y, align 4, !tbaa !6
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %20 = bitcast i32* %y11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div12 = sdiv i32 %21, 2
  %add13 = add nsw i32 %div12, 1
  store i32 %add13, i32* %y11, align 4, !tbaa !6
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc36, %for.end
  %22 = load i32, i32* %y11, align 4, !tbaa !6
  %23 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp15 = icmp slt i32 %22, %23
  br i1 %cmp15, label %for.body17, label %for.cond.cleanup16

for.cond.cleanup16:                               ; preds = %for.cond14
  %24 = bitcast i32* %y11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  br label %for.end38

for.body17:                                       ; preds = %for.cond14
  %25 = load float*, float** %input.addr, align 4, !tbaa !2
  %26 = load i32, i32* %y11, align 4, !tbaa !6
  %27 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div18 = sdiv i32 %27, 2
  %sub = sub nsw i32 %26, %div18
  %mul19 = mul nsw i32 2, %sub
  %28 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 %mul19, %28
  %add21 = add nsw i32 %mul20, 1
  %arrayidx22 = getelementptr inbounds float, float* %25, i32 %add21
  %29 = load float, float* %arrayidx22, align 4, !tbaa !8
  %30 = load float*, float** %output.addr, align 4, !tbaa !2
  %31 = load i32, i32* %y11, align 4, !tbaa !6
  %32 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 %31, %32
  %arrayidx24 = getelementptr inbounds float, float* %30, i32 %mul23
  store float %29, float* %arrayidx24, align 4, !tbaa !8
  %33 = load float*, float** %input.addr, align 4, !tbaa !2
  %34 = load i32, i32* %y11, align 4, !tbaa !6
  %35 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div25 = sdiv i32 %35, 2
  %sub26 = sub nsw i32 %34, %div25
  %36 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul27 = mul nsw i32 %sub26, %36
  %37 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div28 = sdiv i32 %37, 2
  %add29 = add nsw i32 %mul27, %div28
  %mul30 = mul nsw i32 2, %add29
  %add31 = add nsw i32 %mul30, 1
  %arrayidx32 = getelementptr inbounds float, float* %33, i32 %add31
  %38 = load float, float* %arrayidx32, align 4, !tbaa !8
  %39 = load float*, float** %output.addr, align 4, !tbaa !2
  %40 = load i32, i32* %y11, align 4, !tbaa !6
  %41 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul33 = mul nsw i32 %40, %41
  %add34 = add nsw i32 %mul33, 1
  %arrayidx35 = getelementptr inbounds float, float* %39, i32 %add34
  store float %38, float* %arrayidx35, align 4, !tbaa !8
  br label %for.inc36

for.inc36:                                        ; preds = %for.body17
  %42 = load i32, i32* %y11, align 4, !tbaa !6
  %inc37 = add nsw i32 %42, 1
  store i32 %inc37, i32* %y11, align 4, !tbaa !6
  br label %for.cond14

for.end38:                                        ; preds = %for.cond.cleanup16
  %43 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc44, %for.end38
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %cmp40 = icmp slt i32 %44, 2
  br i1 %cmp40, label %for.body42, label %for.cond.cleanup41

for.cond.cleanup41:                               ; preds = %for.cond39
  %45 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  br label %for.end46

for.body42:                                       ; preds = %for.cond39
  %46 = load void (float*, float*, i32)*, void (float*, float*, i32)** %ifft_multi.addr, align 4, !tbaa !2
  %47 = load float*, float** %output.addr, align 4, !tbaa !2
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds float, float* %47, i32 %48
  %49 = load float*, float** %temp.addr, align 4, !tbaa !2
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr43 = getelementptr inbounds float, float* %49, i32 %50
  %51 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %46(float* %add.ptr, float* %add.ptr43, i32 %51)
  br label %for.inc44

for.inc44:                                        ; preds = %for.body42
  %52 = load i32, i32* %vec_size.addr, align 4, !tbaa !6
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %add45 = add nsw i32 %53, %52
  store i32 %add45, i32* %i, align 4, !tbaa !6
  br label %for.cond39

for.end46:                                        ; preds = %for.cond.cleanup41
  %54 = bitcast i32* %y47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #3
  store i32 0, i32* %y47, align 4, !tbaa !6
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc87, %for.end46
  %55 = load i32, i32* %y47, align 4, !tbaa !6
  %56 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp49 = icmp slt i32 %55, %56
  br i1 %cmp49, label %for.body51, label %for.cond.cleanup50

for.cond.cleanup50:                               ; preds = %for.cond48
  store i32 11, i32* %cleanup.dest.slot, align 4
  %57 = bitcast i32* %y47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  br label %for.end89

for.body51:                                       ; preds = %for.cond48
  %58 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #3
  store i32 1, i32* %x, align 4, !tbaa !6
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc65, %for.body51
  %59 = load i32, i32* %x, align 4, !tbaa !6
  %60 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div53 = sdiv i32 %60, 2
  %cmp54 = icmp slt i32 %59, %div53
  br i1 %cmp54, label %for.body56, label %for.cond.cleanup55

for.cond.cleanup55:                               ; preds = %for.cond52
  store i32 14, i32* %cleanup.dest.slot, align 4
  %61 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #3
  br label %for.end67

for.body56:                                       ; preds = %for.cond52
  %62 = load float*, float** %input.addr, align 4, !tbaa !2
  %63 = load i32, i32* %y47, align 4, !tbaa !6
  %64 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul57 = mul nsw i32 %63, %64
  %65 = load i32, i32* %x, align 4, !tbaa !6
  %add58 = add nsw i32 %mul57, %65
  %mul59 = mul nsw i32 2, %add58
  %arrayidx60 = getelementptr inbounds float, float* %62, i32 %mul59
  %66 = load float, float* %arrayidx60, align 4, !tbaa !8
  %67 = load float*, float** %output.addr, align 4, !tbaa !2
  %68 = load i32, i32* %y47, align 4, !tbaa !6
  %69 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul61 = mul nsw i32 %68, %69
  %70 = load i32, i32* %x, align 4, !tbaa !6
  %add62 = add nsw i32 %70, 1
  %add63 = add nsw i32 %mul61, %add62
  %arrayidx64 = getelementptr inbounds float, float* %67, i32 %add63
  store float %66, float* %arrayidx64, align 4, !tbaa !8
  br label %for.inc65

for.inc65:                                        ; preds = %for.body56
  %71 = load i32, i32* %x, align 4, !tbaa !6
  %inc66 = add nsw i32 %71, 1
  store i32 %inc66, i32* %x, align 4, !tbaa !6
  br label %for.cond52

for.end67:                                        ; preds = %for.cond.cleanup55
  %72 = bitcast i32* %x68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #3
  store i32 1, i32* %x68, align 4, !tbaa !6
  br label %for.cond69

for.cond69:                                       ; preds = %for.inc84, %for.end67
  %73 = load i32, i32* %x68, align 4, !tbaa !6
  %74 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div70 = sdiv i32 %74, 2
  %cmp71 = icmp slt i32 %73, %div70
  br i1 %cmp71, label %for.body73, label %for.cond.cleanup72

for.cond.cleanup72:                               ; preds = %for.cond69
  store i32 17, i32* %cleanup.dest.slot, align 4
  %75 = bitcast i32* %x68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  br label %for.end86

for.body73:                                       ; preds = %for.cond69
  %76 = load float*, float** %input.addr, align 4, !tbaa !2
  %77 = load i32, i32* %y47, align 4, !tbaa !6
  %78 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul74 = mul nsw i32 %77, %78
  %79 = load i32, i32* %x68, align 4, !tbaa !6
  %add75 = add nsw i32 %mul74, %79
  %mul76 = mul nsw i32 2, %add75
  %add77 = add nsw i32 %mul76, 1
  %arrayidx78 = getelementptr inbounds float, float* %76, i32 %add77
  %80 = load float, float* %arrayidx78, align 4, !tbaa !8
  %81 = load float*, float** %output.addr, align 4, !tbaa !2
  %82 = load i32, i32* %y47, align 4, !tbaa !6
  %83 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul79 = mul nsw i32 %82, %83
  %84 = load i32, i32* %x68, align 4, !tbaa !6
  %85 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div80 = sdiv i32 %85, 2
  %add81 = add nsw i32 %84, %div80
  %add82 = add nsw i32 %mul79, %add81
  %arrayidx83 = getelementptr inbounds float, float* %81, i32 %add82
  store float %80, float* %arrayidx83, align 4, !tbaa !8
  br label %for.inc84

for.inc84:                                        ; preds = %for.body73
  %86 = load i32, i32* %x68, align 4, !tbaa !6
  %inc85 = add nsw i32 %86, 1
  store i32 %inc85, i32* %x68, align 4, !tbaa !6
  br label %for.cond69

for.end86:                                        ; preds = %for.cond.cleanup72
  br label %for.inc87

for.inc87:                                        ; preds = %for.end86
  %87 = load i32, i32* %y47, align 4, !tbaa !6
  %inc88 = add nsw i32 %87, 1
  store i32 %inc88, i32* %y47, align 4, !tbaa !6
  br label %for.cond48

for.end89:                                        ; preds = %for.cond.cleanup50
  %88 = bitcast i32* %y90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #3
  store i32 2, i32* %y90, align 4, !tbaa !6
  br label %for.cond91

for.cond91:                                       ; preds = %for.inc97, %for.end89
  %89 = load i32, i32* %y90, align 4, !tbaa !6
  %90 = load i32, i32* %vec_size.addr, align 4, !tbaa !6
  %cmp92 = icmp slt i32 %89, %90
  br i1 %cmp92, label %for.body94, label %for.cond.cleanup93

for.cond.cleanup93:                               ; preds = %for.cond91
  store i32 20, i32* %cleanup.dest.slot, align 4
  %91 = bitcast i32* %y90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #3
  br label %for.end99

for.body94:                                       ; preds = %for.cond91
  %92 = load void (float*, float*, i32)*, void (float*, float*, i32)** %fft_single.addr, align 4, !tbaa !2
  %93 = load float*, float** %output.addr, align 4, !tbaa !2
  %94 = load i32, i32* %y90, align 4, !tbaa !6
  %add.ptr95 = getelementptr inbounds float, float* %93, i32 %94
  %95 = load float*, float** %temp.addr, align 4, !tbaa !2
  %96 = load i32, i32* %y90, align 4, !tbaa !6
  %add.ptr96 = getelementptr inbounds float, float* %95, i32 %96
  %97 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %92(float* %add.ptr95, float* %add.ptr96, i32 %97)
  br label %for.inc97

for.inc97:                                        ; preds = %for.body94
  %98 = load i32, i32* %y90, align 4, !tbaa !6
  %inc98 = add nsw i32 %98, 1
  store i32 %inc98, i32* %y90, align 4, !tbaa !6
  br label %for.cond91

for.end99:                                        ; preds = %for.cond.cleanup93
  %99 = bitcast i32* %y100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #3
  %100 = load i32, i32* %vec_size.addr, align 4, !tbaa !6
  %cmp101 = icmp sgt i32 2, %100
  br i1 %cmp101, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end99
  br label %cond.end

cond.false:                                       ; preds = %for.end99
  %101 = load i32, i32* %vec_size.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 2, %cond.true ], [ %101, %cond.false ]
  store i32 %cond, i32* %y100, align 4, !tbaa !6
  br label %for.cond102

for.cond102:                                      ; preds = %for.inc108, %cond.end
  %102 = load i32, i32* %y100, align 4, !tbaa !6
  %103 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp103 = icmp slt i32 %102, %103
  br i1 %cmp103, label %for.body105, label %for.cond.cleanup104

for.cond.cleanup104:                              ; preds = %for.cond102
  store i32 23, i32* %cleanup.dest.slot, align 4
  %104 = bitcast i32* %y100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #3
  br label %for.end110

for.body105:                                      ; preds = %for.cond102
  %105 = load void (float*, float*, i32)*, void (float*, float*, i32)** %fft_multi.addr, align 4, !tbaa !2
  %106 = load float*, float** %output.addr, align 4, !tbaa !2
  %107 = load i32, i32* %y100, align 4, !tbaa !6
  %add.ptr106 = getelementptr inbounds float, float* %106, i32 %107
  %108 = load float*, float** %temp.addr, align 4, !tbaa !2
  %109 = load i32, i32* %y100, align 4, !tbaa !6
  %add.ptr107 = getelementptr inbounds float, float* %108, i32 %109
  %110 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %105(float* %add.ptr106, float* %add.ptr107, i32 %110)
  br label %for.inc108

for.inc108:                                       ; preds = %for.body105
  %111 = load i32, i32* %vec_size.addr, align 4, !tbaa !6
  %112 = load i32, i32* %y100, align 4, !tbaa !6
  %add109 = add nsw i32 %112, %111
  store i32 %add109, i32* %y100, align 4, !tbaa !6
  br label %for.cond102

for.end110:                                       ; preds = %for.cond.cleanup104
  %113 = bitcast i32* %x111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #3
  store i32 0, i32* %x111, align 4, !tbaa !6
  br label %for.cond112

for.cond112:                                      ; preds = %for.inc126, %for.end110
  %114 = load i32, i32* %x111, align 4, !tbaa !6
  %115 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp113 = icmp slt i32 %114, %115
  br i1 %cmp113, label %for.body115, label %for.cond.cleanup114

for.cond.cleanup114:                              ; preds = %for.cond112
  store i32 26, i32* %cleanup.dest.slot, align 4
  %116 = bitcast i32* %x111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  br label %for.end128

for.body115:                                      ; preds = %for.cond112
  %117 = load float*, float** %temp.addr, align 4, !tbaa !2
  %118 = load i32, i32* %x111, align 4, !tbaa !6
  %119 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul116 = mul nsw i32 %118, %119
  %arrayidx117 = getelementptr inbounds float, float* %117, i32 %mul116
  %120 = load float, float* %arrayidx117, align 4, !tbaa !8
  %121 = load float*, float** %output.addr, align 4, !tbaa !2
  %122 = load i32, i32* %x111, align 4, !tbaa !6
  %arrayidx118 = getelementptr inbounds float, float* %121, i32 %122
  store float %120, float* %arrayidx118, align 4, !tbaa !8
  %123 = load float*, float** %temp.addr, align 4, !tbaa !2
  %124 = load i32, i32* %x111, align 4, !tbaa !6
  %125 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul119 = mul nsw i32 %124, %125
  %add120 = add nsw i32 %mul119, 1
  %arrayidx121 = getelementptr inbounds float, float* %123, i32 %add120
  %126 = load float, float* %arrayidx121, align 4, !tbaa !8
  %127 = load float*, float** %output.addr, align 4, !tbaa !2
  %128 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div122 = sdiv i32 %128, 2
  %129 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul123 = mul nsw i32 %div122, %129
  %130 = load i32, i32* %x111, align 4, !tbaa !6
  %add124 = add nsw i32 %mul123, %130
  %arrayidx125 = getelementptr inbounds float, float* %127, i32 %add124
  store float %126, float* %arrayidx125, align 4, !tbaa !8
  br label %for.inc126

for.inc126:                                       ; preds = %for.body115
  %131 = load i32, i32* %x111, align 4, !tbaa !6
  %inc127 = add nsw i32 %131, 1
  store i32 %inc127, i32* %x111, align 4, !tbaa !6
  br label %for.cond112

for.end128:                                       ; preds = %for.cond.cleanup114
  %132 = bitcast i32* %y129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #3
  store i32 1, i32* %y129, align 4, !tbaa !6
  br label %for.cond130

for.cond130:                                      ; preds = %for.inc256, %for.end128
  %133 = load i32, i32* %y129, align 4, !tbaa !6
  %134 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div131 = sdiv i32 %134, 2
  %cmp132 = icmp slt i32 %133, %div131
  br i1 %cmp132, label %for.body134, label %for.cond.cleanup133

for.cond.cleanup133:                              ; preds = %for.cond130
  store i32 29, i32* %cleanup.dest.slot, align 4
  %135 = bitcast i32* %y129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #3
  br label %for.end258

for.body134:                                      ; preds = %for.cond130
  %136 = bitcast i32* %x135 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #3
  store i32 0, i32* %x135, align 4, !tbaa !6
  br label %for.cond136

for.cond136:                                      ; preds = %for.inc163, %for.body134
  %137 = load i32, i32* %x135, align 4, !tbaa !6
  %138 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div137 = sdiv i32 %138, 2
  %cmp138 = icmp sle i32 %137, %div137
  br i1 %cmp138, label %for.body140, label %for.cond.cleanup139

for.cond.cleanup139:                              ; preds = %for.cond136
  store i32 32, i32* %cleanup.dest.slot, align 4
  %139 = bitcast i32* %x135 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #3
  br label %for.end165

for.body140:                                      ; preds = %for.cond136
  %140 = load float*, float** %temp.addr, align 4, !tbaa !2
  %141 = load i32, i32* %y129, align 4, !tbaa !6
  %add141 = add nsw i32 %141, 1
  %142 = load i32, i32* %x135, align 4, !tbaa !6
  %143 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul142 = mul nsw i32 %142, %143
  %add143 = add nsw i32 %add141, %mul142
  %arrayidx144 = getelementptr inbounds float, float* %140, i32 %add143
  %144 = load float, float* %arrayidx144, align 4, !tbaa !8
  %145 = load i32, i32* %x135, align 4, !tbaa !6
  %cmp145 = icmp sgt i32 %145, 0
  br i1 %cmp145, label %land.lhs.true, label %cond.false156

land.lhs.true:                                    ; preds = %for.body140
  %146 = load i32, i32* %x135, align 4, !tbaa !6
  %147 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div146 = sdiv i32 %147, 2
  %cmp147 = icmp slt i32 %146, %div146
  br i1 %cmp147, label %cond.true148, label %cond.false156

cond.true148:                                     ; preds = %land.lhs.true
  %148 = load float*, float** %temp.addr, align 4, !tbaa !2
  %149 = load i32, i32* %y129, align 4, !tbaa !6
  %150 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div149 = sdiv i32 %150, 2
  %add150 = add nsw i32 %149, %div149
  %151 = load i32, i32* %x135, align 4, !tbaa !6
  %152 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div151 = sdiv i32 %152, 2
  %add152 = add nsw i32 %151, %div151
  %153 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul153 = mul nsw i32 %add152, %153
  %add154 = add nsw i32 %add150, %mul153
  %arrayidx155 = getelementptr inbounds float, float* %148, i32 %add154
  %154 = load float, float* %arrayidx155, align 4, !tbaa !8
  br label %cond.end157

cond.false156:                                    ; preds = %land.lhs.true, %for.body140
  br label %cond.end157

cond.end157:                                      ; preds = %cond.false156, %cond.true148
  %cond158 = phi float [ %154, %cond.true148 ], [ 0.000000e+00, %cond.false156 ]
  %add159 = fadd float %144, %cond158
  %155 = load float*, float** %output.addr, align 4, !tbaa !2
  %156 = load i32, i32* %x135, align 4, !tbaa !6
  %157 = load i32, i32* %y129, align 4, !tbaa !6
  %158 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul160 = mul nsw i32 %157, %158
  %add161 = add nsw i32 %156, %mul160
  %arrayidx162 = getelementptr inbounds float, float* %155, i32 %add161
  store float %add159, float* %arrayidx162, align 4, !tbaa !8
  br label %for.inc163

for.inc163:                                       ; preds = %cond.end157
  %159 = load i32, i32* %x135, align 4, !tbaa !6
  %inc164 = add nsw i32 %159, 1
  store i32 %inc164, i32* %x135, align 4, !tbaa !6
  br label %for.cond136

for.end165:                                       ; preds = %for.cond.cleanup139
  %160 = bitcast i32* %x166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #3
  %161 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div167 = sdiv i32 %161, 2
  %add168 = add nsw i32 %div167, 1
  store i32 %add168, i32* %x166, align 4, !tbaa !6
  br label %for.cond169

for.cond169:                                      ; preds = %for.inc190, %for.end165
  %162 = load i32, i32* %x166, align 4, !tbaa !6
  %163 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp170 = icmp slt i32 %162, %163
  br i1 %cmp170, label %for.body172, label %for.cond.cleanup171

for.cond.cleanup171:                              ; preds = %for.cond169
  store i32 35, i32* %cleanup.dest.slot, align 4
  %164 = bitcast i32* %x166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #3
  br label %for.end192

for.body172:                                      ; preds = %for.cond169
  %165 = load float*, float** %temp.addr, align 4, !tbaa !2
  %166 = load i32, i32* %y129, align 4, !tbaa !6
  %add173 = add nsw i32 %166, 1
  %167 = load i32, i32* %n.addr, align 4, !tbaa !6
  %168 = load i32, i32* %x166, align 4, !tbaa !6
  %sub174 = sub nsw i32 %167, %168
  %169 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul175 = mul nsw i32 %sub174, %169
  %add176 = add nsw i32 %add173, %mul175
  %arrayidx177 = getelementptr inbounds float, float* %165, i32 %add176
  %170 = load float, float* %arrayidx177, align 4, !tbaa !8
  %171 = load float*, float** %temp.addr, align 4, !tbaa !2
  %172 = load i32, i32* %y129, align 4, !tbaa !6
  %173 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div178 = sdiv i32 %173, 2
  %add179 = add nsw i32 %172, %div178
  %174 = load i32, i32* %n.addr, align 4, !tbaa !6
  %175 = load i32, i32* %x166, align 4, !tbaa !6
  %sub180 = sub nsw i32 %174, %175
  %176 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div181 = sdiv i32 %176, 2
  %add182 = add nsw i32 %sub180, %div181
  %177 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul183 = mul nsw i32 %add182, %177
  %add184 = add nsw i32 %add179, %mul183
  %arrayidx185 = getelementptr inbounds float, float* %171, i32 %add184
  %178 = load float, float* %arrayidx185, align 4, !tbaa !8
  %sub186 = fsub float %170, %178
  %179 = load float*, float** %output.addr, align 4, !tbaa !2
  %180 = load i32, i32* %x166, align 4, !tbaa !6
  %181 = load i32, i32* %y129, align 4, !tbaa !6
  %182 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul187 = mul nsw i32 %181, %182
  %add188 = add nsw i32 %180, %mul187
  %arrayidx189 = getelementptr inbounds float, float* %179, i32 %add188
  store float %sub186, float* %arrayidx189, align 4, !tbaa !8
  br label %for.inc190

for.inc190:                                       ; preds = %for.body172
  %183 = load i32, i32* %x166, align 4, !tbaa !6
  %inc191 = add nsw i32 %183, 1
  store i32 %inc191, i32* %x166, align 4, !tbaa !6
  br label %for.cond169

for.end192:                                       ; preds = %for.cond.cleanup171
  %184 = bitcast i32* %x193 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %184) #3
  store i32 0, i32* %x193, align 4, !tbaa !6
  br label %for.cond194

for.cond194:                                      ; preds = %for.inc224, %for.end192
  %185 = load i32, i32* %x193, align 4, !tbaa !6
  %186 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div195 = sdiv i32 %186, 2
  %cmp196 = icmp sle i32 %185, %div195
  br i1 %cmp196, label %for.body198, label %for.cond.cleanup197

for.cond.cleanup197:                              ; preds = %for.cond194
  store i32 38, i32* %cleanup.dest.slot, align 4
  %187 = bitcast i32* %x193 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #3
  br label %for.end226

for.body198:                                      ; preds = %for.cond194
  %188 = load float*, float** %temp.addr, align 4, !tbaa !2
  %189 = load i32, i32* %y129, align 4, !tbaa !6
  %190 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div199 = sdiv i32 %190, 2
  %add200 = add nsw i32 %189, %div199
  %191 = load i32, i32* %x193, align 4, !tbaa !6
  %192 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul201 = mul nsw i32 %191, %192
  %add202 = add nsw i32 %add200, %mul201
  %arrayidx203 = getelementptr inbounds float, float* %188, i32 %add202
  %193 = load float, float* %arrayidx203, align 4, !tbaa !8
  %194 = load i32, i32* %x193, align 4, !tbaa !6
  %cmp204 = icmp sgt i32 %194, 0
  br i1 %cmp204, label %land.lhs.true205, label %cond.false215

land.lhs.true205:                                 ; preds = %for.body198
  %195 = load i32, i32* %x193, align 4, !tbaa !6
  %196 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div206 = sdiv i32 %196, 2
  %cmp207 = icmp slt i32 %195, %div206
  br i1 %cmp207, label %cond.true208, label %cond.false215

cond.true208:                                     ; preds = %land.lhs.true205
  %197 = load float*, float** %temp.addr, align 4, !tbaa !2
  %198 = load i32, i32* %y129, align 4, !tbaa !6
  %add209 = add nsw i32 %198, 1
  %199 = load i32, i32* %x193, align 4, !tbaa !6
  %200 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div210 = sdiv i32 %200, 2
  %add211 = add nsw i32 %199, %div210
  %201 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul212 = mul nsw i32 %add211, %201
  %add213 = add nsw i32 %add209, %mul212
  %arrayidx214 = getelementptr inbounds float, float* %197, i32 %add213
  %202 = load float, float* %arrayidx214, align 4, !tbaa !8
  br label %cond.end216

cond.false215:                                    ; preds = %land.lhs.true205, %for.body198
  br label %cond.end216

cond.end216:                                      ; preds = %cond.false215, %cond.true208
  %cond217 = phi float [ %202, %cond.true208 ], [ 0.000000e+00, %cond.false215 ]
  %sub218 = fsub float %193, %cond217
  %203 = load float*, float** %output.addr, align 4, !tbaa !2
  %204 = load i32, i32* %x193, align 4, !tbaa !6
  %205 = load i32, i32* %y129, align 4, !tbaa !6
  %206 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div219 = sdiv i32 %206, 2
  %add220 = add nsw i32 %205, %div219
  %207 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul221 = mul nsw i32 %add220, %207
  %add222 = add nsw i32 %204, %mul221
  %arrayidx223 = getelementptr inbounds float, float* %203, i32 %add222
  store float %sub218, float* %arrayidx223, align 4, !tbaa !8
  br label %for.inc224

for.inc224:                                       ; preds = %cond.end216
  %208 = load i32, i32* %x193, align 4, !tbaa !6
  %inc225 = add nsw i32 %208, 1
  store i32 %inc225, i32* %x193, align 4, !tbaa !6
  br label %for.cond194

for.end226:                                       ; preds = %for.cond.cleanup197
  %209 = bitcast i32* %x227 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %209) #3
  %210 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div228 = sdiv i32 %210, 2
  %add229 = add nsw i32 %div228, 1
  store i32 %add229, i32* %x227, align 4, !tbaa !6
  br label %for.cond230

for.cond230:                                      ; preds = %for.inc253, %for.end226
  %211 = load i32, i32* %x227, align 4, !tbaa !6
  %212 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp231 = icmp slt i32 %211, %212
  br i1 %cmp231, label %for.body233, label %for.cond.cleanup232

for.cond.cleanup232:                              ; preds = %for.cond230
  store i32 41, i32* %cleanup.dest.slot, align 4
  %213 = bitcast i32* %x227 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #3
  br label %for.end255

for.body233:                                      ; preds = %for.cond230
  %214 = load float*, float** %temp.addr, align 4, !tbaa !2
  %215 = load i32, i32* %y129, align 4, !tbaa !6
  %add234 = add nsw i32 %215, 1
  %216 = load i32, i32* %n.addr, align 4, !tbaa !6
  %217 = load i32, i32* %x227, align 4, !tbaa !6
  %sub235 = sub nsw i32 %216, %217
  %218 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div236 = sdiv i32 %218, 2
  %add237 = add nsw i32 %sub235, %div236
  %219 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul238 = mul nsw i32 %add237, %219
  %add239 = add nsw i32 %add234, %mul238
  %arrayidx240 = getelementptr inbounds float, float* %214, i32 %add239
  %220 = load float, float* %arrayidx240, align 4, !tbaa !8
  %221 = load float*, float** %temp.addr, align 4, !tbaa !2
  %222 = load i32, i32* %y129, align 4, !tbaa !6
  %223 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div241 = sdiv i32 %223, 2
  %add242 = add nsw i32 %222, %div241
  %224 = load i32, i32* %n.addr, align 4, !tbaa !6
  %225 = load i32, i32* %x227, align 4, !tbaa !6
  %sub243 = sub nsw i32 %224, %225
  %226 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul244 = mul nsw i32 %sub243, %226
  %add245 = add nsw i32 %add242, %mul244
  %arrayidx246 = getelementptr inbounds float, float* %221, i32 %add245
  %227 = load float, float* %arrayidx246, align 4, !tbaa !8
  %add247 = fadd float %220, %227
  %228 = load float*, float** %output.addr, align 4, !tbaa !2
  %229 = load i32, i32* %x227, align 4, !tbaa !6
  %230 = load i32, i32* %y129, align 4, !tbaa !6
  %231 = load i32, i32* %n.addr, align 4, !tbaa !6
  %div248 = sdiv i32 %231, 2
  %add249 = add nsw i32 %230, %div248
  %232 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul250 = mul nsw i32 %add249, %232
  %add251 = add nsw i32 %229, %mul250
  %arrayidx252 = getelementptr inbounds float, float* %228, i32 %add251
  store float %add247, float* %arrayidx252, align 4, !tbaa !8
  br label %for.inc253

for.inc253:                                       ; preds = %for.body233
  %233 = load i32, i32* %x227, align 4, !tbaa !6
  %inc254 = add nsw i32 %233, 1
  store i32 %inc254, i32* %x227, align 4, !tbaa !6
  br label %for.cond230

for.end255:                                       ; preds = %for.cond.cleanup232
  br label %for.inc256

for.inc256:                                       ; preds = %for.end255
  %234 = load i32, i32* %y129, align 4, !tbaa !6
  %inc257 = add nsw i32 %234, 1
  store i32 %inc257, i32* %y129, align 4, !tbaa !6
  br label %for.cond130

for.end258:                                       ; preds = %for.cond.cleanup133
  %235 = bitcast i32* %y259 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %235) #3
  store i32 0, i32* %y259, align 4, !tbaa !6
  br label %for.cond260

for.cond260:                                      ; preds = %for.inc266, %for.end258
  %236 = load i32, i32* %y259, align 4, !tbaa !6
  %237 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp261 = icmp slt i32 %236, %237
  br i1 %cmp261, label %for.body263, label %for.cond.cleanup262

for.cond.cleanup262:                              ; preds = %for.cond260
  store i32 44, i32* %cleanup.dest.slot, align 4
  %238 = bitcast i32* %y259 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #3
  br label %for.end268

for.body263:                                      ; preds = %for.cond260
  %239 = load void (float*, float*, i32)*, void (float*, float*, i32)** %ifft_multi.addr, align 4, !tbaa !2
  %240 = load float*, float** %output.addr, align 4, !tbaa !2
  %241 = load i32, i32* %y259, align 4, !tbaa !6
  %add.ptr264 = getelementptr inbounds float, float* %240, i32 %241
  %242 = load float*, float** %temp.addr, align 4, !tbaa !2
  %243 = load i32, i32* %y259, align 4, !tbaa !6
  %add.ptr265 = getelementptr inbounds float, float* %242, i32 %243
  %244 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %239(float* %add.ptr264, float* %add.ptr265, i32 %244)
  br label %for.inc266

for.inc266:                                       ; preds = %for.body263
  %245 = load i32, i32* %vec_size.addr, align 4, !tbaa !6
  %246 = load i32, i32* %y259, align 4, !tbaa !6
  %add267 = add nsw i32 %246, %245
  store i32 %add267, i32* %y259, align 4, !tbaa !6
  br label %for.cond260

for.end268:                                       ; preds = %for.cond.cleanup262
  %247 = load void (float*, float*, i32)*, void (float*, float*, i32)** %transpose.addr, align 4, !tbaa !2
  %248 = load float*, float** %temp.addr, align 4, !tbaa !2
  %249 = load float*, float** %output.addr, align 4, !tbaa !2
  %250 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void %247(float* %248, float* %249, i32 %250)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft1d_2_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load float*, float** %input.addr, align 4, !tbaa !2
  %2 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %2
  %add.ptr = getelementptr inbounds float, float* %1, i32 %mul
  %3 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %3, float* %i0, align 4, !tbaa !8
  %4 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load float*, float** %input.addr, align 4, !tbaa !2
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %6
  %add.ptr2 = getelementptr inbounds float, float* %5, i32 %mul1
  %7 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %7, float* %i1, align 4, !tbaa !8
  %8 = load float*, float** %output.addr, align 4, !tbaa !2
  %9 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 0, %9
  %add.ptr4 = getelementptr inbounds float, float* %8, i32 %mul3
  %10 = load float, float* %i0, align 4, !tbaa !8
  %11 = load float, float* %i1, align 4, !tbaa !8
  %add = fadd float %10, %11
  call void @store_float(float* %add.ptr4, float %add)
  %12 = load float*, float** %output.addr, align 4, !tbaa !2
  %13 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 1, %13
  %add.ptr6 = getelementptr inbounds float, float* %12, i32 %mul5
  %14 = load float, float* %i0, align 4, !tbaa !8
  %15 = load float, float* %i1, align 4, !tbaa !8
  %sub = fsub float %14, %15
  call void @store_float(float* %add.ptr6, float %sub)
  %16 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %17 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft1d_4_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %kWeight0 = alloca float, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  %i2 = alloca float, align 4
  %i3 = alloca float, align 4
  %w2 = alloca float, align 4
  %w3 = alloca float, align 4
  %w4 = alloca [2 x float], align 4
  %w5 = alloca [2 x float], align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store float 0.000000e+00, float* %kWeight0, align 4, !tbaa !8
  %1 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load float*, float** %input.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %3
  %add.ptr = getelementptr inbounds float, float* %2, i32 %mul
  %4 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %4, float* %i0, align 4, !tbaa !8
  %5 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load float*, float** %input.addr, align 4, !tbaa !2
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %7
  %add.ptr2 = getelementptr inbounds float, float* %6, i32 %mul1
  %8 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %8, float* %i1, align 4, !tbaa !8
  %9 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load float*, float** %input.addr, align 4, !tbaa !2
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 2, %11
  %add.ptr4 = getelementptr inbounds float, float* %10, i32 %mul3
  %12 = load float, float* %add.ptr4, align 4, !tbaa !8
  store float %12, float* %i2, align 4, !tbaa !8
  %13 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = load float*, float** %input.addr, align 4, !tbaa !2
  %15 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %15
  %add.ptr6 = getelementptr inbounds float, float* %14, i32 %mul5
  %16 = load float, float* %add.ptr6, align 4, !tbaa !8
  store float %16, float* %i3, align 4, !tbaa !8
  %17 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load float, float* %i0, align 4, !tbaa !8
  %19 = load float, float* %i2, align 4, !tbaa !8
  %call = call float @add_float(float %18, float %19)
  store float %call, float* %w2, align 4, !tbaa !8
  %20 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load float, float* %i0, align 4, !tbaa !8
  %22 = load float, float* %i2, align 4, !tbaa !8
  %call7 = call float @sub_float(float %21, float %22)
  store float %call7, float* %w3, align 4, !tbaa !8
  %23 = bitcast [2 x float]* %w4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %23) #3
  %arrayinit.begin = getelementptr inbounds [2 x float], [2 x float]* %w4, i32 0, i32 0
  %24 = load float, float* %i1, align 4, !tbaa !8
  %25 = load float, float* %i1, align 4, !tbaa !8
  %call8 = call float @add_float(float %24, float %25)
  store float %call8, float* %arrayinit.begin, align 4, !tbaa !8
  %arrayinit.element = getelementptr inbounds float, float* %arrayinit.begin, i32 1
  %26 = load float, float* %i3, align 4, !tbaa !8
  %27 = load float, float* %i3, align 4, !tbaa !8
  %call9 = call float @sub_float(float %26, float %27)
  store float %call9, float* %arrayinit.element, align 4, !tbaa !8
  %28 = bitcast [2 x float]* %w5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %28) #3
  %arrayinit.begin10 = getelementptr inbounds [2 x float], [2 x float]* %w5, i32 0, i32 0
  %29 = load float, float* %i1, align 4, !tbaa !8
  %30 = load float, float* %i1, align 4, !tbaa !8
  %call11 = call float @sub_float(float %29, float %30)
  store float %call11, float* %arrayinit.begin10, align 4, !tbaa !8
  %arrayinit.element12 = getelementptr inbounds float, float* %arrayinit.begin10, i32 1
  %31 = load float, float* %i3, align 4, !tbaa !8
  %call13 = call float @sub_float(float 0.000000e+00, float %31)
  %32 = load float, float* %i3, align 4, !tbaa !8
  %call14 = call float @sub_float(float %call13, float %32)
  store float %call14, float* %arrayinit.element12, align 4, !tbaa !8
  %33 = load float*, float** %output.addr, align 4, !tbaa !2
  %34 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 0, %34
  %add.ptr16 = getelementptr inbounds float, float* %33, i32 %mul15
  %35 = load float, float* %w2, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [2 x float], [2 x float]* %w4, i32 0, i32 0
  %36 = load float, float* %arrayidx, align 4, !tbaa !8
  %call17 = call float @add_float(float %35, float %36)
  call void @store_float(float* %add.ptr16, float %call17)
  %37 = load float*, float** %output.addr, align 4, !tbaa !2
  %38 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 1, %38
  %add.ptr19 = getelementptr inbounds float, float* %37, i32 %mul18
  %39 = load float, float* %w3, align 4, !tbaa !8
  %arrayidx20 = getelementptr inbounds [2 x float], [2 x float]* %w5, i32 0, i32 1
  %40 = load float, float* %arrayidx20, align 4, !tbaa !8
  %call21 = call float @add_float(float %39, float %40)
  call void @store_float(float* %add.ptr19, float %call21)
  %41 = load float*, float** %output.addr, align 4, !tbaa !2
  %42 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul22 = mul nsw i32 2, %42
  %add.ptr23 = getelementptr inbounds float, float* %41, i32 %mul22
  %43 = load float, float* %w2, align 4, !tbaa !8
  %arrayidx24 = getelementptr inbounds [2 x float], [2 x float]* %w4, i32 0, i32 0
  %44 = load float, float* %arrayidx24, align 4, !tbaa !8
  %call25 = call float @sub_float(float %43, float %44)
  call void @store_float(float* %add.ptr23, float %call25)
  %45 = load float*, float** %output.addr, align 4, !tbaa !2
  %46 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 3, %46
  %add.ptr27 = getelementptr inbounds float, float* %45, i32 %mul26
  %47 = load float, float* %w3, align 4, !tbaa !8
  %arrayidx28 = getelementptr inbounds [2 x float], [2 x float]* %w5, i32 0, i32 1
  %48 = load float, float* %arrayidx28, align 4, !tbaa !8
  %call29 = call float @sub_float(float %47, float %48)
  call void @store_float(float* %add.ptr27, float %call29)
  %49 = bitcast [2 x float]* %w5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %49) #3
  %50 = bitcast [2 x float]* %w4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %50) #3
  %51 = bitcast float* %w3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast float* %w2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  %55 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft1d_8_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %kWeight0 = alloca float, align 4
  %kWeight2 = alloca float, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  %i2 = alloca float, align 4
  %i3 = alloca float, align 4
  %i4 = alloca float, align 4
  %i5 = alloca float, align 4
  %i6 = alloca float, align 4
  %i7 = alloca float, align 4
  %w6 = alloca float, align 4
  %w7 = alloca float, align 4
  %w8 = alloca [2 x float], align 4
  %w9 = alloca [2 x float], align 4
  %w10 = alloca [2 x float], align 4
  %w11 = alloca [2 x float], align 4
  %w12 = alloca [2 x float], align 4
  %w13 = alloca [2 x float], align 4
  %w14 = alloca [2 x float], align 4
  %w15 = alloca [2 x float], align 4
  %w16 = alloca [2 x float], align 4
  %w17 = alloca [2 x float], align 4
  %w18 = alloca [2 x float], align 4
  %w19 = alloca [2 x float], align 4
  %w20 = alloca [2 x float], align 4
  %w21 = alloca [2 x float], align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store float 0.000000e+00, float* %kWeight0, align 4, !tbaa !8
  %1 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store float 0x3FE6A09EE0000000, float* %kWeight2, align 4, !tbaa !8
  %2 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load float*, float** %input.addr, align 4, !tbaa !2
  %4 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %4
  %add.ptr = getelementptr inbounds float, float* %3, i32 %mul
  %5 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %5, float* %i0, align 4, !tbaa !8
  %6 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load float*, float** %input.addr, align 4, !tbaa !2
  %8 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %8
  %add.ptr2 = getelementptr inbounds float, float* %7, i32 %mul1
  %9 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %9, float* %i1, align 4, !tbaa !8
  %10 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load float*, float** %input.addr, align 4, !tbaa !2
  %12 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 2, %12
  %add.ptr4 = getelementptr inbounds float, float* %11, i32 %mul3
  %13 = load float, float* %add.ptr4, align 4, !tbaa !8
  store float %13, float* %i2, align 4, !tbaa !8
  %14 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load float*, float** %input.addr, align 4, !tbaa !2
  %16 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %16
  %add.ptr6 = getelementptr inbounds float, float* %15, i32 %mul5
  %17 = load float, float* %add.ptr6, align 4, !tbaa !8
  store float %17, float* %i3, align 4, !tbaa !8
  %18 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = load float*, float** %input.addr, align 4, !tbaa !2
  %20 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 4, %20
  %add.ptr8 = getelementptr inbounds float, float* %19, i32 %mul7
  %21 = load float, float* %add.ptr8, align 4, !tbaa !8
  store float %21, float* %i4, align 4, !tbaa !8
  %22 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = load float*, float** %input.addr, align 4, !tbaa !2
  %24 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul9 = mul nsw i32 5, %24
  %add.ptr10 = getelementptr inbounds float, float* %23, i32 %mul9
  %25 = load float, float* %add.ptr10, align 4, !tbaa !8
  store float %25, float* %i5, align 4, !tbaa !8
  %26 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load float*, float** %input.addr, align 4, !tbaa !2
  %28 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 6, %28
  %add.ptr12 = getelementptr inbounds float, float* %27, i32 %mul11
  %29 = load float, float* %add.ptr12, align 4, !tbaa !8
  store float %29, float* %i6, align 4, !tbaa !8
  %30 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  %31 = load float*, float** %input.addr, align 4, !tbaa !2
  %32 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 7, %32
  %add.ptr14 = getelementptr inbounds float, float* %31, i32 %mul13
  %33 = load float, float* %add.ptr14, align 4, !tbaa !8
  store float %33, float* %i7, align 4, !tbaa !8
  %34 = bitcast float* %w6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #3
  %35 = load float, float* %i0, align 4, !tbaa !8
  %36 = load float, float* %i4, align 4, !tbaa !8
  %call = call float @add_float(float %35, float %36)
  store float %call, float* %w6, align 4, !tbaa !8
  %37 = bitcast float* %w7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #3
  %38 = load float, float* %i0, align 4, !tbaa !8
  %39 = load float, float* %i4, align 4, !tbaa !8
  %call15 = call float @sub_float(float %38, float %39)
  store float %call15, float* %w7, align 4, !tbaa !8
  %40 = bitcast [2 x float]* %w8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %40) #3
  %arrayinit.begin = getelementptr inbounds [2 x float], [2 x float]* %w8, i32 0, i32 0
  %41 = load float, float* %i2, align 4, !tbaa !8
  %42 = load float, float* %i2, align 4, !tbaa !8
  %call16 = call float @add_float(float %41, float %42)
  store float %call16, float* %arrayinit.begin, align 4, !tbaa !8
  %arrayinit.element = getelementptr inbounds float, float* %arrayinit.begin, i32 1
  %43 = load float, float* %i6, align 4, !tbaa !8
  %44 = load float, float* %i6, align 4, !tbaa !8
  %call17 = call float @sub_float(float %43, float %44)
  store float %call17, float* %arrayinit.element, align 4, !tbaa !8
  %45 = bitcast [2 x float]* %w9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %45) #3
  %arrayinit.begin18 = getelementptr inbounds [2 x float], [2 x float]* %w9, i32 0, i32 0
  %46 = load float, float* %i2, align 4, !tbaa !8
  %47 = load float, float* %i2, align 4, !tbaa !8
  %call19 = call float @sub_float(float %46, float %47)
  store float %call19, float* %arrayinit.begin18, align 4, !tbaa !8
  %arrayinit.element20 = getelementptr inbounds float, float* %arrayinit.begin18, i32 1
  %48 = load float, float* %i6, align 4, !tbaa !8
  %call21 = call float @sub_float(float 0.000000e+00, float %48)
  %49 = load float, float* %i6, align 4, !tbaa !8
  %call22 = call float @sub_float(float %call21, float %49)
  store float %call22, float* %arrayinit.element20, align 4, !tbaa !8
  %50 = bitcast [2 x float]* %w10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %50) #3
  %arrayinit.begin23 = getelementptr inbounds [2 x float], [2 x float]* %w10, i32 0, i32 0
  %51 = load float, float* %w6, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [2 x float], [2 x float]* %w8, i32 0, i32 0
  %52 = load float, float* %arrayidx, align 4, !tbaa !8
  %call24 = call float @add_float(float %51, float %52)
  store float %call24, float* %arrayinit.begin23, align 4, !tbaa !8
  %arrayinit.element25 = getelementptr inbounds float, float* %arrayinit.begin23, i32 1
  %arrayidx26 = getelementptr inbounds [2 x float], [2 x float]* %w8, i32 0, i32 1
  %53 = load float, float* %arrayidx26, align 4, !tbaa !8
  store float %53, float* %arrayinit.element25, align 4, !tbaa !8
  %54 = bitcast [2 x float]* %w11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %54) #3
  %arrayinit.begin27 = getelementptr inbounds [2 x float], [2 x float]* %w11, i32 0, i32 0
  %55 = load float, float* %w6, align 4, !tbaa !8
  %arrayidx28 = getelementptr inbounds [2 x float], [2 x float]* %w8, i32 0, i32 0
  %56 = load float, float* %arrayidx28, align 4, !tbaa !8
  %call29 = call float @sub_float(float %55, float %56)
  store float %call29, float* %arrayinit.begin27, align 4, !tbaa !8
  %arrayinit.element30 = getelementptr inbounds float, float* %arrayinit.begin27, i32 1
  %arrayidx31 = getelementptr inbounds [2 x float], [2 x float]* %w8, i32 0, i32 1
  %57 = load float, float* %arrayidx31, align 4, !tbaa !8
  %call32 = call float @sub_float(float 0.000000e+00, float %57)
  store float %call32, float* %arrayinit.element30, align 4, !tbaa !8
  %58 = bitcast [2 x float]* %w12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %58) #3
  %arrayinit.begin33 = getelementptr inbounds [2 x float], [2 x float]* %w12, i32 0, i32 0
  %59 = load float, float* %w7, align 4, !tbaa !8
  %arrayidx34 = getelementptr inbounds [2 x float], [2 x float]* %w9, i32 0, i32 1
  %60 = load float, float* %arrayidx34, align 4, !tbaa !8
  %call35 = call float @add_float(float %59, float %60)
  store float %call35, float* %arrayinit.begin33, align 4, !tbaa !8
  %arrayinit.element36 = getelementptr inbounds float, float* %arrayinit.begin33, i32 1
  %arrayidx37 = getelementptr inbounds [2 x float], [2 x float]* %w9, i32 0, i32 0
  %61 = load float, float* %arrayidx37, align 4, !tbaa !8
  %call38 = call float @sub_float(float 0.000000e+00, float %61)
  store float %call38, float* %arrayinit.element36, align 4, !tbaa !8
  %62 = bitcast [2 x float]* %w13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %62) #3
  %arrayinit.begin39 = getelementptr inbounds [2 x float], [2 x float]* %w13, i32 0, i32 0
  %63 = load float, float* %w7, align 4, !tbaa !8
  %arrayidx40 = getelementptr inbounds [2 x float], [2 x float]* %w9, i32 0, i32 1
  %64 = load float, float* %arrayidx40, align 4, !tbaa !8
  %call41 = call float @sub_float(float %63, float %64)
  store float %call41, float* %arrayinit.begin39, align 4, !tbaa !8
  %arrayinit.element42 = getelementptr inbounds float, float* %arrayinit.begin39, i32 1
  %arrayidx43 = getelementptr inbounds [2 x float], [2 x float]* %w9, i32 0, i32 0
  %65 = load float, float* %arrayidx43, align 4, !tbaa !8
  store float %65, float* %arrayinit.element42, align 4, !tbaa !8
  %66 = bitcast [2 x float]* %w14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %66) #3
  %arrayinit.begin44 = getelementptr inbounds [2 x float], [2 x float]* %w14, i32 0, i32 0
  %67 = load float, float* %i1, align 4, !tbaa !8
  %68 = load float, float* %i3, align 4, !tbaa !8
  %call45 = call float @add_float(float %67, float %68)
  store float %call45, float* %arrayinit.begin44, align 4, !tbaa !8
  %arrayinit.element46 = getelementptr inbounds float, float* %arrayinit.begin44, i32 1
  %69 = load float, float* %i7, align 4, !tbaa !8
  %70 = load float, float* %i5, align 4, !tbaa !8
  %call47 = call float @sub_float(float %69, float %70)
  store float %call47, float* %arrayinit.element46, align 4, !tbaa !8
  %71 = bitcast [2 x float]* %w15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %71) #3
  %arrayinit.begin48 = getelementptr inbounds [2 x float], [2 x float]* %w15, i32 0, i32 0
  %72 = load float, float* %i1, align 4, !tbaa !8
  %73 = load float, float* %i3, align 4, !tbaa !8
  %call49 = call float @sub_float(float %72, float %73)
  store float %call49, float* %arrayinit.begin48, align 4, !tbaa !8
  %arrayinit.element50 = getelementptr inbounds float, float* %arrayinit.begin48, i32 1
  %74 = load float, float* %i5, align 4, !tbaa !8
  %call51 = call float @sub_float(float 0.000000e+00, float %74)
  %75 = load float, float* %i7, align 4, !tbaa !8
  %call52 = call float @sub_float(float %call51, float %75)
  store float %call52, float* %arrayinit.element50, align 4, !tbaa !8
  %76 = bitcast [2 x float]* %w16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %76) #3
  %arrayinit.begin53 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %77 = load float, float* %i3, align 4, !tbaa !8
  %78 = load float, float* %i1, align 4, !tbaa !8
  %call54 = call float @add_float(float %77, float %78)
  store float %call54, float* %arrayinit.begin53, align 4, !tbaa !8
  %arrayinit.element55 = getelementptr inbounds float, float* %arrayinit.begin53, i32 1
  %79 = load float, float* %i5, align 4, !tbaa !8
  %80 = load float, float* %i7, align 4, !tbaa !8
  %call56 = call float @sub_float(float %79, float %80)
  store float %call56, float* %arrayinit.element55, align 4, !tbaa !8
  %81 = bitcast [2 x float]* %w17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %81) #3
  %arrayinit.begin57 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 0
  %82 = load float, float* %i3, align 4, !tbaa !8
  %83 = load float, float* %i1, align 4, !tbaa !8
  %call58 = call float @sub_float(float %82, float %83)
  store float %call58, float* %arrayinit.begin57, align 4, !tbaa !8
  %arrayinit.element59 = getelementptr inbounds float, float* %arrayinit.begin57, i32 1
  %84 = load float, float* %i7, align 4, !tbaa !8
  %call60 = call float @sub_float(float 0.000000e+00, float %84)
  %85 = load float, float* %i5, align 4, !tbaa !8
  %call61 = call float @sub_float(float %call60, float %85)
  store float %call61, float* %arrayinit.element59, align 4, !tbaa !8
  %86 = bitcast [2 x float]* %w18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %86) #3
  %arrayinit.begin62 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %arrayidx63 = getelementptr inbounds [2 x float], [2 x float]* %w14, i32 0, i32 0
  %87 = load float, float* %arrayidx63, align 4, !tbaa !8
  %arrayidx64 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %88 = load float, float* %arrayidx64, align 4, !tbaa !8
  %call65 = call float @add_float(float %87, float %88)
  store float %call65, float* %arrayinit.begin62, align 4, !tbaa !8
  %arrayinit.element66 = getelementptr inbounds float, float* %arrayinit.begin62, i32 1
  %arrayidx67 = getelementptr inbounds [2 x float], [2 x float]* %w14, i32 0, i32 1
  %89 = load float, float* %arrayidx67, align 4, !tbaa !8
  %arrayidx68 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 1
  %90 = load float, float* %arrayidx68, align 4, !tbaa !8
  %call69 = call float @add_float(float %89, float %90)
  store float %call69, float* %arrayinit.element66, align 4, !tbaa !8
  %91 = bitcast [2 x float]* %w19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %91) #3
  %arrayinit.begin70 = getelementptr inbounds [2 x float], [2 x float]* %w19, i32 0, i32 0
  %arrayidx71 = getelementptr inbounds [2 x float], [2 x float]* %w14, i32 0, i32 0
  %92 = load float, float* %arrayidx71, align 4, !tbaa !8
  %arrayidx72 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %93 = load float, float* %arrayidx72, align 4, !tbaa !8
  %call73 = call float @sub_float(float %92, float %93)
  store float %call73, float* %arrayinit.begin70, align 4, !tbaa !8
  %arrayinit.element74 = getelementptr inbounds float, float* %arrayinit.begin70, i32 1
  %arrayidx75 = getelementptr inbounds [2 x float], [2 x float]* %w14, i32 0, i32 1
  %94 = load float, float* %arrayidx75, align 4, !tbaa !8
  %arrayidx76 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 1
  %95 = load float, float* %arrayidx76, align 4, !tbaa !8
  %call77 = call float @sub_float(float %94, float %95)
  store float %call77, float* %arrayinit.element74, align 4, !tbaa !8
  %96 = bitcast [2 x float]* %w20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %96) #3
  %arrayinit.begin78 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 0
  %arrayidx79 = getelementptr inbounds [2 x float], [2 x float]* %w15, i32 0, i32 0
  %97 = load float, float* %arrayidx79, align 4, !tbaa !8
  %arrayidx80 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 1
  %98 = load float, float* %arrayidx80, align 4, !tbaa !8
  %call81 = call float @add_float(float %97, float %98)
  store float %call81, float* %arrayinit.begin78, align 4, !tbaa !8
  %arrayinit.element82 = getelementptr inbounds float, float* %arrayinit.begin78, i32 1
  %arrayidx83 = getelementptr inbounds [2 x float], [2 x float]* %w15, i32 0, i32 1
  %99 = load float, float* %arrayidx83, align 4, !tbaa !8
  %arrayidx84 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 0
  %100 = load float, float* %arrayidx84, align 4, !tbaa !8
  %call85 = call float @sub_float(float %99, float %100)
  store float %call85, float* %arrayinit.element82, align 4, !tbaa !8
  %101 = bitcast [2 x float]* %w21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %101) #3
  %arrayinit.begin86 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 0
  %arrayidx87 = getelementptr inbounds [2 x float], [2 x float]* %w15, i32 0, i32 0
  %102 = load float, float* %arrayidx87, align 4, !tbaa !8
  %arrayidx88 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 1
  %103 = load float, float* %arrayidx88, align 4, !tbaa !8
  %call89 = call float @sub_float(float %102, float %103)
  store float %call89, float* %arrayinit.begin86, align 4, !tbaa !8
  %arrayinit.element90 = getelementptr inbounds float, float* %arrayinit.begin86, i32 1
  %arrayidx91 = getelementptr inbounds [2 x float], [2 x float]* %w15, i32 0, i32 1
  %104 = load float, float* %arrayidx91, align 4, !tbaa !8
  %arrayidx92 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 0
  %105 = load float, float* %arrayidx92, align 4, !tbaa !8
  %call93 = call float @add_float(float %104, float %105)
  store float %call93, float* %arrayinit.element90, align 4, !tbaa !8
  %106 = load float*, float** %output.addr, align 4, !tbaa !2
  %107 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul94 = mul nsw i32 0, %107
  %add.ptr95 = getelementptr inbounds float, float* %106, i32 %mul94
  %arrayidx96 = getelementptr inbounds [2 x float], [2 x float]* %w10, i32 0, i32 0
  %108 = load float, float* %arrayidx96, align 4, !tbaa !8
  %arrayidx97 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %109 = load float, float* %arrayidx97, align 4, !tbaa !8
  %call98 = call float @add_float(float %108, float %109)
  call void @store_float(float* %add.ptr95, float %call98)
  %110 = load float*, float** %output.addr, align 4, !tbaa !2
  %111 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul99 = mul nsw i32 1, %111
  %add.ptr100 = getelementptr inbounds float, float* %110, i32 %mul99
  %arrayidx101 = getelementptr inbounds [2 x float], [2 x float]* %w12, i32 0, i32 0
  %112 = load float, float* %arrayidx101, align 4, !tbaa !8
  %arrayidx102 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 0
  %113 = load float, float* %arrayidx102, align 4, !tbaa !8
  %arrayidx103 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 1
  %114 = load float, float* %arrayidx103, align 4, !tbaa !8
  %call104 = call float @add_float(float %113, float %114)
  %call105 = call float @mul_float(float 0x3FE6A09EE0000000, float %call104)
  %call106 = call float @add_float(float %112, float %call105)
  call void @store_float(float* %add.ptr100, float %call106)
  %115 = load float*, float** %output.addr, align 4, !tbaa !2
  %116 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul107 = mul nsw i32 2, %116
  %add.ptr108 = getelementptr inbounds float, float* %115, i32 %mul107
  %arrayidx109 = getelementptr inbounds [2 x float], [2 x float]* %w11, i32 0, i32 0
  %117 = load float, float* %arrayidx109, align 4, !tbaa !8
  %arrayidx110 = getelementptr inbounds [2 x float], [2 x float]* %w19, i32 0, i32 1
  %118 = load float, float* %arrayidx110, align 4, !tbaa !8
  %call111 = call float @add_float(float %117, float %118)
  call void @store_float(float* %add.ptr108, float %call111)
  %119 = load float*, float** %output.addr, align 4, !tbaa !2
  %120 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul112 = mul nsw i32 3, %120
  %add.ptr113 = getelementptr inbounds float, float* %119, i32 %mul112
  %arrayidx114 = getelementptr inbounds [2 x float], [2 x float]* %w13, i32 0, i32 0
  %121 = load float, float* %arrayidx114, align 4, !tbaa !8
  %arrayidx115 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 0
  %122 = load float, float* %arrayidx115, align 4, !tbaa !8
  %arrayidx116 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 1
  %123 = load float, float* %arrayidx116, align 4, !tbaa !8
  %call117 = call float @sub_float(float %122, float %123)
  %call118 = call float @mul_float(float 0x3FE6A09EE0000000, float %call117)
  %call119 = call float @sub_float(float %121, float %call118)
  call void @store_float(float* %add.ptr113, float %call119)
  %124 = load float*, float** %output.addr, align 4, !tbaa !2
  %125 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul120 = mul nsw i32 4, %125
  %add.ptr121 = getelementptr inbounds float, float* %124, i32 %mul120
  %arrayidx122 = getelementptr inbounds [2 x float], [2 x float]* %w10, i32 0, i32 0
  %126 = load float, float* %arrayidx122, align 4, !tbaa !8
  %arrayidx123 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %127 = load float, float* %arrayidx123, align 4, !tbaa !8
  %call124 = call float @sub_float(float %126, float %127)
  call void @store_float(float* %add.ptr121, float %call124)
  %128 = load float*, float** %output.addr, align 4, !tbaa !2
  %129 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul125 = mul nsw i32 5, %129
  %add.ptr126 = getelementptr inbounds float, float* %128, i32 %mul125
  %arrayidx127 = getelementptr inbounds [2 x float], [2 x float]* %w12, i32 0, i32 0
  %130 = load float, float* %arrayidx127, align 4, !tbaa !8
  %arrayidx128 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 0
  %131 = load float, float* %arrayidx128, align 4, !tbaa !8
  %call129 = call float @mul_float(float 0x3FE6A09EE0000000, float %131)
  %call130 = call float @sub_float(float 0.000000e+00, float %call129)
  %arrayidx131 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 1
  %132 = load float, float* %arrayidx131, align 4, !tbaa !8
  %call132 = call float @mul_float(float 0x3FE6A09EE0000000, float %132)
  %call133 = call float @sub_float(float %call130, float %call132)
  %call134 = call float @add_float(float %130, float %call133)
  call void @store_float(float* %add.ptr126, float %call134)
  %133 = load float*, float** %output.addr, align 4, !tbaa !2
  %134 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul135 = mul nsw i32 6, %134
  %add.ptr136 = getelementptr inbounds float, float* %133, i32 %mul135
  %arrayidx137 = getelementptr inbounds [2 x float], [2 x float]* %w11, i32 0, i32 0
  %135 = load float, float* %arrayidx137, align 4, !tbaa !8
  %arrayidx138 = getelementptr inbounds [2 x float], [2 x float]* %w19, i32 0, i32 1
  %136 = load float, float* %arrayidx138, align 4, !tbaa !8
  %call139 = call float @sub_float(float %135, float %136)
  call void @store_float(float* %add.ptr136, float %call139)
  %137 = load float*, float** %output.addr, align 4, !tbaa !2
  %138 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul140 = mul nsw i32 7, %138
  %add.ptr141 = getelementptr inbounds float, float* %137, i32 %mul140
  %arrayidx142 = getelementptr inbounds [2 x float], [2 x float]* %w13, i32 0, i32 0
  %139 = load float, float* %arrayidx142, align 4, !tbaa !8
  %arrayidx143 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 0
  %140 = load float, float* %arrayidx143, align 4, !tbaa !8
  %arrayidx144 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 1
  %141 = load float, float* %arrayidx144, align 4, !tbaa !8
  %call145 = call float @sub_float(float %140, float %141)
  %call146 = call float @mul_float(float 0x3FE6A09EE0000000, float %call145)
  %call147 = call float @add_float(float %139, float %call146)
  call void @store_float(float* %add.ptr141, float %call147)
  %142 = bitcast [2 x float]* %w21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %142) #3
  %143 = bitcast [2 x float]* %w20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %143) #3
  %144 = bitcast [2 x float]* %w19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %144) #3
  %145 = bitcast [2 x float]* %w18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %145) #3
  %146 = bitcast [2 x float]* %w17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %146) #3
  %147 = bitcast [2 x float]* %w16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %147) #3
  %148 = bitcast [2 x float]* %w15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %148) #3
  %149 = bitcast [2 x float]* %w14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %149) #3
  %150 = bitcast [2 x float]* %w13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %150) #3
  %151 = bitcast [2 x float]* %w12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %151) #3
  %152 = bitcast [2 x float]* %w11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %152) #3
  %153 = bitcast [2 x float]* %w10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %153) #3
  %154 = bitcast [2 x float]* %w9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %154) #3
  %155 = bitcast [2 x float]* %w8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %155) #3
  %156 = bitcast float* %w7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #3
  %157 = bitcast float* %w6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #3
  %158 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #3
  %159 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #3
  %160 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #3
  %161 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #3
  %162 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #3
  %163 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #3
  %164 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #3
  %165 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #3
  %166 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #3
  %167 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft1d_16_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %kWeight0 = alloca float, align 4
  %kWeight2 = alloca float, align 4
  %kWeight3 = alloca float, align 4
  %kWeight4 = alloca float, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  %i2 = alloca float, align 4
  %i3 = alloca float, align 4
  %i4 = alloca float, align 4
  %i5 = alloca float, align 4
  %i6 = alloca float, align 4
  %i7 = alloca float, align 4
  %i8 = alloca float, align 4
  %i9 = alloca float, align 4
  %i10 = alloca float, align 4
  %i11 = alloca float, align 4
  %i12 = alloca float, align 4
  %i13 = alloca float, align 4
  %i14 = alloca float, align 4
  %i15 = alloca float, align 4
  %w14 = alloca float, align 4
  %w15 = alloca float, align 4
  %w16 = alloca [2 x float], align 4
  %w17 = alloca [2 x float], align 4
  %w18 = alloca [2 x float], align 4
  %w19 = alloca [2 x float], align 4
  %w20 = alloca [2 x float], align 4
  %w21 = alloca [2 x float], align 4
  %w22 = alloca [2 x float], align 4
  %w23 = alloca [2 x float], align 4
  %w24 = alloca [2 x float], align 4
  %w25 = alloca [2 x float], align 4
  %w26 = alloca [2 x float], align 4
  %w27 = alloca [2 x float], align 4
  %w28 = alloca [2 x float], align 4
  %w29 = alloca [2 x float], align 4
  %w30 = alloca [2 x float], align 4
  %w31 = alloca [2 x float], align 4
  %w32 = alloca [2 x float], align 4
  %w33 = alloca [2 x float], align 4
  %w34 = alloca [2 x float], align 4
  %w35 = alloca [2 x float], align 4
  %w36 = alloca [2 x float], align 4
  %w37 = alloca [2 x float], align 4
  %w38 = alloca [2 x float], align 4
  %w39 = alloca [2 x float], align 4
  %w40 = alloca [2 x float], align 4
  %w41 = alloca [2 x float], align 4
  %w42 = alloca [2 x float], align 4
  %w43 = alloca [2 x float], align 4
  %w44 = alloca [2 x float], align 4
  %w45 = alloca [2 x float], align 4
  %w46 = alloca [2 x float], align 4
  %w47 = alloca [2 x float], align 4
  %w48 = alloca [2 x float], align 4
  %w49 = alloca [2 x float], align 4
  %w50 = alloca [2 x float], align 4
  %w51 = alloca [2 x float], align 4
  %w52 = alloca [2 x float], align 4
  %w53 = alloca [2 x float], align 4
  %w54 = alloca [2 x float], align 4
  %w55 = alloca [2 x float], align 4
  %w56 = alloca [2 x float], align 4
  %w57 = alloca [2 x float], align 4
  %w58 = alloca [2 x float], align 4
  %w59 = alloca [2 x float], align 4
  %w60 = alloca [2 x float], align 4
  %w61 = alloca [2 x float], align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store float 0.000000e+00, float* %kWeight0, align 4, !tbaa !8
  %1 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store float 0x3FE6A09EE0000000, float* %kWeight2, align 4, !tbaa !8
  %2 = bitcast float* %kWeight3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store float 0x3FED906CC0000000, float* %kWeight3, align 4, !tbaa !8
  %3 = bitcast float* %kWeight4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store float 0x3FD87DE0E0000000, float* %kWeight4, align 4, !tbaa !8
  %4 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load float*, float** %input.addr, align 4, !tbaa !2
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %6
  %add.ptr = getelementptr inbounds float, float* %5, i32 %mul
  %7 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %7, float* %i0, align 4, !tbaa !8
  %8 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load float*, float** %input.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %10
  %add.ptr2 = getelementptr inbounds float, float* %9, i32 %mul1
  %11 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %11, float* %i1, align 4, !tbaa !8
  %12 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load float*, float** %input.addr, align 4, !tbaa !2
  %14 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 2, %14
  %add.ptr4 = getelementptr inbounds float, float* %13, i32 %mul3
  %15 = load float, float* %add.ptr4, align 4, !tbaa !8
  store float %15, float* %i2, align 4, !tbaa !8
  %16 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load float*, float** %input.addr, align 4, !tbaa !2
  %18 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %18
  %add.ptr6 = getelementptr inbounds float, float* %17, i32 %mul5
  %19 = load float, float* %add.ptr6, align 4, !tbaa !8
  store float %19, float* %i3, align 4, !tbaa !8
  %20 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load float*, float** %input.addr, align 4, !tbaa !2
  %22 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 4, %22
  %add.ptr8 = getelementptr inbounds float, float* %21, i32 %mul7
  %23 = load float, float* %add.ptr8, align 4, !tbaa !8
  store float %23, float* %i4, align 4, !tbaa !8
  %24 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load float*, float** %input.addr, align 4, !tbaa !2
  %26 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul9 = mul nsw i32 5, %26
  %add.ptr10 = getelementptr inbounds float, float* %25, i32 %mul9
  %27 = load float, float* %add.ptr10, align 4, !tbaa !8
  store float %27, float* %i5, align 4, !tbaa !8
  %28 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load float*, float** %input.addr, align 4, !tbaa !2
  %30 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 6, %30
  %add.ptr12 = getelementptr inbounds float, float* %29, i32 %mul11
  %31 = load float, float* %add.ptr12, align 4, !tbaa !8
  store float %31, float* %i6, align 4, !tbaa !8
  %32 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load float*, float** %input.addr, align 4, !tbaa !2
  %34 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 7, %34
  %add.ptr14 = getelementptr inbounds float, float* %33, i32 %mul13
  %35 = load float, float* %add.ptr14, align 4, !tbaa !8
  store float %35, float* %i7, align 4, !tbaa !8
  %36 = bitcast float* %i8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #3
  %37 = load float*, float** %input.addr, align 4, !tbaa !2
  %38 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 8, %38
  %add.ptr16 = getelementptr inbounds float, float* %37, i32 %mul15
  %39 = load float, float* %add.ptr16, align 4, !tbaa !8
  store float %39, float* %i8, align 4, !tbaa !8
  %40 = bitcast float* %i9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load float*, float** %input.addr, align 4, !tbaa !2
  %42 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 9, %42
  %add.ptr18 = getelementptr inbounds float, float* %41, i32 %mul17
  %43 = load float, float* %add.ptr18, align 4, !tbaa !8
  store float %43, float* %i9, align 4, !tbaa !8
  %44 = bitcast float* %i10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load float*, float** %input.addr, align 4, !tbaa !2
  %46 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul19 = mul nsw i32 10, %46
  %add.ptr20 = getelementptr inbounds float, float* %45, i32 %mul19
  %47 = load float, float* %add.ptr20, align 4, !tbaa !8
  store float %47, float* %i10, align 4, !tbaa !8
  %48 = bitcast float* %i11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #3
  %49 = load float*, float** %input.addr, align 4, !tbaa !2
  %50 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul21 = mul nsw i32 11, %50
  %add.ptr22 = getelementptr inbounds float, float* %49, i32 %mul21
  %51 = load float, float* %add.ptr22, align 4, !tbaa !8
  store float %51, float* %i11, align 4, !tbaa !8
  %52 = bitcast float* %i12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  %53 = load float*, float** %input.addr, align 4, !tbaa !2
  %54 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 12, %54
  %add.ptr24 = getelementptr inbounds float, float* %53, i32 %mul23
  %55 = load float, float* %add.ptr24, align 4, !tbaa !8
  store float %55, float* %i12, align 4, !tbaa !8
  %56 = bitcast float* %i13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #3
  %57 = load float*, float** %input.addr, align 4, !tbaa !2
  %58 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 13, %58
  %add.ptr26 = getelementptr inbounds float, float* %57, i32 %mul25
  %59 = load float, float* %add.ptr26, align 4, !tbaa !8
  store float %59, float* %i13, align 4, !tbaa !8
  %60 = bitcast float* %i14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #3
  %61 = load float*, float** %input.addr, align 4, !tbaa !2
  %62 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul27 = mul nsw i32 14, %62
  %add.ptr28 = getelementptr inbounds float, float* %61, i32 %mul27
  %63 = load float, float* %add.ptr28, align 4, !tbaa !8
  store float %63, float* %i14, align 4, !tbaa !8
  %64 = bitcast float* %i15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  %65 = load float*, float** %input.addr, align 4, !tbaa !2
  %66 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 15, %66
  %add.ptr30 = getelementptr inbounds float, float* %65, i32 %mul29
  %67 = load float, float* %add.ptr30, align 4, !tbaa !8
  store float %67, float* %i15, align 4, !tbaa !8
  %68 = bitcast float* %w14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #3
  %69 = load float, float* %i0, align 4, !tbaa !8
  %70 = load float, float* %i8, align 4, !tbaa !8
  %call = call float @add_float(float %69, float %70)
  store float %call, float* %w14, align 4, !tbaa !8
  %71 = bitcast float* %w15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #3
  %72 = load float, float* %i0, align 4, !tbaa !8
  %73 = load float, float* %i8, align 4, !tbaa !8
  %call31 = call float @sub_float(float %72, float %73)
  store float %call31, float* %w15, align 4, !tbaa !8
  %74 = bitcast [2 x float]* %w16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %74) #3
  %arrayinit.begin = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %75 = load float, float* %i4, align 4, !tbaa !8
  %76 = load float, float* %i4, align 4, !tbaa !8
  %call32 = call float @add_float(float %75, float %76)
  store float %call32, float* %arrayinit.begin, align 4, !tbaa !8
  %arrayinit.element = getelementptr inbounds float, float* %arrayinit.begin, i32 1
  %77 = load float, float* %i12, align 4, !tbaa !8
  %78 = load float, float* %i12, align 4, !tbaa !8
  %call33 = call float @sub_float(float %77, float %78)
  store float %call33, float* %arrayinit.element, align 4, !tbaa !8
  %79 = bitcast [2 x float]* %w17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %79) #3
  %arrayinit.begin34 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 0
  %80 = load float, float* %i4, align 4, !tbaa !8
  %81 = load float, float* %i4, align 4, !tbaa !8
  %call35 = call float @sub_float(float %80, float %81)
  store float %call35, float* %arrayinit.begin34, align 4, !tbaa !8
  %arrayinit.element36 = getelementptr inbounds float, float* %arrayinit.begin34, i32 1
  %82 = load float, float* %i12, align 4, !tbaa !8
  %call37 = call float @sub_float(float 0.000000e+00, float %82)
  %83 = load float, float* %i12, align 4, !tbaa !8
  %call38 = call float @sub_float(float %call37, float %83)
  store float %call38, float* %arrayinit.element36, align 4, !tbaa !8
  %84 = bitcast [2 x float]* %w18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %84) #3
  %arrayinit.begin39 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %85 = load float, float* %w14, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %86 = load float, float* %arrayidx, align 4, !tbaa !8
  %call40 = call float @add_float(float %85, float %86)
  store float %call40, float* %arrayinit.begin39, align 4, !tbaa !8
  %arrayinit.element41 = getelementptr inbounds float, float* %arrayinit.begin39, i32 1
  %arrayidx42 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 1
  %87 = load float, float* %arrayidx42, align 4, !tbaa !8
  store float %87, float* %arrayinit.element41, align 4, !tbaa !8
  %88 = bitcast [2 x float]* %w19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %88) #3
  %arrayinit.begin43 = getelementptr inbounds [2 x float], [2 x float]* %w19, i32 0, i32 0
  %89 = load float, float* %w14, align 4, !tbaa !8
  %arrayidx44 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 0
  %90 = load float, float* %arrayidx44, align 4, !tbaa !8
  %call45 = call float @sub_float(float %89, float %90)
  store float %call45, float* %arrayinit.begin43, align 4, !tbaa !8
  %arrayinit.element46 = getelementptr inbounds float, float* %arrayinit.begin43, i32 1
  %arrayidx47 = getelementptr inbounds [2 x float], [2 x float]* %w16, i32 0, i32 1
  %91 = load float, float* %arrayidx47, align 4, !tbaa !8
  %call48 = call float @sub_float(float 0.000000e+00, float %91)
  store float %call48, float* %arrayinit.element46, align 4, !tbaa !8
  %92 = bitcast [2 x float]* %w20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %92) #3
  %arrayinit.begin49 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 0
  %93 = load float, float* %w15, align 4, !tbaa !8
  %arrayidx50 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 1
  %94 = load float, float* %arrayidx50, align 4, !tbaa !8
  %call51 = call float @add_float(float %93, float %94)
  store float %call51, float* %arrayinit.begin49, align 4, !tbaa !8
  %arrayinit.element52 = getelementptr inbounds float, float* %arrayinit.begin49, i32 1
  %arrayidx53 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 0
  %95 = load float, float* %arrayidx53, align 4, !tbaa !8
  %call54 = call float @sub_float(float 0.000000e+00, float %95)
  store float %call54, float* %arrayinit.element52, align 4, !tbaa !8
  %96 = bitcast [2 x float]* %w21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %96) #3
  %arrayinit.begin55 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 0
  %97 = load float, float* %w15, align 4, !tbaa !8
  %arrayidx56 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 1
  %98 = load float, float* %arrayidx56, align 4, !tbaa !8
  %call57 = call float @sub_float(float %97, float %98)
  store float %call57, float* %arrayinit.begin55, align 4, !tbaa !8
  %arrayinit.element58 = getelementptr inbounds float, float* %arrayinit.begin55, i32 1
  %arrayidx59 = getelementptr inbounds [2 x float], [2 x float]* %w17, i32 0, i32 0
  %99 = load float, float* %arrayidx59, align 4, !tbaa !8
  store float %99, float* %arrayinit.element58, align 4, !tbaa !8
  %100 = bitcast [2 x float]* %w22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %100) #3
  %arrayinit.begin60 = getelementptr inbounds [2 x float], [2 x float]* %w22, i32 0, i32 0
  %101 = load float, float* %i2, align 4, !tbaa !8
  %102 = load float, float* %i6, align 4, !tbaa !8
  %call61 = call float @add_float(float %101, float %102)
  store float %call61, float* %arrayinit.begin60, align 4, !tbaa !8
  %arrayinit.element62 = getelementptr inbounds float, float* %arrayinit.begin60, i32 1
  %103 = load float, float* %i14, align 4, !tbaa !8
  %104 = load float, float* %i10, align 4, !tbaa !8
  %call63 = call float @sub_float(float %103, float %104)
  store float %call63, float* %arrayinit.element62, align 4, !tbaa !8
  %105 = bitcast [2 x float]* %w23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %105) #3
  %arrayinit.begin64 = getelementptr inbounds [2 x float], [2 x float]* %w23, i32 0, i32 0
  %106 = load float, float* %i2, align 4, !tbaa !8
  %107 = load float, float* %i6, align 4, !tbaa !8
  %call65 = call float @sub_float(float %106, float %107)
  store float %call65, float* %arrayinit.begin64, align 4, !tbaa !8
  %arrayinit.element66 = getelementptr inbounds float, float* %arrayinit.begin64, i32 1
  %108 = load float, float* %i10, align 4, !tbaa !8
  %call67 = call float @sub_float(float 0.000000e+00, float %108)
  %109 = load float, float* %i14, align 4, !tbaa !8
  %call68 = call float @sub_float(float %call67, float %109)
  store float %call68, float* %arrayinit.element66, align 4, !tbaa !8
  %110 = bitcast [2 x float]* %w24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %110) #3
  %arrayinit.begin69 = getelementptr inbounds [2 x float], [2 x float]* %w24, i32 0, i32 0
  %111 = load float, float* %i6, align 4, !tbaa !8
  %112 = load float, float* %i2, align 4, !tbaa !8
  %call70 = call float @add_float(float %111, float %112)
  store float %call70, float* %arrayinit.begin69, align 4, !tbaa !8
  %arrayinit.element71 = getelementptr inbounds float, float* %arrayinit.begin69, i32 1
  %113 = load float, float* %i10, align 4, !tbaa !8
  %114 = load float, float* %i14, align 4, !tbaa !8
  %call72 = call float @sub_float(float %113, float %114)
  store float %call72, float* %arrayinit.element71, align 4, !tbaa !8
  %115 = bitcast [2 x float]* %w25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %115) #3
  %arrayinit.begin73 = getelementptr inbounds [2 x float], [2 x float]* %w25, i32 0, i32 0
  %116 = load float, float* %i6, align 4, !tbaa !8
  %117 = load float, float* %i2, align 4, !tbaa !8
  %call74 = call float @sub_float(float %116, float %117)
  store float %call74, float* %arrayinit.begin73, align 4, !tbaa !8
  %arrayinit.element75 = getelementptr inbounds float, float* %arrayinit.begin73, i32 1
  %118 = load float, float* %i14, align 4, !tbaa !8
  %call76 = call float @sub_float(float 0.000000e+00, float %118)
  %119 = load float, float* %i10, align 4, !tbaa !8
  %call77 = call float @sub_float(float %call76, float %119)
  store float %call77, float* %arrayinit.element75, align 4, !tbaa !8
  %120 = bitcast [2 x float]* %w26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %120) #3
  %arrayinit.begin78 = getelementptr inbounds [2 x float], [2 x float]* %w26, i32 0, i32 0
  %arrayidx79 = getelementptr inbounds [2 x float], [2 x float]* %w22, i32 0, i32 0
  %121 = load float, float* %arrayidx79, align 4, !tbaa !8
  %arrayidx80 = getelementptr inbounds [2 x float], [2 x float]* %w24, i32 0, i32 0
  %122 = load float, float* %arrayidx80, align 4, !tbaa !8
  %call81 = call float @add_float(float %121, float %122)
  store float %call81, float* %arrayinit.begin78, align 4, !tbaa !8
  %arrayinit.element82 = getelementptr inbounds float, float* %arrayinit.begin78, i32 1
  %arrayidx83 = getelementptr inbounds [2 x float], [2 x float]* %w22, i32 0, i32 1
  %123 = load float, float* %arrayidx83, align 4, !tbaa !8
  %arrayidx84 = getelementptr inbounds [2 x float], [2 x float]* %w24, i32 0, i32 1
  %124 = load float, float* %arrayidx84, align 4, !tbaa !8
  %call85 = call float @add_float(float %123, float %124)
  store float %call85, float* %arrayinit.element82, align 4, !tbaa !8
  %125 = bitcast [2 x float]* %w27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %125) #3
  %arrayinit.begin86 = getelementptr inbounds [2 x float], [2 x float]* %w27, i32 0, i32 0
  %arrayidx87 = getelementptr inbounds [2 x float], [2 x float]* %w22, i32 0, i32 0
  %126 = load float, float* %arrayidx87, align 4, !tbaa !8
  %arrayidx88 = getelementptr inbounds [2 x float], [2 x float]* %w24, i32 0, i32 0
  %127 = load float, float* %arrayidx88, align 4, !tbaa !8
  %call89 = call float @sub_float(float %126, float %127)
  store float %call89, float* %arrayinit.begin86, align 4, !tbaa !8
  %arrayinit.element90 = getelementptr inbounds float, float* %arrayinit.begin86, i32 1
  %arrayidx91 = getelementptr inbounds [2 x float], [2 x float]* %w22, i32 0, i32 1
  %128 = load float, float* %arrayidx91, align 4, !tbaa !8
  %arrayidx92 = getelementptr inbounds [2 x float], [2 x float]* %w24, i32 0, i32 1
  %129 = load float, float* %arrayidx92, align 4, !tbaa !8
  %call93 = call float @sub_float(float %128, float %129)
  store float %call93, float* %arrayinit.element90, align 4, !tbaa !8
  %130 = bitcast [2 x float]* %w28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %130) #3
  %arrayinit.begin94 = getelementptr inbounds [2 x float], [2 x float]* %w28, i32 0, i32 0
  %arrayidx95 = getelementptr inbounds [2 x float], [2 x float]* %w23, i32 0, i32 0
  %131 = load float, float* %arrayidx95, align 4, !tbaa !8
  %arrayidx96 = getelementptr inbounds [2 x float], [2 x float]* %w25, i32 0, i32 1
  %132 = load float, float* %arrayidx96, align 4, !tbaa !8
  %call97 = call float @add_float(float %131, float %132)
  store float %call97, float* %arrayinit.begin94, align 4, !tbaa !8
  %arrayinit.element98 = getelementptr inbounds float, float* %arrayinit.begin94, i32 1
  %arrayidx99 = getelementptr inbounds [2 x float], [2 x float]* %w23, i32 0, i32 1
  %133 = load float, float* %arrayidx99, align 4, !tbaa !8
  %arrayidx100 = getelementptr inbounds [2 x float], [2 x float]* %w25, i32 0, i32 0
  %134 = load float, float* %arrayidx100, align 4, !tbaa !8
  %call101 = call float @sub_float(float %133, float %134)
  store float %call101, float* %arrayinit.element98, align 4, !tbaa !8
  %135 = bitcast [2 x float]* %w29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %135) #3
  %arrayinit.begin102 = getelementptr inbounds [2 x float], [2 x float]* %w29, i32 0, i32 0
  %arrayidx103 = getelementptr inbounds [2 x float], [2 x float]* %w23, i32 0, i32 0
  %136 = load float, float* %arrayidx103, align 4, !tbaa !8
  %arrayidx104 = getelementptr inbounds [2 x float], [2 x float]* %w25, i32 0, i32 1
  %137 = load float, float* %arrayidx104, align 4, !tbaa !8
  %call105 = call float @sub_float(float %136, float %137)
  store float %call105, float* %arrayinit.begin102, align 4, !tbaa !8
  %arrayinit.element106 = getelementptr inbounds float, float* %arrayinit.begin102, i32 1
  %arrayidx107 = getelementptr inbounds [2 x float], [2 x float]* %w23, i32 0, i32 1
  %138 = load float, float* %arrayidx107, align 4, !tbaa !8
  %arrayidx108 = getelementptr inbounds [2 x float], [2 x float]* %w25, i32 0, i32 0
  %139 = load float, float* %arrayidx108, align 4, !tbaa !8
  %call109 = call float @add_float(float %138, float %139)
  store float %call109, float* %arrayinit.element106, align 4, !tbaa !8
  %140 = bitcast [2 x float]* %w30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %140) #3
  %arrayinit.begin110 = getelementptr inbounds [2 x float], [2 x float]* %w30, i32 0, i32 0
  %arrayidx111 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %141 = load float, float* %arrayidx111, align 4, !tbaa !8
  %arrayidx112 = getelementptr inbounds [2 x float], [2 x float]* %w26, i32 0, i32 0
  %142 = load float, float* %arrayidx112, align 4, !tbaa !8
  %call113 = call float @add_float(float %141, float %142)
  store float %call113, float* %arrayinit.begin110, align 4, !tbaa !8
  %arrayinit.element114 = getelementptr inbounds float, float* %arrayinit.begin110, i32 1
  %arrayidx115 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 1
  %143 = load float, float* %arrayidx115, align 4, !tbaa !8
  %arrayidx116 = getelementptr inbounds [2 x float], [2 x float]* %w26, i32 0, i32 1
  %144 = load float, float* %arrayidx116, align 4, !tbaa !8
  %call117 = call float @add_float(float %143, float %144)
  store float %call117, float* %arrayinit.element114, align 4, !tbaa !8
  %145 = bitcast [2 x float]* %w31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %145) #3
  %arrayinit.begin118 = getelementptr inbounds [2 x float], [2 x float]* %w31, i32 0, i32 0
  %arrayidx119 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 0
  %146 = load float, float* %arrayidx119, align 4, !tbaa !8
  %arrayidx120 = getelementptr inbounds [2 x float], [2 x float]* %w26, i32 0, i32 0
  %147 = load float, float* %arrayidx120, align 4, !tbaa !8
  %call121 = call float @sub_float(float %146, float %147)
  store float %call121, float* %arrayinit.begin118, align 4, !tbaa !8
  %arrayinit.element122 = getelementptr inbounds float, float* %arrayinit.begin118, i32 1
  %arrayidx123 = getelementptr inbounds [2 x float], [2 x float]* %w18, i32 0, i32 1
  %148 = load float, float* %arrayidx123, align 4, !tbaa !8
  %arrayidx124 = getelementptr inbounds [2 x float], [2 x float]* %w26, i32 0, i32 1
  %149 = load float, float* %arrayidx124, align 4, !tbaa !8
  %call125 = call float @sub_float(float %148, float %149)
  store float %call125, float* %arrayinit.element122, align 4, !tbaa !8
  %150 = bitcast [2 x float]* %w32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %150) #3
  %arrayinit.begin126 = getelementptr inbounds [2 x float], [2 x float]* %w32, i32 0, i32 0
  %arrayidx127 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 0
  %151 = load float, float* %arrayidx127, align 4, !tbaa !8
  %arrayidx128 = getelementptr inbounds [2 x float], [2 x float]* %w28, i32 0, i32 0
  %152 = load float, float* %arrayidx128, align 4, !tbaa !8
  %arrayidx129 = getelementptr inbounds [2 x float], [2 x float]* %w28, i32 0, i32 1
  %153 = load float, float* %arrayidx129, align 4, !tbaa !8
  %call130 = call float @add_float(float %152, float %153)
  %call131 = call float @mul_float(float 0x3FE6A09EE0000000, float %call130)
  %call132 = call float @add_float(float %151, float %call131)
  store float %call132, float* %arrayinit.begin126, align 4, !tbaa !8
  %arrayinit.element133 = getelementptr inbounds float, float* %arrayinit.begin126, i32 1
  %arrayidx134 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 1
  %154 = load float, float* %arrayidx134, align 4, !tbaa !8
  %arrayidx135 = getelementptr inbounds [2 x float], [2 x float]* %w28, i32 0, i32 1
  %155 = load float, float* %arrayidx135, align 4, !tbaa !8
  %arrayidx136 = getelementptr inbounds [2 x float], [2 x float]* %w28, i32 0, i32 0
  %156 = load float, float* %arrayidx136, align 4, !tbaa !8
  %call137 = call float @sub_float(float %155, float %156)
  %call138 = call float @mul_float(float 0x3FE6A09EE0000000, float %call137)
  %call139 = call float @add_float(float %154, float %call138)
  store float %call139, float* %arrayinit.element133, align 4, !tbaa !8
  %157 = bitcast [2 x float]* %w33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %157) #3
  %arrayinit.begin140 = getelementptr inbounds [2 x float], [2 x float]* %w33, i32 0, i32 0
  %arrayidx141 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 0
  %158 = load float, float* %arrayidx141, align 4, !tbaa !8
  %arrayidx142 = getelementptr inbounds [2 x float], [2 x float]* %w28, i32 0, i32 0
  %159 = load float, float* %arrayidx142, align 4, !tbaa !8
  %call143 = call float @mul_float(float 0x3FE6A09EE0000000, float %159)
  %call144 = call float @sub_float(float 0.000000e+00, float %call143)
  %arrayidx145 = getelementptr inbounds [2 x float], [2 x float]* %w28, i32 0, i32 1
  %160 = load float, float* %arrayidx145, align 4, !tbaa !8
  %call146 = call float @mul_float(float 0x3FE6A09EE0000000, float %160)
  %call147 = call float @sub_float(float %call144, float %call146)
  %call148 = call float @add_float(float %158, float %call147)
  store float %call148, float* %arrayinit.begin140, align 4, !tbaa !8
  %arrayinit.element149 = getelementptr inbounds float, float* %arrayinit.begin140, i32 1
  %arrayidx150 = getelementptr inbounds [2 x float], [2 x float]* %w20, i32 0, i32 1
  %161 = load float, float* %arrayidx150, align 4, !tbaa !8
  %arrayidx151 = getelementptr inbounds [2 x float], [2 x float]* %w28, i32 0, i32 0
  %162 = load float, float* %arrayidx151, align 4, !tbaa !8
  %arrayidx152 = getelementptr inbounds [2 x float], [2 x float]* %w28, i32 0, i32 1
  %163 = load float, float* %arrayidx152, align 4, !tbaa !8
  %call153 = call float @sub_float(float %162, float %163)
  %call154 = call float @mul_float(float 0x3FE6A09EE0000000, float %call153)
  %call155 = call float @add_float(float %161, float %call154)
  store float %call155, float* %arrayinit.element149, align 4, !tbaa !8
  %164 = bitcast [2 x float]* %w34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %164) #3
  %arrayinit.begin156 = getelementptr inbounds [2 x float], [2 x float]* %w34, i32 0, i32 0
  %arrayidx157 = getelementptr inbounds [2 x float], [2 x float]* %w19, i32 0, i32 0
  %165 = load float, float* %arrayidx157, align 4, !tbaa !8
  %arrayidx158 = getelementptr inbounds [2 x float], [2 x float]* %w27, i32 0, i32 1
  %166 = load float, float* %arrayidx158, align 4, !tbaa !8
  %call159 = call float @add_float(float %165, float %166)
  store float %call159, float* %arrayinit.begin156, align 4, !tbaa !8
  %arrayinit.element160 = getelementptr inbounds float, float* %arrayinit.begin156, i32 1
  %arrayidx161 = getelementptr inbounds [2 x float], [2 x float]* %w19, i32 0, i32 1
  %167 = load float, float* %arrayidx161, align 4, !tbaa !8
  %arrayidx162 = getelementptr inbounds [2 x float], [2 x float]* %w27, i32 0, i32 0
  %168 = load float, float* %arrayidx162, align 4, !tbaa !8
  %call163 = call float @sub_float(float %167, float %168)
  store float %call163, float* %arrayinit.element160, align 4, !tbaa !8
  %169 = bitcast [2 x float]* %w35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %169) #3
  %arrayinit.begin164 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %arrayidx165 = getelementptr inbounds [2 x float], [2 x float]* %w19, i32 0, i32 0
  %170 = load float, float* %arrayidx165, align 4, !tbaa !8
  %arrayidx166 = getelementptr inbounds [2 x float], [2 x float]* %w27, i32 0, i32 1
  %171 = load float, float* %arrayidx166, align 4, !tbaa !8
  %call167 = call float @sub_float(float %170, float %171)
  store float %call167, float* %arrayinit.begin164, align 4, !tbaa !8
  %arrayinit.element168 = getelementptr inbounds float, float* %arrayinit.begin164, i32 1
  %arrayidx169 = getelementptr inbounds [2 x float], [2 x float]* %w19, i32 0, i32 1
  %172 = load float, float* %arrayidx169, align 4, !tbaa !8
  %arrayidx170 = getelementptr inbounds [2 x float], [2 x float]* %w27, i32 0, i32 0
  %173 = load float, float* %arrayidx170, align 4, !tbaa !8
  %call171 = call float @add_float(float %172, float %173)
  store float %call171, float* %arrayinit.element168, align 4, !tbaa !8
  %174 = bitcast [2 x float]* %w36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %174) #3
  %arrayinit.begin172 = getelementptr inbounds [2 x float], [2 x float]* %w36, i32 0, i32 0
  %arrayidx173 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 0
  %175 = load float, float* %arrayidx173, align 4, !tbaa !8
  %arrayidx174 = getelementptr inbounds [2 x float], [2 x float]* %w29, i32 0, i32 0
  %176 = load float, float* %arrayidx174, align 4, !tbaa !8
  %arrayidx175 = getelementptr inbounds [2 x float], [2 x float]* %w29, i32 0, i32 1
  %177 = load float, float* %arrayidx175, align 4, !tbaa !8
  %call176 = call float @sub_float(float %176, float %177)
  %call177 = call float @mul_float(float 0x3FE6A09EE0000000, float %call176)
  %call178 = call float @sub_float(float %175, float %call177)
  store float %call178, float* %arrayinit.begin172, align 4, !tbaa !8
  %arrayinit.element179 = getelementptr inbounds float, float* %arrayinit.begin172, i32 1
  %arrayidx180 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 1
  %178 = load float, float* %arrayidx180, align 4, !tbaa !8
  %arrayidx181 = getelementptr inbounds [2 x float], [2 x float]* %w29, i32 0, i32 1
  %179 = load float, float* %arrayidx181, align 4, !tbaa !8
  %arrayidx182 = getelementptr inbounds [2 x float], [2 x float]* %w29, i32 0, i32 0
  %180 = load float, float* %arrayidx182, align 4, !tbaa !8
  %call183 = call float @add_float(float %179, float %180)
  %call184 = call float @mul_float(float 0x3FE6A09EE0000000, float %call183)
  %call185 = call float @sub_float(float %178, float %call184)
  store float %call185, float* %arrayinit.element179, align 4, !tbaa !8
  %181 = bitcast [2 x float]* %w37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %181) #3
  %arrayinit.begin186 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %arrayidx187 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 0
  %182 = load float, float* %arrayidx187, align 4, !tbaa !8
  %arrayidx188 = getelementptr inbounds [2 x float], [2 x float]* %w29, i32 0, i32 0
  %183 = load float, float* %arrayidx188, align 4, !tbaa !8
  %arrayidx189 = getelementptr inbounds [2 x float], [2 x float]* %w29, i32 0, i32 1
  %184 = load float, float* %arrayidx189, align 4, !tbaa !8
  %call190 = call float @sub_float(float %183, float %184)
  %call191 = call float @mul_float(float 0x3FE6A09EE0000000, float %call190)
  %call192 = call float @add_float(float %182, float %call191)
  store float %call192, float* %arrayinit.begin186, align 4, !tbaa !8
  %arrayinit.element193 = getelementptr inbounds float, float* %arrayinit.begin186, i32 1
  %arrayidx194 = getelementptr inbounds [2 x float], [2 x float]* %w21, i32 0, i32 1
  %185 = load float, float* %arrayidx194, align 4, !tbaa !8
  %arrayidx195 = getelementptr inbounds [2 x float], [2 x float]* %w29, i32 0, i32 1
  %186 = load float, float* %arrayidx195, align 4, !tbaa !8
  %arrayidx196 = getelementptr inbounds [2 x float], [2 x float]* %w29, i32 0, i32 0
  %187 = load float, float* %arrayidx196, align 4, !tbaa !8
  %call197 = call float @add_float(float %186, float %187)
  %call198 = call float @mul_float(float 0x3FE6A09EE0000000, float %call197)
  %call199 = call float @add_float(float %185, float %call198)
  store float %call199, float* %arrayinit.element193, align 4, !tbaa !8
  %188 = bitcast [2 x float]* %w38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %188) #3
  %arrayinit.begin200 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 0
  %189 = load float, float* %i1, align 4, !tbaa !8
  %190 = load float, float* %i7, align 4, !tbaa !8
  %call201 = call float @add_float(float %189, float %190)
  store float %call201, float* %arrayinit.begin200, align 4, !tbaa !8
  %arrayinit.element202 = getelementptr inbounds float, float* %arrayinit.begin200, i32 1
  %191 = load float, float* %i15, align 4, !tbaa !8
  %192 = load float, float* %i9, align 4, !tbaa !8
  %call203 = call float @sub_float(float %191, float %192)
  store float %call203, float* %arrayinit.element202, align 4, !tbaa !8
  %193 = bitcast [2 x float]* %w39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %193) #3
  %arrayinit.begin204 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 0
  %194 = load float, float* %i1, align 4, !tbaa !8
  %195 = load float, float* %i7, align 4, !tbaa !8
  %call205 = call float @sub_float(float %194, float %195)
  store float %call205, float* %arrayinit.begin204, align 4, !tbaa !8
  %arrayinit.element206 = getelementptr inbounds float, float* %arrayinit.begin204, i32 1
  %196 = load float, float* %i9, align 4, !tbaa !8
  %call207 = call float @sub_float(float 0.000000e+00, float %196)
  %197 = load float, float* %i15, align 4, !tbaa !8
  %call208 = call float @sub_float(float %call207, float %197)
  store float %call208, float* %arrayinit.element206, align 4, !tbaa !8
  %198 = bitcast [2 x float]* %w40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %198) #3
  %arrayinit.begin209 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 0
  %199 = load float, float* %i5, align 4, !tbaa !8
  %200 = load float, float* %i3, align 4, !tbaa !8
  %call210 = call float @add_float(float %199, float %200)
  store float %call210, float* %arrayinit.begin209, align 4, !tbaa !8
  %arrayinit.element211 = getelementptr inbounds float, float* %arrayinit.begin209, i32 1
  %201 = load float, float* %i11, align 4, !tbaa !8
  %202 = load float, float* %i13, align 4, !tbaa !8
  %call212 = call float @sub_float(float %201, float %202)
  store float %call212, float* %arrayinit.element211, align 4, !tbaa !8
  %203 = bitcast [2 x float]* %w41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %203) #3
  %arrayinit.begin213 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 0
  %204 = load float, float* %i5, align 4, !tbaa !8
  %205 = load float, float* %i3, align 4, !tbaa !8
  %call214 = call float @sub_float(float %204, float %205)
  store float %call214, float* %arrayinit.begin213, align 4, !tbaa !8
  %arrayinit.element215 = getelementptr inbounds float, float* %arrayinit.begin213, i32 1
  %206 = load float, float* %i13, align 4, !tbaa !8
  %call216 = call float @sub_float(float 0.000000e+00, float %206)
  %207 = load float, float* %i11, align 4, !tbaa !8
  %call217 = call float @sub_float(float %call216, float %207)
  store float %call217, float* %arrayinit.element215, align 4, !tbaa !8
  %208 = bitcast [2 x float]* %w42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %208) #3
  %arrayinit.begin218 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 0
  %arrayidx219 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 0
  %209 = load float, float* %arrayidx219, align 4, !tbaa !8
  %arrayidx220 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 0
  %210 = load float, float* %arrayidx220, align 4, !tbaa !8
  %call221 = call float @add_float(float %209, float %210)
  store float %call221, float* %arrayinit.begin218, align 4, !tbaa !8
  %arrayinit.element222 = getelementptr inbounds float, float* %arrayinit.begin218, i32 1
  %arrayidx223 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 1
  %211 = load float, float* %arrayidx223, align 4, !tbaa !8
  %arrayidx224 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 1
  %212 = load float, float* %arrayidx224, align 4, !tbaa !8
  %call225 = call float @add_float(float %211, float %212)
  store float %call225, float* %arrayinit.element222, align 4, !tbaa !8
  %213 = bitcast [2 x float]* %w43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %213) #3
  %arrayinit.begin226 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 0
  %arrayidx227 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 0
  %214 = load float, float* %arrayidx227, align 4, !tbaa !8
  %arrayidx228 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 0
  %215 = load float, float* %arrayidx228, align 4, !tbaa !8
  %call229 = call float @sub_float(float %214, float %215)
  store float %call229, float* %arrayinit.begin226, align 4, !tbaa !8
  %arrayinit.element230 = getelementptr inbounds float, float* %arrayinit.begin226, i32 1
  %arrayidx231 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 1
  %216 = load float, float* %arrayidx231, align 4, !tbaa !8
  %arrayidx232 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 1
  %217 = load float, float* %arrayidx232, align 4, !tbaa !8
  %call233 = call float @sub_float(float %216, float %217)
  store float %call233, float* %arrayinit.element230, align 4, !tbaa !8
  %218 = bitcast [2 x float]* %w44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %218) #3
  %arrayinit.begin234 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %arrayidx235 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 0
  %219 = load float, float* %arrayidx235, align 4, !tbaa !8
  %arrayidx236 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 1
  %220 = load float, float* %arrayidx236, align 4, !tbaa !8
  %call237 = call float @add_float(float %219, float %220)
  store float %call237, float* %arrayinit.begin234, align 4, !tbaa !8
  %arrayinit.element238 = getelementptr inbounds float, float* %arrayinit.begin234, i32 1
  %arrayidx239 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 1
  %221 = load float, float* %arrayidx239, align 4, !tbaa !8
  %arrayidx240 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 0
  %222 = load float, float* %arrayidx240, align 4, !tbaa !8
  %call241 = call float @sub_float(float %221, float %222)
  store float %call241, float* %arrayinit.element238, align 4, !tbaa !8
  %223 = bitcast [2 x float]* %w45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %223) #3
  %arrayinit.begin242 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %arrayidx243 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 0
  %224 = load float, float* %arrayidx243, align 4, !tbaa !8
  %arrayidx244 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 1
  %225 = load float, float* %arrayidx244, align 4, !tbaa !8
  %call245 = call float @sub_float(float %224, float %225)
  store float %call245, float* %arrayinit.begin242, align 4, !tbaa !8
  %arrayinit.element246 = getelementptr inbounds float, float* %arrayinit.begin242, i32 1
  %arrayidx247 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 1
  %226 = load float, float* %arrayidx247, align 4, !tbaa !8
  %arrayidx248 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 0
  %227 = load float, float* %arrayidx248, align 4, !tbaa !8
  %call249 = call float @add_float(float %226, float %227)
  store float %call249, float* %arrayinit.element246, align 4, !tbaa !8
  %228 = bitcast [2 x float]* %w46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %228) #3
  %arrayinit.begin250 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 0
  %229 = load float, float* %i3, align 4, !tbaa !8
  %230 = load float, float* %i5, align 4, !tbaa !8
  %call251 = call float @add_float(float %229, float %230)
  store float %call251, float* %arrayinit.begin250, align 4, !tbaa !8
  %arrayinit.element252 = getelementptr inbounds float, float* %arrayinit.begin250, i32 1
  %231 = load float, float* %i13, align 4, !tbaa !8
  %232 = load float, float* %i11, align 4, !tbaa !8
  %call253 = call float @sub_float(float %231, float %232)
  store float %call253, float* %arrayinit.element252, align 4, !tbaa !8
  %233 = bitcast [2 x float]* %w47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %233) #3
  %arrayinit.begin254 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 0
  %234 = load float, float* %i3, align 4, !tbaa !8
  %235 = load float, float* %i5, align 4, !tbaa !8
  %call255 = call float @sub_float(float %234, float %235)
  store float %call255, float* %arrayinit.begin254, align 4, !tbaa !8
  %arrayinit.element256 = getelementptr inbounds float, float* %arrayinit.begin254, i32 1
  %236 = load float, float* %i11, align 4, !tbaa !8
  %call257 = call float @sub_float(float 0.000000e+00, float %236)
  %237 = load float, float* %i13, align 4, !tbaa !8
  %call258 = call float @sub_float(float %call257, float %237)
  store float %call258, float* %arrayinit.element256, align 4, !tbaa !8
  %238 = bitcast [2 x float]* %w48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %238) #3
  %arrayinit.begin259 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 0
  %239 = load float, float* %i7, align 4, !tbaa !8
  %240 = load float, float* %i1, align 4, !tbaa !8
  %call260 = call float @add_float(float %239, float %240)
  store float %call260, float* %arrayinit.begin259, align 4, !tbaa !8
  %arrayinit.element261 = getelementptr inbounds float, float* %arrayinit.begin259, i32 1
  %241 = load float, float* %i9, align 4, !tbaa !8
  %242 = load float, float* %i15, align 4, !tbaa !8
  %call262 = call float @sub_float(float %241, float %242)
  store float %call262, float* %arrayinit.element261, align 4, !tbaa !8
  %243 = bitcast [2 x float]* %w49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %243) #3
  %arrayinit.begin263 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 0
  %244 = load float, float* %i7, align 4, !tbaa !8
  %245 = load float, float* %i1, align 4, !tbaa !8
  %call264 = call float @sub_float(float %244, float %245)
  store float %call264, float* %arrayinit.begin263, align 4, !tbaa !8
  %arrayinit.element265 = getelementptr inbounds float, float* %arrayinit.begin263, i32 1
  %246 = load float, float* %i15, align 4, !tbaa !8
  %call266 = call float @sub_float(float 0.000000e+00, float %246)
  %247 = load float, float* %i9, align 4, !tbaa !8
  %call267 = call float @sub_float(float %call266, float %247)
  store float %call267, float* %arrayinit.element265, align 4, !tbaa !8
  %248 = bitcast [2 x float]* %w50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %248) #3
  %arrayinit.begin268 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 0
  %arrayidx269 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 0
  %249 = load float, float* %arrayidx269, align 4, !tbaa !8
  %arrayidx270 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 0
  %250 = load float, float* %arrayidx270, align 4, !tbaa !8
  %call271 = call float @add_float(float %249, float %250)
  store float %call271, float* %arrayinit.begin268, align 4, !tbaa !8
  %arrayinit.element272 = getelementptr inbounds float, float* %arrayinit.begin268, i32 1
  %arrayidx273 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 1
  %251 = load float, float* %arrayidx273, align 4, !tbaa !8
  %arrayidx274 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 1
  %252 = load float, float* %arrayidx274, align 4, !tbaa !8
  %call275 = call float @add_float(float %251, float %252)
  store float %call275, float* %arrayinit.element272, align 4, !tbaa !8
  %253 = bitcast [2 x float]* %w51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %253) #3
  %arrayinit.begin276 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 0
  %arrayidx277 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 0
  %254 = load float, float* %arrayidx277, align 4, !tbaa !8
  %arrayidx278 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 0
  %255 = load float, float* %arrayidx278, align 4, !tbaa !8
  %call279 = call float @sub_float(float %254, float %255)
  store float %call279, float* %arrayinit.begin276, align 4, !tbaa !8
  %arrayinit.element280 = getelementptr inbounds float, float* %arrayinit.begin276, i32 1
  %arrayidx281 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 1
  %256 = load float, float* %arrayidx281, align 4, !tbaa !8
  %arrayidx282 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 1
  %257 = load float, float* %arrayidx282, align 4, !tbaa !8
  %call283 = call float @sub_float(float %256, float %257)
  store float %call283, float* %arrayinit.element280, align 4, !tbaa !8
  %258 = bitcast [2 x float]* %w52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %258) #3
  %arrayinit.begin284 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 0
  %arrayidx285 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 0
  %259 = load float, float* %arrayidx285, align 4, !tbaa !8
  %arrayidx286 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 1
  %260 = load float, float* %arrayidx286, align 4, !tbaa !8
  %call287 = call float @add_float(float %259, float %260)
  store float %call287, float* %arrayinit.begin284, align 4, !tbaa !8
  %arrayinit.element288 = getelementptr inbounds float, float* %arrayinit.begin284, i32 1
  %arrayidx289 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 1
  %261 = load float, float* %arrayidx289, align 4, !tbaa !8
  %arrayidx290 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 0
  %262 = load float, float* %arrayidx290, align 4, !tbaa !8
  %call291 = call float @sub_float(float %261, float %262)
  store float %call291, float* %arrayinit.element288, align 4, !tbaa !8
  %263 = bitcast [2 x float]* %w53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %263) #3
  %arrayinit.begin292 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 0
  %arrayidx293 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 0
  %264 = load float, float* %arrayidx293, align 4, !tbaa !8
  %arrayidx294 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 1
  %265 = load float, float* %arrayidx294, align 4, !tbaa !8
  %call295 = call float @sub_float(float %264, float %265)
  store float %call295, float* %arrayinit.begin292, align 4, !tbaa !8
  %arrayinit.element296 = getelementptr inbounds float, float* %arrayinit.begin292, i32 1
  %arrayidx297 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 1
  %266 = load float, float* %arrayidx297, align 4, !tbaa !8
  %arrayidx298 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 0
  %267 = load float, float* %arrayidx298, align 4, !tbaa !8
  %call299 = call float @add_float(float %266, float %267)
  store float %call299, float* %arrayinit.element296, align 4, !tbaa !8
  %268 = bitcast [2 x float]* %w54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %268) #3
  %arrayinit.begin300 = getelementptr inbounds [2 x float], [2 x float]* %w54, i32 0, i32 0
  %arrayidx301 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 0
  %269 = load float, float* %arrayidx301, align 4, !tbaa !8
  %arrayidx302 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 0
  %270 = load float, float* %arrayidx302, align 4, !tbaa !8
  %call303 = call float @add_float(float %269, float %270)
  store float %call303, float* %arrayinit.begin300, align 4, !tbaa !8
  %arrayinit.element304 = getelementptr inbounds float, float* %arrayinit.begin300, i32 1
  %arrayidx305 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 1
  %271 = load float, float* %arrayidx305, align 4, !tbaa !8
  %arrayidx306 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 1
  %272 = load float, float* %arrayidx306, align 4, !tbaa !8
  %call307 = call float @add_float(float %271, float %272)
  store float %call307, float* %arrayinit.element304, align 4, !tbaa !8
  %273 = bitcast [2 x float]* %w55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %273) #3
  %arrayinit.begin308 = getelementptr inbounds [2 x float], [2 x float]* %w55, i32 0, i32 0
  %arrayidx309 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 0
  %274 = load float, float* %arrayidx309, align 4, !tbaa !8
  %arrayidx310 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 0
  %275 = load float, float* %arrayidx310, align 4, !tbaa !8
  %call311 = call float @sub_float(float %274, float %275)
  store float %call311, float* %arrayinit.begin308, align 4, !tbaa !8
  %arrayinit.element312 = getelementptr inbounds float, float* %arrayinit.begin308, i32 1
  %arrayidx313 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 1
  %276 = load float, float* %arrayidx313, align 4, !tbaa !8
  %arrayidx314 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 1
  %277 = load float, float* %arrayidx314, align 4, !tbaa !8
  %call315 = call float @sub_float(float %276, float %277)
  store float %call315, float* %arrayinit.element312, align 4, !tbaa !8
  %278 = bitcast [2 x float]* %w56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %278) #3
  %arrayinit.begin316 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 0
  %arrayidx317 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %279 = load float, float* %arrayidx317, align 4, !tbaa !8
  %arrayidx318 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 0
  %280 = load float, float* %arrayidx318, align 4, !tbaa !8
  %arrayidx319 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 1
  %281 = load float, float* %arrayidx319, align 4, !tbaa !8
  %call320 = call float @add_float(float %280, float %281)
  %call321 = call float @mul_float(float 0x3FE6A09EE0000000, float %call320)
  %call322 = call float @add_float(float %279, float %call321)
  store float %call322, float* %arrayinit.begin316, align 4, !tbaa !8
  %arrayinit.element323 = getelementptr inbounds float, float* %arrayinit.begin316, i32 1
  %arrayidx324 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 1
  %282 = load float, float* %arrayidx324, align 4, !tbaa !8
  %arrayidx325 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 1
  %283 = load float, float* %arrayidx325, align 4, !tbaa !8
  %arrayidx326 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 0
  %284 = load float, float* %arrayidx326, align 4, !tbaa !8
  %call327 = call float @sub_float(float %283, float %284)
  %call328 = call float @mul_float(float 0x3FE6A09EE0000000, float %call327)
  %call329 = call float @add_float(float %282, float %call328)
  store float %call329, float* %arrayinit.element323, align 4, !tbaa !8
  %285 = bitcast [2 x float]* %w57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %285) #3
  %arrayinit.begin330 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 0
  %arrayidx331 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %286 = load float, float* %arrayidx331, align 4, !tbaa !8
  %arrayidx332 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 0
  %287 = load float, float* %arrayidx332, align 4, !tbaa !8
  %call333 = call float @mul_float(float 0x3FE6A09EE0000000, float %287)
  %call334 = call float @sub_float(float 0.000000e+00, float %call333)
  %arrayidx335 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 1
  %288 = load float, float* %arrayidx335, align 4, !tbaa !8
  %call336 = call float @mul_float(float 0x3FE6A09EE0000000, float %288)
  %call337 = call float @sub_float(float %call334, float %call336)
  %call338 = call float @add_float(float %286, float %call337)
  store float %call338, float* %arrayinit.begin330, align 4, !tbaa !8
  %arrayinit.element339 = getelementptr inbounds float, float* %arrayinit.begin330, i32 1
  %arrayidx340 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 1
  %289 = load float, float* %arrayidx340, align 4, !tbaa !8
  %arrayidx341 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 0
  %290 = load float, float* %arrayidx341, align 4, !tbaa !8
  %arrayidx342 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 1
  %291 = load float, float* %arrayidx342, align 4, !tbaa !8
  %call343 = call float @sub_float(float %290, float %291)
  %call344 = call float @mul_float(float 0x3FE6A09EE0000000, float %call343)
  %call345 = call float @add_float(float %289, float %call344)
  store float %call345, float* %arrayinit.element339, align 4, !tbaa !8
  %292 = bitcast [2 x float]* %w58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %292) #3
  %arrayinit.begin346 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 0
  %arrayidx347 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 0
  %293 = load float, float* %arrayidx347, align 4, !tbaa !8
  %arrayidx348 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 1
  %294 = load float, float* %arrayidx348, align 4, !tbaa !8
  %call349 = call float @add_float(float %293, float %294)
  store float %call349, float* %arrayinit.begin346, align 4, !tbaa !8
  %arrayinit.element350 = getelementptr inbounds float, float* %arrayinit.begin346, i32 1
  %arrayidx351 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 1
  %295 = load float, float* %arrayidx351, align 4, !tbaa !8
  %arrayidx352 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 0
  %296 = load float, float* %arrayidx352, align 4, !tbaa !8
  %call353 = call float @sub_float(float %295, float %296)
  store float %call353, float* %arrayinit.element350, align 4, !tbaa !8
  %297 = bitcast [2 x float]* %w59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %297) #3
  %arrayinit.begin354 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 0
  %arrayidx355 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 0
  %298 = load float, float* %arrayidx355, align 4, !tbaa !8
  %arrayidx356 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 1
  %299 = load float, float* %arrayidx356, align 4, !tbaa !8
  %call357 = call float @sub_float(float %298, float %299)
  store float %call357, float* %arrayinit.begin354, align 4, !tbaa !8
  %arrayinit.element358 = getelementptr inbounds float, float* %arrayinit.begin354, i32 1
  %arrayidx359 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 1
  %300 = load float, float* %arrayidx359, align 4, !tbaa !8
  %arrayidx360 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 0
  %301 = load float, float* %arrayidx360, align 4, !tbaa !8
  %call361 = call float @add_float(float %300, float %301)
  store float %call361, float* %arrayinit.element358, align 4, !tbaa !8
  %302 = bitcast [2 x float]* %w60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %302) #3
  %arrayinit.begin362 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 0
  %arrayidx363 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %303 = load float, float* %arrayidx363, align 4, !tbaa !8
  %arrayidx364 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 0
  %304 = load float, float* %arrayidx364, align 4, !tbaa !8
  %arrayidx365 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 1
  %305 = load float, float* %arrayidx365, align 4, !tbaa !8
  %call366 = call float @sub_float(float %304, float %305)
  %call367 = call float @mul_float(float 0x3FE6A09EE0000000, float %call366)
  %call368 = call float @sub_float(float %303, float %call367)
  store float %call368, float* %arrayinit.begin362, align 4, !tbaa !8
  %arrayinit.element369 = getelementptr inbounds float, float* %arrayinit.begin362, i32 1
  %arrayidx370 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 1
  %306 = load float, float* %arrayidx370, align 4, !tbaa !8
  %arrayidx371 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 1
  %307 = load float, float* %arrayidx371, align 4, !tbaa !8
  %arrayidx372 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 0
  %308 = load float, float* %arrayidx372, align 4, !tbaa !8
  %call373 = call float @add_float(float %307, float %308)
  %call374 = call float @mul_float(float 0x3FE6A09EE0000000, float %call373)
  %call375 = call float @sub_float(float %306, float %call374)
  store float %call375, float* %arrayinit.element369, align 4, !tbaa !8
  %309 = bitcast [2 x float]* %w61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %309) #3
  %arrayinit.begin376 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 0
  %arrayidx377 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %310 = load float, float* %arrayidx377, align 4, !tbaa !8
  %arrayidx378 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 0
  %311 = load float, float* %arrayidx378, align 4, !tbaa !8
  %arrayidx379 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 1
  %312 = load float, float* %arrayidx379, align 4, !tbaa !8
  %call380 = call float @sub_float(float %311, float %312)
  %call381 = call float @mul_float(float 0x3FE6A09EE0000000, float %call380)
  %call382 = call float @add_float(float %310, float %call381)
  store float %call382, float* %arrayinit.begin376, align 4, !tbaa !8
  %arrayinit.element383 = getelementptr inbounds float, float* %arrayinit.begin376, i32 1
  %arrayidx384 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 1
  %313 = load float, float* %arrayidx384, align 4, !tbaa !8
  %arrayidx385 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 1
  %314 = load float, float* %arrayidx385, align 4, !tbaa !8
  %arrayidx386 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 0
  %315 = load float, float* %arrayidx386, align 4, !tbaa !8
  %call387 = call float @add_float(float %314, float %315)
  %call388 = call float @mul_float(float 0x3FE6A09EE0000000, float %call387)
  %call389 = call float @add_float(float %313, float %call388)
  store float %call389, float* %arrayinit.element383, align 4, !tbaa !8
  %316 = load float*, float** %output.addr, align 4, !tbaa !2
  %317 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul390 = mul nsw i32 0, %317
  %add.ptr391 = getelementptr inbounds float, float* %316, i32 %mul390
  %arrayidx392 = getelementptr inbounds [2 x float], [2 x float]* %w30, i32 0, i32 0
  %318 = load float, float* %arrayidx392, align 4, !tbaa !8
  %arrayidx393 = getelementptr inbounds [2 x float], [2 x float]* %w54, i32 0, i32 0
  %319 = load float, float* %arrayidx393, align 4, !tbaa !8
  %call394 = call float @add_float(float %318, float %319)
  call void @store_float(float* %add.ptr391, float %call394)
  %320 = load float*, float** %output.addr, align 4, !tbaa !2
  %321 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul395 = mul nsw i32 1, %321
  %add.ptr396 = getelementptr inbounds float, float* %320, i32 %mul395
  %arrayidx397 = getelementptr inbounds [2 x float], [2 x float]* %w32, i32 0, i32 0
  %322 = load float, float* %arrayidx397, align 4, !tbaa !8
  %arrayidx398 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 0
  %323 = load float, float* %arrayidx398, align 4, !tbaa !8
  %call399 = call float @mul_float(float 0x3FED906CC0000000, float %323)
  %arrayidx400 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 1
  %324 = load float, float* %arrayidx400, align 4, !tbaa !8
  %call401 = call float @mul_float(float 0x3FD87DE0E0000000, float %324)
  %call402 = call float @add_float(float %call399, float %call401)
  %call403 = call float @add_float(float %322, float %call402)
  call void @store_float(float* %add.ptr396, float %call403)
  %325 = load float*, float** %output.addr, align 4, !tbaa !2
  %326 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul404 = mul nsw i32 2, %326
  %add.ptr405 = getelementptr inbounds float, float* %325, i32 %mul404
  %arrayidx406 = getelementptr inbounds [2 x float], [2 x float]* %w34, i32 0, i32 0
  %327 = load float, float* %arrayidx406, align 4, !tbaa !8
  %arrayidx407 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 0
  %328 = load float, float* %arrayidx407, align 4, !tbaa !8
  %arrayidx408 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 1
  %329 = load float, float* %arrayidx408, align 4, !tbaa !8
  %call409 = call float @add_float(float %328, float %329)
  %call410 = call float @mul_float(float 0x3FE6A09EE0000000, float %call409)
  %call411 = call float @add_float(float %327, float %call410)
  call void @store_float(float* %add.ptr405, float %call411)
  %330 = load float*, float** %output.addr, align 4, !tbaa !2
  %331 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul412 = mul nsw i32 3, %331
  %add.ptr413 = getelementptr inbounds float, float* %330, i32 %mul412
  %arrayidx414 = getelementptr inbounds [2 x float], [2 x float]* %w36, i32 0, i32 0
  %332 = load float, float* %arrayidx414, align 4, !tbaa !8
  %arrayidx415 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 0
  %333 = load float, float* %arrayidx415, align 4, !tbaa !8
  %call416 = call float @mul_float(float 0x3FD87DE0E0000000, float %333)
  %arrayidx417 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 1
  %334 = load float, float* %arrayidx417, align 4, !tbaa !8
  %call418 = call float @mul_float(float 0x3FED906CC0000000, float %334)
  %call419 = call float @add_float(float %call416, float %call418)
  %call420 = call float @add_float(float %332, float %call419)
  call void @store_float(float* %add.ptr413, float %call420)
  %335 = load float*, float** %output.addr, align 4, !tbaa !2
  %336 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul421 = mul nsw i32 4, %336
  %add.ptr422 = getelementptr inbounds float, float* %335, i32 %mul421
  %arrayidx423 = getelementptr inbounds [2 x float], [2 x float]* %w31, i32 0, i32 0
  %337 = load float, float* %arrayidx423, align 4, !tbaa !8
  %arrayidx424 = getelementptr inbounds [2 x float], [2 x float]* %w55, i32 0, i32 1
  %338 = load float, float* %arrayidx424, align 4, !tbaa !8
  %call425 = call float @add_float(float %337, float %338)
  call void @store_float(float* %add.ptr422, float %call425)
  %339 = load float*, float** %output.addr, align 4, !tbaa !2
  %340 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul426 = mul nsw i32 5, %340
  %add.ptr427 = getelementptr inbounds float, float* %339, i32 %mul426
  %arrayidx428 = getelementptr inbounds [2 x float], [2 x float]* %w33, i32 0, i32 0
  %341 = load float, float* %arrayidx428, align 4, !tbaa !8
  %arrayidx429 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 0
  %342 = load float, float* %arrayidx429, align 4, !tbaa !8
  %call430 = call float @mul_float(float 0x3FD87DE0E0000000, float %342)
  %arrayidx431 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 1
  %343 = load float, float* %arrayidx431, align 4, !tbaa !8
  %call432 = call float @mul_float(float 0x3FED906CC0000000, float %343)
  %call433 = call float @sub_float(float %call430, float %call432)
  %call434 = call float @sub_float(float %341, float %call433)
  call void @store_float(float* %add.ptr427, float %call434)
  %344 = load float*, float** %output.addr, align 4, !tbaa !2
  %345 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul435 = mul nsw i32 6, %345
  %add.ptr436 = getelementptr inbounds float, float* %344, i32 %mul435
  %arrayidx437 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %346 = load float, float* %arrayidx437, align 4, !tbaa !8
  %arrayidx438 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 0
  %347 = load float, float* %arrayidx438, align 4, !tbaa !8
  %arrayidx439 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 1
  %348 = load float, float* %arrayidx439, align 4, !tbaa !8
  %call440 = call float @sub_float(float %347, float %348)
  %call441 = call float @mul_float(float 0x3FE6A09EE0000000, float %call440)
  %call442 = call float @sub_float(float %346, float %call441)
  call void @store_float(float* %add.ptr436, float %call442)
  %349 = load float*, float** %output.addr, align 4, !tbaa !2
  %350 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul443 = mul nsw i32 7, %350
  %add.ptr444 = getelementptr inbounds float, float* %349, i32 %mul443
  %arrayidx445 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %351 = load float, float* %arrayidx445, align 4, !tbaa !8
  %arrayidx446 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 0
  %352 = load float, float* %arrayidx446, align 4, !tbaa !8
  %call447 = call float @mul_float(float 0x3FED906CC0000000, float %352)
  %arrayidx448 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 1
  %353 = load float, float* %arrayidx448, align 4, !tbaa !8
  %call449 = call float @mul_float(float 0x3FD87DE0E0000000, float %353)
  %call450 = call float @sub_float(float %call447, float %call449)
  %call451 = call float @sub_float(float %351, float %call450)
  call void @store_float(float* %add.ptr444, float %call451)
  %354 = load float*, float** %output.addr, align 4, !tbaa !2
  %355 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul452 = mul nsw i32 8, %355
  %add.ptr453 = getelementptr inbounds float, float* %354, i32 %mul452
  %arrayidx454 = getelementptr inbounds [2 x float], [2 x float]* %w30, i32 0, i32 0
  %356 = load float, float* %arrayidx454, align 4, !tbaa !8
  %arrayidx455 = getelementptr inbounds [2 x float], [2 x float]* %w54, i32 0, i32 0
  %357 = load float, float* %arrayidx455, align 4, !tbaa !8
  %call456 = call float @sub_float(float %356, float %357)
  call void @store_float(float* %add.ptr453, float %call456)
  %358 = load float*, float** %output.addr, align 4, !tbaa !2
  %359 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul457 = mul nsw i32 9, %359
  %add.ptr458 = getelementptr inbounds float, float* %358, i32 %mul457
  %arrayidx459 = getelementptr inbounds [2 x float], [2 x float]* %w32, i32 0, i32 0
  %360 = load float, float* %arrayidx459, align 4, !tbaa !8
  %arrayidx460 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 0
  %361 = load float, float* %arrayidx460, align 4, !tbaa !8
  %call461 = call float @mul_float(float 0x3FED906CC0000000, float %361)
  %call462 = call float @sub_float(float 0.000000e+00, float %call461)
  %arrayidx463 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 1
  %362 = load float, float* %arrayidx463, align 4, !tbaa !8
  %call464 = call float @mul_float(float 0x3FD87DE0E0000000, float %362)
  %call465 = call float @sub_float(float %call462, float %call464)
  %call466 = call float @add_float(float %360, float %call465)
  call void @store_float(float* %add.ptr458, float %call466)
  %363 = load float*, float** %output.addr, align 4, !tbaa !2
  %364 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul467 = mul nsw i32 10, %364
  %add.ptr468 = getelementptr inbounds float, float* %363, i32 %mul467
  %arrayidx469 = getelementptr inbounds [2 x float], [2 x float]* %w34, i32 0, i32 0
  %365 = load float, float* %arrayidx469, align 4, !tbaa !8
  %arrayidx470 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 0
  %366 = load float, float* %arrayidx470, align 4, !tbaa !8
  %call471 = call float @mul_float(float 0x3FE6A09EE0000000, float %366)
  %call472 = call float @sub_float(float 0.000000e+00, float %call471)
  %arrayidx473 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 1
  %367 = load float, float* %arrayidx473, align 4, !tbaa !8
  %call474 = call float @mul_float(float 0x3FE6A09EE0000000, float %367)
  %call475 = call float @sub_float(float %call472, float %call474)
  %call476 = call float @add_float(float %365, float %call475)
  call void @store_float(float* %add.ptr468, float %call476)
  %368 = load float*, float** %output.addr, align 4, !tbaa !2
  %369 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul477 = mul nsw i32 11, %369
  %add.ptr478 = getelementptr inbounds float, float* %368, i32 %mul477
  %arrayidx479 = getelementptr inbounds [2 x float], [2 x float]* %w36, i32 0, i32 0
  %370 = load float, float* %arrayidx479, align 4, !tbaa !8
  %arrayidx480 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 0
  %371 = load float, float* %arrayidx480, align 4, !tbaa !8
  %call481 = call float @mul_float(float 0x3FD87DE0E0000000, float %371)
  %call482 = call float @sub_float(float 0.000000e+00, float %call481)
  %arrayidx483 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 1
  %372 = load float, float* %arrayidx483, align 4, !tbaa !8
  %call484 = call float @mul_float(float 0x3FED906CC0000000, float %372)
  %call485 = call float @sub_float(float %call482, float %call484)
  %call486 = call float @add_float(float %370, float %call485)
  call void @store_float(float* %add.ptr478, float %call486)
  %373 = load float*, float** %output.addr, align 4, !tbaa !2
  %374 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul487 = mul nsw i32 12, %374
  %add.ptr488 = getelementptr inbounds float, float* %373, i32 %mul487
  %arrayidx489 = getelementptr inbounds [2 x float], [2 x float]* %w31, i32 0, i32 0
  %375 = load float, float* %arrayidx489, align 4, !tbaa !8
  %arrayidx490 = getelementptr inbounds [2 x float], [2 x float]* %w55, i32 0, i32 1
  %376 = load float, float* %arrayidx490, align 4, !tbaa !8
  %call491 = call float @sub_float(float %375, float %376)
  call void @store_float(float* %add.ptr488, float %call491)
  %377 = load float*, float** %output.addr, align 4, !tbaa !2
  %378 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul492 = mul nsw i32 13, %378
  %add.ptr493 = getelementptr inbounds float, float* %377, i32 %mul492
  %arrayidx494 = getelementptr inbounds [2 x float], [2 x float]* %w33, i32 0, i32 0
  %379 = load float, float* %arrayidx494, align 4, !tbaa !8
  %arrayidx495 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 0
  %380 = load float, float* %arrayidx495, align 4, !tbaa !8
  %call496 = call float @mul_float(float 0x3FD87DE0E0000000, float %380)
  %arrayidx497 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 1
  %381 = load float, float* %arrayidx497, align 4, !tbaa !8
  %call498 = call float @mul_float(float 0x3FED906CC0000000, float %381)
  %call499 = call float @sub_float(float %call496, float %call498)
  %call500 = call float @add_float(float %379, float %call499)
  call void @store_float(float* %add.ptr493, float %call500)
  %382 = load float*, float** %output.addr, align 4, !tbaa !2
  %383 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul501 = mul nsw i32 14, %383
  %add.ptr502 = getelementptr inbounds float, float* %382, i32 %mul501
  %arrayidx503 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %384 = load float, float* %arrayidx503, align 4, !tbaa !8
  %arrayidx504 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 0
  %385 = load float, float* %arrayidx504, align 4, !tbaa !8
  %arrayidx505 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 1
  %386 = load float, float* %arrayidx505, align 4, !tbaa !8
  %call506 = call float @sub_float(float %385, float %386)
  %call507 = call float @mul_float(float 0x3FE6A09EE0000000, float %call506)
  %call508 = call float @add_float(float %384, float %call507)
  call void @store_float(float* %add.ptr502, float %call508)
  %387 = load float*, float** %output.addr, align 4, !tbaa !2
  %388 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul509 = mul nsw i32 15, %388
  %add.ptr510 = getelementptr inbounds float, float* %387, i32 %mul509
  %arrayidx511 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %389 = load float, float* %arrayidx511, align 4, !tbaa !8
  %arrayidx512 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 0
  %390 = load float, float* %arrayidx512, align 4, !tbaa !8
  %call513 = call float @mul_float(float 0x3FED906CC0000000, float %390)
  %arrayidx514 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 1
  %391 = load float, float* %arrayidx514, align 4, !tbaa !8
  %call515 = call float @mul_float(float 0x3FD87DE0E0000000, float %391)
  %call516 = call float @sub_float(float %call513, float %call515)
  %call517 = call float @add_float(float %389, float %call516)
  call void @store_float(float* %add.ptr510, float %call517)
  %392 = bitcast [2 x float]* %w61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %392) #3
  %393 = bitcast [2 x float]* %w60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %393) #3
  %394 = bitcast [2 x float]* %w59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %394) #3
  %395 = bitcast [2 x float]* %w58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %395) #3
  %396 = bitcast [2 x float]* %w57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %396) #3
  %397 = bitcast [2 x float]* %w56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %397) #3
  %398 = bitcast [2 x float]* %w55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %398) #3
  %399 = bitcast [2 x float]* %w54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %399) #3
  %400 = bitcast [2 x float]* %w53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %400) #3
  %401 = bitcast [2 x float]* %w52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %401) #3
  %402 = bitcast [2 x float]* %w51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %402) #3
  %403 = bitcast [2 x float]* %w50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %403) #3
  %404 = bitcast [2 x float]* %w49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %404) #3
  %405 = bitcast [2 x float]* %w48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %405) #3
  %406 = bitcast [2 x float]* %w47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %406) #3
  %407 = bitcast [2 x float]* %w46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %407) #3
  %408 = bitcast [2 x float]* %w45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %408) #3
  %409 = bitcast [2 x float]* %w44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %409) #3
  %410 = bitcast [2 x float]* %w43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %410) #3
  %411 = bitcast [2 x float]* %w42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %411) #3
  %412 = bitcast [2 x float]* %w41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %412) #3
  %413 = bitcast [2 x float]* %w40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %413) #3
  %414 = bitcast [2 x float]* %w39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %414) #3
  %415 = bitcast [2 x float]* %w38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %415) #3
  %416 = bitcast [2 x float]* %w37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %416) #3
  %417 = bitcast [2 x float]* %w36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %417) #3
  %418 = bitcast [2 x float]* %w35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %418) #3
  %419 = bitcast [2 x float]* %w34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %419) #3
  %420 = bitcast [2 x float]* %w33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %420) #3
  %421 = bitcast [2 x float]* %w32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %421) #3
  %422 = bitcast [2 x float]* %w31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %422) #3
  %423 = bitcast [2 x float]* %w30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %423) #3
  %424 = bitcast [2 x float]* %w29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %424) #3
  %425 = bitcast [2 x float]* %w28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %425) #3
  %426 = bitcast [2 x float]* %w27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %426) #3
  %427 = bitcast [2 x float]* %w26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %427) #3
  %428 = bitcast [2 x float]* %w25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %428) #3
  %429 = bitcast [2 x float]* %w24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %429) #3
  %430 = bitcast [2 x float]* %w23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %430) #3
  %431 = bitcast [2 x float]* %w22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %431) #3
  %432 = bitcast [2 x float]* %w21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %432) #3
  %433 = bitcast [2 x float]* %w20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %433) #3
  %434 = bitcast [2 x float]* %w19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %434) #3
  %435 = bitcast [2 x float]* %w18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %435) #3
  %436 = bitcast [2 x float]* %w17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %436) #3
  %437 = bitcast [2 x float]* %w16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %437) #3
  %438 = bitcast float* %w15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %438) #3
  %439 = bitcast float* %w14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %439) #3
  %440 = bitcast float* %i15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %440) #3
  %441 = bitcast float* %i14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %441) #3
  %442 = bitcast float* %i13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %442) #3
  %443 = bitcast float* %i12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %443) #3
  %444 = bitcast float* %i11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %444) #3
  %445 = bitcast float* %i10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %445) #3
  %446 = bitcast float* %i9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %446) #3
  %447 = bitcast float* %i8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %447) #3
  %448 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %448) #3
  %449 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %449) #3
  %450 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %450) #3
  %451 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %451) #3
  %452 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %452) #3
  %453 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %453) #3
  %454 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %454) #3
  %455 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %455) #3
  %456 = bitcast float* %kWeight4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %456) #3
  %457 = bitcast float* %kWeight3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %457) #3
  %458 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %458) #3
  %459 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %459) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft1d_32_float(float* %input, float* %output, i32 %stride) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %kWeight0 = alloca float, align 4
  %kWeight2 = alloca float, align 4
  %kWeight3 = alloca float, align 4
  %kWeight4 = alloca float, align 4
  %kWeight5 = alloca float, align 4
  %kWeight6 = alloca float, align 4
  %kWeight7 = alloca float, align 4
  %kWeight8 = alloca float, align 4
  %i0 = alloca float, align 4
  %i1 = alloca float, align 4
  %i2 = alloca float, align 4
  %i3 = alloca float, align 4
  %i4 = alloca float, align 4
  %i5 = alloca float, align 4
  %i6 = alloca float, align 4
  %i7 = alloca float, align 4
  %i8 = alloca float, align 4
  %i9 = alloca float, align 4
  %i10 = alloca float, align 4
  %i11 = alloca float, align 4
  %i12 = alloca float, align 4
  %i13 = alloca float, align 4
  %i14 = alloca float, align 4
  %i15 = alloca float, align 4
  %i16 = alloca float, align 4
  %i17 = alloca float, align 4
  %i18 = alloca float, align 4
  %i19 = alloca float, align 4
  %i20 = alloca float, align 4
  %i21 = alloca float, align 4
  %i22 = alloca float, align 4
  %i23 = alloca float, align 4
  %i24 = alloca float, align 4
  %i25 = alloca float, align 4
  %i26 = alloca float, align 4
  %i27 = alloca float, align 4
  %i28 = alloca float, align 4
  %i29 = alloca float, align 4
  %i30 = alloca float, align 4
  %i31 = alloca float, align 4
  %w30 = alloca float, align 4
  %w31 = alloca float, align 4
  %w32 = alloca [2 x float], align 4
  %w33 = alloca [2 x float], align 4
  %w34 = alloca [2 x float], align 4
  %w35 = alloca [2 x float], align 4
  %w36 = alloca [2 x float], align 4
  %w37 = alloca [2 x float], align 4
  %w38 = alloca [2 x float], align 4
  %w39 = alloca [2 x float], align 4
  %w40 = alloca [2 x float], align 4
  %w41 = alloca [2 x float], align 4
  %w42 = alloca [2 x float], align 4
  %w43 = alloca [2 x float], align 4
  %w44 = alloca [2 x float], align 4
  %w45 = alloca [2 x float], align 4
  %w46 = alloca [2 x float], align 4
  %w47 = alloca [2 x float], align 4
  %w48 = alloca [2 x float], align 4
  %w49 = alloca [2 x float], align 4
  %w50 = alloca [2 x float], align 4
  %w51 = alloca [2 x float], align 4
  %w52 = alloca [2 x float], align 4
  %w53 = alloca [2 x float], align 4
  %w54 = alloca [2 x float], align 4
  %w55 = alloca [2 x float], align 4
  %w56 = alloca [2 x float], align 4
  %w57 = alloca [2 x float], align 4
  %w58 = alloca [2 x float], align 4
  %w59 = alloca [2 x float], align 4
  %w60 = alloca [2 x float], align 4
  %w61 = alloca [2 x float], align 4
  %w62 = alloca [2 x float], align 4
  %w63 = alloca [2 x float], align 4
  %w64 = alloca [2 x float], align 4
  %w65 = alloca [2 x float], align 4
  %w66 = alloca [2 x float], align 4
  %w67 = alloca [2 x float], align 4
  %w68 = alloca [2 x float], align 4
  %w69 = alloca [2 x float], align 4
  %w70 = alloca [2 x float], align 4
  %w71 = alloca [2 x float], align 4
  %w72 = alloca [2 x float], align 4
  %w73 = alloca [2 x float], align 4
  %w74 = alloca [2 x float], align 4
  %w75 = alloca [2 x float], align 4
  %w76 = alloca [2 x float], align 4
  %w77 = alloca [2 x float], align 4
  %w78 = alloca [2 x float], align 4
  %w79 = alloca [2 x float], align 4
  %w80 = alloca [2 x float], align 4
  %w81 = alloca [2 x float], align 4
  %w82 = alloca [2 x float], align 4
  %w83 = alloca [2 x float], align 4
  %w84 = alloca [2 x float], align 4
  %w85 = alloca [2 x float], align 4
  %w86 = alloca [2 x float], align 4
  %w87 = alloca [2 x float], align 4
  %w88 = alloca [2 x float], align 4
  %w89 = alloca [2 x float], align 4
  %w90 = alloca [2 x float], align 4
  %w91 = alloca [2 x float], align 4
  %w92 = alloca [2 x float], align 4
  %w93 = alloca [2 x float], align 4
  %w94 = alloca [2 x float], align 4
  %w95 = alloca [2 x float], align 4
  %w96 = alloca [2 x float], align 4
  %w97 = alloca [2 x float], align 4
  %w98 = alloca [2 x float], align 4
  %w99 = alloca [2 x float], align 4
  %w100 = alloca [2 x float], align 4
  %w101 = alloca [2 x float], align 4
  %w102 = alloca [2 x float], align 4
  %w103 = alloca [2 x float], align 4
  %w104 = alloca [2 x float], align 4
  %w105 = alloca [2 x float], align 4
  %w106 = alloca [2 x float], align 4
  %w107 = alloca [2 x float], align 4
  %w108 = alloca [2 x float], align 4
  %w109 = alloca [2 x float], align 4
  %w110 = alloca [2 x float], align 4
  %w111 = alloca [2 x float], align 4
  %w112 = alloca [2 x float], align 4
  %w113 = alloca [2 x float], align 4
  %w114 = alloca [2 x float], align 4
  %w115 = alloca [2 x float], align 4
  %w116 = alloca [2 x float], align 4
  %w117 = alloca [2 x float], align 4
  %w118 = alloca [2 x float], align 4
  %w119 = alloca [2 x float], align 4
  %w120 = alloca [2 x float], align 4
  %w121 = alloca [2 x float], align 4
  %w122 = alloca [2 x float], align 4
  %w123 = alloca [2 x float], align 4
  %w124 = alloca [2 x float], align 4
  %w125 = alloca [2 x float], align 4
  %w126 = alloca [2 x float], align 4
  %w127 = alloca [2 x float], align 4
  %w128 = alloca [2 x float], align 4
  %w129 = alloca [2 x float], align 4
  %w130 = alloca [2 x float], align 4
  %w131 = alloca [2 x float], align 4
  %w132 = alloca [2 x float], align 4
  %w133 = alloca [2 x float], align 4
  %w134 = alloca [2 x float], align 4
  %w135 = alloca [2 x float], align 4
  %w136 = alloca [2 x float], align 4
  %w137 = alloca [2 x float], align 4
  %w138 = alloca [2 x float], align 4
  %w139 = alloca [2 x float], align 4
  %w140 = alloca [2 x float], align 4
  %w141 = alloca [2 x float], align 4
  %w142 = alloca [2 x float], align 4
  %w143 = alloca [2 x float], align 4
  %w144 = alloca [2 x float], align 4
  %w145 = alloca [2 x float], align 4
  %w146 = alloca [2 x float], align 4
  %w147 = alloca [2 x float], align 4
  %w148 = alloca [2 x float], align 4
  %w149 = alloca [2 x float], align 4
  %w150 = alloca [2 x float], align 4
  %w151 = alloca [2 x float], align 4
  %w152 = alloca [2 x float], align 4
  %w153 = alloca [2 x float], align 4
  %w154 = alloca [2 x float], align 4
  %w155 = alloca [2 x float], align 4
  %w156 = alloca [2 x float], align 4
  %w157 = alloca [2 x float], align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  %0 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store float 0.000000e+00, float* %kWeight0, align 4, !tbaa !8
  %1 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store float 0x3FE6A09EE0000000, float* %kWeight2, align 4, !tbaa !8
  %2 = bitcast float* %kWeight3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store float 0x3FED906CC0000000, float* %kWeight3, align 4, !tbaa !8
  %3 = bitcast float* %kWeight4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store float 0x3FD87DE0E0000000, float* %kWeight4, align 4, !tbaa !8
  %4 = bitcast float* %kWeight5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  store float 0x3FEF629740000000, float* %kWeight5, align 4, !tbaa !8
  %5 = bitcast float* %kWeight6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  store float 0x3FC8F8B580000000, float* %kWeight6, align 4, !tbaa !8
  %6 = bitcast float* %kWeight7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  store float 0x3FEA9B6700000000, float* %kWeight7, align 4, !tbaa !8
  %7 = bitcast float* %kWeight8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  store float 0x3FE1C73AC0000000, float* %kWeight8, align 4, !tbaa !8
  %8 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load float*, float** %input.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %10
  %add.ptr = getelementptr inbounds float, float* %9, i32 %mul
  %11 = load float, float* %add.ptr, align 4, !tbaa !8
  store float %11, float* %i0, align 4, !tbaa !8
  %12 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load float*, float** %input.addr, align 4, !tbaa !2
  %14 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %14
  %add.ptr2 = getelementptr inbounds float, float* %13, i32 %mul1
  %15 = load float, float* %add.ptr2, align 4, !tbaa !8
  store float %15, float* %i1, align 4, !tbaa !8
  %16 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load float*, float** %input.addr, align 4, !tbaa !2
  %18 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 2, %18
  %add.ptr4 = getelementptr inbounds float, float* %17, i32 %mul3
  %19 = load float, float* %add.ptr4, align 4, !tbaa !8
  store float %19, float* %i2, align 4, !tbaa !8
  %20 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load float*, float** %input.addr, align 4, !tbaa !2
  %22 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %22
  %add.ptr6 = getelementptr inbounds float, float* %21, i32 %mul5
  %23 = load float, float* %add.ptr6, align 4, !tbaa !8
  store float %23, float* %i3, align 4, !tbaa !8
  %24 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load float*, float** %input.addr, align 4, !tbaa !2
  %26 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 4, %26
  %add.ptr8 = getelementptr inbounds float, float* %25, i32 %mul7
  %27 = load float, float* %add.ptr8, align 4, !tbaa !8
  store float %27, float* %i4, align 4, !tbaa !8
  %28 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load float*, float** %input.addr, align 4, !tbaa !2
  %30 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul9 = mul nsw i32 5, %30
  %add.ptr10 = getelementptr inbounds float, float* %29, i32 %mul9
  %31 = load float, float* %add.ptr10, align 4, !tbaa !8
  store float %31, float* %i5, align 4, !tbaa !8
  %32 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load float*, float** %input.addr, align 4, !tbaa !2
  %34 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 6, %34
  %add.ptr12 = getelementptr inbounds float, float* %33, i32 %mul11
  %35 = load float, float* %add.ptr12, align 4, !tbaa !8
  store float %35, float* %i6, align 4, !tbaa !8
  %36 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #3
  %37 = load float*, float** %input.addr, align 4, !tbaa !2
  %38 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 7, %38
  %add.ptr14 = getelementptr inbounds float, float* %37, i32 %mul13
  %39 = load float, float* %add.ptr14, align 4, !tbaa !8
  store float %39, float* %i7, align 4, !tbaa !8
  %40 = bitcast float* %i8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load float*, float** %input.addr, align 4, !tbaa !2
  %42 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 8, %42
  %add.ptr16 = getelementptr inbounds float, float* %41, i32 %mul15
  %43 = load float, float* %add.ptr16, align 4, !tbaa !8
  store float %43, float* %i8, align 4, !tbaa !8
  %44 = bitcast float* %i9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load float*, float** %input.addr, align 4, !tbaa !2
  %46 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 9, %46
  %add.ptr18 = getelementptr inbounds float, float* %45, i32 %mul17
  %47 = load float, float* %add.ptr18, align 4, !tbaa !8
  store float %47, float* %i9, align 4, !tbaa !8
  %48 = bitcast float* %i10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #3
  %49 = load float*, float** %input.addr, align 4, !tbaa !2
  %50 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul19 = mul nsw i32 10, %50
  %add.ptr20 = getelementptr inbounds float, float* %49, i32 %mul19
  %51 = load float, float* %add.ptr20, align 4, !tbaa !8
  store float %51, float* %i10, align 4, !tbaa !8
  %52 = bitcast float* %i11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  %53 = load float*, float** %input.addr, align 4, !tbaa !2
  %54 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul21 = mul nsw i32 11, %54
  %add.ptr22 = getelementptr inbounds float, float* %53, i32 %mul21
  %55 = load float, float* %add.ptr22, align 4, !tbaa !8
  store float %55, float* %i11, align 4, !tbaa !8
  %56 = bitcast float* %i12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #3
  %57 = load float*, float** %input.addr, align 4, !tbaa !2
  %58 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 12, %58
  %add.ptr24 = getelementptr inbounds float, float* %57, i32 %mul23
  %59 = load float, float* %add.ptr24, align 4, !tbaa !8
  store float %59, float* %i12, align 4, !tbaa !8
  %60 = bitcast float* %i13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #3
  %61 = load float*, float** %input.addr, align 4, !tbaa !2
  %62 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 13, %62
  %add.ptr26 = getelementptr inbounds float, float* %61, i32 %mul25
  %63 = load float, float* %add.ptr26, align 4, !tbaa !8
  store float %63, float* %i13, align 4, !tbaa !8
  %64 = bitcast float* %i14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  %65 = load float*, float** %input.addr, align 4, !tbaa !2
  %66 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul27 = mul nsw i32 14, %66
  %add.ptr28 = getelementptr inbounds float, float* %65, i32 %mul27
  %67 = load float, float* %add.ptr28, align 4, !tbaa !8
  store float %67, float* %i14, align 4, !tbaa !8
  %68 = bitcast float* %i15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #3
  %69 = load float*, float** %input.addr, align 4, !tbaa !2
  %70 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 15, %70
  %add.ptr30 = getelementptr inbounds float, float* %69, i32 %mul29
  %71 = load float, float* %add.ptr30, align 4, !tbaa !8
  store float %71, float* %i15, align 4, !tbaa !8
  %72 = bitcast float* %i16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #3
  %73 = load float*, float** %input.addr, align 4, !tbaa !2
  %74 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul31 = mul nsw i32 16, %74
  %add.ptr32 = getelementptr inbounds float, float* %73, i32 %mul31
  %75 = load float, float* %add.ptr32, align 4, !tbaa !8
  store float %75, float* %i16, align 4, !tbaa !8
  %76 = bitcast float* %i17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #3
  %77 = load float*, float** %input.addr, align 4, !tbaa !2
  %78 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul33 = mul nsw i32 17, %78
  %add.ptr34 = getelementptr inbounds float, float* %77, i32 %mul33
  %79 = load float, float* %add.ptr34, align 4, !tbaa !8
  store float %79, float* %i17, align 4, !tbaa !8
  %80 = bitcast float* %i18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #3
  %81 = load float*, float** %input.addr, align 4, !tbaa !2
  %82 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul35 = mul nsw i32 18, %82
  %add.ptr36 = getelementptr inbounds float, float* %81, i32 %mul35
  %83 = load float, float* %add.ptr36, align 4, !tbaa !8
  store float %83, float* %i18, align 4, !tbaa !8
  %84 = bitcast float* %i19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #3
  %85 = load float*, float** %input.addr, align 4, !tbaa !2
  %86 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul37 = mul nsw i32 19, %86
  %add.ptr38 = getelementptr inbounds float, float* %85, i32 %mul37
  %87 = load float, float* %add.ptr38, align 4, !tbaa !8
  store float %87, float* %i19, align 4, !tbaa !8
  %88 = bitcast float* %i20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #3
  %89 = load float*, float** %input.addr, align 4, !tbaa !2
  %90 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul39 = mul nsw i32 20, %90
  %add.ptr40 = getelementptr inbounds float, float* %89, i32 %mul39
  %91 = load float, float* %add.ptr40, align 4, !tbaa !8
  store float %91, float* %i20, align 4, !tbaa !8
  %92 = bitcast float* %i21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #3
  %93 = load float*, float** %input.addr, align 4, !tbaa !2
  %94 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul41 = mul nsw i32 21, %94
  %add.ptr42 = getelementptr inbounds float, float* %93, i32 %mul41
  %95 = load float, float* %add.ptr42, align 4, !tbaa !8
  store float %95, float* %i21, align 4, !tbaa !8
  %96 = bitcast float* %i22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #3
  %97 = load float*, float** %input.addr, align 4, !tbaa !2
  %98 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul43 = mul nsw i32 22, %98
  %add.ptr44 = getelementptr inbounds float, float* %97, i32 %mul43
  %99 = load float, float* %add.ptr44, align 4, !tbaa !8
  store float %99, float* %i22, align 4, !tbaa !8
  %100 = bitcast float* %i23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #3
  %101 = load float*, float** %input.addr, align 4, !tbaa !2
  %102 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul45 = mul nsw i32 23, %102
  %add.ptr46 = getelementptr inbounds float, float* %101, i32 %mul45
  %103 = load float, float* %add.ptr46, align 4, !tbaa !8
  store float %103, float* %i23, align 4, !tbaa !8
  %104 = bitcast float* %i24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #3
  %105 = load float*, float** %input.addr, align 4, !tbaa !2
  %106 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul47 = mul nsw i32 24, %106
  %add.ptr48 = getelementptr inbounds float, float* %105, i32 %mul47
  %107 = load float, float* %add.ptr48, align 4, !tbaa !8
  store float %107, float* %i24, align 4, !tbaa !8
  %108 = bitcast float* %i25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #3
  %109 = load float*, float** %input.addr, align 4, !tbaa !2
  %110 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul49 = mul nsw i32 25, %110
  %add.ptr50 = getelementptr inbounds float, float* %109, i32 %mul49
  %111 = load float, float* %add.ptr50, align 4, !tbaa !8
  store float %111, float* %i25, align 4, !tbaa !8
  %112 = bitcast float* %i26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #3
  %113 = load float*, float** %input.addr, align 4, !tbaa !2
  %114 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul51 = mul nsw i32 26, %114
  %add.ptr52 = getelementptr inbounds float, float* %113, i32 %mul51
  %115 = load float, float* %add.ptr52, align 4, !tbaa !8
  store float %115, float* %i26, align 4, !tbaa !8
  %116 = bitcast float* %i27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #3
  %117 = load float*, float** %input.addr, align 4, !tbaa !2
  %118 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul53 = mul nsw i32 27, %118
  %add.ptr54 = getelementptr inbounds float, float* %117, i32 %mul53
  %119 = load float, float* %add.ptr54, align 4, !tbaa !8
  store float %119, float* %i27, align 4, !tbaa !8
  %120 = bitcast float* %i28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #3
  %121 = load float*, float** %input.addr, align 4, !tbaa !2
  %122 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul55 = mul nsw i32 28, %122
  %add.ptr56 = getelementptr inbounds float, float* %121, i32 %mul55
  %123 = load float, float* %add.ptr56, align 4, !tbaa !8
  store float %123, float* %i28, align 4, !tbaa !8
  %124 = bitcast float* %i29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #3
  %125 = load float*, float** %input.addr, align 4, !tbaa !2
  %126 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul57 = mul nsw i32 29, %126
  %add.ptr58 = getelementptr inbounds float, float* %125, i32 %mul57
  %127 = load float, float* %add.ptr58, align 4, !tbaa !8
  store float %127, float* %i29, align 4, !tbaa !8
  %128 = bitcast float* %i30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #3
  %129 = load float*, float** %input.addr, align 4, !tbaa !2
  %130 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul59 = mul nsw i32 30, %130
  %add.ptr60 = getelementptr inbounds float, float* %129, i32 %mul59
  %131 = load float, float* %add.ptr60, align 4, !tbaa !8
  store float %131, float* %i30, align 4, !tbaa !8
  %132 = bitcast float* %i31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #3
  %133 = load float*, float** %input.addr, align 4, !tbaa !2
  %134 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul61 = mul nsw i32 31, %134
  %add.ptr62 = getelementptr inbounds float, float* %133, i32 %mul61
  %135 = load float, float* %add.ptr62, align 4, !tbaa !8
  store float %135, float* %i31, align 4, !tbaa !8
  %136 = bitcast float* %w30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #3
  %137 = load float, float* %i0, align 4, !tbaa !8
  %138 = load float, float* %i16, align 4, !tbaa !8
  %call = call float @add_float(float %137, float %138)
  store float %call, float* %w30, align 4, !tbaa !8
  %139 = bitcast float* %w31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #3
  %140 = load float, float* %i0, align 4, !tbaa !8
  %141 = load float, float* %i16, align 4, !tbaa !8
  %call63 = call float @sub_float(float %140, float %141)
  store float %call63, float* %w31, align 4, !tbaa !8
  %142 = bitcast [2 x float]* %w32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %142) #3
  %arrayinit.begin = getelementptr inbounds [2 x float], [2 x float]* %w32, i32 0, i32 0
  %143 = load float, float* %i8, align 4, !tbaa !8
  %144 = load float, float* %i8, align 4, !tbaa !8
  %call64 = call float @add_float(float %143, float %144)
  store float %call64, float* %arrayinit.begin, align 4, !tbaa !8
  %arrayinit.element = getelementptr inbounds float, float* %arrayinit.begin, i32 1
  %145 = load float, float* %i24, align 4, !tbaa !8
  %146 = load float, float* %i24, align 4, !tbaa !8
  %call65 = call float @sub_float(float %145, float %146)
  store float %call65, float* %arrayinit.element, align 4, !tbaa !8
  %147 = bitcast [2 x float]* %w33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %147) #3
  %arrayinit.begin66 = getelementptr inbounds [2 x float], [2 x float]* %w33, i32 0, i32 0
  %148 = load float, float* %i8, align 4, !tbaa !8
  %149 = load float, float* %i8, align 4, !tbaa !8
  %call67 = call float @sub_float(float %148, float %149)
  store float %call67, float* %arrayinit.begin66, align 4, !tbaa !8
  %arrayinit.element68 = getelementptr inbounds float, float* %arrayinit.begin66, i32 1
  %150 = load float, float* %i24, align 4, !tbaa !8
  %call69 = call float @sub_float(float 0.000000e+00, float %150)
  %151 = load float, float* %i24, align 4, !tbaa !8
  %call70 = call float @sub_float(float %call69, float %151)
  store float %call70, float* %arrayinit.element68, align 4, !tbaa !8
  %152 = bitcast [2 x float]* %w34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %152) #3
  %arrayinit.begin71 = getelementptr inbounds [2 x float], [2 x float]* %w34, i32 0, i32 0
  %153 = load float, float* %w30, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [2 x float], [2 x float]* %w32, i32 0, i32 0
  %154 = load float, float* %arrayidx, align 4, !tbaa !8
  %call72 = call float @add_float(float %153, float %154)
  store float %call72, float* %arrayinit.begin71, align 4, !tbaa !8
  %arrayinit.element73 = getelementptr inbounds float, float* %arrayinit.begin71, i32 1
  %arrayidx74 = getelementptr inbounds [2 x float], [2 x float]* %w32, i32 0, i32 1
  %155 = load float, float* %arrayidx74, align 4, !tbaa !8
  store float %155, float* %arrayinit.element73, align 4, !tbaa !8
  %156 = bitcast [2 x float]* %w35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %156) #3
  %arrayinit.begin75 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %157 = load float, float* %w30, align 4, !tbaa !8
  %arrayidx76 = getelementptr inbounds [2 x float], [2 x float]* %w32, i32 0, i32 0
  %158 = load float, float* %arrayidx76, align 4, !tbaa !8
  %call77 = call float @sub_float(float %157, float %158)
  store float %call77, float* %arrayinit.begin75, align 4, !tbaa !8
  %arrayinit.element78 = getelementptr inbounds float, float* %arrayinit.begin75, i32 1
  %arrayidx79 = getelementptr inbounds [2 x float], [2 x float]* %w32, i32 0, i32 1
  %159 = load float, float* %arrayidx79, align 4, !tbaa !8
  %call80 = call float @sub_float(float 0.000000e+00, float %159)
  store float %call80, float* %arrayinit.element78, align 4, !tbaa !8
  %160 = bitcast [2 x float]* %w36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %160) #3
  %arrayinit.begin81 = getelementptr inbounds [2 x float], [2 x float]* %w36, i32 0, i32 0
  %161 = load float, float* %w31, align 4, !tbaa !8
  %arrayidx82 = getelementptr inbounds [2 x float], [2 x float]* %w33, i32 0, i32 1
  %162 = load float, float* %arrayidx82, align 4, !tbaa !8
  %call83 = call float @add_float(float %161, float %162)
  store float %call83, float* %arrayinit.begin81, align 4, !tbaa !8
  %arrayinit.element84 = getelementptr inbounds float, float* %arrayinit.begin81, i32 1
  %arrayidx85 = getelementptr inbounds [2 x float], [2 x float]* %w33, i32 0, i32 0
  %163 = load float, float* %arrayidx85, align 4, !tbaa !8
  %call86 = call float @sub_float(float 0.000000e+00, float %163)
  store float %call86, float* %arrayinit.element84, align 4, !tbaa !8
  %164 = bitcast [2 x float]* %w37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %164) #3
  %arrayinit.begin87 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %165 = load float, float* %w31, align 4, !tbaa !8
  %arrayidx88 = getelementptr inbounds [2 x float], [2 x float]* %w33, i32 0, i32 1
  %166 = load float, float* %arrayidx88, align 4, !tbaa !8
  %call89 = call float @sub_float(float %165, float %166)
  store float %call89, float* %arrayinit.begin87, align 4, !tbaa !8
  %arrayinit.element90 = getelementptr inbounds float, float* %arrayinit.begin87, i32 1
  %arrayidx91 = getelementptr inbounds [2 x float], [2 x float]* %w33, i32 0, i32 0
  %167 = load float, float* %arrayidx91, align 4, !tbaa !8
  store float %167, float* %arrayinit.element90, align 4, !tbaa !8
  %168 = bitcast [2 x float]* %w38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %168) #3
  %arrayinit.begin92 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 0
  %169 = load float, float* %i4, align 4, !tbaa !8
  %170 = load float, float* %i12, align 4, !tbaa !8
  %call93 = call float @add_float(float %169, float %170)
  store float %call93, float* %arrayinit.begin92, align 4, !tbaa !8
  %arrayinit.element94 = getelementptr inbounds float, float* %arrayinit.begin92, i32 1
  %171 = load float, float* %i28, align 4, !tbaa !8
  %172 = load float, float* %i20, align 4, !tbaa !8
  %call95 = call float @sub_float(float %171, float %172)
  store float %call95, float* %arrayinit.element94, align 4, !tbaa !8
  %173 = bitcast [2 x float]* %w39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %173) #3
  %arrayinit.begin96 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 0
  %174 = load float, float* %i4, align 4, !tbaa !8
  %175 = load float, float* %i12, align 4, !tbaa !8
  %call97 = call float @sub_float(float %174, float %175)
  store float %call97, float* %arrayinit.begin96, align 4, !tbaa !8
  %arrayinit.element98 = getelementptr inbounds float, float* %arrayinit.begin96, i32 1
  %176 = load float, float* %i20, align 4, !tbaa !8
  %call99 = call float @sub_float(float 0.000000e+00, float %176)
  %177 = load float, float* %i28, align 4, !tbaa !8
  %call100 = call float @sub_float(float %call99, float %177)
  store float %call100, float* %arrayinit.element98, align 4, !tbaa !8
  %178 = bitcast [2 x float]* %w40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %178) #3
  %arrayinit.begin101 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 0
  %179 = load float, float* %i12, align 4, !tbaa !8
  %180 = load float, float* %i4, align 4, !tbaa !8
  %call102 = call float @add_float(float %179, float %180)
  store float %call102, float* %arrayinit.begin101, align 4, !tbaa !8
  %arrayinit.element103 = getelementptr inbounds float, float* %arrayinit.begin101, i32 1
  %181 = load float, float* %i20, align 4, !tbaa !8
  %182 = load float, float* %i28, align 4, !tbaa !8
  %call104 = call float @sub_float(float %181, float %182)
  store float %call104, float* %arrayinit.element103, align 4, !tbaa !8
  %183 = bitcast [2 x float]* %w41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %183) #3
  %arrayinit.begin105 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 0
  %184 = load float, float* %i12, align 4, !tbaa !8
  %185 = load float, float* %i4, align 4, !tbaa !8
  %call106 = call float @sub_float(float %184, float %185)
  store float %call106, float* %arrayinit.begin105, align 4, !tbaa !8
  %arrayinit.element107 = getelementptr inbounds float, float* %arrayinit.begin105, i32 1
  %186 = load float, float* %i28, align 4, !tbaa !8
  %call108 = call float @sub_float(float 0.000000e+00, float %186)
  %187 = load float, float* %i20, align 4, !tbaa !8
  %call109 = call float @sub_float(float %call108, float %187)
  store float %call109, float* %arrayinit.element107, align 4, !tbaa !8
  %188 = bitcast [2 x float]* %w42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %188) #3
  %arrayinit.begin110 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 0
  %arrayidx111 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 0
  %189 = load float, float* %arrayidx111, align 4, !tbaa !8
  %arrayidx112 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 0
  %190 = load float, float* %arrayidx112, align 4, !tbaa !8
  %call113 = call float @add_float(float %189, float %190)
  store float %call113, float* %arrayinit.begin110, align 4, !tbaa !8
  %arrayinit.element114 = getelementptr inbounds float, float* %arrayinit.begin110, i32 1
  %arrayidx115 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 1
  %191 = load float, float* %arrayidx115, align 4, !tbaa !8
  %arrayidx116 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 1
  %192 = load float, float* %arrayidx116, align 4, !tbaa !8
  %call117 = call float @add_float(float %191, float %192)
  store float %call117, float* %arrayinit.element114, align 4, !tbaa !8
  %193 = bitcast [2 x float]* %w43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %193) #3
  %arrayinit.begin118 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 0
  %arrayidx119 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 0
  %194 = load float, float* %arrayidx119, align 4, !tbaa !8
  %arrayidx120 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 0
  %195 = load float, float* %arrayidx120, align 4, !tbaa !8
  %call121 = call float @sub_float(float %194, float %195)
  store float %call121, float* %arrayinit.begin118, align 4, !tbaa !8
  %arrayinit.element122 = getelementptr inbounds float, float* %arrayinit.begin118, i32 1
  %arrayidx123 = getelementptr inbounds [2 x float], [2 x float]* %w38, i32 0, i32 1
  %196 = load float, float* %arrayidx123, align 4, !tbaa !8
  %arrayidx124 = getelementptr inbounds [2 x float], [2 x float]* %w40, i32 0, i32 1
  %197 = load float, float* %arrayidx124, align 4, !tbaa !8
  %call125 = call float @sub_float(float %196, float %197)
  store float %call125, float* %arrayinit.element122, align 4, !tbaa !8
  %198 = bitcast [2 x float]* %w44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %198) #3
  %arrayinit.begin126 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %arrayidx127 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 0
  %199 = load float, float* %arrayidx127, align 4, !tbaa !8
  %arrayidx128 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 1
  %200 = load float, float* %arrayidx128, align 4, !tbaa !8
  %call129 = call float @add_float(float %199, float %200)
  store float %call129, float* %arrayinit.begin126, align 4, !tbaa !8
  %arrayinit.element130 = getelementptr inbounds float, float* %arrayinit.begin126, i32 1
  %arrayidx131 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 1
  %201 = load float, float* %arrayidx131, align 4, !tbaa !8
  %arrayidx132 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 0
  %202 = load float, float* %arrayidx132, align 4, !tbaa !8
  %call133 = call float @sub_float(float %201, float %202)
  store float %call133, float* %arrayinit.element130, align 4, !tbaa !8
  %203 = bitcast [2 x float]* %w45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %203) #3
  %arrayinit.begin134 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %arrayidx135 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 0
  %204 = load float, float* %arrayidx135, align 4, !tbaa !8
  %arrayidx136 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 1
  %205 = load float, float* %arrayidx136, align 4, !tbaa !8
  %call137 = call float @sub_float(float %204, float %205)
  store float %call137, float* %arrayinit.begin134, align 4, !tbaa !8
  %arrayinit.element138 = getelementptr inbounds float, float* %arrayinit.begin134, i32 1
  %arrayidx139 = getelementptr inbounds [2 x float], [2 x float]* %w39, i32 0, i32 1
  %206 = load float, float* %arrayidx139, align 4, !tbaa !8
  %arrayidx140 = getelementptr inbounds [2 x float], [2 x float]* %w41, i32 0, i32 0
  %207 = load float, float* %arrayidx140, align 4, !tbaa !8
  %call141 = call float @add_float(float %206, float %207)
  store float %call141, float* %arrayinit.element138, align 4, !tbaa !8
  %208 = bitcast [2 x float]* %w46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %208) #3
  %arrayinit.begin142 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 0
  %arrayidx143 = getelementptr inbounds [2 x float], [2 x float]* %w34, i32 0, i32 0
  %209 = load float, float* %arrayidx143, align 4, !tbaa !8
  %arrayidx144 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 0
  %210 = load float, float* %arrayidx144, align 4, !tbaa !8
  %call145 = call float @add_float(float %209, float %210)
  store float %call145, float* %arrayinit.begin142, align 4, !tbaa !8
  %arrayinit.element146 = getelementptr inbounds float, float* %arrayinit.begin142, i32 1
  %arrayidx147 = getelementptr inbounds [2 x float], [2 x float]* %w34, i32 0, i32 1
  %211 = load float, float* %arrayidx147, align 4, !tbaa !8
  %arrayidx148 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 1
  %212 = load float, float* %arrayidx148, align 4, !tbaa !8
  %call149 = call float @add_float(float %211, float %212)
  store float %call149, float* %arrayinit.element146, align 4, !tbaa !8
  %213 = bitcast [2 x float]* %w47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %213) #3
  %arrayinit.begin150 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 0
  %arrayidx151 = getelementptr inbounds [2 x float], [2 x float]* %w34, i32 0, i32 0
  %214 = load float, float* %arrayidx151, align 4, !tbaa !8
  %arrayidx152 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 0
  %215 = load float, float* %arrayidx152, align 4, !tbaa !8
  %call153 = call float @sub_float(float %214, float %215)
  store float %call153, float* %arrayinit.begin150, align 4, !tbaa !8
  %arrayinit.element154 = getelementptr inbounds float, float* %arrayinit.begin150, i32 1
  %arrayidx155 = getelementptr inbounds [2 x float], [2 x float]* %w34, i32 0, i32 1
  %216 = load float, float* %arrayidx155, align 4, !tbaa !8
  %arrayidx156 = getelementptr inbounds [2 x float], [2 x float]* %w42, i32 0, i32 1
  %217 = load float, float* %arrayidx156, align 4, !tbaa !8
  %call157 = call float @sub_float(float %216, float %217)
  store float %call157, float* %arrayinit.element154, align 4, !tbaa !8
  %218 = bitcast [2 x float]* %w48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %218) #3
  %arrayinit.begin158 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 0
  %arrayidx159 = getelementptr inbounds [2 x float], [2 x float]* %w36, i32 0, i32 0
  %219 = load float, float* %arrayidx159, align 4, !tbaa !8
  %arrayidx160 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %220 = load float, float* %arrayidx160, align 4, !tbaa !8
  %arrayidx161 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 1
  %221 = load float, float* %arrayidx161, align 4, !tbaa !8
  %call162 = call float @add_float(float %220, float %221)
  %call163 = call float @mul_float(float 0x3FE6A09EE0000000, float %call162)
  %call164 = call float @add_float(float %219, float %call163)
  store float %call164, float* %arrayinit.begin158, align 4, !tbaa !8
  %arrayinit.element165 = getelementptr inbounds float, float* %arrayinit.begin158, i32 1
  %arrayidx166 = getelementptr inbounds [2 x float], [2 x float]* %w36, i32 0, i32 1
  %222 = load float, float* %arrayidx166, align 4, !tbaa !8
  %arrayidx167 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 1
  %223 = load float, float* %arrayidx167, align 4, !tbaa !8
  %arrayidx168 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %224 = load float, float* %arrayidx168, align 4, !tbaa !8
  %call169 = call float @sub_float(float %223, float %224)
  %call170 = call float @mul_float(float 0x3FE6A09EE0000000, float %call169)
  %call171 = call float @add_float(float %222, float %call170)
  store float %call171, float* %arrayinit.element165, align 4, !tbaa !8
  %225 = bitcast [2 x float]* %w49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %225) #3
  %arrayinit.begin172 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 0
  %arrayidx173 = getelementptr inbounds [2 x float], [2 x float]* %w36, i32 0, i32 0
  %226 = load float, float* %arrayidx173, align 4, !tbaa !8
  %arrayidx174 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %227 = load float, float* %arrayidx174, align 4, !tbaa !8
  %call175 = call float @mul_float(float 0x3FE6A09EE0000000, float %227)
  %call176 = call float @sub_float(float 0.000000e+00, float %call175)
  %arrayidx177 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 1
  %228 = load float, float* %arrayidx177, align 4, !tbaa !8
  %call178 = call float @mul_float(float 0x3FE6A09EE0000000, float %228)
  %call179 = call float @sub_float(float %call176, float %call178)
  %call180 = call float @add_float(float %226, float %call179)
  store float %call180, float* %arrayinit.begin172, align 4, !tbaa !8
  %arrayinit.element181 = getelementptr inbounds float, float* %arrayinit.begin172, i32 1
  %arrayidx182 = getelementptr inbounds [2 x float], [2 x float]* %w36, i32 0, i32 1
  %229 = load float, float* %arrayidx182, align 4, !tbaa !8
  %arrayidx183 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 0
  %230 = load float, float* %arrayidx183, align 4, !tbaa !8
  %arrayidx184 = getelementptr inbounds [2 x float], [2 x float]* %w44, i32 0, i32 1
  %231 = load float, float* %arrayidx184, align 4, !tbaa !8
  %call185 = call float @sub_float(float %230, float %231)
  %call186 = call float @mul_float(float 0x3FE6A09EE0000000, float %call185)
  %call187 = call float @add_float(float %229, float %call186)
  store float %call187, float* %arrayinit.element181, align 4, !tbaa !8
  %232 = bitcast [2 x float]* %w50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %232) #3
  %arrayinit.begin188 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 0
  %arrayidx189 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %233 = load float, float* %arrayidx189, align 4, !tbaa !8
  %arrayidx190 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 1
  %234 = load float, float* %arrayidx190, align 4, !tbaa !8
  %call191 = call float @add_float(float %233, float %234)
  store float %call191, float* %arrayinit.begin188, align 4, !tbaa !8
  %arrayinit.element192 = getelementptr inbounds float, float* %arrayinit.begin188, i32 1
  %arrayidx193 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %235 = load float, float* %arrayidx193, align 4, !tbaa !8
  %arrayidx194 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 0
  %236 = load float, float* %arrayidx194, align 4, !tbaa !8
  %call195 = call float @sub_float(float %235, float %236)
  store float %call195, float* %arrayinit.element192, align 4, !tbaa !8
  %237 = bitcast [2 x float]* %w51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %237) #3
  %arrayinit.begin196 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 0
  %arrayidx197 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 0
  %238 = load float, float* %arrayidx197, align 4, !tbaa !8
  %arrayidx198 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 1
  %239 = load float, float* %arrayidx198, align 4, !tbaa !8
  %call199 = call float @sub_float(float %238, float %239)
  store float %call199, float* %arrayinit.begin196, align 4, !tbaa !8
  %arrayinit.element200 = getelementptr inbounds float, float* %arrayinit.begin196, i32 1
  %arrayidx201 = getelementptr inbounds [2 x float], [2 x float]* %w35, i32 0, i32 1
  %240 = load float, float* %arrayidx201, align 4, !tbaa !8
  %arrayidx202 = getelementptr inbounds [2 x float], [2 x float]* %w43, i32 0, i32 0
  %241 = load float, float* %arrayidx202, align 4, !tbaa !8
  %call203 = call float @add_float(float %240, float %241)
  store float %call203, float* %arrayinit.element200, align 4, !tbaa !8
  %242 = bitcast [2 x float]* %w52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %242) #3
  %arrayinit.begin204 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 0
  %arrayidx205 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %243 = load float, float* %arrayidx205, align 4, !tbaa !8
  %arrayidx206 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %244 = load float, float* %arrayidx206, align 4, !tbaa !8
  %arrayidx207 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 1
  %245 = load float, float* %arrayidx207, align 4, !tbaa !8
  %call208 = call float @sub_float(float %244, float %245)
  %call209 = call float @mul_float(float 0x3FE6A09EE0000000, float %call208)
  %call210 = call float @sub_float(float %243, float %call209)
  store float %call210, float* %arrayinit.begin204, align 4, !tbaa !8
  %arrayinit.element211 = getelementptr inbounds float, float* %arrayinit.begin204, i32 1
  %arrayidx212 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %246 = load float, float* %arrayidx212, align 4, !tbaa !8
  %arrayidx213 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 1
  %247 = load float, float* %arrayidx213, align 4, !tbaa !8
  %arrayidx214 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %248 = load float, float* %arrayidx214, align 4, !tbaa !8
  %call215 = call float @add_float(float %247, float %248)
  %call216 = call float @mul_float(float 0x3FE6A09EE0000000, float %call215)
  %call217 = call float @sub_float(float %246, float %call216)
  store float %call217, float* %arrayinit.element211, align 4, !tbaa !8
  %249 = bitcast [2 x float]* %w53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %249) #3
  %arrayinit.begin218 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 0
  %arrayidx219 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 0
  %250 = load float, float* %arrayidx219, align 4, !tbaa !8
  %arrayidx220 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %251 = load float, float* %arrayidx220, align 4, !tbaa !8
  %arrayidx221 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 1
  %252 = load float, float* %arrayidx221, align 4, !tbaa !8
  %call222 = call float @sub_float(float %251, float %252)
  %call223 = call float @mul_float(float 0x3FE6A09EE0000000, float %call222)
  %call224 = call float @add_float(float %250, float %call223)
  store float %call224, float* %arrayinit.begin218, align 4, !tbaa !8
  %arrayinit.element225 = getelementptr inbounds float, float* %arrayinit.begin218, i32 1
  %arrayidx226 = getelementptr inbounds [2 x float], [2 x float]* %w37, i32 0, i32 1
  %253 = load float, float* %arrayidx226, align 4, !tbaa !8
  %arrayidx227 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 1
  %254 = load float, float* %arrayidx227, align 4, !tbaa !8
  %arrayidx228 = getelementptr inbounds [2 x float], [2 x float]* %w45, i32 0, i32 0
  %255 = load float, float* %arrayidx228, align 4, !tbaa !8
  %call229 = call float @add_float(float %254, float %255)
  %call230 = call float @mul_float(float 0x3FE6A09EE0000000, float %call229)
  %call231 = call float @add_float(float %253, float %call230)
  store float %call231, float* %arrayinit.element225, align 4, !tbaa !8
  %256 = bitcast [2 x float]* %w54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %256) #3
  %arrayinit.begin232 = getelementptr inbounds [2 x float], [2 x float]* %w54, i32 0, i32 0
  %257 = load float, float* %i2, align 4, !tbaa !8
  %258 = load float, float* %i14, align 4, !tbaa !8
  %call233 = call float @add_float(float %257, float %258)
  store float %call233, float* %arrayinit.begin232, align 4, !tbaa !8
  %arrayinit.element234 = getelementptr inbounds float, float* %arrayinit.begin232, i32 1
  %259 = load float, float* %i30, align 4, !tbaa !8
  %260 = load float, float* %i18, align 4, !tbaa !8
  %call235 = call float @sub_float(float %259, float %260)
  store float %call235, float* %arrayinit.element234, align 4, !tbaa !8
  %261 = bitcast [2 x float]* %w55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %261) #3
  %arrayinit.begin236 = getelementptr inbounds [2 x float], [2 x float]* %w55, i32 0, i32 0
  %262 = load float, float* %i2, align 4, !tbaa !8
  %263 = load float, float* %i14, align 4, !tbaa !8
  %call237 = call float @sub_float(float %262, float %263)
  store float %call237, float* %arrayinit.begin236, align 4, !tbaa !8
  %arrayinit.element238 = getelementptr inbounds float, float* %arrayinit.begin236, i32 1
  %264 = load float, float* %i18, align 4, !tbaa !8
  %call239 = call float @sub_float(float 0.000000e+00, float %264)
  %265 = load float, float* %i30, align 4, !tbaa !8
  %call240 = call float @sub_float(float %call239, float %265)
  store float %call240, float* %arrayinit.element238, align 4, !tbaa !8
  %266 = bitcast [2 x float]* %w56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %266) #3
  %arrayinit.begin241 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 0
  %267 = load float, float* %i10, align 4, !tbaa !8
  %268 = load float, float* %i6, align 4, !tbaa !8
  %call242 = call float @add_float(float %267, float %268)
  store float %call242, float* %arrayinit.begin241, align 4, !tbaa !8
  %arrayinit.element243 = getelementptr inbounds float, float* %arrayinit.begin241, i32 1
  %269 = load float, float* %i22, align 4, !tbaa !8
  %270 = load float, float* %i26, align 4, !tbaa !8
  %call244 = call float @sub_float(float %269, float %270)
  store float %call244, float* %arrayinit.element243, align 4, !tbaa !8
  %271 = bitcast [2 x float]* %w57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %271) #3
  %arrayinit.begin245 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 0
  %272 = load float, float* %i10, align 4, !tbaa !8
  %273 = load float, float* %i6, align 4, !tbaa !8
  %call246 = call float @sub_float(float %272, float %273)
  store float %call246, float* %arrayinit.begin245, align 4, !tbaa !8
  %arrayinit.element247 = getelementptr inbounds float, float* %arrayinit.begin245, i32 1
  %274 = load float, float* %i26, align 4, !tbaa !8
  %call248 = call float @sub_float(float 0.000000e+00, float %274)
  %275 = load float, float* %i22, align 4, !tbaa !8
  %call249 = call float @sub_float(float %call248, float %275)
  store float %call249, float* %arrayinit.element247, align 4, !tbaa !8
  %276 = bitcast [2 x float]* %w58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %276) #3
  %arrayinit.begin250 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 0
  %arrayidx251 = getelementptr inbounds [2 x float], [2 x float]* %w54, i32 0, i32 0
  %277 = load float, float* %arrayidx251, align 4, !tbaa !8
  %arrayidx252 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 0
  %278 = load float, float* %arrayidx252, align 4, !tbaa !8
  %call253 = call float @add_float(float %277, float %278)
  store float %call253, float* %arrayinit.begin250, align 4, !tbaa !8
  %arrayinit.element254 = getelementptr inbounds float, float* %arrayinit.begin250, i32 1
  %arrayidx255 = getelementptr inbounds [2 x float], [2 x float]* %w54, i32 0, i32 1
  %279 = load float, float* %arrayidx255, align 4, !tbaa !8
  %arrayidx256 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 1
  %280 = load float, float* %arrayidx256, align 4, !tbaa !8
  %call257 = call float @add_float(float %279, float %280)
  store float %call257, float* %arrayinit.element254, align 4, !tbaa !8
  %281 = bitcast [2 x float]* %w59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %281) #3
  %arrayinit.begin258 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 0
  %arrayidx259 = getelementptr inbounds [2 x float], [2 x float]* %w54, i32 0, i32 0
  %282 = load float, float* %arrayidx259, align 4, !tbaa !8
  %arrayidx260 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 0
  %283 = load float, float* %arrayidx260, align 4, !tbaa !8
  %call261 = call float @sub_float(float %282, float %283)
  store float %call261, float* %arrayinit.begin258, align 4, !tbaa !8
  %arrayinit.element262 = getelementptr inbounds float, float* %arrayinit.begin258, i32 1
  %arrayidx263 = getelementptr inbounds [2 x float], [2 x float]* %w54, i32 0, i32 1
  %284 = load float, float* %arrayidx263, align 4, !tbaa !8
  %arrayidx264 = getelementptr inbounds [2 x float], [2 x float]* %w56, i32 0, i32 1
  %285 = load float, float* %arrayidx264, align 4, !tbaa !8
  %call265 = call float @sub_float(float %284, float %285)
  store float %call265, float* %arrayinit.element262, align 4, !tbaa !8
  %286 = bitcast [2 x float]* %w60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %286) #3
  %arrayinit.begin266 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 0
  %arrayidx267 = getelementptr inbounds [2 x float], [2 x float]* %w55, i32 0, i32 0
  %287 = load float, float* %arrayidx267, align 4, !tbaa !8
  %arrayidx268 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 1
  %288 = load float, float* %arrayidx268, align 4, !tbaa !8
  %call269 = call float @add_float(float %287, float %288)
  store float %call269, float* %arrayinit.begin266, align 4, !tbaa !8
  %arrayinit.element270 = getelementptr inbounds float, float* %arrayinit.begin266, i32 1
  %arrayidx271 = getelementptr inbounds [2 x float], [2 x float]* %w55, i32 0, i32 1
  %289 = load float, float* %arrayidx271, align 4, !tbaa !8
  %arrayidx272 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 0
  %290 = load float, float* %arrayidx272, align 4, !tbaa !8
  %call273 = call float @sub_float(float %289, float %290)
  store float %call273, float* %arrayinit.element270, align 4, !tbaa !8
  %291 = bitcast [2 x float]* %w61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %291) #3
  %arrayinit.begin274 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 0
  %arrayidx275 = getelementptr inbounds [2 x float], [2 x float]* %w55, i32 0, i32 0
  %292 = load float, float* %arrayidx275, align 4, !tbaa !8
  %arrayidx276 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 1
  %293 = load float, float* %arrayidx276, align 4, !tbaa !8
  %call277 = call float @sub_float(float %292, float %293)
  store float %call277, float* %arrayinit.begin274, align 4, !tbaa !8
  %arrayinit.element278 = getelementptr inbounds float, float* %arrayinit.begin274, i32 1
  %arrayidx279 = getelementptr inbounds [2 x float], [2 x float]* %w55, i32 0, i32 1
  %294 = load float, float* %arrayidx279, align 4, !tbaa !8
  %arrayidx280 = getelementptr inbounds [2 x float], [2 x float]* %w57, i32 0, i32 0
  %295 = load float, float* %arrayidx280, align 4, !tbaa !8
  %call281 = call float @add_float(float %294, float %295)
  store float %call281, float* %arrayinit.element278, align 4, !tbaa !8
  %296 = bitcast [2 x float]* %w62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %296) #3
  %arrayinit.begin282 = getelementptr inbounds [2 x float], [2 x float]* %w62, i32 0, i32 0
  %297 = load float, float* %i6, align 4, !tbaa !8
  %298 = load float, float* %i10, align 4, !tbaa !8
  %call283 = call float @add_float(float %297, float %298)
  store float %call283, float* %arrayinit.begin282, align 4, !tbaa !8
  %arrayinit.element284 = getelementptr inbounds float, float* %arrayinit.begin282, i32 1
  %299 = load float, float* %i26, align 4, !tbaa !8
  %300 = load float, float* %i22, align 4, !tbaa !8
  %call285 = call float @sub_float(float %299, float %300)
  store float %call285, float* %arrayinit.element284, align 4, !tbaa !8
  %301 = bitcast [2 x float]* %w63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %301) #3
  %arrayinit.begin286 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 0
  %302 = load float, float* %i6, align 4, !tbaa !8
  %303 = load float, float* %i10, align 4, !tbaa !8
  %call287 = call float @sub_float(float %302, float %303)
  store float %call287, float* %arrayinit.begin286, align 4, !tbaa !8
  %arrayinit.element288 = getelementptr inbounds float, float* %arrayinit.begin286, i32 1
  %304 = load float, float* %i22, align 4, !tbaa !8
  %call289 = call float @sub_float(float 0.000000e+00, float %304)
  %305 = load float, float* %i26, align 4, !tbaa !8
  %call290 = call float @sub_float(float %call289, float %305)
  store float %call290, float* %arrayinit.element288, align 4, !tbaa !8
  %306 = bitcast [2 x float]* %w64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %306) #3
  %arrayinit.begin291 = getelementptr inbounds [2 x float], [2 x float]* %w64, i32 0, i32 0
  %307 = load float, float* %i14, align 4, !tbaa !8
  %308 = load float, float* %i2, align 4, !tbaa !8
  %call292 = call float @add_float(float %307, float %308)
  store float %call292, float* %arrayinit.begin291, align 4, !tbaa !8
  %arrayinit.element293 = getelementptr inbounds float, float* %arrayinit.begin291, i32 1
  %309 = load float, float* %i18, align 4, !tbaa !8
  %310 = load float, float* %i30, align 4, !tbaa !8
  %call294 = call float @sub_float(float %309, float %310)
  store float %call294, float* %arrayinit.element293, align 4, !tbaa !8
  %311 = bitcast [2 x float]* %w65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %311) #3
  %arrayinit.begin295 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 0
  %312 = load float, float* %i14, align 4, !tbaa !8
  %313 = load float, float* %i2, align 4, !tbaa !8
  %call296 = call float @sub_float(float %312, float %313)
  store float %call296, float* %arrayinit.begin295, align 4, !tbaa !8
  %arrayinit.element297 = getelementptr inbounds float, float* %arrayinit.begin295, i32 1
  %314 = load float, float* %i30, align 4, !tbaa !8
  %call298 = call float @sub_float(float 0.000000e+00, float %314)
  %315 = load float, float* %i18, align 4, !tbaa !8
  %call299 = call float @sub_float(float %call298, float %315)
  store float %call299, float* %arrayinit.element297, align 4, !tbaa !8
  %316 = bitcast [2 x float]* %w66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %316) #3
  %arrayinit.begin300 = getelementptr inbounds [2 x float], [2 x float]* %w66, i32 0, i32 0
  %arrayidx301 = getelementptr inbounds [2 x float], [2 x float]* %w62, i32 0, i32 0
  %317 = load float, float* %arrayidx301, align 4, !tbaa !8
  %arrayidx302 = getelementptr inbounds [2 x float], [2 x float]* %w64, i32 0, i32 0
  %318 = load float, float* %arrayidx302, align 4, !tbaa !8
  %call303 = call float @add_float(float %317, float %318)
  store float %call303, float* %arrayinit.begin300, align 4, !tbaa !8
  %arrayinit.element304 = getelementptr inbounds float, float* %arrayinit.begin300, i32 1
  %arrayidx305 = getelementptr inbounds [2 x float], [2 x float]* %w62, i32 0, i32 1
  %319 = load float, float* %arrayidx305, align 4, !tbaa !8
  %arrayidx306 = getelementptr inbounds [2 x float], [2 x float]* %w64, i32 0, i32 1
  %320 = load float, float* %arrayidx306, align 4, !tbaa !8
  %call307 = call float @add_float(float %319, float %320)
  store float %call307, float* %arrayinit.element304, align 4, !tbaa !8
  %321 = bitcast [2 x float]* %w67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %321) #3
  %arrayinit.begin308 = getelementptr inbounds [2 x float], [2 x float]* %w67, i32 0, i32 0
  %arrayidx309 = getelementptr inbounds [2 x float], [2 x float]* %w62, i32 0, i32 0
  %322 = load float, float* %arrayidx309, align 4, !tbaa !8
  %arrayidx310 = getelementptr inbounds [2 x float], [2 x float]* %w64, i32 0, i32 0
  %323 = load float, float* %arrayidx310, align 4, !tbaa !8
  %call311 = call float @sub_float(float %322, float %323)
  store float %call311, float* %arrayinit.begin308, align 4, !tbaa !8
  %arrayinit.element312 = getelementptr inbounds float, float* %arrayinit.begin308, i32 1
  %arrayidx313 = getelementptr inbounds [2 x float], [2 x float]* %w62, i32 0, i32 1
  %324 = load float, float* %arrayidx313, align 4, !tbaa !8
  %arrayidx314 = getelementptr inbounds [2 x float], [2 x float]* %w64, i32 0, i32 1
  %325 = load float, float* %arrayidx314, align 4, !tbaa !8
  %call315 = call float @sub_float(float %324, float %325)
  store float %call315, float* %arrayinit.element312, align 4, !tbaa !8
  %326 = bitcast [2 x float]* %w68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %326) #3
  %arrayinit.begin316 = getelementptr inbounds [2 x float], [2 x float]* %w68, i32 0, i32 0
  %arrayidx317 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 0
  %327 = load float, float* %arrayidx317, align 4, !tbaa !8
  %arrayidx318 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 1
  %328 = load float, float* %arrayidx318, align 4, !tbaa !8
  %call319 = call float @add_float(float %327, float %328)
  store float %call319, float* %arrayinit.begin316, align 4, !tbaa !8
  %arrayinit.element320 = getelementptr inbounds float, float* %arrayinit.begin316, i32 1
  %arrayidx321 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 1
  %329 = load float, float* %arrayidx321, align 4, !tbaa !8
  %arrayidx322 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 0
  %330 = load float, float* %arrayidx322, align 4, !tbaa !8
  %call323 = call float @sub_float(float %329, float %330)
  store float %call323, float* %arrayinit.element320, align 4, !tbaa !8
  %331 = bitcast [2 x float]* %w69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %331) #3
  %arrayinit.begin324 = getelementptr inbounds [2 x float], [2 x float]* %w69, i32 0, i32 0
  %arrayidx325 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 0
  %332 = load float, float* %arrayidx325, align 4, !tbaa !8
  %arrayidx326 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 1
  %333 = load float, float* %arrayidx326, align 4, !tbaa !8
  %call327 = call float @sub_float(float %332, float %333)
  store float %call327, float* %arrayinit.begin324, align 4, !tbaa !8
  %arrayinit.element328 = getelementptr inbounds float, float* %arrayinit.begin324, i32 1
  %arrayidx329 = getelementptr inbounds [2 x float], [2 x float]* %w63, i32 0, i32 1
  %334 = load float, float* %arrayidx329, align 4, !tbaa !8
  %arrayidx330 = getelementptr inbounds [2 x float], [2 x float]* %w65, i32 0, i32 0
  %335 = load float, float* %arrayidx330, align 4, !tbaa !8
  %call331 = call float @add_float(float %334, float %335)
  store float %call331, float* %arrayinit.element328, align 4, !tbaa !8
  %336 = bitcast [2 x float]* %w70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %336) #3
  %arrayinit.begin332 = getelementptr inbounds [2 x float], [2 x float]* %w70, i32 0, i32 0
  %arrayidx333 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 0
  %337 = load float, float* %arrayidx333, align 4, !tbaa !8
  %arrayidx334 = getelementptr inbounds [2 x float], [2 x float]* %w66, i32 0, i32 0
  %338 = load float, float* %arrayidx334, align 4, !tbaa !8
  %call335 = call float @add_float(float %337, float %338)
  store float %call335, float* %arrayinit.begin332, align 4, !tbaa !8
  %arrayinit.element336 = getelementptr inbounds float, float* %arrayinit.begin332, i32 1
  %arrayidx337 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 1
  %339 = load float, float* %arrayidx337, align 4, !tbaa !8
  %arrayidx338 = getelementptr inbounds [2 x float], [2 x float]* %w66, i32 0, i32 1
  %340 = load float, float* %arrayidx338, align 4, !tbaa !8
  %call339 = call float @add_float(float %339, float %340)
  store float %call339, float* %arrayinit.element336, align 4, !tbaa !8
  %341 = bitcast [2 x float]* %w71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %341) #3
  %arrayinit.begin340 = getelementptr inbounds [2 x float], [2 x float]* %w71, i32 0, i32 0
  %arrayidx341 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 0
  %342 = load float, float* %arrayidx341, align 4, !tbaa !8
  %arrayidx342 = getelementptr inbounds [2 x float], [2 x float]* %w66, i32 0, i32 0
  %343 = load float, float* %arrayidx342, align 4, !tbaa !8
  %call343 = call float @sub_float(float %342, float %343)
  store float %call343, float* %arrayinit.begin340, align 4, !tbaa !8
  %arrayinit.element344 = getelementptr inbounds float, float* %arrayinit.begin340, i32 1
  %arrayidx345 = getelementptr inbounds [2 x float], [2 x float]* %w58, i32 0, i32 1
  %344 = load float, float* %arrayidx345, align 4, !tbaa !8
  %arrayidx346 = getelementptr inbounds [2 x float], [2 x float]* %w66, i32 0, i32 1
  %345 = load float, float* %arrayidx346, align 4, !tbaa !8
  %call347 = call float @sub_float(float %344, float %345)
  store float %call347, float* %arrayinit.element344, align 4, !tbaa !8
  %346 = bitcast [2 x float]* %w72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %346) #3
  %arrayinit.begin348 = getelementptr inbounds [2 x float], [2 x float]* %w72, i32 0, i32 0
  %arrayidx349 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 0
  %347 = load float, float* %arrayidx349, align 4, !tbaa !8
  %arrayidx350 = getelementptr inbounds [2 x float], [2 x float]* %w68, i32 0, i32 0
  %348 = load float, float* %arrayidx350, align 4, !tbaa !8
  %arrayidx351 = getelementptr inbounds [2 x float], [2 x float]* %w68, i32 0, i32 1
  %349 = load float, float* %arrayidx351, align 4, !tbaa !8
  %call352 = call float @add_float(float %348, float %349)
  %call353 = call float @mul_float(float 0x3FE6A09EE0000000, float %call352)
  %call354 = call float @add_float(float %347, float %call353)
  store float %call354, float* %arrayinit.begin348, align 4, !tbaa !8
  %arrayinit.element355 = getelementptr inbounds float, float* %arrayinit.begin348, i32 1
  %arrayidx356 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 1
  %350 = load float, float* %arrayidx356, align 4, !tbaa !8
  %arrayidx357 = getelementptr inbounds [2 x float], [2 x float]* %w68, i32 0, i32 1
  %351 = load float, float* %arrayidx357, align 4, !tbaa !8
  %arrayidx358 = getelementptr inbounds [2 x float], [2 x float]* %w68, i32 0, i32 0
  %352 = load float, float* %arrayidx358, align 4, !tbaa !8
  %call359 = call float @sub_float(float %351, float %352)
  %call360 = call float @mul_float(float 0x3FE6A09EE0000000, float %call359)
  %call361 = call float @add_float(float %350, float %call360)
  store float %call361, float* %arrayinit.element355, align 4, !tbaa !8
  %353 = bitcast [2 x float]* %w73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %353) #3
  %arrayinit.begin362 = getelementptr inbounds [2 x float], [2 x float]* %w73, i32 0, i32 0
  %arrayidx363 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 0
  %354 = load float, float* %arrayidx363, align 4, !tbaa !8
  %arrayidx364 = getelementptr inbounds [2 x float], [2 x float]* %w68, i32 0, i32 0
  %355 = load float, float* %arrayidx364, align 4, !tbaa !8
  %call365 = call float @mul_float(float 0x3FE6A09EE0000000, float %355)
  %call366 = call float @sub_float(float 0.000000e+00, float %call365)
  %arrayidx367 = getelementptr inbounds [2 x float], [2 x float]* %w68, i32 0, i32 1
  %356 = load float, float* %arrayidx367, align 4, !tbaa !8
  %call368 = call float @mul_float(float 0x3FE6A09EE0000000, float %356)
  %call369 = call float @sub_float(float %call366, float %call368)
  %call370 = call float @add_float(float %354, float %call369)
  store float %call370, float* %arrayinit.begin362, align 4, !tbaa !8
  %arrayinit.element371 = getelementptr inbounds float, float* %arrayinit.begin362, i32 1
  %arrayidx372 = getelementptr inbounds [2 x float], [2 x float]* %w60, i32 0, i32 1
  %357 = load float, float* %arrayidx372, align 4, !tbaa !8
  %arrayidx373 = getelementptr inbounds [2 x float], [2 x float]* %w68, i32 0, i32 0
  %358 = load float, float* %arrayidx373, align 4, !tbaa !8
  %arrayidx374 = getelementptr inbounds [2 x float], [2 x float]* %w68, i32 0, i32 1
  %359 = load float, float* %arrayidx374, align 4, !tbaa !8
  %call375 = call float @sub_float(float %358, float %359)
  %call376 = call float @mul_float(float 0x3FE6A09EE0000000, float %call375)
  %call377 = call float @add_float(float %357, float %call376)
  store float %call377, float* %arrayinit.element371, align 4, !tbaa !8
  %360 = bitcast [2 x float]* %w74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %360) #3
  %arrayinit.begin378 = getelementptr inbounds [2 x float], [2 x float]* %w74, i32 0, i32 0
  %arrayidx379 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 0
  %361 = load float, float* %arrayidx379, align 4, !tbaa !8
  %arrayidx380 = getelementptr inbounds [2 x float], [2 x float]* %w67, i32 0, i32 1
  %362 = load float, float* %arrayidx380, align 4, !tbaa !8
  %call381 = call float @add_float(float %361, float %362)
  store float %call381, float* %arrayinit.begin378, align 4, !tbaa !8
  %arrayinit.element382 = getelementptr inbounds float, float* %arrayinit.begin378, i32 1
  %arrayidx383 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 1
  %363 = load float, float* %arrayidx383, align 4, !tbaa !8
  %arrayidx384 = getelementptr inbounds [2 x float], [2 x float]* %w67, i32 0, i32 0
  %364 = load float, float* %arrayidx384, align 4, !tbaa !8
  %call385 = call float @sub_float(float %363, float %364)
  store float %call385, float* %arrayinit.element382, align 4, !tbaa !8
  %365 = bitcast [2 x float]* %w75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %365) #3
  %arrayinit.begin386 = getelementptr inbounds [2 x float], [2 x float]* %w75, i32 0, i32 0
  %arrayidx387 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 0
  %366 = load float, float* %arrayidx387, align 4, !tbaa !8
  %arrayidx388 = getelementptr inbounds [2 x float], [2 x float]* %w67, i32 0, i32 1
  %367 = load float, float* %arrayidx388, align 4, !tbaa !8
  %call389 = call float @sub_float(float %366, float %367)
  store float %call389, float* %arrayinit.begin386, align 4, !tbaa !8
  %arrayinit.element390 = getelementptr inbounds float, float* %arrayinit.begin386, i32 1
  %arrayidx391 = getelementptr inbounds [2 x float], [2 x float]* %w59, i32 0, i32 1
  %368 = load float, float* %arrayidx391, align 4, !tbaa !8
  %arrayidx392 = getelementptr inbounds [2 x float], [2 x float]* %w67, i32 0, i32 0
  %369 = load float, float* %arrayidx392, align 4, !tbaa !8
  %call393 = call float @add_float(float %368, float %369)
  store float %call393, float* %arrayinit.element390, align 4, !tbaa !8
  %370 = bitcast [2 x float]* %w76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %370) #3
  %arrayinit.begin394 = getelementptr inbounds [2 x float], [2 x float]* %w76, i32 0, i32 0
  %arrayidx395 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 0
  %371 = load float, float* %arrayidx395, align 4, !tbaa !8
  %arrayidx396 = getelementptr inbounds [2 x float], [2 x float]* %w69, i32 0, i32 0
  %372 = load float, float* %arrayidx396, align 4, !tbaa !8
  %arrayidx397 = getelementptr inbounds [2 x float], [2 x float]* %w69, i32 0, i32 1
  %373 = load float, float* %arrayidx397, align 4, !tbaa !8
  %call398 = call float @sub_float(float %372, float %373)
  %call399 = call float @mul_float(float 0x3FE6A09EE0000000, float %call398)
  %call400 = call float @sub_float(float %371, float %call399)
  store float %call400, float* %arrayinit.begin394, align 4, !tbaa !8
  %arrayinit.element401 = getelementptr inbounds float, float* %arrayinit.begin394, i32 1
  %arrayidx402 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 1
  %374 = load float, float* %arrayidx402, align 4, !tbaa !8
  %arrayidx403 = getelementptr inbounds [2 x float], [2 x float]* %w69, i32 0, i32 1
  %375 = load float, float* %arrayidx403, align 4, !tbaa !8
  %arrayidx404 = getelementptr inbounds [2 x float], [2 x float]* %w69, i32 0, i32 0
  %376 = load float, float* %arrayidx404, align 4, !tbaa !8
  %call405 = call float @add_float(float %375, float %376)
  %call406 = call float @mul_float(float 0x3FE6A09EE0000000, float %call405)
  %call407 = call float @sub_float(float %374, float %call406)
  store float %call407, float* %arrayinit.element401, align 4, !tbaa !8
  %377 = bitcast [2 x float]* %w77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %377) #3
  %arrayinit.begin408 = getelementptr inbounds [2 x float], [2 x float]* %w77, i32 0, i32 0
  %arrayidx409 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 0
  %378 = load float, float* %arrayidx409, align 4, !tbaa !8
  %arrayidx410 = getelementptr inbounds [2 x float], [2 x float]* %w69, i32 0, i32 0
  %379 = load float, float* %arrayidx410, align 4, !tbaa !8
  %arrayidx411 = getelementptr inbounds [2 x float], [2 x float]* %w69, i32 0, i32 1
  %380 = load float, float* %arrayidx411, align 4, !tbaa !8
  %call412 = call float @sub_float(float %379, float %380)
  %call413 = call float @mul_float(float 0x3FE6A09EE0000000, float %call412)
  %call414 = call float @add_float(float %378, float %call413)
  store float %call414, float* %arrayinit.begin408, align 4, !tbaa !8
  %arrayinit.element415 = getelementptr inbounds float, float* %arrayinit.begin408, i32 1
  %arrayidx416 = getelementptr inbounds [2 x float], [2 x float]* %w61, i32 0, i32 1
  %381 = load float, float* %arrayidx416, align 4, !tbaa !8
  %arrayidx417 = getelementptr inbounds [2 x float], [2 x float]* %w69, i32 0, i32 1
  %382 = load float, float* %arrayidx417, align 4, !tbaa !8
  %arrayidx418 = getelementptr inbounds [2 x float], [2 x float]* %w69, i32 0, i32 0
  %383 = load float, float* %arrayidx418, align 4, !tbaa !8
  %call419 = call float @add_float(float %382, float %383)
  %call420 = call float @mul_float(float 0x3FE6A09EE0000000, float %call419)
  %call421 = call float @add_float(float %381, float %call420)
  store float %call421, float* %arrayinit.element415, align 4, !tbaa !8
  %384 = bitcast [2 x float]* %w78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %384) #3
  %arrayinit.begin422 = getelementptr inbounds [2 x float], [2 x float]* %w78, i32 0, i32 0
  %arrayidx423 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 0
  %385 = load float, float* %arrayidx423, align 4, !tbaa !8
  %arrayidx424 = getelementptr inbounds [2 x float], [2 x float]* %w70, i32 0, i32 0
  %386 = load float, float* %arrayidx424, align 4, !tbaa !8
  %call425 = call float @add_float(float %385, float %386)
  store float %call425, float* %arrayinit.begin422, align 4, !tbaa !8
  %arrayinit.element426 = getelementptr inbounds float, float* %arrayinit.begin422, i32 1
  %arrayidx427 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 1
  %387 = load float, float* %arrayidx427, align 4, !tbaa !8
  %arrayidx428 = getelementptr inbounds [2 x float], [2 x float]* %w70, i32 0, i32 1
  %388 = load float, float* %arrayidx428, align 4, !tbaa !8
  %call429 = call float @add_float(float %387, float %388)
  store float %call429, float* %arrayinit.element426, align 4, !tbaa !8
  %389 = bitcast [2 x float]* %w79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %389) #3
  %arrayinit.begin430 = getelementptr inbounds [2 x float], [2 x float]* %w79, i32 0, i32 0
  %arrayidx431 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 0
  %390 = load float, float* %arrayidx431, align 4, !tbaa !8
  %arrayidx432 = getelementptr inbounds [2 x float], [2 x float]* %w70, i32 0, i32 0
  %391 = load float, float* %arrayidx432, align 4, !tbaa !8
  %call433 = call float @sub_float(float %390, float %391)
  store float %call433, float* %arrayinit.begin430, align 4, !tbaa !8
  %arrayinit.element434 = getelementptr inbounds float, float* %arrayinit.begin430, i32 1
  %arrayidx435 = getelementptr inbounds [2 x float], [2 x float]* %w46, i32 0, i32 1
  %392 = load float, float* %arrayidx435, align 4, !tbaa !8
  %arrayidx436 = getelementptr inbounds [2 x float], [2 x float]* %w70, i32 0, i32 1
  %393 = load float, float* %arrayidx436, align 4, !tbaa !8
  %call437 = call float @sub_float(float %392, float %393)
  store float %call437, float* %arrayinit.element434, align 4, !tbaa !8
  %394 = bitcast [2 x float]* %w80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %394) #3
  %arrayinit.begin438 = getelementptr inbounds [2 x float], [2 x float]* %w80, i32 0, i32 0
  %arrayidx439 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 0
  %395 = load float, float* %arrayidx439, align 4, !tbaa !8
  %arrayidx440 = getelementptr inbounds [2 x float], [2 x float]* %w72, i32 0, i32 0
  %396 = load float, float* %arrayidx440, align 4, !tbaa !8
  %call441 = call float @mul_float(float 0x3FED906CC0000000, float %396)
  %arrayidx442 = getelementptr inbounds [2 x float], [2 x float]* %w72, i32 0, i32 1
  %397 = load float, float* %arrayidx442, align 4, !tbaa !8
  %call443 = call float @mul_float(float 0x3FD87DE0E0000000, float %397)
  %call444 = call float @add_float(float %call441, float %call443)
  %call445 = call float @add_float(float %395, float %call444)
  store float %call445, float* %arrayinit.begin438, align 4, !tbaa !8
  %arrayinit.element446 = getelementptr inbounds float, float* %arrayinit.begin438, i32 1
  %arrayidx447 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 1
  %398 = load float, float* %arrayidx447, align 4, !tbaa !8
  %arrayidx448 = getelementptr inbounds [2 x float], [2 x float]* %w72, i32 0, i32 1
  %399 = load float, float* %arrayidx448, align 4, !tbaa !8
  %call449 = call float @mul_float(float 0x3FED906CC0000000, float %399)
  %arrayidx450 = getelementptr inbounds [2 x float], [2 x float]* %w72, i32 0, i32 0
  %400 = load float, float* %arrayidx450, align 4, !tbaa !8
  %call451 = call float @mul_float(float 0x3FD87DE0E0000000, float %400)
  %call452 = call float @sub_float(float %call449, float %call451)
  %call453 = call float @add_float(float %398, float %call452)
  store float %call453, float* %arrayinit.element446, align 4, !tbaa !8
  %401 = bitcast [2 x float]* %w81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %401) #3
  %arrayinit.begin454 = getelementptr inbounds [2 x float], [2 x float]* %w81, i32 0, i32 0
  %arrayidx455 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 0
  %402 = load float, float* %arrayidx455, align 4, !tbaa !8
  %arrayidx456 = getelementptr inbounds [2 x float], [2 x float]* %w72, i32 0, i32 0
  %403 = load float, float* %arrayidx456, align 4, !tbaa !8
  %call457 = call float @mul_float(float 0x3FED906CC0000000, float %403)
  %call458 = call float @sub_float(float 0.000000e+00, float %call457)
  %arrayidx459 = getelementptr inbounds [2 x float], [2 x float]* %w72, i32 0, i32 1
  %404 = load float, float* %arrayidx459, align 4, !tbaa !8
  %call460 = call float @mul_float(float 0x3FD87DE0E0000000, float %404)
  %call461 = call float @sub_float(float %call458, float %call460)
  %call462 = call float @add_float(float %402, float %call461)
  store float %call462, float* %arrayinit.begin454, align 4, !tbaa !8
  %arrayinit.element463 = getelementptr inbounds float, float* %arrayinit.begin454, i32 1
  %arrayidx464 = getelementptr inbounds [2 x float], [2 x float]* %w48, i32 0, i32 1
  %405 = load float, float* %arrayidx464, align 4, !tbaa !8
  %arrayidx465 = getelementptr inbounds [2 x float], [2 x float]* %w72, i32 0, i32 0
  %406 = load float, float* %arrayidx465, align 4, !tbaa !8
  %call466 = call float @mul_float(float 0x3FD87DE0E0000000, float %406)
  %arrayidx467 = getelementptr inbounds [2 x float], [2 x float]* %w72, i32 0, i32 1
  %407 = load float, float* %arrayidx467, align 4, !tbaa !8
  %call468 = call float @mul_float(float 0x3FED906CC0000000, float %407)
  %call469 = call float @sub_float(float %call466, float %call468)
  %call470 = call float @add_float(float %405, float %call469)
  store float %call470, float* %arrayinit.element463, align 4, !tbaa !8
  %408 = bitcast [2 x float]* %w82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %408) #3
  %arrayinit.begin471 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 0
  %arrayidx472 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 0
  %409 = load float, float* %arrayidx472, align 4, !tbaa !8
  %arrayidx473 = getelementptr inbounds [2 x float], [2 x float]* %w74, i32 0, i32 0
  %410 = load float, float* %arrayidx473, align 4, !tbaa !8
  %arrayidx474 = getelementptr inbounds [2 x float], [2 x float]* %w74, i32 0, i32 1
  %411 = load float, float* %arrayidx474, align 4, !tbaa !8
  %call475 = call float @add_float(float %410, float %411)
  %call476 = call float @mul_float(float 0x3FE6A09EE0000000, float %call475)
  %call477 = call float @add_float(float %409, float %call476)
  store float %call477, float* %arrayinit.begin471, align 4, !tbaa !8
  %arrayinit.element478 = getelementptr inbounds float, float* %arrayinit.begin471, i32 1
  %arrayidx479 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 1
  %412 = load float, float* %arrayidx479, align 4, !tbaa !8
  %arrayidx480 = getelementptr inbounds [2 x float], [2 x float]* %w74, i32 0, i32 1
  %413 = load float, float* %arrayidx480, align 4, !tbaa !8
  %arrayidx481 = getelementptr inbounds [2 x float], [2 x float]* %w74, i32 0, i32 0
  %414 = load float, float* %arrayidx481, align 4, !tbaa !8
  %call482 = call float @sub_float(float %413, float %414)
  %call483 = call float @mul_float(float 0x3FE6A09EE0000000, float %call482)
  %call484 = call float @add_float(float %412, float %call483)
  store float %call484, float* %arrayinit.element478, align 4, !tbaa !8
  %415 = bitcast [2 x float]* %w83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %415) #3
  %arrayinit.begin485 = getelementptr inbounds [2 x float], [2 x float]* %w83, i32 0, i32 0
  %arrayidx486 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 0
  %416 = load float, float* %arrayidx486, align 4, !tbaa !8
  %arrayidx487 = getelementptr inbounds [2 x float], [2 x float]* %w74, i32 0, i32 0
  %417 = load float, float* %arrayidx487, align 4, !tbaa !8
  %call488 = call float @mul_float(float 0x3FE6A09EE0000000, float %417)
  %call489 = call float @sub_float(float 0.000000e+00, float %call488)
  %arrayidx490 = getelementptr inbounds [2 x float], [2 x float]* %w74, i32 0, i32 1
  %418 = load float, float* %arrayidx490, align 4, !tbaa !8
  %call491 = call float @mul_float(float 0x3FE6A09EE0000000, float %418)
  %call492 = call float @sub_float(float %call489, float %call491)
  %call493 = call float @add_float(float %416, float %call492)
  store float %call493, float* %arrayinit.begin485, align 4, !tbaa !8
  %arrayinit.element494 = getelementptr inbounds float, float* %arrayinit.begin485, i32 1
  %arrayidx495 = getelementptr inbounds [2 x float], [2 x float]* %w50, i32 0, i32 1
  %419 = load float, float* %arrayidx495, align 4, !tbaa !8
  %arrayidx496 = getelementptr inbounds [2 x float], [2 x float]* %w74, i32 0, i32 0
  %420 = load float, float* %arrayidx496, align 4, !tbaa !8
  %arrayidx497 = getelementptr inbounds [2 x float], [2 x float]* %w74, i32 0, i32 1
  %421 = load float, float* %arrayidx497, align 4, !tbaa !8
  %call498 = call float @sub_float(float %420, float %421)
  %call499 = call float @mul_float(float 0x3FE6A09EE0000000, float %call498)
  %call500 = call float @add_float(float %419, float %call499)
  store float %call500, float* %arrayinit.element494, align 4, !tbaa !8
  %422 = bitcast [2 x float]* %w84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %422) #3
  %arrayinit.begin501 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 0
  %arrayidx502 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 0
  %423 = load float, float* %arrayidx502, align 4, !tbaa !8
  %arrayidx503 = getelementptr inbounds [2 x float], [2 x float]* %w76, i32 0, i32 0
  %424 = load float, float* %arrayidx503, align 4, !tbaa !8
  %call504 = call float @mul_float(float 0x3FD87DE0E0000000, float %424)
  %arrayidx505 = getelementptr inbounds [2 x float], [2 x float]* %w76, i32 0, i32 1
  %425 = load float, float* %arrayidx505, align 4, !tbaa !8
  %call506 = call float @mul_float(float 0x3FED906CC0000000, float %425)
  %call507 = call float @add_float(float %call504, float %call506)
  %call508 = call float @add_float(float %423, float %call507)
  store float %call508, float* %arrayinit.begin501, align 4, !tbaa !8
  %arrayinit.element509 = getelementptr inbounds float, float* %arrayinit.begin501, i32 1
  %arrayidx510 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 1
  %426 = load float, float* %arrayidx510, align 4, !tbaa !8
  %arrayidx511 = getelementptr inbounds [2 x float], [2 x float]* %w76, i32 0, i32 1
  %427 = load float, float* %arrayidx511, align 4, !tbaa !8
  %call512 = call float @mul_float(float 0x3FD87DE0E0000000, float %427)
  %arrayidx513 = getelementptr inbounds [2 x float], [2 x float]* %w76, i32 0, i32 0
  %428 = load float, float* %arrayidx513, align 4, !tbaa !8
  %call514 = call float @mul_float(float 0x3FED906CC0000000, float %428)
  %call515 = call float @sub_float(float %call512, float %call514)
  %call516 = call float @add_float(float %426, float %call515)
  store float %call516, float* %arrayinit.element509, align 4, !tbaa !8
  %429 = bitcast [2 x float]* %w85 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %429) #3
  %arrayinit.begin517 = getelementptr inbounds [2 x float], [2 x float]* %w85, i32 0, i32 0
  %arrayidx518 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 0
  %430 = load float, float* %arrayidx518, align 4, !tbaa !8
  %arrayidx519 = getelementptr inbounds [2 x float], [2 x float]* %w76, i32 0, i32 0
  %431 = load float, float* %arrayidx519, align 4, !tbaa !8
  %call520 = call float @mul_float(float 0x3FD87DE0E0000000, float %431)
  %call521 = call float @sub_float(float 0.000000e+00, float %call520)
  %arrayidx522 = getelementptr inbounds [2 x float], [2 x float]* %w76, i32 0, i32 1
  %432 = load float, float* %arrayidx522, align 4, !tbaa !8
  %call523 = call float @mul_float(float 0x3FED906CC0000000, float %432)
  %call524 = call float @sub_float(float %call521, float %call523)
  %call525 = call float @add_float(float %430, float %call524)
  store float %call525, float* %arrayinit.begin517, align 4, !tbaa !8
  %arrayinit.element526 = getelementptr inbounds float, float* %arrayinit.begin517, i32 1
  %arrayidx527 = getelementptr inbounds [2 x float], [2 x float]* %w52, i32 0, i32 1
  %433 = load float, float* %arrayidx527, align 4, !tbaa !8
  %arrayidx528 = getelementptr inbounds [2 x float], [2 x float]* %w76, i32 0, i32 0
  %434 = load float, float* %arrayidx528, align 4, !tbaa !8
  %call529 = call float @mul_float(float 0x3FED906CC0000000, float %434)
  %arrayidx530 = getelementptr inbounds [2 x float], [2 x float]* %w76, i32 0, i32 1
  %435 = load float, float* %arrayidx530, align 4, !tbaa !8
  %call531 = call float @mul_float(float 0x3FD87DE0E0000000, float %435)
  %call532 = call float @sub_float(float %call529, float %call531)
  %call533 = call float @add_float(float %433, float %call532)
  store float %call533, float* %arrayinit.element526, align 4, !tbaa !8
  %436 = bitcast [2 x float]* %w86 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %436) #3
  %arrayinit.begin534 = getelementptr inbounds [2 x float], [2 x float]* %w86, i32 0, i32 0
  %arrayidx535 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 0
  %437 = load float, float* %arrayidx535, align 4, !tbaa !8
  %arrayidx536 = getelementptr inbounds [2 x float], [2 x float]* %w71, i32 0, i32 1
  %438 = load float, float* %arrayidx536, align 4, !tbaa !8
  %call537 = call float @add_float(float %437, float %438)
  store float %call537, float* %arrayinit.begin534, align 4, !tbaa !8
  %arrayinit.element538 = getelementptr inbounds float, float* %arrayinit.begin534, i32 1
  %arrayidx539 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 1
  %439 = load float, float* %arrayidx539, align 4, !tbaa !8
  %arrayidx540 = getelementptr inbounds [2 x float], [2 x float]* %w71, i32 0, i32 0
  %440 = load float, float* %arrayidx540, align 4, !tbaa !8
  %call541 = call float @sub_float(float %439, float %440)
  store float %call541, float* %arrayinit.element538, align 4, !tbaa !8
  %441 = bitcast [2 x float]* %w87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %441) #3
  %arrayinit.begin542 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 0
  %arrayidx543 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 0
  %442 = load float, float* %arrayidx543, align 4, !tbaa !8
  %arrayidx544 = getelementptr inbounds [2 x float], [2 x float]* %w71, i32 0, i32 1
  %443 = load float, float* %arrayidx544, align 4, !tbaa !8
  %call545 = call float @sub_float(float %442, float %443)
  store float %call545, float* %arrayinit.begin542, align 4, !tbaa !8
  %arrayinit.element546 = getelementptr inbounds float, float* %arrayinit.begin542, i32 1
  %arrayidx547 = getelementptr inbounds [2 x float], [2 x float]* %w47, i32 0, i32 1
  %444 = load float, float* %arrayidx547, align 4, !tbaa !8
  %arrayidx548 = getelementptr inbounds [2 x float], [2 x float]* %w71, i32 0, i32 0
  %445 = load float, float* %arrayidx548, align 4, !tbaa !8
  %call549 = call float @add_float(float %444, float %445)
  store float %call549, float* %arrayinit.element546, align 4, !tbaa !8
  %446 = bitcast [2 x float]* %w88 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %446) #3
  %arrayinit.begin550 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 0
  %arrayidx551 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 0
  %447 = load float, float* %arrayidx551, align 4, !tbaa !8
  %arrayidx552 = getelementptr inbounds [2 x float], [2 x float]* %w73, i32 0, i32 0
  %448 = load float, float* %arrayidx552, align 4, !tbaa !8
  %call553 = call float @mul_float(float 0x3FD87DE0E0000000, float %448)
  %arrayidx554 = getelementptr inbounds [2 x float], [2 x float]* %w73, i32 0, i32 1
  %449 = load float, float* %arrayidx554, align 4, !tbaa !8
  %call555 = call float @mul_float(float 0x3FED906CC0000000, float %449)
  %call556 = call float @sub_float(float %call553, float %call555)
  %call557 = call float @sub_float(float %447, float %call556)
  store float %call557, float* %arrayinit.begin550, align 4, !tbaa !8
  %arrayinit.element558 = getelementptr inbounds float, float* %arrayinit.begin550, i32 1
  %arrayidx559 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 1
  %450 = load float, float* %arrayidx559, align 4, !tbaa !8
  %arrayidx560 = getelementptr inbounds [2 x float], [2 x float]* %w73, i32 0, i32 1
  %451 = load float, float* %arrayidx560, align 4, !tbaa !8
  %call561 = call float @mul_float(float 0x3FD87DE0E0000000, float %451)
  %call562 = call float @sub_float(float 0.000000e+00, float %call561)
  %arrayidx563 = getelementptr inbounds [2 x float], [2 x float]* %w73, i32 0, i32 0
  %452 = load float, float* %arrayidx563, align 4, !tbaa !8
  %call564 = call float @mul_float(float 0x3FED906CC0000000, float %452)
  %call565 = call float @sub_float(float %call562, float %call564)
  %call566 = call float @add_float(float %450, float %call565)
  store float %call566, float* %arrayinit.element558, align 4, !tbaa !8
  %453 = bitcast [2 x float]* %w89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %453) #3
  %arrayinit.begin567 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 0
  %arrayidx568 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 0
  %454 = load float, float* %arrayidx568, align 4, !tbaa !8
  %arrayidx569 = getelementptr inbounds [2 x float], [2 x float]* %w73, i32 0, i32 0
  %455 = load float, float* %arrayidx569, align 4, !tbaa !8
  %call570 = call float @mul_float(float 0x3FD87DE0E0000000, float %455)
  %arrayidx571 = getelementptr inbounds [2 x float], [2 x float]* %w73, i32 0, i32 1
  %456 = load float, float* %arrayidx571, align 4, !tbaa !8
  %call572 = call float @mul_float(float 0x3FED906CC0000000, float %456)
  %call573 = call float @sub_float(float %call570, float %call572)
  %call574 = call float @add_float(float %454, float %call573)
  store float %call574, float* %arrayinit.begin567, align 4, !tbaa !8
  %arrayinit.element575 = getelementptr inbounds float, float* %arrayinit.begin567, i32 1
  %arrayidx576 = getelementptr inbounds [2 x float], [2 x float]* %w49, i32 0, i32 1
  %457 = load float, float* %arrayidx576, align 4, !tbaa !8
  %arrayidx577 = getelementptr inbounds [2 x float], [2 x float]* %w73, i32 0, i32 1
  %458 = load float, float* %arrayidx577, align 4, !tbaa !8
  %call578 = call float @mul_float(float 0x3FD87DE0E0000000, float %458)
  %arrayidx579 = getelementptr inbounds [2 x float], [2 x float]* %w73, i32 0, i32 0
  %459 = load float, float* %arrayidx579, align 4, !tbaa !8
  %call580 = call float @mul_float(float 0x3FED906CC0000000, float %459)
  %call581 = call float @add_float(float %call578, float %call580)
  %call582 = call float @add_float(float %457, float %call581)
  store float %call582, float* %arrayinit.element575, align 4, !tbaa !8
  %460 = bitcast [2 x float]* %w90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %460) #3
  %arrayinit.begin583 = getelementptr inbounds [2 x float], [2 x float]* %w90, i32 0, i32 0
  %arrayidx584 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 0
  %461 = load float, float* %arrayidx584, align 4, !tbaa !8
  %arrayidx585 = getelementptr inbounds [2 x float], [2 x float]* %w75, i32 0, i32 0
  %462 = load float, float* %arrayidx585, align 4, !tbaa !8
  %arrayidx586 = getelementptr inbounds [2 x float], [2 x float]* %w75, i32 0, i32 1
  %463 = load float, float* %arrayidx586, align 4, !tbaa !8
  %call587 = call float @sub_float(float %462, float %463)
  %call588 = call float @mul_float(float 0x3FE6A09EE0000000, float %call587)
  %call589 = call float @sub_float(float %461, float %call588)
  store float %call589, float* %arrayinit.begin583, align 4, !tbaa !8
  %arrayinit.element590 = getelementptr inbounds float, float* %arrayinit.begin583, i32 1
  %arrayidx591 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 1
  %464 = load float, float* %arrayidx591, align 4, !tbaa !8
  %arrayidx592 = getelementptr inbounds [2 x float], [2 x float]* %w75, i32 0, i32 1
  %465 = load float, float* %arrayidx592, align 4, !tbaa !8
  %arrayidx593 = getelementptr inbounds [2 x float], [2 x float]* %w75, i32 0, i32 0
  %466 = load float, float* %arrayidx593, align 4, !tbaa !8
  %call594 = call float @add_float(float %465, float %466)
  %call595 = call float @mul_float(float 0x3FE6A09EE0000000, float %call594)
  %call596 = call float @sub_float(float %464, float %call595)
  store float %call596, float* %arrayinit.element590, align 4, !tbaa !8
  %467 = bitcast [2 x float]* %w91 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %467) #3
  %arrayinit.begin597 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 0
  %arrayidx598 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 0
  %468 = load float, float* %arrayidx598, align 4, !tbaa !8
  %arrayidx599 = getelementptr inbounds [2 x float], [2 x float]* %w75, i32 0, i32 0
  %469 = load float, float* %arrayidx599, align 4, !tbaa !8
  %arrayidx600 = getelementptr inbounds [2 x float], [2 x float]* %w75, i32 0, i32 1
  %470 = load float, float* %arrayidx600, align 4, !tbaa !8
  %call601 = call float @sub_float(float %469, float %470)
  %call602 = call float @mul_float(float 0x3FE6A09EE0000000, float %call601)
  %call603 = call float @add_float(float %468, float %call602)
  store float %call603, float* %arrayinit.begin597, align 4, !tbaa !8
  %arrayinit.element604 = getelementptr inbounds float, float* %arrayinit.begin597, i32 1
  %arrayidx605 = getelementptr inbounds [2 x float], [2 x float]* %w51, i32 0, i32 1
  %471 = load float, float* %arrayidx605, align 4, !tbaa !8
  %arrayidx606 = getelementptr inbounds [2 x float], [2 x float]* %w75, i32 0, i32 1
  %472 = load float, float* %arrayidx606, align 4, !tbaa !8
  %arrayidx607 = getelementptr inbounds [2 x float], [2 x float]* %w75, i32 0, i32 0
  %473 = load float, float* %arrayidx607, align 4, !tbaa !8
  %call608 = call float @add_float(float %472, float %473)
  %call609 = call float @mul_float(float 0x3FE6A09EE0000000, float %call608)
  %call610 = call float @add_float(float %471, float %call609)
  store float %call610, float* %arrayinit.element604, align 4, !tbaa !8
  %474 = bitcast [2 x float]* %w92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %474) #3
  %arrayinit.begin611 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 0
  %arrayidx612 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 0
  %475 = load float, float* %arrayidx612, align 4, !tbaa !8
  %arrayidx613 = getelementptr inbounds [2 x float], [2 x float]* %w77, i32 0, i32 0
  %476 = load float, float* %arrayidx613, align 4, !tbaa !8
  %call614 = call float @mul_float(float 0x3FED906CC0000000, float %476)
  %arrayidx615 = getelementptr inbounds [2 x float], [2 x float]* %w77, i32 0, i32 1
  %477 = load float, float* %arrayidx615, align 4, !tbaa !8
  %call616 = call float @mul_float(float 0x3FD87DE0E0000000, float %477)
  %call617 = call float @sub_float(float %call614, float %call616)
  %call618 = call float @sub_float(float %475, float %call617)
  store float %call618, float* %arrayinit.begin611, align 4, !tbaa !8
  %arrayinit.element619 = getelementptr inbounds float, float* %arrayinit.begin611, i32 1
  %arrayidx620 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 1
  %478 = load float, float* %arrayidx620, align 4, !tbaa !8
  %arrayidx621 = getelementptr inbounds [2 x float], [2 x float]* %w77, i32 0, i32 1
  %479 = load float, float* %arrayidx621, align 4, !tbaa !8
  %call622 = call float @mul_float(float 0x3FED906CC0000000, float %479)
  %call623 = call float @sub_float(float 0.000000e+00, float %call622)
  %arrayidx624 = getelementptr inbounds [2 x float], [2 x float]* %w77, i32 0, i32 0
  %480 = load float, float* %arrayidx624, align 4, !tbaa !8
  %call625 = call float @mul_float(float 0x3FD87DE0E0000000, float %480)
  %call626 = call float @sub_float(float %call623, float %call625)
  %call627 = call float @add_float(float %478, float %call626)
  store float %call627, float* %arrayinit.element619, align 4, !tbaa !8
  %481 = bitcast [2 x float]* %w93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %481) #3
  %arrayinit.begin628 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 0
  %arrayidx629 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 0
  %482 = load float, float* %arrayidx629, align 4, !tbaa !8
  %arrayidx630 = getelementptr inbounds [2 x float], [2 x float]* %w77, i32 0, i32 0
  %483 = load float, float* %arrayidx630, align 4, !tbaa !8
  %call631 = call float @mul_float(float 0x3FED906CC0000000, float %483)
  %arrayidx632 = getelementptr inbounds [2 x float], [2 x float]* %w77, i32 0, i32 1
  %484 = load float, float* %arrayidx632, align 4, !tbaa !8
  %call633 = call float @mul_float(float 0x3FD87DE0E0000000, float %484)
  %call634 = call float @sub_float(float %call631, float %call633)
  %call635 = call float @add_float(float %482, float %call634)
  store float %call635, float* %arrayinit.begin628, align 4, !tbaa !8
  %arrayinit.element636 = getelementptr inbounds float, float* %arrayinit.begin628, i32 1
  %arrayidx637 = getelementptr inbounds [2 x float], [2 x float]* %w53, i32 0, i32 1
  %485 = load float, float* %arrayidx637, align 4, !tbaa !8
  %arrayidx638 = getelementptr inbounds [2 x float], [2 x float]* %w77, i32 0, i32 1
  %486 = load float, float* %arrayidx638, align 4, !tbaa !8
  %call639 = call float @mul_float(float 0x3FED906CC0000000, float %486)
  %arrayidx640 = getelementptr inbounds [2 x float], [2 x float]* %w77, i32 0, i32 0
  %487 = load float, float* %arrayidx640, align 4, !tbaa !8
  %call641 = call float @mul_float(float 0x3FD87DE0E0000000, float %487)
  %call642 = call float @add_float(float %call639, float %call641)
  %call643 = call float @add_float(float %485, float %call642)
  store float %call643, float* %arrayinit.element636, align 4, !tbaa !8
  %488 = bitcast [2 x float]* %w94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %488) #3
  %arrayinit.begin644 = getelementptr inbounds [2 x float], [2 x float]* %w94, i32 0, i32 0
  %489 = load float, float* %i1, align 4, !tbaa !8
  %490 = load float, float* %i15, align 4, !tbaa !8
  %call645 = call float @add_float(float %489, float %490)
  store float %call645, float* %arrayinit.begin644, align 4, !tbaa !8
  %arrayinit.element646 = getelementptr inbounds float, float* %arrayinit.begin644, i32 1
  %491 = load float, float* %i31, align 4, !tbaa !8
  %492 = load float, float* %i17, align 4, !tbaa !8
  %call647 = call float @sub_float(float %491, float %492)
  store float %call647, float* %arrayinit.element646, align 4, !tbaa !8
  %493 = bitcast [2 x float]* %w95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %493) #3
  %arrayinit.begin648 = getelementptr inbounds [2 x float], [2 x float]* %w95, i32 0, i32 0
  %494 = load float, float* %i1, align 4, !tbaa !8
  %495 = load float, float* %i15, align 4, !tbaa !8
  %call649 = call float @sub_float(float %494, float %495)
  store float %call649, float* %arrayinit.begin648, align 4, !tbaa !8
  %arrayinit.element650 = getelementptr inbounds float, float* %arrayinit.begin648, i32 1
  %496 = load float, float* %i17, align 4, !tbaa !8
  %call651 = call float @sub_float(float 0.000000e+00, float %496)
  %497 = load float, float* %i31, align 4, !tbaa !8
  %call652 = call float @sub_float(float %call651, float %497)
  store float %call652, float* %arrayinit.element650, align 4, !tbaa !8
  %498 = bitcast [2 x float]* %w96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %498) #3
  %arrayinit.begin653 = getelementptr inbounds [2 x float], [2 x float]* %w96, i32 0, i32 0
  %499 = load float, float* %i9, align 4, !tbaa !8
  %500 = load float, float* %i7, align 4, !tbaa !8
  %call654 = call float @add_float(float %499, float %500)
  store float %call654, float* %arrayinit.begin653, align 4, !tbaa !8
  %arrayinit.element655 = getelementptr inbounds float, float* %arrayinit.begin653, i32 1
  %501 = load float, float* %i23, align 4, !tbaa !8
  %502 = load float, float* %i25, align 4, !tbaa !8
  %call656 = call float @sub_float(float %501, float %502)
  store float %call656, float* %arrayinit.element655, align 4, !tbaa !8
  %503 = bitcast [2 x float]* %w97 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %503) #3
  %arrayinit.begin657 = getelementptr inbounds [2 x float], [2 x float]* %w97, i32 0, i32 0
  %504 = load float, float* %i9, align 4, !tbaa !8
  %505 = load float, float* %i7, align 4, !tbaa !8
  %call658 = call float @sub_float(float %504, float %505)
  store float %call658, float* %arrayinit.begin657, align 4, !tbaa !8
  %arrayinit.element659 = getelementptr inbounds float, float* %arrayinit.begin657, i32 1
  %506 = load float, float* %i25, align 4, !tbaa !8
  %call660 = call float @sub_float(float 0.000000e+00, float %506)
  %507 = load float, float* %i23, align 4, !tbaa !8
  %call661 = call float @sub_float(float %call660, float %507)
  store float %call661, float* %arrayinit.element659, align 4, !tbaa !8
  %508 = bitcast [2 x float]* %w98 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %508) #3
  %arrayinit.begin662 = getelementptr inbounds [2 x float], [2 x float]* %w98, i32 0, i32 0
  %arrayidx663 = getelementptr inbounds [2 x float], [2 x float]* %w94, i32 0, i32 0
  %509 = load float, float* %arrayidx663, align 4, !tbaa !8
  %arrayidx664 = getelementptr inbounds [2 x float], [2 x float]* %w96, i32 0, i32 0
  %510 = load float, float* %arrayidx664, align 4, !tbaa !8
  %call665 = call float @add_float(float %509, float %510)
  store float %call665, float* %arrayinit.begin662, align 4, !tbaa !8
  %arrayinit.element666 = getelementptr inbounds float, float* %arrayinit.begin662, i32 1
  %arrayidx667 = getelementptr inbounds [2 x float], [2 x float]* %w94, i32 0, i32 1
  %511 = load float, float* %arrayidx667, align 4, !tbaa !8
  %arrayidx668 = getelementptr inbounds [2 x float], [2 x float]* %w96, i32 0, i32 1
  %512 = load float, float* %arrayidx668, align 4, !tbaa !8
  %call669 = call float @add_float(float %511, float %512)
  store float %call669, float* %arrayinit.element666, align 4, !tbaa !8
  %513 = bitcast [2 x float]* %w99 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %513) #3
  %arrayinit.begin670 = getelementptr inbounds [2 x float], [2 x float]* %w99, i32 0, i32 0
  %arrayidx671 = getelementptr inbounds [2 x float], [2 x float]* %w94, i32 0, i32 0
  %514 = load float, float* %arrayidx671, align 4, !tbaa !8
  %arrayidx672 = getelementptr inbounds [2 x float], [2 x float]* %w96, i32 0, i32 0
  %515 = load float, float* %arrayidx672, align 4, !tbaa !8
  %call673 = call float @sub_float(float %514, float %515)
  store float %call673, float* %arrayinit.begin670, align 4, !tbaa !8
  %arrayinit.element674 = getelementptr inbounds float, float* %arrayinit.begin670, i32 1
  %arrayidx675 = getelementptr inbounds [2 x float], [2 x float]* %w94, i32 0, i32 1
  %516 = load float, float* %arrayidx675, align 4, !tbaa !8
  %arrayidx676 = getelementptr inbounds [2 x float], [2 x float]* %w96, i32 0, i32 1
  %517 = load float, float* %arrayidx676, align 4, !tbaa !8
  %call677 = call float @sub_float(float %516, float %517)
  store float %call677, float* %arrayinit.element674, align 4, !tbaa !8
  %518 = bitcast [2 x float]* %w100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %518) #3
  %arrayinit.begin678 = getelementptr inbounds [2 x float], [2 x float]* %w100, i32 0, i32 0
  %arrayidx679 = getelementptr inbounds [2 x float], [2 x float]* %w95, i32 0, i32 0
  %519 = load float, float* %arrayidx679, align 4, !tbaa !8
  %arrayidx680 = getelementptr inbounds [2 x float], [2 x float]* %w97, i32 0, i32 1
  %520 = load float, float* %arrayidx680, align 4, !tbaa !8
  %call681 = call float @add_float(float %519, float %520)
  store float %call681, float* %arrayinit.begin678, align 4, !tbaa !8
  %arrayinit.element682 = getelementptr inbounds float, float* %arrayinit.begin678, i32 1
  %arrayidx683 = getelementptr inbounds [2 x float], [2 x float]* %w95, i32 0, i32 1
  %521 = load float, float* %arrayidx683, align 4, !tbaa !8
  %arrayidx684 = getelementptr inbounds [2 x float], [2 x float]* %w97, i32 0, i32 0
  %522 = load float, float* %arrayidx684, align 4, !tbaa !8
  %call685 = call float @sub_float(float %521, float %522)
  store float %call685, float* %arrayinit.element682, align 4, !tbaa !8
  %523 = bitcast [2 x float]* %w101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %523) #3
  %arrayinit.begin686 = getelementptr inbounds [2 x float], [2 x float]* %w101, i32 0, i32 0
  %arrayidx687 = getelementptr inbounds [2 x float], [2 x float]* %w95, i32 0, i32 0
  %524 = load float, float* %arrayidx687, align 4, !tbaa !8
  %arrayidx688 = getelementptr inbounds [2 x float], [2 x float]* %w97, i32 0, i32 1
  %525 = load float, float* %arrayidx688, align 4, !tbaa !8
  %call689 = call float @sub_float(float %524, float %525)
  store float %call689, float* %arrayinit.begin686, align 4, !tbaa !8
  %arrayinit.element690 = getelementptr inbounds float, float* %arrayinit.begin686, i32 1
  %arrayidx691 = getelementptr inbounds [2 x float], [2 x float]* %w95, i32 0, i32 1
  %526 = load float, float* %arrayidx691, align 4, !tbaa !8
  %arrayidx692 = getelementptr inbounds [2 x float], [2 x float]* %w97, i32 0, i32 0
  %527 = load float, float* %arrayidx692, align 4, !tbaa !8
  %call693 = call float @add_float(float %526, float %527)
  store float %call693, float* %arrayinit.element690, align 4, !tbaa !8
  %528 = bitcast [2 x float]* %w102 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %528) #3
  %arrayinit.begin694 = getelementptr inbounds [2 x float], [2 x float]* %w102, i32 0, i32 0
  %529 = load float, float* %i5, align 4, !tbaa !8
  %530 = load float, float* %i11, align 4, !tbaa !8
  %call695 = call float @add_float(float %529, float %530)
  store float %call695, float* %arrayinit.begin694, align 4, !tbaa !8
  %arrayinit.element696 = getelementptr inbounds float, float* %arrayinit.begin694, i32 1
  %531 = load float, float* %i27, align 4, !tbaa !8
  %532 = load float, float* %i21, align 4, !tbaa !8
  %call697 = call float @sub_float(float %531, float %532)
  store float %call697, float* %arrayinit.element696, align 4, !tbaa !8
  %533 = bitcast [2 x float]* %w103 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %533) #3
  %arrayinit.begin698 = getelementptr inbounds [2 x float], [2 x float]* %w103, i32 0, i32 0
  %534 = load float, float* %i5, align 4, !tbaa !8
  %535 = load float, float* %i11, align 4, !tbaa !8
  %call699 = call float @sub_float(float %534, float %535)
  store float %call699, float* %arrayinit.begin698, align 4, !tbaa !8
  %arrayinit.element700 = getelementptr inbounds float, float* %arrayinit.begin698, i32 1
  %536 = load float, float* %i21, align 4, !tbaa !8
  %call701 = call float @sub_float(float 0.000000e+00, float %536)
  %537 = load float, float* %i27, align 4, !tbaa !8
  %call702 = call float @sub_float(float %call701, float %537)
  store float %call702, float* %arrayinit.element700, align 4, !tbaa !8
  %538 = bitcast [2 x float]* %w104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %538) #3
  %arrayinit.begin703 = getelementptr inbounds [2 x float], [2 x float]* %w104, i32 0, i32 0
  %539 = load float, float* %i13, align 4, !tbaa !8
  %540 = load float, float* %i3, align 4, !tbaa !8
  %call704 = call float @add_float(float %539, float %540)
  store float %call704, float* %arrayinit.begin703, align 4, !tbaa !8
  %arrayinit.element705 = getelementptr inbounds float, float* %arrayinit.begin703, i32 1
  %541 = load float, float* %i19, align 4, !tbaa !8
  %542 = load float, float* %i29, align 4, !tbaa !8
  %call706 = call float @sub_float(float %541, float %542)
  store float %call706, float* %arrayinit.element705, align 4, !tbaa !8
  %543 = bitcast [2 x float]* %w105 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %543) #3
  %arrayinit.begin707 = getelementptr inbounds [2 x float], [2 x float]* %w105, i32 0, i32 0
  %544 = load float, float* %i13, align 4, !tbaa !8
  %545 = load float, float* %i3, align 4, !tbaa !8
  %call708 = call float @sub_float(float %544, float %545)
  store float %call708, float* %arrayinit.begin707, align 4, !tbaa !8
  %arrayinit.element709 = getelementptr inbounds float, float* %arrayinit.begin707, i32 1
  %546 = load float, float* %i29, align 4, !tbaa !8
  %call710 = call float @sub_float(float 0.000000e+00, float %546)
  %547 = load float, float* %i19, align 4, !tbaa !8
  %call711 = call float @sub_float(float %call710, float %547)
  store float %call711, float* %arrayinit.element709, align 4, !tbaa !8
  %548 = bitcast [2 x float]* %w106 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %548) #3
  %arrayinit.begin712 = getelementptr inbounds [2 x float], [2 x float]* %w106, i32 0, i32 0
  %arrayidx713 = getelementptr inbounds [2 x float], [2 x float]* %w102, i32 0, i32 0
  %549 = load float, float* %arrayidx713, align 4, !tbaa !8
  %arrayidx714 = getelementptr inbounds [2 x float], [2 x float]* %w104, i32 0, i32 0
  %550 = load float, float* %arrayidx714, align 4, !tbaa !8
  %call715 = call float @add_float(float %549, float %550)
  store float %call715, float* %arrayinit.begin712, align 4, !tbaa !8
  %arrayinit.element716 = getelementptr inbounds float, float* %arrayinit.begin712, i32 1
  %arrayidx717 = getelementptr inbounds [2 x float], [2 x float]* %w102, i32 0, i32 1
  %551 = load float, float* %arrayidx717, align 4, !tbaa !8
  %arrayidx718 = getelementptr inbounds [2 x float], [2 x float]* %w104, i32 0, i32 1
  %552 = load float, float* %arrayidx718, align 4, !tbaa !8
  %call719 = call float @add_float(float %551, float %552)
  store float %call719, float* %arrayinit.element716, align 4, !tbaa !8
  %553 = bitcast [2 x float]* %w107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %553) #3
  %arrayinit.begin720 = getelementptr inbounds [2 x float], [2 x float]* %w107, i32 0, i32 0
  %arrayidx721 = getelementptr inbounds [2 x float], [2 x float]* %w102, i32 0, i32 0
  %554 = load float, float* %arrayidx721, align 4, !tbaa !8
  %arrayidx722 = getelementptr inbounds [2 x float], [2 x float]* %w104, i32 0, i32 0
  %555 = load float, float* %arrayidx722, align 4, !tbaa !8
  %call723 = call float @sub_float(float %554, float %555)
  store float %call723, float* %arrayinit.begin720, align 4, !tbaa !8
  %arrayinit.element724 = getelementptr inbounds float, float* %arrayinit.begin720, i32 1
  %arrayidx725 = getelementptr inbounds [2 x float], [2 x float]* %w102, i32 0, i32 1
  %556 = load float, float* %arrayidx725, align 4, !tbaa !8
  %arrayidx726 = getelementptr inbounds [2 x float], [2 x float]* %w104, i32 0, i32 1
  %557 = load float, float* %arrayidx726, align 4, !tbaa !8
  %call727 = call float @sub_float(float %556, float %557)
  store float %call727, float* %arrayinit.element724, align 4, !tbaa !8
  %558 = bitcast [2 x float]* %w108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %558) #3
  %arrayinit.begin728 = getelementptr inbounds [2 x float], [2 x float]* %w108, i32 0, i32 0
  %arrayidx729 = getelementptr inbounds [2 x float], [2 x float]* %w103, i32 0, i32 0
  %559 = load float, float* %arrayidx729, align 4, !tbaa !8
  %arrayidx730 = getelementptr inbounds [2 x float], [2 x float]* %w105, i32 0, i32 1
  %560 = load float, float* %arrayidx730, align 4, !tbaa !8
  %call731 = call float @add_float(float %559, float %560)
  store float %call731, float* %arrayinit.begin728, align 4, !tbaa !8
  %arrayinit.element732 = getelementptr inbounds float, float* %arrayinit.begin728, i32 1
  %arrayidx733 = getelementptr inbounds [2 x float], [2 x float]* %w103, i32 0, i32 1
  %561 = load float, float* %arrayidx733, align 4, !tbaa !8
  %arrayidx734 = getelementptr inbounds [2 x float], [2 x float]* %w105, i32 0, i32 0
  %562 = load float, float* %arrayidx734, align 4, !tbaa !8
  %call735 = call float @sub_float(float %561, float %562)
  store float %call735, float* %arrayinit.element732, align 4, !tbaa !8
  %563 = bitcast [2 x float]* %w109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %563) #3
  %arrayinit.begin736 = getelementptr inbounds [2 x float], [2 x float]* %w109, i32 0, i32 0
  %arrayidx737 = getelementptr inbounds [2 x float], [2 x float]* %w103, i32 0, i32 0
  %564 = load float, float* %arrayidx737, align 4, !tbaa !8
  %arrayidx738 = getelementptr inbounds [2 x float], [2 x float]* %w105, i32 0, i32 1
  %565 = load float, float* %arrayidx738, align 4, !tbaa !8
  %call739 = call float @sub_float(float %564, float %565)
  store float %call739, float* %arrayinit.begin736, align 4, !tbaa !8
  %arrayinit.element740 = getelementptr inbounds float, float* %arrayinit.begin736, i32 1
  %arrayidx741 = getelementptr inbounds [2 x float], [2 x float]* %w103, i32 0, i32 1
  %566 = load float, float* %arrayidx741, align 4, !tbaa !8
  %arrayidx742 = getelementptr inbounds [2 x float], [2 x float]* %w105, i32 0, i32 0
  %567 = load float, float* %arrayidx742, align 4, !tbaa !8
  %call743 = call float @add_float(float %566, float %567)
  store float %call743, float* %arrayinit.element740, align 4, !tbaa !8
  %568 = bitcast [2 x float]* %w110 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %568) #3
  %arrayinit.begin744 = getelementptr inbounds [2 x float], [2 x float]* %w110, i32 0, i32 0
  %arrayidx745 = getelementptr inbounds [2 x float], [2 x float]* %w98, i32 0, i32 0
  %569 = load float, float* %arrayidx745, align 4, !tbaa !8
  %arrayidx746 = getelementptr inbounds [2 x float], [2 x float]* %w106, i32 0, i32 0
  %570 = load float, float* %arrayidx746, align 4, !tbaa !8
  %call747 = call float @add_float(float %569, float %570)
  store float %call747, float* %arrayinit.begin744, align 4, !tbaa !8
  %arrayinit.element748 = getelementptr inbounds float, float* %arrayinit.begin744, i32 1
  %arrayidx749 = getelementptr inbounds [2 x float], [2 x float]* %w98, i32 0, i32 1
  %571 = load float, float* %arrayidx749, align 4, !tbaa !8
  %arrayidx750 = getelementptr inbounds [2 x float], [2 x float]* %w106, i32 0, i32 1
  %572 = load float, float* %arrayidx750, align 4, !tbaa !8
  %call751 = call float @add_float(float %571, float %572)
  store float %call751, float* %arrayinit.element748, align 4, !tbaa !8
  %573 = bitcast [2 x float]* %w111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %573) #3
  %arrayinit.begin752 = getelementptr inbounds [2 x float], [2 x float]* %w111, i32 0, i32 0
  %arrayidx753 = getelementptr inbounds [2 x float], [2 x float]* %w98, i32 0, i32 0
  %574 = load float, float* %arrayidx753, align 4, !tbaa !8
  %arrayidx754 = getelementptr inbounds [2 x float], [2 x float]* %w106, i32 0, i32 0
  %575 = load float, float* %arrayidx754, align 4, !tbaa !8
  %call755 = call float @sub_float(float %574, float %575)
  store float %call755, float* %arrayinit.begin752, align 4, !tbaa !8
  %arrayinit.element756 = getelementptr inbounds float, float* %arrayinit.begin752, i32 1
  %arrayidx757 = getelementptr inbounds [2 x float], [2 x float]* %w98, i32 0, i32 1
  %576 = load float, float* %arrayidx757, align 4, !tbaa !8
  %arrayidx758 = getelementptr inbounds [2 x float], [2 x float]* %w106, i32 0, i32 1
  %577 = load float, float* %arrayidx758, align 4, !tbaa !8
  %call759 = call float @sub_float(float %576, float %577)
  store float %call759, float* %arrayinit.element756, align 4, !tbaa !8
  %578 = bitcast [2 x float]* %w112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %578) #3
  %arrayinit.begin760 = getelementptr inbounds [2 x float], [2 x float]* %w112, i32 0, i32 0
  %arrayidx761 = getelementptr inbounds [2 x float], [2 x float]* %w100, i32 0, i32 0
  %579 = load float, float* %arrayidx761, align 4, !tbaa !8
  %arrayidx762 = getelementptr inbounds [2 x float], [2 x float]* %w108, i32 0, i32 0
  %580 = load float, float* %arrayidx762, align 4, !tbaa !8
  %arrayidx763 = getelementptr inbounds [2 x float], [2 x float]* %w108, i32 0, i32 1
  %581 = load float, float* %arrayidx763, align 4, !tbaa !8
  %call764 = call float @add_float(float %580, float %581)
  %call765 = call float @mul_float(float 0x3FE6A09EE0000000, float %call764)
  %call766 = call float @add_float(float %579, float %call765)
  store float %call766, float* %arrayinit.begin760, align 4, !tbaa !8
  %arrayinit.element767 = getelementptr inbounds float, float* %arrayinit.begin760, i32 1
  %arrayidx768 = getelementptr inbounds [2 x float], [2 x float]* %w100, i32 0, i32 1
  %582 = load float, float* %arrayidx768, align 4, !tbaa !8
  %arrayidx769 = getelementptr inbounds [2 x float], [2 x float]* %w108, i32 0, i32 1
  %583 = load float, float* %arrayidx769, align 4, !tbaa !8
  %arrayidx770 = getelementptr inbounds [2 x float], [2 x float]* %w108, i32 0, i32 0
  %584 = load float, float* %arrayidx770, align 4, !tbaa !8
  %call771 = call float @sub_float(float %583, float %584)
  %call772 = call float @mul_float(float 0x3FE6A09EE0000000, float %call771)
  %call773 = call float @add_float(float %582, float %call772)
  store float %call773, float* %arrayinit.element767, align 4, !tbaa !8
  %585 = bitcast [2 x float]* %w113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %585) #3
  %arrayinit.begin774 = getelementptr inbounds [2 x float], [2 x float]* %w113, i32 0, i32 0
  %arrayidx775 = getelementptr inbounds [2 x float], [2 x float]* %w100, i32 0, i32 0
  %586 = load float, float* %arrayidx775, align 4, !tbaa !8
  %arrayidx776 = getelementptr inbounds [2 x float], [2 x float]* %w108, i32 0, i32 0
  %587 = load float, float* %arrayidx776, align 4, !tbaa !8
  %call777 = call float @mul_float(float 0x3FE6A09EE0000000, float %587)
  %call778 = call float @sub_float(float 0.000000e+00, float %call777)
  %arrayidx779 = getelementptr inbounds [2 x float], [2 x float]* %w108, i32 0, i32 1
  %588 = load float, float* %arrayidx779, align 4, !tbaa !8
  %call780 = call float @mul_float(float 0x3FE6A09EE0000000, float %588)
  %call781 = call float @sub_float(float %call778, float %call780)
  %call782 = call float @add_float(float %586, float %call781)
  store float %call782, float* %arrayinit.begin774, align 4, !tbaa !8
  %arrayinit.element783 = getelementptr inbounds float, float* %arrayinit.begin774, i32 1
  %arrayidx784 = getelementptr inbounds [2 x float], [2 x float]* %w100, i32 0, i32 1
  %589 = load float, float* %arrayidx784, align 4, !tbaa !8
  %arrayidx785 = getelementptr inbounds [2 x float], [2 x float]* %w108, i32 0, i32 0
  %590 = load float, float* %arrayidx785, align 4, !tbaa !8
  %arrayidx786 = getelementptr inbounds [2 x float], [2 x float]* %w108, i32 0, i32 1
  %591 = load float, float* %arrayidx786, align 4, !tbaa !8
  %call787 = call float @sub_float(float %590, float %591)
  %call788 = call float @mul_float(float 0x3FE6A09EE0000000, float %call787)
  %call789 = call float @add_float(float %589, float %call788)
  store float %call789, float* %arrayinit.element783, align 4, !tbaa !8
  %592 = bitcast [2 x float]* %w114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %592) #3
  %arrayinit.begin790 = getelementptr inbounds [2 x float], [2 x float]* %w114, i32 0, i32 0
  %arrayidx791 = getelementptr inbounds [2 x float], [2 x float]* %w99, i32 0, i32 0
  %593 = load float, float* %arrayidx791, align 4, !tbaa !8
  %arrayidx792 = getelementptr inbounds [2 x float], [2 x float]* %w107, i32 0, i32 1
  %594 = load float, float* %arrayidx792, align 4, !tbaa !8
  %call793 = call float @add_float(float %593, float %594)
  store float %call793, float* %arrayinit.begin790, align 4, !tbaa !8
  %arrayinit.element794 = getelementptr inbounds float, float* %arrayinit.begin790, i32 1
  %arrayidx795 = getelementptr inbounds [2 x float], [2 x float]* %w99, i32 0, i32 1
  %595 = load float, float* %arrayidx795, align 4, !tbaa !8
  %arrayidx796 = getelementptr inbounds [2 x float], [2 x float]* %w107, i32 0, i32 0
  %596 = load float, float* %arrayidx796, align 4, !tbaa !8
  %call797 = call float @sub_float(float %595, float %596)
  store float %call797, float* %arrayinit.element794, align 4, !tbaa !8
  %597 = bitcast [2 x float]* %w115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %597) #3
  %arrayinit.begin798 = getelementptr inbounds [2 x float], [2 x float]* %w115, i32 0, i32 0
  %arrayidx799 = getelementptr inbounds [2 x float], [2 x float]* %w99, i32 0, i32 0
  %598 = load float, float* %arrayidx799, align 4, !tbaa !8
  %arrayidx800 = getelementptr inbounds [2 x float], [2 x float]* %w107, i32 0, i32 1
  %599 = load float, float* %arrayidx800, align 4, !tbaa !8
  %call801 = call float @sub_float(float %598, float %599)
  store float %call801, float* %arrayinit.begin798, align 4, !tbaa !8
  %arrayinit.element802 = getelementptr inbounds float, float* %arrayinit.begin798, i32 1
  %arrayidx803 = getelementptr inbounds [2 x float], [2 x float]* %w99, i32 0, i32 1
  %600 = load float, float* %arrayidx803, align 4, !tbaa !8
  %arrayidx804 = getelementptr inbounds [2 x float], [2 x float]* %w107, i32 0, i32 0
  %601 = load float, float* %arrayidx804, align 4, !tbaa !8
  %call805 = call float @add_float(float %600, float %601)
  store float %call805, float* %arrayinit.element802, align 4, !tbaa !8
  %602 = bitcast [2 x float]* %w116 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %602) #3
  %arrayinit.begin806 = getelementptr inbounds [2 x float], [2 x float]* %w116, i32 0, i32 0
  %arrayidx807 = getelementptr inbounds [2 x float], [2 x float]* %w101, i32 0, i32 0
  %603 = load float, float* %arrayidx807, align 4, !tbaa !8
  %arrayidx808 = getelementptr inbounds [2 x float], [2 x float]* %w109, i32 0, i32 0
  %604 = load float, float* %arrayidx808, align 4, !tbaa !8
  %arrayidx809 = getelementptr inbounds [2 x float], [2 x float]* %w109, i32 0, i32 1
  %605 = load float, float* %arrayidx809, align 4, !tbaa !8
  %call810 = call float @sub_float(float %604, float %605)
  %call811 = call float @mul_float(float 0x3FE6A09EE0000000, float %call810)
  %call812 = call float @sub_float(float %603, float %call811)
  store float %call812, float* %arrayinit.begin806, align 4, !tbaa !8
  %arrayinit.element813 = getelementptr inbounds float, float* %arrayinit.begin806, i32 1
  %arrayidx814 = getelementptr inbounds [2 x float], [2 x float]* %w101, i32 0, i32 1
  %606 = load float, float* %arrayidx814, align 4, !tbaa !8
  %arrayidx815 = getelementptr inbounds [2 x float], [2 x float]* %w109, i32 0, i32 1
  %607 = load float, float* %arrayidx815, align 4, !tbaa !8
  %arrayidx816 = getelementptr inbounds [2 x float], [2 x float]* %w109, i32 0, i32 0
  %608 = load float, float* %arrayidx816, align 4, !tbaa !8
  %call817 = call float @add_float(float %607, float %608)
  %call818 = call float @mul_float(float 0x3FE6A09EE0000000, float %call817)
  %call819 = call float @sub_float(float %606, float %call818)
  store float %call819, float* %arrayinit.element813, align 4, !tbaa !8
  %609 = bitcast [2 x float]* %w117 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %609) #3
  %arrayinit.begin820 = getelementptr inbounds [2 x float], [2 x float]* %w117, i32 0, i32 0
  %arrayidx821 = getelementptr inbounds [2 x float], [2 x float]* %w101, i32 0, i32 0
  %610 = load float, float* %arrayidx821, align 4, !tbaa !8
  %arrayidx822 = getelementptr inbounds [2 x float], [2 x float]* %w109, i32 0, i32 0
  %611 = load float, float* %arrayidx822, align 4, !tbaa !8
  %arrayidx823 = getelementptr inbounds [2 x float], [2 x float]* %w109, i32 0, i32 1
  %612 = load float, float* %arrayidx823, align 4, !tbaa !8
  %call824 = call float @sub_float(float %611, float %612)
  %call825 = call float @mul_float(float 0x3FE6A09EE0000000, float %call824)
  %call826 = call float @add_float(float %610, float %call825)
  store float %call826, float* %arrayinit.begin820, align 4, !tbaa !8
  %arrayinit.element827 = getelementptr inbounds float, float* %arrayinit.begin820, i32 1
  %arrayidx828 = getelementptr inbounds [2 x float], [2 x float]* %w101, i32 0, i32 1
  %613 = load float, float* %arrayidx828, align 4, !tbaa !8
  %arrayidx829 = getelementptr inbounds [2 x float], [2 x float]* %w109, i32 0, i32 1
  %614 = load float, float* %arrayidx829, align 4, !tbaa !8
  %arrayidx830 = getelementptr inbounds [2 x float], [2 x float]* %w109, i32 0, i32 0
  %615 = load float, float* %arrayidx830, align 4, !tbaa !8
  %call831 = call float @add_float(float %614, float %615)
  %call832 = call float @mul_float(float 0x3FE6A09EE0000000, float %call831)
  %call833 = call float @add_float(float %613, float %call832)
  store float %call833, float* %arrayinit.element827, align 4, !tbaa !8
  %616 = bitcast [2 x float]* %w118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %616) #3
  %arrayinit.begin834 = getelementptr inbounds [2 x float], [2 x float]* %w118, i32 0, i32 0
  %617 = load float, float* %i3, align 4, !tbaa !8
  %618 = load float, float* %i13, align 4, !tbaa !8
  %call835 = call float @add_float(float %617, float %618)
  store float %call835, float* %arrayinit.begin834, align 4, !tbaa !8
  %arrayinit.element836 = getelementptr inbounds float, float* %arrayinit.begin834, i32 1
  %619 = load float, float* %i29, align 4, !tbaa !8
  %620 = load float, float* %i19, align 4, !tbaa !8
  %call837 = call float @sub_float(float %619, float %620)
  store float %call837, float* %arrayinit.element836, align 4, !tbaa !8
  %621 = bitcast [2 x float]* %w119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %621) #3
  %arrayinit.begin838 = getelementptr inbounds [2 x float], [2 x float]* %w119, i32 0, i32 0
  %622 = load float, float* %i3, align 4, !tbaa !8
  %623 = load float, float* %i13, align 4, !tbaa !8
  %call839 = call float @sub_float(float %622, float %623)
  store float %call839, float* %arrayinit.begin838, align 4, !tbaa !8
  %arrayinit.element840 = getelementptr inbounds float, float* %arrayinit.begin838, i32 1
  %624 = load float, float* %i19, align 4, !tbaa !8
  %call841 = call float @sub_float(float 0.000000e+00, float %624)
  %625 = load float, float* %i29, align 4, !tbaa !8
  %call842 = call float @sub_float(float %call841, float %625)
  store float %call842, float* %arrayinit.element840, align 4, !tbaa !8
  %626 = bitcast [2 x float]* %w120 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %626) #3
  %arrayinit.begin843 = getelementptr inbounds [2 x float], [2 x float]* %w120, i32 0, i32 0
  %627 = load float, float* %i11, align 4, !tbaa !8
  %628 = load float, float* %i5, align 4, !tbaa !8
  %call844 = call float @add_float(float %627, float %628)
  store float %call844, float* %arrayinit.begin843, align 4, !tbaa !8
  %arrayinit.element845 = getelementptr inbounds float, float* %arrayinit.begin843, i32 1
  %629 = load float, float* %i21, align 4, !tbaa !8
  %630 = load float, float* %i27, align 4, !tbaa !8
  %call846 = call float @sub_float(float %629, float %630)
  store float %call846, float* %arrayinit.element845, align 4, !tbaa !8
  %631 = bitcast [2 x float]* %w121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %631) #3
  %arrayinit.begin847 = getelementptr inbounds [2 x float], [2 x float]* %w121, i32 0, i32 0
  %632 = load float, float* %i11, align 4, !tbaa !8
  %633 = load float, float* %i5, align 4, !tbaa !8
  %call848 = call float @sub_float(float %632, float %633)
  store float %call848, float* %arrayinit.begin847, align 4, !tbaa !8
  %arrayinit.element849 = getelementptr inbounds float, float* %arrayinit.begin847, i32 1
  %634 = load float, float* %i27, align 4, !tbaa !8
  %call850 = call float @sub_float(float 0.000000e+00, float %634)
  %635 = load float, float* %i21, align 4, !tbaa !8
  %call851 = call float @sub_float(float %call850, float %635)
  store float %call851, float* %arrayinit.element849, align 4, !tbaa !8
  %636 = bitcast [2 x float]* %w122 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %636) #3
  %arrayinit.begin852 = getelementptr inbounds [2 x float], [2 x float]* %w122, i32 0, i32 0
  %arrayidx853 = getelementptr inbounds [2 x float], [2 x float]* %w118, i32 0, i32 0
  %637 = load float, float* %arrayidx853, align 4, !tbaa !8
  %arrayidx854 = getelementptr inbounds [2 x float], [2 x float]* %w120, i32 0, i32 0
  %638 = load float, float* %arrayidx854, align 4, !tbaa !8
  %call855 = call float @add_float(float %637, float %638)
  store float %call855, float* %arrayinit.begin852, align 4, !tbaa !8
  %arrayinit.element856 = getelementptr inbounds float, float* %arrayinit.begin852, i32 1
  %arrayidx857 = getelementptr inbounds [2 x float], [2 x float]* %w118, i32 0, i32 1
  %639 = load float, float* %arrayidx857, align 4, !tbaa !8
  %arrayidx858 = getelementptr inbounds [2 x float], [2 x float]* %w120, i32 0, i32 1
  %640 = load float, float* %arrayidx858, align 4, !tbaa !8
  %call859 = call float @add_float(float %639, float %640)
  store float %call859, float* %arrayinit.element856, align 4, !tbaa !8
  %641 = bitcast [2 x float]* %w123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %641) #3
  %arrayinit.begin860 = getelementptr inbounds [2 x float], [2 x float]* %w123, i32 0, i32 0
  %arrayidx861 = getelementptr inbounds [2 x float], [2 x float]* %w118, i32 0, i32 0
  %642 = load float, float* %arrayidx861, align 4, !tbaa !8
  %arrayidx862 = getelementptr inbounds [2 x float], [2 x float]* %w120, i32 0, i32 0
  %643 = load float, float* %arrayidx862, align 4, !tbaa !8
  %call863 = call float @sub_float(float %642, float %643)
  store float %call863, float* %arrayinit.begin860, align 4, !tbaa !8
  %arrayinit.element864 = getelementptr inbounds float, float* %arrayinit.begin860, i32 1
  %arrayidx865 = getelementptr inbounds [2 x float], [2 x float]* %w118, i32 0, i32 1
  %644 = load float, float* %arrayidx865, align 4, !tbaa !8
  %arrayidx866 = getelementptr inbounds [2 x float], [2 x float]* %w120, i32 0, i32 1
  %645 = load float, float* %arrayidx866, align 4, !tbaa !8
  %call867 = call float @sub_float(float %644, float %645)
  store float %call867, float* %arrayinit.element864, align 4, !tbaa !8
  %646 = bitcast [2 x float]* %w124 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %646) #3
  %arrayinit.begin868 = getelementptr inbounds [2 x float], [2 x float]* %w124, i32 0, i32 0
  %arrayidx869 = getelementptr inbounds [2 x float], [2 x float]* %w119, i32 0, i32 0
  %647 = load float, float* %arrayidx869, align 4, !tbaa !8
  %arrayidx870 = getelementptr inbounds [2 x float], [2 x float]* %w121, i32 0, i32 1
  %648 = load float, float* %arrayidx870, align 4, !tbaa !8
  %call871 = call float @add_float(float %647, float %648)
  store float %call871, float* %arrayinit.begin868, align 4, !tbaa !8
  %arrayinit.element872 = getelementptr inbounds float, float* %arrayinit.begin868, i32 1
  %arrayidx873 = getelementptr inbounds [2 x float], [2 x float]* %w119, i32 0, i32 1
  %649 = load float, float* %arrayidx873, align 4, !tbaa !8
  %arrayidx874 = getelementptr inbounds [2 x float], [2 x float]* %w121, i32 0, i32 0
  %650 = load float, float* %arrayidx874, align 4, !tbaa !8
  %call875 = call float @sub_float(float %649, float %650)
  store float %call875, float* %arrayinit.element872, align 4, !tbaa !8
  %651 = bitcast [2 x float]* %w125 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %651) #3
  %arrayinit.begin876 = getelementptr inbounds [2 x float], [2 x float]* %w125, i32 0, i32 0
  %arrayidx877 = getelementptr inbounds [2 x float], [2 x float]* %w119, i32 0, i32 0
  %652 = load float, float* %arrayidx877, align 4, !tbaa !8
  %arrayidx878 = getelementptr inbounds [2 x float], [2 x float]* %w121, i32 0, i32 1
  %653 = load float, float* %arrayidx878, align 4, !tbaa !8
  %call879 = call float @sub_float(float %652, float %653)
  store float %call879, float* %arrayinit.begin876, align 4, !tbaa !8
  %arrayinit.element880 = getelementptr inbounds float, float* %arrayinit.begin876, i32 1
  %arrayidx881 = getelementptr inbounds [2 x float], [2 x float]* %w119, i32 0, i32 1
  %654 = load float, float* %arrayidx881, align 4, !tbaa !8
  %arrayidx882 = getelementptr inbounds [2 x float], [2 x float]* %w121, i32 0, i32 0
  %655 = load float, float* %arrayidx882, align 4, !tbaa !8
  %call883 = call float @add_float(float %654, float %655)
  store float %call883, float* %arrayinit.element880, align 4, !tbaa !8
  %656 = bitcast [2 x float]* %w126 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %656) #3
  %arrayinit.begin884 = getelementptr inbounds [2 x float], [2 x float]* %w126, i32 0, i32 0
  %657 = load float, float* %i7, align 4, !tbaa !8
  %658 = load float, float* %i9, align 4, !tbaa !8
  %call885 = call float @add_float(float %657, float %658)
  store float %call885, float* %arrayinit.begin884, align 4, !tbaa !8
  %arrayinit.element886 = getelementptr inbounds float, float* %arrayinit.begin884, i32 1
  %659 = load float, float* %i25, align 4, !tbaa !8
  %660 = load float, float* %i23, align 4, !tbaa !8
  %call887 = call float @sub_float(float %659, float %660)
  store float %call887, float* %arrayinit.element886, align 4, !tbaa !8
  %661 = bitcast [2 x float]* %w127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %661) #3
  %arrayinit.begin888 = getelementptr inbounds [2 x float], [2 x float]* %w127, i32 0, i32 0
  %662 = load float, float* %i7, align 4, !tbaa !8
  %663 = load float, float* %i9, align 4, !tbaa !8
  %call889 = call float @sub_float(float %662, float %663)
  store float %call889, float* %arrayinit.begin888, align 4, !tbaa !8
  %arrayinit.element890 = getelementptr inbounds float, float* %arrayinit.begin888, i32 1
  %664 = load float, float* %i23, align 4, !tbaa !8
  %call891 = call float @sub_float(float 0.000000e+00, float %664)
  %665 = load float, float* %i25, align 4, !tbaa !8
  %call892 = call float @sub_float(float %call891, float %665)
  store float %call892, float* %arrayinit.element890, align 4, !tbaa !8
  %666 = bitcast [2 x float]* %w128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %666) #3
  %arrayinit.begin893 = getelementptr inbounds [2 x float], [2 x float]* %w128, i32 0, i32 0
  %667 = load float, float* %i15, align 4, !tbaa !8
  %668 = load float, float* %i1, align 4, !tbaa !8
  %call894 = call float @add_float(float %667, float %668)
  store float %call894, float* %arrayinit.begin893, align 4, !tbaa !8
  %arrayinit.element895 = getelementptr inbounds float, float* %arrayinit.begin893, i32 1
  %669 = load float, float* %i17, align 4, !tbaa !8
  %670 = load float, float* %i31, align 4, !tbaa !8
  %call896 = call float @sub_float(float %669, float %670)
  store float %call896, float* %arrayinit.element895, align 4, !tbaa !8
  %671 = bitcast [2 x float]* %w129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %671) #3
  %arrayinit.begin897 = getelementptr inbounds [2 x float], [2 x float]* %w129, i32 0, i32 0
  %672 = load float, float* %i15, align 4, !tbaa !8
  %673 = load float, float* %i1, align 4, !tbaa !8
  %call898 = call float @sub_float(float %672, float %673)
  store float %call898, float* %arrayinit.begin897, align 4, !tbaa !8
  %arrayinit.element899 = getelementptr inbounds float, float* %arrayinit.begin897, i32 1
  %674 = load float, float* %i31, align 4, !tbaa !8
  %call900 = call float @sub_float(float 0.000000e+00, float %674)
  %675 = load float, float* %i17, align 4, !tbaa !8
  %call901 = call float @sub_float(float %call900, float %675)
  store float %call901, float* %arrayinit.element899, align 4, !tbaa !8
  %676 = bitcast [2 x float]* %w130 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %676) #3
  %arrayinit.begin902 = getelementptr inbounds [2 x float], [2 x float]* %w130, i32 0, i32 0
  %arrayidx903 = getelementptr inbounds [2 x float], [2 x float]* %w126, i32 0, i32 0
  %677 = load float, float* %arrayidx903, align 4, !tbaa !8
  %arrayidx904 = getelementptr inbounds [2 x float], [2 x float]* %w128, i32 0, i32 0
  %678 = load float, float* %arrayidx904, align 4, !tbaa !8
  %call905 = call float @add_float(float %677, float %678)
  store float %call905, float* %arrayinit.begin902, align 4, !tbaa !8
  %arrayinit.element906 = getelementptr inbounds float, float* %arrayinit.begin902, i32 1
  %arrayidx907 = getelementptr inbounds [2 x float], [2 x float]* %w126, i32 0, i32 1
  %679 = load float, float* %arrayidx907, align 4, !tbaa !8
  %arrayidx908 = getelementptr inbounds [2 x float], [2 x float]* %w128, i32 0, i32 1
  %680 = load float, float* %arrayidx908, align 4, !tbaa !8
  %call909 = call float @add_float(float %679, float %680)
  store float %call909, float* %arrayinit.element906, align 4, !tbaa !8
  %681 = bitcast [2 x float]* %w131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %681) #3
  %arrayinit.begin910 = getelementptr inbounds [2 x float], [2 x float]* %w131, i32 0, i32 0
  %arrayidx911 = getelementptr inbounds [2 x float], [2 x float]* %w126, i32 0, i32 0
  %682 = load float, float* %arrayidx911, align 4, !tbaa !8
  %arrayidx912 = getelementptr inbounds [2 x float], [2 x float]* %w128, i32 0, i32 0
  %683 = load float, float* %arrayidx912, align 4, !tbaa !8
  %call913 = call float @sub_float(float %682, float %683)
  store float %call913, float* %arrayinit.begin910, align 4, !tbaa !8
  %arrayinit.element914 = getelementptr inbounds float, float* %arrayinit.begin910, i32 1
  %arrayidx915 = getelementptr inbounds [2 x float], [2 x float]* %w126, i32 0, i32 1
  %684 = load float, float* %arrayidx915, align 4, !tbaa !8
  %arrayidx916 = getelementptr inbounds [2 x float], [2 x float]* %w128, i32 0, i32 1
  %685 = load float, float* %arrayidx916, align 4, !tbaa !8
  %call917 = call float @sub_float(float %684, float %685)
  store float %call917, float* %arrayinit.element914, align 4, !tbaa !8
  %686 = bitcast [2 x float]* %w132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %686) #3
  %arrayinit.begin918 = getelementptr inbounds [2 x float], [2 x float]* %w132, i32 0, i32 0
  %arrayidx919 = getelementptr inbounds [2 x float], [2 x float]* %w127, i32 0, i32 0
  %687 = load float, float* %arrayidx919, align 4, !tbaa !8
  %arrayidx920 = getelementptr inbounds [2 x float], [2 x float]* %w129, i32 0, i32 1
  %688 = load float, float* %arrayidx920, align 4, !tbaa !8
  %call921 = call float @add_float(float %687, float %688)
  store float %call921, float* %arrayinit.begin918, align 4, !tbaa !8
  %arrayinit.element922 = getelementptr inbounds float, float* %arrayinit.begin918, i32 1
  %arrayidx923 = getelementptr inbounds [2 x float], [2 x float]* %w127, i32 0, i32 1
  %689 = load float, float* %arrayidx923, align 4, !tbaa !8
  %arrayidx924 = getelementptr inbounds [2 x float], [2 x float]* %w129, i32 0, i32 0
  %690 = load float, float* %arrayidx924, align 4, !tbaa !8
  %call925 = call float @sub_float(float %689, float %690)
  store float %call925, float* %arrayinit.element922, align 4, !tbaa !8
  %691 = bitcast [2 x float]* %w133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %691) #3
  %arrayinit.begin926 = getelementptr inbounds [2 x float], [2 x float]* %w133, i32 0, i32 0
  %arrayidx927 = getelementptr inbounds [2 x float], [2 x float]* %w127, i32 0, i32 0
  %692 = load float, float* %arrayidx927, align 4, !tbaa !8
  %arrayidx928 = getelementptr inbounds [2 x float], [2 x float]* %w129, i32 0, i32 1
  %693 = load float, float* %arrayidx928, align 4, !tbaa !8
  %call929 = call float @sub_float(float %692, float %693)
  store float %call929, float* %arrayinit.begin926, align 4, !tbaa !8
  %arrayinit.element930 = getelementptr inbounds float, float* %arrayinit.begin926, i32 1
  %arrayidx931 = getelementptr inbounds [2 x float], [2 x float]* %w127, i32 0, i32 1
  %694 = load float, float* %arrayidx931, align 4, !tbaa !8
  %arrayidx932 = getelementptr inbounds [2 x float], [2 x float]* %w129, i32 0, i32 0
  %695 = load float, float* %arrayidx932, align 4, !tbaa !8
  %call933 = call float @add_float(float %694, float %695)
  store float %call933, float* %arrayinit.element930, align 4, !tbaa !8
  %696 = bitcast [2 x float]* %w134 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %696) #3
  %arrayinit.begin934 = getelementptr inbounds [2 x float], [2 x float]* %w134, i32 0, i32 0
  %arrayidx935 = getelementptr inbounds [2 x float], [2 x float]* %w122, i32 0, i32 0
  %697 = load float, float* %arrayidx935, align 4, !tbaa !8
  %arrayidx936 = getelementptr inbounds [2 x float], [2 x float]* %w130, i32 0, i32 0
  %698 = load float, float* %arrayidx936, align 4, !tbaa !8
  %call937 = call float @add_float(float %697, float %698)
  store float %call937, float* %arrayinit.begin934, align 4, !tbaa !8
  %arrayinit.element938 = getelementptr inbounds float, float* %arrayinit.begin934, i32 1
  %arrayidx939 = getelementptr inbounds [2 x float], [2 x float]* %w122, i32 0, i32 1
  %699 = load float, float* %arrayidx939, align 4, !tbaa !8
  %arrayidx940 = getelementptr inbounds [2 x float], [2 x float]* %w130, i32 0, i32 1
  %700 = load float, float* %arrayidx940, align 4, !tbaa !8
  %call941 = call float @add_float(float %699, float %700)
  store float %call941, float* %arrayinit.element938, align 4, !tbaa !8
  %701 = bitcast [2 x float]* %w135 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %701) #3
  %arrayinit.begin942 = getelementptr inbounds [2 x float], [2 x float]* %w135, i32 0, i32 0
  %arrayidx943 = getelementptr inbounds [2 x float], [2 x float]* %w122, i32 0, i32 0
  %702 = load float, float* %arrayidx943, align 4, !tbaa !8
  %arrayidx944 = getelementptr inbounds [2 x float], [2 x float]* %w130, i32 0, i32 0
  %703 = load float, float* %arrayidx944, align 4, !tbaa !8
  %call945 = call float @sub_float(float %702, float %703)
  store float %call945, float* %arrayinit.begin942, align 4, !tbaa !8
  %arrayinit.element946 = getelementptr inbounds float, float* %arrayinit.begin942, i32 1
  %arrayidx947 = getelementptr inbounds [2 x float], [2 x float]* %w122, i32 0, i32 1
  %704 = load float, float* %arrayidx947, align 4, !tbaa !8
  %arrayidx948 = getelementptr inbounds [2 x float], [2 x float]* %w130, i32 0, i32 1
  %705 = load float, float* %arrayidx948, align 4, !tbaa !8
  %call949 = call float @sub_float(float %704, float %705)
  store float %call949, float* %arrayinit.element946, align 4, !tbaa !8
  %706 = bitcast [2 x float]* %w136 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %706) #3
  %arrayinit.begin950 = getelementptr inbounds [2 x float], [2 x float]* %w136, i32 0, i32 0
  %arrayidx951 = getelementptr inbounds [2 x float], [2 x float]* %w124, i32 0, i32 0
  %707 = load float, float* %arrayidx951, align 4, !tbaa !8
  %arrayidx952 = getelementptr inbounds [2 x float], [2 x float]* %w132, i32 0, i32 0
  %708 = load float, float* %arrayidx952, align 4, !tbaa !8
  %arrayidx953 = getelementptr inbounds [2 x float], [2 x float]* %w132, i32 0, i32 1
  %709 = load float, float* %arrayidx953, align 4, !tbaa !8
  %call954 = call float @add_float(float %708, float %709)
  %call955 = call float @mul_float(float 0x3FE6A09EE0000000, float %call954)
  %call956 = call float @add_float(float %707, float %call955)
  store float %call956, float* %arrayinit.begin950, align 4, !tbaa !8
  %arrayinit.element957 = getelementptr inbounds float, float* %arrayinit.begin950, i32 1
  %arrayidx958 = getelementptr inbounds [2 x float], [2 x float]* %w124, i32 0, i32 1
  %710 = load float, float* %arrayidx958, align 4, !tbaa !8
  %arrayidx959 = getelementptr inbounds [2 x float], [2 x float]* %w132, i32 0, i32 1
  %711 = load float, float* %arrayidx959, align 4, !tbaa !8
  %arrayidx960 = getelementptr inbounds [2 x float], [2 x float]* %w132, i32 0, i32 0
  %712 = load float, float* %arrayidx960, align 4, !tbaa !8
  %call961 = call float @sub_float(float %711, float %712)
  %call962 = call float @mul_float(float 0x3FE6A09EE0000000, float %call961)
  %call963 = call float @add_float(float %710, float %call962)
  store float %call963, float* %arrayinit.element957, align 4, !tbaa !8
  %713 = bitcast [2 x float]* %w137 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %713) #3
  %arrayinit.begin964 = getelementptr inbounds [2 x float], [2 x float]* %w137, i32 0, i32 0
  %arrayidx965 = getelementptr inbounds [2 x float], [2 x float]* %w124, i32 0, i32 0
  %714 = load float, float* %arrayidx965, align 4, !tbaa !8
  %arrayidx966 = getelementptr inbounds [2 x float], [2 x float]* %w132, i32 0, i32 0
  %715 = load float, float* %arrayidx966, align 4, !tbaa !8
  %call967 = call float @mul_float(float 0x3FE6A09EE0000000, float %715)
  %call968 = call float @sub_float(float 0.000000e+00, float %call967)
  %arrayidx969 = getelementptr inbounds [2 x float], [2 x float]* %w132, i32 0, i32 1
  %716 = load float, float* %arrayidx969, align 4, !tbaa !8
  %call970 = call float @mul_float(float 0x3FE6A09EE0000000, float %716)
  %call971 = call float @sub_float(float %call968, float %call970)
  %call972 = call float @add_float(float %714, float %call971)
  store float %call972, float* %arrayinit.begin964, align 4, !tbaa !8
  %arrayinit.element973 = getelementptr inbounds float, float* %arrayinit.begin964, i32 1
  %arrayidx974 = getelementptr inbounds [2 x float], [2 x float]* %w124, i32 0, i32 1
  %717 = load float, float* %arrayidx974, align 4, !tbaa !8
  %arrayidx975 = getelementptr inbounds [2 x float], [2 x float]* %w132, i32 0, i32 0
  %718 = load float, float* %arrayidx975, align 4, !tbaa !8
  %arrayidx976 = getelementptr inbounds [2 x float], [2 x float]* %w132, i32 0, i32 1
  %719 = load float, float* %arrayidx976, align 4, !tbaa !8
  %call977 = call float @sub_float(float %718, float %719)
  %call978 = call float @mul_float(float 0x3FE6A09EE0000000, float %call977)
  %call979 = call float @add_float(float %717, float %call978)
  store float %call979, float* %arrayinit.element973, align 4, !tbaa !8
  %720 = bitcast [2 x float]* %w138 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %720) #3
  %arrayinit.begin980 = getelementptr inbounds [2 x float], [2 x float]* %w138, i32 0, i32 0
  %arrayidx981 = getelementptr inbounds [2 x float], [2 x float]* %w123, i32 0, i32 0
  %721 = load float, float* %arrayidx981, align 4, !tbaa !8
  %arrayidx982 = getelementptr inbounds [2 x float], [2 x float]* %w131, i32 0, i32 1
  %722 = load float, float* %arrayidx982, align 4, !tbaa !8
  %call983 = call float @add_float(float %721, float %722)
  store float %call983, float* %arrayinit.begin980, align 4, !tbaa !8
  %arrayinit.element984 = getelementptr inbounds float, float* %arrayinit.begin980, i32 1
  %arrayidx985 = getelementptr inbounds [2 x float], [2 x float]* %w123, i32 0, i32 1
  %723 = load float, float* %arrayidx985, align 4, !tbaa !8
  %arrayidx986 = getelementptr inbounds [2 x float], [2 x float]* %w131, i32 0, i32 0
  %724 = load float, float* %arrayidx986, align 4, !tbaa !8
  %call987 = call float @sub_float(float %723, float %724)
  store float %call987, float* %arrayinit.element984, align 4, !tbaa !8
  %725 = bitcast [2 x float]* %w139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %725) #3
  %arrayinit.begin988 = getelementptr inbounds [2 x float], [2 x float]* %w139, i32 0, i32 0
  %arrayidx989 = getelementptr inbounds [2 x float], [2 x float]* %w123, i32 0, i32 0
  %726 = load float, float* %arrayidx989, align 4, !tbaa !8
  %arrayidx990 = getelementptr inbounds [2 x float], [2 x float]* %w131, i32 0, i32 1
  %727 = load float, float* %arrayidx990, align 4, !tbaa !8
  %call991 = call float @sub_float(float %726, float %727)
  store float %call991, float* %arrayinit.begin988, align 4, !tbaa !8
  %arrayinit.element992 = getelementptr inbounds float, float* %arrayinit.begin988, i32 1
  %arrayidx993 = getelementptr inbounds [2 x float], [2 x float]* %w123, i32 0, i32 1
  %728 = load float, float* %arrayidx993, align 4, !tbaa !8
  %arrayidx994 = getelementptr inbounds [2 x float], [2 x float]* %w131, i32 0, i32 0
  %729 = load float, float* %arrayidx994, align 4, !tbaa !8
  %call995 = call float @add_float(float %728, float %729)
  store float %call995, float* %arrayinit.element992, align 4, !tbaa !8
  %730 = bitcast [2 x float]* %w140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %730) #3
  %arrayinit.begin996 = getelementptr inbounds [2 x float], [2 x float]* %w140, i32 0, i32 0
  %arrayidx997 = getelementptr inbounds [2 x float], [2 x float]* %w125, i32 0, i32 0
  %731 = load float, float* %arrayidx997, align 4, !tbaa !8
  %arrayidx998 = getelementptr inbounds [2 x float], [2 x float]* %w133, i32 0, i32 0
  %732 = load float, float* %arrayidx998, align 4, !tbaa !8
  %arrayidx999 = getelementptr inbounds [2 x float], [2 x float]* %w133, i32 0, i32 1
  %733 = load float, float* %arrayidx999, align 4, !tbaa !8
  %call1000 = call float @sub_float(float %732, float %733)
  %call1001 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1000)
  %call1002 = call float @sub_float(float %731, float %call1001)
  store float %call1002, float* %arrayinit.begin996, align 4, !tbaa !8
  %arrayinit.element1003 = getelementptr inbounds float, float* %arrayinit.begin996, i32 1
  %arrayidx1004 = getelementptr inbounds [2 x float], [2 x float]* %w125, i32 0, i32 1
  %734 = load float, float* %arrayidx1004, align 4, !tbaa !8
  %arrayidx1005 = getelementptr inbounds [2 x float], [2 x float]* %w133, i32 0, i32 1
  %735 = load float, float* %arrayidx1005, align 4, !tbaa !8
  %arrayidx1006 = getelementptr inbounds [2 x float], [2 x float]* %w133, i32 0, i32 0
  %736 = load float, float* %arrayidx1006, align 4, !tbaa !8
  %call1007 = call float @add_float(float %735, float %736)
  %call1008 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1007)
  %call1009 = call float @sub_float(float %734, float %call1008)
  store float %call1009, float* %arrayinit.element1003, align 4, !tbaa !8
  %737 = bitcast [2 x float]* %w141 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %737) #3
  %arrayinit.begin1010 = getelementptr inbounds [2 x float], [2 x float]* %w141, i32 0, i32 0
  %arrayidx1011 = getelementptr inbounds [2 x float], [2 x float]* %w125, i32 0, i32 0
  %738 = load float, float* %arrayidx1011, align 4, !tbaa !8
  %arrayidx1012 = getelementptr inbounds [2 x float], [2 x float]* %w133, i32 0, i32 0
  %739 = load float, float* %arrayidx1012, align 4, !tbaa !8
  %arrayidx1013 = getelementptr inbounds [2 x float], [2 x float]* %w133, i32 0, i32 1
  %740 = load float, float* %arrayidx1013, align 4, !tbaa !8
  %call1014 = call float @sub_float(float %739, float %740)
  %call1015 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1014)
  %call1016 = call float @add_float(float %738, float %call1015)
  store float %call1016, float* %arrayinit.begin1010, align 4, !tbaa !8
  %arrayinit.element1017 = getelementptr inbounds float, float* %arrayinit.begin1010, i32 1
  %arrayidx1018 = getelementptr inbounds [2 x float], [2 x float]* %w125, i32 0, i32 1
  %741 = load float, float* %arrayidx1018, align 4, !tbaa !8
  %arrayidx1019 = getelementptr inbounds [2 x float], [2 x float]* %w133, i32 0, i32 1
  %742 = load float, float* %arrayidx1019, align 4, !tbaa !8
  %arrayidx1020 = getelementptr inbounds [2 x float], [2 x float]* %w133, i32 0, i32 0
  %743 = load float, float* %arrayidx1020, align 4, !tbaa !8
  %call1021 = call float @add_float(float %742, float %743)
  %call1022 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1021)
  %call1023 = call float @add_float(float %741, float %call1022)
  store float %call1023, float* %arrayinit.element1017, align 4, !tbaa !8
  %744 = bitcast [2 x float]* %w142 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %744) #3
  %arrayinit.begin1024 = getelementptr inbounds [2 x float], [2 x float]* %w142, i32 0, i32 0
  %arrayidx1025 = getelementptr inbounds [2 x float], [2 x float]* %w110, i32 0, i32 0
  %745 = load float, float* %arrayidx1025, align 4, !tbaa !8
  %arrayidx1026 = getelementptr inbounds [2 x float], [2 x float]* %w134, i32 0, i32 0
  %746 = load float, float* %arrayidx1026, align 4, !tbaa !8
  %call1027 = call float @add_float(float %745, float %746)
  store float %call1027, float* %arrayinit.begin1024, align 4, !tbaa !8
  %arrayinit.element1028 = getelementptr inbounds float, float* %arrayinit.begin1024, i32 1
  %arrayidx1029 = getelementptr inbounds [2 x float], [2 x float]* %w110, i32 0, i32 1
  %747 = load float, float* %arrayidx1029, align 4, !tbaa !8
  %arrayidx1030 = getelementptr inbounds [2 x float], [2 x float]* %w134, i32 0, i32 1
  %748 = load float, float* %arrayidx1030, align 4, !tbaa !8
  %call1031 = call float @add_float(float %747, float %748)
  store float %call1031, float* %arrayinit.element1028, align 4, !tbaa !8
  %749 = bitcast [2 x float]* %w143 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %749) #3
  %arrayinit.begin1032 = getelementptr inbounds [2 x float], [2 x float]* %w143, i32 0, i32 0
  %arrayidx1033 = getelementptr inbounds [2 x float], [2 x float]* %w110, i32 0, i32 0
  %750 = load float, float* %arrayidx1033, align 4, !tbaa !8
  %arrayidx1034 = getelementptr inbounds [2 x float], [2 x float]* %w134, i32 0, i32 0
  %751 = load float, float* %arrayidx1034, align 4, !tbaa !8
  %call1035 = call float @sub_float(float %750, float %751)
  store float %call1035, float* %arrayinit.begin1032, align 4, !tbaa !8
  %arrayinit.element1036 = getelementptr inbounds float, float* %arrayinit.begin1032, i32 1
  %arrayidx1037 = getelementptr inbounds [2 x float], [2 x float]* %w110, i32 0, i32 1
  %752 = load float, float* %arrayidx1037, align 4, !tbaa !8
  %arrayidx1038 = getelementptr inbounds [2 x float], [2 x float]* %w134, i32 0, i32 1
  %753 = load float, float* %arrayidx1038, align 4, !tbaa !8
  %call1039 = call float @sub_float(float %752, float %753)
  store float %call1039, float* %arrayinit.element1036, align 4, !tbaa !8
  %754 = bitcast [2 x float]* %w144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %754) #3
  %arrayinit.begin1040 = getelementptr inbounds [2 x float], [2 x float]* %w144, i32 0, i32 0
  %arrayidx1041 = getelementptr inbounds [2 x float], [2 x float]* %w112, i32 0, i32 0
  %755 = load float, float* %arrayidx1041, align 4, !tbaa !8
  %arrayidx1042 = getelementptr inbounds [2 x float], [2 x float]* %w136, i32 0, i32 0
  %756 = load float, float* %arrayidx1042, align 4, !tbaa !8
  %call1043 = call float @mul_float(float 0x3FED906CC0000000, float %756)
  %arrayidx1044 = getelementptr inbounds [2 x float], [2 x float]* %w136, i32 0, i32 1
  %757 = load float, float* %arrayidx1044, align 4, !tbaa !8
  %call1045 = call float @mul_float(float 0x3FD87DE0E0000000, float %757)
  %call1046 = call float @add_float(float %call1043, float %call1045)
  %call1047 = call float @add_float(float %755, float %call1046)
  store float %call1047, float* %arrayinit.begin1040, align 4, !tbaa !8
  %arrayinit.element1048 = getelementptr inbounds float, float* %arrayinit.begin1040, i32 1
  %arrayidx1049 = getelementptr inbounds [2 x float], [2 x float]* %w112, i32 0, i32 1
  %758 = load float, float* %arrayidx1049, align 4, !tbaa !8
  %arrayidx1050 = getelementptr inbounds [2 x float], [2 x float]* %w136, i32 0, i32 1
  %759 = load float, float* %arrayidx1050, align 4, !tbaa !8
  %call1051 = call float @mul_float(float 0x3FED906CC0000000, float %759)
  %arrayidx1052 = getelementptr inbounds [2 x float], [2 x float]* %w136, i32 0, i32 0
  %760 = load float, float* %arrayidx1052, align 4, !tbaa !8
  %call1053 = call float @mul_float(float 0x3FD87DE0E0000000, float %760)
  %call1054 = call float @sub_float(float %call1051, float %call1053)
  %call1055 = call float @add_float(float %758, float %call1054)
  store float %call1055, float* %arrayinit.element1048, align 4, !tbaa !8
  %761 = bitcast [2 x float]* %w145 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %761) #3
  %arrayinit.begin1056 = getelementptr inbounds [2 x float], [2 x float]* %w145, i32 0, i32 0
  %arrayidx1057 = getelementptr inbounds [2 x float], [2 x float]* %w112, i32 0, i32 0
  %762 = load float, float* %arrayidx1057, align 4, !tbaa !8
  %arrayidx1058 = getelementptr inbounds [2 x float], [2 x float]* %w136, i32 0, i32 0
  %763 = load float, float* %arrayidx1058, align 4, !tbaa !8
  %call1059 = call float @mul_float(float 0x3FED906CC0000000, float %763)
  %call1060 = call float @sub_float(float 0.000000e+00, float %call1059)
  %arrayidx1061 = getelementptr inbounds [2 x float], [2 x float]* %w136, i32 0, i32 1
  %764 = load float, float* %arrayidx1061, align 4, !tbaa !8
  %call1062 = call float @mul_float(float 0x3FD87DE0E0000000, float %764)
  %call1063 = call float @sub_float(float %call1060, float %call1062)
  %call1064 = call float @add_float(float %762, float %call1063)
  store float %call1064, float* %arrayinit.begin1056, align 4, !tbaa !8
  %arrayinit.element1065 = getelementptr inbounds float, float* %arrayinit.begin1056, i32 1
  %arrayidx1066 = getelementptr inbounds [2 x float], [2 x float]* %w112, i32 0, i32 1
  %765 = load float, float* %arrayidx1066, align 4, !tbaa !8
  %arrayidx1067 = getelementptr inbounds [2 x float], [2 x float]* %w136, i32 0, i32 0
  %766 = load float, float* %arrayidx1067, align 4, !tbaa !8
  %call1068 = call float @mul_float(float 0x3FD87DE0E0000000, float %766)
  %arrayidx1069 = getelementptr inbounds [2 x float], [2 x float]* %w136, i32 0, i32 1
  %767 = load float, float* %arrayidx1069, align 4, !tbaa !8
  %call1070 = call float @mul_float(float 0x3FED906CC0000000, float %767)
  %call1071 = call float @sub_float(float %call1068, float %call1070)
  %call1072 = call float @add_float(float %765, float %call1071)
  store float %call1072, float* %arrayinit.element1065, align 4, !tbaa !8
  %768 = bitcast [2 x float]* %w146 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %768) #3
  %arrayinit.begin1073 = getelementptr inbounds [2 x float], [2 x float]* %w146, i32 0, i32 0
  %arrayidx1074 = getelementptr inbounds [2 x float], [2 x float]* %w114, i32 0, i32 0
  %769 = load float, float* %arrayidx1074, align 4, !tbaa !8
  %arrayidx1075 = getelementptr inbounds [2 x float], [2 x float]* %w138, i32 0, i32 0
  %770 = load float, float* %arrayidx1075, align 4, !tbaa !8
  %arrayidx1076 = getelementptr inbounds [2 x float], [2 x float]* %w138, i32 0, i32 1
  %771 = load float, float* %arrayidx1076, align 4, !tbaa !8
  %call1077 = call float @add_float(float %770, float %771)
  %call1078 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1077)
  %call1079 = call float @add_float(float %769, float %call1078)
  store float %call1079, float* %arrayinit.begin1073, align 4, !tbaa !8
  %arrayinit.element1080 = getelementptr inbounds float, float* %arrayinit.begin1073, i32 1
  %arrayidx1081 = getelementptr inbounds [2 x float], [2 x float]* %w114, i32 0, i32 1
  %772 = load float, float* %arrayidx1081, align 4, !tbaa !8
  %arrayidx1082 = getelementptr inbounds [2 x float], [2 x float]* %w138, i32 0, i32 1
  %773 = load float, float* %arrayidx1082, align 4, !tbaa !8
  %arrayidx1083 = getelementptr inbounds [2 x float], [2 x float]* %w138, i32 0, i32 0
  %774 = load float, float* %arrayidx1083, align 4, !tbaa !8
  %call1084 = call float @sub_float(float %773, float %774)
  %call1085 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1084)
  %call1086 = call float @add_float(float %772, float %call1085)
  store float %call1086, float* %arrayinit.element1080, align 4, !tbaa !8
  %775 = bitcast [2 x float]* %w147 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %775) #3
  %arrayinit.begin1087 = getelementptr inbounds [2 x float], [2 x float]* %w147, i32 0, i32 0
  %arrayidx1088 = getelementptr inbounds [2 x float], [2 x float]* %w114, i32 0, i32 0
  %776 = load float, float* %arrayidx1088, align 4, !tbaa !8
  %arrayidx1089 = getelementptr inbounds [2 x float], [2 x float]* %w138, i32 0, i32 0
  %777 = load float, float* %arrayidx1089, align 4, !tbaa !8
  %call1090 = call float @mul_float(float 0x3FE6A09EE0000000, float %777)
  %call1091 = call float @sub_float(float 0.000000e+00, float %call1090)
  %arrayidx1092 = getelementptr inbounds [2 x float], [2 x float]* %w138, i32 0, i32 1
  %778 = load float, float* %arrayidx1092, align 4, !tbaa !8
  %call1093 = call float @mul_float(float 0x3FE6A09EE0000000, float %778)
  %call1094 = call float @sub_float(float %call1091, float %call1093)
  %call1095 = call float @add_float(float %776, float %call1094)
  store float %call1095, float* %arrayinit.begin1087, align 4, !tbaa !8
  %arrayinit.element1096 = getelementptr inbounds float, float* %arrayinit.begin1087, i32 1
  %arrayidx1097 = getelementptr inbounds [2 x float], [2 x float]* %w114, i32 0, i32 1
  %779 = load float, float* %arrayidx1097, align 4, !tbaa !8
  %arrayidx1098 = getelementptr inbounds [2 x float], [2 x float]* %w138, i32 0, i32 0
  %780 = load float, float* %arrayidx1098, align 4, !tbaa !8
  %arrayidx1099 = getelementptr inbounds [2 x float], [2 x float]* %w138, i32 0, i32 1
  %781 = load float, float* %arrayidx1099, align 4, !tbaa !8
  %call1100 = call float @sub_float(float %780, float %781)
  %call1101 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1100)
  %call1102 = call float @add_float(float %779, float %call1101)
  store float %call1102, float* %arrayinit.element1096, align 4, !tbaa !8
  %782 = bitcast [2 x float]* %w148 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %782) #3
  %arrayinit.begin1103 = getelementptr inbounds [2 x float], [2 x float]* %w148, i32 0, i32 0
  %arrayidx1104 = getelementptr inbounds [2 x float], [2 x float]* %w116, i32 0, i32 0
  %783 = load float, float* %arrayidx1104, align 4, !tbaa !8
  %arrayidx1105 = getelementptr inbounds [2 x float], [2 x float]* %w140, i32 0, i32 0
  %784 = load float, float* %arrayidx1105, align 4, !tbaa !8
  %call1106 = call float @mul_float(float 0x3FD87DE0E0000000, float %784)
  %arrayidx1107 = getelementptr inbounds [2 x float], [2 x float]* %w140, i32 0, i32 1
  %785 = load float, float* %arrayidx1107, align 4, !tbaa !8
  %call1108 = call float @mul_float(float 0x3FED906CC0000000, float %785)
  %call1109 = call float @add_float(float %call1106, float %call1108)
  %call1110 = call float @add_float(float %783, float %call1109)
  store float %call1110, float* %arrayinit.begin1103, align 4, !tbaa !8
  %arrayinit.element1111 = getelementptr inbounds float, float* %arrayinit.begin1103, i32 1
  %arrayidx1112 = getelementptr inbounds [2 x float], [2 x float]* %w116, i32 0, i32 1
  %786 = load float, float* %arrayidx1112, align 4, !tbaa !8
  %arrayidx1113 = getelementptr inbounds [2 x float], [2 x float]* %w140, i32 0, i32 1
  %787 = load float, float* %arrayidx1113, align 4, !tbaa !8
  %call1114 = call float @mul_float(float 0x3FD87DE0E0000000, float %787)
  %arrayidx1115 = getelementptr inbounds [2 x float], [2 x float]* %w140, i32 0, i32 0
  %788 = load float, float* %arrayidx1115, align 4, !tbaa !8
  %call1116 = call float @mul_float(float 0x3FED906CC0000000, float %788)
  %call1117 = call float @sub_float(float %call1114, float %call1116)
  %call1118 = call float @add_float(float %786, float %call1117)
  store float %call1118, float* %arrayinit.element1111, align 4, !tbaa !8
  %789 = bitcast [2 x float]* %w149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %789) #3
  %arrayinit.begin1119 = getelementptr inbounds [2 x float], [2 x float]* %w149, i32 0, i32 0
  %arrayidx1120 = getelementptr inbounds [2 x float], [2 x float]* %w116, i32 0, i32 0
  %790 = load float, float* %arrayidx1120, align 4, !tbaa !8
  %arrayidx1121 = getelementptr inbounds [2 x float], [2 x float]* %w140, i32 0, i32 0
  %791 = load float, float* %arrayidx1121, align 4, !tbaa !8
  %call1122 = call float @mul_float(float 0x3FD87DE0E0000000, float %791)
  %call1123 = call float @sub_float(float 0.000000e+00, float %call1122)
  %arrayidx1124 = getelementptr inbounds [2 x float], [2 x float]* %w140, i32 0, i32 1
  %792 = load float, float* %arrayidx1124, align 4, !tbaa !8
  %call1125 = call float @mul_float(float 0x3FED906CC0000000, float %792)
  %call1126 = call float @sub_float(float %call1123, float %call1125)
  %call1127 = call float @add_float(float %790, float %call1126)
  store float %call1127, float* %arrayinit.begin1119, align 4, !tbaa !8
  %arrayinit.element1128 = getelementptr inbounds float, float* %arrayinit.begin1119, i32 1
  %arrayidx1129 = getelementptr inbounds [2 x float], [2 x float]* %w116, i32 0, i32 1
  %793 = load float, float* %arrayidx1129, align 4, !tbaa !8
  %arrayidx1130 = getelementptr inbounds [2 x float], [2 x float]* %w140, i32 0, i32 0
  %794 = load float, float* %arrayidx1130, align 4, !tbaa !8
  %call1131 = call float @mul_float(float 0x3FED906CC0000000, float %794)
  %arrayidx1132 = getelementptr inbounds [2 x float], [2 x float]* %w140, i32 0, i32 1
  %795 = load float, float* %arrayidx1132, align 4, !tbaa !8
  %call1133 = call float @mul_float(float 0x3FD87DE0E0000000, float %795)
  %call1134 = call float @sub_float(float %call1131, float %call1133)
  %call1135 = call float @add_float(float %793, float %call1134)
  store float %call1135, float* %arrayinit.element1128, align 4, !tbaa !8
  %796 = bitcast [2 x float]* %w150 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %796) #3
  %arrayinit.begin1136 = getelementptr inbounds [2 x float], [2 x float]* %w150, i32 0, i32 0
  %arrayidx1137 = getelementptr inbounds [2 x float], [2 x float]* %w111, i32 0, i32 0
  %797 = load float, float* %arrayidx1137, align 4, !tbaa !8
  %arrayidx1138 = getelementptr inbounds [2 x float], [2 x float]* %w135, i32 0, i32 1
  %798 = load float, float* %arrayidx1138, align 4, !tbaa !8
  %call1139 = call float @add_float(float %797, float %798)
  store float %call1139, float* %arrayinit.begin1136, align 4, !tbaa !8
  %arrayinit.element1140 = getelementptr inbounds float, float* %arrayinit.begin1136, i32 1
  %arrayidx1141 = getelementptr inbounds [2 x float], [2 x float]* %w111, i32 0, i32 1
  %799 = load float, float* %arrayidx1141, align 4, !tbaa !8
  %arrayidx1142 = getelementptr inbounds [2 x float], [2 x float]* %w135, i32 0, i32 0
  %800 = load float, float* %arrayidx1142, align 4, !tbaa !8
  %call1143 = call float @sub_float(float %799, float %800)
  store float %call1143, float* %arrayinit.element1140, align 4, !tbaa !8
  %801 = bitcast [2 x float]* %w151 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %801) #3
  %arrayinit.begin1144 = getelementptr inbounds [2 x float], [2 x float]* %w151, i32 0, i32 0
  %arrayidx1145 = getelementptr inbounds [2 x float], [2 x float]* %w111, i32 0, i32 0
  %802 = load float, float* %arrayidx1145, align 4, !tbaa !8
  %arrayidx1146 = getelementptr inbounds [2 x float], [2 x float]* %w135, i32 0, i32 1
  %803 = load float, float* %arrayidx1146, align 4, !tbaa !8
  %call1147 = call float @sub_float(float %802, float %803)
  store float %call1147, float* %arrayinit.begin1144, align 4, !tbaa !8
  %arrayinit.element1148 = getelementptr inbounds float, float* %arrayinit.begin1144, i32 1
  %arrayidx1149 = getelementptr inbounds [2 x float], [2 x float]* %w111, i32 0, i32 1
  %804 = load float, float* %arrayidx1149, align 4, !tbaa !8
  %arrayidx1150 = getelementptr inbounds [2 x float], [2 x float]* %w135, i32 0, i32 0
  %805 = load float, float* %arrayidx1150, align 4, !tbaa !8
  %call1151 = call float @add_float(float %804, float %805)
  store float %call1151, float* %arrayinit.element1148, align 4, !tbaa !8
  %806 = bitcast [2 x float]* %w152 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %806) #3
  %arrayinit.begin1152 = getelementptr inbounds [2 x float], [2 x float]* %w152, i32 0, i32 0
  %arrayidx1153 = getelementptr inbounds [2 x float], [2 x float]* %w113, i32 0, i32 0
  %807 = load float, float* %arrayidx1153, align 4, !tbaa !8
  %arrayidx1154 = getelementptr inbounds [2 x float], [2 x float]* %w137, i32 0, i32 0
  %808 = load float, float* %arrayidx1154, align 4, !tbaa !8
  %call1155 = call float @mul_float(float 0x3FD87DE0E0000000, float %808)
  %arrayidx1156 = getelementptr inbounds [2 x float], [2 x float]* %w137, i32 0, i32 1
  %809 = load float, float* %arrayidx1156, align 4, !tbaa !8
  %call1157 = call float @mul_float(float 0x3FED906CC0000000, float %809)
  %call1158 = call float @sub_float(float %call1155, float %call1157)
  %call1159 = call float @sub_float(float %807, float %call1158)
  store float %call1159, float* %arrayinit.begin1152, align 4, !tbaa !8
  %arrayinit.element1160 = getelementptr inbounds float, float* %arrayinit.begin1152, i32 1
  %arrayidx1161 = getelementptr inbounds [2 x float], [2 x float]* %w113, i32 0, i32 1
  %810 = load float, float* %arrayidx1161, align 4, !tbaa !8
  %arrayidx1162 = getelementptr inbounds [2 x float], [2 x float]* %w137, i32 0, i32 1
  %811 = load float, float* %arrayidx1162, align 4, !tbaa !8
  %call1163 = call float @mul_float(float 0x3FD87DE0E0000000, float %811)
  %call1164 = call float @sub_float(float 0.000000e+00, float %call1163)
  %arrayidx1165 = getelementptr inbounds [2 x float], [2 x float]* %w137, i32 0, i32 0
  %812 = load float, float* %arrayidx1165, align 4, !tbaa !8
  %call1166 = call float @mul_float(float 0x3FED906CC0000000, float %812)
  %call1167 = call float @sub_float(float %call1164, float %call1166)
  %call1168 = call float @add_float(float %810, float %call1167)
  store float %call1168, float* %arrayinit.element1160, align 4, !tbaa !8
  %813 = bitcast [2 x float]* %w153 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %813) #3
  %arrayinit.begin1169 = getelementptr inbounds [2 x float], [2 x float]* %w153, i32 0, i32 0
  %arrayidx1170 = getelementptr inbounds [2 x float], [2 x float]* %w113, i32 0, i32 0
  %814 = load float, float* %arrayidx1170, align 4, !tbaa !8
  %arrayidx1171 = getelementptr inbounds [2 x float], [2 x float]* %w137, i32 0, i32 0
  %815 = load float, float* %arrayidx1171, align 4, !tbaa !8
  %call1172 = call float @mul_float(float 0x3FD87DE0E0000000, float %815)
  %arrayidx1173 = getelementptr inbounds [2 x float], [2 x float]* %w137, i32 0, i32 1
  %816 = load float, float* %arrayidx1173, align 4, !tbaa !8
  %call1174 = call float @mul_float(float 0x3FED906CC0000000, float %816)
  %call1175 = call float @sub_float(float %call1172, float %call1174)
  %call1176 = call float @add_float(float %814, float %call1175)
  store float %call1176, float* %arrayinit.begin1169, align 4, !tbaa !8
  %arrayinit.element1177 = getelementptr inbounds float, float* %arrayinit.begin1169, i32 1
  %arrayidx1178 = getelementptr inbounds [2 x float], [2 x float]* %w113, i32 0, i32 1
  %817 = load float, float* %arrayidx1178, align 4, !tbaa !8
  %arrayidx1179 = getelementptr inbounds [2 x float], [2 x float]* %w137, i32 0, i32 1
  %818 = load float, float* %arrayidx1179, align 4, !tbaa !8
  %call1180 = call float @mul_float(float 0x3FD87DE0E0000000, float %818)
  %arrayidx1181 = getelementptr inbounds [2 x float], [2 x float]* %w137, i32 0, i32 0
  %819 = load float, float* %arrayidx1181, align 4, !tbaa !8
  %call1182 = call float @mul_float(float 0x3FED906CC0000000, float %819)
  %call1183 = call float @add_float(float %call1180, float %call1182)
  %call1184 = call float @add_float(float %817, float %call1183)
  store float %call1184, float* %arrayinit.element1177, align 4, !tbaa !8
  %820 = bitcast [2 x float]* %w154 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %820) #3
  %arrayinit.begin1185 = getelementptr inbounds [2 x float], [2 x float]* %w154, i32 0, i32 0
  %arrayidx1186 = getelementptr inbounds [2 x float], [2 x float]* %w115, i32 0, i32 0
  %821 = load float, float* %arrayidx1186, align 4, !tbaa !8
  %arrayidx1187 = getelementptr inbounds [2 x float], [2 x float]* %w139, i32 0, i32 0
  %822 = load float, float* %arrayidx1187, align 4, !tbaa !8
  %arrayidx1188 = getelementptr inbounds [2 x float], [2 x float]* %w139, i32 0, i32 1
  %823 = load float, float* %arrayidx1188, align 4, !tbaa !8
  %call1189 = call float @sub_float(float %822, float %823)
  %call1190 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1189)
  %call1191 = call float @sub_float(float %821, float %call1190)
  store float %call1191, float* %arrayinit.begin1185, align 4, !tbaa !8
  %arrayinit.element1192 = getelementptr inbounds float, float* %arrayinit.begin1185, i32 1
  %arrayidx1193 = getelementptr inbounds [2 x float], [2 x float]* %w115, i32 0, i32 1
  %824 = load float, float* %arrayidx1193, align 4, !tbaa !8
  %arrayidx1194 = getelementptr inbounds [2 x float], [2 x float]* %w139, i32 0, i32 1
  %825 = load float, float* %arrayidx1194, align 4, !tbaa !8
  %arrayidx1195 = getelementptr inbounds [2 x float], [2 x float]* %w139, i32 0, i32 0
  %826 = load float, float* %arrayidx1195, align 4, !tbaa !8
  %call1196 = call float @add_float(float %825, float %826)
  %call1197 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1196)
  %call1198 = call float @sub_float(float %824, float %call1197)
  store float %call1198, float* %arrayinit.element1192, align 4, !tbaa !8
  %827 = bitcast [2 x float]* %w155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %827) #3
  %arrayinit.begin1199 = getelementptr inbounds [2 x float], [2 x float]* %w155, i32 0, i32 0
  %arrayidx1200 = getelementptr inbounds [2 x float], [2 x float]* %w115, i32 0, i32 0
  %828 = load float, float* %arrayidx1200, align 4, !tbaa !8
  %arrayidx1201 = getelementptr inbounds [2 x float], [2 x float]* %w139, i32 0, i32 0
  %829 = load float, float* %arrayidx1201, align 4, !tbaa !8
  %arrayidx1202 = getelementptr inbounds [2 x float], [2 x float]* %w139, i32 0, i32 1
  %830 = load float, float* %arrayidx1202, align 4, !tbaa !8
  %call1203 = call float @sub_float(float %829, float %830)
  %call1204 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1203)
  %call1205 = call float @add_float(float %828, float %call1204)
  store float %call1205, float* %arrayinit.begin1199, align 4, !tbaa !8
  %arrayinit.element1206 = getelementptr inbounds float, float* %arrayinit.begin1199, i32 1
  %arrayidx1207 = getelementptr inbounds [2 x float], [2 x float]* %w115, i32 0, i32 1
  %831 = load float, float* %arrayidx1207, align 4, !tbaa !8
  %arrayidx1208 = getelementptr inbounds [2 x float], [2 x float]* %w139, i32 0, i32 1
  %832 = load float, float* %arrayidx1208, align 4, !tbaa !8
  %arrayidx1209 = getelementptr inbounds [2 x float], [2 x float]* %w139, i32 0, i32 0
  %833 = load float, float* %arrayidx1209, align 4, !tbaa !8
  %call1210 = call float @add_float(float %832, float %833)
  %call1211 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1210)
  %call1212 = call float @add_float(float %831, float %call1211)
  store float %call1212, float* %arrayinit.element1206, align 4, !tbaa !8
  %834 = bitcast [2 x float]* %w156 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %834) #3
  %arrayinit.begin1213 = getelementptr inbounds [2 x float], [2 x float]* %w156, i32 0, i32 0
  %arrayidx1214 = getelementptr inbounds [2 x float], [2 x float]* %w117, i32 0, i32 0
  %835 = load float, float* %arrayidx1214, align 4, !tbaa !8
  %arrayidx1215 = getelementptr inbounds [2 x float], [2 x float]* %w141, i32 0, i32 0
  %836 = load float, float* %arrayidx1215, align 4, !tbaa !8
  %call1216 = call float @mul_float(float 0x3FED906CC0000000, float %836)
  %arrayidx1217 = getelementptr inbounds [2 x float], [2 x float]* %w141, i32 0, i32 1
  %837 = load float, float* %arrayidx1217, align 4, !tbaa !8
  %call1218 = call float @mul_float(float 0x3FD87DE0E0000000, float %837)
  %call1219 = call float @sub_float(float %call1216, float %call1218)
  %call1220 = call float @sub_float(float %835, float %call1219)
  store float %call1220, float* %arrayinit.begin1213, align 4, !tbaa !8
  %arrayinit.element1221 = getelementptr inbounds float, float* %arrayinit.begin1213, i32 1
  %arrayidx1222 = getelementptr inbounds [2 x float], [2 x float]* %w117, i32 0, i32 1
  %838 = load float, float* %arrayidx1222, align 4, !tbaa !8
  %arrayidx1223 = getelementptr inbounds [2 x float], [2 x float]* %w141, i32 0, i32 1
  %839 = load float, float* %arrayidx1223, align 4, !tbaa !8
  %call1224 = call float @mul_float(float 0x3FED906CC0000000, float %839)
  %call1225 = call float @sub_float(float 0.000000e+00, float %call1224)
  %arrayidx1226 = getelementptr inbounds [2 x float], [2 x float]* %w141, i32 0, i32 0
  %840 = load float, float* %arrayidx1226, align 4, !tbaa !8
  %call1227 = call float @mul_float(float 0x3FD87DE0E0000000, float %840)
  %call1228 = call float @sub_float(float %call1225, float %call1227)
  %call1229 = call float @add_float(float %838, float %call1228)
  store float %call1229, float* %arrayinit.element1221, align 4, !tbaa !8
  %841 = bitcast [2 x float]* %w157 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %841) #3
  %arrayinit.begin1230 = getelementptr inbounds [2 x float], [2 x float]* %w157, i32 0, i32 0
  %arrayidx1231 = getelementptr inbounds [2 x float], [2 x float]* %w117, i32 0, i32 0
  %842 = load float, float* %arrayidx1231, align 4, !tbaa !8
  %arrayidx1232 = getelementptr inbounds [2 x float], [2 x float]* %w141, i32 0, i32 0
  %843 = load float, float* %arrayidx1232, align 4, !tbaa !8
  %call1233 = call float @mul_float(float 0x3FED906CC0000000, float %843)
  %arrayidx1234 = getelementptr inbounds [2 x float], [2 x float]* %w141, i32 0, i32 1
  %844 = load float, float* %arrayidx1234, align 4, !tbaa !8
  %call1235 = call float @mul_float(float 0x3FD87DE0E0000000, float %844)
  %call1236 = call float @sub_float(float %call1233, float %call1235)
  %call1237 = call float @add_float(float %842, float %call1236)
  store float %call1237, float* %arrayinit.begin1230, align 4, !tbaa !8
  %arrayinit.element1238 = getelementptr inbounds float, float* %arrayinit.begin1230, i32 1
  %arrayidx1239 = getelementptr inbounds [2 x float], [2 x float]* %w117, i32 0, i32 1
  %845 = load float, float* %arrayidx1239, align 4, !tbaa !8
  %arrayidx1240 = getelementptr inbounds [2 x float], [2 x float]* %w141, i32 0, i32 1
  %846 = load float, float* %arrayidx1240, align 4, !tbaa !8
  %call1241 = call float @mul_float(float 0x3FED906CC0000000, float %846)
  %arrayidx1242 = getelementptr inbounds [2 x float], [2 x float]* %w141, i32 0, i32 0
  %847 = load float, float* %arrayidx1242, align 4, !tbaa !8
  %call1243 = call float @mul_float(float 0x3FD87DE0E0000000, float %847)
  %call1244 = call float @add_float(float %call1241, float %call1243)
  %call1245 = call float @add_float(float %845, float %call1244)
  store float %call1245, float* %arrayinit.element1238, align 4, !tbaa !8
  %848 = load float*, float** %output.addr, align 4, !tbaa !2
  %849 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1246 = mul nsw i32 0, %849
  %add.ptr1247 = getelementptr inbounds float, float* %848, i32 %mul1246
  %arrayidx1248 = getelementptr inbounds [2 x float], [2 x float]* %w78, i32 0, i32 0
  %850 = load float, float* %arrayidx1248, align 4, !tbaa !8
  %arrayidx1249 = getelementptr inbounds [2 x float], [2 x float]* %w142, i32 0, i32 0
  %851 = load float, float* %arrayidx1249, align 4, !tbaa !8
  %call1250 = call float @add_float(float %850, float %851)
  call void @store_float(float* %add.ptr1247, float %call1250)
  %852 = load float*, float** %output.addr, align 4, !tbaa !2
  %853 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1251 = mul nsw i32 1, %853
  %add.ptr1252 = getelementptr inbounds float, float* %852, i32 %mul1251
  %arrayidx1253 = getelementptr inbounds [2 x float], [2 x float]* %w80, i32 0, i32 0
  %854 = load float, float* %arrayidx1253, align 4, !tbaa !8
  %arrayidx1254 = getelementptr inbounds [2 x float], [2 x float]* %w144, i32 0, i32 0
  %855 = load float, float* %arrayidx1254, align 4, !tbaa !8
  %call1255 = call float @mul_float(float 0x3FEF629740000000, float %855)
  %arrayidx1256 = getelementptr inbounds [2 x float], [2 x float]* %w144, i32 0, i32 1
  %856 = load float, float* %arrayidx1256, align 4, !tbaa !8
  %call1257 = call float @mul_float(float 0x3FC8F8B580000000, float %856)
  %call1258 = call float @add_float(float %call1255, float %call1257)
  %call1259 = call float @add_float(float %854, float %call1258)
  call void @store_float(float* %add.ptr1252, float %call1259)
  %857 = load float*, float** %output.addr, align 4, !tbaa !2
  %858 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1260 = mul nsw i32 2, %858
  %add.ptr1261 = getelementptr inbounds float, float* %857, i32 %mul1260
  %arrayidx1262 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 0
  %859 = load float, float* %arrayidx1262, align 4, !tbaa !8
  %arrayidx1263 = getelementptr inbounds [2 x float], [2 x float]* %w146, i32 0, i32 0
  %860 = load float, float* %arrayidx1263, align 4, !tbaa !8
  %call1264 = call float @mul_float(float 0x3FED906CC0000000, float %860)
  %arrayidx1265 = getelementptr inbounds [2 x float], [2 x float]* %w146, i32 0, i32 1
  %861 = load float, float* %arrayidx1265, align 4, !tbaa !8
  %call1266 = call float @mul_float(float 0x3FD87DE0E0000000, float %861)
  %call1267 = call float @add_float(float %call1264, float %call1266)
  %call1268 = call float @add_float(float %859, float %call1267)
  call void @store_float(float* %add.ptr1261, float %call1268)
  %862 = load float*, float** %output.addr, align 4, !tbaa !2
  %863 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1269 = mul nsw i32 3, %863
  %add.ptr1270 = getelementptr inbounds float, float* %862, i32 %mul1269
  %arrayidx1271 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 0
  %864 = load float, float* %arrayidx1271, align 4, !tbaa !8
  %arrayidx1272 = getelementptr inbounds [2 x float], [2 x float]* %w148, i32 0, i32 0
  %865 = load float, float* %arrayidx1272, align 4, !tbaa !8
  %call1273 = call float @mul_float(float 0x3FEA9B6700000000, float %865)
  %arrayidx1274 = getelementptr inbounds [2 x float], [2 x float]* %w148, i32 0, i32 1
  %866 = load float, float* %arrayidx1274, align 4, !tbaa !8
  %call1275 = call float @mul_float(float 0x3FE1C73AC0000000, float %866)
  %call1276 = call float @add_float(float %call1273, float %call1275)
  %call1277 = call float @add_float(float %864, float %call1276)
  call void @store_float(float* %add.ptr1270, float %call1277)
  %867 = load float*, float** %output.addr, align 4, !tbaa !2
  %868 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1278 = mul nsw i32 4, %868
  %add.ptr1279 = getelementptr inbounds float, float* %867, i32 %mul1278
  %arrayidx1280 = getelementptr inbounds [2 x float], [2 x float]* %w86, i32 0, i32 0
  %869 = load float, float* %arrayidx1280, align 4, !tbaa !8
  %arrayidx1281 = getelementptr inbounds [2 x float], [2 x float]* %w150, i32 0, i32 0
  %870 = load float, float* %arrayidx1281, align 4, !tbaa !8
  %arrayidx1282 = getelementptr inbounds [2 x float], [2 x float]* %w150, i32 0, i32 1
  %871 = load float, float* %arrayidx1282, align 4, !tbaa !8
  %call1283 = call float @add_float(float %870, float %871)
  %call1284 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1283)
  %call1285 = call float @add_float(float %869, float %call1284)
  call void @store_float(float* %add.ptr1279, float %call1285)
  %872 = load float*, float** %output.addr, align 4, !tbaa !2
  %873 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1286 = mul nsw i32 5, %873
  %add.ptr1287 = getelementptr inbounds float, float* %872, i32 %mul1286
  %arrayidx1288 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 0
  %874 = load float, float* %arrayidx1288, align 4, !tbaa !8
  %arrayidx1289 = getelementptr inbounds [2 x float], [2 x float]* %w152, i32 0, i32 0
  %875 = load float, float* %arrayidx1289, align 4, !tbaa !8
  %call1290 = call float @mul_float(float 0x3FE1C73AC0000000, float %875)
  %arrayidx1291 = getelementptr inbounds [2 x float], [2 x float]* %w152, i32 0, i32 1
  %876 = load float, float* %arrayidx1291, align 4, !tbaa !8
  %call1292 = call float @mul_float(float 0x3FEA9B6700000000, float %876)
  %call1293 = call float @add_float(float %call1290, float %call1292)
  %call1294 = call float @add_float(float %874, float %call1293)
  call void @store_float(float* %add.ptr1287, float %call1294)
  %877 = load float*, float** %output.addr, align 4, !tbaa !2
  %878 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1295 = mul nsw i32 6, %878
  %add.ptr1296 = getelementptr inbounds float, float* %877, i32 %mul1295
  %arrayidx1297 = getelementptr inbounds [2 x float], [2 x float]* %w90, i32 0, i32 0
  %879 = load float, float* %arrayidx1297, align 4, !tbaa !8
  %arrayidx1298 = getelementptr inbounds [2 x float], [2 x float]* %w154, i32 0, i32 0
  %880 = load float, float* %arrayidx1298, align 4, !tbaa !8
  %call1299 = call float @mul_float(float 0x3FD87DE0E0000000, float %880)
  %arrayidx1300 = getelementptr inbounds [2 x float], [2 x float]* %w154, i32 0, i32 1
  %881 = load float, float* %arrayidx1300, align 4, !tbaa !8
  %call1301 = call float @mul_float(float 0x3FED906CC0000000, float %881)
  %call1302 = call float @add_float(float %call1299, float %call1301)
  %call1303 = call float @add_float(float %879, float %call1302)
  call void @store_float(float* %add.ptr1296, float %call1303)
  %882 = load float*, float** %output.addr, align 4, !tbaa !2
  %883 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1304 = mul nsw i32 7, %883
  %add.ptr1305 = getelementptr inbounds float, float* %882, i32 %mul1304
  %arrayidx1306 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 0
  %884 = load float, float* %arrayidx1306, align 4, !tbaa !8
  %arrayidx1307 = getelementptr inbounds [2 x float], [2 x float]* %w156, i32 0, i32 0
  %885 = load float, float* %arrayidx1307, align 4, !tbaa !8
  %call1308 = call float @mul_float(float 0x3FC8F8B580000000, float %885)
  %arrayidx1309 = getelementptr inbounds [2 x float], [2 x float]* %w156, i32 0, i32 1
  %886 = load float, float* %arrayidx1309, align 4, !tbaa !8
  %call1310 = call float @mul_float(float 0x3FEF629740000000, float %886)
  %call1311 = call float @add_float(float %call1308, float %call1310)
  %call1312 = call float @add_float(float %884, float %call1311)
  call void @store_float(float* %add.ptr1305, float %call1312)
  %887 = load float*, float** %output.addr, align 4, !tbaa !2
  %888 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1313 = mul nsw i32 8, %888
  %add.ptr1314 = getelementptr inbounds float, float* %887, i32 %mul1313
  %arrayidx1315 = getelementptr inbounds [2 x float], [2 x float]* %w79, i32 0, i32 0
  %889 = load float, float* %arrayidx1315, align 4, !tbaa !8
  %arrayidx1316 = getelementptr inbounds [2 x float], [2 x float]* %w143, i32 0, i32 1
  %890 = load float, float* %arrayidx1316, align 4, !tbaa !8
  %call1317 = call float @add_float(float %889, float %890)
  call void @store_float(float* %add.ptr1314, float %call1317)
  %891 = load float*, float** %output.addr, align 4, !tbaa !2
  %892 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1318 = mul nsw i32 9, %892
  %add.ptr1319 = getelementptr inbounds float, float* %891, i32 %mul1318
  %arrayidx1320 = getelementptr inbounds [2 x float], [2 x float]* %w81, i32 0, i32 0
  %893 = load float, float* %arrayidx1320, align 4, !tbaa !8
  %arrayidx1321 = getelementptr inbounds [2 x float], [2 x float]* %w145, i32 0, i32 0
  %894 = load float, float* %arrayidx1321, align 4, !tbaa !8
  %call1322 = call float @mul_float(float 0x3FC8F8B580000000, float %894)
  %arrayidx1323 = getelementptr inbounds [2 x float], [2 x float]* %w145, i32 0, i32 1
  %895 = load float, float* %arrayidx1323, align 4, !tbaa !8
  %call1324 = call float @mul_float(float 0x3FEF629740000000, float %895)
  %call1325 = call float @sub_float(float %call1322, float %call1324)
  %call1326 = call float @sub_float(float %893, float %call1325)
  call void @store_float(float* %add.ptr1319, float %call1326)
  %896 = load float*, float** %output.addr, align 4, !tbaa !2
  %897 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1327 = mul nsw i32 10, %897
  %add.ptr1328 = getelementptr inbounds float, float* %896, i32 %mul1327
  %arrayidx1329 = getelementptr inbounds [2 x float], [2 x float]* %w83, i32 0, i32 0
  %898 = load float, float* %arrayidx1329, align 4, !tbaa !8
  %arrayidx1330 = getelementptr inbounds [2 x float], [2 x float]* %w147, i32 0, i32 0
  %899 = load float, float* %arrayidx1330, align 4, !tbaa !8
  %call1331 = call float @mul_float(float 0x3FD87DE0E0000000, float %899)
  %arrayidx1332 = getelementptr inbounds [2 x float], [2 x float]* %w147, i32 0, i32 1
  %900 = load float, float* %arrayidx1332, align 4, !tbaa !8
  %call1333 = call float @mul_float(float 0x3FED906CC0000000, float %900)
  %call1334 = call float @sub_float(float %call1331, float %call1333)
  %call1335 = call float @sub_float(float %898, float %call1334)
  call void @store_float(float* %add.ptr1328, float %call1335)
  %901 = load float*, float** %output.addr, align 4, !tbaa !2
  %902 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1336 = mul nsw i32 11, %902
  %add.ptr1337 = getelementptr inbounds float, float* %901, i32 %mul1336
  %arrayidx1338 = getelementptr inbounds [2 x float], [2 x float]* %w85, i32 0, i32 0
  %903 = load float, float* %arrayidx1338, align 4, !tbaa !8
  %arrayidx1339 = getelementptr inbounds [2 x float], [2 x float]* %w149, i32 0, i32 0
  %904 = load float, float* %arrayidx1339, align 4, !tbaa !8
  %call1340 = call float @mul_float(float 0x3FE1C73AC0000000, float %904)
  %arrayidx1341 = getelementptr inbounds [2 x float], [2 x float]* %w149, i32 0, i32 1
  %905 = load float, float* %arrayidx1341, align 4, !tbaa !8
  %call1342 = call float @mul_float(float 0x3FEA9B6700000000, float %905)
  %call1343 = call float @sub_float(float %call1340, float %call1342)
  %call1344 = call float @sub_float(float %903, float %call1343)
  call void @store_float(float* %add.ptr1337, float %call1344)
  %906 = load float*, float** %output.addr, align 4, !tbaa !2
  %907 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1345 = mul nsw i32 12, %907
  %add.ptr1346 = getelementptr inbounds float, float* %906, i32 %mul1345
  %arrayidx1347 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 0
  %908 = load float, float* %arrayidx1347, align 4, !tbaa !8
  %arrayidx1348 = getelementptr inbounds [2 x float], [2 x float]* %w151, i32 0, i32 0
  %909 = load float, float* %arrayidx1348, align 4, !tbaa !8
  %arrayidx1349 = getelementptr inbounds [2 x float], [2 x float]* %w151, i32 0, i32 1
  %910 = load float, float* %arrayidx1349, align 4, !tbaa !8
  %call1350 = call float @sub_float(float %909, float %910)
  %call1351 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1350)
  %call1352 = call float @sub_float(float %908, float %call1351)
  call void @store_float(float* %add.ptr1346, float %call1352)
  %911 = load float*, float** %output.addr, align 4, !tbaa !2
  %912 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1353 = mul nsw i32 13, %912
  %add.ptr1354 = getelementptr inbounds float, float* %911, i32 %mul1353
  %arrayidx1355 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 0
  %913 = load float, float* %arrayidx1355, align 4, !tbaa !8
  %arrayidx1356 = getelementptr inbounds [2 x float], [2 x float]* %w153, i32 0, i32 0
  %914 = load float, float* %arrayidx1356, align 4, !tbaa !8
  %call1357 = call float @mul_float(float 0x3FEA9B6700000000, float %914)
  %arrayidx1358 = getelementptr inbounds [2 x float], [2 x float]* %w153, i32 0, i32 1
  %915 = load float, float* %arrayidx1358, align 4, !tbaa !8
  %call1359 = call float @mul_float(float 0x3FE1C73AC0000000, float %915)
  %call1360 = call float @sub_float(float %call1357, float %call1359)
  %call1361 = call float @sub_float(float %913, float %call1360)
  call void @store_float(float* %add.ptr1354, float %call1361)
  %916 = load float*, float** %output.addr, align 4, !tbaa !2
  %917 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1362 = mul nsw i32 14, %917
  %add.ptr1363 = getelementptr inbounds float, float* %916, i32 %mul1362
  %arrayidx1364 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 0
  %918 = load float, float* %arrayidx1364, align 4, !tbaa !8
  %arrayidx1365 = getelementptr inbounds [2 x float], [2 x float]* %w155, i32 0, i32 0
  %919 = load float, float* %arrayidx1365, align 4, !tbaa !8
  %call1366 = call float @mul_float(float 0x3FED906CC0000000, float %919)
  %arrayidx1367 = getelementptr inbounds [2 x float], [2 x float]* %w155, i32 0, i32 1
  %920 = load float, float* %arrayidx1367, align 4, !tbaa !8
  %call1368 = call float @mul_float(float 0x3FD87DE0E0000000, float %920)
  %call1369 = call float @sub_float(float %call1366, float %call1368)
  %call1370 = call float @sub_float(float %918, float %call1369)
  call void @store_float(float* %add.ptr1363, float %call1370)
  %921 = load float*, float** %output.addr, align 4, !tbaa !2
  %922 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1371 = mul nsw i32 15, %922
  %add.ptr1372 = getelementptr inbounds float, float* %921, i32 %mul1371
  %arrayidx1373 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 0
  %923 = load float, float* %arrayidx1373, align 4, !tbaa !8
  %arrayidx1374 = getelementptr inbounds [2 x float], [2 x float]* %w157, i32 0, i32 0
  %924 = load float, float* %arrayidx1374, align 4, !tbaa !8
  %call1375 = call float @mul_float(float 0x3FEF629740000000, float %924)
  %arrayidx1376 = getelementptr inbounds [2 x float], [2 x float]* %w157, i32 0, i32 1
  %925 = load float, float* %arrayidx1376, align 4, !tbaa !8
  %call1377 = call float @mul_float(float 0x3FC8F8B580000000, float %925)
  %call1378 = call float @sub_float(float %call1375, float %call1377)
  %call1379 = call float @sub_float(float %923, float %call1378)
  call void @store_float(float* %add.ptr1372, float %call1379)
  %926 = load float*, float** %output.addr, align 4, !tbaa !2
  %927 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1380 = mul nsw i32 16, %927
  %add.ptr1381 = getelementptr inbounds float, float* %926, i32 %mul1380
  %arrayidx1382 = getelementptr inbounds [2 x float], [2 x float]* %w78, i32 0, i32 0
  %928 = load float, float* %arrayidx1382, align 4, !tbaa !8
  %arrayidx1383 = getelementptr inbounds [2 x float], [2 x float]* %w142, i32 0, i32 0
  %929 = load float, float* %arrayidx1383, align 4, !tbaa !8
  %call1384 = call float @sub_float(float %928, float %929)
  call void @store_float(float* %add.ptr1381, float %call1384)
  %930 = load float*, float** %output.addr, align 4, !tbaa !2
  %931 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1385 = mul nsw i32 17, %931
  %add.ptr1386 = getelementptr inbounds float, float* %930, i32 %mul1385
  %arrayidx1387 = getelementptr inbounds [2 x float], [2 x float]* %w80, i32 0, i32 0
  %932 = load float, float* %arrayidx1387, align 4, !tbaa !8
  %arrayidx1388 = getelementptr inbounds [2 x float], [2 x float]* %w144, i32 0, i32 0
  %933 = load float, float* %arrayidx1388, align 4, !tbaa !8
  %call1389 = call float @mul_float(float 0x3FEF629740000000, float %933)
  %call1390 = call float @sub_float(float 0.000000e+00, float %call1389)
  %arrayidx1391 = getelementptr inbounds [2 x float], [2 x float]* %w144, i32 0, i32 1
  %934 = load float, float* %arrayidx1391, align 4, !tbaa !8
  %call1392 = call float @mul_float(float 0x3FC8F8B580000000, float %934)
  %call1393 = call float @sub_float(float %call1390, float %call1392)
  %call1394 = call float @add_float(float %932, float %call1393)
  call void @store_float(float* %add.ptr1386, float %call1394)
  %935 = load float*, float** %output.addr, align 4, !tbaa !2
  %936 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1395 = mul nsw i32 18, %936
  %add.ptr1396 = getelementptr inbounds float, float* %935, i32 %mul1395
  %arrayidx1397 = getelementptr inbounds [2 x float], [2 x float]* %w82, i32 0, i32 0
  %937 = load float, float* %arrayidx1397, align 4, !tbaa !8
  %arrayidx1398 = getelementptr inbounds [2 x float], [2 x float]* %w146, i32 0, i32 0
  %938 = load float, float* %arrayidx1398, align 4, !tbaa !8
  %call1399 = call float @mul_float(float 0x3FED906CC0000000, float %938)
  %call1400 = call float @sub_float(float 0.000000e+00, float %call1399)
  %arrayidx1401 = getelementptr inbounds [2 x float], [2 x float]* %w146, i32 0, i32 1
  %939 = load float, float* %arrayidx1401, align 4, !tbaa !8
  %call1402 = call float @mul_float(float 0x3FD87DE0E0000000, float %939)
  %call1403 = call float @sub_float(float %call1400, float %call1402)
  %call1404 = call float @add_float(float %937, float %call1403)
  call void @store_float(float* %add.ptr1396, float %call1404)
  %940 = load float*, float** %output.addr, align 4, !tbaa !2
  %941 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1405 = mul nsw i32 19, %941
  %add.ptr1406 = getelementptr inbounds float, float* %940, i32 %mul1405
  %arrayidx1407 = getelementptr inbounds [2 x float], [2 x float]* %w84, i32 0, i32 0
  %942 = load float, float* %arrayidx1407, align 4, !tbaa !8
  %arrayidx1408 = getelementptr inbounds [2 x float], [2 x float]* %w148, i32 0, i32 0
  %943 = load float, float* %arrayidx1408, align 4, !tbaa !8
  %call1409 = call float @mul_float(float 0x3FEA9B6700000000, float %943)
  %call1410 = call float @sub_float(float 0.000000e+00, float %call1409)
  %arrayidx1411 = getelementptr inbounds [2 x float], [2 x float]* %w148, i32 0, i32 1
  %944 = load float, float* %arrayidx1411, align 4, !tbaa !8
  %call1412 = call float @mul_float(float 0x3FE1C73AC0000000, float %944)
  %call1413 = call float @sub_float(float %call1410, float %call1412)
  %call1414 = call float @add_float(float %942, float %call1413)
  call void @store_float(float* %add.ptr1406, float %call1414)
  %945 = load float*, float** %output.addr, align 4, !tbaa !2
  %946 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1415 = mul nsw i32 20, %946
  %add.ptr1416 = getelementptr inbounds float, float* %945, i32 %mul1415
  %arrayidx1417 = getelementptr inbounds [2 x float], [2 x float]* %w86, i32 0, i32 0
  %947 = load float, float* %arrayidx1417, align 4, !tbaa !8
  %arrayidx1418 = getelementptr inbounds [2 x float], [2 x float]* %w150, i32 0, i32 0
  %948 = load float, float* %arrayidx1418, align 4, !tbaa !8
  %call1419 = call float @mul_float(float 0x3FE6A09EE0000000, float %948)
  %call1420 = call float @sub_float(float 0.000000e+00, float %call1419)
  %arrayidx1421 = getelementptr inbounds [2 x float], [2 x float]* %w150, i32 0, i32 1
  %949 = load float, float* %arrayidx1421, align 4, !tbaa !8
  %call1422 = call float @mul_float(float 0x3FE6A09EE0000000, float %949)
  %call1423 = call float @sub_float(float %call1420, float %call1422)
  %call1424 = call float @add_float(float %947, float %call1423)
  call void @store_float(float* %add.ptr1416, float %call1424)
  %950 = load float*, float** %output.addr, align 4, !tbaa !2
  %951 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1425 = mul nsw i32 21, %951
  %add.ptr1426 = getelementptr inbounds float, float* %950, i32 %mul1425
  %arrayidx1427 = getelementptr inbounds [2 x float], [2 x float]* %w88, i32 0, i32 0
  %952 = load float, float* %arrayidx1427, align 4, !tbaa !8
  %arrayidx1428 = getelementptr inbounds [2 x float], [2 x float]* %w152, i32 0, i32 0
  %953 = load float, float* %arrayidx1428, align 4, !tbaa !8
  %call1429 = call float @mul_float(float 0x3FE1C73AC0000000, float %953)
  %call1430 = call float @sub_float(float 0.000000e+00, float %call1429)
  %arrayidx1431 = getelementptr inbounds [2 x float], [2 x float]* %w152, i32 0, i32 1
  %954 = load float, float* %arrayidx1431, align 4, !tbaa !8
  %call1432 = call float @mul_float(float 0x3FEA9B6700000000, float %954)
  %call1433 = call float @sub_float(float %call1430, float %call1432)
  %call1434 = call float @add_float(float %952, float %call1433)
  call void @store_float(float* %add.ptr1426, float %call1434)
  %955 = load float*, float** %output.addr, align 4, !tbaa !2
  %956 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1435 = mul nsw i32 22, %956
  %add.ptr1436 = getelementptr inbounds float, float* %955, i32 %mul1435
  %arrayidx1437 = getelementptr inbounds [2 x float], [2 x float]* %w90, i32 0, i32 0
  %957 = load float, float* %arrayidx1437, align 4, !tbaa !8
  %arrayidx1438 = getelementptr inbounds [2 x float], [2 x float]* %w154, i32 0, i32 0
  %958 = load float, float* %arrayidx1438, align 4, !tbaa !8
  %call1439 = call float @mul_float(float 0x3FD87DE0E0000000, float %958)
  %call1440 = call float @sub_float(float 0.000000e+00, float %call1439)
  %arrayidx1441 = getelementptr inbounds [2 x float], [2 x float]* %w154, i32 0, i32 1
  %959 = load float, float* %arrayidx1441, align 4, !tbaa !8
  %call1442 = call float @mul_float(float 0x3FED906CC0000000, float %959)
  %call1443 = call float @sub_float(float %call1440, float %call1442)
  %call1444 = call float @add_float(float %957, float %call1443)
  call void @store_float(float* %add.ptr1436, float %call1444)
  %960 = load float*, float** %output.addr, align 4, !tbaa !2
  %961 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1445 = mul nsw i32 23, %961
  %add.ptr1446 = getelementptr inbounds float, float* %960, i32 %mul1445
  %arrayidx1447 = getelementptr inbounds [2 x float], [2 x float]* %w92, i32 0, i32 0
  %962 = load float, float* %arrayidx1447, align 4, !tbaa !8
  %arrayidx1448 = getelementptr inbounds [2 x float], [2 x float]* %w156, i32 0, i32 0
  %963 = load float, float* %arrayidx1448, align 4, !tbaa !8
  %call1449 = call float @mul_float(float 0x3FC8F8B580000000, float %963)
  %call1450 = call float @sub_float(float 0.000000e+00, float %call1449)
  %arrayidx1451 = getelementptr inbounds [2 x float], [2 x float]* %w156, i32 0, i32 1
  %964 = load float, float* %arrayidx1451, align 4, !tbaa !8
  %call1452 = call float @mul_float(float 0x3FEF629740000000, float %964)
  %call1453 = call float @sub_float(float %call1450, float %call1452)
  %call1454 = call float @add_float(float %962, float %call1453)
  call void @store_float(float* %add.ptr1446, float %call1454)
  %965 = load float*, float** %output.addr, align 4, !tbaa !2
  %966 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1455 = mul nsw i32 24, %966
  %add.ptr1456 = getelementptr inbounds float, float* %965, i32 %mul1455
  %arrayidx1457 = getelementptr inbounds [2 x float], [2 x float]* %w79, i32 0, i32 0
  %967 = load float, float* %arrayidx1457, align 4, !tbaa !8
  %arrayidx1458 = getelementptr inbounds [2 x float], [2 x float]* %w143, i32 0, i32 1
  %968 = load float, float* %arrayidx1458, align 4, !tbaa !8
  %call1459 = call float @sub_float(float %967, float %968)
  call void @store_float(float* %add.ptr1456, float %call1459)
  %969 = load float*, float** %output.addr, align 4, !tbaa !2
  %970 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1460 = mul nsw i32 25, %970
  %add.ptr1461 = getelementptr inbounds float, float* %969, i32 %mul1460
  %arrayidx1462 = getelementptr inbounds [2 x float], [2 x float]* %w81, i32 0, i32 0
  %971 = load float, float* %arrayidx1462, align 4, !tbaa !8
  %arrayidx1463 = getelementptr inbounds [2 x float], [2 x float]* %w145, i32 0, i32 0
  %972 = load float, float* %arrayidx1463, align 4, !tbaa !8
  %call1464 = call float @mul_float(float 0x3FC8F8B580000000, float %972)
  %arrayidx1465 = getelementptr inbounds [2 x float], [2 x float]* %w145, i32 0, i32 1
  %973 = load float, float* %arrayidx1465, align 4, !tbaa !8
  %call1466 = call float @mul_float(float 0x3FEF629740000000, float %973)
  %call1467 = call float @sub_float(float %call1464, float %call1466)
  %call1468 = call float @add_float(float %971, float %call1467)
  call void @store_float(float* %add.ptr1461, float %call1468)
  %974 = load float*, float** %output.addr, align 4, !tbaa !2
  %975 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1469 = mul nsw i32 26, %975
  %add.ptr1470 = getelementptr inbounds float, float* %974, i32 %mul1469
  %arrayidx1471 = getelementptr inbounds [2 x float], [2 x float]* %w83, i32 0, i32 0
  %976 = load float, float* %arrayidx1471, align 4, !tbaa !8
  %arrayidx1472 = getelementptr inbounds [2 x float], [2 x float]* %w147, i32 0, i32 0
  %977 = load float, float* %arrayidx1472, align 4, !tbaa !8
  %call1473 = call float @mul_float(float 0x3FD87DE0E0000000, float %977)
  %arrayidx1474 = getelementptr inbounds [2 x float], [2 x float]* %w147, i32 0, i32 1
  %978 = load float, float* %arrayidx1474, align 4, !tbaa !8
  %call1475 = call float @mul_float(float 0x3FED906CC0000000, float %978)
  %call1476 = call float @sub_float(float %call1473, float %call1475)
  %call1477 = call float @add_float(float %976, float %call1476)
  call void @store_float(float* %add.ptr1470, float %call1477)
  %979 = load float*, float** %output.addr, align 4, !tbaa !2
  %980 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1478 = mul nsw i32 27, %980
  %add.ptr1479 = getelementptr inbounds float, float* %979, i32 %mul1478
  %arrayidx1480 = getelementptr inbounds [2 x float], [2 x float]* %w85, i32 0, i32 0
  %981 = load float, float* %arrayidx1480, align 4, !tbaa !8
  %arrayidx1481 = getelementptr inbounds [2 x float], [2 x float]* %w149, i32 0, i32 0
  %982 = load float, float* %arrayidx1481, align 4, !tbaa !8
  %call1482 = call float @mul_float(float 0x3FE1C73AC0000000, float %982)
  %arrayidx1483 = getelementptr inbounds [2 x float], [2 x float]* %w149, i32 0, i32 1
  %983 = load float, float* %arrayidx1483, align 4, !tbaa !8
  %call1484 = call float @mul_float(float 0x3FEA9B6700000000, float %983)
  %call1485 = call float @sub_float(float %call1482, float %call1484)
  %call1486 = call float @add_float(float %981, float %call1485)
  call void @store_float(float* %add.ptr1479, float %call1486)
  %984 = load float*, float** %output.addr, align 4, !tbaa !2
  %985 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1487 = mul nsw i32 28, %985
  %add.ptr1488 = getelementptr inbounds float, float* %984, i32 %mul1487
  %arrayidx1489 = getelementptr inbounds [2 x float], [2 x float]* %w87, i32 0, i32 0
  %986 = load float, float* %arrayidx1489, align 4, !tbaa !8
  %arrayidx1490 = getelementptr inbounds [2 x float], [2 x float]* %w151, i32 0, i32 0
  %987 = load float, float* %arrayidx1490, align 4, !tbaa !8
  %arrayidx1491 = getelementptr inbounds [2 x float], [2 x float]* %w151, i32 0, i32 1
  %988 = load float, float* %arrayidx1491, align 4, !tbaa !8
  %call1492 = call float @sub_float(float %987, float %988)
  %call1493 = call float @mul_float(float 0x3FE6A09EE0000000, float %call1492)
  %call1494 = call float @add_float(float %986, float %call1493)
  call void @store_float(float* %add.ptr1488, float %call1494)
  %989 = load float*, float** %output.addr, align 4, !tbaa !2
  %990 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1495 = mul nsw i32 29, %990
  %add.ptr1496 = getelementptr inbounds float, float* %989, i32 %mul1495
  %arrayidx1497 = getelementptr inbounds [2 x float], [2 x float]* %w89, i32 0, i32 0
  %991 = load float, float* %arrayidx1497, align 4, !tbaa !8
  %arrayidx1498 = getelementptr inbounds [2 x float], [2 x float]* %w153, i32 0, i32 0
  %992 = load float, float* %arrayidx1498, align 4, !tbaa !8
  %call1499 = call float @mul_float(float 0x3FEA9B6700000000, float %992)
  %arrayidx1500 = getelementptr inbounds [2 x float], [2 x float]* %w153, i32 0, i32 1
  %993 = load float, float* %arrayidx1500, align 4, !tbaa !8
  %call1501 = call float @mul_float(float 0x3FE1C73AC0000000, float %993)
  %call1502 = call float @sub_float(float %call1499, float %call1501)
  %call1503 = call float @add_float(float %991, float %call1502)
  call void @store_float(float* %add.ptr1496, float %call1503)
  %994 = load float*, float** %output.addr, align 4, !tbaa !2
  %995 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1504 = mul nsw i32 30, %995
  %add.ptr1505 = getelementptr inbounds float, float* %994, i32 %mul1504
  %arrayidx1506 = getelementptr inbounds [2 x float], [2 x float]* %w91, i32 0, i32 0
  %996 = load float, float* %arrayidx1506, align 4, !tbaa !8
  %arrayidx1507 = getelementptr inbounds [2 x float], [2 x float]* %w155, i32 0, i32 0
  %997 = load float, float* %arrayidx1507, align 4, !tbaa !8
  %call1508 = call float @mul_float(float 0x3FED906CC0000000, float %997)
  %arrayidx1509 = getelementptr inbounds [2 x float], [2 x float]* %w155, i32 0, i32 1
  %998 = load float, float* %arrayidx1509, align 4, !tbaa !8
  %call1510 = call float @mul_float(float 0x3FD87DE0E0000000, float %998)
  %call1511 = call float @sub_float(float %call1508, float %call1510)
  %call1512 = call float @add_float(float %996, float %call1511)
  call void @store_float(float* %add.ptr1505, float %call1512)
  %999 = load float*, float** %output.addr, align 4, !tbaa !2
  %1000 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul1513 = mul nsw i32 31, %1000
  %add.ptr1514 = getelementptr inbounds float, float* %999, i32 %mul1513
  %arrayidx1515 = getelementptr inbounds [2 x float], [2 x float]* %w93, i32 0, i32 0
  %1001 = load float, float* %arrayidx1515, align 4, !tbaa !8
  %arrayidx1516 = getelementptr inbounds [2 x float], [2 x float]* %w157, i32 0, i32 0
  %1002 = load float, float* %arrayidx1516, align 4, !tbaa !8
  %call1517 = call float @mul_float(float 0x3FEF629740000000, float %1002)
  %arrayidx1518 = getelementptr inbounds [2 x float], [2 x float]* %w157, i32 0, i32 1
  %1003 = load float, float* %arrayidx1518, align 4, !tbaa !8
  %call1519 = call float @mul_float(float 0x3FC8F8B580000000, float %1003)
  %call1520 = call float @sub_float(float %call1517, float %call1519)
  %call1521 = call float @add_float(float %1001, float %call1520)
  call void @store_float(float* %add.ptr1514, float %call1521)
  %1004 = bitcast [2 x float]* %w157 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1004) #3
  %1005 = bitcast [2 x float]* %w156 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1005) #3
  %1006 = bitcast [2 x float]* %w155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1006) #3
  %1007 = bitcast [2 x float]* %w154 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1007) #3
  %1008 = bitcast [2 x float]* %w153 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1008) #3
  %1009 = bitcast [2 x float]* %w152 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1009) #3
  %1010 = bitcast [2 x float]* %w151 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1010) #3
  %1011 = bitcast [2 x float]* %w150 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1011) #3
  %1012 = bitcast [2 x float]* %w149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1012) #3
  %1013 = bitcast [2 x float]* %w148 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1013) #3
  %1014 = bitcast [2 x float]* %w147 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1014) #3
  %1015 = bitcast [2 x float]* %w146 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1015) #3
  %1016 = bitcast [2 x float]* %w145 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1016) #3
  %1017 = bitcast [2 x float]* %w144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1017) #3
  %1018 = bitcast [2 x float]* %w143 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1018) #3
  %1019 = bitcast [2 x float]* %w142 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1019) #3
  %1020 = bitcast [2 x float]* %w141 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1020) #3
  %1021 = bitcast [2 x float]* %w140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1021) #3
  %1022 = bitcast [2 x float]* %w139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1022) #3
  %1023 = bitcast [2 x float]* %w138 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1023) #3
  %1024 = bitcast [2 x float]* %w137 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1024) #3
  %1025 = bitcast [2 x float]* %w136 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1025) #3
  %1026 = bitcast [2 x float]* %w135 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1026) #3
  %1027 = bitcast [2 x float]* %w134 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1027) #3
  %1028 = bitcast [2 x float]* %w133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1028) #3
  %1029 = bitcast [2 x float]* %w132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1029) #3
  %1030 = bitcast [2 x float]* %w131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1030) #3
  %1031 = bitcast [2 x float]* %w130 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1031) #3
  %1032 = bitcast [2 x float]* %w129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1032) #3
  %1033 = bitcast [2 x float]* %w128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1033) #3
  %1034 = bitcast [2 x float]* %w127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1034) #3
  %1035 = bitcast [2 x float]* %w126 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1035) #3
  %1036 = bitcast [2 x float]* %w125 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1036) #3
  %1037 = bitcast [2 x float]* %w124 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1037) #3
  %1038 = bitcast [2 x float]* %w123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1038) #3
  %1039 = bitcast [2 x float]* %w122 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1039) #3
  %1040 = bitcast [2 x float]* %w121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1040) #3
  %1041 = bitcast [2 x float]* %w120 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1041) #3
  %1042 = bitcast [2 x float]* %w119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1042) #3
  %1043 = bitcast [2 x float]* %w118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1043) #3
  %1044 = bitcast [2 x float]* %w117 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1044) #3
  %1045 = bitcast [2 x float]* %w116 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1045) #3
  %1046 = bitcast [2 x float]* %w115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1046) #3
  %1047 = bitcast [2 x float]* %w114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1047) #3
  %1048 = bitcast [2 x float]* %w113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1048) #3
  %1049 = bitcast [2 x float]* %w112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1049) #3
  %1050 = bitcast [2 x float]* %w111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1050) #3
  %1051 = bitcast [2 x float]* %w110 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1051) #3
  %1052 = bitcast [2 x float]* %w109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1052) #3
  %1053 = bitcast [2 x float]* %w108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1053) #3
  %1054 = bitcast [2 x float]* %w107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1054) #3
  %1055 = bitcast [2 x float]* %w106 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1055) #3
  %1056 = bitcast [2 x float]* %w105 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1056) #3
  %1057 = bitcast [2 x float]* %w104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1057) #3
  %1058 = bitcast [2 x float]* %w103 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1058) #3
  %1059 = bitcast [2 x float]* %w102 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1059) #3
  %1060 = bitcast [2 x float]* %w101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1060) #3
  %1061 = bitcast [2 x float]* %w100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1061) #3
  %1062 = bitcast [2 x float]* %w99 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1062) #3
  %1063 = bitcast [2 x float]* %w98 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1063) #3
  %1064 = bitcast [2 x float]* %w97 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1064) #3
  %1065 = bitcast [2 x float]* %w96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1065) #3
  %1066 = bitcast [2 x float]* %w95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1066) #3
  %1067 = bitcast [2 x float]* %w94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1067) #3
  %1068 = bitcast [2 x float]* %w93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1068) #3
  %1069 = bitcast [2 x float]* %w92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1069) #3
  %1070 = bitcast [2 x float]* %w91 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1070) #3
  %1071 = bitcast [2 x float]* %w90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1071) #3
  %1072 = bitcast [2 x float]* %w89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1072) #3
  %1073 = bitcast [2 x float]* %w88 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1073) #3
  %1074 = bitcast [2 x float]* %w87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1074) #3
  %1075 = bitcast [2 x float]* %w86 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1075) #3
  %1076 = bitcast [2 x float]* %w85 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1076) #3
  %1077 = bitcast [2 x float]* %w84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1077) #3
  %1078 = bitcast [2 x float]* %w83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1078) #3
  %1079 = bitcast [2 x float]* %w82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1079) #3
  %1080 = bitcast [2 x float]* %w81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1080) #3
  %1081 = bitcast [2 x float]* %w80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1081) #3
  %1082 = bitcast [2 x float]* %w79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1082) #3
  %1083 = bitcast [2 x float]* %w78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1083) #3
  %1084 = bitcast [2 x float]* %w77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1084) #3
  %1085 = bitcast [2 x float]* %w76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1085) #3
  %1086 = bitcast [2 x float]* %w75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1086) #3
  %1087 = bitcast [2 x float]* %w74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1087) #3
  %1088 = bitcast [2 x float]* %w73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1088) #3
  %1089 = bitcast [2 x float]* %w72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1089) #3
  %1090 = bitcast [2 x float]* %w71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1090) #3
  %1091 = bitcast [2 x float]* %w70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1091) #3
  %1092 = bitcast [2 x float]* %w69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1092) #3
  %1093 = bitcast [2 x float]* %w68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1093) #3
  %1094 = bitcast [2 x float]* %w67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1094) #3
  %1095 = bitcast [2 x float]* %w66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1095) #3
  %1096 = bitcast [2 x float]* %w65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1096) #3
  %1097 = bitcast [2 x float]* %w64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1097) #3
  %1098 = bitcast [2 x float]* %w63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1098) #3
  %1099 = bitcast [2 x float]* %w62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1099) #3
  %1100 = bitcast [2 x float]* %w61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1100) #3
  %1101 = bitcast [2 x float]* %w60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1101) #3
  %1102 = bitcast [2 x float]* %w59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1102) #3
  %1103 = bitcast [2 x float]* %w58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1103) #3
  %1104 = bitcast [2 x float]* %w57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1104) #3
  %1105 = bitcast [2 x float]* %w56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1105) #3
  %1106 = bitcast [2 x float]* %w55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1106) #3
  %1107 = bitcast [2 x float]* %w54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1107) #3
  %1108 = bitcast [2 x float]* %w53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1108) #3
  %1109 = bitcast [2 x float]* %w52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1109) #3
  %1110 = bitcast [2 x float]* %w51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1110) #3
  %1111 = bitcast [2 x float]* %w50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1111) #3
  %1112 = bitcast [2 x float]* %w49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1112) #3
  %1113 = bitcast [2 x float]* %w48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1113) #3
  %1114 = bitcast [2 x float]* %w47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1114) #3
  %1115 = bitcast [2 x float]* %w46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1115) #3
  %1116 = bitcast [2 x float]* %w45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1116) #3
  %1117 = bitcast [2 x float]* %w44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1117) #3
  %1118 = bitcast [2 x float]* %w43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1118) #3
  %1119 = bitcast [2 x float]* %w42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1119) #3
  %1120 = bitcast [2 x float]* %w41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1120) #3
  %1121 = bitcast [2 x float]* %w40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1121) #3
  %1122 = bitcast [2 x float]* %w39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1122) #3
  %1123 = bitcast [2 x float]* %w38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1123) #3
  %1124 = bitcast [2 x float]* %w37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1124) #3
  %1125 = bitcast [2 x float]* %w36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1125) #3
  %1126 = bitcast [2 x float]* %w35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1126) #3
  %1127 = bitcast [2 x float]* %w34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1127) #3
  %1128 = bitcast [2 x float]* %w33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1128) #3
  %1129 = bitcast [2 x float]* %w32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %1129) #3
  %1130 = bitcast float* %w31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1130) #3
  %1131 = bitcast float* %w30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1131) #3
  %1132 = bitcast float* %i31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1132) #3
  %1133 = bitcast float* %i30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1133) #3
  %1134 = bitcast float* %i29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1134) #3
  %1135 = bitcast float* %i28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1135) #3
  %1136 = bitcast float* %i27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1136) #3
  %1137 = bitcast float* %i26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1137) #3
  %1138 = bitcast float* %i25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1138) #3
  %1139 = bitcast float* %i24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1139) #3
  %1140 = bitcast float* %i23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1140) #3
  %1141 = bitcast float* %i22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1141) #3
  %1142 = bitcast float* %i21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1142) #3
  %1143 = bitcast float* %i20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1143) #3
  %1144 = bitcast float* %i19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1144) #3
  %1145 = bitcast float* %i18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1145) #3
  %1146 = bitcast float* %i17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1146) #3
  %1147 = bitcast float* %i16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1147) #3
  %1148 = bitcast float* %i15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1148) #3
  %1149 = bitcast float* %i14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1149) #3
  %1150 = bitcast float* %i13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1150) #3
  %1151 = bitcast float* %i12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1151) #3
  %1152 = bitcast float* %i11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1152) #3
  %1153 = bitcast float* %i10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1153) #3
  %1154 = bitcast float* %i9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1154) #3
  %1155 = bitcast float* %i8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1155) #3
  %1156 = bitcast float* %i7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1156) #3
  %1157 = bitcast float* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1157) #3
  %1158 = bitcast float* %i5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1158) #3
  %1159 = bitcast float* %i4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1159) #3
  %1160 = bitcast float* %i3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1160) #3
  %1161 = bitcast float* %i2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1161) #3
  %1162 = bitcast float* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1162) #3
  %1163 = bitcast float* %i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1163) #3
  %1164 = bitcast float* %kWeight8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1164) #3
  %1165 = bitcast float* %kWeight7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1165) #3
  %1166 = bitcast float* %kWeight6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1166) #3
  %1167 = bitcast float* %kWeight5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1167) #3
  %1168 = bitcast float* %kWeight4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1168) #3
  %1169 = bitcast float* %kWeight3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1169) #3
  %1170 = bitcast float* %kWeight2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1170) #3
  %1171 = bitcast float* %kWeight0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1171) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft2x2_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_ifft_2d_gen(float* %0, float* %1, float* %2, i32 2, void (float*, float*, i32)* @aom_fft1d_2_float, void (float*, float*, i32)* @aom_fft1d_2_float, void (float*, float*, i32)* @aom_ifft1d_2_float, void (float*, float*, i32)* @simple_transpose, i32 1)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft4x4_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_ifft_2d_gen(float* %0, float* %1, float* %2, i32 4, void (float*, float*, i32)* @aom_fft1d_4_float, void (float*, float*, i32)* @aom_fft1d_4_float, void (float*, float*, i32)* @aom_ifft1d_4_float, void (float*, float*, i32)* @simple_transpose, i32 1)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft8x8_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_ifft_2d_gen(float* %0, float* %1, float* %2, i32 8, void (float*, float*, i32)* @aom_fft1d_8_float, void (float*, float*, i32)* @aom_fft1d_8_float, void (float*, float*, i32)* @aom_ifft1d_8_float, void (float*, float*, i32)* @simple_transpose, i32 1)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft16x16_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_ifft_2d_gen(float* %0, float* %1, float* %2, i32 16, void (float*, float*, i32)* @aom_fft1d_16_float, void (float*, float*, i32)* @aom_fft1d_16_float, void (float*, float*, i32)* @aom_ifft1d_16_float, void (float*, float*, i32)* @simple_transpose, i32 1)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_ifft32x32_float_c(float* %input, float* %temp, float* %output) #0 {
entry:
  %input.addr = alloca float*, align 4
  %temp.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  store float* %input, float** %input.addr, align 4, !tbaa !2
  store float* %temp, float** %temp.addr, align 4, !tbaa !2
  store float* %output, float** %output.addr, align 4, !tbaa !2
  %0 = load float*, float** %input.addr, align 4, !tbaa !2
  %1 = load float*, float** %temp.addr, align 4, !tbaa !2
  %2 = load float*, float** %output.addr, align 4, !tbaa !2
  call void @aom_ifft_2d_gen(float* %0, float* %1, float* %2, i32 32, void (float*, float*, i32)* @aom_fft1d_32_float, void (float*, float*, i32)* @aom_fft1d_32_float, void (float*, float*, i32)* @aom_ifft1d_32_float, void (float*, float*, i32)* @simple_transpose, i32 1)
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
