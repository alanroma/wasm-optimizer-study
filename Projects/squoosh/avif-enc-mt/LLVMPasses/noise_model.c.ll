; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/noise_model.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/noise_model.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%struct.aom_noise_strength_lut_t = type { [2 x double]*, i32 }
%struct.aom_noise_strength_solver_t = type { %struct.aom_equation_system_t, double, double, i32, i32, double }
%struct.aom_equation_system_t = type { double*, double*, double*, i32 }
%struct.aom_flat_block_finder_t = type { double*, double*, i32, i32, double, i32 }
%struct.index_and_score_t = type { i32, float }
%struct.aom_noise_model_t = type { %struct.aom_noise_model_params_t, [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t], [2 x i32]*, i32, i32 }
%struct.aom_noise_model_params_t = type { i8, i32, i32, i32 }
%struct.aom_noise_state_t = type { %struct.aom_equation_system_t, %struct.aom_noise_strength_solver_t, i32, double }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.aom_noise_tx_t = type opaque
%struct.aom_denoise_and_model_t = type { i32, i32, float, i32, i32, i32, i32, i32, i32, [3 x float*], [3 x i8*], i8*, %struct.aom_flat_block_finder_t, %struct.aom_noise_model_t }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }

@stderr = external constant %struct._IO_FILE*, align 4
@.str = private unnamed_addr constant [30 x i8] c"Unable to allocate copy of A\0A\00", align 1
@.str.1 = private unnamed_addr constant [20 x i8] c"Failed to init lut\0A\00", align 1
@.str.2 = private unnamed_addr constant [50 x i8] c"Failed to init equation system for block_size=%d\0A\00", align 1
@.str.3 = private unnamed_addr constant [48 x i8] c"Failed to alloc A or AtA_inv for block_size=%d\0A\00", align 1
@.str.4 = private unnamed_addr constant [48 x i8] c"Failed to allocate memory for block of size %d\0A\00", align 1
@.str.5 = private unnamed_addr constant [44 x i8] c"Invalid noise param: lag = %d must be >= 1\0A\00", align 1
@.str.6 = private unnamed_addr constant [45 x i8] c"Invalid noise param: lag = %d must be <= %d\0A\00", align 1
@.str.7 = private unnamed_addr constant [47 x i8] c"Failed to allocate noise state for channel %d\0A\00", align 1
@.str.8 = private unnamed_addr constant [15 x i8] c"Invalid shape\0A\00", align 1
@.str.9 = private unnamed_addr constant [29 x i8] c"block_size = %d must be > 1\0A\00", align 1
@.str.10 = private unnamed_addr constant [31 x i8] c"block_size = %d must be >= %d\0A\00", align 1
@.str.11 = private unnamed_addr constant [49 x i8] c"Not enough flat blocks to update noise estimate\0A\00", align 1
@.str.12 = private unnamed_addr constant [33 x i8] c"Adding block observation failed\0A\00", align 1
@.str.13 = private unnamed_addr constant [49 x i8] c"Solving latest noise equation system failed %d!\0A\00", align 1
@.str.14 = private unnamed_addr constant [39 x i8] c"Solving latest noise strength failed!\0A\00", align 1
@.str.15 = private unnamed_addr constant [51 x i8] c"Solving combined noise equation system failed %d!\0A\00", align 1
@.str.16 = private unnamed_addr constant [41 x i8] c"Solving combined noise strength failed!\0A\00", align 1
@.str.17 = private unnamed_addr constant [21 x i8] c"params.lag = %d > 3\0A\00", align 1
@.str.18 = private unnamed_addr constant [66 x i8] c"aom_wiener_denoise_2d doesn't handle different chroma subsampling\00", align 1
@.str.19 = private unnamed_addr constant [45 x i8] c"Unable to allocate denoise_and_model struct\0A\00", align 1
@.str.20 = private unnamed_addr constant [38 x i8] c"Unable to allocate noise PSD buffers\0A\00", align 1
@.str.21 = private unnamed_addr constant [27 x i8] c"Unable to realloc buffers\0A\00", align 1
@.str.22 = private unnamed_addr constant [25 x i8] c"Unable to denoise image\0A\00", align 1
@.str.23 = private unnamed_addr constant [33 x i8] c"Unable to get grain parameters.\0A\00", align 1
@.str.24 = private unnamed_addr constant [46 x i8] c"Unable to allocate temp values of size %dx%d\0A\00", align 1
@.str.25 = private unnamed_addr constant [51 x i8] c"Failed to allocate system of equations of size %d\0A\00", align 1
@.str.26 = private unnamed_addr constant [48 x i8] c"Failed initialization noise state with size %d\0A\00", align 1
@.str.27 = private unnamed_addr constant [38 x i8] c"Unable to allocate buffer of size %d\0A\00", align 1
@.str.28 = private unnamed_addr constant [36 x i8] c"Unable to allocate denoise buffers\0A\00", align 1
@.str.29 = private unnamed_addr constant [34 x i8] c"Unable to init flat block finder\0A\00", align 1
@.str.30 = private unnamed_addr constant [28 x i8] c"Unable to init noise model\0A\00", align 1

; Function Attrs: nounwind
define hidden i32 @aom_noise_strength_lut_init(%struct.aom_noise_strength_lut_t* %lut, i32 %num_points) #0 {
entry:
  %retval = alloca i32, align 4
  %lut.addr = alloca %struct.aom_noise_strength_lut_t*, align 4
  %num_points.addr = alloca i32, align 4
  store %struct.aom_noise_strength_lut_t* %lut, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  store i32 %num_points, i32* %num_points.addr, align 4, !tbaa !6
  %0 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_noise_strength_lut_t* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points1 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %1, i32 0, i32 1
  store i32 0, i32* %num_points1, align 4, !tbaa !8
  %2 = load i32, i32* %num_points.addr, align 4, !tbaa !6
  %mul = mul i32 %2, 16
  %call = call i8* @aom_malloc(i32 %mul)
  %3 = bitcast i8* %call to [2 x double]*
  %4 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %4, i32 0, i32 0
  store [2 x double]* %3, [2 x double]** %points, align 4, !tbaa !10
  %5 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points2 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %5, i32 0, i32 0
  %6 = load [2 x double]*, [2 x double]** %points2, align 4, !tbaa !10
  %tobool3 = icmp ne [2 x double]* %6, null
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %7 = load i32, i32* %num_points.addr, align 4, !tbaa !6
  %8 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points6 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %8, i32 0, i32 1
  store i32 %7, i32* %num_points6, align 4, !tbaa !8
  %9 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points7 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %9, i32 0, i32 0
  %10 = load [2 x double]*, [2 x double]** %points7, align 4, !tbaa !10
  %11 = bitcast [2 x double]* %10 to i8*
  %12 = load i32, i32* %num_points.addr, align 4, !tbaa !6
  %mul8 = mul i32 16, %12
  call void @llvm.memset.p0i8.i32(i8* align 8 %11, i8 0, i32 %mul8, i1 false)
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4, %if.then
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

declare i8* @aom_malloc(i32) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: nounwind
define hidden void @aom_noise_strength_lut_free(%struct.aom_noise_strength_lut_t* %lut) #0 {
entry:
  %lut.addr = alloca %struct.aom_noise_strength_lut_t*, align 4
  store %struct.aom_noise_strength_lut_t* %lut, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %0 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_noise_strength_lut_t* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %1, i32 0, i32 0
  %2 = load [2 x double]*, [2 x double]** %points, align 4, !tbaa !10
  %3 = bitcast [2 x double]* %2 to i8*
  call void @aom_free(i8* %3)
  %4 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %5 = bitcast %struct.aom_noise_strength_lut_t* %4 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %5, i8 0, i32 8, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

declare void @aom_free(i8*) #1

; Function Attrs: nounwind
define hidden double @aom_noise_strength_lut_eval(%struct.aom_noise_strength_lut_t* %lut, double %x) #0 {
entry:
  %retval = alloca double, align 8
  %lut.addr = alloca %struct.aom_noise_strength_lut_t*, align 4
  %x.addr = alloca double, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %a = alloca double, align 8
  store %struct.aom_noise_strength_lut_t* %lut, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  store double %x, double* %x.addr, align 8, !tbaa !11
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  %1 = load double, double* %x.addr, align 8, !tbaa !11
  %2 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %2, i32 0, i32 0
  %3 = load [2 x double]*, [2 x double]** %points, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [2 x double], [2 x double]* %3, i32 0
  %arrayidx1 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx, i32 0, i32 0
  %4 = load double, double* %arrayidx1, align 8, !tbaa !11
  %cmp = fcmp olt double %1, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points2 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %5, i32 0, i32 0
  %6 = load [2 x double]*, [2 x double]** %points2, align 4, !tbaa !10
  %arrayidx3 = getelementptr inbounds [2 x double], [2 x double]* %6, i32 0
  %arrayidx4 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx3, i32 0, i32 1
  %7 = load double, double* %arrayidx4, align 8, !tbaa !11
  store double %7, double* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %9 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %9, i32 0, i32 1
  %10 = load i32, i32* %num_points, align 4, !tbaa !8
  %sub = sub nsw i32 %10, 1
  %cmp5 = icmp slt i32 %8, %sub
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load double, double* %x.addr, align 8, !tbaa !11
  %12 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points6 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %12, i32 0, i32 0
  %13 = load [2 x double]*, [2 x double]** %points6, align 4, !tbaa !10
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [2 x double], [2 x double]* %13, i32 %14
  %arrayidx8 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx7, i32 0, i32 0
  %15 = load double, double* %arrayidx8, align 8, !tbaa !11
  %cmp9 = fcmp oge double %11, %15
  br i1 %cmp9, label %land.lhs.true, label %if.end37

land.lhs.true:                                    ; preds = %for.body
  %16 = load double, double* %x.addr, align 8, !tbaa !11
  %17 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points10 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %17, i32 0, i32 0
  %18 = load [2 x double]*, [2 x double]** %points10, align 4, !tbaa !10
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %19, 1
  %arrayidx11 = getelementptr inbounds [2 x double], [2 x double]* %18, i32 %add
  %arrayidx12 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx11, i32 0, i32 0
  %20 = load double, double* %arrayidx12, align 8, !tbaa !11
  %cmp13 = fcmp ole double %16, %20
  br i1 %cmp13, label %if.then14, label %if.end37

if.then14:                                        ; preds = %land.lhs.true
  %21 = bitcast double* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %21) #7
  %22 = load double, double* %x.addr, align 8, !tbaa !11
  %23 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points15 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %23, i32 0, i32 0
  %24 = load [2 x double]*, [2 x double]** %points15, align 4, !tbaa !10
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [2 x double], [2 x double]* %24, i32 %25
  %arrayidx17 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx16, i32 0, i32 0
  %26 = load double, double* %arrayidx17, align 8, !tbaa !11
  %sub18 = fsub double %22, %26
  %27 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points19 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %27, i32 0, i32 0
  %28 = load [2 x double]*, [2 x double]** %points19, align 4, !tbaa !10
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %add20 = add nsw i32 %29, 1
  %arrayidx21 = getelementptr inbounds [2 x double], [2 x double]* %28, i32 %add20
  %arrayidx22 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx21, i32 0, i32 0
  %30 = load double, double* %arrayidx22, align 8, !tbaa !11
  %31 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points23 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %31, i32 0, i32 0
  %32 = load [2 x double]*, [2 x double]** %points23, align 4, !tbaa !10
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds [2 x double], [2 x double]* %32, i32 %33
  %arrayidx25 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx24, i32 0, i32 0
  %34 = load double, double* %arrayidx25, align 8, !tbaa !11
  %sub26 = fsub double %30, %34
  %div = fdiv double %sub18, %sub26
  store double %div, double* %a, align 8, !tbaa !11
  %35 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points27 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %35, i32 0, i32 0
  %36 = load [2 x double]*, [2 x double]** %points27, align 4, !tbaa !10
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %add28 = add nsw i32 %37, 1
  %arrayidx29 = getelementptr inbounds [2 x double], [2 x double]* %36, i32 %add28
  %arrayidx30 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx29, i32 0, i32 1
  %38 = load double, double* %arrayidx30, align 8, !tbaa !11
  %39 = load double, double* %a, align 8, !tbaa !11
  %mul = fmul double %38, %39
  %40 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points31 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %40, i32 0, i32 0
  %41 = load [2 x double]*, [2 x double]** %points31, align 4, !tbaa !10
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds [2 x double], [2 x double]* %41, i32 %42
  %arrayidx33 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx32, i32 0, i32 1
  %43 = load double, double* %arrayidx33, align 8, !tbaa !11
  %44 = load double, double* %a, align 8, !tbaa !11
  %sub34 = fsub double 1.000000e+00, %44
  %mul35 = fmul double %43, %sub34
  %add36 = fadd double %mul, %mul35
  store double %add36, double* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  %45 = bitcast double* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %45) #7
  br label %cleanup

if.end37:                                         ; preds = %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end37
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %47 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points38 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %47, i32 0, i32 0
  %48 = load [2 x double]*, [2 x double]** %points38, align 4, !tbaa !10
  %49 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points39 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %49, i32 0, i32 1
  %50 = load i32, i32* %num_points39, align 4, !tbaa !8
  %sub40 = sub nsw i32 %50, 1
  %arrayidx41 = getelementptr inbounds [2 x double], [2 x double]* %48, i32 %sub40
  %arrayidx42 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx41, i32 0, i32 1
  %51 = load double, double* %arrayidx42, align 8, !tbaa !11
  store double %51, double* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then14, %if.then
  %52 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = load double, double* %retval, align 8
  ret double %53
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: nounwind
define hidden void @aom_noise_strength_solver_add_measurement(%struct.aom_noise_strength_solver_t* %solver, double %block_mean, double %noise_std) #0 {
entry:
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  %block_mean.addr = alloca double, align 8
  %noise_std.addr = alloca double, align 8
  %bin = alloca double, align 8
  %bin_i0 = alloca i32, align 4
  %bin_i1 = alloca i32, align 4
  %a = alloca double, align 8
  %n = alloca i32, align 4
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  store double %block_mean, double* %block_mean.addr, align 8, !tbaa !11
  store double %noise_std, double* %noise_std.addr, align 8, !tbaa !11
  %0 = bitcast double* %bin to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %2 = load double, double* %block_mean.addr, align 8, !tbaa !11
  %call = call double @noise_strength_solver_get_bin_index(%struct.aom_noise_strength_solver_t* %1, double %2)
  store double %call, double* %bin, align 8, !tbaa !11
  %3 = bitcast i32* %bin_i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load double, double* %bin, align 8, !tbaa !11
  %5 = call double @llvm.floor.f64(double %4)
  %conv = fptosi double %5 to i32
  store i32 %conv, i32* %bin_i0, align 4, !tbaa !6
  %6 = bitcast i32* %bin_i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %7, i32 0, i32 3
  %8 = load i32, i32* %num_bins, align 8, !tbaa !13
  %sub = sub nsw i32 %8, 1
  %9 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %add = add nsw i32 %9, 1
  %cmp = icmp slt i32 %sub, %add
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins2 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %10, i32 0, i32 3
  %11 = load i32, i32* %num_bins2, align 8, !tbaa !13
  %sub3 = sub nsw i32 %11, 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %12 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %add4 = add nsw i32 %12, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub3, %cond.true ], [ %add4, %cond.false ]
  store i32 %cond, i32* %bin_i1, align 4, !tbaa !6
  %13 = bitcast double* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #7
  %14 = load double, double* %bin, align 8, !tbaa !11
  %15 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %conv5 = sitofp i32 %15 to double
  %sub6 = fsub double %14, %conv5
  store double %sub6, double* %a, align 8, !tbaa !11
  %16 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins7 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %17, i32 0, i32 3
  %18 = load i32, i32* %num_bins7, align 8, !tbaa !13
  store i32 %18, i32* %n, align 4, !tbaa !6
  %19 = load double, double* %a, align 8, !tbaa !11
  %sub8 = fsub double 1.000000e+00, %19
  %20 = load double, double* %a, align 8, !tbaa !11
  %sub9 = fsub double 1.000000e+00, %20
  %mul = fmul double %sub8, %sub9
  %21 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %21, i32 0, i32 0
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 0
  %22 = load double*, double** %A, align 8, !tbaa !16
  %23 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %24 = load i32, i32* %n, align 4, !tbaa !6
  %mul10 = mul nsw i32 %23, %24
  %25 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %add11 = add nsw i32 %mul10, %25
  %arrayidx = getelementptr inbounds double, double* %22, i32 %add11
  %26 = load double, double* %arrayidx, align 8, !tbaa !11
  %add12 = fadd double %26, %mul
  store double %add12, double* %arrayidx, align 8, !tbaa !11
  %27 = load double, double* %a, align 8, !tbaa !11
  %28 = load double, double* %a, align 8, !tbaa !11
  %sub13 = fsub double 1.000000e+00, %28
  %mul14 = fmul double %27, %sub13
  %29 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns15 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %29, i32 0, i32 0
  %A16 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns15, i32 0, i32 0
  %30 = load double*, double** %A16, align 8, !tbaa !16
  %31 = load i32, i32* %bin_i1, align 4, !tbaa !6
  %32 = load i32, i32* %n, align 4, !tbaa !6
  %mul17 = mul nsw i32 %31, %32
  %33 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %add18 = add nsw i32 %mul17, %33
  %arrayidx19 = getelementptr inbounds double, double* %30, i32 %add18
  %34 = load double, double* %arrayidx19, align 8, !tbaa !11
  %add20 = fadd double %34, %mul14
  store double %add20, double* %arrayidx19, align 8, !tbaa !11
  %35 = load double, double* %a, align 8, !tbaa !11
  %36 = load double, double* %a, align 8, !tbaa !11
  %mul21 = fmul double %35, %36
  %37 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns22 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %37, i32 0, i32 0
  %A23 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns22, i32 0, i32 0
  %38 = load double*, double** %A23, align 8, !tbaa !16
  %39 = load i32, i32* %bin_i1, align 4, !tbaa !6
  %40 = load i32, i32* %n, align 4, !tbaa !6
  %mul24 = mul nsw i32 %39, %40
  %41 = load i32, i32* %bin_i1, align 4, !tbaa !6
  %add25 = add nsw i32 %mul24, %41
  %arrayidx26 = getelementptr inbounds double, double* %38, i32 %add25
  %42 = load double, double* %arrayidx26, align 8, !tbaa !11
  %add27 = fadd double %42, %mul21
  store double %add27, double* %arrayidx26, align 8, !tbaa !11
  %43 = load double, double* %a, align 8, !tbaa !11
  %44 = load double, double* %a, align 8, !tbaa !11
  %sub28 = fsub double 1.000000e+00, %44
  %mul29 = fmul double %43, %sub28
  %45 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns30 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %45, i32 0, i32 0
  %A31 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns30, i32 0, i32 0
  %46 = load double*, double** %A31, align 8, !tbaa !16
  %47 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %48 = load i32, i32* %n, align 4, !tbaa !6
  %mul32 = mul nsw i32 %47, %48
  %49 = load i32, i32* %bin_i1, align 4, !tbaa !6
  %add33 = add nsw i32 %mul32, %49
  %arrayidx34 = getelementptr inbounds double, double* %46, i32 %add33
  %50 = load double, double* %arrayidx34, align 8, !tbaa !11
  %add35 = fadd double %50, %mul29
  store double %add35, double* %arrayidx34, align 8, !tbaa !11
  %51 = load double, double* %a, align 8, !tbaa !11
  %sub36 = fsub double 1.000000e+00, %51
  %52 = load double, double* %noise_std.addr, align 8, !tbaa !11
  %mul37 = fmul double %sub36, %52
  %53 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns38 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %53, i32 0, i32 0
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns38, i32 0, i32 1
  %54 = load double*, double** %b, align 4, !tbaa !17
  %55 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds double, double* %54, i32 %55
  %56 = load double, double* %arrayidx39, align 8, !tbaa !11
  %add40 = fadd double %56, %mul37
  store double %add40, double* %arrayidx39, align 8, !tbaa !11
  %57 = load double, double* %a, align 8, !tbaa !11
  %58 = load double, double* %noise_std.addr, align 8, !tbaa !11
  %mul41 = fmul double %57, %58
  %59 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns42 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %59, i32 0, i32 0
  %b43 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns42, i32 0, i32 1
  %60 = load double*, double** %b43, align 4, !tbaa !17
  %61 = load i32, i32* %bin_i1, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds double, double* %60, i32 %61
  %62 = load double, double* %arrayidx44, align 8, !tbaa !11
  %add45 = fadd double %62, %mul41
  store double %add45, double* %arrayidx44, align 8, !tbaa !11
  %63 = load double, double* %noise_std.addr, align 8, !tbaa !11
  %64 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %total = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %64, i32 0, i32 5
  %65 = load double, double* %total, align 8, !tbaa !18
  %add46 = fadd double %65, %63
  store double %add46, double* %total, align 8, !tbaa !18
  %66 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_equations = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %66, i32 0, i32 4
  %67 = load i32, i32* %num_equations, align 4, !tbaa !19
  %inc = add nsw i32 %67, 1
  store i32 %inc, i32* %num_equations, align 4, !tbaa !19
  %68 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #7
  %69 = bitcast double* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %69) #7
  %70 = bitcast i32* %bin_i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  %71 = bitcast i32* %bin_i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  %72 = bitcast double* %bin to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %72) #7
  ret void
}

; Function Attrs: nounwind
define internal double @noise_strength_solver_get_bin_index(%struct.aom_noise_strength_solver_t* %solver, double %value) #0 {
entry:
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  %value.addr = alloca double, align 8
  %val = alloca double, align 8
  %range = alloca double, align 8
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  store double %value, double* %value.addr, align 8, !tbaa !11
  %0 = bitcast double* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %1 = load double, double* %value.addr, align 8, !tbaa !11
  %2 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %min_intensity = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %2, i32 0, i32 1
  %3 = load double, double* %min_intensity, align 8, !tbaa !20
  %4 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %max_intensity = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %4, i32 0, i32 2
  %5 = load double, double* %max_intensity, align 8, !tbaa !21
  %call = call double @fclamp(double %1, double %3, double %5)
  store double %call, double* %val, align 8, !tbaa !11
  %6 = bitcast double* %range to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #7
  %7 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %max_intensity1 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %7, i32 0, i32 2
  %8 = load double, double* %max_intensity1, align 8, !tbaa !21
  %9 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %min_intensity2 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %9, i32 0, i32 1
  %10 = load double, double* %min_intensity2, align 8, !tbaa !20
  %sub = fsub double %8, %10
  store double %sub, double* %range, align 8, !tbaa !11
  %11 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %11, i32 0, i32 3
  %12 = load i32, i32* %num_bins, align 8, !tbaa !13
  %sub3 = sub nsw i32 %12, 1
  %conv = sitofp i32 %sub3 to double
  %13 = load double, double* %val, align 8, !tbaa !11
  %14 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %min_intensity4 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %14, i32 0, i32 1
  %15 = load double, double* %min_intensity4, align 8, !tbaa !20
  %sub5 = fsub double %13, %15
  %mul = fmul double %conv, %sub5
  %16 = load double, double* %range, align 8, !tbaa !11
  %div = fdiv double %mul, %16
  %17 = bitcast double* %range to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %17) #7
  %18 = bitcast double* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %18) #7
  ret double %div
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.floor.f64(double) #4

; Function Attrs: nounwind
define hidden i32 @aom_noise_strength_solver_solve(%struct.aom_noise_strength_solver_t* %solver) #0 {
entry:
  %retval = alloca i32, align 4
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  %n = alloca i32, align 4
  %kAlpha = alloca double, align 8
  %result = alloca i32, align 4
  %mean = alloca double, align 8
  %old_A = alloca double*, align 4
  %A2 = alloca double*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %i_lo = alloca i32, align 4
  %i_hi = alloca i32, align 4
  %i36 = alloca i32, align 4
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %1, i32 0, i32 3
  %2 = load i32, i32* %num_bins, align 8, !tbaa !13
  store i32 %2, i32* %n, align 4, !tbaa !6
  %3 = bitcast double* %kAlpha to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #7
  %4 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_equations = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %4, i32 0, i32 4
  %5 = load i32, i32* %num_equations, align 4, !tbaa !19
  %conv = sitofp i32 %5 to double
  %mul = fmul double 2.000000e+00, %conv
  %6 = load i32, i32* %n, align 4, !tbaa !6
  %conv1 = sitofp i32 %6 to double
  %div = fdiv double %mul, %conv1
  store double %div, double* %kAlpha, align 8, !tbaa !11
  %7 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store i32 0, i32* %result, align 4, !tbaa !6
  %8 = bitcast double* %mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #7
  store double 0.000000e+00, double* %mean, align 8, !tbaa !11
  %9 = bitcast double** %old_A to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %10, i32 0, i32 0
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 0
  %11 = load double*, double** %A, align 8, !tbaa !16
  store double* %11, double** %old_A, align 4, !tbaa !2
  %12 = bitcast double** %A2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load i32, i32* %n, align 4, !tbaa !6
  %mul3 = mul i32 8, %13
  %14 = load i32, i32* %n, align 4, !tbaa !6
  %mul4 = mul i32 %mul3, %14
  %call = call i8* @aom_malloc(i32 %mul4)
  %15 = bitcast i8* %call to double*
  store double* %15, double** %A2, align 4, !tbaa !2
  %16 = load double*, double** %A2, align 4, !tbaa !2
  %tobool = icmp ne double* %16, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %18 = load double*, double** %A2, align 4, !tbaa !2
  %19 = bitcast double* %18 to i8*
  %20 = load double*, double** %old_A, align 4, !tbaa !2
  %21 = bitcast double* %20 to i8*
  %22 = load i32, i32* %n, align 4, !tbaa !6
  %mul6 = mul i32 8, %22
  %23 = load i32, i32* %n, align 4, !tbaa !6
  %mul7 = mul i32 %mul6, %23
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %19, i8* align 8 %21, i32 %mul7, i1 false)
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %26 = load i32, i32* %n, align 4, !tbaa !6
  %cmp = icmp slt i32 %25, %26
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %28 = bitcast i32* %i_lo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %sub = sub nsw i32 %29, 1
  %cmp9 = icmp sgt i32 0, %sub
  br i1 %cmp9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %sub11 = sub nsw i32 %30, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %sub11, %cond.false ]
  store i32 %cond, i32* %i_lo, align 4, !tbaa !6
  %31 = bitcast i32* %i_hi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %32 = load i32, i32* %n, align 4, !tbaa !6
  %sub12 = sub nsw i32 %32, 1
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %33, 1
  %cmp13 = icmp slt i32 %sub12, %add
  br i1 %cmp13, label %cond.true15, label %cond.false17

cond.true15:                                      ; preds = %cond.end
  %34 = load i32, i32* %n, align 4, !tbaa !6
  %sub16 = sub nsw i32 %34, 1
  br label %cond.end19

cond.false17:                                     ; preds = %cond.end
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %add18 = add nsw i32 %35, 1
  br label %cond.end19

cond.end19:                                       ; preds = %cond.false17, %cond.true15
  %cond20 = phi i32 [ %sub16, %cond.true15 ], [ %add18, %cond.false17 ]
  store i32 %cond20, i32* %i_hi, align 4, !tbaa !6
  %36 = load double, double* %kAlpha, align 8, !tbaa !11
  %37 = load double*, double** %A2, align 4, !tbaa !2
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %39 = load i32, i32* %n, align 4, !tbaa !6
  %mul21 = mul nsw i32 %38, %39
  %40 = load i32, i32* %i_lo, align 4, !tbaa !6
  %add22 = add nsw i32 %mul21, %40
  %arrayidx = getelementptr inbounds double, double* %37, i32 %add22
  %41 = load double, double* %arrayidx, align 8, !tbaa !11
  %sub23 = fsub double %41, %36
  store double %sub23, double* %arrayidx, align 8, !tbaa !11
  %42 = load double, double* %kAlpha, align 8, !tbaa !11
  %mul24 = fmul double 2.000000e+00, %42
  %43 = load double*, double** %A2, align 4, !tbaa !2
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %45 = load i32, i32* %n, align 4, !tbaa !6
  %mul25 = mul nsw i32 %44, %45
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %add26 = add nsw i32 %mul25, %46
  %arrayidx27 = getelementptr inbounds double, double* %43, i32 %add26
  %47 = load double, double* %arrayidx27, align 8, !tbaa !11
  %add28 = fadd double %47, %mul24
  store double %add28, double* %arrayidx27, align 8, !tbaa !11
  %48 = load double, double* %kAlpha, align 8, !tbaa !11
  %49 = load double*, double** %A2, align 4, !tbaa !2
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %51 = load i32, i32* %n, align 4, !tbaa !6
  %mul29 = mul nsw i32 %50, %51
  %52 = load i32, i32* %i_hi, align 4, !tbaa !6
  %add30 = add nsw i32 %mul29, %52
  %arrayidx31 = getelementptr inbounds double, double* %49, i32 %add30
  %53 = load double, double* %arrayidx31, align 8, !tbaa !11
  %sub32 = fsub double %53, %48
  store double %sub32, double* %arrayidx31, align 8, !tbaa !11
  %54 = bitcast i32* %i_hi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  %55 = bitcast i32* %i_lo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #7
  br label %for.inc

for.inc:                                          ; preds = %cond.end19
  %56 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %56, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %57 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %total = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %57, i32 0, i32 5
  %58 = load double, double* %total, align 8, !tbaa !18
  %59 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_equations33 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %59, i32 0, i32 4
  %60 = load i32, i32* %num_equations33, align 4, !tbaa !19
  %conv34 = sitofp i32 %60 to double
  %div35 = fdiv double %58, %conv34
  store double %div35, double* %mean, align 8, !tbaa !11
  %61 = bitcast i32* %i36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #7
  store i32 0, i32* %i36, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc50, %for.end
  %62 = load i32, i32* %i36, align 4, !tbaa !6
  %63 = load i32, i32* %n, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %62, %63
  br i1 %cmp38, label %for.body41, label %for.cond.cleanup40

for.cond.cleanup40:                               ; preds = %for.cond37
  store i32 5, i32* %cleanup.dest.slot, align 4
  %64 = bitcast i32* %i36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #7
  br label %for.end52

for.body41:                                       ; preds = %for.cond37
  %65 = load double*, double** %A2, align 4, !tbaa !2
  %66 = load i32, i32* %i36, align 4, !tbaa !6
  %67 = load i32, i32* %n, align 4, !tbaa !6
  %mul42 = mul nsw i32 %66, %67
  %68 = load i32, i32* %i36, align 4, !tbaa !6
  %add43 = add nsw i32 %mul42, %68
  %arrayidx44 = getelementptr inbounds double, double* %65, i32 %add43
  %69 = load double, double* %arrayidx44, align 8, !tbaa !11
  %add45 = fadd double %69, 0x3F20000000000000
  store double %add45, double* %arrayidx44, align 8, !tbaa !11
  %70 = load double, double* %mean, align 8, !tbaa !11
  %div46 = fdiv double %70, 8.192000e+03
  %71 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns47 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %71, i32 0, i32 0
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns47, i32 0, i32 1
  %72 = load double*, double** %b, align 4, !tbaa !17
  %73 = load i32, i32* %i36, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds double, double* %72, i32 %73
  %74 = load double, double* %arrayidx48, align 8, !tbaa !11
  %add49 = fadd double %74, %div46
  store double %add49, double* %arrayidx48, align 8, !tbaa !11
  br label %for.inc50

for.inc50:                                        ; preds = %for.body41
  %75 = load i32, i32* %i36, align 4, !tbaa !6
  %inc51 = add nsw i32 %75, 1
  store i32 %inc51, i32* %i36, align 4, !tbaa !6
  br label %for.cond37

for.end52:                                        ; preds = %for.cond.cleanup40
  %76 = load double*, double** %A2, align 4, !tbaa !2
  %77 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns53 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %77, i32 0, i32 0
  %A54 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns53, i32 0, i32 0
  store double* %76, double** %A54, align 8, !tbaa !16
  %78 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns55 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %78, i32 0, i32 0
  %call56 = call i32 @equation_system_solve(%struct.aom_equation_system_t* %eqns55)
  store i32 %call56, i32* %result, align 4, !tbaa !6
  %79 = load double*, double** %old_A, align 4, !tbaa !2
  %80 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns57 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %80, i32 0, i32 0
  %A58 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns57, i32 0, i32 0
  store double* %79, double** %A58, align 8, !tbaa !16
  %81 = load double*, double** %A2, align 4, !tbaa !2
  %82 = bitcast double* %81 to i8*
  call void @aom_free(i8* %82)
  %83 = load i32, i32* %result, align 4, !tbaa !6
  store i32 %83, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end52, %if.then
  %84 = bitcast double** %A2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #7
  %85 = bitcast double** %old_A to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #7
  %86 = bitcast double* %mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %86) #7
  %87 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #7
  %88 = bitcast double* %kAlpha to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %88) #7
  %89 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #7
  %90 = load i32, i32* %retval, align 4
  ret i32 %90
}

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: nounwind
define internal i32 @equation_system_solve(%struct.aom_equation_system_t* %eqns) #0 {
entry:
  %retval = alloca i32, align 4
  %eqns.addr = alloca %struct.aom_equation_system_t*, align 4
  %n = alloca i32, align 4
  %b = alloca double*, align 4
  %A = alloca double*, align 4
  %ret = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_equation_system_t* %eqns, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %n1 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %1, i32 0, i32 3
  %2 = load i32, i32* %n1, align 4, !tbaa !22
  store i32 %2, i32* %n, align 4, !tbaa !6
  %3 = bitcast double** %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %n, align 4, !tbaa !6
  %mul = mul i32 8, %4
  %call = call i8* @aom_malloc(i32 %mul)
  %5 = bitcast i8* %call to double*
  store double* %5, double** %b, align 4, !tbaa !2
  %6 = bitcast double** %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load i32, i32* %n, align 4, !tbaa !6
  %mul2 = mul i32 8, %7
  %8 = load i32, i32* %n, align 4, !tbaa !6
  %mul3 = mul i32 %mul2, %8
  %call4 = call i8* @aom_malloc(i32 %mul3)
  %9 = bitcast i8* %call4 to double*
  store double* %9, double** %A, align 4, !tbaa !2
  %10 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  store i32 0, i32* %ret, align 4, !tbaa !6
  %11 = load double*, double** %A, align 4, !tbaa !2
  %cmp = icmp eq double* %11, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %12 = load double*, double** %b, align 4, !tbaa !2
  %cmp5 = icmp eq double* %12, null
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %14 = load i32, i32* %n, align 4, !tbaa !6
  %15 = load i32, i32* %n, align 4, !tbaa !6
  %call6 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.24, i32 0, i32 0), i32 %14, i32 %15)
  %16 = load double*, double** %b, align 4, !tbaa !2
  %17 = bitcast double* %16 to i8*
  call void @aom_free(i8* %17)
  %18 = load double*, double** %A, align 4, !tbaa !2
  %19 = bitcast double* %18 to i8*
  call void @aom_free(i8* %19)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %20 = load double*, double** %A, align 4, !tbaa !2
  %21 = bitcast double* %20 to i8*
  %22 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %A7 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %22, i32 0, i32 0
  %23 = load double*, double** %A7, align 4, !tbaa !23
  %24 = bitcast double* %23 to i8*
  %25 = load i32, i32* %n, align 4, !tbaa !6
  %mul8 = mul i32 8, %25
  %26 = load i32, i32* %n, align 4, !tbaa !6
  %mul9 = mul i32 %mul8, %26
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %21, i8* align 8 %24, i32 %mul9, i1 false)
  %27 = load double*, double** %b, align 4, !tbaa !2
  %28 = bitcast double* %27 to i8*
  %29 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %b10 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %29, i32 0, i32 1
  %30 = load double*, double** %b10, align 4, !tbaa !24
  %31 = bitcast double* %30 to i8*
  %32 = load i32, i32* %n, align 4, !tbaa !6
  %mul11 = mul i32 8, %32
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %28, i8* align 8 %31, i32 %mul11, i1 false)
  %33 = load i32, i32* %n, align 4, !tbaa !6
  %34 = load double*, double** %A, align 4, !tbaa !2
  %35 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %n12 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %35, i32 0, i32 3
  %36 = load i32, i32* %n12, align 4, !tbaa !22
  %37 = load double*, double** %b, align 4, !tbaa !2
  %38 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %38, i32 0, i32 2
  %39 = load double*, double** %x, align 4, !tbaa !25
  %call13 = call i32 @linsolve(i32 %33, double* %34, i32 %36, double* %37, double* %39)
  store i32 %call13, i32* %ret, align 4, !tbaa !6
  %40 = load double*, double** %b, align 4, !tbaa !2
  %41 = bitcast double* %40 to i8*
  call void @aom_free(i8* %41)
  %42 = load double*, double** %A, align 4, !tbaa !2
  %43 = bitcast double* %42 to i8*
  call void @aom_free(i8* %43)
  %44 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp14 = icmp eq i32 %44, 0
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %if.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.then15, %if.then
  %45 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #7
  %46 = bitcast double** %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #7
  %47 = bitcast double** %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  %48 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = load i32, i32* %retval, align 4
  ret i32 %49
}

; Function Attrs: nounwind
define hidden i32 @aom_noise_strength_solver_init(%struct.aom_noise_strength_solver_t* %solver, i32 %num_bins, i32 %bit_depth) #0 {
entry:
  %retval = alloca i32, align 4
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  %num_bins.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  store i32 %num_bins, i32* %num_bins.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  %0 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_noise_strength_solver_t* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %2 = bitcast %struct.aom_noise_strength_solver_t* %1 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %2, i8 0, i32 48, i1 false)
  %3 = load i32, i32* %num_bins.addr, align 4, !tbaa !6
  %4 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins1 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %4, i32 0, i32 3
  store i32 %3, i32* %num_bins1, align 8, !tbaa !13
  %5 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %min_intensity = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %5, i32 0, i32 1
  store double 0.000000e+00, double* %min_intensity, align 8, !tbaa !20
  %6 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %shl = shl i32 1, %6
  %sub = sub nsw i32 %shl, 1
  %conv = sitofp i32 %sub to double
  %7 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %max_intensity = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %7, i32 0, i32 2
  store double %conv, double* %max_intensity, align 8, !tbaa !21
  %8 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %total = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %8, i32 0, i32 5
  store double 0.000000e+00, double* %total, align 8, !tbaa !18
  %9 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_equations = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %9, i32 0, i32 4
  store i32 0, i32* %num_equations, align 4, !tbaa !19
  %10 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %10, i32 0, i32 0
  %11 = load i32, i32* %num_bins.addr, align 4, !tbaa !6
  %call = call i32 @equation_system_init(%struct.aom_equation_system_t* %eqns, i32 %11)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: nounwind
define internal i32 @equation_system_init(%struct.aom_equation_system_t* %eqns, i32 %n) #0 {
entry:
  %retval = alloca i32, align 4
  %eqns.addr = alloca %struct.aom_equation_system_t*, align 4
  %n.addr = alloca i32, align 4
  store %struct.aom_equation_system_t* %eqns, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 8, %0
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul1 = mul i32 %mul, %1
  %call = call i8* @aom_malloc(i32 %mul1)
  %2 = bitcast i8* %call to double*
  %3 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %3, i32 0, i32 0
  store double* %2, double** %A, align 4, !tbaa !23
  %4 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul2 = mul i32 8, %4
  %call3 = call i8* @aom_malloc(i32 %mul2)
  %5 = bitcast i8* %call3 to double*
  %6 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %6, i32 0, i32 1
  store double* %5, double** %b, align 4, !tbaa !24
  %7 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul4 = mul i32 8, %7
  %call5 = call i8* @aom_malloc(i32 %mul4)
  %8 = bitcast i8* %call5 to double*
  %9 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %9, i32 0, i32 2
  store double* %8, double** %x, align 4, !tbaa !25
  %10 = load i32, i32* %n.addr, align 4, !tbaa !6
  %11 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %n6 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %11, i32 0, i32 3
  store i32 %10, i32* %n6, align 4, !tbaa !22
  %12 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %A7 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %12, i32 0, i32 0
  %13 = load double*, double** %A7, align 4, !tbaa !23
  %tobool = icmp ne double* %13, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %14 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %b8 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %14, i32 0, i32 1
  %15 = load double*, double** %b8, align 4, !tbaa !24
  %tobool9 = icmp ne double* %15, null
  br i1 %tobool9, label %lor.lhs.false10, label %if.then

lor.lhs.false10:                                  ; preds = %lor.lhs.false
  %16 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %x11 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %16, i32 0, i32 2
  %17 = load double*, double** %x11, align 4, !tbaa !25
  %tobool12 = icmp ne double* %17, null
  br i1 %tobool12, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false10, %lor.lhs.false, %entry
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %19 = load i32, i32* %n.addr, align 4, !tbaa !6
  %call13 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.25, i32 0, i32 0), i32 %19)
  %20 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %A14 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %20, i32 0, i32 0
  %21 = load double*, double** %A14, align 4, !tbaa !23
  %22 = bitcast double* %21 to i8*
  call void @aom_free(i8* %22)
  %23 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %b15 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %23, i32 0, i32 1
  %24 = load double*, double** %b15, align 4, !tbaa !24
  %25 = bitcast double* %24 to i8*
  call void @aom_free(i8* %25)
  %26 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %x16 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %26, i32 0, i32 2
  %27 = load double*, double** %x16, align 4, !tbaa !25
  %28 = bitcast double* %27 to i8*
  call void @aom_free(i8* %28)
  %29 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %30 = bitcast %struct.aom_equation_system_t* %29 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %30, i8 0, i32 16, i1 false)
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false10
  %31 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  call void @equation_system_clear(%struct.aom_equation_system_t* %31)
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %32 = load i32, i32* %retval, align 4
  ret i32 %32
}

; Function Attrs: nounwind
define hidden void @aom_noise_strength_solver_free(%struct.aom_noise_strength_solver_t* %solver) #0 {
entry:
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %0 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_noise_strength_solver_t* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %1, i32 0, i32 0
  call void @equation_system_free(%struct.aom_equation_system_t* %eqns)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @equation_system_free(%struct.aom_equation_system_t* %eqns) #0 {
entry:
  %eqns.addr = alloca %struct.aom_equation_system_t*, align 4
  store %struct.aom_equation_system_t* %eqns, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %0 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_equation_system_t* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %1, i32 0, i32 0
  %2 = load double*, double** %A, align 4, !tbaa !23
  %3 = bitcast double* %2 to i8*
  call void @aom_free(i8* %3)
  %4 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %4, i32 0, i32 1
  %5 = load double*, double** %b, align 4, !tbaa !24
  %6 = bitcast double* %5 to i8*
  call void @aom_free(i8* %6)
  %7 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %7, i32 0, i32 2
  %8 = load double*, double** %x, align 4, !tbaa !25
  %9 = bitcast double* %8 to i8*
  call void @aom_free(i8* %9)
  %10 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %11 = bitcast %struct.aom_equation_system_t* %10 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %11, i8 0, i32 16, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden double @aom_noise_strength_solver_get_center(%struct.aom_noise_strength_solver_t* %solver, i32 %i) #0 {
entry:
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  %i.addr = alloca i32, align 4
  %range = alloca double, align 8
  %n = alloca i32, align 4
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %0 = bitcast double* %range to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %max_intensity = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %1, i32 0, i32 2
  %2 = load double, double* %max_intensity, align 8, !tbaa !21
  %3 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %min_intensity = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %3, i32 0, i32 1
  %4 = load double, double* %min_intensity, align 8, !tbaa !20
  %sub = fsub double %2, %4
  store double %sub, double* %range, align 8, !tbaa !11
  %5 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %6, i32 0, i32 3
  %7 = load i32, i32* %num_bins, align 8, !tbaa !13
  store i32 %7, i32* %n, align 4, !tbaa !6
  %8 = load i32, i32* %i.addr, align 4, !tbaa !6
  %conv = sitofp i32 %8 to double
  %9 = load i32, i32* %n, align 4, !tbaa !6
  %sub1 = sub nsw i32 %9, 1
  %conv2 = sitofp i32 %sub1 to double
  %div = fdiv double %conv, %conv2
  %10 = load double, double* %range, align 8, !tbaa !11
  %mul = fmul double %div, %10
  %11 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %min_intensity3 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %11, i32 0, i32 1
  %12 = load double, double* %min_intensity3, align 8, !tbaa !20
  %add = fadd double %mul, %12
  %13 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  %14 = bitcast double* %range to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %14) #7
  ret double %add
}

; Function Attrs: nounwind
define hidden i32 @aom_noise_strength_solver_fit_piecewise(%struct.aom_noise_strength_solver_t* %solver, i32 %max_output_points, %struct.aom_noise_strength_lut_t* %lut) #0 {
entry:
  %retval = alloca i32, align 4
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  %max_output_points.addr = alloca i32, align 4
  %lut.addr = alloca %struct.aom_noise_strength_lut_t*, align 4
  %kTolerance = alloca double, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %residual = alloca double*, align 4
  %min_index = alloca i32, align 4
  %j = alloca i32, align 4
  %dx = alloca double, align 8
  %avg_residual = alloca double, align 8
  %num_remaining = alloca i32, align 4
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  store i32 %max_output_points, i32* %max_output_points.addr, align 4, !tbaa !6
  store %struct.aom_noise_strength_lut_t* %lut, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %0 = bitcast double* %kTolerance to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %max_intensity = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %1, i32 0, i32 2
  %2 = load double, double* %max_intensity, align 8, !tbaa !21
  %mul = fmul double %2, 6.250000e-03
  %div = fdiv double %mul, 2.550000e+02
  store double %div, double* %kTolerance, align 8, !tbaa !11
  %3 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %4 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %4, i32 0, i32 3
  %5 = load i32, i32* %num_bins, align 8, !tbaa !13
  %call = call i32 @aom_noise_strength_lut_init(%struct.aom_noise_strength_lut_t* %3, i32 %5)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.1, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup62

if.end:                                           ; preds = %entry
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %9 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins2 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %9, i32 0, i32 3
  %10 = load i32, i32* %num_bins2, align 8, !tbaa !13
  %cmp = icmp slt i32 %8, %10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %call3 = call double @aom_noise_strength_solver_get_center(%struct.aom_noise_strength_solver_t* %12, i32 %13)
  %14 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %14, i32 0, i32 0
  %15 = load [2 x double]*, [2 x double]** %points, align 4, !tbaa !10
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [2 x double], [2 x double]* %15, i32 %16
  %arrayidx4 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx, i32 0, i32 0
  store double %call3, double* %arrayidx4, align 8, !tbaa !11
  %17 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %17, i32 0, i32 0
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 2
  %18 = load double*, double** %x, align 8, !tbaa !26
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds double, double* %18, i32 %19
  %20 = load double, double* %arrayidx5, align 8, !tbaa !11
  %21 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points6 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %21, i32 0, i32 0
  %22 = load [2 x double]*, [2 x double]** %points6, align 4, !tbaa !10
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [2 x double], [2 x double]* %22, i32 %23
  %arrayidx8 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx7, i32 0, i32 1
  store double %20, double* %arrayidx8, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %25 = load i32, i32* %max_output_points.addr, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %25, 0
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %for.end
  %26 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins11 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %26, i32 0, i32 3
  %27 = load i32, i32* %num_bins11, align 8, !tbaa !13
  store i32 %27, i32* %max_output_points.addr, align 4, !tbaa !6
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %for.end
  %28 = bitcast double** %residual to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins13 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %29, i32 0, i32 3
  %30 = load i32, i32* %num_bins13, align 8, !tbaa !13
  %mul14 = mul i32 %30, 8
  %call15 = call i8* @aom_malloc(i32 %mul14)
  %31 = bitcast i8* %call15 to double*
  store double* %31, double** %residual, align 4, !tbaa !2
  %32 = load double*, double** %residual, align 4, !tbaa !2
  %33 = bitcast double* %32 to i8*
  %34 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins16 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %34, i32 0, i32 3
  %35 = load i32, i32* %num_bins16, align 8, !tbaa !13
  %mul17 = mul i32 8, %35
  call void @llvm.memset.p0i8.i32(i8* align 8 %33, i8 0, i32 %mul17, i1 false)
  %36 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %37 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %38 = load double*, double** %residual, align 4, !tbaa !2
  %39 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins18 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %39, i32 0, i32 3
  %40 = load i32, i32* %num_bins18, align 8, !tbaa !13
  call void @update_piecewise_linear_residual(%struct.aom_noise_strength_solver_t* %36, %struct.aom_noise_strength_lut_t* %37, double* %38, i32 0, i32 %40)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %if.end12
  %41 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %41, i32 0, i32 1
  %42 = load i32, i32* %num_points, align 4, !tbaa !8
  %cmp19 = icmp sgt i32 %42, 2
  br i1 %cmp19, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %43 = bitcast i32* %min_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  store i32 1, i32* %min_index, align 4, !tbaa !6
  %44 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  store i32 1, i32* %j, align 4, !tbaa !6
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc30, %while.body
  %45 = load i32, i32* %j, align 4, !tbaa !6
  %46 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points21 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %46, i32 0, i32 1
  %47 = load i32, i32* %num_points21, align 4, !tbaa !8
  %sub = sub nsw i32 %47, 1
  %cmp22 = icmp slt i32 %45, %sub
  br i1 %cmp22, label %for.body24, label %for.cond.cleanup23

for.cond.cleanup23:                               ; preds = %for.cond20
  store i32 7, i32* %cleanup.dest.slot, align 4
  %48 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  br label %for.end32

for.body24:                                       ; preds = %for.cond20
  %49 = load double*, double** %residual, align 4, !tbaa !2
  %50 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds double, double* %49, i32 %50
  %51 = load double, double* %arrayidx25, align 8, !tbaa !11
  %52 = load double*, double** %residual, align 4, !tbaa !2
  %53 = load i32, i32* %min_index, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds double, double* %52, i32 %53
  %54 = load double, double* %arrayidx26, align 8, !tbaa !11
  %cmp27 = fcmp olt double %51, %54
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %for.body24
  %55 = load i32, i32* %j, align 4, !tbaa !6
  store i32 %55, i32* %min_index, align 4, !tbaa !6
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %for.body24
  br label %for.inc30

for.inc30:                                        ; preds = %if.end29
  %56 = load i32, i32* %j, align 4, !tbaa !6
  %inc31 = add nsw i32 %56, 1
  store i32 %inc31, i32* %j, align 4, !tbaa !6
  br label %for.cond20

for.end32:                                        ; preds = %for.cond.cleanup23
  %57 = bitcast double* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %57) #7
  %58 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points33 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %58, i32 0, i32 0
  %59 = load [2 x double]*, [2 x double]** %points33, align 4, !tbaa !10
  %60 = load i32, i32* %min_index, align 4, !tbaa !6
  %add = add nsw i32 %60, 1
  %arrayidx34 = getelementptr inbounds [2 x double], [2 x double]* %59, i32 %add
  %arrayidx35 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx34, i32 0, i32 0
  %61 = load double, double* %arrayidx35, align 8, !tbaa !11
  %62 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points36 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %62, i32 0, i32 0
  %63 = load [2 x double]*, [2 x double]** %points36, align 4, !tbaa !10
  %64 = load i32, i32* %min_index, align 4, !tbaa !6
  %sub37 = sub nsw i32 %64, 1
  %arrayidx38 = getelementptr inbounds [2 x double], [2 x double]* %63, i32 %sub37
  %arrayidx39 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx38, i32 0, i32 0
  %65 = load double, double* %arrayidx39, align 8, !tbaa !11
  %sub40 = fsub double %61, %65
  store double %sub40, double* %dx, align 8, !tbaa !11
  %66 = bitcast double* %avg_residual to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %66) #7
  %67 = load double*, double** %residual, align 4, !tbaa !2
  %68 = load i32, i32* %min_index, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds double, double* %67, i32 %68
  %69 = load double, double* %arrayidx41, align 8, !tbaa !11
  %70 = load double, double* %dx, align 8, !tbaa !11
  %div42 = fdiv double %69, %70
  store double %div42, double* %avg_residual, align 8, !tbaa !11
  %71 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points43 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %71, i32 0, i32 1
  %72 = load i32, i32* %num_points43, align 4, !tbaa !8
  %73 = load i32, i32* %max_output_points.addr, align 4, !tbaa !6
  %cmp44 = icmp sle i32 %72, %73
  br i1 %cmp44, label %land.lhs.true, label %if.end47

land.lhs.true:                                    ; preds = %for.end32
  %74 = load double, double* %avg_residual, align 8, !tbaa !11
  %75 = load double, double* %kTolerance, align 8, !tbaa !11
  %cmp45 = fcmp ogt double %74, %75
  br i1 %cmp45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %land.lhs.true
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end47:                                         ; preds = %land.lhs.true, %for.end32
  %76 = bitcast i32* %num_remaining to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #7
  %77 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points48 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %77, i32 0, i32 1
  %78 = load i32, i32* %num_points48, align 4, !tbaa !8
  %79 = load i32, i32* %min_index, align 4, !tbaa !6
  %sub49 = sub nsw i32 %78, %79
  %sub50 = sub nsw i32 %sub49, 1
  store i32 %sub50, i32* %num_remaining, align 4, !tbaa !6
  %80 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points51 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %80, i32 0, i32 0
  %81 = load [2 x double]*, [2 x double]** %points51, align 4, !tbaa !10
  %82 = load i32, i32* %min_index, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds [2 x double], [2 x double]* %81, i32 %82
  %83 = bitcast [2 x double]* %add.ptr to i8*
  %84 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points52 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %84, i32 0, i32 0
  %85 = load [2 x double]*, [2 x double]** %points52, align 4, !tbaa !10
  %86 = load i32, i32* %min_index, align 4, !tbaa !6
  %add.ptr53 = getelementptr inbounds [2 x double], [2 x double]* %85, i32 %86
  %add.ptr54 = getelementptr inbounds [2 x double], [2 x double]* %add.ptr53, i32 1
  %87 = bitcast [2 x double]* %add.ptr54 to i8*
  %88 = load i32, i32* %num_remaining, align 4, !tbaa !6
  %mul55 = mul i32 16, %88
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 8 %83, i8* align 8 %87, i32 %mul55, i1 false)
  %89 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points56 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %89, i32 0, i32 1
  %90 = load i32, i32* %num_points56, align 4, !tbaa !8
  %dec = add nsw i32 %90, -1
  store i32 %dec, i32* %num_points56, align 4, !tbaa !8
  %91 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %92 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %93 = load double*, double** %residual, align 4, !tbaa !2
  %94 = load i32, i32* %min_index, align 4, !tbaa !6
  %sub57 = sub nsw i32 %94, 1
  %95 = load i32, i32* %min_index, align 4, !tbaa !6
  %add58 = add nsw i32 %95, 1
  call void @update_piecewise_linear_residual(%struct.aom_noise_strength_solver_t* %91, %struct.aom_noise_strength_lut_t* %92, double* %93, i32 %sub57, i32 %add58)
  %96 = bitcast i32* %num_remaining to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end47, %if.then46
  %97 = bitcast double* %avg_residual to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %97) #7
  %98 = bitcast double* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %98) #7
  %99 = bitcast i32* %min_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 6, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup, %while.cond
  %100 = load double*, double** %residual, align 4, !tbaa !2
  %101 = bitcast double* %100 to i8*
  call void @aom_free(i8* %101)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %102 = bitcast double** %residual to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #7
  br label %cleanup62

cleanup62:                                        ; preds = %while.end, %if.then
  %103 = bitcast double* %kTolerance to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %103) #7
  %104 = load i32, i32* %retval, align 4
  ret i32 %104

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @update_piecewise_linear_residual(%struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_lut_t* %lut, double* %residual, i32 %start, i32 %end) #0 {
entry:
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  %lut.addr = alloca %struct.aom_noise_strength_lut_t*, align 4
  %residual.addr = alloca double*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dx = alloca double, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %lower = alloca i32, align 4
  %upper = alloca i32, align 4
  %r = alloca double, align 8
  %j = alloca i32, align 4
  %x = alloca double, align 8
  %y = alloca double, align 8
  %a = alloca double, align 8
  %estimate_y = alloca double, align 8
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  store %struct.aom_noise_strength_lut_t* %lut, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  store double* %residual, double** %residual.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  %0 = bitcast double* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %1, i32 0, i32 3
  %2 = load i32, i32* %num_bins, align 8, !tbaa !13
  %conv = sitofp i32 %2 to double
  %div = fdiv double 2.550000e+02, %conv
  store double %div, double* %dx, align 8, !tbaa !11
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %start.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %4, 1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load i32, i32* %start.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %5, %cond.true ], [ 1, %cond.false ]
  store i32 %cond, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc101, %cond.end
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %7 = load i32, i32* %end.addr, align 4, !tbaa !6
  %8 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %8, i32 0, i32 1
  %9 = load i32, i32* %num_points, align 4, !tbaa !8
  %sub = sub nsw i32 %9, 1
  %cmp2 = icmp slt i32 %7, %sub
  br i1 %cmp2, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %for.cond
  %10 = load i32, i32* %end.addr, align 4, !tbaa !6
  br label %cond.end8

cond.false5:                                      ; preds = %for.cond
  %11 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %num_points6 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %11, i32 0, i32 1
  %12 = load i32, i32* %num_points6, align 4, !tbaa !8
  %sub7 = sub nsw i32 %12, 1
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false5, %cond.true4
  %cond9 = phi i32 [ %10, %cond.true4 ], [ %sub7, %cond.false5 ]
  %cmp10 = icmp slt i32 %6, %cond9
  br i1 %cmp10, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %cond.end8
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  br label %for.end104

for.body:                                         ; preds = %cond.end8
  %14 = bitcast i32* %lower to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %16 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %16, i32 0, i32 0
  %17 = load [2 x double]*, [2 x double]** %points, align 4, !tbaa !10
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %sub12 = sub nsw i32 %18, 1
  %arrayidx = getelementptr inbounds [2 x double], [2 x double]* %17, i32 %sub12
  %arrayidx13 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx, i32 0, i32 0
  %19 = load double, double* %arrayidx13, align 8, !tbaa !11
  %call = call double @noise_strength_solver_get_bin_index(%struct.aom_noise_strength_solver_t* %15, double %19)
  %20 = call double @llvm.floor.f64(double %call)
  %conv14 = fptosi double %20 to i32
  %cmp15 = icmp sgt i32 0, %conv14
  br i1 %cmp15, label %cond.true17, label %cond.false18

cond.true17:                                      ; preds = %for.body
  br label %cond.end25

cond.false18:                                     ; preds = %for.body
  %21 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %22 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points19 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %22, i32 0, i32 0
  %23 = load [2 x double]*, [2 x double]** %points19, align 4, !tbaa !10
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %sub20 = sub nsw i32 %24, 1
  %arrayidx21 = getelementptr inbounds [2 x double], [2 x double]* %23, i32 %sub20
  %arrayidx22 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx21, i32 0, i32 0
  %25 = load double, double* %arrayidx22, align 8, !tbaa !11
  %call23 = call double @noise_strength_solver_get_bin_index(%struct.aom_noise_strength_solver_t* %21, double %25)
  %26 = call double @llvm.floor.f64(double %call23)
  %conv24 = fptosi double %26 to i32
  br label %cond.end25

cond.end25:                                       ; preds = %cond.false18, %cond.true17
  %cond26 = phi i32 [ 0, %cond.true17 ], [ %conv24, %cond.false18 ]
  store i32 %cond26, i32* %lower, align 4, !tbaa !6
  %27 = bitcast i32* %upper to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  %28 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins27 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %28, i32 0, i32 3
  %29 = load i32, i32* %num_bins27, align 8, !tbaa !13
  %sub28 = sub nsw i32 %29, 1
  %30 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %31 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points29 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %31, i32 0, i32 0
  %32 = load [2 x double]*, [2 x double]** %points29, align 4, !tbaa !10
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %33, 1
  %arrayidx30 = getelementptr inbounds [2 x double], [2 x double]* %32, i32 %add
  %arrayidx31 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx30, i32 0, i32 0
  %34 = load double, double* %arrayidx31, align 8, !tbaa !11
  %call32 = call double @noise_strength_solver_get_bin_index(%struct.aom_noise_strength_solver_t* %30, double %34)
  %35 = call double @llvm.ceil.f64(double %call32)
  %conv33 = fptosi double %35 to i32
  %cmp34 = icmp slt i32 %sub28, %conv33
  br i1 %cmp34, label %cond.true36, label %cond.false39

cond.true36:                                      ; preds = %cond.end25
  %36 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins37 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %36, i32 0, i32 3
  %37 = load i32, i32* %num_bins37, align 8, !tbaa !13
  %sub38 = sub nsw i32 %37, 1
  br label %cond.end46

cond.false39:                                     ; preds = %cond.end25
  %38 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %39 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points40 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %39, i32 0, i32 0
  %40 = load [2 x double]*, [2 x double]** %points40, align 4, !tbaa !10
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %add41 = add nsw i32 %41, 1
  %arrayidx42 = getelementptr inbounds [2 x double], [2 x double]* %40, i32 %add41
  %arrayidx43 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx42, i32 0, i32 0
  %42 = load double, double* %arrayidx43, align 8, !tbaa !11
  %call44 = call double @noise_strength_solver_get_bin_index(%struct.aom_noise_strength_solver_t* %38, double %42)
  %43 = call double @llvm.ceil.f64(double %call44)
  %conv45 = fptosi double %43 to i32
  br label %cond.end46

cond.end46:                                       ; preds = %cond.false39, %cond.true36
  %cond47 = phi i32 [ %sub38, %cond.true36 ], [ %conv45, %cond.false39 ]
  store i32 %cond47, i32* %upper, align 4, !tbaa !6
  %44 = bitcast double* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %44) #7
  store double 0.000000e+00, double* %r, align 8, !tbaa !11
  %45 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #7
  %46 = load i32, i32* %lower, align 4, !tbaa !6
  store i32 %46, i32* %j, align 4, !tbaa !6
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc, %cond.end46
  %47 = load i32, i32* %j, align 4, !tbaa !6
  %48 = load i32, i32* %upper, align 4, !tbaa !6
  %cmp49 = icmp sle i32 %47, %48
  br i1 %cmp49, label %for.body52, label %for.cond.cleanup51

for.cond.cleanup51:                               ; preds = %for.cond48
  store i32 5, i32* %cleanup.dest.slot, align 4
  %49 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  br label %for.end

for.body52:                                       ; preds = %for.cond48
  %50 = bitcast double* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %50) #7
  %51 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %52 = load i32, i32* %j, align 4, !tbaa !6
  %call53 = call double @aom_noise_strength_solver_get_center(%struct.aom_noise_strength_solver_t* %51, i32 %52)
  store double %call53, double* %x, align 8, !tbaa !11
  %53 = load double, double* %x, align 8, !tbaa !11
  %54 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points54 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %54, i32 0, i32 0
  %55 = load [2 x double]*, [2 x double]** %points54, align 4, !tbaa !10
  %56 = load i32, i32* %i, align 4, !tbaa !6
  %sub55 = sub nsw i32 %56, 1
  %arrayidx56 = getelementptr inbounds [2 x double], [2 x double]* %55, i32 %sub55
  %arrayidx57 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx56, i32 0, i32 0
  %57 = load double, double* %arrayidx57, align 8, !tbaa !11
  %cmp58 = fcmp olt double %53, %57
  br i1 %cmp58, label %if.then, label %if.end

if.then:                                          ; preds = %for.body52
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body52
  %58 = load double, double* %x, align 8, !tbaa !11
  %59 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points60 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %59, i32 0, i32 0
  %60 = load [2 x double]*, [2 x double]** %points60, align 4, !tbaa !10
  %61 = load i32, i32* %i, align 4, !tbaa !6
  %add61 = add nsw i32 %61, 1
  %arrayidx62 = getelementptr inbounds [2 x double], [2 x double]* %60, i32 %add61
  %arrayidx63 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx62, i32 0, i32 0
  %62 = load double, double* %arrayidx63, align 8, !tbaa !11
  %cmp64 = fcmp oge double %58, %62
  br i1 %cmp64, label %if.then66, label %if.end67

if.then66:                                        ; preds = %if.end
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end67:                                         ; preds = %if.end
  %63 = bitcast double* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %63) #7
  %64 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %64, i32 0, i32 0
  %x68 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 2
  %65 = load double*, double** %x68, align 8, !tbaa !26
  %66 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx69 = getelementptr inbounds double, double* %65, i32 %66
  %67 = load double, double* %arrayidx69, align 8, !tbaa !11
  store double %67, double* %y, align 8, !tbaa !11
  %68 = bitcast double* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %68) #7
  %69 = load double, double* %x, align 8, !tbaa !11
  %70 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points70 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %70, i32 0, i32 0
  %71 = load [2 x double]*, [2 x double]** %points70, align 4, !tbaa !10
  %72 = load i32, i32* %i, align 4, !tbaa !6
  %sub71 = sub nsw i32 %72, 1
  %arrayidx72 = getelementptr inbounds [2 x double], [2 x double]* %71, i32 %sub71
  %arrayidx73 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx72, i32 0, i32 0
  %73 = load double, double* %arrayidx73, align 8, !tbaa !11
  %sub74 = fsub double %69, %73
  %74 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points75 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %74, i32 0, i32 0
  %75 = load [2 x double]*, [2 x double]** %points75, align 4, !tbaa !10
  %76 = load i32, i32* %i, align 4, !tbaa !6
  %add76 = add nsw i32 %76, 1
  %arrayidx77 = getelementptr inbounds [2 x double], [2 x double]* %75, i32 %add76
  %arrayidx78 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx77, i32 0, i32 0
  %77 = load double, double* %arrayidx78, align 8, !tbaa !11
  %78 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points79 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %78, i32 0, i32 0
  %79 = load [2 x double]*, [2 x double]** %points79, align 4, !tbaa !10
  %80 = load i32, i32* %i, align 4, !tbaa !6
  %sub80 = sub nsw i32 %80, 1
  %arrayidx81 = getelementptr inbounds [2 x double], [2 x double]* %79, i32 %sub80
  %arrayidx82 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx81, i32 0, i32 0
  %81 = load double, double* %arrayidx82, align 8, !tbaa !11
  %sub83 = fsub double %77, %81
  %div84 = fdiv double %sub74, %sub83
  store double %div84, double* %a, align 8, !tbaa !11
  %82 = bitcast double* %estimate_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %82) #7
  %83 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points85 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %83, i32 0, i32 0
  %84 = load [2 x double]*, [2 x double]** %points85, align 4, !tbaa !10
  %85 = load i32, i32* %i, align 4, !tbaa !6
  %sub86 = sub nsw i32 %85, 1
  %arrayidx87 = getelementptr inbounds [2 x double], [2 x double]* %84, i32 %sub86
  %arrayidx88 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx87, i32 0, i32 1
  %86 = load double, double* %arrayidx88, align 8, !tbaa !11
  %87 = load double, double* %a, align 8, !tbaa !11
  %sub89 = fsub double 1.000000e+00, %87
  %mul = fmul double %86, %sub89
  %88 = load %struct.aom_noise_strength_lut_t*, %struct.aom_noise_strength_lut_t** %lut.addr, align 4, !tbaa !2
  %points90 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %88, i32 0, i32 0
  %89 = load [2 x double]*, [2 x double]** %points90, align 4, !tbaa !10
  %90 = load i32, i32* %i, align 4, !tbaa !6
  %add91 = add nsw i32 %90, 1
  %arrayidx92 = getelementptr inbounds [2 x double], [2 x double]* %89, i32 %add91
  %arrayidx93 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx92, i32 0, i32 1
  %91 = load double, double* %arrayidx93, align 8, !tbaa !11
  %92 = load double, double* %a, align 8, !tbaa !11
  %mul94 = fmul double %91, %92
  %add95 = fadd double %mul, %mul94
  store double %add95, double* %estimate_y, align 8, !tbaa !11
  %93 = load double, double* %y, align 8, !tbaa !11
  %94 = load double, double* %estimate_y, align 8, !tbaa !11
  %sub96 = fsub double %93, %94
  %95 = call double @llvm.fabs.f64(double %sub96)
  %96 = load double, double* %r, align 8, !tbaa !11
  %add97 = fadd double %96, %95
  store double %add97, double* %r, align 8, !tbaa !11
  %97 = bitcast double* %estimate_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %97) #7
  %98 = bitcast double* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %98) #7
  %99 = bitcast double* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %99) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end67, %if.then66, %if.then
  %100 = bitcast double* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %100) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %101 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %101, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond48

for.end:                                          ; preds = %for.cond.cleanup51
  %102 = load double, double* %r, align 8, !tbaa !11
  %103 = load double, double* %dx, align 8, !tbaa !11
  %mul99 = fmul double %102, %103
  %104 = load double*, double** %residual.addr, align 4, !tbaa !2
  %105 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx100 = getelementptr inbounds double, double* %104, i32 %105
  store double %mul99, double* %arrayidx100, align 8, !tbaa !11
  %106 = bitcast double* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %106) #7
  %107 = bitcast i32* %upper to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #7
  %108 = bitcast i32* %lower to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #7
  br label %for.inc101

for.inc101:                                       ; preds = %for.end
  %109 = load i32, i32* %i, align 4, !tbaa !6
  %inc102 = add nsw i32 %109, 1
  store i32 %inc102, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end104:                                       ; preds = %for.cond.cleanup
  %110 = bitcast double* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %110) #7
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #3

; Function Attrs: nounwind
define hidden i32 @aom_flat_block_finder_init(%struct.aom_flat_block_finder_t* %block_finder, i32 %block_size, i32 %bit_depth, i32 %use_highbd) #0 {
entry:
  %retval = alloca i32, align 4
  %block_finder.addr = alloca %struct.aom_flat_block_finder_t*, align 4
  %block_size.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %use_highbd.addr = alloca i32, align 4
  %n = alloca i32, align 4
  %eqns = alloca %struct.aom_equation_system_t, align 4
  %AtA_inv = alloca double*, align 4
  %A = alloca double*, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %yd = alloca double, align 8
  %xd = alloca double, align 8
  %coords = alloca [3 x double], align 16
  %row = alloca i32, align 4
  store %struct.aom_flat_block_finder_t* %block_finder, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %use_highbd, i32* %use_highbd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %2 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, %2
  store i32 %mul, i32* %n, align 4, !tbaa !6
  %3 = bitcast %struct.aom_equation_system_t* %eqns to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #7
  %4 = bitcast double** %AtA_inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store double* null, double** %AtA_inv, align 4, !tbaa !2
  %5 = bitcast double** %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  store double* null, double** %A, align 4, !tbaa !2
  %6 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  %7 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  %9 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  %10 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %A1 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %10, i32 0, i32 1
  store double* null, double** %A1, align 4, !tbaa !27
  %11 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %AtA_inv2 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %11, i32 0, i32 0
  store double* null, double** %AtA_inv2, align 8, !tbaa !29
  %call = call i32 @equation_system_init(%struct.aom_equation_system_t* %eqns, i32 3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %13 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.2, i32 0, i32 0), i32 %13)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %call4 = call i8* @aom_malloc(i32 72)
  %14 = bitcast i8* %call4 to double*
  store double* %14, double** %AtA_inv, align 4, !tbaa !2
  %15 = load i32, i32* %n, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %15
  %mul6 = mul i32 %mul5, 8
  %call7 = call i8* @aom_malloc(i32 %mul6)
  %16 = bitcast i8* %call7 to double*
  store double* %16, double** %A, align 4, !tbaa !2
  %17 = load double*, double** %AtA_inv, align 4, !tbaa !2
  %cmp = icmp eq double* %17, null
  br i1 %cmp, label %if.then9, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %18 = load double*, double** %A, align 4, !tbaa !2
  %cmp8 = icmp eq double* %18, null
  br i1 %cmp8, label %if.then9, label %if.end11

if.then9:                                         ; preds = %lor.lhs.false, %if.end
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %20 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.3, i32 0, i32 0), i32 %20)
  %21 = load double*, double** %AtA_inv, align 4, !tbaa !2
  %22 = bitcast double* %21 to i8*
  call void @aom_free(i8* %22)
  %23 = load double*, double** %A, align 4, !tbaa !2
  %24 = bitcast double* %23 to i8*
  call void @aom_free(i8* %24)
  call void @equation_system_free(%struct.aom_equation_system_t* %eqns)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %lor.lhs.false
  %25 = load double*, double** %A, align 4, !tbaa !2
  %26 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %A12 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %26, i32 0, i32 1
  store double* %25, double** %A12, align 4, !tbaa !27
  %27 = load double*, double** %AtA_inv, align 4, !tbaa !2
  %28 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %AtA_inv13 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %28, i32 0, i32 0
  store double* %27, double** %AtA_inv13, align 8, !tbaa !29
  %29 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %30 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %block_size14 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %30, i32 0, i32 3
  store i32 %29, i32* %block_size14, align 4, !tbaa !30
  %31 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %shl = shl i32 1, %31
  %sub = sub nsw i32 %shl, 1
  %conv = sitofp i32 %sub to double
  %32 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %normalization = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %32, i32 0, i32 4
  store double %conv, double* %normalization, align 8, !tbaa !31
  %33 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %34 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %use_highbd15 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %34, i32 0, i32 5
  store i32 %33, i32* %use_highbd15, align 8, !tbaa !32
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc67, %if.end11
  %35 = load i32, i32* %y, align 4, !tbaa !6
  %36 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp16 = icmp slt i32 %35, %36
  br i1 %cmp16, label %for.body, label %for.end69

for.body:                                         ; preds = %for.cond
  %37 = bitcast double* %yd to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %37) #7
  %38 = load i32, i32* %y, align 4, !tbaa !6
  %conv18 = sitofp i32 %38 to double
  %39 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %conv19 = sitofp i32 %39 to double
  %div = fdiv double %conv19, 2.000000e+00
  %sub20 = fsub double %conv18, %div
  %40 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %conv21 = sitofp i32 %40 to double
  %div22 = fdiv double %conv21, 2.000000e+00
  %div23 = fdiv double %sub20, %div22
  store double %div23, double* %yd, align 8, !tbaa !11
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc64, %for.body
  %41 = load i32, i32* %x, align 4, !tbaa !6
  %42 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp25 = icmp slt i32 %41, %42
  br i1 %cmp25, label %for.body27, label %for.end66

for.body27:                                       ; preds = %for.cond24
  %43 = bitcast double* %xd to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %43) #7
  %44 = load i32, i32* %x, align 4, !tbaa !6
  %conv28 = sitofp i32 %44 to double
  %45 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %conv29 = sitofp i32 %45 to double
  %div30 = fdiv double %conv29, 2.000000e+00
  %sub31 = fsub double %conv28, %div30
  %46 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %conv32 = sitofp i32 %46 to double
  %div33 = fdiv double %conv32, 2.000000e+00
  %div34 = fdiv double %sub31, %div33
  store double %div34, double* %xd, align 8, !tbaa !11
  %47 = bitcast [3 x double]* %coords to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %47) #7
  %arrayinit.begin = getelementptr inbounds [3 x double], [3 x double]* %coords, i32 0, i32 0
  %48 = load double, double* %yd, align 8, !tbaa !11
  store double %48, double* %arrayinit.begin, align 8, !tbaa !11
  %arrayinit.element = getelementptr inbounds double, double* %arrayinit.begin, i32 1
  %49 = load double, double* %xd, align 8, !tbaa !11
  store double %49, double* %arrayinit.element, align 8, !tbaa !11
  %arrayinit.element35 = getelementptr inbounds double, double* %arrayinit.element, i32 1
  store double 1.000000e+00, double* %arrayinit.element35, align 8, !tbaa !11
  %50 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load i32, i32* %y, align 4, !tbaa !6
  %52 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul36 = mul nsw i32 %51, %52
  %53 = load i32, i32* %x, align 4, !tbaa !6
  %add = add nsw i32 %mul36, %53
  store i32 %add, i32* %row, align 4, !tbaa !6
  %54 = load double, double* %yd, align 8, !tbaa !11
  %55 = load double*, double** %A, align 4, !tbaa !2
  %56 = load i32, i32* %row, align 4, !tbaa !6
  %mul37 = mul nsw i32 3, %56
  %add38 = add nsw i32 %mul37, 0
  %arrayidx = getelementptr inbounds double, double* %55, i32 %add38
  store double %54, double* %arrayidx, align 8, !tbaa !11
  %57 = load double, double* %xd, align 8, !tbaa !11
  %58 = load double*, double** %A, align 4, !tbaa !2
  %59 = load i32, i32* %row, align 4, !tbaa !6
  %mul39 = mul nsw i32 3, %59
  %add40 = add nsw i32 %mul39, 1
  %arrayidx41 = getelementptr inbounds double, double* %58, i32 %add40
  store double %57, double* %arrayidx41, align 8, !tbaa !11
  %60 = load double*, double** %A, align 4, !tbaa !2
  %61 = load i32, i32* %row, align 4, !tbaa !6
  %mul42 = mul nsw i32 3, %61
  %add43 = add nsw i32 %mul42, 2
  %arrayidx44 = getelementptr inbounds double, double* %60, i32 %add43
  store double 1.000000e+00, double* %arrayidx44, align 8, !tbaa !11
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond45

for.cond45:                                       ; preds = %for.inc61, %for.body27
  %62 = load i32, i32* %i, align 4, !tbaa !6
  %cmp46 = icmp slt i32 %62, 3
  br i1 %cmp46, label %for.body48, label %for.end63

for.body48:                                       ; preds = %for.cond45
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc, %for.body48
  %63 = load i32, i32* %j, align 4, !tbaa !6
  %cmp50 = icmp slt i32 %63, 3
  br i1 %cmp50, label %for.body52, label %for.end

for.body52:                                       ; preds = %for.cond49
  %64 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx53 = getelementptr inbounds [3 x double], [3 x double]* %coords, i32 0, i32 %64
  %65 = load double, double* %arrayidx53, align 8, !tbaa !11
  %66 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx54 = getelementptr inbounds [3 x double], [3 x double]* %coords, i32 0, i32 %66
  %67 = load double, double* %arrayidx54, align 8, !tbaa !11
  %mul55 = fmul double %65, %67
  %A56 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 0
  %68 = load double*, double** %A56, align 4, !tbaa !23
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %mul57 = mul nsw i32 3, %69
  %70 = load i32, i32* %j, align 4, !tbaa !6
  %add58 = add nsw i32 %mul57, %70
  %arrayidx59 = getelementptr inbounds double, double* %68, i32 %add58
  %71 = load double, double* %arrayidx59, align 8, !tbaa !11
  %add60 = fadd double %71, %mul55
  store double %add60, double* %arrayidx59, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body52
  %72 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %72, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond49

for.end:                                          ; preds = %for.cond49
  br label %for.inc61

for.inc61:                                        ; preds = %for.end
  %73 = load i32, i32* %i, align 4, !tbaa !6
  %inc62 = add nsw i32 %73, 1
  store i32 %inc62, i32* %i, align 4, !tbaa !6
  br label %for.cond45

for.end63:                                        ; preds = %for.cond45
  %74 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #7
  %75 = bitcast [3 x double]* %coords to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %75) #7
  %76 = bitcast double* %xd to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %76) #7
  br label %for.inc64

for.inc64:                                        ; preds = %for.end63
  %77 = load i32, i32* %x, align 4, !tbaa !6
  %inc65 = add nsw i32 %77, 1
  store i32 %inc65, i32* %x, align 4, !tbaa !6
  br label %for.cond24

for.end66:                                        ; preds = %for.cond24
  %78 = bitcast double* %yd to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %78) #7
  br label %for.inc67

for.inc67:                                        ; preds = %for.end66
  %79 = load i32, i32* %y, align 4, !tbaa !6
  %inc68 = add nsw i32 %79, 1
  store i32 %inc68, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end69:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond70

for.cond70:                                       ; preds = %for.inc89, %for.end69
  %80 = load i32, i32* %i, align 4, !tbaa !6
  %cmp71 = icmp slt i32 %80, 3
  br i1 %cmp71, label %for.body73, label %for.end91

for.body73:                                       ; preds = %for.cond70
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 1
  %81 = load double*, double** %b, align 4, !tbaa !24
  %82 = bitcast double* %81 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %82, i8 0, i32 24, i1 false)
  %b74 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 1
  %83 = load double*, double** %b74, align 4, !tbaa !24
  %84 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds double, double* %83, i32 %84
  store double 1.000000e+00, double* %arrayidx75, align 8, !tbaa !11
  %call76 = call i32 @equation_system_solve(%struct.aom_equation_system_t* %eqns)
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc86, %for.body73
  %85 = load i32, i32* %j, align 4, !tbaa !6
  %cmp78 = icmp slt i32 %85, 3
  br i1 %cmp78, label %for.body80, label %for.end88

for.body80:                                       ; preds = %for.cond77
  %x81 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 2
  %86 = load double*, double** %x81, align 4, !tbaa !25
  %87 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx82 = getelementptr inbounds double, double* %86, i32 %87
  %88 = load double, double* %arrayidx82, align 8, !tbaa !11
  %89 = load double*, double** %AtA_inv, align 4, !tbaa !2
  %90 = load i32, i32* %j, align 4, !tbaa !6
  %mul83 = mul nsw i32 %90, 3
  %91 = load i32, i32* %i, align 4, !tbaa !6
  %add84 = add nsw i32 %mul83, %91
  %arrayidx85 = getelementptr inbounds double, double* %89, i32 %add84
  store double %88, double* %arrayidx85, align 8, !tbaa !11
  br label %for.inc86

for.inc86:                                        ; preds = %for.body80
  %92 = load i32, i32* %j, align 4, !tbaa !6
  %inc87 = add nsw i32 %92, 1
  store i32 %inc87, i32* %j, align 4, !tbaa !6
  br label %for.cond77

for.end88:                                        ; preds = %for.cond77
  br label %for.inc89

for.inc89:                                        ; preds = %for.end88
  %93 = load i32, i32* %i, align 4, !tbaa !6
  %inc90 = add nsw i32 %93, 1
  store i32 %inc90, i32* %i, align 4, !tbaa !6
  br label %for.cond70

for.end91:                                        ; preds = %for.cond70
  call void @equation_system_free(%struct.aom_equation_system_t* %eqns)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end91, %if.then9, %if.then
  %94 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #7
  %95 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #7
  %96 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #7
  %97 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #7
  %98 = bitcast double** %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #7
  %99 = bitcast double** %AtA_inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %100 = bitcast %struct.aom_equation_system_t* %eqns to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %100) #7
  %101 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #7
  %102 = load i32, i32* %retval, align 4
  ret i32 %102
}

; Function Attrs: nounwind
define hidden void @aom_flat_block_finder_free(%struct.aom_flat_block_finder_t* %block_finder) #0 {
entry:
  %block_finder.addr = alloca %struct.aom_flat_block_finder_t*, align 4
  store %struct.aom_flat_block_finder_t* %block_finder, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %0 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_flat_block_finder_t* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %A = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %1, i32 0, i32 1
  %2 = load double*, double** %A, align 4, !tbaa !27
  %3 = bitcast double* %2 to i8*
  call void @aom_free(i8* %3)
  %4 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %AtA_inv = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %4, i32 0, i32 0
  %5 = load double*, double** %AtA_inv, align 8, !tbaa !29
  %6 = bitcast double* %5 to i8*
  call void @aom_free(i8* %6)
  %7 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %8 = bitcast %struct.aom_flat_block_finder_t* %7 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %8, i8 0, i32 32, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_flat_block_finder_extract_block(%struct.aom_flat_block_finder_t* %block_finder, i8* %data, i32 %w, i32 %h, i32 %stride, i32 %offsx, i32 %offsy, double* %plane, double* %block) #0 {
entry:
  %block_finder.addr = alloca %struct.aom_flat_block_finder_t*, align 4
  %data.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %offsx.addr = alloca i32, align 4
  %offsy.addr = alloca i32, align 4
  %plane.addr = alloca double*, align 4
  %block.addr = alloca double*, align 4
  %block_size = alloca i32, align 4
  %n = alloca i32, align 4
  %A = alloca double*, align 4
  %AtA_inv = alloca double*, align 4
  %plane_coords = alloca [3 x double], align 16
  %AtA_inv_b = alloca [3 x double], align 16
  %xi = alloca i32, align 4
  %yi = alloca i32, align 4
  %i = alloca i32, align 4
  %data16 = alloca i16*, align 4
  %y = alloca i32, align 4
  %x = alloca i32, align 4
  %y22 = alloca i32, align 4
  %x30 = alloca i32, align 4
  store %struct.aom_flat_block_finder_t* %block_finder, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %offsx, i32* %offsx.addr, align 4, !tbaa !6
  store i32 %offsy, i32* %offsy.addr, align 4, !tbaa !6
  store double* %plane, double** %plane.addr, align 4, !tbaa !2
  store double* %block, double** %block.addr, align 4, !tbaa !2
  %0 = bitcast i32* %block_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %block_size1 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %1, i32 0, i32 3
  %2 = load i32, i32* %block_size1, align 4, !tbaa !30
  store i32 %2, i32* %block_size, align 4, !tbaa !6
  %3 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %block_size, align 4, !tbaa !6
  %5 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul = mul nsw i32 %4, %5
  store i32 %mul, i32* %n, align 4, !tbaa !6
  %6 = bitcast double** %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %A2 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %7, i32 0, i32 1
  %8 = load double*, double** %A2, align 4, !tbaa !27
  store double* %8, double** %A, align 4, !tbaa !2
  %9 = bitcast double** %AtA_inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %AtA_inv3 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %10, i32 0, i32 0
  %11 = load double*, double** %AtA_inv3, align 8, !tbaa !29
  store double* %11, double** %AtA_inv, align 4, !tbaa !2
  %12 = bitcast [3 x double]* %plane_coords to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %12) #7
  %13 = bitcast [3 x double]* %AtA_inv_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %13) #7
  %14 = bitcast i32* %xi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = bitcast i32* %yi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %use_highbd = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %17, i32 0, i32 5
  %18 = load i32, i32* %use_highbd, align 8, !tbaa !32
  %tobool = icmp ne i32 %18, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %19 = bitcast i16** %data16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %21 = bitcast i8* %20 to i16*
  store i16* %21, i16** %data16, align 4, !tbaa !2
  store i32 0, i32* %yi, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %if.then
  %22 = load i32, i32* %yi, align 4, !tbaa !6
  %23 = load i32, i32* %block_size, align 4, !tbaa !6
  %cmp = icmp slt i32 %22, %23
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  %24 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = load i32, i32* %offsy.addr, align 4, !tbaa !6
  %26 = load i32, i32* %yi, align 4, !tbaa !6
  %add = add nsw i32 %25, %26
  %27 = load i32, i32* %h.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %27, 1
  %call = call i32 @clamp(i32 %add, i32 0, i32 %sub)
  store i32 %call, i32* %y, align 4, !tbaa !6
  store i32 0, i32* %xi, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %28 = load i32, i32* %xi, align 4, !tbaa !6
  %29 = load i32, i32* %block_size, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %28, %29
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %30 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  %31 = load i32, i32* %offsx.addr, align 4, !tbaa !6
  %32 = load i32, i32* %xi, align 4, !tbaa !6
  %add7 = add nsw i32 %31, %32
  %33 = load i32, i32* %w.addr, align 4, !tbaa !6
  %sub8 = sub nsw i32 %33, 1
  %call9 = call i32 @clamp(i32 %add7, i32 0, i32 %sub8)
  store i32 %call9, i32* %x, align 4, !tbaa !6
  %34 = load i16*, i16** %data16, align 4, !tbaa !2
  %35 = load i32, i32* %y, align 4, !tbaa !6
  %36 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 %35, %36
  %37 = load i32, i32* %x, align 4, !tbaa !6
  %add11 = add nsw i32 %mul10, %37
  %arrayidx = getelementptr inbounds i16, i16* %34, i32 %add11
  %38 = load i16, i16* %arrayidx, align 2, !tbaa !33
  %conv = uitofp i16 %38 to double
  %39 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %normalization = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %39, i32 0, i32 4
  %40 = load double, double* %normalization, align 8, !tbaa !31
  %div = fdiv double %conv, %40
  %41 = load double*, double** %block.addr, align 4, !tbaa !2
  %42 = load i32, i32* %yi, align 4, !tbaa !6
  %43 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul12 = mul nsw i32 %42, %43
  %44 = load i32, i32* %xi, align 4, !tbaa !6
  %add13 = add nsw i32 %mul12, %44
  %arrayidx14 = getelementptr inbounds double, double* %41, i32 %add13
  store double %div, double* %arrayidx14, align 8, !tbaa !11
  %45 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %46 = load i32, i32* %xi, align 4, !tbaa !6
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %xi, align 4, !tbaa !6
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %47 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %48 = load i32, i32* %yi, align 4, !tbaa !6
  %inc16 = add nsw i32 %48, 1
  store i32 %inc16, i32* %yi, align 4, !tbaa !6
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  %49 = bitcast i16** %data16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %yi, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc46, %if.else
  %50 = load i32, i32* %yi, align 4, !tbaa !6
  %51 = load i32, i32* %block_size, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %50, %51
  br i1 %cmp19, label %for.body21, label %for.end48

for.body21:                                       ; preds = %for.cond18
  %52 = bitcast i32* %y22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #7
  %53 = load i32, i32* %offsy.addr, align 4, !tbaa !6
  %54 = load i32, i32* %yi, align 4, !tbaa !6
  %add23 = add nsw i32 %53, %54
  %55 = load i32, i32* %h.addr, align 4, !tbaa !6
  %sub24 = sub nsw i32 %55, 1
  %call25 = call i32 @clamp(i32 %add23, i32 0, i32 %sub24)
  store i32 %call25, i32* %y22, align 4, !tbaa !6
  store i32 0, i32* %xi, align 4, !tbaa !6
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc43, %for.body21
  %56 = load i32, i32* %xi, align 4, !tbaa !6
  %57 = load i32, i32* %block_size, align 4, !tbaa !6
  %cmp27 = icmp slt i32 %56, %57
  br i1 %cmp27, label %for.body29, label %for.end45

for.body29:                                       ; preds = %for.cond26
  %58 = bitcast i32* %x30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #7
  %59 = load i32, i32* %offsx.addr, align 4, !tbaa !6
  %60 = load i32, i32* %xi, align 4, !tbaa !6
  %add31 = add nsw i32 %59, %60
  %61 = load i32, i32* %w.addr, align 4, !tbaa !6
  %sub32 = sub nsw i32 %61, 1
  %call33 = call i32 @clamp(i32 %add31, i32 0, i32 %sub32)
  store i32 %call33, i32* %x30, align 4, !tbaa !6
  %62 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %63 = load i32, i32* %y22, align 4, !tbaa !6
  %64 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul34 = mul nsw i32 %63, %64
  %65 = load i32, i32* %x30, align 4, !tbaa !6
  %add35 = add nsw i32 %mul34, %65
  %arrayidx36 = getelementptr inbounds i8, i8* %62, i32 %add35
  %66 = load i8, i8* %arrayidx36, align 1, !tbaa !35
  %conv37 = uitofp i8 %66 to double
  %67 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %normalization38 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %67, i32 0, i32 4
  %68 = load double, double* %normalization38, align 8, !tbaa !31
  %div39 = fdiv double %conv37, %68
  %69 = load double*, double** %block.addr, align 4, !tbaa !2
  %70 = load i32, i32* %yi, align 4, !tbaa !6
  %71 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul40 = mul nsw i32 %70, %71
  %72 = load i32, i32* %xi, align 4, !tbaa !6
  %add41 = add nsw i32 %mul40, %72
  %arrayidx42 = getelementptr inbounds double, double* %69, i32 %add41
  store double %div39, double* %arrayidx42, align 8, !tbaa !11
  %73 = bitcast i32* %x30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  br label %for.inc43

for.inc43:                                        ; preds = %for.body29
  %74 = load i32, i32* %xi, align 4, !tbaa !6
  %inc44 = add nsw i32 %74, 1
  store i32 %inc44, i32* %xi, align 4, !tbaa !6
  br label %for.cond26

for.end45:                                        ; preds = %for.cond26
  %75 = bitcast i32* %y22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #7
  br label %for.inc46

for.inc46:                                        ; preds = %for.end45
  %76 = load i32, i32* %yi, align 4, !tbaa !6
  %inc47 = add nsw i32 %76, 1
  store i32 %inc47, i32* %yi, align 4, !tbaa !6
  br label %for.cond18

for.end48:                                        ; preds = %for.cond18
  br label %if.end

if.end:                                           ; preds = %for.end48, %for.end17
  %77 = load double*, double** %block.addr, align 4, !tbaa !2
  %78 = load double*, double** %A, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [3 x double], [3 x double]* %AtA_inv_b, i32 0, i32 0
  %79 = load i32, i32* %n, align 4, !tbaa !6
  call void @multiply_mat(double* %77, double* %78, double* %arraydecay, i32 1, i32 %79, i32 3)
  %80 = load double*, double** %AtA_inv, align 4, !tbaa !2
  %arraydecay49 = getelementptr inbounds [3 x double], [3 x double]* %AtA_inv_b, i32 0, i32 0
  %arraydecay50 = getelementptr inbounds [3 x double], [3 x double]* %plane_coords, i32 0, i32 0
  call void @multiply_mat(double* %80, double* %arraydecay49, double* %arraydecay50, i32 3, i32 3, i32 1)
  %81 = load double*, double** %A, align 4, !tbaa !2
  %arraydecay51 = getelementptr inbounds [3 x double], [3 x double]* %plane_coords, i32 0, i32 0
  %82 = load double*, double** %plane.addr, align 4, !tbaa !2
  %83 = load i32, i32* %n, align 4, !tbaa !6
  call void @multiply_mat(double* %81, double* %arraydecay51, double* %82, i32 %83, i32 3, i32 1)
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc59, %if.end
  %84 = load i32, i32* %i, align 4, !tbaa !6
  %85 = load i32, i32* %n, align 4, !tbaa !6
  %cmp53 = icmp slt i32 %84, %85
  br i1 %cmp53, label %for.body55, label %for.end61

for.body55:                                       ; preds = %for.cond52
  %86 = load double*, double** %plane.addr, align 4, !tbaa !2
  %87 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds double, double* %86, i32 %87
  %88 = load double, double* %arrayidx56, align 8, !tbaa !11
  %89 = load double*, double** %block.addr, align 4, !tbaa !2
  %90 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds double, double* %89, i32 %90
  %91 = load double, double* %arrayidx57, align 8, !tbaa !11
  %sub58 = fsub double %91, %88
  store double %sub58, double* %arrayidx57, align 8, !tbaa !11
  br label %for.inc59

for.inc59:                                        ; preds = %for.body55
  %92 = load i32, i32* %i, align 4, !tbaa !6
  %inc60 = add nsw i32 %92, 1
  store i32 %inc60, i32* %i, align 4, !tbaa !6
  br label %for.cond52

for.end61:                                        ; preds = %for.cond52
  %93 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #7
  %94 = bitcast i32* %yi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #7
  %95 = bitcast i32* %xi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #7
  %96 = bitcast [3 x double]* %AtA_inv_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %96) #7
  %97 = bitcast [3 x double]* %plane_coords to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %97) #7
  %98 = bitcast double** %AtA_inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #7
  %99 = bitcast double** %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %100 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #7
  %101 = bitcast i32* %block_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #5 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal void @multiply_mat(double* %m1, double* %m2, double* %res, i32 %m1_rows, i32 %inner_dim, i32 %m2_cols) #5 {
entry:
  %m1.addr = alloca double*, align 4
  %m2.addr = alloca double*, align 4
  %res.addr = alloca double*, align 4
  %m1_rows.addr = alloca i32, align 4
  %inner_dim.addr = alloca i32, align 4
  %m2_cols.addr = alloca i32, align 4
  %sum = alloca double, align 8
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %inner = alloca i32, align 4
  store double* %m1, double** %m1.addr, align 4, !tbaa !2
  store double* %m2, double** %m2.addr, align 4, !tbaa !2
  store double* %res, double** %res.addr, align 4, !tbaa !2
  store i32 %m1_rows, i32* %m1_rows.addr, align 4, !tbaa !6
  store i32 %inner_dim, i32* %inner_dim.addr, align 4, !tbaa !6
  store i32 %m2_cols, i32* %m2_cols.addr, align 4, !tbaa !6
  %0 = bitcast double* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %1 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast i32* %inner to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %4 = load i32, i32* %row, align 4, !tbaa !6
  %5 = load i32, i32* %m1_rows.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %col, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc12, %for.body
  %6 = load i32, i32* %col, align 4, !tbaa !6
  %7 = load i32, i32* %m2_cols.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %6, %7
  br i1 %cmp2, label %for.body3, label %for.end14

for.body3:                                        ; preds = %for.cond1
  store double 0.000000e+00, double* %sum, align 8, !tbaa !11
  store i32 0, i32* %inner, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body3
  %8 = load i32, i32* %inner, align 4, !tbaa !6
  %9 = load i32, i32* %inner_dim.addr, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %8, %9
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %10 = load double*, double** %m1.addr, align 4, !tbaa !2
  %11 = load i32, i32* %row, align 4, !tbaa !6
  %12 = load i32, i32* %inner_dim.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %11, %12
  %13 = load i32, i32* %inner, align 4, !tbaa !6
  %add = add nsw i32 %mul, %13
  %arrayidx = getelementptr inbounds double, double* %10, i32 %add
  %14 = load double, double* %arrayidx, align 8, !tbaa !11
  %15 = load double*, double** %m2.addr, align 4, !tbaa !2
  %16 = load i32, i32* %inner, align 4, !tbaa !6
  %17 = load i32, i32* %m2_cols.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 %16, %17
  %18 = load i32, i32* %col, align 4, !tbaa !6
  %add8 = add nsw i32 %mul7, %18
  %arrayidx9 = getelementptr inbounds double, double* %15, i32 %add8
  %19 = load double, double* %arrayidx9, align 8, !tbaa !11
  %mul10 = fmul double %14, %19
  %20 = load double, double* %sum, align 8, !tbaa !11
  %add11 = fadd double %20, %mul10
  store double %add11, double* %sum, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %21 = load i32, i32* %inner, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %inner, align 4, !tbaa !6
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %22 = load double, double* %sum, align 8, !tbaa !11
  %23 = load double*, double** %res.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds double, double* %23, i32 1
  store double* %incdec.ptr, double** %res.addr, align 4, !tbaa !2
  store double %22, double* %23, align 8, !tbaa !11
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %24 = load i32, i32* %col, align 4, !tbaa !6
  %inc13 = add nsw i32 %24, 1
  store i32 %inc13, i32* %col, align 4, !tbaa !6
  br label %for.cond1

for.end14:                                        ; preds = %for.cond1
  br label %for.inc15

for.inc15:                                        ; preds = %for.end14
  %25 = load i32, i32* %row, align 4, !tbaa !6
  %inc16 = add nsw i32 %25, 1
  store i32 %inc16, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  %26 = bitcast i32* %inner to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  %28 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #7
  %29 = bitcast double* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %29) #7
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_flat_block_finder_run(%struct.aom_flat_block_finder_t* %block_finder, i8* %data, i32 %w, i32 %h, i32 %stride, i8* %flat_blocks) #0 {
entry:
  %retval = alloca i32, align 4
  %block_finder.addr = alloca %struct.aom_flat_block_finder_t*, align 4
  %data.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %flat_blocks.addr = alloca i8*, align 4
  %block_size = alloca i32, align 4
  %n = alloca i32, align 4
  %kTraceThreshold = alloca double, align 8
  %kRatioThreshold = alloca double, align 8
  %kNormThreshold = alloca double, align 8
  %kVarThreshold = alloca double, align 8
  %num_blocks_w = alloca i32, align 4
  %num_blocks_h = alloca i32, align 4
  %num_flat = alloca i32, align 4
  %bx = alloca i32, align 4
  %by = alloca i32, align 4
  %plane = alloca double*, align 4
  %block = alloca double*, align 4
  %scores = alloca %struct.index_and_score_t*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %Gxx = alloca double, align 8
  %Gxy = alloca double, align 8
  %Gyy = alloca double, align 8
  %var = alloca double, align 8
  %mean = alloca double, align 8
  %xi = alloca i32, align 4
  %yi = alloca i32, align 4
  %gx = alloca double, align 8
  %gy = alloca double, align 8
  %trace = alloca double, align 8
  %det = alloca double, align 8
  %e1 = alloca double, align 8
  %e2 = alloca double, align 8
  %norm = alloca double, align 8
  %ratio = alloca double, align 8
  %is_flat = alloca i32, align 4
  %weights = alloca [5 x double], align 16
  %sum_weights = alloca double, align 8
  %score = alloca float, align 4
  %top_nth_percentile = alloca i32, align 4
  %score_threshold = alloca float, align 4
  %i = alloca i32, align 4
  store %struct.aom_flat_block_finder_t* %block_finder, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %flat_blocks, i8** %flat_blocks.addr, align 4, !tbaa !2
  %0 = bitcast i32* %block_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %block_size1 = getelementptr inbounds %struct.aom_flat_block_finder_t, %struct.aom_flat_block_finder_t* %1, i32 0, i32 3
  %2 = load i32, i32* %block_size1, align 4, !tbaa !30
  store i32 %2, i32* %block_size, align 4, !tbaa !6
  %3 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %block_size, align 4, !tbaa !6
  %5 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul = mul nsw i32 %4, %5
  store i32 %mul, i32* %n, align 4, !tbaa !6
  %6 = bitcast double* %kTraceThreshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #7
  store double 0x3F23333333333333, double* %kTraceThreshold, align 8, !tbaa !11
  %7 = bitcast double* %kRatioThreshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #7
  store double 1.250000e+00, double* %kRatioThreshold, align 8, !tbaa !11
  %8 = bitcast double* %kNormThreshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #7
  store double 7.812500e-05, double* %kNormThreshold, align 8, !tbaa !11
  %9 = bitcast double* %kVarThreshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %9) #7
  %10 = load i32, i32* %n, align 4, !tbaa !6
  %conv = sitofp i32 %10 to double
  %div = fdiv double 5.000000e-03, %conv
  store double %div, double* %kVarThreshold, align 8, !tbaa !11
  %11 = bitcast i32* %num_blocks_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load i32, i32* %w.addr, align 4, !tbaa !6
  %13 = load i32, i32* %block_size, align 4, !tbaa !6
  %add = add nsw i32 %12, %13
  %sub = sub nsw i32 %add, 1
  %14 = load i32, i32* %block_size, align 4, !tbaa !6
  %div2 = sdiv i32 %sub, %14
  store i32 %div2, i32* %num_blocks_w, align 4, !tbaa !6
  %15 = bitcast i32* %num_blocks_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = load i32, i32* %h.addr, align 4, !tbaa !6
  %17 = load i32, i32* %block_size, align 4, !tbaa !6
  %add3 = add nsw i32 %16, %17
  %sub4 = sub nsw i32 %add3, 1
  %18 = load i32, i32* %block_size, align 4, !tbaa !6
  %div5 = sdiv i32 %sub4, %18
  store i32 %div5, i32* %num_blocks_h, align 4, !tbaa !6
  %19 = bitcast i32* %num_flat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  store i32 0, i32* %num_flat, align 4, !tbaa !6
  %20 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  store i32 0, i32* %bx, align 4, !tbaa !6
  %21 = bitcast i32* %by to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  store i32 0, i32* %by, align 4, !tbaa !6
  %22 = bitcast double** %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  %23 = load i32, i32* %n, align 4, !tbaa !6
  %mul6 = mul i32 %23, 8
  %call = call i8* @aom_malloc(i32 %mul6)
  %24 = bitcast i8* %call to double*
  store double* %24, double** %plane, align 4, !tbaa !2
  %25 = bitcast double** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load i32, i32* %n, align 4, !tbaa !6
  %mul7 = mul i32 %26, 8
  %call8 = call i8* @aom_malloc(i32 %mul7)
  %27 = bitcast i8* %call8 to double*
  store double* %27, double** %block, align 4, !tbaa !2
  %28 = bitcast %struct.index_and_score_t** %scores to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %30 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %mul9 = mul nsw i32 %29, %30
  %mul10 = mul i32 %mul9, 8
  %call11 = call i8* @aom_malloc(i32 %mul10)
  %31 = bitcast i8* %call11 to %struct.index_and_score_t*
  store %struct.index_and_score_t* %31, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %32 = load double*, double** %plane, align 4, !tbaa !2
  %cmp = icmp eq double* %32, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %33 = load double*, double** %block, align 4, !tbaa !2
  %cmp13 = icmp eq double* %33, null
  br i1 %cmp13, label %if.then, label %lor.lhs.false15

lor.lhs.false15:                                  ; preds = %lor.lhs.false
  %34 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %cmp16 = icmp eq %struct.index_and_score_t* %34, null
  br i1 %cmp16, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false15, %lor.lhs.false, %entry
  %35 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %36 = load i32, i32* %n, align 4, !tbaa !6
  %call18 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %35, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.4, i32 0, i32 0), i32 %36)
  %37 = load double*, double** %plane, align 4, !tbaa !2
  %38 = bitcast double* %37 to i8*
  call void @aom_free(i8* %38)
  %39 = load double*, double** %block, align 4, !tbaa !2
  %40 = bitcast double* %39 to i8*
  call void @aom_free(i8* %40)
  %41 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %42 = bitcast %struct.index_and_score_t* %41 to i8*
  call void @aom_free(i8* %42)
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false15
  store i32 0, i32* %by, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc171, %if.end
  %43 = load i32, i32* %by, align 4, !tbaa !6
  %44 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %43, %44
  br i1 %cmp19, label %for.body, label %for.end173

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %bx, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc168, %for.body
  %45 = load i32, i32* %bx, align 4, !tbaa !6
  %46 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %45, %46
  br i1 %cmp22, label %for.body24, label %for.end170

for.body24:                                       ; preds = %for.cond21
  %47 = bitcast double* %Gxx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %47) #7
  store double 0.000000e+00, double* %Gxx, align 8, !tbaa !11
  %48 = bitcast double* %Gxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %48) #7
  store double 0.000000e+00, double* %Gxy, align 8, !tbaa !11
  %49 = bitcast double* %Gyy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %49) #7
  store double 0.000000e+00, double* %Gyy, align 8, !tbaa !11
  %50 = bitcast double* %var to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %50) #7
  store double 0.000000e+00, double* %var, align 8, !tbaa !11
  %51 = bitcast double* %mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %51) #7
  store double 0.000000e+00, double* %mean, align 8, !tbaa !11
  %52 = bitcast i32* %xi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #7
  %53 = bitcast i32* %yi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #7
  %54 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder.addr, align 4, !tbaa !2
  %55 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %56 = load i32, i32* %w.addr, align 4, !tbaa !6
  %57 = load i32, i32* %h.addr, align 4, !tbaa !6
  %58 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %59 = load i32, i32* %bx, align 4, !tbaa !6
  %60 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul25 = mul nsw i32 %59, %60
  %61 = load i32, i32* %by, align 4, !tbaa !6
  %62 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul26 = mul nsw i32 %61, %62
  %63 = load double*, double** %plane, align 4, !tbaa !2
  %64 = load double*, double** %block, align 4, !tbaa !2
  call void @aom_flat_block_finder_extract_block(%struct.aom_flat_block_finder_t* %54, i8* %55, i32 %56, i32 %57, i32 %58, i32 %mul25, i32 %mul26, double* %63, double* %64)
  store i32 1, i32* %yi, align 4, !tbaa !6
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc74, %for.body24
  %65 = load i32, i32* %yi, align 4, !tbaa !6
  %66 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub28 = sub nsw i32 %66, 1
  %cmp29 = icmp slt i32 %65, %sub28
  br i1 %cmp29, label %for.body31, label %for.end76

for.body31:                                       ; preds = %for.cond27
  store i32 1, i32* %xi, align 4, !tbaa !6
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %for.body31
  %67 = load i32, i32* %xi, align 4, !tbaa !6
  %68 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub33 = sub nsw i32 %68, 1
  %cmp34 = icmp slt i32 %67, %sub33
  br i1 %cmp34, label %for.body36, label %for.end

for.body36:                                       ; preds = %for.cond32
  %69 = bitcast double* %gx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %69) #7
  %70 = load double*, double** %block, align 4, !tbaa !2
  %71 = load i32, i32* %yi, align 4, !tbaa !6
  %72 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul37 = mul nsw i32 %71, %72
  %73 = load i32, i32* %xi, align 4, !tbaa !6
  %add38 = add nsw i32 %mul37, %73
  %add39 = add nsw i32 %add38, 1
  %arrayidx = getelementptr inbounds double, double* %70, i32 %add39
  %74 = load double, double* %arrayidx, align 8, !tbaa !11
  %75 = load double*, double** %block, align 4, !tbaa !2
  %76 = load i32, i32* %yi, align 4, !tbaa !6
  %77 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul40 = mul nsw i32 %76, %77
  %78 = load i32, i32* %xi, align 4, !tbaa !6
  %add41 = add nsw i32 %mul40, %78
  %sub42 = sub nsw i32 %add41, 1
  %arrayidx43 = getelementptr inbounds double, double* %75, i32 %sub42
  %79 = load double, double* %arrayidx43, align 8, !tbaa !11
  %sub44 = fsub double %74, %79
  %div45 = fdiv double %sub44, 2.000000e+00
  store double %div45, double* %gx, align 8, !tbaa !11
  %80 = bitcast double* %gy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %80) #7
  %81 = load double*, double** %block, align 4, !tbaa !2
  %82 = load i32, i32* %yi, align 4, !tbaa !6
  %83 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul46 = mul nsw i32 %82, %83
  %84 = load i32, i32* %xi, align 4, !tbaa !6
  %add47 = add nsw i32 %mul46, %84
  %85 = load i32, i32* %block_size, align 4, !tbaa !6
  %add48 = add nsw i32 %add47, %85
  %arrayidx49 = getelementptr inbounds double, double* %81, i32 %add48
  %86 = load double, double* %arrayidx49, align 8, !tbaa !11
  %87 = load double*, double** %block, align 4, !tbaa !2
  %88 = load i32, i32* %yi, align 4, !tbaa !6
  %89 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul50 = mul nsw i32 %88, %89
  %90 = load i32, i32* %xi, align 4, !tbaa !6
  %add51 = add nsw i32 %mul50, %90
  %91 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub52 = sub nsw i32 %add51, %91
  %arrayidx53 = getelementptr inbounds double, double* %87, i32 %sub52
  %92 = load double, double* %arrayidx53, align 8, !tbaa !11
  %sub54 = fsub double %86, %92
  %div55 = fdiv double %sub54, 2.000000e+00
  store double %div55, double* %gy, align 8, !tbaa !11
  %93 = load double, double* %gx, align 8, !tbaa !11
  %94 = load double, double* %gx, align 8, !tbaa !11
  %mul56 = fmul double %93, %94
  %95 = load double, double* %Gxx, align 8, !tbaa !11
  %add57 = fadd double %95, %mul56
  store double %add57, double* %Gxx, align 8, !tbaa !11
  %96 = load double, double* %gx, align 8, !tbaa !11
  %97 = load double, double* %gy, align 8, !tbaa !11
  %mul58 = fmul double %96, %97
  %98 = load double, double* %Gxy, align 8, !tbaa !11
  %add59 = fadd double %98, %mul58
  store double %add59, double* %Gxy, align 8, !tbaa !11
  %99 = load double, double* %gy, align 8, !tbaa !11
  %100 = load double, double* %gy, align 8, !tbaa !11
  %mul60 = fmul double %99, %100
  %101 = load double, double* %Gyy, align 8, !tbaa !11
  %add61 = fadd double %101, %mul60
  store double %add61, double* %Gyy, align 8, !tbaa !11
  %102 = load double*, double** %block, align 4, !tbaa !2
  %103 = load i32, i32* %yi, align 4, !tbaa !6
  %104 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul62 = mul nsw i32 %103, %104
  %105 = load i32, i32* %xi, align 4, !tbaa !6
  %add63 = add nsw i32 %mul62, %105
  %arrayidx64 = getelementptr inbounds double, double* %102, i32 %add63
  %106 = load double, double* %arrayidx64, align 8, !tbaa !11
  %107 = load double, double* %mean, align 8, !tbaa !11
  %add65 = fadd double %107, %106
  store double %add65, double* %mean, align 8, !tbaa !11
  %108 = load double*, double** %block, align 4, !tbaa !2
  %109 = load i32, i32* %yi, align 4, !tbaa !6
  %110 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul66 = mul nsw i32 %109, %110
  %111 = load i32, i32* %xi, align 4, !tbaa !6
  %add67 = add nsw i32 %mul66, %111
  %arrayidx68 = getelementptr inbounds double, double* %108, i32 %add67
  %112 = load double, double* %arrayidx68, align 8, !tbaa !11
  %113 = load double*, double** %block, align 4, !tbaa !2
  %114 = load i32, i32* %yi, align 4, !tbaa !6
  %115 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul69 = mul nsw i32 %114, %115
  %116 = load i32, i32* %xi, align 4, !tbaa !6
  %add70 = add nsw i32 %mul69, %116
  %arrayidx71 = getelementptr inbounds double, double* %113, i32 %add70
  %117 = load double, double* %arrayidx71, align 8, !tbaa !11
  %mul72 = fmul double %112, %117
  %118 = load double, double* %var, align 8, !tbaa !11
  %add73 = fadd double %118, %mul72
  store double %add73, double* %var, align 8, !tbaa !11
  %119 = bitcast double* %gy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %119) #7
  %120 = bitcast double* %gx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %120) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body36
  %121 = load i32, i32* %xi, align 4, !tbaa !6
  %inc = add nsw i32 %121, 1
  store i32 %inc, i32* %xi, align 4, !tbaa !6
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  br label %for.inc74

for.inc74:                                        ; preds = %for.end
  %122 = load i32, i32* %yi, align 4, !tbaa !6
  %inc75 = add nsw i32 %122, 1
  store i32 %inc75, i32* %yi, align 4, !tbaa !6
  br label %for.cond27

for.end76:                                        ; preds = %for.cond27
  %123 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub77 = sub nsw i32 %123, 2
  %124 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub78 = sub nsw i32 %124, 2
  %mul79 = mul nsw i32 %sub77, %sub78
  %conv80 = sitofp i32 %mul79 to double
  %125 = load double, double* %mean, align 8, !tbaa !11
  %div81 = fdiv double %125, %conv80
  store double %div81, double* %mean, align 8, !tbaa !11
  %126 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub82 = sub nsw i32 %126, 2
  %127 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub83 = sub nsw i32 %127, 2
  %mul84 = mul nsw i32 %sub82, %sub83
  %conv85 = sitofp i32 %mul84 to double
  %128 = load double, double* %Gxx, align 8, !tbaa !11
  %div86 = fdiv double %128, %conv85
  store double %div86, double* %Gxx, align 8, !tbaa !11
  %129 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub87 = sub nsw i32 %129, 2
  %130 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub88 = sub nsw i32 %130, 2
  %mul89 = mul nsw i32 %sub87, %sub88
  %conv90 = sitofp i32 %mul89 to double
  %131 = load double, double* %Gxy, align 8, !tbaa !11
  %div91 = fdiv double %131, %conv90
  store double %div91, double* %Gxy, align 8, !tbaa !11
  %132 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub92 = sub nsw i32 %132, 2
  %133 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub93 = sub nsw i32 %133, 2
  %mul94 = mul nsw i32 %sub92, %sub93
  %conv95 = sitofp i32 %mul94 to double
  %134 = load double, double* %Gyy, align 8, !tbaa !11
  %div96 = fdiv double %134, %conv95
  store double %div96, double* %Gyy, align 8, !tbaa !11
  %135 = load double, double* %var, align 8, !tbaa !11
  %136 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub97 = sub nsw i32 %136, 2
  %137 = load i32, i32* %block_size, align 4, !tbaa !6
  %sub98 = sub nsw i32 %137, 2
  %mul99 = mul nsw i32 %sub97, %sub98
  %conv100 = sitofp i32 %mul99 to double
  %div101 = fdiv double %135, %conv100
  %138 = load double, double* %mean, align 8, !tbaa !11
  %139 = load double, double* %mean, align 8, !tbaa !11
  %mul102 = fmul double %138, %139
  %sub103 = fsub double %div101, %mul102
  store double %sub103, double* %var, align 8, !tbaa !11
  %140 = bitcast double* %trace to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %140) #7
  %141 = load double, double* %Gxx, align 8, !tbaa !11
  %142 = load double, double* %Gyy, align 8, !tbaa !11
  %add104 = fadd double %141, %142
  store double %add104, double* %trace, align 8, !tbaa !11
  %143 = bitcast double* %det to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %143) #7
  %144 = load double, double* %Gxx, align 8, !tbaa !11
  %145 = load double, double* %Gyy, align 8, !tbaa !11
  %mul105 = fmul double %144, %145
  %146 = load double, double* %Gxy, align 8, !tbaa !11
  %147 = load double, double* %Gxy, align 8, !tbaa !11
  %mul106 = fmul double %146, %147
  %sub107 = fsub double %mul105, %mul106
  store double %sub107, double* %det, align 8, !tbaa !11
  %148 = bitcast double* %e1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %148) #7
  %149 = load double, double* %trace, align 8, !tbaa !11
  %150 = load double, double* %trace, align 8, !tbaa !11
  %151 = load double, double* %trace, align 8, !tbaa !11
  %mul108 = fmul double %150, %151
  %152 = load double, double* %det, align 8, !tbaa !11
  %mul109 = fmul double 4.000000e+00, %152
  %sub110 = fsub double %mul108, %mul109
  %153 = call double @llvm.sqrt.f64(double %sub110)
  %add111 = fadd double %149, %153
  %div112 = fdiv double %add111, 2.000000e+00
  store double %div112, double* %e1, align 8, !tbaa !11
  %154 = bitcast double* %e2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %154) #7
  %155 = load double, double* %trace, align 8, !tbaa !11
  %156 = load double, double* %trace, align 8, !tbaa !11
  %157 = load double, double* %trace, align 8, !tbaa !11
  %mul113 = fmul double %156, %157
  %158 = load double, double* %det, align 8, !tbaa !11
  %mul114 = fmul double 4.000000e+00, %158
  %sub115 = fsub double %mul113, %mul114
  %159 = call double @llvm.sqrt.f64(double %sub115)
  %sub116 = fsub double %155, %159
  %div117 = fdiv double %sub116, 2.000000e+00
  store double %div117, double* %e2, align 8, !tbaa !11
  %160 = bitcast double* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %160) #7
  %161 = load double, double* %e1, align 8, !tbaa !11
  store double %161, double* %norm, align 8, !tbaa !11
  %162 = bitcast double* %ratio to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %162) #7
  %163 = load double, double* %e1, align 8, !tbaa !11
  %164 = load double, double* %e2, align 8, !tbaa !11
  %cmp118 = fcmp ogt double %164, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp118, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end76
  %165 = load double, double* %e2, align 8, !tbaa !11
  br label %cond.end

cond.false:                                       ; preds = %for.end76
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %165, %cond.true ], [ 0x3EB0C6F7A0B5ED8D, %cond.false ]
  %div120 = fdiv double %163, %cond
  store double %div120, double* %ratio, align 8, !tbaa !11
  %166 = bitcast i32* %is_flat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %166) #7
  %167 = load double, double* %trace, align 8, !tbaa !11
  %cmp121 = fcmp olt double %167, 0x3F23333333333333
  br i1 %cmp121, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %cond.end
  %168 = load double, double* %ratio, align 8, !tbaa !11
  %cmp123 = fcmp olt double %168, 1.250000e+00
  br i1 %cmp123, label %land.lhs.true125, label %land.end

land.lhs.true125:                                 ; preds = %land.lhs.true
  %169 = load double, double* %norm, align 8, !tbaa !11
  %cmp126 = fcmp olt double %169, 7.812500e-05
  br i1 %cmp126, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true125
  %170 = load double, double* %var, align 8, !tbaa !11
  %171 = load double, double* %kVarThreshold, align 8, !tbaa !11
  %cmp128 = fcmp ogt double %170, %171
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true125, %land.lhs.true, %cond.end
  %172 = phi i1 [ false, %land.lhs.true125 ], [ false, %land.lhs.true ], [ false, %cond.end ], [ %cmp128, %land.rhs ]
  %land.ext = zext i1 %172 to i32
  store i32 %land.ext, i32* %is_flat, align 4, !tbaa !6
  %173 = bitcast [5 x double]* %weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* %173) #7
  %174 = bitcast [5 x double]* %weights to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %174, i8 0, i32 40, i1 false)
  %175 = bitcast i8* %174 to [5 x double]*
  %176 = getelementptr inbounds [5 x double], [5 x double]* %175, i32 0, i32 0
  store double -6.682000e+03, double* %176, align 16
  %177 = getelementptr inbounds [5 x double], [5 x double]* %175, i32 0, i32 1
  store double -2.056000e-01, double* %177, align 8
  %178 = getelementptr inbounds [5 x double], [5 x double]* %175, i32 0, i32 2
  store double 1.308700e+04, double* %178, align 16
  %179 = getelementptr inbounds [5 x double], [5 x double]* %175, i32 0, i32 3
  store double -1.243400e+04, double* %179, align 8
  %180 = getelementptr inbounds [5 x double], [5 x double]* %175, i32 0, i32 4
  store double 2.569400e+00, double* %180, align 16
  %181 = bitcast double* %sum_weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %181) #7
  %arrayidx130 = getelementptr inbounds [5 x double], [5 x double]* %weights, i32 0, i32 0
  %182 = load double, double* %arrayidx130, align 16, !tbaa !11
  %183 = load double, double* %var, align 8, !tbaa !11
  %mul131 = fmul double %182, %183
  %arrayidx132 = getelementptr inbounds [5 x double], [5 x double]* %weights, i32 0, i32 1
  %184 = load double, double* %arrayidx132, align 8, !tbaa !11
  %185 = load double, double* %ratio, align 8, !tbaa !11
  %mul133 = fmul double %184, %185
  %add134 = fadd double %mul131, %mul133
  %arrayidx135 = getelementptr inbounds [5 x double], [5 x double]* %weights, i32 0, i32 2
  %186 = load double, double* %arrayidx135, align 16, !tbaa !11
  %187 = load double, double* %trace, align 8, !tbaa !11
  %mul136 = fmul double %186, %187
  %add137 = fadd double %add134, %mul136
  %arrayidx138 = getelementptr inbounds [5 x double], [5 x double]* %weights, i32 0, i32 3
  %188 = load double, double* %arrayidx138, align 8, !tbaa !11
  %189 = load double, double* %norm, align 8, !tbaa !11
  %mul139 = fmul double %188, %189
  %add140 = fadd double %add137, %mul139
  %arrayidx141 = getelementptr inbounds [5 x double], [5 x double]* %weights, i32 0, i32 4
  %190 = load double, double* %arrayidx141, align 16, !tbaa !11
  %add142 = fadd double %add140, %190
  store double %add142, double* %sum_weights, align 8, !tbaa !11
  %191 = load double, double* %sum_weights, align 8, !tbaa !11
  %call143 = call double @fclamp(double %191, double -2.500000e+01, double 1.000000e+02)
  store double %call143, double* %sum_weights, align 8, !tbaa !11
  %192 = bitcast float* %score to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %192) #7
  %193 = load double, double* %sum_weights, align 8, !tbaa !11
  %fneg = fneg double %193
  %194 = call double @llvm.exp.f64(double %fneg)
  %add144 = fadd double 1.000000e+00, %194
  %div145 = fdiv double 1.000000e+00, %add144
  %conv146 = fptrunc double %div145 to float
  store float %conv146, float* %score, align 4, !tbaa !36
  %195 = load i32, i32* %is_flat, align 4, !tbaa !6
  %tobool = icmp ne i32 %195, 0
  %196 = zext i1 %tobool to i64
  %cond147 = select i1 %tobool, i32 255, i32 0
  %conv148 = trunc i32 %cond147 to i8
  %197 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %198 = load i32, i32* %by, align 4, !tbaa !6
  %199 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %mul149 = mul nsw i32 %198, %199
  %200 = load i32, i32* %bx, align 4, !tbaa !6
  %add150 = add nsw i32 %mul149, %200
  %arrayidx151 = getelementptr inbounds i8, i8* %197, i32 %add150
  store i8 %conv148, i8* %arrayidx151, align 1, !tbaa !35
  %201 = load double, double* %var, align 8, !tbaa !11
  %202 = load double, double* %kVarThreshold, align 8, !tbaa !11
  %cmp152 = fcmp ogt double %201, %202
  br i1 %cmp152, label %cond.true154, label %cond.false155

cond.true154:                                     ; preds = %land.end
  %203 = load float, float* %score, align 4, !tbaa !36
  br label %cond.end156

cond.false155:                                    ; preds = %land.end
  br label %cond.end156

cond.end156:                                      ; preds = %cond.false155, %cond.true154
  %cond157 = phi float [ %203, %cond.true154 ], [ 0.000000e+00, %cond.false155 ]
  %204 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %205 = load i32, i32* %by, align 4, !tbaa !6
  %206 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %mul158 = mul nsw i32 %205, %206
  %207 = load i32, i32* %bx, align 4, !tbaa !6
  %add159 = add nsw i32 %mul158, %207
  %arrayidx160 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %204, i32 %add159
  %score161 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %arrayidx160, i32 0, i32 1
  store float %cond157, float* %score161, align 4, !tbaa !38
  %208 = load i32, i32* %by, align 4, !tbaa !6
  %209 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %mul162 = mul nsw i32 %208, %209
  %210 = load i32, i32* %bx, align 4, !tbaa !6
  %add163 = add nsw i32 %mul162, %210
  %211 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %212 = load i32, i32* %by, align 4, !tbaa !6
  %213 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %mul164 = mul nsw i32 %212, %213
  %214 = load i32, i32* %bx, align 4, !tbaa !6
  %add165 = add nsw i32 %mul164, %214
  %arrayidx166 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %211, i32 %add165
  %index = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %arrayidx166, i32 0, i32 0
  store i32 %add163, i32* %index, align 4, !tbaa !40
  %215 = load i32, i32* %is_flat, align 4, !tbaa !6
  %216 = load i32, i32* %num_flat, align 4, !tbaa !6
  %add167 = add nsw i32 %216, %215
  store i32 %add167, i32* %num_flat, align 4, !tbaa !6
  %217 = bitcast float* %score to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #7
  %218 = bitcast double* %sum_weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %218) #7
  %219 = bitcast [5 x double]* %weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 40, i8* %219) #7
  %220 = bitcast i32* %is_flat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #7
  %221 = bitcast double* %ratio to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %221) #7
  %222 = bitcast double* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %222) #7
  %223 = bitcast double* %e2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %223) #7
  %224 = bitcast double* %e1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %224) #7
  %225 = bitcast double* %det to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %225) #7
  %226 = bitcast double* %trace to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %226) #7
  %227 = bitcast i32* %yi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #7
  %228 = bitcast i32* %xi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #7
  %229 = bitcast double* %mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %229) #7
  %230 = bitcast double* %var to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %230) #7
  %231 = bitcast double* %Gyy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %231) #7
  %232 = bitcast double* %Gxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %232) #7
  %233 = bitcast double* %Gxx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %233) #7
  br label %for.inc168

for.inc168:                                       ; preds = %cond.end156
  %234 = load i32, i32* %bx, align 4, !tbaa !6
  %inc169 = add nsw i32 %234, 1
  store i32 %inc169, i32* %bx, align 4, !tbaa !6
  br label %for.cond21

for.end170:                                       ; preds = %for.cond21
  br label %for.inc171

for.inc171:                                       ; preds = %for.end170
  %235 = load i32, i32* %by, align 4, !tbaa !6
  %inc172 = add nsw i32 %235, 1
  store i32 %inc172, i32* %by, align 4, !tbaa !6
  br label %for.cond

for.end173:                                       ; preds = %for.cond
  %236 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %237 = bitcast %struct.index_and_score_t* %236 to i8*
  %238 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %239 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %mul174 = mul nsw i32 %238, %239
  call void @qsort(i8* %237, i32 %mul174, i32 8, i32 (i8*, i8*)* @compare_scores)
  %240 = bitcast i32* %top_nth_percentile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %240) #7
  %241 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %242 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %mul175 = mul nsw i32 %241, %242
  %mul176 = mul nsw i32 %mul175, 90
  %div177 = sdiv i32 %mul176, 100
  store i32 %div177, i32* %top_nth_percentile, align 4, !tbaa !6
  %243 = bitcast float* %score_threshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %243) #7
  %244 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %245 = load i32, i32* %top_nth_percentile, align 4, !tbaa !6
  %arrayidx178 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %244, i32 %245
  %score179 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %arrayidx178, i32 0, i32 1
  %246 = load float, float* %score179, align 4, !tbaa !38
  store float %246, float* %score_threshold, align 4, !tbaa !36
  %247 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %247) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond180

for.cond180:                                      ; preds = %for.inc203, %for.end173
  %248 = load i32, i32* %i, align 4, !tbaa !6
  %249 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %250 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %mul181 = mul nsw i32 %249, %250
  %cmp182 = icmp slt i32 %248, %mul181
  br i1 %cmp182, label %for.body184, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond180
  store i32 14, i32* %cleanup.dest.slot, align 4
  %251 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #7
  br label %for.end205

for.body184:                                      ; preds = %for.cond180
  %252 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %253 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx185 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %252, i32 %253
  %score186 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %arrayidx185, i32 0, i32 1
  %254 = load float, float* %score186, align 4, !tbaa !38
  %255 = load float, float* %score_threshold, align 4, !tbaa !36
  %cmp187 = fcmp oge float %254, %255
  br i1 %cmp187, label %if.then189, label %if.end202

if.then189:                                       ; preds = %for.body184
  %256 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %257 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %258 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx190 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %257, i32 %258
  %index191 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %arrayidx190, i32 0, i32 0
  %259 = load i32, i32* %index191, align 4, !tbaa !40
  %arrayidx192 = getelementptr inbounds i8, i8* %256, i32 %259
  %260 = load i8, i8* %arrayidx192, align 1, !tbaa !35
  %conv193 = zext i8 %260 to i32
  %cmp194 = icmp eq i32 %conv193, 0
  %conv195 = zext i1 %cmp194 to i32
  %261 = load i32, i32* %num_flat, align 4, !tbaa !6
  %add196 = add nsw i32 %261, %conv195
  store i32 %add196, i32* %num_flat, align 4, !tbaa !6
  %262 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %263 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %264 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx197 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %263, i32 %264
  %index198 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %arrayidx197, i32 0, i32 0
  %265 = load i32, i32* %index198, align 4, !tbaa !40
  %arrayidx199 = getelementptr inbounds i8, i8* %262, i32 %265
  %266 = load i8, i8* %arrayidx199, align 1, !tbaa !35
  %conv200 = zext i8 %266 to i32
  %or = or i32 %conv200, 1
  %conv201 = trunc i32 %or to i8
  store i8 %conv201, i8* %arrayidx199, align 1, !tbaa !35
  br label %if.end202

if.end202:                                        ; preds = %if.then189, %for.body184
  br label %for.inc203

for.inc203:                                       ; preds = %if.end202
  %267 = load i32, i32* %i, align 4, !tbaa !6
  %inc204 = add nsw i32 %267, 1
  store i32 %inc204, i32* %i, align 4, !tbaa !6
  br label %for.cond180

for.end205:                                       ; preds = %for.cond.cleanup
  %268 = load double*, double** %block, align 4, !tbaa !2
  %269 = bitcast double* %268 to i8*
  call void @aom_free(i8* %269)
  %270 = load double*, double** %plane, align 4, !tbaa !2
  %271 = bitcast double* %270 to i8*
  call void @aom_free(i8* %271)
  %272 = load %struct.index_and_score_t*, %struct.index_and_score_t** %scores, align 4, !tbaa !2
  %273 = bitcast %struct.index_and_score_t* %272 to i8*
  call void @aom_free(i8* %273)
  %274 = load i32, i32* %num_flat, align 4, !tbaa !6
  store i32 %274, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %275 = bitcast float* %score_threshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #7
  %276 = bitcast i32* %top_nth_percentile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #7
  br label %cleanup

cleanup:                                          ; preds = %for.end205, %if.then
  %277 = bitcast %struct.index_and_score_t** %scores to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #7
  %278 = bitcast double** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #7
  %279 = bitcast double** %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #7
  %280 = bitcast i32* %by to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #7
  %281 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #7
  %282 = bitcast i32* %num_flat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #7
  %283 = bitcast i32* %num_blocks_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %283) #7
  %284 = bitcast i32* %num_blocks_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #7
  %285 = bitcast double* %kVarThreshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %285) #7
  %286 = bitcast double* %kNormThreshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %286) #7
  %287 = bitcast double* %kRatioThreshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %287) #7
  %288 = bitcast double* %kTraceThreshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %288) #7
  %289 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %289) #7
  %290 = bitcast i32* %block_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %290) #7
  %291 = load i32, i32* %retval, align 4
  ret i32 %291
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sqrt.f64(double) #4

; Function Attrs: inlinehint nounwind
define internal double @fclamp(double %value, double %low, double %high) #5 {
entry:
  %value.addr = alloca double, align 8
  %low.addr = alloca double, align 8
  %high.addr = alloca double, align 8
  store double %value, double* %value.addr, align 8, !tbaa !11
  store double %low, double* %low.addr, align 8, !tbaa !11
  store double %high, double* %high.addr, align 8, !tbaa !11
  %0 = load double, double* %value.addr, align 8, !tbaa !11
  %1 = load double, double* %low.addr, align 8, !tbaa !11
  %cmp = fcmp olt double %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load double, double* %low.addr, align 8, !tbaa !11
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load double, double* %value.addr, align 8, !tbaa !11
  %4 = load double, double* %high.addr, align 8, !tbaa !11
  %cmp1 = fcmp ogt double %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load double, double* %high.addr, align 8, !tbaa !11
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load double, double* %value.addr, align 8, !tbaa !11
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi double [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi double [ %2, %cond.true ], [ %cond, %cond.end ]
  ret double %cond5
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.exp.f64(double) #4

declare void @qsort(i8*, i32, i32, i32 (i8*, i8*)*) #1

; Function Attrs: nounwind
define internal i32 @compare_scores(i8* %a, i8* %b) #0 {
entry:
  %retval = alloca i32, align 4
  %a.addr = alloca i8*, align 4
  %b.addr = alloca i8*, align 4
  %diff = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %a, i8** %a.addr, align 4, !tbaa !2
  store i8* %b, i8** %b.addr, align 4, !tbaa !2
  %0 = bitcast float* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.index_and_score_t*
  %score = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %2, i32 0, i32 1
  %3 = load float, float* %score, align 4, !tbaa !38
  %4 = load i8*, i8** %b.addr, align 4, !tbaa !2
  %5 = bitcast i8* %4 to %struct.index_and_score_t*
  %score1 = getelementptr inbounds %struct.index_and_score_t, %struct.index_and_score_t* %5, i32 0, i32 1
  %6 = load float, float* %score1, align 4, !tbaa !38
  %sub = fsub float %3, %6
  store float %sub, float* %diff, align 4, !tbaa !36
  %7 = load float, float* %diff, align 4, !tbaa !36
  %cmp = fcmp olt float %7, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %8 = load float, float* %diff, align 4, !tbaa !36
  %cmp2 = fcmp ogt float %8, 0.000000e+00
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end4, %if.then3, %if.then
  %9 = bitcast float* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define hidden i32 @aom_noise_model_init(%struct.aom_noise_model_t* %model, %struct.aom_noise_model_params_t* byval(%struct.aom_noise_model_params_t) align 4 %params) #0 {
entry:
  %retval = alloca i32, align 4
  %model.addr = alloca %struct.aom_noise_model_t*, align 4
  %n = alloca i32, align 4
  %lag = alloca i32, align 4
  %bit_depth = alloca i32, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %i = alloca i32, align 4
  %c = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %max_x = alloca i32, align 4
  store %struct.aom_noise_model_t* %model, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @num_coeffs(%struct.aom_noise_model_params_t* byval(%struct.aom_noise_model_params_t) align 4 %params)
  store i32 %call, i32* %n, align 4, !tbaa !6
  %1 = bitcast i32* %lag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %lag1 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %2 = load i32, i32* %lag1, align 4, !tbaa !41
  store i32 %2, i32* %lag, align 4, !tbaa !6
  %3 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %bit_depth2 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 2
  %4 = load i32, i32* %bit_depth2, align 4, !tbaa !43
  store i32 %4, i32* %bit_depth, align 4, !tbaa !6
  %5 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  %6 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  %8 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store i32 0, i32* %c, align 4, !tbaa !6
  %9 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %10 = bitcast %struct.aom_noise_model_t* %9 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %10, i8 0, i32 512, i1 false)
  %lag3 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %11 = load i32, i32* %lag3, align 4, !tbaa !41
  %cmp = icmp slt i32 %11, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %lag4 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %13 = load i32, i32* %lag4, align 4, !tbaa !41
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.5, i32 0, i32 0), i32 %13)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

if.end:                                           ; preds = %entry
  %lag6 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %14 = load i32, i32* %lag6, align 4, !tbaa !41
  %cmp7 = icmp sgt i32 %14, 4
  br i1 %cmp7, label %if.then8, label %if.end11

if.then8:                                         ; preds = %if.end
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %lag9 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %16 = load i32, i32* %lag9, align 4, !tbaa !41
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.6, i32 0, i32 0), i32 %16, i32 4)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

if.end11:                                         ; preds = %if.end
  %17 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %params12 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %17, i32 0, i32 0
  %18 = bitcast %struct.aom_noise_model_params_t* %params12 to i8*
  %19 = bitcast %struct.aom_noise_model_params_t* %params to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %18, i8* align 4 %19, i32 16, i1 false)
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end11
  %20 = load i32, i32* %c, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %20, 3
  br i1 %cmp13, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %21 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %combined_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %21, i32 0, i32 1
  %22 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state, i32 0, i32 %22
  %23 = load i32, i32* %n, align 4, !tbaa !6
  %24 = load i32, i32* %c, align 4, !tbaa !6
  %cmp14 = icmp sgt i32 %24, 0
  %conv = zext i1 %cmp14 to i32
  %add = add nsw i32 %23, %conv
  %25 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %call15 = call i32 @noise_state_init(%struct.aom_noise_state_t* %arrayidx, i32 %add, i32 %25)
  %tobool = icmp ne i32 %call15, 0
  br i1 %tobool, label %if.end18, label %if.then16

if.then16:                                        ; preds = %for.body
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %27 = load i32, i32* %c, align 4, !tbaa !6
  %call17 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %26, i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.7, i32 0, i32 0), i32 %27)
  %28 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  call void @aom_noise_model_free(%struct.aom_noise_model_t* %28)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

if.end18:                                         ; preds = %for.body
  %29 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %latest_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %29, i32 0, i32 2
  %30 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state, i32 0, i32 %30
  %31 = load i32, i32* %n, align 4, !tbaa !6
  %32 = load i32, i32* %c, align 4, !tbaa !6
  %cmp20 = icmp sgt i32 %32, 0
  %conv21 = zext i1 %cmp20 to i32
  %add22 = add nsw i32 %31, %conv21
  %33 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %call23 = call i32 @noise_state_init(%struct.aom_noise_state_t* %arrayidx19, i32 %add22, i32 %33)
  %tobool24 = icmp ne i32 %call23, 0
  br i1 %tobool24, label %if.end27, label %if.then25

if.then25:                                        ; preds = %if.end18
  %34 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %35 = load i32, i32* %c, align 4, !tbaa !6
  %call26 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %34, i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.7, i32 0, i32 0), i32 %35)
  %36 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  call void @aom_noise_model_free(%struct.aom_noise_model_t* %36)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

if.end27:                                         ; preds = %if.end18
  br label %for.inc

for.inc:                                          ; preds = %if.end27
  %37 = load i32, i32* %c, align 4, !tbaa !6
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %38 = load i32, i32* %n, align 4, !tbaa !6
  %39 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %n28 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %39, i32 0, i32 4
  store i32 %38, i32* %n28, align 4, !tbaa !44
  %40 = load i32, i32* %n, align 4, !tbaa !6
  %mul = mul i32 8, %40
  %call29 = call i8* @aom_malloc(i32 %mul)
  %41 = bitcast i8* %call29 to [2 x i32]*
  %42 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %coords = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %42, i32 0, i32 3
  store [2 x i32]* %41, [2 x i32]** %coords, align 8, !tbaa !46
  %43 = load i32, i32* %lag, align 4, !tbaa !6
  %sub = sub nsw i32 0, %43
  store i32 %sub, i32* %y, align 4, !tbaa !6
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc67, %for.end
  %44 = load i32, i32* %y, align 4, !tbaa !6
  %cmp31 = icmp sle i32 %44, 0
  br i1 %cmp31, label %for.body33, label %for.end69

for.body33:                                       ; preds = %for.cond30
  %45 = bitcast i32* %max_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #7
  %46 = load i32, i32* %y, align 4, !tbaa !6
  %cmp34 = icmp eq i32 %46, 0
  br i1 %cmp34, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body33
  br label %cond.end

cond.false:                                       ; preds = %for.body33
  %47 = load i32, i32* %lag, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ -1, %cond.true ], [ %47, %cond.false ]
  store i32 %cond, i32* %max_x, align 4, !tbaa !6
  %48 = load i32, i32* %lag, align 4, !tbaa !6
  %sub36 = sub nsw i32 0, %48
  store i32 %sub36, i32* %x, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc64, %cond.end
  %49 = load i32, i32* %x, align 4, !tbaa !6
  %50 = load i32, i32* %max_x, align 4, !tbaa !6
  %cmp38 = icmp sle i32 %49, %50
  br i1 %cmp38, label %for.body40, label %for.end66

for.body40:                                       ; preds = %for.cond37
  %shape = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 0
  %51 = load i8, i8* %shape, align 4, !tbaa !47
  %conv41 = zext i8 %51 to i32
  switch i32 %conv41, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb55
  ]

sw.bb:                                            ; preds = %for.body40
  %52 = load i32, i32* %x, align 4, !tbaa !6
  %call42 = call i32 @abs(i32 %52) #8
  %53 = load i32, i32* %y, align 4, !tbaa !6
  %54 = load i32, i32* %lag, align 4, !tbaa !6
  %add43 = add nsw i32 %53, %54
  %cmp44 = icmp sle i32 %call42, %add43
  br i1 %cmp44, label %if.then46, label %if.end54

if.then46:                                        ; preds = %sw.bb
  %55 = load i32, i32* %x, align 4, !tbaa !6
  %56 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %coords47 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %56, i32 0, i32 3
  %57 = load [2 x i32]*, [2 x i32]** %coords47, align 8, !tbaa !46
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds [2 x i32], [2 x i32]* %57, i32 %58
  %arrayidx49 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx48, i32 0, i32 0
  store i32 %55, i32* %arrayidx49, align 4, !tbaa !6
  %59 = load i32, i32* %y, align 4, !tbaa !6
  %60 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %coords50 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %60, i32 0, i32 3
  %61 = load [2 x i32]*, [2 x i32]** %coords50, align 8, !tbaa !46
  %62 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx51 = getelementptr inbounds [2 x i32], [2 x i32]* %61, i32 %62
  %arrayidx52 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx51, i32 0, i32 1
  store i32 %59, i32* %arrayidx52, align 4, !tbaa !6
  %63 = load i32, i32* %i, align 4, !tbaa !6
  %inc53 = add nsw i32 %63, 1
  store i32 %inc53, i32* %i, align 4, !tbaa !6
  br label %if.end54

if.end54:                                         ; preds = %if.then46, %sw.bb
  br label %sw.epilog

sw.bb55:                                          ; preds = %for.body40
  %64 = load i32, i32* %x, align 4, !tbaa !6
  %65 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %coords56 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %65, i32 0, i32 3
  %66 = load [2 x i32]*, [2 x i32]** %coords56, align 8, !tbaa !46
  %67 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds [2 x i32], [2 x i32]* %66, i32 %67
  %arrayidx58 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx57, i32 0, i32 0
  store i32 %64, i32* %arrayidx58, align 4, !tbaa !6
  %68 = load i32, i32* %y, align 4, !tbaa !6
  %69 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %coords59 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %69, i32 0, i32 3
  %70 = load [2 x i32]*, [2 x i32]** %coords59, align 8, !tbaa !46
  %71 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [2 x i32], [2 x i32]* %70, i32 %71
  %arrayidx61 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx60, i32 0, i32 1
  store i32 %68, i32* %arrayidx61, align 4, !tbaa !6
  %72 = load i32, i32* %i, align 4, !tbaa !6
  %inc62 = add nsw i32 %72, 1
  store i32 %inc62, i32* %i, align 4, !tbaa !6
  br label %sw.epilog

sw.default:                                       ; preds = %for.body40
  %73 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call63 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %73, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.8, i32 0, i32 0))
  %74 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  call void @aom_noise_model_free(%struct.aom_noise_model_t* %74)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.epilog:                                        ; preds = %sw.bb55, %if.end54
  br label %for.inc64

for.inc64:                                        ; preds = %sw.epilog
  %75 = load i32, i32* %x, align 4, !tbaa !6
  %inc65 = add nsw i32 %75, 1
  store i32 %inc65, i32* %x, align 4, !tbaa !6
  br label %for.cond37

for.end66:                                        ; preds = %for.cond37
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end66, %sw.default
  %76 = bitcast i32* %max_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup70 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc67

for.inc67:                                        ; preds = %cleanup.cont
  %77 = load i32, i32* %y, align 4, !tbaa !6
  %inc68 = add nsw i32 %77, 1
  store i32 %inc68, i32* %y, align 4, !tbaa !6
  br label %for.cond30

for.end69:                                        ; preds = %for.cond30
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

cleanup70:                                        ; preds = %for.end69, %cleanup, %if.then25, %if.then16, %if.then8, %if.then
  %78 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #7
  %79 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #7
  %80 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #7
  %81 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #7
  %82 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #7
  %83 = bitcast i32* %lag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #7
  %84 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #7
  %85 = load i32, i32* %retval, align 4
  ret i32 %85
}

; Function Attrs: nounwind
define internal i32 @num_coeffs(%struct.aom_noise_model_params_t* byval(%struct.aom_noise_model_params_t) align 4 %params) #0 {
entry:
  %retval = alloca i32, align 4
  %n = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %lag = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %1 = load i32, i32* %lag, align 4, !tbaa !41
  %mul = mul nsw i32 2, %1
  %add = add nsw i32 %mul, 1
  store i32 %add, i32* %n, align 4, !tbaa !6
  %shape = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 0
  %2 = load i8, i8* %shape, align 4, !tbaa !47
  %conv = zext i8 %2 to i32
  switch i32 %conv, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %lag1 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %3 = load i32, i32* %lag1, align 4, !tbaa !41
  %lag2 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %4 = load i32, i32* %lag2, align 4, !tbaa !41
  %add3 = add nsw i32 %4, 1
  %mul4 = mul nsw i32 %3, %add3
  store i32 %mul4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb5:                                           ; preds = %entry
  %5 = load i32, i32* %n, align 4, !tbaa !6
  %6 = load i32, i32* %n, align 4, !tbaa !6
  %mul6 = mul nsw i32 %5, %6
  %div = sdiv i32 %mul6, 2
  store i32 %div, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.epilog:                                        ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %sw.epilog, %sw.bb5, %sw.bb
  %7 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: nounwind
define internal i32 @noise_state_init(%struct.aom_noise_state_t* %state, i32 %n, i32 %bit_depth) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.aom_noise_state_t*, align 4
  %n.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %kNumBins = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_noise_state_t* %state, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  %0 = bitcast i32* %kNumBins to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 20, i32* %kNumBins, align 4, !tbaa !6
  %1 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %1, i32 0, i32 0
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %call = call i32 @equation_system_init(%struct.aom_equation_system_t* %eqns, i32 %2)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %4 = load i32, i32* %n.addr, align 4, !tbaa !6
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.26, i32 0, i32 0), i32 %4)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %ar_gain = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %5, i32 0, i32 3
  store double 1.000000e+00, double* %ar_gain, align 8, !tbaa !48
  %6 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %num_observations = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %6, i32 0, i32 2
  store i32 0, i32* %num_observations, align 8, !tbaa !50
  %7 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %strength_solver = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %7, i32 0, i32 1
  %8 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %call2 = call i32 @aom_noise_strength_solver_init(%struct.aom_noise_strength_solver_t* %strength_solver, i32 20, i32 %8)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %9 = bitcast i32* %kNumBins to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define hidden void @aom_noise_model_free(%struct.aom_noise_model_t* %model) #0 {
entry:
  %model.addr = alloca %struct.aom_noise_model_t*, align 4
  %c = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_noise_model_t* %model, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %0 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %c, align 4, !tbaa !6
  %1 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_noise_model_t* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %2 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %coords = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %2, i32 0, i32 3
  %3 = load [2 x i32]*, [2 x i32]** %coords, align 8, !tbaa !46
  %4 = bitcast [2 x i32]* %3 to i8*
  call void @aom_free(i8* %4)
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %5 = load i32, i32* %c, align 4, !tbaa !6
  %cmp = icmp slt i32 %5, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %latest_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %6, i32 0, i32 2
  %7 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state, i32 0, i32 %7
  %eqns = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx, i32 0, i32 0
  call void @equation_system_free(%struct.aom_equation_system_t* %eqns)
  %8 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %combined_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %8, i32 0, i32 1
  %9 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state, i32 0, i32 %9
  %eqns2 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx1, i32 0, i32 0
  call void @equation_system_free(%struct.aom_equation_system_t* %eqns2)
  %10 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %latest_state3 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %10, i32 0, i32 2
  %11 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state3, i32 0, i32 %11
  %strength_solver = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx4, i32 0, i32 1
  %eqns5 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver, i32 0, i32 0
  call void @equation_system_free(%struct.aom_equation_system_t* %eqns5)
  %12 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %combined_state6 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %12, i32 0, i32 1
  %13 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state6, i32 0, i32 %13
  %strength_solver8 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx7, i32 0, i32 1
  %eqns9 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver8, i32 0, i32 0
  call void @equation_system_free(%struct.aom_equation_system_t* %eqns9)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %c, align 4, !tbaa !6
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %model.addr, align 4, !tbaa !2
  %16 = bitcast %struct.aom_noise_model_t* %15 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %16, i8 0, i32 512, i1 false)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then
  %17 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #6

; Function Attrs: nounwind
define hidden zeroext i8 @aom_noise_model_update(%struct.aom_noise_model_t* %noise_model, i8** %data, i8** %denoised, i32 %w, i32 %h, i32* %stride, i32* %chroma_sub_log2, i8* %flat_blocks, i32 %block_size) #0 {
entry:
  %retval = alloca i8, align 1
  %noise_model.addr = alloca %struct.aom_noise_model_t*, align 4
  %data.addr = alloca i8**, align 4
  %denoised.addr = alloca i8**, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32*, align 4
  %chroma_sub_log2.addr = alloca i32*, align 4
  %flat_blocks.addr = alloca i8*, align 4
  %block_size.addr = alloca i32, align 4
  %num_blocks_w = alloca i32, align 4
  %num_blocks_h = alloca i32, align 4
  %y_model_different = alloca i32, align 4
  %num_blocks = alloca i32, align 4
  %i = alloca i32, align 4
  %channel = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %no_subsampling = alloca [2 x i32], align 4
  %alt_data = alloca i8*, align 4
  %alt_denoised = alloca i8*, align 4
  %sub44 = alloca i32*, align 4
  %is_chroma = alloca i32, align 4
  store %struct.aom_noise_model_t* %noise_model, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  store i8** %data, i8*** %data.addr, align 4, !tbaa !2
  store i8** %denoised, i8*** %denoised.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32* %stride, i32** %stride.addr, align 4, !tbaa !2
  store i32* %chroma_sub_log2, i32** %chroma_sub_log2.addr, align 4, !tbaa !2
  store i8* %flat_blocks, i8** %flat_blocks.addr, align 4, !tbaa !2
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_blocks_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %w.addr, align 4, !tbaa !6
  %2 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %add = add nsw i32 %1, %2
  %sub = sub nsw i32 %add, 1
  %3 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %div = sdiv i32 %sub, %3
  store i32 %div, i32* %num_blocks_w, align 4, !tbaa !6
  %4 = bitcast i32* %num_blocks_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i32, i32* %h.addr, align 4, !tbaa !6
  %6 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %add1 = add nsw i32 %5, %6
  %sub2 = sub nsw i32 %add1, 1
  %7 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %div3 = sdiv i32 %sub2, %7
  store i32 %div3, i32* %num_blocks_h, align 4, !tbaa !6
  %8 = bitcast i32* %y_model_different to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store i32 0, i32* %y_model_different, align 4, !tbaa !6
  %9 = bitcast i32* %num_blocks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %num_blocks, align 4, !tbaa !6
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  %11 = bitcast i32* %channel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  store i32 0, i32* %channel, align 4, !tbaa !6
  %12 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp = icmp sle i32 %12, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %14 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.9, i32 0, i32 0), i32 %14)
  store i8 1, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup159

if.end:                                           ; preds = %entry
  %15 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %16 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %16, i32 0, i32 0
  %lag = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %17 = load i32, i32* %lag, align 4, !tbaa !51
  %mul = mul nsw i32 %17, 2
  %add4 = add nsw i32 %mul, 1
  %cmp5 = icmp slt i32 %15, %add4
  br i1 %cmp5, label %if.then6, label %if.end12

if.then6:                                         ; preds = %if.end
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %19 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %20 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params7 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %20, i32 0, i32 0
  %lag8 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params7, i32 0, i32 1
  %21 = load i32, i32* %lag8, align 4, !tbaa !51
  %mul9 = mul nsw i32 %21, 2
  %add10 = add nsw i32 %mul9, 1
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.10, i32 0, i32 0), i32 %19, i32 %add10)
  store i8 1, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup159

if.end12:                                         ; preds = %if.end
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end12
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %22, 3
  br i1 %cmp13, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %23, i32 0, i32 2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state, i32 0, i32 %24
  %eqns = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx, i32 0, i32 0
  call void @equation_system_clear(%struct.aom_equation_system_t* %eqns)
  %25 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state14 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %25, i32 0, i32 2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state14, i32 0, i32 %26
  %num_observations = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx15, i32 0, i32 2
  store i32 0, i32* %num_observations, align 8, !tbaa !50
  %27 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state16 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %27, i32 0, i32 2
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state16, i32 0, i32 %28
  %strength_solver = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx17, i32 0, i32 1
  call void @noise_strength_solver_clear(%struct.aom_noise_strength_solver_t* %strength_solver)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc26, %for.end
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %31 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %32 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %mul19 = mul nsw i32 %31, %32
  %cmp20 = icmp slt i32 %30, %mul19
  br i1 %cmp20, label %for.body21, label %for.end28

for.body21:                                       ; preds = %for.cond18
  %33 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i8, i8* %33, i32 %34
  %35 = load i8, i8* %arrayidx22, align 1, !tbaa !35
  %tobool = icmp ne i8 %35, 0
  br i1 %tobool, label %if.then23, label %if.end25

if.then23:                                        ; preds = %for.body21
  %36 = load i32, i32* %num_blocks, align 4, !tbaa !6
  %inc24 = add nsw i32 %36, 1
  store i32 %inc24, i32* %num_blocks, align 4, !tbaa !6
  br label %if.end25

if.end25:                                         ; preds = %if.then23, %for.body21
  br label %for.inc26

for.inc26:                                        ; preds = %if.end25
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %inc27 = add nsw i32 %37, 1
  store i32 %inc27, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.end28:                                        ; preds = %for.cond18
  %38 = load i32, i32* %num_blocks, align 4, !tbaa !6
  %cmp29 = icmp sle i32 %38, 1
  br i1 %cmp29, label %if.then30, label %if.end32

if.then30:                                        ; preds = %for.end28
  %39 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call31 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %39, i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.11, i32 0, i32 0))
  store i8 2, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup159

if.end32:                                         ; preds = %for.end28
  store i32 0, i32* %channel, align 4, !tbaa !6
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc153, %if.end32
  %40 = load i32, i32* %channel, align 4, !tbaa !6
  %cmp34 = icmp slt i32 %40, 3
  br i1 %cmp34, label %for.body35, label %for.end155

for.body35:                                       ; preds = %for.cond33
  %41 = bitcast [2 x i32]* %no_subsampling to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %41) #7
  %42 = bitcast [2 x i32]* %no_subsampling to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %42, i8 0, i32 8, i1 false)
  %43 = bitcast i8** %alt_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  %44 = load i32, i32* %channel, align 4, !tbaa !6
  %cmp36 = icmp sgt i32 %44, 0
  br i1 %cmp36, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body35
  %45 = load i8**, i8*** %data.addr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i8*, i8** %45, i32 0
  %46 = load i8*, i8** %arrayidx37, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %for.body35
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %46, %cond.true ], [ null, %cond.false ]
  store i8* %cond, i8** %alt_data, align 4, !tbaa !2
  %47 = bitcast i8** %alt_denoised to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #7
  %48 = load i32, i32* %channel, align 4, !tbaa !6
  %cmp38 = icmp sgt i32 %48, 0
  br i1 %cmp38, label %cond.true39, label %cond.false41

cond.true39:                                      ; preds = %cond.end
  %49 = load i8**, i8*** %denoised.addr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i8*, i8** %49, i32 0
  %50 = load i8*, i8** %arrayidx40, align 4, !tbaa !2
  br label %cond.end42

cond.false41:                                     ; preds = %cond.end
  br label %cond.end42

cond.end42:                                       ; preds = %cond.false41, %cond.true39
  %cond43 = phi i8* [ %50, %cond.true39 ], [ null, %cond.false41 ]
  store i8* %cond43, i8** %alt_denoised, align 4, !tbaa !2
  %51 = bitcast i32** %sub44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #7
  %52 = load i32, i32* %channel, align 4, !tbaa !6
  %cmp45 = icmp sgt i32 %52, 0
  br i1 %cmp45, label %cond.true46, label %cond.false47

cond.true46:                                      ; preds = %cond.end42
  %53 = load i32*, i32** %chroma_sub_log2.addr, align 4, !tbaa !2
  br label %cond.end48

cond.false47:                                     ; preds = %cond.end42
  %arraydecay = getelementptr inbounds [2 x i32], [2 x i32]* %no_subsampling, i32 0, i32 0
  br label %cond.end48

cond.end48:                                       ; preds = %cond.false47, %cond.true46
  %cond49 = phi i32* [ %53, %cond.true46 ], [ %arraydecay, %cond.false47 ]
  store i32* %cond49, i32** %sub44, align 4, !tbaa !2
  %54 = bitcast i32* %is_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  %55 = load i32, i32* %channel, align 4, !tbaa !6
  %cmp50 = icmp ne i32 %55, 0
  %conv = zext i1 %cmp50 to i32
  store i32 %conv, i32* %is_chroma, align 4, !tbaa !6
  %56 = load i8**, i8*** %data.addr, align 4, !tbaa !2
  %57 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx51 = getelementptr inbounds i8*, i8** %56, i32 %57
  %58 = load i8*, i8** %arrayidx51, align 4, !tbaa !2
  %tobool52 = icmp ne i8* %58, null
  br i1 %tobool52, label %lor.lhs.false, label %if.then55

lor.lhs.false:                                    ; preds = %cond.end48
  %59 = load i8**, i8*** %denoised.addr, align 4, !tbaa !2
  %60 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx53 = getelementptr inbounds i8*, i8** %59, i32 %60
  %61 = load i8*, i8** %arrayidx53, align 4, !tbaa !2
  %tobool54 = icmp ne i8* %61, null
  br i1 %tobool54, label %if.end56, label %if.then55

if.then55:                                        ; preds = %lor.lhs.false, %cond.end48
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end56:                                         ; preds = %lor.lhs.false
  %62 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %63 = load i32, i32* %channel, align 4, !tbaa !6
  %64 = load i8**, i8*** %data.addr, align 4, !tbaa !2
  %65 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds i8*, i8** %64, i32 %65
  %66 = load i8*, i8** %arrayidx57, align 4, !tbaa !2
  %67 = load i8**, i8*** %denoised.addr, align 4, !tbaa !2
  %68 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds i8*, i8** %67, i32 %68
  %69 = load i8*, i8** %arrayidx58, align 4, !tbaa !2
  %70 = load i32, i32* %w.addr, align 4, !tbaa !6
  %71 = load i32, i32* %h.addr, align 4, !tbaa !6
  %72 = load i32*, i32** %stride.addr, align 4, !tbaa !2
  %73 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds i32, i32* %72, i32 %73
  %74 = load i32, i32* %arrayidx59, align 4, !tbaa !6
  %75 = load i32*, i32** %sub44, align 4, !tbaa !2
  %76 = load i8*, i8** %alt_data, align 4, !tbaa !2
  %77 = load i8*, i8** %alt_denoised, align 4, !tbaa !2
  %78 = load i32*, i32** %stride.addr, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %78, i32 0
  %79 = load i32, i32* %arrayidx60, align 4, !tbaa !6
  %80 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %81 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %82 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %83 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %call61 = call i32 @add_block_observations(%struct.aom_noise_model_t* %62, i32 %63, i8* %66, i8* %69, i32 %70, i32 %71, i32 %74, i32* %75, i8* %76, i8* %77, i32 %79, i8* %80, i32 %81, i32 %82, i32 %83)
  %tobool62 = icmp ne i32 %call61, 0
  br i1 %tobool62, label %if.end65, label %if.then63

if.then63:                                        ; preds = %if.end56
  %84 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call64 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %84, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.12, i32 0, i32 0))
  store i8 4, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end65:                                         ; preds = %if.end56
  %85 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state66 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %85, i32 0, i32 2
  %86 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state66, i32 0, i32 %86
  %87 = load i32, i32* %is_chroma, align 4, !tbaa !6
  %call68 = call i32 @ar_equation_system_solve(%struct.aom_noise_state_t* %arrayidx67, i32 %87)
  %tobool69 = icmp ne i32 %call68, 0
  br i1 %tobool69, label %if.end78, label %if.then70

if.then70:                                        ; preds = %if.end65
  %88 = load i32, i32* %is_chroma, align 4, !tbaa !6
  %tobool71 = icmp ne i32 %88, 0
  br i1 %tobool71, label %if.then72, label %if.else

if.then72:                                        ; preds = %if.then70
  %89 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state73 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %89, i32 0, i32 2
  %90 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx74 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state73, i32 0, i32 %90
  %eqns75 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx74, i32 0, i32 0
  call void @set_chroma_coefficient_fallback_soln(%struct.aom_equation_system_t* %eqns75)
  br label %if.end77

if.else:                                          ; preds = %if.then70
  %91 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %92 = load i32, i32* %channel, align 4, !tbaa !6
  %call76 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %91, i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.13, i32 0, i32 0), i32 %92)
  store i8 4, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end77:                                         ; preds = %if.then72
  br label %if.end78

if.end78:                                         ; preds = %if.end77, %if.end65
  %93 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %94 = load i32, i32* %channel, align 4, !tbaa !6
  %95 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state79 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %95, i32 0, i32 2
  %96 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx80 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state79, i32 0, i32 %96
  %eqns81 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx80, i32 0, i32 0
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns81, i32 0, i32 2
  %97 = load double*, double** %x, align 8, !tbaa !52
  %98 = load i8**, i8*** %data.addr, align 4, !tbaa !2
  %99 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx82 = getelementptr inbounds i8*, i8** %98, i32 %99
  %100 = load i8*, i8** %arrayidx82, align 4, !tbaa !2
  %101 = load i8**, i8*** %denoised.addr, align 4, !tbaa !2
  %102 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx83 = getelementptr inbounds i8*, i8** %101, i32 %102
  %103 = load i8*, i8** %arrayidx83, align 4, !tbaa !2
  %104 = load i32, i32* %w.addr, align 4, !tbaa !6
  %105 = load i32, i32* %h.addr, align 4, !tbaa !6
  %106 = load i32*, i32** %stride.addr, align 4, !tbaa !2
  %107 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx84 = getelementptr inbounds i32, i32* %106, i32 %107
  %108 = load i32, i32* %arrayidx84, align 4, !tbaa !6
  %109 = load i32*, i32** %sub44, align 4, !tbaa !2
  %110 = load i8*, i8** %alt_data, align 4, !tbaa !2
  %111 = load i32*, i32** %stride.addr, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %111, i32 0
  %112 = load i32, i32* %arrayidx85, align 4, !tbaa !6
  %113 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %114 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %115 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %116 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  call void @add_noise_std_observations(%struct.aom_noise_model_t* %93, i32 %94, double* %97, i8* %100, i8* %103, i32 %104, i32 %105, i32 %108, i32* %109, i8* %110, i32 %112, i8* %113, i32 %114, i32 %115, i32 %116)
  %117 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state86 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %117, i32 0, i32 2
  %118 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state86, i32 0, i32 %118
  %strength_solver88 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx87, i32 0, i32 1
  %call89 = call i32 @aom_noise_strength_solver_solve(%struct.aom_noise_strength_solver_t* %strength_solver88)
  %tobool90 = icmp ne i32 %call89, 0
  br i1 %tobool90, label %if.end93, label %if.then91

if.then91:                                        ; preds = %if.end78
  %119 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call92 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %119, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.14, i32 0, i32 0))
  store i8 4, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end93:                                         ; preds = %if.end78
  %120 = load i32, i32* %channel, align 4, !tbaa !6
  %cmp94 = icmp eq i32 %120, 0
  br i1 %cmp94, label %land.lhs.true, label %if.end104

land.lhs.true:                                    ; preds = %if.end93
  %121 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %121, i32 0, i32 1
  %122 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx96 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state, i32 0, i32 %122
  %strength_solver97 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx96, i32 0, i32 1
  %num_equations = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver97, i32 0, i32 4
  %123 = load i32, i32* %num_equations, align 4, !tbaa !53
  %cmp98 = icmp sgt i32 %123, 0
  br i1 %cmp98, label %land.lhs.true100, label %if.end104

land.lhs.true100:                                 ; preds = %land.lhs.true
  %124 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %call101 = call i32 @is_noise_model_different(%struct.aom_noise_model_t* %124)
  %tobool102 = icmp ne i32 %call101, 0
  br i1 %tobool102, label %if.then103, label %if.end104

if.then103:                                       ; preds = %land.lhs.true100
  store i32 1, i32* %y_model_different, align 4, !tbaa !6
  br label %if.end104

if.end104:                                        ; preds = %if.then103, %land.lhs.true100, %land.lhs.true, %if.end93
  %125 = load i32, i32* %y_model_different, align 4, !tbaa !6
  %tobool105 = icmp ne i32 %125, 0
  br i1 %tobool105, label %if.then106, label %if.end107

if.then106:                                       ; preds = %if.end104
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end107:                                        ; preds = %if.end104
  %126 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state108 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %126, i32 0, i32 2
  %127 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx109 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state108, i32 0, i32 %127
  %num_observations110 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx109, i32 0, i32 2
  %128 = load i32, i32* %num_observations110, align 8, !tbaa !50
  %129 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state111 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %129, i32 0, i32 1
  %130 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx112 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state111, i32 0, i32 %130
  %num_observations113 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx112, i32 0, i32 2
  %131 = load i32, i32* %num_observations113, align 8, !tbaa !50
  %add114 = add nsw i32 %131, %128
  store i32 %add114, i32* %num_observations113, align 8, !tbaa !50
  %132 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state115 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %132, i32 0, i32 1
  %133 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx116 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state115, i32 0, i32 %133
  %eqns117 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx116, i32 0, i32 0
  %134 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state118 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %134, i32 0, i32 2
  %135 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx119 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state118, i32 0, i32 %135
  %eqns120 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx119, i32 0, i32 0
  call void @equation_system_add(%struct.aom_equation_system_t* %eqns117, %struct.aom_equation_system_t* %eqns120)
  %136 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state121 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %136, i32 0, i32 1
  %137 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx122 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state121, i32 0, i32 %137
  %138 = load i32, i32* %is_chroma, align 4, !tbaa !6
  %call123 = call i32 @ar_equation_system_solve(%struct.aom_noise_state_t* %arrayidx122, i32 %138)
  %tobool124 = icmp ne i32 %call123, 0
  br i1 %tobool124, label %if.end134, label %if.then125

if.then125:                                       ; preds = %if.end107
  %139 = load i32, i32* %is_chroma, align 4, !tbaa !6
  %tobool126 = icmp ne i32 %139, 0
  br i1 %tobool126, label %if.then127, label %if.else131

if.then127:                                       ; preds = %if.then125
  %140 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state128 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %140, i32 0, i32 1
  %141 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx129 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state128, i32 0, i32 %141
  %eqns130 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx129, i32 0, i32 0
  call void @set_chroma_coefficient_fallback_soln(%struct.aom_equation_system_t* %eqns130)
  br label %if.end133

if.else131:                                       ; preds = %if.then125
  %142 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %143 = load i32, i32* %channel, align 4, !tbaa !6
  %call132 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %142, i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.15, i32 0, i32 0), i32 %143)
  store i8 4, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end133:                                        ; preds = %if.then127
  br label %if.end134

if.end134:                                        ; preds = %if.end133, %if.end107
  %144 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state135 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %144, i32 0, i32 1
  %145 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx136 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state135, i32 0, i32 %145
  %strength_solver137 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx136, i32 0, i32 1
  %146 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state138 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %146, i32 0, i32 2
  %147 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx139 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state138, i32 0, i32 %147
  %strength_solver140 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx139, i32 0, i32 1
  call void @noise_strength_solver_add(%struct.aom_noise_strength_solver_t* %strength_solver137, %struct.aom_noise_strength_solver_t* %strength_solver140)
  %148 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state141 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %148, i32 0, i32 1
  %149 = load i32, i32* %channel, align 4, !tbaa !6
  %arrayidx142 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state141, i32 0, i32 %149
  %strength_solver143 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx142, i32 0, i32 1
  %call144 = call i32 @aom_noise_strength_solver_solve(%struct.aom_noise_strength_solver_t* %strength_solver143)
  %tobool145 = icmp ne i32 %call144, 0
  br i1 %tobool145, label %if.end148, label %if.then146

if.then146:                                       ; preds = %if.end134
  %150 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call147 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %150, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.16, i32 0, i32 0))
  store i8 4, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end148:                                        ; preds = %if.end134
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end148, %if.then146, %if.else131, %if.then106, %if.then91, %if.else, %if.then63, %if.then55
  %151 = bitcast i32* %is_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #7
  %152 = bitcast i32** %sub44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #7
  %153 = bitcast i8** %alt_denoised to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #7
  %154 = bitcast i8** %alt_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #7
  %155 = bitcast [2 x i32]* %no_subsampling to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %155) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup159 [
    i32 0, label %cleanup.cont
    i32 8, label %for.end155
    i32 10, label %for.inc153
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc153

for.inc153:                                       ; preds = %cleanup.cont, %cleanup
  %156 = load i32, i32* %channel, align 4, !tbaa !6
  %inc154 = add nsw i32 %156, 1
  store i32 %inc154, i32* %channel, align 4, !tbaa !6
  br label %for.cond33

for.end155:                                       ; preds = %cleanup, %for.cond33
  %157 = load i32, i32* %y_model_different, align 4, !tbaa !6
  %tobool156 = icmp ne i32 %157, 0
  %158 = zext i1 %tobool156 to i64
  %cond157 = select i1 %tobool156, i32 3, i32 0
  %conv158 = trunc i32 %cond157 to i8
  store i8 %conv158, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup159

cleanup159:                                       ; preds = %for.end155, %cleanup, %if.then30, %if.then6, %if.then
  %159 = bitcast i32* %channel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #7
  %160 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #7
  %161 = bitcast i32* %num_blocks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #7
  %162 = bitcast i32* %y_model_different to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #7
  %163 = bitcast i32* %num_blocks_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #7
  %164 = bitcast i32* %num_blocks_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #7
  %165 = load i8, i8* %retval, align 1
  ret i8 %165
}

; Function Attrs: nounwind
define internal void @equation_system_clear(%struct.aom_equation_system_t* %eqns) #0 {
entry:
  %eqns.addr = alloca %struct.aom_equation_system_t*, align 4
  %n = alloca i32, align 4
  store %struct.aom_equation_system_t* %eqns, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %n1 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %1, i32 0, i32 3
  %2 = load i32, i32* %n1, align 4, !tbaa !22
  store i32 %2, i32* %n, align 4, !tbaa !6
  %3 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %3, i32 0, i32 0
  %4 = load double*, double** %A, align 4, !tbaa !23
  %5 = bitcast double* %4 to i8*
  %6 = load i32, i32* %n, align 4, !tbaa !6
  %mul = mul i32 8, %6
  %7 = load i32, i32* %n, align 4, !tbaa !6
  %mul2 = mul i32 %mul, %7
  call void @llvm.memset.p0i8.i32(i8* align 8 %5, i8 0, i32 %mul2, i1 false)
  %8 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %8, i32 0, i32 2
  %9 = load double*, double** %x, align 4, !tbaa !25
  %10 = bitcast double* %9 to i8*
  %11 = load i32, i32* %n, align 4, !tbaa !6
  %mul3 = mul i32 8, %11
  call void @llvm.memset.p0i8.i32(i8* align 8 %10, i8 0, i32 %mul3, i1 false)
  %12 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %12, i32 0, i32 1
  %13 = load double*, double** %b, align 4, !tbaa !24
  %14 = bitcast double* %13 to i8*
  %15 = load i32, i32* %n, align 4, !tbaa !6
  %mul4 = mul i32 8, %15
  call void @llvm.memset.p0i8.i32(i8* align 8 %14, i8 0, i32 %mul4, i1 false)
  %16 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  ret void
}

; Function Attrs: nounwind
define internal void @noise_strength_solver_clear(%struct.aom_noise_strength_solver_t* %solver) #0 {
entry:
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %0 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %0, i32 0, i32 0
  call void @equation_system_clear(%struct.aom_equation_system_t* %eqns)
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_equations = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %1, i32 0, i32 4
  store i32 0, i32* %num_equations, align 4, !tbaa !19
  %2 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %total = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %2, i32 0, i32 5
  store double 0.000000e+00, double* %total, align 8, !tbaa !18
  ret void
}

; Function Attrs: nounwind
define internal i32 @add_block_observations(%struct.aom_noise_model_t* %noise_model, i32 %c, i8* %data, i8* %denoised, i32 %w, i32 %h, i32 %stride, i32* %sub_log2, i8* %alt_data, i8* %alt_denoised, i32 %alt_stride, i8* %flat_blocks, i32 %block_size, i32 %num_blocks_w, i32 %num_blocks_h) #0 {
entry:
  %retval = alloca i32, align 4
  %noise_model.addr = alloca %struct.aom_noise_model_t*, align 4
  %c.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  %denoised.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %sub_log2.addr = alloca i32*, align 4
  %alt_data.addr = alloca i8*, align 4
  %alt_denoised.addr = alloca i8*, align 4
  %alt_stride.addr = alloca i32, align 4
  %flat_blocks.addr = alloca i8*, align 4
  %block_size.addr = alloca i32, align 4
  %num_blocks_w.addr = alloca i32, align 4
  %num_blocks_h.addr = alloca i32, align 4
  %lag = alloca i32, align 4
  %num_coords = alloca i32, align 4
  %normalization = alloca double, align 8
  %A = alloca double*, align 4
  %b = alloca double*, align 4
  %buffer = alloca double*, align 4
  %n8 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %by = alloca i32, align 4
  %y_o = alloca i32, align 4
  %bx = alloca i32, align 4
  %x_o = alloca i32, align 4
  %y_start = alloca i32, align 4
  %x_start = alloca i32, align 4
  %y_end = alloca i32, align 4
  %x_end = alloca i32, align 4
  %y = alloca i32, align 4
  %x = alloca i32, align 4
  %val = alloca double, align 8
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.aom_noise_model_t* %noise_model, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  store i32 %c, i32* %c.addr, align 4, !tbaa !6
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i8* %denoised, i8** %denoised.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32* %sub_log2, i32** %sub_log2.addr, align 4, !tbaa !2
  store i8* %alt_data, i8** %alt_data.addr, align 4, !tbaa !2
  store i8* %alt_denoised, i8** %alt_denoised.addr, align 4, !tbaa !2
  store i32 %alt_stride, i32* %alt_stride.addr, align 4, !tbaa !6
  store i8* %flat_blocks, i8** %flat_blocks.addr, align 4, !tbaa !2
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  store i32 %num_blocks_w, i32* %num_blocks_w.addr, align 4, !tbaa !6
  store i32 %num_blocks_h, i32* %num_blocks_h.addr, align 4, !tbaa !6
  %0 = bitcast i32* %lag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %1, i32 0, i32 0
  %lag1 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %2 = load i32, i32* %lag1, align 4, !tbaa !51
  store i32 %2, i32* %lag, align 4, !tbaa !6
  %3 = bitcast i32* %num_coords to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %n = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %4, i32 0, i32 4
  %5 = load i32, i32* %n, align 4, !tbaa !44
  store i32 %5, i32* %num_coords, align 4, !tbaa !6
  %6 = bitcast double* %normalization to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #7
  %7 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params2 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %7, i32 0, i32 0
  %bit_depth = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params2, i32 0, i32 2
  %8 = load i32, i32* %bit_depth, align 8, !tbaa !54
  %shl = shl i32 1, %8
  %sub = sub nsw i32 %shl, 1
  %conv = sitofp i32 %sub to double
  store double %conv, double* %normalization, align 8, !tbaa !11
  %9 = bitcast double** %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %10, i32 0, i32 2
  %11 = load i32, i32* %c.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state, i32 0, i32 %11
  %eqns = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx, i32 0, i32 0
  %A3 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 0
  %12 = load double*, double** %A3, align 8, !tbaa !55
  store double* %12, double** %A, align 4, !tbaa !2
  %13 = bitcast double** %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state4 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %14, i32 0, i32 2
  %15 = load i32, i32* %c.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state4, i32 0, i32 %15
  %eqns6 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx5, i32 0, i32 0
  %b7 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns6, i32 0, i32 1
  %16 = load double*, double** %b7, align 4, !tbaa !56
  store double* %16, double** %b, align 4, !tbaa !2
  %17 = bitcast double** %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = load i32, i32* %num_coords, align 4, !tbaa !6
  %add = add nsw i32 %18, 1
  %mul = mul i32 8, %add
  %call = call i8* @aom_malloc(i32 %mul)
  %19 = bitcast i8* %call to double*
  store double* %19, double** %buffer, align 4, !tbaa !2
  %20 = bitcast i32* %n8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %21 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state9 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %21, i32 0, i32 2
  %22 = load i32, i32* %c.addr, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state9, i32 0, i32 %22
  %eqns11 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx10, i32 0, i32 0
  %n12 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns11, i32 0, i32 3
  %23 = load i32, i32* %n12, align 4, !tbaa !57
  store i32 %23, i32* %n8, align 4, !tbaa !6
  %24 = load double*, double** %buffer, align 4, !tbaa !2
  %tobool = icmp ne double* %24, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %25 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %26 = load i32, i32* %num_coords, align 4, !tbaa !6
  %add13 = add nsw i32 %26, 1
  %call14 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %25, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.27, i32 0, i32 0), i32 %add13)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup200

if.end:                                           ; preds = %entry
  %27 = bitcast i32* %by to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  store i32 0, i32* %by, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc196, %if.end
  %28 = load i32, i32* %by, align 4, !tbaa !6
  %29 = load i32, i32* %num_blocks_h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %28, %29
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i32* %by to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  br label %for.end199

for.body:                                         ; preds = %for.cond
  %31 = bitcast i32* %y_o to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %32 = load i32, i32* %by, align 4, !tbaa !6
  %33 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %34 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %34, i32 1
  %35 = load i32, i32* %arrayidx16, align 4, !tbaa !6
  %shr = ashr i32 %33, %35
  %mul17 = mul nsw i32 %32, %shr
  store i32 %mul17, i32* %y_o, align 4, !tbaa !6
  %36 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  store i32 0, i32* %bx, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc192, %for.body
  %37 = load i32, i32* %bx, align 4, !tbaa !6
  %38 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %37, %38
  br i1 %cmp19, label %for.body22, label %for.cond.cleanup21

for.cond.cleanup21:                               ; preds = %for.cond18
  store i32 5, i32* %cleanup.dest.slot, align 4
  %39 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  br label %for.end195

for.body22:                                       ; preds = %for.cond18
  %40 = bitcast i32* %x_o to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #7
  %41 = load i32, i32* %bx, align 4, !tbaa !6
  %42 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %43 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %43, i32 0
  %44 = load i32, i32* %arrayidx23, align 4, !tbaa !6
  %shr24 = ashr i32 %42, %44
  %mul25 = mul nsw i32 %41, %shr24
  store i32 %mul25, i32* %x_o, align 4, !tbaa !6
  %45 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %46 = load i32, i32* %by, align 4, !tbaa !6
  %47 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %46, %47
  %48 = load i32, i32* %bx, align 4, !tbaa !6
  %add27 = add nsw i32 %mul26, %48
  %arrayidx28 = getelementptr inbounds i8, i8* %45, i32 %add27
  %49 = load i8, i8* %arrayidx28, align 1, !tbaa !35
  %tobool29 = icmp ne i8 %49, 0
  br i1 %tobool29, label %if.end31, label %if.then30

if.then30:                                        ; preds = %for.body22
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %for.body22
  %50 = bitcast i32* %y_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load i32, i32* %by, align 4, !tbaa !6
  %cmp32 = icmp sgt i32 %51, 0
  br i1 %cmp32, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %if.end31
  %52 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %53 = load i32, i32* %by, align 4, !tbaa !6
  %sub34 = sub nsw i32 %53, 1
  %54 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %mul35 = mul nsw i32 %sub34, %54
  %55 = load i32, i32* %bx, align 4, !tbaa !6
  %add36 = add nsw i32 %mul35, %55
  %arrayidx37 = getelementptr inbounds i8, i8* %52, i32 %add36
  %56 = load i8, i8* %arrayidx37, align 1, !tbaa !35
  %conv38 = zext i8 %56 to i32
  %tobool39 = icmp ne i32 %conv38, 0
  br i1 %tobool39, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %if.end31
  %57 = load i32, i32* %lag, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %57, %cond.false ]
  store i32 %cond, i32* %y_start, align 4, !tbaa !6
  %58 = bitcast i32* %x_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #7
  %59 = load i32, i32* %bx, align 4, !tbaa !6
  %cmp40 = icmp sgt i32 %59, 0
  br i1 %cmp40, label %land.lhs.true42, label %cond.false50

land.lhs.true42:                                  ; preds = %cond.end
  %60 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %61 = load i32, i32* %by, align 4, !tbaa !6
  %62 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %mul43 = mul nsw i32 %61, %62
  %63 = load i32, i32* %bx, align 4, !tbaa !6
  %add44 = add nsw i32 %mul43, %63
  %sub45 = sub nsw i32 %add44, 1
  %arrayidx46 = getelementptr inbounds i8, i8* %60, i32 %sub45
  %64 = load i8, i8* %arrayidx46, align 1, !tbaa !35
  %conv47 = zext i8 %64 to i32
  %tobool48 = icmp ne i32 %conv47, 0
  br i1 %tobool48, label %cond.true49, label %cond.false50

cond.true49:                                      ; preds = %land.lhs.true42
  br label %cond.end51

cond.false50:                                     ; preds = %land.lhs.true42, %cond.end
  %65 = load i32, i32* %lag, align 4, !tbaa !6
  br label %cond.end51

cond.end51:                                       ; preds = %cond.false50, %cond.true49
  %cond52 = phi i32 [ 0, %cond.true49 ], [ %65, %cond.false50 ]
  store i32 %cond52, i32* %x_start, align 4, !tbaa !6
  %66 = bitcast i32* %y_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #7
  %67 = load i32, i32* %h.addr, align 4, !tbaa !6
  %68 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i32, i32* %68, i32 1
  %69 = load i32, i32* %arrayidx53, align 4, !tbaa !6
  %shr54 = ashr i32 %67, %69
  %70 = load i32, i32* %by, align 4, !tbaa !6
  %71 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %72 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %72, i32 1
  %73 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %shr56 = ashr i32 %71, %73
  %mul57 = mul nsw i32 %70, %shr56
  %sub58 = sub nsw i32 %shr54, %mul57
  %74 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %75 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i32, i32* %75, i32 1
  %76 = load i32, i32* %arrayidx59, align 4, !tbaa !6
  %shr60 = ashr i32 %74, %76
  %cmp61 = icmp slt i32 %sub58, %shr60
  br i1 %cmp61, label %cond.true63, label %cond.false70

cond.true63:                                      ; preds = %cond.end51
  %77 = load i32, i32* %h.addr, align 4, !tbaa !6
  %78 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %78, i32 1
  %79 = load i32, i32* %arrayidx64, align 4, !tbaa !6
  %shr65 = ashr i32 %77, %79
  %80 = load i32, i32* %by, align 4, !tbaa !6
  %81 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %82 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i32, i32* %82, i32 1
  %83 = load i32, i32* %arrayidx66, align 4, !tbaa !6
  %shr67 = ashr i32 %81, %83
  %mul68 = mul nsw i32 %80, %shr67
  %sub69 = sub nsw i32 %shr65, %mul68
  br label %cond.end73

cond.false70:                                     ; preds = %cond.end51
  %84 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %85 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %85, i32 1
  %86 = load i32, i32* %arrayidx71, align 4, !tbaa !6
  %shr72 = ashr i32 %84, %86
  br label %cond.end73

cond.end73:                                       ; preds = %cond.false70, %cond.true63
  %cond74 = phi i32 [ %sub69, %cond.true63 ], [ %shr72, %cond.false70 ]
  store i32 %cond74, i32* %y_end, align 4, !tbaa !6
  %87 = bitcast i32* %x_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #7
  %88 = load i32, i32* %w.addr, align 4, !tbaa !6
  %89 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds i32, i32* %89, i32 0
  %90 = load i32, i32* %arrayidx75, align 4, !tbaa !6
  %shr76 = ashr i32 %88, %90
  %91 = load i32, i32* %bx, align 4, !tbaa !6
  %92 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %93 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i32, i32* %93, i32 0
  %94 = load i32, i32* %arrayidx77, align 4, !tbaa !6
  %shr78 = ashr i32 %92, %94
  %mul79 = mul nsw i32 %91, %shr78
  %sub80 = sub nsw i32 %shr76, %mul79
  %95 = load i32, i32* %lag, align 4, !tbaa !6
  %sub81 = sub nsw i32 %sub80, %95
  %96 = load i32, i32* %bx, align 4, !tbaa !6
  %add82 = add nsw i32 %96, 1
  %97 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %cmp83 = icmp slt i32 %add82, %97
  br i1 %cmp83, label %land.lhs.true85, label %cond.false95

land.lhs.true85:                                  ; preds = %cond.end73
  %98 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %99 = load i32, i32* %by, align 4, !tbaa !6
  %100 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %mul86 = mul nsw i32 %99, %100
  %101 = load i32, i32* %bx, align 4, !tbaa !6
  %add87 = add nsw i32 %mul86, %101
  %add88 = add nsw i32 %add87, 1
  %arrayidx89 = getelementptr inbounds i8, i8* %98, i32 %add88
  %102 = load i8, i8* %arrayidx89, align 1, !tbaa !35
  %conv90 = zext i8 %102 to i32
  %tobool91 = icmp ne i32 %conv90, 0
  br i1 %tobool91, label %cond.true92, label %cond.false95

cond.true92:                                      ; preds = %land.lhs.true85
  %103 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %104 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %104, i32 0
  %105 = load i32, i32* %arrayidx93, align 4, !tbaa !6
  %shr94 = ashr i32 %103, %105
  br label %cond.end99

cond.false95:                                     ; preds = %land.lhs.true85, %cond.end73
  %106 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %107 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i32, i32* %107, i32 0
  %108 = load i32, i32* %arrayidx96, align 4, !tbaa !6
  %shr97 = ashr i32 %106, %108
  %109 = load i32, i32* %lag, align 4, !tbaa !6
  %sub98 = sub nsw i32 %shr97, %109
  br label %cond.end99

cond.end99:                                       ; preds = %cond.false95, %cond.true92
  %cond100 = phi i32 [ %shr94, %cond.true92 ], [ %sub98, %cond.false95 ]
  %cmp101 = icmp slt i32 %sub81, %cond100
  br i1 %cmp101, label %cond.true103, label %cond.false111

cond.true103:                                     ; preds = %cond.end99
  %110 = load i32, i32* %w.addr, align 4, !tbaa !6
  %111 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx104 = getelementptr inbounds i32, i32* %111, i32 0
  %112 = load i32, i32* %arrayidx104, align 4, !tbaa !6
  %shr105 = ashr i32 %110, %112
  %113 = load i32, i32* %bx, align 4, !tbaa !6
  %114 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %115 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i32, i32* %115, i32 0
  %116 = load i32, i32* %arrayidx106, align 4, !tbaa !6
  %shr107 = ashr i32 %114, %116
  %mul108 = mul nsw i32 %113, %shr107
  %sub109 = sub nsw i32 %shr105, %mul108
  %117 = load i32, i32* %lag, align 4, !tbaa !6
  %sub110 = sub nsw i32 %sub109, %117
  br label %cond.end131

cond.false111:                                    ; preds = %cond.end99
  %118 = load i32, i32* %bx, align 4, !tbaa !6
  %add112 = add nsw i32 %118, 1
  %119 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %cmp113 = icmp slt i32 %add112, %119
  br i1 %cmp113, label %land.lhs.true115, label %cond.false125

land.lhs.true115:                                 ; preds = %cond.false111
  %120 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %121 = load i32, i32* %by, align 4, !tbaa !6
  %122 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %mul116 = mul nsw i32 %121, %122
  %123 = load i32, i32* %bx, align 4, !tbaa !6
  %add117 = add nsw i32 %mul116, %123
  %add118 = add nsw i32 %add117, 1
  %arrayidx119 = getelementptr inbounds i8, i8* %120, i32 %add118
  %124 = load i8, i8* %arrayidx119, align 1, !tbaa !35
  %conv120 = zext i8 %124 to i32
  %tobool121 = icmp ne i32 %conv120, 0
  br i1 %tobool121, label %cond.true122, label %cond.false125

cond.true122:                                     ; preds = %land.lhs.true115
  %125 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %126 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %126, i32 0
  %127 = load i32, i32* %arrayidx123, align 4, !tbaa !6
  %shr124 = ashr i32 %125, %127
  br label %cond.end129

cond.false125:                                    ; preds = %land.lhs.true115, %cond.false111
  %128 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %129 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx126 = getelementptr inbounds i32, i32* %129, i32 0
  %130 = load i32, i32* %arrayidx126, align 4, !tbaa !6
  %shr127 = ashr i32 %128, %130
  %131 = load i32, i32* %lag, align 4, !tbaa !6
  %sub128 = sub nsw i32 %shr127, %131
  br label %cond.end129

cond.end129:                                      ; preds = %cond.false125, %cond.true122
  %cond130 = phi i32 [ %shr124, %cond.true122 ], [ %sub128, %cond.false125 ]
  br label %cond.end131

cond.end131:                                      ; preds = %cond.end129, %cond.true103
  %cond132 = phi i32 [ %sub110, %cond.true103 ], [ %cond130, %cond.end129 ]
  store i32 %cond132, i32* %x_end, align 4, !tbaa !6
  %132 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #7
  %133 = load i32, i32* %y_start, align 4, !tbaa !6
  store i32 %133, i32* %y, align 4, !tbaa !6
  br label %for.cond133

for.cond133:                                      ; preds = %for.inc189, %cond.end131
  %134 = load i32, i32* %y, align 4, !tbaa !6
  %135 = load i32, i32* %y_end, align 4, !tbaa !6
  %cmp134 = icmp slt i32 %134, %135
  br i1 %cmp134, label %for.body137, label %for.cond.cleanup136

for.cond.cleanup136:                              ; preds = %for.cond133
  store i32 8, i32* %cleanup.dest.slot, align 4
  %136 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #7
  br label %for.end191

for.body137:                                      ; preds = %for.cond133
  %137 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #7
  %138 = load i32, i32* %x_start, align 4, !tbaa !6
  store i32 %138, i32* %x, align 4, !tbaa !6
  br label %for.cond138

for.cond138:                                      ; preds = %for.inc186, %for.body137
  %139 = load i32, i32* %x, align 4, !tbaa !6
  %140 = load i32, i32* %x_end, align 4, !tbaa !6
  %cmp139 = icmp slt i32 %139, %140
  br i1 %cmp139, label %for.body142, label %for.cond.cleanup141

for.cond.cleanup141:                              ; preds = %for.cond138
  store i32 11, i32* %cleanup.dest.slot, align 4
  %141 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #7
  br label %for.end188

for.body142:                                      ; preds = %for.cond138
  %142 = bitcast double* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %142) #7
  %143 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params143 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %143, i32 0, i32 0
  %use_highbd = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params143, i32 0, i32 3
  %144 = load i32, i32* %use_highbd, align 4, !tbaa !58
  %tobool144 = icmp ne i32 %144, 0
  br i1 %tobool144, label %cond.true145, label %cond.false149

cond.true145:                                     ; preds = %for.body142
  %145 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %coords = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %145, i32 0, i32 3
  %146 = load [2 x i32]*, [2 x i32]** %coords, align 8, !tbaa !46
  %147 = load i32, i32* %num_coords, align 4, !tbaa !6
  %148 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %149 = bitcast i8* %148 to i16*
  %150 = load i8*, i8** %denoised.addr, align 4, !tbaa !2
  %151 = bitcast i8* %150 to i16*
  %152 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %153 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %154 = load i8*, i8** %alt_data.addr, align 4, !tbaa !2
  %155 = bitcast i8* %154 to i16*
  %156 = load i8*, i8** %alt_denoised.addr, align 4, !tbaa !2
  %157 = bitcast i8* %156 to i16*
  %158 = load i32, i32* %alt_stride.addr, align 4, !tbaa !6
  %159 = load i32, i32* %x, align 4, !tbaa !6
  %160 = load i32, i32* %x_o, align 4, !tbaa !6
  %add146 = add nsw i32 %159, %160
  %161 = load i32, i32* %y, align 4, !tbaa !6
  %162 = load i32, i32* %y_o, align 4, !tbaa !6
  %add147 = add nsw i32 %161, %162
  %163 = load double*, double** %buffer, align 4, !tbaa !2
  %call148 = call double @extract_ar_row_highbd([2 x i32]* %146, i32 %147, i16* %149, i16* %151, i32 %152, i32* %153, i16* %155, i16* %157, i32 %158, i32 %add146, i32 %add147, double* %163)
  br label %cond.end154

cond.false149:                                    ; preds = %for.body142
  %164 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %coords150 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %164, i32 0, i32 3
  %165 = load [2 x i32]*, [2 x i32]** %coords150, align 8, !tbaa !46
  %166 = load i32, i32* %num_coords, align 4, !tbaa !6
  %167 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %168 = load i8*, i8** %denoised.addr, align 4, !tbaa !2
  %169 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %170 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %171 = load i8*, i8** %alt_data.addr, align 4, !tbaa !2
  %172 = load i8*, i8** %alt_denoised.addr, align 4, !tbaa !2
  %173 = load i32, i32* %alt_stride.addr, align 4, !tbaa !6
  %174 = load i32, i32* %x, align 4, !tbaa !6
  %175 = load i32, i32* %x_o, align 4, !tbaa !6
  %add151 = add nsw i32 %174, %175
  %176 = load i32, i32* %y, align 4, !tbaa !6
  %177 = load i32, i32* %y_o, align 4, !tbaa !6
  %add152 = add nsw i32 %176, %177
  %178 = load double*, double** %buffer, align 4, !tbaa !2
  %call153 = call double @extract_ar_row_lowbd([2 x i32]* %165, i32 %166, i8* %167, i8* %168, i32 %169, i32* %170, i8* %171, i8* %172, i32 %173, i32 %add151, i32 %add152, double* %178)
  br label %cond.end154

cond.end154:                                      ; preds = %cond.false149, %cond.true145
  %cond155 = phi double [ %call148, %cond.true145 ], [ %call153, %cond.false149 ]
  store double %cond155, double* %val, align 8, !tbaa !11
  %179 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %179) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond156

for.cond156:                                      ; preds = %for.inc180, %cond.end154
  %180 = load i32, i32* %i, align 4, !tbaa !6
  %181 = load i32, i32* %n8, align 4, !tbaa !6
  %cmp157 = icmp slt i32 %180, %181
  br i1 %cmp157, label %for.body160, label %for.cond.cleanup159

for.cond.cleanup159:                              ; preds = %for.cond156
  store i32 14, i32* %cleanup.dest.slot, align 4
  %182 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #7
  br label %for.end182

for.body160:                                      ; preds = %for.cond156
  %183 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %183) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond161

for.cond161:                                      ; preds = %for.inc, %for.body160
  %184 = load i32, i32* %j, align 4, !tbaa !6
  %185 = load i32, i32* %n8, align 4, !tbaa !6
  %cmp162 = icmp slt i32 %184, %185
  br i1 %cmp162, label %for.body165, label %for.cond.cleanup164

for.cond.cleanup164:                              ; preds = %for.cond161
  store i32 17, i32* %cleanup.dest.slot, align 4
  %186 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #7
  br label %for.end

for.body165:                                      ; preds = %for.cond161
  %187 = load double*, double** %buffer, align 4, !tbaa !2
  %188 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx166 = getelementptr inbounds double, double* %187, i32 %188
  %189 = load double, double* %arrayidx166, align 8, !tbaa !11
  %190 = load double*, double** %buffer, align 4, !tbaa !2
  %191 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx167 = getelementptr inbounds double, double* %190, i32 %191
  %192 = load double, double* %arrayidx167, align 8, !tbaa !11
  %mul168 = fmul double %189, %192
  %193 = load double, double* %normalization, align 8, !tbaa !11
  %194 = load double, double* %normalization, align 8, !tbaa !11
  %mul169 = fmul double %193, %194
  %div = fdiv double %mul168, %mul169
  %195 = load double*, double** %A, align 4, !tbaa !2
  %196 = load i32, i32* %i, align 4, !tbaa !6
  %197 = load i32, i32* %n8, align 4, !tbaa !6
  %mul170 = mul nsw i32 %196, %197
  %198 = load i32, i32* %j, align 4, !tbaa !6
  %add171 = add nsw i32 %mul170, %198
  %arrayidx172 = getelementptr inbounds double, double* %195, i32 %add171
  %199 = load double, double* %arrayidx172, align 8, !tbaa !11
  %add173 = fadd double %199, %div
  store double %add173, double* %arrayidx172, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body165
  %200 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %200, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond161

for.end:                                          ; preds = %for.cond.cleanup164
  %201 = load double*, double** %buffer, align 4, !tbaa !2
  %202 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx174 = getelementptr inbounds double, double* %201, i32 %202
  %203 = load double, double* %arrayidx174, align 8, !tbaa !11
  %204 = load double, double* %val, align 8, !tbaa !11
  %mul175 = fmul double %203, %204
  %205 = load double, double* %normalization, align 8, !tbaa !11
  %206 = load double, double* %normalization, align 8, !tbaa !11
  %mul176 = fmul double %205, %206
  %div177 = fdiv double %mul175, %mul176
  %207 = load double*, double** %b, align 4, !tbaa !2
  %208 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx178 = getelementptr inbounds double, double* %207, i32 %208
  %209 = load double, double* %arrayidx178, align 8, !tbaa !11
  %add179 = fadd double %209, %div177
  store double %add179, double* %arrayidx178, align 8, !tbaa !11
  br label %for.inc180

for.inc180:                                       ; preds = %for.end
  %210 = load i32, i32* %i, align 4, !tbaa !6
  %inc181 = add nsw i32 %210, 1
  store i32 %inc181, i32* %i, align 4, !tbaa !6
  br label %for.cond156

for.end182:                                       ; preds = %for.cond.cleanup159
  %211 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state183 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %211, i32 0, i32 2
  %212 = load i32, i32* %c.addr, align 4, !tbaa !6
  %arrayidx184 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state183, i32 0, i32 %212
  %num_observations = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx184, i32 0, i32 2
  %213 = load i32, i32* %num_observations, align 8, !tbaa !50
  %inc185 = add nsw i32 %213, 1
  store i32 %inc185, i32* %num_observations, align 8, !tbaa !50
  %214 = bitcast double* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %214) #7
  br label %for.inc186

for.inc186:                                       ; preds = %for.end182
  %215 = load i32, i32* %x, align 4, !tbaa !6
  %inc187 = add nsw i32 %215, 1
  store i32 %inc187, i32* %x, align 4, !tbaa !6
  br label %for.cond138

for.end188:                                       ; preds = %for.cond.cleanup141
  br label %for.inc189

for.inc189:                                       ; preds = %for.end188
  %216 = load i32, i32* %y, align 4, !tbaa !6
  %inc190 = add nsw i32 %216, 1
  store i32 %inc190, i32* %y, align 4, !tbaa !6
  br label %for.cond133

for.end191:                                       ; preds = %for.cond.cleanup136
  %217 = bitcast i32* %x_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #7
  %218 = bitcast i32* %y_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #7
  %219 = bitcast i32* %x_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #7
  %220 = bitcast i32* %y_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end191, %if.then30
  %221 = bitcast i32* %x_o to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc192
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc192

for.inc192:                                       ; preds = %cleanup.cont, %cleanup
  %222 = load i32, i32* %bx, align 4, !tbaa !6
  %inc193 = add nsw i32 %222, 1
  store i32 %inc193, i32* %bx, align 4, !tbaa !6
  br label %for.cond18

for.end195:                                       ; preds = %for.cond.cleanup21
  %223 = bitcast i32* %y_o to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #7
  br label %for.inc196

for.inc196:                                       ; preds = %for.end195
  %224 = load i32, i32* %by, align 4, !tbaa !6
  %inc197 = add nsw i32 %224, 1
  store i32 %inc197, i32* %by, align 4, !tbaa !6
  br label %for.cond

for.end199:                                       ; preds = %for.cond.cleanup
  %225 = load double*, double** %buffer, align 4, !tbaa !2
  %226 = bitcast double* %225 to i8*
  call void @aom_free(i8* %226)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup200

cleanup200:                                       ; preds = %for.end199, %if.then
  %227 = bitcast i32* %n8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #7
  %228 = bitcast double** %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #7
  %229 = bitcast double** %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #7
  %230 = bitcast double** %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #7
  %231 = bitcast double* %normalization to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %231) #7
  %232 = bitcast i32* %num_coords to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #7
  %233 = bitcast i32* %lag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #7
  %234 = load i32, i32* %retval, align 4
  ret i32 %234

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i32 @ar_equation_system_solve(%struct.aom_noise_state_t* %state, i32 %is_chroma) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.aom_noise_state_t*, align 4
  %is_chroma.addr = alloca i32, align 4
  %ret = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %var = alloca double, align 8
  %n = alloca i32, align 4
  %i = alloca i32, align 4
  %sum_covar = alloca double, align 8
  %i10 = alloca i32, align 4
  %bi = alloca double, align 8
  %noise_var = alloca double, align 8
  store %struct.aom_noise_state_t* %state, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  store i32 %is_chroma, i32* %is_chroma.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %1, i32 0, i32 0
  %call = call i32 @equation_system_solve(%struct.aom_equation_system_t* %eqns)
  store i32 %call, i32* %ret, align 4, !tbaa !6
  %2 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %ar_gain = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %2, i32 0, i32 3
  store double 1.000000e+00, double* %ar_gain, align 8, !tbaa !48
  %3 = load i32, i32* %ret, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %ret, align 4, !tbaa !6
  store i32 %4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = bitcast double* %var to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #7
  store double 0.000000e+00, double* %var, align 8, !tbaa !11
  %6 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns1 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %7, i32 0, i32 0
  %n2 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns1, i32 0, i32 3
  %8 = load i32, i32* %n2, align 4, !tbaa !57
  store i32 %8, i32* %n, align 4, !tbaa !6
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns3 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %11, i32 0, i32 0
  %n4 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns3, i32 0, i32 3
  %12 = load i32, i32* %n4, align 4, !tbaa !57
  %13 = load i32, i32* %is_chroma.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %12, %13
  %cmp = icmp slt i32 %10, %sub
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns5 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %15, i32 0, i32 0
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns5, i32 0, i32 0
  %16 = load double*, double** %A, align 8, !tbaa !55
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %18 = load i32, i32* %n, align 4, !tbaa !6
  %mul = mul nsw i32 %17, %18
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %mul, %19
  %arrayidx = getelementptr inbounds double, double* %16, i32 %add
  %20 = load double, double* %arrayidx, align 8, !tbaa !11
  %21 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %num_observations = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %21, i32 0, i32 2
  %22 = load i32, i32* %num_observations, align 8, !tbaa !50
  %conv = sitofp i32 %22 to double
  %div = fdiv double %20, %conv
  %23 = load double, double* %var, align 8, !tbaa !11
  %add6 = fadd double %23, %div
  store double %add6, double* %var, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %25 = load i32, i32* %n, align 4, !tbaa !6
  %26 = load i32, i32* %is_chroma.addr, align 4, !tbaa !6
  %sub7 = sub nsw i32 %25, %26
  %conv8 = sitofp i32 %sub7 to double
  %27 = load double, double* %var, align 8, !tbaa !11
  %div9 = fdiv double %27, %conv8
  store double %div9, double* %var, align 8, !tbaa !11
  %28 = bitcast double* %sum_covar to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %28) #7
  store double 0.000000e+00, double* %sum_covar, align 8, !tbaa !11
  %29 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  store i32 0, i32* %i10, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc43, %for.end
  %30 = load i32, i32* %i10, align 4, !tbaa !6
  %31 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns12 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %31, i32 0, i32 0
  %n13 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns12, i32 0, i32 3
  %32 = load i32, i32* %n13, align 4, !tbaa !57
  %33 = load i32, i32* %is_chroma.addr, align 4, !tbaa !6
  %sub14 = sub nsw i32 %32, %33
  %cmp15 = icmp slt i32 %30, %sub14
  br i1 %cmp15, label %for.body18, label %for.cond.cleanup17

for.cond.cleanup17:                               ; preds = %for.cond11
  store i32 5, i32* %cleanup.dest.slot, align 4
  %34 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  br label %for.end45

for.body18:                                       ; preds = %for.cond11
  %35 = bitcast double* %bi to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %35) #7
  %36 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns19 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %36, i32 0, i32 0
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns19, i32 0, i32 1
  %37 = load double*, double** %b, align 4, !tbaa !56
  %38 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds double, double* %37, i32 %38
  %39 = load double, double* %arrayidx20, align 8, !tbaa !11
  store double %39, double* %bi, align 8, !tbaa !11
  %40 = load i32, i32* %is_chroma.addr, align 4, !tbaa !6
  %tobool21 = icmp ne i32 %40, 0
  br i1 %tobool21, label %if.then22, label %if.end34

if.then22:                                        ; preds = %for.body18
  %41 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns23 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %41, i32 0, i32 0
  %A24 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns23, i32 0, i32 0
  %42 = load double*, double** %A24, align 8, !tbaa !55
  %43 = load i32, i32* %i10, align 4, !tbaa !6
  %44 = load i32, i32* %n, align 4, !tbaa !6
  %mul25 = mul nsw i32 %43, %44
  %45 = load i32, i32* %n, align 4, !tbaa !6
  %sub26 = sub nsw i32 %45, 1
  %add27 = add nsw i32 %mul25, %sub26
  %arrayidx28 = getelementptr inbounds double, double* %42, i32 %add27
  %46 = load double, double* %arrayidx28, align 8, !tbaa !11
  %47 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns29 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %47, i32 0, i32 0
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns29, i32 0, i32 2
  %48 = load double*, double** %x, align 8, !tbaa !52
  %49 = load i32, i32* %n, align 4, !tbaa !6
  %sub30 = sub nsw i32 %49, 1
  %arrayidx31 = getelementptr inbounds double, double* %48, i32 %sub30
  %50 = load double, double* %arrayidx31, align 8, !tbaa !11
  %mul32 = fmul double %46, %50
  %51 = load double, double* %bi, align 8, !tbaa !11
  %sub33 = fsub double %51, %mul32
  store double %sub33, double* %bi, align 8, !tbaa !11
  br label %if.end34

if.end34:                                         ; preds = %if.then22, %for.body18
  %52 = load double, double* %bi, align 8, !tbaa !11
  %53 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %eqns35 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %53, i32 0, i32 0
  %x36 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns35, i32 0, i32 2
  %54 = load double*, double** %x36, align 8, !tbaa !52
  %55 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds double, double* %54, i32 %55
  %56 = load double, double* %arrayidx37, align 8, !tbaa !11
  %mul38 = fmul double %52, %56
  %57 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %num_observations39 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %57, i32 0, i32 2
  %58 = load i32, i32* %num_observations39, align 8, !tbaa !50
  %conv40 = sitofp i32 %58 to double
  %div41 = fdiv double %mul38, %conv40
  %59 = load double, double* %sum_covar, align 8, !tbaa !11
  %add42 = fadd double %59, %div41
  store double %add42, double* %sum_covar, align 8, !tbaa !11
  %60 = bitcast double* %bi to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %60) #7
  br label %for.inc43

for.inc43:                                        ; preds = %if.end34
  %61 = load i32, i32* %i10, align 4, !tbaa !6
  %inc44 = add nsw i32 %61, 1
  store i32 %inc44, i32* %i10, align 4, !tbaa !6
  br label %for.cond11

for.end45:                                        ; preds = %for.cond.cleanup17
  %62 = bitcast double* %noise_var to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %62) #7
  %63 = load double, double* %var, align 8, !tbaa !11
  %64 = load double, double* %sum_covar, align 8, !tbaa !11
  %sub46 = fsub double %63, %64
  %cmp47 = fcmp ogt double %sub46, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp47, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end45
  %65 = load double, double* %var, align 8, !tbaa !11
  %66 = load double, double* %sum_covar, align 8, !tbaa !11
  %sub49 = fsub double %65, %66
  br label %cond.end

cond.false:                                       ; preds = %for.end45
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %sub49, %cond.true ], [ 0x3EB0C6F7A0B5ED8D, %cond.false ]
  store double %cond, double* %noise_var, align 8, !tbaa !11
  %67 = load double, double* %var, align 8, !tbaa !11
  %68 = load double, double* %noise_var, align 8, !tbaa !11
  %div50 = fdiv double %67, %68
  %cmp51 = fcmp ogt double %div50, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp51, label %cond.true53, label %cond.false55

cond.true53:                                      ; preds = %cond.end
  %69 = load double, double* %var, align 8, !tbaa !11
  %70 = load double, double* %noise_var, align 8, !tbaa !11
  %div54 = fdiv double %69, %70
  br label %cond.end56

cond.false55:                                     ; preds = %cond.end
  br label %cond.end56

cond.end56:                                       ; preds = %cond.false55, %cond.true53
  %cond57 = phi double [ %div54, %cond.true53 ], [ 0x3EB0C6F7A0B5ED8D, %cond.false55 ]
  %71 = call double @llvm.sqrt.f64(double %cond57)
  %cmp58 = fcmp ogt double 1.000000e+00, %71
  br i1 %cmp58, label %cond.true60, label %cond.false61

cond.true60:                                      ; preds = %cond.end56
  br label %cond.end70

cond.false61:                                     ; preds = %cond.end56
  %72 = load double, double* %var, align 8, !tbaa !11
  %73 = load double, double* %noise_var, align 8, !tbaa !11
  %div62 = fdiv double %72, %73
  %cmp63 = fcmp ogt double %div62, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp63, label %cond.true65, label %cond.false67

cond.true65:                                      ; preds = %cond.false61
  %74 = load double, double* %var, align 8, !tbaa !11
  %75 = load double, double* %noise_var, align 8, !tbaa !11
  %div66 = fdiv double %74, %75
  br label %cond.end68

cond.false67:                                     ; preds = %cond.false61
  br label %cond.end68

cond.end68:                                       ; preds = %cond.false67, %cond.true65
  %cond69 = phi double [ %div66, %cond.true65 ], [ 0x3EB0C6F7A0B5ED8D, %cond.false67 ]
  %76 = call double @llvm.sqrt.f64(double %cond69)
  br label %cond.end70

cond.end70:                                       ; preds = %cond.end68, %cond.true60
  %cond71 = phi double [ 1.000000e+00, %cond.true60 ], [ %76, %cond.end68 ]
  %77 = load %struct.aom_noise_state_t*, %struct.aom_noise_state_t** %state.addr, align 4, !tbaa !2
  %ar_gain72 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %77, i32 0, i32 3
  store double %cond71, double* %ar_gain72, align 8, !tbaa !48
  %78 = load i32, i32* %ret, align 4, !tbaa !6
  store i32 %78, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %79 = bitcast double* %noise_var to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %79) #7
  %80 = bitcast double* %sum_covar to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %80) #7
  %81 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #7
  %82 = bitcast double* %var to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %82) #7
  br label %cleanup

cleanup:                                          ; preds = %cond.end70, %if.then
  %83 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #7
  %84 = load i32, i32* %retval, align 4
  ret i32 %84
}

; Function Attrs: nounwind
define internal void @set_chroma_coefficient_fallback_soln(%struct.aom_equation_system_t* %eqns) #0 {
entry:
  %eqns.addr = alloca %struct.aom_equation_system_t*, align 4
  %kTolerance = alloca double, align 8
  %last = alloca i32, align 4
  store %struct.aom_equation_system_t* %eqns, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %0 = bitcast double* %kTolerance to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  store double 0x3EB0C6F7A0B5ED8D, double* %kTolerance, align 8, !tbaa !11
  %1 = bitcast i32* %last to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %n = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %2, i32 0, i32 3
  %3 = load i32, i32* %n, align 4, !tbaa !22
  %sub = sub nsw i32 %3, 1
  store i32 %sub, i32* %last, align 4, !tbaa !6
  %4 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %4, i32 0, i32 2
  %5 = load double*, double** %x, align 4, !tbaa !25
  %6 = bitcast double* %5 to i8*
  %7 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %n1 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %7, i32 0, i32 3
  %8 = load i32, i32* %n1, align 4, !tbaa !22
  %mul = mul i32 8, %8
  call void @llvm.memset.p0i8.i32(i8* align 8 %6, i8 0, i32 %mul, i1 false)
  %9 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %9, i32 0, i32 0
  %10 = load double*, double** %A, align 4, !tbaa !23
  %11 = load i32, i32* %last, align 4, !tbaa !6
  %12 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %n2 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %12, i32 0, i32 3
  %13 = load i32, i32* %n2, align 4, !tbaa !22
  %mul3 = mul nsw i32 %11, %13
  %14 = load i32, i32* %last, align 4, !tbaa !6
  %add = add nsw i32 %mul3, %14
  %arrayidx = getelementptr inbounds double, double* %10, i32 %add
  %15 = load double, double* %arrayidx, align 8, !tbaa !11
  %16 = call double @llvm.fabs.f64(double %15)
  %cmp = fcmp ogt double %16, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %17 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %17, i32 0, i32 1
  %18 = load double*, double** %b, align 4, !tbaa !24
  %19 = load i32, i32* %last, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds double, double* %18, i32 %19
  %20 = load double, double* %arrayidx4, align 8, !tbaa !11
  %21 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %A5 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %21, i32 0, i32 0
  %22 = load double*, double** %A5, align 4, !tbaa !23
  %23 = load i32, i32* %last, align 4, !tbaa !6
  %24 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %n6 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %24, i32 0, i32 3
  %25 = load i32, i32* %n6, align 4, !tbaa !22
  %mul7 = mul nsw i32 %23, %25
  %26 = load i32, i32* %last, align 4, !tbaa !6
  %add8 = add nsw i32 %mul7, %26
  %arrayidx9 = getelementptr inbounds double, double* %22, i32 %add8
  %27 = load double, double* %arrayidx9, align 8, !tbaa !11
  %div = fdiv double %20, %27
  %28 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns.addr, align 4, !tbaa !2
  %x10 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %28, i32 0, i32 2
  %29 = load double*, double** %x10, align 4, !tbaa !25
  %30 = load i32, i32* %last, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds double, double* %29, i32 %30
  store double %div, double* %arrayidx11, align 8, !tbaa !11
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %31 = bitcast i32* %last to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast double* %kTolerance to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %32) #7
  ret void
}

; Function Attrs: nounwind
define internal void @add_noise_std_observations(%struct.aom_noise_model_t* %noise_model, i32 %c, double* %coeffs, i8* %data, i8* %denoised, i32 %w, i32 %h, i32 %stride, i32* %sub_log2, i8* %alt_data, i32 %alt_stride, i8* %flat_blocks, i32 %block_size, i32 %num_blocks_w, i32 %num_blocks_h) #0 {
entry:
  %noise_model.addr = alloca %struct.aom_noise_model_t*, align 4
  %c.addr = alloca i32, align 4
  %coeffs.addr = alloca double*, align 4
  %data.addr = alloca i8*, align 4
  %denoised.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %sub_log2.addr = alloca i32*, align 4
  %alt_data.addr = alloca i8*, align 4
  %alt_stride.addr = alloca i32, align 4
  %flat_blocks.addr = alloca i8*, align 4
  %block_size.addr = alloca i32, align 4
  %num_blocks_w.addr = alloca i32, align 4
  %num_blocks_h.addr = alloca i32, align 4
  %num_coords = alloca i32, align 4
  %noise_strength_solver = alloca %struct.aom_noise_strength_solver_t*, align 4
  %noise_strength_luma = alloca %struct.aom_noise_strength_solver_t*, align 4
  %luma_gain = alloca double, align 8
  %noise_gain = alloca double, align 8
  %by = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %y_o = alloca i32, align 4
  %bx = alloca i32, align 4
  %x_o = alloca i32, align 4
  %num_samples_h = alloca i32, align 4
  %num_samples_w = alloca i32, align 4
  %block_mean = alloca double, align 8
  %noise_var = alloca double, align 8
  %luma_strength = alloca double, align 8
  %corr = alloca double, align 8
  %uncorr_std = alloca double, align 8
  %adjusted_strength = alloca double, align 8
  store %struct.aom_noise_model_t* %noise_model, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  store i32 %c, i32* %c.addr, align 4, !tbaa !6
  store double* %coeffs, double** %coeffs.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i8* %denoised, i8** %denoised.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32* %sub_log2, i32** %sub_log2.addr, align 4, !tbaa !2
  store i8* %alt_data, i8** %alt_data.addr, align 4, !tbaa !2
  store i32 %alt_stride, i32* %alt_stride.addr, align 4, !tbaa !6
  store i8* %flat_blocks, i8** %flat_blocks.addr, align 4, !tbaa !2
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  store i32 %num_blocks_w, i32* %num_blocks_w.addr, align 4, !tbaa !6
  store i32 %num_blocks_h, i32* %num_blocks_h.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_coords to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %n = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %1, i32 0, i32 4
  %2 = load i32, i32* %n, align 4, !tbaa !44
  store i32 %2, i32* %num_coords, align 4, !tbaa !6
  %3 = bitcast %struct.aom_noise_strength_solver_t** %noise_strength_solver to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %4, i32 0, i32 2
  %5 = load i32, i32* %c.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state, i32 0, i32 %5
  %strength_solver = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx, i32 0, i32 1
  store %struct.aom_noise_strength_solver_t* %strength_solver, %struct.aom_noise_strength_solver_t** %noise_strength_solver, align 4, !tbaa !2
  %6 = bitcast %struct.aom_noise_strength_solver_t** %noise_strength_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state1 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %7, i32 0, i32 2
  %arrayidx2 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state1, i32 0, i32 0
  %strength_solver3 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx2, i32 0, i32 1
  store %struct.aom_noise_strength_solver_t* %strength_solver3, %struct.aom_noise_strength_solver_t** %noise_strength_luma, align 4, !tbaa !2
  %8 = bitcast double* %luma_gain to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #7
  %9 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state4 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %9, i32 0, i32 2
  %arrayidx5 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state4, i32 0, i32 0
  %ar_gain = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx5, i32 0, i32 3
  %10 = load double, double* %ar_gain, align 8, !tbaa !48
  store double %10, double* %luma_gain, align 8, !tbaa !11
  %11 = bitcast double* %noise_gain to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %11) #7
  %12 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state6 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %12, i32 0, i32 2
  %13 = load i32, i32* %c.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state6, i32 0, i32 %13
  %ar_gain8 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx7, i32 0, i32 3
  %14 = load double, double* %ar_gain8, align 8, !tbaa !48
  store double %14, double* %noise_gain, align 8, !tbaa !11
  %15 = bitcast i32* %by to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  store i32 0, i32* %by, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc109, %entry
  %16 = load i32, i32* %by, align 4, !tbaa !6
  %17 = load i32, i32* %num_blocks_h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %16, %17
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %18 = bitcast i32* %by to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  br label %for.end112

for.body:                                         ; preds = %for.cond
  %19 = bitcast i32* %y_o to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load i32, i32* %by, align 4, !tbaa !6
  %21 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %22 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %22, i32 1
  %23 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %shr = ashr i32 %21, %23
  %mul = mul nsw i32 %20, %shr
  store i32 %mul, i32* %y_o, align 4, !tbaa !6
  %24 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  store i32 0, i32* %bx, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body
  %25 = load i32, i32* %bx, align 4, !tbaa !6
  %26 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %25, %26
  br i1 %cmp11, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond10
  store i32 5, i32* %cleanup.dest.slot, align 4
  %27 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  br label %for.end

for.body13:                                       ; preds = %for.cond10
  %28 = bitcast i32* %x_o to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load i32, i32* %bx, align 4, !tbaa !6
  %30 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %31 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i32, i32* %31, i32 0
  %32 = load i32, i32* %arrayidx14, align 4, !tbaa !6
  %shr15 = ashr i32 %30, %32
  %mul16 = mul nsw i32 %29, %shr15
  store i32 %mul16, i32* %x_o, align 4, !tbaa !6
  %33 = load i8*, i8** %flat_blocks.addr, align 4, !tbaa !2
  %34 = load i32, i32* %by, align 4, !tbaa !6
  %35 = load i32, i32* %num_blocks_w.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 %34, %35
  %36 = load i32, i32* %bx, align 4, !tbaa !6
  %add = add nsw i32 %mul17, %36
  %arrayidx18 = getelementptr inbounds i8, i8* %33, i32 %add
  %37 = load i8, i8* %arrayidx18, align 1, !tbaa !35
  %tobool = icmp ne i8 %37, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body13
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body13
  %38 = bitcast i32* %num_samples_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  %39 = load i32, i32* %h.addr, align 4, !tbaa !6
  %40 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %40, i32 1
  %41 = load i32, i32* %arrayidx19, align 4, !tbaa !6
  %shr20 = ashr i32 %39, %41
  %42 = load i32, i32* %by, align 4, !tbaa !6
  %43 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %44 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %44, i32 1
  %45 = load i32, i32* %arrayidx21, align 4, !tbaa !6
  %shr22 = ashr i32 %43, %45
  %mul23 = mul nsw i32 %42, %shr22
  %sub = sub nsw i32 %shr20, %mul23
  %46 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %47 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %47, i32 1
  %48 = load i32, i32* %arrayidx24, align 4, !tbaa !6
  %shr25 = ashr i32 %46, %48
  %cmp26 = icmp slt i32 %sub, %shr25
  br i1 %cmp26, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %49 = load i32, i32* %h.addr, align 4, !tbaa !6
  %50 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %50, i32 1
  %51 = load i32, i32* %arrayidx27, align 4, !tbaa !6
  %shr28 = ashr i32 %49, %51
  %52 = load i32, i32* %by, align 4, !tbaa !6
  %53 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %54 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %54, i32 1
  %55 = load i32, i32* %arrayidx29, align 4, !tbaa !6
  %shr30 = ashr i32 %53, %55
  %mul31 = mul nsw i32 %52, %shr30
  %sub32 = sub nsw i32 %shr28, %mul31
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %56 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %57 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %57, i32 1
  %58 = load i32, i32* %arrayidx33, align 4, !tbaa !6
  %shr34 = ashr i32 %56, %58
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub32, %cond.true ], [ %shr34, %cond.false ]
  store i32 %cond, i32* %num_samples_h, align 4, !tbaa !6
  %59 = bitcast i32* %num_samples_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #7
  %60 = load i32, i32* %w.addr, align 4, !tbaa !6
  %61 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %61, i32 0
  %62 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %shr36 = ashr i32 %60, %62
  %63 = load i32, i32* %bx, align 4, !tbaa !6
  %64 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %65 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %65, i32 0
  %66 = load i32, i32* %arrayidx37, align 4, !tbaa !6
  %shr38 = ashr i32 %64, %66
  %mul39 = mul nsw i32 %63, %shr38
  %sub40 = sub nsw i32 %shr36, %mul39
  %67 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %68 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %68, i32 0
  %69 = load i32, i32* %arrayidx41, align 4, !tbaa !6
  %shr42 = ashr i32 %67, %69
  %cmp43 = icmp slt i32 %sub40, %shr42
  br i1 %cmp43, label %cond.true44, label %cond.false51

cond.true44:                                      ; preds = %cond.end
  %70 = load i32, i32* %w.addr, align 4, !tbaa !6
  %71 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %71, i32 0
  %72 = load i32, i32* %arrayidx45, align 4, !tbaa !6
  %shr46 = ashr i32 %70, %72
  %73 = load i32, i32* %bx, align 4, !tbaa !6
  %74 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %75 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %75, i32 0
  %76 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %shr48 = ashr i32 %74, %76
  %mul49 = mul nsw i32 %73, %shr48
  %sub50 = sub nsw i32 %shr46, %mul49
  br label %cond.end54

cond.false51:                                     ; preds = %cond.end
  %77 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %78 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %78, i32 0
  %79 = load i32, i32* %arrayidx52, align 4, !tbaa !6
  %shr53 = ashr i32 %77, %79
  br label %cond.end54

cond.end54:                                       ; preds = %cond.false51, %cond.true44
  %cond55 = phi i32 [ %sub50, %cond.true44 ], [ %shr53, %cond.false51 ]
  store i32 %cond55, i32* %num_samples_w, align 4, !tbaa !6
  %80 = load i32, i32* %num_samples_w, align 4, !tbaa !6
  %81 = load i32, i32* %num_samples_h, align 4, !tbaa !6
  %mul56 = mul nsw i32 %80, %81
  %82 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp57 = icmp sgt i32 %mul56, %82
  br i1 %cmp57, label %if.then58, label %if.end107

if.then58:                                        ; preds = %cond.end54
  %83 = bitcast double* %block_mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %83) #7
  %84 = load i8*, i8** %alt_data.addr, align 4, !tbaa !2
  %tobool59 = icmp ne i8* %84, null
  br i1 %tobool59, label %cond.true60, label %cond.false61

cond.true60:                                      ; preds = %if.then58
  %85 = load i8*, i8** %alt_data.addr, align 4, !tbaa !2
  br label %cond.end62

cond.false61:                                     ; preds = %if.then58
  %86 = load i8*, i8** %data.addr, align 4, !tbaa !2
  br label %cond.end62

cond.end62:                                       ; preds = %cond.false61, %cond.true60
  %cond63 = phi i8* [ %85, %cond.true60 ], [ %86, %cond.false61 ]
  %87 = load i32, i32* %w.addr, align 4, !tbaa !6
  %88 = load i32, i32* %h.addr, align 4, !tbaa !6
  %89 = load i8*, i8** %alt_data.addr, align 4, !tbaa !2
  %tobool64 = icmp ne i8* %89, null
  br i1 %tobool64, label %cond.true65, label %cond.false66

cond.true65:                                      ; preds = %cond.end62
  %90 = load i32, i32* %alt_stride.addr, align 4, !tbaa !6
  br label %cond.end67

cond.false66:                                     ; preds = %cond.end62
  %91 = load i32, i32* %stride.addr, align 4, !tbaa !6
  br label %cond.end67

cond.end67:                                       ; preds = %cond.false66, %cond.true65
  %cond68 = phi i32 [ %90, %cond.true65 ], [ %91, %cond.false66 ]
  %92 = load i32, i32* %x_o, align 4, !tbaa !6
  %93 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i32, i32* %93, i32 0
  %94 = load i32, i32* %arrayidx69, align 4, !tbaa !6
  %shl = shl i32 %92, %94
  %95 = load i32, i32* %y_o, align 4, !tbaa !6
  %96 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds i32, i32* %96, i32 1
  %97 = load i32, i32* %arrayidx70, align 4, !tbaa !6
  %shl71 = shl i32 %95, %97
  %98 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %99 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %99, i32 0, i32 0
  %use_highbd = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 3
  %100 = load i32, i32* %use_highbd, align 4, !tbaa !58
  %call = call double @get_block_mean(i8* %cond63, i32 %87, i32 %88, i32 %cond68, i32 %shl, i32 %shl71, i32 %98, i32 %100)
  store double %call, double* %block_mean, align 8, !tbaa !11
  %101 = bitcast double* %noise_var to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %101) #7
  %102 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %103 = load i8*, i8** %denoised.addr, align 4, !tbaa !2
  %104 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %105 = load i32, i32* %w.addr, align 4, !tbaa !6
  %106 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds i32, i32* %106, i32 0
  %107 = load i32, i32* %arrayidx72, align 4, !tbaa !6
  %shr73 = ashr i32 %105, %107
  %108 = load i32, i32* %h.addr, align 4, !tbaa !6
  %109 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds i32, i32* %109, i32 1
  %110 = load i32, i32* %arrayidx74, align 4, !tbaa !6
  %shr75 = ashr i32 %108, %110
  %111 = load i32, i32* %x_o, align 4, !tbaa !6
  %112 = load i32, i32* %y_o, align 4, !tbaa !6
  %113 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %114 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds i32, i32* %114, i32 0
  %115 = load i32, i32* %arrayidx76, align 4, !tbaa !6
  %shr77 = ashr i32 %113, %115
  %116 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %117 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds i32, i32* %117, i32 1
  %118 = load i32, i32* %arrayidx78, align 4, !tbaa !6
  %shr79 = ashr i32 %116, %118
  %119 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params80 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %119, i32 0, i32 0
  %use_highbd81 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params80, i32 0, i32 3
  %120 = load i32, i32* %use_highbd81, align 4, !tbaa !58
  %call82 = call double @get_noise_var(i8* %102, i8* %103, i32 %104, i32 %shr73, i32 %shr75, i32 %111, i32 %112, i32 %shr77, i32 %shr79, i32 %120)
  store double %call82, double* %noise_var, align 8, !tbaa !11
  %121 = bitcast double* %luma_strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %121) #7
  %122 = load i32, i32* %c.addr, align 4, !tbaa !6
  %cmp83 = icmp sgt i32 %122, 0
  br i1 %cmp83, label %cond.true84, label %cond.false87

cond.true84:                                      ; preds = %cond.end67
  %123 = load double, double* %luma_gain, align 8, !tbaa !11
  %124 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %noise_strength_luma, align 4, !tbaa !2
  %125 = load double, double* %block_mean, align 8, !tbaa !11
  %call85 = call double @noise_strength_solver_get_value(%struct.aom_noise_strength_solver_t* %124, double %125)
  %mul86 = fmul double %123, %call85
  br label %cond.end88

cond.false87:                                     ; preds = %cond.end67
  br label %cond.end88

cond.end88:                                       ; preds = %cond.false87, %cond.true84
  %cond89 = phi double [ %mul86, %cond.true84 ], [ 0.000000e+00, %cond.false87 ]
  store double %cond89, double* %luma_strength, align 8, !tbaa !11
  %126 = bitcast double* %corr to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %126) #7
  %127 = load i32, i32* %c.addr, align 4, !tbaa !6
  %cmp90 = icmp sgt i32 %127, 0
  br i1 %cmp90, label %cond.true91, label %cond.false93

cond.true91:                                      ; preds = %cond.end88
  %128 = load double*, double** %coeffs.addr, align 4, !tbaa !2
  %129 = load i32, i32* %num_coords, align 4, !tbaa !6
  %arrayidx92 = getelementptr inbounds double, double* %128, i32 %129
  %130 = load double, double* %arrayidx92, align 8, !tbaa !11
  br label %cond.end94

cond.false93:                                     ; preds = %cond.end88
  br label %cond.end94

cond.end94:                                       ; preds = %cond.false93, %cond.true91
  %cond95 = phi double [ %130, %cond.true91 ], [ 0.000000e+00, %cond.false93 ]
  store double %cond95, double* %corr, align 8, !tbaa !11
  %131 = bitcast double* %uncorr_std to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %131) #7
  %132 = load double, double* %noise_var, align 8, !tbaa !11
  %div = fdiv double %132, 1.600000e+01
  %133 = load double, double* %noise_var, align 8, !tbaa !11
  %134 = load double, double* %corr, align 8, !tbaa !11
  %135 = load double, double* %luma_strength, align 8, !tbaa !11
  %mul96 = fmul double %134, %135
  %136 = call double @llvm.pow.f64(double %mul96, double 2.000000e+00)
  %sub97 = fsub double %133, %136
  %cmp98 = fcmp ogt double %div, %sub97
  br i1 %cmp98, label %cond.true99, label %cond.false101

cond.true99:                                      ; preds = %cond.end94
  %137 = load double, double* %noise_var, align 8, !tbaa !11
  %div100 = fdiv double %137, 1.600000e+01
  br label %cond.end104

cond.false101:                                    ; preds = %cond.end94
  %138 = load double, double* %noise_var, align 8, !tbaa !11
  %139 = load double, double* %corr, align 8, !tbaa !11
  %140 = load double, double* %luma_strength, align 8, !tbaa !11
  %mul102 = fmul double %139, %140
  %141 = call double @llvm.pow.f64(double %mul102, double 2.000000e+00)
  %sub103 = fsub double %138, %141
  br label %cond.end104

cond.end104:                                      ; preds = %cond.false101, %cond.true99
  %cond105 = phi double [ %div100, %cond.true99 ], [ %sub103, %cond.false101 ]
  %142 = call double @llvm.sqrt.f64(double %cond105)
  store double %142, double* %uncorr_std, align 8, !tbaa !11
  %143 = bitcast double* %adjusted_strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %143) #7
  %144 = load double, double* %uncorr_std, align 8, !tbaa !11
  %145 = load double, double* %noise_gain, align 8, !tbaa !11
  %div106 = fdiv double %144, %145
  store double %div106, double* %adjusted_strength, align 8, !tbaa !11
  %146 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %noise_strength_solver, align 4, !tbaa !2
  %147 = load double, double* %block_mean, align 8, !tbaa !11
  %148 = load double, double* %adjusted_strength, align 8, !tbaa !11
  call void @aom_noise_strength_solver_add_measurement(%struct.aom_noise_strength_solver_t* %146, double %147, double %148)
  %149 = bitcast double* %adjusted_strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %149) #7
  %150 = bitcast double* %uncorr_std to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %150) #7
  %151 = bitcast double* %corr to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %151) #7
  %152 = bitcast double* %luma_strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %152) #7
  %153 = bitcast double* %noise_var to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %153) #7
  %154 = bitcast double* %block_mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %154) #7
  br label %if.end107

if.end107:                                        ; preds = %cond.end104, %cond.end54
  %155 = bitcast i32* %num_samples_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #7
  %156 = bitcast i32* %num_samples_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end107, %if.then
  %157 = bitcast i32* %x_o to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %158 = load i32, i32* %bx, align 4, !tbaa !6
  %inc = add nsw i32 %158, 1
  store i32 %inc, i32* %bx, align 4, !tbaa !6
  br label %for.cond10

for.end:                                          ; preds = %for.cond.cleanup12
  %159 = bitcast i32* %y_o to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #7
  br label %for.inc109

for.inc109:                                       ; preds = %for.end
  %160 = load i32, i32* %by, align 4, !tbaa !6
  %inc110 = add nsw i32 %160, 1
  store i32 %inc110, i32* %by, align 4, !tbaa !6
  br label %for.cond

for.end112:                                       ; preds = %for.cond.cleanup
  %161 = bitcast double* %noise_gain to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %161) #7
  %162 = bitcast double* %luma_gain to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %162) #7
  %163 = bitcast %struct.aom_noise_strength_solver_t** %noise_strength_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #7
  %164 = bitcast %struct.aom_noise_strength_solver_t** %noise_strength_solver to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #7
  %165 = bitcast i32* %num_coords to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #7
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i32 @is_noise_model_different(%struct.aom_noise_model_t* %noise_model) #0 {
entry:
  %retval = alloca i32, align 4
  %noise_model.addr = alloca %struct.aom_noise_model_t*, align 4
  %kCoeffThreshold = alloca double, align 8
  %kStrengthThreshold = alloca double, align 8
  %c = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %corr = alloca double, align 8
  %dx = alloca double, align 8
  %latest_eqns = alloca %struct.aom_equation_system_t*, align 4
  %combined_eqns = alloca %struct.aom_equation_system_t*, align 4
  %diff = alloca double, align 8
  %total_weight = alloca double, align 8
  %j = alloca i32, align 4
  %weight = alloca double, align 8
  %i = alloca i32, align 4
  store %struct.aom_noise_model_t* %noise_model, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %0 = bitcast double* %kCoeffThreshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  store double 9.000000e-01, double* %kCoeffThreshold, align 8, !tbaa !11
  %1 = bitcast double* %kStrengthThreshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #7
  %2 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %2, i32 0, i32 0
  %bit_depth = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 2
  %3 = load i32, i32* %bit_depth, align 8, !tbaa !54
  %sub = sub nsw i32 %3, 8
  %shl = shl i32 1, %sub
  %conv = sitofp i32 %shl to double
  %mul = fmul double 5.000000e-03, %conv
  store double %mul, double* %kStrengthThreshold, align 8, !tbaa !11
  %4 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc59, %entry
  %5 = load i32, i32* %c, align 4, !tbaa !6
  %cmp = icmp slt i32 %5, 1
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

for.body:                                         ; preds = %for.cond
  %6 = bitcast double* %corr to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #7
  %7 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %7, i32 0, i32 2
  %8 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state, i32 0, i32 %8
  %eqns = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx, i32 0, i32 0
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 2
  %9 = load double*, double** %x, align 8, !tbaa !52
  %10 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %10, i32 0, i32 1
  %11 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state, i32 0, i32 %11
  %eqns3 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx2, i32 0, i32 0
  %x4 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns3, i32 0, i32 2
  %12 = load double*, double** %x4, align 8, !tbaa !52
  %13 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state5 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %13, i32 0, i32 1
  %14 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state5, i32 0, i32 %14
  %eqns7 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx6, i32 0, i32 0
  %n = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns7, i32 0, i32 3
  %15 = load i32, i32* %n, align 4, !tbaa !57
  %call = call double @aom_normalized_cross_correlation(double* %9, double* %12, i32 %15)
  store double %call, double* %corr, align 8, !tbaa !11
  %16 = load double, double* %corr, align 8, !tbaa !11
  %cmp8 = fcmp olt double %16, 9.000000e-01
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup58

if.end:                                           ; preds = %for.body
  %17 = bitcast double* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %17) #7
  %18 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state10 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %18, i32 0, i32 2
  %19 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state10, i32 0, i32 %19
  %strength_solver = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx11, i32 0, i32 1
  %num_bins = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver, i32 0, i32 3
  %20 = load i32, i32* %num_bins, align 8, !tbaa !59
  %conv12 = sitofp i32 %20 to double
  %div = fdiv double 1.000000e+00, %conv12
  store double %div, double* %dx, align 8, !tbaa !11
  %21 = bitcast %struct.aom_equation_system_t** %latest_eqns to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state13 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %22, i32 0, i32 2
  %23 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state13, i32 0, i32 %23
  %strength_solver15 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx14, i32 0, i32 1
  %eqns16 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver15, i32 0, i32 0
  store %struct.aom_equation_system_t* %eqns16, %struct.aom_equation_system_t** %latest_eqns, align 4, !tbaa !2
  %24 = bitcast %struct.aom_equation_system_t** %combined_eqns to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state17 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %25, i32 0, i32 1
  %26 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state17, i32 0, i32 %26
  %strength_solver19 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx18, i32 0, i32 1
  %eqns20 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver19, i32 0, i32 0
  store %struct.aom_equation_system_t* %eqns20, %struct.aom_equation_system_t** %combined_eqns, align 4, !tbaa !2
  %27 = bitcast double* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %27) #7
  store double 0.000000e+00, double* %diff, align 8, !tbaa !11
  %28 = bitcast double* %total_weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %28) #7
  store double 0.000000e+00, double* %total_weight, align 8, !tbaa !11
  %29 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc45, %if.end
  %30 = load i32, i32* %j, align 4, !tbaa !6
  %31 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %latest_eqns, align 4, !tbaa !2
  %n22 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %31, i32 0, i32 3
  %32 = load i32, i32* %n22, align 4, !tbaa !22
  %cmp23 = icmp slt i32 %30, %32
  br i1 %cmp23, label %for.body26, label %for.cond.cleanup25

for.cond.cleanup25:                               ; preds = %for.cond21
  store i32 5, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  br label %for.end47

for.body26:                                       ; preds = %for.cond21
  %34 = bitcast double* %weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %34) #7
  store double 0.000000e+00, double* %weight, align 8, !tbaa !11
  %35 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc, %for.body26
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %latest_eqns, align 4, !tbaa !2
  %n28 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %37, i32 0, i32 3
  %38 = load i32, i32* %n28, align 4, !tbaa !22
  %cmp29 = icmp slt i32 %36, %38
  br i1 %cmp29, label %for.body32, label %for.cond.cleanup31

for.cond.cleanup31:                               ; preds = %for.cond27
  store i32 8, i32* %cleanup.dest.slot, align 4
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  br label %for.end

for.body32:                                       ; preds = %for.cond27
  %40 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %latest_eqns, align 4, !tbaa !2
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %40, i32 0, i32 0
  %41 = load double*, double** %A, align 4, !tbaa !23
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %43 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %latest_eqns, align 4, !tbaa !2
  %n33 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %43, i32 0, i32 3
  %44 = load i32, i32* %n33, align 4, !tbaa !22
  %mul34 = mul nsw i32 %42, %44
  %45 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul34, %45
  %arrayidx35 = getelementptr inbounds double, double* %41, i32 %add
  %46 = load double, double* %arrayidx35, align 8, !tbaa !11
  %47 = load double, double* %weight, align 8, !tbaa !11
  %add36 = fadd double %47, %46
  store double %add36, double* %weight, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body32
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond27

for.end:                                          ; preds = %for.cond.cleanup31
  %49 = load double, double* %weight, align 8, !tbaa !11
  %50 = call double @llvm.sqrt.f64(double %49)
  store double %50, double* %weight, align 8, !tbaa !11
  %51 = load double, double* %weight, align 8, !tbaa !11
  %52 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %latest_eqns, align 4, !tbaa !2
  %x37 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %52, i32 0, i32 2
  %53 = load double*, double** %x37, align 4, !tbaa !25
  %54 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds double, double* %53, i32 %54
  %55 = load double, double* %arrayidx38, align 8, !tbaa !11
  %56 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %combined_eqns, align 4, !tbaa !2
  %x39 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %56, i32 0, i32 2
  %57 = load double*, double** %x39, align 4, !tbaa !25
  %58 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds double, double* %57, i32 %58
  %59 = load double, double* %arrayidx40, align 8, !tbaa !11
  %sub41 = fsub double %55, %59
  %60 = call double @llvm.fabs.f64(double %sub41)
  %mul42 = fmul double %51, %60
  %61 = load double, double* %diff, align 8, !tbaa !11
  %add43 = fadd double %61, %mul42
  store double %add43, double* %diff, align 8, !tbaa !11
  %62 = load double, double* %weight, align 8, !tbaa !11
  %63 = load double, double* %total_weight, align 8, !tbaa !11
  %add44 = fadd double %63, %62
  store double %add44, double* %total_weight, align 8, !tbaa !11
  %64 = bitcast double* %weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %64) #7
  br label %for.inc45

for.inc45:                                        ; preds = %for.end
  %65 = load i32, i32* %j, align 4, !tbaa !6
  %inc46 = add nsw i32 %65, 1
  store i32 %inc46, i32* %j, align 4, !tbaa !6
  br label %for.cond21

for.end47:                                        ; preds = %for.cond.cleanup25
  %66 = load double, double* %diff, align 8, !tbaa !11
  %67 = load double, double* %dx, align 8, !tbaa !11
  %mul48 = fmul double %66, %67
  %68 = load double, double* %total_weight, align 8, !tbaa !11
  %div49 = fdiv double %mul48, %68
  %69 = load double, double* %kStrengthThreshold, align 8, !tbaa !11
  %cmp50 = fcmp ogt double %div49, %69
  br i1 %cmp50, label %if.then52, label %if.end53

if.then52:                                        ; preds = %for.end47
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end53:                                         ; preds = %for.end47
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end53, %if.then52
  %70 = bitcast double* %total_weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %70) #7
  %71 = bitcast double* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %71) #7
  %72 = bitcast %struct.aom_equation_system_t** %combined_eqns to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  %73 = bitcast %struct.aom_equation_system_t** %latest_eqns to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  %74 = bitcast double* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %74) #7
  br label %cleanup58

cleanup58:                                        ; preds = %cleanup, %if.then
  %75 = bitcast double* %corr to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %75) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup61 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup58
  br label %for.inc59

for.inc59:                                        ; preds = %cleanup.cont
  %76 = load i32, i32* %c, align 4, !tbaa !6
  %inc60 = add nsw i32 %76, 1
  store i32 %inc60, i32* %c, align 4, !tbaa !6
  br label %for.cond

cleanup61:                                        ; preds = %cleanup58, %for.cond.cleanup
  %77 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  %cleanup.dest62 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest62, label %cleanup64 [
    i32 2, label %for.end63
  ]

for.end63:                                        ; preds = %cleanup61
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup64

cleanup64:                                        ; preds = %for.end63, %cleanup61
  %78 = bitcast double* %kStrengthThreshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %78) #7
  %79 = bitcast double* %kCoeffThreshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %79) #7
  %80 = load i32, i32* %retval, align 4
  ret i32 %80
}

; Function Attrs: nounwind
define internal void @equation_system_add(%struct.aom_equation_system_t* %dest, %struct.aom_equation_system_t* %src) #0 {
entry:
  %dest.addr = alloca %struct.aom_equation_system_t*, align 4
  %src.addr = alloca %struct.aom_equation_system_t*, align 4
  %n = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.aom_equation_system_t* %dest, %struct.aom_equation_system_t** %dest.addr, align 4, !tbaa !2
  store %struct.aom_equation_system_t* %src, %struct.aom_equation_system_t** %src.addr, align 4, !tbaa !2
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %dest.addr, align 4, !tbaa !2
  %n1 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %1, i32 0, i32 3
  %2 = load i32, i32* %n1, align 4, !tbaa !22
  store i32 %2, i32* %n, align 4, !tbaa !6
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc14, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %n, align 4, !tbaa !6
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end16

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %7 = load i32, i32* %j, align 4, !tbaa !6
  %8 = load i32, i32* %n, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %7, %8
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %9 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %src.addr, align 4, !tbaa !2
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %9, i32 0, i32 0
  %10 = load double*, double** %A, align 4, !tbaa !23
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %n, align 4, !tbaa !6
  %mul = mul nsw i32 %11, %12
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul, %13
  %arrayidx = getelementptr inbounds double, double* %10, i32 %add
  %14 = load double, double* %arrayidx, align 8, !tbaa !11
  %15 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %dest.addr, align 4, !tbaa !2
  %A5 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %15, i32 0, i32 0
  %16 = load double*, double** %A5, align 4, !tbaa !23
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %18 = load i32, i32* %n, align 4, !tbaa !6
  %mul6 = mul nsw i32 %17, %18
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %add7 = add nsw i32 %mul6, %19
  %arrayidx8 = getelementptr inbounds double, double* %16, i32 %add7
  %20 = load double, double* %arrayidx8, align 8, !tbaa !11
  %add9 = fadd double %20, %14
  store double %add9, double* %arrayidx8, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  %22 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %src.addr, align 4, !tbaa !2
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %22, i32 0, i32 1
  %23 = load double*, double** %b, align 4, !tbaa !24
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds double, double* %23, i32 %24
  %25 = load double, double* %arrayidx10, align 8, !tbaa !11
  %26 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %dest.addr, align 4, !tbaa !2
  %b11 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %26, i32 0, i32 1
  %27 = load double*, double** %b11, align 4, !tbaa !24
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds double, double* %27, i32 %28
  %29 = load double, double* %arrayidx12, align 8, !tbaa !11
  %add13 = fadd double %29, %25
  store double %add13, double* %arrayidx12, align 8, !tbaa !11
  br label %for.inc14

for.inc14:                                        ; preds = %for.end
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %inc15 = add nsw i32 %30, 1
  store i32 %inc15, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end16:                                        ; preds = %for.cond
  %31 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  ret void
}

; Function Attrs: nounwind
define internal void @noise_strength_solver_add(%struct.aom_noise_strength_solver_t* %dest, %struct.aom_noise_strength_solver_t* %src) #0 {
entry:
  %dest.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  %src.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  store %struct.aom_noise_strength_solver_t* %dest, %struct.aom_noise_strength_solver_t** %dest.addr, align 4, !tbaa !2
  store %struct.aom_noise_strength_solver_t* %src, %struct.aom_noise_strength_solver_t** %src.addr, align 4, !tbaa !2
  %0 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %dest.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %0, i32 0, i32 0
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %src.addr, align 4, !tbaa !2
  %eqns1 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %1, i32 0, i32 0
  call void @equation_system_add(%struct.aom_equation_system_t* %eqns, %struct.aom_equation_system_t* %eqns1)
  %2 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %src.addr, align 4, !tbaa !2
  %num_equations = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %2, i32 0, i32 4
  %3 = load i32, i32* %num_equations, align 4, !tbaa !19
  %4 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %dest.addr, align 4, !tbaa !2
  %num_equations2 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %4, i32 0, i32 4
  %5 = load i32, i32* %num_equations2, align 4, !tbaa !19
  %add = add nsw i32 %5, %3
  store i32 %add, i32* %num_equations2, align 4, !tbaa !19
  %6 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %src.addr, align 4, !tbaa !2
  %total = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %6, i32 0, i32 5
  %7 = load double, double* %total, align 8, !tbaa !18
  %8 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %dest.addr, align 4, !tbaa !2
  %total3 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %8, i32 0, i32 5
  %9 = load double, double* %total3, align 8, !tbaa !18
  %add4 = fadd double %9, %7
  store double %add4, double* %total3, align 8, !tbaa !18
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_noise_model_save_latest(%struct.aom_noise_model_t* %noise_model) #0 {
entry:
  %noise_model.addr = alloca %struct.aom_noise_model_t*, align 4
  %c = alloca i32, align 4
  store %struct.aom_noise_model_t* %noise_model, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %0 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %c, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %3, i32 0, i32 1
  %4 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state, i32 0, i32 %4
  %eqns = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx, i32 0, i32 0
  %5 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %5, i32 0, i32 2
  %6 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state, i32 0, i32 %6
  %eqns2 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx1, i32 0, i32 0
  call void @equation_system_copy(%struct.aom_equation_system_t* %eqns, %struct.aom_equation_system_t* %eqns2)
  %7 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state3 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %7, i32 0, i32 1
  %8 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state3, i32 0, i32 %8
  %strength_solver = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx4, i32 0, i32 1
  %eqns5 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver, i32 0, i32 0
  %9 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state6 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %9, i32 0, i32 2
  %10 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state6, i32 0, i32 %10
  %strength_solver8 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx7, i32 0, i32 1
  %eqns9 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver8, i32 0, i32 0
  call void @equation_system_copy(%struct.aom_equation_system_t* %eqns5, %struct.aom_equation_system_t* %eqns9)
  %11 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state10 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %11, i32 0, i32 2
  %12 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state10, i32 0, i32 %12
  %strength_solver12 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx11, i32 0, i32 1
  %num_equations = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver12, i32 0, i32 4
  %13 = load i32, i32* %num_equations, align 4, !tbaa !53
  %14 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state13 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %14, i32 0, i32 1
  %15 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state13, i32 0, i32 %15
  %strength_solver15 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx14, i32 0, i32 1
  %num_equations16 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver15, i32 0, i32 4
  store i32 %13, i32* %num_equations16, align 4, !tbaa !53
  %16 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state17 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %16, i32 0, i32 2
  %17 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state17, i32 0, i32 %17
  %num_observations = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx18, i32 0, i32 2
  %18 = load i32, i32* %num_observations, align 8, !tbaa !50
  %19 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state19 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %19, i32 0, i32 1
  %20 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state19, i32 0, i32 %20
  %num_observations21 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx20, i32 0, i32 2
  store i32 %18, i32* %num_observations21, align 8, !tbaa !50
  %21 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %latest_state22 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %21, i32 0, i32 2
  %22 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %latest_state22, i32 0, i32 %22
  %ar_gain = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx23, i32 0, i32 3
  %23 = load double, double* %ar_gain, align 8, !tbaa !48
  %24 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state24 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %24, i32 0, i32 1
  %25 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state24, i32 0, i32 %25
  %ar_gain26 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx25, i32 0, i32 3
  store double %23, double* %ar_gain26, align 8, !tbaa !48
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %c, align 4, !tbaa !6
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal void @equation_system_copy(%struct.aom_equation_system_t* %dst, %struct.aom_equation_system_t* %src) #0 {
entry:
  %dst.addr = alloca %struct.aom_equation_system_t*, align 4
  %src.addr = alloca %struct.aom_equation_system_t*, align 4
  %n = alloca i32, align 4
  store %struct.aom_equation_system_t* %dst, %struct.aom_equation_system_t** %dst.addr, align 4, !tbaa !2
  store %struct.aom_equation_system_t* %src, %struct.aom_equation_system_t** %src.addr, align 4, !tbaa !2
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %dst.addr, align 4, !tbaa !2
  %n1 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %1, i32 0, i32 3
  %2 = load i32, i32* %n1, align 4, !tbaa !22
  store i32 %2, i32* %n, align 4, !tbaa !6
  %3 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %dst.addr, align 4, !tbaa !2
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %3, i32 0, i32 0
  %4 = load double*, double** %A, align 4, !tbaa !23
  %5 = bitcast double* %4 to i8*
  %6 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %src.addr, align 4, !tbaa !2
  %A2 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %6, i32 0, i32 0
  %7 = load double*, double** %A2, align 4, !tbaa !23
  %8 = bitcast double* %7 to i8*
  %9 = load i32, i32* %n, align 4, !tbaa !6
  %mul = mul i32 8, %9
  %10 = load i32, i32* %n, align 4, !tbaa !6
  %mul3 = mul i32 %mul, %10
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %5, i8* align 8 %8, i32 %mul3, i1 false)
  %11 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %dst.addr, align 4, !tbaa !2
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %11, i32 0, i32 2
  %12 = load double*, double** %x, align 4, !tbaa !25
  %13 = bitcast double* %12 to i8*
  %14 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %src.addr, align 4, !tbaa !2
  %x4 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %14, i32 0, i32 2
  %15 = load double*, double** %x4, align 4, !tbaa !25
  %16 = bitcast double* %15 to i8*
  %17 = load i32, i32* %n, align 4, !tbaa !6
  %mul5 = mul i32 8, %17
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %13, i8* align 8 %16, i32 %mul5, i1 false)
  %18 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %dst.addr, align 4, !tbaa !2
  %b = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %18, i32 0, i32 1
  %19 = load double*, double** %b, align 4, !tbaa !24
  %20 = bitcast double* %19 to i8*
  %21 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %src.addr, align 4, !tbaa !2
  %b6 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %21, i32 0, i32 1
  %22 = load double*, double** %b6, align 4, !tbaa !24
  %23 = bitcast double* %22 to i8*
  %24 = load i32, i32* %n, align 4, !tbaa !6
  %mul7 = mul i32 8, %24
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %20, i8* align 8 %23, i32 %mul7, i1 false)
  %25 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_noise_model_get_grain_parameters(%struct.aom_noise_model_t* %noise_model, %struct.aom_film_grain_t* %film_grain) #0 {
entry:
  %retval = alloca i32, align 4
  %noise_model.addr = alloca %struct.aom_noise_model_t*, align 4
  %film_grain.addr = alloca %struct.aom_film_grain_t*, align 4
  %random_seed = alloca i16, align 2
  %scaling_points = alloca [3 x %struct.aom_noise_strength_lut_t], align 16
  %strength_divisor = alloca double, align 8
  %max_scaling_value = alloca double, align 8
  %c = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %max_scaling_value_log2 = alloca i32, align 4
  %scale_factor = alloca double, align 8
  %film_grain_scaling = alloca [3 x [2 x i32]*], align 4
  %c97 = alloca i32, align 4
  %i103 = alloca i32, align 4
  %n_coeff = alloca i32, align 4
  %max_coeff = alloca double, align 8
  %min_coeff = alloca double, align 8
  %y_corr = alloca [2 x double], align 16
  %avg_luma_strength = alloca double, align 8
  %c144 = alloca i32, align 4
  %eqns150 = alloca %struct.aom_equation_system_t*, align 4
  %i154 = alloca i32, align 4
  %solver = alloca %struct.aom_noise_strength_solver_t*, align 4
  %average_strength = alloca double, align 8
  %total_weight = alloca double, align 8
  %i185 = alloca i32, align 4
  %w = alloca double, align 8
  %j = alloca i32, align 4
  %scale_ar_coeff = alloca double, align 8
  %ar_coeffs = alloca [3 x i32*], align 4
  %c279 = alloca i32, align 4
  %eqns285 = alloca %struct.aom_equation_system_t*, align 4
  %i289 = alloca i32, align 4
  store %struct.aom_noise_model_t* %noise_model, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  store %struct.aom_film_grain_t* %film_grain, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %0 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %0, i32 0, i32 0
  %lag = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  %1 = load i32, i32* %lag, align 4, !tbaa !51
  %cmp = icmp sgt i32 %1, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %3 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params1 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %3, i32 0, i32 0
  %lag2 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params1, i32 0, i32 1
  %4 = load i32, i32* %lag2, align 4, !tbaa !51
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %2, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.17, i32 0, i32 0), i32 %4)
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %5 = bitcast i16* %random_seed to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %5) #7
  %6 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %random_seed3 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %6, i32 0, i32 25
  %7 = load i16, i16* %random_seed3, align 4, !tbaa !60
  store i16 %7, i16* %random_seed, align 2, !tbaa !33
  %8 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %9 = bitcast %struct.aom_film_grain_t* %8 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %9, i8 0, i32 648, i1 false)
  %10 = load i16, i16* %random_seed, align 2, !tbaa !33
  %11 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %random_seed4 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %11, i32 0, i32 25
  store i16 %10, i16* %random_seed4, align 4, !tbaa !60
  %12 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %apply_grain = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %12, i32 0, i32 0
  store i32 1, i32* %apply_grain, align 4, !tbaa !62
  %13 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %update_parameters = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %13, i32 0, i32 1
  store i32 1, i32* %update_parameters, align 4, !tbaa !63
  %14 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params5 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %14, i32 0, i32 0
  %lag6 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params5, i32 0, i32 1
  %15 = load i32, i32* %lag6, align 4, !tbaa !51
  %16 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %ar_coeff_lag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %16, i32 0, i32 9
  store i32 %15, i32* %ar_coeff_lag, align 4, !tbaa !64
  %17 = bitcast [3 x %struct.aom_noise_strength_lut_t]* %scaling_points to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %17) #7
  %18 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %18, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state, i32 0, i32 0
  %strength_solver = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx, i32 0, i32 1
  %arraydecay = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 0
  %add.ptr = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arraydecay, i32 0
  %call7 = call i32 @aom_noise_strength_solver_fit_piecewise(%struct.aom_noise_strength_solver_t* %strength_solver, i32 14, %struct.aom_noise_strength_lut_t* %add.ptr)
  %19 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state8 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %19, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state8, i32 0, i32 1
  %strength_solver10 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx9, i32 0, i32 1
  %arraydecay11 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 0
  %add.ptr12 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arraydecay11, i32 1
  %call13 = call i32 @aom_noise_strength_solver_fit_piecewise(%struct.aom_noise_strength_solver_t* %strength_solver10, i32 10, %struct.aom_noise_strength_lut_t* %add.ptr12)
  %20 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state14 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %20, i32 0, i32 1
  %arrayidx15 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state14, i32 0, i32 2
  %strength_solver16 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx15, i32 0, i32 1
  %arraydecay17 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 0
  %add.ptr18 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arraydecay17, i32 2
  %call19 = call i32 @aom_noise_strength_solver_fit_piecewise(%struct.aom_noise_strength_solver_t* %strength_solver16, i32 10, %struct.aom_noise_strength_lut_t* %add.ptr18)
  %21 = bitcast double* %strength_divisor to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %21) #7
  %22 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %params20 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %22, i32 0, i32 0
  %bit_depth = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params20, i32 0, i32 2
  %23 = load i32, i32* %bit_depth, align 8, !tbaa !54
  %sub = sub nsw i32 %23, 8
  %shl = shl i32 1, %sub
  %conv = sitofp i32 %shl to double
  store double %conv, double* %strength_divisor, align 8, !tbaa !11
  %24 = bitcast double* %max_scaling_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %24) #7
  store double 1.000000e-04, double* %max_scaling_value, align 8, !tbaa !11
  %25 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc77, %if.end
  %26 = load i32, i32* %c, align 4, !tbaa !6
  %cmp21 = icmp slt i32 %26, 3
  br i1 %cmp21, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %27 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  br label %for.end79

for.body:                                         ; preds = %for.cond
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc, %for.body
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %30 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %30
  %num_points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx24, i32 0, i32 1
  %31 = load i32, i32* %num_points, align 4, !tbaa !8
  %cmp25 = icmp slt i32 %29, %31
  br i1 %cmp25, label %for.body28, label %for.cond.cleanup27

for.cond.cleanup27:                               ; preds = %for.cond23
  store i32 5, i32* %cleanup.dest.slot, align 4
  %32 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  br label %for.end

for.body28:                                       ; preds = %for.cond23
  %33 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %33
  %points = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx29, i32 0, i32 0
  %34 = load [2 x double]*, [2 x double]** %points, align 8, !tbaa !10
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds [2 x double], [2 x double]* %34, i32 %35
  %arrayidx31 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx30, i32 0, i32 0
  %36 = load double, double* %arrayidx31, align 8, !tbaa !11
  %37 = load double, double* %strength_divisor, align 8, !tbaa !11
  %div = fdiv double %36, %37
  %cmp32 = fcmp olt double 2.550000e+02, %div
  br i1 %cmp32, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body28
  br label %cond.end

cond.false:                                       ; preds = %for.body28
  %38 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %38
  %points35 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx34, i32 0, i32 0
  %39 = load [2 x double]*, [2 x double]** %points35, align 8, !tbaa !10
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds [2 x double], [2 x double]* %39, i32 %40
  %arrayidx37 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx36, i32 0, i32 0
  %41 = load double, double* %arrayidx37, align 8, !tbaa !11
  %42 = load double, double* %strength_divisor, align 8, !tbaa !11
  %div38 = fdiv double %41, %42
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ 2.550000e+02, %cond.true ], [ %div38, %cond.false ]
  %43 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %43
  %points40 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx39, i32 0, i32 0
  %44 = load [2 x double]*, [2 x double]** %points40, align 8, !tbaa !10
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds [2 x double], [2 x double]* %44, i32 %45
  %arrayidx42 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx41, i32 0, i32 0
  store double %cond, double* %arrayidx42, align 8, !tbaa !11
  %46 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %46
  %points44 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx43, i32 0, i32 0
  %47 = load [2 x double]*, [2 x double]** %points44, align 8, !tbaa !10
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds [2 x double], [2 x double]* %47, i32 %48
  %arrayidx46 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx45, i32 0, i32 1
  %49 = load double, double* %arrayidx46, align 8, !tbaa !11
  %50 = load double, double* %strength_divisor, align 8, !tbaa !11
  %div47 = fdiv double %49, %50
  %cmp48 = fcmp olt double 2.550000e+02, %div47
  br i1 %cmp48, label %cond.true50, label %cond.false51

cond.true50:                                      ; preds = %cond.end
  br label %cond.end57

cond.false51:                                     ; preds = %cond.end
  %51 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx52 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %51
  %points53 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx52, i32 0, i32 0
  %52 = load [2 x double]*, [2 x double]** %points53, align 8, !tbaa !10
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx54 = getelementptr inbounds [2 x double], [2 x double]* %52, i32 %53
  %arrayidx55 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx54, i32 0, i32 1
  %54 = load double, double* %arrayidx55, align 8, !tbaa !11
  %55 = load double, double* %strength_divisor, align 8, !tbaa !11
  %div56 = fdiv double %54, %55
  br label %cond.end57

cond.end57:                                       ; preds = %cond.false51, %cond.true50
  %cond58 = phi double [ 2.550000e+02, %cond.true50 ], [ %div56, %cond.false51 ]
  %56 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %56
  %points60 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx59, i32 0, i32 0
  %57 = load [2 x double]*, [2 x double]** %points60, align 8, !tbaa !10
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds [2 x double], [2 x double]* %57, i32 %58
  %arrayidx62 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx61, i32 0, i32 1
  store double %cond58, double* %arrayidx62, align 8, !tbaa !11
  %59 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %59
  %points64 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx63, i32 0, i32 0
  %60 = load [2 x double]*, [2 x double]** %points64, align 8, !tbaa !10
  %61 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds [2 x double], [2 x double]* %60, i32 %61
  %arrayidx66 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx65, i32 0, i32 1
  %62 = load double, double* %arrayidx66, align 8, !tbaa !11
  %63 = load double, double* %max_scaling_value, align 8, !tbaa !11
  %cmp67 = fcmp ogt double %62, %63
  br i1 %cmp67, label %cond.true69, label %cond.false74

cond.true69:                                      ; preds = %cond.end57
  %64 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx70 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %64
  %points71 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx70, i32 0, i32 0
  %65 = load [2 x double]*, [2 x double]** %points71, align 8, !tbaa !10
  %66 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx72 = getelementptr inbounds [2 x double], [2 x double]* %65, i32 %66
  %arrayidx73 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx72, i32 0, i32 1
  %67 = load double, double* %arrayidx73, align 8, !tbaa !11
  br label %cond.end75

cond.false74:                                     ; preds = %cond.end57
  %68 = load double, double* %max_scaling_value, align 8, !tbaa !11
  br label %cond.end75

cond.end75:                                       ; preds = %cond.false74, %cond.true69
  %cond76 = phi double [ %67, %cond.true69 ], [ %68, %cond.false74 ]
  store double %cond76, double* %max_scaling_value, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %cond.end75
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %69, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond23

for.end:                                          ; preds = %for.cond.cleanup27
  br label %for.inc77

for.inc77:                                        ; preds = %for.end
  %70 = load i32, i32* %c, align 4, !tbaa !6
  %inc78 = add nsw i32 %70, 1
  store i32 %inc78, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.end79:                                        ; preds = %for.cond.cleanup
  %71 = bitcast i32* %max_scaling_value_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #7
  %72 = load double, double* %max_scaling_value, align 8, !tbaa !11
  %73 = call double @llvm.log2.f64(double %72)
  %add = fadd double %73, 1.000000e+00
  %74 = call double @llvm.floor.f64(double %add)
  %conv80 = fptosi double %74 to i32
  %call81 = call i32 @clamp(i32 %conv80, i32 2, i32 5)
  store i32 %call81, i32* %max_scaling_value_log2, align 4, !tbaa !6
  %75 = load i32, i32* %max_scaling_value_log2, align 4, !tbaa !6
  %sub82 = sub nsw i32 8, %75
  %add83 = add nsw i32 5, %sub82
  %76 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %scaling_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %76, i32 0, i32 8
  store i32 %add83, i32* %scaling_shift, align 4, !tbaa !65
  %77 = bitcast double* %scale_factor to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %77) #7
  %78 = load i32, i32* %max_scaling_value_log2, align 4, !tbaa !6
  %sub84 = sub nsw i32 8, %78
  %shl85 = shl i32 1, %sub84
  %conv86 = sitofp i32 %shl85 to double
  store double %conv86, double* %scale_factor, align 8, !tbaa !11
  %arrayidx87 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 0
  %num_points88 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx87, i32 0, i32 1
  %79 = load i32, i32* %num_points88, align 4, !tbaa !8
  %80 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %80, i32 0, i32 3
  store i32 %79, i32* %num_y_points, align 4, !tbaa !66
  %arrayidx89 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 1
  %num_points90 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx89, i32 0, i32 1
  %81 = load i32, i32* %num_points90, align 4, !tbaa !8
  %82 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %num_cb_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %82, i32 0, i32 5
  store i32 %81, i32* %num_cb_points, align 4, !tbaa !67
  %arrayidx91 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 2
  %num_points92 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx91, i32 0, i32 1
  %83 = load i32, i32* %num_points92, align 4, !tbaa !8
  %84 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %num_cr_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %84, i32 0, i32 7
  store i32 %83, i32* %num_cr_points, align 4, !tbaa !68
  %85 = bitcast [3 x [2 x i32]*]* %film_grain_scaling to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %85) #7
  %arrayinit.begin = getelementptr inbounds [3 x [2 x i32]*], [3 x [2 x i32]*]* %film_grain_scaling, i32 0, i32 0
  %86 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %scaling_points_y = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %86, i32 0, i32 2
  %arraydecay93 = getelementptr inbounds [14 x [2 x i32]], [14 x [2 x i32]]* %scaling_points_y, i32 0, i32 0
  store [2 x i32]* %arraydecay93, [2 x i32]** %arrayinit.begin, align 4, !tbaa !2
  %arrayinit.element = getelementptr inbounds [2 x i32]*, [2 x i32]** %arrayinit.begin, i32 1
  %87 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %scaling_points_cb = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %87, i32 0, i32 4
  %arraydecay94 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cb, i32 0, i32 0
  store [2 x i32]* %arraydecay94, [2 x i32]** %arrayinit.element, align 4, !tbaa !2
  %arrayinit.element95 = getelementptr inbounds [2 x i32]*, [2 x i32]** %arrayinit.element, i32 1
  %88 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %scaling_points_cr = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %88, i32 0, i32 6
  %arraydecay96 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cr, i32 0, i32 0
  store [2 x i32]* %arraydecay96, [2 x i32]** %arrayinit.element95, align 4, !tbaa !2
  %89 = bitcast i32* %c97 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #7
  store i32 0, i32* %c97, align 4, !tbaa !6
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc133, %for.end79
  %90 = load i32, i32* %c97, align 4, !tbaa !6
  %cmp99 = icmp slt i32 %90, 3
  br i1 %cmp99, label %for.body102, label %for.cond.cleanup101

for.cond.cleanup101:                              ; preds = %for.cond98
  store i32 8, i32* %cleanup.dest.slot, align 4
  %91 = bitcast i32* %c97 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #7
  br label %for.end135

for.body102:                                      ; preds = %for.cond98
  %92 = bitcast i32* %i103 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #7
  store i32 0, i32* %i103, align 4, !tbaa !6
  br label %for.cond104

for.cond104:                                      ; preds = %for.inc130, %for.body102
  %93 = load i32, i32* %i103, align 4, !tbaa !6
  %94 = load i32, i32* %c97, align 4, !tbaa !6
  %arrayidx105 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %94
  %num_points106 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx105, i32 0, i32 1
  %95 = load i32, i32* %num_points106, align 4, !tbaa !8
  %cmp107 = icmp slt i32 %93, %95
  br i1 %cmp107, label %for.body110, label %for.cond.cleanup109

for.cond.cleanup109:                              ; preds = %for.cond104
  store i32 11, i32* %cleanup.dest.slot, align 4
  %96 = bitcast i32* %i103 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #7
  br label %for.end132

for.body110:                                      ; preds = %for.cond104
  %97 = load i32, i32* %c97, align 4, !tbaa !6
  %arrayidx111 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %97
  %points112 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx111, i32 0, i32 0
  %98 = load [2 x double]*, [2 x double]** %points112, align 8, !tbaa !10
  %99 = load i32, i32* %i103, align 4, !tbaa !6
  %arrayidx113 = getelementptr inbounds [2 x double], [2 x double]* %98, i32 %99
  %arrayidx114 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx113, i32 0, i32 0
  %100 = load double, double* %arrayidx114, align 8, !tbaa !11
  %add115 = fadd double %100, 5.000000e-01
  %conv116 = fptosi double %add115 to i32
  %101 = load i32, i32* %c97, align 4, !tbaa !6
  %arrayidx117 = getelementptr inbounds [3 x [2 x i32]*], [3 x [2 x i32]*]* %film_grain_scaling, i32 0, i32 %101
  %102 = load [2 x i32]*, [2 x i32]** %arrayidx117, align 4, !tbaa !2
  %103 = load i32, i32* %i103, align 4, !tbaa !6
  %arrayidx118 = getelementptr inbounds [2 x i32], [2 x i32]* %102, i32 %103
  %arrayidx119 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx118, i32 0, i32 0
  store i32 %conv116, i32* %arrayidx119, align 4, !tbaa !6
  %104 = load double, double* %scale_factor, align 8, !tbaa !11
  %105 = load i32, i32* %c97, align 4, !tbaa !6
  %arrayidx120 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 %105
  %points121 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arrayidx120, i32 0, i32 0
  %106 = load [2 x double]*, [2 x double]** %points121, align 8, !tbaa !10
  %107 = load i32, i32* %i103, align 4, !tbaa !6
  %arrayidx122 = getelementptr inbounds [2 x double], [2 x double]* %106, i32 %107
  %arrayidx123 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx122, i32 0, i32 1
  %108 = load double, double* %arrayidx123, align 8, !tbaa !11
  %mul = fmul double %104, %108
  %add124 = fadd double %mul, 5.000000e-01
  %conv125 = fptosi double %add124 to i32
  %call126 = call i32 @clamp(i32 %conv125, i32 0, i32 255)
  %109 = load i32, i32* %c97, align 4, !tbaa !6
  %arrayidx127 = getelementptr inbounds [3 x [2 x i32]*], [3 x [2 x i32]*]* %film_grain_scaling, i32 0, i32 %109
  %110 = load [2 x i32]*, [2 x i32]** %arrayidx127, align 4, !tbaa !2
  %111 = load i32, i32* %i103, align 4, !tbaa !6
  %arrayidx128 = getelementptr inbounds [2 x i32], [2 x i32]* %110, i32 %111
  %arrayidx129 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx128, i32 0, i32 1
  store i32 %call126, i32* %arrayidx129, align 4, !tbaa !6
  br label %for.inc130

for.inc130:                                       ; preds = %for.body110
  %112 = load i32, i32* %i103, align 4, !tbaa !6
  %inc131 = add nsw i32 %112, 1
  store i32 %inc131, i32* %i103, align 4, !tbaa !6
  br label %for.cond104

for.end132:                                       ; preds = %for.cond.cleanup109
  br label %for.inc133

for.inc133:                                       ; preds = %for.end132
  %113 = load i32, i32* %c97, align 4, !tbaa !6
  %inc134 = add nsw i32 %113, 1
  store i32 %inc134, i32* %c97, align 4, !tbaa !6
  br label %for.cond98

for.end135:                                       ; preds = %for.cond.cleanup101
  %arraydecay136 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 0
  %add.ptr137 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arraydecay136, i32 0
  call void @aom_noise_strength_lut_free(%struct.aom_noise_strength_lut_t* %add.ptr137)
  %arraydecay138 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 0
  %add.ptr139 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arraydecay138, i32 1
  call void @aom_noise_strength_lut_free(%struct.aom_noise_strength_lut_t* %add.ptr139)
  %arraydecay140 = getelementptr inbounds [3 x %struct.aom_noise_strength_lut_t], [3 x %struct.aom_noise_strength_lut_t]* %scaling_points, i32 0, i32 0
  %add.ptr141 = getelementptr inbounds %struct.aom_noise_strength_lut_t, %struct.aom_noise_strength_lut_t* %arraydecay140, i32 2
  call void @aom_noise_strength_lut_free(%struct.aom_noise_strength_lut_t* %add.ptr141)
  %114 = bitcast i32* %n_coeff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #7
  %115 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state142 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %115, i32 0, i32 1
  %arrayidx143 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state142, i32 0, i32 0
  %eqns = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx143, i32 0, i32 0
  %n = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 3
  %116 = load i32, i32* %n, align 4, !tbaa !57
  store i32 %116, i32* %n_coeff, align 4, !tbaa !6
  %117 = bitcast double* %max_coeff to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %117) #7
  store double 1.000000e-04, double* %max_coeff, align 8, !tbaa !11
  %118 = bitcast double* %min_coeff to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %118) #7
  store double -1.000000e-04, double* %min_coeff, align 8, !tbaa !11
  %119 = bitcast [2 x double]* %y_corr to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %119) #7
  %120 = bitcast [2 x double]* %y_corr to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %120, i8 0, i32 16, i1 false)
  %121 = bitcast double* %avg_luma_strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %121) #7
  store double 0.000000e+00, double* %avg_luma_strength, align 8, !tbaa !11
  %122 = bitcast i32* %c144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #7
  store i32 0, i32* %c144, align 4, !tbaa !6
  br label %for.cond145

for.cond145:                                      ; preds = %for.inc255, %for.end135
  %123 = load i32, i32* %c144, align 4, !tbaa !6
  %cmp146 = icmp slt i32 %123, 3
  br i1 %cmp146, label %for.body149, label %for.cond.cleanup148

for.cond.cleanup148:                              ; preds = %for.cond145
  store i32 14, i32* %cleanup.dest.slot, align 4
  %124 = bitcast i32* %c144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #7
  br label %for.end257

for.body149:                                      ; preds = %for.cond145
  %125 = bitcast %struct.aom_equation_system_t** %eqns150 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #7
  %126 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state151 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %126, i32 0, i32 1
  %127 = load i32, i32* %c144, align 4, !tbaa !6
  %arrayidx152 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state151, i32 0, i32 %127
  %eqns153 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx152, i32 0, i32 0
  store %struct.aom_equation_system_t* %eqns153, %struct.aom_equation_system_t** %eqns150, align 4, !tbaa !2
  %128 = bitcast i32* %i154 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #7
  store i32 0, i32* %i154, align 4, !tbaa !6
  br label %for.cond155

for.cond155:                                      ; preds = %for.inc179, %for.body149
  %129 = load i32, i32* %i154, align 4, !tbaa !6
  %130 = load i32, i32* %n_coeff, align 4, !tbaa !6
  %cmp156 = icmp slt i32 %129, %130
  br i1 %cmp156, label %for.body159, label %for.cond.cleanup158

for.cond.cleanup158:                              ; preds = %for.cond155
  store i32 17, i32* %cleanup.dest.slot, align 4
  %131 = bitcast i32* %i154 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #7
  br label %for.end181

for.body159:                                      ; preds = %for.cond155
  %132 = load double, double* %max_coeff, align 8, !tbaa !11
  %133 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns150, align 4, !tbaa !2
  %x = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %133, i32 0, i32 2
  %134 = load double*, double** %x, align 4, !tbaa !25
  %135 = load i32, i32* %i154, align 4, !tbaa !6
  %arrayidx160 = getelementptr inbounds double, double* %134, i32 %135
  %136 = load double, double* %arrayidx160, align 8, !tbaa !11
  %cmp161 = fcmp ogt double %132, %136
  br i1 %cmp161, label %cond.true163, label %cond.false164

cond.true163:                                     ; preds = %for.body159
  %137 = load double, double* %max_coeff, align 8, !tbaa !11
  br label %cond.end167

cond.false164:                                    ; preds = %for.body159
  %138 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns150, align 4, !tbaa !2
  %x165 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %138, i32 0, i32 2
  %139 = load double*, double** %x165, align 4, !tbaa !25
  %140 = load i32, i32* %i154, align 4, !tbaa !6
  %arrayidx166 = getelementptr inbounds double, double* %139, i32 %140
  %141 = load double, double* %arrayidx166, align 8, !tbaa !11
  br label %cond.end167

cond.end167:                                      ; preds = %cond.false164, %cond.true163
  %cond168 = phi double [ %137, %cond.true163 ], [ %141, %cond.false164 ]
  store double %cond168, double* %max_coeff, align 8, !tbaa !11
  %142 = load double, double* %min_coeff, align 8, !tbaa !11
  %143 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns150, align 4, !tbaa !2
  %x169 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %143, i32 0, i32 2
  %144 = load double*, double** %x169, align 4, !tbaa !25
  %145 = load i32, i32* %i154, align 4, !tbaa !6
  %arrayidx170 = getelementptr inbounds double, double* %144, i32 %145
  %146 = load double, double* %arrayidx170, align 8, !tbaa !11
  %cmp171 = fcmp olt double %142, %146
  br i1 %cmp171, label %cond.true173, label %cond.false174

cond.true173:                                     ; preds = %cond.end167
  %147 = load double, double* %min_coeff, align 8, !tbaa !11
  br label %cond.end177

cond.false174:                                    ; preds = %cond.end167
  %148 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns150, align 4, !tbaa !2
  %x175 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %148, i32 0, i32 2
  %149 = load double*, double** %x175, align 4, !tbaa !25
  %150 = load i32, i32* %i154, align 4, !tbaa !6
  %arrayidx176 = getelementptr inbounds double, double* %149, i32 %150
  %151 = load double, double* %arrayidx176, align 8, !tbaa !11
  br label %cond.end177

cond.end177:                                      ; preds = %cond.false174, %cond.true173
  %cond178 = phi double [ %147, %cond.true173 ], [ %151, %cond.false174 ]
  store double %cond178, double* %min_coeff, align 8, !tbaa !11
  br label %for.inc179

for.inc179:                                       ; preds = %cond.end177
  %152 = load i32, i32* %i154, align 4, !tbaa !6
  %inc180 = add nsw i32 %152, 1
  store i32 %inc180, i32* %i154, align 4, !tbaa !6
  br label %for.cond155

for.end181:                                       ; preds = %for.cond.cleanup158
  %153 = bitcast %struct.aom_noise_strength_solver_t** %solver to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %153) #7
  %154 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state182 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %154, i32 0, i32 1
  %155 = load i32, i32* %c144, align 4, !tbaa !6
  %arrayidx183 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state182, i32 0, i32 %155
  %strength_solver184 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx183, i32 0, i32 1
  store %struct.aom_noise_strength_solver_t* %strength_solver184, %struct.aom_noise_strength_solver_t** %solver, align 4, !tbaa !2
  %156 = bitcast double* %average_strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %156) #7
  store double 0.000000e+00, double* %average_strength, align 8, !tbaa !11
  %157 = bitcast double* %total_weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %157) #7
  store double 0.000000e+00, double* %total_weight, align 8, !tbaa !11
  %158 = bitcast i32* %i185 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %158) #7
  store i32 0, i32* %i185, align 4, !tbaa !6
  br label %for.cond186

for.cond186:                                      ; preds = %for.inc216, %for.end181
  %159 = load i32, i32* %i185, align 4, !tbaa !6
  %160 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver, align 4, !tbaa !2
  %eqns187 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %160, i32 0, i32 0
  %n188 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns187, i32 0, i32 3
  %161 = load i32, i32* %n188, align 4, !tbaa !69
  %cmp189 = icmp slt i32 %159, %161
  br i1 %cmp189, label %for.body192, label %for.cond.cleanup191

for.cond.cleanup191:                              ; preds = %for.cond186
  store i32 20, i32* %cleanup.dest.slot, align 4
  %162 = bitcast i32* %i185 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #7
  br label %for.end218

for.body192:                                      ; preds = %for.cond186
  %163 = bitcast double* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %163) #7
  store double 0.000000e+00, double* %w, align 8, !tbaa !11
  %164 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %164) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond193

for.cond193:                                      ; preds = %for.inc207, %for.body192
  %165 = load i32, i32* %j, align 4, !tbaa !6
  %166 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver, align 4, !tbaa !2
  %eqns194 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %166, i32 0, i32 0
  %n195 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns194, i32 0, i32 3
  %167 = load i32, i32* %n195, align 4, !tbaa !69
  %cmp196 = icmp slt i32 %165, %167
  br i1 %cmp196, label %for.body199, label %for.cond.cleanup198

for.cond.cleanup198:                              ; preds = %for.cond193
  store i32 23, i32* %cleanup.dest.slot, align 4
  %168 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #7
  br label %for.end209

for.body199:                                      ; preds = %for.cond193
  %169 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver, align 4, !tbaa !2
  %eqns200 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %169, i32 0, i32 0
  %A = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns200, i32 0, i32 0
  %170 = load double*, double** %A, align 8, !tbaa !16
  %171 = load i32, i32* %i185, align 4, !tbaa !6
  %172 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver, align 4, !tbaa !2
  %eqns201 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %172, i32 0, i32 0
  %n202 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns201, i32 0, i32 3
  %173 = load i32, i32* %n202, align 4, !tbaa !69
  %mul203 = mul nsw i32 %171, %173
  %174 = load i32, i32* %j, align 4, !tbaa !6
  %add204 = add nsw i32 %mul203, %174
  %arrayidx205 = getelementptr inbounds double, double* %170, i32 %add204
  %175 = load double, double* %arrayidx205, align 8, !tbaa !11
  %176 = load double, double* %w, align 8, !tbaa !11
  %add206 = fadd double %176, %175
  store double %add206, double* %w, align 8, !tbaa !11
  br label %for.inc207

for.inc207:                                       ; preds = %for.body199
  %177 = load i32, i32* %j, align 4, !tbaa !6
  %inc208 = add nsw i32 %177, 1
  store i32 %inc208, i32* %j, align 4, !tbaa !6
  br label %for.cond193

for.end209:                                       ; preds = %for.cond.cleanup198
  %178 = load double, double* %w, align 8, !tbaa !11
  %179 = call double @llvm.sqrt.f64(double %178)
  store double %179, double* %w, align 8, !tbaa !11
  %180 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver, align 4, !tbaa !2
  %eqns210 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %180, i32 0, i32 0
  %x211 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns210, i32 0, i32 2
  %181 = load double*, double** %x211, align 8, !tbaa !26
  %182 = load i32, i32* %i185, align 4, !tbaa !6
  %arrayidx212 = getelementptr inbounds double, double* %181, i32 %182
  %183 = load double, double* %arrayidx212, align 8, !tbaa !11
  %184 = load double, double* %w, align 8, !tbaa !11
  %mul213 = fmul double %183, %184
  %185 = load double, double* %average_strength, align 8, !tbaa !11
  %add214 = fadd double %185, %mul213
  store double %add214, double* %average_strength, align 8, !tbaa !11
  %186 = load double, double* %w, align 8, !tbaa !11
  %187 = load double, double* %total_weight, align 8, !tbaa !11
  %add215 = fadd double %187, %186
  store double %add215, double* %total_weight, align 8, !tbaa !11
  %188 = bitcast double* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %188) #7
  br label %for.inc216

for.inc216:                                       ; preds = %for.end209
  %189 = load i32, i32* %i185, align 4, !tbaa !6
  %inc217 = add nsw i32 %189, 1
  store i32 %inc217, i32* %i185, align 4, !tbaa !6
  br label %for.cond186

for.end218:                                       ; preds = %for.cond.cleanup191
  %190 = load double, double* %total_weight, align 8, !tbaa !11
  %cmp219 = fcmp oeq double %190, 0.000000e+00
  br i1 %cmp219, label %if.then221, label %if.else

if.then221:                                       ; preds = %for.end218
  store double 1.000000e+00, double* %average_strength, align 8, !tbaa !11
  br label %if.end223

if.else:                                          ; preds = %for.end218
  %191 = load double, double* %total_weight, align 8, !tbaa !11
  %192 = load double, double* %average_strength, align 8, !tbaa !11
  %div222 = fdiv double %192, %191
  store double %div222, double* %average_strength, align 8, !tbaa !11
  br label %if.end223

if.end223:                                        ; preds = %if.else, %if.then221
  %193 = load i32, i32* %c144, align 4, !tbaa !6
  %cmp224 = icmp eq i32 %193, 0
  br i1 %cmp224, label %if.then226, label %if.else227

if.then226:                                       ; preds = %if.end223
  %194 = load double, double* %average_strength, align 8, !tbaa !11
  store double %194, double* %avg_luma_strength, align 8, !tbaa !11
  br label %if.end254

if.else227:                                       ; preds = %if.end223
  %195 = load double, double* %avg_luma_strength, align 8, !tbaa !11
  %196 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns150, align 4, !tbaa !2
  %x228 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %196, i32 0, i32 2
  %197 = load double*, double** %x228, align 4, !tbaa !25
  %198 = load i32, i32* %n_coeff, align 4, !tbaa !6
  %arrayidx229 = getelementptr inbounds double, double* %197, i32 %198
  %199 = load double, double* %arrayidx229, align 8, !tbaa !11
  %mul230 = fmul double %195, %199
  %200 = load double, double* %average_strength, align 8, !tbaa !11
  %div231 = fdiv double %mul230, %200
  %201 = load i32, i32* %c144, align 4, !tbaa !6
  %sub232 = sub nsw i32 %201, 1
  %arrayidx233 = getelementptr inbounds [2 x double], [2 x double]* %y_corr, i32 0, i32 %sub232
  store double %div231, double* %arrayidx233, align 8, !tbaa !11
  %202 = load double, double* %max_coeff, align 8, !tbaa !11
  %203 = load i32, i32* %c144, align 4, !tbaa !6
  %sub234 = sub nsw i32 %203, 1
  %arrayidx235 = getelementptr inbounds [2 x double], [2 x double]* %y_corr, i32 0, i32 %sub234
  %204 = load double, double* %arrayidx235, align 8, !tbaa !11
  %cmp236 = fcmp ogt double %202, %204
  br i1 %cmp236, label %cond.true238, label %cond.false239

cond.true238:                                     ; preds = %if.else227
  %205 = load double, double* %max_coeff, align 8, !tbaa !11
  br label %cond.end242

cond.false239:                                    ; preds = %if.else227
  %206 = load i32, i32* %c144, align 4, !tbaa !6
  %sub240 = sub nsw i32 %206, 1
  %arrayidx241 = getelementptr inbounds [2 x double], [2 x double]* %y_corr, i32 0, i32 %sub240
  %207 = load double, double* %arrayidx241, align 8, !tbaa !11
  br label %cond.end242

cond.end242:                                      ; preds = %cond.false239, %cond.true238
  %cond243 = phi double [ %205, %cond.true238 ], [ %207, %cond.false239 ]
  store double %cond243, double* %max_coeff, align 8, !tbaa !11
  %208 = load double, double* %min_coeff, align 8, !tbaa !11
  %209 = load i32, i32* %c144, align 4, !tbaa !6
  %sub244 = sub nsw i32 %209, 1
  %arrayidx245 = getelementptr inbounds [2 x double], [2 x double]* %y_corr, i32 0, i32 %sub244
  %210 = load double, double* %arrayidx245, align 8, !tbaa !11
  %cmp246 = fcmp olt double %208, %210
  br i1 %cmp246, label %cond.true248, label %cond.false249

cond.true248:                                     ; preds = %cond.end242
  %211 = load double, double* %min_coeff, align 8, !tbaa !11
  br label %cond.end252

cond.false249:                                    ; preds = %cond.end242
  %212 = load i32, i32* %c144, align 4, !tbaa !6
  %sub250 = sub nsw i32 %212, 1
  %arrayidx251 = getelementptr inbounds [2 x double], [2 x double]* %y_corr, i32 0, i32 %sub250
  %213 = load double, double* %arrayidx251, align 8, !tbaa !11
  br label %cond.end252

cond.end252:                                      ; preds = %cond.false249, %cond.true248
  %cond253 = phi double [ %211, %cond.true248 ], [ %213, %cond.false249 ]
  store double %cond253, double* %min_coeff, align 8, !tbaa !11
  br label %if.end254

if.end254:                                        ; preds = %cond.end252, %if.then226
  %214 = bitcast double* %total_weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %214) #7
  %215 = bitcast double* %average_strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %215) #7
  %216 = bitcast %struct.aom_noise_strength_solver_t** %solver to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #7
  %217 = bitcast %struct.aom_equation_system_t** %eqns150 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #7
  br label %for.inc255

for.inc255:                                       ; preds = %if.end254
  %218 = load i32, i32* %c144, align 4, !tbaa !6
  %inc256 = add nsw i32 %218, 1
  store i32 %inc256, i32* %c144, align 4, !tbaa !6
  br label %for.cond145

for.end257:                                       ; preds = %for.cond.cleanup148
  %219 = load double, double* %max_coeff, align 8, !tbaa !11
  %220 = call double @llvm.log2.f64(double %219)
  %221 = call double @llvm.floor.f64(double %220)
  %add258 = fadd double 1.000000e+00, %221
  %222 = load double, double* %min_coeff, align 8, !tbaa !11
  %fneg = fneg double %222
  %223 = call double @llvm.log2.f64(double %fneg)
  %224 = call double @llvm.ceil.f64(double %223)
  %cmp259 = fcmp ogt double %add258, %224
  br i1 %cmp259, label %cond.true261, label %cond.false263

cond.true261:                                     ; preds = %for.end257
  %225 = load double, double* %max_coeff, align 8, !tbaa !11
  %226 = call double @llvm.log2.f64(double %225)
  %227 = call double @llvm.floor.f64(double %226)
  %add262 = fadd double 1.000000e+00, %227
  br label %cond.end265

cond.false263:                                    ; preds = %for.end257
  %228 = load double, double* %min_coeff, align 8, !tbaa !11
  %fneg264 = fneg double %228
  %229 = call double @llvm.log2.f64(double %fneg264)
  %230 = call double @llvm.ceil.f64(double %229)
  br label %cond.end265

cond.end265:                                      ; preds = %cond.false263, %cond.true261
  %cond266 = phi double [ %add262, %cond.true261 ], [ %230, %cond.false263 ]
  %conv267 = fptosi double %cond266 to i32
  %sub268 = sub nsw i32 7, %conv267
  %call269 = call i32 @clamp(i32 %sub268, i32 6, i32 9)
  %231 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %ar_coeff_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %231, i32 0, i32 13
  store i32 %call269, i32* %ar_coeff_shift, align 4, !tbaa !70
  %232 = bitcast double* %scale_ar_coeff to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %232) #7
  %233 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %ar_coeff_shift270 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %233, i32 0, i32 13
  %234 = load i32, i32* %ar_coeff_shift270, align 4, !tbaa !70
  %shl271 = shl i32 1, %234
  %conv272 = sitofp i32 %shl271 to double
  store double %conv272, double* %scale_ar_coeff, align 8, !tbaa !11
  %235 = bitcast [3 x i32*]* %ar_coeffs to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %235) #7
  %arrayinit.begin273 = getelementptr inbounds [3 x i32*], [3 x i32*]* %ar_coeffs, i32 0, i32 0
  %236 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %ar_coeffs_y = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %236, i32 0, i32 10
  %arraydecay274 = getelementptr inbounds [24 x i32], [24 x i32]* %ar_coeffs_y, i32 0, i32 0
  store i32* %arraydecay274, i32** %arrayinit.begin273, align 4, !tbaa !2
  %arrayinit.element275 = getelementptr inbounds i32*, i32** %arrayinit.begin273, i32 1
  %237 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %ar_coeffs_cb = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %237, i32 0, i32 11
  %arraydecay276 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cb, i32 0, i32 0
  store i32* %arraydecay276, i32** %arrayinit.element275, align 4, !tbaa !2
  %arrayinit.element277 = getelementptr inbounds i32*, i32** %arrayinit.element275, i32 1
  %238 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %ar_coeffs_cr = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %238, i32 0, i32 12
  %arraydecay278 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cr, i32 0, i32 0
  store i32* %arraydecay278, i32** %arrayinit.element277, align 4, !tbaa !2
  %239 = bitcast i32* %c279 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %239) #7
  store i32 0, i32* %c279, align 4, !tbaa !6
  br label %for.cond280

for.cond280:                                      ; preds = %for.inc316, %cond.end265
  %240 = load i32, i32* %c279, align 4, !tbaa !6
  %cmp281 = icmp slt i32 %240, 3
  br i1 %cmp281, label %for.body284, label %for.cond.cleanup283

for.cond.cleanup283:                              ; preds = %for.cond280
  store i32 26, i32* %cleanup.dest.slot, align 4
  %241 = bitcast i32* %c279 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #7
  br label %for.end318

for.body284:                                      ; preds = %for.cond280
  %242 = bitcast %struct.aom_equation_system_t** %eqns285 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %242) #7
  %243 = load %struct.aom_noise_model_t*, %struct.aom_noise_model_t** %noise_model.addr, align 4, !tbaa !2
  %combined_state286 = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %243, i32 0, i32 1
  %244 = load i32, i32* %c279, align 4, !tbaa !6
  %arrayidx287 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state286, i32 0, i32 %244
  %eqns288 = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx287, i32 0, i32 0
  store %struct.aom_equation_system_t* %eqns288, %struct.aom_equation_system_t** %eqns285, align 4, !tbaa !2
  %245 = bitcast i32* %i289 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %245) #7
  store i32 0, i32* %i289, align 4, !tbaa !6
  br label %for.cond290

for.cond290:                                      ; preds = %for.inc302, %for.body284
  %246 = load i32, i32* %i289, align 4, !tbaa !6
  %247 = load i32, i32* %n_coeff, align 4, !tbaa !6
  %cmp291 = icmp slt i32 %246, %247
  br i1 %cmp291, label %for.body294, label %for.cond.cleanup293

for.cond.cleanup293:                              ; preds = %for.cond290
  store i32 29, i32* %cleanup.dest.slot, align 4
  %248 = bitcast i32* %i289 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #7
  br label %for.end304

for.body294:                                      ; preds = %for.cond290
  %249 = load double, double* %scale_ar_coeff, align 8, !tbaa !11
  %250 = load %struct.aom_equation_system_t*, %struct.aom_equation_system_t** %eqns285, align 4, !tbaa !2
  %x295 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %250, i32 0, i32 2
  %251 = load double*, double** %x295, align 4, !tbaa !25
  %252 = load i32, i32* %i289, align 4, !tbaa !6
  %arrayidx296 = getelementptr inbounds double, double* %251, i32 %252
  %253 = load double, double* %arrayidx296, align 8, !tbaa !11
  %mul297 = fmul double %249, %253
  %254 = call double @llvm.round.f64(double %mul297)
  %conv298 = fptosi double %254 to i32
  %call299 = call i32 @clamp(i32 %conv298, i32 -128, i32 127)
  %255 = load i32, i32* %c279, align 4, !tbaa !6
  %arrayidx300 = getelementptr inbounds [3 x i32*], [3 x i32*]* %ar_coeffs, i32 0, i32 %255
  %256 = load i32*, i32** %arrayidx300, align 4, !tbaa !2
  %257 = load i32, i32* %i289, align 4, !tbaa !6
  %arrayidx301 = getelementptr inbounds i32, i32* %256, i32 %257
  store i32 %call299, i32* %arrayidx301, align 4, !tbaa !6
  br label %for.inc302

for.inc302:                                       ; preds = %for.body294
  %258 = load i32, i32* %i289, align 4, !tbaa !6
  %inc303 = add nsw i32 %258, 1
  store i32 %inc303, i32* %i289, align 4, !tbaa !6
  br label %for.cond290

for.end304:                                       ; preds = %for.cond.cleanup293
  %259 = load i32, i32* %c279, align 4, !tbaa !6
  %cmp305 = icmp sgt i32 %259, 0
  br i1 %cmp305, label %if.then307, label %if.end315

if.then307:                                       ; preds = %for.end304
  %260 = load double, double* %scale_ar_coeff, align 8, !tbaa !11
  %261 = load i32, i32* %c279, align 4, !tbaa !6
  %sub308 = sub nsw i32 %261, 1
  %arrayidx309 = getelementptr inbounds [2 x double], [2 x double]* %y_corr, i32 0, i32 %sub308
  %262 = load double, double* %arrayidx309, align 8, !tbaa !11
  %mul310 = fmul double %260, %262
  %263 = call double @llvm.round.f64(double %mul310)
  %conv311 = fptosi double %263 to i32
  %call312 = call i32 @clamp(i32 %conv311, i32 -128, i32 127)
  %264 = load i32, i32* %c279, align 4, !tbaa !6
  %arrayidx313 = getelementptr inbounds [3 x i32*], [3 x i32*]* %ar_coeffs, i32 0, i32 %264
  %265 = load i32*, i32** %arrayidx313, align 4, !tbaa !2
  %266 = load i32, i32* %n_coeff, align 4, !tbaa !6
  %arrayidx314 = getelementptr inbounds i32, i32* %265, i32 %266
  store i32 %call312, i32* %arrayidx314, align 4, !tbaa !6
  br label %if.end315

if.end315:                                        ; preds = %if.then307, %for.end304
  %267 = bitcast %struct.aom_equation_system_t** %eqns285 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #7
  br label %for.inc316

for.inc316:                                       ; preds = %if.end315
  %268 = load i32, i32* %c279, align 4, !tbaa !6
  %inc317 = add nsw i32 %268, 1
  store i32 %inc317, i32* %c279, align 4, !tbaa !6
  br label %for.cond280

for.end318:                                       ; preds = %for.cond.cleanup283
  %269 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %cb_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %269, i32 0, i32 14
  store i32 128, i32* %cb_mult, align 4, !tbaa !71
  %270 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %cb_luma_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %270, i32 0, i32 15
  store i32 192, i32* %cb_luma_mult, align 4, !tbaa !72
  %271 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %cb_offset = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %271, i32 0, i32 16
  store i32 256, i32* %cb_offset, align 4, !tbaa !73
  %272 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %cr_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %272, i32 0, i32 17
  store i32 128, i32* %cr_mult, align 4, !tbaa !74
  %273 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %cr_luma_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %273, i32 0, i32 18
  store i32 192, i32* %cr_luma_mult, align 4, !tbaa !75
  %274 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %cr_offset = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %274, i32 0, i32 19
  store i32 256, i32* %cr_offset, align 4, !tbaa !76
  %275 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %275, i32 0, i32 23
  store i32 0, i32* %chroma_scaling_from_luma, align 4, !tbaa !77
  %276 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %grain_scale_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %276, i32 0, i32 24
  store i32 0, i32* %grain_scale_shift, align 4, !tbaa !78
  %277 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %overlap_flag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %277, i32 0, i32 20
  store i32 1, i32* %overlap_flag, align 4, !tbaa !79
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %278 = bitcast [3 x i32*]* %ar_coeffs to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %278) #7
  %279 = bitcast double* %scale_ar_coeff to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %279) #7
  %280 = bitcast double* %avg_luma_strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %280) #7
  %281 = bitcast [2 x double]* %y_corr to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %281) #7
  %282 = bitcast double* %min_coeff to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %282) #7
  %283 = bitcast double* %max_coeff to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %283) #7
  %284 = bitcast i32* %n_coeff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #7
  %285 = bitcast [3 x [2 x i32]*]* %film_grain_scaling to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %285) #7
  %286 = bitcast double* %scale_factor to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %286) #7
  %287 = bitcast i32* %max_scaling_value_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #7
  %288 = bitcast double* %max_scaling_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %288) #7
  %289 = bitcast double* %strength_divisor to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %289) #7
  %290 = bitcast [3 x %struct.aom_noise_strength_lut_t]* %scaling_points to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %290) #7
  %291 = bitcast i16* %random_seed to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %291) #7
  br label %return

return:                                           ; preds = %for.end318, %if.then
  %292 = load i32, i32* %retval, align 4
  ret i32 %292
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.log2.f64(double) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.ceil.f64(double) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.round.f64(double) #4

; Function Attrs: nounwind
define hidden i32 @aom_wiener_denoise_2d(i8** %data, i8** %denoised, i32 %w, i32 %h, i32* %stride, i32* %chroma_sub, float** %noise_psd, i32 %block_size, i32 %bit_depth, i32 %use_highbd) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca i8**, align 4
  %denoised.addr = alloca i8**, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32*, align 4
  %chroma_sub.addr = alloca i32*, align 4
  %noise_psd.addr = alloca float**, align 4
  %block_size.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %use_highbd.addr = alloca i32, align 4
  %plane = alloca float*, align 4
  %block = alloca float*, align 4
  %window_full = alloca float*, align 4
  %window_chroma = alloca float*, align 4
  %block_d = alloca double*, align 4
  %plane_d = alloca double*, align 4
  %tx_full = alloca %struct.aom_noise_tx_t*, align 4
  %tx_chroma = alloca %struct.aom_noise_tx_t*, align 4
  %num_blocks_w = alloca i32, align 4
  %num_blocks_h = alloca i32, align 4
  %result_stride = alloca i32, align 4
  %result_height = alloca i32, align 4
  %result = alloca float*, align 4
  %init_success = alloca i32, align 4
  %block_finder_full = alloca %struct.aom_flat_block_finder_t, align 8
  %block_finder_chroma = alloca %struct.aom_flat_block_finder_t, align 8
  %kBlockNormalization = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %c = alloca i32, align 4
  %window_function = alloca float*, align 4
  %block_finder = alloca %struct.aom_flat_block_finder_t*, align 4
  %chroma_sub_h = alloca i32, align 4
  %chroma_sub_w = alloca i32, align 4
  %tx = alloca %struct.aom_noise_tx_t*, align 4
  %offsy = alloca i32, align 4
  %offsx = alloca i32, align 4
  %by = alloca i32, align 4
  %bx = alloca i32, align 4
  %pixels_per_block = alloca i32, align 4
  %j = alloca i32, align 4
  %y = alloca i32, align 4
  %y_result = alloca i32, align 4
  %x = alloca i32, align 4
  %x_result = alloca i32, align 4
  store i8** %data, i8*** %data.addr, align 4, !tbaa !2
  store i8** %denoised, i8*** %denoised.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32* %stride, i32** %stride.addr, align 4, !tbaa !2
  store i32* %chroma_sub, i32** %chroma_sub.addr, align 4, !tbaa !2
  store float** %noise_psd, float*** %noise_psd.addr, align 4, !tbaa !2
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %use_highbd, i32* %use_highbd.addr, align 4, !tbaa !6
  %0 = bitcast float** %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store float* null, float** %plane, align 4, !tbaa !2
  %1 = bitcast float** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store float* null, float** %block, align 4, !tbaa !2
  %2 = bitcast float** %window_full to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store float* null, float** %window_full, align 4, !tbaa !2
  %3 = bitcast float** %window_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store float* null, float** %window_chroma, align 4, !tbaa !2
  %4 = bitcast double** %block_d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store double* null, double** %block_d, align 4, !tbaa !2
  %5 = bitcast double** %plane_d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  store double* null, double** %plane_d, align 4, !tbaa !2
  %6 = bitcast %struct.aom_noise_tx_t** %tx_full to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  store %struct.aom_noise_tx_t* null, %struct.aom_noise_tx_t** %tx_full, align 4, !tbaa !2
  %7 = bitcast %struct.aom_noise_tx_t** %tx_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store %struct.aom_noise_tx_t* null, %struct.aom_noise_tx_t** %tx_chroma, align 4, !tbaa !2
  %8 = bitcast i32* %num_blocks_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load i32, i32* %w.addr, align 4, !tbaa !6
  %10 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %add = add nsw i32 %9, %10
  %sub = sub nsw i32 %add, 1
  %11 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %div = sdiv i32 %sub, %11
  store i32 %div, i32* %num_blocks_w, align 4, !tbaa !6
  %12 = bitcast i32* %num_blocks_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load i32, i32* %h.addr, align 4, !tbaa !6
  %14 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %add1 = add nsw i32 %13, %14
  %sub2 = sub nsw i32 %add1, 1
  %15 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %div3 = sdiv i32 %sub2, %15
  store i32 %div3, i32* %num_blocks_h, align 4, !tbaa !6
  %16 = bitcast i32* %result_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %add4 = add nsw i32 %17, 2
  %18 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add4, %18
  store i32 %mul, i32* %result_stride, align 4, !tbaa !6
  %19 = bitcast i32* %result_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %add5 = add nsw i32 %20, 2
  %21 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 %add5, %21
  store i32 %mul6, i32* %result_height, align 4, !tbaa !6
  %22 = bitcast float** %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  store float* null, float** %result, align 4, !tbaa !2
  %23 = bitcast i32* %init_success to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  store i32 1, i32* %init_success, align 4, !tbaa !6
  %24 = bitcast %struct.aom_flat_block_finder_t* %block_finder_full to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %24) #7
  %25 = bitcast %struct.aom_flat_block_finder_t* %block_finder_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %25) #7
  %26 = bitcast float* %kBlockNormalization to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %shl = shl i32 1, %27
  %sub7 = sub nsw i32 %shl, 1
  %conv = sitofp i32 %sub7 to float
  store float %conv, float* %kBlockNormalization, align 4, !tbaa !36
  %28 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %28, i32 0
  %29 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %30 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %30, i32 1
  %31 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %cmp = icmp ne i32 %29, %31
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %32 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %32, i8* getelementptr inbounds ([66 x i8], [66 x i8]* @.str.18, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup245

if.end:                                           ; preds = %entry
  %33 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %34 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %35 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %call10 = call i32 @aom_flat_block_finder_init(%struct.aom_flat_block_finder_t* %block_finder_full, i32 %33, i32 %34, i32 %35)
  %36 = load i32, i32* %init_success, align 4, !tbaa !6
  %and = and i32 %36, %call10
  store i32 %and, i32* %init_success, align 4, !tbaa !6
  %37 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %add11 = add nsw i32 %37, 2
  %38 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul12 = mul nsw i32 %add11, %38
  %39 = load i32, i32* %result_stride, align 4, !tbaa !6
  %mul13 = mul nsw i32 %mul12, %39
  %mul14 = mul i32 %mul13, 4
  %call15 = call i8* @aom_malloc(i32 %mul14)
  %40 = bitcast i8* %call15 to float*
  store float* %40, float** %result, align 4, !tbaa !2
  %41 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %42 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul16 = mul nsw i32 %41, %42
  %mul17 = mul i32 %mul16, 4
  %call18 = call i8* @aom_malloc(i32 %mul17)
  %43 = bitcast i8* %call18 to float*
  store float* %43, float** %plane, align 4, !tbaa !2
  %44 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul19 = mul nsw i32 2, %44
  %45 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 %mul19, %45
  %mul21 = mul i32 %mul20, 4
  %call22 = call i8* @aom_memalign(i32 32, i32 %mul21)
  %46 = bitcast i8* %call22 to float*
  store float* %46, float** %block, align 4, !tbaa !2
  %47 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %48 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 %47, %48
  %mul24 = mul i32 %mul23, 8
  %call25 = call i8* @aom_malloc(i32 %mul24)
  %49 = bitcast i8* %call25 to double*
  store double* %49, double** %block_d, align 4, !tbaa !2
  %50 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %51 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %50, %51
  %mul27 = mul i32 %mul26, 8
  %call28 = call i8* @aom_malloc(i32 %mul27)
  %52 = bitcast i8* %call28 to double*
  store double* %52, double** %plane_d, align 4, !tbaa !2
  %53 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %call29 = call float* @get_half_cos_window(i32 %53)
  store float* %call29, float** %window_full, align 4, !tbaa !2
  %54 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %call30 = call %struct.aom_noise_tx_t* @aom_noise_tx_malloc(i32 %54)
  store %struct.aom_noise_tx_t* %call30, %struct.aom_noise_tx_t** %tx_full, align 4, !tbaa !2
  %55 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i32, i32* %55, i32 0
  %56 = load i32, i32* %arrayidx31, align 4, !tbaa !6
  %cmp32 = icmp ne i32 %56, 0
  br i1 %cmp32, label %if.then34, label %if.else

if.then34:                                        ; preds = %if.end
  %57 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %58 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %58, i32 0
  %59 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %shr = ashr i32 %57, %59
  %60 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %61 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %call36 = call i32 @aom_flat_block_finder_init(%struct.aom_flat_block_finder_t* %block_finder_chroma, i32 %shr, i32 %60, i32 %61)
  %62 = load i32, i32* %init_success, align 4, !tbaa !6
  %and37 = and i32 %62, %call36
  store i32 %and37, i32* %init_success, align 4, !tbaa !6
  %63 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %64 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i32, i32* %64, i32 0
  %65 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %shr39 = ashr i32 %63, %65
  %call40 = call float* @get_half_cos_window(i32 %shr39)
  store float* %call40, float** %window_chroma, align 4, !tbaa !2
  %66 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %67 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %67, i32 0
  %68 = load i32, i32* %arrayidx41, align 4, !tbaa !6
  %shr42 = ashr i32 %66, %68
  %call43 = call %struct.aom_noise_tx_t* @aom_noise_tx_malloc(i32 %shr42)
  store %struct.aom_noise_tx_t* %call43, %struct.aom_noise_tx_t** %tx_chroma, align 4, !tbaa !2
  br label %if.end44

if.else:                                          ; preds = %if.end
  %69 = load float*, float** %window_full, align 4, !tbaa !2
  store float* %69, float** %window_chroma, align 4, !tbaa !2
  %70 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx_full, align 4, !tbaa !2
  store %struct.aom_noise_tx_t* %70, %struct.aom_noise_tx_t** %tx_chroma, align 4, !tbaa !2
  br label %if.end44

if.end44:                                         ; preds = %if.else, %if.then34
  %71 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx_full, align 4, !tbaa !2
  %cmp45 = icmp ne %struct.aom_noise_tx_t* %71, null
  br i1 %cmp45, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %if.end44
  %72 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx_chroma, align 4, !tbaa !2
  %cmp47 = icmp ne %struct.aom_noise_tx_t* %72, null
  br i1 %cmp47, label %land.lhs.true49, label %land.end

land.lhs.true49:                                  ; preds = %land.lhs.true
  %73 = load float*, float** %plane, align 4, !tbaa !2
  %cmp50 = icmp ne float* %73, null
  br i1 %cmp50, label %land.lhs.true52, label %land.end

land.lhs.true52:                                  ; preds = %land.lhs.true49
  %74 = load double*, double** %plane_d, align 4, !tbaa !2
  %cmp53 = icmp ne double* %74, null
  br i1 %cmp53, label %land.lhs.true55, label %land.end

land.lhs.true55:                                  ; preds = %land.lhs.true52
  %75 = load float*, float** %block, align 4, !tbaa !2
  %cmp56 = icmp ne float* %75, null
  br i1 %cmp56, label %land.lhs.true58, label %land.end

land.lhs.true58:                                  ; preds = %land.lhs.true55
  %76 = load double*, double** %block_d, align 4, !tbaa !2
  %cmp59 = icmp ne double* %76, null
  br i1 %cmp59, label %land.lhs.true61, label %land.end

land.lhs.true61:                                  ; preds = %land.lhs.true58
  %77 = load float*, float** %window_full, align 4, !tbaa !2
  %cmp62 = icmp ne float* %77, null
  br i1 %cmp62, label %land.lhs.true64, label %land.end

land.lhs.true64:                                  ; preds = %land.lhs.true61
  %78 = load float*, float** %window_chroma, align 4, !tbaa !2
  %cmp65 = icmp ne float* %78, null
  br i1 %cmp65, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true64
  %79 = load float*, float** %result, align 4, !tbaa !2
  %cmp67 = icmp ne float* %79, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true64, %land.lhs.true61, %land.lhs.true58, %land.lhs.true55, %land.lhs.true52, %land.lhs.true49, %land.lhs.true, %if.end44
  %80 = phi i1 [ false, %land.lhs.true64 ], [ false, %land.lhs.true61 ], [ false, %land.lhs.true58 ], [ false, %land.lhs.true55 ], [ false, %land.lhs.true52 ], [ false, %land.lhs.true49 ], [ false, %land.lhs.true ], [ false, %if.end44 ], [ %cmp67, %land.rhs ]
  %land.ext = zext i1 %80 to i32
  %81 = load i32, i32* %init_success, align 4, !tbaa !6
  %and69 = and i32 %81, %land.ext
  store i32 %and69, i32* %init_success, align 4, !tbaa !6
  %82 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #7
  %83 = load i32, i32* %init_success, align 4, !tbaa !6
  %tobool = icmp ne i32 %83, 0
  %84 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 0, i32 3
  store i32 %cond, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc236, %land.end
  %85 = load i32, i32* %c, align 4, !tbaa !6
  %cmp70 = icmp slt i32 %85, 3
  br i1 %cmp70, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %86 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #7
  br label %for.end239

for.body:                                         ; preds = %for.cond
  %87 = bitcast float** %window_function to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #7
  %88 = load i32, i32* %c, align 4, !tbaa !6
  %cmp72 = icmp eq i32 %88, 0
  br i1 %cmp72, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %89 = load float*, float** %window_full, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %90 = load float*, float** %window_chroma, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond74 = phi float* [ %89, %cond.true ], [ %90, %cond.false ]
  store float* %cond74, float** %window_function, align 4, !tbaa !2
  %91 = bitcast %struct.aom_flat_block_finder_t** %block_finder to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #7
  store %struct.aom_flat_block_finder_t* %block_finder_full, %struct.aom_flat_block_finder_t** %block_finder, align 4, !tbaa !2
  %92 = bitcast i32* %chroma_sub_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #7
  %93 = load i32, i32* %c, align 4, !tbaa !6
  %cmp75 = icmp sgt i32 %93, 0
  br i1 %cmp75, label %cond.true77, label %cond.false79

cond.true77:                                      ; preds = %cond.end
  %94 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds i32, i32* %94, i32 1
  %95 = load i32, i32* %arrayidx78, align 4, !tbaa !6
  br label %cond.end80

cond.false79:                                     ; preds = %cond.end
  br label %cond.end80

cond.end80:                                       ; preds = %cond.false79, %cond.true77
  %cond81 = phi i32 [ %95, %cond.true77 ], [ 0, %cond.false79 ]
  store i32 %cond81, i32* %chroma_sub_h, align 4, !tbaa !6
  %96 = bitcast i32* %chroma_sub_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #7
  %97 = load i32, i32* %c, align 4, !tbaa !6
  %cmp82 = icmp sgt i32 %97, 0
  br i1 %cmp82, label %cond.true84, label %cond.false86

cond.true84:                                      ; preds = %cond.end80
  %98 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %98, i32 0
  %99 = load i32, i32* %arrayidx85, align 4, !tbaa !6
  br label %cond.end87

cond.false86:                                     ; preds = %cond.end80
  br label %cond.end87

cond.end87:                                       ; preds = %cond.false86, %cond.true84
  %cond88 = phi i32 [ %99, %cond.true84 ], [ 0, %cond.false86 ]
  store i32 %cond88, i32* %chroma_sub_w, align 4, !tbaa !6
  %100 = bitcast %struct.aom_noise_tx_t** %tx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #7
  %101 = load i32, i32* %c, align 4, !tbaa !6
  %cmp89 = icmp sgt i32 %101, 0
  br i1 %cmp89, label %land.lhs.true91, label %cond.false96

land.lhs.true91:                                  ; preds = %cond.end87
  %102 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i32, i32* %102, i32 0
  %103 = load i32, i32* %arrayidx92, align 4, !tbaa !6
  %cmp93 = icmp sgt i32 %103, 0
  br i1 %cmp93, label %cond.true95, label %cond.false96

cond.true95:                                      ; preds = %land.lhs.true91
  %104 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx_chroma, align 4, !tbaa !2
  br label %cond.end97

cond.false96:                                     ; preds = %land.lhs.true91, %cond.end87
  %105 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx_full, align 4, !tbaa !2
  br label %cond.end97

cond.end97:                                       ; preds = %cond.false96, %cond.true95
  %cond98 = phi %struct.aom_noise_tx_t* [ %104, %cond.true95 ], [ %105, %cond.false96 ]
  store %struct.aom_noise_tx_t* %cond98, %struct.aom_noise_tx_t** %tx, align 4, !tbaa !2
  %106 = load i8**, i8*** %data.addr, align 4, !tbaa !2
  %107 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx99 = getelementptr inbounds i8*, i8** %106, i32 %107
  %108 = load i8*, i8** %arrayidx99, align 4, !tbaa !2
  %tobool100 = icmp ne i8* %108, null
  br i1 %tobool100, label %lor.lhs.false, label %if.then103

lor.lhs.false:                                    ; preds = %cond.end97
  %109 = load i8**, i8*** %denoised.addr, align 4, !tbaa !2
  %110 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx101 = getelementptr inbounds i8*, i8** %109, i32 %110
  %111 = load i8*, i8** %arrayidx101, align 4, !tbaa !2
  %tobool102 = icmp ne i8* %111, null
  br i1 %tobool102, label %if.end104, label %if.then103

if.then103:                                       ; preds = %lor.lhs.false, %cond.end97
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end104:                                        ; preds = %lor.lhs.false
  %112 = load i32, i32* %c, align 4, !tbaa !6
  %cmp105 = icmp sgt i32 %112, 0
  br i1 %cmp105, label %land.lhs.true107, label %if.end112

land.lhs.true107:                                 ; preds = %if.end104
  %113 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %113, i32 0
  %114 = load i32, i32* %arrayidx108, align 4, !tbaa !6
  %cmp109 = icmp ne i32 %114, 0
  br i1 %cmp109, label %if.then111, label %if.end112

if.then111:                                       ; preds = %land.lhs.true107
  store %struct.aom_flat_block_finder_t* %block_finder_chroma, %struct.aom_flat_block_finder_t** %block_finder, align 4, !tbaa !2
  br label %if.end112

if.end112:                                        ; preds = %if.then111, %land.lhs.true107, %if.end104
  %115 = load float*, float** %result, align 4, !tbaa !2
  %116 = bitcast float* %115 to i8*
  %117 = load i32, i32* %result_stride, align 4, !tbaa !6
  %mul113 = mul i32 4, %117
  %118 = load i32, i32* %result_height, align 4, !tbaa !6
  %mul114 = mul i32 %mul113, %118
  call void @llvm.memset.p0i8.i32(i8* align 4 %116, i8 0, i32 %mul114, i1 false)
  %119 = bitcast i32* %offsy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #7
  store i32 0, i32* %offsy, align 4, !tbaa !6
  br label %for.cond115

for.cond115:                                      ; preds = %for.inc219, %if.end112
  %120 = load i32, i32* %offsy, align 4, !tbaa !6
  %121 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %122 = load i32, i32* %chroma_sub_h, align 4, !tbaa !6
  %shr116 = ashr i32 %121, %122
  %cmp117 = icmp slt i32 %120, %shr116
  br i1 %cmp117, label %for.body120, label %for.cond.cleanup119

for.cond.cleanup119:                              ; preds = %for.cond115
  store i32 5, i32* %cleanup.dest.slot, align 4
  %123 = bitcast i32* %offsy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #7
  br label %for.end223

for.body120:                                      ; preds = %for.cond115
  %124 = bitcast i32* %offsx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #7
  store i32 0, i32* %offsx, align 4, !tbaa !6
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc214, %for.body120
  %125 = load i32, i32* %offsx, align 4, !tbaa !6
  %126 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %127 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr122 = ashr i32 %126, %127
  %cmp123 = icmp slt i32 %125, %shr122
  br i1 %cmp123, label %for.body126, label %for.cond.cleanup125

for.cond.cleanup125:                              ; preds = %for.cond121
  store i32 8, i32* %cleanup.dest.slot, align 4
  %128 = bitcast i32* %offsx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #7
  br label %for.end218

for.body126:                                      ; preds = %for.cond121
  %129 = bitcast i32* %by to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #7
  store i32 -1, i32* %by, align 4, !tbaa !6
  br label %for.cond127

for.cond127:                                      ; preds = %for.inc211, %for.body126
  %130 = load i32, i32* %by, align 4, !tbaa !6
  %131 = load i32, i32* %num_blocks_h, align 4, !tbaa !6
  %cmp128 = icmp slt i32 %130, %131
  br i1 %cmp128, label %for.body131, label %for.cond.cleanup130

for.cond.cleanup130:                              ; preds = %for.cond127
  store i32 11, i32* %cleanup.dest.slot, align 4
  %132 = bitcast i32* %by to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #7
  br label %for.end213

for.body131:                                      ; preds = %for.cond127
  %133 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %133) #7
  store i32 -1, i32* %bx, align 4, !tbaa !6
  br label %for.cond132

for.cond132:                                      ; preds = %for.inc208, %for.body131
  %134 = load i32, i32* %bx, align 4, !tbaa !6
  %135 = load i32, i32* %num_blocks_w, align 4, !tbaa !6
  %cmp133 = icmp slt i32 %134, %135
  br i1 %cmp133, label %for.body136, label %for.cond.cleanup135

for.cond.cleanup135:                              ; preds = %for.cond132
  store i32 14, i32* %cleanup.dest.slot, align 4
  %136 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #7
  br label %for.end210

for.body136:                                      ; preds = %for.cond132
  %137 = bitcast i32* %pixels_per_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #7
  %138 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %139 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr137 = ashr i32 %138, %139
  %140 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %141 = load i32, i32* %chroma_sub_h, align 4, !tbaa !6
  %shr138 = ashr i32 %140, %141
  %mul139 = mul nsw i32 %shr137, %shr138
  store i32 %mul139, i32* %pixels_per_block, align 4, !tbaa !6
  %142 = load %struct.aom_flat_block_finder_t*, %struct.aom_flat_block_finder_t** %block_finder, align 4, !tbaa !2
  %143 = load i8**, i8*** %data.addr, align 4, !tbaa !2
  %144 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx140 = getelementptr inbounds i8*, i8** %143, i32 %144
  %145 = load i8*, i8** %arrayidx140, align 4, !tbaa !2
  %146 = load i32, i32* %w.addr, align 4, !tbaa !6
  %147 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr141 = ashr i32 %146, %147
  %148 = load i32, i32* %h.addr, align 4, !tbaa !6
  %149 = load i32, i32* %chroma_sub_h, align 4, !tbaa !6
  %shr142 = ashr i32 %148, %149
  %150 = load i32*, i32** %stride.addr, align 4, !tbaa !2
  %151 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx143 = getelementptr inbounds i32, i32* %150, i32 %151
  %152 = load i32, i32* %arrayidx143, align 4, !tbaa !6
  %153 = load i32, i32* %bx, align 4, !tbaa !6
  %154 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %155 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr144 = ashr i32 %154, %155
  %mul145 = mul nsw i32 %153, %shr144
  %156 = load i32, i32* %offsx, align 4, !tbaa !6
  %add146 = add nsw i32 %mul145, %156
  %157 = load i32, i32* %by, align 4, !tbaa !6
  %158 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %159 = load i32, i32* %chroma_sub_h, align 4, !tbaa !6
  %shr147 = ashr i32 %158, %159
  %mul148 = mul nsw i32 %157, %shr147
  %160 = load i32, i32* %offsy, align 4, !tbaa !6
  %add149 = add nsw i32 %mul148, %160
  %161 = load double*, double** %plane_d, align 4, !tbaa !2
  %162 = load double*, double** %block_d, align 4, !tbaa !2
  call void @aom_flat_block_finder_extract_block(%struct.aom_flat_block_finder_t* %142, i8* %145, i32 %shr141, i32 %shr142, i32 %152, i32 %add146, i32 %add149, double* %161, double* %162)
  %163 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %163) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond150

for.cond150:                                      ; preds = %for.inc, %for.body136
  %164 = load i32, i32* %j, align 4, !tbaa !6
  %165 = load i32, i32* %pixels_per_block, align 4, !tbaa !6
  %cmp151 = icmp slt i32 %164, %165
  br i1 %cmp151, label %for.body154, label %for.cond.cleanup153

for.cond.cleanup153:                              ; preds = %for.cond150
  store i32 17, i32* %cleanup.dest.slot, align 4
  %166 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #7
  br label %for.end

for.body154:                                      ; preds = %for.cond150
  %167 = load double*, double** %block_d, align 4, !tbaa !2
  %168 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx155 = getelementptr inbounds double, double* %167, i32 %168
  %169 = load double, double* %arrayidx155, align 8, !tbaa !11
  %conv156 = fptrunc double %169 to float
  %170 = load float*, float** %block, align 4, !tbaa !2
  %171 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx157 = getelementptr inbounds float, float* %170, i32 %171
  store float %conv156, float* %arrayidx157, align 4, !tbaa !36
  %172 = load double*, double** %plane_d, align 4, !tbaa !2
  %173 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx158 = getelementptr inbounds double, double* %172, i32 %173
  %174 = load double, double* %arrayidx158, align 8, !tbaa !11
  %conv159 = fptrunc double %174 to float
  %175 = load float*, float** %plane, align 4, !tbaa !2
  %176 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx160 = getelementptr inbounds float, float* %175, i32 %176
  store float %conv159, float* %arrayidx160, align 4, !tbaa !36
  br label %for.inc

for.inc:                                          ; preds = %for.body154
  %177 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %177, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond150

for.end:                                          ; preds = %for.cond.cleanup153
  %178 = load float*, float** %window_function, align 4, !tbaa !2
  %179 = load float*, float** %block, align 4, !tbaa !2
  %180 = load i32, i32* %pixels_per_block, align 4, !tbaa !6
  call void @pointwise_multiply(float* %178, float* %179, i32 %180)
  %181 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx, align 4, !tbaa !2
  %182 = load float*, float** %block, align 4, !tbaa !2
  call void @aom_noise_tx_forward(%struct.aom_noise_tx_t* %181, float* %182)
  %183 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx, align 4, !tbaa !2
  %184 = load float**, float*** %noise_psd.addr, align 4, !tbaa !2
  %185 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx161 = getelementptr inbounds float*, float** %184, i32 %185
  %186 = load float*, float** %arrayidx161, align 4, !tbaa !2
  call void @aom_noise_tx_filter(%struct.aom_noise_tx_t* %183, float* %186)
  %187 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx, align 4, !tbaa !2
  %188 = load float*, float** %block, align 4, !tbaa !2
  call void @aom_noise_tx_inverse(%struct.aom_noise_tx_t* %187, float* %188)
  %189 = load float*, float** %window_function, align 4, !tbaa !2
  %190 = load float*, float** %plane, align 4, !tbaa !2
  %191 = load i32, i32* %pixels_per_block, align 4, !tbaa !6
  call void @pointwise_multiply(float* %189, float* %190, i32 %191)
  %192 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %192) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond162

for.cond162:                                      ; preds = %for.inc205, %for.end
  %193 = load i32, i32* %y, align 4, !tbaa !6
  %194 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %195 = load i32, i32* %chroma_sub_h, align 4, !tbaa !6
  %shr163 = ashr i32 %194, %195
  %cmp164 = icmp slt i32 %193, %shr163
  br i1 %cmp164, label %for.body167, label %for.cond.cleanup166

for.cond.cleanup166:                              ; preds = %for.cond162
  store i32 20, i32* %cleanup.dest.slot, align 4
  %196 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #7
  br label %for.end207

for.body167:                                      ; preds = %for.cond162
  %197 = bitcast i32* %y_result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %197) #7
  %198 = load i32, i32* %y, align 4, !tbaa !6
  %199 = load i32, i32* %by, align 4, !tbaa !6
  %add168 = add nsw i32 %199, 1
  %200 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %201 = load i32, i32* %chroma_sub_h, align 4, !tbaa !6
  %shr169 = ashr i32 %200, %201
  %mul170 = mul nsw i32 %add168, %shr169
  %add171 = add nsw i32 %198, %mul170
  %202 = load i32, i32* %offsy, align 4, !tbaa !6
  %add172 = add nsw i32 %add171, %202
  store i32 %add172, i32* %y_result, align 4, !tbaa !6
  %203 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %203) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond173

for.cond173:                                      ; preds = %for.inc202, %for.body167
  %204 = load i32, i32* %x, align 4, !tbaa !6
  %205 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %206 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr174 = ashr i32 %205, %206
  %cmp175 = icmp slt i32 %204, %shr174
  br i1 %cmp175, label %for.body178, label %for.cond.cleanup177

for.cond.cleanup177:                              ; preds = %for.cond173
  store i32 23, i32* %cleanup.dest.slot, align 4
  %207 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #7
  br label %for.end204

for.body178:                                      ; preds = %for.cond173
  %208 = bitcast i32* %x_result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %208) #7
  %209 = load i32, i32* %x, align 4, !tbaa !6
  %210 = load i32, i32* %bx, align 4, !tbaa !6
  %add179 = add nsw i32 %210, 1
  %211 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %212 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr180 = ashr i32 %211, %212
  %mul181 = mul nsw i32 %add179, %shr180
  %add182 = add nsw i32 %209, %mul181
  %213 = load i32, i32* %offsx, align 4, !tbaa !6
  %add183 = add nsw i32 %add182, %213
  store i32 %add183, i32* %x_result, align 4, !tbaa !6
  %214 = load float*, float** %block, align 4, !tbaa !2
  %215 = load i32, i32* %y, align 4, !tbaa !6
  %216 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %217 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr184 = ashr i32 %216, %217
  %mul185 = mul nsw i32 %215, %shr184
  %218 = load i32, i32* %x, align 4, !tbaa !6
  %add186 = add nsw i32 %mul185, %218
  %arrayidx187 = getelementptr inbounds float, float* %214, i32 %add186
  %219 = load float, float* %arrayidx187, align 4, !tbaa !36
  %220 = load float*, float** %plane, align 4, !tbaa !2
  %221 = load i32, i32* %y, align 4, !tbaa !6
  %222 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %223 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr188 = ashr i32 %222, %223
  %mul189 = mul nsw i32 %221, %shr188
  %224 = load i32, i32* %x, align 4, !tbaa !6
  %add190 = add nsw i32 %mul189, %224
  %arrayidx191 = getelementptr inbounds float, float* %220, i32 %add190
  %225 = load float, float* %arrayidx191, align 4, !tbaa !36
  %add192 = fadd float %219, %225
  %226 = load float*, float** %window_function, align 4, !tbaa !2
  %227 = load i32, i32* %y, align 4, !tbaa !6
  %228 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %229 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr193 = ashr i32 %228, %229
  %mul194 = mul nsw i32 %227, %shr193
  %230 = load i32, i32* %x, align 4, !tbaa !6
  %add195 = add nsw i32 %mul194, %230
  %arrayidx196 = getelementptr inbounds float, float* %226, i32 %add195
  %231 = load float, float* %arrayidx196, align 4, !tbaa !36
  %mul197 = fmul float %add192, %231
  %232 = load float*, float** %result, align 4, !tbaa !2
  %233 = load i32, i32* %y_result, align 4, !tbaa !6
  %234 = load i32, i32* %result_stride, align 4, !tbaa !6
  %mul198 = mul nsw i32 %233, %234
  %235 = load i32, i32* %x_result, align 4, !tbaa !6
  %add199 = add nsw i32 %mul198, %235
  %arrayidx200 = getelementptr inbounds float, float* %232, i32 %add199
  %236 = load float, float* %arrayidx200, align 4, !tbaa !36
  %add201 = fadd float %236, %mul197
  store float %add201, float* %arrayidx200, align 4, !tbaa !36
  %237 = bitcast i32* %x_result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %237) #7
  br label %for.inc202

for.inc202:                                       ; preds = %for.body178
  %238 = load i32, i32* %x, align 4, !tbaa !6
  %inc203 = add nsw i32 %238, 1
  store i32 %inc203, i32* %x, align 4, !tbaa !6
  br label %for.cond173

for.end204:                                       ; preds = %for.cond.cleanup177
  %239 = bitcast i32* %y_result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #7
  br label %for.inc205

for.inc205:                                       ; preds = %for.end204
  %240 = load i32, i32* %y, align 4, !tbaa !6
  %inc206 = add nsw i32 %240, 1
  store i32 %inc206, i32* %y, align 4, !tbaa !6
  br label %for.cond162

for.end207:                                       ; preds = %for.cond.cleanup166
  %241 = bitcast i32* %pixels_per_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #7
  br label %for.inc208

for.inc208:                                       ; preds = %for.end207
  %242 = load i32, i32* %bx, align 4, !tbaa !6
  %inc209 = add nsw i32 %242, 1
  store i32 %inc209, i32* %bx, align 4, !tbaa !6
  br label %for.cond132

for.end210:                                       ; preds = %for.cond.cleanup135
  br label %for.inc211

for.inc211:                                       ; preds = %for.end210
  %243 = load i32, i32* %by, align 4, !tbaa !6
  %inc212 = add nsw i32 %243, 1
  store i32 %inc212, i32* %by, align 4, !tbaa !6
  br label %for.cond127

for.end213:                                       ; preds = %for.cond.cleanup130
  br label %for.inc214

for.inc214:                                       ; preds = %for.end213
  %244 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %245 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %shr215 = ashr i32 %244, %245
  %div216 = sdiv i32 %shr215, 2
  %246 = load i32, i32* %offsx, align 4, !tbaa !6
  %add217 = add nsw i32 %246, %div216
  store i32 %add217, i32* %offsx, align 4, !tbaa !6
  br label %for.cond121

for.end218:                                       ; preds = %for.cond.cleanup125
  br label %for.inc219

for.inc219:                                       ; preds = %for.end218
  %247 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %248 = load i32, i32* %chroma_sub_h, align 4, !tbaa !6
  %shr220 = ashr i32 %247, %248
  %div221 = sdiv i32 %shr220, 2
  %249 = load i32, i32* %offsy, align 4, !tbaa !6
  %add222 = add nsw i32 %249, %div221
  store i32 %add222, i32* %offsy, align 4, !tbaa !6
  br label %for.cond115

for.end223:                                       ; preds = %for.cond.cleanup119
  %250 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool224 = icmp ne i32 %250, 0
  br i1 %tobool224, label %if.then225, label %if.else228

if.then225:                                       ; preds = %for.end223
  %251 = load float*, float** %result, align 4, !tbaa !2
  %252 = load i32, i32* %result_stride, align 4, !tbaa !6
  %253 = load i8**, i8*** %denoised.addr, align 4, !tbaa !2
  %254 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx226 = getelementptr inbounds i8*, i8** %253, i32 %254
  %255 = load i8*, i8** %arrayidx226, align 4, !tbaa !2
  %256 = bitcast i8* %255 to i16*
  %257 = load i32, i32* %w.addr, align 4, !tbaa !6
  %258 = load i32, i32* %h.addr, align 4, !tbaa !6
  %259 = load i32*, i32** %stride.addr, align 4, !tbaa !2
  %260 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx227 = getelementptr inbounds i32, i32* %259, i32 %260
  %261 = load i32, i32* %arrayidx227, align 4, !tbaa !6
  %262 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %263 = load i32, i32* %chroma_sub_h, align 4, !tbaa !6
  %264 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %265 = load float, float* %kBlockNormalization, align 4, !tbaa !36
  call void @dither_and_quantize_highbd(float* %251, i32 %252, i16* %256, i32 %257, i32 %258, i32 %261, i32 %262, i32 %263, i32 %264, float %265)
  br label %if.end231

if.else228:                                       ; preds = %for.end223
  %266 = load float*, float** %result, align 4, !tbaa !2
  %267 = load i32, i32* %result_stride, align 4, !tbaa !6
  %268 = load i8**, i8*** %denoised.addr, align 4, !tbaa !2
  %269 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx229 = getelementptr inbounds i8*, i8** %268, i32 %269
  %270 = load i8*, i8** %arrayidx229, align 4, !tbaa !2
  %271 = load i32, i32* %w.addr, align 4, !tbaa !6
  %272 = load i32, i32* %h.addr, align 4, !tbaa !6
  %273 = load i32*, i32** %stride.addr, align 4, !tbaa !2
  %274 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx230 = getelementptr inbounds i32, i32* %273, i32 %274
  %275 = load i32, i32* %arrayidx230, align 4, !tbaa !6
  %276 = load i32, i32* %chroma_sub_w, align 4, !tbaa !6
  %277 = load i32, i32* %chroma_sub_h, align 4, !tbaa !6
  %278 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %279 = load float, float* %kBlockNormalization, align 4, !tbaa !36
  call void @dither_and_quantize_lowbd(float* %266, i32 %267, i8* %270, i32 %271, i32 %272, i32 %275, i32 %276, i32 %277, i32 %278, float %279)
  br label %if.end231

if.end231:                                        ; preds = %if.else228, %if.then225
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end231, %if.then103
  %280 = bitcast %struct.aom_noise_tx_t** %tx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #7
  %281 = bitcast i32* %chroma_sub_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #7
  %282 = bitcast i32* %chroma_sub_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #7
  %283 = bitcast %struct.aom_flat_block_finder_t** %block_finder to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %283) #7
  %284 = bitcast float** %window_function to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc236
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc236

for.inc236:                                       ; preds = %cleanup.cont, %cleanup
  %285 = load i32, i32* %c, align 4, !tbaa !6
  %inc237 = add nsw i32 %285, 1
  store i32 %inc237, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.end239:                                       ; preds = %for.cond.cleanup
  %286 = load float*, float** %result, align 4, !tbaa !2
  %287 = bitcast float* %286 to i8*
  call void @aom_free(i8* %287)
  %288 = load float*, float** %plane, align 4, !tbaa !2
  %289 = bitcast float* %288 to i8*
  call void @aom_free(i8* %289)
  %290 = load float*, float** %block, align 4, !tbaa !2
  %291 = bitcast float* %290 to i8*
  call void @aom_free(i8* %291)
  %292 = load double*, double** %plane_d, align 4, !tbaa !2
  %293 = bitcast double* %292 to i8*
  call void @aom_free(i8* %293)
  %294 = load double*, double** %block_d, align 4, !tbaa !2
  %295 = bitcast double* %294 to i8*
  call void @aom_free(i8* %295)
  %296 = load float*, float** %window_full, align 4, !tbaa !2
  %297 = bitcast float* %296 to i8*
  call void @aom_free(i8* %297)
  %298 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx_full, align 4, !tbaa !2
  call void @aom_noise_tx_free(%struct.aom_noise_tx_t* %298)
  call void @aom_flat_block_finder_free(%struct.aom_flat_block_finder_t* %block_finder_full)
  %299 = load i32*, i32** %chroma_sub.addr, align 4, !tbaa !2
  %arrayidx240 = getelementptr inbounds i32, i32* %299, i32 0
  %300 = load i32, i32* %arrayidx240, align 4, !tbaa !6
  %cmp241 = icmp ne i32 %300, 0
  br i1 %cmp241, label %if.then243, label %if.end244

if.then243:                                       ; preds = %for.end239
  call void @aom_flat_block_finder_free(%struct.aom_flat_block_finder_t* %block_finder_chroma)
  %301 = load float*, float** %window_chroma, align 4, !tbaa !2
  %302 = bitcast float* %301 to i8*
  call void @aom_free(i8* %302)
  %303 = load %struct.aom_noise_tx_t*, %struct.aom_noise_tx_t** %tx_chroma, align 4, !tbaa !2
  call void @aom_noise_tx_free(%struct.aom_noise_tx_t* %303)
  br label %if.end244

if.end244:                                        ; preds = %if.then243, %for.end239
  %304 = load i32, i32* %init_success, align 4, !tbaa !6
  store i32 %304, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup245

cleanup245:                                       ; preds = %if.end244, %if.then
  %305 = bitcast float* %kBlockNormalization to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %305) #7
  %306 = bitcast %struct.aom_flat_block_finder_t* %block_finder_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %306) #7
  %307 = bitcast %struct.aom_flat_block_finder_t* %block_finder_full to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %307) #7
  %308 = bitcast i32* %init_success to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %308) #7
  %309 = bitcast float** %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %309) #7
  %310 = bitcast i32* %result_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %310) #7
  %311 = bitcast i32* %result_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %311) #7
  %312 = bitcast i32* %num_blocks_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %312) #7
  %313 = bitcast i32* %num_blocks_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #7
  %314 = bitcast %struct.aom_noise_tx_t** %tx_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #7
  %315 = bitcast %struct.aom_noise_tx_t** %tx_full to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #7
  %316 = bitcast double** %plane_d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #7
  %317 = bitcast double** %block_d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #7
  %318 = bitcast float** %window_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %318) #7
  %319 = bitcast float** %window_full to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #7
  %320 = bitcast float** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #7
  %321 = bitcast float** %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %321) #7
  %322 = load i32, i32* %retval, align 4
  ret i32 %322

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare i8* @aom_memalign(i32, i32) #1

; Function Attrs: nounwind
define internal float* @get_half_cos_window(i32 %block_size) #0 {
entry:
  %block_size.addr = alloca i32, align 4
  %window_function = alloca float*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %cos_yd = alloca double, align 8
  %x = alloca i32, align 4
  %cos_xd = alloca double, align 8
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  %0 = bitcast float** %window_function to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %2 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, %2
  %mul1 = mul i32 %mul, 4
  %call = call i8* @aom_malloc(i32 %mul1)
  %3 = bitcast i8* %call to float*
  store float* %3, float** %window_function, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc19, %entry
  %5 = load i32, i32* %y, align 4, !tbaa !6
  %6 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end21

for.body:                                         ; preds = %for.cond
  %8 = bitcast double* %cos_yd to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #7
  %9 = load i32, i32* %y, align 4, !tbaa !6
  %conv = sitofp i32 %9 to double
  %add = fadd double 5.000000e-01, %conv
  %mul2 = fmul double %add, 0x400921FB54442D18
  %10 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %conv3 = sitofp i32 %10 to double
  %div = fdiv double %mul2, %conv3
  %sub = fsub double %div, 0x3FF921FB54442D18
  %11 = call double @llvm.cos.f64(double %sub)
  store double %11, double* %cos_yd, align 8, !tbaa !11
  %12 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %13 = load i32, i32* %x, align 4, !tbaa !6
  %14 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %13, %14
  br i1 %cmp5, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond4
  store i32 5, i32* %cleanup.dest.slot, align 4
  %15 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  br label %for.end

for.body8:                                        ; preds = %for.cond4
  %16 = bitcast double* %cos_xd to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %16) #7
  %17 = load i32, i32* %x, align 4, !tbaa !6
  %conv9 = sitofp i32 %17 to double
  %add10 = fadd double 5.000000e-01, %conv9
  %mul11 = fmul double %add10, 0x400921FB54442D18
  %18 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %conv12 = sitofp i32 %18 to double
  %div13 = fdiv double %mul11, %conv12
  %sub14 = fsub double %div13, 0x3FF921FB54442D18
  %19 = call double @llvm.cos.f64(double %sub14)
  store double %19, double* %cos_xd, align 8, !tbaa !11
  %20 = load double, double* %cos_yd, align 8, !tbaa !11
  %21 = load double, double* %cos_xd, align 8, !tbaa !11
  %mul15 = fmul double %20, %21
  %conv16 = fptrunc double %mul15 to float
  %22 = load float*, float** %window_function, align 4, !tbaa !2
  %23 = load i32, i32* %y, align 4, !tbaa !6
  %24 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 %23, %24
  %25 = load i32, i32* %x, align 4, !tbaa !6
  %add18 = add nsw i32 %mul17, %25
  %arrayidx = getelementptr inbounds float, float* %22, i32 %add18
  store float %conv16, float* %arrayidx, align 4, !tbaa !36
  %26 = bitcast double* %cos_xd to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %26) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %27 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond4

for.end:                                          ; preds = %for.cond.cleanup7
  %28 = bitcast double* %cos_yd to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %28) #7
  br label %for.inc19

for.inc19:                                        ; preds = %for.end
  %29 = load i32, i32* %y, align 4, !tbaa !6
  %inc20 = add nsw i32 %29, 1
  store i32 %inc20, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end21:                                        ; preds = %for.cond.cleanup
  %30 = load float*, float** %window_function, align 4, !tbaa !2
  store i32 1, i32* %cleanup.dest.slot, align 4
  %31 = bitcast float** %window_function to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  ret float* %30
}

declare %struct.aom_noise_tx_t* @aom_noise_tx_malloc(i32) #1

; Function Attrs: nounwind
define internal void @pointwise_multiply(float* %a, float* %b, i32 %n) #0 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %a.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = load float, float* %arrayidx, align 4, !tbaa !36
  %7 = load float*, float** %b.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx1, align 4, !tbaa !36
  %mul = fmul float %9, %6
  store float %mul, float* %arrayidx1, align 4, !tbaa !36
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @aom_noise_tx_forward(%struct.aom_noise_tx_t*, float*) #1

declare void @aom_noise_tx_filter(%struct.aom_noise_tx_t*, float*) #1

declare void @aom_noise_tx_inverse(%struct.aom_noise_tx_t*, float*) #1

; Function Attrs: nounwind
define internal void @dither_and_quantize_highbd(float* %result, i32 %result_stride, i16* %denoised, i32 %w, i32 %h, i32 %stride, i32 %chroma_sub_w, i32 %chroma_sub_h, i32 %block_size, float %block_normalization) #0 {
entry:
  %result.addr = alloca float*, align 4
  %result_stride.addr = alloca i32, align 4
  %denoised.addr = alloca i16*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %chroma_sub_w.addr = alloca i32, align 4
  %chroma_sub_h.addr = alloca i32, align 4
  %block_size.addr = alloca i32, align 4
  %block_normalization.addr = alloca float, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %result_idx = alloca i32, align 4
  %new_val = alloca i16, align 2
  %err = alloca float, align 4
  store float* %result, float** %result.addr, align 4, !tbaa !2
  store i32 %result_stride, i32* %result_stride.addr, align 4, !tbaa !6
  store i16* %denoised, i16** %denoised.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %chroma_sub_w, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  store i32 %chroma_sub_h, i32* %chroma_sub_h.addr, align 4, !tbaa !6
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  store float %block_normalization, float* %block_normalization.addr, align 4, !tbaa !36
  %0 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc79, %entry
  %1 = load i32, i32* %y, align 4, !tbaa !6
  %2 = load i32, i32* %h.addr, align 4, !tbaa !6
  %3 = load i32, i32* %chroma_sub_h.addr, align 4, !tbaa !6
  %shr = ashr i32 %2, %3
  %cmp = icmp slt i32 %1, %shr
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  br label %for.end81

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %x, align 4, !tbaa !6
  %7 = load i32, i32* %w.addr, align 4, !tbaa !6
  %8 = load i32, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  %shr2 = ashr i32 %7, %8
  %cmp3 = icmp slt i32 %6, %shr2
  br i1 %cmp3, label %for.body5, label %for.cond.cleanup4

for.cond.cleanup4:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %9 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  br label %for.end

for.body5:                                        ; preds = %for.cond1
  %10 = bitcast i32* %result_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load i32, i32* %y, align 4, !tbaa !6
  %12 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %13 = load i32, i32* %chroma_sub_h.addr, align 4, !tbaa !6
  %shr6 = ashr i32 %12, %13
  %add = add nsw i32 %11, %shr6
  %14 = load i32, i32* %result_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add, %14
  %15 = load i32, i32* %x, align 4, !tbaa !6
  %add7 = add nsw i32 %mul, %15
  %16 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %17 = load i32, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  %shr8 = ashr i32 %16, %17
  %add9 = add nsw i32 %add7, %shr8
  store i32 %add9, i32* %result_idx, align 4, !tbaa !6
  %18 = bitcast i16* %new_val to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %18) #7
  %19 = load float*, float** %result.addr, align 4, !tbaa !2
  %20 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %19, i32 %20
  %21 = load float, float* %arrayidx, align 4, !tbaa !36
  %22 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %mul10 = fmul float %21, %22
  %add11 = fadd float %mul10, 5.000000e-01
  %cmp12 = fcmp ogt float %add11, 0.000000e+00
  br i1 %cmp12, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body5
  %23 = load float*, float** %result.addr, align 4, !tbaa !2
  %24 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds float, float* %23, i32 %24
  %25 = load float, float* %arrayidx13, align 4, !tbaa !36
  %26 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %mul14 = fmul float %25, %26
  %add15 = fadd float %mul14, 5.000000e-01
  br label %cond.end

cond.false:                                       ; preds = %for.body5
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %add15, %cond.true ], [ 0.000000e+00, %cond.false ]
  %27 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %cmp16 = fcmp olt float %cond, %27
  br i1 %cmp16, label %cond.true17, label %cond.false29

cond.true17:                                      ; preds = %cond.end
  %28 = load float*, float** %result.addr, align 4, !tbaa !2
  %29 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds float, float* %28, i32 %29
  %30 = load float, float* %arrayidx18, align 4, !tbaa !36
  %31 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %mul19 = fmul float %30, %31
  %add20 = fadd float %mul19, 5.000000e-01
  %cmp21 = fcmp ogt float %add20, 0.000000e+00
  br i1 %cmp21, label %cond.true22, label %cond.false26

cond.true22:                                      ; preds = %cond.true17
  %32 = load float*, float** %result.addr, align 4, !tbaa !2
  %33 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds float, float* %32, i32 %33
  %34 = load float, float* %arrayidx23, align 4, !tbaa !36
  %35 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %mul24 = fmul float %34, %35
  %add25 = fadd float %mul24, 5.000000e-01
  br label %cond.end27

cond.false26:                                     ; preds = %cond.true17
  br label %cond.end27

cond.end27:                                       ; preds = %cond.false26, %cond.true22
  %cond28 = phi float [ %add25, %cond.true22 ], [ 0.000000e+00, %cond.false26 ]
  br label %cond.end30

cond.false29:                                     ; preds = %cond.end
  %36 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  br label %cond.end30

cond.end30:                                       ; preds = %cond.false29, %cond.end27
  %cond31 = phi float [ %cond28, %cond.end27 ], [ %36, %cond.false29 ]
  %conv = fptoui float %cond31 to i16
  store i16 %conv, i16* %new_val, align 2, !tbaa !33
  %37 = bitcast float* %err to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #7
  %38 = load i16, i16* %new_val, align 2, !tbaa !33
  %conv32 = uitofp i16 %38 to float
  %39 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %div = fdiv float %conv32, %39
  %40 = load float*, float** %result.addr, align 4, !tbaa !2
  %41 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds float, float* %40, i32 %41
  %42 = load float, float* %arrayidx33, align 4, !tbaa !36
  %sub = fsub float %div, %42
  %fneg = fneg float %sub
  store float %fneg, float* %err, align 4, !tbaa !36
  %43 = load i16, i16* %new_val, align 2, !tbaa !33
  %44 = load i16*, i16** %denoised.addr, align 4, !tbaa !2
  %45 = load i32, i32* %y, align 4, !tbaa !6
  %46 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul34 = mul nsw i32 %45, %46
  %47 = load i32, i32* %x, align 4, !tbaa !6
  %add35 = add nsw i32 %mul34, %47
  %arrayidx36 = getelementptr inbounds i16, i16* %44, i32 %add35
  store i16 %43, i16* %arrayidx36, align 2, !tbaa !33
  %48 = load i32, i32* %x, align 4, !tbaa !6
  %add37 = add nsw i32 %48, 1
  %49 = load i32, i32* %w.addr, align 4, !tbaa !6
  %50 = load i32, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  %shr38 = ashr i32 %49, %50
  %cmp39 = icmp slt i32 %add37, %shr38
  br i1 %cmp39, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end30
  %51 = load float, float* %err, align 4, !tbaa !36
  %mul41 = fmul float %51, 7.000000e+00
  %div42 = fdiv float %mul41, 1.600000e+01
  %52 = load float*, float** %result.addr, align 4, !tbaa !2
  %53 = load i32, i32* %result_idx, align 4, !tbaa !6
  %add43 = add nsw i32 %53, 1
  %arrayidx44 = getelementptr inbounds float, float* %52, i32 %add43
  %54 = load float, float* %arrayidx44, align 4, !tbaa !36
  %add45 = fadd float %54, %div42
  store float %add45, float* %arrayidx44, align 4, !tbaa !36
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end30
  %55 = load i32, i32* %y, align 4, !tbaa !6
  %add46 = add nsw i32 %55, 1
  %56 = load i32, i32* %h.addr, align 4, !tbaa !6
  %57 = load i32, i32* %chroma_sub_h.addr, align 4, !tbaa !6
  %shr47 = ashr i32 %56, %57
  %cmp48 = icmp slt i32 %add46, %shr47
  br i1 %cmp48, label %if.then50, label %if.end78

if.then50:                                        ; preds = %if.end
  %58 = load i32, i32* %x, align 4, !tbaa !6
  %cmp51 = icmp sgt i32 %58, 0
  br i1 %cmp51, label %if.then53, label %if.end60

if.then53:                                        ; preds = %if.then50
  %59 = load float, float* %err, align 4, !tbaa !36
  %mul54 = fmul float %59, 3.000000e+00
  %div55 = fdiv float %mul54, 1.600000e+01
  %60 = load float*, float** %result.addr, align 4, !tbaa !2
  %61 = load i32, i32* %result_idx, align 4, !tbaa !6
  %62 = load i32, i32* %result_stride.addr, align 4, !tbaa !6
  %add56 = add nsw i32 %61, %62
  %sub57 = sub nsw i32 %add56, 1
  %arrayidx58 = getelementptr inbounds float, float* %60, i32 %sub57
  %63 = load float, float* %arrayidx58, align 4, !tbaa !36
  %add59 = fadd float %63, %div55
  store float %add59, float* %arrayidx58, align 4, !tbaa !36
  br label %if.end60

if.end60:                                         ; preds = %if.then53, %if.then50
  %64 = load float, float* %err, align 4, !tbaa !36
  %mul61 = fmul float %64, 5.000000e+00
  %div62 = fdiv float %mul61, 1.600000e+01
  %65 = load float*, float** %result.addr, align 4, !tbaa !2
  %66 = load i32, i32* %result_idx, align 4, !tbaa !6
  %67 = load i32, i32* %result_stride.addr, align 4, !tbaa !6
  %add63 = add nsw i32 %66, %67
  %arrayidx64 = getelementptr inbounds float, float* %65, i32 %add63
  %68 = load float, float* %arrayidx64, align 4, !tbaa !36
  %add65 = fadd float %68, %div62
  store float %add65, float* %arrayidx64, align 4, !tbaa !36
  %69 = load i32, i32* %x, align 4, !tbaa !6
  %add66 = add nsw i32 %69, 1
  %70 = load i32, i32* %w.addr, align 4, !tbaa !6
  %71 = load i32, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  %shr67 = ashr i32 %70, %71
  %cmp68 = icmp slt i32 %add66, %shr67
  br i1 %cmp68, label %if.then70, label %if.end77

if.then70:                                        ; preds = %if.end60
  %72 = load float, float* %err, align 4, !tbaa !36
  %mul71 = fmul float %72, 1.000000e+00
  %div72 = fdiv float %mul71, 1.600000e+01
  %73 = load float*, float** %result.addr, align 4, !tbaa !2
  %74 = load i32, i32* %result_idx, align 4, !tbaa !6
  %75 = load i32, i32* %result_stride.addr, align 4, !tbaa !6
  %add73 = add nsw i32 %74, %75
  %add74 = add nsw i32 %add73, 1
  %arrayidx75 = getelementptr inbounds float, float* %73, i32 %add74
  %76 = load float, float* %arrayidx75, align 4, !tbaa !36
  %add76 = fadd float %76, %div72
  store float %add76, float* %arrayidx75, align 4, !tbaa !36
  br label %if.end77

if.end77:                                         ; preds = %if.then70, %if.end60
  br label %if.end78

if.end78:                                         ; preds = %if.end77, %if.end
  %77 = bitcast float* %err to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  %78 = bitcast i16* %new_val to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %78) #7
  %79 = bitcast i32* %result_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #7
  br label %for.inc

for.inc:                                          ; preds = %if.end78
  %80 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %80, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup4
  br label %for.inc79

for.inc79:                                        ; preds = %for.end
  %81 = load i32, i32* %y, align 4, !tbaa !6
  %inc80 = add nsw i32 %81, 1
  store i32 %inc80, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end81:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal void @dither_and_quantize_lowbd(float* %result, i32 %result_stride, i8* %denoised, i32 %w, i32 %h, i32 %stride, i32 %chroma_sub_w, i32 %chroma_sub_h, i32 %block_size, float %block_normalization) #0 {
entry:
  %result.addr = alloca float*, align 4
  %result_stride.addr = alloca i32, align 4
  %denoised.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %chroma_sub_w.addr = alloca i32, align 4
  %chroma_sub_h.addr = alloca i32, align 4
  %block_size.addr = alloca i32, align 4
  %block_normalization.addr = alloca float, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %result_idx = alloca i32, align 4
  %new_val = alloca i8, align 1
  %err = alloca float, align 4
  store float* %result, float** %result.addr, align 4, !tbaa !2
  store i32 %result_stride, i32* %result_stride.addr, align 4, !tbaa !6
  store i8* %denoised, i8** %denoised.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %chroma_sub_w, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  store i32 %chroma_sub_h, i32* %chroma_sub_h.addr, align 4, !tbaa !6
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  store float %block_normalization, float* %block_normalization.addr, align 4, !tbaa !36
  %0 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc79, %entry
  %1 = load i32, i32* %y, align 4, !tbaa !6
  %2 = load i32, i32* %h.addr, align 4, !tbaa !6
  %3 = load i32, i32* %chroma_sub_h.addr, align 4, !tbaa !6
  %shr = ashr i32 %2, %3
  %cmp = icmp slt i32 %1, %shr
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  br label %for.end81

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %x, align 4, !tbaa !6
  %7 = load i32, i32* %w.addr, align 4, !tbaa !6
  %8 = load i32, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  %shr2 = ashr i32 %7, %8
  %cmp3 = icmp slt i32 %6, %shr2
  br i1 %cmp3, label %for.body5, label %for.cond.cleanup4

for.cond.cleanup4:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %9 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  br label %for.end

for.body5:                                        ; preds = %for.cond1
  %10 = bitcast i32* %result_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load i32, i32* %y, align 4, !tbaa !6
  %12 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %13 = load i32, i32* %chroma_sub_h.addr, align 4, !tbaa !6
  %shr6 = ashr i32 %12, %13
  %add = add nsw i32 %11, %shr6
  %14 = load i32, i32* %result_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add, %14
  %15 = load i32, i32* %x, align 4, !tbaa !6
  %add7 = add nsw i32 %mul, %15
  %16 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %17 = load i32, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  %shr8 = ashr i32 %16, %17
  %add9 = add nsw i32 %add7, %shr8
  store i32 %add9, i32* %result_idx, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %new_val) #7
  %18 = load float*, float** %result.addr, align 4, !tbaa !2
  %19 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %18, i32 %19
  %20 = load float, float* %arrayidx, align 4, !tbaa !36
  %21 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %mul10 = fmul float %20, %21
  %add11 = fadd float %mul10, 5.000000e-01
  %cmp12 = fcmp ogt float %add11, 0.000000e+00
  br i1 %cmp12, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body5
  %22 = load float*, float** %result.addr, align 4, !tbaa !2
  %23 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds float, float* %22, i32 %23
  %24 = load float, float* %arrayidx13, align 4, !tbaa !36
  %25 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %mul14 = fmul float %24, %25
  %add15 = fadd float %mul14, 5.000000e-01
  br label %cond.end

cond.false:                                       ; preds = %for.body5
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %add15, %cond.true ], [ 0.000000e+00, %cond.false ]
  %26 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %cmp16 = fcmp olt float %cond, %26
  br i1 %cmp16, label %cond.true17, label %cond.false29

cond.true17:                                      ; preds = %cond.end
  %27 = load float*, float** %result.addr, align 4, !tbaa !2
  %28 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds float, float* %27, i32 %28
  %29 = load float, float* %arrayidx18, align 4, !tbaa !36
  %30 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %mul19 = fmul float %29, %30
  %add20 = fadd float %mul19, 5.000000e-01
  %cmp21 = fcmp ogt float %add20, 0.000000e+00
  br i1 %cmp21, label %cond.true22, label %cond.false26

cond.true22:                                      ; preds = %cond.true17
  %31 = load float*, float** %result.addr, align 4, !tbaa !2
  %32 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds float, float* %31, i32 %32
  %33 = load float, float* %arrayidx23, align 4, !tbaa !36
  %34 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %mul24 = fmul float %33, %34
  %add25 = fadd float %mul24, 5.000000e-01
  br label %cond.end27

cond.false26:                                     ; preds = %cond.true17
  br label %cond.end27

cond.end27:                                       ; preds = %cond.false26, %cond.true22
  %cond28 = phi float [ %add25, %cond.true22 ], [ 0.000000e+00, %cond.false26 ]
  br label %cond.end30

cond.false29:                                     ; preds = %cond.end
  %35 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  br label %cond.end30

cond.end30:                                       ; preds = %cond.false29, %cond.end27
  %cond31 = phi float [ %cond28, %cond.end27 ], [ %35, %cond.false29 ]
  %conv = fptoui float %cond31 to i8
  store i8 %conv, i8* %new_val, align 1, !tbaa !35
  %36 = bitcast float* %err to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  %37 = load i8, i8* %new_val, align 1, !tbaa !35
  %conv32 = uitofp i8 %37 to float
  %38 = load float, float* %block_normalization.addr, align 4, !tbaa !36
  %div = fdiv float %conv32, %38
  %39 = load float*, float** %result.addr, align 4, !tbaa !2
  %40 = load i32, i32* %result_idx, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds float, float* %39, i32 %40
  %41 = load float, float* %arrayidx33, align 4, !tbaa !36
  %sub = fsub float %div, %41
  %fneg = fneg float %sub
  store float %fneg, float* %err, align 4, !tbaa !36
  %42 = load i8, i8* %new_val, align 1, !tbaa !35
  %43 = load i8*, i8** %denoised.addr, align 4, !tbaa !2
  %44 = load i32, i32* %y, align 4, !tbaa !6
  %45 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul34 = mul nsw i32 %44, %45
  %46 = load i32, i32* %x, align 4, !tbaa !6
  %add35 = add nsw i32 %mul34, %46
  %arrayidx36 = getelementptr inbounds i8, i8* %43, i32 %add35
  store i8 %42, i8* %arrayidx36, align 1, !tbaa !35
  %47 = load i32, i32* %x, align 4, !tbaa !6
  %add37 = add nsw i32 %47, 1
  %48 = load i32, i32* %w.addr, align 4, !tbaa !6
  %49 = load i32, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  %shr38 = ashr i32 %48, %49
  %cmp39 = icmp slt i32 %add37, %shr38
  br i1 %cmp39, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end30
  %50 = load float, float* %err, align 4, !tbaa !36
  %mul41 = fmul float %50, 7.000000e+00
  %div42 = fdiv float %mul41, 1.600000e+01
  %51 = load float*, float** %result.addr, align 4, !tbaa !2
  %52 = load i32, i32* %result_idx, align 4, !tbaa !6
  %add43 = add nsw i32 %52, 1
  %arrayidx44 = getelementptr inbounds float, float* %51, i32 %add43
  %53 = load float, float* %arrayidx44, align 4, !tbaa !36
  %add45 = fadd float %53, %div42
  store float %add45, float* %arrayidx44, align 4, !tbaa !36
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end30
  %54 = load i32, i32* %y, align 4, !tbaa !6
  %add46 = add nsw i32 %54, 1
  %55 = load i32, i32* %h.addr, align 4, !tbaa !6
  %56 = load i32, i32* %chroma_sub_h.addr, align 4, !tbaa !6
  %shr47 = ashr i32 %55, %56
  %cmp48 = icmp slt i32 %add46, %shr47
  br i1 %cmp48, label %if.then50, label %if.end78

if.then50:                                        ; preds = %if.end
  %57 = load i32, i32* %x, align 4, !tbaa !6
  %cmp51 = icmp sgt i32 %57, 0
  br i1 %cmp51, label %if.then53, label %if.end60

if.then53:                                        ; preds = %if.then50
  %58 = load float, float* %err, align 4, !tbaa !36
  %mul54 = fmul float %58, 3.000000e+00
  %div55 = fdiv float %mul54, 1.600000e+01
  %59 = load float*, float** %result.addr, align 4, !tbaa !2
  %60 = load i32, i32* %result_idx, align 4, !tbaa !6
  %61 = load i32, i32* %result_stride.addr, align 4, !tbaa !6
  %add56 = add nsw i32 %60, %61
  %sub57 = sub nsw i32 %add56, 1
  %arrayidx58 = getelementptr inbounds float, float* %59, i32 %sub57
  %62 = load float, float* %arrayidx58, align 4, !tbaa !36
  %add59 = fadd float %62, %div55
  store float %add59, float* %arrayidx58, align 4, !tbaa !36
  br label %if.end60

if.end60:                                         ; preds = %if.then53, %if.then50
  %63 = load float, float* %err, align 4, !tbaa !36
  %mul61 = fmul float %63, 5.000000e+00
  %div62 = fdiv float %mul61, 1.600000e+01
  %64 = load float*, float** %result.addr, align 4, !tbaa !2
  %65 = load i32, i32* %result_idx, align 4, !tbaa !6
  %66 = load i32, i32* %result_stride.addr, align 4, !tbaa !6
  %add63 = add nsw i32 %65, %66
  %arrayidx64 = getelementptr inbounds float, float* %64, i32 %add63
  %67 = load float, float* %arrayidx64, align 4, !tbaa !36
  %add65 = fadd float %67, %div62
  store float %add65, float* %arrayidx64, align 4, !tbaa !36
  %68 = load i32, i32* %x, align 4, !tbaa !6
  %add66 = add nsw i32 %68, 1
  %69 = load i32, i32* %w.addr, align 4, !tbaa !6
  %70 = load i32, i32* %chroma_sub_w.addr, align 4, !tbaa !6
  %shr67 = ashr i32 %69, %70
  %cmp68 = icmp slt i32 %add66, %shr67
  br i1 %cmp68, label %if.then70, label %if.end77

if.then70:                                        ; preds = %if.end60
  %71 = load float, float* %err, align 4, !tbaa !36
  %mul71 = fmul float %71, 1.000000e+00
  %div72 = fdiv float %mul71, 1.600000e+01
  %72 = load float*, float** %result.addr, align 4, !tbaa !2
  %73 = load i32, i32* %result_idx, align 4, !tbaa !6
  %74 = load i32, i32* %result_stride.addr, align 4, !tbaa !6
  %add73 = add nsw i32 %73, %74
  %add74 = add nsw i32 %add73, 1
  %arrayidx75 = getelementptr inbounds float, float* %72, i32 %add74
  %75 = load float, float* %arrayidx75, align 4, !tbaa !36
  %add76 = fadd float %75, %div72
  store float %add76, float* %arrayidx75, align 4, !tbaa !36
  br label %if.end77

if.end77:                                         ; preds = %if.then70, %if.end60
  br label %if.end78

if.end78:                                         ; preds = %if.end77, %if.end
  %76 = bitcast float* %err to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %new_val) #7
  %77 = bitcast i32* %result_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  br label %for.inc

for.inc:                                          ; preds = %if.end78
  %78 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %78, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup4
  br label %for.inc79

for.inc79:                                        ; preds = %for.end
  %79 = load i32, i32* %y, align 4, !tbaa !6
  %inc80 = add nsw i32 %79, 1
  store i32 %inc80, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end81:                                        ; preds = %for.cond.cleanup
  ret void
}

declare void @aom_noise_tx_free(%struct.aom_noise_tx_t*) #1

; Function Attrs: nounwind
define hidden %struct.aom_denoise_and_model_t* @aom_denoise_and_model_alloc(i32 %bit_depth, i32 %block_size, float %noise_level) #0 {
entry:
  %retval = alloca %struct.aom_denoise_and_model_t*, align 4
  %bit_depth.addr = alloca i32, align 4
  %block_size.addr = alloca i32, align 4
  %noise_level.addr = alloca float, align 4
  %ctx = alloca %struct.aom_denoise_and_model_t*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  store float %noise_level, float* %noise_level.addr, align 4, !tbaa !36
  %0 = bitcast %struct.aom_denoise_and_model_t** %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i8* @aom_malloc(i32 608)
  %1 = bitcast i8* %call to %struct.aom_denoise_and_model_t*
  store %struct.aom_denoise_and_model_t* %1, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %2 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_denoise_and_model_t* %2, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.19, i32 0, i32 0))
  store %struct.aom_denoise_and_model_t* null, %struct.aom_denoise_and_model_t** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %5 = bitcast %struct.aom_denoise_and_model_t* %4 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %5, i8 0, i32 608, i1 false)
  %6 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %7 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %block_size2 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %7, i32 0, i32 0
  store i32 %6, i32* %block_size2, align 8, !tbaa !80
  %8 = load float, float* %noise_level.addr, align 4, !tbaa !36
  %9 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %noise_level3 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %9, i32 0, i32 2
  store float %8, float* %noise_level3, align 8, !tbaa !82
  %10 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %11 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %bit_depth4 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %11, i32 0, i32 1
  store i32 %10, i32* %bit_depth4, align 4, !tbaa !83
  %12 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul = mul i32 4, %12
  %13 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul5 = mul i32 %mul, %13
  %call6 = call i8* @aom_malloc(i32 %mul5)
  %14 = bitcast i8* %call6 to float*
  %15 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %noise_psd = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %15, i32 0, i32 9
  %arrayidx = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd, i32 0, i32 0
  store float* %14, float** %arrayidx, align 4, !tbaa !2
  %16 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul7 = mul i32 4, %16
  %17 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul8 = mul i32 %mul7, %17
  %call9 = call i8* @aom_malloc(i32 %mul8)
  %18 = bitcast i8* %call9 to float*
  %19 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %noise_psd10 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %19, i32 0, i32 9
  %arrayidx11 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd10, i32 0, i32 1
  store float* %18, float** %arrayidx11, align 4, !tbaa !2
  %20 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul12 = mul i32 4, %20
  %21 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %mul13 = mul i32 %mul12, %21
  %call14 = call i8* @aom_malloc(i32 %mul13)
  %22 = bitcast i8* %call14 to float*
  %23 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %noise_psd15 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %23, i32 0, i32 9
  %arrayidx16 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd15, i32 0, i32 2
  store float* %22, float** %arrayidx16, align 4, !tbaa !2
  %24 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %noise_psd17 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %24, i32 0, i32 9
  %arrayidx18 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd17, i32 0, i32 0
  %25 = load float*, float** %arrayidx18, align 4, !tbaa !2
  %tobool19 = icmp ne float* %25, null
  br i1 %tobool19, label %lor.lhs.false, label %if.then27

lor.lhs.false:                                    ; preds = %if.end
  %26 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %noise_psd20 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %26, i32 0, i32 9
  %arrayidx21 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd20, i32 0, i32 1
  %27 = load float*, float** %arrayidx21, align 4, !tbaa !2
  %tobool22 = icmp ne float* %27, null
  br i1 %tobool22, label %lor.lhs.false23, label %if.then27

lor.lhs.false23:                                  ; preds = %lor.lhs.false
  %28 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  %noise_psd24 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %28, i32 0, i32 9
  %arrayidx25 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd24, i32 0, i32 2
  %29 = load float*, float** %arrayidx25, align 4, !tbaa !2
  %tobool26 = icmp ne float* %29, null
  br i1 %tobool26, label %if.end29, label %if.then27

if.then27:                                        ; preds = %lor.lhs.false23, %lor.lhs.false, %if.end
  %30 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call28 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %30, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.20, i32 0, i32 0))
  %31 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  call void @aom_denoise_and_model_free(%struct.aom_denoise_and_model_t* %31)
  store %struct.aom_denoise_and_model_t* null, %struct.aom_denoise_and_model_t** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end29:                                         ; preds = %lor.lhs.false23
  %32 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx, align 4, !tbaa !2
  store %struct.aom_denoise_and_model_t* %32, %struct.aom_denoise_and_model_t** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end29, %if.then27, %if.then
  %33 = bitcast %struct.aom_denoise_and_model_t** %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %retval, align 4
  ret %struct.aom_denoise_and_model_t* %34
}

; Function Attrs: nounwind
define hidden void @aom_denoise_and_model_free(%struct.aom_denoise_and_model_t* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.aom_denoise_and_model_t*, align 4
  %i = alloca i32, align 4
  store %struct.aom_denoise_and_model_t* %ctx, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %0 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_blocks = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %0, i32 0, i32 11
  %1 = load i8*, i8** %flat_blocks, align 4, !tbaa !84
  call void @aom_free(i8* %1)
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %5, i32 0, i32 10
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised, i32 0, i32 %6
  %7 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  call void @aom_free(i8* %7)
  %8 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_psd = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %8, i32 0, i32 9
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd, i32 0, i32 %9
  %10 = load float*, float** %arrayidx1, align 4, !tbaa !2
  %11 = bitcast float* %10 to i8*
  call void @aom_free(i8* %11)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %13 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_model = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %13, i32 0, i32 13
  call void @aom_noise_model_free(%struct.aom_noise_model_t* %noise_model)
  %14 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_block_finder = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %14, i32 0, i32 12
  call void @aom_flat_block_finder_free(%struct.aom_flat_block_finder_t* %flat_block_finder)
  %15 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %16 = bitcast %struct.aom_denoise_and_model_t* %15 to i8*
  call void @aom_free(i8* %16)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_denoise_and_model_run(%struct.aom_denoise_and_model_t* %ctx, %struct.yv12_buffer_config* %sd, %struct.aom_film_grain_t* %film_grain) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_denoise_and_model_t*, align 4
  %sd.addr = alloca %struct.yv12_buffer_config*, align 4
  %film_grain.addr = alloca %struct.aom_film_grain_t*, align 4
  %block_size = alloca i32, align 4
  %use_highbd = alloca i32, align 4
  %raw_data = alloca [3 x i8*], align 4
  %data = alloca [3 x i8*], align 4
  %strides = alloca [3 x i32], align 4
  %chroma_sub_log2 = alloca [2 x i32], align 4
  %cleanup.dest.slot = alloca i32, align 4
  %status = alloca i8, align 1
  %have_noise_estimate = alloca i32, align 4
  store %struct.aom_denoise_and_model_t* %ctx, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %sd, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  store %struct.aom_film_grain_t* %film_grain, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %0 = bitcast i32* %block_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %block_size1 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %1, i32 0, i32 0
  %2 = load i32, i32* %block_size1, align 8, !tbaa !80
  store i32 %2, i32* %block_size, align 4, !tbaa !6
  %3 = bitcast i32* %use_highbd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %4, i32 0, i32 26
  %5 = load i32, i32* %flags, align 4, !tbaa !85
  %and = and i32 %5, 8
  %cmp = icmp ne i32 %and, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %use_highbd, align 4, !tbaa !6
  %6 = bitcast [3 x i8*]* %raw_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %6) #7
  %arrayinit.begin = getelementptr inbounds [3 x i8*], [3 x i8*]* %raw_data, i32 0, i32 0
  %7 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %9 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %8, i32 0, i32 5
  %10 = bitcast %union.anon.8* %9 to %struct.anon.9*
  %y_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %10, i32 0, i32 0
  %11 = load i8*, i8** %y_buffer, align 4, !tbaa !35
  %12 = ptrtoint i8* %11 to i32
  %shl = shl i32 %12, 1
  %13 = inttoptr i32 %shl to i16*
  %14 = bitcast i16* %13 to i8*
  br label %cond.end

cond.false:                                       ; preds = %entry
  %15 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %16 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %15, i32 0, i32 5
  %17 = bitcast %union.anon.8* %16 to %struct.anon.9*
  %y_buffer2 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %17, i32 0, i32 0
  %18 = load i8*, i8** %y_buffer2, align 4, !tbaa !35
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %14, %cond.true ], [ %18, %cond.false ]
  store i8* %cond, i8** %arrayinit.begin, align 4, !tbaa !2
  %arrayinit.element = getelementptr inbounds i8*, i8** %arrayinit.begin, i32 1
  %19 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %tobool3 = icmp ne i32 %19, 0
  br i1 %tobool3, label %cond.true4, label %cond.false6

cond.true4:                                       ; preds = %cond.end
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %21 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %20, i32 0, i32 5
  %22 = bitcast %union.anon.8* %21 to %struct.anon.9*
  %u_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %22, i32 0, i32 1
  %23 = load i8*, i8** %u_buffer, align 4, !tbaa !35
  %24 = ptrtoint i8* %23 to i32
  %shl5 = shl i32 %24, 1
  %25 = inttoptr i32 %shl5 to i16*
  %26 = bitcast i16* %25 to i8*
  br label %cond.end8

cond.false6:                                      ; preds = %cond.end
  %27 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %28 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %27, i32 0, i32 5
  %29 = bitcast %union.anon.8* %28 to %struct.anon.9*
  %u_buffer7 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %29, i32 0, i32 1
  %30 = load i8*, i8** %u_buffer7, align 4, !tbaa !35
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false6, %cond.true4
  %cond9 = phi i8* [ %26, %cond.true4 ], [ %30, %cond.false6 ]
  store i8* %cond9, i8** %arrayinit.element, align 4, !tbaa !2
  %arrayinit.element10 = getelementptr inbounds i8*, i8** %arrayinit.element, i32 1
  %31 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %tobool11 = icmp ne i32 %31, 0
  br i1 %tobool11, label %cond.true12, label %cond.false14

cond.true12:                                      ; preds = %cond.end8
  %32 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %33 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %32, i32 0, i32 5
  %34 = bitcast %union.anon.8* %33 to %struct.anon.9*
  %v_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %34, i32 0, i32 2
  %35 = load i8*, i8** %v_buffer, align 4, !tbaa !35
  %36 = ptrtoint i8* %35 to i32
  %shl13 = shl i32 %36, 1
  %37 = inttoptr i32 %shl13 to i16*
  %38 = bitcast i16* %37 to i8*
  br label %cond.end16

cond.false14:                                     ; preds = %cond.end8
  %39 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %40 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %39, i32 0, i32 5
  %41 = bitcast %union.anon.8* %40 to %struct.anon.9*
  %v_buffer15 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %41, i32 0, i32 2
  %42 = load i8*, i8** %v_buffer15, align 4, !tbaa !35
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false14, %cond.true12
  %cond17 = phi i8* [ %38, %cond.true12 ], [ %42, %cond.false14 ]
  store i8* %cond17, i8** %arrayinit.element10, align 4, !tbaa !2
  %43 = bitcast [3 x i8*]* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %43) #7
  %arrayinit.begin18 = getelementptr inbounds [3 x i8*], [3 x i8*]* %data, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %raw_data, i32 0, i32 0
  %44 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %44, i8** %arrayinit.begin18, align 4, !tbaa !2
  %arrayinit.element19 = getelementptr inbounds i8*, i8** %arrayinit.begin18, i32 1
  %arrayidx20 = getelementptr inbounds [3 x i8*], [3 x i8*]* %raw_data, i32 0, i32 1
  %45 = load i8*, i8** %arrayidx20, align 4, !tbaa !2
  store i8* %45, i8** %arrayinit.element19, align 4, !tbaa !2
  %arrayinit.element21 = getelementptr inbounds i8*, i8** %arrayinit.element19, i32 1
  %arrayidx22 = getelementptr inbounds [3 x i8*], [3 x i8*]* %raw_data, i32 0, i32 2
  %46 = load i8*, i8** %arrayidx22, align 4, !tbaa !2
  store i8* %46, i8** %arrayinit.element21, align 4, !tbaa !2
  %47 = bitcast [3 x i32]* %strides to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %47) #7
  %arrayinit.begin23 = getelementptr inbounds [3 x i32], [3 x i32]* %strides, i32 0, i32 0
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %49 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 4
  %50 = bitcast %union.anon.6* %49 to %struct.anon.7*
  %y_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %50, i32 0, i32 0
  %51 = load i32, i32* %y_stride, align 4, !tbaa !35
  store i32 %51, i32* %arrayinit.begin23, align 4, !tbaa !6
  %arrayinit.element24 = getelementptr inbounds i32, i32* %arrayinit.begin23, i32 1
  %52 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %53 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %52, i32 0, i32 4
  %54 = bitcast %union.anon.6* %53 to %struct.anon.7*
  %uv_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %54, i32 0, i32 1
  %55 = load i32, i32* %uv_stride, align 4, !tbaa !35
  store i32 %55, i32* %arrayinit.element24, align 4, !tbaa !6
  %arrayinit.element25 = getelementptr inbounds i32, i32* %arrayinit.element24, i32 1
  %56 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %57 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %56, i32 0, i32 4
  %58 = bitcast %union.anon.6* %57 to %struct.anon.7*
  %uv_stride26 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %58, i32 0, i32 1
  %59 = load i32, i32* %uv_stride26, align 4, !tbaa !35
  store i32 %59, i32* %arrayinit.element25, align 4, !tbaa !6
  %60 = bitcast [2 x i32]* %chroma_sub_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %60) #7
  %arrayinit.begin27 = getelementptr inbounds [2 x i32], [2 x i32]* %chroma_sub_log2, i32 0, i32 0
  %61 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %61, i32 0, i32 14
  %62 = load i32, i32* %subsampling_x, align 4, !tbaa !88
  store i32 %62, i32* %arrayinit.begin27, align 4, !tbaa !6
  %arrayinit.element28 = getelementptr inbounds i32, i32* %arrayinit.begin27, i32 1
  %63 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %63, i32 0, i32 15
  %64 = load i32, i32* %subsampling_y, align 4, !tbaa !89
  store i32 %64, i32* %arrayinit.element28, align 4, !tbaa !6
  %65 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %66 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %call = call i32 @denoise_and_model_realloc_if_necessary(%struct.aom_denoise_and_model_t* %65, %struct.yv12_buffer_config* %66)
  %tobool29 = icmp ne i32 %call, 0
  br i1 %tobool29, label %if.end, label %if.then

if.then:                                          ; preds = %cond.end16
  %67 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call30 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %67, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.21, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup103

if.end:                                           ; preds = %cond.end16
  %68 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_block_finder = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %68, i32 0, i32 12
  %arrayidx31 = getelementptr inbounds [3 x i8*], [3 x i8*]* %data, i32 0, i32 0
  %69 = load i8*, i8** %arrayidx31, align 4, !tbaa !2
  %70 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %71 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %70, i32 0, i32 0
  %72 = bitcast %union.anon* %71 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %72, i32 0, i32 0
  %73 = load i32, i32* %y_width, align 4, !tbaa !35
  %74 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %75 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %74, i32 0, i32 1
  %76 = bitcast %union.anon.0* %75 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %76, i32 0, i32 0
  %77 = load i32, i32* %y_height, align 4, !tbaa !35
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* %strides, i32 0, i32 0
  %78 = load i32, i32* %arrayidx32, align 4, !tbaa !6
  %79 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_blocks = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %79, i32 0, i32 11
  %80 = load i8*, i8** %flat_blocks, align 4, !tbaa !84
  %call33 = call i32 @aom_flat_block_finder_run(%struct.aom_flat_block_finder_t* %flat_block_finder, i8* %69, i32 %73, i32 %77, i32 %78, i8* %80)
  %arraydecay = getelementptr inbounds [3 x i8*], [3 x i8*]* %data, i32 0, i32 0
  %81 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %81, i32 0, i32 10
  %arraydecay34 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised, i32 0, i32 0
  %82 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %83 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %82, i32 0, i32 0
  %84 = bitcast %union.anon* %83 to %struct.anon*
  %y_width35 = getelementptr inbounds %struct.anon, %struct.anon* %84, i32 0, i32 0
  %85 = load i32, i32* %y_width35, align 4, !tbaa !35
  %86 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %87 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %86, i32 0, i32 1
  %88 = bitcast %union.anon.0* %87 to %struct.anon.1*
  %y_height36 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %88, i32 0, i32 0
  %89 = load i32, i32* %y_height36, align 4, !tbaa !35
  %arraydecay37 = getelementptr inbounds [3 x i32], [3 x i32]* %strides, i32 0, i32 0
  %arraydecay38 = getelementptr inbounds [2 x i32], [2 x i32]* %chroma_sub_log2, i32 0, i32 0
  %90 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_psd = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %90, i32 0, i32 9
  %arraydecay39 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd, i32 0, i32 0
  %91 = load i32, i32* %block_size, align 4, !tbaa !6
  %92 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %bit_depth = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %92, i32 0, i32 1
  %93 = load i32, i32* %bit_depth, align 4, !tbaa !83
  %94 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %call40 = call i32 @aom_wiener_denoise_2d(i8** %arraydecay, i8** %arraydecay34, i32 %85, i32 %89, i32* %arraydecay37, i32* %arraydecay38, float** %arraydecay39, i32 %91, i32 %93, i32 %94)
  %tobool41 = icmp ne i32 %call40, 0
  br i1 %tobool41, label %if.end44, label %if.then42

if.then42:                                        ; preds = %if.end
  %95 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call43 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %95, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.22, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup103

if.end44:                                         ; preds = %if.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %status) #7
  %96 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_model = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %96, i32 0, i32 13
  %arraydecay45 = getelementptr inbounds [3 x i8*], [3 x i8*]* %data, i32 0, i32 0
  %97 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised46 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %97, i32 0, i32 10
  %arraydecay47 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised46, i32 0, i32 0
  %98 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %99 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %98, i32 0, i32 0
  %100 = bitcast %union.anon* %99 to %struct.anon*
  %y_width48 = getelementptr inbounds %struct.anon, %struct.anon* %100, i32 0, i32 0
  %101 = load i32, i32* %y_width48, align 4, !tbaa !35
  %102 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %103 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %102, i32 0, i32 1
  %104 = bitcast %union.anon.0* %103 to %struct.anon.1*
  %y_height49 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %104, i32 0, i32 0
  %105 = load i32, i32* %y_height49, align 4, !tbaa !35
  %arraydecay50 = getelementptr inbounds [3 x i32], [3 x i32]* %strides, i32 0, i32 0
  %arraydecay51 = getelementptr inbounds [2 x i32], [2 x i32]* %chroma_sub_log2, i32 0, i32 0
  %106 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_blocks52 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %106, i32 0, i32 11
  %107 = load i8*, i8** %flat_blocks52, align 4, !tbaa !84
  %108 = load i32, i32* %block_size, align 4, !tbaa !6
  %call53 = call zeroext i8 @aom_noise_model_update(%struct.aom_noise_model_t* %noise_model, i8** %arraydecay45, i8** %arraydecay47, i32 %101, i32 %105, i32* %arraydecay50, i32* %arraydecay51, i8* %107, i32 %108)
  store i8 %call53, i8* %status, align 1, !tbaa !35
  %109 = bitcast i32* %have_noise_estimate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #7
  store i32 0, i32* %have_noise_estimate, align 4, !tbaa !6
  %110 = load i8, i8* %status, align 1, !tbaa !35
  %conv54 = zext i8 %110 to i32
  %cmp55 = icmp eq i32 %conv54, 0
  br i1 %cmp55, label %if.then57, label %if.else

if.then57:                                        ; preds = %if.end44
  store i32 1, i32* %have_noise_estimate, align 4, !tbaa !6
  br label %if.end69

if.else:                                          ; preds = %if.end44
  %111 = load i8, i8* %status, align 1, !tbaa !35
  %conv58 = zext i8 %111 to i32
  %cmp59 = icmp eq i32 %conv58, 3
  br i1 %cmp59, label %if.then61, label %if.else63

if.then61:                                        ; preds = %if.else
  %112 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_model62 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %112, i32 0, i32 13
  call void @aom_noise_model_save_latest(%struct.aom_noise_model_t* %noise_model62)
  store i32 1, i32* %have_noise_estimate, align 4, !tbaa !6
  br label %if.end68

if.else63:                                        ; preds = %if.else
  %113 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_model64 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %113, i32 0, i32 13
  %combined_state = getelementptr inbounds %struct.aom_noise_model_t, %struct.aom_noise_model_t* %noise_model64, i32 0, i32 1
  %arrayidx65 = getelementptr inbounds [3 x %struct.aom_noise_state_t], [3 x %struct.aom_noise_state_t]* %combined_state, i32 0, i32 0
  %strength_solver = getelementptr inbounds %struct.aom_noise_state_t, %struct.aom_noise_state_t* %arrayidx65, i32 0, i32 1
  %num_equations = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %strength_solver, i32 0, i32 4
  %114 = load i32, i32* %num_equations, align 4, !tbaa !53
  %cmp66 = icmp sgt i32 %114, 0
  %conv67 = zext i1 %cmp66 to i32
  store i32 %conv67, i32* %have_noise_estimate, align 4, !tbaa !6
  br label %if.end68

if.end68:                                         ; preds = %if.else63, %if.then61
  br label %if.end69

if.end69:                                         ; preds = %if.end68, %if.then57
  %115 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %apply_grain = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %115, i32 0, i32 0
  store i32 0, i32* %apply_grain, align 4, !tbaa !62
  %116 = load i32, i32* %have_noise_estimate, align 4, !tbaa !6
  %tobool70 = icmp ne i32 %116, 0
  br i1 %tobool70, label %if.then71, label %if.end101

if.then71:                                        ; preds = %if.end69
  %117 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_model72 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %117, i32 0, i32 13
  %118 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %call73 = call i32 @aom_noise_model_get_grain_parameters(%struct.aom_noise_model_t* %noise_model72, %struct.aom_film_grain_t* %118)
  %tobool74 = icmp ne i32 %call73, 0
  br i1 %tobool74, label %if.end77, label %if.then75

if.then75:                                        ; preds = %if.then71
  %119 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call76 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %119, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.23, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end77:                                         ; preds = %if.then71
  %120 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %random_seed = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %120, i32 0, i32 25
  %121 = load i16, i16* %random_seed, align 4, !tbaa !60
  %tobool78 = icmp ne i16 %121, 0
  br i1 %tobool78, label %if.end81, label %if.then79

if.then79:                                        ; preds = %if.end77
  %122 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %film_grain.addr, align 4, !tbaa !2
  %random_seed80 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %122, i32 0, i32 25
  store i16 7391, i16* %random_seed80, align 4, !tbaa !60
  br label %if.end81

if.end81:                                         ; preds = %if.then79, %if.end77
  %arrayidx82 = getelementptr inbounds [3 x i8*], [3 x i8*]* %raw_data, i32 0, i32 0
  %123 = load i8*, i8** %arrayidx82, align 4, !tbaa !2
  %124 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised83 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %124, i32 0, i32 10
  %arrayidx84 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised83, i32 0, i32 0
  %125 = load i8*, i8** %arrayidx84, align 8, !tbaa !2
  %arrayidx85 = getelementptr inbounds [3 x i32], [3 x i32]* %strides, i32 0, i32 0
  %126 = load i32, i32* %arrayidx85, align 4, !tbaa !6
  %127 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %128 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %127, i32 0, i32 1
  %129 = bitcast %union.anon.0* %128 to %struct.anon.1*
  %y_height86 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %129, i32 0, i32 0
  %130 = load i32, i32* %y_height86, align 4, !tbaa !35
  %mul = mul nsw i32 %126, %130
  %131 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %shl87 = shl i32 %mul, %131
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %123, i8* align 1 %125, i32 %shl87, i1 false)
  %arrayidx88 = getelementptr inbounds [3 x i8*], [3 x i8*]* %raw_data, i32 0, i32 1
  %132 = load i8*, i8** %arrayidx88, align 4, !tbaa !2
  %133 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised89 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %133, i32 0, i32 10
  %arrayidx90 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised89, i32 0, i32 1
  %134 = load i8*, i8** %arrayidx90, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds [3 x i32], [3 x i32]* %strides, i32 0, i32 1
  %135 = load i32, i32* %arrayidx91, align 4, !tbaa !6
  %136 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %137 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %136, i32 0, i32 1
  %138 = bitcast %union.anon.0* %137 to %struct.anon.1*
  %uv_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %138, i32 0, i32 1
  %139 = load i32, i32* %uv_height, align 4, !tbaa !35
  %mul92 = mul nsw i32 %135, %139
  %140 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %shl93 = shl i32 %mul92, %140
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %132, i8* align 1 %134, i32 %shl93, i1 false)
  %arrayidx94 = getelementptr inbounds [3 x i8*], [3 x i8*]* %raw_data, i32 0, i32 2
  %141 = load i8*, i8** %arrayidx94, align 4, !tbaa !2
  %142 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised95 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %142, i32 0, i32 10
  %arrayidx96 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised95, i32 0, i32 2
  %143 = load i8*, i8** %arrayidx96, align 8, !tbaa !2
  %arrayidx97 = getelementptr inbounds [3 x i32], [3 x i32]* %strides, i32 0, i32 2
  %144 = load i32, i32* %arrayidx97, align 4, !tbaa !6
  %145 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %146 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %145, i32 0, i32 1
  %147 = bitcast %union.anon.0* %146 to %struct.anon.1*
  %uv_height98 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %147, i32 0, i32 1
  %148 = load i32, i32* %uv_height98, align 4, !tbaa !35
  %mul99 = mul nsw i32 %144, %148
  %149 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %shl100 = shl i32 %mul99, %149
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %141, i8* align 1 %143, i32 %shl100, i1 false)
  br label %if.end101

if.end101:                                        ; preds = %if.end81, %if.end69
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end101, %if.then75
  %150 = bitcast i32* %have_noise_estimate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %status) #7
  br label %cleanup103

cleanup103:                                       ; preds = %cleanup, %if.then42, %if.then
  %151 = bitcast [2 x i32]* %chroma_sub_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %151) #7
  %152 = bitcast [3 x i32]* %strides to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %152) #7
  %153 = bitcast [3 x i8*]* %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %153) #7
  %154 = bitcast [3 x i8*]* %raw_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %154) #7
  %155 = bitcast i32* %use_highbd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #7
  %156 = bitcast i32* %block_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #7
  %157 = load i32, i32* %retval, align 4
  ret i32 %157
}

; Function Attrs: nounwind
define internal i32 @denoise_and_model_realloc_if_necessary(%struct.aom_denoise_and_model_t* %ctx, %struct.yv12_buffer_config* %sd) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_denoise_and_model_t*, align 4
  %sd.addr = alloca %struct.yv12_buffer_config*, align 4
  %use_highbd = alloca i32, align 4
  %block_size = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %params = alloca %struct.aom_noise_model_params_t, align 4
  %y_noise_level = alloca float, align 4
  %uv_noise_level = alloca float, align 4
  %i87 = alloca i32, align 4
  store %struct.aom_denoise_and_model_t* %ctx, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %sd, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %0 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %0, i32 0, i32 3
  %1 = load i32, i32* %width, align 4, !tbaa !90
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 0
  %4 = bitcast %union.anon* %3 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %4, i32 0, i32 0
  %5 = load i32, i32* %y_width, align 4, !tbaa !35
  %cmp = icmp eq i32 %1, %5
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %6 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %6, i32 0, i32 4
  %7 = load i32, i32* %height, align 8, !tbaa !91
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %9 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %8, i32 0, i32 1
  %10 = bitcast %union.anon.0* %9 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %10, i32 0, i32 0
  %11 = load i32, i32* %y_height, align 4, !tbaa !35
  %cmp1 = icmp eq i32 %7, %11
  br i1 %cmp1, label %land.lhs.true2, label %if.end

land.lhs.true2:                                   ; preds = %land.lhs.true
  %12 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %y_stride = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %12, i32 0, i32 5
  %13 = load i32, i32* %y_stride, align 4, !tbaa !92
  %14 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %15 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %14, i32 0, i32 4
  %16 = bitcast %union.anon.6* %15 to %struct.anon.7*
  %y_stride3 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %16, i32 0, i32 0
  %17 = load i32, i32* %y_stride3, align 4, !tbaa !35
  %cmp4 = icmp eq i32 %13, %17
  br i1 %cmp4, label %land.lhs.true5, label %if.end

land.lhs.true5:                                   ; preds = %land.lhs.true2
  %18 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %uv_stride = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %18, i32 0, i32 6
  %19 = load i32, i32* %uv_stride, align 8, !tbaa !93
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %21 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %20, i32 0, i32 4
  %22 = bitcast %union.anon.6* %21 to %struct.anon.7*
  %uv_stride6 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %22, i32 0, i32 1
  %23 = load i32, i32* %uv_stride6, align 4, !tbaa !35
  %cmp7 = icmp eq i32 %19, %23
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true5
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true5, %land.lhs.true2, %land.lhs.true, %entry
  %24 = bitcast i32* %use_highbd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %25, i32 0, i32 26
  %26 = load i32, i32* %flags, align 4, !tbaa !85
  %and = and i32 %26, 8
  %cmp8 = icmp ne i32 %and, 0
  %conv = zext i1 %cmp8 to i32
  store i32 %conv, i32* %use_highbd, align 4, !tbaa !6
  %27 = bitcast i32* %block_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  %28 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %block_size9 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %28, i32 0, i32 0
  %29 = load i32, i32* %block_size9, align 8, !tbaa !80
  store i32 %29, i32* %block_size, align 4, !tbaa !6
  %30 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %31 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %30, i32 0, i32 0
  %32 = bitcast %union.anon* %31 to %struct.anon*
  %y_width10 = getelementptr inbounds %struct.anon, %struct.anon* %32, i32 0, i32 0
  %33 = load i32, i32* %y_width10, align 4, !tbaa !35
  %34 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %width11 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %34, i32 0, i32 3
  store i32 %33, i32* %width11, align 4, !tbaa !90
  %35 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %36 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %35, i32 0, i32 1
  %37 = bitcast %union.anon.0* %36 to %struct.anon.1*
  %y_height12 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %37, i32 0, i32 0
  %38 = load i32, i32* %y_height12, align 4, !tbaa !35
  %39 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %height13 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %39, i32 0, i32 4
  store i32 %38, i32* %height13, align 8, !tbaa !91
  %40 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %41 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %40, i32 0, i32 4
  %42 = bitcast %union.anon.6* %41 to %struct.anon.7*
  %y_stride14 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %42, i32 0, i32 0
  %43 = load i32, i32* %y_stride14, align 4, !tbaa !35
  %44 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %y_stride15 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %44, i32 0, i32 5
  store i32 %43, i32* %y_stride15, align 4, !tbaa !92
  %45 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %46 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %45, i32 0, i32 4
  %47 = bitcast %union.anon.6* %46 to %struct.anon.7*
  %uv_stride16 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %47, i32 0, i32 1
  %48 = load i32, i32* %uv_stride16, align 4, !tbaa !35
  %49 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %uv_stride17 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %49, i32 0, i32 6
  store i32 %48, i32* %uv_stride17, align 8, !tbaa !93
  %50 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %cmp18 = icmp slt i32 %51, 3
  br i1 %cmp18, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %52 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %53 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %53, i32 0, i32 10
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised, i32 0, i32 %54
  %55 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  call void @aom_free(i8* %55)
  %56 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised20 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %56, i32 0, i32 10
  %57 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised20, i32 0, i32 %57
  store i8* null, i8** %arrayidx21, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %59 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_blocks = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %59, i32 0, i32 11
  %60 = load i8*, i8** %flat_blocks, align 4, !tbaa !84
  call void @aom_free(i8* %60)
  %61 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_blocks22 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %61, i32 0, i32 11
  store i8* null, i8** %flat_blocks22, align 4, !tbaa !84
  %62 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %63 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %62, i32 0, i32 4
  %64 = bitcast %union.anon.6* %63 to %struct.anon.7*
  %y_stride23 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %64, i32 0, i32 0
  %65 = load i32, i32* %y_stride23, align 4, !tbaa !35
  %66 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %67 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %66, i32 0, i32 1
  %68 = bitcast %union.anon.0* %67 to %struct.anon.1*
  %y_height24 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %68, i32 0, i32 0
  %69 = load i32, i32* %y_height24, align 4, !tbaa !35
  %mul = mul nsw i32 %65, %69
  %70 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %shl = shl i32 %mul, %70
  %call = call i8* @aom_malloc(i32 %shl)
  %71 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised25 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %71, i32 0, i32 10
  %arrayidx26 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised25, i32 0, i32 0
  store i8* %call, i8** %arrayidx26, align 8, !tbaa !2
  %72 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %73 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %72, i32 0, i32 4
  %74 = bitcast %union.anon.6* %73 to %struct.anon.7*
  %uv_stride27 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %74, i32 0, i32 1
  %75 = load i32, i32* %uv_stride27, align 4, !tbaa !35
  %76 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %77 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %76, i32 0, i32 1
  %78 = bitcast %union.anon.0* %77 to %struct.anon.1*
  %uv_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %78, i32 0, i32 1
  %79 = load i32, i32* %uv_height, align 4, !tbaa !35
  %mul28 = mul nsw i32 %75, %79
  %80 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %shl29 = shl i32 %mul28, %80
  %call30 = call i8* @aom_malloc(i32 %shl29)
  %81 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised31 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %81, i32 0, i32 10
  %arrayidx32 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised31, i32 0, i32 1
  store i8* %call30, i8** %arrayidx32, align 4, !tbaa !2
  %82 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %83 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %82, i32 0, i32 4
  %84 = bitcast %union.anon.6* %83 to %struct.anon.7*
  %uv_stride33 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %84, i32 0, i32 1
  %85 = load i32, i32* %uv_stride33, align 4, !tbaa !35
  %86 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %87 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %86, i32 0, i32 1
  %88 = bitcast %union.anon.0* %87 to %struct.anon.1*
  %uv_height34 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %88, i32 0, i32 1
  %89 = load i32, i32* %uv_height34, align 4, !tbaa !35
  %mul35 = mul nsw i32 %85, %89
  %90 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %shl36 = shl i32 %mul35, %90
  %call37 = call i8* @aom_malloc(i32 %shl36)
  %91 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised38 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %91, i32 0, i32 10
  %arrayidx39 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised38, i32 0, i32 2
  store i8* %call37, i8** %arrayidx39, align 8, !tbaa !2
  %92 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised40 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %92, i32 0, i32 10
  %arrayidx41 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised40, i32 0, i32 0
  %93 = load i8*, i8** %arrayidx41, align 8, !tbaa !2
  %tobool = icmp ne i8* %93, null
  br i1 %tobool, label %lor.lhs.false, label %if.then49

lor.lhs.false:                                    ; preds = %for.end
  %94 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised42 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %94, i32 0, i32 10
  %arrayidx43 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised42, i32 0, i32 1
  %95 = load i8*, i8** %arrayidx43, align 4, !tbaa !2
  %tobool44 = icmp ne i8* %95, null
  br i1 %tobool44, label %lor.lhs.false45, label %if.then49

lor.lhs.false45:                                  ; preds = %lor.lhs.false
  %96 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %denoised46 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %96, i32 0, i32 10
  %arrayidx47 = getelementptr inbounds [3 x i8*], [3 x i8*]* %denoised46, i32 0, i32 2
  %97 = load i8*, i8** %arrayidx47, align 8, !tbaa !2
  %tobool48 = icmp ne i8* %97, null
  br i1 %tobool48, label %if.end51, label %if.then49

if.then49:                                        ; preds = %lor.lhs.false45, %lor.lhs.false, %for.end
  %98 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call50 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %98, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.28, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end51:                                         ; preds = %lor.lhs.false45
  %99 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %100 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %99, i32 0, i32 0
  %101 = bitcast %union.anon* %100 to %struct.anon*
  %y_width52 = getelementptr inbounds %struct.anon, %struct.anon* %101, i32 0, i32 0
  %102 = load i32, i32* %y_width52, align 4, !tbaa !35
  %103 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %block_size53 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %103, i32 0, i32 0
  %104 = load i32, i32* %block_size53, align 8, !tbaa !80
  %add = add nsw i32 %102, %104
  %sub = sub nsw i32 %add, 1
  %105 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %block_size54 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %105, i32 0, i32 0
  %106 = load i32, i32* %block_size54, align 8, !tbaa !80
  %div = sdiv i32 %sub, %106
  %107 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %num_blocks_w = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %107, i32 0, i32 7
  store i32 %div, i32* %num_blocks_w, align 4, !tbaa !94
  %108 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %109 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %108, i32 0, i32 1
  %110 = bitcast %union.anon.0* %109 to %struct.anon.1*
  %y_height55 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %110, i32 0, i32 0
  %111 = load i32, i32* %y_height55, align 4, !tbaa !35
  %112 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %block_size56 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %112, i32 0, i32 0
  %113 = load i32, i32* %block_size56, align 8, !tbaa !80
  %add57 = add nsw i32 %111, %113
  %sub58 = sub nsw i32 %add57, 1
  %114 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %block_size59 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %114, i32 0, i32 0
  %115 = load i32, i32* %block_size59, align 8, !tbaa !80
  %div60 = sdiv i32 %sub58, %115
  %116 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %num_blocks_h = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %116, i32 0, i32 8
  store i32 %div60, i32* %num_blocks_h, align 8, !tbaa !95
  %117 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %num_blocks_w61 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %117, i32 0, i32 7
  %118 = load i32, i32* %num_blocks_w61, align 4, !tbaa !94
  %119 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %num_blocks_h62 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %119, i32 0, i32 8
  %120 = load i32, i32* %num_blocks_h62, align 8, !tbaa !95
  %mul63 = mul nsw i32 %118, %120
  %call64 = call i8* @aom_malloc(i32 %mul63)
  %121 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_blocks65 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %121, i32 0, i32 11
  store i8* %call64, i8** %flat_blocks65, align 4, !tbaa !84
  %122 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_block_finder = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %122, i32 0, i32 12
  call void @aom_flat_block_finder_free(%struct.aom_flat_block_finder_t* %flat_block_finder)
  %123 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %flat_block_finder66 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %123, i32 0, i32 12
  %124 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %block_size67 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %124, i32 0, i32 0
  %125 = load i32, i32* %block_size67, align 8, !tbaa !80
  %126 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %bit_depth = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %126, i32 0, i32 1
  %127 = load i32, i32* %bit_depth, align 4, !tbaa !83
  %128 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %call68 = call i32 @aom_flat_block_finder_init(%struct.aom_flat_block_finder_t* %flat_block_finder66, i32 %125, i32 %127, i32 %128)
  %tobool69 = icmp ne i32 %call68, 0
  br i1 %tobool69, label %if.end72, label %if.then70

if.then70:                                        ; preds = %if.end51
  %129 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call71 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %129, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.29, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end72:                                         ; preds = %if.end51
  %130 = bitcast %struct.aom_noise_model_params_t* %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %130) #7
  %shape = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 0
  store i8 1, i8* %shape, align 4, !tbaa !47
  %lag = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 1
  store i32 3, i32* %lag, align 4, !tbaa !41
  %bit_depth73 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 2
  %131 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %bit_depth74 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %131, i32 0, i32 1
  %132 = load i32, i32* %bit_depth74, align 4, !tbaa !83
  store i32 %132, i32* %bit_depth73, align 4, !tbaa !43
  %use_highbd75 = getelementptr inbounds %struct.aom_noise_model_params_t, %struct.aom_noise_model_params_t* %params, i32 0, i32 3
  %133 = load i32, i32* %use_highbd, align 4, !tbaa !6
  store i32 %133, i32* %use_highbd75, align 4, !tbaa !96
  %134 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_model = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %134, i32 0, i32 13
  call void @aom_noise_model_free(%struct.aom_noise_model_t* %noise_model)
  %135 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_model76 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %135, i32 0, i32 13
  %call77 = call i32 @aom_noise_model_init(%struct.aom_noise_model_t* %noise_model76, %struct.aom_noise_model_params_t* byval(%struct.aom_noise_model_params_t) align 4 %params)
  %tobool78 = icmp ne i32 %call77, 0
  br i1 %tobool78, label %if.end81, label %if.then79

if.then79:                                        ; preds = %if.end72
  %136 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call80 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %136, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.30, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end81:                                         ; preds = %if.end72
  %137 = bitcast float* %y_noise_level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #7
  %138 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %block_size82 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %138, i32 0, i32 0
  %139 = load i32, i32* %block_size82, align 8, !tbaa !80
  %140 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_level = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %140, i32 0, i32 2
  %141 = load float, float* %noise_level, align 8, !tbaa !82
  %call83 = call float @aom_noise_psd_get_default_value(i32 %139, float %141)
  store float %call83, float* %y_noise_level, align 4, !tbaa !36
  %142 = bitcast float* %uv_noise_level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #7
  %143 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %block_size84 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %143, i32 0, i32 0
  %144 = load i32, i32* %block_size84, align 8, !tbaa !80
  %145 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %145, i32 0, i32 14
  %146 = load i32, i32* %subsampling_x, align 4, !tbaa !88
  %shr = ashr i32 %144, %146
  %147 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_level85 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %147, i32 0, i32 2
  %148 = load float, float* %noise_level85, align 8, !tbaa !82
  %call86 = call float @aom_noise_psd_get_default_value(i32 %shr, float %148)
  store float %call86, float* %uv_noise_level, align 4, !tbaa !36
  %149 = bitcast i32* %i87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #7
  store i32 0, i32* %i87, align 4, !tbaa !6
  br label %for.cond88

for.cond88:                                       ; preds = %for.inc102, %if.end81
  %150 = load i32, i32* %i87, align 4, !tbaa !6
  %151 = load i32, i32* %block_size, align 4, !tbaa !6
  %152 = load i32, i32* %block_size, align 4, !tbaa !6
  %mul89 = mul nsw i32 %151, %152
  %cmp90 = icmp slt i32 %150, %mul89
  br i1 %cmp90, label %for.body93, label %for.cond.cleanup92

for.cond.cleanup92:                               ; preds = %for.cond88
  store i32 5, i32* %cleanup.dest.slot, align 4
  %153 = bitcast i32* %i87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #7
  br label %for.end104

for.body93:                                       ; preds = %for.cond88
  %154 = load float, float* %y_noise_level, align 4, !tbaa !36
  %155 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_psd = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %155, i32 0, i32 9
  %arrayidx94 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd, i32 0, i32 0
  %156 = load float*, float** %arrayidx94, align 4, !tbaa !2
  %157 = load i32, i32* %i87, align 4, !tbaa !6
  %arrayidx95 = getelementptr inbounds float, float* %156, i32 %157
  store float %154, float* %arrayidx95, align 4, !tbaa !36
  %158 = load float, float* %uv_noise_level, align 4, !tbaa !36
  %159 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_psd96 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %159, i32 0, i32 9
  %arrayidx97 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd96, i32 0, i32 2
  %160 = load float*, float** %arrayidx97, align 4, !tbaa !2
  %161 = load i32, i32* %i87, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds float, float* %160, i32 %161
  store float %158, float* %arrayidx98, align 4, !tbaa !36
  %162 = load %struct.aom_denoise_and_model_t*, %struct.aom_denoise_and_model_t** %ctx.addr, align 4, !tbaa !2
  %noise_psd99 = getelementptr inbounds %struct.aom_denoise_and_model_t, %struct.aom_denoise_and_model_t* %162, i32 0, i32 9
  %arrayidx100 = getelementptr inbounds [3 x float*], [3 x float*]* %noise_psd99, i32 0, i32 1
  %163 = load float*, float** %arrayidx100, align 4, !tbaa !2
  %164 = load i32, i32* %i87, align 4, !tbaa !6
  %arrayidx101 = getelementptr inbounds float, float* %163, i32 %164
  store float %158, float* %arrayidx101, align 4, !tbaa !36
  br label %for.inc102

for.inc102:                                       ; preds = %for.body93
  %165 = load i32, i32* %i87, align 4, !tbaa !6
  %inc103 = add nsw i32 %165, 1
  store i32 %inc103, i32* %i87, align 4, !tbaa !6
  br label %for.cond88

for.end104:                                       ; preds = %for.cond.cleanup92
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %166 = bitcast float* %uv_noise_level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #7
  %167 = bitcast float* %y_noise_level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #7
  br label %cleanup

cleanup:                                          ; preds = %for.end104, %if.then79
  %168 = bitcast %struct.aom_noise_model_params_t* %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %168) #7
  br label %cleanup105

cleanup105:                                       ; preds = %cleanup, %if.then70, %if.then49
  %169 = bitcast i32* %block_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #7
  %170 = bitcast i32* %use_highbd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #7
  br label %return

return:                                           ; preds = %cleanup105, %if.then
  %171 = load i32, i32* %retval, align 4
  ret i32 %171
}

; Function Attrs: inlinehint nounwind
define internal i32 @linsolve(i32 %n, double* %A, i32 %stride, double* %b, double* %x) #5 {
entry:
  %retval = alloca i32, align 4
  %n.addr = alloca i32, align 4
  %A.addr = alloca double*, align 4
  %stride.addr = alloca i32, align 4
  %b.addr = alloca double*, align 4
  %x.addr = alloca double*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %c = alloca double, align 8
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store double* %A, double** %A.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store double* %b, double** %b.addr, align 4, !tbaa !2
  store double* %x, double** %x.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast double* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #7
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc75, %entry
  %4 = load i32, i32* %k, align 4, !tbaa !6
  %5 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %5, 1
  %cmp = icmp slt i32 %4, %sub
  br i1 %cmp, label %for.body, label %for.end77

for.body:                                         ; preds = %for.cond
  %6 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 %6, 1
  store i32 %sub1, i32* %i, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc33, %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %8 = load i32, i32* %k, align 4, !tbaa !6
  %cmp3 = icmp sgt i32 %7, %8
  br i1 %cmp3, label %for.body4, label %for.end34

for.body4:                                        ; preds = %for.cond2
  %9 = load double*, double** %A.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %sub5 = sub nsw i32 %10, 1
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %sub5, %11
  %12 = load i32, i32* %k, align 4, !tbaa !6
  %add = add nsw i32 %mul, %12
  %arrayidx = getelementptr inbounds double, double* %9, i32 %add
  %13 = load double, double* %arrayidx, align 8, !tbaa !11
  %14 = call double @llvm.fabs.f64(double %13)
  %15 = load double*, double** %A.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 %16, %17
  %18 = load i32, i32* %k, align 4, !tbaa !6
  %add7 = add nsw i32 %mul6, %18
  %arrayidx8 = getelementptr inbounds double, double* %15, i32 %add7
  %19 = load double, double* %arrayidx8, align 8, !tbaa !11
  %20 = call double @llvm.fabs.f64(double %19)
  %cmp9 = fcmp olt double %14, %20
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body4
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %if.then
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %22 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %21, %22
  br i1 %cmp11, label %for.body12, label %for.end

for.body12:                                       ; preds = %for.cond10
  %23 = load double*, double** %A.addr, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %25 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 %24, %25
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %add14 = add nsw i32 %mul13, %26
  %arrayidx15 = getelementptr inbounds double, double* %23, i32 %add14
  %27 = load double, double* %arrayidx15, align 8, !tbaa !11
  store double %27, double* %c, align 8, !tbaa !11
  %28 = load double*, double** %A.addr, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %sub16 = sub nsw i32 %29, 1
  %30 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 %sub16, %30
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %add18 = add nsw i32 %mul17, %31
  %arrayidx19 = getelementptr inbounds double, double* %28, i32 %add18
  %32 = load double, double* %arrayidx19, align 8, !tbaa !11
  %33 = load double*, double** %A.addr, align 4, !tbaa !2
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %35 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 %34, %35
  %36 = load i32, i32* %j, align 4, !tbaa !6
  %add21 = add nsw i32 %mul20, %36
  %arrayidx22 = getelementptr inbounds double, double* %33, i32 %add21
  store double %32, double* %arrayidx22, align 8, !tbaa !11
  %37 = load double, double* %c, align 8, !tbaa !11
  %38 = load double*, double** %A.addr, align 4, !tbaa !2
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %sub23 = sub nsw i32 %39, 1
  %40 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul24 = mul nsw i32 %sub23, %40
  %41 = load i32, i32* %j, align 4, !tbaa !6
  %add25 = add nsw i32 %mul24, %41
  %arrayidx26 = getelementptr inbounds double, double* %38, i32 %add25
  store double %37, double* %arrayidx26, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body12
  %42 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %42, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond10

for.end:                                          ; preds = %for.cond10
  %43 = load double*, double** %b.addr, align 4, !tbaa !2
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds double, double* %43, i32 %44
  %45 = load double, double* %arrayidx27, align 8, !tbaa !11
  store double %45, double* %c, align 8, !tbaa !11
  %46 = load double*, double** %b.addr, align 4, !tbaa !2
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %sub28 = sub nsw i32 %47, 1
  %arrayidx29 = getelementptr inbounds double, double* %46, i32 %sub28
  %48 = load double, double* %arrayidx29, align 8, !tbaa !11
  %49 = load double*, double** %b.addr, align 4, !tbaa !2
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds double, double* %49, i32 %50
  store double %48, double* %arrayidx30, align 8, !tbaa !11
  %51 = load double, double* %c, align 8, !tbaa !11
  %52 = load double*, double** %b.addr, align 4, !tbaa !2
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %sub31 = sub nsw i32 %53, 1
  %arrayidx32 = getelementptr inbounds double, double* %52, i32 %sub31
  store double %51, double* %arrayidx32, align 8, !tbaa !11
  br label %if.end

if.end:                                           ; preds = %for.end, %for.body4
  br label %for.inc33

for.inc33:                                        ; preds = %if.end
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %dec = add nsw i32 %54, -1
  store i32 %dec, i32* %i, align 4, !tbaa !6
  br label %for.cond2

for.end34:                                        ; preds = %for.cond2
  %55 = load i32, i32* %k, align 4, !tbaa !6
  store i32 %55, i32* %i, align 4, !tbaa !6
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc72, %for.end34
  %56 = load i32, i32* %i, align 4, !tbaa !6
  %57 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub36 = sub nsw i32 %57, 1
  %cmp37 = icmp slt i32 %56, %sub36
  br i1 %cmp37, label %for.body38, label %for.end74

for.body38:                                       ; preds = %for.cond35
  %58 = load double*, double** %A.addr, align 4, !tbaa !2
  %59 = load i32, i32* %k, align 4, !tbaa !6
  %60 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul39 = mul nsw i32 %59, %60
  %61 = load i32, i32* %k, align 4, !tbaa !6
  %add40 = add nsw i32 %mul39, %61
  %arrayidx41 = getelementptr inbounds double, double* %58, i32 %add40
  %62 = load double, double* %arrayidx41, align 8, !tbaa !11
  %63 = call double @llvm.fabs.f64(double %62)
  %cmp42 = fcmp olt double %63, 0x3C9CD2B297D889BC
  br i1 %cmp42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %for.body38
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end44:                                         ; preds = %for.body38
  %64 = load double*, double** %A.addr, align 4, !tbaa !2
  %65 = load i32, i32* %i, align 4, !tbaa !6
  %add45 = add nsw i32 %65, 1
  %66 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul46 = mul nsw i32 %add45, %66
  %67 = load i32, i32* %k, align 4, !tbaa !6
  %add47 = add nsw i32 %mul46, %67
  %arrayidx48 = getelementptr inbounds double, double* %64, i32 %add47
  %68 = load double, double* %arrayidx48, align 8, !tbaa !11
  %69 = load double*, double** %A.addr, align 4, !tbaa !2
  %70 = load i32, i32* %k, align 4, !tbaa !6
  %71 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul49 = mul nsw i32 %70, %71
  %72 = load i32, i32* %k, align 4, !tbaa !6
  %add50 = add nsw i32 %mul49, %72
  %arrayidx51 = getelementptr inbounds double, double* %69, i32 %add50
  %73 = load double, double* %arrayidx51, align 8, !tbaa !11
  %div = fdiv double %68, %73
  store double %div, double* %c, align 8, !tbaa !11
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc64, %if.end44
  %74 = load i32, i32* %j, align 4, !tbaa !6
  %75 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp53 = icmp slt i32 %74, %75
  br i1 %cmp53, label %for.body54, label %for.end66

for.body54:                                       ; preds = %for.cond52
  %76 = load double, double* %c, align 8, !tbaa !11
  %77 = load double*, double** %A.addr, align 4, !tbaa !2
  %78 = load i32, i32* %k, align 4, !tbaa !6
  %79 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul55 = mul nsw i32 %78, %79
  %80 = load i32, i32* %j, align 4, !tbaa !6
  %add56 = add nsw i32 %mul55, %80
  %arrayidx57 = getelementptr inbounds double, double* %77, i32 %add56
  %81 = load double, double* %arrayidx57, align 8, !tbaa !11
  %mul58 = fmul double %76, %81
  %82 = load double*, double** %A.addr, align 4, !tbaa !2
  %83 = load i32, i32* %i, align 4, !tbaa !6
  %add59 = add nsw i32 %83, 1
  %84 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul60 = mul nsw i32 %add59, %84
  %85 = load i32, i32* %j, align 4, !tbaa !6
  %add61 = add nsw i32 %mul60, %85
  %arrayidx62 = getelementptr inbounds double, double* %82, i32 %add61
  %86 = load double, double* %arrayidx62, align 8, !tbaa !11
  %sub63 = fsub double %86, %mul58
  store double %sub63, double* %arrayidx62, align 8, !tbaa !11
  br label %for.inc64

for.inc64:                                        ; preds = %for.body54
  %87 = load i32, i32* %j, align 4, !tbaa !6
  %inc65 = add nsw i32 %87, 1
  store i32 %inc65, i32* %j, align 4, !tbaa !6
  br label %for.cond52

for.end66:                                        ; preds = %for.cond52
  %88 = load double, double* %c, align 8, !tbaa !11
  %89 = load double*, double** %b.addr, align 4, !tbaa !2
  %90 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds double, double* %89, i32 %90
  %91 = load double, double* %arrayidx67, align 8, !tbaa !11
  %mul68 = fmul double %88, %91
  %92 = load double*, double** %b.addr, align 4, !tbaa !2
  %93 = load i32, i32* %i, align 4, !tbaa !6
  %add69 = add nsw i32 %93, 1
  %arrayidx70 = getelementptr inbounds double, double* %92, i32 %add69
  %94 = load double, double* %arrayidx70, align 8, !tbaa !11
  %sub71 = fsub double %94, %mul68
  store double %sub71, double* %arrayidx70, align 8, !tbaa !11
  br label %for.inc72

for.inc72:                                        ; preds = %for.end66
  %95 = load i32, i32* %i, align 4, !tbaa !6
  %inc73 = add nsw i32 %95, 1
  store i32 %inc73, i32* %i, align 4, !tbaa !6
  br label %for.cond35

for.end74:                                        ; preds = %for.cond35
  br label %for.inc75

for.inc75:                                        ; preds = %for.end74
  %96 = load i32, i32* %k, align 4, !tbaa !6
  %inc76 = add nsw i32 %96, 1
  store i32 %inc76, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.end77:                                        ; preds = %for.cond
  %97 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub78 = sub nsw i32 %97, 1
  store i32 %sub78, i32* %i, align 4, !tbaa !6
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc109, %for.end77
  %98 = load i32, i32* %i, align 4, !tbaa !6
  %cmp80 = icmp sge i32 %98, 0
  br i1 %cmp80, label %for.body81, label %for.end111

for.body81:                                       ; preds = %for.cond79
  %99 = load double*, double** %A.addr, align 4, !tbaa !2
  %100 = load i32, i32* %i, align 4, !tbaa !6
  %101 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul82 = mul nsw i32 %100, %101
  %102 = load i32, i32* %i, align 4, !tbaa !6
  %add83 = add nsw i32 %mul82, %102
  %arrayidx84 = getelementptr inbounds double, double* %99, i32 %add83
  %103 = load double, double* %arrayidx84, align 8, !tbaa !11
  %104 = call double @llvm.fabs.f64(double %103)
  %cmp85 = fcmp olt double %104, 0x3C9CD2B297D889BC
  br i1 %cmp85, label %if.then86, label %if.end87

if.then86:                                        ; preds = %for.body81
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end87:                                         ; preds = %for.body81
  store double 0.000000e+00, double* %c, align 8, !tbaa !11
  %105 = load i32, i32* %i, align 4, !tbaa !6
  %add88 = add nsw i32 %105, 1
  store i32 %add88, i32* %j, align 4, !tbaa !6
  br label %for.cond89

for.cond89:                                       ; preds = %for.inc99, %if.end87
  %106 = load i32, i32* %j, align 4, !tbaa !6
  %107 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub90 = sub nsw i32 %107, 1
  %cmp91 = icmp sle i32 %106, %sub90
  br i1 %cmp91, label %for.body92, label %for.end101

for.body92:                                       ; preds = %for.cond89
  %108 = load double*, double** %A.addr, align 4, !tbaa !2
  %109 = load i32, i32* %i, align 4, !tbaa !6
  %110 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul93 = mul nsw i32 %109, %110
  %111 = load i32, i32* %j, align 4, !tbaa !6
  %add94 = add nsw i32 %mul93, %111
  %arrayidx95 = getelementptr inbounds double, double* %108, i32 %add94
  %112 = load double, double* %arrayidx95, align 8, !tbaa !11
  %113 = load double*, double** %x.addr, align 4, !tbaa !2
  %114 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx96 = getelementptr inbounds double, double* %113, i32 %114
  %115 = load double, double* %arrayidx96, align 8, !tbaa !11
  %mul97 = fmul double %112, %115
  %116 = load double, double* %c, align 8, !tbaa !11
  %add98 = fadd double %116, %mul97
  store double %add98, double* %c, align 8, !tbaa !11
  br label %for.inc99

for.inc99:                                        ; preds = %for.body92
  %117 = load i32, i32* %j, align 4, !tbaa !6
  %inc100 = add nsw i32 %117, 1
  store i32 %inc100, i32* %j, align 4, !tbaa !6
  br label %for.cond89

for.end101:                                       ; preds = %for.cond89
  %118 = load double*, double** %b.addr, align 4, !tbaa !2
  %119 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx102 = getelementptr inbounds double, double* %118, i32 %119
  %120 = load double, double* %arrayidx102, align 8, !tbaa !11
  %121 = load double, double* %c, align 8, !tbaa !11
  %sub103 = fsub double %120, %121
  %122 = load double*, double** %A.addr, align 4, !tbaa !2
  %123 = load i32, i32* %i, align 4, !tbaa !6
  %124 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul104 = mul nsw i32 %123, %124
  %125 = load i32, i32* %i, align 4, !tbaa !6
  %add105 = add nsw i32 %mul104, %125
  %arrayidx106 = getelementptr inbounds double, double* %122, i32 %add105
  %126 = load double, double* %arrayidx106, align 8, !tbaa !11
  %div107 = fdiv double %sub103, %126
  %127 = load double*, double** %x.addr, align 4, !tbaa !2
  %128 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx108 = getelementptr inbounds double, double* %127, i32 %128
  store double %div107, double* %arrayidx108, align 8, !tbaa !11
  br label %for.inc109

for.inc109:                                       ; preds = %for.end101
  %129 = load i32, i32* %i, align 4, !tbaa !6
  %dec110 = add nsw i32 %129, -1
  store i32 %dec110, i32* %i, align 4, !tbaa !6
  br label %for.cond79

for.end111:                                       ; preds = %for.cond79
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end111, %if.then86, %if.then43
  %130 = bitcast double* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %130) #7
  %131 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #7
  %132 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #7
  %133 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #7
  %134 = load i32, i32* %retval, align 4
  ret i32 %134
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #4

; Function Attrs: nounwind
define internal double @extract_ar_row_highbd([2 x i32]* %coords, i32 %num_coords, i16* %data, i16* %denoised, i32 %stride, i32* %sub_log2, i16* %alt_data, i16* %alt_denoised, i32 %alt_stride, i32 %x, i32 %y, double* %buffer) #0 {
entry:
  %coords.addr = alloca [2 x i32]*, align 4
  %num_coords.addr = alloca i32, align 4
  %data.addr = alloca i16*, align 4
  %denoised.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %sub_log2.addr = alloca i32*, align 4
  %alt_data.addr = alloca i16*, align 4
  %alt_denoised.addr = alloca i16*, align 4
  %alt_stride.addr = alloca i32, align 4
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %buffer.addr = alloca double*, align 4
  %i = alloca i32, align 4
  %x_i = alloca i32, align 4
  %y_i = alloca i32, align 4
  %val = alloca double, align 8
  %avg_data = alloca double, align 8
  %avg_denoised = alloca double, align 8
  %num_samples = alloca i32, align 4
  %dy_i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %y_up = alloca i32, align 4
  %dx_i = alloca i32, align 4
  %x_up = alloca i32, align 4
  store [2 x i32]* %coords, [2 x i32]** %coords.addr, align 4, !tbaa !2
  store i32 %num_coords, i32* %num_coords.addr, align 4, !tbaa !6
  store i16* %data, i16** %data.addr, align 4, !tbaa !2
  store i16* %denoised, i16** %denoised.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32* %sub_log2, i32** %sub_log2.addr, align 4, !tbaa !2
  store i16* %alt_data, i16** %alt_data.addr, align 4, !tbaa !2
  store i16* %alt_denoised, i16** %alt_denoised.addr, align 4, !tbaa !2
  store i32 %alt_stride, i32* %alt_stride.addr, align 4, !tbaa !6
  store i32 %x, i32* %x.addr, align 4, !tbaa !6
  store i32 %y, i32* %y.addr, align 4, !tbaa !6
  store double* %buffer, double** %buffer.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %num_coords.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %x_i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i32, i32* %x.addr, align 4, !tbaa !6
  %6 = load [2 x i32]*, [2 x i32]** %coords.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %6, i32 %7
  %arrayidx1 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 0
  %8 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %add = add nsw i32 %5, %8
  store i32 %add, i32* %x_i, align 4, !tbaa !6
  %9 = bitcast i32* %y_i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i32, i32* %y.addr, align 4, !tbaa !6
  %11 = load [2 x i32]*, [2 x i32]** %coords.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %11, i32 %12
  %arrayidx3 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx2, i32 0, i32 1
  %13 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %add4 = add nsw i32 %10, %13
  store i32 %add4, i32* %y_i, align 4, !tbaa !6
  %14 = load i16*, i16** %data.addr, align 4, !tbaa !2
  %15 = load i32, i32* %y_i, align 4, !tbaa !6
  %16 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %15, %16
  %17 = load i32, i32* %x_i, align 4, !tbaa !6
  %add5 = add nsw i32 %mul, %17
  %arrayidx6 = getelementptr inbounds i16, i16* %14, i32 %add5
  %18 = load i16, i16* %arrayidx6, align 2, !tbaa !33
  %conv = uitofp i16 %18 to double
  %19 = load i16*, i16** %denoised.addr, align 4, !tbaa !2
  %20 = load i32, i32* %y_i, align 4, !tbaa !6
  %21 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 %20, %21
  %22 = load i32, i32* %x_i, align 4, !tbaa !6
  %add8 = add nsw i32 %mul7, %22
  %arrayidx9 = getelementptr inbounds i16, i16* %19, i32 %add8
  %23 = load i16, i16* %arrayidx9, align 2, !tbaa !33
  %conv10 = zext i16 %23 to i32
  %conv11 = sitofp i32 %conv10 to double
  %sub = fsub double %conv, %conv11
  %24 = load double*, double** %buffer.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds double, double* %24, i32 %25
  store double %sub, double* %arrayidx12, align 8, !tbaa !11
  %26 = bitcast i32* %y_i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast i32* %x_i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %29 = bitcast double* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %29) #7
  %30 = load i16*, i16** %data.addr, align 4, !tbaa !2
  %31 = load i32, i32* %y.addr, align 4, !tbaa !6
  %32 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 %31, %32
  %33 = load i32, i32* %x.addr, align 4, !tbaa !6
  %add14 = add nsw i32 %mul13, %33
  %arrayidx15 = getelementptr inbounds i16, i16* %30, i32 %add14
  %34 = load i16, i16* %arrayidx15, align 2, !tbaa !33
  %conv16 = uitofp i16 %34 to double
  %35 = load i16*, i16** %denoised.addr, align 4, !tbaa !2
  %36 = load i32, i32* %y.addr, align 4, !tbaa !6
  %37 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 %36, %37
  %38 = load i32, i32* %x.addr, align 4, !tbaa !6
  %add18 = add nsw i32 %mul17, %38
  %arrayidx19 = getelementptr inbounds i16, i16* %35, i32 %add18
  %39 = load i16, i16* %arrayidx19, align 2, !tbaa !33
  %conv20 = zext i16 %39 to i32
  %conv21 = sitofp i32 %conv20 to double
  %sub22 = fsub double %conv16, %conv21
  store double %sub22, double* %val, align 8, !tbaa !11
  %40 = load i16*, i16** %alt_data.addr, align 4, !tbaa !2
  %tobool = icmp ne i16* %40, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.end
  %41 = load i16*, i16** %alt_denoised.addr, align 4, !tbaa !2
  %tobool23 = icmp ne i16* %41, null
  br i1 %tobool23, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %42 = bitcast double* %avg_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %42) #7
  store double 0.000000e+00, double* %avg_data, align 8, !tbaa !11
  %43 = bitcast double* %avg_denoised to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %43) #7
  store double 0.000000e+00, double* %avg_denoised, align 8, !tbaa !11
  %44 = bitcast i32* %num_samples to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  store i32 0, i32* %num_samples, align 4, !tbaa !6
  %45 = bitcast i32* %dy_i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #7
  store i32 0, i32* %dy_i, align 4, !tbaa !6
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc59, %if.then
  %46 = load i32, i32* %dy_i, align 4, !tbaa !6
  %47 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %47, i32 1
  %48 = load i32, i32* %arrayidx25, align 4, !tbaa !6
  %shl = shl i32 1, %48
  %cmp26 = icmp slt i32 %46, %shl
  br i1 %cmp26, label %for.body29, label %for.cond.cleanup28

for.cond.cleanup28:                               ; preds = %for.cond24
  store i32 5, i32* %cleanup.dest.slot, align 4
  %49 = bitcast i32* %dy_i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  br label %for.end61

for.body29:                                       ; preds = %for.cond24
  %50 = bitcast i32* %y_up to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load i32, i32* %y.addr, align 4, !tbaa !6
  %52 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %52, i32 1
  %53 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %shl31 = shl i32 %51, %53
  %54 = load i32, i32* %dy_i, align 4, !tbaa !6
  %add32 = add nsw i32 %shl31, %54
  store i32 %add32, i32* %y_up, align 4, !tbaa !6
  %55 = bitcast i32* %dx_i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #7
  store i32 0, i32* %dx_i, align 4, !tbaa !6
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc56, %for.body29
  %56 = load i32, i32* %dx_i, align 4, !tbaa !6
  %57 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %57, i32 0
  %58 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %shl35 = shl i32 1, %58
  %cmp36 = icmp slt i32 %56, %shl35
  br i1 %cmp36, label %for.body39, label %for.cond.cleanup38

for.cond.cleanup38:                               ; preds = %for.cond33
  store i32 8, i32* %cleanup.dest.slot, align 4
  %59 = bitcast i32* %dx_i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #7
  br label %for.end58

for.body39:                                       ; preds = %for.cond33
  %60 = bitcast i32* %x_up to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #7
  %61 = load i32, i32* %x.addr, align 4, !tbaa !6
  %62 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %62, i32 0
  %63 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %shl41 = shl i32 %61, %63
  %64 = load i32, i32* %dx_i, align 4, !tbaa !6
  %add42 = add nsw i32 %shl41, %64
  store i32 %add42, i32* %x_up, align 4, !tbaa !6
  %65 = load i16*, i16** %alt_data.addr, align 4, !tbaa !2
  %66 = load i32, i32* %y_up, align 4, !tbaa !6
  %67 = load i32, i32* %alt_stride.addr, align 4, !tbaa !6
  %mul43 = mul nsw i32 %66, %67
  %68 = load i32, i32* %x_up, align 4, !tbaa !6
  %add44 = add nsw i32 %mul43, %68
  %arrayidx45 = getelementptr inbounds i16, i16* %65, i32 %add44
  %69 = load i16, i16* %arrayidx45, align 2, !tbaa !33
  %conv46 = zext i16 %69 to i32
  %conv47 = sitofp i32 %conv46 to double
  %70 = load double, double* %avg_data, align 8, !tbaa !11
  %add48 = fadd double %70, %conv47
  store double %add48, double* %avg_data, align 8, !tbaa !11
  %71 = load i16*, i16** %alt_denoised.addr, align 4, !tbaa !2
  %72 = load i32, i32* %y_up, align 4, !tbaa !6
  %73 = load i32, i32* %alt_stride.addr, align 4, !tbaa !6
  %mul49 = mul nsw i32 %72, %73
  %74 = load i32, i32* %x_up, align 4, !tbaa !6
  %add50 = add nsw i32 %mul49, %74
  %arrayidx51 = getelementptr inbounds i16, i16* %71, i32 %add50
  %75 = load i16, i16* %arrayidx51, align 2, !tbaa !33
  %conv52 = zext i16 %75 to i32
  %conv53 = sitofp i32 %conv52 to double
  %76 = load double, double* %avg_denoised, align 8, !tbaa !11
  %add54 = fadd double %76, %conv53
  store double %add54, double* %avg_denoised, align 8, !tbaa !11
  %77 = load i32, i32* %num_samples, align 4, !tbaa !6
  %inc55 = add nsw i32 %77, 1
  store i32 %inc55, i32* %num_samples, align 4, !tbaa !6
  %78 = bitcast i32* %x_up to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #7
  br label %for.inc56

for.inc56:                                        ; preds = %for.body39
  %79 = load i32, i32* %dx_i, align 4, !tbaa !6
  %inc57 = add nsw i32 %79, 1
  store i32 %inc57, i32* %dx_i, align 4, !tbaa !6
  br label %for.cond33

for.end58:                                        ; preds = %for.cond.cleanup38
  %80 = bitcast i32* %y_up to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #7
  br label %for.inc59

for.inc59:                                        ; preds = %for.end58
  %81 = load i32, i32* %dy_i, align 4, !tbaa !6
  %inc60 = add nsw i32 %81, 1
  store i32 %inc60, i32* %dy_i, align 4, !tbaa !6
  br label %for.cond24

for.end61:                                        ; preds = %for.cond.cleanup28
  %82 = load double, double* %avg_data, align 8, !tbaa !11
  %83 = load double, double* %avg_denoised, align 8, !tbaa !11
  %sub62 = fsub double %82, %83
  %84 = load i32, i32* %num_samples, align 4, !tbaa !6
  %conv63 = sitofp i32 %84 to double
  %div = fdiv double %sub62, %conv63
  %85 = load double*, double** %buffer.addr, align 4, !tbaa !2
  %86 = load i32, i32* %num_coords.addr, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds double, double* %85, i32 %86
  store double %div, double* %arrayidx64, align 8, !tbaa !11
  %87 = bitcast i32* %num_samples to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #7
  %88 = bitcast double* %avg_denoised to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %88) #7
  %89 = bitcast double* %avg_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %89) #7
  br label %if.end

if.end:                                           ; preds = %for.end61, %land.lhs.true, %for.end
  %90 = load double, double* %val, align 8, !tbaa !11
  store i32 1, i32* %cleanup.dest.slot, align 4
  %91 = bitcast double* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %91) #7
  ret double %90
}

; Function Attrs: nounwind
define internal double @extract_ar_row_lowbd([2 x i32]* %coords, i32 %num_coords, i8* %data, i8* %denoised, i32 %stride, i32* %sub_log2, i8* %alt_data, i8* %alt_denoised, i32 %alt_stride, i32 %x, i32 %y, double* %buffer) #0 {
entry:
  %coords.addr = alloca [2 x i32]*, align 4
  %num_coords.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  %denoised.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %sub_log2.addr = alloca i32*, align 4
  %alt_data.addr = alloca i8*, align 4
  %alt_denoised.addr = alloca i8*, align 4
  %alt_stride.addr = alloca i32, align 4
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %buffer.addr = alloca double*, align 4
  %i = alloca i32, align 4
  %x_i = alloca i32, align 4
  %y_i = alloca i32, align 4
  %val = alloca double, align 8
  %avg_data = alloca double, align 8
  %avg_denoised = alloca double, align 8
  %num_samples = alloca i32, align 4
  %dy_i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %y_up = alloca i32, align 4
  %dx_i = alloca i32, align 4
  %x_up = alloca i32, align 4
  store [2 x i32]* %coords, [2 x i32]** %coords.addr, align 4, !tbaa !2
  store i32 %num_coords, i32* %num_coords.addr, align 4, !tbaa !6
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i8* %denoised, i8** %denoised.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32* %sub_log2, i32** %sub_log2.addr, align 4, !tbaa !2
  store i8* %alt_data, i8** %alt_data.addr, align 4, !tbaa !2
  store i8* %alt_denoised, i8** %alt_denoised.addr, align 4, !tbaa !2
  store i32 %alt_stride, i32* %alt_stride.addr, align 4, !tbaa !6
  store i32 %x, i32* %x.addr, align 4, !tbaa !6
  store i32 %y, i32* %y.addr, align 4, !tbaa !6
  store double* %buffer, double** %buffer.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %num_coords.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %x_i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i32, i32* %x.addr, align 4, !tbaa !6
  %6 = load [2 x i32]*, [2 x i32]** %coords.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %6, i32 %7
  %arrayidx1 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 0
  %8 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %add = add nsw i32 %5, %8
  store i32 %add, i32* %x_i, align 4, !tbaa !6
  %9 = bitcast i32* %y_i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i32, i32* %y.addr, align 4, !tbaa !6
  %11 = load [2 x i32]*, [2 x i32]** %coords.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %11, i32 %12
  %arrayidx3 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx2, i32 0, i32 1
  %13 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %add4 = add nsw i32 %10, %13
  store i32 %add4, i32* %y_i, align 4, !tbaa !6
  %14 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %15 = load i32, i32* %y_i, align 4, !tbaa !6
  %16 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %15, %16
  %17 = load i32, i32* %x_i, align 4, !tbaa !6
  %add5 = add nsw i32 %mul, %17
  %arrayidx6 = getelementptr inbounds i8, i8* %14, i32 %add5
  %18 = load i8, i8* %arrayidx6, align 1, !tbaa !35
  %conv = uitofp i8 %18 to double
  %19 = load i8*, i8** %denoised.addr, align 4, !tbaa !2
  %20 = load i32, i32* %y_i, align 4, !tbaa !6
  %21 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 %20, %21
  %22 = load i32, i32* %x_i, align 4, !tbaa !6
  %add8 = add nsw i32 %mul7, %22
  %arrayidx9 = getelementptr inbounds i8, i8* %19, i32 %add8
  %23 = load i8, i8* %arrayidx9, align 1, !tbaa !35
  %conv10 = zext i8 %23 to i32
  %conv11 = sitofp i32 %conv10 to double
  %sub = fsub double %conv, %conv11
  %24 = load double*, double** %buffer.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds double, double* %24, i32 %25
  store double %sub, double* %arrayidx12, align 8, !tbaa !11
  %26 = bitcast i32* %y_i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast i32* %x_i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %29 = bitcast double* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %29) #7
  %30 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %31 = load i32, i32* %y.addr, align 4, !tbaa !6
  %32 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 %31, %32
  %33 = load i32, i32* %x.addr, align 4, !tbaa !6
  %add14 = add nsw i32 %mul13, %33
  %arrayidx15 = getelementptr inbounds i8, i8* %30, i32 %add14
  %34 = load i8, i8* %arrayidx15, align 1, !tbaa !35
  %conv16 = uitofp i8 %34 to double
  %35 = load i8*, i8** %denoised.addr, align 4, !tbaa !2
  %36 = load i32, i32* %y.addr, align 4, !tbaa !6
  %37 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 %36, %37
  %38 = load i32, i32* %x.addr, align 4, !tbaa !6
  %add18 = add nsw i32 %mul17, %38
  %arrayidx19 = getelementptr inbounds i8, i8* %35, i32 %add18
  %39 = load i8, i8* %arrayidx19, align 1, !tbaa !35
  %conv20 = zext i8 %39 to i32
  %conv21 = sitofp i32 %conv20 to double
  %sub22 = fsub double %conv16, %conv21
  store double %sub22, double* %val, align 8, !tbaa !11
  %40 = load i8*, i8** %alt_data.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %40, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.end
  %41 = load i8*, i8** %alt_denoised.addr, align 4, !tbaa !2
  %tobool23 = icmp ne i8* %41, null
  br i1 %tobool23, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %42 = bitcast double* %avg_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %42) #7
  store double 0.000000e+00, double* %avg_data, align 8, !tbaa !11
  %43 = bitcast double* %avg_denoised to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %43) #7
  store double 0.000000e+00, double* %avg_denoised, align 8, !tbaa !11
  %44 = bitcast i32* %num_samples to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  store i32 0, i32* %num_samples, align 4, !tbaa !6
  %45 = bitcast i32* %dy_i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #7
  store i32 0, i32* %dy_i, align 4, !tbaa !6
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc59, %if.then
  %46 = load i32, i32* %dy_i, align 4, !tbaa !6
  %47 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %47, i32 1
  %48 = load i32, i32* %arrayidx25, align 4, !tbaa !6
  %shl = shl i32 1, %48
  %cmp26 = icmp slt i32 %46, %shl
  br i1 %cmp26, label %for.body29, label %for.cond.cleanup28

for.cond.cleanup28:                               ; preds = %for.cond24
  store i32 5, i32* %cleanup.dest.slot, align 4
  %49 = bitcast i32* %dy_i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  br label %for.end61

for.body29:                                       ; preds = %for.cond24
  %50 = bitcast i32* %y_up to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load i32, i32* %y.addr, align 4, !tbaa !6
  %52 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %52, i32 1
  %53 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %shl31 = shl i32 %51, %53
  %54 = load i32, i32* %dy_i, align 4, !tbaa !6
  %add32 = add nsw i32 %shl31, %54
  store i32 %add32, i32* %y_up, align 4, !tbaa !6
  %55 = bitcast i32* %dx_i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #7
  store i32 0, i32* %dx_i, align 4, !tbaa !6
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc56, %for.body29
  %56 = load i32, i32* %dx_i, align 4, !tbaa !6
  %57 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %57, i32 0
  %58 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %shl35 = shl i32 1, %58
  %cmp36 = icmp slt i32 %56, %shl35
  br i1 %cmp36, label %for.body39, label %for.cond.cleanup38

for.cond.cleanup38:                               ; preds = %for.cond33
  store i32 8, i32* %cleanup.dest.slot, align 4
  %59 = bitcast i32* %dx_i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #7
  br label %for.end58

for.body39:                                       ; preds = %for.cond33
  %60 = bitcast i32* %x_up to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #7
  %61 = load i32, i32* %x.addr, align 4, !tbaa !6
  %62 = load i32*, i32** %sub_log2.addr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %62, i32 0
  %63 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %shl41 = shl i32 %61, %63
  %64 = load i32, i32* %dx_i, align 4, !tbaa !6
  %add42 = add nsw i32 %shl41, %64
  store i32 %add42, i32* %x_up, align 4, !tbaa !6
  %65 = load i8*, i8** %alt_data.addr, align 4, !tbaa !2
  %66 = load i32, i32* %y_up, align 4, !tbaa !6
  %67 = load i32, i32* %alt_stride.addr, align 4, !tbaa !6
  %mul43 = mul nsw i32 %66, %67
  %68 = load i32, i32* %x_up, align 4, !tbaa !6
  %add44 = add nsw i32 %mul43, %68
  %arrayidx45 = getelementptr inbounds i8, i8* %65, i32 %add44
  %69 = load i8, i8* %arrayidx45, align 1, !tbaa !35
  %conv46 = zext i8 %69 to i32
  %conv47 = sitofp i32 %conv46 to double
  %70 = load double, double* %avg_data, align 8, !tbaa !11
  %add48 = fadd double %70, %conv47
  store double %add48, double* %avg_data, align 8, !tbaa !11
  %71 = load i8*, i8** %alt_denoised.addr, align 4, !tbaa !2
  %72 = load i32, i32* %y_up, align 4, !tbaa !6
  %73 = load i32, i32* %alt_stride.addr, align 4, !tbaa !6
  %mul49 = mul nsw i32 %72, %73
  %74 = load i32, i32* %x_up, align 4, !tbaa !6
  %add50 = add nsw i32 %mul49, %74
  %arrayidx51 = getelementptr inbounds i8, i8* %71, i32 %add50
  %75 = load i8, i8* %arrayidx51, align 1, !tbaa !35
  %conv52 = zext i8 %75 to i32
  %conv53 = sitofp i32 %conv52 to double
  %76 = load double, double* %avg_denoised, align 8, !tbaa !11
  %add54 = fadd double %76, %conv53
  store double %add54, double* %avg_denoised, align 8, !tbaa !11
  %77 = load i32, i32* %num_samples, align 4, !tbaa !6
  %inc55 = add nsw i32 %77, 1
  store i32 %inc55, i32* %num_samples, align 4, !tbaa !6
  %78 = bitcast i32* %x_up to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #7
  br label %for.inc56

for.inc56:                                        ; preds = %for.body39
  %79 = load i32, i32* %dx_i, align 4, !tbaa !6
  %inc57 = add nsw i32 %79, 1
  store i32 %inc57, i32* %dx_i, align 4, !tbaa !6
  br label %for.cond33

for.end58:                                        ; preds = %for.cond.cleanup38
  %80 = bitcast i32* %y_up to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #7
  br label %for.inc59

for.inc59:                                        ; preds = %for.end58
  %81 = load i32, i32* %dy_i, align 4, !tbaa !6
  %inc60 = add nsw i32 %81, 1
  store i32 %inc60, i32* %dy_i, align 4, !tbaa !6
  br label %for.cond24

for.end61:                                        ; preds = %for.cond.cleanup28
  %82 = load double, double* %avg_data, align 8, !tbaa !11
  %83 = load double, double* %avg_denoised, align 8, !tbaa !11
  %sub62 = fsub double %82, %83
  %84 = load i32, i32* %num_samples, align 4, !tbaa !6
  %conv63 = sitofp i32 %84 to double
  %div = fdiv double %sub62, %conv63
  %85 = load double*, double** %buffer.addr, align 4, !tbaa !2
  %86 = load i32, i32* %num_coords.addr, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds double, double* %85, i32 %86
  store double %div, double* %arrayidx64, align 8, !tbaa !11
  %87 = bitcast i32* %num_samples to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #7
  %88 = bitcast double* %avg_denoised to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %88) #7
  %89 = bitcast double* %avg_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %89) #7
  br label %if.end

if.end:                                           ; preds = %for.end61, %land.lhs.true, %for.end
  %90 = load double, double* %val, align 8, !tbaa !11
  store i32 1, i32* %cleanup.dest.slot, align 4
  %91 = bitcast double* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %91) #7
  ret double %90
}

; Function Attrs: inlinehint nounwind
define internal double @get_block_mean(i8* %data, i32 %w, i32 %h, i32 %stride, i32 %x_o, i32 %y_o, i32 %block_size, i32 %use_highbd) #5 {
entry:
  %retval = alloca double, align 8
  %data.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %x_o.addr = alloca i32, align 4
  %y_o.addr = alloca i32, align 4
  %block_size.addr = alloca i32, align 4
  %use_highbd.addr = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %x_o, i32* %x_o.addr, align 4, !tbaa !6
  store i32 %y_o, i32* %y_o.addr, align 4, !tbaa !6
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  store i32 %use_highbd, i32* %use_highbd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to i16*
  %3 = load i32, i32* %w.addr, align 4, !tbaa !6
  %4 = load i32, i32* %h.addr, align 4, !tbaa !6
  %5 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %6 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %7 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %8 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %call = call double @get_block_mean_highbd(i16* %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7, i32 %8)
  store double %call, double* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %9 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %10 = load i32, i32* %w.addr, align 4, !tbaa !6
  %11 = load i32, i32* %h.addr, align 4, !tbaa !6
  %12 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %13 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %14 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %15 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %call1 = call double @get_block_mean_lowbd(i8* %9, i32 %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15)
  store double %call1, double* %retval, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  %16 = load double, double* %retval, align 8
  ret double %16
}

; Function Attrs: inlinehint nounwind
define internal double @get_noise_var(i8* %data, i8* %denoised, i32 %w, i32 %h, i32 %stride, i32 %x_o, i32 %y_o, i32 %block_size_x, i32 %block_size_y, i32 %use_highbd) #5 {
entry:
  %retval = alloca double, align 8
  %data.addr = alloca i8*, align 4
  %denoised.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %x_o.addr = alloca i32, align 4
  %y_o.addr = alloca i32, align 4
  %block_size_x.addr = alloca i32, align 4
  %block_size_y.addr = alloca i32, align 4
  %use_highbd.addr = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i8* %denoised, i8** %denoised.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %x_o, i32* %x_o.addr, align 4, !tbaa !6
  store i32 %y_o, i32* %y_o.addr, align 4, !tbaa !6
  store i32 %block_size_x, i32* %block_size_x.addr, align 4, !tbaa !6
  store i32 %block_size_y, i32* %block_size_y.addr, align 4, !tbaa !6
  store i32 %use_highbd, i32* %use_highbd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to i16*
  %3 = load i8*, i8** %denoised.addr, align 4, !tbaa !2
  %4 = bitcast i8* %3 to i16*
  %5 = load i32, i32* %w.addr, align 4, !tbaa !6
  %6 = load i32, i32* %h.addr, align 4, !tbaa !6
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %8 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %9 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %10 = load i32, i32* %block_size_x.addr, align 4, !tbaa !6
  %11 = load i32, i32* %block_size_y.addr, align 4, !tbaa !6
  %call = call double @get_noise_var_highbd(i16* %2, i16* %4, i32 %5, i32 %6, i32 %7, i32 %8, i32 %9, i32 %10, i32 %11)
  store double %call, double* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %12 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %13 = load i8*, i8** %denoised.addr, align 4, !tbaa !2
  %14 = load i32, i32* %w.addr, align 4, !tbaa !6
  %15 = load i32, i32* %h.addr, align 4, !tbaa !6
  %16 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %17 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %18 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %19 = load i32, i32* %block_size_x.addr, align 4, !tbaa !6
  %20 = load i32, i32* %block_size_y.addr, align 4, !tbaa !6
  %call1 = call double @get_noise_var_lowbd(i8* %12, i8* %13, i32 %14, i32 %15, i32 %16, i32 %17, i32 %18, i32 %19, i32 %20)
  store double %call1, double* %retval, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  %21 = load double, double* %retval, align 8
  ret double %21
}

; Function Attrs: nounwind
define internal double @noise_strength_solver_get_value(%struct.aom_noise_strength_solver_t* %solver, double %x) #0 {
entry:
  %solver.addr = alloca %struct.aom_noise_strength_solver_t*, align 4
  %x.addr = alloca double, align 8
  %bin = alloca double, align 8
  %bin_i0 = alloca i32, align 4
  %bin_i1 = alloca i32, align 4
  %a = alloca double, align 8
  store %struct.aom_noise_strength_solver_t* %solver, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  store double %x, double* %x.addr, align 8, !tbaa !11
  %0 = bitcast double* %bin to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %1 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %2 = load double, double* %x.addr, align 8, !tbaa !11
  %call = call double @noise_strength_solver_get_bin_index(%struct.aom_noise_strength_solver_t* %1, double %2)
  store double %call, double* %bin, align 8, !tbaa !11
  %3 = bitcast i32* %bin_i0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load double, double* %bin, align 8, !tbaa !11
  %5 = call double @llvm.floor.f64(double %4)
  %conv = fptosi double %5 to i32
  store i32 %conv, i32* %bin_i0, align 4, !tbaa !6
  %6 = bitcast i32* %bin_i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %7, i32 0, i32 3
  %8 = load i32, i32* %num_bins, align 8, !tbaa !13
  %sub = sub nsw i32 %8, 1
  %9 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %add = add nsw i32 %9, 1
  %cmp = icmp slt i32 %sub, %add
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %num_bins2 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %10, i32 0, i32 3
  %11 = load i32, i32* %num_bins2, align 8, !tbaa !13
  %sub3 = sub nsw i32 %11, 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %12 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %add4 = add nsw i32 %12, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub3, %cond.true ], [ %add4, %cond.false ]
  store i32 %cond, i32* %bin_i1, align 4, !tbaa !6
  %13 = bitcast double* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #7
  %14 = load double, double* %bin, align 8, !tbaa !11
  %15 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %conv5 = sitofp i32 %15 to double
  %sub6 = fsub double %14, %conv5
  store double %sub6, double* %a, align 8, !tbaa !11
  %16 = load double, double* %a, align 8, !tbaa !11
  %sub7 = fsub double 1.000000e+00, %16
  %17 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %17, i32 0, i32 0
  %x8 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns, i32 0, i32 2
  %18 = load double*, double** %x8, align 8, !tbaa !26
  %19 = load i32, i32* %bin_i0, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds double, double* %18, i32 %19
  %20 = load double, double* %arrayidx, align 8, !tbaa !11
  %mul = fmul double %sub7, %20
  %21 = load double, double* %a, align 8, !tbaa !11
  %22 = load %struct.aom_noise_strength_solver_t*, %struct.aom_noise_strength_solver_t** %solver.addr, align 4, !tbaa !2
  %eqns9 = getelementptr inbounds %struct.aom_noise_strength_solver_t, %struct.aom_noise_strength_solver_t* %22, i32 0, i32 0
  %x10 = getelementptr inbounds %struct.aom_equation_system_t, %struct.aom_equation_system_t* %eqns9, i32 0, i32 2
  %23 = load double*, double** %x10, align 8, !tbaa !26
  %24 = load i32, i32* %bin_i1, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds double, double* %23, i32 %24
  %25 = load double, double* %arrayidx11, align 8, !tbaa !11
  %mul12 = fmul double %21, %25
  %add13 = fadd double %mul, %mul12
  %26 = bitcast double* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %26) #7
  %27 = bitcast i32* %bin_i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  %28 = bitcast i32* %bin_i0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #7
  %29 = bitcast double* %bin to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %29) #7
  ret double %add13
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #4

; Function Attrs: nounwind
define internal double @get_block_mean_highbd(i16* %data, i32 %w, i32 %h, i32 %stride, i32 %x_o, i32 %y_o, i32 %block_size) #0 {
entry:
  %data.addr = alloca i16*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %x_o.addr = alloca i32, align 4
  %y_o.addr = alloca i32, align 4
  %block_size.addr = alloca i32, align 4
  %max_h = alloca i32, align 4
  %max_w = alloca i32, align 4
  %block_mean = alloca double, align 8
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  store i16* %data, i16** %data.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %x_o, i32* %x_o.addr, align 4, !tbaa !6
  store i32 %y_o, i32* %y_o.addr, align 4, !tbaa !6
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  %0 = bitcast i32* %max_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %h.addr, align 4, !tbaa !6
  %2 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %1, %2
  %3 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %sub, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32, i32* %h.addr, align 4, !tbaa !6
  %5 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 %4, %5
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub1, %cond.true ], [ %6, %cond.false ]
  store i32 %cond, i32* %max_h, align 4, !tbaa !6
  %7 = bitcast i32* %max_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load i32, i32* %w.addr, align 4, !tbaa !6
  %9 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %sub2 = sub nsw i32 %8, %9
  %10 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %sub2, %10
  br i1 %cmp3, label %cond.true4, label %cond.false6

cond.true4:                                       ; preds = %cond.end
  %11 = load i32, i32* %w.addr, align 4, !tbaa !6
  %12 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %sub5 = sub nsw i32 %11, %12
  br label %cond.end7

cond.false6:                                      ; preds = %cond.end
  %13 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  br label %cond.end7

cond.end7:                                        ; preds = %cond.false6, %cond.true4
  %cond8 = phi i32 [ %sub5, %cond.true4 ], [ %13, %cond.false6 ]
  store i32 %cond8, i32* %max_w, align 4, !tbaa !6
  %14 = bitcast double* %block_mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %14) #7
  store double 0.000000e+00, double* %block_mean, align 8, !tbaa !11
  %15 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc18, %cond.end7
  %16 = load i32, i32* %y, align 4, !tbaa !6
  %17 = load i32, i32* %max_h, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %16, %17
  br i1 %cmp9, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %18 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  br label %for.end20

for.body:                                         ; preds = %for.cond
  %19 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body
  %20 = load i32, i32* %x, align 4, !tbaa !6
  %21 = load i32, i32* %max_w, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %20, %21
  br i1 %cmp11, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond10
  store i32 5, i32* %cleanup.dest.slot, align 4
  %22 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  br label %for.end

for.body13:                                       ; preds = %for.cond10
  %23 = load i16*, i16** %data.addr, align 4, !tbaa !2
  %24 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %25 = load i32, i32* %y, align 4, !tbaa !6
  %add = add nsw i32 %24, %25
  %26 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add, %26
  %27 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %add14 = add nsw i32 %mul, %27
  %28 = load i32, i32* %x, align 4, !tbaa !6
  %add15 = add nsw i32 %add14, %28
  %arrayidx = getelementptr inbounds i16, i16* %23, i32 %add15
  %29 = load i16, i16* %arrayidx, align 2, !tbaa !33
  %conv = zext i16 %29 to i32
  %conv16 = sitofp i32 %conv to double
  %30 = load double, double* %block_mean, align 8, !tbaa !11
  %add17 = fadd double %30, %conv16
  store double %add17, double* %block_mean, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %31 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond10

for.end:                                          ; preds = %for.cond.cleanup12
  br label %for.inc18

for.inc18:                                        ; preds = %for.end
  %32 = load i32, i32* %y, align 4, !tbaa !6
  %inc19 = add nsw i32 %32, 1
  store i32 %inc19, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end20:                                        ; preds = %for.cond.cleanup
  %33 = load double, double* %block_mean, align 8, !tbaa !11
  %34 = load i32, i32* %max_w, align 4, !tbaa !6
  %35 = load i32, i32* %max_h, align 4, !tbaa !6
  %mul21 = mul nsw i32 %34, %35
  %conv22 = sitofp i32 %mul21 to double
  %div = fdiv double %33, %conv22
  store i32 1, i32* %cleanup.dest.slot, align 4
  %36 = bitcast double* %block_mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %36) #7
  %37 = bitcast i32* %max_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast i32* %max_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  ret double %div
}

; Function Attrs: nounwind
define internal double @get_block_mean_lowbd(i8* %data, i32 %w, i32 %h, i32 %stride, i32 %x_o, i32 %y_o, i32 %block_size) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %x_o.addr = alloca i32, align 4
  %y_o.addr = alloca i32, align 4
  %block_size.addr = alloca i32, align 4
  %max_h = alloca i32, align 4
  %max_w = alloca i32, align 4
  %block_mean = alloca double, align 8
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %x_o, i32* %x_o.addr, align 4, !tbaa !6
  store i32 %y_o, i32* %y_o.addr, align 4, !tbaa !6
  store i32 %block_size, i32* %block_size.addr, align 4, !tbaa !6
  %0 = bitcast i32* %max_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %h.addr, align 4, !tbaa !6
  %2 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %1, %2
  %3 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %sub, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32, i32* %h.addr, align 4, !tbaa !6
  %5 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 %4, %5
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub1, %cond.true ], [ %6, %cond.false ]
  store i32 %cond, i32* %max_h, align 4, !tbaa !6
  %7 = bitcast i32* %max_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load i32, i32* %w.addr, align 4, !tbaa !6
  %9 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %sub2 = sub nsw i32 %8, %9
  %10 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %sub2, %10
  br i1 %cmp3, label %cond.true4, label %cond.false6

cond.true4:                                       ; preds = %cond.end
  %11 = load i32, i32* %w.addr, align 4, !tbaa !6
  %12 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %sub5 = sub nsw i32 %11, %12
  br label %cond.end7

cond.false6:                                      ; preds = %cond.end
  %13 = load i32, i32* %block_size.addr, align 4, !tbaa !6
  br label %cond.end7

cond.end7:                                        ; preds = %cond.false6, %cond.true4
  %cond8 = phi i32 [ %sub5, %cond.true4 ], [ %13, %cond.false6 ]
  store i32 %cond8, i32* %max_w, align 4, !tbaa !6
  %14 = bitcast double* %block_mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %14) #7
  store double 0.000000e+00, double* %block_mean, align 8, !tbaa !11
  %15 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc18, %cond.end7
  %16 = load i32, i32* %y, align 4, !tbaa !6
  %17 = load i32, i32* %max_h, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %16, %17
  br i1 %cmp9, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %18 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  br label %for.end20

for.body:                                         ; preds = %for.cond
  %19 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body
  %20 = load i32, i32* %x, align 4, !tbaa !6
  %21 = load i32, i32* %max_w, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %20, %21
  br i1 %cmp11, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond10
  store i32 5, i32* %cleanup.dest.slot, align 4
  %22 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  br label %for.end

for.body13:                                       ; preds = %for.cond10
  %23 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %24 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %25 = load i32, i32* %y, align 4, !tbaa !6
  %add = add nsw i32 %24, %25
  %26 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add, %26
  %27 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %add14 = add nsw i32 %mul, %27
  %28 = load i32, i32* %x, align 4, !tbaa !6
  %add15 = add nsw i32 %add14, %28
  %arrayidx = getelementptr inbounds i8, i8* %23, i32 %add15
  %29 = load i8, i8* %arrayidx, align 1, !tbaa !35
  %conv = zext i8 %29 to i32
  %conv16 = sitofp i32 %conv to double
  %30 = load double, double* %block_mean, align 8, !tbaa !11
  %add17 = fadd double %30, %conv16
  store double %add17, double* %block_mean, align 8, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %31 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond10

for.end:                                          ; preds = %for.cond.cleanup12
  br label %for.inc18

for.inc18:                                        ; preds = %for.end
  %32 = load i32, i32* %y, align 4, !tbaa !6
  %inc19 = add nsw i32 %32, 1
  store i32 %inc19, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end20:                                        ; preds = %for.cond.cleanup
  %33 = load double, double* %block_mean, align 8, !tbaa !11
  %34 = load i32, i32* %max_w, align 4, !tbaa !6
  %35 = load i32, i32* %max_h, align 4, !tbaa !6
  %mul21 = mul nsw i32 %34, %35
  %conv22 = sitofp i32 %mul21 to double
  %div = fdiv double %33, %conv22
  store i32 1, i32* %cleanup.dest.slot, align 4
  %36 = bitcast double* %block_mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %36) #7
  %37 = bitcast i32* %max_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast i32* %max_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  ret double %div
}

; Function Attrs: nounwind
define internal double @get_noise_var_highbd(i16* %data, i16* %denoised, i32 %stride, i32 %w, i32 %h, i32 %x_o, i32 %y_o, i32 %block_size_x, i32 %block_size_y) #0 {
entry:
  %data.addr = alloca i16*, align 4
  %denoised.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %x_o.addr = alloca i32, align 4
  %y_o.addr = alloca i32, align 4
  %block_size_x.addr = alloca i32, align 4
  %block_size_y.addr = alloca i32, align 4
  %max_h = alloca i32, align 4
  %max_w = alloca i32, align 4
  %noise_var = alloca double, align 8
  %noise_mean = alloca double, align 8
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %noise = alloca double, align 8
  store i16* %data, i16** %data.addr, align 4, !tbaa !2
  store i16* %denoised, i16** %denoised.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %x_o, i32* %x_o.addr, align 4, !tbaa !6
  store i32 %y_o, i32* %y_o.addr, align 4, !tbaa !6
  store i32 %block_size_x, i32* %block_size_x.addr, align 4, !tbaa !6
  store i32 %block_size_y, i32* %block_size_y.addr, align 4, !tbaa !6
  %0 = bitcast i32* %max_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %h.addr, align 4, !tbaa !6
  %2 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %1, %2
  %3 = load i32, i32* %block_size_y.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %sub, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32, i32* %h.addr, align 4, !tbaa !6
  %5 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 %4, %5
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i32, i32* %block_size_y.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub1, %cond.true ], [ %6, %cond.false ]
  store i32 %cond, i32* %max_h, align 4, !tbaa !6
  %7 = bitcast i32* %max_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load i32, i32* %w.addr, align 4, !tbaa !6
  %9 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %sub2 = sub nsw i32 %8, %9
  %10 = load i32, i32* %block_size_x.addr, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %sub2, %10
  br i1 %cmp3, label %cond.true4, label %cond.false6

cond.true4:                                       ; preds = %cond.end
  %11 = load i32, i32* %w.addr, align 4, !tbaa !6
  %12 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %sub5 = sub nsw i32 %11, %12
  br label %cond.end7

cond.false6:                                      ; preds = %cond.end
  %13 = load i32, i32* %block_size_x.addr, align 4, !tbaa !6
  br label %cond.end7

cond.end7:                                        ; preds = %cond.false6, %cond.true4
  %cond8 = phi i32 [ %sub5, %cond.true4 ], [ %13, %cond.false6 ]
  store i32 %cond8, i32* %max_w, align 4, !tbaa !6
  %14 = bitcast double* %noise_var to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %14) #7
  store double 0.000000e+00, double* %noise_var, align 8, !tbaa !11
  %15 = bitcast double* %noise_mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %15) #7
  store double 0.000000e+00, double* %noise_mean, align 8, !tbaa !11
  %16 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc27, %cond.end7
  %17 = load i32, i32* %y, align 4, !tbaa !6
  %18 = load i32, i32* %max_h, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %17, %18
  br i1 %cmp9, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  br label %for.end29

for.body:                                         ; preds = %for.cond
  %20 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body
  %21 = load i32, i32* %x, align 4, !tbaa !6
  %22 = load i32, i32* %max_w, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %21, %22
  br i1 %cmp11, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond10
  store i32 5, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  br label %for.end

for.body13:                                       ; preds = %for.cond10
  %24 = bitcast double* %noise to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %24) #7
  %25 = load i16*, i16** %data.addr, align 4, !tbaa !2
  %26 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %27 = load i32, i32* %y, align 4, !tbaa !6
  %add = add nsw i32 %26, %27
  %28 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add, %28
  %29 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %add14 = add nsw i32 %mul, %29
  %30 = load i32, i32* %x, align 4, !tbaa !6
  %add15 = add nsw i32 %add14, %30
  %arrayidx = getelementptr inbounds i16, i16* %25, i32 %add15
  %31 = load i16, i16* %arrayidx, align 2, !tbaa !33
  %conv = uitofp i16 %31 to double
  %32 = load i16*, i16** %denoised.addr, align 4, !tbaa !2
  %33 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %34 = load i32, i32* %y, align 4, !tbaa !6
  %add16 = add nsw i32 %33, %34
  %35 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 %add16, %35
  %36 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %add18 = add nsw i32 %mul17, %36
  %37 = load i32, i32* %x, align 4, !tbaa !6
  %add19 = add nsw i32 %add18, %37
  %arrayidx20 = getelementptr inbounds i16, i16* %32, i32 %add19
  %38 = load i16, i16* %arrayidx20, align 2, !tbaa !33
  %conv21 = zext i16 %38 to i32
  %conv22 = sitofp i32 %conv21 to double
  %sub23 = fsub double %conv, %conv22
  store double %sub23, double* %noise, align 8, !tbaa !11
  %39 = load double, double* %noise, align 8, !tbaa !11
  %40 = load double, double* %noise_mean, align 8, !tbaa !11
  %add24 = fadd double %40, %39
  store double %add24, double* %noise_mean, align 8, !tbaa !11
  %41 = load double, double* %noise, align 8, !tbaa !11
  %42 = load double, double* %noise, align 8, !tbaa !11
  %mul25 = fmul double %41, %42
  %43 = load double, double* %noise_var, align 8, !tbaa !11
  %add26 = fadd double %43, %mul25
  store double %add26, double* %noise_var, align 8, !tbaa !11
  %44 = bitcast double* %noise to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %44) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %45 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond10

for.end:                                          ; preds = %for.cond.cleanup12
  br label %for.inc27

for.inc27:                                        ; preds = %for.end
  %46 = load i32, i32* %y, align 4, !tbaa !6
  %inc28 = add nsw i32 %46, 1
  store i32 %inc28, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end29:                                        ; preds = %for.cond.cleanup
  %47 = load i32, i32* %max_w, align 4, !tbaa !6
  %48 = load i32, i32* %max_h, align 4, !tbaa !6
  %mul30 = mul nsw i32 %47, %48
  %conv31 = sitofp i32 %mul30 to double
  %49 = load double, double* %noise_mean, align 8, !tbaa !11
  %div = fdiv double %49, %conv31
  store double %div, double* %noise_mean, align 8, !tbaa !11
  %50 = load double, double* %noise_var, align 8, !tbaa !11
  %51 = load i32, i32* %max_w, align 4, !tbaa !6
  %52 = load i32, i32* %max_h, align 4, !tbaa !6
  %mul32 = mul nsw i32 %51, %52
  %conv33 = sitofp i32 %mul32 to double
  %div34 = fdiv double %50, %conv33
  %53 = load double, double* %noise_mean, align 8, !tbaa !11
  %54 = load double, double* %noise_mean, align 8, !tbaa !11
  %mul35 = fmul double %53, %54
  %sub36 = fsub double %div34, %mul35
  store i32 1, i32* %cleanup.dest.slot, align 4
  %55 = bitcast double* %noise_mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %55) #7
  %56 = bitcast double* %noise_var to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %56) #7
  %57 = bitcast i32* %max_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %58 = bitcast i32* %max_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #7
  ret double %sub36
}

; Function Attrs: nounwind
define internal double @get_noise_var_lowbd(i8* %data, i8* %denoised, i32 %stride, i32 %w, i32 %h, i32 %x_o, i32 %y_o, i32 %block_size_x, i32 %block_size_y) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %denoised.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %x_o.addr = alloca i32, align 4
  %y_o.addr = alloca i32, align 4
  %block_size_x.addr = alloca i32, align 4
  %block_size_y.addr = alloca i32, align 4
  %max_h = alloca i32, align 4
  %max_w = alloca i32, align 4
  %noise_var = alloca double, align 8
  %noise_mean = alloca double, align 8
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %noise = alloca double, align 8
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i8* %denoised, i8** %denoised.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %x_o, i32* %x_o.addr, align 4, !tbaa !6
  store i32 %y_o, i32* %y_o.addr, align 4, !tbaa !6
  store i32 %block_size_x, i32* %block_size_x.addr, align 4, !tbaa !6
  store i32 %block_size_y, i32* %block_size_y.addr, align 4, !tbaa !6
  %0 = bitcast i32* %max_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %h.addr, align 4, !tbaa !6
  %2 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %1, %2
  %3 = load i32, i32* %block_size_y.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %sub, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32, i32* %h.addr, align 4, !tbaa !6
  %5 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 %4, %5
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i32, i32* %block_size_y.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub1, %cond.true ], [ %6, %cond.false ]
  store i32 %cond, i32* %max_h, align 4, !tbaa !6
  %7 = bitcast i32* %max_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load i32, i32* %w.addr, align 4, !tbaa !6
  %9 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %sub2 = sub nsw i32 %8, %9
  %10 = load i32, i32* %block_size_x.addr, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %sub2, %10
  br i1 %cmp3, label %cond.true4, label %cond.false6

cond.true4:                                       ; preds = %cond.end
  %11 = load i32, i32* %w.addr, align 4, !tbaa !6
  %12 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %sub5 = sub nsw i32 %11, %12
  br label %cond.end7

cond.false6:                                      ; preds = %cond.end
  %13 = load i32, i32* %block_size_x.addr, align 4, !tbaa !6
  br label %cond.end7

cond.end7:                                        ; preds = %cond.false6, %cond.true4
  %cond8 = phi i32 [ %sub5, %cond.true4 ], [ %13, %cond.false6 ]
  store i32 %cond8, i32* %max_w, align 4, !tbaa !6
  %14 = bitcast double* %noise_var to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %14) #7
  store double 0.000000e+00, double* %noise_var, align 8, !tbaa !11
  %15 = bitcast double* %noise_mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %15) #7
  store double 0.000000e+00, double* %noise_mean, align 8, !tbaa !11
  %16 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc27, %cond.end7
  %17 = load i32, i32* %y, align 4, !tbaa !6
  %18 = load i32, i32* %max_h, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %17, %18
  br i1 %cmp9, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  br label %for.end29

for.body:                                         ; preds = %for.cond
  %20 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body
  %21 = load i32, i32* %x, align 4, !tbaa !6
  %22 = load i32, i32* %max_w, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %21, %22
  br i1 %cmp11, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond10
  store i32 5, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  br label %for.end

for.body13:                                       ; preds = %for.cond10
  %24 = bitcast double* %noise to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %24) #7
  %25 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %26 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %27 = load i32, i32* %y, align 4, !tbaa !6
  %add = add nsw i32 %26, %27
  %28 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add, %28
  %29 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %add14 = add nsw i32 %mul, %29
  %30 = load i32, i32* %x, align 4, !tbaa !6
  %add15 = add nsw i32 %add14, %30
  %arrayidx = getelementptr inbounds i8, i8* %25, i32 %add15
  %31 = load i8, i8* %arrayidx, align 1, !tbaa !35
  %conv = uitofp i8 %31 to double
  %32 = load i8*, i8** %denoised.addr, align 4, !tbaa !2
  %33 = load i32, i32* %y_o.addr, align 4, !tbaa !6
  %34 = load i32, i32* %y, align 4, !tbaa !6
  %add16 = add nsw i32 %33, %34
  %35 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 %add16, %35
  %36 = load i32, i32* %x_o.addr, align 4, !tbaa !6
  %add18 = add nsw i32 %mul17, %36
  %37 = load i32, i32* %x, align 4, !tbaa !6
  %add19 = add nsw i32 %add18, %37
  %arrayidx20 = getelementptr inbounds i8, i8* %32, i32 %add19
  %38 = load i8, i8* %arrayidx20, align 1, !tbaa !35
  %conv21 = zext i8 %38 to i32
  %conv22 = sitofp i32 %conv21 to double
  %sub23 = fsub double %conv, %conv22
  store double %sub23, double* %noise, align 8, !tbaa !11
  %39 = load double, double* %noise, align 8, !tbaa !11
  %40 = load double, double* %noise_mean, align 8, !tbaa !11
  %add24 = fadd double %40, %39
  store double %add24, double* %noise_mean, align 8, !tbaa !11
  %41 = load double, double* %noise, align 8, !tbaa !11
  %42 = load double, double* %noise, align 8, !tbaa !11
  %mul25 = fmul double %41, %42
  %43 = load double, double* %noise_var, align 8, !tbaa !11
  %add26 = fadd double %43, %mul25
  store double %add26, double* %noise_var, align 8, !tbaa !11
  %44 = bitcast double* %noise to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %44) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %45 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond10

for.end:                                          ; preds = %for.cond.cleanup12
  br label %for.inc27

for.inc27:                                        ; preds = %for.end
  %46 = load i32, i32* %y, align 4, !tbaa !6
  %inc28 = add nsw i32 %46, 1
  store i32 %inc28, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end29:                                        ; preds = %for.cond.cleanup
  %47 = load i32, i32* %max_w, align 4, !tbaa !6
  %48 = load i32, i32* %max_h, align 4, !tbaa !6
  %mul30 = mul nsw i32 %47, %48
  %conv31 = sitofp i32 %mul30 to double
  %49 = load double, double* %noise_mean, align 8, !tbaa !11
  %div = fdiv double %49, %conv31
  store double %div, double* %noise_mean, align 8, !tbaa !11
  %50 = load double, double* %noise_var, align 8, !tbaa !11
  %51 = load i32, i32* %max_w, align 4, !tbaa !6
  %52 = load i32, i32* %max_h, align 4, !tbaa !6
  %mul32 = mul nsw i32 %51, %52
  %conv33 = sitofp i32 %mul32 to double
  %div34 = fdiv double %50, %conv33
  %53 = load double, double* %noise_mean, align 8, !tbaa !11
  %54 = load double, double* %noise_mean, align 8, !tbaa !11
  %mul35 = fmul double %53, %54
  %sub36 = fsub double %div34, %mul35
  store i32 1, i32* %cleanup.dest.slot, align 4
  %55 = bitcast double* %noise_mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %55) #7
  %56 = bitcast double* %noise_var to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %56) #7
  %57 = bitcast i32* %max_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %58 = bitcast i32* %max_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #7
  ret double %sub36
}

declare double @aom_normalized_cross_correlation(double*, double*, i32) #1

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.cos.f64(double) #4

declare float @aom_noise_psd_get_default_value(i32, float) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 4}
!9 = !{!"", !3, i64 0, !7, i64 4}
!10 = !{!9, !3, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"double", !4, i64 0}
!13 = !{!14, !7, i64 32}
!14 = !{!"", !15, i64 0, !12, i64 16, !12, i64 24, !7, i64 32, !7, i64 36, !12, i64 40}
!15 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !7, i64 12}
!16 = !{!14, !3, i64 0}
!17 = !{!14, !3, i64 4}
!18 = !{!14, !12, i64 40}
!19 = !{!14, !7, i64 36}
!20 = !{!14, !12, i64 16}
!21 = !{!14, !12, i64 24}
!22 = !{!15, !7, i64 12}
!23 = !{!15, !3, i64 0}
!24 = !{!15, !3, i64 4}
!25 = !{!15, !3, i64 8}
!26 = !{!14, !3, i64 8}
!27 = !{!28, !3, i64 4}
!28 = !{!"", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !12, i64 16, !7, i64 24}
!29 = !{!28, !3, i64 0}
!30 = !{!28, !7, i64 12}
!31 = !{!28, !12, i64 16}
!32 = !{!28, !7, i64 24}
!33 = !{!34, !34, i64 0}
!34 = !{!"short", !4, i64 0}
!35 = !{!4, !4, i64 0}
!36 = !{!37, !37, i64 0}
!37 = !{!"float", !4, i64 0}
!38 = !{!39, !37, i64 4}
!39 = !{!"", !7, i64 0, !37, i64 4}
!40 = !{!39, !7, i64 0}
!41 = !{!42, !7, i64 4}
!42 = !{!"", !4, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!43 = !{!42, !7, i64 8}
!44 = !{!45, !7, i64 500}
!45 = !{!"", !42, i64 0, !4, i64 16, !4, i64 256, !3, i64 496, !7, i64 500, !7, i64 504}
!46 = !{!45, !3, i64 496}
!47 = !{!42, !4, i64 0}
!48 = !{!49, !12, i64 72}
!49 = !{!"", !15, i64 0, !14, i64 16, !7, i64 64, !12, i64 72}
!50 = !{!49, !7, i64 64}
!51 = !{!45, !7, i64 4}
!52 = !{!49, !3, i64 8}
!53 = !{!49, !7, i64 52}
!54 = !{!45, !7, i64 8}
!55 = !{!49, !3, i64 0}
!56 = !{!49, !3, i64 4}
!57 = !{!49, !7, i64 12}
!58 = !{!45, !7, i64 12}
!59 = !{!49, !7, i64 48}
!60 = !{!61, !34, i64 644}
!61 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !34, i64 644}
!62 = !{!61, !7, i64 0}
!63 = !{!61, !7, i64 4}
!64 = !{!61, !7, i64 296}
!65 = !{!61, !7, i64 292}
!66 = !{!61, !7, i64 120}
!67 = !{!61, !7, i64 204}
!68 = !{!61, !7, i64 288}
!69 = !{!14, !7, i64 12}
!70 = !{!61, !7, i64 596}
!71 = !{!61, !7, i64 600}
!72 = !{!61, !7, i64 604}
!73 = !{!61, !7, i64 608}
!74 = !{!61, !7, i64 612}
!75 = !{!61, !7, i64 616}
!76 = !{!61, !7, i64 620}
!77 = !{!61, !7, i64 636}
!78 = !{!61, !7, i64 640}
!79 = !{!61, !7, i64 624}
!80 = !{!81, !7, i64 0}
!81 = !{!"aom_denoise_and_model_t", !7, i64 0, !7, i64 4, !37, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !4, i64 36, !4, i64 48, !3, i64 60, !28, i64 64, !45, i64 96}
!82 = !{!81, !37, i64 8}
!83 = !{!81, !7, i64 4}
!84 = !{!81, !3, i64 60}
!85 = !{!86, !7, i64 140}
!86 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !87, i64 80, !7, i64 84, !87, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!87 = !{!"long", !4, i64 0}
!88 = !{!86, !7, i64 92}
!89 = !{!86, !7, i64 96}
!90 = !{!81, !7, i64 12}
!91 = !{!81, !7, i64 16}
!92 = !{!81, !7, i64 20}
!93 = !{!81, !7, i64 24}
!94 = !{!81, !7, i64 28}
!95 = !{!81, !7, i64 32}
!96 = !{!42, !7, i64 12}
