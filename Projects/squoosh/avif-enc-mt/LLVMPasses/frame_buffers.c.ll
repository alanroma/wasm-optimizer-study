; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/frame_buffers.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/frame_buffers.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }

; Function Attrs: nounwind
define hidden i32 @av1_alloc_internal_frame_buffers(%struct.InternalFrameBufferList* %list) #0 {
entry:
  %retval = alloca i32, align 4
  %list.addr = alloca %struct.InternalFrameBufferList*, align 4
  store %struct.InternalFrameBufferList* %list, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %0 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  call void @av1_free_internal_frame_buffers(%struct.InternalFrameBufferList* %0)
  %1 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %num_internal_frame_buffers = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %1, i32 0, i32 0
  store i32 16, i32* %num_internal_frame_buffers, align 4, !tbaa !6
  %2 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %num_internal_frame_buffers1 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %2, i32 0, i32 0
  %3 = load i32, i32* %num_internal_frame_buffers1, align 4, !tbaa !6
  %call = call i8* @aom_calloc(i32 %3, i32 12)
  %4 = bitcast i8* %call to %struct.InternalFrameBuffer*
  %5 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %5, i32 0, i32 1
  store %struct.InternalFrameBuffer* %4, %struct.InternalFrameBuffer** %int_fb, align 4, !tbaa !9
  %6 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb2 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %6, i32 0, i32 1
  %7 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb2, align 4, !tbaa !9
  %cmp = icmp eq %struct.InternalFrameBuffer* %7, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %num_internal_frame_buffers3 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %8, i32 0, i32 0
  store i32 0, i32* %num_internal_frame_buffers3, align 4, !tbaa !6
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: nounwind
define hidden void @av1_free_internal_frame_buffers(%struct.InternalFrameBufferList* %list) #0 {
entry:
  %list.addr = alloca %struct.InternalFrameBufferList*, align 4
  %i = alloca i32, align 4
  store %struct.InternalFrameBufferList* %list, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !10
  %2 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %num_internal_frame_buffers = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %2, i32 0, i32 0
  %3 = load i32, i32* %num_internal_frame_buffers, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %4, i32 0, i32 1
  %5 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb, align 4, !tbaa !9
  %6 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %5, i32 %6
  %data = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx, i32 0, i32 0
  %7 = load i8*, i8** %data, align 4, !tbaa !11
  call void @aom_free(i8* %7)
  %8 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb1 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %8, i32 0, i32 1
  %9 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb1, align 4, !tbaa !9
  %10 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx2 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %9, i32 %10
  %data3 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx2, i32 0, i32 0
  store i8* null, i8** %data3, align 4, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb4 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %12, i32 0, i32 1
  %13 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb4, align 4, !tbaa !9
  %14 = bitcast %struct.InternalFrameBuffer* %13 to i8*
  call void @aom_free(i8* %14)
  %15 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb5 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %15, i32 0, i32 1
  store %struct.InternalFrameBuffer* null, %struct.InternalFrameBuffer** %int_fb5, align 4, !tbaa !9
  %16 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %num_internal_frame_buffers6 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %16, i32 0, i32 0
  store i32 0, i32* %num_internal_frame_buffers6, align 4, !tbaa !6
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  ret void
}

declare i8* @aom_calloc(i32, i32) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare void @aom_free(i8*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden void @av1_zero_unused_internal_frame_buffers(%struct.InternalFrameBufferList* %list) #0 {
entry:
  %list.addr = alloca %struct.InternalFrameBufferList*, align 4
  %i = alloca i32, align 4
  store %struct.InternalFrameBufferList* %list, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !10
  %2 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %num_internal_frame_buffers = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %2, i32 0, i32 0
  %3 = load i32, i32* %num_internal_frame_buffers, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %4, i32 0, i32 1
  %5 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb, align 4, !tbaa !9
  %6 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %5, i32 %6
  %data = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx, i32 0, i32 0
  %7 = load i8*, i8** %data, align 4, !tbaa !11
  %tobool = icmp ne i8* %7, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %8 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb1 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %8, i32 0, i32 1
  %9 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb1, align 4, !tbaa !9
  %10 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx2 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %9, i32 %10
  %in_use = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx2, i32 0, i32 2
  %11 = load i32, i32* %in_use, align 4, !tbaa !14
  %tobool3 = icmp ne i32 %11, 0
  br i1 %tobool3, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %12 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb4 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %12, i32 0, i32 1
  %13 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb4, align 4, !tbaa !9
  %14 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx5 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %13, i32 %14
  %data6 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx5, i32 0, i32 0
  %15 = load i8*, i8** %data6, align 4, !tbaa !11
  %16 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %list.addr, align 4, !tbaa !2
  %int_fb7 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %16, i32 0, i32 1
  %17 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb7, align 4, !tbaa !9
  %18 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx8 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %17, i32 %18
  %size = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx8, i32 0, i32 1
  %19 = load i32, i32* %size, align 4, !tbaa !15
  call void @llvm.memset.p0i8.i32(i8* align 1 %15, i8 0, i32 %19, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %20 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define hidden i32 @av1_get_frame_buffer(i8* %cb_priv, i32 %min_size, %struct.aom_codec_frame_buffer* %fb) #0 {
entry:
  %retval = alloca i32, align 4
  %cb_priv.addr = alloca i8*, align 4
  %min_size.addr = alloca i32, align 4
  %fb.addr = alloca %struct.aom_codec_frame_buffer*, align 4
  %i = alloca i32, align 4
  %int_fb_list = alloca %struct.InternalFrameBufferList*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %cb_priv, i8** %cb_priv.addr, align 4, !tbaa !2
  store i32 %min_size, i32* %min_size.addr, align 4, !tbaa !16
  store %struct.aom_codec_frame_buffer* %fb, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.InternalFrameBufferList** %int_fb_list to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i8*, i8** %cb_priv.addr, align 4, !tbaa !2
  %3 = bitcast i8* %2 to %struct.InternalFrameBufferList*
  store %struct.InternalFrameBufferList* %3, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %4 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %cmp = icmp eq %struct.InternalFrameBufferList* %4, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %5 = load i32, i32* %i, align 4, !tbaa !10
  %6 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %num_internal_frame_buffers = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %6, i32 0, i32 0
  %7 = load i32, i32* %num_internal_frame_buffers, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %5, %7
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %8, i32 0, i32 1
  %9 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb, align 4, !tbaa !9
  %10 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %9, i32 %10
  %in_use = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx, i32 0, i32 2
  %11 = load i32, i32* %in_use, align 4, !tbaa !14
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.end3, label %if.then2

if.then2:                                         ; preds = %for.body
  br label %for.end

if.end3:                                          ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end3
  %12 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %if.then2, %for.cond
  %13 = load i32, i32* %i, align 4, !tbaa !10
  %14 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %num_internal_frame_buffers4 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %14, i32 0, i32 0
  %15 = load i32, i32* %num_internal_frame_buffers4, align 4, !tbaa !6
  %cmp5 = icmp eq i32 %13, %15
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %for.end
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %for.end
  %16 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb8 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %16, i32 0, i32 1
  %17 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb8, align 4, !tbaa !9
  %18 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx9 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %17, i32 %18
  %size = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx9, i32 0, i32 1
  %19 = load i32, i32* %size, align 4, !tbaa !15
  %20 = load i32, i32* %min_size.addr, align 4, !tbaa !16
  %cmp10 = icmp ult i32 %19, %20
  br i1 %cmp10, label %if.then11, label %if.end29

if.then11:                                        ; preds = %if.end7
  %21 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb12 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %21, i32 0, i32 1
  %22 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb12, align 4, !tbaa !9
  %23 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx13 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %22, i32 %23
  %data = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx13, i32 0, i32 0
  %24 = load i8*, i8** %data, align 4, !tbaa !11
  call void @aom_free(i8* %24)
  %25 = load i32, i32* %min_size.addr, align 4, !tbaa !16
  %call = call i8* @aom_calloc(i32 1, i32 %25)
  %26 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb14 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %26, i32 0, i32 1
  %27 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb14, align 4, !tbaa !9
  %28 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx15 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %27, i32 %28
  %data16 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx15, i32 0, i32 0
  store i8* %call, i8** %data16, align 4, !tbaa !11
  %29 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb17 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %29, i32 0, i32 1
  %30 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb17, align 4, !tbaa !9
  %31 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx18 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %30, i32 %31
  %data19 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx18, i32 0, i32 0
  %32 = load i8*, i8** %data19, align 4, !tbaa !11
  %tobool20 = icmp ne i8* %32, null
  br i1 %tobool20, label %if.end25, label %if.then21

if.then21:                                        ; preds = %if.then11
  %33 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb22 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %33, i32 0, i32 1
  %34 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb22, align 4, !tbaa !9
  %35 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx23 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %34, i32 %35
  %size24 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx23, i32 0, i32 1
  store i32 0, i32* %size24, align 4, !tbaa !15
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end25:                                         ; preds = %if.then11
  %36 = load i32, i32* %min_size.addr, align 4, !tbaa !16
  %37 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb26 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %37, i32 0, i32 1
  %38 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb26, align 4, !tbaa !9
  %39 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx27 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %38, i32 %39
  %size28 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx27, i32 0, i32 1
  store i32 %36, i32* %size28, align 4, !tbaa !15
  br label %if.end29

if.end29:                                         ; preds = %if.end25, %if.end7
  %40 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb30 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %40, i32 0, i32 1
  %41 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb30, align 4, !tbaa !9
  %42 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx31 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %41, i32 %42
  %data32 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx31, i32 0, i32 0
  %43 = load i8*, i8** %data32, align 4, !tbaa !11
  %44 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %data33 = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %44, i32 0, i32 0
  store i8* %43, i8** %data33, align 4, !tbaa !17
  %45 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb34 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %45, i32 0, i32 1
  %46 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb34, align 4, !tbaa !9
  %47 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx35 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %46, i32 %47
  %size36 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx35, i32 0, i32 1
  %48 = load i32, i32* %size36, align 4, !tbaa !15
  %49 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %size37 = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %49, i32 0, i32 1
  store i32 %48, i32* %size37, align 4, !tbaa !19
  %50 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb38 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %50, i32 0, i32 1
  %51 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb38, align 4, !tbaa !9
  %52 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx39 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %51, i32 %52
  %in_use40 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %arrayidx39, i32 0, i32 2
  store i32 1, i32* %in_use40, align 4, !tbaa !14
  %53 = load %struct.InternalFrameBufferList*, %struct.InternalFrameBufferList** %int_fb_list, align 4, !tbaa !2
  %int_fb41 = getelementptr inbounds %struct.InternalFrameBufferList, %struct.InternalFrameBufferList* %53, i32 0, i32 1
  %54 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb41, align 4, !tbaa !9
  %55 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx42 = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %54, i32 %55
  %56 = bitcast %struct.InternalFrameBuffer* %arrayidx42 to i8*
  %57 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %57, i32 0, i32 2
  store i8* %56, i8** %priv, align 4, !tbaa !20
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end29, %if.then21, %if.then6, %if.then
  %58 = bitcast %struct.InternalFrameBufferList** %int_fb_list to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  %60 = load i32, i32* %retval, align 4
  ret i32 %60
}

; Function Attrs: nounwind
define hidden i32 @av1_release_frame_buffer(i8* %cb_priv, %struct.aom_codec_frame_buffer* %fb) #0 {
entry:
  %cb_priv.addr = alloca i8*, align 4
  %fb.addr = alloca %struct.aom_codec_frame_buffer*, align 4
  %int_fb = alloca %struct.InternalFrameBuffer*, align 4
  store i8* %cb_priv, i8** %cb_priv.addr, align 4, !tbaa !2
  store %struct.aom_codec_frame_buffer* %fb, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %0 = bitcast %struct.InternalFrameBuffer** %int_fb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %1, i32 0, i32 2
  %2 = load i8*, i8** %priv, align 4, !tbaa !20
  %3 = bitcast i8* %2 to %struct.InternalFrameBuffer*
  store %struct.InternalFrameBuffer* %3, %struct.InternalFrameBuffer** %int_fb, align 4, !tbaa !2
  %4 = load i8*, i8** %cb_priv.addr, align 4, !tbaa !2
  %5 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb, align 4, !tbaa !2
  %tobool = icmp ne %struct.InternalFrameBuffer* %5, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.InternalFrameBuffer*, %struct.InternalFrameBuffer** %int_fb, align 4, !tbaa !2
  %in_use = getelementptr inbounds %struct.InternalFrameBuffer, %struct.InternalFrameBuffer* %6, i32 0, i32 2
  store i32 0, i32* %in_use, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = bitcast %struct.InternalFrameBuffer** %int_fb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  ret i32 0
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 0}
!7 = !{!"InternalFrameBufferList", !8, i64 0, !3, i64 4}
!8 = !{!"int", !4, i64 0}
!9 = !{!7, !3, i64 4}
!10 = !{!8, !8, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"InternalFrameBuffer", !3, i64 0, !13, i64 4, !8, i64 8}
!13 = !{!"long", !4, i64 0}
!14 = !{!12, !8, i64 8}
!15 = !{!12, !13, i64 4}
!16 = !{!13, !13, i64 0}
!17 = !{!18, !3, i64 0}
!18 = !{!"aom_codec_frame_buffer", !3, i64 0, !13, i64 4, !3, i64 8}
!19 = !{!18, !13, i64 4}
!20 = !{!18, !3, i64 8}
