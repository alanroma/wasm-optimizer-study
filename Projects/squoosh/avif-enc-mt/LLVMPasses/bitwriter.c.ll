; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/bitwriter.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/bitwriter.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_writer = type { i32, i8*, %struct.od_ec_enc, i8 }
%struct.od_ec_enc = type { i8*, i32, i16*, i32, i32, i32, i16, i16, i32 }

; Function Attrs: nounwind
define hidden void @aom_start_encode(%struct.aom_writer* %w, i8* %source) #0 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %source.addr = alloca i8*, align 4
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %1 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.aom_writer, %struct.aom_writer* %1, i32 0, i32 1
  store i8* %0, i8** %buffer, align 4, !tbaa !6
  %2 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %pos = getelementptr inbounds %struct.aom_writer, %struct.aom_writer* %2, i32 0, i32 0
  store i32 0, i32* %pos, align 4, !tbaa !11
  %3 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_writer, %struct.aom_writer* %3, i32 0, i32 2
  call void @od_ec_enc_init(%struct.od_ec_enc* %ec, i32 62025)
  ret void
}

declare void @od_ec_enc_init(%struct.od_ec_enc*, i32) #1

; Function Attrs: nounwind
define hidden i32 @aom_stop_encode(%struct.aom_writer* %w) #0 {
entry:
  %w.addr = alloca %struct.aom_writer*, align 4
  %nb_bits = alloca i32, align 4
  %bytes = alloca i32, align 4
  %data = alloca i8*, align 4
  store %struct.aom_writer* %w, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %0 = bitcast i32* %nb_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i8** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_writer, %struct.aom_writer* %3, i32 0, i32 2
  %call = call i8* @od_ec_enc_done(%struct.od_ec_enc* %ec, i32* %bytes)
  store i8* %call, i8** %data, align 4, !tbaa !2
  %4 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %ec1 = getelementptr inbounds %struct.aom_writer, %struct.aom_writer* %4, i32 0, i32 2
  %call2 = call i32 @od_ec_enc_tell(%struct.od_ec_enc* %ec1)
  store i32 %call2, i32* %nb_bits, align 4, !tbaa !12
  %5 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.aom_writer, %struct.aom_writer* %5, i32 0, i32 1
  %6 = load i8*, i8** %buffer, align 4, !tbaa !6
  %7 = load i8*, i8** %data, align 4, !tbaa !2
  %8 = load i32, i32* %bytes, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %6, i8* align 1 %7, i32 %8, i1 false)
  %9 = load i32, i32* %bytes, align 4, !tbaa !12
  %10 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %pos = getelementptr inbounds %struct.aom_writer, %struct.aom_writer* %10, i32 0, i32 0
  store i32 %9, i32* %pos, align 4, !tbaa !11
  %11 = load %struct.aom_writer*, %struct.aom_writer** %w.addr, align 4, !tbaa !2
  %ec3 = getelementptr inbounds %struct.aom_writer, %struct.aom_writer* %11, i32 0, i32 2
  call void @od_ec_enc_clear(%struct.od_ec_enc* %ec3)
  %12 = load i32, i32* %nb_bits, align 4, !tbaa !12
  %13 = bitcast i8** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %nb_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  ret i32 %12
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @od_ec_enc_done(%struct.od_ec_enc*, i32*) #1

declare i32 @od_ec_enc_tell(%struct.od_ec_enc*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

declare void @od_ec_enc_clear(%struct.od_ec_enc*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"aom_writer", !8, i64 0, !3, i64 4, !9, i64 8, !4, i64 40}
!8 = !{!"int", !4, i64 0}
!9 = !{!"od_ec_enc", !3, i64 0, !8, i64 4, !3, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !10, i64 24, !10, i64 26, !8, i64 28}
!10 = !{!"short", !4, i64 0}
!11 = !{!7, !8, i64 0}
!12 = !{!8, !8, i64 0}
