; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/pred_common.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/pred_common.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_mv = type { i32 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.InterpFilters = type { i16, i16 }

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_switchable_interp(%struct.macroblockd* %xd, i32 %dir) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %dir.addr = alloca i32, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %ctx_offset = alloca i32, align 4
  %ref_frame3 = alloca i8, align 1
  %filter_type_ctx = alloca i32, align 4
  %left_type = alloca i32, align 4
  %above_type = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %4 = bitcast i32* %ctx_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 12
  %arrayidx1 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 1
  %6 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = sext i8 %6 to i32
  %cmp = icmp sgt i32 %conv, 0
  %conv2 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv2, 4
  store i32 %mul, i32* %ctx_offset, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_frame3) #3
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %ref_frame4 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 12
  %arrayidx5 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame4, i32 0, i32 0
  %8 = load i8, i8* %arrayidx5, align 4, !tbaa !14
  store i8 %8, i8* %ref_frame3, align 1, !tbaa !14
  %9 = bitcast i32* %filter_type_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load i32, i32* %ctx_offset, align 4, !tbaa !6
  %11 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %and = and i32 %11, 1
  %mul6 = mul nsw i32 %and, 8
  %add = add nsw i32 %10, %mul6
  store i32 %add, i32* %filter_type_ctx, align 4, !tbaa !6
  %12 = bitcast i32* %left_type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  store i32 3, i32* %left_type, align 4, !tbaa !6
  %13 = bitcast i32* %above_type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  store i32 3, i32* %above_type, align 4, !tbaa !6
  %14 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %14, i32 0, i32 8
  %15 = load i8, i8* %left_available, align 1, !tbaa !15, !range !16
  %tobool = trunc i8 %15 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi7 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %16, i32 0, i32 6
  %17 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi7, align 4, !tbaa !8
  %arrayidx8 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %17, i32 -1
  %18 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx8, align 4, !tbaa !2
  %19 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %20 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %21 = load i8, i8* %ref_frame3, align 1, !tbaa !14
  %call = call zeroext i8 @get_ref_filter_type(%struct.MB_MODE_INFO* %18, %struct.macroblockd* %19, i32 %20, i8 signext %21)
  %conv9 = zext i8 %call to i32
  store i32 %conv9, i32* %left_type, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %22 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %22, i32 0, i32 7
  %23 = load i8, i8* %up_available, align 8, !tbaa !17, !range !16
  %tobool10 = trunc i8 %23 to i1
  br i1 %tobool10, label %if.then11, label %if.end16

if.then11:                                        ; preds = %if.end
  %24 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi12 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %24, i32 0, i32 6
  %25 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi12, align 4, !tbaa !8
  %26 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %26, i32 0, i32 2
  %27 = load i32, i32* %mi_stride, align 8, !tbaa !18
  %sub = sub nsw i32 0, %27
  %arrayidx13 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %25, i32 %sub
  %28 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx13, align 4, !tbaa !2
  %29 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %30 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %31 = load i8, i8* %ref_frame3, align 1, !tbaa !14
  %call14 = call zeroext i8 @get_ref_filter_type(%struct.MB_MODE_INFO* %28, %struct.macroblockd* %29, i32 %30, i8 signext %31)
  %conv15 = zext i8 %call14 to i32
  store i32 %conv15, i32* %above_type, align 4, !tbaa !6
  br label %if.end16

if.end16:                                         ; preds = %if.then11, %if.end
  %32 = load i32, i32* %left_type, align 4, !tbaa !6
  %33 = load i32, i32* %above_type, align 4, !tbaa !6
  %cmp17 = icmp eq i32 %32, %33
  br i1 %cmp17, label %if.then19, label %if.else

if.then19:                                        ; preds = %if.end16
  %34 = load i32, i32* %left_type, align 4, !tbaa !6
  %35 = load i32, i32* %filter_type_ctx, align 4, !tbaa !6
  %add20 = add nsw i32 %35, %34
  store i32 %add20, i32* %filter_type_ctx, align 4, !tbaa !6
  br label %if.end34

if.else:                                          ; preds = %if.end16
  %36 = load i32, i32* %left_type, align 4, !tbaa !6
  %cmp21 = icmp eq i32 %36, 3
  br i1 %cmp21, label %if.then23, label %if.else25

if.then23:                                        ; preds = %if.else
  %37 = load i32, i32* %above_type, align 4, !tbaa !6
  %38 = load i32, i32* %filter_type_ctx, align 4, !tbaa !6
  %add24 = add nsw i32 %38, %37
  store i32 %add24, i32* %filter_type_ctx, align 4, !tbaa !6
  br label %if.end33

if.else25:                                        ; preds = %if.else
  %39 = load i32, i32* %above_type, align 4, !tbaa !6
  %cmp26 = icmp eq i32 %39, 3
  br i1 %cmp26, label %if.then28, label %if.else30

if.then28:                                        ; preds = %if.else25
  %40 = load i32, i32* %left_type, align 4, !tbaa !6
  %41 = load i32, i32* %filter_type_ctx, align 4, !tbaa !6
  %add29 = add nsw i32 %41, %40
  store i32 %add29, i32* %filter_type_ctx, align 4, !tbaa !6
  br label %if.end32

if.else30:                                        ; preds = %if.else25
  %42 = load i32, i32* %filter_type_ctx, align 4, !tbaa !6
  %add31 = add nsw i32 %42, 3
  store i32 %add31, i32* %filter_type_ctx, align 4, !tbaa !6
  br label %if.end32

if.end32:                                         ; preds = %if.else30, %if.then28
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.then23
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.then19
  %43 = load i32, i32* %filter_type_ctx, align 4, !tbaa !6
  %44 = bitcast i32* %above_type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  %45 = bitcast i32* %left_type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  %46 = bitcast i32* %filter_type_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_frame3) #3
  %47 = bitcast i32* %ctx_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  ret i32 %43
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal zeroext i8 @get_ref_filter_type(%struct.MB_MODE_INFO* %ref_mbmi, %struct.macroblockd* %xd, i32 %dir, i8 signext %ref_frame) #0 {
entry:
  %ref_mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %dir.addr = alloca i32, align 4
  %ref_frame.addr = alloca i8, align 1
  store %struct.MB_MODE_INFO* %ref_mbmi, %struct.MB_MODE_INFO** %ref_mbmi.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !14
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ref_mbmi.addr, align 4, !tbaa !2
  %ref_frame1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame1, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !14
  %conv = sext i8 %2 to i32
  %3 = load i8, i8* %ref_frame.addr, align 1, !tbaa !14
  %conv2 = sext i8 %3 to i32
  %cmp = icmp eq i32 %conv, %conv2
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ref_mbmi.addr, align 4, !tbaa !2
  %ref_frame4 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %4, i32 0, i32 12
  %arrayidx5 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame4, i32 0, i32 1
  %5 = load i8, i8* %arrayidx5, align 1, !tbaa !14
  %conv6 = sext i8 %5 to i32
  %6 = load i8, i8* %ref_frame.addr, align 1, !tbaa !14
  %conv7 = sext i8 %6 to i32
  %cmp8 = icmp eq i32 %conv6, %conv7
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ref_mbmi.addr, align 4, !tbaa !2
  %interp_filters = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 4
  %8 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %and = and i32 %8, 1
  %call = call zeroext i8 @av1_extract_interp_filter(%union.int_interpfilters* byval(%union.int_interpfilters) align 4 %interp_filters, i32 %and)
  %conv10 = zext i8 %call to i32
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv10, %cond.true ], [ 3, %cond.false ]
  %conv11 = trunc i32 %cond to i8
  ret i8 %conv11
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @av1_get_palette_cache(%struct.macroblockd* %xd, i32 %plane, i16* %cache) #0 {
entry:
  %retval = alloca i32, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %cache.addr = alloca i16*, align 4
  %row = alloca i32, align 4
  %above_mi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mi = alloca %struct.MB_MODE_INFO*, align 4
  %above_n = alloca i32, align 4
  %left_n = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %above_idx = alloca i32, align 4
  %left_idx = alloca i32, align 4
  %n = alloca i32, align 4
  %above_colors = alloca i16*, align 4
  %left_colors = alloca i16*, align 4
  %v_above = alloca i16, align 2
  %v_left = alloca i16, align 2
  %val = alloca i16, align 2
  %val68 = alloca i16, align 2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i16* %cache, i16** %cache.addr, align 4, !tbaa !2
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_top_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 19
  %2 = load i32, i32* %mb_to_top_edge, align 4, !tbaa !19
  %sub = sub nsw i32 0, %2
  %shr = ashr i32 %sub, 3
  store i32 %shr, i32* %row, align 4, !tbaa !6
  %3 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i32, i32* %row, align 4, !tbaa !6
  %rem = srem i32 %4, 64
  %tobool = icmp ne i32 %rem, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 12
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 16, !tbaa !20
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.MB_MODE_INFO* [ %6, %cond.true ], [ null, %cond.false ]
  store %struct.MB_MODE_INFO* %cond, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !2
  %7 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 11
  %9 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !21
  store %struct.MB_MODE_INFO* %9, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !2
  %10 = bitcast i32* %above_n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  store i32 0, i32* %above_n, align 4, !tbaa !6
  %11 = bitcast i32* %left_n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  store i32 0, i32* %left_n, align 4, !tbaa !6
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.MB_MODE_INFO* %12, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %13 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !2
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %13, i32 0, i32 5
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 1
  %14 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %14, 0
  %conv = zext i1 %cmp to i32
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 %conv
  %15 = load i8, i8* %arrayidx, align 1, !tbaa !14
  %conv2 = zext i8 %15 to i32
  store i32 %conv2, i32* %above_n, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %16 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !2
  %tobool3 = icmp ne %struct.MB_MODE_INFO* %16, null
  br i1 %tobool3, label %if.then4, label %if.end11

if.then4:                                         ; preds = %if.end
  %17 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !2
  %palette_mode_info5 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %17, i32 0, i32 5
  %palette_size6 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info5, i32 0, i32 1
  %18 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp7 = icmp ne i32 %18, 0
  %conv8 = zext i1 %cmp7 to i32
  %arrayidx9 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size6, i32 0, i32 %conv8
  %19 = load i8, i8* %arrayidx9, align 1, !tbaa !14
  %conv10 = zext i8 %19 to i32
  store i32 %conv10, i32* %left_n, align 4, !tbaa !6
  br label %if.end11

if.end11:                                         ; preds = %if.then4, %if.end
  %20 = load i32, i32* %above_n, align 4, !tbaa !6
  %cmp12 = icmp eq i32 %20, 0
  br i1 %cmp12, label %land.lhs.true, label %if.end17

land.lhs.true:                                    ; preds = %if.end11
  %21 = load i32, i32* %left_n, align 4, !tbaa !6
  %cmp14 = icmp eq i32 %21, 0
  br i1 %cmp14, label %if.then16, label %if.end17

if.then16:                                        ; preds = %land.lhs.true
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %land.lhs.true, %if.end11
  %22 = bitcast i32* %above_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %23, 8
  store i32 %mul, i32* %above_idx, align 4, !tbaa !6
  %24 = bitcast i32* %left_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 %25, 8
  store i32 %mul18, i32* %left_idx, align 4, !tbaa !6
  %26 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  store i32 0, i32* %n, align 4, !tbaa !6
  %27 = bitcast i16** %above_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #3
  %28 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !2
  %tobool19 = icmp ne %struct.MB_MODE_INFO* %28, null
  br i1 %tobool19, label %cond.true20, label %cond.false22

cond.true20:                                      ; preds = %if.end17
  %29 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !2
  %palette_mode_info21 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %29, i32 0, i32 5
  %palette_colors = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info21, i32 0, i32 0
  %arraydecay = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors, i32 0, i32 0
  br label %cond.end23

cond.false22:                                     ; preds = %if.end17
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false22, %cond.true20
  %cond24 = phi i16* [ %arraydecay, %cond.true20 ], [ null, %cond.false22 ]
  store i16* %cond24, i16** %above_colors, align 4, !tbaa !2
  %30 = bitcast i16** %left_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  %31 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !2
  %tobool25 = icmp ne %struct.MB_MODE_INFO* %31, null
  br i1 %tobool25, label %cond.true26, label %cond.false30

cond.true26:                                      ; preds = %cond.end23
  %32 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !2
  %palette_mode_info27 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %32, i32 0, i32 5
  %palette_colors28 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info27, i32 0, i32 0
  %arraydecay29 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors28, i32 0, i32 0
  br label %cond.end31

cond.false30:                                     ; preds = %cond.end23
  br label %cond.end31

cond.end31:                                       ; preds = %cond.false30, %cond.true26
  %cond32 = phi i16* [ %arraydecay29, %cond.true26 ], [ null, %cond.false30 ]
  store i16* %cond32, i16** %left_colors, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end54, %cond.end31
  %33 = load i32, i32* %above_n, align 4, !tbaa !6
  %cmp33 = icmp sgt i32 %33, 0
  br i1 %cmp33, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %34 = load i32, i32* %left_n, align 4, !tbaa !6
  %cmp35 = icmp sgt i32 %34, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %35 = phi i1 [ false, %while.cond ], [ %cmp35, %land.rhs ]
  br i1 %35, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %36 = bitcast i16* %v_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %36) #3
  %37 = load i16*, i16** %above_colors, align 4, !tbaa !2
  %38 = load i32, i32* %above_idx, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds i16, i16* %37, i32 %38
  %39 = load i16, i16* %arrayidx37, align 2, !tbaa !22
  store i16 %39, i16* %v_above, align 2, !tbaa !22
  %40 = bitcast i16* %v_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %40) #3
  %41 = load i16*, i16** %left_colors, align 4, !tbaa !2
  %42 = load i32, i32* %left_idx, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds i16, i16* %41, i32 %42
  %43 = load i16, i16* %arrayidx38, align 2, !tbaa !22
  store i16 %43, i16* %v_left, align 2, !tbaa !22
  %44 = load i16, i16* %v_left, align 2, !tbaa !22
  %conv39 = zext i16 %44 to i32
  %45 = load i16, i16* %v_above, align 2, !tbaa !22
  %conv40 = zext i16 %45 to i32
  %cmp41 = icmp slt i32 %conv39, %conv40
  br i1 %cmp41, label %if.then43, label %if.else

if.then43:                                        ; preds = %while.body
  %46 = load i16*, i16** %cache.addr, align 4, !tbaa !2
  %47 = load i16, i16* %v_left, align 2, !tbaa !22
  call void @palette_add_to_cache(i16* %46, i32* %n, i16 zeroext %47)
  %48 = load i32, i32* %left_idx, align 4, !tbaa !6
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %left_idx, align 4, !tbaa !6
  %49 = load i32, i32* %left_n, align 4, !tbaa !6
  %dec = add nsw i32 %49, -1
  store i32 %dec, i32* %left_n, align 4, !tbaa !6
  br label %if.end54

if.else:                                          ; preds = %while.body
  %50 = load i16*, i16** %cache.addr, align 4, !tbaa !2
  %51 = load i16, i16* %v_above, align 2, !tbaa !22
  call void @palette_add_to_cache(i16* %50, i32* %n, i16 zeroext %51)
  %52 = load i32, i32* %above_idx, align 4, !tbaa !6
  %inc44 = add nsw i32 %52, 1
  store i32 %inc44, i32* %above_idx, align 4, !tbaa !6
  %53 = load i32, i32* %above_n, align 4, !tbaa !6
  %dec45 = add nsw i32 %53, -1
  store i32 %dec45, i32* %above_n, align 4, !tbaa !6
  %54 = load i16, i16* %v_left, align 2, !tbaa !22
  %conv46 = zext i16 %54 to i32
  %55 = load i16, i16* %v_above, align 2, !tbaa !22
  %conv47 = zext i16 %55 to i32
  %cmp48 = icmp eq i32 %conv46, %conv47
  br i1 %cmp48, label %if.then50, label %if.end53

if.then50:                                        ; preds = %if.else
  %56 = load i32, i32* %left_idx, align 4, !tbaa !6
  %inc51 = add nsw i32 %56, 1
  store i32 %inc51, i32* %left_idx, align 4, !tbaa !6
  %57 = load i32, i32* %left_n, align 4, !tbaa !6
  %dec52 = add nsw i32 %57, -1
  store i32 %dec52, i32* %left_n, align 4, !tbaa !6
  br label %if.end53

if.end53:                                         ; preds = %if.then50, %if.else
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.then43
  %58 = bitcast i16* %v_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %58) #3
  %59 = bitcast i16* %v_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %59) #3
  br label %while.cond

while.end:                                        ; preds = %land.end
  br label %while.cond55

while.cond55:                                     ; preds = %while.body59, %while.end
  %60 = load i32, i32* %above_n, align 4, !tbaa !6
  %dec56 = add nsw i32 %60, -1
  store i32 %dec56, i32* %above_n, align 4, !tbaa !6
  %cmp57 = icmp sgt i32 %60, 0
  br i1 %cmp57, label %while.body59, label %while.end62

while.body59:                                     ; preds = %while.cond55
  %61 = bitcast i16* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %61) #3
  %62 = load i16*, i16** %above_colors, align 4, !tbaa !2
  %63 = load i32, i32* %above_idx, align 4, !tbaa !6
  %inc60 = add nsw i32 %63, 1
  store i32 %inc60, i32* %above_idx, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds i16, i16* %62, i32 %63
  %64 = load i16, i16* %arrayidx61, align 2, !tbaa !22
  store i16 %64, i16* %val, align 2, !tbaa !22
  %65 = load i16*, i16** %cache.addr, align 4, !tbaa !2
  %66 = load i16, i16* %val, align 2, !tbaa !22
  call void @palette_add_to_cache(i16* %65, i32* %n, i16 zeroext %66)
  %67 = bitcast i16* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %67) #3
  br label %while.cond55

while.end62:                                      ; preds = %while.cond55
  br label %while.cond63

while.cond63:                                     ; preds = %while.body67, %while.end62
  %68 = load i32, i32* %left_n, align 4, !tbaa !6
  %dec64 = add nsw i32 %68, -1
  store i32 %dec64, i32* %left_n, align 4, !tbaa !6
  %cmp65 = icmp sgt i32 %68, 0
  br i1 %cmp65, label %while.body67, label %while.end71

while.body67:                                     ; preds = %while.cond63
  %69 = bitcast i16* %val68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %69) #3
  %70 = load i16*, i16** %left_colors, align 4, !tbaa !2
  %71 = load i32, i32* %left_idx, align 4, !tbaa !6
  %inc69 = add nsw i32 %71, 1
  store i32 %inc69, i32* %left_idx, align 4, !tbaa !6
  %arrayidx70 = getelementptr inbounds i16, i16* %70, i32 %71
  %72 = load i16, i16* %arrayidx70, align 2, !tbaa !22
  store i16 %72, i16* %val68, align 2, !tbaa !22
  %73 = load i16*, i16** %cache.addr, align 4, !tbaa !2
  %74 = load i16, i16* %val68, align 2, !tbaa !22
  call void @palette_add_to_cache(i16* %73, i32* %n, i16 zeroext %74)
  %75 = bitcast i16* %val68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %75) #3
  br label %while.cond63

while.end71:                                      ; preds = %while.cond63
  %76 = load i32, i32* %n, align 4, !tbaa !6
  store i32 %76, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %77 = bitcast i16** %left_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  %78 = bitcast i16** %above_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  %79 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  %80 = bitcast i32* %left_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #3
  %81 = bitcast i32* %above_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #3
  br label %cleanup

cleanup:                                          ; preds = %while.end71, %if.then16
  %82 = bitcast i32* %left_n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #3
  %83 = bitcast i32* %above_n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #3
  %84 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #3
  %85 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #3
  %86 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #3
  %87 = load i32, i32* %retval, align 4
  ret i32 %87
}

; Function Attrs: nounwind
define internal void @palette_add_to_cache(i16* %cache, i32* %n, i16 zeroext %val) #0 {
entry:
  %cache.addr = alloca i16*, align 4
  %n.addr = alloca i32*, align 4
  %val.addr = alloca i16, align 2
  store i16* %cache, i16** %cache.addr, align 4, !tbaa !2
  store i32* %n, i32** %n.addr, align 4, !tbaa !2
  store i16 %val, i16* %val.addr, align 2, !tbaa !22
  %0 = load i32*, i32** %n.addr, align 4, !tbaa !2
  %1 = load i32, i32* %0, align 4, !tbaa !6
  %cmp = icmp sgt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load i16, i16* %val.addr, align 2, !tbaa !22
  %conv = zext i16 %2 to i32
  %3 = load i16*, i16** %cache.addr, align 4, !tbaa !2
  %4 = load i32*, i32** %n.addr, align 4, !tbaa !2
  %5 = load i32, i32* %4, align 4, !tbaa !6
  %sub = sub nsw i32 %5, 1
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 %sub
  %6 = load i16, i16* %arrayidx, align 2, !tbaa !22
  %conv1 = zext i16 %6 to i32
  %cmp2 = icmp eq i32 %conv, %conv1
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %7 = load i16, i16* %val.addr, align 2, !tbaa !22
  %8 = load i16*, i16** %cache.addr, align 4, !tbaa !2
  %9 = load i32*, i32** %n.addr, align 4, !tbaa !2
  %10 = load i32, i32* %9, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %9, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds i16, i16* %8, i32 %10
  store i16 %7, i16* %arrayidx4, align 2, !tbaa !22
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_get_intra_inter_context(%struct.macroblockd* %xd) #0 {
entry:
  %retval = alloca i32, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %above_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %has_above = alloca i32, align 4
  %has_left = alloca i32, align 4
  %above_intra = alloca i32, align 4
  %left_intra = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast %struct.MB_MODE_INFO** %above_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %above_mbmi1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 12
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi1, align 16, !tbaa !20
  store %struct.MB_MODE_INFO* %2, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %3 = bitcast %struct.MB_MODE_INFO** %left_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_mbmi2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 11
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi2, align 4, !tbaa !21
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %6 = bitcast i32* %has_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 7
  %8 = load i8, i8* %up_available, align 8, !tbaa !17, !range !16
  %tobool = trunc i8 %8 to i1
  %conv = zext i1 %tobool to i32
  store i32 %conv, i32* %has_above, align 4, !tbaa !6
  %9 = bitcast i32* %has_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %10, i32 0, i32 8
  %11 = load i8, i8* %left_available, align 1, !tbaa !15, !range !16
  %tobool3 = trunc i8 %11 to i1
  %conv4 = zext i1 %tobool3 to i32
  store i32 %conv4, i32* %has_left, align 4, !tbaa !6
  %12 = load i32, i32* %has_above, align 4, !tbaa !6
  %tobool5 = icmp ne i32 %12, 0
  br i1 %tobool5, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %13 = load i32, i32* %has_left, align 4, !tbaa !6
  %tobool6 = icmp ne i32 %13, 0
  br i1 %tobool6, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %14 = bitcast i32* %above_intra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %15)
  %tobool7 = icmp ne i32 %call, 0
  %lnot = xor i1 %tobool7, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %above_intra, align 4, !tbaa !6
  %16 = bitcast i32* %left_intra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %call8 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %17)
  %tobool9 = icmp ne i32 %call8, 0
  %lnot10 = xor i1 %tobool9, true
  %lnot.ext11 = zext i1 %lnot10 to i32
  store i32 %lnot.ext11, i32* %left_intra, align 4, !tbaa !6
  %18 = load i32, i32* %left_intra, align 4, !tbaa !6
  %tobool12 = icmp ne i32 %18, 0
  br i1 %tobool12, label %land.lhs.true13, label %cond.false

land.lhs.true13:                                  ; preds = %if.then
  %19 = load i32, i32* %above_intra, align 4, !tbaa !6
  %tobool14 = icmp ne i32 %19, 0
  br i1 %tobool14, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true13
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true13, %if.then
  %20 = load i32, i32* %left_intra, align 4, !tbaa !6
  %tobool15 = icmp ne i32 %20, 0
  br i1 %tobool15, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %cond.false
  %21 = load i32, i32* %above_intra, align 4, !tbaa !6
  %tobool16 = icmp ne i32 %21, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %cond.false
  %22 = phi i1 [ true, %cond.false ], [ %tobool16, %lor.rhs ]
  %lor.ext = zext i1 %22 to i32
  br label %cond.end

cond.end:                                         ; preds = %lor.end, %cond.true
  %cond = phi i32 [ 3, %cond.true ], [ %lor.ext, %lor.end ]
  store i32 %cond, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %left_intra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #3
  %24 = bitcast i32* %above_intra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  br label %cleanup

if.else:                                          ; preds = %land.lhs.true, %entry
  %25 = load i32, i32* %has_above, align 4, !tbaa !6
  %tobool17 = icmp ne i32 %25, 0
  br i1 %tobool17, label %if.then19, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %26 = load i32, i32* %has_left, align 4, !tbaa !6
  %tobool18 = icmp ne i32 %26, 0
  br i1 %tobool18, label %if.then19, label %if.else29

if.then19:                                        ; preds = %lor.lhs.false, %if.else
  %27 = load i32, i32* %has_above, align 4, !tbaa !6
  %tobool20 = icmp ne i32 %27, 0
  br i1 %tobool20, label %cond.true21, label %cond.false22

cond.true21:                                      ; preds = %if.then19
  %28 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  br label %cond.end23

cond.false22:                                     ; preds = %if.then19
  %29 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false22, %cond.true21
  %cond24 = phi %struct.MB_MODE_INFO* [ %28, %cond.true21 ], [ %29, %cond.false22 ]
  %call25 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %cond24)
  %tobool26 = icmp ne i32 %call25, 0
  %lnot27 = xor i1 %tobool26, true
  %lnot.ext28 = zext i1 %lnot27 to i32
  %mul = mul nsw i32 2, %lnot.ext28
  store i32 %mul, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else29:                                        ; preds = %lor.lhs.false
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else29, %cond.end23, %cond.end
  %30 = bitcast i32* %has_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  %31 = bitcast i32* %has_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  %32 = bitcast %struct.MB_MODE_INFO** %left_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  %33 = bitcast %struct.MB_MODE_INFO** %above_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #3
  %34 = load i32, i32* %retval, align 4
  ret i32 %34
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_block(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !14
  %conv = sext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  ret i32 %lor.ext
}

; Function Attrs: nounwind
define hidden i32 @av1_get_reference_mode_context(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ctx = alloca i32, align 4
  %above_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %has_above = alloca i32, align 4
  %has_left = alloca i32, align 4
  %edge_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.MB_MODE_INFO** %above_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %above_mbmi1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 12
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi1, align 16, !tbaa !20
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %4 = bitcast %struct.MB_MODE_INFO** %left_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_mbmi2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 11
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi2, align 4, !tbaa !21
  store %struct.MB_MODE_INFO* %6, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %7 = bitcast i32* %has_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 7
  %9 = load i8, i8* %up_available, align 8, !tbaa !17, !range !16
  %tobool = trunc i8 %9 to i1
  %conv = zext i1 %tobool to i32
  store i32 %conv, i32* %has_above, align 4, !tbaa !6
  %10 = bitcast i32* %has_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %11, i32 0, i32 8
  %12 = load i8, i8* %left_available, align 1, !tbaa !15, !range !16
  %tobool3 = trunc i8 %12 to i1
  %conv4 = zext i1 %tobool3 to i32
  store i32 %conv4, i32* %has_left, align 4, !tbaa !6
  %13 = load i32, i32* %has_above, align 4, !tbaa !6
  %tobool5 = icmp ne i32 %13, 0
  br i1 %tobool5, label %land.lhs.true, label %if.else73

land.lhs.true:                                    ; preds = %entry
  %14 = load i32, i32* %has_left, align 4, !tbaa !6
  %tobool6 = icmp ne i32 %14, 0
  br i1 %tobool6, label %if.then, label %if.else73

if.then:                                          ; preds = %land.lhs.true
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %call = call i32 @has_second_ref(%struct.MB_MODE_INFO* %15)
  %tobool7 = icmp ne i32 %call, 0
  br i1 %tobool7, label %if.else, label %land.lhs.true8

land.lhs.true8:                                   ; preds = %if.then
  %16 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %call9 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %16)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.else, label %if.then11

if.then11:                                        ; preds = %land.lhs.true8
  %17 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %17, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %18 = load i8, i8* %arrayidx, align 4, !tbaa !14
  %conv12 = sext i8 %18 to i32
  %cmp = icmp sge i32 %conv12, 5
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.then11
  %19 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %ref_frame14 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %19, i32 0, i32 12
  %arrayidx15 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame14, i32 0, i32 0
  %20 = load i8, i8* %arrayidx15, align 4, !tbaa !14
  %conv16 = sext i8 %20 to i32
  %cmp17 = icmp sle i32 %conv16, 7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.then11
  %21 = phi i1 [ false, %if.then11 ], [ %cmp17, %land.rhs ]
  %land.ext = zext i1 %21 to i32
  %22 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %ref_frame19 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %22, i32 0, i32 12
  %arrayidx20 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame19, i32 0, i32 0
  %23 = load i8, i8* %arrayidx20, align 4, !tbaa !14
  %conv21 = sext i8 %23 to i32
  %cmp22 = icmp sge i32 %conv21, 5
  br i1 %cmp22, label %land.rhs24, label %land.end30

land.rhs24:                                       ; preds = %land.end
  %24 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %ref_frame25 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %24, i32 0, i32 12
  %arrayidx26 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame25, i32 0, i32 0
  %25 = load i8, i8* %arrayidx26, align 4, !tbaa !14
  %conv27 = sext i8 %25 to i32
  %cmp28 = icmp sle i32 %conv27, 7
  br label %land.end30

land.end30:                                       ; preds = %land.rhs24, %land.end
  %26 = phi i1 [ false, %land.end ], [ %cmp28, %land.rhs24 ]
  %land.ext31 = zext i1 %26 to i32
  %xor = xor i32 %land.ext, %land.ext31
  store i32 %xor, i32* %ctx, align 4, !tbaa !6
  br label %if.end72

if.else:                                          ; preds = %land.lhs.true8, %if.then
  %27 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %call32 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %27)
  %tobool33 = icmp ne i32 %call32, 0
  br i1 %tobool33, label %if.else48, label %if.then34

if.then34:                                        ; preds = %if.else
  %28 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %ref_frame35 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %28, i32 0, i32 12
  %arrayidx36 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame35, i32 0, i32 0
  %29 = load i8, i8* %arrayidx36, align 4, !tbaa !14
  %conv37 = sext i8 %29 to i32
  %cmp38 = icmp sge i32 %conv37, 5
  br i1 %cmp38, label %land.lhs.true40, label %lor.rhs

land.lhs.true40:                                  ; preds = %if.then34
  %30 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %ref_frame41 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %30, i32 0, i32 12
  %arrayidx42 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame41, i32 0, i32 0
  %31 = load i8, i8* %arrayidx42, align 4, !tbaa !14
  %conv43 = sext i8 %31 to i32
  %cmp44 = icmp sle i32 %conv43, 7
  br i1 %cmp44, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true40, %if.then34
  %32 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %call46 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %32)
  %tobool47 = icmp ne i32 %call46, 0
  %lnot = xor i1 %tobool47, true
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.lhs.true40
  %33 = phi i1 [ true, %land.lhs.true40 ], [ %lnot, %lor.rhs ]
  %lor.ext = zext i1 %33 to i32
  %add = add nsw i32 2, %lor.ext
  store i32 %add, i32* %ctx, align 4, !tbaa !6
  br label %if.end71

if.else48:                                        ; preds = %if.else
  %34 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %call49 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %34)
  %tobool50 = icmp ne i32 %call49, 0
  br i1 %tobool50, label %if.else70, label %if.then51

if.then51:                                        ; preds = %if.else48
  %35 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %ref_frame52 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %35, i32 0, i32 12
  %arrayidx53 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame52, i32 0, i32 0
  %36 = load i8, i8* %arrayidx53, align 4, !tbaa !14
  %conv54 = sext i8 %36 to i32
  %cmp55 = icmp sge i32 %conv54, 5
  br i1 %cmp55, label %land.lhs.true57, label %lor.rhs63

land.lhs.true57:                                  ; preds = %if.then51
  %37 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %ref_frame58 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %37, i32 0, i32 12
  %arrayidx59 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame58, i32 0, i32 0
  %38 = load i8, i8* %arrayidx59, align 4, !tbaa !14
  %conv60 = sext i8 %38 to i32
  %cmp61 = icmp sle i32 %conv60, 7
  br i1 %cmp61, label %lor.end67, label %lor.rhs63

lor.rhs63:                                        ; preds = %land.lhs.true57, %if.then51
  %39 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %call64 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %39)
  %tobool65 = icmp ne i32 %call64, 0
  %lnot66 = xor i1 %tobool65, true
  br label %lor.end67

lor.end67:                                        ; preds = %lor.rhs63, %land.lhs.true57
  %40 = phi i1 [ true, %land.lhs.true57 ], [ %lnot66, %lor.rhs63 ]
  %lor.ext68 = zext i1 %40 to i32
  %add69 = add nsw i32 2, %lor.ext68
  store i32 %add69, i32* %ctx, align 4, !tbaa !6
  br label %if.end

if.else70:                                        ; preds = %if.else48
  store i32 4, i32* %ctx, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else70, %lor.end67
  br label %if.end71

if.end71:                                         ; preds = %if.end, %lor.end
  br label %if.end72

if.end72:                                         ; preds = %if.end71, %land.end30
  br label %if.end98

if.else73:                                        ; preds = %land.lhs.true, %entry
  %41 = load i32, i32* %has_above, align 4, !tbaa !6
  %tobool74 = icmp ne i32 %41, 0
  br i1 %tobool74, label %if.then76, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else73
  %42 = load i32, i32* %has_left, align 4, !tbaa !6
  %tobool75 = icmp ne i32 %42, 0
  br i1 %tobool75, label %if.then76, label %if.else96

if.then76:                                        ; preds = %lor.lhs.false, %if.else73
  %43 = bitcast %struct.MB_MODE_INFO** %edge_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #3
  %44 = load i32, i32* %has_above, align 4, !tbaa !6
  %tobool77 = icmp ne i32 %44, 0
  br i1 %tobool77, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then76
  %45 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %if.then76
  %46 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.MB_MODE_INFO* [ %45, %cond.true ], [ %46, %cond.false ]
  store %struct.MB_MODE_INFO* %cond, %struct.MB_MODE_INFO** %edge_mbmi, align 4, !tbaa !2
  %47 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %edge_mbmi, align 4, !tbaa !2
  %call78 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %47)
  %tobool79 = icmp ne i32 %call78, 0
  br i1 %tobool79, label %if.else94, label %if.then80

if.then80:                                        ; preds = %cond.end
  %48 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %edge_mbmi, align 4, !tbaa !2
  %ref_frame81 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %48, i32 0, i32 12
  %arrayidx82 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame81, i32 0, i32 0
  %49 = load i8, i8* %arrayidx82, align 4, !tbaa !14
  %conv83 = sext i8 %49 to i32
  %cmp84 = icmp sge i32 %conv83, 5
  br i1 %cmp84, label %land.rhs86, label %land.end92

land.rhs86:                                       ; preds = %if.then80
  %50 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %edge_mbmi, align 4, !tbaa !2
  %ref_frame87 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %50, i32 0, i32 12
  %arrayidx88 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame87, i32 0, i32 0
  %51 = load i8, i8* %arrayidx88, align 4, !tbaa !14
  %conv89 = sext i8 %51 to i32
  %cmp90 = icmp sle i32 %conv89, 7
  br label %land.end92

land.end92:                                       ; preds = %land.rhs86, %if.then80
  %52 = phi i1 [ false, %if.then80 ], [ %cmp90, %land.rhs86 ]
  %land.ext93 = zext i1 %52 to i32
  store i32 %land.ext93, i32* %ctx, align 4, !tbaa !6
  br label %if.end95

if.else94:                                        ; preds = %cond.end
  store i32 3, i32* %ctx, align 4, !tbaa !6
  br label %if.end95

if.end95:                                         ; preds = %if.else94, %land.end92
  %53 = bitcast %struct.MB_MODE_INFO** %edge_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  br label %if.end97

if.else96:                                        ; preds = %lor.lhs.false
  store i32 1, i32* %ctx, align 4, !tbaa !6
  br label %if.end97

if.end97:                                         ; preds = %if.else96, %if.end95
  br label %if.end98

if.end98:                                         ; preds = %if.end97, %if.end72
  %54 = load i32, i32* %ctx, align 4, !tbaa !6
  %55 = bitcast i32* %has_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i32* %has_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast %struct.MB_MODE_INFO** %left_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast %struct.MB_MODE_INFO** %above_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  %59 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  ret i32 %54
}

; Function Attrs: inlinehint nounwind
define internal i32 @has_second_ref(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 1
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !14
  %conv = sext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 0
  %conv1 = zext i1 %cmp to i32
  ret i32 %conv1
}

; Function Attrs: nounwind
define hidden i32 @av1_get_comp_reference_type_context(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred_context = alloca i32, align 4
  %above_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %above_in_image = alloca i32, align 4
  %left_in_image = alloca i32, align 4
  %above_intra = alloca i32, align 4
  %left_intra = alloca i32, align 4
  %inter_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %a_sg = alloca i32, align 4
  %l_sg = alloca i32, align 4
  %frfa = alloca i8, align 1
  %frfl = alloca i8, align 1
  %uni_rfc = alloca i32, align 4
  %a_uni_rfc = alloca i32, align 4
  %l_uni_rfc = alloca i32, align 4
  %edge_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.MB_MODE_INFO** %above_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %above_mbmi1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 12
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi1, align 16, !tbaa !20
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %4 = bitcast %struct.MB_MODE_INFO** %left_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_mbmi2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 11
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi2, align 4, !tbaa !21
  store %struct.MB_MODE_INFO* %6, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %7 = bitcast i32* %above_in_image to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 7
  %9 = load i8, i8* %up_available, align 8, !tbaa !17, !range !16
  %tobool = trunc i8 %9 to i1
  %conv = zext i1 %tobool to i32
  store i32 %conv, i32* %above_in_image, align 4, !tbaa !6
  %10 = bitcast i32* %left_in_image to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %11, i32 0, i32 8
  %12 = load i8, i8* %left_available, align 1, !tbaa !15, !range !16
  %tobool3 = trunc i8 %12 to i1
  %conv4 = zext i1 %tobool3 to i32
  store i32 %conv4, i32* %left_in_image, align 4, !tbaa !6
  %13 = load i32, i32* %above_in_image, align 4, !tbaa !6
  %tobool5 = icmp ne i32 %13, 0
  br i1 %tobool5, label %land.lhs.true, label %if.else128

land.lhs.true:                                    ; preds = %entry
  %14 = load i32, i32* %left_in_image, align 4, !tbaa !6
  %tobool6 = icmp ne i32 %14, 0
  br i1 %tobool6, label %if.then, label %if.else128

if.then:                                          ; preds = %land.lhs.true
  %15 = bitcast i32* %above_intra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %16)
  %tobool7 = icmp ne i32 %call, 0
  %lnot = xor i1 %tobool7, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %above_intra, align 4, !tbaa !6
  %17 = bitcast i32* %left_intra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %call8 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %18)
  %tobool9 = icmp ne i32 %call8, 0
  %lnot10 = xor i1 %tobool9, true
  %lnot.ext11 = zext i1 %lnot10 to i32
  store i32 %lnot.ext11, i32* %left_intra, align 4, !tbaa !6
  %19 = load i32, i32* %above_intra, align 4, !tbaa !6
  %tobool12 = icmp ne i32 %19, 0
  br i1 %tobool12, label %land.lhs.true13, label %if.else

land.lhs.true13:                                  ; preds = %if.then
  %20 = load i32, i32* %left_intra, align 4, !tbaa !6
  %tobool14 = icmp ne i32 %20, 0
  br i1 %tobool14, label %if.then15, label %if.else

if.then15:                                        ; preds = %land.lhs.true13
  store i32 2, i32* %pred_context, align 4, !tbaa !6
  br label %if.end127

if.else:                                          ; preds = %land.lhs.true13, %if.then
  %21 = load i32, i32* %above_intra, align 4, !tbaa !6
  %tobool16 = icmp ne i32 %21, 0
  br i1 %tobool16, label %if.then18, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %22 = load i32, i32* %left_intra, align 4, !tbaa !6
  %tobool17 = icmp ne i32 %22, 0
  br i1 %tobool17, label %if.then18, label %if.else25

if.then18:                                        ; preds = %lor.lhs.false, %if.else
  %23 = bitcast %struct.MB_MODE_INFO** %inter_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = load i32, i32* %above_intra, align 4, !tbaa !6
  %tobool19 = icmp ne i32 %24, 0
  br i1 %tobool19, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then18
  %25 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %if.then18
  %26 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.MB_MODE_INFO* [ %25, %cond.true ], [ %26, %cond.false ]
  store %struct.MB_MODE_INFO* %cond, %struct.MB_MODE_INFO** %inter_mbmi, align 4, !tbaa !2
  %27 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %inter_mbmi, align 4, !tbaa !2
  %call20 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %27)
  %tobool21 = icmp ne i32 %call20, 0
  br i1 %tobool21, label %if.else23, label %if.then22

if.then22:                                        ; preds = %cond.end
  store i32 2, i32* %pred_context, align 4, !tbaa !6
  br label %if.end

if.else23:                                        ; preds = %cond.end
  %28 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %inter_mbmi, align 4, !tbaa !2
  %call24 = call i32 @has_uni_comp_refs(%struct.MB_MODE_INFO* %28)
  %mul = mul nsw i32 2, %call24
  %add = add nsw i32 1, %mul
  store i32 %add, i32* %pred_context, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else23, %if.then22
  %29 = bitcast %struct.MB_MODE_INFO** %inter_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #3
  br label %if.end126

if.else25:                                        ; preds = %lor.lhs.false
  %30 = bitcast i32* %a_sg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  %31 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %call26 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %31)
  %tobool27 = icmp ne i32 %call26, 0
  %lnot28 = xor i1 %tobool27, true
  %lnot.ext29 = zext i1 %lnot28 to i32
  store i32 %lnot.ext29, i32* %a_sg, align 4, !tbaa !6
  %32 = bitcast i32* %l_sg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %call30 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %33)
  %tobool31 = icmp ne i32 %call30, 0
  %lnot32 = xor i1 %tobool31, true
  %lnot.ext33 = zext i1 %lnot32 to i32
  store i32 %lnot.ext33, i32* %l_sg, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %frfa) #3
  %34 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %34, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %35 = load i8, i8* %arrayidx, align 4, !tbaa !14
  store i8 %35, i8* %frfa, align 1, !tbaa !14
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %frfl) #3
  %36 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %ref_frame34 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %36, i32 0, i32 12
  %arrayidx35 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame34, i32 0, i32 0
  %37 = load i8, i8* %arrayidx35, align 4, !tbaa !14
  store i8 %37, i8* %frfl, align 1, !tbaa !14
  %38 = load i32, i32* %a_sg, align 4, !tbaa !6
  %tobool36 = icmp ne i32 %38, 0
  br i1 %tobool36, label %land.lhs.true37, label %if.else59

land.lhs.true37:                                  ; preds = %if.else25
  %39 = load i32, i32* %l_sg, align 4, !tbaa !6
  %tobool38 = icmp ne i32 %39, 0
  br i1 %tobool38, label %if.then39, label %if.else59

if.then39:                                        ; preds = %land.lhs.true37
  %40 = load i8, i8* %frfa, align 1, !tbaa !14
  %conv40 = sext i8 %40 to i32
  %cmp = icmp sge i32 %conv40, 5
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.then39
  %41 = load i8, i8* %frfa, align 1, !tbaa !14
  %conv42 = sext i8 %41 to i32
  %cmp43 = icmp sle i32 %conv42, 7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.then39
  %42 = phi i1 [ false, %if.then39 ], [ %cmp43, %land.rhs ]
  %land.ext = zext i1 %42 to i32
  %43 = load i8, i8* %frfl, align 1, !tbaa !14
  %conv45 = sext i8 %43 to i32
  %cmp46 = icmp sge i32 %conv45, 5
  br i1 %cmp46, label %land.rhs48, label %land.end52

land.rhs48:                                       ; preds = %land.end
  %44 = load i8, i8* %frfl, align 1, !tbaa !14
  %conv49 = sext i8 %44 to i32
  %cmp50 = icmp sle i32 %conv49, 7
  br label %land.end52

land.end52:                                       ; preds = %land.rhs48, %land.end
  %45 = phi i1 [ false, %land.end ], [ %cmp50, %land.rhs48 ]
  %land.ext53 = zext i1 %45 to i32
  %xor = xor i32 %land.ext, %land.ext53
  %tobool54 = icmp ne i32 %xor, 0
  %lnot55 = xor i1 %tobool54, true
  %lnot.ext56 = zext i1 %lnot55 to i32
  %mul57 = mul nsw i32 2, %lnot.ext56
  %add58 = add nsw i32 1, %mul57
  store i32 %add58, i32* %pred_context, align 4, !tbaa !6
  br label %if.end125

if.else59:                                        ; preds = %land.lhs.true37, %if.else25
  %46 = load i32, i32* %l_sg, align 4, !tbaa !6
  %tobool60 = icmp ne i32 %46, 0
  br i1 %tobool60, label %if.then63, label %lor.lhs.false61

lor.lhs.false61:                                  ; preds = %if.else59
  %47 = load i32, i32* %a_sg, align 4, !tbaa !6
  %tobool62 = icmp ne i32 %47, 0
  br i1 %tobool62, label %if.then63, label %if.else98

if.then63:                                        ; preds = %lor.lhs.false61, %if.else59
  %48 = bitcast i32* %uni_rfc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #3
  %49 = load i32, i32* %a_sg, align 4, !tbaa !6
  %tobool64 = icmp ne i32 %49, 0
  br i1 %tobool64, label %cond.true65, label %cond.false67

cond.true65:                                      ; preds = %if.then63
  %50 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %call66 = call i32 @has_uni_comp_refs(%struct.MB_MODE_INFO* %50)
  br label %cond.end69

cond.false67:                                     ; preds = %if.then63
  %51 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %call68 = call i32 @has_uni_comp_refs(%struct.MB_MODE_INFO* %51)
  br label %cond.end69

cond.end69:                                       ; preds = %cond.false67, %cond.true65
  %cond70 = phi i32 [ %call66, %cond.true65 ], [ %call68, %cond.false67 ]
  store i32 %cond70, i32* %uni_rfc, align 4, !tbaa !6
  %52 = load i32, i32* %uni_rfc, align 4, !tbaa !6
  %tobool71 = icmp ne i32 %52, 0
  br i1 %tobool71, label %if.else73, label %if.then72

if.then72:                                        ; preds = %cond.end69
  store i32 1, i32* %pred_context, align 4, !tbaa !6
  br label %if.end97

if.else73:                                        ; preds = %cond.end69
  %53 = load i8, i8* %frfa, align 1, !tbaa !14
  %conv74 = sext i8 %53 to i32
  %cmp75 = icmp sge i32 %conv74, 5
  br i1 %cmp75, label %land.rhs77, label %land.end81

land.rhs77:                                       ; preds = %if.else73
  %54 = load i8, i8* %frfa, align 1, !tbaa !14
  %conv78 = sext i8 %54 to i32
  %cmp79 = icmp sle i32 %conv78, 7
  br label %land.end81

land.end81:                                       ; preds = %land.rhs77, %if.else73
  %55 = phi i1 [ false, %if.else73 ], [ %cmp79, %land.rhs77 ]
  %land.ext82 = zext i1 %55 to i32
  %56 = load i8, i8* %frfl, align 1, !tbaa !14
  %conv83 = sext i8 %56 to i32
  %cmp84 = icmp sge i32 %conv83, 5
  br i1 %cmp84, label %land.rhs86, label %land.end90

land.rhs86:                                       ; preds = %land.end81
  %57 = load i8, i8* %frfl, align 1, !tbaa !14
  %conv87 = sext i8 %57 to i32
  %cmp88 = icmp sle i32 %conv87, 7
  br label %land.end90

land.end90:                                       ; preds = %land.rhs86, %land.end81
  %58 = phi i1 [ false, %land.end81 ], [ %cmp88, %land.rhs86 ]
  %land.ext91 = zext i1 %58 to i32
  %xor92 = xor i32 %land.ext82, %land.ext91
  %tobool93 = icmp ne i32 %xor92, 0
  %lnot94 = xor i1 %tobool93, true
  %lnot.ext95 = zext i1 %lnot94 to i32
  %add96 = add nsw i32 3, %lnot.ext95
  store i32 %add96, i32* %pred_context, align 4, !tbaa !6
  br label %if.end97

if.end97:                                         ; preds = %land.end90, %if.then72
  %59 = bitcast i32* %uni_rfc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  br label %if.end124

if.else98:                                        ; preds = %lor.lhs.false61
  %60 = bitcast i32* %a_uni_rfc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #3
  %61 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  %call99 = call i32 @has_uni_comp_refs(%struct.MB_MODE_INFO* %61)
  store i32 %call99, i32* %a_uni_rfc, align 4, !tbaa !6
  %62 = bitcast i32* %l_uni_rfc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #3
  %63 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  %call100 = call i32 @has_uni_comp_refs(%struct.MB_MODE_INFO* %63)
  store i32 %call100, i32* %l_uni_rfc, align 4, !tbaa !6
  %64 = load i32, i32* %a_uni_rfc, align 4, !tbaa !6
  %tobool101 = icmp ne i32 %64, 0
  br i1 %tobool101, label %if.else105, label %land.lhs.true102

land.lhs.true102:                                 ; preds = %if.else98
  %65 = load i32, i32* %l_uni_rfc, align 4, !tbaa !6
  %tobool103 = icmp ne i32 %65, 0
  br i1 %tobool103, label %if.else105, label %if.then104

if.then104:                                       ; preds = %land.lhs.true102
  store i32 0, i32* %pred_context, align 4, !tbaa !6
  br label %if.end123

if.else105:                                       ; preds = %land.lhs.true102, %if.else98
  %66 = load i32, i32* %a_uni_rfc, align 4, !tbaa !6
  %tobool106 = icmp ne i32 %66, 0
  br i1 %tobool106, label %lor.lhs.false107, label %if.then109

lor.lhs.false107:                                 ; preds = %if.else105
  %67 = load i32, i32* %l_uni_rfc, align 4, !tbaa !6
  %tobool108 = icmp ne i32 %67, 0
  br i1 %tobool108, label %if.else110, label %if.then109

if.then109:                                       ; preds = %lor.lhs.false107, %if.else105
  store i32 2, i32* %pred_context, align 4, !tbaa !6
  br label %if.end122

if.else110:                                       ; preds = %lor.lhs.false107
  %68 = load i8, i8* %frfa, align 1, !tbaa !14
  %conv111 = sext i8 %68 to i32
  %cmp112 = icmp eq i32 %conv111, 5
  %conv113 = zext i1 %cmp112 to i32
  %69 = load i8, i8* %frfl, align 1, !tbaa !14
  %conv114 = sext i8 %69 to i32
  %cmp115 = icmp eq i32 %conv114, 5
  %conv116 = zext i1 %cmp115 to i32
  %xor117 = xor i32 %conv113, %conv116
  %tobool118 = icmp ne i32 %xor117, 0
  %lnot119 = xor i1 %tobool118, true
  %lnot.ext120 = zext i1 %lnot119 to i32
  %add121 = add nsw i32 3, %lnot.ext120
  store i32 %add121, i32* %pred_context, align 4, !tbaa !6
  br label %if.end122

if.end122:                                        ; preds = %if.else110, %if.then109
  br label %if.end123

if.end123:                                        ; preds = %if.end122, %if.then104
  %70 = bitcast i32* %l_uni_rfc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #3
  %71 = bitcast i32* %a_uni_rfc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #3
  br label %if.end124

if.end124:                                        ; preds = %if.end123, %if.end97
  br label %if.end125

if.end125:                                        ; preds = %if.end124, %land.end52
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %frfl) #3
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %frfa) #3
  %72 = bitcast i32* %l_sg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast i32* %a_sg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  br label %if.end126

if.end126:                                        ; preds = %if.end125, %if.end
  br label %if.end127

if.end127:                                        ; preds = %if.end126, %if.then15
  %74 = bitcast i32* %left_intra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  %75 = bitcast i32* %above_intra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  br label %if.end152

if.else128:                                       ; preds = %land.lhs.true, %entry
  %76 = load i32, i32* %above_in_image, align 4, !tbaa !6
  %tobool129 = icmp ne i32 %76, 0
  br i1 %tobool129, label %if.then132, label %lor.lhs.false130

lor.lhs.false130:                                 ; preds = %if.else128
  %77 = load i32, i32* %left_in_image, align 4, !tbaa !6
  %tobool131 = icmp ne i32 %77, 0
  br i1 %tobool131, label %if.then132, label %if.else150

if.then132:                                       ; preds = %lor.lhs.false130, %if.else128
  %78 = bitcast %struct.MB_MODE_INFO** %edge_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #3
  %79 = load i32, i32* %above_in_image, align 4, !tbaa !6
  %tobool133 = icmp ne i32 %79, 0
  br i1 %tobool133, label %cond.true134, label %cond.false135

cond.true134:                                     ; preds = %if.then132
  %80 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !2
  br label %cond.end136

cond.false135:                                    ; preds = %if.then132
  %81 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !2
  br label %cond.end136

cond.end136:                                      ; preds = %cond.false135, %cond.true134
  %cond137 = phi %struct.MB_MODE_INFO* [ %80, %cond.true134 ], [ %81, %cond.false135 ]
  store %struct.MB_MODE_INFO* %cond137, %struct.MB_MODE_INFO** %edge_mbmi, align 4, !tbaa !2
  %82 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %edge_mbmi, align 4, !tbaa !2
  %call138 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %82)
  %tobool139 = icmp ne i32 %call138, 0
  br i1 %tobool139, label %if.else141, label %if.then140

if.then140:                                       ; preds = %cond.end136
  store i32 2, i32* %pred_context, align 4, !tbaa !6
  br label %if.end149

if.else141:                                       ; preds = %cond.end136
  %83 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %edge_mbmi, align 4, !tbaa !2
  %call142 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %83)
  %tobool143 = icmp ne i32 %call142, 0
  br i1 %tobool143, label %if.else145, label %if.then144

if.then144:                                       ; preds = %if.else141
  store i32 2, i32* %pred_context, align 4, !tbaa !6
  br label %if.end148

if.else145:                                       ; preds = %if.else141
  %84 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %edge_mbmi, align 4, !tbaa !2
  %call146 = call i32 @has_uni_comp_refs(%struct.MB_MODE_INFO* %84)
  %mul147 = mul nsw i32 4, %call146
  store i32 %mul147, i32* %pred_context, align 4, !tbaa !6
  br label %if.end148

if.end148:                                        ; preds = %if.else145, %if.then144
  br label %if.end149

if.end149:                                        ; preds = %if.end148, %if.then140
  %85 = bitcast %struct.MB_MODE_INFO** %edge_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #3
  br label %if.end151

if.else150:                                       ; preds = %lor.lhs.false130
  store i32 2, i32* %pred_context, align 4, !tbaa !6
  br label %if.end151

if.end151:                                        ; preds = %if.else150, %if.end149
  br label %if.end152

if.end152:                                        ; preds = %if.end151, %if.end127
  %86 = load i32, i32* %pred_context, align 4, !tbaa !6
  %87 = bitcast i32* %left_in_image to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #3
  %88 = bitcast i32* %above_in_image to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #3
  %89 = bitcast %struct.MB_MODE_INFO** %left_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #3
  %90 = bitcast %struct.MB_MODE_INFO** %above_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #3
  %91 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #3
  ret i32 %86
}

; Function Attrs: inlinehint nounwind
define internal i32 @has_uni_comp_refs(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @has_second_ref(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !14
  %conv = sext i8 %2 to i32
  %cmp = icmp sge i32 %conv, 5
  %conv1 = zext i1 %cmp to i32
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %3, i32 0, i32 12
  %arrayidx3 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame2, i32 0, i32 1
  %4 = load i8, i8* %arrayidx3, align 1, !tbaa !14
  %conv4 = sext i8 %4 to i32
  %cmp5 = icmp sge i32 %conv4, 5
  %conv6 = zext i1 %cmp5 to i32
  %xor = xor i32 %conv1, %conv6
  %tobool7 = icmp ne i32 %xor, 0
  %lnot = xor i1 %tobool7, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %5 = phi i1 [ false, %entry ], [ %lnot, %land.rhs ]
  %land.ext = zext i1 %5 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_uni_comp_ref_p(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %frf_count = alloca i32, align 4
  %brf_count = alloca i32, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 39
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts, i32 0, i32 0
  store i8* %arrayidx, i8** %ref_counts, align 4, !tbaa !2
  %2 = bitcast i32* %frf_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = zext i8 %4 to i32
  %5 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 2
  %6 = load i8, i8* %arrayidx2, align 1, !tbaa !14
  %conv3 = zext i8 %6 to i32
  %add = add nsw i32 %conv, %conv3
  %7 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %7, i32 3
  %8 = load i8, i8* %arrayidx4, align 1, !tbaa !14
  %conv5 = zext i8 %8 to i32
  %add6 = add nsw i32 %add, %conv5
  %9 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %9, i32 4
  %10 = load i8, i8* %arrayidx7, align 1, !tbaa !14
  %conv8 = zext i8 %10 to i32
  %add9 = add nsw i32 %add6, %conv8
  store i32 %add9, i32* %frf_count, align 4, !tbaa !6
  %11 = bitcast i32* %brf_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %12, i32 5
  %13 = load i8, i8* %arrayidx10, align 1, !tbaa !14
  %conv11 = zext i8 %13 to i32
  %14 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %14, i32 6
  %15 = load i8, i8* %arrayidx12, align 1, !tbaa !14
  %conv13 = zext i8 %15 to i32
  %add14 = add nsw i32 %conv11, %conv13
  %16 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %16, i32 7
  %17 = load i8, i8* %arrayidx15, align 1, !tbaa !14
  %conv16 = zext i8 %17 to i32
  %add17 = add nsw i32 %add14, %conv16
  store i32 %add17, i32* %brf_count, align 4, !tbaa !6
  %18 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = load i32, i32* %frf_count, align 4, !tbaa !6
  %20 = load i32, i32* %brf_count, align 4, !tbaa !6
  %cmp = icmp eq i32 %19, %20
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %21 = load i32, i32* %frf_count, align 4, !tbaa !6
  %22 = load i32, i32* %brf_count, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %21, %22
  %23 = zext i1 %cmp19 to i64
  %cond = select i1 %cmp19, i32 0, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond21 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond21, i32* %pred_context, align 4, !tbaa !6
  %24 = load i32, i32* %pred_context, align 4, !tbaa !6
  %25 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #3
  %26 = bitcast i32* %brf_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  %27 = bitcast i32* %frf_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  %28 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  ret i32 %24
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_uni_comp_ref_p1(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %last2_count = alloca i32, align 4
  %last3_or_gld_count = alloca i32, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 39
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts, i32 0, i32 0
  store i8* %arrayidx, i8** %ref_counts, align 4, !tbaa !2
  %2 = bitcast i32* %last2_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 2
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %last2_count, align 4, !tbaa !6
  %5 = bitcast i32* %last3_or_gld_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 3
  %7 = load i8, i8* %arrayidx2, align 1, !tbaa !14
  %conv3 = zext i8 %7 to i32
  %8 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %8, i32 4
  %9 = load i8, i8* %arrayidx4, align 1, !tbaa !14
  %conv5 = zext i8 %9 to i32
  %add = add nsw i32 %conv3, %conv5
  store i32 %add, i32* %last3_or_gld_count, align 4, !tbaa !6
  %10 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load i32, i32* %last2_count, align 4, !tbaa !6
  %12 = load i32, i32* %last3_or_gld_count, align 4, !tbaa !6
  %cmp = icmp eq i32 %11, %12
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %13 = load i32, i32* %last2_count, align 4, !tbaa !6
  %14 = load i32, i32* %last3_or_gld_count, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %13, %14
  %15 = zext i1 %cmp7 to i64
  %cond = select i1 %cmp7, i32 0, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond9 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond9, i32* %pred_context, align 4, !tbaa !6
  %16 = load i32, i32* %pred_context, align 4, !tbaa !6
  %17 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i32* %last3_or_gld_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %last2_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  ret i32 %16
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_uni_comp_ref_p2(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %last3_count = alloca i32, align 4
  %gld_count = alloca i32, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 39
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts, i32 0, i32 0
  store i8* %arrayidx, i8** %ref_counts, align 4, !tbaa !2
  %2 = bitcast i32* %last3_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 3
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %last3_count, align 4, !tbaa !6
  %5 = bitcast i32* %gld_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 4
  %7 = load i8, i8* %arrayidx2, align 1, !tbaa !14
  %conv3 = zext i8 %7 to i32
  store i32 %conv3, i32* %gld_count, align 4, !tbaa !6
  %8 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load i32, i32* %last3_count, align 4, !tbaa !6
  %10 = load i32, i32* %gld_count, align 4, !tbaa !6
  %cmp = icmp eq i32 %9, %10
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i32, i32* %last3_count, align 4, !tbaa !6
  %12 = load i32, i32* %gld_count, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %11, %12
  %13 = zext i1 %cmp5 to i64
  %cond = select i1 %cmp5, i32 0, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond7 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond7, i32* %pred_context, align 4, !tbaa !6
  %14 = load i32, i32* %pred_context, align 4, !tbaa !6
  %15 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %gld_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %last3_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  ret i32 %14
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_comp_ref_p(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_ll2_or_l3gld(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @get_pred_context_ll2_or_l3gld(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %last_last2_count = alloca i32, align 4
  %last3_gld_count = alloca i32, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 39
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts, i32 0, i32 0
  store i8* %arrayidx, i8** %ref_counts, align 4, !tbaa !2
  %2 = bitcast i32* %last_last2_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = zext i8 %4 to i32
  %5 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 2
  %6 = load i8, i8* %arrayidx2, align 1, !tbaa !14
  %conv3 = zext i8 %6 to i32
  %add = add nsw i32 %conv, %conv3
  store i32 %add, i32* %last_last2_count, align 4, !tbaa !6
  %7 = bitcast i32* %last3_gld_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %8, i32 3
  %9 = load i8, i8* %arrayidx4, align 1, !tbaa !14
  %conv5 = zext i8 %9 to i32
  %10 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %10, i32 4
  %11 = load i8, i8* %arrayidx6, align 1, !tbaa !14
  %conv7 = zext i8 %11 to i32
  %add8 = add nsw i32 %conv5, %conv7
  store i32 %add8, i32* %last3_gld_count, align 4, !tbaa !6
  %12 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load i32, i32* %last_last2_count, align 4, !tbaa !6
  %14 = load i32, i32* %last3_gld_count, align 4, !tbaa !6
  %cmp = icmp eq i32 %13, %14
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %15 = load i32, i32* %last_last2_count, align 4, !tbaa !6
  %16 = load i32, i32* %last3_gld_count, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %15, %16
  %17 = zext i1 %cmp10 to i64
  %cond = select i1 %cmp10, i32 0, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond12 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond12, i32* %pred_context, align 4, !tbaa !6
  %18 = load i32, i32* %pred_context, align 4, !tbaa !6
  %19 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %last3_gld_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  %21 = bitcast i32* %last_last2_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  %22 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #3
  ret i32 %18
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_comp_ref_p1(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_last_or_last2(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @get_pred_context_last_or_last2(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %last_count = alloca i32, align 4
  %last2_count = alloca i32, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 39
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts, i32 0, i32 0
  store i8* %arrayidx, i8** %ref_counts, align 4, !tbaa !2
  %2 = bitcast i32* %last_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %last_count, align 4, !tbaa !6
  %5 = bitcast i32* %last2_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 2
  %7 = load i8, i8* %arrayidx2, align 1, !tbaa !14
  %conv3 = zext i8 %7 to i32
  store i32 %conv3, i32* %last2_count, align 4, !tbaa !6
  %8 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load i32, i32* %last_count, align 4, !tbaa !6
  %10 = load i32, i32* %last2_count, align 4, !tbaa !6
  %cmp = icmp eq i32 %9, %10
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i32, i32* %last_count, align 4, !tbaa !6
  %12 = load i32, i32* %last2_count, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %11, %12
  %13 = zext i1 %cmp5 to i64
  %cond = select i1 %cmp5, i32 0, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond7 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond7, i32* %pred_context, align 4, !tbaa !6
  %14 = load i32, i32* %pred_context, align 4, !tbaa !6
  %15 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %last2_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %last_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  ret i32 %14
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_comp_ref_p2(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_last3_or_gld(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @get_pred_context_last3_or_gld(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %last3_count = alloca i32, align 4
  %gld_count = alloca i32, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 39
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts, i32 0, i32 0
  store i8* %arrayidx, i8** %ref_counts, align 4, !tbaa !2
  %2 = bitcast i32* %last3_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 3
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %last3_count, align 4, !tbaa !6
  %5 = bitcast i32* %gld_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 4
  %7 = load i8, i8* %arrayidx2, align 1, !tbaa !14
  %conv3 = zext i8 %7 to i32
  store i32 %conv3, i32* %gld_count, align 4, !tbaa !6
  %8 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load i32, i32* %last3_count, align 4, !tbaa !6
  %10 = load i32, i32* %gld_count, align 4, !tbaa !6
  %cmp = icmp eq i32 %9, %10
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i32, i32* %last3_count, align 4, !tbaa !6
  %12 = load i32, i32* %gld_count, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %11, %12
  %13 = zext i1 %cmp5 to i64
  %cond = select i1 %cmp5, i32 0, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond7 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond7, i32* %pred_context, align 4, !tbaa !6
  %14 = load i32, i32* %pred_context, align 4, !tbaa !6
  %15 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %gld_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %last3_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  ret i32 %14
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_comp_bwdref_p(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_brfarf2_or_arf(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @get_pred_context_brfarf2_or_arf(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %brfarf2_count = alloca i32, align 4
  %arf_count = alloca i32, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 39
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts, i32 0, i32 0
  store i8* %arrayidx, i8** %ref_counts, align 4, !tbaa !2
  %2 = bitcast i32* %brfarf2_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 5
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = zext i8 %4 to i32
  %5 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 6
  %6 = load i8, i8* %arrayidx2, align 1, !tbaa !14
  %conv3 = zext i8 %6 to i32
  %add = add nsw i32 %conv, %conv3
  store i32 %add, i32* %brfarf2_count, align 4, !tbaa !6
  %7 = bitcast i32* %arf_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %8, i32 7
  %9 = load i8, i8* %arrayidx4, align 1, !tbaa !14
  %conv5 = zext i8 %9 to i32
  store i32 %conv5, i32* %arf_count, align 4, !tbaa !6
  %10 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load i32, i32* %brfarf2_count, align 4, !tbaa !6
  %12 = load i32, i32* %arf_count, align 4, !tbaa !6
  %cmp = icmp eq i32 %11, %12
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %13 = load i32, i32* %brfarf2_count, align 4, !tbaa !6
  %14 = load i32, i32* %arf_count, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %13, %14
  %15 = zext i1 %cmp7 to i64
  %cond = select i1 %cmp7, i32 0, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond9 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond9, i32* %pred_context, align 4, !tbaa !6
  %16 = load i32, i32* %pred_context, align 4, !tbaa !6
  %17 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i32* %arf_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %brfarf2_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  ret i32 %16
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_comp_bwdref_p1(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_brf_or_arf2(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @get_pred_context_brf_or_arf2(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %brf_count = alloca i32, align 4
  %arf2_count = alloca i32, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 39
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts, i32 0, i32 0
  store i8* %arrayidx, i8** %ref_counts, align 4, !tbaa !2
  %2 = bitcast i32* %brf_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 5
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %brf_count, align 4, !tbaa !6
  %5 = bitcast i32* %arf2_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 6
  %7 = load i8, i8* %arrayidx2, align 1, !tbaa !14
  %conv3 = zext i8 %7 to i32
  store i32 %conv3, i32* %arf2_count, align 4, !tbaa !6
  %8 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load i32, i32* %brf_count, align 4, !tbaa !6
  %10 = load i32, i32* %arf2_count, align 4, !tbaa !6
  %cmp = icmp eq i32 %9, %10
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i32, i32* %brf_count, align 4, !tbaa !6
  %12 = load i32, i32* %arf2_count, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %11, %12
  %13 = zext i1 %cmp5 to i64
  %cond = select i1 %cmp5, i32 0, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond7 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond7, i32* %pred_context, align 4, !tbaa !6
  %14 = load i32, i32* %pred_context, align 4, !tbaa !6
  %15 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %arf2_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %brf_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  ret i32 %14
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_single_ref_p1(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %fwd_count = alloca i32, align 4
  %bwd_count = alloca i32, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 39
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts, i32 0, i32 0
  store i8* %arrayidx, i8** %ref_counts, align 4, !tbaa !2
  %2 = bitcast i32* %fwd_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !14
  %conv = zext i8 %4 to i32
  %5 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 2
  %6 = load i8, i8* %arrayidx2, align 1, !tbaa !14
  %conv3 = zext i8 %6 to i32
  %add = add nsw i32 %conv, %conv3
  %7 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %7, i32 3
  %8 = load i8, i8* %arrayidx4, align 1, !tbaa !14
  %conv5 = zext i8 %8 to i32
  %add6 = add nsw i32 %add, %conv5
  %9 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %9, i32 4
  %10 = load i8, i8* %arrayidx7, align 1, !tbaa !14
  %conv8 = zext i8 %10 to i32
  %add9 = add nsw i32 %add6, %conv8
  store i32 %add9, i32* %fwd_count, align 4, !tbaa !6
  %11 = bitcast i32* %bwd_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %12, i32 5
  %13 = load i8, i8* %arrayidx10, align 1, !tbaa !14
  %conv11 = zext i8 %13 to i32
  %14 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %14, i32 6
  %15 = load i8, i8* %arrayidx12, align 1, !tbaa !14
  %conv13 = zext i8 %15 to i32
  %add14 = add nsw i32 %conv11, %conv13
  %16 = load i8*, i8** %ref_counts, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %16, i32 7
  %17 = load i8, i8* %arrayidx15, align 1, !tbaa !14
  %conv16 = zext i8 %17 to i32
  %add17 = add nsw i32 %add14, %conv16
  store i32 %add17, i32* %bwd_count, align 4, !tbaa !6
  %18 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = load i32, i32* %fwd_count, align 4, !tbaa !6
  %20 = load i32, i32* %bwd_count, align 4, !tbaa !6
  %cmp = icmp eq i32 %19, %20
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %21 = load i32, i32* %fwd_count, align 4, !tbaa !6
  %22 = load i32, i32* %bwd_count, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %21, %22
  %23 = zext i1 %cmp19 to i64
  %cond = select i1 %cmp19, i32 0, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond21 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond21, i32* %pred_context, align 4, !tbaa !6
  %24 = load i32, i32* %pred_context, align 4, !tbaa !6
  %25 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #3
  %26 = bitcast i32* %bwd_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  %27 = bitcast i32* %fwd_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  %28 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  ret i32 %24
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_single_ref_p2(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_brfarf2_or_arf(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_single_ref_p3(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_ll2_or_l3gld(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_single_ref_p4(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_last_or_last2(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_single_ref_p5(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_last3_or_gld(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @av1_get_pred_context_single_ref_p6(%struct.macroblockd* %xd) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @get_pred_context_brf_or_arf2(%struct.macroblockd* %0)
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_extract_interp_filter(%union.int_interpfilters* byval(%union.int_interpfilters) align 4 %filters, i32 %dir) #2 {
entry:
  %dir.addr = alloca i32, align 4
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  %0 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %as_filters = bitcast %union.int_interpfilters* %filters to %struct.InterpFilters*
  %x_filter = getelementptr inbounds %struct.InterpFilters, %struct.InterpFilters* %as_filters, i32 0, i32 1
  %1 = load i16, i16* %x_filter, align 2, !tbaa !14
  %conv = zext i16 %1 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %as_filters1 = bitcast %union.int_interpfilters* %filters to %struct.InterpFilters*
  %y_filter = getelementptr inbounds %struct.InterpFilters, %struct.InterpFilters* %as_filters1, i32 0, i32 0
  %2 = load i16, i16* %y_filter, align 4, !tbaa !14
  %conv2 = zext i16 %2 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %conv2, %cond.false ]
  %conv3 = trunc i32 %cond to i8
  ret i8 %conv3
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_intrabc_block(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  ret i32 %conv
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 4084}
!9 = !{!"macroblockd", !7, i64 0, !7, i64 4, !7, i64 8, !10, i64 12, !4, i64 16, !11, i64 4060, !3, i64 4084, !10, i64 4088, !10, i64 4089, !10, i64 4090, !10, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !7, i64 4112, !7, i64 4116, !7, i64 4120, !7, i64 4124, !7, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !7, i64 6836, !4, i64 6840, !4, i64 6872, !7, i64 6904, !7, i64 6908, !3, i64 6912, !3, i64 6916, !7, i64 6920, !7, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !12, i64 39720, !13, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!10 = !{!"_Bool", !4, i64 0}
!11 = !{!"TileInfo", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20}
!12 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !7, i64 4104, !4, i64 4108, !7, i64 4236, !7, i64 4240, !7, i64 4244, !7, i64 4248, !7, i64 4252, !7, i64 4256}
!13 = !{!"dist_wtd_comp_params", !7, i64 0, !7, i64 4, !7, i64 8}
!14 = !{!4, !4, i64 0}
!15 = !{!9, !10, i64 4089}
!16 = !{i8 0, i8 2}
!17 = !{!9, !10, i64 4088}
!18 = !{!9, !7, i64 8}
!19 = !{!9, !7, i64 4124}
!20 = !{!9, !3, i64 4096}
!21 = !{!9, !3, i64 4092}
!22 = !{!23, !23, i64 0}
!23 = !{!"short", !4, i64 0}
