; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/restoration.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/restoration.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.sgr_params_type = type { [2 x i32], [2 x i32] }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.AV1PixelRect = type { i32, i32, i32, i32 }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.RestorationTileLimits = type { i32, i32, i32, i32 }
%struct.AV1LrStruct = type { void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, [3 x %struct.FilterFrameCtxt], %struct.yv12_buffer_config*, %struct.yv12_buffer_config* }
%struct.FilterFrameCtxt = type { %struct.RestorationInfo*, i32, i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.AV1PixelRect }
%struct.AV1LrSyncData = type opaque

@av1_sgr_params = hidden constant [16 x %struct.sgr_params_type] [%struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 140, i32 3236] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 112, i32 2158] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 93, i32 1618] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 80, i32 1438] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 70, i32 1295] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 58, i32 1177] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 47, i32 1079] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 37, i32 996] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 30, i32 925] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 1], [2 x i32] [i32 25, i32 863] }, %struct.sgr_params_type { [2 x i32] [i32 0, i32 1], [2 x i32] [i32 -1, i32 2589] }, %struct.sgr_params_type { [2 x i32] [i32 0, i32 1], [2 x i32] [i32 -1, i32 1618] }, %struct.sgr_params_type { [2 x i32] [i32 0, i32 1], [2 x i32] [i32 -1, i32 1177] }, %struct.sgr_params_type { [2 x i32] [i32 0, i32 1], [2 x i32] [i32 -1, i32 925] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 0], [2 x i32] [i32 56, i32 -1] }, %struct.sgr_params_type { [2 x i32] [i32 2, i32 0], [2 x i32] [i32 22, i32 -1] }], align 16
@.str = private unnamed_addr constant [34 x i8] c"Failed to allocate rsi->unit_info\00", align 1
@av1_x_by_xplus1 = hidden constant [256 x i32] [i32 1, i32 128, i32 171, i32 192, i32 205, i32 213, i32 219, i32 224, i32 228, i32 230, i32 233, i32 235, i32 236, i32 238, i32 239, i32 240, i32 241, i32 242, i32 243, i32 243, i32 244, i32 244, i32 245, i32 245, i32 246, i32 246, i32 247, i32 247, i32 247, i32 247, i32 248, i32 248, i32 248, i32 248, i32 249, i32 249, i32 249, i32 249, i32 249, i32 250, i32 250, i32 250, i32 250, i32 250, i32 250, i32 250, i32 251, i32 251, i32 251, i32 251, i32 251, i32 251, i32 251, i32 251, i32 251, i32 251, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 252, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 253, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 254, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 256], align 16
@av1_one_by_x = hidden constant [25 x i32] [i32 4096, i32 2048, i32 1365, i32 1024, i32 819, i32 683, i32 585, i32 512, i32 455, i32 410, i32 372, i32 341, i32 315, i32 293, i32 273, i32 256, i32 241, i32 228, i32 216, i32 205, i32 195, i32 186, i32 178, i32 171, i32 164], align 16
@stripe_filters = internal constant [4 x void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)*] [void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)* @wiener_filter_stripe, void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)* @sgrproj_filter_stripe, void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)* @wiener_filter_stripe_highbd, void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)* @sgrproj_filter_stripe_highbd], align 16
@.str.1 = private unnamed_addr constant [42 x i8] c"Failed to allocate restoration dst buffer\00", align 1
@av1_loop_restoration_copy_planes.copy_funs = internal constant [3 x void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)*] [void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)* @aom_yv12_partial_coloc_copy_y_c, void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)* @aom_yv12_partial_coloc_copy_u_c, void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)* @aom_yv12_partial_coloc_copy_v_c], align 4
@mi_size_high = internal constant [22 x i8] c"\01\02\01\02\04\02\04\08\04\08\10\08\10 \10 \04\01\08\02\10\04", align 16
@mi_size_wide = internal constant [22 x i8] c"\01\01\02\02\02\04\04\04\08\08\08\10\10\10  \01\04\02\08\04\10", align 16

; Function Attrs: nounwind
define hidden void @av1_whole_frame_rect(%struct.AV1PixelRect* noalias sret align 4 %agg.result, %struct.AV1Common* %cm, i32 %is_uv) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %is_uv.addr = alloca i32, align 4
  %ss_x = alloca i32, align 4
  %ss_y = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %is_uv, i32* %is_uv.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %is_uv.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 37
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 32
  %3 = load i32, i32* %subsampling_x, align 16, !tbaa !8
  %tobool1 = icmp ne i32 %3, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %4 = phi i1 [ false, %entry ], [ %tobool1, %land.rhs ]
  %land.ext = zext i1 %4 to i32
  store i32 %land.ext, i32* %ss_x, align 4, !tbaa !6
  %5 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %is_uv.addr, align 4, !tbaa !6
  %tobool2 = icmp ne i32 %6, 0
  br i1 %tobool2, label %land.rhs3, label %land.end6

land.rhs3:                                        ; preds = %land.end
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 37
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params4, i32 0, i32 33
  %8 = load i32, i32* %subsampling_y, align 4, !tbaa !32
  %tobool5 = icmp ne i32 %8, 0
  br label %land.end6

land.end6:                                        ; preds = %land.rhs3, %land.end
  %9 = phi i1 [ false, %land.end ], [ %tobool5, %land.rhs3 ]
  %land.ext7 = zext i1 %9 to i32
  store i32 %land.ext7, i32* %ss_y, align 4, !tbaa !6
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 1
  store i32 0, i32* %top, align 4, !tbaa !33
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 3
  %11 = load i32, i32* %height, align 4, !tbaa !34
  %12 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shl = shl i32 1, %12
  %shr = ashr i32 %shl, 1
  %add = add nsw i32 %11, %shr
  %13 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shr8 = ashr i32 %add, %13
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 3
  store i32 %shr8, i32* %bottom, align 4, !tbaa !35
  %left = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 0
  store i32 0, i32* %left, align 4, !tbaa !36
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 6
  %15 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !37
  %16 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shl9 = shl i32 1, %16
  %shr10 = ashr i32 %shl9, 1
  %add11 = add nsw i32 %15, %shr10
  %17 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shr12 = ashr i32 %add11, %17
  %right = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 2
  store i32 %shr12, i32* %right, align 4, !tbaa !38
  %18 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @av1_lr_count_units_in_tile(i32 %unit_size, i32 %tile_size) #0 {
entry:
  %unit_size.addr = alloca i32, align 4
  %tile_size.addr = alloca i32, align 4
  store i32 %unit_size, i32* %unit_size.addr, align 4, !tbaa !6
  store i32 %tile_size, i32* %tile_size.addr, align 4, !tbaa !6
  %0 = load i32, i32* %tile_size.addr, align 4, !tbaa !6
  %1 = load i32, i32* %unit_size.addr, align 4, !tbaa !6
  %shr = ashr i32 %1, 1
  %add = add nsw i32 %0, %shr
  %2 = load i32, i32* %unit_size.addr, align 4, !tbaa !6
  %div = sdiv i32 %add, %2
  %cmp = icmp sgt i32 %div, 1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load i32, i32* %tile_size.addr, align 4, !tbaa !6
  %4 = load i32, i32* %unit_size.addr, align 4, !tbaa !6
  %shr1 = ashr i32 %4, 1
  %add2 = add nsw i32 %3, %shr1
  %5 = load i32, i32* %unit_size.addr, align 4, !tbaa !6
  %div3 = sdiv i32 %add2, %5
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %div3, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define hidden void @av1_alloc_restoration_struct(%struct.AV1Common* %cm, %struct.RestorationInfo* %rsi, i32 %is_uv) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %rsi.addr = alloca %struct.RestorationInfo*, align 4
  %is_uv.addr = alloca i32, align 4
  %tile_rect = alloca %struct.AV1PixelRect, align 4
  %max_tile_w = alloca i32, align 4
  %max_tile_h = alloca i32, align 4
  %unit_size = alloca i32, align 4
  %hpertile = alloca i32, align 4
  %vpertile = alloca i32, align 4
  %ntiles = alloca i32, align 4
  %nunits = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.RestorationInfo* %rsi, %struct.RestorationInfo** %rsi.addr, align 4, !tbaa !2
  store i32 %is_uv, i32* %is_uv.addr, align 4, !tbaa !6
  %0 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %2 = load i32, i32* %is_uv.addr, align 4, !tbaa !6
  call void @av1_whole_frame_rect(%struct.AV1PixelRect* sret align 4 %tile_rect, %struct.AV1Common* %1, i32 %2)
  %3 = bitcast i32* %max_tile_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %right = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 2
  %4 = load i32, i32* %right, align 4, !tbaa !38
  %left = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 0
  %5 = load i32, i32* %left, align 4, !tbaa !36
  %sub = sub nsw i32 %4, %5
  store i32 %sub, i32* %max_tile_w, align 4, !tbaa !6
  %6 = bitcast i32* %max_tile_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %7 = load i32, i32* %bottom, align 4, !tbaa !35
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %8 = load i32, i32* %top, align 4, !tbaa !33
  %sub1 = sub nsw i32 %7, %8
  store i32 %sub1, i32* %max_tile_h, align 4, !tbaa !6
  %9 = bitcast i32* %unit_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi.addr, align 4, !tbaa !2
  %restoration_unit_size = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %10, i32 0, i32 1
  %11 = load i32, i32* %restoration_unit_size, align 4, !tbaa !39
  store i32 %11, i32* %unit_size, align 4, !tbaa !6
  %12 = bitcast i32* %hpertile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load i32, i32* %unit_size, align 4, !tbaa !6
  %14 = load i32, i32* %max_tile_w, align 4, !tbaa !6
  %call = call i32 @av1_lr_count_units_in_tile(i32 %13, i32 %14)
  store i32 %call, i32* %hpertile, align 4, !tbaa !6
  %15 = bitcast i32* %vpertile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load i32, i32* %unit_size, align 4, !tbaa !6
  %17 = load i32, i32* %max_tile_h, align 4, !tbaa !6
  %call2 = call i32 @av1_lr_count_units_in_tile(i32 %16, i32 %17)
  store i32 %call2, i32* %vpertile, align 4, !tbaa !6
  %18 = load i32, i32* %hpertile, align 4, !tbaa !6
  %19 = load i32, i32* %vpertile, align 4, !tbaa !6
  %mul = mul nsw i32 %18, %19
  %20 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi.addr, align 4, !tbaa !2
  %units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %20, i32 0, i32 2
  store i32 %mul, i32* %units_per_tile, align 4, !tbaa !42
  %21 = load i32, i32* %hpertile, align 4, !tbaa !6
  %22 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi.addr, align 4, !tbaa !2
  %horz_units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %22, i32 0, i32 4
  store i32 %21, i32* %horz_units_per_tile, align 4, !tbaa !43
  %23 = load i32, i32* %vpertile, align 4, !tbaa !6
  %24 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi.addr, align 4, !tbaa !2
  %vert_units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %24, i32 0, i32 3
  store i32 %23, i32* %vert_units_per_tile, align 4, !tbaa !44
  %25 = bitcast i32* %ntiles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  store i32 1, i32* %ntiles, align 4, !tbaa !6
  %26 = bitcast i32* %nunits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #5
  %27 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi.addr, align 4, !tbaa !2
  %units_per_tile3 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %27, i32 0, i32 2
  %28 = load i32, i32* %units_per_tile3, align 4, !tbaa !42
  %mul4 = mul nsw i32 1, %28
  store i32 %mul4, i32* %nunits, align 4, !tbaa !6
  %29 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi.addr, align 4, !tbaa !2
  %unit_info = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %29, i32 0, i32 5
  %30 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %unit_info, align 4, !tbaa !45
  %31 = bitcast %struct.RestorationUnitInfo* %30 to i8*
  call void @aom_free(i8* %31)
  br label %do.body

do.body:                                          ; preds = %entry
  %32 = load i32, i32* %nunits, align 4, !tbaa !6
  %mul5 = mul i32 64, %32
  %call6 = call i8* @aom_memalign(i32 16, i32 %mul5)
  %33 = bitcast i8* %call6 to %struct.RestorationUnitInfo*
  %34 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi.addr, align 4, !tbaa !2
  %unit_info7 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %34, i32 0, i32 5
  store %struct.RestorationUnitInfo* %33, %struct.RestorationUnitInfo** %unit_info7, align 4, !tbaa !45
  %35 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi.addr, align 4, !tbaa !2
  %unit_info8 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %35, i32 0, i32 5
  %36 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %unit_info8, align 4, !tbaa !45
  %tobool = icmp ne %struct.RestorationUnitInfo* %36, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  %37 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %37, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 2, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %38 = bitcast i32* %nunits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #5
  %39 = bitcast i32* %ntiles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #5
  %40 = bitcast i32* %vpertile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #5
  %41 = bitcast i32* %hpertile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #5
  %42 = bitcast i32* %unit_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #5
  %43 = bitcast i32* %max_tile_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #5
  %44 = bitcast i32* %max_tile_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #5
  %45 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #5
  ret void
}

declare void @aom_free(i8*) #2

declare i8* @aom_memalign(i32, i32) #2

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #2

; Function Attrs: nounwind
define hidden void @av1_free_restoration_struct(%struct.RestorationInfo* %rst_info) #0 {
entry:
  %rst_info.addr = alloca %struct.RestorationInfo*, align 4
  store %struct.RestorationInfo* %rst_info, %struct.RestorationInfo** %rst_info.addr, align 4, !tbaa !2
  %0 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rst_info.addr, align 4, !tbaa !2
  %unit_info = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %0, i32 0, i32 5
  %1 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %unit_info, align 4, !tbaa !45
  %2 = bitcast %struct.RestorationUnitInfo* %1 to i8*
  call void @aom_free(i8* %2)
  %3 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rst_info.addr, align 4, !tbaa !2
  %unit_info1 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %3, i32 0, i32 5
  store %struct.RestorationUnitInfo* null, %struct.RestorationUnitInfo** %unit_info1, align 4, !tbaa !45
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_loop_restoration_precal() #0 {
entry:
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_extend_frame(i8* %data, i32 %width, i32 %height, i32 %stride, i32 %border_horz, i32 %border_vert, i32 %highbd) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %border_horz.addr = alloca i32, align 4
  %border_vert.addr = alloca i32, align 4
  %highbd.addr = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %border_horz, i32* %border_horz.addr, align 4, !tbaa !6
  store i32 %border_vert, i32* %border_vert.addr, align 4, !tbaa !6
  store i32 %highbd, i32* %highbd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %2 = ptrtoint i8* %1 to i32
  %shl = shl i32 %2, 1
  %3 = inttoptr i32 %shl to i16*
  %4 = load i32, i32* %width.addr, align 4, !tbaa !6
  %5 = load i32, i32* %height.addr, align 4, !tbaa !6
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %7 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %8 = load i32, i32* %border_vert.addr, align 4, !tbaa !6
  call void @extend_frame_highbd(i16* %3, i32 %4, i32 %5, i32 %6, i32 %7, i32 %8)
  br label %return

if.end:                                           ; preds = %entry
  %9 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %10 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %11 = load i32, i32* %width.addr, align 4, !tbaa !6
  %12 = load i32, i32* %height.addr, align 4, !tbaa !6
  %13 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %14 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %15 = load i32, i32* %border_vert.addr, align 4, !tbaa !6
  call void @extend_frame_lowbd(i8* %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @extend_frame_highbd(i16* %data, i32 %width, i32 %height, i32 %stride, i32 %border_horz, i32 %border_vert) #0 {
entry:
  %data.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %border_horz.addr = alloca i32, align 4
  %border_vert.addr = alloca i32, align 4
  %data_p = alloca i16*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i16* %data, i16** %data.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %border_horz, i32* %border_horz.addr, align 4, !tbaa !6
  store i32 %border_vert, i32* %border_vert.addr, align 4, !tbaa !6
  %0 = bitcast i16** %data_p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc14, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end16

for.body:                                         ; preds = %for.cond
  %5 = load i16*, i16** %data.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %6, %7
  %add.ptr = getelementptr inbounds i16, i16* %5, i32 %mul
  store i16* %add.ptr, i16** %data_p, align 4, !tbaa !2
  %8 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %8
  store i32 %sub, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %j, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %9, 0
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %10 = load i16*, i16** %data_p, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %10, i32 0
  %11 = load i16, i16* %arrayidx, align 2, !tbaa !46
  %12 = load i16*, i16** %data_p, align 4, !tbaa !2
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds i16, i16* %12, i32 %13
  store i16 %11, i16* %arrayidx4, align 2, !tbaa !46
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %15 = load i32, i32* %width.addr, align 4, !tbaa !6
  store i32 %15, i32* %j, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc11, %for.end
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %17 = load i32, i32* %width.addr, align 4, !tbaa !6
  %18 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %add = add nsw i32 %17, %18
  %cmp6 = icmp slt i32 %16, %add
  br i1 %cmp6, label %for.body7, label %for.end13

for.body7:                                        ; preds = %for.cond5
  %19 = load i16*, i16** %data_p, align 4, !tbaa !2
  %20 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub8 = sub nsw i32 %20, 1
  %arrayidx9 = getelementptr inbounds i16, i16* %19, i32 %sub8
  %21 = load i16, i16* %arrayidx9, align 2, !tbaa !46
  %22 = load i16*, i16** %data_p, align 4, !tbaa !2
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds i16, i16* %22, i32 %23
  store i16 %21, i16* %arrayidx10, align 2, !tbaa !46
  br label %for.inc11

for.inc11:                                        ; preds = %for.body7
  %24 = load i32, i32* %j, align 4, !tbaa !6
  %inc12 = add nsw i32 %24, 1
  store i32 %inc12, i32* %j, align 4, !tbaa !6
  br label %for.cond5

for.end13:                                        ; preds = %for.cond5
  br label %for.inc14

for.inc14:                                        ; preds = %for.end13
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end16:                                        ; preds = %for.cond
  %26 = load i16*, i16** %data.addr, align 4, !tbaa !2
  %27 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %idx.neg = sub i32 0, %27
  %add.ptr17 = getelementptr inbounds i16, i16* %26, i32 %idx.neg
  store i16* %add.ptr17, i16** %data_p, align 4, !tbaa !2
  %28 = load i32, i32* %border_vert.addr, align 4, !tbaa !6
  %sub18 = sub nsw i32 0, %28
  store i32 %sub18, i32* %i, align 4, !tbaa !6
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc27, %for.end16
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %cmp20 = icmp slt i32 %29, 0
  br i1 %cmp20, label %for.body21, label %for.end29

for.body21:                                       ; preds = %for.cond19
  %30 = load i16*, i16** %data_p, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %32 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul22 = mul nsw i32 %31, %32
  %add.ptr23 = getelementptr inbounds i16, i16* %30, i32 %mul22
  %33 = bitcast i16* %add.ptr23 to i8*
  %34 = load i16*, i16** %data_p, align 4, !tbaa !2
  %35 = bitcast i16* %34 to i8*
  %36 = load i32, i32* %width.addr, align 4, !tbaa !6
  %37 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %mul24 = mul nsw i32 2, %37
  %add25 = add nsw i32 %36, %mul24
  %mul26 = mul i32 %add25, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %33, i8* align 2 %35, i32 %mul26, i1 false)
  br label %for.inc27

for.inc27:                                        ; preds = %for.body21
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %inc28 = add nsw i32 %38, 1
  store i32 %inc28, i32* %i, align 4, !tbaa !6
  br label %for.cond19

for.end29:                                        ; preds = %for.cond19
  %39 = load i32, i32* %height.addr, align 4, !tbaa !6
  store i32 %39, i32* %i, align 4, !tbaa !6
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc42, %for.end29
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %height.addr, align 4, !tbaa !6
  %42 = load i32, i32* %border_vert.addr, align 4, !tbaa !6
  %add31 = add nsw i32 %41, %42
  %cmp32 = icmp slt i32 %40, %add31
  br i1 %cmp32, label %for.body33, label %for.end44

for.body33:                                       ; preds = %for.cond30
  %43 = load i16*, i16** %data_p, align 4, !tbaa !2
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %45 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul34 = mul nsw i32 %44, %45
  %add.ptr35 = getelementptr inbounds i16, i16* %43, i32 %mul34
  %46 = bitcast i16* %add.ptr35 to i8*
  %47 = load i16*, i16** %data_p, align 4, !tbaa !2
  %48 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub36 = sub nsw i32 %48, 1
  %49 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul37 = mul nsw i32 %sub36, %49
  %add.ptr38 = getelementptr inbounds i16, i16* %47, i32 %mul37
  %50 = bitcast i16* %add.ptr38 to i8*
  %51 = load i32, i32* %width.addr, align 4, !tbaa !6
  %52 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %mul39 = mul nsw i32 2, %52
  %add40 = add nsw i32 %51, %mul39
  %mul41 = mul i32 %add40, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %46, i8* align 2 %50, i32 %mul41, i1 false)
  br label %for.inc42

for.inc42:                                        ; preds = %for.body33
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %inc43 = add nsw i32 %53, 1
  store i32 %inc43, i32* %i, align 4, !tbaa !6
  br label %for.cond30

for.end44:                                        ; preds = %for.cond30
  %54 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #5
  %55 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  %56 = bitcast i16** %data_p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #5
  ret void
}

; Function Attrs: nounwind
define internal void @extend_frame_lowbd(i8* %data, i32 %width, i32 %height, i32 %stride, i32 %border_horz, i32 %border_vert) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %border_horz.addr = alloca i32, align 4
  %border_vert.addr = alloca i32, align 4
  %data_p = alloca i8*, align 4
  %i = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %border_horz, i32* %border_horz.addr, align 4, !tbaa !6
  store i32 %border_vert, i32* %border_vert.addr, align 4, !tbaa !6
  %0 = bitcast i8** %data_p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %mul
  store i8* %add.ptr, i8** %data_p, align 4, !tbaa !2
  %7 = load i8*, i8** %data_p, align 4, !tbaa !2
  %8 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %idx.neg = sub i32 0, %8
  %add.ptr1 = getelementptr inbounds i8, i8* %7, i32 %idx.neg
  %9 = load i8*, i8** %data_p, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 0
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !47
  %conv = zext i8 %10 to i32
  %11 = trunc i32 %conv to i8
  %12 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr1, i8 %11, i32 %12, i1 false)
  %13 = load i8*, i8** %data_p, align 4, !tbaa !2
  %14 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add.ptr2 = getelementptr inbounds i8, i8* %13, i32 %14
  %15 = load i8*, i8** %data_p, align 4, !tbaa !2
  %16 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %16, 1
  %arrayidx3 = getelementptr inbounds i8, i8* %15, i32 %sub
  %17 = load i8, i8* %arrayidx3, align 1, !tbaa !47
  %conv4 = zext i8 %17 to i32
  %18 = trunc i32 %conv4 to i8
  %19 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr2, i8 %18, i32 %19, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %22 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %idx.neg5 = sub i32 0, %22
  %add.ptr6 = getelementptr inbounds i8, i8* %21, i32 %idx.neg5
  store i8* %add.ptr6, i8** %data_p, align 4, !tbaa !2
  %23 = load i32, i32* %border_vert.addr, align 4, !tbaa !6
  %sub7 = sub nsw i32 0, %23
  store i32 %sub7, i32* %i, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc15, %for.end
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %24, 0
  br i1 %cmp9, label %for.body11, label %for.end17

for.body11:                                       ; preds = %for.cond8
  %25 = load i8*, i8** %data_p, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %27 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul12 = mul nsw i32 %26, %27
  %add.ptr13 = getelementptr inbounds i8, i8* %25, i32 %mul12
  %28 = load i8*, i8** %data_p, align 4, !tbaa !2
  %29 = load i32, i32* %width.addr, align 4, !tbaa !6
  %30 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %mul14 = mul nsw i32 2, %30
  %add = add nsw i32 %29, %mul14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr13, i8* align 1 %28, i32 %add, i1 false)
  br label %for.inc15

for.inc15:                                        ; preds = %for.body11
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %inc16 = add nsw i32 %31, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !6
  br label %for.cond8

for.end17:                                        ; preds = %for.cond8
  %32 = load i32, i32* %height.addr, align 4, !tbaa !6
  store i32 %32, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc30, %for.end17
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %34 = load i32, i32* %height.addr, align 4, !tbaa !6
  %35 = load i32, i32* %border_vert.addr, align 4, !tbaa !6
  %add19 = add nsw i32 %34, %35
  %cmp20 = icmp slt i32 %33, %add19
  br i1 %cmp20, label %for.body22, label %for.end32

for.body22:                                       ; preds = %for.cond18
  %36 = load i8*, i8** %data_p, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %38 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 %37, %38
  %add.ptr24 = getelementptr inbounds i8, i8* %36, i32 %mul23
  %39 = load i8*, i8** %data_p, align 4, !tbaa !2
  %40 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub25 = sub nsw i32 %40, 1
  %41 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %sub25, %41
  %add.ptr27 = getelementptr inbounds i8, i8* %39, i32 %mul26
  %42 = load i32, i32* %width.addr, align 4, !tbaa !6
  %43 = load i32, i32* %border_horz.addr, align 4, !tbaa !6
  %mul28 = mul nsw i32 2, %43
  %add29 = add nsw i32 %42, %mul28
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr24, i8* align 1 %add.ptr27, i32 %add29, i1 false)
  br label %for.inc30

for.inc30:                                        ; preds = %for.body22
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %inc31 = add nsw i32 %44, 1
  store i32 %inc31, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.end32:                                        ; preds = %for.cond18
  %45 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #5
  %46 = bitcast i8** %data_p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_decode_xq(i32* %xqd, i32* %xq, %struct.sgr_params_type* %params) #0 {
entry:
  %xqd.addr = alloca i32*, align 4
  %xq.addr = alloca i32*, align 4
  %params.addr = alloca %struct.sgr_params_type*, align 4
  store i32* %xqd, i32** %xqd.addr, align 4, !tbaa !2
  store i32* %xq, i32** %xq.addr, align 4, !tbaa !2
  store %struct.sgr_params_type* %params, %struct.sgr_params_type** %params.addr, align 4, !tbaa !2
  %0 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params.addr, align 4, !tbaa !2
  %r = getelementptr inbounds %struct.sgr_params_type, %struct.sgr_params_type* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %r, i32 0, i32 0
  %1 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32*, i32** %xq.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %2, i32 0
  store i32 0, i32* %arrayidx1, align 4, !tbaa !6
  %3 = load i32*, i32** %xqd.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %3, i32 1
  %4 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  %sub = sub nsw i32 128, %4
  %5 = load i32*, i32** %xq.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %5, i32 1
  store i32 %sub, i32* %arrayidx3, align 4, !tbaa !6
  br label %if.end19

if.else:                                          ; preds = %entry
  %6 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params.addr, align 4, !tbaa !2
  %r4 = getelementptr inbounds %struct.sgr_params_type, %struct.sgr_params_type* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [2 x i32], [2 x i32]* %r4, i32 0, i32 1
  %7 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %cmp6 = icmp eq i32 %7, 0
  br i1 %cmp6, label %if.then7, label %if.else11

if.then7:                                         ; preds = %if.else
  %8 = load i32*, i32** %xqd.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %8, i32 0
  %9 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %10 = load i32*, i32** %xq.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %10, i32 0
  store i32 %9, i32* %arrayidx9, align 4, !tbaa !6
  %11 = load i32*, i32** %xq.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %11, i32 1
  store i32 0, i32* %arrayidx10, align 4, !tbaa !6
  br label %if.end

if.else11:                                        ; preds = %if.else
  %12 = load i32*, i32** %xqd.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %12, i32 0
  %13 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %14 = load i32*, i32** %xq.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %14, i32 0
  store i32 %13, i32* %arrayidx13, align 4, !tbaa !6
  %15 = load i32*, i32** %xq.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i32, i32* %15, i32 0
  %16 = load i32, i32* %arrayidx14, align 4, !tbaa !6
  %sub15 = sub nsw i32 128, %16
  %17 = load i32*, i32** %xqd.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %17, i32 1
  %18 = load i32, i32* %arrayidx16, align 4, !tbaa !6
  %sub17 = sub nsw i32 %sub15, %18
  %19 = load i32*, i32** %xq.addr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i32, i32* %19, i32 1
  store i32 %sub17, i32* %arrayidx18, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else11, %if.then7
  br label %if.end19

if.end19:                                         ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_selfguided_restoration_c(i8* %dgd8, i32 %width, i32 %height, i32 %dgd_stride, i32* %flt0, i32* %flt1, i32 %flt_stride, i32 %sgr_params_idx, i32 %bit_depth, i32 %highbd) #0 {
entry:
  %dgd8.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %dgd_stride.addr = alloca i32, align 4
  %flt0.addr = alloca i32*, align 4
  %flt1.addr = alloca i32*, align 4
  %flt_stride.addr = alloca i32, align 4
  %sgr_params_idx.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %highbd.addr = alloca i32, align 4
  %dgd32_ = alloca [8100 x i32], align 16
  %dgd32_stride = alloca i32, align 4
  %dgd32 = alloca i32*, align 4
  %dgd16 = alloca i16*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %i16 = alloca i32, align 4
  %j23 = alloca i32, align 4
  %params = alloca %struct.sgr_params_type*, align 4
  store i8* %dgd8, i8** %dgd8.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %dgd_stride, i32* %dgd_stride.addr, align 4, !tbaa !6
  store i32* %flt0, i32** %flt0.addr, align 4, !tbaa !2
  store i32* %flt1, i32** %flt1.addr, align 4, !tbaa !2
  store i32 %flt_stride, i32* %flt_stride.addr, align 4, !tbaa !6
  store i32 %sgr_params_idx, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %highbd, i32* %highbd.addr, align 4, !tbaa !6
  %0 = bitcast [8100 x i32]* %dgd32_ to i8*
  call void @llvm.lifetime.start.p0i8(i64 32400, i8* %0) #5
  %1 = bitcast i32* %dgd32_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add = add nsw i32 %2, 6
  store i32 %add, i32* %dgd32_stride, align 4, !tbaa !6
  %3 = bitcast i32** %dgd32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %arraydecay = getelementptr inbounds [8100 x i32], [8100 x i32]* %dgd32_, i32 0, i32 0
  %4 = load i32, i32* %dgd32_stride, align 4, !tbaa !6
  %mul = mul nsw i32 %4, 3
  %add.ptr = getelementptr inbounds i32, i32* %arraydecay, i32 %mul
  %add.ptr1 = getelementptr inbounds i32, i32* %add.ptr, i32 3
  store i32* %add.ptr1, i32** %dgd32, align 4, !tbaa !2
  %5 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = bitcast i16** %dgd16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i8*, i8** %dgd8.addr, align 4, !tbaa !2
  %8 = ptrtoint i8* %7 to i32
  %shl = shl i32 %8, 1
  %9 = inttoptr i32 %shl to i16*
  store i16* %9, i16** %dgd16, align 4, !tbaa !2
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  store i32 -3, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %if.then
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %height.addr, align 4, !tbaa !6
  %add2 = add nsw i32 %12, 3
  %cmp = icmp slt i32 %11, %add2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  br label %for.end15

for.body:                                         ; preds = %for.cond
  %14 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  store i32 -3, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %16 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add4 = add nsw i32 %16, 3
  %cmp5 = icmp slt i32 %15, %add4
  br i1 %cmp5, label %for.body7, label %for.cond.cleanup6

for.cond.cleanup6:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  br label %for.end

for.body7:                                        ; preds = %for.cond3
  %18 = load i16*, i16** %dgd16, align 4, !tbaa !2
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %20 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %mul8 = mul nsw i32 %19, %20
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %add9 = add nsw i32 %mul8, %21
  %arrayidx = getelementptr inbounds i16, i16* %18, i32 %add9
  %22 = load i16, i16* %arrayidx, align 2, !tbaa !46
  %conv = zext i16 %22 to i32
  %23 = load i32*, i32** %dgd32, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %25 = load i32, i32* %dgd32_stride, align 4, !tbaa !6
  %mul10 = mul nsw i32 %24, %25
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %add11 = add nsw i32 %mul10, %26
  %arrayidx12 = getelementptr inbounds i32, i32* %23, i32 %add11
  store i32 %conv, i32* %arrayidx12, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body7
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.end:                                          ; preds = %for.cond.cleanup6
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %inc14 = add nsw i32 %28, 1
  store i32 %inc14, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end15:                                        ; preds = %for.cond.cleanup
  %29 = bitcast i16** %dgd16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  br label %if.end

if.else:                                          ; preds = %entry
  %30 = bitcast i32* %i16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #5
  store i32 -3, i32* %i16, align 4, !tbaa !6
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc40, %if.else
  %31 = load i32, i32* %i16, align 4, !tbaa !6
  %32 = load i32, i32* %height.addr, align 4, !tbaa !6
  %add18 = add nsw i32 %32, 3
  %cmp19 = icmp slt i32 %31, %add18
  br i1 %cmp19, label %for.body22, label %for.cond.cleanup21

for.cond.cleanup21:                               ; preds = %for.cond17
  store i32 8, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %i16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  br label %for.end42

for.body22:                                       ; preds = %for.cond17
  %34 = bitcast i32* %j23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #5
  store i32 -3, i32* %j23, align 4, !tbaa !6
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc37, %for.body22
  %35 = load i32, i32* %j23, align 4, !tbaa !6
  %36 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add25 = add nsw i32 %36, 3
  %cmp26 = icmp slt i32 %35, %add25
  br i1 %cmp26, label %for.body29, label %for.cond.cleanup28

for.cond.cleanup28:                               ; preds = %for.cond24
  store i32 11, i32* %cleanup.dest.slot, align 4
  %37 = bitcast i32* %j23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  br label %for.end39

for.body29:                                       ; preds = %for.cond24
  %38 = load i8*, i8** %dgd8.addr, align 4, !tbaa !2
  %39 = load i32, i32* %i16, align 4, !tbaa !6
  %40 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %mul30 = mul nsw i32 %39, %40
  %41 = load i32, i32* %j23, align 4, !tbaa !6
  %add31 = add nsw i32 %mul30, %41
  %arrayidx32 = getelementptr inbounds i8, i8* %38, i32 %add31
  %42 = load i8, i8* %arrayidx32, align 1, !tbaa !47
  %conv33 = zext i8 %42 to i32
  %43 = load i32*, i32** %dgd32, align 4, !tbaa !2
  %44 = load i32, i32* %i16, align 4, !tbaa !6
  %45 = load i32, i32* %dgd32_stride, align 4, !tbaa !6
  %mul34 = mul nsw i32 %44, %45
  %46 = load i32, i32* %j23, align 4, !tbaa !6
  %add35 = add nsw i32 %mul34, %46
  %arrayidx36 = getelementptr inbounds i32, i32* %43, i32 %add35
  store i32 %conv33, i32* %arrayidx36, align 4, !tbaa !6
  br label %for.inc37

for.inc37:                                        ; preds = %for.body29
  %47 = load i32, i32* %j23, align 4, !tbaa !6
  %inc38 = add nsw i32 %47, 1
  store i32 %inc38, i32* %j23, align 4, !tbaa !6
  br label %for.cond24

for.end39:                                        ; preds = %for.cond.cleanup28
  br label %for.inc40

for.inc40:                                        ; preds = %for.end39
  %48 = load i32, i32* %i16, align 4, !tbaa !6
  %inc41 = add nsw i32 %48, 1
  store i32 %inc41, i32* %i16, align 4, !tbaa !6
  br label %for.cond17

for.end42:                                        ; preds = %for.cond.cleanup21
  br label %if.end

if.end:                                           ; preds = %for.end42, %for.end15
  %49 = bitcast %struct.sgr_params_type** %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #5
  %50 = load i32, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds [16 x %struct.sgr_params_type], [16 x %struct.sgr_params_type]* @av1_sgr_params, i32 0, i32 %50
  store %struct.sgr_params_type* %arrayidx43, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %51 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %r = getelementptr inbounds %struct.sgr_params_type, %struct.sgr_params_type* %51, i32 0, i32 0
  %arrayidx44 = getelementptr inbounds [2 x i32], [2 x i32]* %r, i32 0, i32 0
  %52 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  %cmp45 = icmp sgt i32 %52, 0
  br i1 %cmp45, label %if.then47, label %if.end48

if.then47:                                        ; preds = %if.end
  %53 = load i32*, i32** %dgd32, align 4, !tbaa !2
  %54 = load i32, i32* %width.addr, align 4, !tbaa !6
  %55 = load i32, i32* %height.addr, align 4, !tbaa !6
  %56 = load i32, i32* %dgd32_stride, align 4, !tbaa !6
  %57 = load i32*, i32** %flt0.addr, align 4, !tbaa !2
  %58 = load i32, i32* %flt_stride.addr, align 4, !tbaa !6
  %59 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %60 = load i32, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  call void @selfguided_restoration_fast_internal(i32* %53, i32 %54, i32 %55, i32 %56, i32* %57, i32 %58, i32 %59, i32 %60, i32 0)
  br label %if.end48

if.end48:                                         ; preds = %if.then47, %if.end
  %61 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %r49 = getelementptr inbounds %struct.sgr_params_type, %struct.sgr_params_type* %61, i32 0, i32 0
  %arrayidx50 = getelementptr inbounds [2 x i32], [2 x i32]* %r49, i32 0, i32 1
  %62 = load i32, i32* %arrayidx50, align 4, !tbaa !6
  %cmp51 = icmp sgt i32 %62, 0
  br i1 %cmp51, label %if.then53, label %if.end54

if.then53:                                        ; preds = %if.end48
  %63 = load i32*, i32** %dgd32, align 4, !tbaa !2
  %64 = load i32, i32* %width.addr, align 4, !tbaa !6
  %65 = load i32, i32* %height.addr, align 4, !tbaa !6
  %66 = load i32, i32* %dgd32_stride, align 4, !tbaa !6
  %67 = load i32*, i32** %flt1.addr, align 4, !tbaa !2
  %68 = load i32, i32* %flt_stride.addr, align 4, !tbaa !6
  %69 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %70 = load i32, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  call void @selfguided_restoration_internal(i32* %63, i32 %64, i32 %65, i32 %66, i32* %67, i32 %68, i32 %69, i32 %70, i32 1)
  br label %if.end54

if.end54:                                         ; preds = %if.then53, %if.end48
  store i32 1, i32* %cleanup.dest.slot, align 4
  %71 = bitcast %struct.sgr_params_type** %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #5
  %72 = bitcast i32** %dgd32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #5
  %73 = bitcast i32* %dgd32_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #5
  %74 = bitcast [8100 x i32]* %dgd32_ to i8*
  call void @llvm.lifetime.end.p0i8(i64 32400, i8* %74) #5
  ret i32 0
}

; Function Attrs: nounwind
define internal void @selfguided_restoration_fast_internal(i32* %dgd, i32 %width, i32 %height, i32 %dgd_stride, i32* %dst, i32 %dst_stride, i32 %bit_depth, i32 %sgr_params_idx, i32 %radius_idx) #0 {
entry:
  %dgd.addr = alloca i32*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %dgd_stride.addr = alloca i32, align 4
  %dst.addr = alloca i32*, align 4
  %dst_stride.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %sgr_params_idx.addr = alloca i32, align 4
  %radius_idx.addr = alloca i32, align 4
  %params = alloca %struct.sgr_params_type*, align 4
  %r = alloca i32, align 4
  %width_ext = alloca i32, align 4
  %buf_stride = alloca i32, align 4
  %A_ = alloca [8100 x i32], align 16
  %B_ = alloca [8100 x i32], align 16
  %A = alloca i32*, align 4
  %B = alloca i32*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %l = alloca i32, align 4
  %m = alloca i32, align 4
  %nb = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %v = alloca i32, align 4
  %k73 = alloca i32, align 4
  %l76 = alloca i32, align 4
  %m79 = alloca i32, align 4
  %nb82 = alloca i32, align 4
  %a83 = alloca i32, align 4
  %b93 = alloca i32, align 4
  %v103 = alloca i32, align 4
  store i32* %dgd, i32** %dgd.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %dgd_stride, i32* %dgd_stride.addr, align 4, !tbaa !6
  store i32* %dst, i32** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %sgr_params_idx, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  store i32 %radius_idx, i32* %radius_idx.addr, align 4, !tbaa !6
  %0 = bitcast %struct.sgr_params_type** %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [16 x %struct.sgr_params_type], [16 x %struct.sgr_params_type]* @av1_sgr_params, i32 0, i32 %1
  store %struct.sgr_params_type* %arrayidx, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %2 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %r1 = getelementptr inbounds %struct.sgr_params_type, %struct.sgr_params_type* %3, i32 0, i32 0
  %4 = load i32, i32* %radius_idx.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %r1, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  store i32 %5, i32* %r, align 4, !tbaa !6
  %6 = bitcast i32* %width_ext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add = add nsw i32 %7, 6
  store i32 %add, i32* %width_ext, align 4, !tbaa !6
  %8 = bitcast i32* %buf_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i32, i32* %width_ext, align 4, !tbaa !6
  %add3 = add nsw i32 %9, 3
  %and = and i32 %add3, -4
  %add4 = add nsw i32 %and, 16
  store i32 %add4, i32* %buf_stride, align 4, !tbaa !6
  %10 = bitcast [8100 x i32]* %A_ to i8*
  call void @llvm.lifetime.start.p0i8(i64 32400, i8* %10) #5
  %11 = bitcast [8100 x i32]* %B_ to i8*
  call void @llvm.lifetime.start.p0i8(i64 32400, i8* %11) #5
  %12 = bitcast i32** %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %arraydecay = getelementptr inbounds [8100 x i32], [8100 x i32]* %A_, i32 0, i32 0
  store i32* %arraydecay, i32** %A, align 4, !tbaa !2
  %13 = bitcast i32** %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %arraydecay5 = getelementptr inbounds [8100 x i32], [8100 x i32]* %B_, i32 0, i32 0
  store i32* %arraydecay5, i32** %B, align 4, !tbaa !2
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load i32*, i32** %dgd.addr, align 4, !tbaa !2
  %17 = load i32, i32* %width.addr, align 4, !tbaa !6
  %18 = load i32, i32* %height.addr, align 4, !tbaa !6
  %19 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %20 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %21 = load i32, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  %22 = load i32, i32* %radius_idx.addr, align 4, !tbaa !6
  %23 = load i32*, i32** %A, align 4, !tbaa !2
  %24 = load i32*, i32** %B, align 4, !tbaa !2
  call void @calculate_intermediate_result(i32* %16, i32 %17, i32 %18, i32 %19, i32 %20, i32 %21, i32 %22, i32 1, i32* %23, i32* %24)
  %25 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul = mul nsw i32 3, %25
  %add6 = add nsw i32 %mul, 3
  %26 = load i32*, i32** %A, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %26, i32 %add6
  store i32* %add.ptr, i32** %A, align 4, !tbaa !2
  %27 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul7 = mul nsw i32 3, %27
  %add8 = add nsw i32 %mul7, 3
  %28 = load i32*, i32** %B, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i32, i32* %28, i32 %add8
  store i32* %add.ptr9, i32** %B, align 4, !tbaa !2
  %29 = load i32, i32* %r, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc113, %entry
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %31 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %30, %31
  br i1 %cmp, label %for.body, label %for.end115

for.body:                                         ; preds = %for.cond
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %and10 = and i32 %32, 1
  %tobool = icmp ne i32 %and10, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %for.body
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc, %if.then
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %34 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp12 = icmp slt i32 %33, %34
  br i1 %cmp12, label %for.body13, label %for.end

for.body13:                                       ; preds = %for.cond11
  %35 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #5
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul14 = mul nsw i32 %36, %37
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %add15 = add nsw i32 %mul14, %38
  store i32 %add15, i32* %k, align 4, !tbaa !6
  %39 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #5
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %mul16 = mul nsw i32 %40, %41
  %42 = load i32, i32* %j, align 4, !tbaa !6
  %add17 = add nsw i32 %mul16, %42
  store i32 %add17, i32* %l, align 4, !tbaa !6
  %43 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #5
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %45 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 %44, %45
  %46 = load i32, i32* %j, align 4, !tbaa !6
  %add19 = add nsw i32 %mul18, %46
  store i32 %add19, i32* %m, align 4, !tbaa !6
  %47 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  store i32 5, i32* %nb, align 4, !tbaa !6
  %48 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #5
  %49 = load i32*, i32** %A, align 4, !tbaa !2
  %50 = load i32, i32* %k, align 4, !tbaa !6
  %51 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub = sub nsw i32 %50, %51
  %arrayidx20 = getelementptr inbounds i32, i32* %49, i32 %sub
  %52 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %53 = load i32*, i32** %A, align 4, !tbaa !2
  %54 = load i32, i32* %k, align 4, !tbaa !6
  %55 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add21 = add nsw i32 %54, %55
  %arrayidx22 = getelementptr inbounds i32, i32* %53, i32 %add21
  %56 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  %add23 = add nsw i32 %52, %56
  %mul24 = mul nsw i32 %add23, 6
  %57 = load i32*, i32** %A, align 4, !tbaa !2
  %58 = load i32, i32* %k, align 4, !tbaa !6
  %sub25 = sub nsw i32 %58, 1
  %59 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub26 = sub nsw i32 %sub25, %59
  %arrayidx27 = getelementptr inbounds i32, i32* %57, i32 %sub26
  %60 = load i32, i32* %arrayidx27, align 4, !tbaa !6
  %61 = load i32*, i32** %A, align 4, !tbaa !2
  %62 = load i32, i32* %k, align 4, !tbaa !6
  %sub28 = sub nsw i32 %62, 1
  %63 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add29 = add nsw i32 %sub28, %63
  %arrayidx30 = getelementptr inbounds i32, i32* %61, i32 %add29
  %64 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %add31 = add nsw i32 %60, %64
  %65 = load i32*, i32** %A, align 4, !tbaa !2
  %66 = load i32, i32* %k, align 4, !tbaa !6
  %add32 = add nsw i32 %66, 1
  %67 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub33 = sub nsw i32 %add32, %67
  %arrayidx34 = getelementptr inbounds i32, i32* %65, i32 %sub33
  %68 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %add35 = add nsw i32 %add31, %68
  %69 = load i32*, i32** %A, align 4, !tbaa !2
  %70 = load i32, i32* %k, align 4, !tbaa !6
  %add36 = add nsw i32 %70, 1
  %71 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add37 = add nsw i32 %add36, %71
  %arrayidx38 = getelementptr inbounds i32, i32* %69, i32 %add37
  %72 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %add39 = add nsw i32 %add35, %72
  %mul40 = mul nsw i32 %add39, 5
  %add41 = add nsw i32 %mul24, %mul40
  store i32 %add41, i32* %a, align 4, !tbaa !6
  %73 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #5
  %74 = load i32*, i32** %B, align 4, !tbaa !2
  %75 = load i32, i32* %k, align 4, !tbaa !6
  %76 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub42 = sub nsw i32 %75, %76
  %arrayidx43 = getelementptr inbounds i32, i32* %74, i32 %sub42
  %77 = load i32, i32* %arrayidx43, align 4, !tbaa !6
  %78 = load i32*, i32** %B, align 4, !tbaa !2
  %79 = load i32, i32* %k, align 4, !tbaa !6
  %80 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add44 = add nsw i32 %79, %80
  %arrayidx45 = getelementptr inbounds i32, i32* %78, i32 %add44
  %81 = load i32, i32* %arrayidx45, align 4, !tbaa !6
  %add46 = add nsw i32 %77, %81
  %mul47 = mul nsw i32 %add46, 6
  %82 = load i32*, i32** %B, align 4, !tbaa !2
  %83 = load i32, i32* %k, align 4, !tbaa !6
  %sub48 = sub nsw i32 %83, 1
  %84 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub49 = sub nsw i32 %sub48, %84
  %arrayidx50 = getelementptr inbounds i32, i32* %82, i32 %sub49
  %85 = load i32, i32* %arrayidx50, align 4, !tbaa !6
  %86 = load i32*, i32** %B, align 4, !tbaa !2
  %87 = load i32, i32* %k, align 4, !tbaa !6
  %sub51 = sub nsw i32 %87, 1
  %88 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add52 = add nsw i32 %sub51, %88
  %arrayidx53 = getelementptr inbounds i32, i32* %86, i32 %add52
  %89 = load i32, i32* %arrayidx53, align 4, !tbaa !6
  %add54 = add nsw i32 %85, %89
  %90 = load i32*, i32** %B, align 4, !tbaa !2
  %91 = load i32, i32* %k, align 4, !tbaa !6
  %add55 = add nsw i32 %91, 1
  %92 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub56 = sub nsw i32 %add55, %92
  %arrayidx57 = getelementptr inbounds i32, i32* %90, i32 %sub56
  %93 = load i32, i32* %arrayidx57, align 4, !tbaa !6
  %add58 = add nsw i32 %add54, %93
  %94 = load i32*, i32** %B, align 4, !tbaa !2
  %95 = load i32, i32* %k, align 4, !tbaa !6
  %add59 = add nsw i32 %95, 1
  %96 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add60 = add nsw i32 %add59, %96
  %arrayidx61 = getelementptr inbounds i32, i32* %94, i32 %add60
  %97 = load i32, i32* %arrayidx61, align 4, !tbaa !6
  %add62 = add nsw i32 %add58, %97
  %mul63 = mul nsw i32 %add62, 5
  %add64 = add nsw i32 %mul47, %mul63
  store i32 %add64, i32* %b, align 4, !tbaa !6
  %98 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #5
  %99 = load i32, i32* %a, align 4, !tbaa !6
  %100 = load i32*, i32** %dgd.addr, align 4, !tbaa !2
  %101 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds i32, i32* %100, i32 %101
  %102 = load i32, i32* %arrayidx65, align 4, !tbaa !6
  %mul66 = mul nsw i32 %99, %102
  %103 = load i32, i32* %b, align 4, !tbaa !6
  %add67 = add nsw i32 %mul66, %103
  store i32 %add67, i32* %v, align 4, !tbaa !6
  %104 = load i32, i32* %v, align 4, !tbaa !6
  %add68 = add nsw i32 %104, 256
  %shr = ashr i32 %add68, 9
  %105 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %106 = load i32, i32* %m, align 4, !tbaa !6
  %arrayidx69 = getelementptr inbounds i32, i32* %105, i32 %106
  store i32 %shr, i32* %arrayidx69, align 4, !tbaa !6
  %107 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #5
  %108 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  %109 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #5
  %110 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #5
  %111 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #5
  %112 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #5
  %113 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #5
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %114 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %114, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond11

for.end:                                          ; preds = %for.cond11
  br label %if.end

if.else:                                          ; preds = %for.body
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond70

for.cond70:                                       ; preds = %for.inc110, %if.else
  %115 = load i32, i32* %j, align 4, !tbaa !6
  %116 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp71 = icmp slt i32 %115, %116
  br i1 %cmp71, label %for.body72, label %for.end112

for.body72:                                       ; preds = %for.cond70
  %117 = bitcast i32* %k73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #5
  %118 = load i32, i32* %i, align 4, !tbaa !6
  %119 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul74 = mul nsw i32 %118, %119
  %120 = load i32, i32* %j, align 4, !tbaa !6
  %add75 = add nsw i32 %mul74, %120
  store i32 %add75, i32* %k73, align 4, !tbaa !6
  %121 = bitcast i32* %l76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #5
  %122 = load i32, i32* %i, align 4, !tbaa !6
  %123 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %mul77 = mul nsw i32 %122, %123
  %124 = load i32, i32* %j, align 4, !tbaa !6
  %add78 = add nsw i32 %mul77, %124
  store i32 %add78, i32* %l76, align 4, !tbaa !6
  %125 = bitcast i32* %m79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #5
  %126 = load i32, i32* %i, align 4, !tbaa !6
  %127 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul80 = mul nsw i32 %126, %127
  %128 = load i32, i32* %j, align 4, !tbaa !6
  %add81 = add nsw i32 %mul80, %128
  store i32 %add81, i32* %m79, align 4, !tbaa !6
  %129 = bitcast i32* %nb82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #5
  store i32 4, i32* %nb82, align 4, !tbaa !6
  %130 = bitcast i32* %a83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %130) #5
  %131 = load i32*, i32** %A, align 4, !tbaa !2
  %132 = load i32, i32* %k73, align 4, !tbaa !6
  %arrayidx84 = getelementptr inbounds i32, i32* %131, i32 %132
  %133 = load i32, i32* %arrayidx84, align 4, !tbaa !6
  %mul85 = mul nsw i32 %133, 6
  %134 = load i32*, i32** %A, align 4, !tbaa !2
  %135 = load i32, i32* %k73, align 4, !tbaa !6
  %sub86 = sub nsw i32 %135, 1
  %arrayidx87 = getelementptr inbounds i32, i32* %134, i32 %sub86
  %136 = load i32, i32* %arrayidx87, align 4, !tbaa !6
  %137 = load i32*, i32** %A, align 4, !tbaa !2
  %138 = load i32, i32* %k73, align 4, !tbaa !6
  %add88 = add nsw i32 %138, 1
  %arrayidx89 = getelementptr inbounds i32, i32* %137, i32 %add88
  %139 = load i32, i32* %arrayidx89, align 4, !tbaa !6
  %add90 = add nsw i32 %136, %139
  %mul91 = mul nsw i32 %add90, 5
  %add92 = add nsw i32 %mul85, %mul91
  store i32 %add92, i32* %a83, align 4, !tbaa !6
  %140 = bitcast i32* %b93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #5
  %141 = load i32*, i32** %B, align 4, !tbaa !2
  %142 = load i32, i32* %k73, align 4, !tbaa !6
  %arrayidx94 = getelementptr inbounds i32, i32* %141, i32 %142
  %143 = load i32, i32* %arrayidx94, align 4, !tbaa !6
  %mul95 = mul nsw i32 %143, 6
  %144 = load i32*, i32** %B, align 4, !tbaa !2
  %145 = load i32, i32* %k73, align 4, !tbaa !6
  %sub96 = sub nsw i32 %145, 1
  %arrayidx97 = getelementptr inbounds i32, i32* %144, i32 %sub96
  %146 = load i32, i32* %arrayidx97, align 4, !tbaa !6
  %147 = load i32*, i32** %B, align 4, !tbaa !2
  %148 = load i32, i32* %k73, align 4, !tbaa !6
  %add98 = add nsw i32 %148, 1
  %arrayidx99 = getelementptr inbounds i32, i32* %147, i32 %add98
  %149 = load i32, i32* %arrayidx99, align 4, !tbaa !6
  %add100 = add nsw i32 %146, %149
  %mul101 = mul nsw i32 %add100, 5
  %add102 = add nsw i32 %mul95, %mul101
  store i32 %add102, i32* %b93, align 4, !tbaa !6
  %150 = bitcast i32* %v103 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #5
  %151 = load i32, i32* %a83, align 4, !tbaa !6
  %152 = load i32*, i32** %dgd.addr, align 4, !tbaa !2
  %153 = load i32, i32* %l76, align 4, !tbaa !6
  %arrayidx104 = getelementptr inbounds i32, i32* %152, i32 %153
  %154 = load i32, i32* %arrayidx104, align 4, !tbaa !6
  %mul105 = mul nsw i32 %151, %154
  %155 = load i32, i32* %b93, align 4, !tbaa !6
  %add106 = add nsw i32 %mul105, %155
  store i32 %add106, i32* %v103, align 4, !tbaa !6
  %156 = load i32, i32* %v103, align 4, !tbaa !6
  %add107 = add nsw i32 %156, 128
  %shr108 = ashr i32 %add107, 8
  %157 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %158 = load i32, i32* %m79, align 4, !tbaa !6
  %arrayidx109 = getelementptr inbounds i32, i32* %157, i32 %158
  store i32 %shr108, i32* %arrayidx109, align 4, !tbaa !6
  %159 = bitcast i32* %v103 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #5
  %160 = bitcast i32* %b93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #5
  %161 = bitcast i32* %a83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #5
  %162 = bitcast i32* %nb82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #5
  %163 = bitcast i32* %m79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #5
  %164 = bitcast i32* %l76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #5
  %165 = bitcast i32* %k73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #5
  br label %for.inc110

for.inc110:                                       ; preds = %for.body72
  %166 = load i32, i32* %j, align 4, !tbaa !6
  %inc111 = add nsw i32 %166, 1
  store i32 %inc111, i32* %j, align 4, !tbaa !6
  br label %for.cond70

for.end112:                                       ; preds = %for.cond70
  br label %if.end

if.end:                                           ; preds = %for.end112, %for.end
  br label %for.inc113

for.inc113:                                       ; preds = %if.end
  %167 = load i32, i32* %i, align 4, !tbaa !6
  %inc114 = add nsw i32 %167, 1
  store i32 %inc114, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end115:                                       ; preds = %for.cond
  %168 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #5
  %169 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #5
  %170 = bitcast i32** %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #5
  %171 = bitcast i32** %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #5
  %172 = bitcast [8100 x i32]* %B_ to i8*
  call void @llvm.lifetime.end.p0i8(i64 32400, i8* %172) #5
  %173 = bitcast [8100 x i32]* %A_ to i8*
  call void @llvm.lifetime.end.p0i8(i64 32400, i8* %173) #5
  %174 = bitcast i32* %buf_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #5
  %175 = bitcast i32* %width_ext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #5
  %176 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #5
  %177 = bitcast %struct.sgr_params_type** %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #5
  ret void
}

; Function Attrs: nounwind
define internal void @selfguided_restoration_internal(i32* %dgd, i32 %width, i32 %height, i32 %dgd_stride, i32* %dst, i32 %dst_stride, i32 %bit_depth, i32 %sgr_params_idx, i32 %radius_idx) #0 {
entry:
  %dgd.addr = alloca i32*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %dgd_stride.addr = alloca i32, align 4
  %dst.addr = alloca i32*, align 4
  %dst_stride.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %sgr_params_idx.addr = alloca i32, align 4
  %radius_idx.addr = alloca i32, align 4
  %width_ext = alloca i32, align 4
  %buf_stride = alloca i32, align 4
  %A_ = alloca [8100 x i32], align 16
  %B_ = alloca [8100 x i32], align 16
  %A = alloca i32*, align 4
  %B = alloca i32*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %l = alloca i32, align 4
  %m = alloca i32, align 4
  %nb = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %v = alloca i32, align 4
  store i32* %dgd, i32** %dgd.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %dgd_stride, i32* %dgd_stride.addr, align 4, !tbaa !6
  store i32* %dst, i32** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %sgr_params_idx, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  store i32 %radius_idx, i32* %radius_idx.addr, align 4, !tbaa !6
  %0 = bitcast i32* %width_ext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add = add nsw i32 %1, 6
  store i32 %add, i32* %width_ext, align 4, !tbaa !6
  %2 = bitcast i32* %buf_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %width_ext, align 4, !tbaa !6
  %add1 = add nsw i32 %3, 3
  %and = and i32 %add1, -4
  %add2 = add nsw i32 %and, 16
  store i32 %add2, i32* %buf_stride, align 4, !tbaa !6
  %4 = bitcast [8100 x i32]* %A_ to i8*
  call void @llvm.lifetime.start.p0i8(i64 32400, i8* %4) #5
  %5 = bitcast [8100 x i32]* %B_ to i8*
  call void @llvm.lifetime.start.p0i8(i64 32400, i8* %5) #5
  %6 = bitcast i32** %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %arraydecay = getelementptr inbounds [8100 x i32], [8100 x i32]* %A_, i32 0, i32 0
  store i32* %arraydecay, i32** %A, align 4, !tbaa !2
  %7 = bitcast i32** %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %arraydecay3 = getelementptr inbounds [8100 x i32], [8100 x i32]* %B_, i32 0, i32 0
  store i32* %arraydecay3, i32** %B, align 4, !tbaa !2
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load i32*, i32** %dgd.addr, align 4, !tbaa !2
  %11 = load i32, i32* %width.addr, align 4, !tbaa !6
  %12 = load i32, i32* %height.addr, align 4, !tbaa !6
  %13 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %14 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %15 = load i32, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  %16 = load i32, i32* %radius_idx.addr, align 4, !tbaa !6
  %17 = load i32*, i32** %A, align 4, !tbaa !2
  %18 = load i32*, i32** %B, align 4, !tbaa !2
  call void @calculate_intermediate_result(i32* %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15, i32 %16, i32 0, i32* %17, i32* %18)
  %19 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul = mul nsw i32 3, %19
  %add4 = add nsw i32 %mul, 3
  %20 = load i32*, i32** %A, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %20, i32 %add4
  store i32* %add.ptr, i32** %A, align 4, !tbaa !2
  %21 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %21
  %add6 = add nsw i32 %mul5, 3
  %22 = load i32*, i32** %B, align 4, !tbaa !2
  %add.ptr7 = getelementptr inbounds i32, i32* %22, i32 %add6
  store i32* %add.ptr7, i32** %B, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc82, %entry
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %24 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %23, %24
  br i1 %cmp, label %for.body, label %for.end84

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %26 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %25, %26
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %27 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %29 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul11 = mul nsw i32 %28, %29
  %30 = load i32, i32* %j, align 4, !tbaa !6
  %add12 = add nsw i32 %mul11, %30
  store i32 %add12, i32* %k, align 4, !tbaa !6
  %31 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #5
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %33 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 %32, %33
  %34 = load i32, i32* %j, align 4, !tbaa !6
  %add14 = add nsw i32 %mul13, %34
  store i32 %add14, i32* %l, align 4, !tbaa !6
  %35 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #5
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 %36, %37
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %add16 = add nsw i32 %mul15, %38
  store i32 %add16, i32* %m, align 4, !tbaa !6
  %39 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #5
  store i32 5, i32* %nb, align 4, !tbaa !6
  %40 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  %41 = load i32*, i32** %A, align 4, !tbaa !2
  %42 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %41, i32 %42
  %43 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %44 = load i32*, i32** %A, align 4, !tbaa !2
  %45 = load i32, i32* %k, align 4, !tbaa !6
  %sub = sub nsw i32 %45, 1
  %arrayidx17 = getelementptr inbounds i32, i32* %44, i32 %sub
  %46 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %add18 = add nsw i32 %43, %46
  %47 = load i32*, i32** %A, align 4, !tbaa !2
  %48 = load i32, i32* %k, align 4, !tbaa !6
  %add19 = add nsw i32 %48, 1
  %arrayidx20 = getelementptr inbounds i32, i32* %47, i32 %add19
  %49 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %add21 = add nsw i32 %add18, %49
  %50 = load i32*, i32** %A, align 4, !tbaa !2
  %51 = load i32, i32* %k, align 4, !tbaa !6
  %52 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub22 = sub nsw i32 %51, %52
  %arrayidx23 = getelementptr inbounds i32, i32* %50, i32 %sub22
  %53 = load i32, i32* %arrayidx23, align 4, !tbaa !6
  %add24 = add nsw i32 %add21, %53
  %54 = load i32*, i32** %A, align 4, !tbaa !2
  %55 = load i32, i32* %k, align 4, !tbaa !6
  %56 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add25 = add nsw i32 %55, %56
  %arrayidx26 = getelementptr inbounds i32, i32* %54, i32 %add25
  %57 = load i32, i32* %arrayidx26, align 4, !tbaa !6
  %add27 = add nsw i32 %add24, %57
  %mul28 = mul nsw i32 %add27, 4
  %58 = load i32*, i32** %A, align 4, !tbaa !2
  %59 = load i32, i32* %k, align 4, !tbaa !6
  %sub29 = sub nsw i32 %59, 1
  %60 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub30 = sub nsw i32 %sub29, %60
  %arrayidx31 = getelementptr inbounds i32, i32* %58, i32 %sub30
  %61 = load i32, i32* %arrayidx31, align 4, !tbaa !6
  %62 = load i32*, i32** %A, align 4, !tbaa !2
  %63 = load i32, i32* %k, align 4, !tbaa !6
  %sub32 = sub nsw i32 %63, 1
  %64 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add33 = add nsw i32 %sub32, %64
  %arrayidx34 = getelementptr inbounds i32, i32* %62, i32 %add33
  %65 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %add35 = add nsw i32 %61, %65
  %66 = load i32*, i32** %A, align 4, !tbaa !2
  %67 = load i32, i32* %k, align 4, !tbaa !6
  %add36 = add nsw i32 %67, 1
  %68 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub37 = sub nsw i32 %add36, %68
  %arrayidx38 = getelementptr inbounds i32, i32* %66, i32 %sub37
  %69 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %add39 = add nsw i32 %add35, %69
  %70 = load i32*, i32** %A, align 4, !tbaa !2
  %71 = load i32, i32* %k, align 4, !tbaa !6
  %add40 = add nsw i32 %71, 1
  %72 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add41 = add nsw i32 %add40, %72
  %arrayidx42 = getelementptr inbounds i32, i32* %70, i32 %add41
  %73 = load i32, i32* %arrayidx42, align 4, !tbaa !6
  %add43 = add nsw i32 %add39, %73
  %mul44 = mul nsw i32 %add43, 3
  %add45 = add nsw i32 %mul28, %mul44
  store i32 %add45, i32* %a, align 4, !tbaa !6
  %74 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #5
  %75 = load i32*, i32** %B, align 4, !tbaa !2
  %76 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds i32, i32* %75, i32 %76
  %77 = load i32, i32* %arrayidx46, align 4, !tbaa !6
  %78 = load i32*, i32** %B, align 4, !tbaa !2
  %79 = load i32, i32* %k, align 4, !tbaa !6
  %sub47 = sub nsw i32 %79, 1
  %arrayidx48 = getelementptr inbounds i32, i32* %78, i32 %sub47
  %80 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %add49 = add nsw i32 %77, %80
  %81 = load i32*, i32** %B, align 4, !tbaa !2
  %82 = load i32, i32* %k, align 4, !tbaa !6
  %add50 = add nsw i32 %82, 1
  %arrayidx51 = getelementptr inbounds i32, i32* %81, i32 %add50
  %83 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  %add52 = add nsw i32 %add49, %83
  %84 = load i32*, i32** %B, align 4, !tbaa !2
  %85 = load i32, i32* %k, align 4, !tbaa !6
  %86 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub53 = sub nsw i32 %85, %86
  %arrayidx54 = getelementptr inbounds i32, i32* %84, i32 %sub53
  %87 = load i32, i32* %arrayidx54, align 4, !tbaa !6
  %add55 = add nsw i32 %add52, %87
  %88 = load i32*, i32** %B, align 4, !tbaa !2
  %89 = load i32, i32* %k, align 4, !tbaa !6
  %90 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add56 = add nsw i32 %89, %90
  %arrayidx57 = getelementptr inbounds i32, i32* %88, i32 %add56
  %91 = load i32, i32* %arrayidx57, align 4, !tbaa !6
  %add58 = add nsw i32 %add55, %91
  %mul59 = mul nsw i32 %add58, 4
  %92 = load i32*, i32** %B, align 4, !tbaa !2
  %93 = load i32, i32* %k, align 4, !tbaa !6
  %sub60 = sub nsw i32 %93, 1
  %94 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub61 = sub nsw i32 %sub60, %94
  %arrayidx62 = getelementptr inbounds i32, i32* %92, i32 %sub61
  %95 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  %96 = load i32*, i32** %B, align 4, !tbaa !2
  %97 = load i32, i32* %k, align 4, !tbaa !6
  %sub63 = sub nsw i32 %97, 1
  %98 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add64 = add nsw i32 %sub63, %98
  %arrayidx65 = getelementptr inbounds i32, i32* %96, i32 %add64
  %99 = load i32, i32* %arrayidx65, align 4, !tbaa !6
  %add66 = add nsw i32 %95, %99
  %100 = load i32*, i32** %B, align 4, !tbaa !2
  %101 = load i32, i32* %k, align 4, !tbaa !6
  %add67 = add nsw i32 %101, 1
  %102 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %sub68 = sub nsw i32 %add67, %102
  %arrayidx69 = getelementptr inbounds i32, i32* %100, i32 %sub68
  %103 = load i32, i32* %arrayidx69, align 4, !tbaa !6
  %add70 = add nsw i32 %add66, %103
  %104 = load i32*, i32** %B, align 4, !tbaa !2
  %105 = load i32, i32* %k, align 4, !tbaa !6
  %add71 = add nsw i32 %105, 1
  %106 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %add72 = add nsw i32 %add71, %106
  %arrayidx73 = getelementptr inbounds i32, i32* %104, i32 %add72
  %107 = load i32, i32* %arrayidx73, align 4, !tbaa !6
  %add74 = add nsw i32 %add70, %107
  %mul75 = mul nsw i32 %add74, 3
  %add76 = add nsw i32 %mul59, %mul75
  store i32 %add76, i32* %b, align 4, !tbaa !6
  %108 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #5
  %109 = load i32, i32* %a, align 4, !tbaa !6
  %110 = load i32*, i32** %dgd.addr, align 4, !tbaa !2
  %111 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx77 = getelementptr inbounds i32, i32* %110, i32 %111
  %112 = load i32, i32* %arrayidx77, align 4, !tbaa !6
  %mul78 = mul nsw i32 %109, %112
  %113 = load i32, i32* %b, align 4, !tbaa !6
  %add79 = add nsw i32 %mul78, %113
  store i32 %add79, i32* %v, align 4, !tbaa !6
  %114 = load i32, i32* %v, align 4, !tbaa !6
  %add80 = add nsw i32 %114, 256
  %shr = ashr i32 %add80, 9
  %115 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %116 = load i32, i32* %m, align 4, !tbaa !6
  %arrayidx81 = getelementptr inbounds i32, i32* %115, i32 %116
  store i32 %shr, i32* %arrayidx81, align 4, !tbaa !6
  %117 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  %121 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  %122 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  %123 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %124 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %124, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  br label %for.inc82

for.inc82:                                        ; preds = %for.end
  %125 = load i32, i32* %i, align 4, !tbaa !6
  %inc83 = add nsw i32 %125, 1
  store i32 %inc83, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end84:                                        ; preds = %for.cond
  %126 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #5
  %127 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #5
  %128 = bitcast i32** %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #5
  %129 = bitcast i32** %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #5
  %130 = bitcast [8100 x i32]* %B_ to i8*
  call void @llvm.lifetime.end.p0i8(i64 32400, i8* %130) #5
  %131 = bitcast [8100 x i32]* %A_ to i8*
  call void @llvm.lifetime.end.p0i8(i64 32400, i8* %131) #5
  %132 = bitcast i32* %buf_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #5
  %133 = bitcast i32* %width_ext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_apply_selfguided_restoration_c(i8* %dat8, i32 %width, i32 %height, i32 %stride, i32 %eps, i32* %xqd, i8* %dst8, i32 %dst_stride, i32* %tmpbuf, i32 %bit_depth, i32 %highbd) #0 {
entry:
  %dat8.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %eps.addr = alloca i32, align 4
  %xqd.addr = alloca i32*, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %bit_depth.addr = alloca i32, align 4
  %highbd.addr = alloca i32, align 4
  %flt0 = alloca i32*, align 4
  %flt1 = alloca i32*, align 4
  %ret = alloca i32, align 4
  %params = alloca %struct.sgr_params_type*, align 4
  %xq = alloca [2 x i32], align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %dst8ij = alloca i8*, align 4
  %dat8ij = alloca i8*, align 4
  %pre_u = alloca i16, align 2
  %u = alloca i32, align 4
  %v = alloca i32, align 4
  %w = alloca i16, align 2
  %out = alloca i16, align 2
  store i8* %dat8, i8** %dat8.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %eps, i32* %eps.addr, align 4, !tbaa !6
  store i32* %xqd, i32** %xqd.addr, align 4, !tbaa !2
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %highbd, i32* %highbd.addr, align 4, !tbaa !6
  %0 = bitcast i32** %flt0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  store i32* %1, i32** %flt0, align 4, !tbaa !2
  %2 = bitcast i32** %flt1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32*, i32** %flt0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %3, i32 161588
  store i32* %add.ptr, i32** %flt1, align 4, !tbaa !2
  %4 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i8*, i8** %dat8.addr, align 4, !tbaa !2
  %6 = load i32, i32* %width.addr, align 4, !tbaa !6
  %7 = load i32, i32* %height.addr, align 4, !tbaa !6
  %8 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %9 = load i32*, i32** %flt0, align 4, !tbaa !2
  %10 = load i32*, i32** %flt1, align 4, !tbaa !2
  %11 = load i32, i32* %width.addr, align 4, !tbaa !6
  %12 = load i32, i32* %eps.addr, align 4, !tbaa !6
  %13 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %14 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %call = call i32 @av1_selfguided_restoration_c(i8* %5, i32 %6, i32 %7, i32 %8, i32* %9, i32* %10, i32 %11, i32 %12, i32 %13, i32 %14)
  store i32 %call, i32* %ret, align 4, !tbaa !6
  %15 = load i32, i32* %ret, align 4, !tbaa !6
  %16 = bitcast %struct.sgr_params_type** %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load i32, i32* %eps.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [16 x %struct.sgr_params_type], [16 x %struct.sgr_params_type]* @av1_sgr_params, i32 0, i32 %17
  store %struct.sgr_params_type* %arrayidx, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %18 = bitcast [2 x i32]* %xq to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %18) #5
  %19 = load i32*, i32** %xqd.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [2 x i32], [2 x i32]* %xq, i32 0, i32 0
  %20 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params, align 4, !tbaa !2
  call void @av1_decode_xq(i32* %19, i32* %arraydecay, %struct.sgr_params_type* %20)
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc43, %entry
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %23 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %22, %23
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  br label %for.end45

for.body:                                         ; preds = %for.cond
  %25 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %27 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %26, %27
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %28 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %29 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #5
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %31 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %30, %31
  %32 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul, %32
  store i32 %add, i32* %k, align 4, !tbaa !6
  %33 = bitcast i8** %dst8ij to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #5
  %34 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %36 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 %35, %36
  %add.ptr6 = getelementptr inbounds i8, i8* %34, i32 %mul5
  %37 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr7 = getelementptr inbounds i8, i8* %add.ptr6, i32 %37
  store i8* %add.ptr7, i8** %dst8ij, align 4, !tbaa !2
  %38 = bitcast i8** %dat8ij to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #5
  %39 = load i8*, i8** %dat8.addr, align 4, !tbaa !2
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul8 = mul nsw i32 %40, %41
  %add.ptr9 = getelementptr inbounds i8, i8* %39, i32 %mul8
  %42 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr10 = getelementptr inbounds i8, i8* %add.ptr9, i32 %42
  store i8* %add.ptr10, i8** %dat8ij, align 4, !tbaa !2
  %43 = bitcast i16* %pre_u to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %43) #5
  %44 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %44, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body4
  %45 = load i8*, i8** %dat8ij, align 4, !tbaa !2
  %46 = ptrtoint i8* %45 to i32
  %shl = shl i32 %46, 1
  %47 = inttoptr i32 %shl to i16*
  %48 = load i16, i16* %47, align 2, !tbaa !46
  %conv = zext i16 %48 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body4
  %49 = load i8*, i8** %dat8ij, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !47
  %conv11 = zext i8 %50 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %conv11, %cond.false ]
  %conv12 = trunc i32 %cond to i16
  store i16 %conv12, i16* %pre_u, align 2, !tbaa !46
  %51 = bitcast i32* %u to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #5
  %52 = load i16, i16* %pre_u, align 2, !tbaa !46
  %conv13 = zext i16 %52 to i32
  %shl14 = shl i32 %conv13, 4
  store i32 %shl14, i32* %u, align 4, !tbaa !6
  %53 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #5
  %54 = load i32, i32* %u, align 4, !tbaa !6
  %shl15 = shl i32 %54, 7
  store i32 %shl15, i32* %v, align 4, !tbaa !6
  %55 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %r = getelementptr inbounds %struct.sgr_params_type, %struct.sgr_params_type* %55, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [2 x i32], [2 x i32]* %r, i32 0, i32 0
  %56 = load i32, i32* %arrayidx16, align 4, !tbaa !6
  %cmp17 = icmp sgt i32 %56, 0
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %arrayidx19 = getelementptr inbounds [2 x i32], [2 x i32]* %xq, i32 0, i32 0
  %57 = load i32, i32* %arrayidx19, align 4, !tbaa !6
  %58 = load i32*, i32** %flt0, align 4, !tbaa !2
  %59 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds i32, i32* %58, i32 %59
  %60 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %61 = load i32, i32* %u, align 4, !tbaa !6
  %sub = sub nsw i32 %60, %61
  %mul21 = mul nsw i32 %57, %sub
  %62 = load i32, i32* %v, align 4, !tbaa !6
  %add22 = add nsw i32 %62, %mul21
  store i32 %add22, i32* %v, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %63 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %r23 = getelementptr inbounds %struct.sgr_params_type, %struct.sgr_params_type* %63, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [2 x i32], [2 x i32]* %r23, i32 0, i32 1
  %64 = load i32, i32* %arrayidx24, align 4, !tbaa !6
  %cmp25 = icmp sgt i32 %64, 0
  br i1 %cmp25, label %if.then27, label %if.end33

if.then27:                                        ; preds = %if.end
  %arrayidx28 = getelementptr inbounds [2 x i32], [2 x i32]* %xq, i32 0, i32 1
  %65 = load i32, i32* %arrayidx28, align 4, !tbaa !6
  %66 = load i32*, i32** %flt1, align 4, !tbaa !2
  %67 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx29, align 4, !tbaa !6
  %69 = load i32, i32* %u, align 4, !tbaa !6
  %sub30 = sub nsw i32 %68, %69
  %mul31 = mul nsw i32 %65, %sub30
  %70 = load i32, i32* %v, align 4, !tbaa !6
  %add32 = add nsw i32 %70, %mul31
  store i32 %add32, i32* %v, align 4, !tbaa !6
  br label %if.end33

if.end33:                                         ; preds = %if.then27, %if.end
  %71 = bitcast i16* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %71) #5
  %72 = load i32, i32* %v, align 4, !tbaa !6
  %add34 = add nsw i32 %72, 1024
  %shr = ashr i32 %add34, 11
  %conv35 = trunc i32 %shr to i16
  store i16 %conv35, i16* %w, align 2, !tbaa !46
  %73 = bitcast i16* %out to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %73) #5
  %74 = load i16, i16* %w, align 2, !tbaa !46
  %conv36 = sext i16 %74 to i32
  %75 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %call37 = call zeroext i16 @clip_pixel_highbd(i32 %conv36, i32 %75)
  store i16 %call37, i16* %out, align 2, !tbaa !46
  %76 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %tobool38 = icmp ne i32 %76, 0
  br i1 %tobool38, label %if.then39, label %if.else

if.then39:                                        ; preds = %if.end33
  %77 = load i16, i16* %out, align 2, !tbaa !46
  %78 = load i8*, i8** %dst8ij, align 4, !tbaa !2
  %79 = ptrtoint i8* %78 to i32
  %shl40 = shl i32 %79, 1
  %80 = inttoptr i32 %shl40 to i16*
  store i16 %77, i16* %80, align 2, !tbaa !46
  br label %if.end42

if.else:                                          ; preds = %if.end33
  %81 = load i16, i16* %out, align 2, !tbaa !46
  %conv41 = trunc i16 %81 to i8
  %82 = load i8*, i8** %dst8ij, align 4, !tbaa !2
  store i8 %conv41, i8* %82, align 1, !tbaa !47
  br label %if.end42

if.end42:                                         ; preds = %if.else, %if.then39
  %83 = bitcast i16* %out to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %83) #5
  %84 = bitcast i16* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %84) #5
  %85 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #5
  %86 = bitcast i32* %u to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #5
  %87 = bitcast i16* %pre_u to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %87) #5
  %88 = bitcast i8** %dat8ij to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #5
  %89 = bitcast i8** %dst8ij to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #5
  %90 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end42
  %91 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %91, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc43

for.inc43:                                        ; preds = %for.end
  %92 = load i32, i32* %i, align 4, !tbaa !6
  %inc44 = add nsw i32 %92, 1
  store i32 %inc44, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end45:                                        ; preds = %for.cond.cleanup
  %93 = bitcast [2 x i32]* %xq to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %93) #5
  %94 = bitcast %struct.sgr_params_type** %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  %95 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #5
  %96 = bitcast i32** %flt1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #5
  %97 = bitcast i32** %flt0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #5
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @clip_pixel_highbd(i32 %val, i32 %bd) #3 {
entry:
  %retval = alloca i16, align 2
  %val.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 8, label %sw.bb
    i32 10, label %sw.bb1
    i32 12, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb
  %1 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call = call i32 @clamp(i32 %1, i32 0, i32 255)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %retval, align 2
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call2 = call i32 @clamp(i32 %2, i32 0, i32 1023)
  %conv3 = trunc i32 %call2 to i16
  store i16 %conv3, i16* %retval, align 2
  br label %return

sw.bb4:                                           ; preds = %entry
  %3 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call5 = call i32 @clamp(i32 %3, i32 0, i32 4095)
  %conv6 = trunc i32 %call5 to i16
  store i16 %conv6, i16* %retval, align 2
  br label %return

return:                                           ; preds = %sw.bb4, %sw.bb1, %sw.default
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: nounwind
define hidden void @av1_loop_restoration_filter_unit(%struct.RestorationTileLimits* %limits, %struct.RestorationUnitInfo* %rui, %struct.RestorationStripeBoundaries* %rsb, %struct.RestorationLineBuffers* %rlbs, %struct.AV1PixelRect* %tile_rect, i32 %tile_stripe0, i32 %ss_x, i32 %ss_y, i32 %highbd, i32 %bit_depth, i8* %data8, i32 %stride, i8* %dst8, i32 %dst_stride, i32* %tmpbuf, i32 %optimized_lr) #0 {
entry:
  %limits.addr = alloca %struct.RestorationTileLimits*, align 4
  %rui.addr = alloca %struct.RestorationUnitInfo*, align 4
  %rsb.addr = alloca %struct.RestorationStripeBoundaries*, align 4
  %rlbs.addr = alloca %struct.RestorationLineBuffers*, align 4
  %tile_rect.addr = alloca %struct.AV1PixelRect*, align 4
  %tile_stripe0.addr = alloca i32, align 4
  %ss_x.addr = alloca i32, align 4
  %ss_y.addr = alloca i32, align 4
  %highbd.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %data8.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %optimized_lr.addr = alloca i32, align 4
  %unit_rtype = alloca i8, align 1
  %unit_h = alloca i32, align 4
  %unit_w = alloca i32, align 4
  %data8_tl = alloca i8*, align 4
  %dst8_tl = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %filter_idx = alloca i32, align 4
  %stripe_filter = alloca void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)*, align 4
  %procunit_width = alloca i32, align 4
  %remaining_stripes = alloca %struct.RestorationTileLimits, align 4
  %i = alloca i32, align 4
  %copy_above = alloca i32, align 4
  %copy_below = alloca i32, align 4
  %full_stripe_height = alloca i32, align 4
  %runit_offset = alloca i32, align 4
  %tile_stripe = alloca i32, align 4
  %frame_stripe = alloca i32, align 4
  %rsb_row = alloca i32, align 4
  %nominal_stripe_height = alloca i32, align 4
  %h = alloca i32, align 4
  store %struct.RestorationTileLimits* %limits, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  store %struct.RestorationUnitInfo* %rui, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  store %struct.RestorationStripeBoundaries* %rsb, %struct.RestorationStripeBoundaries** %rsb.addr, align 4, !tbaa !2
  store %struct.RestorationLineBuffers* %rlbs, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  store %struct.AV1PixelRect* %tile_rect, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  store i32 %tile_stripe0, i32* %tile_stripe0.addr, align 4, !tbaa !6
  store i32 %ss_x, i32* %ss_x.addr, align 4, !tbaa !6
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !6
  store i32 %highbd, i32* %highbd.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i8* %data8, i8** %data8.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store i32 %optimized_lr, i32* %optimized_lr.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %unit_rtype) #5
  %0 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %restoration_type = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %0, i32 0, i32 0
  %1 = load i8, i8* %restoration_type, align 16, !tbaa !48
  store i8 %1, i8* %unit_rtype, align 1, !tbaa !47
  %2 = bitcast i32* %unit_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_end = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %3, i32 0, i32 3
  %4 = load i32, i32* %v_end, align 4, !tbaa !35
  %5 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %5, i32 0, i32 2
  %6 = load i32, i32* %v_start, align 4, !tbaa !38
  %sub = sub nsw i32 %4, %6
  store i32 %sub, i32* %unit_h, align 4, !tbaa !6
  %7 = bitcast i32* %unit_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_end = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %8, i32 0, i32 1
  %9 = load i32, i32* %h_end, align 4, !tbaa !33
  %10 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %10, i32 0, i32 0
  %11 = load i32, i32* %h_start, align 4, !tbaa !36
  %sub1 = sub nsw i32 %9, %11
  store i32 %sub1, i32* %unit_w, align 4, !tbaa !6
  %12 = bitcast i8** %data8_tl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %14 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start2 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %14, i32 0, i32 2
  %15 = load i32, i32* %v_start2, align 4, !tbaa !38
  %16 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %15, %16
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %mul
  %17 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_start3 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %17, i32 0, i32 0
  %18 = load i32, i32* %h_start3, align 4, !tbaa !36
  %add.ptr4 = getelementptr inbounds i8, i8* %add.ptr, i32 %18
  store i8* %add.ptr4, i8** %data8_tl, align 4, !tbaa !2
  %19 = bitcast i8** %dst8_tl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %21 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start5 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %21, i32 0, i32 2
  %22 = load i32, i32* %v_start5, align 4, !tbaa !38
  %23 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 %22, %23
  %add.ptr7 = getelementptr inbounds i8, i8* %20, i32 %mul6
  %24 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_start8 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %24, i32 0, i32 0
  %25 = load i32, i32* %h_start8, align 4, !tbaa !36
  %add.ptr9 = getelementptr inbounds i8, i8* %add.ptr7, i32 %25
  store i8* %add.ptr9, i8** %dst8_tl, align 4, !tbaa !2
  %26 = load i8, i8* %unit_rtype, align 1, !tbaa !47
  %conv = zext i8 %26 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %27 = load i32, i32* %unit_w, align 4, !tbaa !6
  %28 = load i32, i32* %unit_h, align 4, !tbaa !6
  %29 = load i8*, i8** %data8_tl, align 4, !tbaa !2
  %30 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %31 = load i8*, i8** %dst8_tl, align 4, !tbaa !2
  %32 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %33 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  call void @copy_tile(i32 %27, i32 %28, i8* %29, i32 %30, i8* %31, i32 %32, i32 %33)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %34 = bitcast i32* %filter_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #5
  %35 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 2, %35
  %36 = load i8, i8* %unit_rtype, align 1, !tbaa !47
  %conv12 = zext i8 %36 to i32
  %cmp13 = icmp eq i32 %conv12, 2
  %conv14 = zext i1 %cmp13 to i32
  %add = add nsw i32 %mul11, %conv14
  store i32 %add, i32* %filter_idx, align 4, !tbaa !6
  %37 = bitcast void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)** %stripe_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #5
  %38 = load i32, i32* %filter_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)*], [4 x void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)*]* @stripe_filters, i32 0, i32 %38
  %39 = load void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)*, void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)** %arrayidx, align 4, !tbaa !2
  store void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)* %39, void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)** %stripe_filter, align 4, !tbaa !2
  %40 = bitcast i32* %procunit_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  %41 = load i32, i32* %ss_x.addr, align 4, !tbaa !6
  %shr = ashr i32 64, %41
  store i32 %shr, i32* %procunit_width, align 4, !tbaa !6
  %42 = bitcast %struct.RestorationTileLimits* %remaining_stripes to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #5
  %43 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %44 = bitcast %struct.RestorationTileLimits* %remaining_stripes to i8*
  %45 = bitcast %struct.RestorationTileLimits* %43 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %44, i8* align 4 %45, i32 16, i1 false), !tbaa.struct !52
  %46 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %cond.end40, %if.end
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %48 = load i32, i32* %unit_h, align 4, !tbaa !6
  %cmp15 = icmp slt i32 %47, %48
  br i1 %cmp15, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %49 = bitcast i32* %copy_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #5
  %50 = bitcast i32* %copy_below to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #5
  %51 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start17 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %51, i32 0, i32 2
  %52 = load i32, i32* %v_start17, align 4, !tbaa !38
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %add18 = add nsw i32 %52, %53
  %v_start19 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %remaining_stripes, i32 0, i32 2
  store i32 %add18, i32* %v_start19, align 4, !tbaa !38
  %54 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %55 = load i32, i32* %ss_y.addr, align 4, !tbaa !6
  call void @get_stripe_boundary_info(%struct.RestorationTileLimits* %remaining_stripes, %struct.AV1PixelRect* %54, i32 %55, i32* %copy_above, i32* %copy_below)
  %56 = bitcast i32* %full_stripe_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #5
  %57 = load i32, i32* %ss_y.addr, align 4, !tbaa !6
  %shr20 = ashr i32 64, %57
  store i32 %shr20, i32* %full_stripe_height, align 4, !tbaa !6
  %58 = bitcast i32* %runit_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #5
  %59 = load i32, i32* %ss_y.addr, align 4, !tbaa !6
  %shr21 = ashr i32 8, %59
  store i32 %shr21, i32* %runit_offset, align 4, !tbaa !6
  %60 = bitcast i32* %tile_stripe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #5
  %v_start22 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %remaining_stripes, i32 0, i32 2
  %61 = load i32, i32* %v_start22, align 4, !tbaa !38
  %62 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %62, i32 0, i32 1
  %63 = load i32, i32* %top, align 4, !tbaa !33
  %sub23 = sub nsw i32 %61, %63
  %64 = load i32, i32* %runit_offset, align 4, !tbaa !6
  %add24 = add nsw i32 %sub23, %64
  %65 = load i32, i32* %full_stripe_height, align 4, !tbaa !6
  %div = sdiv i32 %add24, %65
  store i32 %div, i32* %tile_stripe, align 4, !tbaa !6
  %66 = bitcast i32* %frame_stripe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #5
  %67 = load i32, i32* %tile_stripe0.addr, align 4, !tbaa !6
  %68 = load i32, i32* %tile_stripe, align 4, !tbaa !6
  %add25 = add nsw i32 %67, %68
  store i32 %add25, i32* %frame_stripe, align 4, !tbaa !6
  %69 = bitcast i32* %rsb_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #5
  %70 = load i32, i32* %frame_stripe, align 4, !tbaa !6
  %mul26 = mul nsw i32 2, %70
  store i32 %mul26, i32* %rsb_row, align 4, !tbaa !6
  %71 = bitcast i32* %nominal_stripe_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #5
  %72 = load i32, i32* %full_stripe_height, align 4, !tbaa !6
  %73 = load i32, i32* %tile_stripe, align 4, !tbaa !6
  %cmp27 = icmp eq i32 %73, 0
  br i1 %cmp27, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %74 = load i32, i32* %runit_offset, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %while.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %74, %cond.true ], [ 0, %cond.false ]
  %sub29 = sub nsw i32 %72, %cond
  store i32 %sub29, i32* %nominal_stripe_height, align 4, !tbaa !6
  %75 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #5
  %76 = load i32, i32* %nominal_stripe_height, align 4, !tbaa !6
  %v_end30 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %remaining_stripes, i32 0, i32 3
  %77 = load i32, i32* %v_end30, align 4, !tbaa !35
  %v_start31 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %remaining_stripes, i32 0, i32 2
  %78 = load i32, i32* %v_start31, align 4, !tbaa !38
  %sub32 = sub nsw i32 %77, %78
  %cmp33 = icmp slt i32 %76, %sub32
  br i1 %cmp33, label %cond.true35, label %cond.false36

cond.true35:                                      ; preds = %cond.end
  %79 = load i32, i32* %nominal_stripe_height, align 4, !tbaa !6
  br label %cond.end40

cond.false36:                                     ; preds = %cond.end
  %v_end37 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %remaining_stripes, i32 0, i32 3
  %80 = load i32, i32* %v_end37, align 4, !tbaa !35
  %v_start38 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %remaining_stripes, i32 0, i32 2
  %81 = load i32, i32* %v_start38, align 4, !tbaa !38
  %sub39 = sub nsw i32 %80, %81
  br label %cond.end40

cond.end40:                                       ; preds = %cond.false36, %cond.true35
  %cond41 = phi i32 [ %79, %cond.true35 ], [ %sub39, %cond.false36 ]
  store i32 %cond41, i32* %h, align 4, !tbaa !6
  %82 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %rsb.addr, align 4, !tbaa !2
  %83 = load i32, i32* %rsb_row, align 4, !tbaa !6
  %84 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %85 = load i32, i32* %h, align 4, !tbaa !6
  %86 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %87 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %88 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %89 = load i32, i32* %copy_above, align 4, !tbaa !6
  %90 = load i32, i32* %copy_below, align 4, !tbaa !6
  %91 = load i32, i32* %optimized_lr.addr, align 4, !tbaa !6
  call void @setup_processing_stripe_boundary(%struct.RestorationTileLimits* %remaining_stripes, %struct.RestorationStripeBoundaries* %82, i32 %83, i32 %84, i32 %85, i8* %86, i32 %87, %struct.RestorationLineBuffers* %88, i32 %89, i32 %90, i32 %91)
  %92 = load void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)*, void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)** %stripe_filter, align 4, !tbaa !2
  %93 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %94 = load i32, i32* %unit_w, align 4, !tbaa !6
  %95 = load i32, i32* %h, align 4, !tbaa !6
  %96 = load i32, i32* %procunit_width, align 4, !tbaa !6
  %97 = load i8*, i8** %data8_tl, align 4, !tbaa !2
  %98 = load i32, i32* %i, align 4, !tbaa !6
  %99 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul42 = mul nsw i32 %98, %99
  %add.ptr43 = getelementptr inbounds i8, i8* %97, i32 %mul42
  %100 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %101 = load i8*, i8** %dst8_tl, align 4, !tbaa !2
  %102 = load i32, i32* %i, align 4, !tbaa !6
  %103 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul44 = mul nsw i32 %102, %103
  %add.ptr45 = getelementptr inbounds i8, i8* %101, i32 %mul44
  %104 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %105 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  %106 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  call void %92(%struct.RestorationUnitInfo* %93, i32 %94, i32 %95, i32 %96, i8* %add.ptr43, i32 %100, i8* %add.ptr45, i32 %104, i32* %105, i32 %106)
  %107 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %108 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %109 = load i32, i32* %h, align 4, !tbaa !6
  %110 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %111 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %112 = load i32, i32* %copy_above, align 4, !tbaa !6
  %113 = load i32, i32* %copy_below, align 4, !tbaa !6
  %114 = load i32, i32* %optimized_lr.addr, align 4, !tbaa !6
  call void @restore_processing_stripe_boundary(%struct.RestorationTileLimits* %remaining_stripes, %struct.RestorationLineBuffers* %107, i32 %108, i32 %109, i8* %110, i32 %111, i32 %112, i32 %113, i32 %114)
  %115 = load i32, i32* %h, align 4, !tbaa !6
  %116 = load i32, i32* %i, align 4, !tbaa !6
  %add46 = add nsw i32 %116, %115
  store i32 %add46, i32* %i, align 4, !tbaa !6
  %117 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast i32* %nominal_stripe_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %rsb_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i32* %frame_stripe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  %121 = bitcast i32* %tile_stripe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  %122 = bitcast i32* %runit_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  %123 = bitcast i32* %full_stripe_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  %124 = bitcast i32* %copy_below to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  %125 = bitcast i32* %copy_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #5
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %126 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #5
  %127 = bitcast %struct.RestorationTileLimits* %remaining_stripes to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %127) #5
  %128 = bitcast i32* %procunit_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #5
  %129 = bitcast void (%struct.RestorationUnitInfo*, i32, i32, i32, i8*, i32, i8*, i32, i32*, i32)** %stripe_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #5
  %130 = bitcast i32* %filter_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %131 = bitcast i8** %dst8_tl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #5
  %132 = bitcast i8** %data8_tl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #5
  %133 = bitcast i32* %unit_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #5
  %134 = bitcast i32* %unit_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %unit_rtype) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @copy_tile(i32 %width, i32 %height, i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %highbd) #0 {
entry:
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %highbd.addr = alloca i32, align 4
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %highbd, i32* %highbd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %width.addr, align 4, !tbaa !6
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %3 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %7 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %8 = ptrtoint i8* %7 to i32
  %shl1 = shl i32 %8, 1
  %9 = inttoptr i32 %shl1 to i16*
  %10 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  call void @copy_tile_highbd(i32 %1, i32 %2, i16* %5, i32 %6, i16* %9, i32 %10)
  br label %return

if.end:                                           ; preds = %entry
  %11 = load i32, i32* %highbd.addr, align 4, !tbaa !6
  %12 = load i32, i32* %width.addr, align 4, !tbaa !6
  %13 = load i32, i32* %height.addr, align 4, !tbaa !6
  %14 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %15 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %16 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %17 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  call void @copy_tile_lowbd(i32 %12, i32 %13, i8* %14, i32 %15, i8* %16, i32 %17)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal void @get_stripe_boundary_info(%struct.RestorationTileLimits* %limits, %struct.AV1PixelRect* %tile_rect, i32 %ss_y, i32* %copy_above, i32* %copy_below) #0 {
entry:
  %limits.addr = alloca %struct.RestorationTileLimits*, align 4
  %tile_rect.addr = alloca %struct.AV1PixelRect*, align 4
  %ss_y.addr = alloca i32, align 4
  %copy_above.addr = alloca i32*, align 4
  %copy_below.addr = alloca i32*, align 4
  %full_stripe_height = alloca i32, align 4
  %runit_offset = alloca i32, align 4
  %first_stripe_in_tile = alloca i32, align 4
  %this_stripe_height = alloca i32, align 4
  %last_stripe_in_tile = alloca i32, align 4
  store %struct.RestorationTileLimits* %limits, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  store %struct.AV1PixelRect* %tile_rect, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !6
  store i32* %copy_above, i32** %copy_above.addr, align 4, !tbaa !2
  store i32* %copy_below, i32** %copy_below.addr, align 4, !tbaa !2
  %0 = load i32*, i32** %copy_above.addr, align 4, !tbaa !2
  store i32 1, i32* %0, align 4, !tbaa !6
  %1 = load i32*, i32** %copy_below.addr, align 4, !tbaa !2
  store i32 1, i32* %1, align 4, !tbaa !6
  %2 = bitcast i32* %full_stripe_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %ss_y.addr, align 4, !tbaa !6
  %shr = ashr i32 64, %3
  store i32 %shr, i32* %full_stripe_height, align 4, !tbaa !6
  %4 = bitcast i32* %runit_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %ss_y.addr, align 4, !tbaa !6
  %shr1 = ashr i32 8, %5
  store i32 %shr1, i32* %runit_offset, align 4, !tbaa !6
  %6 = bitcast i32* %first_stripe_in_tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %7, i32 0, i32 2
  %8 = load i32, i32* %v_start, align 4, !tbaa !38
  %9 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %9, i32 0, i32 1
  %10 = load i32, i32* %top, align 4, !tbaa !33
  %cmp = icmp eq i32 %8, %10
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %first_stripe_in_tile, align 4, !tbaa !6
  %11 = bitcast i32* %this_stripe_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %full_stripe_height, align 4, !tbaa !6
  %13 = load i32, i32* %first_stripe_in_tile, align 4, !tbaa !6
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %14 = load i32, i32* %runit_offset, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %14, %cond.true ], [ 0, %cond.false ]
  %sub = sub nsw i32 %12, %cond
  store i32 %sub, i32* %this_stripe_height, align 4, !tbaa !6
  %15 = bitcast i32* %last_stripe_in_tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start2 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %16, i32 0, i32 2
  %17 = load i32, i32* %v_start2, align 4, !tbaa !38
  %18 = load i32, i32* %this_stripe_height, align 4, !tbaa !6
  %add = add nsw i32 %17, %18
  %19 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %19, i32 0, i32 3
  %20 = load i32, i32* %bottom, align 4, !tbaa !35
  %cmp3 = icmp sge i32 %add, %20
  %conv4 = zext i1 %cmp3 to i32
  store i32 %conv4, i32* %last_stripe_in_tile, align 4, !tbaa !6
  %21 = load i32, i32* %first_stripe_in_tile, align 4, !tbaa !6
  %tobool5 = icmp ne i32 %21, 0
  br i1 %tobool5, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %22 = load i32*, i32** %copy_above.addr, align 4, !tbaa !2
  store i32 0, i32* %22, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %23 = load i32, i32* %last_stripe_in_tile, align 4, !tbaa !6
  %tobool6 = icmp ne i32 %23, 0
  br i1 %tobool6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end
  %24 = load i32*, i32** %copy_below.addr, align 4, !tbaa !2
  store i32 0, i32* %24, align 4, !tbaa !6
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %if.end
  %25 = bitcast i32* %last_stripe_in_tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast i32* %this_stripe_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast i32* %first_stripe_in_tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %28 = bitcast i32* %runit_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i32* %full_stripe_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  ret void
}

; Function Attrs: nounwind
define internal void @setup_processing_stripe_boundary(%struct.RestorationTileLimits* %limits, %struct.RestorationStripeBoundaries* %rsb, i32 %rsb_row, i32 %use_highbd, i32 %h, i8* %data8, i32 %data_stride, %struct.RestorationLineBuffers* %rlbs, i32 %copy_above, i32 %copy_below, i32 %opt) #0 {
entry:
  %limits.addr = alloca %struct.RestorationTileLimits*, align 4
  %rsb.addr = alloca %struct.RestorationStripeBoundaries*, align 4
  %rsb_row.addr = alloca i32, align 4
  %use_highbd.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %data8.addr = alloca i8*, align 4
  %data_stride.addr = alloca i32, align 4
  %rlbs.addr = alloca %struct.RestorationLineBuffers*, align 4
  %copy_above.addr = alloca i32, align 4
  %copy_below.addr = alloca i32, align 4
  %opt.addr = alloca i32, align 4
  %buf_stride = alloca i32, align 4
  %buf_x0_off = alloca i32, align 4
  %line_width = alloca i32, align 4
  %line_size = alloca i32, align 4
  %data_x0 = alloca i32, align 4
  %data8_tl = alloca i8*, align 4
  %i = alloca i32, align 4
  %buf_row = alloca i32, align 4
  %buf_off = alloca i32, align 4
  %buf = alloca i8*, align 4
  %dst8 = alloca i8*, align 4
  %stripe_end = alloca i32, align 4
  %data8_bl = alloca i8*, align 4
  %i37 = alloca i32, align 4
  %buf_row42 = alloca i32, align 4
  %buf_off49 = alloca i32, align 4
  %src = alloca i8*, align 4
  %dst854 = alloca i8*, align 4
  %data8_tl77 = alloca i8*, align 4
  %dst882 = alloca i8*, align 4
  %stripe_end113 = alloca i32, align 4
  %data8_bl116 = alloca i8*, align 4
  %dst8120 = alloca i8*, align 4
  store %struct.RestorationTileLimits* %limits, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  store %struct.RestorationStripeBoundaries* %rsb, %struct.RestorationStripeBoundaries** %rsb.addr, align 4, !tbaa !2
  store i32 %rsb_row, i32* %rsb_row.addr, align 4, !tbaa !6
  store i32 %use_highbd, i32* %use_highbd.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i8* %data8, i8** %data8.addr, align 4, !tbaa !2
  store i32 %data_stride, i32* %data_stride.addr, align 4, !tbaa !6
  store %struct.RestorationLineBuffers* %rlbs, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  store i32 %copy_above, i32* %copy_above.addr, align 4, !tbaa !6
  store i32 %copy_below, i32* %copy_below.addr, align 4, !tbaa !6
  store i32 %opt, i32* %opt.addr, align 4, !tbaa !6
  %0 = bitcast i32* %buf_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %rsb.addr, align 4, !tbaa !2
  %stripe_boundary_stride = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %1, i32 0, i32 2
  %2 = load i32, i32* %stripe_boundary_stride, align 4, !tbaa !53
  store i32 %2, i32* %buf_stride, align 4, !tbaa !6
  %3 = bitcast i32* %buf_x0_off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %4, i32 0, i32 0
  %5 = load i32, i32* %h_start, align 4, !tbaa !36
  store i32 %5, i32* %buf_x0_off, align 4, !tbaa !6
  %6 = bitcast i32* %line_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_end = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %7, i32 0, i32 1
  %8 = load i32, i32* %h_end, align 4, !tbaa !33
  %9 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_start1 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %9, i32 0, i32 0
  %10 = load i32, i32* %h_start1, align 4, !tbaa !36
  %sub = sub nsw i32 %8, %10
  %add = add nsw i32 %sub, 8
  store i32 %add, i32* %line_width, align 4, !tbaa !6
  %11 = bitcast i32* %line_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %line_width, align 4, !tbaa !6
  %13 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl = shl i32 %12, %13
  store i32 %shl, i32* %line_size, align 4, !tbaa !6
  %14 = bitcast i32* %data_x0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_start2 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %15, i32 0, i32 0
  %16 = load i32, i32* %h_start2, align 4, !tbaa !36
  %sub3 = sub nsw i32 %16, 4
  store i32 %sub3, i32* %data_x0, align 4, !tbaa !6
  %17 = load i32, i32* %opt.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %17, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %18 = load i32, i32* %copy_above.addr, align 4, !tbaa !6
  %tobool4 = icmp ne i32 %18, 0
  br i1 %tobool4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  %19 = bitcast i8** %data8_tl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %21 = load i32, i32* %data_x0, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %20, i32 %21
  %22 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %22, i32 0, i32 2
  %23 = load i32, i32* %v_start, align 4, !tbaa !38
  %24 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %23, %24
  %add.ptr6 = getelementptr inbounds i8, i8* %add.ptr, i32 %mul
  store i8* %add.ptr6, i8** %data8_tl, align 4, !tbaa !2
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  store i32 -3, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then5
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %26, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %28 = bitcast i32* %buf_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  %29 = load i32, i32* %rsb_row.addr, align 4, !tbaa !6
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %add7 = add nsw i32 %30, 2
  %cmp8 = icmp sgt i32 %add7, 0
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %add9 = add nsw i32 %31, 2
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add9, %cond.true ], [ 0, %cond.false ]
  %add10 = add nsw i32 %29, %cond
  store i32 %add10, i32* %buf_row, align 4, !tbaa !6
  %32 = bitcast i32* %buf_off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  %33 = load i32, i32* %buf_x0_off, align 4, !tbaa !6
  %34 = load i32, i32* %buf_row, align 4, !tbaa !6
  %35 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul11 = mul nsw i32 %34, %35
  %add12 = add nsw i32 %33, %mul11
  store i32 %add12, i32* %buf_off, align 4, !tbaa !6
  %36 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #5
  %37 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %rsb.addr, align 4, !tbaa !2
  %stripe_boundary_above = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %37, i32 0, i32 0
  %38 = load i8*, i8** %stripe_boundary_above, align 4, !tbaa !54
  %39 = load i32, i32* %buf_off, align 4, !tbaa !6
  %40 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl13 = shl i32 %39, %40
  %add.ptr14 = getelementptr inbounds i8, i8* %38, i32 %shl13
  store i8* %add.ptr14, i8** %buf, align 4, !tbaa !2
  %41 = bitcast i8** %dst8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #5
  %42 = load i8*, i8** %data8_tl, align 4, !tbaa !2
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %44 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 %43, %44
  %add.ptr16 = getelementptr inbounds i8, i8* %42, i32 %mul15
  store i8* %add.ptr16, i8** %dst8, align 4, !tbaa !2
  %45 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %tmp_save_above = getelementptr inbounds %struct.RestorationLineBuffers, %struct.RestorationLineBuffers* %45, i32 0, i32 0
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %add17 = add nsw i32 %46, 3
  %arrayidx = getelementptr inbounds [3 x [392 x i16]], [3 x [392 x i16]]* %tmp_save_above, i32 0, i32 %add17
  %arraydecay = getelementptr inbounds [392 x i16], [392 x i16]* %arrayidx, i32 0, i32 0
  %47 = bitcast i16* %arraydecay to i8*
  %48 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool18 = icmp ne i32 %48, 0
  br i1 %tobool18, label %cond.true19, label %cond.false21

cond.true19:                                      ; preds = %cond.end
  %49 = load i8*, i8** %dst8, align 4, !tbaa !2
  %50 = ptrtoint i8* %49 to i32
  %shl20 = shl i32 %50, 1
  %51 = inttoptr i32 %shl20 to i16*
  %52 = bitcast i16* %51 to i8*
  br label %cond.end22

cond.false21:                                     ; preds = %cond.end
  %53 = load i8*, i8** %dst8, align 4, !tbaa !2
  br label %cond.end22

cond.end22:                                       ; preds = %cond.false21, %cond.true19
  %cond23 = phi i8* [ %52, %cond.true19 ], [ %53, %cond.false21 ]
  %54 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %47, i8* align 1 %cond23, i32 %54, i1 false)
  %55 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool24 = icmp ne i32 %55, 0
  br i1 %tobool24, label %cond.true25, label %cond.false27

cond.true25:                                      ; preds = %cond.end22
  %56 = load i8*, i8** %dst8, align 4, !tbaa !2
  %57 = ptrtoint i8* %56 to i32
  %shl26 = shl i32 %57, 1
  %58 = inttoptr i32 %shl26 to i16*
  %59 = bitcast i16* %58 to i8*
  br label %cond.end28

cond.false27:                                     ; preds = %cond.end22
  %60 = load i8*, i8** %dst8, align 4, !tbaa !2
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false27, %cond.true25
  %cond29 = phi i8* [ %59, %cond.true25 ], [ %60, %cond.false27 ]
  %61 = load i8*, i8** %buf, align 4, !tbaa !2
  %62 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %cond29, i8* align 1 %61, i32 %62, i1 false)
  %63 = bitcast i8** %dst8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  %64 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  %65 = bitcast i32* %buf_off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %66 = bitcast i32* %buf_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end28
  %67 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %67, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %68 = bitcast i8** %data8_tl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  %69 = load i32, i32* %copy_below.addr, align 4, !tbaa !6
  %tobool30 = icmp ne i32 %69, 0
  br i1 %tobool30, label %if.then31, label %if.end74

if.then31:                                        ; preds = %if.end
  %70 = bitcast i32* %stripe_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #5
  %71 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start32 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %71, i32 0, i32 2
  %72 = load i32, i32* %v_start32, align 4, !tbaa !38
  %73 = load i32, i32* %h.addr, align 4, !tbaa !6
  %add33 = add nsw i32 %72, %73
  store i32 %add33, i32* %stripe_end, align 4, !tbaa !6
  %74 = bitcast i8** %data8_bl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #5
  %75 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %76 = load i32, i32* %data_x0, align 4, !tbaa !6
  %add.ptr34 = getelementptr inbounds i8, i8* %75, i32 %76
  %77 = load i32, i32* %stripe_end, align 4, !tbaa !6
  %78 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul35 = mul nsw i32 %77, %78
  %add.ptr36 = getelementptr inbounds i8, i8* %add.ptr34, i32 %mul35
  store i8* %add.ptr36, i8** %data8_bl, align 4, !tbaa !2
  %79 = bitcast i32* %i37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #5
  store i32 0, i32* %i37, align 4, !tbaa !6
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc71, %if.then31
  %80 = load i32, i32* %i37, align 4, !tbaa !6
  %cmp39 = icmp slt i32 %80, 3
  br i1 %cmp39, label %for.body41, label %for.cond.cleanup40

for.cond.cleanup40:                               ; preds = %for.cond38
  %81 = bitcast i32* %i37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #5
  br label %for.end73

for.body41:                                       ; preds = %for.cond38
  %82 = bitcast i32* %buf_row42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #5
  %83 = load i32, i32* %rsb_row.addr, align 4, !tbaa !6
  %84 = load i32, i32* %i37, align 4, !tbaa !6
  %cmp43 = icmp slt i32 %84, 1
  br i1 %cmp43, label %cond.true44, label %cond.false45

cond.true44:                                      ; preds = %for.body41
  %85 = load i32, i32* %i37, align 4, !tbaa !6
  br label %cond.end46

cond.false45:                                     ; preds = %for.body41
  br label %cond.end46

cond.end46:                                       ; preds = %cond.false45, %cond.true44
  %cond47 = phi i32 [ %85, %cond.true44 ], [ 1, %cond.false45 ]
  %add48 = add nsw i32 %83, %cond47
  store i32 %add48, i32* %buf_row42, align 4, !tbaa !6
  %86 = bitcast i32* %buf_off49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #5
  %87 = load i32, i32* %buf_x0_off, align 4, !tbaa !6
  %88 = load i32, i32* %buf_row42, align 4, !tbaa !6
  %89 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul50 = mul nsw i32 %88, %89
  %add51 = add nsw i32 %87, %mul50
  store i32 %add51, i32* %buf_off49, align 4, !tbaa !6
  %90 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #5
  %91 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %rsb.addr, align 4, !tbaa !2
  %stripe_boundary_below = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %91, i32 0, i32 1
  %92 = load i8*, i8** %stripe_boundary_below, align 4, !tbaa !55
  %93 = load i32, i32* %buf_off49, align 4, !tbaa !6
  %94 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl52 = shl i32 %93, %94
  %add.ptr53 = getelementptr inbounds i8, i8* %92, i32 %shl52
  store i8* %add.ptr53, i8** %src, align 4, !tbaa !2
  %95 = bitcast i8** %dst854 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #5
  %96 = load i8*, i8** %data8_bl, align 4, !tbaa !2
  %97 = load i32, i32* %i37, align 4, !tbaa !6
  %98 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul55 = mul nsw i32 %97, %98
  %add.ptr56 = getelementptr inbounds i8, i8* %96, i32 %mul55
  store i8* %add.ptr56, i8** %dst854, align 4, !tbaa !2
  %99 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %tmp_save_below = getelementptr inbounds %struct.RestorationLineBuffers, %struct.RestorationLineBuffers* %99, i32 0, i32 1
  %100 = load i32, i32* %i37, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds [3 x [392 x i16]], [3 x [392 x i16]]* %tmp_save_below, i32 0, i32 %100
  %arraydecay58 = getelementptr inbounds [392 x i16], [392 x i16]* %arrayidx57, i32 0, i32 0
  %101 = bitcast i16* %arraydecay58 to i8*
  %102 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool59 = icmp ne i32 %102, 0
  br i1 %tobool59, label %cond.true60, label %cond.false62

cond.true60:                                      ; preds = %cond.end46
  %103 = load i8*, i8** %dst854, align 4, !tbaa !2
  %104 = ptrtoint i8* %103 to i32
  %shl61 = shl i32 %104, 1
  %105 = inttoptr i32 %shl61 to i16*
  %106 = bitcast i16* %105 to i8*
  br label %cond.end63

cond.false62:                                     ; preds = %cond.end46
  %107 = load i8*, i8** %dst854, align 4, !tbaa !2
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false62, %cond.true60
  %cond64 = phi i8* [ %106, %cond.true60 ], [ %107, %cond.false62 ]
  %108 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %101, i8* align 1 %cond64, i32 %108, i1 false)
  %109 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool65 = icmp ne i32 %109, 0
  br i1 %tobool65, label %cond.true66, label %cond.false68

cond.true66:                                      ; preds = %cond.end63
  %110 = load i8*, i8** %dst854, align 4, !tbaa !2
  %111 = ptrtoint i8* %110 to i32
  %shl67 = shl i32 %111, 1
  %112 = inttoptr i32 %shl67 to i16*
  %113 = bitcast i16* %112 to i8*
  br label %cond.end69

cond.false68:                                     ; preds = %cond.end63
  %114 = load i8*, i8** %dst854, align 4, !tbaa !2
  br label %cond.end69

cond.end69:                                       ; preds = %cond.false68, %cond.true66
  %cond70 = phi i8* [ %113, %cond.true66 ], [ %114, %cond.false68 ]
  %115 = load i8*, i8** %src, align 4, !tbaa !2
  %116 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %cond70, i8* align 1 %115, i32 %116, i1 false)
  %117 = bitcast i8** %dst854 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %buf_off49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i32* %buf_row42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  br label %for.inc71

for.inc71:                                        ; preds = %cond.end69
  %121 = load i32, i32* %i37, align 4, !tbaa !6
  %inc72 = add nsw i32 %121, 1
  store i32 %inc72, i32* %i37, align 4, !tbaa !6
  br label %for.cond38

for.end73:                                        ; preds = %for.cond.cleanup40
  %122 = bitcast i8** %data8_bl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  %123 = bitcast i32* %stripe_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  br label %if.end74

if.end74:                                         ; preds = %for.end73, %if.end
  br label %if.end149

if.else:                                          ; preds = %entry
  %124 = load i32, i32* %copy_above.addr, align 4, !tbaa !6
  %tobool75 = icmp ne i32 %124, 0
  br i1 %tobool75, label %if.then76, label %if.end110

if.then76:                                        ; preds = %if.else
  %125 = bitcast i8** %data8_tl77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #5
  %126 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %127 = load i32, i32* %data_x0, align 4, !tbaa !6
  %add.ptr78 = getelementptr inbounds i8, i8* %126, i32 %127
  %128 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start79 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %128, i32 0, i32 2
  %129 = load i32, i32* %v_start79, align 4, !tbaa !38
  %130 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul80 = mul nsw i32 %129, %130
  %add.ptr81 = getelementptr inbounds i8, i8* %add.ptr78, i32 %mul80
  store i8* %add.ptr81, i8** %data8_tl77, align 4, !tbaa !2
  %131 = bitcast i8** %dst882 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #5
  %132 = load i8*, i8** %data8_tl77, align 4, !tbaa !2
  %133 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul83 = mul nsw i32 -3, %133
  %add.ptr84 = getelementptr inbounds i8, i8* %132, i32 %mul83
  store i8* %add.ptr84, i8** %dst882, align 4, !tbaa !2
  %134 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %tmp_save_above85 = getelementptr inbounds %struct.RestorationLineBuffers, %struct.RestorationLineBuffers* %134, i32 0, i32 0
  %arrayidx86 = getelementptr inbounds [3 x [392 x i16]], [3 x [392 x i16]]* %tmp_save_above85, i32 0, i32 0
  %arraydecay87 = getelementptr inbounds [392 x i16], [392 x i16]* %arrayidx86, i32 0, i32 0
  %135 = bitcast i16* %arraydecay87 to i8*
  %136 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool88 = icmp ne i32 %136, 0
  br i1 %tobool88, label %cond.true89, label %cond.false91

cond.true89:                                      ; preds = %if.then76
  %137 = load i8*, i8** %dst882, align 4, !tbaa !2
  %138 = ptrtoint i8* %137 to i32
  %shl90 = shl i32 %138, 1
  %139 = inttoptr i32 %shl90 to i16*
  %140 = bitcast i16* %139 to i8*
  br label %cond.end92

cond.false91:                                     ; preds = %if.then76
  %141 = load i8*, i8** %dst882, align 4, !tbaa !2
  br label %cond.end92

cond.end92:                                       ; preds = %cond.false91, %cond.true89
  %cond93 = phi i8* [ %140, %cond.true89 ], [ %141, %cond.false91 ]
  %142 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %135, i8* align 1 %cond93, i32 %142, i1 false)
  %143 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool94 = icmp ne i32 %143, 0
  br i1 %tobool94, label %cond.true95, label %cond.false97

cond.true95:                                      ; preds = %cond.end92
  %144 = load i8*, i8** %dst882, align 4, !tbaa !2
  %145 = ptrtoint i8* %144 to i32
  %shl96 = shl i32 %145, 1
  %146 = inttoptr i32 %shl96 to i16*
  %147 = bitcast i16* %146 to i8*
  br label %cond.end98

cond.false97:                                     ; preds = %cond.end92
  %148 = load i8*, i8** %dst882, align 4, !tbaa !2
  br label %cond.end98

cond.end98:                                       ; preds = %cond.false97, %cond.true95
  %cond99 = phi i8* [ %147, %cond.true95 ], [ %148, %cond.false97 ]
  %149 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool100 = icmp ne i32 %149, 0
  br i1 %tobool100, label %cond.true101, label %cond.false105

cond.true101:                                     ; preds = %cond.end98
  %150 = load i8*, i8** %data8_tl77, align 4, !tbaa !2
  %151 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul102 = mul nsw i32 -2, %151
  %add.ptr103 = getelementptr inbounds i8, i8* %150, i32 %mul102
  %152 = ptrtoint i8* %add.ptr103 to i32
  %shl104 = shl i32 %152, 1
  %153 = inttoptr i32 %shl104 to i16*
  %154 = bitcast i16* %153 to i8*
  br label %cond.end108

cond.false105:                                    ; preds = %cond.end98
  %155 = load i8*, i8** %data8_tl77, align 4, !tbaa !2
  %156 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul106 = mul nsw i32 -2, %156
  %add.ptr107 = getelementptr inbounds i8, i8* %155, i32 %mul106
  br label %cond.end108

cond.end108:                                      ; preds = %cond.false105, %cond.true101
  %cond109 = phi i8* [ %154, %cond.true101 ], [ %add.ptr107, %cond.false105 ]
  %157 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %cond99, i8* align 1 %cond109, i32 %157, i1 false)
  %158 = bitcast i8** %dst882 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #5
  %159 = bitcast i8** %data8_tl77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #5
  br label %if.end110

if.end110:                                        ; preds = %cond.end108, %if.else
  %160 = load i32, i32* %copy_below.addr, align 4, !tbaa !6
  %tobool111 = icmp ne i32 %160, 0
  br i1 %tobool111, label %if.then112, label %if.end148

if.then112:                                       ; preds = %if.end110
  %161 = bitcast i32* %stripe_end113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %161) #5
  %162 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start114 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %162, i32 0, i32 2
  %163 = load i32, i32* %v_start114, align 4, !tbaa !38
  %164 = load i32, i32* %h.addr, align 4, !tbaa !6
  %add115 = add nsw i32 %163, %164
  store i32 %add115, i32* %stripe_end113, align 4, !tbaa !6
  %165 = bitcast i8** %data8_bl116 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %165) #5
  %166 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %167 = load i32, i32* %data_x0, align 4, !tbaa !6
  %add.ptr117 = getelementptr inbounds i8, i8* %166, i32 %167
  %168 = load i32, i32* %stripe_end113, align 4, !tbaa !6
  %169 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul118 = mul nsw i32 %168, %169
  %add.ptr119 = getelementptr inbounds i8, i8* %add.ptr117, i32 %mul118
  store i8* %add.ptr119, i8** %data8_bl116, align 4, !tbaa !2
  %170 = bitcast i8** %dst8120 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %170) #5
  %171 = load i8*, i8** %data8_bl116, align 4, !tbaa !2
  %172 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul121 = mul nsw i32 2, %172
  %add.ptr122 = getelementptr inbounds i8, i8* %171, i32 %mul121
  store i8* %add.ptr122, i8** %dst8120, align 4, !tbaa !2
  %173 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %tmp_save_below123 = getelementptr inbounds %struct.RestorationLineBuffers, %struct.RestorationLineBuffers* %173, i32 0, i32 1
  %arrayidx124 = getelementptr inbounds [3 x [392 x i16]], [3 x [392 x i16]]* %tmp_save_below123, i32 0, i32 2
  %arraydecay125 = getelementptr inbounds [392 x i16], [392 x i16]* %arrayidx124, i32 0, i32 0
  %174 = bitcast i16* %arraydecay125 to i8*
  %175 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool126 = icmp ne i32 %175, 0
  br i1 %tobool126, label %cond.true127, label %cond.false129

cond.true127:                                     ; preds = %if.then112
  %176 = load i8*, i8** %dst8120, align 4, !tbaa !2
  %177 = ptrtoint i8* %176 to i32
  %shl128 = shl i32 %177, 1
  %178 = inttoptr i32 %shl128 to i16*
  %179 = bitcast i16* %178 to i8*
  br label %cond.end130

cond.false129:                                    ; preds = %if.then112
  %180 = load i8*, i8** %dst8120, align 4, !tbaa !2
  br label %cond.end130

cond.end130:                                      ; preds = %cond.false129, %cond.true127
  %cond131 = phi i8* [ %179, %cond.true127 ], [ %180, %cond.false129 ]
  %181 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %174, i8* align 1 %cond131, i32 %181, i1 false)
  %182 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool132 = icmp ne i32 %182, 0
  br i1 %tobool132, label %cond.true133, label %cond.false135

cond.true133:                                     ; preds = %cond.end130
  %183 = load i8*, i8** %dst8120, align 4, !tbaa !2
  %184 = ptrtoint i8* %183 to i32
  %shl134 = shl i32 %184, 1
  %185 = inttoptr i32 %shl134 to i16*
  %186 = bitcast i16* %185 to i8*
  br label %cond.end136

cond.false135:                                    ; preds = %cond.end130
  %187 = load i8*, i8** %dst8120, align 4, !tbaa !2
  br label %cond.end136

cond.end136:                                      ; preds = %cond.false135, %cond.true133
  %cond137 = phi i8* [ %186, %cond.true133 ], [ %187, %cond.false135 ]
  %188 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool138 = icmp ne i32 %188, 0
  br i1 %tobool138, label %cond.true139, label %cond.false143

cond.true139:                                     ; preds = %cond.end136
  %189 = load i8*, i8** %data8_bl116, align 4, !tbaa !2
  %190 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul140 = mul nsw i32 1, %190
  %add.ptr141 = getelementptr inbounds i8, i8* %189, i32 %mul140
  %191 = ptrtoint i8* %add.ptr141 to i32
  %shl142 = shl i32 %191, 1
  %192 = inttoptr i32 %shl142 to i16*
  %193 = bitcast i16* %192 to i8*
  br label %cond.end146

cond.false143:                                    ; preds = %cond.end136
  %194 = load i8*, i8** %data8_bl116, align 4, !tbaa !2
  %195 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul144 = mul nsw i32 1, %195
  %add.ptr145 = getelementptr inbounds i8, i8* %194, i32 %mul144
  br label %cond.end146

cond.end146:                                      ; preds = %cond.false143, %cond.true139
  %cond147 = phi i8* [ %193, %cond.true139 ], [ %add.ptr145, %cond.false143 ]
  %196 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %cond137, i8* align 1 %cond147, i32 %196, i1 false)
  %197 = bitcast i8** %dst8120 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #5
  %198 = bitcast i8** %data8_bl116 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #5
  %199 = bitcast i32* %stripe_end113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #5
  br label %if.end148

if.end148:                                        ; preds = %cond.end146, %if.end110
  br label %if.end149

if.end149:                                        ; preds = %if.end148, %if.end74
  %200 = bitcast i32* %data_x0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #5
  %201 = bitcast i32* %line_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #5
  %202 = bitcast i32* %line_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #5
  %203 = bitcast i32* %buf_x0_off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #5
  %204 = bitcast i32* %buf_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #5
  ret void
}

; Function Attrs: nounwind
define internal void @restore_processing_stripe_boundary(%struct.RestorationTileLimits* %limits, %struct.RestorationLineBuffers* %rlbs, i32 %use_highbd, i32 %h, i8* %data8, i32 %data_stride, i32 %copy_above, i32 %copy_below, i32 %opt) #0 {
entry:
  %limits.addr = alloca %struct.RestorationTileLimits*, align 4
  %rlbs.addr = alloca %struct.RestorationLineBuffers*, align 4
  %use_highbd.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %data8.addr = alloca i8*, align 4
  %data_stride.addr = alloca i32, align 4
  %copy_above.addr = alloca i32, align 4
  %copy_below.addr = alloca i32, align 4
  %opt.addr = alloca i32, align 4
  %line_width = alloca i32, align 4
  %line_size = alloca i32, align 4
  %data_x0 = alloca i32, align 4
  %data8_tl = alloca i8*, align 4
  %i = alloca i32, align 4
  %dst8 = alloca i8*, align 4
  %stripe_bottom = alloca i32, align 4
  %data8_bl = alloca i8*, align 4
  %i18 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %dst828 = alloca i8*, align 4
  %data8_tl45 = alloca i8*, align 4
  %dst850 = alloca i8*, align 4
  %stripe_bottom65 = alloca i32, align 4
  %data8_bl68 = alloca i8*, align 4
  %dst877 = alloca i8*, align 4
  store %struct.RestorationTileLimits* %limits, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  store %struct.RestorationLineBuffers* %rlbs, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  store i32 %use_highbd, i32* %use_highbd.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i8* %data8, i8** %data8.addr, align 4, !tbaa !2
  store i32 %data_stride, i32* %data_stride.addr, align 4, !tbaa !6
  store i32 %copy_above, i32* %copy_above.addr, align 4, !tbaa !6
  store i32 %copy_below, i32* %copy_below.addr, align 4, !tbaa !6
  store i32 %opt, i32* %opt.addr, align 4, !tbaa !6
  %0 = bitcast i32* %line_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_end = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %1, i32 0, i32 1
  %2 = load i32, i32* %h_end, align 4, !tbaa !33
  %3 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %3, i32 0, i32 0
  %4 = load i32, i32* %h_start, align 4, !tbaa !36
  %sub = sub nsw i32 %2, %4
  %add = add nsw i32 %sub, 8
  store i32 %add, i32* %line_width, align 4, !tbaa !6
  %5 = bitcast i32* %line_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %line_width, align 4, !tbaa !6
  %7 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl = shl i32 %6, %7
  store i32 %shl, i32* %line_size, align 4, !tbaa !6
  %8 = bitcast i32* %data_x0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_start1 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %9, i32 0, i32 0
  %10 = load i32, i32* %h_start1, align 4, !tbaa !36
  %sub2 = sub nsw i32 %10, 4
  store i32 %sub2, i32* %data_x0, align 4, !tbaa !6
  %11 = load i32, i32* %opt.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %12 = load i32, i32* %copy_above.addr, align 4, !tbaa !6
  %tobool3 = icmp ne i32 %12, 0
  br i1 %tobool3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %13 = bitcast i8** %data8_tl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %15 = load i32, i32* %data_x0, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %16, i32 0, i32 2
  %17 = load i32, i32* %v_start, align 4, !tbaa !38
  %18 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %17, %18
  %add.ptr5 = getelementptr inbounds i8, i8* %add.ptr, i32 %mul
  store i8* %add.ptr5, i8** %data8_tl, align 4, !tbaa !2
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  store i32 -3, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then4
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %20, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %22 = bitcast i8** %dst8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = load i8*, i8** %data8_tl, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %25 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 %24, %25
  %add.ptr7 = getelementptr inbounds i8, i8* %23, i32 %mul6
  store i8* %add.ptr7, i8** %dst8, align 4, !tbaa !2
  %26 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool8 = icmp ne i32 %26, 0
  br i1 %tobool8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %27 = load i8*, i8** %dst8, align 4, !tbaa !2
  %28 = ptrtoint i8* %27 to i32
  %shl9 = shl i32 %28, 1
  %29 = inttoptr i32 %shl9 to i16*
  %30 = bitcast i16* %29 to i8*
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %31 = load i8*, i8** %dst8, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %30, %cond.true ], [ %31, %cond.false ]
  %32 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %tmp_save_above = getelementptr inbounds %struct.RestorationLineBuffers, %struct.RestorationLineBuffers* %32, i32 0, i32 0
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %add10 = add nsw i32 %33, 3
  %arrayidx = getelementptr inbounds [3 x [392 x i16]], [3 x [392 x i16]]* %tmp_save_above, i32 0, i32 %add10
  %arraydecay = getelementptr inbounds [392 x i16], [392 x i16]* %arrayidx, i32 0, i32 0
  %34 = bitcast i16* %arraydecay to i8*
  %35 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %cond, i8* align 2 %34, i32 %35, i1 false)
  %36 = bitcast i8** %dst8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %38 = bitcast i8** %data8_tl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #5
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  %39 = load i32, i32* %copy_below.addr, align 4, !tbaa !6
  %tobool11 = icmp ne i32 %39, 0
  br i1 %tobool11, label %if.then12, label %if.end42

if.then12:                                        ; preds = %if.end
  %40 = bitcast i32* %stripe_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  %41 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start13 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %41, i32 0, i32 2
  %42 = load i32, i32* %v_start13, align 4, !tbaa !38
  %43 = load i32, i32* %h.addr, align 4, !tbaa !6
  %add14 = add nsw i32 %42, %43
  store i32 %add14, i32* %stripe_bottom, align 4, !tbaa !6
  %44 = bitcast i8** %data8_bl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  %45 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %46 = load i32, i32* %data_x0, align 4, !tbaa !6
  %add.ptr15 = getelementptr inbounds i8, i8* %45, i32 %46
  %47 = load i32, i32* %stripe_bottom, align 4, !tbaa !6
  %48 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul16 = mul nsw i32 %47, %48
  %add.ptr17 = getelementptr inbounds i8, i8* %add.ptr15, i32 %mul16
  store i8* %add.ptr17, i8** %data8_bl, align 4, !tbaa !2
  %49 = bitcast i32* %i18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #5
  store i32 0, i32* %i18, align 4, !tbaa !6
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc39, %if.then12
  %50 = load i32, i32* %i18, align 4, !tbaa !6
  %cmp20 = icmp slt i32 %50, 3
  br i1 %cmp20, label %for.body22, label %for.cond.cleanup21

for.cond.cleanup21:                               ; preds = %for.cond19
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body22:                                       ; preds = %for.cond19
  %51 = load i32, i32* %stripe_bottom, align 4, !tbaa !6
  %52 = load i32, i32* %i18, align 4, !tbaa !6
  %add23 = add nsw i32 %51, %52
  %53 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_end = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %53, i32 0, i32 3
  %54 = load i32, i32* %v_end, align 4, !tbaa !35
  %add24 = add nsw i32 %54, 3
  %cmp25 = icmp sge i32 %add23, %add24
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %for.body22
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %for.body22
  %55 = bitcast i8** %dst828 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #5
  %56 = load i8*, i8** %data8_bl, align 4, !tbaa !2
  %57 = load i32, i32* %i18, align 4, !tbaa !6
  %58 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 %57, %58
  %add.ptr30 = getelementptr inbounds i8, i8* %56, i32 %mul29
  store i8* %add.ptr30, i8** %dst828, align 4, !tbaa !2
  %59 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool31 = icmp ne i32 %59, 0
  br i1 %tobool31, label %cond.true32, label %cond.false34

cond.true32:                                      ; preds = %if.end27
  %60 = load i8*, i8** %dst828, align 4, !tbaa !2
  %61 = ptrtoint i8* %60 to i32
  %shl33 = shl i32 %61, 1
  %62 = inttoptr i32 %shl33 to i16*
  %63 = bitcast i16* %62 to i8*
  br label %cond.end35

cond.false34:                                     ; preds = %if.end27
  %64 = load i8*, i8** %dst828, align 4, !tbaa !2
  br label %cond.end35

cond.end35:                                       ; preds = %cond.false34, %cond.true32
  %cond36 = phi i8* [ %63, %cond.true32 ], [ %64, %cond.false34 ]
  %65 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %tmp_save_below = getelementptr inbounds %struct.RestorationLineBuffers, %struct.RestorationLineBuffers* %65, i32 0, i32 1
  %66 = load i32, i32* %i18, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [3 x [392 x i16]], [3 x [392 x i16]]* %tmp_save_below, i32 0, i32 %66
  %arraydecay38 = getelementptr inbounds [392 x i16], [392 x i16]* %arrayidx37, i32 0, i32 0
  %67 = bitcast i16* %arraydecay38 to i8*
  %68 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %cond36, i8* align 2 %67, i32 %68, i1 false)
  %69 = bitcast i8** %dst828 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #5
  br label %for.inc39

for.inc39:                                        ; preds = %cond.end35
  %70 = load i32, i32* %i18, align 4, !tbaa !6
  %inc40 = add nsw i32 %70, 1
  store i32 %inc40, i32* %i18, align 4, !tbaa !6
  br label %for.cond19

cleanup:                                          ; preds = %if.then26, %for.cond.cleanup21
  %71 = bitcast i32* %i18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #5
  br label %for.end41

for.end41:                                        ; preds = %cleanup
  %72 = bitcast i8** %data8_bl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #5
  %73 = bitcast i32* %stripe_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #5
  br label %if.end42

if.end42:                                         ; preds = %for.end41, %if.end
  br label %if.end91

if.else:                                          ; preds = %entry
  %74 = load i32, i32* %copy_above.addr, align 4, !tbaa !6
  %tobool43 = icmp ne i32 %74, 0
  br i1 %tobool43, label %if.then44, label %if.end62

if.then44:                                        ; preds = %if.else
  %75 = bitcast i8** %data8_tl45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #5
  %76 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %77 = load i32, i32* %data_x0, align 4, !tbaa !6
  %add.ptr46 = getelementptr inbounds i8, i8* %76, i32 %77
  %78 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start47 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %78, i32 0, i32 2
  %79 = load i32, i32* %v_start47, align 4, !tbaa !38
  %80 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul48 = mul nsw i32 %79, %80
  %add.ptr49 = getelementptr inbounds i8, i8* %add.ptr46, i32 %mul48
  store i8* %add.ptr49, i8** %data8_tl45, align 4, !tbaa !2
  %81 = bitcast i8** %dst850 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #5
  %82 = load i8*, i8** %data8_tl45, align 4, !tbaa !2
  %83 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul51 = mul nsw i32 -3, %83
  %add.ptr52 = getelementptr inbounds i8, i8* %82, i32 %mul51
  store i8* %add.ptr52, i8** %dst850, align 4, !tbaa !2
  %84 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool53 = icmp ne i32 %84, 0
  br i1 %tobool53, label %cond.true54, label %cond.false56

cond.true54:                                      ; preds = %if.then44
  %85 = load i8*, i8** %dst850, align 4, !tbaa !2
  %86 = ptrtoint i8* %85 to i32
  %shl55 = shl i32 %86, 1
  %87 = inttoptr i32 %shl55 to i16*
  %88 = bitcast i16* %87 to i8*
  br label %cond.end57

cond.false56:                                     ; preds = %if.then44
  %89 = load i8*, i8** %dst850, align 4, !tbaa !2
  br label %cond.end57

cond.end57:                                       ; preds = %cond.false56, %cond.true54
  %cond58 = phi i8* [ %88, %cond.true54 ], [ %89, %cond.false56 ]
  %90 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %tmp_save_above59 = getelementptr inbounds %struct.RestorationLineBuffers, %struct.RestorationLineBuffers* %90, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [3 x [392 x i16]], [3 x [392 x i16]]* %tmp_save_above59, i32 0, i32 0
  %arraydecay61 = getelementptr inbounds [392 x i16], [392 x i16]* %arrayidx60, i32 0, i32 0
  %91 = bitcast i16* %arraydecay61 to i8*
  %92 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %cond58, i8* align 2 %91, i32 %92, i1 false)
  %93 = bitcast i8** %dst850 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #5
  %94 = bitcast i8** %data8_tl45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  br label %if.end62

if.end62:                                         ; preds = %cond.end57, %if.else
  %95 = load i32, i32* %copy_below.addr, align 4, !tbaa !6
  %tobool63 = icmp ne i32 %95, 0
  br i1 %tobool63, label %if.then64, label %if.end90

if.then64:                                        ; preds = %if.end62
  %96 = bitcast i32* %stripe_bottom65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #5
  %97 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_start66 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %97, i32 0, i32 2
  %98 = load i32, i32* %v_start66, align 4, !tbaa !38
  %99 = load i32, i32* %h.addr, align 4, !tbaa !6
  %add67 = add nsw i32 %98, %99
  store i32 %add67, i32* %stripe_bottom65, align 4, !tbaa !6
  %100 = bitcast i8** %data8_bl68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #5
  %101 = load i8*, i8** %data8.addr, align 4, !tbaa !2
  %102 = load i32, i32* %data_x0, align 4, !tbaa !6
  %add.ptr69 = getelementptr inbounds i8, i8* %101, i32 %102
  %103 = load i32, i32* %stripe_bottom65, align 4, !tbaa !6
  %104 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul70 = mul nsw i32 %103, %104
  %add.ptr71 = getelementptr inbounds i8, i8* %add.ptr69, i32 %mul70
  store i8* %add.ptr71, i8** %data8_bl68, align 4, !tbaa !2
  %105 = load i32, i32* %stripe_bottom65, align 4, !tbaa !6
  %add72 = add nsw i32 %105, 2
  %106 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %v_end73 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %106, i32 0, i32 3
  %107 = load i32, i32* %v_end73, align 4, !tbaa !35
  %add74 = add nsw i32 %107, 3
  %cmp75 = icmp slt i32 %add72, %add74
  br i1 %cmp75, label %if.then76, label %if.end89

if.then76:                                        ; preds = %if.then64
  %108 = bitcast i8** %dst877 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #5
  %109 = load i8*, i8** %data8_bl68, align 4, !tbaa !2
  %110 = load i32, i32* %data_stride.addr, align 4, !tbaa !6
  %mul78 = mul nsw i32 2, %110
  %add.ptr79 = getelementptr inbounds i8, i8* %109, i32 %mul78
  store i8* %add.ptr79, i8** %dst877, align 4, !tbaa !2
  %111 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool80 = icmp ne i32 %111, 0
  br i1 %tobool80, label %cond.true81, label %cond.false83

cond.true81:                                      ; preds = %if.then76
  %112 = load i8*, i8** %dst877, align 4, !tbaa !2
  %113 = ptrtoint i8* %112 to i32
  %shl82 = shl i32 %113, 1
  %114 = inttoptr i32 %shl82 to i16*
  %115 = bitcast i16* %114 to i8*
  br label %cond.end84

cond.false83:                                     ; preds = %if.then76
  %116 = load i8*, i8** %dst877, align 4, !tbaa !2
  br label %cond.end84

cond.end84:                                       ; preds = %cond.false83, %cond.true81
  %cond85 = phi i8* [ %115, %cond.true81 ], [ %116, %cond.false83 ]
  %117 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %tmp_save_below86 = getelementptr inbounds %struct.RestorationLineBuffers, %struct.RestorationLineBuffers* %117, i32 0, i32 1
  %arrayidx87 = getelementptr inbounds [3 x [392 x i16]], [3 x [392 x i16]]* %tmp_save_below86, i32 0, i32 2
  %arraydecay88 = getelementptr inbounds [392 x i16], [392 x i16]* %arrayidx87, i32 0, i32 0
  %118 = bitcast i16* %arraydecay88 to i8*
  %119 = load i32, i32* %line_size, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %cond85, i8* align 2 %118, i32 %119, i1 false)
  %120 = bitcast i8** %dst877 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  br label %if.end89

if.end89:                                         ; preds = %cond.end84, %if.then64
  %121 = bitcast i8** %data8_bl68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  %122 = bitcast i32* %stripe_bottom65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  br label %if.end90

if.end90:                                         ; preds = %if.end89, %if.end62
  br label %if.end91

if.end91:                                         ; preds = %if.end90, %if.end42
  %123 = bitcast i32* %data_x0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  %124 = bitcast i32* %line_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  %125 = bitcast i32* %line_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_loop_restoration_filter_frame_init(%struct.AV1LrStruct* %lr_ctxt, %struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, i32 %optimized_lr, i32 %num_planes) #0 {
entry:
  %lr_ctxt.addr = alloca %struct.AV1LrStruct*, align 4
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %optimized_lr.addr = alloca i32, align 4
  %num_planes.addr = alloca i32, align 4
  %seq_params = alloca %struct.SequenceHeader*, align 4
  %bit_depth = alloca i32, align 4
  %highbd = alloca i32, align 4
  %frame_width = alloca i32, align 4
  %frame_height = alloca i32, align 4
  %plane = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %rsi = alloca %struct.RestorationInfo*, align 4
  %rtype = alloca i8, align 1
  %is_uv = alloca i32, align 4
  %plane_width = alloca i32, align 4
  %plane_height = alloca i32, align 4
  %lr_plane_ctxt = alloca %struct.FilterFrameCtxt*, align 4
  %tmp = alloca %struct.AV1PixelRect, align 4
  store %struct.AV1LrStruct* %lr_ctxt, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %optimized_lr, i32* %optimized_lr.addr, align 4, !tbaa !6
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = bitcast %struct.SequenceHeader** %seq_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 37
  store %struct.SequenceHeader* %seq_params1, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %2 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %bit_depth2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %3, i32 0, i32 25
  %4 = load i32, i32* %bit_depth2, align 8, !tbaa !56
  store i32 %4, i32* %bit_depth, align 4, !tbaa !6
  %5 = bitcast i32* %highbd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %use_highbitdepth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %6, i32 0, i32 26
  %7 = load i8, i8* %use_highbitdepth, align 4, !tbaa !57
  %conv = zext i8 %7 to i32
  store i32 %conv, i32* %highbd, align 4, !tbaa !6
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 32
  %9 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %9, i32 0, i32 3
  store %struct.yv12_buffer_config* %rst_frame, %struct.yv12_buffer_config** %dst, align 4, !tbaa !58
  %10 = bitcast i32* %frame_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %12 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 2
  %crop_widths = bitcast %union.anon.2* %12 to [2 x i32]*
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths, i32 0, i32 0
  %13 = load i32, i32* %arrayidx, align 4, !tbaa !47
  store i32 %13, i32* %frame_width, align 4, !tbaa !6
  %14 = bitcast i32* %frame_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %16 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %15, i32 0, i32 3
  %crop_heights = bitcast %union.anon.4* %16 to [2 x i32]*
  %arrayidx3 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights, i32 0, i32 0
  %17 = load i32, i32* %arrayidx3, align 4, !tbaa !47
  store i32 %17, i32* %frame_height, align 4, !tbaa !6
  %18 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %dst4 = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %18, i32 0, i32 3
  %19 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst4, align 4, !tbaa !58
  %20 = load i32, i32* %frame_width, align 4, !tbaa !6
  %21 = load i32, i32* %frame_height, align 4, !tbaa !6
  %22 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %22, i32 0, i32 32
  %23 = load i32, i32* %subsampling_x, align 8, !tbaa !60
  %24 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %24, i32 0, i32 33
  %25 = load i32, i32* %subsampling_y, align 4, !tbaa !61
  %26 = load i32, i32* %highbd, align 4, !tbaa !6
  %27 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %27, i32 0, i32 21
  %byte_alignment = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 15
  %28 = load i32, i32* %byte_alignment, align 4, !tbaa !62
  %call = call i32 @aom_realloc_frame_buffer(%struct.yv12_buffer_config* %19, i32 %20, i32 %21, i32 %23, i32 %25, i32 %26, i32 32, i32 %28, %struct.aom_codec_frame_buffer* null, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* null, i8* null)
  %cmp = icmp slt i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %29 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %29, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 2, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %30 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %on_rest_unit = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %30, i32 0, i32 0
  store void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* @filter_frame_on_unit, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)** %on_rest_unit, align 4, !tbaa !63
  %31 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %32 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %frame6 = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %32, i32 0, i32 2
  store %struct.yv12_buffer_config* %31, %struct.yv12_buffer_config** %frame6, align 4, !tbaa !64
  %33 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #5
  store i32 0, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %34 = load i32, i32* %plane, align 4, !tbaa !6
  %35 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %34, %35
  br i1 %cmp7, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %37 = bitcast %struct.RestorationInfo** %rsi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #5
  %38 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %38, i32 0, i32 29
  %39 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %39
  store %struct.RestorationInfo* %arrayidx9, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %rtype) #5
  %40 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %frame_restoration_type = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %40, i32 0, i32 0
  %41 = load i8, i8* %frame_restoration_type, align 4, !tbaa !65
  store i8 %41, i8* %rtype, align 1, !tbaa !47
  %42 = load i32, i32* %optimized_lr.addr, align 4, !tbaa !6
  %43 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %optimized_lr10 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %43, i32 0, i32 7
  store i32 %42, i32* %optimized_lr10, align 4, !tbaa !66
  %44 = load i8, i8* %rtype, align 1, !tbaa !47
  %conv11 = zext i8 %44 to i32
  %cmp12 = icmp eq i32 %conv11, 0
  br i1 %cmp12, label %if.then14, label %if.end15

if.then14:                                        ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %for.body
  %45 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #5
  %46 = load i32, i32* %plane, align 4, !tbaa !6
  %cmp16 = icmp sgt i32 %46, 0
  %conv17 = zext i1 %cmp16 to i32
  store i32 %conv17, i32* %is_uv, align 4, !tbaa !6
  %47 = bitcast i32* %plane_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %49 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 2
  %crop_widths18 = bitcast %union.anon.2* %49 to [2 x i32]*
  %50 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths18, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx19, align 4, !tbaa !47
  store i32 %51, i32* %plane_width, align 4, !tbaa !6
  %52 = bitcast i32* %plane_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #5
  %53 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %54 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %53, i32 0, i32 3
  %crop_heights20 = bitcast %union.anon.4* %54 to [2 x i32]*
  %55 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights20, i32 0, i32 %55
  %56 = load i32, i32* %arrayidx21, align 4, !tbaa !47
  store i32 %56, i32* %plane_height, align 4, !tbaa !6
  %57 = bitcast %struct.FilterFrameCtxt** %lr_plane_ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #5
  %58 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %ctxt = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %58, i32 0, i32 1
  %59 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [3 x %struct.FilterFrameCtxt], [3 x %struct.FilterFrameCtxt]* %ctxt, i32 0, i32 %59
  store %struct.FilterFrameCtxt* %arrayidx22, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %60 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %61 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %60, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %61 to [3 x i8*]*
  %62 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %62
  %63 = load i8*, i8** %arrayidx23, align 4, !tbaa !47
  %64 = load i32, i32* %plane_width, align 4, !tbaa !6
  %65 = load i32, i32* %plane_height, align 4, !tbaa !6
  %66 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %67 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %66, i32 0, i32 4
  %strides = bitcast %union.anon.6* %67 to [2 x i32]*
  %68 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %68
  %69 = load i32, i32* %arrayidx24, align 4, !tbaa !47
  %70 = load i32, i32* %highbd, align 4, !tbaa !6
  call void @av1_extend_frame(i8* %63, i32 %64, i32 %65, i32 %69, i32 3, i32 3, i32 %70)
  %71 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %72 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %rsi25 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %72, i32 0, i32 0
  store %struct.RestorationInfo* %71, %struct.RestorationInfo** %rsi25, align 4, !tbaa !67
  %73 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool = icmp ne i32 %73, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end15
  %74 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %subsampling_x26 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %74, i32 0, i32 32
  %75 = load i32, i32* %subsampling_x26, align 8, !tbaa !60
  %tobool27 = icmp ne i32 %75, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end15
  %76 = phi i1 [ false, %if.end15 ], [ %tobool27, %land.rhs ]
  %land.ext = zext i1 %76 to i32
  %77 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %ss_x = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %77, i32 0, i32 2
  store i32 %land.ext, i32* %ss_x, align 4, !tbaa !69
  %78 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool28 = icmp ne i32 %78, 0
  br i1 %tobool28, label %land.rhs29, label %land.end32

land.rhs29:                                       ; preds = %land.end
  %79 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %subsampling_y30 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %79, i32 0, i32 33
  %80 = load i32, i32* %subsampling_y30, align 4, !tbaa !61
  %tobool31 = icmp ne i32 %80, 0
  br label %land.end32

land.end32:                                       ; preds = %land.rhs29, %land.end
  %81 = phi i1 [ false, %land.end ], [ %tobool31, %land.rhs29 ]
  %land.ext33 = zext i1 %81 to i32
  %82 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %ss_y = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %82, i32 0, i32 3
  store i32 %land.ext33, i32* %ss_y, align 4, !tbaa !70
  %83 = load i32, i32* %highbd, align 4, !tbaa !6
  %84 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %highbd34 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %84, i32 0, i32 4
  store i32 %83, i32* %highbd34, align 4, !tbaa !71
  %85 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %86 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %bit_depth35 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %86, i32 0, i32 5
  store i32 %85, i32* %bit_depth35, align 4, !tbaa !72
  %87 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %88 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %87, i32 0, i32 5
  %buffers36 = bitcast %union.anon.8* %88 to [3 x i8*]*
  %89 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers36, i32 0, i32 %89
  %90 = load i8*, i8** %arrayidx37, align 4, !tbaa !47
  %91 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %data8 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %91, i32 0, i32 6
  store i8* %90, i8** %data8, align 4, !tbaa !73
  %92 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %dst38 = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %92, i32 0, i32 3
  %93 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst38, align 4, !tbaa !58
  %94 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %93, i32 0, i32 5
  %buffers39 = bitcast %union.anon.8* %94 to [3 x i8*]*
  %95 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers39, i32 0, i32 %95
  %96 = load i8*, i8** %arrayidx40, align 4, !tbaa !47
  %97 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %dst8 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %97, i32 0, i32 7
  store i8* %96, i8** %dst8, align 4, !tbaa !74
  %98 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %99 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %98, i32 0, i32 4
  %strides41 = bitcast %union.anon.6* %99 to [2 x i32]*
  %100 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds [2 x i32], [2 x i32]* %strides41, i32 0, i32 %100
  %101 = load i32, i32* %arrayidx42, align 4, !tbaa !47
  %102 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %data_stride = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %102, i32 0, i32 8
  store i32 %101, i32* %data_stride, align 4, !tbaa !75
  %103 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %dst43 = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %103, i32 0, i32 3
  %104 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst43, align 4, !tbaa !58
  %105 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %104, i32 0, i32 4
  %strides44 = bitcast %union.anon.6* %105 to [2 x i32]*
  %106 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds [2 x i32], [2 x i32]* %strides44, i32 0, i32 %106
  %107 = load i32, i32* %arrayidx45, align 4, !tbaa !47
  %108 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %dst_stride = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %108, i32 0, i32 9
  store i32 %107, i32* %dst_stride, align 4, !tbaa !76
  %109 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %tile_rect = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %109, i32 0, i32 10
  %110 = bitcast %struct.AV1PixelRect* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %110) #5
  %111 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %112 = load i32, i32* %is_uv, align 4, !tbaa !6
  call void @av1_whole_frame_rect(%struct.AV1PixelRect* sret align 4 %tmp, %struct.AV1Common* %111, i32 %112)
  %113 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  %114 = bitcast %struct.AV1PixelRect* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %113, i8* align 4 %114, i32 16, i1 false), !tbaa.struct !52
  %115 = bitcast %struct.AV1PixelRect* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %115) #5
  %116 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %lr_plane_ctxt, align 4, !tbaa !2
  %tile_stripe0 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %116, i32 0, i32 1
  store i32 0, i32* %tile_stripe0, align 4, !tbaa !77
  %117 = bitcast %struct.FilterFrameCtxt** %lr_plane_ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast i32* %plane_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %plane_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %land.end32, %if.then14
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %rtype) #5
  %121 = bitcast %struct.RestorationInfo** %rsi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %122 = load i32, i32* %plane, align 4, !tbaa !6
  %inc = add nsw i32 %122, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %123 = bitcast i32* %frame_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  %124 = bitcast i32* %frame_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  %125 = bitcast i32* %highbd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #5
  %126 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #5
  %127 = bitcast %struct.SequenceHeader** %seq_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #5
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare i32 @aom_realloc_frame_buffer(%struct.yv12_buffer_config*, i32, i32, i32, i32, i32, i32, i32, %struct.aom_codec_frame_buffer*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i8*) #2

; Function Attrs: nounwind
define internal void @filter_frame_on_unit(%struct.RestorationTileLimits* %limits, %struct.AV1PixelRect* %tile_rect, i32 %rest_unit_idx, i8* %priv, i32* %tmpbuf, %struct.RestorationLineBuffers* %rlbs) #0 {
entry:
  %limits.addr = alloca %struct.RestorationTileLimits*, align 4
  %tile_rect.addr = alloca %struct.AV1PixelRect*, align 4
  %rest_unit_idx.addr = alloca i32, align 4
  %priv.addr = alloca i8*, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %rlbs.addr = alloca %struct.RestorationLineBuffers*, align 4
  %ctxt = alloca %struct.FilterFrameCtxt*, align 4
  %rsi = alloca %struct.RestorationInfo*, align 4
  store %struct.RestorationTileLimits* %limits, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  store %struct.AV1PixelRect* %tile_rect, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  store i32 %rest_unit_idx, i32* %rest_unit_idx.addr, align 4, !tbaa !6
  store i8* %priv, i8** %priv.addr, align 4, !tbaa !2
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store %struct.RestorationLineBuffers* %rlbs, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %0 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %priv.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.FilterFrameCtxt*
  store %struct.FilterFrameCtxt* %2, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %3 = bitcast %struct.RestorationInfo** %rsi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %rsi1 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %4, i32 0, i32 0
  %5 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi1, align 4, !tbaa !67
  store %struct.RestorationInfo* %5, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %6 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %7 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %unit_info = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %7, i32 0, i32 5
  %8 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %unit_info, align 4, !tbaa !45
  %9 = load i32, i32* %rest_unit_idx.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %8, i32 %9
  %10 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %boundaries = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %10, i32 0, i32 6
  %11 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %12 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %13 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %tile_stripe0 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %13, i32 0, i32 1
  %14 = load i32, i32* %tile_stripe0, align 4, !tbaa !77
  %15 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %ss_x = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %15, i32 0, i32 2
  %16 = load i32, i32* %ss_x, align 4, !tbaa !69
  %17 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %ss_y = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %17, i32 0, i32 3
  %18 = load i32, i32* %ss_y, align 4, !tbaa !70
  %19 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %highbd = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %19, i32 0, i32 4
  %20 = load i32, i32* %highbd, align 4, !tbaa !71
  %21 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %bit_depth = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %21, i32 0, i32 5
  %22 = load i32, i32* %bit_depth, align 4, !tbaa !72
  %23 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %data8 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %23, i32 0, i32 6
  %24 = load i8*, i8** %data8, align 4, !tbaa !73
  %25 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %data_stride = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %25, i32 0, i32 8
  %26 = load i32, i32* %data_stride, align 4, !tbaa !75
  %27 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %dst8 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %27, i32 0, i32 7
  %28 = load i8*, i8** %dst8, align 4, !tbaa !74
  %29 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %dst_stride = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %29, i32 0, i32 9
  %30 = load i32, i32* %dst_stride, align 4, !tbaa !76
  %31 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  %32 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %optimized_lr = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %32, i32 0, i32 7
  %33 = load i32, i32* %optimized_lr, align 4, !tbaa !66
  call void @av1_loop_restoration_filter_unit(%struct.RestorationTileLimits* %6, %struct.RestorationUnitInfo* %arrayidx, %struct.RestorationStripeBoundaries* %boundaries, %struct.RestorationLineBuffers* %11, %struct.AV1PixelRect* %12, i32 %14, i32 %16, i32 %18, i32 %20, i32 %22, i8* %24, i32 %26, i8* %28, i32 %30, i32* %31, i32 %33)
  %34 = bitcast %struct.RestorationInfo** %rsi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  %35 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_loop_restoration_copy_planes(%struct.AV1LrStruct* %loop_rest_ctxt, %struct.AV1Common* %cm, i32 %num_planes) #0 {
entry:
  %loop_rest_ctxt.addr = alloca %struct.AV1LrStruct*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %num_planes.addr = alloca i32, align 4
  %plane = alloca i32, align 4
  %tile_rect = alloca %struct.AV1PixelRect, align 4
  store %struct.AV1LrStruct* %loop_rest_ctxt, %struct.AV1LrStruct** %loop_rest_ctxt.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %plane, align 4, !tbaa !6
  %2 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 29
  %5 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %5
  %frame_restoration_type = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx, i32 0, i32 0
  %6 = load i8, i8* %frame_restoration_type, align 4, !tbaa !65
  %conv = zext i8 %6 to i32
  %cmp1 = icmp eq i32 %conv, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %7 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #5
  %8 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %loop_rest_ctxt.addr, align 4, !tbaa !2
  %ctxt = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %8, i32 0, i32 1
  %9 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [3 x %struct.FilterFrameCtxt], [3 x %struct.FilterFrameCtxt]* %ctxt, i32 0, i32 %9
  %tile_rect4 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx3, i32 0, i32 10
  %10 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  %11 = bitcast %struct.AV1PixelRect* %tile_rect4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !52
  %12 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [3 x void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)*], [3 x void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)*]* @av1_loop_restoration_copy_planes.copy_funs, i32 0, i32 %12
  %13 = load void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)*, void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)** %arrayidx5, align 4, !tbaa !2
  %14 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %loop_rest_ctxt.addr, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %14, i32 0, i32 3
  %15 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst, align 4, !tbaa !58
  %16 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %loop_rest_ctxt.addr, align 4, !tbaa !2
  %frame = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %16, i32 0, i32 2
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame, align 4, !tbaa !64
  %left = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 0
  %18 = load i32, i32* %left, align 4, !tbaa !36
  %right = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 2
  %19 = load i32, i32* %right, align 4, !tbaa !38
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %20 = load i32, i32* %top, align 4, !tbaa !33
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %21 = load i32, i32* %bottom, align 4, !tbaa !35
  call void %13(%struct.yv12_buffer_config* %15, %struct.yv12_buffer_config* %17, i32 %18, i32 %19, i32 %20, i32 %21)
  %22 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end, %if.then
  %23 = load i32, i32* %plane, align 4, !tbaa !6
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @aom_yv12_partial_coloc_copy_y_c(%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32) #2

declare void @aom_yv12_partial_coloc_copy_u_c(%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32) #2

declare void @aom_yv12_partial_coloc_copy_v_c(%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32) #2

; Function Attrs: nounwind
define hidden void @av1_loop_restoration_filter_frame(%struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, i32 %optimized_lr, i8* %lr_ctxt) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %optimized_lr.addr = alloca i32, align 4
  %lr_ctxt.addr = alloca i8*, align 4
  %num_planes = alloca i32, align 4
  %loop_rest_ctxt = alloca %struct.AV1LrStruct*, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %optimized_lr, i32* %optimized_lr.addr, align 4, !tbaa !6
  store i8* %lr_ctxt, i8** %lr_ctxt.addr, align 4, !tbaa !2
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !6
  %2 = bitcast %struct.AV1LrStruct** %loop_rest_ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i8*, i8** %lr_ctxt.addr, align 4, !tbaa !2
  %4 = bitcast i8* %3 to %struct.AV1LrStruct*
  store %struct.AV1LrStruct* %4, %struct.AV1LrStruct** %loop_rest_ctxt, align 4, !tbaa !2
  %5 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %loop_rest_ctxt, align 4, !tbaa !2
  %6 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %8 = load i32, i32* %optimized_lr.addr, align 4, !tbaa !6
  %9 = load i32, i32* %num_planes, align 4, !tbaa !6
  call void @av1_loop_restoration_filter_frame_init(%struct.AV1LrStruct* %5, %struct.yv12_buffer_config* %6, %struct.AV1Common* %7, i32 %8, i32 %9)
  %10 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %loop_rest_ctxt, align 4, !tbaa !2
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %12 = load i32, i32* %num_planes, align 4, !tbaa !6
  call void @foreach_rest_unit_in_planes(%struct.AV1LrStruct* %10, %struct.AV1Common* %11, i32 %12)
  %13 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %loop_rest_ctxt, align 4, !tbaa !2
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %15 = load i32, i32* %num_planes, align 4, !tbaa !6
  call void @av1_loop_restoration_copy_planes(%struct.AV1LrStruct* %13, %struct.AV1Common* %14, i32 %15)
  %16 = bitcast %struct.AV1LrStruct** %loop_rest_ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_num_planes(%struct.AV1Common* %cm) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %1 = load i8, i8* %monochrome, align 1, !tbaa !78
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 3
  ret i32 %cond
}

; Function Attrs: nounwind
define internal void @foreach_rest_unit_in_planes(%struct.AV1LrStruct* %lr_ctxt, %struct.AV1Common* %cm, i32 %num_planes) #0 {
entry:
  %lr_ctxt.addr = alloca %struct.AV1LrStruct*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %num_planes.addr = alloca i32, align 4
  %ctxt = alloca %struct.FilterFrameCtxt*, align 4
  %plane = alloca i32, align 4
  store %struct.AV1LrStruct* %lr_ctxt, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %ctxt1 = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [3 x %struct.FilterFrameCtxt], [3 x %struct.FilterFrameCtxt]* %ctxt1, i32 0, i32 0
  store %struct.FilterFrameCtxt* %arraydecay, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %2 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %plane, align 4, !tbaa !6
  %4 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 29
  %7 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %7
  %frame_restoration_type = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx, i32 0, i32 0
  %8 = load i8, i8* %frame_restoration_type, align 4, !tbaa !65
  %conv = zext i8 %8 to i32
  %cmp2 = icmp eq i32 %conv, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %10 = load i32, i32* %plane, align 4, !tbaa !6
  %11 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %on_rest_unit = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %11, i32 0, i32 0
  %12 = load void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)** %on_rest_unit, align 4, !tbaa !63
  %13 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %14 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %13, i32 %14
  %15 = bitcast %struct.FilterFrameCtxt* %arrayidx4 to i8*
  %16 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %17 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %16, i32 %17
  %tile_rect = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx5, i32 0, i32 10
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_tmpbuf = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 30
  %19 = load i32*, i32** %rst_tmpbuf, align 8, !tbaa !79
  %20 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rlbs = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %20, i32 0, i32 31
  %21 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs, align 4, !tbaa !80
  call void @av1_foreach_rest_unit_in_plane(%struct.AV1Common* %9, i32 %10, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %12, i8* %15, %struct.AV1PixelRect* %tile_rect, i32* %19, %struct.RestorationLineBuffers* %21)
  br label %for.inc

for.inc:                                          ; preds = %if.end, %if.then
  %22 = load i32, i32* %plane, align 4, !tbaa !6
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %23 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_foreach_rest_unit_in_row(%struct.RestorationTileLimits* %limits, %struct.AV1PixelRect* %tile_rect, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %on_rest_unit, i32 %row_number, i32 %unit_size, i32 %unit_idx0, i32 %hunits_per_tile, i32 %vunits_per_tile, i32 %plane, i8* %priv, i32* %tmpbuf, %struct.RestorationLineBuffers* %rlbs, void (i8*, i32, i32, i32)* %on_sync_read, void (i8*, i32, i32, i32, i32)* %on_sync_write, %struct.AV1LrSyncData* %lr_sync) #0 {
entry:
  %limits.addr = alloca %struct.RestorationTileLimits*, align 4
  %tile_rect.addr = alloca %struct.AV1PixelRect*, align 4
  %on_rest_unit.addr = alloca void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, align 4
  %row_number.addr = alloca i32, align 4
  %unit_size.addr = alloca i32, align 4
  %unit_idx0.addr = alloca i32, align 4
  %hunits_per_tile.addr = alloca i32, align 4
  %vunits_per_tile.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %priv.addr = alloca i8*, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %rlbs.addr = alloca %struct.RestorationLineBuffers*, align 4
  %on_sync_read.addr = alloca void (i8*, i32, i32, i32)*, align 4
  %on_sync_write.addr = alloca void (i8*, i32, i32, i32, i32)*, align 4
  %lr_sync.addr = alloca %struct.AV1LrSyncData*, align 4
  %tile_w = alloca i32, align 4
  %ext_size = alloca i32, align 4
  %x0 = alloca i32, align 4
  %j = alloca i32, align 4
  %remaining_w = alloca i32, align 4
  %w = alloca i32, align 4
  %unit_idx = alloca i32, align 4
  store %struct.RestorationTileLimits* %limits, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  store %struct.AV1PixelRect* %tile_rect, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  store void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %on_rest_unit, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)** %on_rest_unit.addr, align 4, !tbaa !2
  store i32 %row_number, i32* %row_number.addr, align 4, !tbaa !6
  store i32 %unit_size, i32* %unit_size.addr, align 4, !tbaa !6
  store i32 %unit_idx0, i32* %unit_idx0.addr, align 4, !tbaa !6
  store i32 %hunits_per_tile, i32* %hunits_per_tile.addr, align 4, !tbaa !6
  store i32 %vunits_per_tile, i32* %vunits_per_tile.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i8* %priv, i8** %priv.addr, align 4, !tbaa !2
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store %struct.RestorationLineBuffers* %rlbs, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  store void (i8*, i32, i32, i32)* %on_sync_read, void (i8*, i32, i32, i32)** %on_sync_read.addr, align 4, !tbaa !2
  store void (i8*, i32, i32, i32, i32)* %on_sync_write, void (i8*, i32, i32, i32, i32)** %on_sync_write.addr, align 4, !tbaa !2
  store %struct.AV1LrSyncData* %lr_sync, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %0 = bitcast i32* %tile_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %right = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %1, i32 0, i32 2
  %2 = load i32, i32* %right, align 4, !tbaa !38
  %3 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %left = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %3, i32 0, i32 0
  %4 = load i32, i32* %left, align 4, !tbaa !36
  %sub = sub nsw i32 %2, %4
  store i32 %sub, i32* %tile_w, align 4, !tbaa !6
  %5 = bitcast i32* %ext_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %unit_size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %6, 3
  %div = sdiv i32 %mul, 2
  store i32 %div, i32* %ext_size, align 4, !tbaa !6
  %7 = bitcast i32* %x0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  store i32 0, i32* %x0, align 4, !tbaa !6
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %9 = load i32, i32* %x0, align 4, !tbaa !6
  %10 = load i32, i32* %tile_w, align 4, !tbaa !6
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = bitcast i32* %remaining_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %tile_w, align 4, !tbaa !6
  %13 = load i32, i32* %x0, align 4, !tbaa !6
  %sub1 = sub nsw i32 %12, %13
  store i32 %sub1, i32* %remaining_w, align 4, !tbaa !6
  %14 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load i32, i32* %remaining_w, align 4, !tbaa !6
  %16 = load i32, i32* %ext_size, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %15, %16
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %17 = load i32, i32* %remaining_w, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %18 = load i32, i32* %unit_size.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %17, %cond.true ], [ %18, %cond.false ]
  store i32 %cond, i32* %w, align 4, !tbaa !6
  %19 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %left3 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %19, i32 0, i32 0
  %20 = load i32, i32* %left3, align 4, !tbaa !36
  %21 = load i32, i32* %x0, align 4, !tbaa !6
  %add = add nsw i32 %20, %21
  %22 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %22, i32 0, i32 0
  store i32 %add, i32* %h_start, align 4, !tbaa !36
  %23 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %left4 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %23, i32 0, i32 0
  %24 = load i32, i32* %left4, align 4, !tbaa !36
  %25 = load i32, i32* %x0, align 4, !tbaa !6
  %add5 = add nsw i32 %24, %25
  %26 = load i32, i32* %w, align 4, !tbaa !6
  %add6 = add nsw i32 %add5, %26
  %27 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %h_end = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %27, i32 0, i32 1
  store i32 %add6, i32* %h_end, align 4, !tbaa !33
  %28 = bitcast i32* %unit_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  %29 = load i32, i32* %unit_idx0.addr, align 4, !tbaa !6
  %30 = load i32, i32* %row_number.addr, align 4, !tbaa !6
  %31 = load i32, i32* %hunits_per_tile.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 %30, %31
  %add8 = add nsw i32 %29, %mul7
  %32 = load i32, i32* %j, align 4, !tbaa !6
  %add9 = add nsw i32 %add8, %32
  store i32 %add9, i32* %unit_idx, align 4, !tbaa !6
  %33 = load void (i8*, i32, i32, i32)*, void (i8*, i32, i32, i32)** %on_sync_read.addr, align 4, !tbaa !2
  %34 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %35 = bitcast %struct.AV1LrSyncData* %34 to i8*
  %36 = load i32, i32* %row_number.addr, align 4, !tbaa !6
  %37 = load i32, i32* %j, align 4, !tbaa !6
  %38 = load i32, i32* %plane.addr, align 4, !tbaa !6
  call void %33(i8* %35, i32 %36, i32 %37, i32 %38)
  %39 = load i32, i32* %row_number.addr, align 4, !tbaa !6
  %add10 = add nsw i32 %39, 1
  %40 = load i32, i32* %vunits_per_tile.addr, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %add10, %40
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %41 = load void (i8*, i32, i32, i32)*, void (i8*, i32, i32, i32)** %on_sync_read.addr, align 4, !tbaa !2
  %42 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %43 = bitcast %struct.AV1LrSyncData* %42 to i8*
  %44 = load i32, i32* %row_number.addr, align 4, !tbaa !6
  %add12 = add nsw i32 %44, 2
  %45 = load i32, i32* %j, align 4, !tbaa !6
  %46 = load i32, i32* %plane.addr, align 4, !tbaa !6
  call void %41(i8* %43, i32 %add12, i32 %45, i32 %46)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %47 = load void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)** %on_rest_unit.addr, align 4, !tbaa !2
  %48 = load %struct.RestorationTileLimits*, %struct.RestorationTileLimits** %limits.addr, align 4, !tbaa !2
  %49 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %50 = load i32, i32* %unit_idx, align 4, !tbaa !6
  %51 = load i8*, i8** %priv.addr, align 4, !tbaa !2
  %52 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  %53 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  call void %47(%struct.RestorationTileLimits* %48, %struct.AV1PixelRect* %49, i32 %50, i8* %51, i32* %52, %struct.RestorationLineBuffers* %53)
  %54 = load void (i8*, i32, i32, i32, i32)*, void (i8*, i32, i32, i32, i32)** %on_sync_write.addr, align 4, !tbaa !2
  %55 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %56 = bitcast %struct.AV1LrSyncData* %55 to i8*
  %57 = load i32, i32* %row_number.addr, align 4, !tbaa !6
  %58 = load i32, i32* %j, align 4, !tbaa !6
  %59 = load i32, i32* %hunits_per_tile.addr, align 4, !tbaa !6
  %60 = load i32, i32* %plane.addr, align 4, !tbaa !6
  call void %54(i8* %56, i32 %57, i32 %58, i32 %59, i32 %60)
  %61 = load i32, i32* %w, align 4, !tbaa !6
  %62 = load i32, i32* %x0, align 4, !tbaa !6
  %add13 = add nsw i32 %62, %61
  store i32 %add13, i32* %x0, align 4, !tbaa !6
  %63 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %63, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  %64 = bitcast i32* %unit_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  %65 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %66 = bitcast i32* %remaining_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %67 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast i32* %x0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  %69 = bitcast i32* %ext_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #5
  %70 = bitcast i32* %tile_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_lr_sync_read_dummy(i8* %lr_sync, i32 %r, i32 %c, i32 %plane) #0 {
entry:
  %lr_sync.addr = alloca i8*, align 4
  %r.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  store i8* %lr_sync, i8** %lr_sync.addr, align 4, !tbaa !2
  store i32 %r, i32* %r.addr, align 4, !tbaa !6
  store i32 %c, i32* %c.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %lr_sync.addr, align 4, !tbaa !2
  %1 = load i32, i32* %r.addr, align 4, !tbaa !6
  %2 = load i32, i32* %c.addr, align 4, !tbaa !6
  %3 = load i32, i32* %plane.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_lr_sync_write_dummy(i8* %lr_sync, i32 %r, i32 %c, i32 %sb_cols, i32 %plane) #0 {
entry:
  %lr_sync.addr = alloca i8*, align 4
  %r.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  %sb_cols.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  store i8* %lr_sync, i8** %lr_sync.addr, align 4, !tbaa !2
  store i32 %r, i32* %r.addr, align 4, !tbaa !6
  store i32 %c, i32* %c.addr, align 4, !tbaa !6
  store i32 %sb_cols, i32* %sb_cols.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %lr_sync.addr, align 4, !tbaa !2
  %1 = load i32, i32* %r.addr, align 4, !tbaa !6
  %2 = load i32, i32* %c.addr, align 4, !tbaa !6
  %3 = load i32, i32* %sb_cols.addr, align 4, !tbaa !6
  %4 = load i32, i32* %plane.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_foreach_rest_unit_in_plane(%struct.AV1Common* %cm, i32 %plane, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %on_rest_unit, i8* %priv, %struct.AV1PixelRect* %tile_rect, i32* %tmpbuf, %struct.RestorationLineBuffers* %rlbs) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %plane.addr = alloca i32, align 4
  %on_rest_unit.addr = alloca void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, align 4
  %priv.addr = alloca i8*, align 4
  %tile_rect.addr = alloca %struct.AV1PixelRect*, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %rlbs.addr = alloca %struct.RestorationLineBuffers*, align 4
  %is_uv = alloca i32, align 4
  %ss_y = alloca i32, align 4
  %rsi = alloca %struct.RestorationInfo*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %on_rest_unit, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)** %on_rest_unit.addr, align 4, !tbaa !2
  store i8* %priv, i8** %priv.addr, align 4, !tbaa !2
  store %struct.AV1PixelRect* %tile_rect, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store %struct.RestorationLineBuffers* %rlbs, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %0 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %1, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !6
  %2 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 37
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 33
  %5 = load i32, i32* %subsampling_y, align 4, !tbaa !32
  %tobool1 = icmp ne i32 %5, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %tobool1, %land.rhs ]
  %land.ext = zext i1 %6 to i32
  store i32 %land.ext, i32* %ss_y, align 4, !tbaa !6
  %7 = bitcast %struct.RestorationInfo** %rsi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 29
  %9 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %9
  store %struct.RestorationInfo* %arrayidx, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %10 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %11 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %horz_units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %11, i32 0, i32 4
  %12 = load i32, i32* %horz_units_per_tile, align 4, !tbaa !43
  %13 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %vert_units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %13, i32 0, i32 3
  %14 = load i32, i32* %vert_units_per_tile, align 4, !tbaa !44
  %15 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %15, i32 0, i32 2
  %16 = load i32, i32* %units_per_tile, align 4, !tbaa !42
  %17 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %restoration_unit_size = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %17, i32 0, i32 1
  %18 = load i32, i32* %restoration_unit_size, align 4, !tbaa !39
  %19 = load i32, i32* %ss_y, align 4, !tbaa !6
  %20 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %21 = load void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)** %on_rest_unit.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %priv.addr, align 4, !tbaa !2
  %23 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  %24 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  call void @foreach_rest_unit_in_tile(%struct.AV1PixelRect* %10, i32 0, i32 0, i32 1, i32 %12, i32 %14, i32 %16, i32 %18, i32 %19, i32 %20, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %21, i8* %22, i32* %23, %struct.RestorationLineBuffers* %24)
  %25 = bitcast %struct.RestorationInfo** %rsi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  ret void
}

; Function Attrs: nounwind
define internal void @foreach_rest_unit_in_tile(%struct.AV1PixelRect* %tile_rect, i32 %tile_row, i32 %tile_col, i32 %tile_cols, i32 %hunits_per_tile, i32 %vunits_per_tile, i32 %units_per_tile, i32 %unit_size, i32 %ss_y, i32 %plane, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %on_rest_unit, i8* %priv, i32* %tmpbuf, %struct.RestorationLineBuffers* %rlbs) #0 {
entry:
  %tile_rect.addr = alloca %struct.AV1PixelRect*, align 4
  %tile_row.addr = alloca i32, align 4
  %tile_col.addr = alloca i32, align 4
  %tile_cols.addr = alloca i32, align 4
  %hunits_per_tile.addr = alloca i32, align 4
  %vunits_per_tile.addr = alloca i32, align 4
  %units_per_tile.addr = alloca i32, align 4
  %unit_size.addr = alloca i32, align 4
  %ss_y.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %on_rest_unit.addr = alloca void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, align 4
  %priv.addr = alloca i8*, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %rlbs.addr = alloca %struct.RestorationLineBuffers*, align 4
  %tile_h = alloca i32, align 4
  %ext_size = alloca i32, align 4
  %tile_idx = alloca i32, align 4
  %unit_idx0 = alloca i32, align 4
  %y0 = alloca i32, align 4
  %i = alloca i32, align 4
  %remaining_h = alloca i32, align 4
  %h = alloca i32, align 4
  %limits = alloca %struct.RestorationTileLimits, align 4
  %voffset = alloca i32, align 4
  store %struct.AV1PixelRect* %tile_rect, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  store i32 %tile_row, i32* %tile_row.addr, align 4, !tbaa !6
  store i32 %tile_col, i32* %tile_col.addr, align 4, !tbaa !6
  store i32 %tile_cols, i32* %tile_cols.addr, align 4, !tbaa !6
  store i32 %hunits_per_tile, i32* %hunits_per_tile.addr, align 4, !tbaa !6
  store i32 %vunits_per_tile, i32* %vunits_per_tile.addr, align 4, !tbaa !6
  store i32 %units_per_tile, i32* %units_per_tile.addr, align 4, !tbaa !6
  store i32 %unit_size, i32* %unit_size.addr, align 4, !tbaa !6
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %on_rest_unit, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)** %on_rest_unit.addr, align 4, !tbaa !2
  store i8* %priv, i8** %priv.addr, align 4, !tbaa !2
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store %struct.RestorationLineBuffers* %rlbs, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  %0 = bitcast i32* %tile_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %1, i32 0, i32 3
  %2 = load i32, i32* %bottom, align 4, !tbaa !35
  %3 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %3, i32 0, i32 1
  %4 = load i32, i32* %top, align 4, !tbaa !33
  %sub = sub nsw i32 %2, %4
  store i32 %sub, i32* %tile_h, align 4, !tbaa !6
  %5 = bitcast i32* %ext_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %unit_size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %6, 3
  %div = sdiv i32 %mul, 2
  store i32 %div, i32* %ext_size, align 4, !tbaa !6
  %7 = bitcast i32* %tile_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %tile_col.addr, align 4, !tbaa !6
  %9 = load i32, i32* %tile_row.addr, align 4, !tbaa !6
  %10 = load i32, i32* %tile_cols.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 %9, %10
  %add = add nsw i32 %8, %mul1
  store i32 %add, i32* %tile_idx, align 4, !tbaa !6
  %11 = bitcast i32* %unit_idx0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %tile_idx, align 4, !tbaa !6
  %13 = load i32, i32* %units_per_tile.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 %12, %13
  store i32 %mul2, i32* %unit_idx0, align 4, !tbaa !6
  %14 = bitcast i32* %y0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  store i32 0, i32* %y0, align 4, !tbaa !6
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %16 = load i32, i32* %y0, align 4, !tbaa !6
  %17 = load i32, i32* %tile_h, align 4, !tbaa !6
  %cmp = icmp slt i32 %16, %17
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %18 = bitcast i32* %remaining_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load i32, i32* %tile_h, align 4, !tbaa !6
  %20 = load i32, i32* %y0, align 4, !tbaa !6
  %sub3 = sub nsw i32 %19, %20
  store i32 %sub3, i32* %remaining_h, align 4, !tbaa !6
  %21 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load i32, i32* %remaining_h, align 4, !tbaa !6
  %23 = load i32, i32* %ext_size, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %22, %23
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %24 = load i32, i32* %remaining_h, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %25 = load i32, i32* %unit_size.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %24, %cond.true ], [ %25, %cond.false ]
  store i32 %cond, i32* %h, align 4, !tbaa !6
  %26 = bitcast %struct.RestorationTileLimits* %limits to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #5
  %27 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %top5 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %27, i32 0, i32 1
  %28 = load i32, i32* %top5, align 4, !tbaa !33
  %29 = load i32, i32* %y0, align 4, !tbaa !6
  %add6 = add nsw i32 %28, %29
  %v_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  store i32 %add6, i32* %v_start, align 4, !tbaa !38
  %30 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %top7 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %30, i32 0, i32 1
  %31 = load i32, i32* %top7, align 4, !tbaa !33
  %32 = load i32, i32* %y0, align 4, !tbaa !6
  %add8 = add nsw i32 %31, %32
  %33 = load i32, i32* %h, align 4, !tbaa !6
  %add9 = add nsw i32 %add8, %33
  %v_end = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  store i32 %add9, i32* %v_end, align 4, !tbaa !35
  %34 = bitcast i32* %voffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #5
  %35 = load i32, i32* %ss_y.addr, align 4, !tbaa !6
  %shr = ashr i32 8, %35
  store i32 %shr, i32* %voffset, align 4, !tbaa !6
  %36 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %top10 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %36, i32 0, i32 1
  %37 = load i32, i32* %top10, align 4, !tbaa !33
  %v_start11 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  %38 = load i32, i32* %v_start11, align 4, !tbaa !38
  %39 = load i32, i32* %voffset, align 4, !tbaa !6
  %sub12 = sub nsw i32 %38, %39
  %cmp13 = icmp sgt i32 %37, %sub12
  br i1 %cmp13, label %cond.true14, label %cond.false16

cond.true14:                                      ; preds = %cond.end
  %40 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %top15 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %40, i32 0, i32 1
  %41 = load i32, i32* %top15, align 4, !tbaa !33
  br label %cond.end19

cond.false16:                                     ; preds = %cond.end
  %v_start17 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  %42 = load i32, i32* %v_start17, align 4, !tbaa !38
  %43 = load i32, i32* %voffset, align 4, !tbaa !6
  %sub18 = sub nsw i32 %42, %43
  br label %cond.end19

cond.end19:                                       ; preds = %cond.false16, %cond.true14
  %cond20 = phi i32 [ %41, %cond.true14 ], [ %sub18, %cond.false16 ]
  %v_start21 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  store i32 %cond20, i32* %v_start21, align 4, !tbaa !38
  %v_end22 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  %44 = load i32, i32* %v_end22, align 4, !tbaa !35
  %45 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %bottom23 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %45, i32 0, i32 3
  %46 = load i32, i32* %bottom23, align 4, !tbaa !35
  %cmp24 = icmp slt i32 %44, %46
  br i1 %cmp24, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end19
  %47 = load i32, i32* %voffset, align 4, !tbaa !6
  %v_end25 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  %48 = load i32, i32* %v_end25, align 4, !tbaa !35
  %sub26 = sub nsw i32 %48, %47
  store i32 %sub26, i32* %v_end25, align 4, !tbaa !35
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end19
  %49 = load %struct.AV1PixelRect*, %struct.AV1PixelRect** %tile_rect.addr, align 4, !tbaa !2
  %50 = load void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)** %on_rest_unit.addr, align 4, !tbaa !2
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %52 = load i32, i32* %unit_size.addr, align 4, !tbaa !6
  %53 = load i32, i32* %unit_idx0, align 4, !tbaa !6
  %54 = load i32, i32* %hunits_per_tile.addr, align 4, !tbaa !6
  %55 = load i32, i32* %vunits_per_tile.addr, align 4, !tbaa !6
  %56 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %57 = load i8*, i8** %priv.addr, align 4, !tbaa !2
  %58 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  %59 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs.addr, align 4, !tbaa !2
  call void @av1_foreach_rest_unit_in_row(%struct.RestorationTileLimits* %limits, %struct.AV1PixelRect* %49, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %50, i32 %51, i32 %52, i32 %53, i32 %54, i32 %55, i32 %56, i8* %57, i32* %58, %struct.RestorationLineBuffers* %59, void (i8*, i32, i32, i32)* @av1_lr_sync_read_dummy, void (i8*, i32, i32, i32, i32)* @av1_lr_sync_write_dummy, %struct.AV1LrSyncData* null)
  %60 = load i32, i32* %h, align 4, !tbaa !6
  %61 = load i32, i32* %y0, align 4, !tbaa !6
  %add27 = add nsw i32 %61, %60
  store i32 %add27, i32* %y0, align 4, !tbaa !6
  %62 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %62, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  %63 = bitcast i32* %voffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  %64 = bitcast %struct.RestorationTileLimits* %limits to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #5
  %65 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %66 = bitcast i32* %remaining_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %67 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast i32* %y0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  %69 = bitcast i32* %unit_idx0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #5
  %70 = bitcast i32* %tile_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #5
  %71 = bitcast i32* %ext_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #5
  %72 = bitcast i32* %tile_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #5
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_loop_restoration_corners_in_sb(%struct.AV1Common* %cm, i32 %plane, i32 %mi_row, i32 %mi_col, i8 zeroext %bsize, i32* %rcol0, i32* %rcol1, i32* %rrow0, i32* %rrow1) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %plane.addr = alloca i32, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %bsize.addr = alloca i8, align 1
  %rcol0.addr = alloca i32*, align 4
  %rcol1.addr = alloca i32*, align 4
  %rrow0.addr = alloca i32*, align 4
  %rrow1.addr = alloca i32*, align 4
  %is_uv = alloca i32, align 4
  %tile_rect = alloca %struct.AV1PixelRect, align 4
  %tile_w = alloca i32, align 4
  %tile_h = alloca i32, align 4
  %mi_top = alloca i32, align 4
  %mi_left = alloca i32, align 4
  %mi_rel_row0 = alloca i32, align 4
  %mi_rel_col0 = alloca i32, align 4
  %mi_rel_row1 = alloca i32, align 4
  %mi_rel_col1 = alloca i32, align 4
  %rsi = alloca %struct.RestorationInfo*, align 4
  %size = alloca i32, align 4
  %horz_units = alloca i32, align 4
  %vert_units = alloca i32, align 4
  %ss_x = alloca i32, align 4
  %ss_y = alloca i32, align 4
  %mi_size_x = alloca i32, align 4
  %mi_size_y = alloca i32, align 4
  %mi_to_num_x = alloca i32, align 4
  %mi_to_num_y = alloca i32, align 4
  %denom_x = alloca i32, align 4
  %denom_y = alloca i32, align 4
  %rnd_x = alloca i32, align 4
  %rnd_y = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !47
  store i32* %rcol0, i32** %rcol0.addr, align 4, !tbaa !2
  store i32* %rcol1, i32** %rcol1.addr, align 4, !tbaa !2
  store i32* %rrow0, i32** %rrow0.addr, align 4, !tbaa !2
  store i32* %rrow1, i32** %rrow1.addr, align 4, !tbaa !2
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !47
  %conv = zext i8 %0 to i32
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 7
  %2 = load i8, i8* %sb_size, align 4, !tbaa !81
  %conv1 = zext i8 %2 to i32
  %cmp = icmp ne i32 %conv, %conv1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 29
  %4 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %4
  %frame_restoration_type = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx, i32 0, i32 0
  %5 = load i8, i8* %frame_restoration_type, align 4, !tbaa !65
  %conv3 = zext i8 %5 to i32
  %cmp4 = icmp eq i32 %conv3, 0
  br i1 %cmp4, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end
  %6 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp8 = icmp sgt i32 %7, 0
  %conv9 = zext i1 %cmp8 to i32
  store i32 %conv9, i32* %is_uv, align 4, !tbaa !6
  %8 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #5
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %10 = load i32, i32* %is_uv, align 4, !tbaa !6
  call void @av1_whole_frame_rect(%struct.AV1PixelRect* sret align 4 %tile_rect, %struct.AV1Common* %9, i32 %10)
  %11 = bitcast i32* %tile_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %right = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 2
  %12 = load i32, i32* %right, align 4, !tbaa !38
  %left = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 0
  %13 = load i32, i32* %left, align 4, !tbaa !36
  %sub = sub nsw i32 %12, %13
  store i32 %sub, i32* %tile_w, align 4, !tbaa !6
  %14 = bitcast i32* %tile_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %15 = load i32, i32* %bottom, align 4, !tbaa !35
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %16 = load i32, i32* %top, align 4, !tbaa !33
  %sub10 = sub nsw i32 %15, %16
  store i32 %sub10, i32* %tile_h, align 4, !tbaa !6
  %17 = bitcast i32* %mi_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  store i32 0, i32* %mi_top, align 4, !tbaa !6
  %18 = bitcast i32* %mi_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  store i32 0, i32* %mi_left, align 4, !tbaa !6
  %19 = bitcast i32* %mi_rel_row0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %sub11 = sub nsw i32 %20, 0
  store i32 %sub11, i32* %mi_rel_row0, align 4, !tbaa !6
  %21 = bitcast i32* %mi_rel_col0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %sub12 = sub nsw i32 %22, 0
  store i32 %sub12, i32* %mi_rel_col0, align 4, !tbaa !6
  %23 = bitcast i32* %mi_rel_row1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  %24 = load i32, i32* %mi_rel_row0, align 4, !tbaa !6
  %25 = load i8, i8* %bsize.addr, align 1, !tbaa !47
  %idxprom = zext i8 %25 to i32
  %arrayidx13 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom
  %26 = load i8, i8* %arrayidx13, align 1, !tbaa !47
  %conv14 = zext i8 %26 to i32
  %add = add nsw i32 %24, %conv14
  store i32 %add, i32* %mi_rel_row1, align 4, !tbaa !6
  %27 = bitcast i32* %mi_rel_col1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  %28 = load i32, i32* %mi_rel_col0, align 4, !tbaa !6
  %29 = load i8, i8* %bsize.addr, align 1, !tbaa !47
  %idxprom15 = zext i8 %29 to i32
  %arrayidx16 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom15
  %30 = load i8, i8* %arrayidx16, align 1, !tbaa !47
  %conv17 = zext i8 %30 to i32
  %add18 = add nsw i32 %28, %conv17
  store i32 %add18, i32* %mi_rel_col1, align 4, !tbaa !6
  %31 = bitcast %struct.RestorationInfo** %rsi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #5
  %32 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info19 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %32, i32 0, i32 29
  %33 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info19, i32 0, i32 %33
  store %struct.RestorationInfo* %arrayidx20, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %34 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #5
  %35 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !2
  %restoration_unit_size = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %35, i32 0, i32 1
  %36 = load i32, i32* %restoration_unit_size, align 4, !tbaa !39
  store i32 %36, i32* %size, align 4, !tbaa !6
  %37 = bitcast i32* %horz_units to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #5
  %38 = load i32, i32* %size, align 4, !tbaa !6
  %39 = load i32, i32* %tile_w, align 4, !tbaa !6
  %call = call i32 @av1_lr_count_units_in_tile(i32 %38, i32 %39)
  store i32 %call, i32* %horz_units, align 4, !tbaa !6
  %40 = bitcast i32* %vert_units to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  %41 = load i32, i32* %size, align 4, !tbaa !6
  %42 = load i32, i32* %tile_h, align 4, !tbaa !6
  %call21 = call i32 @av1_lr_count_units_in_tile(i32 %41, i32 %42)
  store i32 %call21, i32* %vert_units, align 4, !tbaa !6
  %43 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #5
  %44 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool = icmp ne i32 %44, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end7
  %45 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params22 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %45, i32 0, i32 37
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params22, i32 0, i32 32
  %46 = load i32, i32* %subsampling_x, align 16, !tbaa !8
  %tobool23 = icmp ne i32 %46, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end7
  %47 = phi i1 [ false, %if.end7 ], [ %tobool23, %land.rhs ]
  %land.ext = zext i1 %47 to i32
  store i32 %land.ext, i32* %ss_x, align 4, !tbaa !6
  %48 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #5
  %49 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool24 = icmp ne i32 %49, 0
  br i1 %tobool24, label %land.rhs25, label %land.end28

land.rhs25:                                       ; preds = %land.end
  %50 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params26 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %50, i32 0, i32 37
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params26, i32 0, i32 33
  %51 = load i32, i32* %subsampling_y, align 4, !tbaa !32
  %tobool27 = icmp ne i32 %51, 0
  br label %land.end28

land.end28:                                       ; preds = %land.rhs25, %land.end
  %52 = phi i1 [ false, %land.end ], [ %tobool27, %land.rhs25 ]
  %land.ext29 = zext i1 %52 to i32
  store i32 %land.ext29, i32* %ss_y, align 4, !tbaa !6
  %53 = bitcast i32* %mi_size_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #5
  %54 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shr = ashr i32 4, %54
  store i32 %shr, i32* %mi_size_x, align 4, !tbaa !6
  %55 = bitcast i32* %mi_size_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #5
  %56 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shr30 = ashr i32 4, %56
  store i32 %shr30, i32* %mi_size_y, align 4, !tbaa !6
  %57 = bitcast i32* %mi_to_num_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #5
  %58 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call31 = call i32 @av1_superres_scaled(%struct.AV1Common* %58)
  %tobool32 = icmp ne i32 %call31, 0
  br i1 %tobool32, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.end28
  %59 = load i32, i32* %mi_size_x, align 4, !tbaa !6
  %60 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_scale_denominator = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %60, i32 0, i32 8
  %61 = load i8, i8* %superres_scale_denominator, align 8, !tbaa !82
  %conv33 = zext i8 %61 to i32
  %mul = mul nsw i32 %59, %conv33
  br label %cond.end

cond.false:                                       ; preds = %land.end28
  %62 = load i32, i32* %mi_size_x, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ %62, %cond.false ]
  store i32 %cond, i32* %mi_to_num_x, align 4, !tbaa !6
  %63 = bitcast i32* %mi_to_num_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #5
  %64 = load i32, i32* %mi_size_y, align 4, !tbaa !6
  store i32 %64, i32* %mi_to_num_y, align 4, !tbaa !6
  %65 = bitcast i32* %denom_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #5
  %66 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call34 = call i32 @av1_superres_scaled(%struct.AV1Common* %66)
  %tobool35 = icmp ne i32 %call34, 0
  br i1 %tobool35, label %cond.true36, label %cond.false38

cond.true36:                                      ; preds = %cond.end
  %67 = load i32, i32* %size, align 4, !tbaa !6
  %mul37 = mul nsw i32 %67, 8
  br label %cond.end39

cond.false38:                                     ; preds = %cond.end
  %68 = load i32, i32* %size, align 4, !tbaa !6
  br label %cond.end39

cond.end39:                                       ; preds = %cond.false38, %cond.true36
  %cond40 = phi i32 [ %mul37, %cond.true36 ], [ %68, %cond.false38 ]
  store i32 %cond40, i32* %denom_x, align 4, !tbaa !6
  %69 = bitcast i32* %denom_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #5
  %70 = load i32, i32* %size, align 4, !tbaa !6
  store i32 %70, i32* %denom_y, align 4, !tbaa !6
  %71 = bitcast i32* %rnd_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #5
  %72 = load i32, i32* %denom_x, align 4, !tbaa !6
  %sub41 = sub nsw i32 %72, 1
  store i32 %sub41, i32* %rnd_x, align 4, !tbaa !6
  %73 = bitcast i32* %rnd_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #5
  %74 = load i32, i32* %denom_y, align 4, !tbaa !6
  %sub42 = sub nsw i32 %74, 1
  store i32 %sub42, i32* %rnd_y, align 4, !tbaa !6
  %75 = load i32, i32* %mi_rel_col0, align 4, !tbaa !6
  %76 = load i32, i32* %mi_to_num_x, align 4, !tbaa !6
  %mul43 = mul nsw i32 %75, %76
  %77 = load i32, i32* %rnd_x, align 4, !tbaa !6
  %add44 = add nsw i32 %mul43, %77
  %78 = load i32, i32* %denom_x, align 4, !tbaa !6
  %div = sdiv i32 %add44, %78
  %79 = load i32*, i32** %rcol0.addr, align 4, !tbaa !2
  store i32 %div, i32* %79, align 4, !tbaa !6
  %80 = load i32, i32* %mi_rel_row0, align 4, !tbaa !6
  %81 = load i32, i32* %mi_to_num_y, align 4, !tbaa !6
  %mul45 = mul nsw i32 %80, %81
  %82 = load i32, i32* %rnd_y, align 4, !tbaa !6
  %add46 = add nsw i32 %mul45, %82
  %83 = load i32, i32* %denom_y, align 4, !tbaa !6
  %div47 = sdiv i32 %add46, %83
  %84 = load i32*, i32** %rrow0.addr, align 4, !tbaa !2
  store i32 %div47, i32* %84, align 4, !tbaa !6
  %85 = load i32, i32* %mi_rel_col1, align 4, !tbaa !6
  %86 = load i32, i32* %mi_to_num_x, align 4, !tbaa !6
  %mul48 = mul nsw i32 %85, %86
  %87 = load i32, i32* %rnd_x, align 4, !tbaa !6
  %add49 = add nsw i32 %mul48, %87
  %88 = load i32, i32* %denom_x, align 4, !tbaa !6
  %div50 = sdiv i32 %add49, %88
  %89 = load i32, i32* %horz_units, align 4, !tbaa !6
  %cmp51 = icmp slt i32 %div50, %89
  br i1 %cmp51, label %cond.true53, label %cond.false57

cond.true53:                                      ; preds = %cond.end39
  %90 = load i32, i32* %mi_rel_col1, align 4, !tbaa !6
  %91 = load i32, i32* %mi_to_num_x, align 4, !tbaa !6
  %mul54 = mul nsw i32 %90, %91
  %92 = load i32, i32* %rnd_x, align 4, !tbaa !6
  %add55 = add nsw i32 %mul54, %92
  %93 = load i32, i32* %denom_x, align 4, !tbaa !6
  %div56 = sdiv i32 %add55, %93
  br label %cond.end58

cond.false57:                                     ; preds = %cond.end39
  %94 = load i32, i32* %horz_units, align 4, !tbaa !6
  br label %cond.end58

cond.end58:                                       ; preds = %cond.false57, %cond.true53
  %cond59 = phi i32 [ %div56, %cond.true53 ], [ %94, %cond.false57 ]
  %95 = load i32*, i32** %rcol1.addr, align 4, !tbaa !2
  store i32 %cond59, i32* %95, align 4, !tbaa !6
  %96 = load i32, i32* %mi_rel_row1, align 4, !tbaa !6
  %97 = load i32, i32* %mi_to_num_y, align 4, !tbaa !6
  %mul60 = mul nsw i32 %96, %97
  %98 = load i32, i32* %rnd_y, align 4, !tbaa !6
  %add61 = add nsw i32 %mul60, %98
  %99 = load i32, i32* %denom_y, align 4, !tbaa !6
  %div62 = sdiv i32 %add61, %99
  %100 = load i32, i32* %vert_units, align 4, !tbaa !6
  %cmp63 = icmp slt i32 %div62, %100
  br i1 %cmp63, label %cond.true65, label %cond.false69

cond.true65:                                      ; preds = %cond.end58
  %101 = load i32, i32* %mi_rel_row1, align 4, !tbaa !6
  %102 = load i32, i32* %mi_to_num_y, align 4, !tbaa !6
  %mul66 = mul nsw i32 %101, %102
  %103 = load i32, i32* %rnd_y, align 4, !tbaa !6
  %add67 = add nsw i32 %mul66, %103
  %104 = load i32, i32* %denom_y, align 4, !tbaa !6
  %div68 = sdiv i32 %add67, %104
  br label %cond.end70

cond.false69:                                     ; preds = %cond.end58
  %105 = load i32, i32* %vert_units, align 4, !tbaa !6
  br label %cond.end70

cond.end70:                                       ; preds = %cond.false69, %cond.true65
  %cond71 = phi i32 [ %div68, %cond.true65 ], [ %105, %cond.false69 ]
  %106 = load i32*, i32** %rrow1.addr, align 4, !tbaa !2
  store i32 %cond71, i32* %106, align 4, !tbaa !6
  %107 = load i32*, i32** %rcol0.addr, align 4, !tbaa !2
  %108 = load i32, i32* %107, align 4, !tbaa !6
  %109 = load i32*, i32** %rcol1.addr, align 4, !tbaa !2
  %110 = load i32, i32* %109, align 4, !tbaa !6
  %cmp72 = icmp slt i32 %108, %110
  br i1 %cmp72, label %land.rhs74, label %land.end77

land.rhs74:                                       ; preds = %cond.end70
  %111 = load i32*, i32** %rrow0.addr, align 4, !tbaa !2
  %112 = load i32, i32* %111, align 4, !tbaa !6
  %113 = load i32*, i32** %rrow1.addr, align 4, !tbaa !2
  %114 = load i32, i32* %113, align 4, !tbaa !6
  %cmp75 = icmp slt i32 %112, %114
  br label %land.end77

land.end77:                                       ; preds = %land.rhs74, %cond.end70
  %115 = phi i1 [ false, %cond.end70 ], [ %cmp75, %land.rhs74 ]
  %land.ext78 = zext i1 %115 to i32
  store i32 %land.ext78, i32* %retval, align 4
  %116 = bitcast i32* %rnd_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #5
  %117 = bitcast i32* %rnd_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast i32* %denom_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %denom_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i32* %mi_to_num_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  %121 = bitcast i32* %mi_to_num_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  %122 = bitcast i32* %mi_size_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  %123 = bitcast i32* %mi_size_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  %124 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  %125 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #5
  %126 = bitcast i32* %vert_units to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #5
  %127 = bitcast i32* %horz_units to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #5
  %128 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #5
  %129 = bitcast %struct.RestorationInfo** %rsi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #5
  %130 = bitcast i32* %mi_rel_col1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #5
  %131 = bitcast i32* %mi_rel_row1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #5
  %132 = bitcast i32* %mi_rel_col0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #5
  %133 = bitcast i32* %mi_rel_row0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #5
  %134 = bitcast i32* %mi_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #5
  %135 = bitcast i32* %mi_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #5
  %136 = bitcast i32* %tile_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #5
  %137 = bitcast i32* %tile_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #5
  %138 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %138) #5
  %139 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #5
  br label %return

return:                                           ; preds = %land.end77, %if.then6, %if.then
  %140 = load i32, i32* %retval, align 4
  ret i32 %140
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_superres_scaled(%struct.AV1Common* %cm) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 2
  %1 = load i32, i32* %width, align 16, !tbaa !83
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 6
  %3 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !37
  %cmp = icmp eq i32 %1, %3
  %lnot = xor i1 %cmp, true
  %lnot.ext = zext i1 %lnot to i32
  ret i32 %lnot.ext
}

; Function Attrs: nounwind
define hidden void @av1_loop_restoration_save_boundary_lines(%struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, i32 %after_cdef) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %after_cdef.addr = alloca i32, align 4
  %num_planes = alloca i32, align 4
  %use_highbd = alloca i32, align 4
  %p = alloca i32, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %after_cdef, i32* %after_cdef.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !6
  %2 = bitcast i32* %use_highbd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 37
  %use_highbitdepth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 26
  %4 = load i8, i8* %use_highbitdepth, align 4, !tbaa !84
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %use_highbd, align 4, !tbaa !6
  %5 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  store i32 0, i32* %p, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %p, align 4, !tbaa !6
  %7 = load i32, i32* %num_planes, align 4, !tbaa !6
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %8 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %10 = load i32, i32* %use_highbd, align 4, !tbaa !6
  %11 = load i32, i32* %p, align 4, !tbaa !6
  %12 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %13 = load i32, i32* %after_cdef.addr, align 4, !tbaa !6
  call void @save_tile_row_boundary_lines(%struct.yv12_buffer_config* %9, i32 %10, i32 %11, %struct.AV1Common* %12, i32 %13)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %p, align 4, !tbaa !6
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %p, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %15 = bitcast i32* %use_highbd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %16 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  ret void
}

; Function Attrs: nounwind
define internal void @save_tile_row_boundary_lines(%struct.yv12_buffer_config* %frame, i32 %use_highbd, i32 %plane, %struct.AV1Common* %cm, i32 %after_cdef) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %use_highbd.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %after_cdef.addr = alloca i32, align 4
  %is_uv = alloca i32, align 4
  %ss_y = alloca i32, align 4
  %stripe_height = alloca i32, align 4
  %stripe_off = alloca i32, align 4
  %tile_rect = alloca %struct.AV1PixelRect, align 4
  %stripe0 = alloca i32, align 4
  %boundaries = alloca %struct.RestorationStripeBoundaries*, align 4
  %plane_height = alloca i32, align 4
  %tile_stripe = alloca i32, align 4
  %rel_y0 = alloca i32, align 4
  %y0 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %rel_y1 = alloca i32, align 4
  %y1 = alloca i32, align 4
  %frame_stripe = alloca i32, align 4
  %use_deblock_above = alloca i32, align 4
  %use_deblock_below = alloca i32, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store i32 %use_highbd, i32* %use_highbd.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %after_cdef, i32* %after_cdef.addr, align 4, !tbaa !6
  %0 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %1, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !6
  %2 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 37
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 33
  %5 = load i32, i32* %subsampling_y, align 4, !tbaa !32
  %tobool1 = icmp ne i32 %5, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %tobool1, %land.rhs ]
  %land.ext = zext i1 %6 to i32
  store i32 %land.ext, i32* %ss_y, align 4, !tbaa !6
  %7 = bitcast i32* %stripe_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shr = ashr i32 64, %8
  store i32 %shr, i32* %stripe_height, align 4, !tbaa !6
  %9 = bitcast i32* %stripe_off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shr2 = ashr i32 8, %10
  store i32 %shr2, i32* %stripe_off, align 4, !tbaa !6
  %11 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #5
  %12 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %13 = load i32, i32* %is_uv, align 4, !tbaa !6
  call void @av1_whole_frame_rect(%struct.AV1PixelRect* sret align 4 %tile_rect, %struct.AV1Common* %12, i32 %13)
  %14 = bitcast i32* %stripe0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  store i32 0, i32* %stripe0, align 4, !tbaa !6
  %15 = bitcast %struct.RestorationStripeBoundaries** %boundaries to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %16, i32 0, i32 29
  %17 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %17
  %boundaries3 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx, i32 0, i32 6
  store %struct.RestorationStripeBoundaries* %boundaries3, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !2
  %18 = bitcast i32* %plane_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %19, i32 0, i32 3
  %20 = load i32, i32* %height, align 4, !tbaa !34
  %21 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shl = shl i32 1, %21
  %shr4 = ashr i32 %shl, 1
  %add = add nsw i32 %20, %shr4
  %22 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shr5 = ashr i32 %add, %22
  store i32 %shr5, i32* %plane_height, align 4, !tbaa !6
  %23 = bitcast i32* %tile_stripe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  store i32 0, i32* %tile_stripe, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %land.end
  %24 = bitcast i32* %rel_y0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  %25 = load i32, i32* %tile_stripe, align 4, !tbaa !6
  %26 = load i32, i32* %stripe_height, align 4, !tbaa !6
  %mul = mul nsw i32 %25, %26
  %27 = load i32, i32* %stripe_off, align 4, !tbaa !6
  %sub = sub nsw i32 %mul, %27
  %cmp6 = icmp sgt i32 0, %sub
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.cond
  br label %cond.end

cond.false:                                       ; preds = %for.cond
  %28 = load i32, i32* %tile_stripe, align 4, !tbaa !6
  %29 = load i32, i32* %stripe_height, align 4, !tbaa !6
  %mul8 = mul nsw i32 %28, %29
  %30 = load i32, i32* %stripe_off, align 4, !tbaa !6
  %sub9 = sub nsw i32 %mul8, %30
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %sub9, %cond.false ]
  store i32 %cond, i32* %rel_y0, align 4, !tbaa !6
  %31 = bitcast i32* %y0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #5
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %32 = load i32, i32* %top, align 4, !tbaa !33
  %33 = load i32, i32* %rel_y0, align 4, !tbaa !6
  %add10 = add nsw i32 %32, %33
  store i32 %add10, i32* %y0, align 4, !tbaa !6
  %34 = load i32, i32* %y0, align 4, !tbaa !6
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %35 = load i32, i32* %bottom, align 4, !tbaa !35
  %cmp11 = icmp sge i32 %34, %35
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %cond.end
  %36 = bitcast i32* %rel_y1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #5
  %37 = load i32, i32* %tile_stripe, align 4, !tbaa !6
  %add13 = add nsw i32 %37, 1
  %38 = load i32, i32* %stripe_height, align 4, !tbaa !6
  %mul14 = mul nsw i32 %add13, %38
  %39 = load i32, i32* %stripe_off, align 4, !tbaa !6
  %sub15 = sub nsw i32 %mul14, %39
  store i32 %sub15, i32* %rel_y1, align 4, !tbaa !6
  %40 = bitcast i32* %y1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  %top16 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %41 = load i32, i32* %top16, align 4, !tbaa !33
  %42 = load i32, i32* %rel_y1, align 4, !tbaa !6
  %add17 = add nsw i32 %41, %42
  %bottom18 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %43 = load i32, i32* %bottom18, align 4, !tbaa !35
  %cmp19 = icmp slt i32 %add17, %43
  br i1 %cmp19, label %cond.true21, label %cond.false24

cond.true21:                                      ; preds = %if.end
  %top22 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %44 = load i32, i32* %top22, align 4, !tbaa !33
  %45 = load i32, i32* %rel_y1, align 4, !tbaa !6
  %add23 = add nsw i32 %44, %45
  br label %cond.end26

cond.false24:                                     ; preds = %if.end
  %bottom25 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %46 = load i32, i32* %bottom25, align 4, !tbaa !35
  br label %cond.end26

cond.end26:                                       ; preds = %cond.false24, %cond.true21
  %cond27 = phi i32 [ %add23, %cond.true21 ], [ %46, %cond.false24 ]
  store i32 %cond27, i32* %y1, align 4, !tbaa !6
  %47 = bitcast i32* %frame_stripe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  %48 = load i32, i32* %tile_stripe, align 4, !tbaa !6
  %add28 = add nsw i32 0, %48
  store i32 %add28, i32* %frame_stripe, align 4, !tbaa !6
  %49 = bitcast i32* %use_deblock_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #5
  %50 = load i32, i32* %frame_stripe, align 4, !tbaa !6
  %cmp29 = icmp sgt i32 %50, 0
  %conv30 = zext i1 %cmp29 to i32
  store i32 %conv30, i32* %use_deblock_above, align 4, !tbaa !6
  %51 = bitcast i32* %use_deblock_below to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #5
  %52 = load i32, i32* %y1, align 4, !tbaa !6
  %53 = load i32, i32* %plane_height, align 4, !tbaa !6
  %cmp31 = icmp slt i32 %52, %53
  %conv32 = zext i1 %cmp31 to i32
  store i32 %conv32, i32* %use_deblock_below, align 4, !tbaa !6
  %54 = load i32, i32* %after_cdef.addr, align 4, !tbaa !6
  %tobool33 = icmp ne i32 %54, 0
  br i1 %tobool33, label %if.else, label %if.then34

if.then34:                                        ; preds = %cond.end26
  %55 = load i32, i32* %use_deblock_above, align 4, !tbaa !6
  %tobool35 = icmp ne i32 %55, 0
  br i1 %tobool35, label %if.then36, label %if.end38

if.then36:                                        ; preds = %if.then34
  %56 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %57 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %58 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %59 = load i32, i32* %y0, align 4, !tbaa !6
  %sub37 = sub nsw i32 %59, 2
  %60 = load i32, i32* %frame_stripe, align 4, !tbaa !6
  %61 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %62 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !2
  call void @save_deblock_boundary_lines(%struct.yv12_buffer_config* %56, %struct.AV1Common* %57, i32 %58, i32 %sub37, i32 %60, i32 %61, i32 1, %struct.RestorationStripeBoundaries* %62)
  br label %if.end38

if.end38:                                         ; preds = %if.then36, %if.then34
  %63 = load i32, i32* %use_deblock_below, align 4, !tbaa !6
  %tobool39 = icmp ne i32 %63, 0
  br i1 %tobool39, label %if.then40, label %if.end41

if.then40:                                        ; preds = %if.end38
  %64 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %65 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %66 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %67 = load i32, i32* %y1, align 4, !tbaa !6
  %68 = load i32, i32* %frame_stripe, align 4, !tbaa !6
  %69 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %70 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !2
  call void @save_deblock_boundary_lines(%struct.yv12_buffer_config* %64, %struct.AV1Common* %65, i32 %66, i32 %67, i32 %68, i32 %69, i32 0, %struct.RestorationStripeBoundaries* %70)
  br label %if.end41

if.end41:                                         ; preds = %if.then40, %if.end38
  br label %if.end49

if.else:                                          ; preds = %cond.end26
  %71 = load i32, i32* %use_deblock_above, align 4, !tbaa !6
  %tobool42 = icmp ne i32 %71, 0
  br i1 %tobool42, label %if.end44, label %if.then43

if.then43:                                        ; preds = %if.else
  %72 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %73 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %74 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %75 = load i32, i32* %y0, align 4, !tbaa !6
  %76 = load i32, i32* %frame_stripe, align 4, !tbaa !6
  %77 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %78 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !2
  call void @save_cdef_boundary_lines(%struct.yv12_buffer_config* %72, %struct.AV1Common* %73, i32 %74, i32 %75, i32 %76, i32 %77, i32 1, %struct.RestorationStripeBoundaries* %78)
  br label %if.end44

if.end44:                                         ; preds = %if.then43, %if.else
  %79 = load i32, i32* %use_deblock_below, align 4, !tbaa !6
  %tobool45 = icmp ne i32 %79, 0
  br i1 %tobool45, label %if.end48, label %if.then46

if.then46:                                        ; preds = %if.end44
  %80 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %81 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %82 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %83 = load i32, i32* %y1, align 4, !tbaa !6
  %sub47 = sub nsw i32 %83, 1
  %84 = load i32, i32* %frame_stripe, align 4, !tbaa !6
  %85 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %86 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !2
  call void @save_cdef_boundary_lines(%struct.yv12_buffer_config* %80, %struct.AV1Common* %81, i32 %82, i32 %sub47, i32 %84, i32 %85, i32 0, %struct.RestorationStripeBoundaries* %86)
  br label %if.end48

if.end48:                                         ; preds = %if.then46, %if.end44
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.end41
  %87 = bitcast i32* %use_deblock_below to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #5
  %88 = bitcast i32* %use_deblock_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #5
  %89 = bitcast i32* %frame_stripe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #5
  %90 = bitcast i32* %y1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #5
  %91 = bitcast i32* %rel_y1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end49, %if.then
  %92 = bitcast i32* %y0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #5
  %93 = bitcast i32* %rel_y0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %94 = load i32, i32* %tile_stripe, align 4, !tbaa !6
  %inc = add nsw i32 %94, 1
  store i32 %inc, i32* %tile_stripe, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %cleanup
  %95 = bitcast i32* %tile_stripe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #5
  %96 = bitcast i32* %plane_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #5
  %97 = bitcast %struct.RestorationStripeBoundaries** %boundaries to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #5
  %98 = bitcast i32* %stripe0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #5
  %99 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %99) #5
  %100 = bitcast i32* %stripe_off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #5
  %101 = bitcast i32* %stripe_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #5
  %102 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #5
  %103 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #5
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: nounwind
define internal void @calculate_intermediate_result(i32* %dgd, i32 %width, i32 %height, i32 %dgd_stride, i32 %bit_depth, i32 %sgr_params_idx, i32 %radius_idx, i32 %pass, i32* %A, i32* %B) #0 {
entry:
  %dgd.addr = alloca i32*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %dgd_stride.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %sgr_params_idx.addr = alloca i32, align 4
  %radius_idx.addr = alloca i32, align 4
  %pass.addr = alloca i32, align 4
  %A.addr = alloca i32*, align 4
  %B.addr = alloca i32*, align 4
  %params = alloca %struct.sgr_params_type*, align 4
  %r = alloca i32, align 4
  %width_ext = alloca i32, align 4
  %height_ext = alloca i32, align 4
  %buf_stride = alloca i32, align 4
  %step = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %n = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %p = alloca i32, align 4
  %s = alloca i32, align 4
  %z = alloca i32, align 4
  store i32* %dgd, i32** %dgd.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %dgd_stride, i32* %dgd_stride.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %sgr_params_idx, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  store i32 %radius_idx, i32* %radius_idx.addr, align 4, !tbaa !6
  store i32 %pass, i32* %pass.addr, align 4, !tbaa !6
  store i32* %A, i32** %A.addr, align 4, !tbaa !2
  store i32* %B, i32** %B.addr, align 4, !tbaa !2
  %0 = bitcast %struct.sgr_params_type** %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %sgr_params_idx.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [16 x %struct.sgr_params_type], [16 x %struct.sgr_params_type]* @av1_sgr_params, i32 0, i32 %1
  store %struct.sgr_params_type* %arrayidx, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %2 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %r1 = getelementptr inbounds %struct.sgr_params_type, %struct.sgr_params_type* %3, i32 0, i32 0
  %4 = load i32, i32* %radius_idx.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %r1, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  store i32 %5, i32* %r, align 4, !tbaa !6
  %6 = bitcast i32* %width_ext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add = add nsw i32 %7, 6
  store i32 %add, i32* %width_ext, align 4, !tbaa !6
  %8 = bitcast i32* %height_ext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i32, i32* %height.addr, align 4, !tbaa !6
  %add3 = add nsw i32 %9, 6
  store i32 %add3, i32* %height_ext, align 4, !tbaa !6
  %10 = bitcast i32* %buf_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load i32, i32* %width_ext, align 4, !tbaa !6
  %add4 = add nsw i32 %11, 3
  %and = and i32 %add4, -4
  %add5 = add nsw i32 %and, 16
  store i32 %add5, i32* %buf_stride, align 4, !tbaa !6
  %12 = bitcast i32* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load i32, i32* %pass.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %13, 0
  %14 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 2
  store i32 %cond, i32* %step, align 4, !tbaa !6
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load i32*, i32** %dgd.addr, align 4, !tbaa !2
  %18 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %18, 3
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i32, i32* %17, i32 %idx.neg
  %add.ptr6 = getelementptr inbounds i32, i32* %add.ptr, i32 -3
  %19 = load i32, i32* %width_ext, align 4, !tbaa !6
  %20 = load i32, i32* %height_ext, align 4, !tbaa !6
  %21 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %22 = load i32, i32* %r, align 4, !tbaa !6
  %23 = load i32*, i32** %B.addr, align 4, !tbaa !2
  %24 = load i32, i32* %buf_stride, align 4, !tbaa !6
  call void @boxsum(i32* %add.ptr6, i32 %19, i32 %20, i32 %21, i32 %22, i32 0, i32* %23, i32 %24)
  %25 = load i32*, i32** %dgd.addr, align 4, !tbaa !2
  %26 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 %26, 3
  %idx.neg8 = sub i32 0, %mul7
  %add.ptr9 = getelementptr inbounds i32, i32* %25, i32 %idx.neg8
  %add.ptr10 = getelementptr inbounds i32, i32* %add.ptr9, i32 -3
  %27 = load i32, i32* %width_ext, align 4, !tbaa !6
  %28 = load i32, i32* %height_ext, align 4, !tbaa !6
  %29 = load i32, i32* %dgd_stride.addr, align 4, !tbaa !6
  %30 = load i32, i32* %r, align 4, !tbaa !6
  %31 = load i32*, i32** %A.addr, align 4, !tbaa !2
  %32 = load i32, i32* %buf_stride, align 4, !tbaa !6
  call void @boxsum(i32* %add.ptr10, i32 %27, i32 %28, i32 %29, i32 %30, i32 1, i32* %31, i32 %32)
  %33 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul11 = mul nsw i32 3, %33
  %add12 = add nsw i32 %mul11, 3
  %34 = load i32*, i32** %A.addr, align 4, !tbaa !2
  %add.ptr13 = getelementptr inbounds i32, i32* %34, i32 %add12
  store i32* %add.ptr13, i32** %A.addr, align 4, !tbaa !2
  %35 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul14 = mul nsw i32 3, %35
  %add15 = add nsw i32 %mul14, 3
  %36 = load i32*, i32** %B.addr, align 4, !tbaa !2
  %add.ptr16 = getelementptr inbounds i32, i32* %36, i32 %add15
  store i32* %add.ptr16, i32** %B.addr, align 4, !tbaa !2
  store i32 -1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc72, %entry
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %38 = load i32, i32* %height.addr, align 4, !tbaa !6
  %add17 = add nsw i32 %38, 1
  %cmp18 = icmp slt i32 %37, %add17
  br i1 %cmp18, label %for.body, label %for.end74

for.body:                                         ; preds = %for.cond
  store i32 -1, i32* %j, align 4, !tbaa !6
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc, %for.body
  %39 = load i32, i32* %j, align 4, !tbaa !6
  %40 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add20 = add nsw i32 %40, 1
  %cmp21 = icmp slt i32 %39, %add20
  br i1 %cmp21, label %for.body22, label %for.end

for.body22:                                       ; preds = %for.cond19
  %41 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #5
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %43 = load i32, i32* %buf_stride, align 4, !tbaa !6
  %mul23 = mul nsw i32 %42, %43
  %44 = load i32, i32* %j, align 4, !tbaa !6
  %add24 = add nsw i32 %mul23, %44
  store i32 %add24, i32* %k, align 4, !tbaa !6
  %45 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #5
  %46 = load i32, i32* %r, align 4, !tbaa !6
  %mul25 = mul nsw i32 2, %46
  %add26 = add nsw i32 %mul25, 1
  %47 = load i32, i32* %r, align 4, !tbaa !6
  %mul27 = mul nsw i32 2, %47
  %add28 = add nsw i32 %mul27, 1
  %mul29 = mul nsw i32 %add26, %add28
  store i32 %mul29, i32* %n, align 4, !tbaa !6
  %48 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #5
  %49 = load i32*, i32** %A.addr, align 4, !tbaa !2
  %50 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds i32, i32* %49, i32 %50
  %51 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %52 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %52, 8
  %mul31 = mul nsw i32 2, %sub
  %shl = shl i32 1, %mul31
  %shr = ashr i32 %shl, 1
  %add32 = add nsw i32 %51, %shr
  %53 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub33 = sub nsw i32 %53, 8
  %mul34 = mul nsw i32 2, %sub33
  %shr35 = ashr i32 %add32, %mul34
  store i32 %shr35, i32* %a, align 4, !tbaa !6
  %54 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #5
  %55 = load i32*, i32** %B.addr, align 4, !tbaa !2
  %56 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds i32, i32* %55, i32 %56
  %57 = load i32, i32* %arrayidx36, align 4, !tbaa !6
  %58 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub37 = sub nsw i32 %58, 8
  %shl38 = shl i32 1, %sub37
  %shr39 = ashr i32 %shl38, 1
  %add40 = add nsw i32 %57, %shr39
  %59 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub41 = sub nsw i32 %59, 8
  %shr42 = ashr i32 %add40, %sub41
  store i32 %shr42, i32* %b, align 4, !tbaa !6
  %60 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #5
  %61 = load i32, i32* %a, align 4, !tbaa !6
  %62 = load i32, i32* %n, align 4, !tbaa !6
  %mul43 = mul i32 %61, %62
  %63 = load i32, i32* %b, align 4, !tbaa !6
  %64 = load i32, i32* %b, align 4, !tbaa !6
  %mul44 = mul i32 %63, %64
  %cmp45 = icmp ult i32 %mul43, %mul44
  br i1 %cmp45, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body22
  br label %cond.end

cond.false:                                       ; preds = %for.body22
  %65 = load i32, i32* %a, align 4, !tbaa !6
  %66 = load i32, i32* %n, align 4, !tbaa !6
  %mul46 = mul i32 %65, %66
  %67 = load i32, i32* %b, align 4, !tbaa !6
  %68 = load i32, i32* %b, align 4, !tbaa !6
  %mul47 = mul i32 %67, %68
  %sub48 = sub i32 %mul46, %mul47
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond49 = phi i32 [ 0, %cond.true ], [ %sub48, %cond.false ]
  store i32 %cond49, i32* %p, align 4, !tbaa !6
  %69 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #5
  %70 = load %struct.sgr_params_type*, %struct.sgr_params_type** %params, align 4, !tbaa !2
  %s50 = getelementptr inbounds %struct.sgr_params_type, %struct.sgr_params_type* %70, i32 0, i32 1
  %71 = load i32, i32* %radius_idx.addr, align 4, !tbaa !6
  %arrayidx51 = getelementptr inbounds [2 x i32], [2 x i32]* %s50, i32 0, i32 %71
  %72 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  store i32 %72, i32* %s, align 4, !tbaa !6
  %73 = bitcast i32* %z to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #5
  %74 = load i32, i32* %p, align 4, !tbaa !6
  %75 = load i32, i32* %s, align 4, !tbaa !6
  %mul52 = mul i32 %74, %75
  %add53 = add i32 %mul52, 524288
  %shr54 = lshr i32 %add53, 20
  store i32 %shr54, i32* %z, align 4, !tbaa !6
  %76 = load i32, i32* %z, align 4, !tbaa !6
  %cmp55 = icmp ult i32 %76, 255
  br i1 %cmp55, label %cond.true56, label %cond.false57

cond.true56:                                      ; preds = %cond.end
  %77 = load i32, i32* %z, align 4, !tbaa !6
  br label %cond.end58

cond.false57:                                     ; preds = %cond.end
  br label %cond.end58

cond.end58:                                       ; preds = %cond.false57, %cond.true56
  %cond59 = phi i32 [ %77, %cond.true56 ], [ 255, %cond.false57 ]
  %arrayidx60 = getelementptr inbounds [256 x i32], [256 x i32]* @av1_x_by_xplus1, i32 0, i32 %cond59
  %78 = load i32, i32* %arrayidx60, align 4, !tbaa !6
  %79 = load i32*, i32** %A.addr, align 4, !tbaa !2
  %80 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds i32, i32* %79, i32 %80
  store i32 %78, i32* %arrayidx61, align 4, !tbaa !6
  %81 = load i32*, i32** %A.addr, align 4, !tbaa !2
  %82 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds i32, i32* %81, i32 %82
  %83 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  %sub63 = sub nsw i32 256, %83
  %84 = load i32*, i32** %B.addr, align 4, !tbaa !2
  %85 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds i32, i32* %84, i32 %85
  %86 = load i32, i32* %arrayidx64, align 4, !tbaa !6
  %mul65 = mul i32 %sub63, %86
  %87 = load i32, i32* %n, align 4, !tbaa !6
  %sub66 = sub nsw i32 %87, 1
  %arrayidx67 = getelementptr inbounds [25 x i32], [25 x i32]* @av1_one_by_x, i32 0, i32 %sub66
  %88 = load i32, i32* %arrayidx67, align 4, !tbaa !6
  %mul68 = mul i32 %mul65, %88
  %add69 = add i32 %mul68, 2048
  %shr70 = lshr i32 %add69, 12
  %89 = load i32*, i32** %B.addr, align 4, !tbaa !2
  %90 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx71 = getelementptr inbounds i32, i32* %89, i32 %90
  store i32 %shr70, i32* %arrayidx71, align 4, !tbaa !6
  %91 = bitcast i32* %z to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #5
  %92 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #5
  %93 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #5
  %94 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  %95 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #5
  %96 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #5
  %97 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end58
  %98 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %98, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond19

for.end:                                          ; preds = %for.cond19
  br label %for.inc72

for.inc72:                                        ; preds = %for.end
  %99 = load i32, i32* %step, align 4, !tbaa !6
  %100 = load i32, i32* %i, align 4, !tbaa !6
  %add73 = add nsw i32 %100, %99
  store i32 %add73, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end74:                                        ; preds = %for.cond
  %101 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #5
  %102 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #5
  %103 = bitcast i32* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #5
  %104 = bitcast i32* %buf_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #5
  %105 = bitcast i32* %height_ext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #5
  %106 = bitcast i32* %width_ext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #5
  %107 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #5
  %108 = bitcast %struct.sgr_params_type** %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  ret void
}

; Function Attrs: nounwind
define internal void @boxsum(i32* %src, i32 %width, i32 %height, i32 %src_stride, i32 %r, i32 %sqr, i32* %dst, i32 %dst_stride) #0 {
entry:
  %src.addr = alloca i32*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %src_stride.addr = alloca i32, align 4
  %r.addr = alloca i32, align 4
  %sqr.addr = alloca i32, align 4
  %dst.addr = alloca i32*, align 4
  %dst_stride.addr = alloca i32, align 4
  store i32* %src, i32** %src.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i32 %r, i32* %r.addr, align 4, !tbaa !6
  store i32 %sqr, i32* %sqr.addr, align 4, !tbaa !6
  store i32* %dst, i32** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  %0 = load i32, i32* %r.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %2 = load i32, i32* %width.addr, align 4, !tbaa !6
  %3 = load i32, i32* %height.addr, align 4, !tbaa !6
  %4 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %5 = load i32, i32* %sqr.addr, align 4, !tbaa !6
  %6 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %7 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  call void @boxsum1(i32* %1, i32 %2, i32 %3, i32 %4, i32 %5, i32* %6, i32 %7)
  br label %if.end4

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %r.addr, align 4, !tbaa !6
  %cmp1 = icmp eq i32 %8, 2
  br i1 %cmp1, label %if.then2, label %if.else3

if.then2:                                         ; preds = %if.else
  %9 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %10 = load i32, i32* %width.addr, align 4, !tbaa !6
  %11 = load i32, i32* %height.addr, align 4, !tbaa !6
  %12 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %13 = load i32, i32* %sqr.addr, align 4, !tbaa !6
  %14 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %15 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  call void @boxsum2(i32* %9, i32 %10, i32 %11, i32 %12, i32 %13, i32* %14, i32 %15)
  br label %if.end

if.else3:                                         ; preds = %if.else
  br label %if.end

if.end:                                           ; preds = %if.else3, %if.then2
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @boxsum1(i32* %src, i32 %width, i32 %height, i32 %src_stride, i32 %sqr, i32* %dst, i32 %dst_stride) #0 {
entry:
  %src.addr = alloca i32*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %src_stride.addr = alloca i32, align 4
  %sqr.addr = alloca i32, align 4
  %dst.addr = alloca i32*, align 4
  %dst_stride.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  store i32* %src, i32** %src.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i32 %sqr, i32* %sqr.addr, align 4, !tbaa !6
  store i32* %dst, i32** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %sqr.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %if.then
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %7 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %8 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %9 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %10, i32* %a, align 4, !tbaa !6
  %11 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %12 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %12, %13
  %arrayidx1 = getelementptr inbounds i32, i32* %11, i32 %add
  %14 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  store i32 %14, i32* %b, align 4, !tbaa !6
  %15 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %16 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 2, %16
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %add2 = add nsw i32 %mul, %17
  %arrayidx3 = getelementptr inbounds i32, i32* %15, i32 %add2
  %18 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  store i32 %18, i32* %c, align 4, !tbaa !6
  %19 = load i32, i32* %a, align 4, !tbaa !6
  %20 = load i32, i32* %b, align 4, !tbaa !6
  %add4 = add nsw i32 %19, %20
  %21 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i32, i32* %21, i32 %22
  store i32 %add4, i32* %arrayidx5, align 4, !tbaa !6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %24 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %24, 2
  %cmp7 = icmp slt i32 %23, %sub
  br i1 %cmp7, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond6
  %25 = load i32, i32* %a, align 4, !tbaa !6
  %26 = load i32, i32* %b, align 4, !tbaa !6
  %add9 = add nsw i32 %25, %26
  %27 = load i32, i32* %c, align 4, !tbaa !6
  %add10 = add nsw i32 %add9, %27
  %28 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %30 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 %29, %30
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %add12 = add nsw i32 %mul11, %31
  %arrayidx13 = getelementptr inbounds i32, i32* %28, i32 %add12
  store i32 %add10, i32* %arrayidx13, align 4, !tbaa !6
  %32 = load i32, i32* %b, align 4, !tbaa !6
  store i32 %32, i32* %a, align 4, !tbaa !6
  %33 = load i32, i32* %c, align 4, !tbaa !6
  store i32 %33, i32* %b, align 4, !tbaa !6
  %34 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %add14 = add nsw i32 %35, 2
  %36 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 %add14, %36
  %37 = load i32, i32* %j, align 4, !tbaa !6
  %add16 = add nsw i32 %mul15, %37
  %arrayidx17 = getelementptr inbounds i32, i32* %34, i32 %add16
  %38 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  store i32 %38, i32* %c, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %39, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond6

for.end:                                          ; preds = %for.cond6
  %40 = load i32, i32* %a, align 4, !tbaa !6
  %41 = load i32, i32* %b, align 4, !tbaa !6
  %add18 = add nsw i32 %40, %41
  %42 = load i32, i32* %c, align 4, !tbaa !6
  %add19 = add nsw i32 %add18, %42
  %43 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %45 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 %44, %45
  %46 = load i32, i32* %j, align 4, !tbaa !6
  %add21 = add nsw i32 %mul20, %46
  %arrayidx22 = getelementptr inbounds i32, i32* %43, i32 %add21
  store i32 %add19, i32* %arrayidx22, align 4, !tbaa !6
  %47 = load i32, i32* %b, align 4, !tbaa !6
  %48 = load i32, i32* %c, align 4, !tbaa !6
  %add23 = add nsw i32 %47, %48
  %49 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %add24 = add nsw i32 %50, 1
  %51 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 %add24, %51
  %52 = load i32, i32* %j, align 4, !tbaa !6
  %add26 = add nsw i32 %mul25, %52
  %arrayidx27 = getelementptr inbounds i32, i32* %49, i32 %add26
  store i32 %add23, i32* %arrayidx27, align 4, !tbaa !6
  br label %for.inc28

for.inc28:                                        ; preds = %for.end
  %53 = load i32, i32* %j, align 4, !tbaa !6
  %inc29 = add nsw i32 %53, 1
  store i32 %inc29, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end30:                                        ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc82, %if.else
  %54 = load i32, i32* %j, align 4, !tbaa !6
  %55 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp32 = icmp slt i32 %54, %55
  br i1 %cmp32, label %for.body33, label %for.end84

for.body33:                                       ; preds = %for.cond31
  %56 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %57 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds i32, i32* %56, i32 %57
  %58 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %59 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %60 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %mul36 = mul nsw i32 %58, %61
  store i32 %mul36, i32* %a, align 4, !tbaa !6
  %62 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %63 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %64 = load i32, i32* %j, align 4, !tbaa !6
  %add37 = add nsw i32 %63, %64
  %arrayidx38 = getelementptr inbounds i32, i32* %62, i32 %add37
  %65 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %66 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %67 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %68 = load i32, i32* %j, align 4, !tbaa !6
  %add39 = add nsw i32 %67, %68
  %arrayidx40 = getelementptr inbounds i32, i32* %66, i32 %add39
  %69 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %mul41 = mul nsw i32 %65, %69
  store i32 %mul41, i32* %b, align 4, !tbaa !6
  %70 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %71 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul42 = mul nsw i32 2, %71
  %72 = load i32, i32* %j, align 4, !tbaa !6
  %add43 = add nsw i32 %mul42, %72
  %arrayidx44 = getelementptr inbounds i32, i32* %70, i32 %add43
  %73 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  %74 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %75 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul45 = mul nsw i32 2, %75
  %76 = load i32, i32* %j, align 4, !tbaa !6
  %add46 = add nsw i32 %mul45, %76
  %arrayidx47 = getelementptr inbounds i32, i32* %74, i32 %add46
  %77 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %mul48 = mul nsw i32 %73, %77
  store i32 %mul48, i32* %c, align 4, !tbaa !6
  %78 = load i32, i32* %a, align 4, !tbaa !6
  %79 = load i32, i32* %b, align 4, !tbaa !6
  %add49 = add nsw i32 %78, %79
  %80 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %81 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx50 = getelementptr inbounds i32, i32* %80, i32 %81
  store i32 %add49, i32* %arrayidx50, align 4, !tbaa !6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc69, %for.body33
  %82 = load i32, i32* %i, align 4, !tbaa !6
  %83 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub52 = sub nsw i32 %83, 2
  %cmp53 = icmp slt i32 %82, %sub52
  br i1 %cmp53, label %for.body54, label %for.end71

for.body54:                                       ; preds = %for.cond51
  %84 = load i32, i32* %a, align 4, !tbaa !6
  %85 = load i32, i32* %b, align 4, !tbaa !6
  %add55 = add nsw i32 %84, %85
  %86 = load i32, i32* %c, align 4, !tbaa !6
  %add56 = add nsw i32 %add55, %86
  %87 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %88 = load i32, i32* %i, align 4, !tbaa !6
  %89 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul57 = mul nsw i32 %88, %89
  %90 = load i32, i32* %j, align 4, !tbaa !6
  %add58 = add nsw i32 %mul57, %90
  %arrayidx59 = getelementptr inbounds i32, i32* %87, i32 %add58
  store i32 %add56, i32* %arrayidx59, align 4, !tbaa !6
  %91 = load i32, i32* %b, align 4, !tbaa !6
  store i32 %91, i32* %a, align 4, !tbaa !6
  %92 = load i32, i32* %c, align 4, !tbaa !6
  store i32 %92, i32* %b, align 4, !tbaa !6
  %93 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %94 = load i32, i32* %i, align 4, !tbaa !6
  %add60 = add nsw i32 %94, 2
  %95 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul61 = mul nsw i32 %add60, %95
  %96 = load i32, i32* %j, align 4, !tbaa !6
  %add62 = add nsw i32 %mul61, %96
  %arrayidx63 = getelementptr inbounds i32, i32* %93, i32 %add62
  %97 = load i32, i32* %arrayidx63, align 4, !tbaa !6
  %98 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %99 = load i32, i32* %i, align 4, !tbaa !6
  %add64 = add nsw i32 %99, 2
  %100 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul65 = mul nsw i32 %add64, %100
  %101 = load i32, i32* %j, align 4, !tbaa !6
  %add66 = add nsw i32 %mul65, %101
  %arrayidx67 = getelementptr inbounds i32, i32* %98, i32 %add66
  %102 = load i32, i32* %arrayidx67, align 4, !tbaa !6
  %mul68 = mul nsw i32 %97, %102
  store i32 %mul68, i32* %c, align 4, !tbaa !6
  br label %for.inc69

for.inc69:                                        ; preds = %for.body54
  %103 = load i32, i32* %i, align 4, !tbaa !6
  %inc70 = add nsw i32 %103, 1
  store i32 %inc70, i32* %i, align 4, !tbaa !6
  br label %for.cond51

for.end71:                                        ; preds = %for.cond51
  %104 = load i32, i32* %a, align 4, !tbaa !6
  %105 = load i32, i32* %b, align 4, !tbaa !6
  %add72 = add nsw i32 %104, %105
  %106 = load i32, i32* %c, align 4, !tbaa !6
  %add73 = add nsw i32 %add72, %106
  %107 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %108 = load i32, i32* %i, align 4, !tbaa !6
  %109 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul74 = mul nsw i32 %108, %109
  %110 = load i32, i32* %j, align 4, !tbaa !6
  %add75 = add nsw i32 %mul74, %110
  %arrayidx76 = getelementptr inbounds i32, i32* %107, i32 %add75
  store i32 %add73, i32* %arrayidx76, align 4, !tbaa !6
  %111 = load i32, i32* %b, align 4, !tbaa !6
  %112 = load i32, i32* %c, align 4, !tbaa !6
  %add77 = add nsw i32 %111, %112
  %113 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %114 = load i32, i32* %i, align 4, !tbaa !6
  %add78 = add nsw i32 %114, 1
  %115 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul79 = mul nsw i32 %add78, %115
  %116 = load i32, i32* %j, align 4, !tbaa !6
  %add80 = add nsw i32 %mul79, %116
  %arrayidx81 = getelementptr inbounds i32, i32* %113, i32 %add80
  store i32 %add77, i32* %arrayidx81, align 4, !tbaa !6
  br label %for.inc82

for.inc82:                                        ; preds = %for.end71
  %117 = load i32, i32* %j, align 4, !tbaa !6
  %inc83 = add nsw i32 %117, 1
  store i32 %inc83, i32* %j, align 4, !tbaa !6
  br label %for.cond31

for.end84:                                        ; preds = %for.cond31
  br label %if.end

if.end:                                           ; preds = %for.end84, %for.end30
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond85

for.cond85:                                       ; preds = %for.inc125, %if.end
  %118 = load i32, i32* %i, align 4, !tbaa !6
  %119 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp86 = icmp slt i32 %118, %119
  br i1 %cmp86, label %for.body87, label %for.end127

for.body87:                                       ; preds = %for.cond85
  %120 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %121 = load i32, i32* %i, align 4, !tbaa !6
  %122 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul88 = mul nsw i32 %121, %122
  %arrayidx89 = getelementptr inbounds i32, i32* %120, i32 %mul88
  %123 = load i32, i32* %arrayidx89, align 4, !tbaa !6
  store i32 %123, i32* %a, align 4, !tbaa !6
  %124 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %125 = load i32, i32* %i, align 4, !tbaa !6
  %126 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul90 = mul nsw i32 %125, %126
  %add91 = add nsw i32 %mul90, 1
  %arrayidx92 = getelementptr inbounds i32, i32* %124, i32 %add91
  %127 = load i32, i32* %arrayidx92, align 4, !tbaa !6
  store i32 %127, i32* %b, align 4, !tbaa !6
  %128 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %129 = load i32, i32* %i, align 4, !tbaa !6
  %130 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul93 = mul nsw i32 %129, %130
  %add94 = add nsw i32 %mul93, 2
  %arrayidx95 = getelementptr inbounds i32, i32* %128, i32 %add94
  %131 = load i32, i32* %arrayidx95, align 4, !tbaa !6
  store i32 %131, i32* %c, align 4, !tbaa !6
  %132 = load i32, i32* %a, align 4, !tbaa !6
  %133 = load i32, i32* %b, align 4, !tbaa !6
  %add96 = add nsw i32 %132, %133
  %134 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %135 = load i32, i32* %i, align 4, !tbaa !6
  %136 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul97 = mul nsw i32 %135, %136
  %arrayidx98 = getelementptr inbounds i32, i32* %134, i32 %mul97
  store i32 %add96, i32* %arrayidx98, align 4, !tbaa !6
  store i32 1, i32* %j, align 4, !tbaa !6
  br label %for.cond99

for.cond99:                                       ; preds = %for.inc112, %for.body87
  %137 = load i32, i32* %j, align 4, !tbaa !6
  %138 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub100 = sub nsw i32 %138, 2
  %cmp101 = icmp slt i32 %137, %sub100
  br i1 %cmp101, label %for.body102, label %for.end114

for.body102:                                      ; preds = %for.cond99
  %139 = load i32, i32* %a, align 4, !tbaa !6
  %140 = load i32, i32* %b, align 4, !tbaa !6
  %add103 = add nsw i32 %139, %140
  %141 = load i32, i32* %c, align 4, !tbaa !6
  %add104 = add nsw i32 %add103, %141
  %142 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %143 = load i32, i32* %i, align 4, !tbaa !6
  %144 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul105 = mul nsw i32 %143, %144
  %145 = load i32, i32* %j, align 4, !tbaa !6
  %add106 = add nsw i32 %mul105, %145
  %arrayidx107 = getelementptr inbounds i32, i32* %142, i32 %add106
  store i32 %add104, i32* %arrayidx107, align 4, !tbaa !6
  %146 = load i32, i32* %b, align 4, !tbaa !6
  store i32 %146, i32* %a, align 4, !tbaa !6
  %147 = load i32, i32* %c, align 4, !tbaa !6
  store i32 %147, i32* %b, align 4, !tbaa !6
  %148 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %149 = load i32, i32* %i, align 4, !tbaa !6
  %150 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul108 = mul nsw i32 %149, %150
  %151 = load i32, i32* %j, align 4, !tbaa !6
  %add109 = add nsw i32 %151, 2
  %add110 = add nsw i32 %mul108, %add109
  %arrayidx111 = getelementptr inbounds i32, i32* %148, i32 %add110
  %152 = load i32, i32* %arrayidx111, align 4, !tbaa !6
  store i32 %152, i32* %c, align 4, !tbaa !6
  br label %for.inc112

for.inc112:                                       ; preds = %for.body102
  %153 = load i32, i32* %j, align 4, !tbaa !6
  %inc113 = add nsw i32 %153, 1
  store i32 %inc113, i32* %j, align 4, !tbaa !6
  br label %for.cond99

for.end114:                                       ; preds = %for.cond99
  %154 = load i32, i32* %a, align 4, !tbaa !6
  %155 = load i32, i32* %b, align 4, !tbaa !6
  %add115 = add nsw i32 %154, %155
  %156 = load i32, i32* %c, align 4, !tbaa !6
  %add116 = add nsw i32 %add115, %156
  %157 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %158 = load i32, i32* %i, align 4, !tbaa !6
  %159 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul117 = mul nsw i32 %158, %159
  %160 = load i32, i32* %j, align 4, !tbaa !6
  %add118 = add nsw i32 %mul117, %160
  %arrayidx119 = getelementptr inbounds i32, i32* %157, i32 %add118
  store i32 %add116, i32* %arrayidx119, align 4, !tbaa !6
  %161 = load i32, i32* %b, align 4, !tbaa !6
  %162 = load i32, i32* %c, align 4, !tbaa !6
  %add120 = add nsw i32 %161, %162
  %163 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %164 = load i32, i32* %i, align 4, !tbaa !6
  %165 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul121 = mul nsw i32 %164, %165
  %166 = load i32, i32* %j, align 4, !tbaa !6
  %add122 = add nsw i32 %166, 1
  %add123 = add nsw i32 %mul121, %add122
  %arrayidx124 = getelementptr inbounds i32, i32* %163, i32 %add123
  store i32 %add120, i32* %arrayidx124, align 4, !tbaa !6
  br label %for.inc125

for.inc125:                                       ; preds = %for.end114
  %167 = load i32, i32* %i, align 4, !tbaa !6
  %inc126 = add nsw i32 %167, 1
  store i32 %inc126, i32* %i, align 4, !tbaa !6
  br label %for.cond85

for.end127:                                       ; preds = %for.cond85
  %168 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #5
  %169 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #5
  %170 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #5
  %171 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #5
  %172 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #5
  ret void
}

; Function Attrs: nounwind
define internal void @boxsum2(i32* %src, i32 %width, i32 %height, i32 %src_stride, i32 %sqr, i32* %dst, i32 %dst_stride) #0 {
entry:
  %src.addr = alloca i32*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %src_stride.addr = alloca i32, align 4
  %sqr.addr = alloca i32, align 4
  %dst.addr = alloca i32*, align 4
  %dst_stride.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  %d = alloca i32, align 4
  %e = alloca i32, align 4
  store i32* %src, i32** %src.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i32 %sqr, i32* %sqr.addr, align 4, !tbaa !6
  store i32* %dst, i32** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i32* %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %sqr.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc52, %if.then
  %8 = load i32, i32* %j, align 4, !tbaa !6
  %9 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end54

for.body:                                         ; preds = %for.cond
  %10 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %10, i32 %11
  %12 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %12, i32* %a, align 4, !tbaa !6
  %13 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %14 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %14, %15
  %arrayidx1 = getelementptr inbounds i32, i32* %13, i32 %add
  %16 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  store i32 %16, i32* %b, align 4, !tbaa !6
  %17 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %18 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 2, %18
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %add2 = add nsw i32 %mul, %19
  %arrayidx3 = getelementptr inbounds i32, i32* %17, i32 %add2
  %20 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  store i32 %20, i32* %c, align 4, !tbaa !6
  %21 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %22 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 3, %22
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %add5 = add nsw i32 %mul4, %23
  %arrayidx6 = getelementptr inbounds i32, i32* %21, i32 %add5
  %24 = load i32, i32* %arrayidx6, align 4, !tbaa !6
  store i32 %24, i32* %d, align 4, !tbaa !6
  %25 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %26 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 4, %26
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %add8 = add nsw i32 %mul7, %27
  %arrayidx9 = getelementptr inbounds i32, i32* %25, i32 %add8
  %28 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  store i32 %28, i32* %e, align 4, !tbaa !6
  %29 = load i32, i32* %a, align 4, !tbaa !6
  %30 = load i32, i32* %b, align 4, !tbaa !6
  %add10 = add nsw i32 %29, %30
  %31 = load i32, i32* %c, align 4, !tbaa !6
  %add11 = add nsw i32 %add10, %31
  %32 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds i32, i32* %32, i32 %33
  store i32 %add11, i32* %arrayidx12, align 4, !tbaa !6
  %34 = load i32, i32* %a, align 4, !tbaa !6
  %35 = load i32, i32* %b, align 4, !tbaa !6
  %add13 = add nsw i32 %34, %35
  %36 = load i32, i32* %c, align 4, !tbaa !6
  %add14 = add nsw i32 %add13, %36
  %37 = load i32, i32* %d, align 4, !tbaa !6
  %add15 = add nsw i32 %add14, %37
  %38 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %39 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %40 = load i32, i32* %j, align 4, !tbaa !6
  %add16 = add nsw i32 %39, %40
  %arrayidx17 = getelementptr inbounds i32, i32* %38, i32 %add16
  store i32 %add15, i32* %arrayidx17, align 4, !tbaa !6
  store i32 2, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %for.body
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %42 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %42, 3
  %cmp19 = icmp slt i32 %41, %sub
  br i1 %cmp19, label %for.body20, label %for.end

for.body20:                                       ; preds = %for.cond18
  %43 = load i32, i32* %a, align 4, !tbaa !6
  %44 = load i32, i32* %b, align 4, !tbaa !6
  %add21 = add nsw i32 %43, %44
  %45 = load i32, i32* %c, align 4, !tbaa !6
  %add22 = add nsw i32 %add21, %45
  %46 = load i32, i32* %d, align 4, !tbaa !6
  %add23 = add nsw i32 %add22, %46
  %47 = load i32, i32* %e, align 4, !tbaa !6
  %add24 = add nsw i32 %add23, %47
  %48 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %50 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 %49, %50
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %add26 = add nsw i32 %mul25, %51
  %arrayidx27 = getelementptr inbounds i32, i32* %48, i32 %add26
  store i32 %add24, i32* %arrayidx27, align 4, !tbaa !6
  %52 = load i32, i32* %b, align 4, !tbaa !6
  store i32 %52, i32* %a, align 4, !tbaa !6
  %53 = load i32, i32* %c, align 4, !tbaa !6
  store i32 %53, i32* %b, align 4, !tbaa !6
  %54 = load i32, i32* %d, align 4, !tbaa !6
  store i32 %54, i32* %c, align 4, !tbaa !6
  %55 = load i32, i32* %e, align 4, !tbaa !6
  store i32 %55, i32* %d, align 4, !tbaa !6
  %56 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %57 = load i32, i32* %i, align 4, !tbaa !6
  %add28 = add nsw i32 %57, 3
  %58 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 %add28, %58
  %59 = load i32, i32* %j, align 4, !tbaa !6
  %add30 = add nsw i32 %mul29, %59
  %arrayidx31 = getelementptr inbounds i32, i32* %56, i32 %add30
  %60 = load i32, i32* %arrayidx31, align 4, !tbaa !6
  store i32 %60, i32* %e, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body20
  %61 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %61, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.end:                                          ; preds = %for.cond18
  %62 = load i32, i32* %a, align 4, !tbaa !6
  %63 = load i32, i32* %b, align 4, !tbaa !6
  %add32 = add nsw i32 %62, %63
  %64 = load i32, i32* %c, align 4, !tbaa !6
  %add33 = add nsw i32 %add32, %64
  %65 = load i32, i32* %d, align 4, !tbaa !6
  %add34 = add nsw i32 %add33, %65
  %66 = load i32, i32* %e, align 4, !tbaa !6
  %add35 = add nsw i32 %add34, %66
  %67 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %68 = load i32, i32* %i, align 4, !tbaa !6
  %69 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul36 = mul nsw i32 %68, %69
  %70 = load i32, i32* %j, align 4, !tbaa !6
  %add37 = add nsw i32 %mul36, %70
  %arrayidx38 = getelementptr inbounds i32, i32* %67, i32 %add37
  store i32 %add35, i32* %arrayidx38, align 4, !tbaa !6
  %71 = load i32, i32* %b, align 4, !tbaa !6
  %72 = load i32, i32* %c, align 4, !tbaa !6
  %add39 = add nsw i32 %71, %72
  %73 = load i32, i32* %d, align 4, !tbaa !6
  %add40 = add nsw i32 %add39, %73
  %74 = load i32, i32* %e, align 4, !tbaa !6
  %add41 = add nsw i32 %add40, %74
  %75 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %76 = load i32, i32* %i, align 4, !tbaa !6
  %add42 = add nsw i32 %76, 1
  %77 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul43 = mul nsw i32 %add42, %77
  %78 = load i32, i32* %j, align 4, !tbaa !6
  %add44 = add nsw i32 %mul43, %78
  %arrayidx45 = getelementptr inbounds i32, i32* %75, i32 %add44
  store i32 %add41, i32* %arrayidx45, align 4, !tbaa !6
  %79 = load i32, i32* %c, align 4, !tbaa !6
  %80 = load i32, i32* %d, align 4, !tbaa !6
  %add46 = add nsw i32 %79, %80
  %81 = load i32, i32* %e, align 4, !tbaa !6
  %add47 = add nsw i32 %add46, %81
  %82 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %83 = load i32, i32* %i, align 4, !tbaa !6
  %add48 = add nsw i32 %83, 2
  %84 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul49 = mul nsw i32 %add48, %84
  %85 = load i32, i32* %j, align 4, !tbaa !6
  %add50 = add nsw i32 %mul49, %85
  %arrayidx51 = getelementptr inbounds i32, i32* %82, i32 %add50
  store i32 %add47, i32* %arrayidx51, align 4, !tbaa !6
  br label %for.inc52

for.inc52:                                        ; preds = %for.end
  %86 = load i32, i32* %j, align 4, !tbaa !6
  %inc53 = add nsw i32 %86, 1
  store i32 %inc53, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end54:                                        ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc138, %if.else
  %87 = load i32, i32* %j, align 4, !tbaa !6
  %88 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp56 = icmp slt i32 %87, %88
  br i1 %cmp56, label %for.body57, label %for.end140

for.body57:                                       ; preds = %for.cond55
  %89 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %90 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds i32, i32* %89, i32 %90
  %91 = load i32, i32* %arrayidx58, align 4, !tbaa !6
  %92 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %93 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds i32, i32* %92, i32 %93
  %94 = load i32, i32* %arrayidx59, align 4, !tbaa !6
  %mul60 = mul nsw i32 %91, %94
  store i32 %mul60, i32* %a, align 4, !tbaa !6
  %95 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %96 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %97 = load i32, i32* %j, align 4, !tbaa !6
  %add61 = add nsw i32 %96, %97
  %arrayidx62 = getelementptr inbounds i32, i32* %95, i32 %add61
  %98 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  %99 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %100 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %101 = load i32, i32* %j, align 4, !tbaa !6
  %add63 = add nsw i32 %100, %101
  %arrayidx64 = getelementptr inbounds i32, i32* %99, i32 %add63
  %102 = load i32, i32* %arrayidx64, align 4, !tbaa !6
  %mul65 = mul nsw i32 %98, %102
  store i32 %mul65, i32* %b, align 4, !tbaa !6
  %103 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %104 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul66 = mul nsw i32 2, %104
  %105 = load i32, i32* %j, align 4, !tbaa !6
  %add67 = add nsw i32 %mul66, %105
  %arrayidx68 = getelementptr inbounds i32, i32* %103, i32 %add67
  %106 = load i32, i32* %arrayidx68, align 4, !tbaa !6
  %107 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %108 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul69 = mul nsw i32 2, %108
  %109 = load i32, i32* %j, align 4, !tbaa !6
  %add70 = add nsw i32 %mul69, %109
  %arrayidx71 = getelementptr inbounds i32, i32* %107, i32 %add70
  %110 = load i32, i32* %arrayidx71, align 4, !tbaa !6
  %mul72 = mul nsw i32 %106, %110
  store i32 %mul72, i32* %c, align 4, !tbaa !6
  %111 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %112 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul73 = mul nsw i32 3, %112
  %113 = load i32, i32* %j, align 4, !tbaa !6
  %add74 = add nsw i32 %mul73, %113
  %arrayidx75 = getelementptr inbounds i32, i32* %111, i32 %add74
  %114 = load i32, i32* %arrayidx75, align 4, !tbaa !6
  %115 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %116 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul76 = mul nsw i32 3, %116
  %117 = load i32, i32* %j, align 4, !tbaa !6
  %add77 = add nsw i32 %mul76, %117
  %arrayidx78 = getelementptr inbounds i32, i32* %115, i32 %add77
  %118 = load i32, i32* %arrayidx78, align 4, !tbaa !6
  %mul79 = mul nsw i32 %114, %118
  store i32 %mul79, i32* %d, align 4, !tbaa !6
  %119 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %120 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul80 = mul nsw i32 4, %120
  %121 = load i32, i32* %j, align 4, !tbaa !6
  %add81 = add nsw i32 %mul80, %121
  %arrayidx82 = getelementptr inbounds i32, i32* %119, i32 %add81
  %122 = load i32, i32* %arrayidx82, align 4, !tbaa !6
  %123 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %124 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul83 = mul nsw i32 4, %124
  %125 = load i32, i32* %j, align 4, !tbaa !6
  %add84 = add nsw i32 %mul83, %125
  %arrayidx85 = getelementptr inbounds i32, i32* %123, i32 %add84
  %126 = load i32, i32* %arrayidx85, align 4, !tbaa !6
  %mul86 = mul nsw i32 %122, %126
  store i32 %mul86, i32* %e, align 4, !tbaa !6
  %127 = load i32, i32* %a, align 4, !tbaa !6
  %128 = load i32, i32* %b, align 4, !tbaa !6
  %add87 = add nsw i32 %127, %128
  %129 = load i32, i32* %c, align 4, !tbaa !6
  %add88 = add nsw i32 %add87, %129
  %130 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %131 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx89 = getelementptr inbounds i32, i32* %130, i32 %131
  store i32 %add88, i32* %arrayidx89, align 4, !tbaa !6
  %132 = load i32, i32* %a, align 4, !tbaa !6
  %133 = load i32, i32* %b, align 4, !tbaa !6
  %add90 = add nsw i32 %132, %133
  %134 = load i32, i32* %c, align 4, !tbaa !6
  %add91 = add nsw i32 %add90, %134
  %135 = load i32, i32* %d, align 4, !tbaa !6
  %add92 = add nsw i32 %add91, %135
  %136 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %137 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %138 = load i32, i32* %j, align 4, !tbaa !6
  %add93 = add nsw i32 %137, %138
  %arrayidx94 = getelementptr inbounds i32, i32* %136, i32 %add93
  store i32 %add92, i32* %arrayidx94, align 4, !tbaa !6
  store i32 2, i32* %i, align 4, !tbaa !6
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc115, %for.body57
  %139 = load i32, i32* %i, align 4, !tbaa !6
  %140 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub96 = sub nsw i32 %140, 3
  %cmp97 = icmp slt i32 %139, %sub96
  br i1 %cmp97, label %for.body98, label %for.end117

for.body98:                                       ; preds = %for.cond95
  %141 = load i32, i32* %a, align 4, !tbaa !6
  %142 = load i32, i32* %b, align 4, !tbaa !6
  %add99 = add nsw i32 %141, %142
  %143 = load i32, i32* %c, align 4, !tbaa !6
  %add100 = add nsw i32 %add99, %143
  %144 = load i32, i32* %d, align 4, !tbaa !6
  %add101 = add nsw i32 %add100, %144
  %145 = load i32, i32* %e, align 4, !tbaa !6
  %add102 = add nsw i32 %add101, %145
  %146 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %147 = load i32, i32* %i, align 4, !tbaa !6
  %148 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul103 = mul nsw i32 %147, %148
  %149 = load i32, i32* %j, align 4, !tbaa !6
  %add104 = add nsw i32 %mul103, %149
  %arrayidx105 = getelementptr inbounds i32, i32* %146, i32 %add104
  store i32 %add102, i32* %arrayidx105, align 4, !tbaa !6
  %150 = load i32, i32* %b, align 4, !tbaa !6
  store i32 %150, i32* %a, align 4, !tbaa !6
  %151 = load i32, i32* %c, align 4, !tbaa !6
  store i32 %151, i32* %b, align 4, !tbaa !6
  %152 = load i32, i32* %d, align 4, !tbaa !6
  store i32 %152, i32* %c, align 4, !tbaa !6
  %153 = load i32, i32* %e, align 4, !tbaa !6
  store i32 %153, i32* %d, align 4, !tbaa !6
  %154 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %155 = load i32, i32* %i, align 4, !tbaa !6
  %add106 = add nsw i32 %155, 3
  %156 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul107 = mul nsw i32 %add106, %156
  %157 = load i32, i32* %j, align 4, !tbaa !6
  %add108 = add nsw i32 %mul107, %157
  %arrayidx109 = getelementptr inbounds i32, i32* %154, i32 %add108
  %158 = load i32, i32* %arrayidx109, align 4, !tbaa !6
  %159 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %160 = load i32, i32* %i, align 4, !tbaa !6
  %add110 = add nsw i32 %160, 3
  %161 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul111 = mul nsw i32 %add110, %161
  %162 = load i32, i32* %j, align 4, !tbaa !6
  %add112 = add nsw i32 %mul111, %162
  %arrayidx113 = getelementptr inbounds i32, i32* %159, i32 %add112
  %163 = load i32, i32* %arrayidx113, align 4, !tbaa !6
  %mul114 = mul nsw i32 %158, %163
  store i32 %mul114, i32* %e, align 4, !tbaa !6
  br label %for.inc115

for.inc115:                                       ; preds = %for.body98
  %164 = load i32, i32* %i, align 4, !tbaa !6
  %inc116 = add nsw i32 %164, 1
  store i32 %inc116, i32* %i, align 4, !tbaa !6
  br label %for.cond95

for.end117:                                       ; preds = %for.cond95
  %165 = load i32, i32* %a, align 4, !tbaa !6
  %166 = load i32, i32* %b, align 4, !tbaa !6
  %add118 = add nsw i32 %165, %166
  %167 = load i32, i32* %c, align 4, !tbaa !6
  %add119 = add nsw i32 %add118, %167
  %168 = load i32, i32* %d, align 4, !tbaa !6
  %add120 = add nsw i32 %add119, %168
  %169 = load i32, i32* %e, align 4, !tbaa !6
  %add121 = add nsw i32 %add120, %169
  %170 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %171 = load i32, i32* %i, align 4, !tbaa !6
  %172 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul122 = mul nsw i32 %171, %172
  %173 = load i32, i32* %j, align 4, !tbaa !6
  %add123 = add nsw i32 %mul122, %173
  %arrayidx124 = getelementptr inbounds i32, i32* %170, i32 %add123
  store i32 %add121, i32* %arrayidx124, align 4, !tbaa !6
  %174 = load i32, i32* %b, align 4, !tbaa !6
  %175 = load i32, i32* %c, align 4, !tbaa !6
  %add125 = add nsw i32 %174, %175
  %176 = load i32, i32* %d, align 4, !tbaa !6
  %add126 = add nsw i32 %add125, %176
  %177 = load i32, i32* %e, align 4, !tbaa !6
  %add127 = add nsw i32 %add126, %177
  %178 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %179 = load i32, i32* %i, align 4, !tbaa !6
  %add128 = add nsw i32 %179, 1
  %180 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul129 = mul nsw i32 %add128, %180
  %181 = load i32, i32* %j, align 4, !tbaa !6
  %add130 = add nsw i32 %mul129, %181
  %arrayidx131 = getelementptr inbounds i32, i32* %178, i32 %add130
  store i32 %add127, i32* %arrayidx131, align 4, !tbaa !6
  %182 = load i32, i32* %c, align 4, !tbaa !6
  %183 = load i32, i32* %d, align 4, !tbaa !6
  %add132 = add nsw i32 %182, %183
  %184 = load i32, i32* %e, align 4, !tbaa !6
  %add133 = add nsw i32 %add132, %184
  %185 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %186 = load i32, i32* %i, align 4, !tbaa !6
  %add134 = add nsw i32 %186, 2
  %187 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul135 = mul nsw i32 %add134, %187
  %188 = load i32, i32* %j, align 4, !tbaa !6
  %add136 = add nsw i32 %mul135, %188
  %arrayidx137 = getelementptr inbounds i32, i32* %185, i32 %add136
  store i32 %add133, i32* %arrayidx137, align 4, !tbaa !6
  br label %for.inc138

for.inc138:                                       ; preds = %for.end117
  %189 = load i32, i32* %j, align 4, !tbaa !6
  %inc139 = add nsw i32 %189, 1
  store i32 %inc139, i32* %j, align 4, !tbaa !6
  br label %for.cond55

for.end140:                                       ; preds = %for.cond55
  br label %if.end

if.end:                                           ; preds = %for.end140, %for.end54
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond141

for.cond141:                                      ; preds = %for.inc206, %if.end
  %190 = load i32, i32* %i, align 4, !tbaa !6
  %191 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp142 = icmp slt i32 %190, %191
  br i1 %cmp142, label %for.body143, label %for.end208

for.body143:                                      ; preds = %for.cond141
  %192 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %193 = load i32, i32* %i, align 4, !tbaa !6
  %194 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul144 = mul nsw i32 %193, %194
  %arrayidx145 = getelementptr inbounds i32, i32* %192, i32 %mul144
  %195 = load i32, i32* %arrayidx145, align 4, !tbaa !6
  store i32 %195, i32* %a, align 4, !tbaa !6
  %196 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %197 = load i32, i32* %i, align 4, !tbaa !6
  %198 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul146 = mul nsw i32 %197, %198
  %add147 = add nsw i32 %mul146, 1
  %arrayidx148 = getelementptr inbounds i32, i32* %196, i32 %add147
  %199 = load i32, i32* %arrayidx148, align 4, !tbaa !6
  store i32 %199, i32* %b, align 4, !tbaa !6
  %200 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %201 = load i32, i32* %i, align 4, !tbaa !6
  %202 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul149 = mul nsw i32 %201, %202
  %add150 = add nsw i32 %mul149, 2
  %arrayidx151 = getelementptr inbounds i32, i32* %200, i32 %add150
  %203 = load i32, i32* %arrayidx151, align 4, !tbaa !6
  store i32 %203, i32* %c, align 4, !tbaa !6
  %204 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %205 = load i32, i32* %i, align 4, !tbaa !6
  %206 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul152 = mul nsw i32 %205, %206
  %add153 = add nsw i32 %mul152, 3
  %arrayidx154 = getelementptr inbounds i32, i32* %204, i32 %add153
  %207 = load i32, i32* %arrayidx154, align 4, !tbaa !6
  store i32 %207, i32* %d, align 4, !tbaa !6
  %208 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %209 = load i32, i32* %i, align 4, !tbaa !6
  %210 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul155 = mul nsw i32 %209, %210
  %add156 = add nsw i32 %mul155, 4
  %arrayidx157 = getelementptr inbounds i32, i32* %208, i32 %add156
  %211 = load i32, i32* %arrayidx157, align 4, !tbaa !6
  store i32 %211, i32* %e, align 4, !tbaa !6
  %212 = load i32, i32* %a, align 4, !tbaa !6
  %213 = load i32, i32* %b, align 4, !tbaa !6
  %add158 = add nsw i32 %212, %213
  %214 = load i32, i32* %c, align 4, !tbaa !6
  %add159 = add nsw i32 %add158, %214
  %215 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %216 = load i32, i32* %i, align 4, !tbaa !6
  %217 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul160 = mul nsw i32 %216, %217
  %arrayidx161 = getelementptr inbounds i32, i32* %215, i32 %mul160
  store i32 %add159, i32* %arrayidx161, align 4, !tbaa !6
  %218 = load i32, i32* %a, align 4, !tbaa !6
  %219 = load i32, i32* %b, align 4, !tbaa !6
  %add162 = add nsw i32 %218, %219
  %220 = load i32, i32* %c, align 4, !tbaa !6
  %add163 = add nsw i32 %add162, %220
  %221 = load i32, i32* %d, align 4, !tbaa !6
  %add164 = add nsw i32 %add163, %221
  %222 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %223 = load i32, i32* %i, align 4, !tbaa !6
  %224 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul165 = mul nsw i32 %223, %224
  %add166 = add nsw i32 %mul165, 1
  %arrayidx167 = getelementptr inbounds i32, i32* %222, i32 %add166
  store i32 %add164, i32* %arrayidx167, align 4, !tbaa !6
  store i32 2, i32* %j, align 4, !tbaa !6
  br label %for.cond168

for.cond168:                                      ; preds = %for.inc183, %for.body143
  %225 = load i32, i32* %j, align 4, !tbaa !6
  %226 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub169 = sub nsw i32 %226, 3
  %cmp170 = icmp slt i32 %225, %sub169
  br i1 %cmp170, label %for.body171, label %for.end185

for.body171:                                      ; preds = %for.cond168
  %227 = load i32, i32* %a, align 4, !tbaa !6
  %228 = load i32, i32* %b, align 4, !tbaa !6
  %add172 = add nsw i32 %227, %228
  %229 = load i32, i32* %c, align 4, !tbaa !6
  %add173 = add nsw i32 %add172, %229
  %230 = load i32, i32* %d, align 4, !tbaa !6
  %add174 = add nsw i32 %add173, %230
  %231 = load i32, i32* %e, align 4, !tbaa !6
  %add175 = add nsw i32 %add174, %231
  %232 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %233 = load i32, i32* %i, align 4, !tbaa !6
  %234 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul176 = mul nsw i32 %233, %234
  %235 = load i32, i32* %j, align 4, !tbaa !6
  %add177 = add nsw i32 %mul176, %235
  %arrayidx178 = getelementptr inbounds i32, i32* %232, i32 %add177
  store i32 %add175, i32* %arrayidx178, align 4, !tbaa !6
  %236 = load i32, i32* %b, align 4, !tbaa !6
  store i32 %236, i32* %a, align 4, !tbaa !6
  %237 = load i32, i32* %c, align 4, !tbaa !6
  store i32 %237, i32* %b, align 4, !tbaa !6
  %238 = load i32, i32* %d, align 4, !tbaa !6
  store i32 %238, i32* %c, align 4, !tbaa !6
  %239 = load i32, i32* %e, align 4, !tbaa !6
  store i32 %239, i32* %d, align 4, !tbaa !6
  %240 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %241 = load i32, i32* %i, align 4, !tbaa !6
  %242 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul179 = mul nsw i32 %241, %242
  %243 = load i32, i32* %j, align 4, !tbaa !6
  %add180 = add nsw i32 %243, 3
  %add181 = add nsw i32 %mul179, %add180
  %arrayidx182 = getelementptr inbounds i32, i32* %240, i32 %add181
  %244 = load i32, i32* %arrayidx182, align 4, !tbaa !6
  store i32 %244, i32* %e, align 4, !tbaa !6
  br label %for.inc183

for.inc183:                                       ; preds = %for.body171
  %245 = load i32, i32* %j, align 4, !tbaa !6
  %inc184 = add nsw i32 %245, 1
  store i32 %inc184, i32* %j, align 4, !tbaa !6
  br label %for.cond168

for.end185:                                       ; preds = %for.cond168
  %246 = load i32, i32* %a, align 4, !tbaa !6
  %247 = load i32, i32* %b, align 4, !tbaa !6
  %add186 = add nsw i32 %246, %247
  %248 = load i32, i32* %c, align 4, !tbaa !6
  %add187 = add nsw i32 %add186, %248
  %249 = load i32, i32* %d, align 4, !tbaa !6
  %add188 = add nsw i32 %add187, %249
  %250 = load i32, i32* %e, align 4, !tbaa !6
  %add189 = add nsw i32 %add188, %250
  %251 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %252 = load i32, i32* %i, align 4, !tbaa !6
  %253 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul190 = mul nsw i32 %252, %253
  %254 = load i32, i32* %j, align 4, !tbaa !6
  %add191 = add nsw i32 %mul190, %254
  %arrayidx192 = getelementptr inbounds i32, i32* %251, i32 %add191
  store i32 %add189, i32* %arrayidx192, align 4, !tbaa !6
  %255 = load i32, i32* %b, align 4, !tbaa !6
  %256 = load i32, i32* %c, align 4, !tbaa !6
  %add193 = add nsw i32 %255, %256
  %257 = load i32, i32* %d, align 4, !tbaa !6
  %add194 = add nsw i32 %add193, %257
  %258 = load i32, i32* %e, align 4, !tbaa !6
  %add195 = add nsw i32 %add194, %258
  %259 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %260 = load i32, i32* %i, align 4, !tbaa !6
  %261 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul196 = mul nsw i32 %260, %261
  %262 = load i32, i32* %j, align 4, !tbaa !6
  %add197 = add nsw i32 %262, 1
  %add198 = add nsw i32 %mul196, %add197
  %arrayidx199 = getelementptr inbounds i32, i32* %259, i32 %add198
  store i32 %add195, i32* %arrayidx199, align 4, !tbaa !6
  %263 = load i32, i32* %c, align 4, !tbaa !6
  %264 = load i32, i32* %d, align 4, !tbaa !6
  %add200 = add nsw i32 %263, %264
  %265 = load i32, i32* %e, align 4, !tbaa !6
  %add201 = add nsw i32 %add200, %265
  %266 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %267 = load i32, i32* %i, align 4, !tbaa !6
  %268 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul202 = mul nsw i32 %267, %268
  %269 = load i32, i32* %j, align 4, !tbaa !6
  %add203 = add nsw i32 %269, 2
  %add204 = add nsw i32 %mul202, %add203
  %arrayidx205 = getelementptr inbounds i32, i32* %266, i32 %add204
  store i32 %add201, i32* %arrayidx205, align 4, !tbaa !6
  br label %for.inc206

for.inc206:                                       ; preds = %for.end185
  %270 = load i32, i32* %i, align 4, !tbaa !6
  %inc207 = add nsw i32 %270, 1
  store i32 %inc207, i32* %i, align 4, !tbaa !6
  br label %for.cond141

for.end208:                                       ; preds = %for.cond141
  %271 = bitcast i32* %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %271) #5
  %272 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #5
  %273 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #5
  %274 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #5
  %275 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #5
  %276 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #5
  %277 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #5
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #3 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: nounwind
define internal void @copy_tile_highbd(i32 %width, i32 %height, i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride) #0 {
entry:
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds i16, i16* %4, i32 %mul
  %7 = bitcast i16* %add.ptr to i8*
  %8 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 %9, %10
  %add.ptr2 = getelementptr inbounds i16, i16* %8, i32 %mul1
  %11 = bitcast i16* %add.ptr2 to i8*
  %12 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul3 = mul i32 %12, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %7, i8* align 2 %11, i32 %mul3, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal void @copy_tile_lowbd(i32 %width, i32 %height, i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride) #0 {
entry:
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %mul
  %7 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %9 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 %8, %9
  %add.ptr2 = getelementptr inbounds i8, i8* %7, i32 %mul1
  %10 = load i32, i32* %width.addr, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %add.ptr2, i32 %10, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal void @wiener_filter_stripe(%struct.RestorationUnitInfo* %rui, i32 %stripe_width, i32 %stripe_height, i32 %procunit_width, i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32* %tmpbuf, i32 %bit_depth) #0 {
entry:
  %rui.addr = alloca %struct.RestorationUnitInfo*, align 4
  %stripe_width.addr = alloca i32, align 4
  %stripe_height.addr = alloca i32, align 4
  %procunit_width.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %bit_depth.addr = alloca i32, align 4
  %conv_params = alloca %struct.ConvolveParams, align 4
  %j = alloca i32, align 4
  %w = alloca i32, align 4
  %src_p = alloca i8*, align 4
  %dst_p = alloca i8*, align 4
  store %struct.RestorationUnitInfo* %rui, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  store i32 %stripe_width, i32* %stripe_width.addr, align 4, !tbaa !6
  store i32 %stripe_height, i32* %stripe_height.addr, align 4, !tbaa !6
  store i32 %procunit_width, i32* %procunit_width.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  %0 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  %1 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %2 = bitcast %struct.ConvolveParams* %conv_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 44, i8* %2) #5
  call void @get_conv_params_wiener(%struct.ConvolveParams* sret align 4 %conv_params, i32 8)
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %j, align 4, !tbaa !6
  %5 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  %9 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %sub = sub nsw i32 %9, %10
  %add = add nsw i32 %sub, 15
  %and = and i32 %add, -16
  %cmp1 = icmp slt i32 %8, %and
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %11 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %12 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %sub2 = sub nsw i32 %12, %13
  %add3 = add nsw i32 %sub2, 15
  %and4 = and i32 %add3, -16
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %11, %cond.true ], [ %and4, %cond.false ]
  store i32 %cond, i32* %w, align 4, !tbaa !6
  %14 = bitcast i8** %src_p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %15, i32 %16
  store i8* %add.ptr, i8** %src_p, align 4, !tbaa !2
  %17 = bitcast i8** %dst_p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr5 = getelementptr inbounds i8, i8* %18, i32 %19
  store i8* %add.ptr5, i8** %dst_p, align 4, !tbaa !2
  %20 = load i8*, i8** %src_p, align 4, !tbaa !2
  %21 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %22 = load i8*, i8** %dst_p, align 4, !tbaa !2
  %23 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %24 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %wiener_info = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %24, i32 0, i32 2
  %hfilter = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %wiener_info, i32 0, i32 1
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %hfilter, i32 0, i32 0
  %25 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %wiener_info6 = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %25, i32 0, i32 2
  %vfilter = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %wiener_info6, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [8 x i16], [8 x i16]* %vfilter, i32 0, i32 0
  %26 = load i32, i32* %w, align 4, !tbaa !6
  %27 = load i32, i32* %stripe_height.addr, align 4, !tbaa !6
  call void @av1_wiener_convolve_add_src_c(i8* %20, i32 %21, i8* %22, i32 %23, i16* %arraydecay, i32 16, i16* %arraydecay7, i32 16, i32 %26, i32 %27, %struct.ConvolveParams* %conv_params)
  %28 = bitcast i8** %dst_p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i8** %src_p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  %30 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %31 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  %32 = load i32, i32* %j, align 4, !tbaa !6
  %add8 = add nsw i32 %32, %31
  store i32 %add8, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %33 = bitcast %struct.ConvolveParams* %conv_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 44, i8* %33) #5
  ret void
}

; Function Attrs: nounwind
define internal void @sgrproj_filter_stripe(%struct.RestorationUnitInfo* %rui, i32 %stripe_width, i32 %stripe_height, i32 %procunit_width, i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32* %tmpbuf, i32 %bit_depth) #0 {
entry:
  %rui.addr = alloca %struct.RestorationUnitInfo*, align 4
  %stripe_width.addr = alloca i32, align 4
  %stripe_height.addr = alloca i32, align 4
  %procunit_width.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %bit_depth.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %w = alloca i32, align 4
  store %struct.RestorationUnitInfo* %rui, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  store i32 %stripe_width, i32* %stripe_width.addr, align 4, !tbaa !6
  store i32 %stripe_height, i32* %stripe_height.addr, align 4, !tbaa !6
  store i32 %procunit_width, i32* %procunit_width.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  %0 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %j, align 4, !tbaa !6
  %3 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  %7 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %8 = load i32, i32* %j, align 4, !tbaa !6
  %sub = sub nsw i32 %7, %8
  %cmp1 = icmp slt i32 %6, %sub
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %9 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %10 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %sub2 = sub nsw i32 %10, %11
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %9, %cond.true ], [ %sub2, %cond.false ]
  store i32 %cond, i32* %w, align 4, !tbaa !6
  %12 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %12, i32 %13
  %14 = load i32, i32* %w, align 4, !tbaa !6
  %15 = load i32, i32* %stripe_height.addr, align 4, !tbaa !6
  %16 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %17 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %sgrproj_info = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %17, i32 0, i32 3
  %ep = getelementptr inbounds %struct.SgrprojInfo, %struct.SgrprojInfo* %sgrproj_info, i32 0, i32 0
  %18 = load i32, i32* %ep, align 16, !tbaa !85
  %19 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %sgrproj_info3 = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %19, i32 0, i32 3
  %xqd = getelementptr inbounds %struct.SgrprojInfo, %struct.SgrprojInfo* %sgrproj_info3, i32 0, i32 1
  %arraydecay = getelementptr inbounds [2 x i32], [2 x i32]* %xqd, i32 0, i32 0
  %20 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr4 = getelementptr inbounds i8, i8* %20, i32 %21
  %22 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %23 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  %24 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  call void @av1_apply_selfguided_restoration_c(i8* %add.ptr, i32 %14, i32 %15, i32 %16, i32 %18, i32* %arraydecay, i8* %add.ptr4, i32 %22, i32* %23, i32 %24, i32 0)
  %25 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %26 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %27, %26
  store i32 %add, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal void @wiener_filter_stripe_highbd(%struct.RestorationUnitInfo* %rui, i32 %stripe_width, i32 %stripe_height, i32 %procunit_width, i8* %src8, i32 %src_stride, i8* %dst8, i32 %dst_stride, i32* %tmpbuf, i32 %bit_depth) #0 {
entry:
  %rui.addr = alloca %struct.RestorationUnitInfo*, align 4
  %stripe_width.addr = alloca i32, align 4
  %stripe_height.addr = alloca i32, align 4
  %procunit_width.addr = alloca i32, align 4
  %src8.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %bit_depth.addr = alloca i32, align 4
  %conv_params = alloca %struct.ConvolveParams, align 4
  %j = alloca i32, align 4
  %w = alloca i32, align 4
  %src8_p = alloca i8*, align 4
  %dst8_p = alloca i8*, align 4
  store %struct.RestorationUnitInfo* %rui, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  store i32 %stripe_width, i32* %stripe_width.addr, align 4, !tbaa !6
  store i32 %stripe_height, i32* %stripe_height.addr, align 4, !tbaa !6
  store i32 %procunit_width, i32* %procunit_width.addr, align 4, !tbaa !6
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  %0 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  %1 = bitcast %struct.ConvolveParams* %conv_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 44, i8* %1) #5
  %2 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  call void @get_conv_params_wiener(%struct.ConvolveParams* sret align 4 %conv_params, i32 %2)
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %j, align 4, !tbaa !6
  %5 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  %9 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %sub = sub nsw i32 %9, %10
  %add = add nsw i32 %sub, 15
  %and = and i32 %add, -16
  %cmp1 = icmp slt i32 %8, %and
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %11 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %12 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %sub2 = sub nsw i32 %12, %13
  %add3 = add nsw i32 %sub2, 15
  %and4 = and i32 %add3, -16
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %11, %cond.true ], [ %and4, %cond.false ]
  store i32 %cond, i32* %w, align 4, !tbaa !6
  %14 = bitcast i8** %src8_p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %15, i32 %16
  store i8* %add.ptr, i8** %src8_p, align 4, !tbaa !2
  %17 = bitcast i8** %dst8_p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr5 = getelementptr inbounds i8, i8* %18, i32 %19
  store i8* %add.ptr5, i8** %dst8_p, align 4, !tbaa !2
  %20 = load i8*, i8** %src8_p, align 4, !tbaa !2
  %21 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %22 = load i8*, i8** %dst8_p, align 4, !tbaa !2
  %23 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %24 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %wiener_info = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %24, i32 0, i32 2
  %hfilter = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %wiener_info, i32 0, i32 1
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %hfilter, i32 0, i32 0
  %25 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %wiener_info6 = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %25, i32 0, i32 2
  %vfilter = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %wiener_info6, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [8 x i16], [8 x i16]* %vfilter, i32 0, i32 0
  %26 = load i32, i32* %w, align 4, !tbaa !6
  %27 = load i32, i32* %stripe_height.addr, align 4, !tbaa !6
  %28 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  call void @av1_highbd_wiener_convolve_add_src_c(i8* %20, i32 %21, i8* %22, i32 %23, i16* %arraydecay, i32 16, i16* %arraydecay7, i32 16, i32 %26, i32 %27, %struct.ConvolveParams* %conv_params, i32 %28)
  %29 = bitcast i8** %dst8_p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  %30 = bitcast i8** %src8_p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  %31 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %32 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %add8 = add nsw i32 %33, %32
  store i32 %add8, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %34 = bitcast %struct.ConvolveParams* %conv_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 44, i8* %34) #5
  ret void
}

; Function Attrs: nounwind
define internal void @sgrproj_filter_stripe_highbd(%struct.RestorationUnitInfo* %rui, i32 %stripe_width, i32 %stripe_height, i32 %procunit_width, i8* %src8, i32 %src_stride, i8* %dst8, i32 %dst_stride, i32* %tmpbuf, i32 %bit_depth) #0 {
entry:
  %rui.addr = alloca %struct.RestorationUnitInfo*, align 4
  %stripe_width.addr = alloca i32, align 4
  %stripe_height.addr = alloca i32, align 4
  %procunit_width.addr = alloca i32, align 4
  %src8.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %tmpbuf.addr = alloca i32*, align 4
  %bit_depth.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %w = alloca i32, align 4
  store %struct.RestorationUnitInfo* %rui, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  store i32 %stripe_width, i32* %stripe_width.addr, align 4, !tbaa !6
  store i32 %stripe_height, i32* %stripe_height.addr, align 4, !tbaa !6
  store i32 %procunit_width, i32* %procunit_width.addr, align 4, !tbaa !6
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32* %tmpbuf, i32** %tmpbuf.addr, align 4, !tbaa !2
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !6
  %2 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  %6 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %7 = load i32, i32* %j, align 4, !tbaa !6
  %sub = sub nsw i32 %6, %7
  %cmp1 = icmp slt i32 %5, %sub
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %8 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %9 = load i32, i32* %stripe_width.addr, align 4, !tbaa !6
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %sub2 = sub nsw i32 %9, %10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %8, %cond.true ], [ %sub2, %cond.false ]
  store i32 %cond, i32* %w, align 4, !tbaa !6
  %11 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load i32, i32* %w, align 4, !tbaa !6
  %14 = load i32, i32* %stripe_height.addr, align 4, !tbaa !6
  %15 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %16 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %sgrproj_info = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %16, i32 0, i32 3
  %ep = getelementptr inbounds %struct.SgrprojInfo, %struct.SgrprojInfo* %sgrproj_info, i32 0, i32 0
  %17 = load i32, i32* %ep, align 16, !tbaa !85
  %18 = load %struct.RestorationUnitInfo*, %struct.RestorationUnitInfo** %rui.addr, align 4, !tbaa !2
  %sgrproj_info3 = getelementptr inbounds %struct.RestorationUnitInfo, %struct.RestorationUnitInfo* %18, i32 0, i32 3
  %xqd = getelementptr inbounds %struct.SgrprojInfo, %struct.SgrprojInfo* %sgrproj_info3, i32 0, i32 1
  %arraydecay = getelementptr inbounds [2 x i32], [2 x i32]* %xqd, i32 0, i32 0
  %19 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %20 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr4 = getelementptr inbounds i8, i8* %19, i32 %20
  %21 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %22 = load i32*, i32** %tmpbuf.addr, align 4, !tbaa !2
  %23 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  call void @av1_apply_selfguided_restoration_c(i8* %add.ptr, i32 %13, i32 %14, i32 %15, i32 %17, i32* %arraydecay, i8* %add.ptr4, i32 %21, i32* %22, i32 %23, i32 1)
  %24 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %25 = load i32, i32* %procunit_width.addr, align 4, !tbaa !6
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %26, %25
  store i32 %add, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @get_conv_params_wiener(%struct.ConvolveParams* noalias sret align 4 %agg.result, i32 %bd) #3 {
entry:
  %bd.addr = alloca i32, align 4
  %intbufrange = alloca i32, align 4
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 0
  store i32 0, i32* %do_average, align 4, !tbaa !86
  %is_compound = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 6
  store i32 0, i32* %is_compound, align 4, !tbaa !88
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 3
  store i32 3, i32* %round_0, align 4, !tbaa !89
  %round_01 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 3
  %1 = load i32, i32* %round_01, align 4, !tbaa !89
  %sub = sub nsw i32 14, %1
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 4
  store i32 %sub, i32* %round_1, align 4, !tbaa !90
  %2 = bitcast i32* %intbufrange to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add = add nsw i32 %3, 7
  %round_02 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 3
  %4 = load i32, i32* %round_02, align 4, !tbaa !89
  %sub3 = sub nsw i32 %add, %4
  %add4 = add nsw i32 %sub3, 2
  store i32 %add4, i32* %intbufrange, align 4, !tbaa !6
  %5 = load i32, i32* %intbufrange, align 4, !tbaa !6
  %cmp = icmp sgt i32 %5, 16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load i32, i32* %intbufrange, align 4, !tbaa !6
  %sub5 = sub nsw i32 %6, 16
  %round_06 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 3
  %7 = load i32, i32* %round_06, align 4, !tbaa !89
  %add7 = add nsw i32 %7, %sub5
  store i32 %add7, i32* %round_06, align 4, !tbaa !89
  %8 = load i32, i32* %intbufrange, align 4, !tbaa !6
  %sub8 = sub nsw i32 %8, 16
  %round_19 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 4
  %9 = load i32, i32* %round_19, align 4, !tbaa !90
  %sub10 = sub nsw i32 %9, %sub8
  store i32 %sub10, i32* %round_19, align 4, !tbaa !90
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %dst = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 1
  store i16* null, i16** %dst, align 4, !tbaa !91
  %dst_stride = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 2
  store i32 0, i32* %dst_stride, align 4, !tbaa !92
  %plane = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 5
  store i32 0, i32* %plane, align 4, !tbaa !93
  %10 = bitcast i32* %intbufrange to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  ret void
}

declare void @av1_wiener_convolve_add_src_c(i8*, i32, i8*, i32, i16*, i32, i16*, i32, i32, i32, %struct.ConvolveParams*) #2

declare void @av1_highbd_wiener_convolve_add_src_c(i8*, i32, i8*, i32, i16*, i32, i16*, i32, i32, i32, %struct.ConvolveParams*, i32) #2

; Function Attrs: nounwind
define internal void @save_deblock_boundary_lines(%struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, i32 %plane, i32 %row, i32 %stripe, i32 %use_highbd, i32 %is_above, %struct.RestorationStripeBoundaries* %boundaries) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %plane.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  %stripe.addr = alloca i32, align 4
  %use_highbd.addr = alloca i32, align 4
  %is_above.addr = alloca i32, align 4
  %boundaries.addr = alloca %struct.RestorationStripeBoundaries*, align 4
  %is_uv = alloca i32, align 4
  %src_buf = alloca i8*, align 4
  %src_stride = alloca i32, align 4
  %src_rows = alloca i8*, align 4
  %bdry_buf = alloca i8*, align 4
  %bdry_start = alloca i8*, align 4
  %bdry_stride = alloca i32, align 4
  %bdry_rows = alloca i8*, align 4
  %lines_to_save = alloca i32, align 4
  %upscaled_width = alloca i32, align 4
  %line_bytes = alloca i32, align 4
  %ss_x = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  store i32 %stripe, i32* %stripe.addr, align 4, !tbaa !6
  store i32 %use_highbd, i32* %use_highbd.addr, align 4, !tbaa !6
  store i32 %is_above, i32* %is_above.addr, align 4, !tbaa !6
  store %struct.RestorationStripeBoundaries* %boundaries, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %0 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %1, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !6
  %2 = bitcast i8** %src_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %5 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %4, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %5 to [3 x i8*]*
  %6 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %6
  %7 = load i8*, i8** %arrayidx, align 4, !tbaa !47
  %8 = ptrtoint i8* %7 to i32
  %shl = shl i32 %8, 1
  %9 = inttoptr i32 %shl to i16*
  %10 = bitcast i16* %9 to i8*
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %12 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 5
  %buffers1 = bitcast %union.anon.8* %12 to [3 x i8*]*
  %13 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers1, i32 0, i32 %13
  %14 = load i8*, i8** %arrayidx2, align 4, !tbaa !47
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %10, %cond.true ], [ %14, %cond.false ]
  store i8* %cond, i8** %src_buf, align 4, !tbaa !2
  %15 = bitcast i32* %src_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %17 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %16, i32 0, i32 4
  %strides = bitcast %union.anon.6* %17 to [2 x i32]*
  %18 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx3, align 4, !tbaa !47
  %20 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl4 = shl i32 %19, %20
  store i32 %shl4, i32* %src_stride, align 4, !tbaa !6
  %21 = bitcast i8** %src_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load i8*, i8** %src_buf, align 4, !tbaa !2
  %23 = load i32, i32* %row.addr, align 4, !tbaa !6
  %24 = load i32, i32* %src_stride, align 4, !tbaa !6
  %mul = mul nsw i32 %23, %24
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 %mul
  store i8* %add.ptr, i8** %src_rows, align 4, !tbaa !2
  %25 = bitcast i8** %bdry_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  %26 = load i32, i32* %is_above.addr, align 4, !tbaa !6
  %tobool5 = icmp ne i32 %26, 0
  br i1 %tobool5, label %cond.true6, label %cond.false7

cond.true6:                                       ; preds = %cond.end
  %27 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %stripe_boundary_above = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %27, i32 0, i32 0
  %28 = load i8*, i8** %stripe_boundary_above, align 4, !tbaa !54
  br label %cond.end8

cond.false7:                                      ; preds = %cond.end
  %29 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %stripe_boundary_below = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %29, i32 0, i32 1
  %30 = load i8*, i8** %stripe_boundary_below, align 4, !tbaa !55
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false7, %cond.true6
  %cond9 = phi i8* [ %28, %cond.true6 ], [ %30, %cond.false7 ]
  store i8* %cond9, i8** %bdry_buf, align 4, !tbaa !2
  %31 = bitcast i8** %bdry_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #5
  %32 = load i8*, i8** %bdry_buf, align 4, !tbaa !2
  %33 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl10 = shl i32 4, %33
  %add.ptr11 = getelementptr inbounds i8, i8* %32, i32 %shl10
  store i8* %add.ptr11, i8** %bdry_start, align 4, !tbaa !2
  %34 = bitcast i32* %bdry_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #5
  %35 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %stripe_boundary_stride = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %35, i32 0, i32 2
  %36 = load i32, i32* %stripe_boundary_stride, align 4, !tbaa !53
  %37 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl12 = shl i32 %36, %37
  store i32 %shl12, i32* %bdry_stride, align 4, !tbaa !6
  %38 = bitcast i8** %bdry_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #5
  %39 = load i8*, i8** %bdry_start, align 4, !tbaa !2
  %40 = load i32, i32* %stripe.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 2, %40
  %41 = load i32, i32* %bdry_stride, align 4, !tbaa !6
  %mul14 = mul nsw i32 %mul13, %41
  %add.ptr15 = getelementptr inbounds i8, i8* %39, i32 %mul14
  store i8* %add.ptr15, i8** %bdry_rows, align 4, !tbaa !2
  %42 = bitcast i32* %lines_to_save to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #5
  %43 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %44 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %43, i32 0, i32 3
  %crop_heights = bitcast %union.anon.4* %44 to [2 x i32]*
  %45 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx16, align 4, !tbaa !47
  %47 = load i32, i32* %row.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %46, %47
  %cmp17 = icmp slt i32 2, %sub
  br i1 %cmp17, label %cond.true19, label %cond.false20

cond.true19:                                      ; preds = %cond.end8
  br label %cond.end24

cond.false20:                                     ; preds = %cond.end8
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %49 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 3
  %crop_heights21 = bitcast %union.anon.4* %49 to [2 x i32]*
  %50 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights21, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx22, align 4, !tbaa !47
  %52 = load i32, i32* %row.addr, align 4, !tbaa !6
  %sub23 = sub nsw i32 %51, %52
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false20, %cond.true19
  %cond25 = phi i32 [ 2, %cond.true19 ], [ %sub23, %cond.false20 ]
  store i32 %cond25, i32* %lines_to_save, align 4, !tbaa !6
  %53 = bitcast i32* %upscaled_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #5
  %54 = bitcast i32* %line_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #5
  %55 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_superres_scaled(%struct.AV1Common* %55)
  %tobool26 = icmp ne i32 %call, 0
  br i1 %tobool26, label %if.then, label %if.else40

if.then:                                          ; preds = %cond.end24
  %56 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #5
  %57 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool27 = icmp ne i32 %57, 0
  br i1 %tobool27, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.then
  %58 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %58, i32 0, i32 37
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 32
  %59 = load i32, i32* %subsampling_x, align 16, !tbaa !8
  %tobool28 = icmp ne i32 %59, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.then
  %60 = phi i1 [ false, %if.then ], [ %tobool28, %land.rhs ]
  %land.ext = zext i1 %60 to i32
  store i32 %land.ext, i32* %ss_x, align 4, !tbaa !6
  %61 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %61, i32 0, i32 6
  %62 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !37
  %63 = load i32, i32* %ss_x, align 4, !tbaa !6
  %add = add nsw i32 %62, %63
  %64 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shr = ashr i32 %add, %64
  store i32 %shr, i32* %upscaled_width, align 4, !tbaa !6
  %65 = load i32, i32* %upscaled_width, align 4, !tbaa !6
  %66 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl29 = shl i32 %65, %66
  store i32 %shl29, i32* %line_bytes, align 4, !tbaa !6
  %67 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool30 = icmp ne i32 %67, 0
  br i1 %tobool30, label %if.then31, label %if.else

if.then31:                                        ; preds = %land.end
  %68 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %69 = load i8*, i8** %src_rows, align 4, !tbaa !2
  %70 = ptrtoint i8* %69 to i32
  %shr32 = lshr i32 %70, 1
  %71 = inttoptr i32 %shr32 to i8*
  %72 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %73 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %72, i32 0, i32 4
  %strides33 = bitcast %union.anon.6* %73 to [2 x i32]*
  %74 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds [2 x i32], [2 x i32]* %strides33, i32 0, i32 %74
  %75 = load i32, i32* %arrayidx34, align 4, !tbaa !47
  %76 = load i8*, i8** %bdry_rows, align 4, !tbaa !2
  %77 = ptrtoint i8* %76 to i32
  %shr35 = lshr i32 %77, 1
  %78 = inttoptr i32 %shr35 to i8*
  %79 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %stripe_boundary_stride36 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %79, i32 0, i32 2
  %80 = load i32, i32* %stripe_boundary_stride36, align 4, !tbaa !53
  %81 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %82 = load i32, i32* %lines_to_save, align 4, !tbaa !6
  call void @av1_upscale_normative_rows(%struct.AV1Common* %68, i8* %71, i32 %75, i8* %78, i32 %80, i32 %81, i32 %82)
  br label %if.end

if.else:                                          ; preds = %land.end
  %83 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %84 = load i8*, i8** %src_rows, align 4, !tbaa !2
  %85 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %86 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %85, i32 0, i32 4
  %strides37 = bitcast %union.anon.6* %86 to [2 x i32]*
  %87 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds [2 x i32], [2 x i32]* %strides37, i32 0, i32 %87
  %88 = load i32, i32* %arrayidx38, align 4, !tbaa !47
  %89 = load i8*, i8** %bdry_rows, align 4, !tbaa !2
  %90 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %stripe_boundary_stride39 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %90, i32 0, i32 2
  %91 = load i32, i32* %stripe_boundary_stride39, align 4, !tbaa !53
  %92 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %93 = load i32, i32* %lines_to_save, align 4, !tbaa !6
  call void @av1_upscale_normative_rows(%struct.AV1Common* %83, i8* %84, i32 %88, i8* %89, i32 %91, i32 %92, i32 %93)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then31
  %94 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  br label %if.end49

if.else40:                                        ; preds = %cond.end24
  %95 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %96 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %95, i32 0, i32 2
  %crop_widths = bitcast %union.anon.2* %96 to [2 x i32]*
  %97 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths, i32 0, i32 %97
  %98 = load i32, i32* %arrayidx41, align 4, !tbaa !47
  store i32 %98, i32* %upscaled_width, align 4, !tbaa !6
  %99 = load i32, i32* %upscaled_width, align 4, !tbaa !6
  %100 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl42 = shl i32 %99, %100
  store i32 %shl42, i32* %line_bytes, align 4, !tbaa !6
  %101 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else40
  %102 = load i32, i32* %i, align 4, !tbaa !6
  %103 = load i32, i32* %lines_to_save, align 4, !tbaa !6
  %cmp43 = icmp slt i32 %102, %103
  br i1 %cmp43, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %104 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %105 = load i8*, i8** %bdry_rows, align 4, !tbaa !2
  %106 = load i32, i32* %i, align 4, !tbaa !6
  %107 = load i32, i32* %bdry_stride, align 4, !tbaa !6
  %mul45 = mul nsw i32 %106, %107
  %add.ptr46 = getelementptr inbounds i8, i8* %105, i32 %mul45
  %108 = load i8*, i8** %src_rows, align 4, !tbaa !2
  %109 = load i32, i32* %i, align 4, !tbaa !6
  %110 = load i32, i32* %src_stride, align 4, !tbaa !6
  %mul47 = mul nsw i32 %109, %110
  %add.ptr48 = getelementptr inbounds i8, i8* %108, i32 %mul47
  %111 = load i32, i32* %line_bytes, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr46, i8* align 1 %add.ptr48, i32 %111, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %112 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %112, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end49

if.end49:                                         ; preds = %for.end, %if.end
  %113 = load i32, i32* %lines_to_save, align 4, !tbaa !6
  %cmp50 = icmp eq i32 %113, 1
  br i1 %cmp50, label %if.then52, label %if.end54

if.then52:                                        ; preds = %if.end49
  %114 = load i8*, i8** %bdry_rows, align 4, !tbaa !2
  %115 = load i32, i32* %bdry_stride, align 4, !tbaa !6
  %add.ptr53 = getelementptr inbounds i8, i8* %114, i32 %115
  %116 = load i8*, i8** %bdry_rows, align 4, !tbaa !2
  %117 = load i32, i32* %line_bytes, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr53, i8* align 1 %116, i32 %117, i1 false)
  br label %if.end54

if.end54:                                         ; preds = %if.then52, %if.end49
  %118 = load i8*, i8** %bdry_rows, align 4, !tbaa !2
  %119 = load i32, i32* %upscaled_width, align 4, !tbaa !6
  %120 = load i32, i32* %bdry_stride, align 4, !tbaa !6
  %121 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  call void @extend_lines(i8* %118, i32 %119, i32 2, i32 %120, i32 4, i32 %121)
  %122 = bitcast i32* %line_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  %123 = bitcast i32* %upscaled_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  %124 = bitcast i32* %lines_to_save to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  %125 = bitcast i8** %bdry_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #5
  %126 = bitcast i32* %bdry_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #5
  %127 = bitcast i8** %bdry_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #5
  %128 = bitcast i8** %bdry_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #5
  %129 = bitcast i8** %src_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #5
  %130 = bitcast i32* %src_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #5
  %131 = bitcast i8** %src_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #5
  %132 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #5
  ret void
}

; Function Attrs: nounwind
define internal void @save_cdef_boundary_lines(%struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, i32 %plane, i32 %row, i32 %stripe, i32 %use_highbd, i32 %is_above, %struct.RestorationStripeBoundaries* %boundaries) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %plane.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  %stripe.addr = alloca i32, align 4
  %use_highbd.addr = alloca i32, align 4
  %is_above.addr = alloca i32, align 4
  %boundaries.addr = alloca %struct.RestorationStripeBoundaries*, align 4
  %is_uv = alloca i32, align 4
  %src_buf = alloca i8*, align 4
  %src_stride = alloca i32, align 4
  %src_rows = alloca i8*, align 4
  %bdry_buf = alloca i8*, align 4
  %bdry_start = alloca i8*, align 4
  %bdry_stride = alloca i32, align 4
  %bdry_rows = alloca i8*, align 4
  %src_width = alloca i32, align 4
  %ss_x = alloca i32, align 4
  %upscaled_width = alloca i32, align 4
  %line_bytes = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  store i32 %stripe, i32* %stripe.addr, align 4, !tbaa !6
  store i32 %use_highbd, i32* %use_highbd.addr, align 4, !tbaa !6
  store i32 %is_above, i32* %is_above.addr, align 4, !tbaa !6
  store %struct.RestorationStripeBoundaries* %boundaries, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %0 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %1, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !6
  %2 = bitcast i8** %src_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %5 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %4, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %5 to [3 x i8*]*
  %6 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %6
  %7 = load i8*, i8** %arrayidx, align 4, !tbaa !47
  %8 = ptrtoint i8* %7 to i32
  %shl = shl i32 %8, 1
  %9 = inttoptr i32 %shl to i16*
  %10 = bitcast i16* %9 to i8*
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %12 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 5
  %buffers1 = bitcast %union.anon.8* %12 to [3 x i8*]*
  %13 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers1, i32 0, i32 %13
  %14 = load i8*, i8** %arrayidx2, align 4, !tbaa !47
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %10, %cond.true ], [ %14, %cond.false ]
  store i8* %cond, i8** %src_buf, align 4, !tbaa !2
  %15 = bitcast i32* %src_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %17 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %16, i32 0, i32 4
  %strides = bitcast %union.anon.6* %17 to [2 x i32]*
  %18 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx3, align 4, !tbaa !47
  %20 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl4 = shl i32 %19, %20
  store i32 %shl4, i32* %src_stride, align 4, !tbaa !6
  %21 = bitcast i8** %src_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load i8*, i8** %src_buf, align 4, !tbaa !2
  %23 = load i32, i32* %row.addr, align 4, !tbaa !6
  %24 = load i32, i32* %src_stride, align 4, !tbaa !6
  %mul = mul nsw i32 %23, %24
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 %mul
  store i8* %add.ptr, i8** %src_rows, align 4, !tbaa !2
  %25 = bitcast i8** %bdry_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  %26 = load i32, i32* %is_above.addr, align 4, !tbaa !6
  %tobool5 = icmp ne i32 %26, 0
  br i1 %tobool5, label %cond.true6, label %cond.false7

cond.true6:                                       ; preds = %cond.end
  %27 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %stripe_boundary_above = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %27, i32 0, i32 0
  %28 = load i8*, i8** %stripe_boundary_above, align 4, !tbaa !54
  br label %cond.end8

cond.false7:                                      ; preds = %cond.end
  %29 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %stripe_boundary_below = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %29, i32 0, i32 1
  %30 = load i8*, i8** %stripe_boundary_below, align 4, !tbaa !55
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false7, %cond.true6
  %cond9 = phi i8* [ %28, %cond.true6 ], [ %30, %cond.false7 ]
  store i8* %cond9, i8** %bdry_buf, align 4, !tbaa !2
  %31 = bitcast i8** %bdry_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #5
  %32 = load i8*, i8** %bdry_buf, align 4, !tbaa !2
  %33 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl10 = shl i32 4, %33
  %add.ptr11 = getelementptr inbounds i8, i8* %32, i32 %shl10
  store i8* %add.ptr11, i8** %bdry_start, align 4, !tbaa !2
  %34 = bitcast i32* %bdry_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #5
  %35 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries.addr, align 4, !tbaa !2
  %stripe_boundary_stride = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %35, i32 0, i32 2
  %36 = load i32, i32* %stripe_boundary_stride, align 4, !tbaa !53
  %37 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl12 = shl i32 %36, %37
  store i32 %shl12, i32* %bdry_stride, align 4, !tbaa !6
  %38 = bitcast i8** %bdry_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #5
  %39 = load i8*, i8** %bdry_start, align 4, !tbaa !2
  %40 = load i32, i32* %stripe.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 2, %40
  %41 = load i32, i32* %bdry_stride, align 4, !tbaa !6
  %mul14 = mul nsw i32 %mul13, %41
  %add.ptr15 = getelementptr inbounds i8, i8* %39, i32 %mul14
  store i8* %add.ptr15, i8** %bdry_rows, align 4, !tbaa !2
  %42 = bitcast i32* %src_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #5
  %43 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %44 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %43, i32 0, i32 2
  %crop_widths = bitcast %union.anon.2* %44 to [2 x i32]*
  %45 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx16, align 4, !tbaa !47
  store i32 %46, i32* %src_width, align 4, !tbaa !6
  %47 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  %48 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool17 = icmp ne i32 %48, 0
  br i1 %tobool17, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %cond.end8
  %49 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %49, i32 0, i32 37
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 32
  %50 = load i32, i32* %subsampling_x, align 16, !tbaa !8
  %tobool18 = icmp ne i32 %50, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %cond.end8
  %51 = phi i1 [ false, %cond.end8 ], [ %tobool18, %land.rhs ]
  %land.ext = zext i1 %51 to i32
  store i32 %land.ext, i32* %ss_x, align 4, !tbaa !6
  %52 = bitcast i32* %upscaled_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #5
  %53 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_superres_scaled(%struct.AV1Common* %53)
  %tobool19 = icmp ne i32 %call, 0
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %land.end
  %54 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %54, i32 0, i32 6
  %55 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !37
  %56 = load i32, i32* %ss_x, align 4, !tbaa !6
  %add = add nsw i32 %55, %56
  %57 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shr = ashr i32 %add, %57
  br label %cond.end22

cond.false21:                                     ; preds = %land.end
  %58 = load i32, i32* %src_width, align 4, !tbaa !6
  br label %cond.end22

cond.end22:                                       ; preds = %cond.false21, %cond.true20
  %cond23 = phi i32 [ %shr, %cond.true20 ], [ %58, %cond.false21 ]
  store i32 %cond23, i32* %upscaled_width, align 4, !tbaa !6
  %59 = bitcast i32* %line_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #5
  %60 = load i32, i32* %upscaled_width, align 4, !tbaa !6
  %61 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  %shl24 = shl i32 %60, %61
  store i32 %shl24, i32* %line_bytes, align 4, !tbaa !6
  %62 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end22
  %63 = load i32, i32* %i, align 4, !tbaa !6
  %cmp25 = icmp slt i32 %63, 2
  br i1 %cmp25, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %64 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %65 = load i8*, i8** %bdry_rows, align 4, !tbaa !2
  %66 = load i32, i32* %i, align 4, !tbaa !6
  %67 = load i32, i32* %bdry_stride, align 4, !tbaa !6
  %mul27 = mul nsw i32 %66, %67
  %add.ptr28 = getelementptr inbounds i8, i8* %65, i32 %mul27
  %68 = load i8*, i8** %src_rows, align 4, !tbaa !2
  %69 = load i32, i32* %line_bytes, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr28, i8* align 1 %68, i32 %69, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %70 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %70, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %71 = load i8*, i8** %bdry_rows, align 4, !tbaa !2
  %72 = load i32, i32* %upscaled_width, align 4, !tbaa !6
  %73 = load i32, i32* %bdry_stride, align 4, !tbaa !6
  %74 = load i32, i32* %use_highbd.addr, align 4, !tbaa !6
  call void @extend_lines(i8* %71, i32 %72, i32 2, i32 %73, i32 4, i32 %74)
  %75 = bitcast i32* %line_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #5
  %76 = bitcast i32* %upscaled_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #5
  %77 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #5
  %78 = bitcast i32* %src_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  %79 = bitcast i8** %bdry_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #5
  %80 = bitcast i32* %bdry_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #5
  %81 = bitcast i8** %bdry_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #5
  %82 = bitcast i8** %bdry_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #5
  %83 = bitcast i8** %src_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #5
  %84 = bitcast i32* %src_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #5
  %85 = bitcast i8** %src_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #5
  %86 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #5
  ret void
}

declare void @av1_upscale_normative_rows(%struct.AV1Common*, i8*, i32, i8*, i32, i32, i32) #2

; Function Attrs: nounwind
define internal void @extend_lines(i8* %buf, i32 %width, i32 %height, i32 %stride, i32 %extend, i32 %use_highbitdepth) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %extend.addr = alloca i32, align 4
  %use_highbitdepth.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %buf16 = alloca i16*, align 4
  store i8* %buf, i8** %buf.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %extend, i32* %extend.addr, align 4, !tbaa !6
  store i32 %use_highbitdepth, i32* %use_highbitdepth.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %use_highbitdepth.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %5 = bitcast i16** %buf16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %7 = bitcast i8* %6 to i16*
  store i16* %7, i16** %buf16, align 4, !tbaa !2
  %8 = load i16*, i16** %buf16, align 4, !tbaa !2
  %9 = load i32, i32* %extend.addr, align 4, !tbaa !6
  %idx.neg = sub i32 0, %9
  %add.ptr = getelementptr inbounds i16, i16* %8, i32 %idx.neg
  %10 = bitcast i16* %add.ptr to i8*
  %11 = load i16*, i16** %buf16, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %11, i32 0
  %12 = load i16, i16* %arrayidx, align 2, !tbaa !46
  %conv = zext i16 %12 to i32
  %13 = load i32, i32* %extend.addr, align 4, !tbaa !6
  %call = call i8* @aom_memset16(i8* %10, i32 %conv, i32 %13)
  %14 = load i16*, i16** %buf16, align 4, !tbaa !2
  %15 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add.ptr1 = getelementptr inbounds i16, i16* %14, i32 %15
  %16 = bitcast i16* %add.ptr1 to i8*
  %17 = load i16*, i16** %buf16, align 4, !tbaa !2
  %18 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %18, 1
  %arrayidx2 = getelementptr inbounds i16, i16* %17, i32 %sub
  %19 = load i16, i16* %arrayidx2, align 2, !tbaa !46
  %conv3 = zext i16 %19 to i32
  %20 = load i32, i32* %extend.addr, align 4, !tbaa !6
  %call4 = call i8* @aom_memset16(i8* %16, i32 %conv3, i32 %20)
  %21 = bitcast i16** %buf16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  br label %if.end

if.else:                                          ; preds = %for.body
  %22 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %23 = load i32, i32* %extend.addr, align 4, !tbaa !6
  %idx.neg5 = sub i32 0, %23
  %add.ptr6 = getelementptr inbounds i8, i8* %22, i32 %idx.neg5
  %24 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %24, i32 0
  %25 = load i8, i8* %arrayidx7, align 1, !tbaa !47
  %conv8 = zext i8 %25 to i32
  %26 = trunc i32 %conv8 to i8
  %27 = load i32, i32* %extend.addr, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr6, i8 %26, i32 %27, i1 false)
  %28 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %29 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add.ptr9 = getelementptr inbounds i8, i8* %28, i32 %29
  %30 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %31 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub10 = sub nsw i32 %31, 1
  %arrayidx11 = getelementptr inbounds i8, i8* %30, i32 %sub10
  %32 = load i8, i8* %arrayidx11, align 1, !tbaa !47
  %conv12 = zext i8 %32 to i32
  %33 = trunc i32 %conv12 to i8
  %34 = load i32, i32* %extend.addr, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr9, i8 %33, i32 %34, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %35 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %36 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %add.ptr13 = getelementptr inbounds i8, i8* %36, i32 %35
  store i8* %add.ptr13, i8** %buf.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare i8* @aom_memset16(i8*, i32, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 16288}
!9 = !{!"AV1Common", !10, i64 0, !12, i64 40, !7, i64 288, !7, i64 292, !7, i64 296, !7, i64 300, !7, i64 304, !7, i64 308, !4, i64 312, !13, i64 313, !4, i64 316, !7, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !14, i64 492, !4, i64 580, !4, i64 1284, !7, i64 1316, !7, i64 1320, !7, i64 1324, !15, i64 1328, !16, i64 1356, !17, i64 1420, !18, i64 10676, !3, i64 10848, !19, i64 10864, !20, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !21, i64 14880, !23, i64 15028, !24, i64 15168, !26, i64 15816, !4, i64 15836, !27, i64 16192, !3, i64 18128, !3, i64 18132, !30, i64 18136, !3, i64 18724, !31, i64 18728, !7, i64 18760, !4, i64 18764, !3, i64 18796, !7, i64 18800, !4, i64 18804, !4, i64 18836, !7, i64 18844, !7, i64 18848, !7, i64 18852, !7, i64 18856}
!10 = !{!"", !4, i64 0, !4, i64 1, !7, i64 4, !7, i64 8, !7, i64 12, !11, i64 16, !7, i64 32, !7, i64 36}
!11 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!12 = !{!"aom_internal_error_info", !4, i64 0, !7, i64 4, !4, i64 8, !7, i64 88, !4, i64 92}
!13 = !{!"_Bool", !4, i64 0}
!14 = !{!"scale_factors", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!15 = !{!"", !13, i64 0, !13, i64 1, !13, i64 2, !13, i64 3, !13, i64 4, !13, i64 5, !13, i64 6, !13, i64 7, !13, i64 8, !13, i64 9, !13, i64 10, !13, i64 11, !4, i64 12, !4, i64 13, !7, i64 16, !7, i64 20, !4, i64 24}
!16 = !{!"CommonModeInfoParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !7, i64 28, !4, i64 32, !3, i64 36, !7, i64 40, !7, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!17 = !{!"CommonQuantParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !13, i64 9240, !7, i64 9244, !7, i64 9248, !7, i64 9252}
!18 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !7, i64 164, !4, i64 168}
!19 = !{!"", !4, i64 0, !4, i64 3072}
!20 = !{!"loopfilter", !4, i64 0, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !7, i64 32}
!21 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !22, i64 80, !7, i64 84, !22, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!22 = !{!"long", !4, i64 0}
!23 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !4, i64 72, !7, i64 136}
!24 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !25, i64 644}
!25 = !{!"short", !4, i64 0}
!26 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!27 = !{!"SequenceHeader", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !4, i64 16, !7, i64 20, !7, i64 24, !4, i64 28, !7, i64 32, !7, i64 36, !11, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !7, i64 112, !4, i64 116, !7, i64 244, !28, i64 248, !4, i64 264, !29, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!28 = !{!"aom_timing", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!29 = !{!"aom_dec_model_info", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!30 = !{!"CommonTileParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !4, i64 60, !4, i64 320, !7, i64 580, !7, i64 584}
!31 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !7, i64 20, !7, i64 24, !7, i64 28}
!32 = !{!9, !7, i64 16292}
!33 = !{!11, !7, i64 4}
!34 = !{!9, !7, i64 292}
!35 = !{!11, !7, i64 12}
!36 = !{!11, !7, i64 0}
!37 = !{!9, !7, i64 304}
!38 = !{!11, !7, i64 8}
!39 = !{!40, !7, i64 4}
!40 = !{!"", !4, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !41, i64 24, !7, i64 40}
!41 = !{!"", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12}
!42 = !{!40, !7, i64 8}
!43 = !{!40, !7, i64 16}
!44 = !{!40, !7, i64 12}
!45 = !{!40, !3, i64 20}
!46 = !{!25, !25, i64 0}
!47 = !{!4, !4, i64 0}
!48 = !{!49, !4, i64 0}
!49 = !{!"", !4, i64 0, !50, i64 16, !51, i64 48}
!50 = !{!"", !4, i64 0, !4, i64 16}
!51 = !{!"", !7, i64 0, !4, i64 4}
!52 = !{i64 0, i64 4, !6, i64 4, i64 4, !6, i64 8, i64 4, !6, i64 12, i64 4, !6}
!53 = !{!41, !7, i64 8}
!54 = !{!41, !3, i64 0}
!55 = !{!41, !3, i64 4}
!56 = !{!27, !4, i64 72}
!57 = !{!27, !4, i64 76}
!58 = !{!59, !3, i64 176}
!59 = !{!"AV1LrStruct", !3, i64 0, !4, i64 4, !3, i64 172, !3, i64 176}
!60 = !{!27, !7, i64 96}
!61 = !{!27, !7, i64 100}
!62 = !{!9, !7, i64 1348}
!63 = !{!59, !3, i64 0}
!64 = !{!59, !3, i64 172}
!65 = !{!40, !4, i64 0}
!66 = !{!40, !7, i64 40}
!67 = !{!68, !3, i64 0}
!68 = !{!"FilterFrameCtxt", !3, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !3, i64 28, !7, i64 32, !7, i64 36, !11, i64 40}
!69 = !{!68, !7, i64 8}
!70 = !{!68, !7, i64 12}
!71 = !{!68, !7, i64 16}
!72 = !{!68, !7, i64 20}
!73 = !{!68, !3, i64 24}
!74 = !{!68, !3, i64 28}
!75 = !{!68, !7, i64 32}
!76 = !{!68, !7, i64 36}
!77 = !{!68, !7, i64 4}
!78 = !{!9, !4, i64 16269}
!79 = !{!9, !3, i64 14872}
!80 = !{!9, !3, i64 14876}
!81 = !{!9, !4, i64 16220}
!82 = !{!9, !4, i64 312}
!83 = !{!9, !7, i64 288}
!84 = !{!9, !4, i64 16268}
!85 = !{!49, !7, i64 48}
!86 = !{!87, !7, i64 0}
!87 = !{!"ConvolveParams", !7, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40}
!88 = !{!87, !7, i64 24}
!89 = !{!87, !7, i64 12}
!90 = !{!87, !7, i64 16}
!91 = !{!87, !3, i64 4}
!92 = !{!87, !7, i64 8}
!93 = !{!87, !7, i64 20}
