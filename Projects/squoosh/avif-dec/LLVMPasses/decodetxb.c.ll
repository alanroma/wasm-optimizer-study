; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/decodetxb.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/decodetxb.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.SCAN_ORDER = type { i16*, i16* }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.aom_reader = type { i8*, i8*, %struct.od_ec_dec, %struct.Accounting*, i8 }
%struct.od_ec_dec = type { i8*, i32, i8*, i8*, i32, i16, i16 }
%struct.Accounting = type { %struct.AccountingSymbols, i32, [1021 x i16], %struct.AccountingSymbolContext, i32 }
%struct.AccountingSymbols = type { %struct.AccountingSymbol*, i32, i32, i32, %struct.AccountingDictionary }
%struct.AccountingSymbol = type { %struct.AccountingSymbolContext, i32, i32, i32 }
%struct.AccountingDictionary = type { [256 x i8*], i32 }
%struct.AccountingSymbolContext = type { i16, i16 }
%struct.txb_ctx = type { i32, i32 }

@__func__.av1_read_coeffs_txb = private unnamed_addr constant [20 x i8] c"av1_read_coeffs_txb\00", align 1
@tx_type_to_class = internal constant [16 x i8] c"\00\00\00\00\00\00\00\00\00\00\02\01\02\01\02\01", align 16
@txsize_log2_minus4 = internal constant [19 x i8] c"\00\02\04\06\06\01\01\03\03\05\05\06\06\02\02\04\04\05\05", align 16
@av1_eob_offset_bits = external constant [12 x i16], align 16
@tx_size_wide_unit = internal constant [19 x i32] [i32 1, i32 2, i32 4, i32 8, i32 16, i32 1, i32 2, i32 2, i32 4, i32 4, i32 8, i32 8, i32 16, i32 1, i32 4, i32 2, i32 8, i32 4, i32 16], align 16
@tx_size_high_unit = internal constant [19 x i32] [i32 1, i32 2, i32 4, i32 8, i32 16, i32 2, i32 1, i32 4, i32 2, i32 8, i32 4, i32 16, i32 8, i32 4, i32 1, i32 8, i32 2, i32 16, i32 4], align 16
@txsize_sqr_map = internal constant [19 x i8] c"\00\01\02\03\04\00\00\01\01\02\02\03\03\00\00\01\01\02\02", align 16
@txsize_sqr_up_map = internal constant [19 x i8] c"\00\01\02\03\04\01\01\02\02\03\03\04\04\02\02\03\03\04\04", align 16
@tx_size_wide_log2 = internal constant [19 x i32] [i32 2, i32 3, i32 4, i32 5, i32 6, i32 2, i32 3, i32 3, i32 4, i32 4, i32 5, i32 5, i32 6, i32 2, i32 4, i32 3, i32 5, i32 4, i32 6], align 16
@tx_size_wide = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 4, i32 8, i32 8, i32 16, i32 16, i32 32, i32 32, i32 64, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64], align 16
@tx_size_high = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 8, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64, i32 32, i32 16, i32 4, i32 32, i32 8, i32 64, i32 16], align 16
@update_cdf.nsymbs2speed = internal constant [17 x i32] [i32 0, i32 0, i32 1, i32 1, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2], align 16
@intra_mode_to_tx_type._intra_mode_to_tx_type = internal constant [13 x i8] c"\00\01\02\00\03\01\02\02\01\03\01\02\03", align 1
@get_uv_mode.uv2y = internal constant [16 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B\0C\00\19\19", align 16
@av1_ext_tx_set_lookup = internal constant [2 x [2 x i8]] [[2 x i8] c"\03\02", [2 x i8] c"\05\04"], align 1
@av1_ext_tx_used = internal constant <{ <{ i32, [15 x i32] }>, [16 x i32], [16 x i32], [16 x i32], [16 x i32], [16 x i32] }> <{ <{ i32, [15 x i32] }> <{ i32 1, [15 x i32] zeroinitializer }>, [16 x i32] [i32 1, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 1, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0], [16 x i32] [i32 1, i32 1, i32 1, i32 1, i32 0, i32 0, i32 0, i32 0, i32 0, i32 1, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0], [16 x i32] [i32 1, i32 1, i32 1, i32 1, i32 0, i32 0, i32 0, i32 0, i32 0, i32 1, i32 1, i32 1, i32 0, i32 0, i32 0, i32 0], [16 x i32] [i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 0, i32 0, i32 0, i32 0], [16 x i32] [i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1] }>, align 16
@av1_scan_orders = external constant [19 x [16 x %struct.SCAN_ORDER]], align 16
@av1_eob_group_start = external constant [12 x i16], align 16
@__func__.read_coeffs_reverse_2d = private unnamed_addr constant [23 x i8] c"read_coeffs_reverse_2d\00", align 1
@av1_nz_map_ctx_offset = external global [19 x i8*], align 16
@__func__.read_coeffs_reverse = private unnamed_addr constant [20 x i8] c"read_coeffs_reverse\00", align 1
@clip_max3 = internal constant [256 x i8] c"\00\01\02\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03\03", align 16
@nz_map_ctx_offset_1d = internal constant [32 x i32] [i32 26, i32 31, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36, i32 36], align 16
@__func__.read_golomb = private unnamed_addr constant [12 x i8] c"read_golomb\00", align 1
@.str = private unnamed_addr constant [30 x i8] c"Invalid length in read_golomb\00", align 1
@ss_size_lookup = internal constant [22 x [2 x [2 x i8]]] [[2 x [2 x i8]] zeroinitializer, [2 x [2 x i8]] [[2 x i8] c"\01\00", [2 x i8] c"\FF\00"], [2 x [2 x i8]] [[2 x i8] c"\02\FF", [2 x i8] zeroinitializer], [2 x [2 x i8]] [[2 x i8] c"\03\02", [2 x i8] c"\01\00"], [2 x [2 x i8]] [[2 x i8] c"\04\03", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\05\FF", [2 x i8] c"\03\02"], [2 x [2 x i8]] [[2 x i8] c"\06\05", [2 x i8] c"\04\03"], [2 x [2 x i8]] [[2 x i8] c"\07\06", [2 x i8] c"\FF\04"], [2 x [2 x i8]] [[2 x i8] c"\08\FF", [2 x i8] c"\06\05"], [2 x [2 x i8]] [[2 x i8] c"\09\08", [2 x i8] c"\07\06"], [2 x [2 x i8]] [[2 x i8] c"\0A\09", [2 x i8] c"\FF\07"], [2 x [2 x i8]] [[2 x i8] c"\0B\FF", [2 x i8] c"\09\08"], [2 x [2 x i8]] [[2 x i8] c"\0C\0B", [2 x i8] c"\0A\09"], [2 x [2 x i8]] [[2 x i8] c"\0D\0C", [2 x i8] c"\FF\0A"], [2 x [2 x i8]] [[2 x i8] c"\0E\FF", [2 x i8] c"\0C\0B"], [2 x [2 x i8]] [[2 x i8] c"\0F\0E", [2 x i8] c"\0D\0C"], [2 x [2 x i8]] [[2 x i8] c"\10\01", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\11\FF", [2 x i8] c"\02\02"], [2 x [2 x i8]] [[2 x i8] c"\12\04", [2 x i8] c"\FF\10"], [2 x [2 x i8]] [[2 x i8] c"\13\FF", [2 x i8] c"\05\11"], [2 x [2 x i8]] [[2 x i8] c"\14\07", [2 x i8] c"\FF\12"], [2 x [2 x i8]] [[2 x i8] c"\15\FF", [2 x i8] c"\08\13"]], align 16
@get_txb_ctx.signs = internal constant [3 x i8] c"\00\FF\01", align 1
@get_txb_ctx.dc_sign_contexts = internal constant [65 x i8] c"\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\00\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02", align 16
@txsize_to_bsize = internal constant [19 x i8] c"\00\03\06\09\0C\01\02\04\05\07\08\0A\0B\10\11\12\13\14\15", align 16
@get_txb_ctx.skip_contexts = internal constant [5 x [5 x i8]] [[5 x i8] c"\01\02\02\02\03", [5 x i8] c"\02\04\04\04\05", [5 x i8] c"\02\04\04\04\05", [5 x i8] c"\02\04\04\04\05", [5 x i8] c"\03\05\05\05\06"], align 16
@num_pels_log2_lookup = internal constant [22 x i8] c"\04\05\05\06\07\07\08\09\09\0A\0B\0B\0C\0D\0D\0E\06\06\08\08\0A\0A", align 16

; Function Attrs: nounwind
define hidden zeroext i8 @av1_read_coeffs_txb(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r, i32 %blk_row, i32 %blk_col, i32 %plane, %struct.txb_ctx* %txb_ctx, i8 zeroext %tx_size) #0 {
entry:
  %retval = alloca i8, align 1
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %blk_row.addr = alloca i32, align 4
  %blk_col.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %txb_ctx.addr = alloca %struct.txb_ctx*, align 4
  %tx_size.addr = alloca i8, align 1
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %max_value = alloca i32, align 4
  %min_value = alloca i32, align 4
  %txs_ctx = alloca i8, align 1
  %plane_type = alloca i8, align 1
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %dequant = alloca i16*, align 4
  %tcoeffs = alloca i32*, align 4
  %shift = alloca i32, align 4
  %bwl = alloca i32, align 4
  %width = alloca i32, align 4
  %height = alloca i32, align 4
  %cul_level = alloca i32, align 4
  %dc_val = alloca i32, align 4
  %levels_buf = alloca [1312 x i8], align 16
  %levels = alloca i8*, align 4
  %all_zero = alloca i32, align 4
  %eob_data = alloca %struct.eob_info*, align 4
  %eob = alloca i16*, align 4
  %max_scan_line = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tx_type = alloca i8, align 1
  %tx_class = alloca i8, align 1
  %iqmatrix = alloca i8*, align 4
  %scan_order = alloca %struct.SCAN_ORDER*, align 4
  %scan = alloca i16*, align 4
  %eob_extra = alloca i32, align 4
  %eob_pt = alloca i32, align 4
  %eob_multi_size = alloca i32, align 4
  %eob_multi_ctx = alloca i32, align 4
  %eob_offset_bits = alloca i32, align 4
  %eob_ctx = alloca i32, align 4
  %bit = alloca i32, align 4
  %i = alloca i32, align 4
  %c = alloca i32, align 4
  %pos = alloca i32, align 4
  %coeff_ctx = alloca i32, align 4
  %nsymbs = alloca i32, align 4
  %cdf = alloca i16*, align 4
  %level = alloca i32, align 4
  %br_ctx = alloca i32, align 4
  %idx = alloca i32, align 4
  %k = alloca i32, align 4
  %base_cdf = alloca [5 x i16]*, align 4
  %br_cdf = alloca [5 x i16]*, align 4
  %c221 = alloca i32, align 4
  %pos228 = alloca i32, align 4
  %sign = alloca i8, align 1
  %level231 = alloca i32, align 4
  %dc_sign_ctx = alloca i32, align 4
  %dq_coeff = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %blk_row, i32* %blk_row.addr, align 4, !tbaa !6
  store i32 %blk_col, i32* %blk_col.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.txb_ctx* %txb_ctx, %struct.txb_ctx** %txb_ctx.addr, align 4, !tbaa !2
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  %0 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 40
  %2 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !9
  store %struct.frame_contexts* %2, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %3 = bitcast i32* %max_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 41
  %5 = load i32, i32* %bd, align 4, !tbaa !15
  %add = add nsw i32 7, %5
  %shl = shl i32 1, %add
  %sub = sub nsw i32 %shl, 1
  store i32 %sub, i32* %max_value, align 4, !tbaa !6
  %6 = bitcast i32* %min_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 41
  %8 = load i32, i32* %bd1, align 4, !tbaa !15
  %add2 = add nsw i32 7, %8
  %shl3 = shl i32 1, %add2
  %sub4 = sub nsw i32 0, %shl3
  store i32 %sub4, i32* %min_value, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %txs_ctx) #6
  %9 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call = call zeroext i8 @get_txsize_entropy_ctx(i8 zeroext %9)
  store i8 %call, i8* %txs_ctx, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %plane_type) #6
  %10 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %call5 = call zeroext i8 @get_plane_type(i32 %10)
  store i8 %call5, i8* %plane_type, align 1, !tbaa !8
  %11 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %12, i32 0, i32 6
  %13 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %13, i32 0
  %14 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %14, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %15 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane6 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %16, i32 0, i32 4
  %17 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane6, i32 0, i32 %17
  store %struct.macroblockd_plane* %arrayidx7, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %18 = bitcast i16** %dequant to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %seg_dequant_QTX = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %19, i32 0, i32 10
  %20 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %segment_id = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %20, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id, align 4
  %bf.lshr = lshr i8 %bf.load, 1
  %bf.clear = and i8 %bf.lshr, 7
  %idxprom = zext i8 %bf.clear to i32
  %arrayidx8 = getelementptr inbounds [8 x [2 x i16]], [8 x [2 x i16]]* %seg_dequant_QTX, i32 0, i32 %idxprom
  %arraydecay = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx8, i32 0, i32 0
  store i16* %arraydecay, i16** %dequant, align 4, !tbaa !2
  %21 = bitcast i32** %tcoeffs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dqcoeff_block = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %22, i32 0, i32 1
  %23 = load i32*, i32** %dqcoeff_block, align 4, !tbaa !17
  %24 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cb_offset = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %24, i32 0, i32 58
  %25 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds [3 x i16], [3 x i16]* %cb_offset, i32 0, i32 %25
  %26 = load i16, i16* %arrayidx9, align 2, !tbaa !20
  %conv = zext i16 %26 to i32
  %add.ptr = getelementptr inbounds i32, i32* %23, i32 %conv
  store i32* %add.ptr, i32** %tcoeffs, align 4, !tbaa !2
  %27 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call10 = call i32 @av1_get_tx_scale(i8 zeroext %28)
  store i32 %call10, i32* %shift, align 4, !tbaa !6
  %29 = bitcast i32* %bwl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call11 = call i32 @get_txb_bwl(i8 zeroext %30)
  store i32 %call11, i32* %bwl, align 4, !tbaa !6
  %31 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call12 = call i32 @get_txb_wide(i8 zeroext %32)
  store i32 %call12, i32* %width, align 4, !tbaa !6
  %33 = bitcast i32* %height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  %34 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call13 = call i32 @get_txb_high(i8 zeroext %34)
  store i32 %call13, i32* %height, align 4, !tbaa !6
  %35 = bitcast i32* %cul_level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  store i32 0, i32* %cul_level, align 4, !tbaa !6
  %36 = bitcast i32* %dc_val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  store i32 0, i32* %dc_val, align 4, !tbaa !6
  %37 = bitcast [1312 x i8]* %levels_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 1312, i8* %37) #6
  %38 = bitcast i8** %levels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  %arraydecay14 = getelementptr inbounds [1312 x i8], [1312 x i8]* %levels_buf, i32 0, i32 0
  %39 = load i32, i32* %width, align 4, !tbaa !6
  %call15 = call i8* @set_levels(i8* %arraydecay14, i32 %39)
  store i8* %call15, i8** %levels, align 4, !tbaa !2
  %40 = bitcast i32* %all_zero to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  %41 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %42 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %txb_skip_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %42, i32 0, i32 0
  %43 = load i8, i8* %txs_ctx, align 1, !tbaa !8
  %idxprom16 = zext i8 %43 to i32
  %arrayidx17 = getelementptr inbounds [5 x [13 x [3 x i16]]], [5 x [13 x [3 x i16]]]* %txb_skip_cdf, i32 0, i32 %idxprom16
  %44 = load %struct.txb_ctx*, %struct.txb_ctx** %txb_ctx.addr, align 4, !tbaa !2
  %txb_skip_ctx = getelementptr inbounds %struct.txb_ctx, %struct.txb_ctx* %44, i32 0, i32 0
  %45 = load i32, i32* %txb_skip_ctx, align 4, !tbaa !22
  %arrayidx18 = getelementptr inbounds [13 x [3 x i16]], [13 x [3 x i16]]* %arrayidx17, i32 0, i32 %45
  %arraydecay19 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx18, i32 0, i32 0
  %call20 = call i32 @aom_read_symbol_(%struct.aom_reader* %41, i16* %arraydecay19, i32 2, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  store i32 %call20, i32* %all_zero, align 4, !tbaa !6
  %46 = bitcast %struct.eob_info** %eob_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #6
  %47 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %eob_data21 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %47, i32 0, i32 2
  %48 = load %struct.eob_info*, %struct.eob_info** %eob_data21, align 4, !tbaa !24
  %49 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %txb_offset = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %49, i32 0, i32 59
  %50 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [3 x i16], [3 x i16]* %txb_offset, i32 0, i32 %50
  %51 = load i16, i16* %arrayidx22, align 2, !tbaa !20
  %conv23 = zext i16 %51 to i32
  %add.ptr24 = getelementptr inbounds %struct.eob_info, %struct.eob_info* %48, i32 %conv23
  store %struct.eob_info* %add.ptr24, %struct.eob_info** %eob_data, align 4, !tbaa !2
  %52 = bitcast i16** %eob to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load %struct.eob_info*, %struct.eob_info** %eob_data, align 4, !tbaa !2
  %eob25 = getelementptr inbounds %struct.eob_info, %struct.eob_info* %53, i32 0, i32 0
  store i16* %eob25, i16** %eob, align 4, !tbaa !2
  %54 = bitcast i16** %max_scan_line to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #6
  %55 = load %struct.eob_info*, %struct.eob_info** %eob_data, align 4, !tbaa !2
  %max_scan_line26 = getelementptr inbounds %struct.eob_info, %struct.eob_info* %55, i32 0, i32 1
  store i16* %max_scan_line26, i16** %max_scan_line, align 4, !tbaa !2
  %56 = load i16*, i16** %max_scan_line, align 4, !tbaa !2
  store i16 0, i16* %56, align 2, !tbaa !20
  %57 = load i16*, i16** %eob, align 4, !tbaa !2
  store i16 0, i16* %57, align 2, !tbaa !20
  %58 = load i32, i32* %all_zero, align 4, !tbaa !6
  %tobool = icmp ne i32 %58, 0
  br i1 %tobool, label %if.then, label %if.end31

if.then:                                          ; preds = %entry
  %59 = load i16*, i16** %max_scan_line, align 4, !tbaa !2
  store i16 0, i16* %59, align 2, !tbaa !20
  %60 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %60, 0
  br i1 %cmp, label %if.then28, label %if.end

if.then28:                                        ; preds = %if.then
  %61 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tx_type_map = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %61, i32 0, i32 15
  %62 = load i8*, i8** %tx_type_map, align 4, !tbaa !25
  %63 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %64 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tx_type_map_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %64, i32 0, i32 16
  %65 = load i32, i32* %tx_type_map_stride, align 16, !tbaa !26
  %mul = mul nsw i32 %63, %65
  %66 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %add29 = add nsw i32 %mul, %66
  %arrayidx30 = getelementptr inbounds i8, i8* %62, i32 %add29
  store i8 0, i8* %arrayidx30, align 1, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then28, %if.then
  store i8 0, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup314

if.end31:                                         ; preds = %entry
  %67 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp32 = icmp eq i32 %67, 0
  br i1 %cmp32, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end31
  %68 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %69 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %70 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %71 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %72 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %73 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  call void @av1_read_tx_type(%struct.AV1Common* %68, %struct.macroblockd* %69, i32 %70, i32 %71, i8 zeroext %72, %struct.aom_reader* %73)
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.end31
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type) #6
  %74 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %75 = load i8, i8* %plane_type, align 1, !tbaa !8
  %76 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %77 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %78 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %79 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %79, i32 0, i32 21
  %reduced_tx_set_used = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 9
  %80 = load i8, i8* %reduced_tx_set_used, align 1, !tbaa !27, !range !49
  %tobool36 = trunc i8 %80 to i1
  %conv37 = zext i1 %tobool36 to i32
  %call38 = call zeroext i8 @av1_get_tx_type(%struct.macroblockd* %74, i8 zeroext %75, i32 %76, i32 %77, i8 zeroext %78, i32 %conv37)
  store i8 %call38, i8* %tx_type, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_class) #6
  %81 = load i8, i8* %tx_type, align 1, !tbaa !8
  %idxprom39 = zext i8 %81 to i32
  %arrayidx40 = getelementptr inbounds [16 x i8], [16 x i8]* @tx_type_to_class, i32 0, i32 %idxprom39
  %82 = load i8, i8* %arrayidx40, align 1, !tbaa !8
  store i8 %82, i8* %tx_class, align 1, !tbaa !8
  %83 = bitcast i8** %iqmatrix to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #6
  %84 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %quant_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %84, i32 0, i32 23
  %85 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %86 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %87 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %88 = load i8, i8* %tx_type, align 1, !tbaa !8
  %call41 = call i8* @av1_get_iqmatrix(%struct.CommonQuantParams* %quant_params, %struct.macroblockd* %85, i32 %86, i8 zeroext %87, i8 zeroext %88)
  store i8* %call41, i8** %iqmatrix, align 4, !tbaa !2
  %89 = bitcast %struct.SCAN_ORDER** %scan_order to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #6
  %90 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %91 = load i8, i8* %tx_type, align 1, !tbaa !8
  %call42 = call %struct.SCAN_ORDER* @get_scan(i8 zeroext %90, i8 zeroext %91)
  store %struct.SCAN_ORDER* %call42, %struct.SCAN_ORDER** %scan_order, align 4, !tbaa !2
  %92 = bitcast i16** %scan to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #6
  %93 = load %struct.SCAN_ORDER*, %struct.SCAN_ORDER** %scan_order, align 4, !tbaa !2
  %scan43 = getelementptr inbounds %struct.SCAN_ORDER, %struct.SCAN_ORDER* %93, i32 0, i32 0
  %94 = load i16*, i16** %scan43, align 4, !tbaa !50
  store i16* %94, i16** %scan, align 4, !tbaa !2
  %95 = bitcast i32* %eob_extra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #6
  store i32 0, i32* %eob_extra, align 4, !tbaa !6
  %96 = bitcast i32* %eob_pt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #6
  store i32 1, i32* %eob_pt, align 4, !tbaa !6
  %97 = bitcast i32* %eob_multi_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #6
  %98 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom44 = zext i8 %98 to i32
  %arrayidx45 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_log2_minus4, i32 0, i32 %idxprom44
  %99 = load i8, i8* %arrayidx45, align 1, !tbaa !8
  %conv46 = sext i8 %99 to i32
  store i32 %conv46, i32* %eob_multi_size, align 4, !tbaa !6
  %100 = bitcast i32* %eob_multi_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #6
  %101 = load i8, i8* %tx_class, align 1, !tbaa !8
  %conv47 = zext i8 %101 to i32
  %cmp48 = icmp eq i32 %conv47, 0
  %102 = zext i1 %cmp48 to i64
  %cond = select i1 %cmp48, i32 0, i32 1
  store i32 %cond, i32* %eob_multi_ctx, align 4, !tbaa !6
  %103 = load i32, i32* %eob_multi_size, align 4, !tbaa !6
  switch i32 %103, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb56
    i32 2, label %sw.bb63
    i32 3, label %sw.bb70
    i32 4, label %sw.bb77
    i32 5, label %sw.bb84
    i32 6, label %sw.bb91
  ]

sw.bb:                                            ; preds = %if.end35
  %104 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %105 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %eob_flag_cdf16 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %105, i32 0, i32 3
  %106 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom50 = zext i8 %106 to i32
  %arrayidx51 = getelementptr inbounds [2 x [2 x [6 x i16]]], [2 x [2 x [6 x i16]]]* %eob_flag_cdf16, i32 0, i32 %idxprom50
  %107 = load i32, i32* %eob_multi_ctx, align 4, !tbaa !6
  %arrayidx52 = getelementptr inbounds [2 x [6 x i16]], [2 x [6 x i16]]* %arrayidx51, i32 0, i32 %107
  %arraydecay53 = getelementptr inbounds [6 x i16], [6 x i16]* %arrayidx52, i32 0, i32 0
  %call54 = call i32 @aom_read_symbol_(%struct.aom_reader* %104, i16* %arraydecay53, i32 5, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %add55 = add nsw i32 %call54, 1
  store i32 %add55, i32* %eob_pt, align 4, !tbaa !6
  br label %sw.epilog

sw.bb56:                                          ; preds = %if.end35
  %108 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %109 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %eob_flag_cdf32 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %109, i32 0, i32 4
  %110 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom57 = zext i8 %110 to i32
  %arrayidx58 = getelementptr inbounds [2 x [2 x [7 x i16]]], [2 x [2 x [7 x i16]]]* %eob_flag_cdf32, i32 0, i32 %idxprom57
  %111 = load i32, i32* %eob_multi_ctx, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds [2 x [7 x i16]], [2 x [7 x i16]]* %arrayidx58, i32 0, i32 %111
  %arraydecay60 = getelementptr inbounds [7 x i16], [7 x i16]* %arrayidx59, i32 0, i32 0
  %call61 = call i32 @aom_read_symbol_(%struct.aom_reader* %108, i16* %arraydecay60, i32 6, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %add62 = add nsw i32 %call61, 1
  store i32 %add62, i32* %eob_pt, align 4, !tbaa !6
  br label %sw.epilog

sw.bb63:                                          ; preds = %if.end35
  %112 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %113 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %eob_flag_cdf64 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %113, i32 0, i32 5
  %114 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom64 = zext i8 %114 to i32
  %arrayidx65 = getelementptr inbounds [2 x [2 x [8 x i16]]], [2 x [2 x [8 x i16]]]* %eob_flag_cdf64, i32 0, i32 %idxprom64
  %115 = load i32, i32* %eob_multi_ctx, align 4, !tbaa !6
  %arrayidx66 = getelementptr inbounds [2 x [8 x i16]], [2 x [8 x i16]]* %arrayidx65, i32 0, i32 %115
  %arraydecay67 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx66, i32 0, i32 0
  %call68 = call i32 @aom_read_symbol_(%struct.aom_reader* %112, i16* %arraydecay67, i32 7, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %add69 = add nsw i32 %call68, 1
  store i32 %add69, i32* %eob_pt, align 4, !tbaa !6
  br label %sw.epilog

sw.bb70:                                          ; preds = %if.end35
  %116 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %117 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %eob_flag_cdf128 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %117, i32 0, i32 6
  %118 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom71 = zext i8 %118 to i32
  %arrayidx72 = getelementptr inbounds [2 x [2 x [9 x i16]]], [2 x [2 x [9 x i16]]]* %eob_flag_cdf128, i32 0, i32 %idxprom71
  %119 = load i32, i32* %eob_multi_ctx, align 4, !tbaa !6
  %arrayidx73 = getelementptr inbounds [2 x [9 x i16]], [2 x [9 x i16]]* %arrayidx72, i32 0, i32 %119
  %arraydecay74 = getelementptr inbounds [9 x i16], [9 x i16]* %arrayidx73, i32 0, i32 0
  %call75 = call i32 @aom_read_symbol_(%struct.aom_reader* %116, i16* %arraydecay74, i32 8, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %add76 = add nsw i32 %call75, 1
  store i32 %add76, i32* %eob_pt, align 4, !tbaa !6
  br label %sw.epilog

sw.bb77:                                          ; preds = %if.end35
  %120 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %121 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %eob_flag_cdf256 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %121, i32 0, i32 7
  %122 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom78 = zext i8 %122 to i32
  %arrayidx79 = getelementptr inbounds [2 x [2 x [10 x i16]]], [2 x [2 x [10 x i16]]]* %eob_flag_cdf256, i32 0, i32 %idxprom78
  %123 = load i32, i32* %eob_multi_ctx, align 4, !tbaa !6
  %arrayidx80 = getelementptr inbounds [2 x [10 x i16]], [2 x [10 x i16]]* %arrayidx79, i32 0, i32 %123
  %arraydecay81 = getelementptr inbounds [10 x i16], [10 x i16]* %arrayidx80, i32 0, i32 0
  %call82 = call i32 @aom_read_symbol_(%struct.aom_reader* %120, i16* %arraydecay81, i32 9, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %add83 = add nsw i32 %call82, 1
  store i32 %add83, i32* %eob_pt, align 4, !tbaa !6
  br label %sw.epilog

sw.bb84:                                          ; preds = %if.end35
  %124 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %125 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %eob_flag_cdf512 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %125, i32 0, i32 8
  %126 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom85 = zext i8 %126 to i32
  %arrayidx86 = getelementptr inbounds [2 x [2 x [11 x i16]]], [2 x [2 x [11 x i16]]]* %eob_flag_cdf512, i32 0, i32 %idxprom85
  %127 = load i32, i32* %eob_multi_ctx, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds [2 x [11 x i16]], [2 x [11 x i16]]* %arrayidx86, i32 0, i32 %127
  %arraydecay88 = getelementptr inbounds [11 x i16], [11 x i16]* %arrayidx87, i32 0, i32 0
  %call89 = call i32 @aom_read_symbol_(%struct.aom_reader* %124, i16* %arraydecay88, i32 10, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %add90 = add nsw i32 %call89, 1
  store i32 %add90, i32* %eob_pt, align 4, !tbaa !6
  br label %sw.epilog

sw.bb91:                                          ; preds = %if.end35
  br label %sw.default

sw.default:                                       ; preds = %if.end35, %sw.bb91
  %128 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %129 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %eob_flag_cdf1024 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %129, i32 0, i32 9
  %130 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom92 = zext i8 %130 to i32
  %arrayidx93 = getelementptr inbounds [2 x [2 x [12 x i16]]], [2 x [2 x [12 x i16]]]* %eob_flag_cdf1024, i32 0, i32 %idxprom92
  %131 = load i32, i32* %eob_multi_ctx, align 4, !tbaa !6
  %arrayidx94 = getelementptr inbounds [2 x [12 x i16]], [2 x [12 x i16]]* %arrayidx93, i32 0, i32 %131
  %arraydecay95 = getelementptr inbounds [12 x i16], [12 x i16]* %arrayidx94, i32 0, i32 0
  %call96 = call i32 @aom_read_symbol_(%struct.aom_reader* %128, i16* %arraydecay95, i32 11, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %add97 = add nsw i32 %call96, 1
  store i32 %add97, i32* %eob_pt, align 4, !tbaa !6
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb84, %sw.bb77, %sw.bb70, %sw.bb63, %sw.bb56, %sw.bb
  %132 = bitcast i32* %eob_offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #6
  %133 = load i32, i32* %eob_pt, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds [12 x i16], [12 x i16]* @av1_eob_offset_bits, i32 0, i32 %133
  %134 = load i16, i16* %arrayidx98, align 2, !tbaa !20
  %conv99 = sext i16 %134 to i32
  store i32 %conv99, i32* %eob_offset_bits, align 4, !tbaa !6
  %135 = load i32, i32* %eob_offset_bits, align 4, !tbaa !6
  %cmp100 = icmp sgt i32 %135, 0
  br i1 %cmp100, label %if.then102, label %if.end127

if.then102:                                       ; preds = %sw.epilog
  %136 = bitcast i32* %eob_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #6
  %137 = load i32, i32* %eob_pt, align 4, !tbaa !6
  %sub103 = sub nsw i32 %137, 3
  store i32 %sub103, i32* %eob_ctx, align 4, !tbaa !6
  %138 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #6
  %139 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %140 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %eob_extra_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %140, i32 0, i32 1
  %141 = load i8, i8* %txs_ctx, align 1, !tbaa !8
  %idxprom104 = zext i8 %141 to i32
  %arrayidx105 = getelementptr inbounds [5 x [2 x [9 x [3 x i16]]]], [5 x [2 x [9 x [3 x i16]]]]* %eob_extra_cdf, i32 0, i32 %idxprom104
  %142 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom106 = zext i8 %142 to i32
  %arrayidx107 = getelementptr inbounds [2 x [9 x [3 x i16]]], [2 x [9 x [3 x i16]]]* %arrayidx105, i32 0, i32 %idxprom106
  %143 = load i32, i32* %eob_ctx, align 4, !tbaa !6
  %arrayidx108 = getelementptr inbounds [9 x [3 x i16]], [9 x [3 x i16]]* %arrayidx107, i32 0, i32 %143
  %arraydecay109 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx108, i32 0, i32 0
  %call110 = call i32 @aom_read_symbol_(%struct.aom_reader* %139, i16* %arraydecay109, i32 2, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  store i32 %call110, i32* %bit, align 4, !tbaa !6
  %144 = load i32, i32* %bit, align 4, !tbaa !6
  %tobool111 = icmp ne i32 %144, 0
  br i1 %tobool111, label %if.then112, label %if.end116

if.then112:                                       ; preds = %if.then102
  %145 = load i32, i32* %eob_offset_bits, align 4, !tbaa !6
  %sub113 = sub nsw i32 %145, 1
  %shl114 = shl i32 1, %sub113
  %146 = load i32, i32* %eob_extra, align 4, !tbaa !6
  %add115 = add nsw i32 %146, %shl114
  store i32 %add115, i32* %eob_extra, align 4, !tbaa !6
  br label %if.end116

if.end116:                                        ; preds = %if.then112, %if.then102
  %147 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %147) #6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end116
  %148 = load i32, i32* %i, align 4, !tbaa !6
  %149 = load i32, i32* %eob_offset_bits, align 4, !tbaa !6
  %cmp117 = icmp slt i32 %148, %149
  br i1 %cmp117, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 3, i32* %cleanup.dest.slot, align 4
  %150 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %151 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call119 = call i32 @aom_read_bit_(%struct.aom_reader* %151, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  store i32 %call119, i32* %bit, align 4, !tbaa !6
  %152 = load i32, i32* %bit, align 4, !tbaa !6
  %tobool120 = icmp ne i32 %152, 0
  br i1 %tobool120, label %if.then121, label %if.end126

if.then121:                                       ; preds = %for.body
  %153 = load i32, i32* %eob_offset_bits, align 4, !tbaa !6
  %sub122 = sub nsw i32 %153, 1
  %154 = load i32, i32* %i, align 4, !tbaa !6
  %sub123 = sub nsw i32 %sub122, %154
  %shl124 = shl i32 1, %sub123
  %155 = load i32, i32* %eob_extra, align 4, !tbaa !6
  %add125 = add nsw i32 %155, %shl124
  store i32 %add125, i32* %eob_extra, align 4, !tbaa !6
  br label %if.end126

if.end126:                                        ; preds = %if.then121, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end126
  %156 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %156, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %157 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #6
  %158 = bitcast i32* %eob_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #6
  br label %if.end127

if.end127:                                        ; preds = %for.end, %sw.epilog
  %159 = load i32, i32* %eob_pt, align 4, !tbaa !6
  %160 = load i32, i32* %eob_extra, align 4, !tbaa !6
  %call128 = call i32 @rec_eob_pos(i32 %159, i32 %160)
  %conv129 = trunc i32 %call128 to i16
  %161 = load i16*, i16** %eob, align 4, !tbaa !2
  store i16 %conv129, i16* %161, align 2, !tbaa !20
  %162 = load i16*, i16** %eob, align 4, !tbaa !2
  %163 = load i16, i16* %162, align 2, !tbaa !20
  %conv130 = zext i16 %163 to i32
  %cmp131 = icmp sgt i32 %conv130, 1
  br i1 %cmp131, label %if.then133, label %if.end140

if.then133:                                       ; preds = %if.end127
  %arraydecay134 = getelementptr inbounds [1312 x i8], [1312 x i8]* %levels_buf, i32 0, i32 0
  %164 = load i32, i32* %width, align 4, !tbaa !6
  %add135 = add nsw i32 %164, 4
  %165 = load i32, i32* %height, align 4, !tbaa !6
  %add136 = add nsw i32 %165, 4
  %mul137 = mul nsw i32 %add135, %add136
  %add138 = add nsw i32 %mul137, 16
  %mul139 = mul i32 1, %add138
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay134, i8 0, i32 %mul139, i1 false)
  br label %if.end140

if.end140:                                        ; preds = %if.then133, %if.end127
  %166 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %166) #6
  %167 = load i16*, i16** %eob, align 4, !tbaa !2
  %168 = load i16, i16* %167, align 2, !tbaa !20
  %conv141 = zext i16 %168 to i32
  %sub142 = sub nsw i32 %conv141, 1
  store i32 %sub142, i32* %c, align 4, !tbaa !6
  %169 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %169) #6
  %170 = load i16*, i16** %scan, align 4, !tbaa !2
  %171 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx143 = getelementptr inbounds i16, i16* %170, i32 %171
  %172 = load i16, i16* %arrayidx143, align 2, !tbaa !20
  %conv144 = sext i16 %172 to i32
  store i32 %conv144, i32* %pos, align 4, !tbaa !6
  %173 = bitcast i32* %coeff_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %173) #6
  %174 = load i32, i32* %bwl, align 4, !tbaa !6
  %175 = load i32, i32* %height, align 4, !tbaa !6
  %176 = load i32, i32* %c, align 4, !tbaa !6
  %call145 = call i32 @get_lower_levels_ctx_eob(i32 %174, i32 %175, i32 %176)
  store i32 %call145, i32* %coeff_ctx, align 4, !tbaa !6
  %177 = bitcast i32* %nsymbs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %177) #6
  store i32 3, i32* %nsymbs, align 4, !tbaa !6
  %178 = bitcast i16** %cdf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %178) #6
  %179 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %coeff_base_eob_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %179, i32 0, i32 10
  %180 = load i8, i8* %txs_ctx, align 1, !tbaa !8
  %idxprom146 = zext i8 %180 to i32
  %arrayidx147 = getelementptr inbounds [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [4 x [4 x i16]]]]* %coeff_base_eob_cdf, i32 0, i32 %idxprom146
  %181 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom148 = zext i8 %181 to i32
  %arrayidx149 = getelementptr inbounds [2 x [4 x [4 x i16]]], [2 x [4 x [4 x i16]]]* %arrayidx147, i32 0, i32 %idxprom148
  %182 = load i32, i32* %coeff_ctx, align 4, !tbaa !6
  %arrayidx150 = getelementptr inbounds [4 x [4 x i16]], [4 x [4 x i16]]* %arrayidx149, i32 0, i32 %182
  %arraydecay151 = getelementptr inbounds [4 x i16], [4 x i16]* %arrayidx150, i32 0, i32 0
  store i16* %arraydecay151, i16** %cdf, align 4, !tbaa !2
  %183 = bitcast i32* %level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %183) #6
  %184 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %185 = load i16*, i16** %cdf, align 4, !tbaa !2
  %call152 = call i32 @aom_read_symbol_(%struct.aom_reader* %184, i16* %185, i32 3, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %add153 = add nsw i32 %call152, 1
  store i32 %add153, i32* %level, align 4, !tbaa !6
  %186 = load i32, i32* %level, align 4, !tbaa !6
  %cmp154 = icmp sgt i32 %186, 2
  br i1 %cmp154, label %if.then156, label %if.end183

if.then156:                                       ; preds = %if.end140
  %187 = bitcast i32* %br_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #6
  %188 = load i32, i32* %pos, align 4, !tbaa !6
  %189 = load i32, i32* %bwl, align 4, !tbaa !6
  %190 = load i8, i8* %tx_class, align 1, !tbaa !8
  %call157 = call i32 @get_br_ctx_eob(i32 %188, i32 %189, i8 zeroext %190)
  store i32 %call157, i32* %br_ctx, align 4, !tbaa !6
  %191 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %coeff_br_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %191, i32 0, i32 12
  %192 = load i8, i8* %txs_ctx, align 1, !tbaa !8
  %conv158 = zext i8 %192 to i32
  %cmp159 = icmp slt i32 %conv158, 3
  br i1 %cmp159, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then156
  %193 = load i8, i8* %txs_ctx, align 1, !tbaa !8
  %conv161 = zext i8 %193 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.then156
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond162 = phi i32 [ %conv161, %cond.true ], [ 3, %cond.false ]
  %arrayidx163 = getelementptr inbounds [5 x [2 x [21 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]]* %coeff_br_cdf, i32 0, i32 %cond162
  %194 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom164 = zext i8 %194 to i32
  %arrayidx165 = getelementptr inbounds [2 x [21 x [5 x i16]]], [2 x [21 x [5 x i16]]]* %arrayidx163, i32 0, i32 %idxprom164
  %195 = load i32, i32* %br_ctx, align 4, !tbaa !6
  %arrayidx166 = getelementptr inbounds [21 x [5 x i16]], [21 x [5 x i16]]* %arrayidx165, i32 0, i32 %195
  %arraydecay167 = getelementptr inbounds [5 x i16], [5 x i16]* %arrayidx166, i32 0, i32 0
  store i16* %arraydecay167, i16** %cdf, align 4, !tbaa !2
  %196 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %196) #6
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond168

for.cond168:                                      ; preds = %for.inc179, %cond.end
  %197 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp169 = icmp slt i32 %197, 12
  br i1 %cmp169, label %for.body172, label %for.cond.cleanup171

for.cond.cleanup171:                              ; preds = %for.cond168
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup181

for.body172:                                      ; preds = %for.cond168
  %198 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #6
  %199 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %200 = load i16*, i16** %cdf, align 4, !tbaa !2
  %call173 = call i32 @aom_read_symbol_(%struct.aom_reader* %199, i16* %200, i32 4, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  store i32 %call173, i32* %k, align 4, !tbaa !6
  %201 = load i32, i32* %k, align 4, !tbaa !6
  %202 = load i32, i32* %level, align 4, !tbaa !6
  %add174 = add nsw i32 %202, %201
  store i32 %add174, i32* %level, align 4, !tbaa !6
  %203 = load i32, i32* %k, align 4, !tbaa !6
  %cmp175 = icmp slt i32 %203, 3
  br i1 %cmp175, label %if.then177, label %if.end178

if.then177:                                       ; preds = %for.body172
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end178:                                        ; preds = %for.body172
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end178, %if.then177
  %204 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup181 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc179

for.inc179:                                       ; preds = %cleanup.cont
  %205 = load i32, i32* %idx, align 4, !tbaa !6
  %add180 = add nsw i32 %205, 3
  store i32 %add180, i32* %idx, align 4, !tbaa !6
  br label %for.cond168

cleanup181:                                       ; preds = %cleanup, %for.cond.cleanup171
  %206 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #6
  br label %for.end182

for.end182:                                       ; preds = %cleanup181
  %207 = bitcast i32* %br_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #6
  br label %if.end183

if.end183:                                        ; preds = %for.end182, %if.end140
  %208 = load i32, i32* %level, align 4, !tbaa !6
  %conv184 = trunc i32 %208 to i8
  %209 = load i8*, i8** %levels, align 4, !tbaa !2
  %210 = load i32, i32* %pos, align 4, !tbaa !6
  %211 = load i32, i32* %bwl, align 4, !tbaa !6
  %call185 = call i32 @get_padded_idx(i32 %210, i32 %211)
  %arrayidx186 = getelementptr inbounds i8, i8* %209, i32 %call185
  store i8 %conv184, i8* %arrayidx186, align 1, !tbaa !8
  %212 = bitcast i32* %level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #6
  %213 = bitcast i16** %cdf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #6
  %214 = bitcast i32* %nsymbs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #6
  %215 = bitcast i32* %coeff_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #6
  %216 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #6
  %217 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #6
  %218 = load i16*, i16** %eob, align 4, !tbaa !2
  %219 = load i16, i16* %218, align 2, !tbaa !20
  %conv187 = zext i16 %219 to i32
  %cmp188 = icmp sgt i32 %conv187, 1
  br i1 %cmp188, label %if.then190, label %if.end220

if.then190:                                       ; preds = %if.end183
  %220 = bitcast [5 x i16]** %base_cdf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %220) #6
  %221 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %coeff_base_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %221, i32 0, i32 11
  %222 = load i8, i8* %txs_ctx, align 1, !tbaa !8
  %idxprom191 = zext i8 %222 to i32
  %arrayidx192 = getelementptr inbounds [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [42 x [5 x i16]]]]* %coeff_base_cdf, i32 0, i32 %idxprom191
  %223 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom193 = zext i8 %223 to i32
  %arrayidx194 = getelementptr inbounds [2 x [42 x [5 x i16]]], [2 x [42 x [5 x i16]]]* %arrayidx192, i32 0, i32 %idxprom193
  %arraydecay195 = getelementptr inbounds [42 x [5 x i16]], [42 x [5 x i16]]* %arrayidx194, i32 0, i32 0
  store [5 x i16]* %arraydecay195, [5 x i16]** %base_cdf, align 4, !tbaa !2
  %224 = bitcast [5 x i16]** %br_cdf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %224) #6
  %225 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %coeff_br_cdf196 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %225, i32 0, i32 12
  %226 = load i8, i8* %txs_ctx, align 1, !tbaa !8
  %conv197 = zext i8 %226 to i32
  %cmp198 = icmp slt i32 %conv197, 3
  br i1 %cmp198, label %cond.true200, label %cond.false202

cond.true200:                                     ; preds = %if.then190
  %227 = load i8, i8* %txs_ctx, align 1, !tbaa !8
  %conv201 = zext i8 %227 to i32
  br label %cond.end203

cond.false202:                                    ; preds = %if.then190
  br label %cond.end203

cond.end203:                                      ; preds = %cond.false202, %cond.true200
  %cond204 = phi i32 [ %conv201, %cond.true200 ], [ 3, %cond.false202 ]
  %arrayidx205 = getelementptr inbounds [5 x [2 x [21 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]]* %coeff_br_cdf196, i32 0, i32 %cond204
  %228 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom206 = zext i8 %228 to i32
  %arrayidx207 = getelementptr inbounds [2 x [21 x [5 x i16]]], [2 x [21 x [5 x i16]]]* %arrayidx205, i32 0, i32 %idxprom206
  %arraydecay208 = getelementptr inbounds [21 x [5 x i16]], [21 x [5 x i16]]* %arrayidx207, i32 0, i32 0
  store [5 x i16]* %arraydecay208, [5 x i16]** %br_cdf, align 4, !tbaa !2
  %229 = load i8, i8* %tx_class, align 1, !tbaa !8
  %conv209 = zext i8 %229 to i32
  %cmp210 = icmp eq i32 %conv209, 0
  br i1 %cmp210, label %if.then212, label %if.else

if.then212:                                       ; preds = %cond.end203
  %230 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %231 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %232 = load i16*, i16** %eob, align 4, !tbaa !2
  %233 = load i16, i16* %232, align 2, !tbaa !20
  %conv213 = zext i16 %233 to i32
  %sub214 = sub nsw i32 %conv213, 1
  %sub215 = sub nsw i32 %sub214, 1
  %234 = load i16*, i16** %scan, align 4, !tbaa !2
  %235 = load i32, i32* %bwl, align 4, !tbaa !6
  %236 = load i8*, i8** %levels, align 4, !tbaa !2
  %237 = load [5 x i16]*, [5 x i16]** %base_cdf, align 4, !tbaa !2
  %238 = load [5 x i16]*, [5 x i16]** %br_cdf, align 4, !tbaa !2
  call void @read_coeffs_reverse_2d(%struct.aom_reader* %230, i8 zeroext %231, i32 1, i32 %sub215, i16* %234, i32 %235, i8* %236, [5 x i16]* %237, [5 x i16]* %238)
  %239 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %240 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %241 = load i8, i8* %tx_class, align 1, !tbaa !8
  %242 = load i16*, i16** %scan, align 4, !tbaa !2
  %243 = load i32, i32* %bwl, align 4, !tbaa !6
  %244 = load i8*, i8** %levels, align 4, !tbaa !2
  %245 = load [5 x i16]*, [5 x i16]** %base_cdf, align 4, !tbaa !2
  %246 = load [5 x i16]*, [5 x i16]** %br_cdf, align 4, !tbaa !2
  call void @read_coeffs_reverse(%struct.aom_reader* %239, i8 zeroext %240, i8 zeroext %241, i32 0, i32 0, i16* %242, i32 %243, i8* %244, [5 x i16]* %245, [5 x i16]* %246)
  br label %if.end219

if.else:                                          ; preds = %cond.end203
  %247 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %248 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %249 = load i8, i8* %tx_class, align 1, !tbaa !8
  %250 = load i16*, i16** %eob, align 4, !tbaa !2
  %251 = load i16, i16* %250, align 2, !tbaa !20
  %conv216 = zext i16 %251 to i32
  %sub217 = sub nsw i32 %conv216, 1
  %sub218 = sub nsw i32 %sub217, 1
  %252 = load i16*, i16** %scan, align 4, !tbaa !2
  %253 = load i32, i32* %bwl, align 4, !tbaa !6
  %254 = load i8*, i8** %levels, align 4, !tbaa !2
  %255 = load [5 x i16]*, [5 x i16]** %base_cdf, align 4, !tbaa !2
  %256 = load [5 x i16]*, [5 x i16]** %br_cdf, align 4, !tbaa !2
  call void @read_coeffs_reverse(%struct.aom_reader* %247, i8 zeroext %248, i8 zeroext %249, i32 0, i32 %sub218, i16* %252, i32 %253, i8* %254, [5 x i16]* %255, [5 x i16]* %256)
  br label %if.end219

if.end219:                                        ; preds = %if.else, %if.then212
  %257 = bitcast [5 x i16]** %br_cdf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #6
  %258 = bitcast [5 x i16]** %base_cdf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #6
  br label %if.end220

if.end220:                                        ; preds = %if.end219, %if.end183
  %259 = bitcast i32* %c221 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %259) #6
  store i32 0, i32* %c221, align 4, !tbaa !6
  br label %for.cond222

for.cond222:                                      ; preds = %for.inc293, %if.end220
  %260 = load i32, i32* %c221, align 4, !tbaa !6
  %261 = load i16*, i16** %eob, align 4, !tbaa !2
  %262 = load i16, i16* %261, align 2, !tbaa !20
  %conv223 = zext i16 %262 to i32
  %cmp224 = icmp slt i32 %260, %conv223
  br i1 %cmp224, label %for.body227, label %for.cond.cleanup226

for.cond.cleanup226:                              ; preds = %for.cond222
  store i32 9, i32* %cleanup.dest.slot, align 4
  %263 = bitcast i32* %c221 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #6
  br label %for.end296

for.body227:                                      ; preds = %for.cond222
  %264 = bitcast i32* %pos228 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %264) #6
  %265 = load i16*, i16** %scan, align 4, !tbaa !2
  %266 = load i32, i32* %c221, align 4, !tbaa !6
  %arrayidx229 = getelementptr inbounds i16, i16* %265, i32 %266
  %267 = load i16, i16* %arrayidx229, align 2, !tbaa !20
  %conv230 = sext i16 %267 to i32
  store i32 %conv230, i32* %pos228, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %sign) #6
  %268 = bitcast i32* %level231 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %268) #6
  %269 = load i8*, i8** %levels, align 4, !tbaa !2
  %270 = load i32, i32* %pos228, align 4, !tbaa !6
  %271 = load i32, i32* %bwl, align 4, !tbaa !6
  %call232 = call i32 @get_padded_idx(i32 %270, i32 %271)
  %arrayidx233 = getelementptr inbounds i8, i8* %269, i32 %call232
  %272 = load i8, i8* %arrayidx233, align 1, !tbaa !8
  %conv234 = zext i8 %272 to i32
  store i32 %conv234, i32* %level231, align 4, !tbaa !6
  %273 = load i32, i32* %level231, align 4, !tbaa !6
  %tobool235 = icmp ne i32 %273, 0
  br i1 %tobool235, label %if.then236, label %if.end292

if.then236:                                       ; preds = %for.body227
  %274 = load i16*, i16** %max_scan_line, align 4, !tbaa !2
  %275 = load i16, i16* %274, align 2, !tbaa !20
  %conv237 = zext i16 %275 to i32
  %276 = load i32, i32* %pos228, align 4, !tbaa !6
  %cmp238 = icmp sgt i32 %conv237, %276
  br i1 %cmp238, label %cond.true240, label %cond.false242

cond.true240:                                     ; preds = %if.then236
  %277 = load i16*, i16** %max_scan_line, align 4, !tbaa !2
  %278 = load i16, i16* %277, align 2, !tbaa !20
  %conv241 = zext i16 %278 to i32
  br label %cond.end243

cond.false242:                                    ; preds = %if.then236
  %279 = load i32, i32* %pos228, align 4, !tbaa !6
  br label %cond.end243

cond.end243:                                      ; preds = %cond.false242, %cond.true240
  %cond244 = phi i32 [ %conv241, %cond.true240 ], [ %279, %cond.false242 ]
  %conv245 = trunc i32 %cond244 to i16
  %280 = load i16*, i16** %max_scan_line, align 4, !tbaa !2
  store i16 %conv245, i16* %280, align 2, !tbaa !20
  %281 = load i32, i32* %c221, align 4, !tbaa !6
  %cmp246 = icmp eq i32 %281, 0
  br i1 %cmp246, label %if.then248, label %if.else256

if.then248:                                       ; preds = %cond.end243
  %282 = bitcast i32* %dc_sign_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %282) #6
  %283 = load %struct.txb_ctx*, %struct.txb_ctx** %txb_ctx.addr, align 4, !tbaa !2
  %dc_sign_ctx249 = getelementptr inbounds %struct.txb_ctx, %struct.txb_ctx* %283, i32 0, i32 1
  %284 = load i32, i32* %dc_sign_ctx249, align 4, !tbaa !52
  store i32 %284, i32* %dc_sign_ctx, align 4, !tbaa !6
  %285 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %286 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !2
  %dc_sign_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %286, i32 0, i32 2
  %287 = load i8, i8* %plane_type, align 1, !tbaa !8
  %idxprom250 = zext i8 %287 to i32
  %arrayidx251 = getelementptr inbounds [2 x [3 x [3 x i16]]], [2 x [3 x [3 x i16]]]* %dc_sign_cdf, i32 0, i32 %idxprom250
  %288 = load i32, i32* %dc_sign_ctx, align 4, !tbaa !6
  %arrayidx252 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %arrayidx251, i32 0, i32 %288
  %arraydecay253 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx252, i32 0, i32 0
  %call254 = call i32 @aom_read_symbol_(%struct.aom_reader* %285, i16* %arraydecay253, i32 2, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %conv255 = trunc i32 %call254 to i8
  store i8 %conv255, i8* %sign, align 1, !tbaa !8
  %289 = bitcast i32* %dc_sign_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %289) #6
  br label %if.end259

if.else256:                                       ; preds = %cond.end243
  %290 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call257 = call i32 @aom_read_bit_(%struct.aom_reader* %290, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.av1_read_coeffs_txb, i32 0, i32 0))
  %conv258 = trunc i32 %call257 to i8
  store i8 %conv258, i8* %sign, align 1, !tbaa !8
  br label %if.end259

if.end259:                                        ; preds = %if.else256, %if.then248
  %291 = load i32, i32* %level231, align 4, !tbaa !6
  %cmp260 = icmp sge i32 %291, 15
  br i1 %cmp260, label %if.then262, label %if.end265

if.then262:                                       ; preds = %if.end259
  %292 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %293 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call263 = call i32 @read_golomb(%struct.macroblockd* %292, %struct.aom_reader* %293)
  %294 = load i32, i32* %level231, align 4, !tbaa !6
  %add264 = add nsw i32 %294, %call263
  store i32 %add264, i32* %level231, align 4, !tbaa !6
  br label %if.end265

if.end265:                                        ; preds = %if.then262, %if.end259
  %295 = load i32, i32* %c221, align 4, !tbaa !6
  %cmp266 = icmp eq i32 %295, 0
  br i1 %cmp266, label %if.then268, label %if.end276

if.then268:                                       ; preds = %if.end265
  %296 = load i8, i8* %sign, align 1, !tbaa !8
  %conv269 = zext i8 %296 to i32
  %tobool270 = icmp ne i32 %conv269, 0
  br i1 %tobool270, label %cond.true271, label %cond.false273

cond.true271:                                     ; preds = %if.then268
  %297 = load i32, i32* %level231, align 4, !tbaa !6
  %sub272 = sub nsw i32 0, %297
  br label %cond.end274

cond.false273:                                    ; preds = %if.then268
  %298 = load i32, i32* %level231, align 4, !tbaa !6
  br label %cond.end274

cond.end274:                                      ; preds = %cond.false273, %cond.true271
  %cond275 = phi i32 [ %sub272, %cond.true271 ], [ %298, %cond.false273 ]
  store i32 %cond275, i32* %dc_val, align 4, !tbaa !6
  br label %if.end276

if.end276:                                        ; preds = %cond.end274, %if.end265
  %299 = load i32, i32* %level231, align 4, !tbaa !6
  %and = and i32 %299, 1048575
  store i32 %and, i32* %level231, align 4, !tbaa !6
  %300 = load i32, i32* %level231, align 4, !tbaa !6
  %301 = load i32, i32* %cul_level, align 4, !tbaa !6
  %add277 = add nsw i32 %301, %300
  store i32 %add277, i32* %cul_level, align 4, !tbaa !6
  %302 = bitcast i32* %dq_coeff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %302) #6
  %303 = load i32, i32* %level231, align 4, !tbaa !6
  %conv278 = sext i32 %303 to i64
  %304 = load i16*, i16** %dequant, align 4, !tbaa !2
  %305 = load i16*, i16** %scan, align 4, !tbaa !2
  %306 = load i32, i32* %c221, align 4, !tbaa !6
  %arrayidx279 = getelementptr inbounds i16, i16* %305, i32 %306
  %307 = load i16, i16* %arrayidx279, align 2, !tbaa !20
  %conv280 = sext i16 %307 to i32
  %308 = load i8*, i8** %iqmatrix, align 4, !tbaa !2
  %call281 = call i32 @get_dqv(i16* %304, i32 %conv280, i8* %308)
  %conv282 = sext i32 %call281 to i64
  %mul283 = mul nsw i64 %conv278, %conv282
  %and284 = and i64 %mul283, 16777215
  %conv285 = trunc i64 %and284 to i32
  store i32 %conv285, i32* %dq_coeff, align 4, !tbaa !6
  %309 = load i32, i32* %dq_coeff, align 4, !tbaa !6
  %310 = load i32, i32* %shift, align 4, !tbaa !6
  %shr = ashr i32 %309, %310
  store i32 %shr, i32* %dq_coeff, align 4, !tbaa !6
  %311 = load i8, i8* %sign, align 1, !tbaa !8
  %tobool286 = icmp ne i8 %311, 0
  br i1 %tobool286, label %if.then287, label %if.end289

if.then287:                                       ; preds = %if.end276
  %312 = load i32, i32* %dq_coeff, align 4, !tbaa !6
  %sub288 = sub nsw i32 0, %312
  store i32 %sub288, i32* %dq_coeff, align 4, !tbaa !6
  br label %if.end289

if.end289:                                        ; preds = %if.then287, %if.end276
  %313 = load i32, i32* %dq_coeff, align 4, !tbaa !6
  %314 = load i32, i32* %min_value, align 4, !tbaa !6
  %315 = load i32, i32* %max_value, align 4, !tbaa !6
  %call290 = call i32 @clamp(i32 %313, i32 %314, i32 %315)
  %316 = load i32*, i32** %tcoeffs, align 4, !tbaa !2
  %317 = load i32, i32* %pos228, align 4, !tbaa !6
  %arrayidx291 = getelementptr inbounds i32, i32* %316, i32 %317
  store i32 %call290, i32* %arrayidx291, align 4, !tbaa !6
  %318 = bitcast i32* %dq_coeff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %318) #6
  br label %if.end292

if.end292:                                        ; preds = %if.end289, %for.body227
  %319 = bitcast i32* %level231 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %sign) #6
  %320 = bitcast i32* %pos228 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #6
  br label %for.inc293

for.inc293:                                       ; preds = %if.end292
  %321 = load i32, i32* %c221, align 4, !tbaa !6
  %inc294 = add nsw i32 %321, 1
  store i32 %inc294, i32* %c221, align 4, !tbaa !6
  br label %for.cond222

for.end296:                                       ; preds = %for.cond.cleanup226
  %322 = load i32, i32* %cul_level, align 4, !tbaa !6
  %cmp297 = icmp slt i32 7, %322
  br i1 %cmp297, label %cond.true299, label %cond.false300

cond.true299:                                     ; preds = %for.end296
  br label %cond.end301

cond.false300:                                    ; preds = %for.end296
  %323 = load i32, i32* %cul_level, align 4, !tbaa !6
  br label %cond.end301

cond.end301:                                      ; preds = %cond.false300, %cond.true299
  %cond302 = phi i32 [ 7, %cond.true299 ], [ %323, %cond.false300 ]
  store i32 %cond302, i32* %cul_level, align 4, !tbaa !6
  %324 = load i32, i32* %dc_val, align 4, !tbaa !6
  call void @set_dc_sign(i32* %cul_level, i32 %324)
  %325 = load i32, i32* %cul_level, align 4, !tbaa !6
  %conv303 = trunc i32 %325 to i8
  store i8 %conv303, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %326 = bitcast i32* %eob_offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %326) #6
  %327 = bitcast i32* %eob_multi_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %327) #6
  %328 = bitcast i32* %eob_multi_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %328) #6
  %329 = bitcast i32* %eob_pt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %329) #6
  %330 = bitcast i32* %eob_extra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %330) #6
  %331 = bitcast i16** %scan to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #6
  %332 = bitcast %struct.SCAN_ORDER** %scan_order to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #6
  %333 = bitcast i8** %iqmatrix to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_class) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type) #6
  br label %cleanup314

cleanup314:                                       ; preds = %cond.end301, %if.end
  %334 = bitcast i16** %max_scan_line to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #6
  %335 = bitcast i16** %eob to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %335) #6
  %336 = bitcast %struct.eob_info** %eob_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #6
  %337 = bitcast i32* %all_zero to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #6
  %338 = bitcast i8** %levels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #6
  %339 = bitcast [1312 x i8]* %levels_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 1312, i8* %339) #6
  %340 = bitcast i32* %dc_val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #6
  %341 = bitcast i32* %cul_level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %341) #6
  %342 = bitcast i32* %height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %342) #6
  %343 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %343) #6
  %344 = bitcast i32* %bwl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %344) #6
  %345 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %345) #6
  %346 = bitcast i32** %tcoeffs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %346) #6
  %347 = bitcast i16** %dequant to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %347) #6
  %348 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %348) #6
  %349 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %349) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %plane_type) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %txs_ctx) #6
  %350 = bitcast i32* %min_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %350) #6
  %351 = bitcast i32* %max_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %351) #6
  %352 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %352) #6
  %353 = load i8, i8* %retval, align 1
  ret i8 %353
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_txsize_entropy_ctx(i8 zeroext %txsize) #2 {
entry:
  %txsize.addr = alloca i8, align 1
  store i8 %txsize, i8* %txsize.addr, align 1, !tbaa !8
  %0 = load i8, i8* %txsize.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_map, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %1 to i32
  %2 = load i8, i8* %txsize.addr, align 1, !tbaa !8
  %idxprom1 = zext i8 %2 to i32
  %arrayidx2 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_up_map, i32 0, i32 %idxprom1
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = zext i8 %3 to i32
  %add = add nsw i32 %conv, %conv3
  %add4 = add nsw i32 %add, 1
  %shr = ashr i32 %add4, 1
  %conv5 = trunc i32 %shr to i8
  ret i8 %conv5
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_plane_type(i32 %plane) #2 {
entry:
  %plane.addr = alloca i32, align 4
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  %0 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, 0
  %1 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 0, i32 1
  %conv = trunc i32 %cond to i8
  ret i8 %conv
}

declare i32 @av1_get_tx_scale(i8 zeroext) #3

; Function Attrs: inlinehint nounwind
define internal i32 @get_txb_bwl(i8 zeroext %tx_size) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call = call zeroext i8 @av1_get_adjusted_tx_size(i8 zeroext %0)
  store i8 %call, i8* %tx_size.addr, align 1, !tbaa !8
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_log2, i32 0, i32 %idxprom
  %2 = load i32, i32* %arrayidx, align 4, !tbaa !6
  ret i32 %2
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_txb_wide(i8 zeroext %tx_size) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call = call zeroext i8 @av1_get_adjusted_tx_size(i8 zeroext %0)
  store i8 %call, i8* %tx_size.addr, align 1, !tbaa !8
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %2 = load i32, i32* %arrayidx, align 4, !tbaa !6
  ret i32 %2
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_txb_high(i8 zeroext %tx_size) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call = call zeroext i8 @av1_get_adjusted_tx_size(i8 zeroext %0)
  store i8 %call, i8* %tx_size.addr, align 1, !tbaa !8
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom
  %2 = load i32, i32* %arrayidx, align 4, !tbaa !6
  ret i32 %2
}

; Function Attrs: inlinehint nounwind
define internal i8* @set_levels(i8* %levels_buf, i32 %width) #2 {
entry:
  %levels_buf.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  store i8* %levels_buf, i8** %levels_buf.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %levels_buf.addr, align 4, !tbaa !2
  %1 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add = add nsw i32 %1, 4
  %mul = mul nsw i32 0, %add
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 %mul
  ret i8* %add.ptr
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_symbol_(%struct.aom_reader* %r, i16* %cdf, i32 %nsymbs, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %cdf.addr = alloca i16*, align 4
  %nsymbs.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %ret = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !2
  store i32 %nsymbs, i32* %nsymbs.addr, align 4, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %3 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %call = call i32 @aom_read_cdf_(%struct.aom_reader* %1, i16* %2, i32 %3, i8* %4)
  store i32 %call, i32* %ret, align 4, !tbaa !6
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %allow_update_cdf = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %5, i32 0, i32 4
  %6 = load i8, i8* %allow_update_cdf, align 4, !tbaa !53
  %tobool = icmp ne i8 %6, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %8 = load i32, i32* %ret, align 4, !tbaa !6
  %conv = trunc i32 %8 to i8
  %9 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  call void @update_cdf(i16* %7, i8 signext %conv, i32 %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i32, i32* %ret, align 4, !tbaa !6
  %11 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret i32 %10
}

declare void @av1_read_tx_type(%struct.AV1Common*, %struct.macroblockd*, i32, i32, i8 zeroext, %struct.aom_reader*) #3

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_get_tx_type(%struct.macroblockd* %xd, i8 zeroext %plane_type, i32 %blk_row, i32 %blk_col, i8 zeroext %tx_size, i32 %reduced_tx_set) #2 {
entry:
  %retval = alloca i8, align 1
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane_type.addr = alloca i8, align 1
  %blk_row.addr = alloca i32, align 4
  %blk_col.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %reduced_tx_set.addr = alloca i32, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tx_type = alloca i8, align 1
  %pd = alloca %struct.macroblockd_plane*, align 4
  %tx_set_type = alloca i8, align 1
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %plane_type, i8* %plane_type.addr, align 1, !tbaa !8
  store i32 %blk_row, i32* %blk_row.addr, align 4, !tbaa !6
  store i32 %blk_col, i32* %blk_col.addr, align 4, !tbaa !6
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i32 %reduced_tx_set, i32* %reduced_tx_set.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %lossless = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 43
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %segment_id = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id, align 4
  %bf.lshr = lshr i8 %bf.load, 1
  %bf.clear = and i8 %bf.lshr, 7
  %idxprom = zext i8 %bf.clear to i32
  %arrayidx1 = getelementptr inbounds [8 x i32], [8 x i32]* %lossless, i32 0, i32 %idxprom
  %6 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %7 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom2 = zext i8 %7 to i32
  %arrayidx3 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_up_map, i32 0, i32 %idxprom2
  %8 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  %conv = zext i8 %8 to i32
  %cmp = icmp sgt i32 %conv, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i8 0, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type) #6
  %9 = load i8, i8* %plane_type.addr, align 1, !tbaa !8
  %conv5 = zext i8 %9 to i32
  %cmp6 = icmp eq i32 %conv5, 0
  br i1 %cmp6, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.end
  %10 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tx_type_map = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %10, i32 0, i32 15
  %11 = load i8*, i8** %tx_type_map, align 4, !tbaa !25
  %12 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tx_type_map_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %13, i32 0, i32 16
  %14 = load i32, i32* %tx_type_map_stride, align 16, !tbaa !26
  %mul = mul nsw i32 %12, %14
  %15 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %add = add nsw i32 %mul, %15
  %arrayidx9 = getelementptr inbounds i8, i8* %11, i32 %add
  %16 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  store i8 %16, i8* %tx_type, align 1, !tbaa !8
  br label %if.end32

if.else:                                          ; preds = %if.end
  %17 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %17)
  %tobool10 = icmp ne i32 %call, 0
  br i1 %tobool10, label %if.then11, label %if.else20

if.then11:                                        ; preds = %if.else
  %18 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %19, i32 0, i32 4
  %20 = load i8, i8* %plane_type.addr, align 1, !tbaa !8
  %idxprom12 = zext i8 %20 to i32
  %arrayidx13 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 %idxprom12
  store %struct.macroblockd_plane* %arrayidx13, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %21 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %21, i32 0, i32 5
  %22 = load i32, i32* %subsampling_y, align 4, !tbaa !56
  %23 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %shl = shl i32 %23, %22
  store i32 %shl, i32* %blk_row.addr, align 4, !tbaa !6
  %24 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %24, i32 0, i32 4
  %25 = load i32, i32* %subsampling_x, align 4, !tbaa !57
  %26 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %shl14 = shl i32 %26, %25
  store i32 %shl14, i32* %blk_col.addr, align 4, !tbaa !6
  %27 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tx_type_map15 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %27, i32 0, i32 15
  %28 = load i8*, i8** %tx_type_map15, align 4, !tbaa !25
  %29 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %30 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tx_type_map_stride16 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %30, i32 0, i32 16
  %31 = load i32, i32* %tx_type_map_stride16, align 16, !tbaa !26
  %mul17 = mul nsw i32 %29, %31
  %32 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %add18 = add nsw i32 %mul17, %32
  %arrayidx19 = getelementptr inbounds i8, i8* %28, i32 %add18
  %33 = load i8, i8* %arrayidx19, align 1, !tbaa !8
  store i8 %33, i8* %tx_type, align 1, !tbaa !8
  %34 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  br label %if.end22

if.else20:                                        ; preds = %if.else
  %35 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %call21 = call zeroext i8 @intra_mode_to_tx_type(%struct.MB_MODE_INFO* %35, i8 zeroext 1)
  store i8 %call21, i8* %tx_type, align 1, !tbaa !8
  br label %if.end22

if.end22:                                         ; preds = %if.else20, %if.then11
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_set_type) #6
  %36 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %37 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %call23 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %37)
  %38 = load i32, i32* %reduced_tx_set.addr, align 4, !tbaa !6
  %call24 = call zeroext i8 @av1_get_ext_tx_set_type(i8 zeroext %36, i32 %call23, i32 %38)
  store i8 %call24, i8* %tx_set_type, align 1, !tbaa !8
  %39 = load i8, i8* %tx_set_type, align 1, !tbaa !8
  %idxprom25 = zext i8 %39 to i32
  %arrayidx26 = getelementptr inbounds [6 x [16 x i32]], [6 x [16 x i32]]* bitcast (<{ <{ i32, [15 x i32] }>, [16 x i32], [16 x i32], [16 x i32], [16 x i32], [16 x i32] }>* @av1_ext_tx_used to [6 x [16 x i32]]*), i32 0, i32 %idxprom25
  %40 = load i8, i8* %tx_type, align 1, !tbaa !8
  %idxprom27 = zext i8 %40 to i32
  %arrayidx28 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx26, i32 0, i32 %idxprom27
  %41 = load i32, i32* %arrayidx28, align 4, !tbaa !6
  %tobool29 = icmp ne i32 %41, 0
  br i1 %tobool29, label %if.end31, label %if.then30

if.then30:                                        ; preds = %if.end22
  store i8 0, i8* %tx_type, align 1, !tbaa !8
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %if.end22
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_set_type) #6
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.then8
  %42 = load i8, i8* %tx_type, align 1, !tbaa !8
  store i8 %42, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end32, %if.then
  %43 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  %44 = load i8, i8* %retval, align 1
  ret i8 %44
}

declare i8* @av1_get_iqmatrix(%struct.CommonQuantParams*, %struct.macroblockd*, i32, i8 zeroext, i8 zeroext) #3

; Function Attrs: inlinehint nounwind
define internal %struct.SCAN_ORDER* @get_scan(i8 zeroext %tx_size, i8 zeroext %tx_type) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  %tx_type.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !8
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %1 = load i8, i8* %tx_type.addr, align 1, !tbaa !8
  %call = call %struct.SCAN_ORDER* @get_default_scan(i8 zeroext %0, i8 zeroext %1)
  ret %struct.SCAN_ORDER* %call
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_bit_(%struct.aom_reader* %r, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %acct_str.addr = alloca i8*, align 4
  %ret = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_read_(%struct.aom_reader* %1, i32 128, i8* null)
  store i32 %call, i32* %ret, align 4, !tbaa !6
  %2 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %3, i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i32, i32* %ret, align 4, !tbaa !6
  %6 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret i32 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @rec_eob_pos(i32 %eob_token, i32 %extra) #2 {
entry:
  %eob_token.addr = alloca i32, align 4
  %extra.addr = alloca i32, align 4
  %eob = alloca i32, align 4
  store i32 %eob_token, i32* %eob_token.addr, align 4, !tbaa !6
  store i32 %extra, i32* %extra.addr, align 4, !tbaa !6
  %0 = bitcast i32* %eob to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %eob_token.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [12 x i16], [12 x i16]* @av1_eob_group_start, i32 0, i32 %1
  %2 = load i16, i16* %arrayidx, align 2, !tbaa !20
  %conv = sext i16 %2 to i32
  store i32 %conv, i32* %eob, align 4, !tbaa !6
  %3 = load i32, i32* %eob, align 4, !tbaa !6
  %cmp = icmp sgt i32 %3, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %extra.addr, align 4, !tbaa !6
  %5 = load i32, i32* %eob, align 4, !tbaa !6
  %add = add nsw i32 %5, %4
  store i32 %add, i32* %eob, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load i32, i32* %eob, align 4, !tbaa !6
  %7 = bitcast i32* %eob to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret i32 %6
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: inlinehint nounwind
define internal i32 @get_lower_levels_ctx_eob(i32 %bwl, i32 %height, i32 %scan_idx) #2 {
entry:
  %retval = alloca i32, align 4
  %bwl.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %scan_idx.addr = alloca i32, align 4
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %scan_idx, i32* %scan_idx.addr, align 4, !tbaa !6
  %0 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %3 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl = shl i32 %2, %3
  %div = sdiv i32 %shl, 8
  %cmp1 = icmp sle i32 %1, %div
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %4 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %5 = load i32, i32* %height.addr, align 4, !tbaa !6
  %6 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl4 = shl i32 %5, %6
  %div5 = sdiv i32 %shl4, 4
  %cmp6 = icmp sle i32 %4, %div5
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end3
  store i32 2, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %if.end3
  store i32 3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end8, %if.then7, %if.then2, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: alwaysinline nounwind
define internal i32 @get_br_ctx_eob(i32 %c, i32 %bwl, i8 zeroext %tx_class) #5 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i32, align 4
  %bwl.addr = alloca i32, align 4
  %tx_class.addr = alloca i8, align 1
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %c, i32* %c.addr, align 4, !tbaa !6
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  store i8 %tx_class, i8* %tx_class.addr, align 1, !tbaa !8
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %c.addr, align 4, !tbaa !6
  %2 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shr = ashr i32 %1, %2
  store i32 %shr, i32* %row, align 4, !tbaa !6
  %3 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %c.addr, align 4, !tbaa !6
  %5 = load i32, i32* %row, align 4, !tbaa !6
  %6 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl = shl i32 %5, %6
  %sub = sub nsw i32 %4, %shl
  store i32 %sub, i32* %col, align 4, !tbaa !6
  %7 = load i32, i32* %c.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %7, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %8 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %conv = zext i8 %8 to i32
  %cmp1 = icmp eq i32 %conv, 0
  br i1 %cmp1, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %if.end
  %9 = load i32, i32* %row, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %9, 2
  br i1 %cmp3, label %land.lhs.true5, label %lor.lhs.false

land.lhs.true5:                                   ; preds = %land.lhs.true
  %10 = load i32, i32* %col, align 4, !tbaa !6
  %cmp6 = icmp slt i32 %10, 2
  br i1 %cmp6, label %if.then21, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true5, %land.lhs.true, %if.end
  %11 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %conv8 = zext i8 %11 to i32
  %cmp9 = icmp eq i32 %conv8, 1
  br i1 %cmp9, label %land.lhs.true11, label %lor.lhs.false14

land.lhs.true11:                                  ; preds = %lor.lhs.false
  %12 = load i32, i32* %col, align 4, !tbaa !6
  %cmp12 = icmp eq i32 %12, 0
  br i1 %cmp12, label %if.then21, label %lor.lhs.false14

lor.lhs.false14:                                  ; preds = %land.lhs.true11, %lor.lhs.false
  %13 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %conv15 = zext i8 %13 to i32
  %cmp16 = icmp eq i32 %conv15, 2
  br i1 %cmp16, label %land.lhs.true18, label %if.end22

land.lhs.true18:                                  ; preds = %lor.lhs.false14
  %14 = load i32, i32* %row, align 4, !tbaa !6
  %cmp19 = icmp eq i32 %14, 0
  br i1 %cmp19, label %if.then21, label %if.end22

if.then21:                                        ; preds = %land.lhs.true18, %land.lhs.true11, %land.lhs.true5
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %land.lhs.true18, %lor.lhs.false14
  store i32 14, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end22, %if.then21, %if.then
  %15 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_padded_idx(i32 %idx, i32 %bwl) #2 {
entry:
  %idx.addr = alloca i32, align 4
  %bwl.addr = alloca i32, align 4
  store i32 %idx, i32* %idx.addr, align 4, !tbaa !6
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  %0 = load i32, i32* %idx.addr, align 4, !tbaa !6
  %1 = load i32, i32* %idx.addr, align 4, !tbaa !6
  %2 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shr = ashr i32 %1, %2
  %shl = shl i32 %shr, 2
  %add = add nsw i32 %0, %shl
  ret i32 %add
}

; Function Attrs: inlinehint nounwind
define internal void @read_coeffs_reverse_2d(%struct.aom_reader* %r, i8 zeroext %tx_size, i32 %start_si, i32 %end_si, i16* %scan, i32 %bwl, i8* %levels, [5 x i16]* %base_cdf, [5 x i16]* %br_cdf) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %tx_size.addr = alloca i8, align 1
  %start_si.addr = alloca i32, align 4
  %end_si.addr = alloca i32, align 4
  %scan.addr = alloca i16*, align 4
  %bwl.addr = alloca i32, align 4
  %levels.addr = alloca i8*, align 4
  %base_cdf.addr = alloca [5 x i16]*, align 4
  %br_cdf.addr = alloca [5 x i16]*, align 4
  %c = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pos = alloca i32, align 4
  %coeff_ctx = alloca i32, align 4
  %nsymbs = alloca i32, align 4
  %level = alloca i32, align 4
  %br_ctx = alloca i32, align 4
  %cdf = alloca i16*, align 4
  %idx = alloca i32, align 4
  %k = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i32 %start_si, i32* %start_si.addr, align 4, !tbaa !6
  store i32 %end_si, i32* %end_si.addr, align 4, !tbaa !6
  store i16* %scan, i16** %scan.addr, align 4, !tbaa !2
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  store i8* %levels, i8** %levels.addr, align 4, !tbaa !2
  store [5 x i16]* %base_cdf, [5 x i16]** %base_cdf.addr, align 4, !tbaa !2
  store [5 x i16]* %br_cdf, [5 x i16]** %br_cdf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %end_si.addr, align 4, !tbaa !6
  store i32 %1, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc23, %entry
  %2 = load i32, i32* %c, align 4, !tbaa !6
  %3 = load i32, i32* %start_si.addr, align 4, !tbaa !6
  %cmp = icmp sge i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  br label %for.end25

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i16*, i16** %scan.addr, align 4, !tbaa !2
  %7 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx, align 2, !tbaa !20
  %conv = sext i16 %8 to i32
  store i32 %conv, i32* %pos, align 4, !tbaa !6
  %9 = bitcast i32* %coeff_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %11 = load i32, i32* %pos, align 4, !tbaa !6
  %12 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %13 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call = call i32 @get_lower_levels_ctx_2d(i8* %10, i32 %11, i32 %12, i8 zeroext %13)
  store i32 %call, i32* %coeff_ctx, align 4, !tbaa !6
  %14 = bitcast i32* %nsymbs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  store i32 4, i32* %nsymbs, align 4, !tbaa !6
  %15 = bitcast i32* %level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %17 = load [5 x i16]*, [5 x i16]** %base_cdf.addr, align 4, !tbaa !2
  %18 = load i32, i32* %coeff_ctx, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds [5 x i16], [5 x i16]* %17, i32 %18
  %arraydecay = getelementptr inbounds [5 x i16], [5 x i16]* %arrayidx1, i32 0, i32 0
  %call2 = call i32 @aom_read_symbol_(%struct.aom_reader* %16, i16* %arraydecay, i32 4, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_coeffs_reverse_2d, i32 0, i32 0))
  store i32 %call2, i32* %level, align 4, !tbaa !6
  %19 = load i32, i32* %level, align 4, !tbaa !6
  %cmp3 = icmp sgt i32 %19, 2
  br i1 %cmp3, label %if.then, label %if.end19

if.then:                                          ; preds = %for.body
  %20 = bitcast i32* %br_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %22 = load i32, i32* %pos, align 4, !tbaa !6
  %23 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %call5 = call i32 @get_br_ctx_2d(i8* %21, i32 %22, i32 %23)
  store i32 %call5, i32* %br_ctx, align 4, !tbaa !6
  %24 = bitcast i16** %cdf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load [5 x i16]*, [5 x i16]** %br_cdf.addr, align 4, !tbaa !2
  %26 = load i32, i32* %br_ctx, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [5 x i16], [5 x i16]* %25, i32 %26
  %arraydecay7 = getelementptr inbounds [5 x i16], [5 x i16]* %arrayidx6, i32 0, i32 0
  store i16* %arraydecay7, i16** %cdf, align 4, !tbaa !2
  %27 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.then
  %28 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %28, 12
  br i1 %cmp9, label %for.body12, label %for.cond.cleanup11

for.cond.cleanup11:                               ; preds = %for.cond8
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup18

for.body12:                                       ; preds = %for.cond8
  %29 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %31 = load i16*, i16** %cdf, align 4, !tbaa !2
  %call13 = call i32 @aom_read_symbol_(%struct.aom_reader* %30, i16* %31, i32 4, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_coeffs_reverse_2d, i32 0, i32 0))
  store i32 %call13, i32* %k, align 4, !tbaa !6
  %32 = load i32, i32* %k, align 4, !tbaa !6
  %33 = load i32, i32* %level, align 4, !tbaa !6
  %add = add nsw i32 %33, %32
  store i32 %add, i32* %level, align 4, !tbaa !6
  %34 = load i32, i32* %k, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %34, 3
  br i1 %cmp14, label %if.then16, label %if.end

if.then16:                                        ; preds = %for.body12
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then16
  %35 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup18 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %36 = load i32, i32* %idx, align 4, !tbaa !6
  %add17 = add nsw i32 %36, 3
  store i32 %add17, i32* %idx, align 4, !tbaa !6
  br label %for.cond8

cleanup18:                                        ; preds = %cleanup, %for.cond.cleanup11
  %37 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  br label %for.end

for.end:                                          ; preds = %cleanup18
  %38 = bitcast i16** %cdf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  %39 = bitcast i32* %br_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  br label %if.end19

if.end19:                                         ; preds = %for.end, %for.body
  %40 = load i32, i32* %level, align 4, !tbaa !6
  %conv20 = trunc i32 %40 to i8
  %41 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %42 = load i32, i32* %pos, align 4, !tbaa !6
  %43 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %call21 = call i32 @get_padded_idx(i32 %42, i32 %43)
  %arrayidx22 = getelementptr inbounds i8, i8* %41, i32 %call21
  store i8 %conv20, i8* %arrayidx22, align 1, !tbaa !8
  %44 = bitcast i32* %level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  %45 = bitcast i32* %nsymbs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  %46 = bitcast i32* %coeff_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  %47 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  br label %for.inc23

for.inc23:                                        ; preds = %if.end19
  %48 = load i32, i32* %c, align 4, !tbaa !6
  %dec = add nsw i32 %48, -1
  store i32 %dec, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.end25:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @read_coeffs_reverse(%struct.aom_reader* %r, i8 zeroext %tx_size, i8 zeroext %tx_class, i32 %start_si, i32 %end_si, i16* %scan, i32 %bwl, i8* %levels, [5 x i16]* %base_cdf, [5 x i16]* %br_cdf) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %tx_size.addr = alloca i8, align 1
  %tx_class.addr = alloca i8, align 1
  %start_si.addr = alloca i32, align 4
  %end_si.addr = alloca i32, align 4
  %scan.addr = alloca i16*, align 4
  %bwl.addr = alloca i32, align 4
  %levels.addr = alloca i8*, align 4
  %base_cdf.addr = alloca [5 x i16]*, align 4
  %br_cdf.addr = alloca [5 x i16]*, align 4
  %c = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pos = alloca i32, align 4
  %coeff_ctx = alloca i32, align 4
  %nsymbs = alloca i32, align 4
  %level = alloca i32, align 4
  %br_ctx = alloca i32, align 4
  %cdf = alloca i16*, align 4
  %idx = alloca i32, align 4
  %k = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i8 %tx_class, i8* %tx_class.addr, align 1, !tbaa !8
  store i32 %start_si, i32* %start_si.addr, align 4, !tbaa !6
  store i32 %end_si, i32* %end_si.addr, align 4, !tbaa !6
  store i16* %scan, i16** %scan.addr, align 4, !tbaa !2
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  store i8* %levels, i8** %levels.addr, align 4, !tbaa !2
  store [5 x i16]* %base_cdf, [5 x i16]** %base_cdf.addr, align 4, !tbaa !2
  store [5 x i16]* %br_cdf, [5 x i16]** %br_cdf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %end_si.addr, align 4, !tbaa !6
  store i32 %1, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc23, %entry
  %2 = load i32, i32* %c, align 4, !tbaa !6
  %3 = load i32, i32* %start_si.addr, align 4, !tbaa !6
  %cmp = icmp sge i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  br label %for.end25

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i16*, i16** %scan.addr, align 4, !tbaa !2
  %7 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx, align 2, !tbaa !20
  %conv = sext i16 %8 to i32
  store i32 %conv, i32* %pos, align 4, !tbaa !6
  %9 = bitcast i32* %coeff_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %11 = load i32, i32* %pos, align 4, !tbaa !6
  %12 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %13 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %14 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %call = call i32 @get_lower_levels_ctx(i8* %10, i32 %11, i32 %12, i8 zeroext %13, i8 zeroext %14)
  store i32 %call, i32* %coeff_ctx, align 4, !tbaa !6
  %15 = bitcast i32* %nsymbs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  store i32 4, i32* %nsymbs, align 4, !tbaa !6
  %16 = bitcast i32* %level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %18 = load [5 x i16]*, [5 x i16]** %base_cdf.addr, align 4, !tbaa !2
  %19 = load i32, i32* %coeff_ctx, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds [5 x i16], [5 x i16]* %18, i32 %19
  %arraydecay = getelementptr inbounds [5 x i16], [5 x i16]* %arrayidx1, i32 0, i32 0
  %call2 = call i32 @aom_read_symbol_(%struct.aom_reader* %17, i16* %arraydecay, i32 4, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.read_coeffs_reverse, i32 0, i32 0))
  store i32 %call2, i32* %level, align 4, !tbaa !6
  %20 = load i32, i32* %level, align 4, !tbaa !6
  %cmp3 = icmp sgt i32 %20, 2
  br i1 %cmp3, label %if.then, label %if.end19

if.then:                                          ; preds = %for.body
  %21 = bitcast i32* %br_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %23 = load i32, i32* %pos, align 4, !tbaa !6
  %24 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %25 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %call5 = call i32 @get_br_ctx(i8* %22, i32 %23, i32 %24, i8 zeroext %25)
  store i32 %call5, i32* %br_ctx, align 4, !tbaa !6
  %26 = bitcast i16** %cdf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load [5 x i16]*, [5 x i16]** %br_cdf.addr, align 4, !tbaa !2
  %28 = load i32, i32* %br_ctx, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [5 x i16], [5 x i16]* %27, i32 %28
  %arraydecay7 = getelementptr inbounds [5 x i16], [5 x i16]* %arrayidx6, i32 0, i32 0
  store i16* %arraydecay7, i16** %cdf, align 4, !tbaa !2
  %29 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.then
  %30 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %30, 12
  br i1 %cmp9, label %for.body12, label %for.cond.cleanup11

for.cond.cleanup11:                               ; preds = %for.cond8
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup18

for.body12:                                       ; preds = %for.cond8
  %31 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %33 = load i16*, i16** %cdf, align 4, !tbaa !2
  %call13 = call i32 @aom_read_symbol_(%struct.aom_reader* %32, i16* %33, i32 4, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.read_coeffs_reverse, i32 0, i32 0))
  store i32 %call13, i32* %k, align 4, !tbaa !6
  %34 = load i32, i32* %k, align 4, !tbaa !6
  %35 = load i32, i32* %level, align 4, !tbaa !6
  %add = add nsw i32 %35, %34
  store i32 %add, i32* %level, align 4, !tbaa !6
  %36 = load i32, i32* %k, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %36, 3
  br i1 %cmp14, label %if.then16, label %if.end

if.then16:                                        ; preds = %for.body12
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then16
  %37 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup18 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %38 = load i32, i32* %idx, align 4, !tbaa !6
  %add17 = add nsw i32 %38, 3
  store i32 %add17, i32* %idx, align 4, !tbaa !6
  br label %for.cond8

cleanup18:                                        ; preds = %cleanup, %for.cond.cleanup11
  %39 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  br label %for.end

for.end:                                          ; preds = %cleanup18
  %40 = bitcast i16** %cdf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #6
  %41 = bitcast i32* %br_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #6
  br label %if.end19

if.end19:                                         ; preds = %for.end, %for.body
  %42 = load i32, i32* %level, align 4, !tbaa !6
  %conv20 = trunc i32 %42 to i8
  %43 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %44 = load i32, i32* %pos, align 4, !tbaa !6
  %45 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %call21 = call i32 @get_padded_idx(i32 %44, i32 %45)
  %arrayidx22 = getelementptr inbounds i8, i8* %43, i32 %call21
  store i8 %conv20, i8* %arrayidx22, align 1, !tbaa !8
  %46 = bitcast i32* %level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  %47 = bitcast i32* %nsymbs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  %48 = bitcast i32* %coeff_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #6
  %49 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  br label %for.inc23

for.inc23:                                        ; preds = %if.end19
  %50 = load i32, i32* %c, align 4, !tbaa !6
  %dec = add nsw i32 %50, -1
  store i32 %dec, i32* %c, align 4, !tbaa !6
  br label %for.cond

for.end25:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal i32 @read_golomb(%struct.macroblockd* %xd, %struct.aom_reader* %r) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %x = alloca i32, align 4
  %length = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %0 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 1, i32* %x, align 4, !tbaa !6
  %1 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %length, align 4, !tbaa !6
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  %lnot = xor i1 %tobool, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_read_bit_(%struct.aom_reader* %4, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @__func__.read_golomb, i32 0, i32 0))
  store i32 %call, i32* %i, align 4, !tbaa !6
  %5 = load i32, i32* %length, align 4, !tbaa !6
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %length, align 4, !tbaa !6
  %6 = load i32, i32* %length, align 4, !tbaa !6
  %cmp = icmp sgt i32 %6, 20
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %error_info = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 46
  %8 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info, align 16, !tbaa !58
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %8, i32 7, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0))
  br label %while.end

if.end:                                           ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %if.then, %while.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.end
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = load i32, i32* %length, align 4, !tbaa !6
  %sub = sub nsw i32 %10, 1
  %cmp1 = icmp slt i32 %9, %sub
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %x, align 4, !tbaa !6
  %shl = shl i32 %11, 1
  store i32 %shl, i32* %x, align 4, !tbaa !6
  %12 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call2 = call i32 @aom_read_bit_(%struct.aom_reader* %12, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @__func__.read_golomb, i32 0, i32 0))
  %13 = load i32, i32* %x, align 4, !tbaa !6
  %add = add nsw i32 %13, %call2
  store i32 %add, i32* %x, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %inc3 = add nsw i32 %14, 1
  store i32 %inc3, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load i32, i32* %x, align 4, !tbaa !6
  %sub4 = sub nsw i32 %15, 1
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  ret i32 %sub4
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_dqv(i16* %dequant, i32 %coeff_idx, i8* %iqmatrix) #2 {
entry:
  %dequant.addr = alloca i16*, align 4
  %coeff_idx.addr = alloca i32, align 4
  %iqmatrix.addr = alloca i8*, align 4
  %dqv = alloca i32, align 4
  store i16* %dequant, i16** %dequant.addr, align 4, !tbaa !2
  store i32 %coeff_idx, i32* %coeff_idx.addr, align 4, !tbaa !6
  store i8* %iqmatrix, i8** %iqmatrix.addr, align 4, !tbaa !2
  %0 = bitcast i32* %dqv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i16*, i16** %dequant.addr, align 4, !tbaa !2
  %2 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %2, 0
  %lnot = xor i1 %tobool, true
  %lnot1 = xor i1 %lnot, true
  %lnot.ext = zext i1 %lnot1 to i32
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 %lnot.ext
  %3 = load i16, i16* %arrayidx, align 2, !tbaa !20
  %conv = sext i16 %3 to i32
  store i32 %conv, i32* %dqv, align 4, !tbaa !6
  %4 = load i8*, i8** %iqmatrix.addr, align 4, !tbaa !2
  %cmp = icmp ne i8* %4, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i8*, i8** %iqmatrix.addr, align 4, !tbaa !2
  %6 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  %conv4 = zext i8 %7 to i32
  %8 = load i32, i32* %dqv, align 4, !tbaa !6
  %mul = mul nsw i32 %conv4, %8
  %add = add nsw i32 %mul, 16
  %shr = ashr i32 %add, 5
  store i32 %shr, i32* %dqv, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load i32, i32* %dqv, align 4, !tbaa !6
  %10 = bitcast i32* %dqv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  ret i32 %9
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal void @set_dc_sign(i32* %cul_level, i32 %dc_val) #2 {
entry:
  %cul_level.addr = alloca i32*, align 4
  %dc_val.addr = alloca i32, align 4
  store i32* %cul_level, i32** %cul_level.addr, align 4, !tbaa !2
  store i32 %dc_val, i32* %dc_val.addr, align 4, !tbaa !6
  %0 = load i32, i32* %dc_val.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32*, i32** %cul_level.addr, align 4, !tbaa !2
  %2 = load i32, i32* %1, align 4, !tbaa !6
  %or = or i32 %2, 8
  store i32 %or, i32* %1, align 4, !tbaa !6
  br label %if.end3

if.else:                                          ; preds = %entry
  %3 = load i32, i32* %dc_val.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, 0
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.else
  %4 = load i32*, i32** %cul_level.addr, align 4, !tbaa !2
  %5 = load i32, i32* %4, align 4, !tbaa !6
  %add = add nsw i32 %5, 16
  store i32 %add, i32* %4, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.else
  br label %if.end3

if.end3:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_read_coeffs_txb_facade(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r, i32 %plane, i32 %row, i32 %col, i8 zeroext %tx_size) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %plane.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %bsize = alloca i8, align 1
  %plane_bsize = alloca i8, align 1
  %txb_ctx = alloca %struct.txb_ctx, align 4
  %cul_level = alloca i8, align 1
  %plane_type = alloca i8, align 1
  %tx_type = alloca i8, align 1
  %txw = alloca i32, align 4
  %txh = alloca i32, align 4
  %tx_unit = alloca i32, align 4
  %stride = alloca i32, align 4
  %idy = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %idx = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  store i32 %col, i32* %col.addr, align 4, !tbaa !6
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %4 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 4
  %6 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane1, i32 0, i32 %6
  store %struct.macroblockd_plane* %arrayidx2, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 6
  %8 = load i8, i8* %sb_type, align 2, !tbaa !59
  store i8 %8, i8* %bsize, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %plane_bsize) #6
  %9 = load i8, i8* %bsize, align 1, !tbaa !8
  %10 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %10, i32 0, i32 4
  %11 = load i32, i32* %subsampling_x, align 4, !tbaa !57
  %12 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %12, i32 0, i32 5
  %13 = load i32, i32* %subsampling_y, align 4, !tbaa !56
  %call = call zeroext i8 @get_plane_block_size(i8 zeroext %9, i32 %11, i32 %13)
  store i8 %call, i8* %plane_bsize, align 1, !tbaa !8
  %14 = bitcast %struct.txb_ctx* %txb_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %14) #6
  %15 = load i8, i8* %plane_bsize, align 1, !tbaa !8
  %16 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %17 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %18 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %above_entropy_context = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %18, i32 0, i32 8
  %19 = load i8*, i8** %above_entropy_context, align 4, !tbaa !65
  %20 = load i32, i32* %col.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %19, i32 %20
  %21 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %left_entropy_context = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %21, i32 0, i32 9
  %22 = load i8*, i8** %left_entropy_context, align 4, !tbaa !66
  %23 = load i32, i32* %row.addr, align 4, !tbaa !6
  %add.ptr3 = getelementptr inbounds i8, i8* %22, i32 %23
  call void @get_txb_ctx(i8 zeroext %15, i8 zeroext %16, i32 %17, i8* %add.ptr, i8* %add.ptr3, %struct.txb_ctx* %txb_ctx)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %cul_level) #6
  %24 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %25 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %26 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %27 = load i32, i32* %row.addr, align 4, !tbaa !6
  %28 = load i32, i32* %col.addr, align 4, !tbaa !6
  %29 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %30 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %call4 = call zeroext i8 @av1_read_coeffs_txb(%struct.AV1Common* %24, %struct.macroblockd* %25, %struct.aom_reader* %26, i32 %27, i32 %28, i32 %29, %struct.txb_ctx* %txb_ctx, i8 zeroext %30)
  store i8 %call4, i8* %cul_level, align 1, !tbaa !8
  %31 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %32 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %33 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %34 = load i8, i8* %plane_bsize, align 1, !tbaa !8
  %35 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %36 = load i8, i8* %cul_level, align 1, !tbaa !8
  %conv = zext i8 %36 to i32
  %37 = load i32, i32* %col.addr, align 4, !tbaa !6
  %38 = load i32, i32* %row.addr, align 4, !tbaa !6
  call void @av1_set_entropy_contexts(%struct.macroblockd* %31, %struct.macroblockd_plane* %32, i32 %33, i8 zeroext %34, i8 zeroext %35, i32 %conv, i32 %37, i32 %38)
  %39 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %call5 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %39)
  %tobool = icmp ne i32 %call5, 0
  br i1 %tobool, label %if.then, label %if.end35

if.then:                                          ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %plane_type) #6
  %40 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %call6 = call zeroext i8 @get_plane_type(i32 %40)
  store i8 %call6, i8* %plane_type, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type) #6
  %41 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %42 = load i8, i8* %plane_type, align 1, !tbaa !8
  %43 = load i32, i32* %row.addr, align 4, !tbaa !6
  %44 = load i32, i32* %col.addr, align 4, !tbaa !6
  %45 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %46 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %46, i32 0, i32 21
  %reduced_tx_set_used = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 9
  %47 = load i8, i8* %reduced_tx_set_used, align 1, !tbaa !27, !range !49
  %tobool7 = trunc i8 %47 to i1
  %conv8 = zext i1 %tobool7 to i32
  %call9 = call zeroext i8 @av1_get_tx_type(%struct.macroblockd* %41, i8 zeroext %42, i32 %43, i32 %44, i8 zeroext %45, i32 %conv8)
  store i8 %call9, i8* %tx_type, align 1, !tbaa !8
  %48 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %48, 0
  br i1 %cmp, label %if.then11, label %if.end34

if.then11:                                        ; preds = %if.then
  %49 = bitcast i32* %txw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #6
  %50 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %50 to i32
  %arrayidx12 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_unit, i32 0, i32 %idxprom
  %51 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  store i32 %51, i32* %txw, align 4, !tbaa !6
  %52 = bitcast i32* %txh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom13 = zext i8 %53 to i32
  %arrayidx14 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_unit, i32 0, i32 %idxprom13
  %54 = load i32, i32* %arrayidx14, align 4, !tbaa !6
  store i32 %54, i32* %txh, align 4, !tbaa !6
  %55 = load i32, i32* %txw, align 4, !tbaa !6
  %56 = load i32, i32* getelementptr inbounds ([19 x i32], [19 x i32]* @tx_size_wide_unit, i32 0, i32 4), align 16, !tbaa !6
  %cmp15 = icmp eq i32 %55, %56
  br i1 %cmp15, label %if.then19, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then11
  %57 = load i32, i32* %txh, align 4, !tbaa !6
  %58 = load i32, i32* getelementptr inbounds ([19 x i32], [19 x i32]* @tx_size_high_unit, i32 0, i32 4), align 16, !tbaa !6
  %cmp17 = icmp eq i32 %57, %58
  br i1 %cmp17, label %if.then19, label %if.end

if.then19:                                        ; preds = %lor.lhs.false, %if.then11
  %59 = bitcast i32* %tx_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #6
  %60 = load i32, i32* getelementptr inbounds ([19 x i32], [19 x i32]* @tx_size_wide_unit, i32 0, i32 2), align 8, !tbaa !6
  store i32 %60, i32* %tx_unit, align 4, !tbaa !6
  %61 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  %62 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tx_type_map_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %62, i32 0, i32 16
  %63 = load i32, i32* %tx_type_map_stride, align 16, !tbaa !26
  store i32 %63, i32* %stride, align 4, !tbaa !6
  %64 = bitcast i32* %idy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #6
  store i32 0, i32* %idy, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc31, %if.then19
  %65 = load i32, i32* %idy, align 4, !tbaa !6
  %66 = load i32, i32* %txh, align 4, !tbaa !6
  %cmp20 = icmp slt i32 %65, %66
  br i1 %cmp20, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %67 = bitcast i32* %idy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  br label %for.end33

for.body:                                         ; preds = %for.cond
  %68 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #6
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %for.body
  %69 = load i32, i32* %idx, align 4, !tbaa !6
  %70 = load i32, i32* %txw, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %69, %70
  br i1 %cmp23, label %for.body26, label %for.cond.cleanup25

for.cond.cleanup25:                               ; preds = %for.cond22
  store i32 5, i32* %cleanup.dest.slot, align 4
  %71 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  br label %for.end

for.body26:                                       ; preds = %for.cond22
  %72 = load i8, i8* %tx_type, align 1, !tbaa !8
  %73 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tx_type_map = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %73, i32 0, i32 15
  %74 = load i8*, i8** %tx_type_map, align 4, !tbaa !25
  %75 = load i32, i32* %row.addr, align 4, !tbaa !6
  %76 = load i32, i32* %idy, align 4, !tbaa !6
  %add = add nsw i32 %75, %76
  %77 = load i32, i32* %stride, align 4, !tbaa !6
  %mul = mul nsw i32 %add, %77
  %78 = load i32, i32* %col.addr, align 4, !tbaa !6
  %add27 = add nsw i32 %mul, %78
  %79 = load i32, i32* %idx, align 4, !tbaa !6
  %add28 = add nsw i32 %add27, %79
  %arrayidx29 = getelementptr inbounds i8, i8* %74, i32 %add28
  store i8 %72, i8* %arrayidx29, align 1, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body26
  %80 = load i32, i32* %tx_unit, align 4, !tbaa !6
  %81 = load i32, i32* %idx, align 4, !tbaa !6
  %add30 = add nsw i32 %81, %80
  store i32 %add30, i32* %idx, align 4, !tbaa !6
  br label %for.cond22

for.end:                                          ; preds = %for.cond.cleanup25
  br label %for.inc31

for.inc31:                                        ; preds = %for.end
  %82 = load i32, i32* %tx_unit, align 4, !tbaa !6
  %83 = load i32, i32* %idy, align 4, !tbaa !6
  %add32 = add nsw i32 %83, %82
  store i32 %add32, i32* %idy, align 4, !tbaa !6
  br label %for.cond

for.end33:                                        ; preds = %for.cond.cleanup
  %84 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #6
  %85 = bitcast i32* %tx_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  br label %if.end

if.end:                                           ; preds = %for.end33, %lor.lhs.false
  %86 = bitcast i32* %txh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #6
  %87 = bitcast i32* %txw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #6
  br label %if.end34

if.end34:                                         ; preds = %if.end, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %plane_type) #6
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %entry
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %cul_level) #6
  %88 = bitcast %struct.txb_ctx* %txb_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %88) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %plane_bsize) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  %89 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #6
  %90 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_plane_block_size(i8 zeroext %bsize, i32 %subsampling_x, i32 %subsampling_y) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x [2 x [2 x i8]]], [22 x [2 x [2 x i8]]]* @ss_size_lookup, i32 0, i32 %idxprom
  %1 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds [2 x [2 x i8]], [2 x [2 x i8]]* %arrayidx, i32 0, i32 %1
  %2 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx1, i32 0, i32 %2
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  ret i8 %3
}

; Function Attrs: inlinehint nounwind
define internal void @get_txb_ctx(i8 zeroext %plane_bsize, i8 zeroext %tx_size, i32 %plane, i8* %a, i8* %l, %struct.txb_ctx* %txb_ctx) #2 {
entry:
  %plane_bsize.addr = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %a.addr = alloca i8*, align 4
  %l.addr = alloca i8*, align 4
  %txb_ctx.addr = alloca %struct.txb_ctx*, align 4
  %txb_w_unit = alloca i32, align 4
  %txb_h_unit = alloca i32, align 4
  %dc_sign = alloca i32, align 4
  %k = alloca i32, align 4
  %sign = alloca i32, align 4
  %sign8 = alloca i32, align 4
  %top = alloca i32, align 4
  %left = alloca i32, align 4
  %ctx_base = alloca i32, align 4
  %ctx_offset = alloca i32, align 4
  store i8 %plane_bsize, i8* %plane_bsize.addr, align 1, !tbaa !8
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i8* %a, i8** %a.addr, align 4, !tbaa !2
  store i8* %l, i8** %l.addr, align 4, !tbaa !2
  store %struct.txb_ctx* %txb_ctx, %struct.txb_ctx** %txb_ctx.addr, align 4, !tbaa !2
  %0 = bitcast i32* %txb_w_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_unit, i32 0, i32 %idxprom
  %2 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %2, i32* %txb_w_unit, align 4, !tbaa !6
  %3 = bitcast i32* %txb_h_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_unit, i32 0, i32 %idxprom1
  %5 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  store i32 %5, i32* %txb_h_unit, align 4, !tbaa !6
  %6 = bitcast i32* %dc_sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 0, i32* %dc_sign, align 4, !tbaa !6
  %7 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %8 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %10 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  %conv = zext i8 %11 to i32
  %shr = ashr i32 %conv, 3
  store i32 %shr, i32* %sign, align 4, !tbaa !6
  %12 = load i32, i32* %sign, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [3 x i8], [3 x i8]* @get_txb_ctx.signs, i32 0, i32 %12
  %13 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = sext i8 %13 to i32
  %14 = load i32, i32* %dc_sign, align 4, !tbaa !6
  %add = add nsw i32 %14, %conv5
  store i32 %add, i32* %dc_sign, align 4, !tbaa !6
  %15 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %16 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  %17 = load i32, i32* %txb_w_unit, align 4, !tbaa !6
  %cmp = icmp slt i32 %inc, %17
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %do.body7

do.body7:                                         ; preds = %do.cond15, %do.end
  %18 = bitcast i32* %sign8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %20 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds i8, i8* %19, i32 %20
  %21 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  %conv10 = zext i8 %21 to i32
  %shr11 = ashr i32 %conv10, 3
  store i32 %shr11, i32* %sign8, align 4, !tbaa !6
  %22 = load i32, i32* %sign8, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [3 x i8], [3 x i8]* @get_txb_ctx.signs, i32 0, i32 %22
  %23 = load i8, i8* %arrayidx12, align 1, !tbaa !8
  %conv13 = sext i8 %23 to i32
  %24 = load i32, i32* %dc_sign, align 4, !tbaa !6
  %add14 = add nsw i32 %24, %conv13
  store i32 %add14, i32* %dc_sign, align 4, !tbaa !6
  %25 = bitcast i32* %sign8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #6
  br label %do.cond15

do.cond15:                                        ; preds = %do.body7
  %26 = load i32, i32* %k, align 4, !tbaa !6
  %inc16 = add nsw i32 %26, 1
  store i32 %inc16, i32* %k, align 4, !tbaa !6
  %27 = load i32, i32* %txb_h_unit, align 4, !tbaa !6
  %cmp17 = icmp slt i32 %inc16, %27
  br i1 %cmp17, label %do.body7, label %do.end19

do.end19:                                         ; preds = %do.cond15
  %28 = load i32, i32* %dc_sign, align 4, !tbaa !6
  %add20 = add nsw i32 %28, 32
  %arrayidx21 = getelementptr inbounds [65 x i8], [65 x i8]* @get_txb_ctx.dc_sign_contexts, i32 0, i32 %add20
  %29 = load i8, i8* %arrayidx21, align 1, !tbaa !8
  %conv22 = sext i8 %29 to i32
  %30 = load %struct.txb_ctx*, %struct.txb_ctx** %txb_ctx.addr, align 4, !tbaa !2
  %dc_sign_ctx = getelementptr inbounds %struct.txb_ctx, %struct.txb_ctx* %30, i32 0, i32 1
  store i32 %conv22, i32* %dc_sign_ctx, align 4, !tbaa !52
  %31 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp23 = icmp eq i32 %31, 0
  br i1 %cmp23, label %if.then, label %if.else62

if.then:                                          ; preds = %do.end19
  %32 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !8
  %conv25 = zext i8 %32 to i32
  %33 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom26 = zext i8 %33 to i32
  %arrayidx27 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_to_bsize, i32 0, i32 %idxprom26
  %34 = load i8, i8* %arrayidx27, align 1, !tbaa !8
  %conv28 = zext i8 %34 to i32
  %cmp29 = icmp eq i32 %conv25, %conv28
  br i1 %cmp29, label %if.then31, label %if.else

if.then31:                                        ; preds = %if.then
  %35 = load %struct.txb_ctx*, %struct.txb_ctx** %txb_ctx.addr, align 4, !tbaa !2
  %txb_skip_ctx = getelementptr inbounds %struct.txb_ctx, %struct.txb_ctx* %35, i32 0, i32 0
  store i32 0, i32* %txb_skip_ctx, align 4, !tbaa !22
  br label %if.end

if.else:                                          ; preds = %if.then
  %36 = bitcast i32* %top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  store i32 0, i32* %top, align 4, !tbaa !6
  %37 = bitcast i32* %left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #6
  store i32 0, i32* %left, align 4, !tbaa !6
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %do.body32

do.body32:                                        ; preds = %do.cond35, %if.else
  %38 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %39 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds i8, i8* %38, i32 %39
  %40 = load i8, i8* %arrayidx33, align 1, !tbaa !8
  %conv34 = sext i8 %40 to i32
  %41 = load i32, i32* %top, align 4, !tbaa !6
  %or = or i32 %41, %conv34
  store i32 %or, i32* %top, align 4, !tbaa !6
  br label %do.cond35

do.cond35:                                        ; preds = %do.body32
  %42 = load i32, i32* %k, align 4, !tbaa !6
  %inc36 = add nsw i32 %42, 1
  store i32 %inc36, i32* %k, align 4, !tbaa !6
  %43 = load i32, i32* %txb_w_unit, align 4, !tbaa !6
  %cmp37 = icmp slt i32 %inc36, %43
  br i1 %cmp37, label %do.body32, label %do.end39

do.end39:                                         ; preds = %do.cond35
  %44 = load i32, i32* %top, align 4, !tbaa !6
  %and = and i32 %44, 7
  store i32 %and, i32* %top, align 4, !tbaa !6
  %45 = load i32, i32* %top, align 4, !tbaa !6
  %cmp40 = icmp slt i32 %45, 4
  br i1 %cmp40, label %cond.true, label %cond.false

cond.true:                                        ; preds = %do.end39
  %46 = load i32, i32* %top, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %do.end39
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %46, %cond.true ], [ 4, %cond.false ]
  store i32 %cond, i32* %top, align 4, !tbaa !6
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %do.body42

do.body42:                                        ; preds = %do.cond46, %cond.end
  %47 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %48 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds i8, i8* %47, i32 %48
  %49 = load i8, i8* %arrayidx43, align 1, !tbaa !8
  %conv44 = sext i8 %49 to i32
  %50 = load i32, i32* %left, align 4, !tbaa !6
  %or45 = or i32 %50, %conv44
  store i32 %or45, i32* %left, align 4, !tbaa !6
  br label %do.cond46

do.cond46:                                        ; preds = %do.body42
  %51 = load i32, i32* %k, align 4, !tbaa !6
  %inc47 = add nsw i32 %51, 1
  store i32 %inc47, i32* %k, align 4, !tbaa !6
  %52 = load i32, i32* %txb_h_unit, align 4, !tbaa !6
  %cmp48 = icmp slt i32 %inc47, %52
  br i1 %cmp48, label %do.body42, label %do.end50

do.end50:                                         ; preds = %do.cond46
  %53 = load i32, i32* %left, align 4, !tbaa !6
  %and51 = and i32 %53, 7
  store i32 %and51, i32* %left, align 4, !tbaa !6
  %54 = load i32, i32* %left, align 4, !tbaa !6
  %cmp52 = icmp slt i32 %54, 4
  br i1 %cmp52, label %cond.true54, label %cond.false55

cond.true54:                                      ; preds = %do.end50
  %55 = load i32, i32* %left, align 4, !tbaa !6
  br label %cond.end56

cond.false55:                                     ; preds = %do.end50
  br label %cond.end56

cond.end56:                                       ; preds = %cond.false55, %cond.true54
  %cond57 = phi i32 [ %55, %cond.true54 ], [ 4, %cond.false55 ]
  store i32 %cond57, i32* %left, align 4, !tbaa !6
  %56 = load i32, i32* %top, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds [5 x [5 x i8]], [5 x [5 x i8]]* @get_txb_ctx.skip_contexts, i32 0, i32 %56
  %57 = load i32, i32* %left, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds [5 x i8], [5 x i8]* %arrayidx58, i32 0, i32 %57
  %58 = load i8, i8* %arrayidx59, align 1, !tbaa !8
  %conv60 = zext i8 %58 to i32
  %59 = load %struct.txb_ctx*, %struct.txb_ctx** %txb_ctx.addr, align 4, !tbaa !2
  %txb_skip_ctx61 = getelementptr inbounds %struct.txb_ctx, %struct.txb_ctx* %59, i32 0, i32 0
  store i32 %conv60, i32* %txb_skip_ctx61, align 4, !tbaa !22
  %60 = bitcast i32* %left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  br label %if.end

if.end:                                           ; preds = %cond.end56, %if.then31
  br label %if.end76

if.else62:                                        ; preds = %do.end19
  %62 = bitcast i32* %ctx_base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #6
  %63 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %64 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %65 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %call = call i32 @get_entropy_context(i8 zeroext %63, i8* %64, i8* %65)
  store i32 %call, i32* %ctx_base, align 4, !tbaa !6
  %66 = bitcast i32* %ctx_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  %67 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !8
  %idxprom63 = zext i8 %67 to i32
  %arrayidx64 = getelementptr inbounds [22 x i8], [22 x i8]* @num_pels_log2_lookup, i32 0, i32 %idxprom63
  %68 = load i8, i8* %arrayidx64, align 1, !tbaa !8
  %conv65 = zext i8 %68 to i32
  %69 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom66 = zext i8 %69 to i32
  %arrayidx67 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_to_bsize, i32 0, i32 %idxprom66
  %70 = load i8, i8* %arrayidx67, align 1, !tbaa !8
  %idxprom68 = zext i8 %70 to i32
  %arrayidx69 = getelementptr inbounds [22 x i8], [22 x i8]* @num_pels_log2_lookup, i32 0, i32 %idxprom68
  %71 = load i8, i8* %arrayidx69, align 1, !tbaa !8
  %conv70 = zext i8 %71 to i32
  %cmp71 = icmp sgt i32 %conv65, %conv70
  %72 = zext i1 %cmp71 to i64
  %cond73 = select i1 %cmp71, i32 10, i32 7
  store i32 %cond73, i32* %ctx_offset, align 4, !tbaa !6
  %73 = load i32, i32* %ctx_base, align 4, !tbaa !6
  %74 = load i32, i32* %ctx_offset, align 4, !tbaa !6
  %add74 = add nsw i32 %73, %74
  %75 = load %struct.txb_ctx*, %struct.txb_ctx** %txb_ctx.addr, align 4, !tbaa !2
  %txb_skip_ctx75 = getelementptr inbounds %struct.txb_ctx, %struct.txb_ctx* %75, i32 0, i32 0
  store i32 %add74, i32* %txb_skip_ctx75, align 4, !tbaa !22
  %76 = bitcast i32* %ctx_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  %77 = bitcast i32* %ctx_base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #6
  br label %if.end76

if.end76:                                         ; preds = %if.else62, %if.end
  %78 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #6
  %79 = bitcast i32* %dc_sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #6
  %80 = bitcast i32* %txb_h_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #6
  %81 = bitcast i32* %txb_w_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #6
  ret void
}

declare void @av1_set_entropy_contexts(%struct.macroblockd*, %struct.macroblockd_plane*, i32, i8 zeroext, i8 zeroext, i32, i32, i32) #3

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_block(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !8
  %conv = sext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_get_adjusted_tx_size(i8 zeroext %tx_size) #2 {
entry:
  %retval = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 4, label %sw.bb
    i32 12, label %sw.bb
    i32 11, label %sw.bb
    i32 18, label %sw.bb1
    i32 17, label %sw.bb2
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry
  store i8 3, i8* %retval, align 1
  br label %return

sw.bb1:                                           ; preds = %entry
  store i8 10, i8* %retval, align 1
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8 9, i8* %retval, align 1
  br label %return

sw.default:                                       ; preds = %entry
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  store i8 %1, i8* %retval, align 1
  br label %return

return:                                           ; preds = %sw.default, %sw.bb2, %sw.bb1, %sw.bb
  %2 = load i8, i8* %retval, align 1
  ret i8 %2
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_cdf_(%struct.aom_reader* %r, i16* %cdf, i32 %nsymbs, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %cdf.addr = alloca i16*, align 4
  %nsymbs.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %symb = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !2
  store i32 %nsymbs, i32* %nsymbs.addr, align 4, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %symb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %1, i32 0, i32 2
  %2 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %3 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %call = call i32 @od_ec_decode_cdf_q15(%struct.od_ec_dec* %ec, i16* %2, i32 %3)
  store i32 %call, i32* %symb, align 4, !tbaa !6
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %4, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %5, i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %8 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %8, 2
  %conv = zext i1 %cmp to i32
  call void @aom_update_symb_counts(%struct.aom_reader* %7, i32 %conv)
  %9 = load i32, i32* %symb, align 4, !tbaa !6
  %10 = bitcast i32* %symb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  ret i32 %9
}

; Function Attrs: inlinehint nounwind
define internal void @update_cdf(i16* %cdf, i8 signext %val, i32 %nsymbs) #2 {
entry:
  %cdf.addr = alloca i16*, align 4
  %val.addr = alloca i8, align 1
  %nsymbs.addr = alloca i32, align 4
  %rate = alloca i32, align 4
  %i = alloca i32, align 4
  %tmp = alloca i32, align 4
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !2
  store i8 %val, i8* %val.addr, align 1, !tbaa !8
  store i32 %nsymbs, i32* %nsymbs.addr, align 4, !tbaa !6
  %0 = bitcast i32* %rate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 %4
  %5 = load i16, i16* %arrayidx, align 2, !tbaa !20
  %conv = zext i16 %5 to i32
  %cmp = icmp sgt i32 %conv, 15
  %conv1 = zext i1 %cmp to i32
  %add = add nsw i32 3, %conv1
  %6 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %7 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx2, align 2, !tbaa !20
  %conv3 = zext i16 %8 to i32
  %cmp4 = icmp sgt i32 %conv3, 31
  %conv5 = zext i1 %cmp4 to i32
  %add6 = add nsw i32 %add, %conv5
  %9 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [17 x i32], [17 x i32]* @update_cdf.nsymbs2speed, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx7, align 4, !tbaa !6
  %add8 = add nsw i32 %add6, %10
  store i32 %add8, i32* %rate, align 4, !tbaa !6
  store i32 32768, i32* %tmp, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %12, 1
  %cmp9 = icmp slt i32 %11, %sub
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %14 = load i8, i8* %val.addr, align 1, !tbaa !8
  %conv11 = sext i8 %14 to i32
  %cmp12 = icmp eq i32 %13, %conv11
  br i1 %cmp12, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %15 = load i32, i32* %tmp, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %15, %cond.false ]
  store i32 %cond, i32* %tmp, align 4, !tbaa !6
  %16 = load i32, i32* %tmp, align 4, !tbaa !6
  %17 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds i16, i16* %17, i32 %18
  %19 = load i16, i16* %arrayidx14, align 2, !tbaa !20
  %conv15 = zext i16 %19 to i32
  %cmp16 = icmp slt i32 %16, %conv15
  br i1 %cmp16, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %20 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds i16, i16* %20, i32 %21
  %22 = load i16, i16* %arrayidx18, align 2, !tbaa !20
  %conv19 = zext i16 %22 to i32
  %23 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub20 = sub nsw i32 %conv19, %23
  %24 = load i32, i32* %rate, align 4, !tbaa !6
  %shr = ashr i32 %sub20, %24
  %25 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds i16, i16* %25, i32 %26
  %27 = load i16, i16* %arrayidx21, align 2, !tbaa !20
  %conv22 = zext i16 %27 to i32
  %sub23 = sub nsw i32 %conv22, %shr
  %conv24 = trunc i32 %sub23 to i16
  store i16 %conv24, i16* %arrayidx21, align 2, !tbaa !20
  br label %if.end

if.else:                                          ; preds = %cond.end
  %28 = load i32, i32* %tmp, align 4, !tbaa !6
  %29 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds i16, i16* %29, i32 %30
  %31 = load i16, i16* %arrayidx25, align 2, !tbaa !20
  %conv26 = zext i16 %31 to i32
  %sub27 = sub nsw i32 %28, %conv26
  %32 = load i32, i32* %rate, align 4, !tbaa !6
  %shr28 = ashr i32 %sub27, %32
  %33 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds i16, i16* %33, i32 %34
  %35 = load i16, i16* %arrayidx29, align 2, !tbaa !20
  %conv30 = zext i16 %35 to i32
  %add31 = add nsw i32 %conv30, %shr28
  %conv32 = trunc i32 %add31 to i16
  store i16 %conv32, i16* %arrayidx29, align 2, !tbaa !20
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %38 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds i16, i16* %37, i32 %38
  %39 = load i16, i16* %arrayidx33, align 2, !tbaa !20
  %conv34 = zext i16 %39 to i32
  %cmp35 = icmp slt i32 %conv34, 32
  %conv36 = zext i1 %cmp35 to i32
  %40 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %41 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds i16, i16* %40, i32 %41
  %42 = load i16, i16* %arrayidx37, align 2, !tbaa !20
  %conv38 = zext i16 %42 to i32
  %add39 = add nsw i32 %conv38, %conv36
  %conv40 = trunc i32 %add39 to i16
  store i16 %conv40, i16* %arrayidx37, align 2, !tbaa !20
  %43 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  %44 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  %45 = bitcast i32* %rate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  ret void
}

declare i32 @od_ec_decode_cdf_q15(%struct.od_ec_dec*, i16*, i32) #3

; Function Attrs: inlinehint nounwind
define internal void @aom_process_accounting(%struct.aom_reader* %r, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %acct_str.addr = alloca i8*, align 4
  %tell_frac = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting, align 4, !tbaa !67
  %cmp = icmp ne %struct.Accounting* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %tell_frac to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_reader_tell_frac(%struct.aom_reader* %3)
  store i32 %call, i32* %tell_frac, align 4, !tbaa !6
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting1 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %4, i32 0, i32 3
  %5 = load %struct.Accounting*, %struct.Accounting** %accounting1, align 4, !tbaa !67
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %7 = load i32, i32* %tell_frac, align 4, !tbaa !6
  %8 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting2 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %8, i32 0, i32 3
  %9 = load %struct.Accounting*, %struct.Accounting** %accounting2, align 4, !tbaa !67
  %last_tell_frac = getelementptr inbounds %struct.Accounting, %struct.Accounting* %9, i32 0, i32 4
  %10 = load i32, i32* %last_tell_frac, align 4, !tbaa !68
  %sub = sub i32 %7, %10
  call void @aom_accounting_record(%struct.Accounting* %5, i8* %6, i32 %sub)
  %11 = load i32, i32* %tell_frac, align 4, !tbaa !6
  %12 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting3 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %12, i32 0, i32 3
  %13 = load %struct.Accounting*, %struct.Accounting** %accounting3, align 4, !tbaa !67
  %last_tell_frac4 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %13, i32 0, i32 4
  store i32 %11, i32* %last_tell_frac4, align 4, !tbaa !68
  %14 = bitcast i32* %tell_frac to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @aom_update_symb_counts(%struct.aom_reader* %r, i32 %is_binary) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %is_binary.addr = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %is_binary, i32* %is_binary.addr, align 4, !tbaa !6
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting, align 4, !tbaa !67
  %cmp = icmp ne %struct.Accounting* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %is_binary.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %2, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting1 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %3, i32 0, i32 3
  %4 = load %struct.Accounting*, %struct.Accounting** %accounting1, align 4, !tbaa !67
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %4, i32 0, i32 0
  %num_multi_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 2
  %5 = load i32, i32* %num_multi_syms, align 4, !tbaa !73
  %add = add nsw i32 %5, %lnot.ext
  store i32 %add, i32* %num_multi_syms, align 4, !tbaa !73
  %6 = load i32, i32* %is_binary.addr, align 4, !tbaa !6
  %tobool2 = icmp ne i32 %6, 0
  %lnot3 = xor i1 %tobool2, true
  %lnot5 = xor i1 %lnot3, true
  %lnot.ext6 = zext i1 %lnot5 to i32
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting7 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %7, i32 0, i32 3
  %8 = load %struct.Accounting*, %struct.Accounting** %accounting7, align 4, !tbaa !67
  %syms8 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %8, i32 0, i32 0
  %num_binary_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms8, i32 0, i32 3
  %9 = load i32, i32* %num_binary_syms, align 4, !tbaa !74
  %add9 = add nsw i32 %9, %lnot.ext6
  store i32 %add9, i32* %num_binary_syms, align 4, !tbaa !74
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare i32 @aom_reader_tell_frac(%struct.aom_reader*) #3

declare void @aom_accounting_record(%struct.Accounting*, i8*, i32) #3

; Function Attrs: nounwind
define internal zeroext i8 @intra_mode_to_tx_type(%struct.MB_MODE_INFO* %mbmi, i8 zeroext %plane_type) #0 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %plane_type.addr = alloca i8, align 1
  %mode = alloca i8, align 1
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  store i8 %plane_type, i8* %plane_type.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mode) #6
  %0 = load i8, i8* %plane_type.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %mode2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 7
  %2 = load i8, i8* %mode2, align 1, !tbaa !75
  %conv3 = zext i8 %2 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %uv_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %3, i32 0, i32 8
  %4 = load i8, i8* %uv_mode, align 4, !tbaa !76
  %call = call zeroext i8 @get_uv_mode(i8 zeroext %4)
  %conv4 = zext i8 %call to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv3, %cond.true ], [ %conv4, %cond.false ]
  %conv5 = trunc i32 %cond to i8
  store i8 %conv5, i8* %mode, align 1, !tbaa !8
  %5 = load i8, i8* %mode, align 1, !tbaa !8
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [13 x i8], [13 x i8]* @intra_mode_to_tx_type._intra_mode_to_tx_type, i32 0, i32 %idxprom
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mode) #6
  ret i8 %6
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_get_ext_tx_set_type(i8 zeroext %tx_size, i32 %is_inter, i32 %use_reduced_set) #2 {
entry:
  %retval = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %is_inter.addr = alloca i32, align 4
  %use_reduced_set.addr = alloca i32, align 4
  %tx_size_sqr_up = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %tx_size_sqr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i32 %is_inter, i32* %is_inter.addr, align 4, !tbaa !6
  store i32 %use_reduced_set, i32* %use_reduced_set.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size_sqr_up) #6
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_up_map, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %1, i8* %tx_size_sqr_up, align 1, !tbaa !8
  %2 = load i8, i8* %tx_size_sqr_up, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8 0, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load i8, i8* %tx_size_sqr_up, align 1, !tbaa !8
  %conv2 = zext i8 %3 to i32
  %cmp3 = icmp eq i32 %conv2, 3
  br i1 %cmp3, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %4 = load i32, i32* %is_inter.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %4, 0
  %5 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  %conv6 = trunc i32 %cond to i8
  store i8 %conv6, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  %6 = load i32, i32* %use_reduced_set.addr, align 4, !tbaa !6
  %tobool8 = icmp ne i32 %6, 0
  br i1 %tobool8, label %if.then9, label %if.end13

if.then9:                                         ; preds = %if.end7
  %7 = load i32, i32* %is_inter.addr, align 4, !tbaa !6
  %tobool10 = icmp ne i32 %7, 0
  %8 = zext i1 %tobool10 to i64
  %cond11 = select i1 %tobool10, i32 1, i32 2
  %conv12 = trunc i32 %cond11 to i8
  store i8 %conv12, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.end7
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size_sqr) #6
  %9 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom14 = zext i8 %9 to i32
  %arrayidx15 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_map, i32 0, i32 %idxprom14
  %10 = load i8, i8* %arrayidx15, align 1, !tbaa !8
  store i8 %10, i8* %tx_size_sqr, align 1, !tbaa !8
  %11 = load i32, i32* %is_inter.addr, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [2 x [2 x i8]], [2 x [2 x i8]]* @av1_ext_tx_set_lookup, i32 0, i32 %11
  %12 = load i8, i8* %tx_size_sqr, align 1, !tbaa !8
  %conv17 = zext i8 %12 to i32
  %cmp18 = icmp eq i32 %conv17, 2
  %conv19 = zext i1 %cmp18 to i32
  %arrayidx20 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx16, i32 0, i32 %conv19
  %13 = load i8, i8* %arrayidx20, align 1, !tbaa !8
  store i8 %13, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size_sqr) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end13, %if.then9, %if.then5, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size_sqr_up) #6
  %14 = load i8, i8* %retval, align 1
  ret i8 %14
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_uv_mode(i8 zeroext %mode) #2 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [16 x i8], [16 x i8]* @get_uv_mode.uv2y, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  ret i8 %1
}

; Function Attrs: inlinehint nounwind
define internal %struct.SCAN_ORDER* @get_default_scan(i8 zeroext %tx_size, i8 zeroext %tx_type) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  %tx_type.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !8
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x [16 x %struct.SCAN_ORDER]], [19 x [16 x %struct.SCAN_ORDER]]* @av1_scan_orders, i32 0, i32 %idxprom
  %1 = load i8, i8* %tx_type.addr, align 1, !tbaa !8
  %idxprom1 = zext i8 %1 to i32
  %arrayidx2 = getelementptr inbounds [16 x %struct.SCAN_ORDER], [16 x %struct.SCAN_ORDER]* %arrayidx, i32 0, i32 %idxprom1
  ret %struct.SCAN_ORDER* %arrayidx2
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_(%struct.aom_reader* %r, i32 %prob, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %prob.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %p = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %prob, i32* %prob.addr, align 4, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %prob.addr, align 4, !tbaa !6
  %shl = shl i32 %1, 15
  %sub = sub nsw i32 8388607, %shl
  %2 = load i32, i32* %prob.addr, align 4, !tbaa !6
  %add = add nsw i32 %sub, %2
  %shr = ashr i32 %add, 8
  store i32 %shr, i32* %p, align 4, !tbaa !6
  %3 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %4, i32 0, i32 2
  %5 = load i32, i32* %p, align 4, !tbaa !6
  %call = call i32 @od_ec_decode_bool_q15(%struct.od_ec_dec* %ec, i32 %5)
  store i32 %call, i32* %bit, align 4, !tbaa !6
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %8 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %7, i8* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  call void @aom_update_symb_counts(%struct.aom_reader* %9, i32 1)
  %10 = load i32, i32* %bit, align 4, !tbaa !6
  %11 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  ret i32 %10
}

declare i32 @od_ec_decode_bool_q15(%struct.od_ec_dec*, i32) #3

; Function Attrs: inlinehint nounwind
define internal i32 @get_lower_levels_ctx_2d(i8* %levels, i32 %coeff_idx, i32 %bwl, i8 zeroext %tx_size) #2 {
entry:
  %levels.addr = alloca i8*, align 4
  %coeff_idx.addr = alloca i32, align 4
  %bwl.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %mag = alloca i32, align 4
  %ctx = alloca i32, align 4
  store i8* %levels, i8** %levels.addr, align 4, !tbaa !2
  store i32 %coeff_idx, i32* %coeff_idx.addr, align 4, !tbaa !6
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  %0 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %2 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %3 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %call = call i32 @get_padded_idx(i32 %2, i32 %3)
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %call
  store i8* %add.ptr, i8** %levels.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 1
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %5 to i32
  %cmp = icmp slt i32 %conv, 3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 1
  %7 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = zext i8 %7 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv3, %cond.true ], [ 3, %cond.false ]
  store i32 %cond, i32* %mag, align 4, !tbaa !6
  %8 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %9 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl = shl i32 1, %9
  %add = add nsw i32 %shl, 4
  %arrayidx4 = getelementptr inbounds i8, i8* %8, i32 %add
  %10 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = zext i8 %10 to i32
  %cmp6 = icmp slt i32 %conv5, 3
  br i1 %cmp6, label %cond.true8, label %cond.false13

cond.true8:                                       ; preds = %cond.end
  %11 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %12 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl9 = shl i32 1, %12
  %add10 = add nsw i32 %shl9, 4
  %arrayidx11 = getelementptr inbounds i8, i8* %11, i32 %add10
  %13 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  %conv12 = zext i8 %13 to i32
  br label %cond.end14

cond.false13:                                     ; preds = %cond.end
  br label %cond.end14

cond.end14:                                       ; preds = %cond.false13, %cond.true8
  %cond15 = phi i32 [ %conv12, %cond.true8 ], [ 3, %cond.false13 ]
  %14 = load i32, i32* %mag, align 4, !tbaa !6
  %add16 = add nsw i32 %14, %cond15
  store i32 %add16, i32* %mag, align 4, !tbaa !6
  %15 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %16 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl17 = shl i32 1, %16
  %add18 = add nsw i32 %shl17, 4
  %add19 = add nsw i32 %add18, 1
  %arrayidx20 = getelementptr inbounds i8, i8* %15, i32 %add19
  %17 = load i8, i8* %arrayidx20, align 1, !tbaa !8
  %conv21 = zext i8 %17 to i32
  %cmp22 = icmp slt i32 %conv21, 3
  br i1 %cmp22, label %cond.true24, label %cond.false30

cond.true24:                                      ; preds = %cond.end14
  %18 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %19 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl25 = shl i32 1, %19
  %add26 = add nsw i32 %shl25, 4
  %add27 = add nsw i32 %add26, 1
  %arrayidx28 = getelementptr inbounds i8, i8* %18, i32 %add27
  %20 = load i8, i8* %arrayidx28, align 1, !tbaa !8
  %conv29 = zext i8 %20 to i32
  br label %cond.end31

cond.false30:                                     ; preds = %cond.end14
  br label %cond.end31

cond.end31:                                       ; preds = %cond.false30, %cond.true24
  %cond32 = phi i32 [ %conv29, %cond.true24 ], [ 3, %cond.false30 ]
  %21 = load i32, i32* %mag, align 4, !tbaa !6
  %add33 = add nsw i32 %21, %cond32
  store i32 %add33, i32* %mag, align 4, !tbaa !6
  %22 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i8, i8* %22, i32 2
  %23 = load i8, i8* %arrayidx34, align 1, !tbaa !8
  %conv35 = zext i8 %23 to i32
  %cmp36 = icmp slt i32 %conv35, 3
  br i1 %cmp36, label %cond.true38, label %cond.false41

cond.true38:                                      ; preds = %cond.end31
  %24 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i8, i8* %24, i32 2
  %25 = load i8, i8* %arrayidx39, align 1, !tbaa !8
  %conv40 = zext i8 %25 to i32
  br label %cond.end42

cond.false41:                                     ; preds = %cond.end31
  br label %cond.end42

cond.end42:                                       ; preds = %cond.false41, %cond.true38
  %cond43 = phi i32 [ %conv40, %cond.true38 ], [ 3, %cond.false41 ]
  %26 = load i32, i32* %mag, align 4, !tbaa !6
  %add44 = add nsw i32 %26, %cond43
  store i32 %add44, i32* %mag, align 4, !tbaa !6
  %27 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %28 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl45 = shl i32 2, %28
  %add46 = add nsw i32 %shl45, 8
  %arrayidx47 = getelementptr inbounds i8, i8* %27, i32 %add46
  %29 = load i8, i8* %arrayidx47, align 1, !tbaa !8
  %conv48 = zext i8 %29 to i32
  %cmp49 = icmp slt i32 %conv48, 3
  br i1 %cmp49, label %cond.true51, label %cond.false56

cond.true51:                                      ; preds = %cond.end42
  %30 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %31 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl52 = shl i32 2, %31
  %add53 = add nsw i32 %shl52, 8
  %arrayidx54 = getelementptr inbounds i8, i8* %30, i32 %add53
  %32 = load i8, i8* %arrayidx54, align 1, !tbaa !8
  %conv55 = zext i8 %32 to i32
  br label %cond.end57

cond.false56:                                     ; preds = %cond.end42
  br label %cond.end57

cond.end57:                                       ; preds = %cond.false56, %cond.true51
  %cond58 = phi i32 [ %conv55, %cond.true51 ], [ 3, %cond.false56 ]
  %33 = load i32, i32* %mag, align 4, !tbaa !6
  %add59 = add nsw i32 %33, %cond58
  store i32 %add59, i32* %mag, align 4, !tbaa !6
  %34 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %35 = load i32, i32* %mag, align 4, !tbaa !6
  %add60 = add nsw i32 %35, 1
  %shr = ashr i32 %add60, 1
  %cmp61 = icmp slt i32 %shr, 4
  br i1 %cmp61, label %cond.true63, label %cond.false66

cond.true63:                                      ; preds = %cond.end57
  %36 = load i32, i32* %mag, align 4, !tbaa !6
  %add64 = add nsw i32 %36, 1
  %shr65 = ashr i32 %add64, 1
  br label %cond.end67

cond.false66:                                     ; preds = %cond.end57
  br label %cond.end67

cond.end67:                                       ; preds = %cond.false66, %cond.true63
  %cond68 = phi i32 [ %shr65, %cond.true63 ], [ 4, %cond.false66 ]
  store i32 %cond68, i32* %ctx, align 4, !tbaa !6
  %37 = load i32, i32* %ctx, align 4, !tbaa !6
  %38 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %38 to i32
  %arrayidx69 = getelementptr inbounds [19 x i8*], [19 x i8*]* @av1_nz_map_ctx_offset, i32 0, i32 %idxprom
  %39 = load i8*, i8** %arrayidx69, align 4, !tbaa !2
  %40 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %arrayidx70 = getelementptr inbounds i8, i8* %39, i32 %40
  %41 = load i8, i8* %arrayidx70, align 1, !tbaa !8
  %conv71 = sext i8 %41 to i32
  %add72 = add nsw i32 %37, %conv71
  %42 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  %43 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  ret i32 %add72
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_br_ctx_2d(i8* %levels, i32 %c, i32 %bwl) #2 {
entry:
  %retval = alloca i32, align 4
  %levels.addr = alloca i8*, align 4
  %c.addr = alloca i32, align 4
  %bwl.addr = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %stride = alloca i32, align 4
  %pos = alloca i32, align 4
  %mag = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %levels, i8** %levels.addr, align 4, !tbaa !2
  store i32 %c, i32* %c.addr, align 4, !tbaa !6
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %c.addr, align 4, !tbaa !6
  %2 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shr = ashr i32 %1, %2
  store i32 %shr, i32* %row, align 4, !tbaa !6
  %3 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %c.addr, align 4, !tbaa !6
  %5 = load i32, i32* %row, align 4, !tbaa !6
  %6 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl = shl i32 %5, %6
  %sub = sub nsw i32 %4, %shl
  store i32 %sub, i32* %col, align 4, !tbaa !6
  %7 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl1 = shl i32 1, %8
  %add = add nsw i32 %shl1, 4
  store i32 %add, i32* %stride, align 4, !tbaa !6
  %9 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i32, i32* %row, align 4, !tbaa !6
  %11 = load i32, i32* %stride, align 4, !tbaa !6
  %mul = mul nsw i32 %10, %11
  %12 = load i32, i32* %col, align 4, !tbaa !6
  %add2 = add nsw i32 %mul, %12
  store i32 %add2, i32* %pos, align 4, !tbaa !6
  %13 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %15 = load i32, i32* %pos, align 4, !tbaa !6
  %add3 = add nsw i32 %15, 1
  %arrayidx = getelementptr inbounds i8, i8* %14, i32 %add3
  %16 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %16 to i32
  %cmp = icmp slt i32 %conv, 15
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %17 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %18 = load i32, i32* %pos, align 4, !tbaa !6
  %add5 = add nsw i32 %18, 1
  %arrayidx6 = getelementptr inbounds i8, i8* %17, i32 %add5
  %19 = load i8, i8* %arrayidx6, align 1, !tbaa !8
  %conv7 = zext i8 %19 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv7, %cond.true ], [ 15, %cond.false ]
  %20 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %21 = load i32, i32* %pos, align 4, !tbaa !6
  %22 = load i32, i32* %stride, align 4, !tbaa !6
  %add8 = add nsw i32 %21, %22
  %arrayidx9 = getelementptr inbounds i8, i8* %20, i32 %add8
  %23 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  %conv10 = zext i8 %23 to i32
  %cmp11 = icmp slt i32 %conv10, 15
  br i1 %cmp11, label %cond.true13, label %cond.false17

cond.true13:                                      ; preds = %cond.end
  %24 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %25 = load i32, i32* %pos, align 4, !tbaa !6
  %26 = load i32, i32* %stride, align 4, !tbaa !6
  %add14 = add nsw i32 %25, %26
  %arrayidx15 = getelementptr inbounds i8, i8* %24, i32 %add14
  %27 = load i8, i8* %arrayidx15, align 1, !tbaa !8
  %conv16 = zext i8 %27 to i32
  br label %cond.end18

cond.false17:                                     ; preds = %cond.end
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false17, %cond.true13
  %cond19 = phi i32 [ %conv16, %cond.true13 ], [ 15, %cond.false17 ]
  %add20 = add nsw i32 %cond, %cond19
  %28 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %29 = load i32, i32* %pos, align 4, !tbaa !6
  %add21 = add nsw i32 %29, 1
  %30 = load i32, i32* %stride, align 4, !tbaa !6
  %add22 = add nsw i32 %add21, %30
  %arrayidx23 = getelementptr inbounds i8, i8* %28, i32 %add22
  %31 = load i8, i8* %arrayidx23, align 1, !tbaa !8
  %conv24 = zext i8 %31 to i32
  %cmp25 = icmp slt i32 %conv24, 15
  br i1 %cmp25, label %cond.true27, label %cond.false32

cond.true27:                                      ; preds = %cond.end18
  %32 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %33 = load i32, i32* %pos, align 4, !tbaa !6
  %add28 = add nsw i32 %33, 1
  %34 = load i32, i32* %stride, align 4, !tbaa !6
  %add29 = add nsw i32 %add28, %34
  %arrayidx30 = getelementptr inbounds i8, i8* %32, i32 %add29
  %35 = load i8, i8* %arrayidx30, align 1, !tbaa !8
  %conv31 = zext i8 %35 to i32
  br label %cond.end33

cond.false32:                                     ; preds = %cond.end18
  br label %cond.end33

cond.end33:                                       ; preds = %cond.false32, %cond.true27
  %cond34 = phi i32 [ %conv31, %cond.true27 ], [ 15, %cond.false32 ]
  %add35 = add nsw i32 %add20, %cond34
  store i32 %add35, i32* %mag, align 4, !tbaa !6
  %36 = load i32, i32* %mag, align 4, !tbaa !6
  %add36 = add nsw i32 %36, 1
  %shr37 = ashr i32 %add36, 1
  %cmp38 = icmp slt i32 %shr37, 6
  br i1 %cmp38, label %cond.true40, label %cond.false43

cond.true40:                                      ; preds = %cond.end33
  %37 = load i32, i32* %mag, align 4, !tbaa !6
  %add41 = add nsw i32 %37, 1
  %shr42 = ashr i32 %add41, 1
  br label %cond.end44

cond.false43:                                     ; preds = %cond.end33
  br label %cond.end44

cond.end44:                                       ; preds = %cond.false43, %cond.true40
  %cond45 = phi i32 [ %shr42, %cond.true40 ], [ 6, %cond.false43 ]
  store i32 %cond45, i32* %mag, align 4, !tbaa !6
  %38 = load i32, i32* %row, align 4, !tbaa !6
  %39 = load i32, i32* %col, align 4, !tbaa !6
  %or = or i32 %38, %39
  %cmp46 = icmp slt i32 %or, 2
  br i1 %cmp46, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end44
  %40 = load i32, i32* %mag, align 4, !tbaa !6
  %add48 = add nsw i32 %40, 7
  store i32 %add48, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %cond.end44
  %41 = load i32, i32* %mag, align 4, !tbaa !6
  %add49 = add nsw i32 %41, 14
  store i32 %add49, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %42 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  %43 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  %44 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  %45 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  %46 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  %47 = load i32, i32* %retval, align 4
  ret i32 %47
}

; Function Attrs: alwaysinline nounwind
define internal i32 @get_lower_levels_ctx(i8* %levels, i32 %coeff_idx, i32 %bwl, i8 zeroext %tx_size, i8 zeroext %tx_class) #5 {
entry:
  %levels.addr = alloca i8*, align 4
  %coeff_idx.addr = alloca i32, align 4
  %bwl.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %tx_class.addr = alloca i8, align 1
  %stats = alloca i32, align 4
  store i8* %levels, i8** %levels.addr, align 4, !tbaa !2
  store i32 %coeff_idx, i32* %coeff_idx.addr, align 4, !tbaa !6
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i8 %tx_class, i8* %tx_class.addr, align 1, !tbaa !8
  %0 = bitcast i32* %stats to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %2 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %3 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %call = call i32 @get_padded_idx(i32 %2, i32 %3)
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %call
  %4 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %5 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %call1 = call i32 @get_nz_mag(i8* %add.ptr, i32 %4, i8 zeroext %5)
  store i32 %call1, i32* %stats, align 4, !tbaa !6
  %6 = load i32, i32* %stats, align 4, !tbaa !6
  %7 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %8 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %9 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %10 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %call2 = call i32 @get_nz_map_ctx_from_stats(i32 %6, i32 %7, i32 %8, i8 zeroext %9, i8 zeroext %10)
  %11 = bitcast i32* %stats to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret i32 %call2
}

; Function Attrs: alwaysinline nounwind
define internal i32 @get_br_ctx(i8* %levels, i32 %c, i32 %bwl, i8 zeroext %tx_class) #5 {
entry:
  %retval = alloca i32, align 4
  %levels.addr = alloca i8*, align 4
  %c.addr = alloca i32, align 4
  %bwl.addr = alloca i32, align 4
  %tx_class.addr = alloca i8, align 1
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %stride = alloca i32, align 4
  %pos = alloca i32, align 4
  %mag = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %levels, i8** %levels.addr, align 4, !tbaa !2
  store i32 %c, i32* %c.addr, align 4, !tbaa !6
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  store i8 %tx_class, i8* %tx_class.addr, align 1, !tbaa !8
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %c.addr, align 4, !tbaa !6
  %2 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shr = ashr i32 %1, %2
  store i32 %shr, i32* %row, align 4, !tbaa !6
  %3 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %c.addr, align 4, !tbaa !6
  %5 = load i32, i32* %row, align 4, !tbaa !6
  %6 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl = shl i32 %5, %6
  %sub = sub nsw i32 %4, %shl
  store i32 %sub, i32* %col, align 4, !tbaa !6
  %7 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl1 = shl i32 1, %8
  %add = add nsw i32 %shl1, 4
  store i32 %add, i32* %stride, align 4, !tbaa !6
  %9 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i32, i32* %row, align 4, !tbaa !6
  %11 = load i32, i32* %stride, align 4, !tbaa !6
  %mul = mul nsw i32 %10, %11
  %12 = load i32, i32* %col, align 4, !tbaa !6
  %add2 = add nsw i32 %mul, %12
  store i32 %add2, i32* %pos, align 4, !tbaa !6
  %13 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %15 = load i32, i32* %pos, align 4, !tbaa !6
  %add3 = add nsw i32 %15, 1
  %arrayidx = getelementptr inbounds i8, i8* %14, i32 %add3
  %16 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %16 to i32
  store i32 %conv, i32* %mag, align 4, !tbaa !6
  %17 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %18 = load i32, i32* %pos, align 4, !tbaa !6
  %19 = load i32, i32* %stride, align 4, !tbaa !6
  %add4 = add nsw i32 %18, %19
  %arrayidx5 = getelementptr inbounds i8, i8* %17, i32 %add4
  %20 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv6 = zext i8 %20 to i32
  %21 = load i32, i32* %mag, align 4, !tbaa !6
  %add7 = add nsw i32 %21, %conv6
  store i32 %add7, i32* %mag, align 4, !tbaa !6
  %22 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %conv8 = zext i8 %22 to i32
  switch i32 %conv8, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb28
    i32 2, label %sw.bb52
  ]

sw.bb:                                            ; preds = %entry
  %23 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %24 = load i32, i32* %pos, align 4, !tbaa !6
  %25 = load i32, i32* %stride, align 4, !tbaa !6
  %add9 = add nsw i32 %24, %25
  %add10 = add nsw i32 %add9, 1
  %arrayidx11 = getelementptr inbounds i8, i8* %23, i32 %add10
  %26 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  %conv12 = zext i8 %26 to i32
  %27 = load i32, i32* %mag, align 4, !tbaa !6
  %add13 = add nsw i32 %27, %conv12
  store i32 %add13, i32* %mag, align 4, !tbaa !6
  %28 = load i32, i32* %mag, align 4, !tbaa !6
  %add14 = add nsw i32 %28, 1
  %shr15 = ashr i32 %add14, 1
  %cmp = icmp slt i32 %shr15, 6
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.bb
  %29 = load i32, i32* %mag, align 4, !tbaa !6
  %add17 = add nsw i32 %29, 1
  %shr18 = ashr i32 %add17, 1
  br label %cond.end

cond.false:                                       ; preds = %sw.bb
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shr18, %cond.true ], [ 6, %cond.false ]
  store i32 %cond, i32* %mag, align 4, !tbaa !6
  %30 = load i32, i32* %c.addr, align 4, !tbaa !6
  %cmp19 = icmp eq i32 %30, 0
  br i1 %cmp19, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %31 = load i32, i32* %mag, align 4, !tbaa !6
  store i32 %31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %cond.end
  %32 = load i32, i32* %row, align 4, !tbaa !6
  %cmp21 = icmp slt i32 %32, 2
  br i1 %cmp21, label %land.lhs.true, label %if.end27

land.lhs.true:                                    ; preds = %if.end
  %33 = load i32, i32* %col, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %33, 2
  br i1 %cmp23, label %if.then25, label %if.end27

if.then25:                                        ; preds = %land.lhs.true
  %34 = load i32, i32* %mag, align 4, !tbaa !6
  %add26 = add nsw i32 %34, 7
  store i32 %add26, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %land.lhs.true, %if.end
  br label %sw.epilog

sw.bb28:                                          ; preds = %entry
  %35 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %36 = load i32, i32* %pos, align 4, !tbaa !6
  %add29 = add nsw i32 %36, 2
  %arrayidx30 = getelementptr inbounds i8, i8* %35, i32 %add29
  %37 = load i8, i8* %arrayidx30, align 1, !tbaa !8
  %conv31 = zext i8 %37 to i32
  %38 = load i32, i32* %mag, align 4, !tbaa !6
  %add32 = add nsw i32 %38, %conv31
  store i32 %add32, i32* %mag, align 4, !tbaa !6
  %39 = load i32, i32* %mag, align 4, !tbaa !6
  %add33 = add nsw i32 %39, 1
  %shr34 = ashr i32 %add33, 1
  %cmp35 = icmp slt i32 %shr34, 6
  br i1 %cmp35, label %cond.true37, label %cond.false40

cond.true37:                                      ; preds = %sw.bb28
  %40 = load i32, i32* %mag, align 4, !tbaa !6
  %add38 = add nsw i32 %40, 1
  %shr39 = ashr i32 %add38, 1
  br label %cond.end41

cond.false40:                                     ; preds = %sw.bb28
  br label %cond.end41

cond.end41:                                       ; preds = %cond.false40, %cond.true37
  %cond42 = phi i32 [ %shr39, %cond.true37 ], [ 6, %cond.false40 ]
  store i32 %cond42, i32* %mag, align 4, !tbaa !6
  %41 = load i32, i32* %c.addr, align 4, !tbaa !6
  %cmp43 = icmp eq i32 %41, 0
  br i1 %cmp43, label %if.then45, label %if.end46

if.then45:                                        ; preds = %cond.end41
  %42 = load i32, i32* %mag, align 4, !tbaa !6
  store i32 %42, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %cond.end41
  %43 = load i32, i32* %col, align 4, !tbaa !6
  %cmp47 = icmp eq i32 %43, 0
  br i1 %cmp47, label %if.then49, label %if.end51

if.then49:                                        ; preds = %if.end46
  %44 = load i32, i32* %mag, align 4, !tbaa !6
  %add50 = add nsw i32 %44, 7
  store i32 %add50, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end51:                                         ; preds = %if.end46
  br label %sw.epilog

sw.bb52:                                          ; preds = %entry
  %45 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %46 = load i32, i32* %pos, align 4, !tbaa !6
  %47 = load i32, i32* %stride, align 4, !tbaa !6
  %shl53 = shl i32 %47, 1
  %add54 = add nsw i32 %46, %shl53
  %arrayidx55 = getelementptr inbounds i8, i8* %45, i32 %add54
  %48 = load i8, i8* %arrayidx55, align 1, !tbaa !8
  %conv56 = zext i8 %48 to i32
  %49 = load i32, i32* %mag, align 4, !tbaa !6
  %add57 = add nsw i32 %49, %conv56
  store i32 %add57, i32* %mag, align 4, !tbaa !6
  %50 = load i32, i32* %mag, align 4, !tbaa !6
  %add58 = add nsw i32 %50, 1
  %shr59 = ashr i32 %add58, 1
  %cmp60 = icmp slt i32 %shr59, 6
  br i1 %cmp60, label %cond.true62, label %cond.false65

cond.true62:                                      ; preds = %sw.bb52
  %51 = load i32, i32* %mag, align 4, !tbaa !6
  %add63 = add nsw i32 %51, 1
  %shr64 = ashr i32 %add63, 1
  br label %cond.end66

cond.false65:                                     ; preds = %sw.bb52
  br label %cond.end66

cond.end66:                                       ; preds = %cond.false65, %cond.true62
  %cond67 = phi i32 [ %shr64, %cond.true62 ], [ 6, %cond.false65 ]
  store i32 %cond67, i32* %mag, align 4, !tbaa !6
  %52 = load i32, i32* %c.addr, align 4, !tbaa !6
  %cmp68 = icmp eq i32 %52, 0
  br i1 %cmp68, label %if.then70, label %if.end71

if.then70:                                        ; preds = %cond.end66
  %53 = load i32, i32* %mag, align 4, !tbaa !6
  store i32 %53, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end71:                                         ; preds = %cond.end66
  %54 = load i32, i32* %row, align 4, !tbaa !6
  %cmp72 = icmp eq i32 %54, 0
  br i1 %cmp72, label %if.then74, label %if.end76

if.then74:                                        ; preds = %if.end71
  %55 = load i32, i32* %mag, align 4, !tbaa !6
  %add75 = add nsw i32 %55, 7
  store i32 %add75, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end76:                                         ; preds = %if.end71
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end76, %if.end51, %if.end27
  %56 = load i32, i32* %mag, align 4, !tbaa !6
  %add77 = add nsw i32 %56, 14
  store i32 %add77, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %sw.epilog, %if.then74, %if.then70, %if.then49, %if.then45, %if.then25, %if.then
  %57 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  %58 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = load i32, i32* %retval, align 4
  ret i32 %62
}

; Function Attrs: alwaysinline nounwind
define internal i32 @get_nz_mag(i8* %levels, i32 %bwl, i8 zeroext %tx_class) #5 {
entry:
  %levels.addr = alloca i8*, align 4
  %bwl.addr = alloca i32, align 4
  %tx_class.addr = alloca i8, align 1
  %mag = alloca i32, align 4
  store i8* %levels, i8** %levels.addr, align 4, !tbaa !2
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  store i8 %tx_class, i8* %tx_class.addr, align 1, !tbaa !8
  %0 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 1
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %idxprom = zext i8 %2 to i32
  %arrayidx1 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom
  %3 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  %conv = zext i8 %3 to i32
  store i32 %conv, i32* %mag, align 4, !tbaa !6
  %4 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl = shl i32 1, %5
  %add = add nsw i32 %shl, 4
  %arrayidx2 = getelementptr inbounds i8, i8* %4, i32 %add
  %6 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %idxprom3 = zext i8 %6 to i32
  %arrayidx4 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom3
  %7 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = zext i8 %7 to i32
  %8 = load i32, i32* %mag, align 4, !tbaa !6
  %add6 = add nsw i32 %8, %conv5
  store i32 %add6, i32* %mag, align 4, !tbaa !6
  %9 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %conv7 = zext i8 %9 to i32
  %cmp = icmp eq i32 %conv7, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %10 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %11 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl9 = shl i32 1, %11
  %add10 = add nsw i32 %shl9, 4
  %add11 = add nsw i32 %add10, 1
  %arrayidx12 = getelementptr inbounds i8, i8* %10, i32 %add11
  %12 = load i8, i8* %arrayidx12, align 1, !tbaa !8
  %idxprom13 = zext i8 %12 to i32
  %arrayidx14 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom13
  %13 = load i8, i8* %arrayidx14, align 1, !tbaa !8
  %conv15 = zext i8 %13 to i32
  %14 = load i32, i32* %mag, align 4, !tbaa !6
  %add16 = add nsw i32 %14, %conv15
  store i32 %add16, i32* %mag, align 4, !tbaa !6
  %15 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i8, i8* %15, i32 2
  %16 = load i8, i8* %arrayidx17, align 1, !tbaa !8
  %idxprom18 = zext i8 %16 to i32
  %arrayidx19 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom18
  %17 = load i8, i8* %arrayidx19, align 1, !tbaa !8
  %conv20 = zext i8 %17 to i32
  %18 = load i32, i32* %mag, align 4, !tbaa !6
  %add21 = add nsw i32 %18, %conv20
  store i32 %add21, i32* %mag, align 4, !tbaa !6
  %19 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %20 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl22 = shl i32 2, %20
  %add23 = add nsw i32 %shl22, 8
  %arrayidx24 = getelementptr inbounds i8, i8* %19, i32 %add23
  %21 = load i8, i8* %arrayidx24, align 1, !tbaa !8
  %idxprom25 = zext i8 %21 to i32
  %arrayidx26 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom25
  %22 = load i8, i8* %arrayidx26, align 1, !tbaa !8
  %conv27 = zext i8 %22 to i32
  %23 = load i32, i32* %mag, align 4, !tbaa !6
  %add28 = add nsw i32 %23, %conv27
  store i32 %add28, i32* %mag, align 4, !tbaa !6
  br label %if.end70

if.else:                                          ; preds = %entry
  %24 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %conv29 = zext i8 %24 to i32
  %cmp30 = icmp eq i32 %conv29, 2
  br i1 %cmp30, label %if.then32, label %if.else54

if.then32:                                        ; preds = %if.else
  %25 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %26 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl33 = shl i32 2, %26
  %add34 = add nsw i32 %shl33, 8
  %arrayidx35 = getelementptr inbounds i8, i8* %25, i32 %add34
  %27 = load i8, i8* %arrayidx35, align 1, !tbaa !8
  %idxprom36 = zext i8 %27 to i32
  %arrayidx37 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom36
  %28 = load i8, i8* %arrayidx37, align 1, !tbaa !8
  %conv38 = zext i8 %28 to i32
  %29 = load i32, i32* %mag, align 4, !tbaa !6
  %add39 = add nsw i32 %29, %conv38
  store i32 %add39, i32* %mag, align 4, !tbaa !6
  %30 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %31 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl40 = shl i32 3, %31
  %add41 = add nsw i32 %shl40, 12
  %arrayidx42 = getelementptr inbounds i8, i8* %30, i32 %add41
  %32 = load i8, i8* %arrayidx42, align 1, !tbaa !8
  %idxprom43 = zext i8 %32 to i32
  %arrayidx44 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom43
  %33 = load i8, i8* %arrayidx44, align 1, !tbaa !8
  %conv45 = zext i8 %33 to i32
  %34 = load i32, i32* %mag, align 4, !tbaa !6
  %add46 = add nsw i32 %34, %conv45
  store i32 %add46, i32* %mag, align 4, !tbaa !6
  %35 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %36 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl47 = shl i32 4, %36
  %add48 = add nsw i32 %shl47, 16
  %arrayidx49 = getelementptr inbounds i8, i8* %35, i32 %add48
  %37 = load i8, i8* %arrayidx49, align 1, !tbaa !8
  %idxprom50 = zext i8 %37 to i32
  %arrayidx51 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom50
  %38 = load i8, i8* %arrayidx51, align 1, !tbaa !8
  %conv52 = zext i8 %38 to i32
  %39 = load i32, i32* %mag, align 4, !tbaa !6
  %add53 = add nsw i32 %39, %conv52
  store i32 %add53, i32* %mag, align 4, !tbaa !6
  br label %if.end

if.else54:                                        ; preds = %if.else
  %40 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i8, i8* %40, i32 2
  %41 = load i8, i8* %arrayidx55, align 1, !tbaa !8
  %idxprom56 = zext i8 %41 to i32
  %arrayidx57 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom56
  %42 = load i8, i8* %arrayidx57, align 1, !tbaa !8
  %conv58 = zext i8 %42 to i32
  %43 = load i32, i32* %mag, align 4, !tbaa !6
  %add59 = add nsw i32 %43, %conv58
  store i32 %add59, i32* %mag, align 4, !tbaa !6
  %44 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i8, i8* %44, i32 3
  %45 = load i8, i8* %arrayidx60, align 1, !tbaa !8
  %idxprom61 = zext i8 %45 to i32
  %arrayidx62 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom61
  %46 = load i8, i8* %arrayidx62, align 1, !tbaa !8
  %conv63 = zext i8 %46 to i32
  %47 = load i32, i32* %mag, align 4, !tbaa !6
  %add64 = add nsw i32 %47, %conv63
  store i32 %add64, i32* %mag, align 4, !tbaa !6
  %48 = load i8*, i8** %levels.addr, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %48, i32 4
  %49 = load i8, i8* %arrayidx65, align 1, !tbaa !8
  %idxprom66 = zext i8 %49 to i32
  %arrayidx67 = getelementptr inbounds [256 x i8], [256 x i8]* @clip_max3, i32 0, i32 %idxprom66
  %50 = load i8, i8* %arrayidx67, align 1, !tbaa !8
  %conv68 = zext i8 %50 to i32
  %51 = load i32, i32* %mag, align 4, !tbaa !6
  %add69 = add nsw i32 %51, %conv68
  store i32 %add69, i32* %mag, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else54, %if.then32
  br label %if.end70

if.end70:                                         ; preds = %if.end, %if.then
  %52 = load i32, i32* %mag, align 4, !tbaa !6
  %53 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  ret i32 %52
}

; Function Attrs: alwaysinline nounwind
define internal i32 @get_nz_map_ctx_from_stats(i32 %stats, i32 %coeff_idx, i32 %bwl, i8 zeroext %tx_size, i8 zeroext %tx_class) #5 {
entry:
  %retval = alloca i32, align 4
  %stats.addr = alloca i32, align 4
  %coeff_idx.addr = alloca i32, align 4
  %bwl.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %tx_class.addr = alloca i8, align 1
  %ctx = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %row13 = alloca i32, align 4
  store i32 %stats, i32* %stats.addr, align 4, !tbaa !6
  store i32 %coeff_idx, i32* %coeff_idx.addr, align 4, !tbaa !6
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i8 %tx_class, i8* %tx_class.addr, align 1, !tbaa !8
  %0 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %1 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %or = or i32 %conv, %1
  %cmp = icmp eq i32 %or, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %stats.addr, align 4, !tbaa !6
  %add = add nsw i32 %3, 1
  %shr = ashr i32 %add, 1
  store i32 %shr, i32* %ctx, align 4, !tbaa !6
  %4 = load i32, i32* %ctx, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %4, 4
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %5 = load i32, i32* %ctx, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %5, %cond.true ], [ 4, %cond.false ]
  store i32 %cond, i32* %ctx, align 4, !tbaa !6
  %6 = load i8, i8* %tx_class.addr, align 1, !tbaa !8
  %conv4 = zext i8 %6 to i32
  switch i32 %conv4, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb8
    i32 2, label %sw.bb12
  ]

sw.bb:                                            ; preds = %cond.end
  %7 = load i32, i32* %ctx, align 4, !tbaa !6
  %8 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %8 to i32
  %arrayidx = getelementptr inbounds [19 x i8*], [19 x i8*]* @av1_nz_map_ctx_offset, i32 0, i32 %idxprom
  %9 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %10 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv6 = sext i8 %11 to i32
  %add7 = add nsw i32 %7, %conv6
  store i32 %add7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb8:                                           ; preds = %cond.end
  %12 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %14 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shr9 = ashr i32 %13, %14
  store i32 %shr9, i32* %row, align 4, !tbaa !6
  %15 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %17 = load i32, i32* %row, align 4, !tbaa !6
  %18 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl = shl i32 %17, %18
  %sub = sub nsw i32 %16, %shl
  store i32 %sub, i32* %col, align 4, !tbaa !6
  %19 = load i32, i32* %ctx, align 4, !tbaa !6
  %20 = load i32, i32* %col, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [32 x i32], [32 x i32]* @nz_map_ctx_offset_1d, i32 0, i32 %20
  %21 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %add11 = add nsw i32 %19, %21
  store i32 %add11, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %22 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  br label %cleanup

sw.bb12:                                          ; preds = %cond.end
  %24 = bitcast i32* %row13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load i32, i32* %coeff_idx.addr, align 4, !tbaa !6
  %26 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shr14 = ashr i32 %25, %26
  store i32 %shr14, i32* %row13, align 4, !tbaa !6
  %27 = load i32, i32* %ctx, align 4, !tbaa !6
  %28 = load i32, i32* %row13, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [32 x i32], [32 x i32]* @nz_map_ctx_offset_1d, i32 0, i32 %28
  %29 = load i32, i32* %arrayidx15, align 4, !tbaa !6
  %add16 = add nsw i32 %27, %29
  store i32 %add16, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i32* %row13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  br label %cleanup

sw.default:                                       ; preds = %cond.end
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %sw.epilog, %sw.bb12, %sw.bb8, %sw.bb
  %31 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %32 = load i32, i32* %retval, align 4
  ret i32 %32
}

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #3

; Function Attrs: inlinehint nounwind
define internal i32 @get_entropy_context(i8 zeroext %tx_size, i8* %a, i8* %l) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  %a.addr = alloca i8*, align 4
  %l.addr = alloca i8*, align 4
  %above_ec = alloca i8, align 1
  %left_ec = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i8* %a, i8** %a.addr, align 4, !tbaa !2
  store i8* %l, i8** %l.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %above_ec) #6
  store i8 0, i8* %above_ec, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %left_ec) #6
  store i8 0, i8* %left_ec, align 1, !tbaa !8
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 5, label %sw.bb9
    i32 6, label %sw.bb17
    i32 7, label %sw.bb29
    i32 8, label %sw.bb42
    i32 9, label %sw.bb55
    i32 10, label %sw.bb68
    i32 1, label %sw.bb81
    i32 2, label %sw.bb94
    i32 3, label %sw.bb107
    i32 4, label %sw.bb120
    i32 11, label %sw.bb135
    i32 12, label %sw.bb150
    i32 13, label %sw.bb165
    i32 14, label %sw.bb177
    i32 15, label %sw.bb189
    i32 16, label %sw.bb202
    i32 17, label %sw.bb215
    i32 18, label %sw.bb230
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 0
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv1 = sext i8 %2 to i32
  %cmp = icmp ne i32 %conv1, 0
  %conv2 = zext i1 %cmp to i32
  %conv3 = trunc i32 %conv2 to i8
  store i8 %conv3, i8* %above_ec, align 1, !tbaa !8
  %3 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = sext i8 %4 to i32
  %cmp6 = icmp ne i32 %conv5, 0
  %conv7 = zext i1 %cmp6 to i32
  %conv8 = trunc i32 %conv7 to i8
  store i8 %conv8, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb9:                                           ; preds = %entry
  %5 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %5, i32 0
  %6 = load i8, i8* %arrayidx10, align 1, !tbaa !8
  %conv11 = sext i8 %6 to i32
  %cmp12 = icmp ne i32 %conv11, 0
  %conv13 = zext i1 %cmp12 to i32
  %conv14 = trunc i32 %conv13 to i8
  store i8 %conv14, i8* %above_ec, align 1, !tbaa !8
  %7 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %8 = bitcast i8* %7 to i16*
  %9 = load i16, i16* %8, align 2, !tbaa !20
  %tobool = icmp ne i16 %9, 0
  %lnot = xor i1 %tobool, true
  %lnot15 = xor i1 %lnot, true
  %lnot.ext = zext i1 %lnot15 to i32
  %conv16 = trunc i32 %lnot.ext to i8
  store i8 %conv16, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb17:                                          ; preds = %entry
  %10 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %11 = bitcast i8* %10 to i16*
  %12 = load i16, i16* %11, align 2, !tbaa !20
  %tobool18 = icmp ne i16 %12, 0
  %lnot19 = xor i1 %tobool18, true
  %lnot21 = xor i1 %lnot19, true
  %lnot.ext22 = zext i1 %lnot21 to i32
  %conv23 = trunc i32 %lnot.ext22 to i8
  store i8 %conv23, i8* %above_ec, align 1, !tbaa !8
  %13 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %13, i32 0
  %14 = load i8, i8* %arrayidx24, align 1, !tbaa !8
  %conv25 = sext i8 %14 to i32
  %cmp26 = icmp ne i32 %conv25, 0
  %conv27 = zext i1 %cmp26 to i32
  %conv28 = trunc i32 %conv27 to i8
  store i8 %conv28, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb29:                                          ; preds = %entry
  %15 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %16 = bitcast i8* %15 to i16*
  %17 = load i16, i16* %16, align 2, !tbaa !20
  %tobool30 = icmp ne i16 %17, 0
  %lnot31 = xor i1 %tobool30, true
  %lnot33 = xor i1 %lnot31, true
  %lnot.ext34 = zext i1 %lnot33 to i32
  %conv35 = trunc i32 %lnot.ext34 to i8
  store i8 %conv35, i8* %above_ec, align 1, !tbaa !8
  %18 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %19 = bitcast i8* %18 to i32*
  %20 = load i32, i32* %19, align 4, !tbaa !6
  %tobool36 = icmp ne i32 %20, 0
  %lnot37 = xor i1 %tobool36, true
  %lnot39 = xor i1 %lnot37, true
  %lnot.ext40 = zext i1 %lnot39 to i32
  %conv41 = trunc i32 %lnot.ext40 to i8
  store i8 %conv41, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb42:                                          ; preds = %entry
  %21 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %22 = bitcast i8* %21 to i32*
  %23 = load i32, i32* %22, align 4, !tbaa !6
  %tobool43 = icmp ne i32 %23, 0
  %lnot44 = xor i1 %tobool43, true
  %lnot46 = xor i1 %lnot44, true
  %lnot.ext47 = zext i1 %lnot46 to i32
  %conv48 = trunc i32 %lnot.ext47 to i8
  store i8 %conv48, i8* %above_ec, align 1, !tbaa !8
  %24 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %25 = bitcast i8* %24 to i16*
  %26 = load i16, i16* %25, align 2, !tbaa !20
  %tobool49 = icmp ne i16 %26, 0
  %lnot50 = xor i1 %tobool49, true
  %lnot52 = xor i1 %lnot50, true
  %lnot.ext53 = zext i1 %lnot52 to i32
  %conv54 = trunc i32 %lnot.ext53 to i8
  store i8 %conv54, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb55:                                          ; preds = %entry
  %27 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %28 = bitcast i8* %27 to i32*
  %29 = load i32, i32* %28, align 4, !tbaa !6
  %tobool56 = icmp ne i32 %29, 0
  %lnot57 = xor i1 %tobool56, true
  %lnot59 = xor i1 %lnot57, true
  %lnot.ext60 = zext i1 %lnot59 to i32
  %conv61 = trunc i32 %lnot.ext60 to i8
  store i8 %conv61, i8* %above_ec, align 1, !tbaa !8
  %30 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %31 = bitcast i8* %30 to i64*
  %32 = load i64, i64* %31, align 8, !tbaa !77
  %tobool62 = icmp ne i64 %32, 0
  %lnot63 = xor i1 %tobool62, true
  %lnot65 = xor i1 %lnot63, true
  %lnot.ext66 = zext i1 %lnot65 to i32
  %conv67 = trunc i32 %lnot.ext66 to i8
  store i8 %conv67, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb68:                                          ; preds = %entry
  %33 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %34 = bitcast i8* %33 to i64*
  %35 = load i64, i64* %34, align 8, !tbaa !77
  %tobool69 = icmp ne i64 %35, 0
  %lnot70 = xor i1 %tobool69, true
  %lnot72 = xor i1 %lnot70, true
  %lnot.ext73 = zext i1 %lnot72 to i32
  %conv74 = trunc i32 %lnot.ext73 to i8
  store i8 %conv74, i8* %above_ec, align 1, !tbaa !8
  %36 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %37 = bitcast i8* %36 to i32*
  %38 = load i32, i32* %37, align 4, !tbaa !6
  %tobool75 = icmp ne i32 %38, 0
  %lnot76 = xor i1 %tobool75, true
  %lnot78 = xor i1 %lnot76, true
  %lnot.ext79 = zext i1 %lnot78 to i32
  %conv80 = trunc i32 %lnot.ext79 to i8
  store i8 %conv80, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb81:                                          ; preds = %entry
  %39 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %40 = bitcast i8* %39 to i16*
  %41 = load i16, i16* %40, align 2, !tbaa !20
  %tobool82 = icmp ne i16 %41, 0
  %lnot83 = xor i1 %tobool82, true
  %lnot85 = xor i1 %lnot83, true
  %lnot.ext86 = zext i1 %lnot85 to i32
  %conv87 = trunc i32 %lnot.ext86 to i8
  store i8 %conv87, i8* %above_ec, align 1, !tbaa !8
  %42 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %43 = bitcast i8* %42 to i16*
  %44 = load i16, i16* %43, align 2, !tbaa !20
  %tobool88 = icmp ne i16 %44, 0
  %lnot89 = xor i1 %tobool88, true
  %lnot91 = xor i1 %lnot89, true
  %lnot.ext92 = zext i1 %lnot91 to i32
  %conv93 = trunc i32 %lnot.ext92 to i8
  store i8 %conv93, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb94:                                          ; preds = %entry
  %45 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %46 = bitcast i8* %45 to i32*
  %47 = load i32, i32* %46, align 4, !tbaa !6
  %tobool95 = icmp ne i32 %47, 0
  %lnot96 = xor i1 %tobool95, true
  %lnot98 = xor i1 %lnot96, true
  %lnot.ext99 = zext i1 %lnot98 to i32
  %conv100 = trunc i32 %lnot.ext99 to i8
  store i8 %conv100, i8* %above_ec, align 1, !tbaa !8
  %48 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %49 = bitcast i8* %48 to i32*
  %50 = load i32, i32* %49, align 4, !tbaa !6
  %tobool101 = icmp ne i32 %50, 0
  %lnot102 = xor i1 %tobool101, true
  %lnot104 = xor i1 %lnot102, true
  %lnot.ext105 = zext i1 %lnot104 to i32
  %conv106 = trunc i32 %lnot.ext105 to i8
  store i8 %conv106, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb107:                                         ; preds = %entry
  %51 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %52 = bitcast i8* %51 to i64*
  %53 = load i64, i64* %52, align 8, !tbaa !77
  %tobool108 = icmp ne i64 %53, 0
  %lnot109 = xor i1 %tobool108, true
  %lnot111 = xor i1 %lnot109, true
  %lnot.ext112 = zext i1 %lnot111 to i32
  %conv113 = trunc i32 %lnot.ext112 to i8
  store i8 %conv113, i8* %above_ec, align 1, !tbaa !8
  %54 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %55 = bitcast i8* %54 to i64*
  %56 = load i64, i64* %55, align 8, !tbaa !77
  %tobool114 = icmp ne i64 %56, 0
  %lnot115 = xor i1 %tobool114, true
  %lnot117 = xor i1 %lnot115, true
  %lnot.ext118 = zext i1 %lnot117 to i32
  %conv119 = trunc i32 %lnot.ext118 to i8
  store i8 %conv119, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb120:                                         ; preds = %entry
  %57 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %58 = bitcast i8* %57 to i64*
  %59 = load i64, i64* %58, align 8, !tbaa !77
  %60 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %60, i32 8
  %61 = bitcast i8* %add.ptr to i64*
  %62 = load i64, i64* %61, align 8, !tbaa !77
  %or = or i64 %59, %62
  %tobool121 = icmp ne i64 %or, 0
  %lnot122 = xor i1 %tobool121, true
  %lnot124 = xor i1 %lnot122, true
  %lnot.ext125 = zext i1 %lnot124 to i32
  %conv126 = trunc i32 %lnot.ext125 to i8
  store i8 %conv126, i8* %above_ec, align 1, !tbaa !8
  %63 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %64 = bitcast i8* %63 to i64*
  %65 = load i64, i64* %64, align 8, !tbaa !77
  %66 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %add.ptr127 = getelementptr inbounds i8, i8* %66, i32 8
  %67 = bitcast i8* %add.ptr127 to i64*
  %68 = load i64, i64* %67, align 8, !tbaa !77
  %or128 = or i64 %65, %68
  %tobool129 = icmp ne i64 %or128, 0
  %lnot130 = xor i1 %tobool129, true
  %lnot132 = xor i1 %lnot130, true
  %lnot.ext133 = zext i1 %lnot132 to i32
  %conv134 = trunc i32 %lnot.ext133 to i8
  store i8 %conv134, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb135:                                         ; preds = %entry
  %69 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %70 = bitcast i8* %69 to i64*
  %71 = load i64, i64* %70, align 8, !tbaa !77
  %tobool136 = icmp ne i64 %71, 0
  %lnot137 = xor i1 %tobool136, true
  %lnot139 = xor i1 %lnot137, true
  %lnot.ext140 = zext i1 %lnot139 to i32
  %conv141 = trunc i32 %lnot.ext140 to i8
  store i8 %conv141, i8* %above_ec, align 1, !tbaa !8
  %72 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %73 = bitcast i8* %72 to i64*
  %74 = load i64, i64* %73, align 8, !tbaa !77
  %75 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %add.ptr142 = getelementptr inbounds i8, i8* %75, i32 8
  %76 = bitcast i8* %add.ptr142 to i64*
  %77 = load i64, i64* %76, align 8, !tbaa !77
  %or143 = or i64 %74, %77
  %tobool144 = icmp ne i64 %or143, 0
  %lnot145 = xor i1 %tobool144, true
  %lnot147 = xor i1 %lnot145, true
  %lnot.ext148 = zext i1 %lnot147 to i32
  %conv149 = trunc i32 %lnot.ext148 to i8
  store i8 %conv149, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb150:                                         ; preds = %entry
  %78 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %79 = bitcast i8* %78 to i64*
  %80 = load i64, i64* %79, align 8, !tbaa !77
  %81 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %add.ptr151 = getelementptr inbounds i8, i8* %81, i32 8
  %82 = bitcast i8* %add.ptr151 to i64*
  %83 = load i64, i64* %82, align 8, !tbaa !77
  %or152 = or i64 %80, %83
  %tobool153 = icmp ne i64 %or152, 0
  %lnot154 = xor i1 %tobool153, true
  %lnot156 = xor i1 %lnot154, true
  %lnot.ext157 = zext i1 %lnot156 to i32
  %conv158 = trunc i32 %lnot.ext157 to i8
  store i8 %conv158, i8* %above_ec, align 1, !tbaa !8
  %84 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %85 = bitcast i8* %84 to i64*
  %86 = load i64, i64* %85, align 8, !tbaa !77
  %tobool159 = icmp ne i64 %86, 0
  %lnot160 = xor i1 %tobool159, true
  %lnot162 = xor i1 %lnot160, true
  %lnot.ext163 = zext i1 %lnot162 to i32
  %conv164 = trunc i32 %lnot.ext163 to i8
  store i8 %conv164, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb165:                                         ; preds = %entry
  %87 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i8, i8* %87, i32 0
  %88 = load i8, i8* %arrayidx166, align 1, !tbaa !8
  %conv167 = sext i8 %88 to i32
  %cmp168 = icmp ne i32 %conv167, 0
  %conv169 = zext i1 %cmp168 to i32
  %conv170 = trunc i32 %conv169 to i8
  store i8 %conv170, i8* %above_ec, align 1, !tbaa !8
  %89 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %90 = bitcast i8* %89 to i32*
  %91 = load i32, i32* %90, align 4, !tbaa !6
  %tobool171 = icmp ne i32 %91, 0
  %lnot172 = xor i1 %tobool171, true
  %lnot174 = xor i1 %lnot172, true
  %lnot.ext175 = zext i1 %lnot174 to i32
  %conv176 = trunc i32 %lnot.ext175 to i8
  store i8 %conv176, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb177:                                         ; preds = %entry
  %92 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %93 = bitcast i8* %92 to i32*
  %94 = load i32, i32* %93, align 4, !tbaa !6
  %tobool178 = icmp ne i32 %94, 0
  %lnot179 = xor i1 %tobool178, true
  %lnot181 = xor i1 %lnot179, true
  %lnot.ext182 = zext i1 %lnot181 to i32
  %conv183 = trunc i32 %lnot.ext182 to i8
  store i8 %conv183, i8* %above_ec, align 1, !tbaa !8
  %95 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %arrayidx184 = getelementptr inbounds i8, i8* %95, i32 0
  %96 = load i8, i8* %arrayidx184, align 1, !tbaa !8
  %conv185 = sext i8 %96 to i32
  %cmp186 = icmp ne i32 %conv185, 0
  %conv187 = zext i1 %cmp186 to i32
  %conv188 = trunc i32 %conv187 to i8
  store i8 %conv188, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb189:                                         ; preds = %entry
  %97 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %98 = bitcast i8* %97 to i16*
  %99 = load i16, i16* %98, align 2, !tbaa !20
  %tobool190 = icmp ne i16 %99, 0
  %lnot191 = xor i1 %tobool190, true
  %lnot193 = xor i1 %lnot191, true
  %lnot.ext194 = zext i1 %lnot193 to i32
  %conv195 = trunc i32 %lnot.ext194 to i8
  store i8 %conv195, i8* %above_ec, align 1, !tbaa !8
  %100 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %101 = bitcast i8* %100 to i64*
  %102 = load i64, i64* %101, align 8, !tbaa !77
  %tobool196 = icmp ne i64 %102, 0
  %lnot197 = xor i1 %tobool196, true
  %lnot199 = xor i1 %lnot197, true
  %lnot.ext200 = zext i1 %lnot199 to i32
  %conv201 = trunc i32 %lnot.ext200 to i8
  store i8 %conv201, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb202:                                         ; preds = %entry
  %103 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %104 = bitcast i8* %103 to i64*
  %105 = load i64, i64* %104, align 8, !tbaa !77
  %tobool203 = icmp ne i64 %105, 0
  %lnot204 = xor i1 %tobool203, true
  %lnot206 = xor i1 %lnot204, true
  %lnot.ext207 = zext i1 %lnot206 to i32
  %conv208 = trunc i32 %lnot.ext207 to i8
  store i8 %conv208, i8* %above_ec, align 1, !tbaa !8
  %106 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %107 = bitcast i8* %106 to i16*
  %108 = load i16, i16* %107, align 2, !tbaa !20
  %tobool209 = icmp ne i16 %108, 0
  %lnot210 = xor i1 %tobool209, true
  %lnot212 = xor i1 %lnot210, true
  %lnot.ext213 = zext i1 %lnot212 to i32
  %conv214 = trunc i32 %lnot.ext213 to i8
  store i8 %conv214, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb215:                                         ; preds = %entry
  %109 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %110 = bitcast i8* %109 to i32*
  %111 = load i32, i32* %110, align 4, !tbaa !6
  %tobool216 = icmp ne i32 %111, 0
  %lnot217 = xor i1 %tobool216, true
  %lnot219 = xor i1 %lnot217, true
  %lnot.ext220 = zext i1 %lnot219 to i32
  %conv221 = trunc i32 %lnot.ext220 to i8
  store i8 %conv221, i8* %above_ec, align 1, !tbaa !8
  %112 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %113 = bitcast i8* %112 to i64*
  %114 = load i64, i64* %113, align 8, !tbaa !77
  %115 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %add.ptr222 = getelementptr inbounds i8, i8* %115, i32 8
  %116 = bitcast i8* %add.ptr222 to i64*
  %117 = load i64, i64* %116, align 8, !tbaa !77
  %or223 = or i64 %114, %117
  %tobool224 = icmp ne i64 %or223, 0
  %lnot225 = xor i1 %tobool224, true
  %lnot227 = xor i1 %lnot225, true
  %lnot.ext228 = zext i1 %lnot227 to i32
  %conv229 = trunc i32 %lnot.ext228 to i8
  store i8 %conv229, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.bb230:                                         ; preds = %entry
  %118 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %119 = bitcast i8* %118 to i64*
  %120 = load i64, i64* %119, align 8, !tbaa !77
  %121 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %add.ptr231 = getelementptr inbounds i8, i8* %121, i32 8
  %122 = bitcast i8* %add.ptr231 to i64*
  %123 = load i64, i64* %122, align 8, !tbaa !77
  %or232 = or i64 %120, %123
  %tobool233 = icmp ne i64 %or232, 0
  %lnot234 = xor i1 %tobool233, true
  %lnot236 = xor i1 %lnot234, true
  %lnot.ext237 = zext i1 %lnot236 to i32
  %conv238 = trunc i32 %lnot.ext237 to i8
  store i8 %conv238, i8* %above_ec, align 1, !tbaa !8
  %124 = load i8*, i8** %l.addr, align 4, !tbaa !2
  %125 = bitcast i8* %124 to i32*
  %126 = load i32, i32* %125, align 4, !tbaa !6
  %tobool239 = icmp ne i32 %126, 0
  %lnot240 = xor i1 %tobool239, true
  %lnot242 = xor i1 %lnot240, true
  %lnot.ext243 = zext i1 %lnot242 to i32
  %conv244 = trunc i32 %lnot.ext243 to i8
  store i8 %conv244, i8* %left_ec, align 1, !tbaa !8
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb230, %sw.bb215, %sw.bb202, %sw.bb189, %sw.bb177, %sw.bb165, %sw.bb150, %sw.bb135, %sw.bb120, %sw.bb107, %sw.bb94, %sw.bb81, %sw.bb68, %sw.bb55, %sw.bb42, %sw.bb29, %sw.bb17, %sw.bb9, %sw.bb
  %127 = load i8, i8* %above_ec, align 1, !tbaa !8
  %128 = load i8, i8* %left_ec, align 1, !tbaa !8
  %call = call i32 @combine_entropy_contexts(i8 signext %127, i8 signext %128)
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %left_ec) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %above_ec) #6
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define internal i32 @combine_entropy_contexts(i8 signext %a, i8 signext %b) #2 {
entry:
  %a.addr = alloca i8, align 1
  %b.addr = alloca i8, align 1
  store i8 %a, i8* %a.addr, align 1, !tbaa !8
  store i8 %b, i8* %b.addr, align 1, !tbaa !8
  %0 = load i8, i8* %a.addr, align 1, !tbaa !8
  %conv = sext i8 %0 to i32
  %cmp = icmp ne i32 %conv, 0
  %conv1 = zext i1 %cmp to i32
  %1 = load i8, i8* %b.addr, align 1, !tbaa !8
  %conv2 = sext i8 %1 to i32
  %cmp3 = icmp ne i32 %conv2, 0
  %conv4 = zext i1 %cmp3 to i32
  %add = add nsw i32 %conv1, %conv4
  ret i32 %add
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_intrabc_block(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  ret i32 %conv
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !3, i64 6832}
!10 = !{!"macroblockd", !7, i64 0, !7, i64 4, !7, i64 8, !11, i64 12, !4, i64 16, !12, i64 4060, !3, i64 4084, !11, i64 4088, !11, i64 4089, !11, i64 4090, !11, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !7, i64 4112, !7, i64 4116, !7, i64 4120, !7, i64 4124, !7, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !7, i64 6836, !4, i64 6840, !4, i64 6872, !7, i64 6904, !7, i64 6908, !3, i64 6912, !3, i64 6916, !7, i64 6920, !7, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !13, i64 39720, !14, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!11 = !{!"_Bool", !4, i64 0}
!12 = !{!"TileInfo", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20}
!13 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !7, i64 4104, !4, i64 4108, !7, i64 4236, !7, i64 4240, !7, i64 4244, !7, i64 4248, !7, i64 4252, !7, i64 4256}
!14 = !{!"dist_wtd_comp_params", !7, i64 0, !7, i64 4, !7, i64 8}
!15 = !{!10, !7, i64 6836}
!16 = !{!10, !3, i64 4084}
!17 = !{!18, !3, i64 4}
!18 = !{!"macroblockd_plane", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !7, i64 16, !7, i64 20, !19, i64 24, !4, i64 44, !3, i64 84, !3, i64 88, !4, i64 92, !3, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!19 = !{!"buf_2d", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!20 = !{!21, !21, i64 0}
!21 = !{!"short", !4, i64 0}
!22 = !{!23, !7, i64 0}
!23 = !{!"txb_ctx", !7, i64 0, !7, i64 4}
!24 = !{!18, !3, i64 8}
!25 = !{!10, !3, i64 4108}
!26 = !{!10, !7, i64 4112}
!27 = !{!28, !11, i64 1337}
!28 = !{!"AV1Common", !29, i64 0, !31, i64 40, !7, i64 288, !7, i64 292, !7, i64 296, !7, i64 300, !7, i64 304, !7, i64 308, !4, i64 312, !11, i64 313, !4, i64 316, !7, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !32, i64 492, !4, i64 580, !4, i64 1284, !7, i64 1316, !7, i64 1320, !7, i64 1324, !33, i64 1328, !34, i64 1356, !35, i64 1420, !36, i64 10676, !3, i64 10848, !37, i64 10864, !38, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !39, i64 14880, !41, i64 15028, !42, i64 15168, !43, i64 15816, !4, i64 15836, !44, i64 16192, !3, i64 18128, !3, i64 18132, !47, i64 18136, !3, i64 18724, !48, i64 18728, !7, i64 18760, !4, i64 18764, !3, i64 18796, !7, i64 18800, !4, i64 18804, !4, i64 18836, !7, i64 18844, !7, i64 18848, !7, i64 18852, !7, i64 18856}
!29 = !{!"", !4, i64 0, !4, i64 1, !7, i64 4, !7, i64 8, !7, i64 12, !30, i64 16, !7, i64 32, !7, i64 36}
!30 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!31 = !{!"aom_internal_error_info", !4, i64 0, !7, i64 4, !4, i64 8, !7, i64 88, !4, i64 92}
!32 = !{!"scale_factors", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!33 = !{!"", !11, i64 0, !11, i64 1, !11, i64 2, !11, i64 3, !11, i64 4, !11, i64 5, !11, i64 6, !11, i64 7, !11, i64 8, !11, i64 9, !11, i64 10, !11, i64 11, !4, i64 12, !4, i64 13, !7, i64 16, !7, i64 20, !4, i64 24}
!34 = !{!"CommonModeInfoParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !7, i64 28, !4, i64 32, !3, i64 36, !7, i64 40, !7, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!35 = !{!"CommonQuantParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !11, i64 9240, !7, i64 9244, !7, i64 9248, !7, i64 9252}
!36 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !7, i64 164, !4, i64 168}
!37 = !{!"", !4, i64 0, !4, i64 3072}
!38 = !{!"loopfilter", !4, i64 0, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !7, i64 32}
!39 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !40, i64 80, !7, i64 84, !40, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!40 = !{!"long", !4, i64 0}
!41 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !4, i64 72, !7, i64 136}
!42 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !21, i64 644}
!43 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!44 = !{!"SequenceHeader", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !4, i64 16, !7, i64 20, !7, i64 24, !4, i64 28, !7, i64 32, !7, i64 36, !30, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !7, i64 112, !4, i64 116, !7, i64 244, !45, i64 248, !4, i64 264, !46, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!45 = !{!"aom_timing", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!46 = !{!"aom_dec_model_info", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!47 = !{!"CommonTileParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !4, i64 60, !4, i64 320, !7, i64 580, !7, i64 584}
!48 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !7, i64 20, !7, i64 24, !7, i64 28}
!49 = !{i8 0, i8 2}
!50 = !{!51, !3, i64 0}
!51 = !{!"", !3, i64 0, !3, i64 4}
!52 = !{!23, !7, i64 4}
!53 = !{!54, !4, i64 36}
!54 = !{!"aom_reader", !3, i64 0, !3, i64 4, !55, i64 8, !3, i64 32, !4, i64 36}
!55 = !{!"od_ec_dec", !3, i64 0, !7, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !21, i64 20, !21, i64 22}
!56 = !{!18, !7, i64 20}
!57 = !{!18, !7, i64 16}
!58 = !{!10, !3, i64 6912}
!59 = !{!60, !4, i64 118}
!60 = !{!"MB_MODE_INFO", !61, i64 0, !62, i64 8, !4, i64 52, !7, i64 60, !4, i64 64, !63, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !64, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!61 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!62 = !{!"", !4, i64 0, !21, i64 32, !21, i64 34, !21, i64 36, !21, i64 38, !4, i64 40, !4, i64 41}
!63 = !{!"", !4, i64 0, !4, i64 48}
!64 = !{!"", !4, i64 0, !4, i64 1}
!65 = !{!18, !3, i64 84}
!66 = !{!18, !3, i64 88}
!67 = !{!54, !3, i64 32}
!68 = !{!69, !7, i64 3096}
!69 = !{!"Accounting", !70, i64 0, !7, i64 1044, !4, i64 1048, !72, i64 3090, !7, i64 3096}
!70 = !{!"", !3, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !71, i64 16}
!71 = !{!"", !4, i64 0, !7, i64 1024}
!72 = !{!"", !21, i64 0, !21, i64 2}
!73 = !{!69, !7, i64 8}
!74 = !{!69, !7, i64 12}
!75 = !{!60, !4, i64 119}
!76 = !{!60, !4, i64 120}
!77 = !{!78, !78, i64 0}
!78 = !{!"long long", !4, i64 0}
