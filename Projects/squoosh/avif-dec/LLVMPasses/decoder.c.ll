; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/decoder.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/decoder.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.AV1Decoder = type { %struct.macroblockd, %struct.AV1Common, %struct.AVxWorker, %struct.AV1LfSyncData, %struct.AV1LrSyncData, %struct.AV1LrStruct, %struct.AVxWorker*, i32, %struct.DecWorkerData*, %struct.ThreadData, %struct.TileDataDec*, i32, [64 x [64 x %struct.TileBufferDec]], %struct.AV1DecTileMTData, i32, [4 x %struct.RefCntBuffer*], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.Accounting, i32, i32, i32, i32, i32, i32, i32, i32, %struct.DataBuffer, i32, i32, i32, i32, i32, i32, %struct.EXTERNAL_REFERENCES, %struct.yv12_buffer_config, %struct.CB_BUFFER*, i32, i32, %struct.AV1DecRowMTInfo, %struct.aom_metadata_array*, i32, i32, i32, i32, [8 x i32] }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_mv = type { i32 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.AVxWorker = type { %struct.AVxWorkerImpl*, i32, i8*, i32 (i8*, i8*)*, i8*, i8*, i32 }
%struct.AVxWorkerImpl = type opaque
%struct.AV1LfSyncData = type { [3 x i32*], i32, i32, %struct.LoopFilterWorkerData*, i32, %struct.AV1LfMTInfo*, i32, i32 }
%struct.LoopFilterWorkerData = type { %struct.yv12_buffer_config*, %struct.AV1Common*, [3 x %struct.macroblockd_plane], %struct.macroblockd* }
%struct.AV1LfMTInfo = type { i32, i32, i32 }
%struct.AV1LrSyncData = type { [3 x i32*], i32, i32, i32, i32, %struct.LoopRestorationWorkerData*, %struct.AV1LrMTInfo*, i32, i32 }
%struct.LoopRestorationWorkerData = type { i32*, i8*, i8* }
%struct.AV1LrMTInfo = type { i32, i32, i32, i32, i32, i32, i32 }
%struct.AV1LrStruct = type { void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, [3 x %struct.FilterFrameCtxt], %struct.yv12_buffer_config*, %struct.yv12_buffer_config* }
%struct.RestorationTileLimits = type { i32, i32, i32, i32 }
%struct.AV1PixelRect = type { i32, i32, i32, i32 }
%struct.FilterFrameCtxt = type { %struct.RestorationInfo*, i32, i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.AV1PixelRect }
%struct.DecWorkerData = type { %struct.ThreadData*, i8*, %struct.aom_internal_error_info }
%struct.ThreadData = type { %struct.macroblockd, %struct.CB_BUFFER, %struct.aom_reader*, [2 x i8*], i32, i32, i16*, [2 x i8*], void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, i8)*, void (%struct.AV1Common*, %struct.macroblockd*)*, [8 x i8] }
%struct.CB_BUFFER = type { [3 x [16384 x i32]], [3 x [1024 x %struct.eob_info]], [2 x [16384 x i8]] }
%struct.aom_reader = type { i8*, i8*, %struct.od_ec_dec, %struct.Accounting*, i8 }
%struct.od_ec_dec = type { i8*, i32, i8*, i8*, i32, i16, i16 }
%struct.TileDataDec = type { %struct.TileInfo, %struct.aom_reader, %struct.frame_contexts, %struct.AV1DecRowMTSyncData }
%struct.AV1DecRowMTSyncData = type { i32, i32*, i32, i32, i32, i32, i32, i32 }
%struct.TileBufferDec = type { i8*, i32 }
%struct.AV1DecTileMTData = type { %struct.TileJobsDec*, i32, i32, i32, i32 }
%struct.TileJobsDec = type { %struct.TileBufferDec*, %struct.TileDataDec* }
%struct.Accounting = type { %struct.AccountingSymbols, i32, [1021 x i16], %struct.AccountingSymbolContext, i32 }
%struct.AccountingSymbols = type { %struct.AccountingSymbol*, i32, i32, i32, %struct.AccountingDictionary }
%struct.AccountingSymbol = type { %struct.AccountingSymbolContext, i32, i32, i32 }
%struct.AccountingDictionary = type { [256 x i8*], i32 }
%struct.AccountingSymbolContext = type { i16, i16 }
%struct.DataBuffer = type { i8*, i32 }
%struct.EXTERNAL_REFERENCES = type { [128 x %struct.yv12_buffer_config], i32 }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.AV1DecRowMTInfo = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.AVxWorkerInterface = type { void (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)* }

@.str = private unnamed_addr constant [26 x i8] c"Failed to allocate cm->fc\00", align 1
@.str.1 = private unnamed_addr constant [45 x i8] c"Failed to allocate cm->default_frame_context\00", align 1
@.str.2 = private unnamed_addr constant [14 x i8] c"aom lf worker\00", align 1
@.str.3 = private unnamed_addr constant [19 x i8] c"No reference frame\00", align 1
@.str.4 = private unnamed_addr constant [28 x i8] c"Incorrect buffer dimensions\00", align 1
@aom_once.done = internal global i32 0, align 4

; Function Attrs: nounwind
define hidden %struct.AV1Decoder* @av1_decoder_create(%struct.BufferPool* %pool) #0 {
entry:
  %retval = alloca %struct.AV1Decoder*, align 4
  %pool.addr = alloca %struct.BufferPool*, align 4
  %pbi = alloca %struct.AV1Decoder*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %i = alloca i32, align 4
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i8* @aom_memalign(i32 32, i32 404192)
  %1 = bitcast i8* %call to %struct.AV1Decoder*
  store volatile %struct.AV1Decoder* %1, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %2 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %tobool = icmp ne %struct.AV1Decoder* %2, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.AV1Decoder* null, %struct.AV1Decoder** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

if.end:                                           ; preds = %entry
  %3 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %4 = bitcast %struct.AV1Decoder* %3 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 32 %4, i8 0, i32 404192, i1 false)
  %5 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %6, i32 0, i32 1
  store volatile %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %7 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 1
  %jmp = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error, i32 0, i32 4
  %arraydecay = getelementptr inbounds [1 x %struct.__jmp_buf_tag], [1 x %struct.__jmp_buf_tag]* %jmp, i32 0, i32 0
  %call1 = call i32 @setjmp(%struct.__jmp_buf_tag* %arraydecay) #8
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %8 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 1
  %setjmp = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error4, i32 0, i32 3
  store i32 0, i32* %setjmp, align 8, !tbaa !6
  %9 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  call void @av1_decoder_remove(%struct.AV1Decoder* %9)
  store %struct.AV1Decoder* null, %struct.AV1Decoder** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %if.end
  %10 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 1
  %setjmp7 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error6, i32 0, i32 3
  store i32 1, i32* %setjmp7, align 8, !tbaa !6
  br label %do.body

do.body:                                          ; preds = %if.end5
  %call8 = call i8* @aom_memalign(i32 32, i32 21264)
  %11 = bitcast i8* %call8 to %struct.frame_contexts*
  %12 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %fc = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %12, i32 0, i32 38
  store %struct.frame_contexts* %11, %struct.frame_contexts** %fc, align 16, !tbaa !31
  %13 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %fc9 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 38
  %14 = load %struct.frame_contexts*, %struct.frame_contexts** %fc9, align 16, !tbaa !31
  %tobool10 = icmp ne %struct.frame_contexts* %14, null
  br i1 %tobool10, label %if.end13, label %if.then11

if.then11:                                        ; preds = %do.body
  %15 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error12 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error12, i32 2, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0))
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end13
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body14

do.body14:                                        ; preds = %do.end
  %call15 = call i8* @aom_memalign(i32 32, i32 21264)
  %16 = bitcast i8* %call15 to %struct.frame_contexts*
  %17 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %default_frame_context = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 39
  store %struct.frame_contexts* %16, %struct.frame_contexts** %default_frame_context, align 4, !tbaa !32
  %18 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %default_frame_context16 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 39
  %19 = load %struct.frame_contexts*, %struct.frame_contexts** %default_frame_context16, align 4, !tbaa !32
  %tobool17 = icmp ne %struct.frame_contexts* %19, null
  br i1 %tobool17, label %if.end20, label %if.then18

if.then18:                                        ; preds = %do.body14
  %20 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error19 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %20, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error19, i32 2, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end20

if.end20:                                         ; preds = %if.then18, %do.body14
  br label %do.cond21

do.cond21:                                        ; preds = %if.end20
  br label %do.end22

do.end22:                                         ; preds = %do.cond21
  %21 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %fc23 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 38
  %22 = load %struct.frame_contexts*, %struct.frame_contexts** %fc23, align 16, !tbaa !31
  %23 = bitcast %struct.frame_contexts* %22 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %23, i8 0, i32 21264, i1 false)
  %24 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %default_frame_context24 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %24, i32 0, i32 39
  %25 = load %struct.frame_contexts*, %struct.frame_contexts** %default_frame_context24, align 4, !tbaa !32
  %26 = bitcast %struct.frame_contexts* %25 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %26, i8 0, i32 21264, i1 false)
  %27 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %need_resync = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %27, i32 0, i32 21
  store i32 1, i32* %need_resync, align 4, !tbaa !33
  call void @aom_once(void ()* @initialize_dec)
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  store i32 0, i32* %i, align 4, !tbaa !53
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end22
  %29 = load i32, i32* %i, align 4, !tbaa !53
  %cmp = icmp slt i32 %29, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 6, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %31 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %31, i32 0, i32 17
  %32 = load i32, i32* %i, align 4, !tbaa !53
  %arrayidx = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %32
  store %struct.RefCntBuffer* null, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %33 = load i32, i32* %i, align 4, !tbaa !53
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !53
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %34 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %34, i32 0, i32 0
  %frame_number = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 4
  store i32 0, i32* %frame_number, align 4, !tbaa !54
  %35 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %decoding_first_frame = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %35, i32 0, i32 17
  store i32 1, i32* %decoding_first_frame, align 4, !tbaa !55
  %36 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %37 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %common25 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %37, i32 0, i32 1
  %buffer_pool = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common25, i32 0, i32 41
  store %struct.BufferPool* %36, %struct.BufferPool** %buffer_pool, align 4, !tbaa !56
  %38 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %38, i32 0, i32 37
  %bit_depth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 25
  store i32 8, i32* %bit_depth, align 8, !tbaa !57
  %39 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %39, i32 0, i32 22
  %free_mi = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 13
  store void (%struct.CommonModeInfoParams*)* @dec_free_mi, void (%struct.CommonModeInfoParams*)** %free_mi, align 4, !tbaa !58
  %40 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %mi_params26 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %40, i32 0, i32 22
  %setup_mi = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params26, i32 0, i32 14
  store void (%struct.CommonModeInfoParams*)* @dec_setup_mi, void (%struct.CommonModeInfoParams*)** %setup_mi, align 4, !tbaa !59
  %41 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %mi_params27 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %41, i32 0, i32 22
  %set_mb_mi = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params27, i32 0, i32 15
  store void (%struct.CommonModeInfoParams*, i32, i32)* @dec_set_mb_mi, void (%struct.CommonModeInfoParams*, i32, i32)** %set_mb_mi, align 4, !tbaa !60
  %42 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  call void @av1_loop_filter_init(%struct.AV1Common* %42)
  %43 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %quant_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %43, i32 0, i32 23
  %44 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %call28 = call i32 @av1_num_planes(%struct.AV1Common* %44)
  call void @av1_qm_init(%struct.CommonQuantParams* %quant_params, i32 %call28)
  call void bitcast (void (...)* @av1_loop_restoration_precal to void ()*)()
  %45 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %acct_enabled = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %45, i32 0, i32 27
  store i32 1, i32* %acct_enabled, align 4, !tbaa !61
  %46 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %46, i32 0, i32 28
  call void @aom_accounting_init(%struct.Accounting* %accounting)
  %47 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error29 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %47, i32 0, i32 1
  %setjmp30 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error29, i32 0, i32 3
  store i32 0, i32* %setjmp30, align 8, !tbaa !6
  %call31 = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  %init = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %call31, i32 0, i32 0
  %48 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %init, align 4, !tbaa !62
  %49 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %lf_worker = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %49, i32 0, i32 2
  call void %48(%struct.AVxWorker* %lf_worker)
  %50 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %lf_worker32 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %50, i32 0, i32 2
  %thread_name = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %lf_worker32, i32 0, i32 2
  store i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i32 0, i32 0), i8** %thread_name, align 8, !tbaa !64
  %51 = load volatile %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  store %struct.AV1Decoder* %51, %struct.AV1Decoder** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then3
  %52 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  br label %cleanup33

cleanup33:                                        ; preds = %cleanup, %if.then
  %53 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %54 = load %struct.AV1Decoder*, %struct.AV1Decoder** %retval, align 4
  ret %struct.AV1Decoder* %54
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @aom_memalign(i32, i32) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: returns_twice
declare i32 @setjmp(%struct.__jmp_buf_tag*) #4

; Function Attrs: nounwind
define hidden void @av1_decoder_remove(%struct.AV1Decoder* %pbi) #0 {
entry:
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %worker_idx = alloca i32, align 4
  %thread_data5 = alloca %struct.DecWorkerData*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %tile_data = alloca %struct.TileDataDec*, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.AV1Decoder* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %2 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %tile_list_outbuf = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %2, i32 0, i32 45
  %call = call i32 @aom_free_frame_buffer(%struct.yv12_buffer_config* %tile_list_outbuf)
  %call1 = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  %end = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %call1, i32 0, i32 5
  %3 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %end, align 4, !tbaa !65
  %4 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %lf_worker = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %4, i32 0, i32 2
  call void %3(%struct.AVxWorker* %lf_worker)
  %5 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %lf_worker2 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %5, i32 0, i32 2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %lf_worker2, i32 0, i32 4
  %6 = load i8*, i8** %data1, align 16, !tbaa !66
  call void @aom_free(i8* %6)
  %7 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %thread_data = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %7, i32 0, i32 8
  %8 = load %struct.DecWorkerData*, %struct.DecWorkerData** %thread_data, align 4, !tbaa !67
  %tobool3 = icmp ne %struct.DecWorkerData* %8, null
  br i1 %tobool3, label %if.then4, label %if.end9

if.then4:                                         ; preds = %if.end
  %9 = bitcast i32* %worker_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %worker_idx, align 4, !tbaa !53
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then4
  %10 = load i32, i32* %worker_idx, align 4, !tbaa !53
  %11 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %max_threads = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %11, i32 0, i32 19
  %12 = load i32, i32* %max_threads, align 4, !tbaa !68
  %sub = sub nsw i32 %12, 1
  %cmp = icmp slt i32 %10, %sub
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %worker_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = bitcast %struct.DecWorkerData** %thread_data5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %thread_data6 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %15, i32 0, i32 8
  %16 = load %struct.DecWorkerData*, %struct.DecWorkerData** %thread_data6, align 4, !tbaa !67
  %17 = load i32, i32* %worker_idx, align 4, !tbaa !53
  %add.ptr = getelementptr inbounds %struct.DecWorkerData, %struct.DecWorkerData* %16, i32 %17
  store %struct.DecWorkerData* %add.ptr, %struct.DecWorkerData** %thread_data5, align 4, !tbaa !2
  %18 = load %struct.DecWorkerData*, %struct.DecWorkerData** %thread_data5, align 4, !tbaa !2
  %td = getelementptr inbounds %struct.DecWorkerData, %struct.DecWorkerData* %18, i32 0, i32 0
  %19 = load %struct.ThreadData*, %struct.ThreadData** %td, align 4, !tbaa !69
  call void @av1_free_mc_tmp_buf(%struct.ThreadData* %19)
  %20 = load %struct.DecWorkerData*, %struct.DecWorkerData** %thread_data5, align 4, !tbaa !2
  %td7 = getelementptr inbounds %struct.DecWorkerData, %struct.DecWorkerData* %20, i32 0, i32 0
  %21 = load %struct.ThreadData*, %struct.ThreadData** %td7, align 4, !tbaa !69
  %22 = bitcast %struct.ThreadData* %21 to i8*
  call void @aom_free(i8* %22)
  %23 = bitcast %struct.DecWorkerData** %thread_data5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %24 = load i32, i32* %worker_idx, align 4, !tbaa !53
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %worker_idx, align 4, !tbaa !53
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %25 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %thread_data8 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %25, i32 0, i32 8
  %26 = load %struct.DecWorkerData*, %struct.DecWorkerData** %thread_data8, align 4, !tbaa !67
  %27 = bitcast %struct.DecWorkerData* %26 to i8*
  call void @aom_free(i8* %27)
  br label %if.end9

if.end9:                                          ; preds = %for.end, %if.end
  store i32 0, i32* %i, align 4, !tbaa !53
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %if.end9
  %28 = load i32, i32* %i, align 4, !tbaa !53
  %29 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_workers = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %29, i32 0, i32 7
  %30 = load i32, i32* %num_workers, align 8, !tbaa !71
  %cmp11 = icmp slt i32 %28, %30
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %31 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %32 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %tile_workers = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %32, i32 0, i32 6
  %33 = load %struct.AVxWorker*, %struct.AVxWorker** %tile_workers, align 4, !tbaa !72
  %34 = load i32, i32* %i, align 4, !tbaa !53
  %arrayidx = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %33, i32 %34
  store %struct.AVxWorker* %arrayidx, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %call13 = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  %end14 = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %call13, i32 0, i32 5
  %35 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %end14, align 4, !tbaa !65
  %36 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  call void %35(%struct.AVxWorker* %36)
  %37 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %38 = load i32, i32* %i, align 4, !tbaa !53
  %inc16 = add nsw i32 %38, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !53
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  store i32 0, i32* %i, align 4, !tbaa !53
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc23, %for.end17
  %39 = load i32, i32* %i, align 4, !tbaa !53
  %40 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %allocated_tiles = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %40, i32 0, i32 11
  %41 = load i32, i32* %allocated_tiles, align 4, !tbaa !73
  %cmp19 = icmp slt i32 %39, %41
  br i1 %cmp19, label %for.body20, label %for.end25

for.body20:                                       ; preds = %for.cond18
  %42 = bitcast %struct.TileDataDec** %tile_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  %43 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %tile_data21 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %43, i32 0, i32 10
  %44 = load %struct.TileDataDec*, %struct.TileDataDec** %tile_data21, align 32, !tbaa !74
  %45 = load i32, i32* %i, align 4, !tbaa !53
  %add.ptr22 = getelementptr inbounds %struct.TileDataDec, %struct.TileDataDec* %44, i32 %45
  store %struct.TileDataDec* %add.ptr22, %struct.TileDataDec** %tile_data, align 4, !tbaa !2
  %46 = load %struct.TileDataDec*, %struct.TileDataDec** %tile_data, align 4, !tbaa !2
  %dec_row_mt_sync = getelementptr inbounds %struct.TileDataDec, %struct.TileDataDec* %46, i32 0, i32 3
  call void @av1_dec_row_mt_dealloc(%struct.AV1DecRowMTSyncData* %dec_row_mt_sync)
  %47 = bitcast %struct.TileDataDec** %tile_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  br label %for.inc23

for.inc23:                                        ; preds = %for.body20
  %48 = load i32, i32* %i, align 4, !tbaa !53
  %inc24 = add nsw i32 %48, 1
  store i32 %inc24, i32* %i, align 4, !tbaa !53
  br label %for.cond18

for.end25:                                        ; preds = %for.cond18
  %49 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %tile_data26 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %49, i32 0, i32 10
  %50 = load %struct.TileDataDec*, %struct.TileDataDec** %tile_data26, align 32, !tbaa !74
  %51 = bitcast %struct.TileDataDec* %50 to i8*
  call void @aom_free(i8* %51)
  %52 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %tile_workers27 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %52, i32 0, i32 6
  %53 = load %struct.AVxWorker*, %struct.AVxWorker** %tile_workers27, align 4, !tbaa !72
  %54 = bitcast %struct.AVxWorker* %53 to i8*
  call void @aom_free(i8* %54)
  %55 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_workers28 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %55, i32 0, i32 7
  %56 = load i32, i32* %num_workers28, align 8, !tbaa !71
  %cmp29 = icmp sgt i32 %56, 0
  br i1 %cmp29, label %if.then30, label %if.end32

if.then30:                                        ; preds = %for.end25
  %57 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %lf_row_sync = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %57, i32 0, i32 3
  call void @av1_loop_filter_dealloc(%struct.AV1LfSyncData* %lf_row_sync)
  %58 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %lr_row_sync = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %58, i32 0, i32 4
  %59 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_workers31 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %59, i32 0, i32 7
  %60 = load i32, i32* %num_workers31, align 8, !tbaa !71
  call void @av1_loop_restoration_dealloc(%struct.AV1LrSyncData* %lr_row_sync, i32 %60)
  %61 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %tile_mt_info = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %61, i32 0, i32 13
  call void @av1_dealloc_dec_jobs(%struct.AV1DecTileMTData* %tile_mt_info)
  br label %if.end32

if.end32:                                         ; preds = %if.then30, %for.end25
  %62 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  call void @av1_dec_free_cb_buf(%struct.AV1Decoder* %62)
  %63 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %63, i32 0, i32 28
  call void @aom_accounting_clear(%struct.Accounting* %accounting)
  %64 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %td33 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %64, i32 0, i32 9
  call void @av1_free_mc_tmp_buf(%struct.ThreadData* %td33)
  %65 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %65, i32 0, i32 50
  %66 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata, align 8, !tbaa !75
  call void @aom_img_metadata_array_free(%struct.aom_metadata_array* %66)
  %67 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %68 = bitcast %struct.AV1Decoder* %67 to i8*
  call void @aom_free(i8* %68)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end32, %if.then
  %69 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #2

; Function Attrs: nounwind
define internal void @aom_once(void ()* %func) #0 {
entry:
  %func.addr = alloca void ()*, align 4
  store void ()* %func, void ()** %func.addr, align 4, !tbaa !2
  %0 = load i32, i32* @aom_once.done, align 4, !tbaa !53
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %1 = load void ()*, void ()** %func.addr, align 4, !tbaa !2
  call void %1()
  store i32 1, i32* @aom_once.done, align 4, !tbaa !53
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define internal void @initialize_dec() #0 {
entry:
  call void @av1_rtcd()
  call void @aom_dsp_rtcd()
  call void @aom_scale_rtcd()
  call void @av1_init_intra_predictors()
  call void bitcast (void (...)* @av1_init_wedge_masks to void ()*)()
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @dec_free_mi(%struct.CommonModeInfoParams* %mi_params) #0 {
entry:
  %mi_params.addr = alloca %struct.CommonModeInfoParams*, align 4
  store %struct.CommonModeInfoParams* %mi_params, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %0 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_alloc = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %0, i32 0, i32 5
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi_alloc, align 4, !tbaa !76
  %2 = bitcast %struct.MB_MODE_INFO* %1 to i8*
  call void @aom_free(i8* %2)
  %3 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_alloc1 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %3, i32 0, i32 5
  store %struct.MB_MODE_INFO* null, %struct.MB_MODE_INFO** %mi_alloc1, align 4, !tbaa !76
  %4 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_grid_base = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %4, i32 0, i32 9
  %5 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base, align 4, !tbaa !77
  %6 = bitcast %struct.MB_MODE_INFO** %5 to i8*
  call void @aom_free(i8* %6)
  %7 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_grid_base2 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %7, i32 0, i32 9
  store %struct.MB_MODE_INFO** null, %struct.MB_MODE_INFO*** %mi_grid_base2, align 4, !tbaa !77
  %8 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_alloc_size = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %8, i32 0, i32 6
  store i32 0, i32* %mi_alloc_size, align 4, !tbaa !78
  %9 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %tx_type_map = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %9, i32 0, i32 12
  %10 = load i8*, i8** %tx_type_map, align 4, !tbaa !79
  call void @aom_free(i8* %10)
  %11 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %tx_type_map3 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %11, i32 0, i32 12
  store i8* null, i8** %tx_type_map3, align 4, !tbaa !79
  ret void
}

; Function Attrs: nounwind
define internal void @dec_setup_mi(%struct.CommonModeInfoParams* %mi_params) #0 {
entry:
  %mi_params.addr = alloca %struct.CommonModeInfoParams*, align 4
  %mi_grid_size = alloca i32, align 4
  store %struct.CommonModeInfoParams* %mi_params, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %0 = bitcast i32* %mi_grid_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %1, i32 0, i32 11
  %2 = load i32, i32* %mi_stride, align 4, !tbaa !80
  %3 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %3, i32 0, i32 3
  %4 = load i32, i32* %mi_rows, align 4, !tbaa !81
  %call = call i32 @calc_mi_size(i32 %4)
  %mul = mul nsw i32 %2, %call
  store i32 %mul, i32* %mi_grid_size, align 4, !tbaa !53
  %5 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_grid_base = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %5, i32 0, i32 9
  %6 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base, align 4, !tbaa !77
  %7 = bitcast %struct.MB_MODE_INFO** %6 to i8*
  %8 = load i32, i32* %mi_grid_size, align 4, !tbaa !53
  %mul1 = mul i32 %8, 4
  call void @llvm.memset.p0i8.i32(i8* align 4 %7, i8 0, i32 %mul1, i1 false)
  %9 = bitcast i32* %mi_grid_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  ret void
}

; Function Attrs: nounwind
define internal void @dec_set_mb_mi(%struct.CommonModeInfoParams* %mi_params, i32 %width, i32 %height) #0 {
entry:
  %mi_params.addr = alloca %struct.CommonModeInfoParams*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %aligned_width = alloca i32, align 4
  %aligned_height = alloca i32, align 4
  store %struct.CommonModeInfoParams* %mi_params, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !53
  store i32 %height, i32* %height.addr, align 4, !tbaa !53
  %0 = bitcast i32* %aligned_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %width.addr, align 4, !tbaa !53
  %add = add nsw i32 %1, 7
  %and = and i32 %add, -8
  store i32 %and, i32* %aligned_width, align 4, !tbaa !53
  %2 = bitcast i32* %aligned_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i32, i32* %height.addr, align 4, !tbaa !53
  %add1 = add nsw i32 %3, 7
  %and2 = and i32 %add1, -8
  store i32 %and2, i32* %aligned_height, align 4, !tbaa !53
  %4 = load i32, i32* %aligned_width, align 4, !tbaa !53
  %shr = ashr i32 %4, 2
  %5 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %5, i32 0, i32 4
  store i32 %shr, i32* %mi_cols, align 4, !tbaa !82
  %6 = load i32, i32* %aligned_height, align 4, !tbaa !53
  %shr3 = ashr i32 %6, 2
  %7 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %7, i32 0, i32 3
  store i32 %shr3, i32* %mi_rows, align 4, !tbaa !81
  %8 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_cols4 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %8, i32 0, i32 4
  %9 = load i32, i32* %mi_cols4, align 4, !tbaa !82
  %call = call i32 @calc_mi_size(i32 %9)
  %10 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %10, i32 0, i32 11
  store i32 %call, i32* %mi_stride, align 4, !tbaa !80
  %11 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_cols5 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %11, i32 0, i32 4
  %12 = load i32, i32* %mi_cols5, align 4, !tbaa !82
  %add6 = add nsw i32 %12, 2
  %shr7 = ashr i32 %add6, 2
  %13 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mb_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %13, i32 0, i32 1
  store i32 %shr7, i32* %mb_cols, align 4, !tbaa !83
  %14 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_rows8 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %14, i32 0, i32 3
  %15 = load i32, i32* %mi_rows8, align 4, !tbaa !81
  %add9 = add nsw i32 %15, 2
  %shr10 = ashr i32 %add9, 2
  %16 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mb_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %16, i32 0, i32 0
  store i32 %shr10, i32* %mb_rows, align 4, !tbaa !84
  %17 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mb_rows11 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %17, i32 0, i32 0
  %18 = load i32, i32* %mb_rows11, align 4, !tbaa !84
  %19 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mb_cols12 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %19, i32 0, i32 1
  %20 = load i32, i32* %mb_cols12, align 4, !tbaa !83
  %mul = mul nsw i32 %18, %20
  %21 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %MBs = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %21, i32 0, i32 2
  store i32 %mul, i32* %MBs, align 4, !tbaa !85
  %22 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_alloc_bsize = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %22, i32 0, i32 8
  store i8 0, i8* %mi_alloc_bsize, align 4, !tbaa !86
  %23 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_stride13 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %23, i32 0, i32 11
  %24 = load i32, i32* %mi_stride13, align 4, !tbaa !80
  %25 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_alloc_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %25, i32 0, i32 7
  store i32 %24, i32* %mi_alloc_stride, align 4, !tbaa !87
  %26 = bitcast i32* %aligned_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast i32* %aligned_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  ret void
}

declare void @av1_loop_filter_init(%struct.AV1Common*) #2

declare void @av1_qm_init(%struct.CommonQuantParams*, i32) #2

; Function Attrs: inlinehint nounwind
define internal i32 @av1_num_planes(%struct.AV1Common* %cm) #5 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %1 = load i8, i8* %monochrome, align 1, !tbaa !88
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 3
  ret i32 %cond
}

declare void @av1_loop_restoration_precal(...) #6

declare void @aom_accounting_init(%struct.Accounting*) #2

declare %struct.AVxWorkerInterface* @aom_get_worker_interface() #2

; Function Attrs: nounwind
define hidden void @av1_dealloc_dec_jobs(%struct.AV1DecTileMTData* %tile_mt_info) #0 {
entry:
  %tile_mt_info.addr = alloca %struct.AV1DecTileMTData*, align 4
  store %struct.AV1DecTileMTData* %tile_mt_info, %struct.AV1DecTileMTData** %tile_mt_info.addr, align 4, !tbaa !2
  %0 = load %struct.AV1DecTileMTData*, %struct.AV1DecTileMTData** %tile_mt_info.addr, align 4, !tbaa !2
  %cmp = icmp ne %struct.AV1DecTileMTData* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.AV1DecTileMTData*, %struct.AV1DecTileMTData** %tile_mt_info.addr, align 4, !tbaa !2
  %job_queue = getelementptr inbounds %struct.AV1DecTileMTData, %struct.AV1DecTileMTData* %1, i32 0, i32 0
  %2 = load %struct.TileJobsDec*, %struct.TileJobsDec** %job_queue, align 4, !tbaa !89
  %3 = bitcast %struct.TileJobsDec* %2 to i8*
  call void @aom_free(i8* %3)
  %4 = load %struct.AV1DecTileMTData*, %struct.AV1DecTileMTData** %tile_mt_info.addr, align 4, !tbaa !2
  %5 = bitcast %struct.AV1DecTileMTData* %4 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %5, i8 0, i32 20, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare void @aom_free(i8*) #2

; Function Attrs: nounwind
define hidden void @av1_dec_free_cb_buf(%struct.AV1Decoder* %pbi) #0 {
entry:
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %cb_buffer_base = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %0, i32 0, i32 46
  %1 = load %struct.CB_BUFFER*, %struct.CB_BUFFER** %cb_buffer_base, align 4, !tbaa !90
  %2 = bitcast %struct.CB_BUFFER* %1 to i8*
  call void @aom_free(i8* %2)
  %3 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %cb_buffer_base1 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %3, i32 0, i32 46
  store %struct.CB_BUFFER* null, %struct.CB_BUFFER** %cb_buffer_base1, align 4, !tbaa !90
  %4 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %cb_buffer_alloc_size = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %4, i32 0, i32 47
  store i32 0, i32* %cb_buffer_alloc_size, align 8, !tbaa !91
  ret void
}

declare i32 @aom_free_frame_buffer(%struct.yv12_buffer_config*) #2

declare void @av1_free_mc_tmp_buf(%struct.ThreadData*) #2

declare void @av1_dec_row_mt_dealloc(%struct.AV1DecRowMTSyncData*) #2

declare void @av1_loop_filter_dealloc(%struct.AV1LfSyncData*) #2

declare void @av1_loop_restoration_dealloc(%struct.AV1LrSyncData*, i32) #2

declare void @aom_accounting_clear(%struct.Accounting*) #2

declare void @aom_img_metadata_array_free(%struct.aom_metadata_array*) #2

; Function Attrs: nounwind
define hidden void @av1_visit_palette(%struct.AV1Decoder* %pbi, %struct.macroblockd* %xd, %struct.aom_reader* %r, void (%struct.macroblockd*, i32, %struct.aom_reader*)* %visit) #0 {
entry:
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %visit.addr = alloca void (%struct.macroblockd*, i32, %struct.aom_reader*)*, align 4
  %plane = alloca i32, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store void (%struct.macroblockd*, i32, %struct.aom_reader*)* %visit, void (%struct.macroblockd*, i32, %struct.aom_reader*)** %visit.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 6
  %1 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !92
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %1, i32 0
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %2)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end14, label %if.then

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store i32 0, i32* %plane, align 4, !tbaa !53
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %4 = load i32, i32* %plane, align 4, !tbaa !53
  %5 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %5, i32 0, i32 1
  %call1 = call i32 @av1_num_planes(%struct.AV1Common* %common)
  %cmp = icmp slt i32 2, %call1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.cond
  br label %cond.end

cond.false:                                       ; preds = %for.cond
  %6 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %common2 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %6, i32 0, i32 1
  %call3 = call i32 @av1_num_planes(%struct.AV1Common* %common2)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 2, %cond.true ], [ %call3, %cond.false ]
  %cmp4 = icmp slt i32 %4, %cond
  br i1 %cmp4, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %cond.end
  %7 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %cond.end
  %8 = load i32, i32* %plane, align 4, !tbaa !53
  %cmp5 = icmp eq i32 %8, 0
  br i1 %cmp5, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %is_chroma_ref = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %9, i32 0, i32 3
  %10 = load i8, i8* %is_chroma_ref, align 4, !tbaa !93, !range !94
  %tobool6 = trunc i8 %10 to i1
  br i1 %tobool6, label %if.then7, label %if.else

if.then7:                                         ; preds = %lor.lhs.false, %for.body
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %11, i32 0, i32 6
  %12 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi8, align 4, !tbaa !92
  %arrayidx9 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %12, i32 0
  %13 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx9, align 4, !tbaa !2
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %13, i32 0, i32 5
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 1
  %14 = load i32, i32* %plane, align 4, !tbaa !53
  %arrayidx10 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 %14
  %15 = load i8, i8* %arrayidx10, align 1, !tbaa !95
  %tobool11 = icmp ne i8 %15, 0
  br i1 %tobool11, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.then7
  %16 = load void (%struct.macroblockd*, i32, %struct.aom_reader*)*, void (%struct.macroblockd*, i32, %struct.aom_reader*)** %visit.addr, align 4, !tbaa !2
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %18 = load i32, i32* %plane, align 4, !tbaa !53
  %19 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  call void %16(%struct.macroblockd* %17, i32 %18, %struct.aom_reader* %19)
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.then7
  br label %if.end13

if.else:                                          ; preds = %lor.lhs.false
  br label %if.end13

if.end13:                                         ; preds = %if.else, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %20 = load i32, i32* %plane, align 4, !tbaa !53
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !53
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end14

if.end14:                                         ; preds = %for.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_block(%struct.MB_MODE_INFO* %mbmi) #5 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !95
  %conv = sext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  ret i32 %lor.ext
}

; Function Attrs: nounwind
define hidden i32 @av1_copy_reference_dec(%struct.AV1Decoder* %pbi, i32 %idx, %struct.yv12_buffer_config* %sd) #0 {
entry:
  %retval = alloca i32, align 4
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %idx.addr = alloca i32, align 4
  %sd.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %num_planes = alloca i32, align 4
  %cfg = alloca %struct.yv12_buffer_config*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  store i32 %idx, i32* %idx.addr, align 4, !tbaa !53
  store %struct.yv12_buffer_config* %sd, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %1, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %2 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %3)
  store i32 %call, i32* %num_planes, align 4, !tbaa !53
  %4 = bitcast %struct.yv12_buffer_config** %cfg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %6 = load i32, i32* %idx.addr, align 4, !tbaa !53
  %call1 = call %struct.yv12_buffer_config* @get_ref_frame(%struct.AV1Common* %5, i32 %6)
  store %struct.yv12_buffer_config* %call1, %struct.yv12_buffer_config** %cfg, align 4, !tbaa !2
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %cfg, align 4, !tbaa !2
  %cmp = icmp eq %struct.yv12_buffer_config* %7, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 1, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0))
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %9 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %cfg, align 4, !tbaa !2
  %10 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %call2 = call i32 @equal_dimensions(%struct.yv12_buffer_config* %9, %struct.yv12_buffer_config* %10)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.else, label %if.then3

if.then3:                                         ; preds = %if.end
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error4, i32 1, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end5

if.else:                                          ; preds = %if.end
  %12 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %cfg, align 4, !tbaa !2
  %13 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %14 = load i32, i32* %num_planes, align 4, !tbaa !53
  call void @aom_yv12_copy_frame_c(%struct.yv12_buffer_config* %12, %struct.yv12_buffer_config* %13, i32 %14)
  br label %if.end5

if.end5:                                          ; preds = %if.else, %if.then3
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 1
  %error_code = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error6, i32 0, i32 0
  %16 = load i32, i32* %error_code, align 8, !tbaa !96
  store i32 %16, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end5, %if.then
  %17 = bitcast %struct.yv12_buffer_config** %cfg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %18 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  %19 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

; Function Attrs: inlinehint nounwind
define internal %struct.yv12_buffer_config* @get_ref_frame(%struct.AV1Common* %cm, i32 %index) #5 {
entry:
  %retval = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %index.addr = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !53
  %0 = load i32, i32* %index.addr, align 4, !tbaa !53
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4, !tbaa !53
  %cmp1 = icmp sge i32 %1, 8
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store %struct.yv12_buffer_config* null, %struct.yv12_buffer_config** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 17
  %3 = load i32, i32* %index.addr, align 4, !tbaa !53
  %arrayidx = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %3
  %4 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  %cmp2 = icmp eq %struct.RefCntBuffer* %4, null
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store %struct.yv12_buffer_config* null, %struct.yv12_buffer_config** %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_map5 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 17
  %6 = load i32, i32* %index.addr, align 4, !tbaa !53
  %arrayidx6 = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map5, i32 0, i32 %6
  %7 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx6, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %7, i32 0, i32 17
  store %struct.yv12_buffer_config* %buf, %struct.yv12_buffer_config** %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3, %if.then
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %retval, align 4
  ret %struct.yv12_buffer_config* %8
}

; Function Attrs: nounwind
define internal i32 @equal_dimensions(%struct.yv12_buffer_config* %a, %struct.yv12_buffer_config* %b) #0 {
entry:
  %a.addr = alloca %struct.yv12_buffer_config*, align 4
  %b.addr = alloca %struct.yv12_buffer_config*, align 4
  store %struct.yv12_buffer_config* %a, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %b, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %1 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %0, i32 0, i32 1
  %2 = bitcast %union.anon.0* %1 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %2, i32 0, i32 0
  %3 = load i32, i32* %y_height, align 4, !tbaa !95
  %4 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %5 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %4, i32 0, i32 1
  %6 = bitcast %union.anon.0* %5 to %struct.anon.1*
  %y_height1 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %6, i32 0, i32 0
  %7 = load i32, i32* %y_height1, align 4, !tbaa !95
  %cmp = icmp eq i32 %3, %7
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %9 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %8, i32 0, i32 0
  %10 = bitcast %union.anon* %9 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %10, i32 0, i32 0
  %11 = load i32, i32* %y_width, align 4, !tbaa !95
  %12 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %13 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %12, i32 0, i32 0
  %14 = bitcast %union.anon* %13 to %struct.anon*
  %y_width2 = getelementptr inbounds %struct.anon, %struct.anon* %14, i32 0, i32 0
  %15 = load i32, i32* %y_width2, align 4, !tbaa !95
  %cmp3 = icmp eq i32 %11, %15
  br i1 %cmp3, label %land.lhs.true4, label %land.end

land.lhs.true4:                                   ; preds = %land.lhs.true
  %16 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %17 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %16, i32 0, i32 1
  %18 = bitcast %union.anon.0* %17 to %struct.anon.1*
  %uv_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %18, i32 0, i32 1
  %19 = load i32, i32* %uv_height, align 4, !tbaa !95
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %21 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %20, i32 0, i32 1
  %22 = bitcast %union.anon.0* %21 to %struct.anon.1*
  %uv_height5 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %22, i32 0, i32 1
  %23 = load i32, i32* %uv_height5, align 4, !tbaa !95
  %cmp6 = icmp eq i32 %19, %23
  br i1 %cmp6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true4
  %24 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %25 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %24, i32 0, i32 0
  %26 = bitcast %union.anon* %25 to %struct.anon*
  %uv_width = getelementptr inbounds %struct.anon, %struct.anon* %26, i32 0, i32 1
  %27 = load i32, i32* %uv_width, align 4, !tbaa !95
  %28 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %29 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %28, i32 0, i32 0
  %30 = bitcast %union.anon* %29 to %struct.anon*
  %uv_width7 = getelementptr inbounds %struct.anon, %struct.anon* %30, i32 0, i32 1
  %31 = load i32, i32* %uv_width7, align 4, !tbaa !95
  %cmp8 = icmp eq i32 %27, %31
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true4, %land.lhs.true, %entry
  %32 = phi i1 [ false, %land.lhs.true4 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp8, %land.rhs ]
  %land.ext = zext i1 %32 to i32
  ret i32 %land.ext
}

declare void @aom_yv12_copy_frame_c(%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32) #2

; Function Attrs: nounwind
define hidden i32 @av1_set_reference_dec(%struct.AV1Common* %cm, i32 %idx, i32 %use_external_ref, %struct.yv12_buffer_config* %sd) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %idx.addr = alloca i32, align 4
  %use_external_ref.addr = alloca i32, align 4
  %sd.addr = alloca %struct.yv12_buffer_config*, align 4
  %num_planes = alloca i32, align 4
  %ref_buf = alloca %struct.yv12_buffer_config*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %idx, i32* %idx.addr, align 4, !tbaa !53
  store i32 %use_external_ref, i32* %use_external_ref.addr, align 4, !tbaa !53
  store %struct.yv12_buffer_config* %sd, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !53
  %2 = bitcast %struct.yv12_buffer_config** %ref_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store %struct.yv12_buffer_config* null, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %4 = load i32, i32* %idx.addr, align 4, !tbaa !53
  %call1 = call %struct.yv12_buffer_config* @get_ref_frame(%struct.AV1Common* %3, i32 %4)
  store %struct.yv12_buffer_config* %call1, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %cmp = icmp eq %struct.yv12_buffer_config* %5, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 1, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0))
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %7 = load i32, i32* %use_external_ref.addr, align 4, !tbaa !53
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.else8, label %if.then2

if.then2:                                         ; preds = %if.end
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %9 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %call3 = call i32 @equal_dimensions(%struct.yv12_buffer_config* %8, %struct.yv12_buffer_config* %9)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.else, label %if.then5

if.then5:                                         ; preds = %if.then2
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error6, i32 1, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end7

if.else:                                          ; preds = %if.then2
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %12 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %13 = load i32, i32* %num_planes, align 4, !tbaa !53
  call void @aom_yv12_copy_frame_c(%struct.yv12_buffer_config* %11, %struct.yv12_buffer_config* %12, i32 %13)
  br label %if.end7

if.end7:                                          ; preds = %if.else, %if.then5
  br label %if.end25

if.else8:                                         ; preds = %if.end
  %14 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %15 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %call9 = call i32 @equal_dimensions_and_border(%struct.yv12_buffer_config* %14, %struct.yv12_buffer_config* %15)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.else13, label %if.then11

if.then11:                                        ; preds = %if.else8
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error12 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %16, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error12, i32 1, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end24

if.else13:                                        ; preds = %if.else8
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %18 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 5
  %19 = bitcast %union.anon.8* %18 to %struct.anon.9*
  %y_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %19, i32 0, i32 0
  %20 = load i8*, i8** %y_buffer, align 4, !tbaa !95
  %21 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %store_buf_adr = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %21, i32 0, i32 7
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %store_buf_adr, i32 0, i32 0
  store i8* %20, i8** %arrayidx, align 4, !tbaa !2
  %22 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %23 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %22, i32 0, i32 5
  %24 = bitcast %union.anon.8* %23 to %struct.anon.9*
  %u_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %24, i32 0, i32 1
  %25 = load i8*, i8** %u_buffer, align 4, !tbaa !95
  %26 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %store_buf_adr14 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %26, i32 0, i32 7
  %arrayidx15 = getelementptr inbounds [3 x i8*], [3 x i8*]* %store_buf_adr14, i32 0, i32 1
  store i8* %25, i8** %arrayidx15, align 4, !tbaa !2
  %27 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %28 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %27, i32 0, i32 5
  %29 = bitcast %union.anon.8* %28 to %struct.anon.9*
  %v_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %29, i32 0, i32 2
  %30 = load i8*, i8** %v_buffer, align 4, !tbaa !95
  %31 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %store_buf_adr16 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %31, i32 0, i32 7
  %arrayidx17 = getelementptr inbounds [3 x i8*], [3 x i8*]* %store_buf_adr16, i32 0, i32 2
  store i8* %30, i8** %arrayidx17, align 4, !tbaa !2
  %32 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %33 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %32, i32 0, i32 5
  %34 = bitcast %union.anon.8* %33 to %struct.anon.9*
  %y_buffer18 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %34, i32 0, i32 0
  %35 = load i8*, i8** %y_buffer18, align 4, !tbaa !95
  %36 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %37 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %36, i32 0, i32 5
  %38 = bitcast %union.anon.8* %37 to %struct.anon.9*
  %y_buffer19 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %38, i32 0, i32 0
  store i8* %35, i8** %y_buffer19, align 4, !tbaa !95
  %39 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %40 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %39, i32 0, i32 5
  %41 = bitcast %union.anon.8* %40 to %struct.anon.9*
  %u_buffer20 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %41, i32 0, i32 1
  %42 = load i8*, i8** %u_buffer20, align 4, !tbaa !95
  %43 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %44 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %43, i32 0, i32 5
  %45 = bitcast %union.anon.8* %44 to %struct.anon.9*
  %u_buffer21 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %45, i32 0, i32 1
  store i8* %42, i8** %u_buffer21, align 4, !tbaa !95
  %46 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %47 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %46, i32 0, i32 5
  %48 = bitcast %union.anon.8* %47 to %struct.anon.9*
  %v_buffer22 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %48, i32 0, i32 2
  %49 = load i8*, i8** %v_buffer22, align 4, !tbaa !95
  %50 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %51 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %50, i32 0, i32 5
  %52 = bitcast %union.anon.8* %51 to %struct.anon.9*
  %v_buffer23 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %52, i32 0, i32 2
  store i8* %49, i8** %v_buffer23, align 4, !tbaa !95
  %53 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ref_buf, align 4, !tbaa !2
  %use_external_reference_buffers = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %53, i32 0, i32 6
  store i32 1, i32* %use_external_reference_buffers, align 4, !tbaa !97
  br label %if.end24

if.end24:                                         ; preds = %if.else13, %if.then11
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.end7
  %54 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error26 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %54, i32 0, i32 1
  %error_code = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error26, i32 0, i32 0
  %55 = load i32, i32* %error_code, align 8, !tbaa !96
  store i32 %55, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end25, %if.then
  %56 = bitcast %struct.yv12_buffer_config** %ref_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #7
  %57 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %58 = load i32, i32* %retval, align 4
  ret i32 %58
}

; Function Attrs: nounwind
define internal i32 @equal_dimensions_and_border(%struct.yv12_buffer_config* %a, %struct.yv12_buffer_config* %b) #0 {
entry:
  %a.addr = alloca %struct.yv12_buffer_config*, align 4
  %b.addr = alloca %struct.yv12_buffer_config*, align 4
  store %struct.yv12_buffer_config* %a, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %b, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %1 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %0, i32 0, i32 1
  %2 = bitcast %union.anon.0* %1 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %2, i32 0, i32 0
  %3 = load i32, i32* %y_height, align 4, !tbaa !95
  %4 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %5 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %4, i32 0, i32 1
  %6 = bitcast %union.anon.0* %5 to %struct.anon.1*
  %y_height1 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %6, i32 0, i32 0
  %7 = load i32, i32* %y_height1, align 4, !tbaa !95
  %cmp = icmp eq i32 %3, %7
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %9 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %8, i32 0, i32 0
  %10 = bitcast %union.anon* %9 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %10, i32 0, i32 0
  %11 = load i32, i32* %y_width, align 4, !tbaa !95
  %12 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %13 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %12, i32 0, i32 0
  %14 = bitcast %union.anon* %13 to %struct.anon*
  %y_width2 = getelementptr inbounds %struct.anon, %struct.anon* %14, i32 0, i32 0
  %15 = load i32, i32* %y_width2, align 4, !tbaa !95
  %cmp3 = icmp eq i32 %11, %15
  br i1 %cmp3, label %land.lhs.true4, label %land.end

land.lhs.true4:                                   ; preds = %land.lhs.true
  %16 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %17 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %16, i32 0, i32 1
  %18 = bitcast %union.anon.0* %17 to %struct.anon.1*
  %uv_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %18, i32 0, i32 1
  %19 = load i32, i32* %uv_height, align 4, !tbaa !95
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %21 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %20, i32 0, i32 1
  %22 = bitcast %union.anon.0* %21 to %struct.anon.1*
  %uv_height5 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %22, i32 0, i32 1
  %23 = load i32, i32* %uv_height5, align 4, !tbaa !95
  %cmp6 = icmp eq i32 %19, %23
  br i1 %cmp6, label %land.lhs.true7, label %land.end

land.lhs.true7:                                   ; preds = %land.lhs.true4
  %24 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %25 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %24, i32 0, i32 0
  %26 = bitcast %union.anon* %25 to %struct.anon*
  %uv_width = getelementptr inbounds %struct.anon, %struct.anon* %26, i32 0, i32 1
  %27 = load i32, i32* %uv_width, align 4, !tbaa !95
  %28 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %29 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %28, i32 0, i32 0
  %30 = bitcast %union.anon* %29 to %struct.anon*
  %uv_width8 = getelementptr inbounds %struct.anon, %struct.anon* %30, i32 0, i32 1
  %31 = load i32, i32* %uv_width8, align 4, !tbaa !95
  %cmp9 = icmp eq i32 %27, %31
  br i1 %cmp9, label %land.lhs.true10, label %land.end

land.lhs.true10:                                  ; preds = %land.lhs.true7
  %32 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %33 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %32, i32 0, i32 4
  %34 = bitcast %union.anon.6* %33 to %struct.anon.7*
  %y_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %34, i32 0, i32 0
  %35 = load i32, i32* %y_stride, align 4, !tbaa !95
  %36 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %37 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %36, i32 0, i32 4
  %38 = bitcast %union.anon.6* %37 to %struct.anon.7*
  %y_stride11 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %38, i32 0, i32 0
  %39 = load i32, i32* %y_stride11, align 4, !tbaa !95
  %cmp12 = icmp eq i32 %35, %39
  br i1 %cmp12, label %land.lhs.true13, label %land.end

land.lhs.true13:                                  ; preds = %land.lhs.true10
  %40 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %41 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %40, i32 0, i32 4
  %42 = bitcast %union.anon.6* %41 to %struct.anon.7*
  %uv_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %42, i32 0, i32 1
  %43 = load i32, i32* %uv_stride, align 4, !tbaa !95
  %44 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %45 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %44, i32 0, i32 4
  %46 = bitcast %union.anon.6* %45 to %struct.anon.7*
  %uv_stride14 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %46, i32 0, i32 1
  %47 = load i32, i32* %uv_stride14, align 4, !tbaa !95
  %cmp15 = icmp eq i32 %43, %47
  br i1 %cmp15, label %land.lhs.true16, label %land.end

land.lhs.true16:                                  ; preds = %land.lhs.true13
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %border = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 12
  %49 = load i32, i32* %border, align 4, !tbaa !98
  %50 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %border17 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %50, i32 0, i32 12
  %51 = load i32, i32* %border17, align 4, !tbaa !98
  %cmp18 = icmp eq i32 %49, %51
  br i1 %cmp18, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true16
  %52 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %a.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %52, i32 0, i32 26
  %53 = load i32, i32* %flags, align 4, !tbaa !99
  %and = and i32 %53, 8
  %54 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %b.addr, align 4, !tbaa !2
  %flags19 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %54, i32 0, i32 26
  %55 = load i32, i32* %flags19, align 4, !tbaa !99
  %and20 = and i32 %55, 8
  %cmp21 = icmp eq i32 %and, %and20
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true16, %land.lhs.true13, %land.lhs.true10, %land.lhs.true7, %land.lhs.true4, %land.lhs.true, %entry
  %56 = phi i1 [ false, %land.lhs.true16 ], [ false, %land.lhs.true13 ], [ false, %land.lhs.true10 ], [ false, %land.lhs.true7 ], [ false, %land.lhs.true4 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp21, %land.rhs ]
  %land.ext = zext i1 %56 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define hidden i32 @av1_copy_new_frame_dec(%struct.AV1Common* %cm, %struct.yv12_buffer_config* %new_frame, %struct.yv12_buffer_config* %sd) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %new_frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %sd.addr = alloca %struct.yv12_buffer_config*, align 4
  %num_planes = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %new_frame, %struct.yv12_buffer_config** %new_frame.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %sd, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !53
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %new_frame.addr, align 4, !tbaa !2
  %3 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %call1 = call i32 @equal_dimensions_and_border(%struct.yv12_buffer_config* %2, %struct.yv12_buffer_config* %3)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 1, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %new_frame.addr, align 4, !tbaa !2
  %6 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd.addr, align 4, !tbaa !2
  %7 = load i32, i32* %num_planes, align 4, !tbaa !53
  call void @aom_yv12_copy_frame_c(%struct.yv12_buffer_config* %5, %struct.yv12_buffer_config* %6, i32 %7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 1
  %error_code = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error2, i32 0, i32 0
  %9 = load i32, i32* %error_code, align 8, !tbaa !96
  %10 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  ret i32 %9
}

; Function Attrs: nounwind
define hidden i32 @av1_receive_compressed_data(%struct.AV1Decoder* %pbi, i32 %size, i8** %psource) #0 {
entry:
  %retval = alloca i32, align 4
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %size.addr = alloca i32, align 4
  %psource.addr = alloca i8**, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %source = alloca i8*, align 4
  %ref_buf = alloca %struct.RefCntBuffer*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %winterface = alloca %struct.AVxWorkerInterface*, align 4
  %i = alloca i32, align 4
  %frame_decoded = alloca i32, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !100
  store i8** %psource, i8*** %psource.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %1, i32 0, i32 1
  store volatile %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %2 = bitcast i8** %source to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i8**, i8*** %psource.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %3, align 4, !tbaa !2
  store i8* %4, i8** %source, align 4, !tbaa !2
  %5 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 1
  %error_code = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error, i32 0, i32 0
  store i32 0, i32* %error_code, align 8, !tbaa !96
  %6 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 1
  %has_detail = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error1, i32 0, i32 1
  store i32 0, i32* %has_detail, align 4, !tbaa !101
  %7 = load i32, i32* %size.addr, align 4, !tbaa !100
  %cmp = icmp eq i32 %7, 0
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %8 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %call = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %9, i8 signext 1)
  store %struct.RefCntBuffer* %call, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %10 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %cmp2 = icmp ne %struct.RefCntBuffer* %10, null
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %11 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %11, i32 0, i32 17
  %corrupted = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf, i32 0, i32 25
  store i32 1, i32* %corrupted, align 4, !tbaa !102
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %12 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  %13 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %call5 = call %struct.RefCntBuffer* @assign_cur_frame_new_fb(%struct.AV1Common* %13)
  %cmp6 = icmp eq %struct.RefCntBuffer* %call5, null
  br i1 %cmp6, label %if.then7, label %if.end10

if.then7:                                         ; preds = %if.end4
  %14 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 1
  %error_code9 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error8, i32 0, i32 0
  store i32 2, i32* %error_code9, align 8, !tbaa !96
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup60

if.end10:                                         ; preds = %if.end4
  %15 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error11 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 1
  %jmp = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error11, i32 0, i32 4
  %arraydecay = getelementptr inbounds [1 x %struct.__jmp_buf_tag], [1 x %struct.__jmp_buf_tag]* %jmp, i32 0, i32 0
  %call12 = call i32 @setjmp(%struct.__jmp_buf_tag* %arraydecay) #8
  %tobool = icmp ne i32 %call12, 0
  br i1 %tobool, label %if.then13, label %if.end20

if.then13:                                        ; preds = %if.end10
  %16 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %call14 = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  store %struct.AVxWorkerInterface* %call14, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error15 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 1
  %setjmp = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error15, i32 0, i32 3
  store i32 0, i32* %setjmp, align 8, !tbaa !6
  %19 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %19, i32 0, i32 2
  %20 = load i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)** %sync, align 4, !tbaa !108
  %21 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %lf_worker = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %21, i32 0, i32 2
  %call16 = call i32 %20(%struct.AVxWorker* %lf_worker)
  store i32 0, i32* %i, align 4, !tbaa !53
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then13
  %22 = load i32, i32* %i, align 4, !tbaa !53
  %23 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_workers = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %23, i32 0, i32 7
  %24 = load i32, i32* %num_workers, align 8, !tbaa !71
  %cmp17 = icmp slt i32 %22, %24
  br i1 %cmp17, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %25 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %sync18 = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %25, i32 0, i32 2
  %26 = load i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)** %sync18, align 4, !tbaa !108
  %27 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %tile_workers = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %27, i32 0, i32 6
  %28 = load %struct.AVxWorker*, %struct.AVxWorker** %tile_workers, align 4, !tbaa !72
  %29 = load i32, i32* %i, align 4, !tbaa !53
  %arrayidx = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %28, i32 %29
  %call19 = call i32 %26(%struct.AVxWorker* %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !53
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !53
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %31 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  call void @release_current_frame(%struct.AV1Decoder* %31)
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %32 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  br label %cleanup60

if.end20:                                         ; preds = %if.end10
  %34 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error21 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %34, i32 0, i32 1
  %setjmp22 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error21, i32 0, i32 3
  store i32 1, i32* %setjmp22, align 8, !tbaa !6
  %35 = bitcast i32* %frame_decoded to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  %36 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %37 = load i8*, i8** %source, align 4, !tbaa !2
  %38 = load i8*, i8** %source, align 4, !tbaa !2
  %39 = load i32, i32* %size.addr, align 4, !tbaa !100
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 %39
  %40 = load i8**, i8*** %psource.addr, align 4, !tbaa !2
  %call23 = call i32 @aom_decode_frame_from_obus(%struct.AV1Decoder* %36, i8* %37, i8* %add.ptr, i8** %40)
  store i32 %call23, i32* %frame_decoded, align 4, !tbaa !53
  %41 = load i32, i32* %frame_decoded, align 4, !tbaa !53
  %cmp24 = icmp slt i32 %41, 0
  br i1 %cmp24, label %if.then25, label %if.end28

if.then25:                                        ; preds = %if.end20
  %42 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  call void @release_current_frame(%struct.AV1Decoder* %42)
  %43 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error26 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %43, i32 0, i32 1
  %setjmp27 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error26, i32 0, i32 3
  store i32 0, i32* %setjmp27, align 8, !tbaa !6
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end28:                                         ; preds = %if.end20
  %44 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %45 = load i32, i32* %frame_decoded, align 4, !tbaa !53
  call void @update_frame_buffers(%struct.AV1Decoder* %44, i32 %45)
  %46 = load i32, i32* %frame_decoded, align 4, !tbaa !53
  %tobool29 = icmp ne i32 %46, 0
  br i1 %tobool29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.end28
  %47 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %decoding_first_frame = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %47, i32 0, i32 17
  store i32 0, i32* %decoding_first_frame, align 4, !tbaa !55
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %if.end28
  %48 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error32 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %48, i32 0, i32 1
  %error_code33 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error32, i32 0, i32 0
  %49 = load i32, i32* %error_code33, align 8, !tbaa !96
  %cmp34 = icmp ne i32 %49, 0
  br i1 %cmp34, label %if.then35, label %if.end38

if.then35:                                        ; preds = %if.end31
  %50 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error36 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %50, i32 0, i32 1
  %setjmp37 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error36, i32 0, i32 3
  store i32 0, i32* %setjmp37, align 8, !tbaa !6
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end38:                                         ; preds = %if.end31
  %51 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %show_existing_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %51, i32 0, i32 20
  %52 = load i32, i32* %show_existing_frame, align 4, !tbaa !109
  %tobool39 = icmp ne i32 %52, 0
  br i1 %tobool39, label %if.end57, label %if.then40

if.then40:                                        ; preds = %if.end38
  %53 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %53, i32 0, i32 24
  %enabled = getelementptr inbounds %struct.segmentation, %struct.segmentation* %seg, i32 0, i32 0
  %54 = load i8, i8* %enabled, align 4, !tbaa !110
  %tobool41 = icmp ne i8 %54, 0
  br i1 %tobool41, label %if.then42, label %if.end56

if.then42:                                        ; preds = %if.then40
  %55 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %prev_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %55, i32 0, i32 12
  %56 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %prev_frame, align 4, !tbaa !111
  %tobool43 = icmp ne %struct.RefCntBuffer* %56, null
  br i1 %tobool43, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then42
  %57 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %57, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %58 = load i32, i32* %mi_rows, align 4, !tbaa !112
  %59 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %prev_frame44 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %59, i32 0, i32 12
  %60 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %prev_frame44, align 4, !tbaa !111
  %mi_rows45 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %60, i32 0, i32 8
  %61 = load i32, i32* %mi_rows45, align 4, !tbaa !113
  %cmp46 = icmp eq i32 %58, %61
  br i1 %cmp46, label %land.lhs.true47, label %if.else

land.lhs.true47:                                  ; preds = %land.lhs.true
  %62 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %mi_params48 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %62, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params48, i32 0, i32 4
  %63 = load i32, i32* %mi_cols, align 4, !tbaa !114
  %64 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %prev_frame49 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %64, i32 0, i32 12
  %65 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %prev_frame49, align 4, !tbaa !111
  %mi_cols50 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %65, i32 0, i32 9
  %66 = load i32, i32* %mi_cols50, align 4, !tbaa !115
  %cmp51 = icmp eq i32 %63, %66
  br i1 %cmp51, label %if.then52, label %if.else

if.then52:                                        ; preds = %land.lhs.true47
  %67 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %prev_frame53 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %67, i32 0, i32 12
  %68 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %prev_frame53, align 4, !tbaa !111
  %seg_map = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %68, i32 0, i32 6
  %69 = load i8*, i8** %seg_map, align 4, !tbaa !116
  %70 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %last_frame_seg_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %70, i32 0, i32 25
  store i8* %69, i8** %last_frame_seg_map, align 16, !tbaa !117
  br label %if.end55

if.else:                                          ; preds = %land.lhs.true47, %land.lhs.true, %if.then42
  %71 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %last_frame_seg_map54 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %71, i32 0, i32 25
  store i8* null, i8** %last_frame_seg_map54, align 16, !tbaa !117
  br label %if.end55

if.end55:                                         ; preds = %if.else, %if.then52
  br label %if.end56

if.end56:                                         ; preds = %if.end55, %if.then40
  br label %if.end57

if.end57:                                         ; preds = %if.end56, %if.end38
  %72 = load volatile %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error58 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %72, i32 0, i32 1
  %setjmp59 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error58, i32 0, i32 3
  store i32 0, i32* %setjmp59, align 8, !tbaa !6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end57, %if.then35, %if.then25
  %73 = bitcast i32* %frame_decoded to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  br label %cleanup60

cleanup60:                                        ; preds = %cleanup, %for.end, %if.then7
  %74 = bitcast i8** %source to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #7
  %75 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #7
  %76 = load i32, i32* %retval, align 4
  ret i32 %76
}

; Function Attrs: inlinehint nounwind
define internal %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %cm, i8 signext %ref_frame) #5 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  %map_idx = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !95
  %0 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %2 = load i8, i8* %ref_frame.addr, align 1, !tbaa !95
  %call = call i32 @get_ref_frame_map_idx(%struct.AV1Common* %1, i8 signext %2)
  store i32 %call, i32* %map_idx, align 4, !tbaa !53
  %3 = load i32, i32* %map_idx, align 4, !tbaa !53
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 17
  %5 = load i32, i32* %map_idx, align 4, !tbaa !53
  %arrayidx = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %5
  %6 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.RefCntBuffer* [ %6, %cond.true ], [ null, %cond.false ]
  %7 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret %struct.RefCntBuffer* %cond
}

; Function Attrs: inlinehint nounwind
define internal %struct.RefCntBuffer* @assign_cur_frame_new_fb(%struct.AV1Common* %cm) #5 {
entry:
  %retval = alloca %struct.RefCntBuffer*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %new_fb_idx = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 13
  %1 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !118
  %cmp = icmp ne %struct.RefCntBuffer* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 13
  %3 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame1, align 8, !tbaa !118
  %ref_count = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %3, i32 0, i32 0
  %4 = load i32, i32* %ref_count, align 4, !tbaa !119
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %ref_count, align 4, !tbaa !119
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 13
  store %struct.RefCntBuffer* null, %struct.RefCntBuffer** %cur_frame2, align 8, !tbaa !118
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = bitcast i32* %new_fb_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @get_free_fb(%struct.AV1Common* %7)
  store i32 %call, i32* %new_fb_idx, align 4, !tbaa !53
  %8 = load i32, i32* %new_fb_idx, align 4, !tbaa !53
  %cmp3 = icmp eq i32 %8, -1
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  store %struct.RefCntBuffer* null, %struct.RefCntBuffer** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %if.end
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 41
  %10 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool, align 4, !tbaa !120
  %frame_bufs = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %10, i32 0, i32 3
  %11 = load i32, i32* %new_fb_idx, align 4, !tbaa !53
  %arrayidx = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs, i32 0, i32 %11
  %12 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %12, i32 0, i32 13
  store %struct.RefCntBuffer* %arrayidx, %struct.RefCntBuffer** %cur_frame6, align 8, !tbaa !118
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 13
  %14 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame7, align 8, !tbaa !118
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %14, i32 0, i32 17
  %buf_8bit_valid = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf, i32 0, i32 9
  store i32 0, i32* %buf_8bit_valid, align 4, !tbaa !121
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 13
  %16 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame8, align 8, !tbaa !118
  %interp_filter_selected = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %16, i32 0, i32 19
  %17 = bitcast [4 x i32]* %interp_filter_selected to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %17, i8 0, i32 16, i1 false)
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame9 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 13
  %19 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame9, align 8, !tbaa !118
  store %struct.RefCntBuffer* %19, %struct.RefCntBuffer** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end5, %if.then4
  %20 = bitcast i32* %new_fb_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  %21 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %retval, align 4
  ret %struct.RefCntBuffer* %21
}

; Function Attrs: nounwind
define internal void @release_current_frame(%struct.AV1Decoder* %pbi) #0 {
entry:
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %pool = alloca %struct.BufferPool*, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %1, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %2 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 41
  %4 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool, align 4, !tbaa !120
  store %struct.BufferPool* %4, %struct.BufferPool** %pool, align 4, !tbaa !2
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 13
  %6 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !118
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %6, i32 0, i32 17
  %corrupted = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf, i32 0, i32 25
  store i32 1, i32* %corrupted, align 4, !tbaa !102
  %7 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @lock_buffer_pool(%struct.BufferPool* %7)
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 13
  %9 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame1, align 8, !tbaa !118
  %10 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @decrease_ref_count(%struct.RefCntBuffer* %9, %struct.BufferPool* %10)
  %11 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @unlock_buffer_pool(%struct.BufferPool* %11)
  %12 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %12, i32 0, i32 13
  store %struct.RefCntBuffer* null, %struct.RefCntBuffer** %cur_frame2, align 8, !tbaa !118
  %13 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  %14 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  ret void
}

declare i32 @aom_decode_frame_from_obus(%struct.AV1Decoder*, i8*, i8*, i8**) #2

; Function Attrs: nounwind
define internal void @update_frame_buffers(%struct.AV1Decoder* %pbi, i32 %frame_decoded) #0 {
entry:
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %frame_decoded.addr = alloca i32, align 4
  %ref_index = alloca i32, align 4
  %mask = alloca i32, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %pool = alloca %struct.BufferPool*, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  store i32 %frame_decoded, i32* %frame_decoded.addr, align 4, !tbaa !53
  %0 = bitcast i32* %ref_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %ref_index, align 4, !tbaa !53
  %1 = bitcast i32* %mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %3, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %4 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 41
  %6 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool, align 4, !tbaa !120
  store %struct.BufferPool* %6, %struct.BufferPool** %pool, align 4, !tbaa !2
  %7 = load i32, i32* %frame_decoded.addr, align 4, !tbaa !53
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.else40

if.then:                                          ; preds = %entry
  %8 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @lock_buffer_pool(%struct.BufferPool* %8)
  %9 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %camera_frame_header_ready = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %9, i32 0, i32 35
  %10 = load i32, i32* %camera_frame_header_ready, align 4, !tbaa !122
  %tobool1 = icmp ne i32 %10, 0
  br i1 %tobool1, label %if.end10, label %if.then2

if.then2:                                         ; preds = %if.then
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 0
  %refresh_frame_flags = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 6
  %12 = load i32, i32* %refresh_frame_flags, align 16, !tbaa !123
  store i32 %12, i32* %mask, align 4, !tbaa !53
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %13 = load i32, i32* %mask, align 4, !tbaa !53
  %tobool3 = icmp ne i32 %13, 0
  br i1 %tobool3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i32, i32* %mask, align 4, !tbaa !53
  %and = and i32 %14, 1
  %tobool4 = icmp ne i32 %and, 0
  br i1 %tobool4, label %if.then5, label %if.end

if.then5:                                         ; preds = %for.body
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 17
  %16 = load i32, i32* %ref_index, align 4, !tbaa !53
  %arrayidx = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %16
  %17 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  %18 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @decrease_ref_count(%struct.RefCntBuffer* %17, %struct.BufferPool* %18)
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %19, i32 0, i32 13
  %20 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !118
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %ref_frame_map6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 17
  %22 = load i32, i32* %ref_index, align 4, !tbaa !53
  %arrayidx7 = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map6, i32 0, i32 %22
  store %struct.RefCntBuffer* %20, %struct.RefCntBuffer** %arrayidx7, align 4, !tbaa !2
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %23, i32 0, i32 13
  %24 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame8, align 8, !tbaa !118
  %ref_count = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %24, i32 0, i32 0
  %25 = load i32, i32* %ref_count, align 4, !tbaa !119
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %ref_count, align 4, !tbaa !119
  br label %if.end

if.end:                                           ; preds = %if.then5, %for.body
  %26 = load i32, i32* %ref_index, align 4, !tbaa !53
  %inc9 = add nsw i32 %26, 1
  store i32 %inc9, i32* %ref_index, align 4, !tbaa !53
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %27 = load i32, i32* %mask, align 4, !tbaa !53
  %shr = ashr i32 %27, 1
  store i32 %shr, i32* %mask, align 4, !tbaa !53
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end10

if.end10:                                         ; preds = %for.end, %if.then
  %28 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %show_existing_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %28, i32 0, i32 20
  %29 = load i32, i32* %show_existing_frame, align 4, !tbaa !109
  %tobool11 = icmp ne i32 %29, 0
  br i1 %tobool11, label %if.then13, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end10
  %30 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %show_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %30, i32 0, i32 18
  %31 = load i32, i32* %show_frame, align 4, !tbaa !124
  %tobool12 = icmp ne i32 %31, 0
  br i1 %tobool12, label %if.then13, label %if.else37

if.then13:                                        ; preds = %lor.lhs.false, %if.end10
  %32 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %output_all_layers = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %32, i32 0, i32 14
  %33 = load i32, i32* %output_all_layers, align 4, !tbaa !125
  %tobool14 = icmp ne i32 %33, 0
  br i1 %tobool14, label %if.then15, label %if.else25

if.then15:                                        ; preds = %if.then13
  %34 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %34, i32 0, i32 16
  %35 = load i32, i32* %num_output_frames, align 16, !tbaa !126
  %cmp = icmp uge i32 %35, 4
  br i1 %cmp, label %if.then16, label %if.else

if.then16:                                        ; preds = %if.then15
  %36 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame17 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %36, i32 0, i32 13
  %37 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame17, align 8, !tbaa !118
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %37, i32 0, i32 17
  %corrupted = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf, i32 0, i32 25
  store i32 1, i32* %corrupted, align 4, !tbaa !102
  %38 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame18 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %38, i32 0, i32 13
  %39 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame18, align 8, !tbaa !118
  %40 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @decrease_ref_count(%struct.RefCntBuffer* %39, %struct.BufferPool* %40)
  %41 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %41, i32 0, i32 1
  %error_code = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %error, i32 0, i32 0
  store i32 5, i32* %error_code, align 8, !tbaa !96
  br label %if.end24

if.else:                                          ; preds = %if.then15
  %42 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame19 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %42, i32 0, i32 13
  %43 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame19, align 8, !tbaa !118
  %44 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %44, i32 0, i32 15
  %45 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_output_frames20 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %45, i32 0, i32 16
  %46 = load i32, i32* %num_output_frames20, align 16, !tbaa !126
  %arrayidx21 = getelementptr inbounds [4 x %struct.RefCntBuffer*], [4 x %struct.RefCntBuffer*]* %output_frames, i32 0, i32 %46
  store %struct.RefCntBuffer* %43, %struct.RefCntBuffer** %arrayidx21, align 4, !tbaa !2
  %47 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_output_frames22 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %47, i32 0, i32 16
  %48 = load i32, i32* %num_output_frames22, align 16, !tbaa !126
  %inc23 = add i32 %48, 1
  store i32 %inc23, i32* %num_output_frames22, align 16, !tbaa !126
  br label %if.end24

if.end24:                                         ; preds = %if.else, %if.then16
  br label %if.end36

if.else25:                                        ; preds = %if.then13
  %49 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_output_frames26 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %49, i32 0, i32 16
  %50 = load i32, i32* %num_output_frames26, align 16, !tbaa !126
  %cmp27 = icmp ugt i32 %50, 0
  br i1 %cmp27, label %if.then28, label %if.end31

if.then28:                                        ; preds = %if.else25
  %51 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %output_frames29 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %51, i32 0, i32 15
  %arrayidx30 = getelementptr inbounds [4 x %struct.RefCntBuffer*], [4 x %struct.RefCntBuffer*]* %output_frames29, i32 0, i32 0
  %52 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx30, align 32, !tbaa !2
  %53 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @decrease_ref_count(%struct.RefCntBuffer* %52, %struct.BufferPool* %53)
  br label %if.end31

if.end31:                                         ; preds = %if.then28, %if.else25
  %54 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame32 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %54, i32 0, i32 13
  %55 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame32, align 8, !tbaa !118
  %56 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %output_frames33 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %56, i32 0, i32 15
  %arrayidx34 = getelementptr inbounds [4 x %struct.RefCntBuffer*], [4 x %struct.RefCntBuffer*]* %output_frames33, i32 0, i32 0
  store %struct.RefCntBuffer* %55, %struct.RefCntBuffer** %arrayidx34, align 32, !tbaa !2
  %57 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_output_frames35 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %57, i32 0, i32 16
  store i32 1, i32* %num_output_frames35, align 16, !tbaa !126
  br label %if.end36

if.end36:                                         ; preds = %if.end31, %if.end24
  br label %if.end39

if.else37:                                        ; preds = %lor.lhs.false
  %58 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame38 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %58, i32 0, i32 13
  %59 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame38, align 8, !tbaa !118
  %60 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @decrease_ref_count(%struct.RefCntBuffer* %59, %struct.BufferPool* %60)
  br label %if.end39

if.end39:                                         ; preds = %if.else37, %if.end36
  %61 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @unlock_buffer_pool(%struct.BufferPool* %61)
  br label %if.end42

if.else40:                                        ; preds = %entry
  %62 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @lock_buffer_pool(%struct.BufferPool* %62)
  %63 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame41 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %63, i32 0, i32 13
  %64 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame41, align 8, !tbaa !118
  %65 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @decrease_ref_count(%struct.RefCntBuffer* %64, %struct.BufferPool* %65)
  %66 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @unlock_buffer_pool(%struct.BufferPool* %66)
  br label %if.end42

if.end42:                                         ; preds = %if.else40, %if.end39
  %67 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame43 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %67, i32 0, i32 13
  store %struct.RefCntBuffer* null, %struct.RefCntBuffer** %cur_frame43, align 8, !tbaa !118
  %68 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %camera_frame_header_ready44 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %68, i32 0, i32 35
  %69 = load i32, i32* %camera_frame_header_ready44, align 4, !tbaa !122
  %tobool45 = icmp ne i32 %69, 0
  br i1 %tobool45, label %if.end54, label %if.then46

if.then46:                                        ; preds = %if.end42
  store i32 0, i32* %ref_index, align 4, !tbaa !53
  br label %for.cond47

for.cond47:                                       ; preds = %for.inc51, %if.then46
  %70 = load i32, i32* %ref_index, align 4, !tbaa !53
  %cmp48 = icmp slt i32 %70, 7
  br i1 %cmp48, label %for.body49, label %for.end53

for.body49:                                       ; preds = %for.cond47
  %71 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %remapped_ref_idx = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %71, i32 0, i32 14
  %72 = load i32, i32* %ref_index, align 4, !tbaa !53
  %arrayidx50 = getelementptr inbounds [8 x i32], [8 x i32]* %remapped_ref_idx, i32 0, i32 %72
  store i32 -1, i32* %arrayidx50, align 4, !tbaa !53
  br label %for.inc51

for.inc51:                                        ; preds = %for.body49
  %73 = load i32, i32* %ref_index, align 4, !tbaa !53
  %inc52 = add nsw i32 %73, 1
  store i32 %inc52, i32* %ref_index, align 4, !tbaa !53
  br label %for.cond47

for.end53:                                        ; preds = %for.cond47
  br label %if.end54

if.end54:                                         ; preds = %for.end53, %if.end42
  %74 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #7
  %75 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #7
  %76 = bitcast i32* %mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #7
  %77 = bitcast i32* %ref_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_get_raw_frame(%struct.AV1Decoder* %pbi, i32 %index, %struct.yv12_buffer_config** %sd, %struct.aom_film_grain_t** %grain_params) #0 {
entry:
  %retval = alloca i32, align 4
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %index.addr = alloca i32, align 4
  %sd.addr = alloca %struct.yv12_buffer_config**, align 4
  %grain_params.addr = alloca %struct.aom_film_grain_t**, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !100
  store %struct.yv12_buffer_config** %sd, %struct.yv12_buffer_config*** %sd.addr, align 4, !tbaa !2
  store %struct.aom_film_grain_t** %grain_params, %struct.aom_film_grain_t*** %grain_params.addr, align 4, !tbaa !2
  %0 = load i32, i32* %index.addr, align 4, !tbaa !100
  %1 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %1, i32 0, i32 16
  %2 = load i32, i32* %num_output_frames, align 16, !tbaa !126
  %cmp = icmp uge i32 %0, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %3, i32 0, i32 15
  %4 = load i32, i32* %index.addr, align 4, !tbaa !100
  %arrayidx = getelementptr inbounds [4 x %struct.RefCntBuffer*], [4 x %struct.RefCntBuffer*]* %output_frames, i32 0, i32 %4
  %5 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %5, i32 0, i32 17
  %6 = load %struct.yv12_buffer_config**, %struct.yv12_buffer_config*** %sd.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %buf, %struct.yv12_buffer_config** %6, align 4, !tbaa !2
  %7 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %output_frames1 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %7, i32 0, i32 15
  %8 = load i32, i32* %index.addr, align 4, !tbaa !100
  %arrayidx2 = getelementptr inbounds [4 x %struct.RefCntBuffer*], [4 x %struct.RefCntBuffer*]* %output_frames1, i32 0, i32 %8
  %9 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx2, align 4, !tbaa !2
  %film_grain_params = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %9, i32 0, i32 15
  %10 = load %struct.aom_film_grain_t**, %struct.aom_film_grain_t*** %grain_params.addr, align 4, !tbaa !2
  store %struct.aom_film_grain_t* %film_grain_params, %struct.aom_film_grain_t** %10, align 4, !tbaa !2
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: nounwind
define hidden i32 @av1_get_frame_to_show(%struct.AV1Decoder* %pbi, %struct.yv12_buffer_config* %frame) #0 {
entry:
  %retval = alloca i32, align 4
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %0, i32 0, i32 16
  %1 = load i32, i32* %num_output_frames, align 16, !tbaa !126
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %3 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %3, i32 0, i32 15
  %4 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %num_output_frames1 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %4, i32 0, i32 16
  %5 = load i32, i32* %num_output_frames1, align 16, !tbaa !126
  %sub = sub i32 %5, 1
  %arrayidx = getelementptr inbounds [4 x %struct.RefCntBuffer*], [4 x %struct.RefCntBuffer*]* %output_frames, i32 0, i32 %sub
  %6 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %6, i32 0, i32 17
  %7 = bitcast %struct.yv12_buffer_config* %2 to i8*
  %8 = bitcast %struct.yv12_buffer_config* %buf to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 148, i1 false), !tbaa.struct !127
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare void @av1_rtcd() #2

declare void @aom_dsp_rtcd() #2

declare void @aom_scale_rtcd() #2

declare void @av1_init_intra_predictors() #2

declare void @av1_init_wedge_masks(...) #6

; Function Attrs: inlinehint nounwind
define internal i32 @calc_mi_size(i32 %len) #5 {
entry:
  %len.addr = alloca i32, align 4
  store i32 %len, i32* %len.addr, align 4, !tbaa !53
  %0 = load i32, i32* %len.addr, align 4, !tbaa !53
  %add = add nsw i32 %0, 31
  %and = and i32 %add, -32
  ret i32 %and
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_intrabc_block(%struct.MB_MODE_INFO* %mbmi) #5 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  ret i32 %conv
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_ref_frame_map_idx(%struct.AV1Common* %cm, i8 signext %ref_frame) #5 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !95
  %0 = load i8, i8* %ref_frame.addr, align 1, !tbaa !95
  %conv = sext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 1
  br i1 %cmp, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %ref_frame.addr, align 1, !tbaa !95
  %conv2 = sext i8 %1 to i32
  %cmp3 = icmp sle i32 %conv2, 8
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %remapped_ref_idx = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 14
  %3 = load i8, i8* %ref_frame.addr, align 1, !tbaa !95
  %conv5 = sext i8 %3 to i32
  %sub = sub nsw i32 %conv5, 1
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %remapped_ref_idx, i32 0, i32 %sub
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !53
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ -1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_free_fb(%struct.AV1Common* %cm) #5 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %frame_bufs = alloca %struct.RefCntBuffer*, align 4
  %i = alloca i32, align 4
  %ybf = alloca %struct.yv12_buffer_config*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = bitcast %struct.RefCntBuffer** %frame_bufs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 41
  %2 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool, align 4, !tbaa !120
  %frame_bufs1 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %2, i32 0, i32 3
  %arraydecay = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs1, i32 0, i32 0
  store %struct.RefCntBuffer* %arraydecay, %struct.RefCntBuffer** %frame_bufs, align 4, !tbaa !2
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %buffer_pool2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 41
  %5 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool2, align 4, !tbaa !120
  call void @lock_buffer_pool(%struct.BufferPool* %5)
  store i32 0, i32* %i, align 4, !tbaa !53
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !53
  %cmp = icmp slt i32 %6, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %frame_bufs, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !53
  %arrayidx = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %7, i32 %8
  %ref_count = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx, i32 0, i32 0
  %9 = load i32, i32* %ref_count, align 4, !tbaa !119
  %cmp3 = icmp eq i32 %9, 0
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4, !tbaa !53
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !53
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %11 = load i32, i32* %i, align 4, !tbaa !53
  %cmp4 = icmp ne i32 %11, 16
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %for.end
  %12 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %frame_bufs, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !53
  %arrayidx6 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %12, i32 %13
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx6, i32 0, i32 17
  %use_external_reference_buffers = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf, i32 0, i32 6
  %14 = load i32, i32* %use_external_reference_buffers, align 4, !tbaa !128
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.then7, label %if.end16

if.then7:                                         ; preds = %if.then5
  %15 = bitcast %struct.yv12_buffer_config** %ybf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %frame_bufs, align 4, !tbaa !2
  %17 = load i32, i32* %i, align 4, !tbaa !53
  %arrayidx8 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %16, i32 %17
  %buf9 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx8, i32 0, i32 17
  store %struct.yv12_buffer_config* %buf9, %struct.yv12_buffer_config** %ybf, align 4, !tbaa !2
  %18 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf, align 4, !tbaa !2
  %store_buf_adr = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %18, i32 0, i32 7
  %arrayidx10 = getelementptr inbounds [3 x i8*], [3 x i8*]* %store_buf_adr, i32 0, i32 0
  %19 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf, align 4, !tbaa !2
  %21 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %20, i32 0, i32 5
  %22 = bitcast %union.anon.8* %21 to %struct.anon.9*
  %y_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %22, i32 0, i32 0
  store i8* %19, i8** %y_buffer, align 4, !tbaa !95
  %23 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf, align 4, !tbaa !2
  %store_buf_adr11 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %23, i32 0, i32 7
  %arrayidx12 = getelementptr inbounds [3 x i8*], [3 x i8*]* %store_buf_adr11, i32 0, i32 1
  %24 = load i8*, i8** %arrayidx12, align 4, !tbaa !2
  %25 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf, align 4, !tbaa !2
  %26 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %25, i32 0, i32 5
  %27 = bitcast %union.anon.8* %26 to %struct.anon.9*
  %u_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %27, i32 0, i32 1
  store i8* %24, i8** %u_buffer, align 4, !tbaa !95
  %28 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf, align 4, !tbaa !2
  %store_buf_adr13 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %28, i32 0, i32 7
  %arrayidx14 = getelementptr inbounds [3 x i8*], [3 x i8*]* %store_buf_adr13, i32 0, i32 2
  %29 = load i8*, i8** %arrayidx14, align 4, !tbaa !2
  %30 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf, align 4, !tbaa !2
  %31 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %30, i32 0, i32 5
  %32 = bitcast %union.anon.8* %31 to %struct.anon.9*
  %v_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %32, i32 0, i32 2
  store i8* %29, i8** %v_buffer, align 4, !tbaa !95
  %33 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf, align 4, !tbaa !2
  %use_external_reference_buffers15 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %33, i32 0, i32 6
  store i32 0, i32* %use_external_reference_buffers15, align 4, !tbaa !97
  %34 = bitcast %struct.yv12_buffer_config** %ybf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  br label %if.end16

if.end16:                                         ; preds = %if.then7, %if.then5
  %35 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %frame_bufs, align 4, !tbaa !2
  %36 = load i32, i32* %i, align 4, !tbaa !53
  %arrayidx17 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %35, i32 %36
  %ref_count18 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx17, i32 0, i32 0
  store i32 1, i32* %ref_count18, align 4, !tbaa !119
  br label %if.end19

if.else:                                          ; preds = %for.end
  store i32 -1, i32* %i, align 4, !tbaa !53
  br label %if.end19

if.end19:                                         ; preds = %if.else, %if.end16
  %37 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %buffer_pool20 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %37, i32 0, i32 41
  %38 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool20, align 4, !tbaa !120
  call void @unlock_buffer_pool(%struct.BufferPool* %38)
  %39 = load i32, i32* %i, align 4, !tbaa !53
  %40 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast %struct.RefCntBuffer** %frame_bufs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  ret i32 %39
}

; Function Attrs: nounwind
define internal void @lock_buffer_pool(%struct.BufferPool* %pool) #0 {
entry:
  %pool.addr = alloca %struct.BufferPool*, align 4
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %0 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal void @unlock_buffer_pool(%struct.BufferPool* %pool) #0 {
entry:
  %pool.addr = alloca %struct.BufferPool*, align 4
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %0 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @decrease_ref_count(%struct.RefCntBuffer* %buf, %struct.BufferPool* %pool) #5 {
entry:
  %buf.addr = alloca %struct.RefCntBuffer*, align 4
  %pool.addr = alloca %struct.BufferPool*, align 4
  store %struct.RefCntBuffer* %buf, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %0 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %cmp = icmp ne %struct.RefCntBuffer* %0, null
  br i1 %cmp, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %1 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %ref_count = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %1, i32 0, i32 0
  %2 = load i32, i32* %ref_count, align 4, !tbaa !119
  %dec = add nsw i32 %2, -1
  store i32 %dec, i32* %ref_count, align 4, !tbaa !119
  %3 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %ref_count1 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %3, i32 0, i32 0
  %4 = load i32, i32* %ref_count1, align 4, !tbaa !119
  %cmp2 = icmp eq i32 %4, 0
  br i1 %cmp2, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %5 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %5, i32 0, i32 16
  %data = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer, i32 0, i32 0
  %6 = load i8*, i8** %data, align 4, !tbaa !129
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.then3, label %if.end

if.then3:                                         ; preds = %land.lhs.true
  %7 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %release_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %7, i32 0, i32 2
  %8 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb, align 4, !tbaa !130
  %9 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %cb_priv = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %9, i32 0, i32 0
  %10 = load i8*, i8** %cb_priv, align 4, !tbaa !133
  %11 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer4 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %11, i32 0, i32 16
  %call = call i32 %8(i8* %10, %struct.aom_codec_frame_buffer* %raw_frame_buffer4)
  %12 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer5 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %12, i32 0, i32 16
  %data6 = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer5, i32 0, i32 0
  store i8* null, i8** %data6, align 4, !tbaa !129
  %13 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer7 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %13, i32 0, i32 16
  %size = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer7, i32 0, i32 1
  store i32 0, i32* %size, align 4, !tbaa !134
  %14 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer8 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %14, i32 0, i32 16
  %priv = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer8, i32 0, i32 2
  store i8* null, i8** %priv, align 4, !tbaa !135
  br label %if.end

if.end:                                           ; preds = %if.then3, %land.lhs.true, %if.then
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { returns_twice "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-prototype" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { returns_twice }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !9, i64 128}
!7 = !{!"AV1Common", !8, i64 0, !11, i64 40, !9, i64 288, !9, i64 292, !9, i64 296, !9, i64 300, !9, i64 304, !9, i64 308, !4, i64 312, !12, i64 313, !4, i64 316, !9, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !13, i64 492, !4, i64 580, !4, i64 1284, !9, i64 1316, !9, i64 1320, !9, i64 1324, !14, i64 1328, !15, i64 1356, !16, i64 1420, !17, i64 10676, !3, i64 10848, !18, i64 10864, !19, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !20, i64 14880, !22, i64 15028, !23, i64 15168, !25, i64 15816, !4, i64 15836, !26, i64 16192, !3, i64 18128, !3, i64 18132, !29, i64 18136, !3, i64 18724, !30, i64 18728, !9, i64 18760, !4, i64 18764, !3, i64 18796, !9, i64 18800, !4, i64 18804, !4, i64 18836, !9, i64 18844, !9, i64 18848, !9, i64 18852, !9, i64 18856}
!8 = !{!"", !4, i64 0, !4, i64 1, !9, i64 4, !9, i64 8, !9, i64 12, !10, i64 16, !9, i64 32, !9, i64 36}
!9 = !{!"int", !4, i64 0}
!10 = !{!"", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!11 = !{!"aom_internal_error_info", !4, i64 0, !9, i64 4, !4, i64 8, !9, i64 88, !4, i64 92}
!12 = !{!"_Bool", !4, i64 0}
!13 = !{!"scale_factors", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!14 = !{!"", !12, i64 0, !12, i64 1, !12, i64 2, !12, i64 3, !12, i64 4, !12, i64 5, !12, i64 6, !12, i64 7, !12, i64 8, !12, i64 9, !12, i64 10, !12, i64 11, !4, i64 12, !4, i64 13, !9, i64 16, !9, i64 20, !4, i64 24}
!15 = !{!"CommonModeInfoParams", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !3, i64 20, !9, i64 24, !9, i64 28, !4, i64 32, !3, i64 36, !9, i64 40, !9, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!16 = !{!"CommonQuantParams", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !12, i64 9240, !9, i64 9244, !9, i64 9248, !9, i64 9252}
!17 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !9, i64 164, !4, i64 168}
!18 = !{!"", !4, i64 0, !4, i64 3072}
!19 = !{!"loopfilter", !4, i64 0, !9, i64 8, !9, i64 12, !9, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !9, i64 32}
!20 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !9, i64 52, !4, i64 56, !3, i64 68, !9, i64 72, !3, i64 76, !21, i64 80, !9, i64 84, !21, i64 88, !9, i64 92, !9, i64 96, !9, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !9, i64 128, !9, i64 132, !9, i64 136, !9, i64 140, !3, i64 144}
!21 = !{!"long", !4, i64 0}
!22 = !{!"", !9, i64 0, !9, i64 4, !4, i64 8, !4, i64 72, !9, i64 136}
!23 = !{!"", !9, i64 0, !9, i64 4, !4, i64 8, !9, i64 120, !4, i64 124, !9, i64 204, !4, i64 208, !9, i64 288, !9, i64 292, !9, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !9, i64 596, !9, i64 600, !9, i64 604, !9, i64 608, !9, i64 612, !9, i64 616, !9, i64 620, !9, i64 624, !9, i64 628, !9, i64 632, !9, i64 636, !9, i64 640, !24, i64 644}
!24 = !{!"short", !4, i64 0}
!25 = !{!"", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16}
!26 = !{!"SequenceHeader", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !4, i64 16, !9, i64 20, !9, i64 24, !4, i64 28, !9, i64 32, !9, i64 36, !10, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !9, i64 92, !9, i64 96, !9, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !9, i64 112, !4, i64 116, !9, i64 244, !27, i64 248, !4, i64 264, !28, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!27 = !{!"aom_timing", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!28 = !{!"aom_dec_model_info", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!29 = !{!"CommonTileParams", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28, !9, i64 32, !9, i64 36, !9, i64 40, !9, i64 44, !9, i64 48, !9, i64 52, !9, i64 56, !4, i64 60, !4, i64 320, !9, i64 580, !9, i64 584}
!30 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !9, i64 20, !9, i64 24, !9, i64 28}
!31 = !{!7, !3, i64 18128}
!32 = !{!7, !3, i64 18132}
!33 = !{!34, !9, i64 381796}
!34 = !{!"AV1Decoder", !35, i64 0, !7, i64 44032, !39, i64 62896, !40, i64 62924, !41, i64 62964, !42, i64 63008, !3, i64 63188, !9, i64 63192, !3, i64 63196, !43, i64 63200, !3, i64 348960, !9, i64 348964, !4, i64 348968, !45, i64 381736, !9, i64 381756, !4, i64 381760, !21, i64 381776, !9, i64 381780, !9, i64 381784, !9, i64 381788, !9, i64 381792, !9, i64 381796, !9, i64 381800, !9, i64 381804, !9, i64 381808, !9, i64 381812, !9, i64 381816, !9, i64 381820, !46, i64 381824, !9, i64 384924, !9, i64 384928, !9, i64 384932, !9, i64 384936, !9, i64 384940, !9, i64 384944, !9, i64 384948, !21, i64 384952, !50, i64 384956, !9, i64 384964, !9, i64 384968, !9, i64 384972, !9, i64 384976, !9, i64 384980, !9, i64 384984, !51, i64 384988, !20, i64 403936, !3, i64 404084, !9, i64 404088, !9, i64 404092, !52, i64 404096, !3, i64 404136, !9, i64 404140, !9, i64 404144, !9, i64 404148, !9, i64 404152, !4, i64 404156}
!35 = !{!"macroblockd", !9, i64 0, !9, i64 4, !9, i64 8, !12, i64 12, !4, i64 16, !36, i64 4060, !3, i64 4084, !12, i64 4088, !12, i64 4089, !12, i64 4090, !12, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !9, i64 4112, !9, i64 4116, !9, i64 4120, !9, i64 4124, !9, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !9, i64 6836, !4, i64 6840, !4, i64 6872, !9, i64 6904, !9, i64 6908, !3, i64 6912, !3, i64 6916, !9, i64 6920, !9, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !37, i64 39720, !38, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!36 = !{!"TileInfo", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20}
!37 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !9, i64 4104, !4, i64 4108, !9, i64 4236, !9, i64 4240, !9, i64 4244, !9, i64 4248, !9, i64 4252, !9, i64 4256}
!38 = !{!"dist_wtd_comp_params", !9, i64 0, !9, i64 4, !9, i64 8}
!39 = !{!"", !3, i64 0, !4, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !9, i64 24}
!40 = !{!"AV1LfSyncData", !4, i64 0, !9, i64 12, !9, i64 16, !3, i64 20, !9, i64 24, !3, i64 28, !9, i64 32, !9, i64 36}
!41 = !{!"AV1LrSyncData", !4, i64 0, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !3, i64 28, !3, i64 32, !9, i64 36, !9, i64 40}
!42 = !{!"AV1LrStruct", !3, i64 0, !4, i64 4, !3, i64 172, !3, i64 176}
!43 = !{!"ThreadData", !35, i64 0, !44, i64 44032, !3, i64 285696, !4, i64 285700, !9, i64 285708, !9, i64 285712, !3, i64 285716, !4, i64 285720, !3, i64 285728, !3, i64 285732, !3, i64 285736, !3, i64 285740, !3, i64 285744, !3, i64 285748}
!44 = !{!"", !4, i64 0, !4, i64 196608, !4, i64 208896}
!45 = !{!"AV1DecTileMTData", !3, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16}
!46 = !{!"Accounting", !47, i64 0, !9, i64 1044, !4, i64 1048, !49, i64 3090, !9, i64 3096}
!47 = !{!"", !3, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !48, i64 16}
!48 = !{!"", !4, i64 0, !9, i64 1024}
!49 = !{!"", !24, i64 0, !24, i64 2}
!50 = !{!"DataBuffer", !3, i64 0, !21, i64 4}
!51 = !{!"EXTERNAL_REFERENCES", !4, i64 0, !9, i64 18944}
!52 = !{!"AV1DecRowMTInfo", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28, !9, i64 32, !9, i64 36}
!53 = !{!9, !9, i64 0}
!54 = !{!7, !9, i64 12}
!55 = !{!34, !9, i64 381780}
!56 = !{!34, !3, i64 62756}
!57 = !{!7, !4, i64 16264}
!58 = !{!7, !3, i64 1408}
!59 = !{!7, !3, i64 1412}
!60 = !{!7, !3, i64 1416}
!61 = !{!34, !9, i64 381820}
!62 = !{!63, !3, i64 0}
!63 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20}
!64 = !{!34, !3, i64 62904}
!65 = !{!63, !3, i64 20}
!66 = !{!34, !3, i64 62912}
!67 = !{!34, !3, i64 63196}
!68 = !{!34, !9, i64 381788}
!69 = !{!70, !3, i64 0}
!70 = !{!"DecWorkerData", !3, i64 0, !3, i64 4, !11, i64 8}
!71 = !{!34, !9, i64 63192}
!72 = !{!34, !3, i64 63188}
!73 = !{!34, !9, i64 348964}
!74 = !{!34, !3, i64 348960}
!75 = !{!34, !3, i64 404136}
!76 = !{!15, !3, i64 20}
!77 = !{!15, !3, i64 36}
!78 = !{!15, !9, i64 24}
!79 = !{!15, !3, i64 48}
!80 = !{!15, !9, i64 44}
!81 = !{!15, !9, i64 12}
!82 = !{!15, !9, i64 16}
!83 = !{!15, !9, i64 4}
!84 = !{!15, !9, i64 0}
!85 = !{!15, !9, i64 8}
!86 = !{!15, !4, i64 32}
!87 = !{!15, !9, i64 28}
!88 = !{!7, !4, i64 16269}
!89 = !{!45, !3, i64 0}
!90 = !{!34, !3, i64 404084}
!91 = !{!34, !9, i64 404088}
!92 = !{!35, !3, i64 4084}
!93 = !{!35, !12, i64 12}
!94 = !{i8 0, i8 2}
!95 = !{!4, !4, i64 0}
!96 = !{!7, !4, i64 40}
!97 = !{!20, !9, i64 52}
!98 = !{!20, !9, i64 84}
!99 = !{!20, !9, i64 140}
!100 = !{!21, !21, i64 0}
!101 = !{!7, !9, i64 44}
!102 = !{!103, !9, i64 1420}
!103 = !{!"RefCntBuffer", !9, i64 0, !9, i64 4, !4, i64 8, !9, i64 36, !4, i64 40, !3, i64 68, !3, i64 72, !17, i64 76, !9, i64 248, !9, i64 252, !9, i64 256, !9, i64 260, !4, i64 264, !9, i64 616, !4, i64 620, !23, i64 624, !104, i64 1272, !20, i64 1284, !4, i64 1432, !4, i64 1436, !4, i64 1452, !4, i64 1460, !105, i64 1464}
!104 = !{!"aom_codec_frame_buffer", !3, i64 0, !21, i64 4, !3, i64 8}
!105 = !{!"frame_contexts", !4, i64 0, !4, i64 390, !4, i64 930, !4, i64 966, !4, i64 1014, !4, i64 1070, !4, i64 1134, !4, i64 1206, !4, i64 1286, !4, i64 1374, !4, i64 1470, !4, i64 1790, !4, i64 5990, !4, i64 8090, !4, i64 8126, !4, i64 8138, !4, i64 8174, !4, i64 8192, !4, i64 8336, !4, i64 8468, !4, i64 9216, !4, i64 9240, !4, i64 9372, !4, i64 9412, !4, i64 9588, !4, i64 9720, !4, i64 9832, !4, i64 9944, !4, i64 10574, !4, i64 11204, !4, i64 11330, !4, i64 11342, !4, i64 11372, !4, i64 11480, !4, i64 11510, !4, i64 11564, !4, i64 11618, !4, i64 11654, !4, i64 11780, !4, i64 11816, !4, i64 11852, !4, i64 11870, !4, i64 11888, !106, i64 11912, !106, i64 12198, !4, i64 12484, !107, i64 12490, !4, i64 12580, !4, i64 12712, !4, i64 12724, !4, i64 12732, !4, i64 12738, !4, i64 12744, !4, i64 12856, !4, i64 13636, !4, i64 14076, !4, i64 14204, !4, i64 14904, !4, i64 15032, !4, i64 15128, !4, i64 15138, !4, i64 15178, !4, i64 15188, !4, i64 20492, !4, i64 21036, !4, i64 21054, !9, i64 21260}
!106 = !{!"", !4, i64 0, !4, i64 10}
!107 = !{!"segmentation_probs", !4, i64 0, !4, i64 18, !4, i64 36}
!108 = !{!63, !3, i64 8}
!109 = !{!7, !9, i64 1324}
!110 = !{!7, !4, i64 10676}
!111 = !{!7, !3, i64 452}
!112 = !{!7, !9, i64 1368}
!113 = !{!103, !9, i64 248}
!114 = !{!7, !9, i64 1372}
!115 = !{!103, !9, i64 252}
!116 = !{!103, !3, i64 72}
!117 = !{!7, !3, i64 10848}
!118 = !{!7, !3, i64 456}
!119 = !{!103, !9, i64 0}
!120 = !{!7, !3, i64 18724}
!121 = !{!103, !9, i64 1356}
!122 = !{!34, !9, i64 384948}
!123 = !{!7, !9, i64 32}
!124 = !{!7, !9, i64 1316}
!125 = !{!34, !9, i64 381756}
!126 = !{!34, !21, i64 381776}
!127 = !{i64 0, i64 4, !53, i64 4, i64 4, !53, i64 0, i64 8, !95, i64 8, i64 4, !53, i64 12, i64 4, !53, i64 8, i64 8, !95, i64 16, i64 4, !53, i64 20, i64 4, !53, i64 16, i64 8, !95, i64 24, i64 4, !53, i64 28, i64 4, !53, i64 24, i64 8, !95, i64 32, i64 4, !53, i64 36, i64 4, !53, i64 32, i64 8, !95, i64 40, i64 4, !2, i64 44, i64 4, !2, i64 48, i64 4, !2, i64 40, i64 12, !95, i64 52, i64 4, !53, i64 56, i64 12, !95, i64 68, i64 4, !2, i64 72, i64 4, !53, i64 76, i64 4, !2, i64 80, i64 4, !100, i64 84, i64 4, !53, i64 88, i64 4, !100, i64 92, i64 4, !53, i64 96, i64 4, !53, i64 100, i64 4, !53, i64 104, i64 4, !95, i64 108, i64 4, !95, i64 112, i64 4, !95, i64 116, i64 1, !95, i64 120, i64 4, !95, i64 124, i64 4, !95, i64 128, i64 4, !53, i64 132, i64 4, !53, i64 136, i64 4, !53, i64 140, i64 4, !53, i64 144, i64 4, !2}
!128 = !{!103, !9, i64 1336}
!129 = !{!103, !3, i64 1272}
!130 = !{!131, !3, i64 8}
!131 = !{!"BufferPool", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !132, i64 363660}
!132 = !{!"InternalFrameBufferList", !9, i64 0, !3, i64 4}
!133 = !{!131, !3, i64 0}
!134 = !{!103, !21, i64 1276}
!135 = !{!103, !3, i64 1280}
