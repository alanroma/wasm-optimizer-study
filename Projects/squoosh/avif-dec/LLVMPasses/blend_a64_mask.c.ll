; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/blend_a64_mask.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/blend_a64_mask.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }

; Function Attrs: nounwind
define hidden void @aom_lowbd_blend_a64_d16_mask_c(i8* %dst, i32 %dst_stride, i16* %src0, i32 %src0_stride, i16* %src1, i32 %src1_stride, i8* %mask, i32 %mask_stride, i32 %w, i32 %h, i32 %subw, i32 %subh, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src0.addr = alloca i16*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i16*, align 4
  %src1_stride.addr = alloca i32, align 4
  %mask.addr = alloca i8*, align 4
  %mask_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %subw.addr = alloca i32, align 4
  %subh.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %bd = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %round_offset = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %res = alloca i32, align 4
  %m = alloca i32, align 4
  %res53 = alloca i32, align 4
  %m54 = alloca i32, align 4
  %res131 = alloca i32, align 4
  %m132 = alloca i32, align 4
  %res184 = alloca i32, align 4
  %m185 = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i16* %src0, i16** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i16* %src1, i16** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %mask_stride, i32* %mask_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %subw, i32* %subw.addr, align 4, !tbaa !6
  store i32 %subh, i32* %subh.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 8, i32* %bd, align 4, !tbaa !6
  %3 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 3
  %5 = load i32, i32* %round_0, align 4, !tbaa !8
  %sub = sub nsw i32 22, %5
  store i32 %sub, i32* %offset_bits, align 4, !tbaa !6
  %6 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %8 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %8, i32 0, i32 4
  %9 = load i32, i32* %round_1, align 4, !tbaa !10
  %sub1 = sub nsw i32 %7, %9
  %shl = shl i32 1, %sub1
  %10 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %11 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_12 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %11, i32 0, i32 4
  %12 = load i32, i32* %round_12, align 4, !tbaa !10
  %sub3 = sub nsw i32 %10, %12
  %sub4 = sub nsw i32 %sub3, 1
  %shl5 = shl i32 1, %sub4
  %add = add nsw i32 %shl, %shl5
  store i32 %add, i32* %round_offset, align 4, !tbaa !6
  %13 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_06 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %14, i32 0, i32 3
  %15 = load i32, i32* %round_06, align 4, !tbaa !8
  %sub7 = sub nsw i32 14, %15
  %16 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_18 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %16, i32 0, i32 4
  %17 = load i32, i32* %round_18, align 4, !tbaa !10
  %sub9 = sub nsw i32 %sub7, %17
  store i32 %sub9, i32* %round_bits, align 4, !tbaa !6
  %18 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %18, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %19 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp10 = icmp eq i32 %19, 0
  br i1 %cmp10, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc36, %if.then
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %20, %21
  br i1 %cmp11, label %for.body, label %for.end38

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %for.body
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %23 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %22, %23
  br i1 %cmp13, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond12
  %24 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  %26 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %28 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul = mul i32 %27, %28
  %29 = load i32, i32* %j, align 4, !tbaa !6
  %add15 = add i32 %mul, %29
  %arrayidx = getelementptr inbounds i8, i8* %26, i32 %add15
  %30 = load i8, i8* %arrayidx, align 1, !tbaa !11
  %conv = zext i8 %30 to i32
  store i32 %conv, i32* %m, align 4, !tbaa !6
  %31 = load i32, i32* %m, align 4, !tbaa !6
  %32 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %34 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul16 = mul i32 %33, %34
  %35 = load i32, i32* %j, align 4, !tbaa !6
  %add17 = add i32 %mul16, %35
  %arrayidx18 = getelementptr inbounds i16, i16* %32, i32 %add17
  %36 = load i16, i16* %arrayidx18, align 2, !tbaa !12
  %conv19 = zext i16 %36 to i32
  %mul20 = mul nsw i32 %31, %conv19
  %37 = load i32, i32* %m, align 4, !tbaa !6
  %sub21 = sub nsw i32 64, %37
  %38 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %40 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul22 = mul i32 %39, %40
  %41 = load i32, i32* %j, align 4, !tbaa !6
  %add23 = add i32 %mul22, %41
  %arrayidx24 = getelementptr inbounds i16, i16* %38, i32 %add23
  %42 = load i16, i16* %arrayidx24, align 2, !tbaa !12
  %conv25 = zext i16 %42 to i32
  %mul26 = mul nsw i32 %sub21, %conv25
  %add27 = add nsw i32 %mul20, %mul26
  %shr = ashr i32 %add27, 6
  store i32 %shr, i32* %res, align 4, !tbaa !6
  %43 = load i32, i32* %round_offset, align 4, !tbaa !6
  %44 = load i32, i32* %res, align 4, !tbaa !6
  %sub28 = sub nsw i32 %44, %43
  store i32 %sub28, i32* %res, align 4, !tbaa !6
  %45 = load i32, i32* %res, align 4, !tbaa !6
  %46 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl29 = shl i32 1, %46
  %shr30 = ashr i32 %shl29, 1
  %add31 = add nsw i32 %45, %shr30
  %47 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr32 = ashr i32 %add31, %47
  %call = call zeroext i8 @clip_pixel(i32 %shr32)
  %48 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %50 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul33 = mul i32 %49, %50
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %add34 = add i32 %mul33, %51
  %arrayidx35 = getelementptr inbounds i8, i8* %48, i32 %add34
  store i8 %call, i8* %arrayidx35, align 1, !tbaa !11
  %52 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %54 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond12

for.end:                                          ; preds = %for.cond12
  br label %for.inc36

for.inc36:                                        ; preds = %for.end
  %55 = load i32, i32* %i, align 4, !tbaa !6
  %inc37 = add nsw i32 %55, 1
  store i32 %inc37, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end38:                                        ; preds = %for.cond
  br label %if.end229

if.else:                                          ; preds = %land.lhs.true, %entry
  %56 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp39 = icmp eq i32 %56, 1
  br i1 %cmp39, label %land.lhs.true41, label %if.else116

land.lhs.true41:                                  ; preds = %if.else
  %57 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp42 = icmp eq i32 %57, 1
  br i1 %cmp42, label %if.then44, label %if.else116

if.then44:                                        ; preds = %land.lhs.true41
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond45

for.cond45:                                       ; preds = %for.inc113, %if.then44
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %59 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp46 = icmp slt i32 %58, %59
  br i1 %cmp46, label %for.body48, label %for.end115

for.body48:                                       ; preds = %for.cond45
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc110, %for.body48
  %60 = load i32, i32* %j, align 4, !tbaa !6
  %61 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp50 = icmp slt i32 %60, %61
  br i1 %cmp50, label %for.body52, label %for.end112

for.body52:                                       ; preds = %for.cond49
  %62 = bitcast i32* %res53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #3
  %63 = bitcast i32* %m54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #3
  %64 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %65 = load i32, i32* %i, align 4, !tbaa !6
  %mul55 = mul nsw i32 2, %65
  %66 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul56 = mul i32 %mul55, %66
  %67 = load i32, i32* %j, align 4, !tbaa !6
  %mul57 = mul nsw i32 2, %67
  %add58 = add i32 %mul56, %mul57
  %arrayidx59 = getelementptr inbounds i8, i8* %64, i32 %add58
  %68 = load i8, i8* %arrayidx59, align 1, !tbaa !11
  %conv60 = zext i8 %68 to i32
  %69 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %70 = load i32, i32* %i, align 4, !tbaa !6
  %mul61 = mul nsw i32 2, %70
  %add62 = add nsw i32 %mul61, 1
  %71 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul63 = mul i32 %add62, %71
  %72 = load i32, i32* %j, align 4, !tbaa !6
  %mul64 = mul nsw i32 2, %72
  %add65 = add i32 %mul63, %mul64
  %arrayidx66 = getelementptr inbounds i8, i8* %69, i32 %add65
  %73 = load i8, i8* %arrayidx66, align 1, !tbaa !11
  %conv67 = zext i8 %73 to i32
  %add68 = add nsw i32 %conv60, %conv67
  %74 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %75 = load i32, i32* %i, align 4, !tbaa !6
  %mul69 = mul nsw i32 2, %75
  %76 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul70 = mul i32 %mul69, %76
  %77 = load i32, i32* %j, align 4, !tbaa !6
  %mul71 = mul nsw i32 2, %77
  %add72 = add nsw i32 %mul71, 1
  %add73 = add i32 %mul70, %add72
  %arrayidx74 = getelementptr inbounds i8, i8* %74, i32 %add73
  %78 = load i8, i8* %arrayidx74, align 1, !tbaa !11
  %conv75 = zext i8 %78 to i32
  %add76 = add nsw i32 %add68, %conv75
  %79 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %80 = load i32, i32* %i, align 4, !tbaa !6
  %mul77 = mul nsw i32 2, %80
  %add78 = add nsw i32 %mul77, 1
  %81 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul79 = mul i32 %add78, %81
  %82 = load i32, i32* %j, align 4, !tbaa !6
  %mul80 = mul nsw i32 2, %82
  %add81 = add nsw i32 %mul80, 1
  %add82 = add i32 %mul79, %add81
  %arrayidx83 = getelementptr inbounds i8, i8* %79, i32 %add82
  %83 = load i8, i8* %arrayidx83, align 1, !tbaa !11
  %conv84 = zext i8 %83 to i32
  %add85 = add nsw i32 %add76, %conv84
  %add86 = add nsw i32 %add85, 2
  %shr87 = ashr i32 %add86, 2
  store i32 %shr87, i32* %m54, align 4, !tbaa !6
  %84 = load i32, i32* %m54, align 4, !tbaa !6
  %85 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %86 = load i32, i32* %i, align 4, !tbaa !6
  %87 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul88 = mul i32 %86, %87
  %88 = load i32, i32* %j, align 4, !tbaa !6
  %add89 = add i32 %mul88, %88
  %arrayidx90 = getelementptr inbounds i16, i16* %85, i32 %add89
  %89 = load i16, i16* %arrayidx90, align 2, !tbaa !12
  %conv91 = zext i16 %89 to i32
  %mul92 = mul nsw i32 %84, %conv91
  %90 = load i32, i32* %m54, align 4, !tbaa !6
  %sub93 = sub nsw i32 64, %90
  %91 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %92 = load i32, i32* %i, align 4, !tbaa !6
  %93 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul94 = mul i32 %92, %93
  %94 = load i32, i32* %j, align 4, !tbaa !6
  %add95 = add i32 %mul94, %94
  %arrayidx96 = getelementptr inbounds i16, i16* %91, i32 %add95
  %95 = load i16, i16* %arrayidx96, align 2, !tbaa !12
  %conv97 = zext i16 %95 to i32
  %mul98 = mul nsw i32 %sub93, %conv97
  %add99 = add nsw i32 %mul92, %mul98
  %shr100 = ashr i32 %add99, 6
  store i32 %shr100, i32* %res53, align 4, !tbaa !6
  %96 = load i32, i32* %round_offset, align 4, !tbaa !6
  %97 = load i32, i32* %res53, align 4, !tbaa !6
  %sub101 = sub nsw i32 %97, %96
  store i32 %sub101, i32* %res53, align 4, !tbaa !6
  %98 = load i32, i32* %res53, align 4, !tbaa !6
  %99 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl102 = shl i32 1, %99
  %shr103 = ashr i32 %shl102, 1
  %add104 = add nsw i32 %98, %shr103
  %100 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr105 = ashr i32 %add104, %100
  %call106 = call zeroext i8 @clip_pixel(i32 %shr105)
  %101 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %102 = load i32, i32* %i, align 4, !tbaa !6
  %103 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul107 = mul i32 %102, %103
  %104 = load i32, i32* %j, align 4, !tbaa !6
  %add108 = add i32 %mul107, %104
  %arrayidx109 = getelementptr inbounds i8, i8* %101, i32 %add108
  store i8 %call106, i8* %arrayidx109, align 1, !tbaa !11
  %105 = bitcast i32* %m54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #3
  %106 = bitcast i32* %res53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #3
  br label %for.inc110

for.inc110:                                       ; preds = %for.body52
  %107 = load i32, i32* %j, align 4, !tbaa !6
  %inc111 = add nsw i32 %107, 1
  store i32 %inc111, i32* %j, align 4, !tbaa !6
  br label %for.cond49

for.end112:                                       ; preds = %for.cond49
  br label %for.inc113

for.inc113:                                       ; preds = %for.end112
  %108 = load i32, i32* %i, align 4, !tbaa !6
  %inc114 = add nsw i32 %108, 1
  store i32 %inc114, i32* %i, align 4, !tbaa !6
  br label %for.cond45

for.end115:                                       ; preds = %for.cond45
  br label %if.end228

if.else116:                                       ; preds = %land.lhs.true41, %if.else
  %109 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp117 = icmp eq i32 %109, 1
  br i1 %cmp117, label %land.lhs.true119, label %if.else175

land.lhs.true119:                                 ; preds = %if.else116
  %110 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp120 = icmp eq i32 %110, 0
  br i1 %cmp120, label %if.then122, label %if.else175

if.then122:                                       ; preds = %land.lhs.true119
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond123

for.cond123:                                      ; preds = %for.inc172, %if.then122
  %111 = load i32, i32* %i, align 4, !tbaa !6
  %112 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp124 = icmp slt i32 %111, %112
  br i1 %cmp124, label %for.body126, label %for.end174

for.body126:                                      ; preds = %for.cond123
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond127

for.cond127:                                      ; preds = %for.inc169, %for.body126
  %113 = load i32, i32* %j, align 4, !tbaa !6
  %114 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp128 = icmp slt i32 %113, %114
  br i1 %cmp128, label %for.body130, label %for.end171

for.body130:                                      ; preds = %for.cond127
  %115 = bitcast i32* %res131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %115) #3
  %116 = bitcast i32* %m132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #3
  %117 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %118 = load i32, i32* %i, align 4, !tbaa !6
  %119 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul133 = mul i32 %118, %119
  %120 = load i32, i32* %j, align 4, !tbaa !6
  %mul134 = mul nsw i32 2, %120
  %add135 = add i32 %mul133, %mul134
  %arrayidx136 = getelementptr inbounds i8, i8* %117, i32 %add135
  %121 = load i8, i8* %arrayidx136, align 1, !tbaa !11
  %conv137 = zext i8 %121 to i32
  %122 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %123 = load i32, i32* %i, align 4, !tbaa !6
  %124 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul138 = mul i32 %123, %124
  %125 = load i32, i32* %j, align 4, !tbaa !6
  %mul139 = mul nsw i32 2, %125
  %add140 = add nsw i32 %mul139, 1
  %add141 = add i32 %mul138, %add140
  %arrayidx142 = getelementptr inbounds i8, i8* %122, i32 %add141
  %126 = load i8, i8* %arrayidx142, align 1, !tbaa !11
  %conv143 = zext i8 %126 to i32
  %add144 = add nsw i32 %conv137, %conv143
  %add145 = add nsw i32 %add144, 1
  %shr146 = ashr i32 %add145, 1
  store i32 %shr146, i32* %m132, align 4, !tbaa !6
  %127 = load i32, i32* %m132, align 4, !tbaa !6
  %128 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %129 = load i32, i32* %i, align 4, !tbaa !6
  %130 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul147 = mul i32 %129, %130
  %131 = load i32, i32* %j, align 4, !tbaa !6
  %add148 = add i32 %mul147, %131
  %arrayidx149 = getelementptr inbounds i16, i16* %128, i32 %add148
  %132 = load i16, i16* %arrayidx149, align 2, !tbaa !12
  %conv150 = zext i16 %132 to i32
  %mul151 = mul nsw i32 %127, %conv150
  %133 = load i32, i32* %m132, align 4, !tbaa !6
  %sub152 = sub nsw i32 64, %133
  %134 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %135 = load i32, i32* %i, align 4, !tbaa !6
  %136 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul153 = mul i32 %135, %136
  %137 = load i32, i32* %j, align 4, !tbaa !6
  %add154 = add i32 %mul153, %137
  %arrayidx155 = getelementptr inbounds i16, i16* %134, i32 %add154
  %138 = load i16, i16* %arrayidx155, align 2, !tbaa !12
  %conv156 = zext i16 %138 to i32
  %mul157 = mul nsw i32 %sub152, %conv156
  %add158 = add nsw i32 %mul151, %mul157
  %shr159 = ashr i32 %add158, 6
  store i32 %shr159, i32* %res131, align 4, !tbaa !6
  %139 = load i32, i32* %round_offset, align 4, !tbaa !6
  %140 = load i32, i32* %res131, align 4, !tbaa !6
  %sub160 = sub nsw i32 %140, %139
  store i32 %sub160, i32* %res131, align 4, !tbaa !6
  %141 = load i32, i32* %res131, align 4, !tbaa !6
  %142 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl161 = shl i32 1, %142
  %shr162 = ashr i32 %shl161, 1
  %add163 = add nsw i32 %141, %shr162
  %143 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr164 = ashr i32 %add163, %143
  %call165 = call zeroext i8 @clip_pixel(i32 %shr164)
  %144 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %145 = load i32, i32* %i, align 4, !tbaa !6
  %146 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul166 = mul i32 %145, %146
  %147 = load i32, i32* %j, align 4, !tbaa !6
  %add167 = add i32 %mul166, %147
  %arrayidx168 = getelementptr inbounds i8, i8* %144, i32 %add167
  store i8 %call165, i8* %arrayidx168, align 1, !tbaa !11
  %148 = bitcast i32* %m132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #3
  %149 = bitcast i32* %res131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #3
  br label %for.inc169

for.inc169:                                       ; preds = %for.body130
  %150 = load i32, i32* %j, align 4, !tbaa !6
  %inc170 = add nsw i32 %150, 1
  store i32 %inc170, i32* %j, align 4, !tbaa !6
  br label %for.cond127

for.end171:                                       ; preds = %for.cond127
  br label %for.inc172

for.inc172:                                       ; preds = %for.end171
  %151 = load i32, i32* %i, align 4, !tbaa !6
  %inc173 = add nsw i32 %151, 1
  store i32 %inc173, i32* %i, align 4, !tbaa !6
  br label %for.cond123

for.end174:                                       ; preds = %for.cond123
  br label %if.end

if.else175:                                       ; preds = %land.lhs.true119, %if.else116
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond176

for.cond176:                                      ; preds = %for.inc225, %if.else175
  %152 = load i32, i32* %i, align 4, !tbaa !6
  %153 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp177 = icmp slt i32 %152, %153
  br i1 %cmp177, label %for.body179, label %for.end227

for.body179:                                      ; preds = %for.cond176
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond180

for.cond180:                                      ; preds = %for.inc222, %for.body179
  %154 = load i32, i32* %j, align 4, !tbaa !6
  %155 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp181 = icmp slt i32 %154, %155
  br i1 %cmp181, label %for.body183, label %for.end224

for.body183:                                      ; preds = %for.cond180
  %156 = bitcast i32* %res184 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %156) #3
  %157 = bitcast i32* %m185 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #3
  %158 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %159 = load i32, i32* %i, align 4, !tbaa !6
  %mul186 = mul nsw i32 2, %159
  %160 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul187 = mul i32 %mul186, %160
  %161 = load i32, i32* %j, align 4, !tbaa !6
  %add188 = add i32 %mul187, %161
  %arrayidx189 = getelementptr inbounds i8, i8* %158, i32 %add188
  %162 = load i8, i8* %arrayidx189, align 1, !tbaa !11
  %conv190 = zext i8 %162 to i32
  %163 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %164 = load i32, i32* %i, align 4, !tbaa !6
  %mul191 = mul nsw i32 2, %164
  %add192 = add nsw i32 %mul191, 1
  %165 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul193 = mul i32 %add192, %165
  %166 = load i32, i32* %j, align 4, !tbaa !6
  %add194 = add i32 %mul193, %166
  %arrayidx195 = getelementptr inbounds i8, i8* %163, i32 %add194
  %167 = load i8, i8* %arrayidx195, align 1, !tbaa !11
  %conv196 = zext i8 %167 to i32
  %add197 = add nsw i32 %conv190, %conv196
  %add198 = add nsw i32 %add197, 1
  %shr199 = ashr i32 %add198, 1
  store i32 %shr199, i32* %m185, align 4, !tbaa !6
  %168 = load i32, i32* %m185, align 4, !tbaa !6
  %169 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %170 = load i32, i32* %i, align 4, !tbaa !6
  %171 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul200 = mul i32 %170, %171
  %172 = load i32, i32* %j, align 4, !tbaa !6
  %add201 = add i32 %mul200, %172
  %arrayidx202 = getelementptr inbounds i16, i16* %169, i32 %add201
  %173 = load i16, i16* %arrayidx202, align 2, !tbaa !12
  %conv203 = zext i16 %173 to i32
  %mul204 = mul nsw i32 %168, %conv203
  %174 = load i32, i32* %m185, align 4, !tbaa !6
  %sub205 = sub nsw i32 64, %174
  %175 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %176 = load i32, i32* %i, align 4, !tbaa !6
  %177 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul206 = mul i32 %176, %177
  %178 = load i32, i32* %j, align 4, !tbaa !6
  %add207 = add i32 %mul206, %178
  %arrayidx208 = getelementptr inbounds i16, i16* %175, i32 %add207
  %179 = load i16, i16* %arrayidx208, align 2, !tbaa !12
  %conv209 = zext i16 %179 to i32
  %mul210 = mul nsw i32 %sub205, %conv209
  %add211 = add nsw i32 %mul204, %mul210
  %shr212 = ashr i32 %add211, 6
  store i32 %shr212, i32* %res184, align 4, !tbaa !6
  %180 = load i32, i32* %round_offset, align 4, !tbaa !6
  %181 = load i32, i32* %res184, align 4, !tbaa !6
  %sub213 = sub nsw i32 %181, %180
  store i32 %sub213, i32* %res184, align 4, !tbaa !6
  %182 = load i32, i32* %res184, align 4, !tbaa !6
  %183 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl214 = shl i32 1, %183
  %shr215 = ashr i32 %shl214, 1
  %add216 = add nsw i32 %182, %shr215
  %184 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr217 = ashr i32 %add216, %184
  %call218 = call zeroext i8 @clip_pixel(i32 %shr217)
  %185 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %186 = load i32, i32* %i, align 4, !tbaa !6
  %187 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul219 = mul i32 %186, %187
  %188 = load i32, i32* %j, align 4, !tbaa !6
  %add220 = add i32 %mul219, %188
  %arrayidx221 = getelementptr inbounds i8, i8* %185, i32 %add220
  store i8 %call218, i8* %arrayidx221, align 1, !tbaa !11
  %189 = bitcast i32* %m185 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #3
  %190 = bitcast i32* %res184 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #3
  br label %for.inc222

for.inc222:                                       ; preds = %for.body183
  %191 = load i32, i32* %j, align 4, !tbaa !6
  %inc223 = add nsw i32 %191, 1
  store i32 %inc223, i32* %j, align 4, !tbaa !6
  br label %for.cond180

for.end224:                                       ; preds = %for.cond180
  br label %for.inc225

for.inc225:                                       ; preds = %for.end224
  %192 = load i32, i32* %i, align 4, !tbaa !6
  %inc226 = add nsw i32 %192, 1
  store i32 %inc226, i32* %i, align 4, !tbaa !6
  br label %for.cond176

for.end227:                                       ; preds = %for.cond176
  br label %if.end

if.end:                                           ; preds = %for.end227, %for.end174
  br label %if.end228

if.end228:                                        ; preds = %if.end, %for.end115
  br label %if.end229

if.end229:                                        ; preds = %if.end228, %for.end38
  %193 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #3
  %194 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #3
  %195 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #3
  %196 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #3
  %197 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #3
  %198 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @clip_pixel(i32 %val) #2 {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  %0 = load i32, i32* %val.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %0, 255
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %1, 0
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %2 = load i32, i32* %val.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ 0, %cond.true2 ], [ %2, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ 255, %cond.true ], [ %cond, %cond.end ]
  %conv = trunc i32 %cond5 to i8
  ret i8 %conv
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_highbd_blend_a64_d16_mask_c(i8* %dst_8, i32 %dst_stride, i16* %src0, i32 %src0_stride, i16* %src1, i32 %src1_stride, i8* %mask, i32 %mask_stride, i32 %w, i32 %h, i32 %subw, i32 %subh, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %dst_8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src0.addr = alloca i16*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i16*, align 4
  %src1_stride.addr = alloca i32, align 4
  %mask.addr = alloca i8*, align 4
  %mask_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %subw.addr = alloca i32, align 4
  %subh.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %round_offset = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %dst = alloca i16*, align 4
  %saturation_value = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %res = alloca i32, align 4
  %m = alloca i32, align 4
  %v = alloca i32, align 4
  %i48 = alloca i32, align 4
  %j54 = alloca i32, align 4
  %res60 = alloca i32, align 4
  %m61 = alloca i32, align 4
  %v93 = alloca i32, align 4
  %i125 = alloca i32, align 4
  %j131 = alloca i32, align 4
  %res137 = alloca i32, align 4
  %m138 = alloca i32, align 4
  %v159 = alloca i32, align 4
  %i184 = alloca i32, align 4
  %j190 = alloca i32, align 4
  %res196 = alloca i32, align 4
  %m197 = alloca i32, align 4
  %v216 = alloca i32, align 4
  store i8* %dst_8, i8** %dst_8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i16* %src0, i16** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i16* %src1, i16** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %mask_stride, i32* %mask_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %subw, i32* %subw.addr, align 4, !tbaa !6
  store i32 %subh, i32* %subh.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add = add nsw i32 %1, 14
  %2 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %2, i32 0, i32 3
  %3 = load i32, i32* %round_0, align 4, !tbaa !8
  %sub = sub nsw i32 %add, %3
  store i32 %sub, i32* %offset_bits, align 4, !tbaa !6
  %4 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %6 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %6, i32 0, i32 4
  %7 = load i32, i32* %round_1, align 4, !tbaa !10
  %sub1 = sub nsw i32 %5, %7
  %shl = shl i32 1, %sub1
  %8 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %9 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_12 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %9, i32 0, i32 4
  %10 = load i32, i32* %round_12, align 4, !tbaa !10
  %sub3 = sub nsw i32 %8, %10
  %sub4 = sub nsw i32 %sub3, 1
  %shl5 = shl i32 1, %sub4
  %add6 = add nsw i32 %shl, %shl5
  store i32 %add6, i32* %round_offset, align 4, !tbaa !6
  %11 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_07 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %12, i32 0, i32 3
  %13 = load i32, i32* %round_07, align 4, !tbaa !8
  %sub8 = sub nsw i32 14, %13
  %14 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_19 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %14, i32 0, i32 4
  %15 = load i32, i32* %round_19, align 4, !tbaa !10
  %sub10 = sub nsw i32 %sub8, %15
  store i32 %sub10, i32* %round_bits, align 4, !tbaa !6
  %16 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load i8*, i8** %dst_8.addr, align 4, !tbaa !2
  %18 = ptrtoint i8* %17 to i32
  %shl11 = shl i32 %18, 1
  %19 = inttoptr i32 %shl11 to i16*
  store i16* %19, i16** %dst, align 4, !tbaa !2
  %20 = bitcast i32* %saturation_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load i32, i32* %bd.addr, align 4, !tbaa !6
  switch i32 %21, label %sw.default [
    i32 8, label %sw.bb
    i32 10, label %sw.bb12
    i32 12, label %sw.bb13
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb
  store i32 255, i32* %saturation_value, align 4, !tbaa !6
  br label %sw.epilog

sw.bb12:                                          ; preds = %entry
  store i32 1023, i32* %saturation_value, align 4, !tbaa !6
  br label %sw.epilog

sw.bb13:                                          ; preds = %entry
  store i32 4095, i32* %saturation_value, align 4, !tbaa !6
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb13, %sw.bb12, %sw.default
  %22 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %22, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %sw.epilog
  %23 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp14 = icmp eq i32 %23, 0
  br i1 %cmp14, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc39, %if.then
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %26 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp15 = icmp slt i32 %25, %26
  br i1 %cmp15, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 3, i32* %cleanup.dest.slot, align 4
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  br label %for.end41

for.body:                                         ; preds = %for.cond
  %28 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc, %for.body
  %29 = load i32, i32* %j, align 4, !tbaa !6
  %30 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp17 = icmp slt i32 %29, %30
  br i1 %cmp17, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond16
  store i32 6, i32* %cleanup.dest.slot, align 4
  %31 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  br label %for.end

for.body19:                                       ; preds = %for.cond16
  %32 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #3
  %34 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %35 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx, align 1, !tbaa !11
  %conv = zext i8 %36 to i32
  store i32 %conv, i32* %m, align 4, !tbaa !6
  %37 = load i32, i32* %m, align 4, !tbaa !6
  %38 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %39 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds i16, i16* %38, i32 %39
  %40 = load i16, i16* %arrayidx20, align 2, !tbaa !12
  %conv21 = zext i16 %40 to i32
  %mul = mul nsw i32 %37, %conv21
  %41 = load i32, i32* %m, align 4, !tbaa !6
  %sub22 = sub nsw i32 64, %41
  %42 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %43 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds i16, i16* %42, i32 %43
  %44 = load i16, i16* %arrayidx23, align 2, !tbaa !12
  %conv24 = zext i16 %44 to i32
  %mul25 = mul nsw i32 %sub22, %conv24
  %add26 = add nsw i32 %mul, %mul25
  %shr = ashr i32 %add26, 6
  store i32 %shr, i32* %res, align 4, !tbaa !6
  %45 = load i32, i32* %round_offset, align 4, !tbaa !6
  %46 = load i32, i32* %res, align 4, !tbaa !6
  %sub27 = sub nsw i32 %46, %45
  store i32 %sub27, i32* %res, align 4, !tbaa !6
  %47 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #3
  %48 = load i32, i32* %res, align 4, !tbaa !6
  %49 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl28 = shl i32 1, %49
  %shr29 = ashr i32 %shl28, 1
  %add30 = add nsw i32 %48, %shr29
  %50 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr31 = ashr i32 %add30, %50
  %call = call i32 @negative_to_zero(i32 %shr31)
  store i32 %call, i32* %v, align 4, !tbaa !6
  %51 = load i32, i32* %v, align 4, !tbaa !6
  %52 = load i32, i32* %saturation_value, align 4, !tbaa !6
  %cmp32 = icmp ult i32 %51, %52
  br i1 %cmp32, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body19
  %53 = load i32, i32* %v, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body19
  %54 = load i32, i32* %saturation_value, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %53, %cond.true ], [ %54, %cond.false ]
  %conv34 = trunc i32 %cond to i16
  %55 = load i16*, i16** %dst, align 4, !tbaa !2
  %56 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds i16, i16* %55, i32 %56
  store i16 %conv34, i16* %arrayidx35, align 2, !tbaa !12
  %57 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  %59 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %60 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %60, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond16

for.end:                                          ; preds = %for.cond.cleanup18
  %61 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %62 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %62, i32 %61
  store i8* %add.ptr, i8** %mask.addr, align 4, !tbaa !2
  %63 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %64 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %add.ptr36 = getelementptr inbounds i16, i16* %64, i32 %63
  store i16* %add.ptr36, i16** %src0.addr, align 4, !tbaa !2
  %65 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %66 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %add.ptr37 = getelementptr inbounds i16, i16* %66, i32 %65
  store i16* %add.ptr37, i16** %src1.addr, align 4, !tbaa !2
  %67 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %68 = load i16*, i16** %dst, align 4, !tbaa !2
  %add.ptr38 = getelementptr inbounds i16, i16* %68, i32 %67
  store i16* %add.ptr38, i16** %dst, align 4, !tbaa !2
  br label %for.inc39

for.inc39:                                        ; preds = %for.end
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %inc40 = add nsw i32 %69, 1
  store i32 %inc40, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end41:                                        ; preds = %for.cond.cleanup
  br label %if.end242

if.else:                                          ; preds = %land.lhs.true, %sw.epilog
  %70 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp42 = icmp eq i32 %70, 1
  br i1 %cmp42, label %land.lhs.true44, label %if.else118

land.lhs.true44:                                  ; preds = %if.else
  %71 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp45 = icmp eq i32 %71, 1
  br i1 %cmp45, label %if.then47, label %if.else118

if.then47:                                        ; preds = %land.lhs.true44
  %72 = bitcast i32* %i48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #3
  store i32 0, i32* %i48, align 4, !tbaa !6
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc115, %if.then47
  %73 = load i32, i32* %i48, align 4, !tbaa !6
  %74 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp50 = icmp slt i32 %73, %74
  br i1 %cmp50, label %for.body53, label %for.cond.cleanup52

for.cond.cleanup52:                               ; preds = %for.cond49
  store i32 9, i32* %cleanup.dest.slot, align 4
  %75 = bitcast i32* %i48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  br label %for.end117

for.body53:                                       ; preds = %for.cond49
  %76 = bitcast i32* %j54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #3
  store i32 0, i32* %j54, align 4, !tbaa !6
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc107, %for.body53
  %77 = load i32, i32* %j54, align 4, !tbaa !6
  %78 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp56 = icmp slt i32 %77, %78
  br i1 %cmp56, label %for.body59, label %for.cond.cleanup58

for.cond.cleanup58:                               ; preds = %for.cond55
  store i32 12, i32* %cleanup.dest.slot, align 4
  %79 = bitcast i32* %j54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  br label %for.end109

for.body59:                                       ; preds = %for.cond55
  %80 = bitcast i32* %res60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #3
  %81 = bitcast i32* %m61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #3
  %82 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %83 = load i32, i32* %j54, align 4, !tbaa !6
  %mul62 = mul nsw i32 2, %83
  %arrayidx63 = getelementptr inbounds i8, i8* %82, i32 %mul62
  %84 = load i8, i8* %arrayidx63, align 1, !tbaa !11
  %conv64 = zext i8 %84 to i32
  %85 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %86 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %87 = load i32, i32* %j54, align 4, !tbaa !6
  %mul65 = mul nsw i32 2, %87
  %add66 = add i32 %86, %mul65
  %arrayidx67 = getelementptr inbounds i8, i8* %85, i32 %add66
  %88 = load i8, i8* %arrayidx67, align 1, !tbaa !11
  %conv68 = zext i8 %88 to i32
  %add69 = add nsw i32 %conv64, %conv68
  %89 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %90 = load i32, i32* %j54, align 4, !tbaa !6
  %mul70 = mul nsw i32 2, %90
  %add71 = add nsw i32 %mul70, 1
  %arrayidx72 = getelementptr inbounds i8, i8* %89, i32 %add71
  %91 = load i8, i8* %arrayidx72, align 1, !tbaa !11
  %conv73 = zext i8 %91 to i32
  %add74 = add nsw i32 %add69, %conv73
  %92 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %93 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %94 = load i32, i32* %j54, align 4, !tbaa !6
  %mul75 = mul nsw i32 2, %94
  %add76 = add i32 %93, %mul75
  %add77 = add i32 %add76, 1
  %arrayidx78 = getelementptr inbounds i8, i8* %92, i32 %add77
  %95 = load i8, i8* %arrayidx78, align 1, !tbaa !11
  %conv79 = zext i8 %95 to i32
  %add80 = add nsw i32 %add74, %conv79
  %add81 = add nsw i32 %add80, 2
  %shr82 = ashr i32 %add81, 2
  store i32 %shr82, i32* %m61, align 4, !tbaa !6
  %96 = load i32, i32* %m61, align 4, !tbaa !6
  %97 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %98 = load i32, i32* %j54, align 4, !tbaa !6
  %arrayidx83 = getelementptr inbounds i16, i16* %97, i32 %98
  %99 = load i16, i16* %arrayidx83, align 2, !tbaa !12
  %conv84 = zext i16 %99 to i32
  %mul85 = mul nsw i32 %96, %conv84
  %100 = load i32, i32* %m61, align 4, !tbaa !6
  %sub86 = sub nsw i32 64, %100
  %101 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %102 = load i32, i32* %j54, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds i16, i16* %101, i32 %102
  %103 = load i16, i16* %arrayidx87, align 2, !tbaa !12
  %conv88 = zext i16 %103 to i32
  %mul89 = mul nsw i32 %sub86, %conv88
  %add90 = add nsw i32 %mul85, %mul89
  %shr91 = ashr i32 %add90, 6
  store i32 %shr91, i32* %res60, align 4, !tbaa !6
  %104 = load i32, i32* %round_offset, align 4, !tbaa !6
  %105 = load i32, i32* %res60, align 4, !tbaa !6
  %sub92 = sub nsw i32 %105, %104
  store i32 %sub92, i32* %res60, align 4, !tbaa !6
  %106 = bitcast i32* %v93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #3
  %107 = load i32, i32* %res60, align 4, !tbaa !6
  %108 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl94 = shl i32 1, %108
  %shr95 = ashr i32 %shl94, 1
  %add96 = add nsw i32 %107, %shr95
  %109 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr97 = ashr i32 %add96, %109
  %call98 = call i32 @negative_to_zero(i32 %shr97)
  store i32 %call98, i32* %v93, align 4, !tbaa !6
  %110 = load i32, i32* %v93, align 4, !tbaa !6
  %111 = load i32, i32* %saturation_value, align 4, !tbaa !6
  %cmp99 = icmp ult i32 %110, %111
  br i1 %cmp99, label %cond.true101, label %cond.false102

cond.true101:                                     ; preds = %for.body59
  %112 = load i32, i32* %v93, align 4, !tbaa !6
  br label %cond.end103

cond.false102:                                    ; preds = %for.body59
  %113 = load i32, i32* %saturation_value, align 4, !tbaa !6
  br label %cond.end103

cond.end103:                                      ; preds = %cond.false102, %cond.true101
  %cond104 = phi i32 [ %112, %cond.true101 ], [ %113, %cond.false102 ]
  %conv105 = trunc i32 %cond104 to i16
  %114 = load i16*, i16** %dst, align 4, !tbaa !2
  %115 = load i32, i32* %j54, align 4, !tbaa !6
  %arrayidx106 = getelementptr inbounds i16, i16* %114, i32 %115
  store i16 %conv105, i16* %arrayidx106, align 2, !tbaa !12
  %116 = bitcast i32* %v93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  %117 = bitcast i32* %m61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast i32* %res60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  br label %for.inc107

for.inc107:                                       ; preds = %cond.end103
  %119 = load i32, i32* %j54, align 4, !tbaa !6
  %inc108 = add nsw i32 %119, 1
  store i32 %inc108, i32* %j54, align 4, !tbaa !6
  br label %for.cond55

for.end109:                                       ; preds = %for.cond.cleanup58
  %120 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul110 = mul i32 2, %120
  %121 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr111 = getelementptr inbounds i8, i8* %121, i32 %mul110
  store i8* %add.ptr111, i8** %mask.addr, align 4, !tbaa !2
  %122 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %123 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %add.ptr112 = getelementptr inbounds i16, i16* %123, i32 %122
  store i16* %add.ptr112, i16** %src0.addr, align 4, !tbaa !2
  %124 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %125 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %add.ptr113 = getelementptr inbounds i16, i16* %125, i32 %124
  store i16* %add.ptr113, i16** %src1.addr, align 4, !tbaa !2
  %126 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %127 = load i16*, i16** %dst, align 4, !tbaa !2
  %add.ptr114 = getelementptr inbounds i16, i16* %127, i32 %126
  store i16* %add.ptr114, i16** %dst, align 4, !tbaa !2
  br label %for.inc115

for.inc115:                                       ; preds = %for.end109
  %128 = load i32, i32* %i48, align 4, !tbaa !6
  %inc116 = add nsw i32 %128, 1
  store i32 %inc116, i32* %i48, align 4, !tbaa !6
  br label %for.cond49

for.end117:                                       ; preds = %for.cond.cleanup52
  br label %if.end241

if.else118:                                       ; preds = %land.lhs.true44, %if.else
  %129 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp119 = icmp eq i32 %129, 1
  br i1 %cmp119, label %land.lhs.true121, label %if.else183

land.lhs.true121:                                 ; preds = %if.else118
  %130 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp122 = icmp eq i32 %130, 0
  br i1 %cmp122, label %if.then124, label %if.else183

if.then124:                                       ; preds = %land.lhs.true121
  %131 = bitcast i32* %i125 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #3
  store i32 0, i32* %i125, align 4, !tbaa !6
  br label %for.cond126

for.cond126:                                      ; preds = %for.inc180, %if.then124
  %132 = load i32, i32* %i125, align 4, !tbaa !6
  %133 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp127 = icmp slt i32 %132, %133
  br i1 %cmp127, label %for.body130, label %for.cond.cleanup129

for.cond.cleanup129:                              ; preds = %for.cond126
  store i32 15, i32* %cleanup.dest.slot, align 4
  %134 = bitcast i32* %i125 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #3
  br label %for.end182

for.body130:                                      ; preds = %for.cond126
  %135 = bitcast i32* %j131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #3
  store i32 0, i32* %j131, align 4, !tbaa !6
  br label %for.cond132

for.cond132:                                      ; preds = %for.inc173, %for.body130
  %136 = load i32, i32* %j131, align 4, !tbaa !6
  %137 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp133 = icmp slt i32 %136, %137
  br i1 %cmp133, label %for.body136, label %for.cond.cleanup135

for.cond.cleanup135:                              ; preds = %for.cond132
  store i32 18, i32* %cleanup.dest.slot, align 4
  %138 = bitcast i32* %j131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #3
  br label %for.end175

for.body136:                                      ; preds = %for.cond132
  %139 = bitcast i32* %res137 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #3
  %140 = bitcast i32* %m138 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #3
  %141 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %142 = load i32, i32* %j131, align 4, !tbaa !6
  %mul139 = mul nsw i32 2, %142
  %arrayidx140 = getelementptr inbounds i8, i8* %141, i32 %mul139
  %143 = load i8, i8* %arrayidx140, align 1, !tbaa !11
  %conv141 = zext i8 %143 to i32
  %144 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %145 = load i32, i32* %j131, align 4, !tbaa !6
  %mul142 = mul nsw i32 2, %145
  %add143 = add nsw i32 %mul142, 1
  %arrayidx144 = getelementptr inbounds i8, i8* %144, i32 %add143
  %146 = load i8, i8* %arrayidx144, align 1, !tbaa !11
  %conv145 = zext i8 %146 to i32
  %add146 = add nsw i32 %conv141, %conv145
  %add147 = add nsw i32 %add146, 1
  %shr148 = ashr i32 %add147, 1
  store i32 %shr148, i32* %m138, align 4, !tbaa !6
  %147 = load i32, i32* %m138, align 4, !tbaa !6
  %148 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %149 = load i32, i32* %j131, align 4, !tbaa !6
  %arrayidx149 = getelementptr inbounds i16, i16* %148, i32 %149
  %150 = load i16, i16* %arrayidx149, align 2, !tbaa !12
  %conv150 = zext i16 %150 to i32
  %mul151 = mul nsw i32 %147, %conv150
  %151 = load i32, i32* %m138, align 4, !tbaa !6
  %sub152 = sub nsw i32 64, %151
  %152 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %153 = load i32, i32* %j131, align 4, !tbaa !6
  %arrayidx153 = getelementptr inbounds i16, i16* %152, i32 %153
  %154 = load i16, i16* %arrayidx153, align 2, !tbaa !12
  %conv154 = zext i16 %154 to i32
  %mul155 = mul nsw i32 %sub152, %conv154
  %add156 = add nsw i32 %mul151, %mul155
  %shr157 = ashr i32 %add156, 6
  store i32 %shr157, i32* %res137, align 4, !tbaa !6
  %155 = load i32, i32* %round_offset, align 4, !tbaa !6
  %156 = load i32, i32* %res137, align 4, !tbaa !6
  %sub158 = sub nsw i32 %156, %155
  store i32 %sub158, i32* %res137, align 4, !tbaa !6
  %157 = bitcast i32* %v159 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #3
  %158 = load i32, i32* %res137, align 4, !tbaa !6
  %159 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl160 = shl i32 1, %159
  %shr161 = ashr i32 %shl160, 1
  %add162 = add nsw i32 %158, %shr161
  %160 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr163 = ashr i32 %add162, %160
  %call164 = call i32 @negative_to_zero(i32 %shr163)
  store i32 %call164, i32* %v159, align 4, !tbaa !6
  %161 = load i32, i32* %v159, align 4, !tbaa !6
  %162 = load i32, i32* %saturation_value, align 4, !tbaa !6
  %cmp165 = icmp ult i32 %161, %162
  br i1 %cmp165, label %cond.true167, label %cond.false168

cond.true167:                                     ; preds = %for.body136
  %163 = load i32, i32* %v159, align 4, !tbaa !6
  br label %cond.end169

cond.false168:                                    ; preds = %for.body136
  %164 = load i32, i32* %saturation_value, align 4, !tbaa !6
  br label %cond.end169

cond.end169:                                      ; preds = %cond.false168, %cond.true167
  %cond170 = phi i32 [ %163, %cond.true167 ], [ %164, %cond.false168 ]
  %conv171 = trunc i32 %cond170 to i16
  %165 = load i16*, i16** %dst, align 4, !tbaa !2
  %166 = load i32, i32* %j131, align 4, !tbaa !6
  %arrayidx172 = getelementptr inbounds i16, i16* %165, i32 %166
  store i16 %conv171, i16* %arrayidx172, align 2, !tbaa !12
  %167 = bitcast i32* %v159 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #3
  %168 = bitcast i32* %m138 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #3
  %169 = bitcast i32* %res137 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #3
  br label %for.inc173

for.inc173:                                       ; preds = %cond.end169
  %170 = load i32, i32* %j131, align 4, !tbaa !6
  %inc174 = add nsw i32 %170, 1
  store i32 %inc174, i32* %j131, align 4, !tbaa !6
  br label %for.cond132

for.end175:                                       ; preds = %for.cond.cleanup135
  %171 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %172 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr176 = getelementptr inbounds i8, i8* %172, i32 %171
  store i8* %add.ptr176, i8** %mask.addr, align 4, !tbaa !2
  %173 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %174 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %add.ptr177 = getelementptr inbounds i16, i16* %174, i32 %173
  store i16* %add.ptr177, i16** %src0.addr, align 4, !tbaa !2
  %175 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %176 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %add.ptr178 = getelementptr inbounds i16, i16* %176, i32 %175
  store i16* %add.ptr178, i16** %src1.addr, align 4, !tbaa !2
  %177 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %178 = load i16*, i16** %dst, align 4, !tbaa !2
  %add.ptr179 = getelementptr inbounds i16, i16* %178, i32 %177
  store i16* %add.ptr179, i16** %dst, align 4, !tbaa !2
  br label %for.inc180

for.inc180:                                       ; preds = %for.end175
  %179 = load i32, i32* %i125, align 4, !tbaa !6
  %inc181 = add nsw i32 %179, 1
  store i32 %inc181, i32* %i125, align 4, !tbaa !6
  br label %for.cond126

for.end182:                                       ; preds = %for.cond.cleanup129
  br label %if.end

if.else183:                                       ; preds = %land.lhs.true121, %if.else118
  %180 = bitcast i32* %i184 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %180) #3
  store i32 0, i32* %i184, align 4, !tbaa !6
  br label %for.cond185

for.cond185:                                      ; preds = %for.inc238, %if.else183
  %181 = load i32, i32* %i184, align 4, !tbaa !6
  %182 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp186 = icmp slt i32 %181, %182
  br i1 %cmp186, label %for.body189, label %for.cond.cleanup188

for.cond.cleanup188:                              ; preds = %for.cond185
  store i32 21, i32* %cleanup.dest.slot, align 4
  %183 = bitcast i32* %i184 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #3
  br label %for.end240

for.body189:                                      ; preds = %for.cond185
  %184 = bitcast i32* %j190 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %184) #3
  store i32 0, i32* %j190, align 4, !tbaa !6
  br label %for.cond191

for.cond191:                                      ; preds = %for.inc230, %for.body189
  %185 = load i32, i32* %j190, align 4, !tbaa !6
  %186 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp192 = icmp slt i32 %185, %186
  br i1 %cmp192, label %for.body195, label %for.cond.cleanup194

for.cond.cleanup194:                              ; preds = %for.cond191
  store i32 24, i32* %cleanup.dest.slot, align 4
  %187 = bitcast i32* %j190 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #3
  br label %for.end232

for.body195:                                      ; preds = %for.cond191
  %188 = bitcast i32* %res196 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %188) #3
  %189 = bitcast i32* %m197 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %189) #3
  %190 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %191 = load i32, i32* %j190, align 4, !tbaa !6
  %arrayidx198 = getelementptr inbounds i8, i8* %190, i32 %191
  %192 = load i8, i8* %arrayidx198, align 1, !tbaa !11
  %conv199 = zext i8 %192 to i32
  %193 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %194 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %195 = load i32, i32* %j190, align 4, !tbaa !6
  %add200 = add i32 %194, %195
  %arrayidx201 = getelementptr inbounds i8, i8* %193, i32 %add200
  %196 = load i8, i8* %arrayidx201, align 1, !tbaa !11
  %conv202 = zext i8 %196 to i32
  %add203 = add nsw i32 %conv199, %conv202
  %add204 = add nsw i32 %add203, 1
  %shr205 = ashr i32 %add204, 1
  store i32 %shr205, i32* %m197, align 4, !tbaa !6
  %197 = load i32, i32* %m197, align 4, !tbaa !6
  %198 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %199 = load i32, i32* %j190, align 4, !tbaa !6
  %arrayidx206 = getelementptr inbounds i16, i16* %198, i32 %199
  %200 = load i16, i16* %arrayidx206, align 2, !tbaa !12
  %conv207 = zext i16 %200 to i32
  %mul208 = mul nsw i32 %197, %conv207
  %201 = load i32, i32* %m197, align 4, !tbaa !6
  %sub209 = sub nsw i32 64, %201
  %202 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %203 = load i32, i32* %j190, align 4, !tbaa !6
  %arrayidx210 = getelementptr inbounds i16, i16* %202, i32 %203
  %204 = load i16, i16* %arrayidx210, align 2, !tbaa !12
  %conv211 = zext i16 %204 to i32
  %mul212 = mul nsw i32 %sub209, %conv211
  %add213 = add nsw i32 %mul208, %mul212
  %shr214 = ashr i32 %add213, 6
  store i32 %shr214, i32* %res196, align 4, !tbaa !6
  %205 = load i32, i32* %round_offset, align 4, !tbaa !6
  %206 = load i32, i32* %res196, align 4, !tbaa !6
  %sub215 = sub nsw i32 %206, %205
  store i32 %sub215, i32* %res196, align 4, !tbaa !6
  %207 = bitcast i32* %v216 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %207) #3
  %208 = load i32, i32* %res196, align 4, !tbaa !6
  %209 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl217 = shl i32 1, %209
  %shr218 = ashr i32 %shl217, 1
  %add219 = add nsw i32 %208, %shr218
  %210 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr220 = ashr i32 %add219, %210
  %call221 = call i32 @negative_to_zero(i32 %shr220)
  store i32 %call221, i32* %v216, align 4, !tbaa !6
  %211 = load i32, i32* %v216, align 4, !tbaa !6
  %212 = load i32, i32* %saturation_value, align 4, !tbaa !6
  %cmp222 = icmp ult i32 %211, %212
  br i1 %cmp222, label %cond.true224, label %cond.false225

cond.true224:                                     ; preds = %for.body195
  %213 = load i32, i32* %v216, align 4, !tbaa !6
  br label %cond.end226

cond.false225:                                    ; preds = %for.body195
  %214 = load i32, i32* %saturation_value, align 4, !tbaa !6
  br label %cond.end226

cond.end226:                                      ; preds = %cond.false225, %cond.true224
  %cond227 = phi i32 [ %213, %cond.true224 ], [ %214, %cond.false225 ]
  %conv228 = trunc i32 %cond227 to i16
  %215 = load i16*, i16** %dst, align 4, !tbaa !2
  %216 = load i32, i32* %j190, align 4, !tbaa !6
  %arrayidx229 = getelementptr inbounds i16, i16* %215, i32 %216
  store i16 %conv228, i16* %arrayidx229, align 2, !tbaa !12
  %217 = bitcast i32* %v216 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #3
  %218 = bitcast i32* %m197 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #3
  %219 = bitcast i32* %res196 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #3
  br label %for.inc230

for.inc230:                                       ; preds = %cond.end226
  %220 = load i32, i32* %j190, align 4, !tbaa !6
  %inc231 = add nsw i32 %220, 1
  store i32 %inc231, i32* %j190, align 4, !tbaa !6
  br label %for.cond191

for.end232:                                       ; preds = %for.cond.cleanup194
  %221 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul233 = mul i32 2, %221
  %222 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr234 = getelementptr inbounds i8, i8* %222, i32 %mul233
  store i8* %add.ptr234, i8** %mask.addr, align 4, !tbaa !2
  %223 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %224 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %add.ptr235 = getelementptr inbounds i16, i16* %224, i32 %223
  store i16* %add.ptr235, i16** %src0.addr, align 4, !tbaa !2
  %225 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %226 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %add.ptr236 = getelementptr inbounds i16, i16* %226, i32 %225
  store i16* %add.ptr236, i16** %src1.addr, align 4, !tbaa !2
  %227 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %228 = load i16*, i16** %dst, align 4, !tbaa !2
  %add.ptr237 = getelementptr inbounds i16, i16* %228, i32 %227
  store i16* %add.ptr237, i16** %dst, align 4, !tbaa !2
  br label %for.inc238

for.inc238:                                       ; preds = %for.end232
  %229 = load i32, i32* %i184, align 4, !tbaa !6
  %inc239 = add nsw i32 %229, 1
  store i32 %inc239, i32* %i184, align 4, !tbaa !6
  br label %for.cond185

for.end240:                                       ; preds = %for.cond.cleanup188
  br label %if.end

if.end:                                           ; preds = %for.end240, %for.end182
  br label %if.end241

if.end241:                                        ; preds = %if.end, %for.end117
  br label %if.end242

if.end242:                                        ; preds = %if.end241, %for.end41
  %230 = bitcast i32* %saturation_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #3
  %231 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #3
  %232 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #3
  %233 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #3
  %234 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #3
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @negative_to_zero(i32 %value) #2 {
entry:
  %value.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %value.addr, align 4, !tbaa !6
  %shr = ashr i32 %1, 31
  %neg = xor i32 %shr, -1
  %and = and i32 %0, %neg
  ret i32 %and
}

; Function Attrs: nounwind
define hidden void @aom_blend_a64_mask_c(i8* %dst, i32 %dst_stride, i8* %src0, i32 %src0_stride, i8* %src1, i32 %src1_stride, i8* %mask, i32 %mask_stride, i32 %w, i32 %h, i32 %subw, i32 %subh) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src0.addr = alloca i8*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i8*, align 4
  %src1_stride.addr = alloca i32, align 4
  %mask.addr = alloca i8*, align 4
  %mask_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %subw.addr = alloca i32, align 4
  %subh.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %m = alloca i32, align 4
  %m39 = alloca i32, align 4
  %m112 = alloca i32, align 4
  %m160 = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i8* %src0, i8** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i8* %src1, i8** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %mask_stride, i32* %mask_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %subw, i32* %subw.addr, align 4, !tbaa !6
  store i32 %subh, i32* %subh.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %3 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp1 = icmp eq i32 %3, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc22, %if.then
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %5 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %4, %5
  br i1 %cmp2, label %for.body, label %for.end24

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %7 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %6, %7
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond3
  %8 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul = mul i32 %10, %11
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %add = add i32 %mul, %12
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %add
  %13 = load i8, i8* %arrayidx, align 1, !tbaa !11
  %conv = zext i8 %13 to i32
  store i32 %conv, i32* %m, align 4, !tbaa !6
  %14 = load i32, i32* %m, align 4, !tbaa !6
  %15 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul6 = mul i32 %16, %17
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %add7 = add i32 %mul6, %18
  %arrayidx8 = getelementptr inbounds i8, i8* %15, i32 %add7
  %19 = load i8, i8* %arrayidx8, align 1, !tbaa !11
  %conv9 = zext i8 %19 to i32
  %mul10 = mul nsw i32 %14, %conv9
  %20 = load i32, i32* %m, align 4, !tbaa !6
  %sub = sub nsw i32 64, %20
  %21 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %23 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul11 = mul i32 %22, %23
  %24 = load i32, i32* %j, align 4, !tbaa !6
  %add12 = add i32 %mul11, %24
  %arrayidx13 = getelementptr inbounds i8, i8* %21, i32 %add12
  %25 = load i8, i8* %arrayidx13, align 1, !tbaa !11
  %conv14 = zext i8 %25 to i32
  %mul15 = mul nsw i32 %sub, %conv14
  %add16 = add nsw i32 %mul10, %mul15
  %add17 = add nsw i32 %add16, 32
  %shr = ashr i32 %add17, 6
  %conv18 = trunc i32 %shr to i8
  %26 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %28 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul19 = mul i32 %27, %28
  %29 = load i32, i32* %j, align 4, !tbaa !6
  %add20 = add i32 %mul19, %29
  %arrayidx21 = getelementptr inbounds i8, i8* %26, i32 %add20
  store i8 %conv18, i8* %arrayidx21, align 1, !tbaa !11
  %30 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %inc23 = add nsw i32 %32, 1
  store i32 %inc23, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end24:                                        ; preds = %for.cond
  br label %if.end200

if.else:                                          ; preds = %land.lhs.true, %entry
  %33 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp25 = icmp eq i32 %33, 1
  br i1 %cmp25, label %land.lhs.true27, label %if.else97

land.lhs.true27:                                  ; preds = %if.else
  %34 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp28 = icmp eq i32 %34, 1
  br i1 %cmp28, label %if.then30, label %if.else97

if.then30:                                        ; preds = %land.lhs.true27
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc94, %if.then30
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %36 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp32 = icmp slt i32 %35, %36
  br i1 %cmp32, label %for.body34, label %for.end96

for.body34:                                       ; preds = %for.cond31
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc91, %for.body34
  %37 = load i32, i32* %j, align 4, !tbaa !6
  %38 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp36 = icmp slt i32 %37, %38
  br i1 %cmp36, label %for.body38, label %for.end93

for.body38:                                       ; preds = %for.cond35
  %39 = bitcast i32* %m39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #3
  %40 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %mul40 = mul nsw i32 2, %41
  %42 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul41 = mul i32 %mul40, %42
  %43 = load i32, i32* %j, align 4, !tbaa !6
  %mul42 = mul nsw i32 2, %43
  %add43 = add i32 %mul41, %mul42
  %arrayidx44 = getelementptr inbounds i8, i8* %40, i32 %add43
  %44 = load i8, i8* %arrayidx44, align 1, !tbaa !11
  %conv45 = zext i8 %44 to i32
  %45 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %mul46 = mul nsw i32 2, %46
  %add47 = add nsw i32 %mul46, 1
  %47 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul48 = mul i32 %add47, %47
  %48 = load i32, i32* %j, align 4, !tbaa !6
  %mul49 = mul nsw i32 2, %48
  %add50 = add i32 %mul48, %mul49
  %arrayidx51 = getelementptr inbounds i8, i8* %45, i32 %add50
  %49 = load i8, i8* %arrayidx51, align 1, !tbaa !11
  %conv52 = zext i8 %49 to i32
  %add53 = add nsw i32 %conv45, %conv52
  %50 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %mul54 = mul nsw i32 2, %51
  %52 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul55 = mul i32 %mul54, %52
  %53 = load i32, i32* %j, align 4, !tbaa !6
  %mul56 = mul nsw i32 2, %53
  %add57 = add nsw i32 %mul56, 1
  %add58 = add i32 %mul55, %add57
  %arrayidx59 = getelementptr inbounds i8, i8* %50, i32 %add58
  %54 = load i8, i8* %arrayidx59, align 1, !tbaa !11
  %conv60 = zext i8 %54 to i32
  %add61 = add nsw i32 %add53, %conv60
  %55 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %56 = load i32, i32* %i, align 4, !tbaa !6
  %mul62 = mul nsw i32 2, %56
  %add63 = add nsw i32 %mul62, 1
  %57 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul64 = mul i32 %add63, %57
  %58 = load i32, i32* %j, align 4, !tbaa !6
  %mul65 = mul nsw i32 2, %58
  %add66 = add nsw i32 %mul65, 1
  %add67 = add i32 %mul64, %add66
  %arrayidx68 = getelementptr inbounds i8, i8* %55, i32 %add67
  %59 = load i8, i8* %arrayidx68, align 1, !tbaa !11
  %conv69 = zext i8 %59 to i32
  %add70 = add nsw i32 %add61, %conv69
  %add71 = add nsw i32 %add70, 2
  %shr72 = ashr i32 %add71, 2
  store i32 %shr72, i32* %m39, align 4, !tbaa !6
  %60 = load i32, i32* %m39, align 4, !tbaa !6
  %61 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %62 = load i32, i32* %i, align 4, !tbaa !6
  %63 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul73 = mul i32 %62, %63
  %64 = load i32, i32* %j, align 4, !tbaa !6
  %add74 = add i32 %mul73, %64
  %arrayidx75 = getelementptr inbounds i8, i8* %61, i32 %add74
  %65 = load i8, i8* %arrayidx75, align 1, !tbaa !11
  %conv76 = zext i8 %65 to i32
  %mul77 = mul nsw i32 %60, %conv76
  %66 = load i32, i32* %m39, align 4, !tbaa !6
  %sub78 = sub nsw i32 64, %66
  %67 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %68 = load i32, i32* %i, align 4, !tbaa !6
  %69 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul79 = mul i32 %68, %69
  %70 = load i32, i32* %j, align 4, !tbaa !6
  %add80 = add i32 %mul79, %70
  %arrayidx81 = getelementptr inbounds i8, i8* %67, i32 %add80
  %71 = load i8, i8* %arrayidx81, align 1, !tbaa !11
  %conv82 = zext i8 %71 to i32
  %mul83 = mul nsw i32 %sub78, %conv82
  %add84 = add nsw i32 %mul77, %mul83
  %add85 = add nsw i32 %add84, 32
  %shr86 = ashr i32 %add85, 6
  %conv87 = trunc i32 %shr86 to i8
  %72 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %73 = load i32, i32* %i, align 4, !tbaa !6
  %74 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul88 = mul i32 %73, %74
  %75 = load i32, i32* %j, align 4, !tbaa !6
  %add89 = add i32 %mul88, %75
  %arrayidx90 = getelementptr inbounds i8, i8* %72, i32 %add89
  store i8 %conv87, i8* %arrayidx90, align 1, !tbaa !11
  %76 = bitcast i32* %m39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  br label %for.inc91

for.inc91:                                        ; preds = %for.body38
  %77 = load i32, i32* %j, align 4, !tbaa !6
  %inc92 = add nsw i32 %77, 1
  store i32 %inc92, i32* %j, align 4, !tbaa !6
  br label %for.cond35

for.end93:                                        ; preds = %for.cond35
  br label %for.inc94

for.inc94:                                        ; preds = %for.end93
  %78 = load i32, i32* %i, align 4, !tbaa !6
  %inc95 = add nsw i32 %78, 1
  store i32 %inc95, i32* %i, align 4, !tbaa !6
  br label %for.cond31

for.end96:                                        ; preds = %for.cond31
  br label %if.end199

if.else97:                                        ; preds = %land.lhs.true27, %if.else
  %79 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp98 = icmp eq i32 %79, 1
  br i1 %cmp98, label %land.lhs.true100, label %if.else151

land.lhs.true100:                                 ; preds = %if.else97
  %80 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp101 = icmp eq i32 %80, 0
  br i1 %cmp101, label %if.then103, label %if.else151

if.then103:                                       ; preds = %land.lhs.true100
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond104

for.cond104:                                      ; preds = %for.inc148, %if.then103
  %81 = load i32, i32* %i, align 4, !tbaa !6
  %82 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp105 = icmp slt i32 %81, %82
  br i1 %cmp105, label %for.body107, label %for.end150

for.body107:                                      ; preds = %for.cond104
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond108

for.cond108:                                      ; preds = %for.inc145, %for.body107
  %83 = load i32, i32* %j, align 4, !tbaa !6
  %84 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp109 = icmp slt i32 %83, %84
  br i1 %cmp109, label %for.body111, label %for.end147

for.body111:                                      ; preds = %for.cond108
  %85 = bitcast i32* %m112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #3
  %86 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %87 = load i32, i32* %i, align 4, !tbaa !6
  %88 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul113 = mul i32 %87, %88
  %89 = load i32, i32* %j, align 4, !tbaa !6
  %mul114 = mul nsw i32 2, %89
  %add115 = add i32 %mul113, %mul114
  %arrayidx116 = getelementptr inbounds i8, i8* %86, i32 %add115
  %90 = load i8, i8* %arrayidx116, align 1, !tbaa !11
  %conv117 = zext i8 %90 to i32
  %91 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %92 = load i32, i32* %i, align 4, !tbaa !6
  %93 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul118 = mul i32 %92, %93
  %94 = load i32, i32* %j, align 4, !tbaa !6
  %mul119 = mul nsw i32 2, %94
  %add120 = add nsw i32 %mul119, 1
  %add121 = add i32 %mul118, %add120
  %arrayidx122 = getelementptr inbounds i8, i8* %91, i32 %add121
  %95 = load i8, i8* %arrayidx122, align 1, !tbaa !11
  %conv123 = zext i8 %95 to i32
  %add124 = add nsw i32 %conv117, %conv123
  %add125 = add nsw i32 %add124, 1
  %shr126 = ashr i32 %add125, 1
  store i32 %shr126, i32* %m112, align 4, !tbaa !6
  %96 = load i32, i32* %m112, align 4, !tbaa !6
  %97 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %98 = load i32, i32* %i, align 4, !tbaa !6
  %99 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul127 = mul i32 %98, %99
  %100 = load i32, i32* %j, align 4, !tbaa !6
  %add128 = add i32 %mul127, %100
  %arrayidx129 = getelementptr inbounds i8, i8* %97, i32 %add128
  %101 = load i8, i8* %arrayidx129, align 1, !tbaa !11
  %conv130 = zext i8 %101 to i32
  %mul131 = mul nsw i32 %96, %conv130
  %102 = load i32, i32* %m112, align 4, !tbaa !6
  %sub132 = sub nsw i32 64, %102
  %103 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %104 = load i32, i32* %i, align 4, !tbaa !6
  %105 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul133 = mul i32 %104, %105
  %106 = load i32, i32* %j, align 4, !tbaa !6
  %add134 = add i32 %mul133, %106
  %arrayidx135 = getelementptr inbounds i8, i8* %103, i32 %add134
  %107 = load i8, i8* %arrayidx135, align 1, !tbaa !11
  %conv136 = zext i8 %107 to i32
  %mul137 = mul nsw i32 %sub132, %conv136
  %add138 = add nsw i32 %mul131, %mul137
  %add139 = add nsw i32 %add138, 32
  %shr140 = ashr i32 %add139, 6
  %conv141 = trunc i32 %shr140 to i8
  %108 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %109 = load i32, i32* %i, align 4, !tbaa !6
  %110 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul142 = mul i32 %109, %110
  %111 = load i32, i32* %j, align 4, !tbaa !6
  %add143 = add i32 %mul142, %111
  %arrayidx144 = getelementptr inbounds i8, i8* %108, i32 %add143
  store i8 %conv141, i8* %arrayidx144, align 1, !tbaa !11
  %112 = bitcast i32* %m112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #3
  br label %for.inc145

for.inc145:                                       ; preds = %for.body111
  %113 = load i32, i32* %j, align 4, !tbaa !6
  %inc146 = add nsw i32 %113, 1
  store i32 %inc146, i32* %j, align 4, !tbaa !6
  br label %for.cond108

for.end147:                                       ; preds = %for.cond108
  br label %for.inc148

for.inc148:                                       ; preds = %for.end147
  %114 = load i32, i32* %i, align 4, !tbaa !6
  %inc149 = add nsw i32 %114, 1
  store i32 %inc149, i32* %i, align 4, !tbaa !6
  br label %for.cond104

for.end150:                                       ; preds = %for.cond104
  br label %if.end

if.else151:                                       ; preds = %land.lhs.true100, %if.else97
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond152

for.cond152:                                      ; preds = %for.inc196, %if.else151
  %115 = load i32, i32* %i, align 4, !tbaa !6
  %116 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp153 = icmp slt i32 %115, %116
  br i1 %cmp153, label %for.body155, label %for.end198

for.body155:                                      ; preds = %for.cond152
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond156

for.cond156:                                      ; preds = %for.inc193, %for.body155
  %117 = load i32, i32* %j, align 4, !tbaa !6
  %118 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp157 = icmp slt i32 %117, %118
  br i1 %cmp157, label %for.body159, label %for.end195

for.body159:                                      ; preds = %for.cond156
  %119 = bitcast i32* %m160 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #3
  %120 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %121 = load i32, i32* %i, align 4, !tbaa !6
  %mul161 = mul nsw i32 2, %121
  %122 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul162 = mul i32 %mul161, %122
  %123 = load i32, i32* %j, align 4, !tbaa !6
  %add163 = add i32 %mul162, %123
  %arrayidx164 = getelementptr inbounds i8, i8* %120, i32 %add163
  %124 = load i8, i8* %arrayidx164, align 1, !tbaa !11
  %conv165 = zext i8 %124 to i32
  %125 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %126 = load i32, i32* %i, align 4, !tbaa !6
  %mul166 = mul nsw i32 2, %126
  %add167 = add nsw i32 %mul166, 1
  %127 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul168 = mul i32 %add167, %127
  %128 = load i32, i32* %j, align 4, !tbaa !6
  %add169 = add i32 %mul168, %128
  %arrayidx170 = getelementptr inbounds i8, i8* %125, i32 %add169
  %129 = load i8, i8* %arrayidx170, align 1, !tbaa !11
  %conv171 = zext i8 %129 to i32
  %add172 = add nsw i32 %conv165, %conv171
  %add173 = add nsw i32 %add172, 1
  %shr174 = ashr i32 %add173, 1
  store i32 %shr174, i32* %m160, align 4, !tbaa !6
  %130 = load i32, i32* %m160, align 4, !tbaa !6
  %131 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %132 = load i32, i32* %i, align 4, !tbaa !6
  %133 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul175 = mul i32 %132, %133
  %134 = load i32, i32* %j, align 4, !tbaa !6
  %add176 = add i32 %mul175, %134
  %arrayidx177 = getelementptr inbounds i8, i8* %131, i32 %add176
  %135 = load i8, i8* %arrayidx177, align 1, !tbaa !11
  %conv178 = zext i8 %135 to i32
  %mul179 = mul nsw i32 %130, %conv178
  %136 = load i32, i32* %m160, align 4, !tbaa !6
  %sub180 = sub nsw i32 64, %136
  %137 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %138 = load i32, i32* %i, align 4, !tbaa !6
  %139 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul181 = mul i32 %138, %139
  %140 = load i32, i32* %j, align 4, !tbaa !6
  %add182 = add i32 %mul181, %140
  %arrayidx183 = getelementptr inbounds i8, i8* %137, i32 %add182
  %141 = load i8, i8* %arrayidx183, align 1, !tbaa !11
  %conv184 = zext i8 %141 to i32
  %mul185 = mul nsw i32 %sub180, %conv184
  %add186 = add nsw i32 %mul179, %mul185
  %add187 = add nsw i32 %add186, 32
  %shr188 = ashr i32 %add187, 6
  %conv189 = trunc i32 %shr188 to i8
  %142 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %143 = load i32, i32* %i, align 4, !tbaa !6
  %144 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul190 = mul i32 %143, %144
  %145 = load i32, i32* %j, align 4, !tbaa !6
  %add191 = add i32 %mul190, %145
  %arrayidx192 = getelementptr inbounds i8, i8* %142, i32 %add191
  store i8 %conv189, i8* %arrayidx192, align 1, !tbaa !11
  %146 = bitcast i32* %m160 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #3
  br label %for.inc193

for.inc193:                                       ; preds = %for.body159
  %147 = load i32, i32* %j, align 4, !tbaa !6
  %inc194 = add nsw i32 %147, 1
  store i32 %inc194, i32* %j, align 4, !tbaa !6
  br label %for.cond156

for.end195:                                       ; preds = %for.cond156
  br label %for.inc196

for.inc196:                                       ; preds = %for.end195
  %148 = load i32, i32* %i, align 4, !tbaa !6
  %inc197 = add nsw i32 %148, 1
  store i32 %inc197, i32* %i, align 4, !tbaa !6
  br label %for.cond152

for.end198:                                       ; preds = %for.cond152
  br label %if.end

if.end:                                           ; preds = %for.end198, %for.end150
  br label %if.end199

if.end199:                                        ; preds = %if.end, %for.end96
  br label %if.end200

if.end200:                                        ; preds = %if.end199, %for.end24
  %149 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #3
  %150 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_blend_a64_mask_c(i8* %dst_8, i32 %dst_stride, i8* %src0_8, i32 %src0_stride, i8* %src1_8, i32 %src1_stride, i8* %mask, i32 %mask_stride, i32 %w, i32 %h, i32 %subw, i32 %subh, i32 %bd) #0 {
entry:
  %dst_8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src0_8.addr = alloca i8*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1_8.addr = alloca i8*, align 4
  %src1_stride.addr = alloca i32, align 4
  %mask.addr = alloca i8*, align 4
  %mask_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %subw.addr = alloca i32, align 4
  %subh.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %dst = alloca i16*, align 4
  %src0 = alloca i16*, align 4
  %src1 = alloca i16*, align 4
  %m = alloca i32, align 4
  %m41 = alloca i32, align 4
  %m114 = alloca i32, align 4
  %m162 = alloca i32, align 4
  store i8* %dst_8, i8** %dst_8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i8* %src0_8, i8** %src0_8.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i8* %src1_8, i8** %src1_8.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %mask_stride, i32* %mask_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %subw, i32* %subw.addr, align 4, !tbaa !6
  store i32 %subh, i32* %subh.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %dst_8.addr, align 4, !tbaa !2
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  store i16* %5, i16** %dst, align 4, !tbaa !2
  %6 = bitcast i16** %src0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load i8*, i8** %src0_8.addr, align 4, !tbaa !2
  %8 = ptrtoint i8* %7 to i32
  %shl1 = shl i32 %8, 1
  %9 = inttoptr i32 %shl1 to i16*
  store i16* %9, i16** %src0, align 4, !tbaa !2
  %10 = bitcast i16** %src1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load i8*, i8** %src1_8.addr, align 4, !tbaa !2
  %12 = ptrtoint i8* %11 to i32
  %shl2 = shl i32 %12, 1
  %13 = inttoptr i32 %shl2 to i16*
  store i16* %13, i16** %src1, align 4, !tbaa !2
  %14 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %15 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %15, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %16 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp3 = icmp eq i32 %16, 0
  br i1 %cmp3, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc24, %if.then
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %18 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %17, %18
  br i1 %cmp4, label %for.body, label %for.end26

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %20 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp6 = icmp slt i32 %19, %20
  br i1 %cmp6, label %for.body7, label %for.end

for.body7:                                        ; preds = %for.cond5
  %21 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %24 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul = mul i32 %23, %24
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %add = add i32 %mul, %25
  %arrayidx = getelementptr inbounds i8, i8* %22, i32 %add
  %26 = load i8, i8* %arrayidx, align 1, !tbaa !11
  %conv = zext i8 %26 to i32
  store i32 %conv, i32* %m, align 4, !tbaa !6
  %27 = load i32, i32* %m, align 4, !tbaa !6
  %28 = load i16*, i16** %src0, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %30 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul8 = mul i32 %29, %30
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %add9 = add i32 %mul8, %31
  %arrayidx10 = getelementptr inbounds i16, i16* %28, i32 %add9
  %32 = load i16, i16* %arrayidx10, align 2, !tbaa !12
  %conv11 = zext i16 %32 to i32
  %mul12 = mul nsw i32 %27, %conv11
  %33 = load i32, i32* %m, align 4, !tbaa !6
  %sub = sub nsw i32 64, %33
  %34 = load i16*, i16** %src1, align 4, !tbaa !2
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %36 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul13 = mul i32 %35, %36
  %37 = load i32, i32* %j, align 4, !tbaa !6
  %add14 = add i32 %mul13, %37
  %arrayidx15 = getelementptr inbounds i16, i16* %34, i32 %add14
  %38 = load i16, i16* %arrayidx15, align 2, !tbaa !12
  %conv16 = zext i16 %38 to i32
  %mul17 = mul nsw i32 %sub, %conv16
  %add18 = add nsw i32 %mul12, %mul17
  %add19 = add nsw i32 %add18, 32
  %shr = ashr i32 %add19, 6
  %conv20 = trunc i32 %shr to i16
  %39 = load i16*, i16** %dst, align 4, !tbaa !2
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul21 = mul i32 %40, %41
  %42 = load i32, i32* %j, align 4, !tbaa !6
  %add22 = add i32 %mul21, %42
  %arrayidx23 = getelementptr inbounds i16, i16* %39, i32 %add22
  store i16 %conv20, i16* %arrayidx23, align 2, !tbaa !12
  %43 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body7
  %44 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond5

for.end:                                          ; preds = %for.cond5
  br label %for.inc24

for.inc24:                                        ; preds = %for.end
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %inc25 = add nsw i32 %45, 1
  store i32 %inc25, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end26:                                        ; preds = %for.cond
  br label %if.end202

if.else:                                          ; preds = %land.lhs.true, %entry
  %46 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp27 = icmp eq i32 %46, 1
  br i1 %cmp27, label %land.lhs.true29, label %if.else99

land.lhs.true29:                                  ; preds = %if.else
  %47 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp30 = icmp eq i32 %47, 1
  br i1 %cmp30, label %if.then32, label %if.else99

if.then32:                                        ; preds = %land.lhs.true29
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc96, %if.then32
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %49 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp34 = icmp slt i32 %48, %49
  br i1 %cmp34, label %for.body36, label %for.end98

for.body36:                                       ; preds = %for.cond33
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc93, %for.body36
  %50 = load i32, i32* %j, align 4, !tbaa !6
  %51 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %50, %51
  br i1 %cmp38, label %for.body40, label %for.end95

for.body40:                                       ; preds = %for.cond37
  %52 = bitcast i32* %m41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  %53 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %mul42 = mul nsw i32 2, %54
  %55 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul43 = mul i32 %mul42, %55
  %56 = load i32, i32* %j, align 4, !tbaa !6
  %mul44 = mul nsw i32 2, %56
  %add45 = add i32 %mul43, %mul44
  %arrayidx46 = getelementptr inbounds i8, i8* %53, i32 %add45
  %57 = load i8, i8* %arrayidx46, align 1, !tbaa !11
  %conv47 = zext i8 %57 to i32
  %58 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %mul48 = mul nsw i32 2, %59
  %add49 = add nsw i32 %mul48, 1
  %60 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul50 = mul i32 %add49, %60
  %61 = load i32, i32* %j, align 4, !tbaa !6
  %mul51 = mul nsw i32 2, %61
  %add52 = add i32 %mul50, %mul51
  %arrayidx53 = getelementptr inbounds i8, i8* %58, i32 %add52
  %62 = load i8, i8* %arrayidx53, align 1, !tbaa !11
  %conv54 = zext i8 %62 to i32
  %add55 = add nsw i32 %conv47, %conv54
  %63 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %64 = load i32, i32* %i, align 4, !tbaa !6
  %mul56 = mul nsw i32 2, %64
  %65 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul57 = mul i32 %mul56, %65
  %66 = load i32, i32* %j, align 4, !tbaa !6
  %mul58 = mul nsw i32 2, %66
  %add59 = add nsw i32 %mul58, 1
  %add60 = add i32 %mul57, %add59
  %arrayidx61 = getelementptr inbounds i8, i8* %63, i32 %add60
  %67 = load i8, i8* %arrayidx61, align 1, !tbaa !11
  %conv62 = zext i8 %67 to i32
  %add63 = add nsw i32 %add55, %conv62
  %68 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %mul64 = mul nsw i32 2, %69
  %add65 = add nsw i32 %mul64, 1
  %70 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul66 = mul i32 %add65, %70
  %71 = load i32, i32* %j, align 4, !tbaa !6
  %mul67 = mul nsw i32 2, %71
  %add68 = add nsw i32 %mul67, 1
  %add69 = add i32 %mul66, %add68
  %arrayidx70 = getelementptr inbounds i8, i8* %68, i32 %add69
  %72 = load i8, i8* %arrayidx70, align 1, !tbaa !11
  %conv71 = zext i8 %72 to i32
  %add72 = add nsw i32 %add63, %conv71
  %add73 = add nsw i32 %add72, 2
  %shr74 = ashr i32 %add73, 2
  store i32 %shr74, i32* %m41, align 4, !tbaa !6
  %73 = load i32, i32* %m41, align 4, !tbaa !6
  %74 = load i16*, i16** %src0, align 4, !tbaa !2
  %75 = load i32, i32* %i, align 4, !tbaa !6
  %76 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul75 = mul i32 %75, %76
  %77 = load i32, i32* %j, align 4, !tbaa !6
  %add76 = add i32 %mul75, %77
  %arrayidx77 = getelementptr inbounds i16, i16* %74, i32 %add76
  %78 = load i16, i16* %arrayidx77, align 2, !tbaa !12
  %conv78 = zext i16 %78 to i32
  %mul79 = mul nsw i32 %73, %conv78
  %79 = load i32, i32* %m41, align 4, !tbaa !6
  %sub80 = sub nsw i32 64, %79
  %80 = load i16*, i16** %src1, align 4, !tbaa !2
  %81 = load i32, i32* %i, align 4, !tbaa !6
  %82 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul81 = mul i32 %81, %82
  %83 = load i32, i32* %j, align 4, !tbaa !6
  %add82 = add i32 %mul81, %83
  %arrayidx83 = getelementptr inbounds i16, i16* %80, i32 %add82
  %84 = load i16, i16* %arrayidx83, align 2, !tbaa !12
  %conv84 = zext i16 %84 to i32
  %mul85 = mul nsw i32 %sub80, %conv84
  %add86 = add nsw i32 %mul79, %mul85
  %add87 = add nsw i32 %add86, 32
  %shr88 = ashr i32 %add87, 6
  %conv89 = trunc i32 %shr88 to i16
  %85 = load i16*, i16** %dst, align 4, !tbaa !2
  %86 = load i32, i32* %i, align 4, !tbaa !6
  %87 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul90 = mul i32 %86, %87
  %88 = load i32, i32* %j, align 4, !tbaa !6
  %add91 = add i32 %mul90, %88
  %arrayidx92 = getelementptr inbounds i16, i16* %85, i32 %add91
  store i16 %conv89, i16* %arrayidx92, align 2, !tbaa !12
  %89 = bitcast i32* %m41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #3
  br label %for.inc93

for.inc93:                                        ; preds = %for.body40
  %90 = load i32, i32* %j, align 4, !tbaa !6
  %inc94 = add nsw i32 %90, 1
  store i32 %inc94, i32* %j, align 4, !tbaa !6
  br label %for.cond37

for.end95:                                        ; preds = %for.cond37
  br label %for.inc96

for.inc96:                                        ; preds = %for.end95
  %91 = load i32, i32* %i, align 4, !tbaa !6
  %inc97 = add nsw i32 %91, 1
  store i32 %inc97, i32* %i, align 4, !tbaa !6
  br label %for.cond33

for.end98:                                        ; preds = %for.cond33
  br label %if.end201

if.else99:                                        ; preds = %land.lhs.true29, %if.else
  %92 = load i32, i32* %subw.addr, align 4, !tbaa !6
  %cmp100 = icmp eq i32 %92, 1
  br i1 %cmp100, label %land.lhs.true102, label %if.else153

land.lhs.true102:                                 ; preds = %if.else99
  %93 = load i32, i32* %subh.addr, align 4, !tbaa !6
  %cmp103 = icmp eq i32 %93, 0
  br i1 %cmp103, label %if.then105, label %if.else153

if.then105:                                       ; preds = %land.lhs.true102
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond106

for.cond106:                                      ; preds = %for.inc150, %if.then105
  %94 = load i32, i32* %i, align 4, !tbaa !6
  %95 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp107 = icmp slt i32 %94, %95
  br i1 %cmp107, label %for.body109, label %for.end152

for.body109:                                      ; preds = %for.cond106
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond110

for.cond110:                                      ; preds = %for.inc147, %for.body109
  %96 = load i32, i32* %j, align 4, !tbaa !6
  %97 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp111 = icmp slt i32 %96, %97
  br i1 %cmp111, label %for.body113, label %for.end149

for.body113:                                      ; preds = %for.cond110
  %98 = bitcast i32* %m114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #3
  %99 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %100 = load i32, i32* %i, align 4, !tbaa !6
  %101 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul115 = mul i32 %100, %101
  %102 = load i32, i32* %j, align 4, !tbaa !6
  %mul116 = mul nsw i32 2, %102
  %add117 = add i32 %mul115, %mul116
  %arrayidx118 = getelementptr inbounds i8, i8* %99, i32 %add117
  %103 = load i8, i8* %arrayidx118, align 1, !tbaa !11
  %conv119 = zext i8 %103 to i32
  %104 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %105 = load i32, i32* %i, align 4, !tbaa !6
  %106 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul120 = mul i32 %105, %106
  %107 = load i32, i32* %j, align 4, !tbaa !6
  %mul121 = mul nsw i32 2, %107
  %add122 = add nsw i32 %mul121, 1
  %add123 = add i32 %mul120, %add122
  %arrayidx124 = getelementptr inbounds i8, i8* %104, i32 %add123
  %108 = load i8, i8* %arrayidx124, align 1, !tbaa !11
  %conv125 = zext i8 %108 to i32
  %add126 = add nsw i32 %conv119, %conv125
  %add127 = add nsw i32 %add126, 1
  %shr128 = ashr i32 %add127, 1
  store i32 %shr128, i32* %m114, align 4, !tbaa !6
  %109 = load i32, i32* %m114, align 4, !tbaa !6
  %110 = load i16*, i16** %src0, align 4, !tbaa !2
  %111 = load i32, i32* %i, align 4, !tbaa !6
  %112 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul129 = mul i32 %111, %112
  %113 = load i32, i32* %j, align 4, !tbaa !6
  %add130 = add i32 %mul129, %113
  %arrayidx131 = getelementptr inbounds i16, i16* %110, i32 %add130
  %114 = load i16, i16* %arrayidx131, align 2, !tbaa !12
  %conv132 = zext i16 %114 to i32
  %mul133 = mul nsw i32 %109, %conv132
  %115 = load i32, i32* %m114, align 4, !tbaa !6
  %sub134 = sub nsw i32 64, %115
  %116 = load i16*, i16** %src1, align 4, !tbaa !2
  %117 = load i32, i32* %i, align 4, !tbaa !6
  %118 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul135 = mul i32 %117, %118
  %119 = load i32, i32* %j, align 4, !tbaa !6
  %add136 = add i32 %mul135, %119
  %arrayidx137 = getelementptr inbounds i16, i16* %116, i32 %add136
  %120 = load i16, i16* %arrayidx137, align 2, !tbaa !12
  %conv138 = zext i16 %120 to i32
  %mul139 = mul nsw i32 %sub134, %conv138
  %add140 = add nsw i32 %mul133, %mul139
  %add141 = add nsw i32 %add140, 32
  %shr142 = ashr i32 %add141, 6
  %conv143 = trunc i32 %shr142 to i16
  %121 = load i16*, i16** %dst, align 4, !tbaa !2
  %122 = load i32, i32* %i, align 4, !tbaa !6
  %123 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul144 = mul i32 %122, %123
  %124 = load i32, i32* %j, align 4, !tbaa !6
  %add145 = add i32 %mul144, %124
  %arrayidx146 = getelementptr inbounds i16, i16* %121, i32 %add145
  store i16 %conv143, i16* %arrayidx146, align 2, !tbaa !12
  %125 = bitcast i32* %m114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  br label %for.inc147

for.inc147:                                       ; preds = %for.body113
  %126 = load i32, i32* %j, align 4, !tbaa !6
  %inc148 = add nsw i32 %126, 1
  store i32 %inc148, i32* %j, align 4, !tbaa !6
  br label %for.cond110

for.end149:                                       ; preds = %for.cond110
  br label %for.inc150

for.inc150:                                       ; preds = %for.end149
  %127 = load i32, i32* %i, align 4, !tbaa !6
  %inc151 = add nsw i32 %127, 1
  store i32 %inc151, i32* %i, align 4, !tbaa !6
  br label %for.cond106

for.end152:                                       ; preds = %for.cond106
  br label %if.end

if.else153:                                       ; preds = %land.lhs.true102, %if.else99
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond154

for.cond154:                                      ; preds = %for.inc198, %if.else153
  %128 = load i32, i32* %i, align 4, !tbaa !6
  %129 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp155 = icmp slt i32 %128, %129
  br i1 %cmp155, label %for.body157, label %for.end200

for.body157:                                      ; preds = %for.cond154
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond158

for.cond158:                                      ; preds = %for.inc195, %for.body157
  %130 = load i32, i32* %j, align 4, !tbaa !6
  %131 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp159 = icmp slt i32 %130, %131
  br i1 %cmp159, label %for.body161, label %for.end197

for.body161:                                      ; preds = %for.cond158
  %132 = bitcast i32* %m162 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #3
  %133 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %134 = load i32, i32* %i, align 4, !tbaa !6
  %mul163 = mul nsw i32 2, %134
  %135 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul164 = mul i32 %mul163, %135
  %136 = load i32, i32* %j, align 4, !tbaa !6
  %add165 = add i32 %mul164, %136
  %arrayidx166 = getelementptr inbounds i8, i8* %133, i32 %add165
  %137 = load i8, i8* %arrayidx166, align 1, !tbaa !11
  %conv167 = zext i8 %137 to i32
  %138 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %139 = load i32, i32* %i, align 4, !tbaa !6
  %mul168 = mul nsw i32 2, %139
  %add169 = add nsw i32 %mul168, 1
  %140 = load i32, i32* %mask_stride.addr, align 4, !tbaa !6
  %mul170 = mul i32 %add169, %140
  %141 = load i32, i32* %j, align 4, !tbaa !6
  %add171 = add i32 %mul170, %141
  %arrayidx172 = getelementptr inbounds i8, i8* %138, i32 %add171
  %142 = load i8, i8* %arrayidx172, align 1, !tbaa !11
  %conv173 = zext i8 %142 to i32
  %add174 = add nsw i32 %conv167, %conv173
  %add175 = add nsw i32 %add174, 1
  %shr176 = ashr i32 %add175, 1
  store i32 %shr176, i32* %m162, align 4, !tbaa !6
  %143 = load i32, i32* %m162, align 4, !tbaa !6
  %144 = load i16*, i16** %src0, align 4, !tbaa !2
  %145 = load i32, i32* %i, align 4, !tbaa !6
  %146 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul177 = mul i32 %145, %146
  %147 = load i32, i32* %j, align 4, !tbaa !6
  %add178 = add i32 %mul177, %147
  %arrayidx179 = getelementptr inbounds i16, i16* %144, i32 %add178
  %148 = load i16, i16* %arrayidx179, align 2, !tbaa !12
  %conv180 = zext i16 %148 to i32
  %mul181 = mul nsw i32 %143, %conv180
  %149 = load i32, i32* %m162, align 4, !tbaa !6
  %sub182 = sub nsw i32 64, %149
  %150 = load i16*, i16** %src1, align 4, !tbaa !2
  %151 = load i32, i32* %i, align 4, !tbaa !6
  %152 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul183 = mul i32 %151, %152
  %153 = load i32, i32* %j, align 4, !tbaa !6
  %add184 = add i32 %mul183, %153
  %arrayidx185 = getelementptr inbounds i16, i16* %150, i32 %add184
  %154 = load i16, i16* %arrayidx185, align 2, !tbaa !12
  %conv186 = zext i16 %154 to i32
  %mul187 = mul nsw i32 %sub182, %conv186
  %add188 = add nsw i32 %mul181, %mul187
  %add189 = add nsw i32 %add188, 32
  %shr190 = ashr i32 %add189, 6
  %conv191 = trunc i32 %shr190 to i16
  %155 = load i16*, i16** %dst, align 4, !tbaa !2
  %156 = load i32, i32* %i, align 4, !tbaa !6
  %157 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul192 = mul i32 %156, %157
  %158 = load i32, i32* %j, align 4, !tbaa !6
  %add193 = add i32 %mul192, %158
  %arrayidx194 = getelementptr inbounds i16, i16* %155, i32 %add193
  store i16 %conv191, i16* %arrayidx194, align 2, !tbaa !12
  %159 = bitcast i32* %m162 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #3
  br label %for.inc195

for.inc195:                                       ; preds = %for.body161
  %160 = load i32, i32* %j, align 4, !tbaa !6
  %inc196 = add nsw i32 %160, 1
  store i32 %inc196, i32* %j, align 4, !tbaa !6
  br label %for.cond158

for.end197:                                       ; preds = %for.cond158
  br label %for.inc198

for.inc198:                                       ; preds = %for.end197
  %161 = load i32, i32* %i, align 4, !tbaa !6
  %inc199 = add nsw i32 %161, 1
  store i32 %inc199, i32* %i, align 4, !tbaa !6
  br label %for.cond154

for.end200:                                       ; preds = %for.cond154
  br label %if.end

if.end:                                           ; preds = %for.end200, %for.end152
  br label %if.end201

if.end201:                                        ; preds = %if.end, %for.end98
  br label %if.end202

if.end202:                                        ; preds = %if.end201, %for.end26
  %162 = bitcast i16** %src1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #3
  %163 = bitcast i16** %src0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #3
  %164 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #3
  %165 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #3
  %166 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #3
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 12}
!9 = !{!"ConvolveParams", !7, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40}
!10 = !{!9, !7, i64 16}
!11 = !{!4, !4, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"short", !4, i64 0}
