; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_util/debug_util.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_util/debug_util.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@frame_idx_w = internal global i32 0, align 4
@frame_idx_r = internal global i32 0, align 4

; Function Attrs: nounwind
define hidden void @aom_bitstream_queue_set_frame_write(i32 %frame_idx) #0 {
entry:
  %frame_idx.addr = alloca i32, align 4
  store i32 %frame_idx, i32* %frame_idx.addr, align 4, !tbaa !2
  %0 = load i32, i32* %frame_idx.addr, align 4, !tbaa !2
  store i32 %0, i32* @frame_idx_w, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_bitstream_queue_get_frame_writee() #0 {
entry:
  %0 = load i32, i32* @frame_idx_w, align 4, !tbaa !2
  ret i32 %0
}

; Function Attrs: nounwind
define hidden void @aom_bitstream_queue_set_frame_read(i32 %frame_idx) #0 {
entry:
  %frame_idx.addr = alloca i32, align 4
  store i32 %frame_idx, i32* %frame_idx.addr, align 4, !tbaa !2
  %0 = load i32, i32* %frame_idx.addr, align 4, !tbaa !2
  store i32 %0, i32* @frame_idx_r, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_bitstream_queue_get_frame_read() #0 {
entry:
  %0 = load i32, i32* @frame_idx_r, align 4, !tbaa !2
  ret i32 %0
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
