; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/blend_a64_hmask.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/blend_a64_hmask.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @aom_blend_a64_hmask_c(i8* %dst, i32 %dst_stride, i8* %src0, i32 %src0_stride, i8* %src1, i32 %src1_stride, i8* %mask, i32 %w, i32 %h) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src0.addr = alloca i8*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i8*, align 4
  %src1_stride.addr = alloca i32, align 4
  %mask.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i8* %src0, i8** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i8* %src1, i8** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc20, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end22

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %j, align 4, !tbaa !6
  %5 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %4, %5
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %6 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %7 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %8 to i32
  %9 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul = mul i32 %10, %11
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %add = add i32 %mul, %12
  %arrayidx4 = getelementptr inbounds i8, i8* %9, i32 %add
  %13 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = zext i8 %13 to i32
  %mul6 = mul nsw i32 %conv, %conv5
  %14 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  %conv8 = zext i8 %16 to i32
  %sub = sub nsw i32 64, %conv8
  %17 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %19 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul9 = mul i32 %18, %19
  %20 = load i32, i32* %j, align 4, !tbaa !6
  %add10 = add i32 %mul9, %20
  %arrayidx11 = getelementptr inbounds i8, i8* %17, i32 %add10
  %21 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  %conv12 = zext i8 %21 to i32
  %mul13 = mul nsw i32 %sub, %conv12
  %add14 = add nsw i32 %mul6, %mul13
  %add15 = add nsw i32 %add14, 32
  %shr = ashr i32 %add15, 6
  %conv16 = trunc i32 %shr to i8
  %22 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %24 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul17 = mul i32 %23, %24
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %add18 = add i32 %mul17, %25
  %arrayidx19 = getelementptr inbounds i8, i8* %22, i32 %add18
  store i8 %conv16, i8* %arrayidx19, align 1, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc20

for.inc20:                                        ; preds = %for.end
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %inc21 = add nsw i32 %27, 1
  store i32 %inc21, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end22:                                        ; preds = %for.cond
  %28 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #2
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_highbd_blend_a64_hmask_c(i8* %dst_8, i32 %dst_stride, i8* %src0_8, i32 %src0_stride, i8* %src1_8, i32 %src1_stride, i8* %mask, i32 %w, i32 %h, i32 %bd) #0 {
entry:
  %dst_8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src0_8.addr = alloca i8*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1_8.addr = alloca i8*, align 4
  %src1_stride.addr = alloca i32, align 4
  %mask.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %dst = alloca i16*, align 4
  %src0 = alloca i16*, align 4
  %src1 = alloca i16*, align 4
  store i8* %dst_8, i8** %dst_8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i8* %src0_8, i8** %src0_8.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i8* %src1_8, i8** %src1_8.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = load i8*, i8** %dst_8.addr, align 4, !tbaa !2
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  store i16* %5, i16** %dst, align 4, !tbaa !2
  %6 = bitcast i16** %src0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = load i8*, i8** %src0_8.addr, align 4, !tbaa !2
  %8 = ptrtoint i8* %7 to i32
  %shl1 = shl i32 %8, 1
  %9 = inttoptr i32 %shl1 to i16*
  store i16* %9, i16** %src0, align 4, !tbaa !2
  %10 = bitcast i16** %src1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = load i8*, i8** %src1_8.addr, align 4, !tbaa !2
  %12 = ptrtoint i8* %11 to i32
  %shl2 = shl i32 %12, 1
  %13 = inttoptr i32 %shl2 to i16*
  store i16* %13, i16** %src1, align 4, !tbaa !2
  %14 = load i32, i32* %bd.addr, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc22, %entry
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %16 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %15, %16
  br i1 %cmp, label %for.body, label %for.end24

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %18 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %17, %18
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond3
  %19 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %20 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %19, i32 %20
  %21 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %21 to i32
  %22 = load i16*, i16** %src0, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %24 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul = mul i32 %23, %24
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %add = add i32 %mul, %25
  %arrayidx6 = getelementptr inbounds i16, i16* %22, i32 %add
  %26 = load i16, i16* %arrayidx6, align 2, !tbaa !9
  %conv7 = zext i16 %26 to i32
  %mul8 = mul nsw i32 %conv, %conv7
  %27 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds i8, i8* %27, i32 %28
  %29 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  %conv10 = zext i8 %29 to i32
  %sub = sub nsw i32 64, %conv10
  %30 = load i16*, i16** %src1, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %32 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul11 = mul i32 %31, %32
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %add12 = add i32 %mul11, %33
  %arrayidx13 = getelementptr inbounds i16, i16* %30, i32 %add12
  %34 = load i16, i16* %arrayidx13, align 2, !tbaa !9
  %conv14 = zext i16 %34 to i32
  %mul15 = mul nsw i32 %sub, %conv14
  %add16 = add nsw i32 %mul8, %mul15
  %add17 = add nsw i32 %add16, 32
  %shr = ashr i32 %add17, 6
  %conv18 = trunc i32 %shr to i16
  %35 = load i16*, i16** %dst, align 4, !tbaa !2
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul19 = mul i32 %36, %37
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %add20 = add i32 %mul19, %38
  %arrayidx21 = getelementptr inbounds i16, i16* %35, i32 %add20
  store i16 %conv18, i16* %arrayidx21, align 2, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %39 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %39, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %inc23 = add nsw i32 %40, 1
  store i32 %inc23, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end24:                                        ; preds = %for.cond
  %41 = bitcast i16** %src1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #2
  %42 = bitcast i16** %src0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #2
  %43 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #2
  %44 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #2
  %45 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #2
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"short", !4, i64 0}
