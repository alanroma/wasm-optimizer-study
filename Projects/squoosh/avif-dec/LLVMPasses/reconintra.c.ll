; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/reconintra.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/reconintra.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }

@av1_filter_intra_taps = hidden constant [5 x [8 x [8 x i8]]] [[8 x [8 x i8]] [[8 x i8] c"\FA\0A\00\00\00\0C\00\00", [8 x i8] c"\FB\02\0A\00\00\09\00\00", [8 x i8] c"\FD\01\01\0A\00\07\00\00", [8 x i8] c"\FD\01\01\02\0A\05\00\00", [8 x i8] c"\FC\06\00\00\00\02\0C\00", [8 x i8] c"\FD\02\06\00\00\02\09\00", [8 x i8] c"\FD\02\02\06\00\02\07\00", [8 x i8] c"\FD\01\02\02\06\03\05\00"], [8 x [8 x i8]] [[8 x i8] c"\F6\10\00\00\00\0A\00\00", [8 x i8] c"\FA\00\10\00\00\06\00\00", [8 x i8] c"\FC\00\00\10\00\04\00\00", [8 x i8] c"\FE\00\00\00\10\02\00\00", [8 x i8] c"\F6\10\00\00\00\00\0A\00", [8 x i8] c"\FA\00\10\00\00\00\06\00", [8 x i8] c"\FC\00\00\10\00\00\04\00", [8 x i8] c"\FE\00\00\00\10\00\02\00"], [8 x [8 x i8]] [[8 x i8] c"\F8\08\00\00\00\10\00\00", [8 x i8] c"\F8\00\08\00\00\10\00\00", [8 x i8] c"\F8\00\00\08\00\10\00\00", [8 x i8] c"\F8\00\00\00\08\10\00\00", [8 x i8] c"\FC\04\00\00\00\00\10\00", [8 x i8] c"\FC\00\04\00\00\00\10\00", [8 x i8] c"\FC\00\00\04\00\00\10\00", [8 x i8] c"\FC\00\00\00\04\00\10\00"], [8 x [8 x i8]] [[8 x i8] c"\FE\08\00\00\00\0A\00\00", [8 x i8] c"\FF\03\08\00\00\06\00\00", [8 x i8] c"\FF\02\03\08\00\04\00\00", [8 x i8] c"\00\01\02\03\08\02\00\00", [8 x i8] c"\FF\04\00\00\00\03\0A\00", [8 x i8] c"\FF\03\04\00\00\04\06\00", [8 x i8] c"\FF\02\03\04\00\04\04\00", [8 x i8] c"\FF\02\02\03\04\03\03\00"], [8 x [8 x i8]] [[8 x i8] c"\F4\0E\00\00\00\0E\00\00", [8 x i8] c"\F6\00\0E\00\00\0C\00\00", [8 x i8] c"\F7\00\00\0E\00\0B\00\00", [8 x i8] c"\F8\00\00\00\0E\0A\00\00", [8 x i8] c"\F6\0C\00\00\00\00\0E\00", [8 x i8] c"\F7\01\0C\00\00\00\0C\00", [8 x i8] c"\F8\00\00\0C\00\01\0B\00", [8 x i8] c"\F9\00\00\01\0C\01\09\00"]], align 16
@tx_size_wide = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 4, i32 8, i32 8, i32 16, i32 16, i32 32, i32 32, i32 64, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64], align 16
@tx_size_high = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 8, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64, i32 32, i32 16, i32 4, i32 32, i32 8, i32 64, i32 16], align 16
@__const.av1_filter_intra_edge_c.kernel = private unnamed_addr constant [3 x [5 x i32]] [[5 x i32] [i32 0, i32 4, i32 8, i32 4, i32 0], [5 x i32] [i32 0, i32 5, i32 6, i32 5, i32 0], [5 x i32] [i32 2, i32 4, i32 4, i32 4, i32 2]], align 16
@__const.av1_filter_intra_edge_high_c.kernel = private unnamed_addr constant [3 x [5 x i32]] [[5 x i32] [i32 0, i32 4, i32 8, i32 4, i32 0], [5 x i32] [i32 0, i32 5, i32 6, i32 5, i32 0], [5 x i32] [i32 2, i32 4, i32 4, i32 4, i32 2]], align 16
@tx_size_wide_unit = internal constant [19 x i32] [i32 1, i32 2, i32 4, i32 8, i32 16, i32 1, i32 2, i32 2, i32 4, i32 4, i32 8, i32 8, i32 16, i32 1, i32 4, i32 2, i32 8, i32 4, i32 16], align 16
@tx_size_high_unit = internal constant [19 x i32] [i32 1, i32 2, i32 4, i32 8, i32 16, i32 2, i32 1, i32 4, i32 2, i32 8, i32 4, i32 16, i32 8, i32 4, i32 1, i32 8, i32 2, i32 16, i32 4], align 16
@mi_size_wide = internal constant [22 x i8] c"\01\01\02\02\02\04\04\04\08\08\08\10\10\10  \01\04\02\08\04\10", align 16
@block_size_wide = internal constant [22 x i8] c"\04\04\08\08\08\10\10\10   @@@\80\80\04\10\08 \10@", align 16
@mi_size_high = internal constant [22 x i8] c"\01\02\01\02\04\02\04\08\04\08\10\08\10 \10 \04\01\08\02\10\04", align 16
@mi_size_wide_log2 = internal constant [22 x i8] c"\00\00\01\01\01\02\02\02\03\03\03\04\04\04\05\05\00\02\01\03\02\04", align 16
@mi_size_high_log2 = internal constant [22 x i8] c"\00\01\00\01\02\01\02\03\02\03\04\03\04\05\04\05\02\00\03\01\04\02", align 16
@has_tr_vert_tables = internal constant [16 x i8*] [i8* null, i8* getelementptr inbounds ([64 x i8], [64 x i8]* @has_tr_4x8, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @has_tr_vert_8x8, i32 0, i32 0), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @has_tr_8x16, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @has_tr_vert_16x16, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @has_tr_16x32, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @has_tr_vert_32x32, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_32x64, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_vert_64x64, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_64x128, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_128x128, i32 0, i32 0)], align 16
@has_tr_tables = internal constant [22 x i8*] [i8* getelementptr inbounds ([128 x i8], [128 x i8]* @has_tr_4x4, i32 0, i32 0), i8* getelementptr inbounds ([64 x i8], [64 x i8]* @has_tr_4x8, i32 0, i32 0), i8* getelementptr inbounds ([64 x i8], [64 x i8]* @has_tr_8x4, i32 0, i32 0), i8* getelementptr inbounds ([32 x i8], [32 x i8]* @has_tr_8x8, i32 0, i32 0), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @has_tr_8x16, i32 0, i32 0), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @has_tr_16x8, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @has_tr_16x16, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @has_tr_16x32, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @has_tr_32x16, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @has_tr_32x32, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_32x64, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_64x32, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_64x64, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_64x128, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_128x64, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_tr_128x128, i32 0, i32 0), i8* getelementptr inbounds ([32 x i8], [32 x i8]* @has_tr_4x16, i32 0, i32 0), i8* getelementptr inbounds ([32 x i8], [32 x i8]* @has_tr_16x4, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @has_tr_8x32, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @has_tr_32x8, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @has_tr_16x64, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @has_tr_64x16, i32 0, i32 0)], align 16
@has_tr_4x8 = internal global [64 x i8] c"\FF\FF\FF\FFwwww\7F\7F\7F\7Fwwww\FF\7F\FF\7Fwwww\7F\7F\7F\7Fwwww\FF\FF\FF\7Fwwww\7F\7F\7F\7Fwwww\FF\7F\FF\7Fwwww\7F\7F\7F\7Fwwww", align 16
@has_tr_vert_8x8 = internal global [32 x i8] c"\FF\FF\00\00ww\00\00\7F\7F\00\00ww\00\00\FF\7F\00\00ww\00\00\7F\7F\00\00ww\00\00", align 16
@has_tr_8x16 = internal global [16 x i8] c"\FF\FFww\7F\7Fww\FF\7Fww\7F\7Fww", align 16
@has_tr_vert_16x16 = internal global [8 x i8] c"\FF\00w\00\7F\00w\00", align 1
@has_tr_16x32 = internal global [4 x i8] c"\FFw\7Fw", align 1
@has_tr_vert_32x32 = internal global [2 x i8] c"\0F\07", align 1
@has_tr_32x64 = internal global [1 x i8] c"\7F", align 1
@has_tr_vert_64x64 = internal global [1 x i8] c"\03", align 1
@has_tr_64x128 = internal global [1 x i8] c"\03", align 1
@has_tr_128x128 = internal global [1 x i8] c"\01", align 1
@has_tr_4x4 = internal global [128 x i8] c"\FF\FF\FF\FFUUUUwwwwUUUU\7F\7F\7F\7FUUUUwwwwUUUU\FF\7F\FF\7FUUUUwwwwUUUU\7F\7F\7F\7FUUUUwwwwUUUU\FF\FF\FF\7FUUUUwwwwUUUU\7F\7F\7F\7FUUUUwwwwUUUU\FF\7F\FF\7FUUUUwwwwUUUU\7F\7F\7F\7FUUUUwwwwUUUU", align 16
@has_tr_8x4 = internal global [64 x i8] c"\FF\FF\00\00UU\00\00ww\00\00UU\00\00\7F\7F\00\00UU\00\00ww\00\00UU\00\00\FF\7F\00\00UU\00\00ww\00\00UU\00\00\7F\7F\00\00UU\00\00ww\00\00UU\00\00", align 16
@has_tr_8x8 = internal global [32 x i8] c"\FF\FFUUwwUU\7F\7FUUwwUU\FF\7FUUwwUU\7F\7FUUwwUU", align 16
@has_tr_16x8 = internal global [16 x i8] c"\FF\00U\00w\00U\00\7F\00U\00w\00U\00", align 16
@has_tr_16x16 = internal global [8 x i8] c"\FFUwU\7FUwU", align 1
@has_tr_32x16 = internal global [4 x i8] c"\0F\05\07\05", align 1
@has_tr_32x32 = internal global [2 x i8] c"_W", align 1
@has_tr_64x32 = internal global [1 x i8] c"\13", align 1
@has_tr_64x64 = internal global [1 x i8] c"\07", align 1
@has_tr_128x64 = internal global [1 x i8] c"\01", align 1
@has_tr_4x16 = internal global [32 x i8] c"\FF\FF\FF\FF\7F\7F\7F\7F\FF\7F\FF\7F\7F\7F\7F\7F\FF\FF\FF\7F\7F\7F\7F\7F\FF\7F\FF\7F\7F\7F\7F\7F", align 16
@has_tr_16x4 = internal global [32 x i8] c"\FF\00\00\00U\00\00\00w\00\00\00U\00\00\00\7F\00\00\00U\00\00\00w\00\00\00U\00\00\00", align 16
@has_tr_8x32 = internal global [8 x i8] c"\FF\FF\7F\7F\FF\7F\7F\7F", align 1
@has_tr_32x8 = internal global [8 x i8] c"\0F\00\05\00\07\00\05\00", align 1
@has_tr_16x64 = internal global [2 x i8] c"\FF\7F", align 1
@has_tr_64x16 = internal global [2 x i8] c"\03\01", align 1
@has_bl_vert_tables = internal constant [16 x i8*] [i8* null, i8* getelementptr inbounds ([64 x i8], [64 x i8]* @has_bl_4x8, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @has_bl_vert_8x8, i32 0, i32 0), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @has_bl_8x16, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @has_bl_vert_16x16, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @has_bl_16x32, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @has_bl_vert_32x32, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_32x64, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_vert_64x64, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_64x128, i32 0, i32 0), i8* null, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_128x128, i32 0, i32 0)], align 16
@has_bl_tables = internal constant [22 x i8*] [i8* getelementptr inbounds ([128 x i8], [128 x i8]* @has_bl_4x4, i32 0, i32 0), i8* getelementptr inbounds ([64 x i8], [64 x i8]* @has_bl_4x8, i32 0, i32 0), i8* getelementptr inbounds ([64 x i8], [64 x i8]* @has_bl_8x4, i32 0, i32 0), i8* getelementptr inbounds ([32 x i8], [32 x i8]* @has_bl_8x8, i32 0, i32 0), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @has_bl_8x16, i32 0, i32 0), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @has_bl_16x8, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @has_bl_16x16, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @has_bl_16x32, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @has_bl_32x16, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @has_bl_32x32, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_32x64, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_64x32, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_64x64, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_64x128, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_128x64, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @has_bl_128x128, i32 0, i32 0), i8* getelementptr inbounds ([32 x i8], [32 x i8]* @has_bl_4x16, i32 0, i32 0), i8* getelementptr inbounds ([32 x i8], [32 x i8]* @has_bl_16x4, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @has_bl_8x32, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @has_bl_32x8, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @has_bl_16x64, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @has_bl_64x16, i32 0, i32 0)], align 16
@has_bl_4x8 = internal global [64 x i8] c"\10\11\11\11\00\01\01\01\10\11\11\11\00\00\01\00\10\11\11\11\00\01\01\01\10\11\11\11\00\00\00\00\10\11\11\11\00\01\01\01\10\11\11\11\00\00\01\00\10\11\11\11\00\01\01\01\10\11\11\11\00\00\00\00", align 16
@has_bl_vert_8x8 = internal global [32 x i8] c"\FE\FF\10\11\FE\FF\00\01\FE\FF\10\11\FE\FF\00\00\FE\FF\10\11\FE\FF\00\01\FE\FF\10\11\FE\FF\00\00", align 16
@has_bl_8x16 = internal global [16 x i8] c"\10\11\00\01\10\11\00\00\10\11\00\01\10\11\00\00", align 16
@has_bl_vert_16x16 = internal global [8 x i8] c"\FE\10\FE\00\FE\10\FE\00", align 1
@has_bl_16x32 = internal global [4 x i8] c"\10\00\10\00", align 1
@has_bl_vert_32x32 = internal global [2 x i8] c"\0E\0E", align 1
@has_bl_32x64 = internal global [1 x i8] zeroinitializer, align 1
@has_bl_vert_64x64 = internal global [1 x i8] c"\02", align 1
@has_bl_64x128 = internal global [1 x i8] zeroinitializer, align 1
@has_bl_128x128 = internal global [1 x i8] zeroinitializer, align 1
@has_bl_4x4 = internal global [128 x i8] c"TUUU\10\11\11\11TUUU\00\01\01\01TUUU\10\11\11\11TUUU\00\00\01\00TUUU\10\11\11\11TUUU\00\01\01\01TUUU\10\11\11\11TUUU\00\00\00\00TUUU\10\11\11\11TUUU\00\01\01\01TUUU\10\11\11\11TUUU\00\00\01\00TUUU\10\11\11\11TUUU\00\01\01\01TUUU\10\11\11\11TUUU\00\00\00\00", align 16
@has_bl_8x4 = internal global [64 x i8] c"\FE\FFTU\FE\FF\10\11\FE\FFTU\FE\FF\00\01\FE\FFTU\FE\FF\10\11\FE\FFTU\FE\FF\00\00\FE\FFTU\FE\FF\10\11\FE\FFTU\FE\FF\00\01\FE\FFTU\FE\FF\10\11\FE\FFTU\FE\FF\00\00", align 16
@has_bl_8x8 = internal global [32 x i8] c"TU\10\11TU\00\01TU\10\11TU\00\00TU\10\11TU\00\01TU\10\11TU\00\00", align 16
@has_bl_16x8 = internal global [16 x i8] c"\FET\FE\10\FET\FE\00\FET\FE\10\FET\FE\00", align 16
@has_bl_16x16 = internal global [8 x i8] c"T\10T\00T\10T\00", align 1
@has_bl_32x16 = internal global [4 x i8] c"N\0EN\0E", align 1
@has_bl_32x32 = internal global [2 x i8] c"\04\04", align 1
@has_bl_64x32 = internal global [1 x i8] c"\22", align 1
@has_bl_64x64 = internal global [1 x i8] zeroinitializer, align 1
@has_bl_128x64 = internal global [1 x i8] zeroinitializer, align 1
@has_bl_4x16 = internal global [32 x i8] c"\00\01\01\01\00\00\01\00\00\01\01\01\00\00\00\00\00\01\01\01\00\00\01\00\00\01\01\01\00\00\00\00", align 16
@has_bl_16x4 = internal global [32 x i8] c"\FE\FE\FET\FE\FE\FE\10\FE\FE\FET\FE\FE\FE\00\FE\FE\FET\FE\FE\FE\10\FE\FE\FET\FE\FE\FE\00", align 16
@has_bl_8x32 = internal global [8 x i8] c"\00\01\00\00\00\01\00\00", align 1
@has_bl_32x8 = internal global [8 x i8] c"\EEN\EE\0E\EEN\EE\0E", align 1
@has_bl_16x64 = internal global [2 x i8] zeroinitializer, align 1
@has_bl_64x16 = internal global [2 x i8] c"**", align 1
@extend_modes = internal constant [13 x i8] c"\06\04\02\0C\16\16\16\22\0C\06\06\06\16", align 1
@mode_to_angle_map = internal constant [13 x i8] c"\00Z\B4-\87q\9D\CBC\00\00\00\00", align 1
@dc_pred_high = internal global [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]] zeroinitializer, align 16
@pred_high = internal global [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]] zeroinitializer, align 16
@__const.filter_intra_edge_corner_high.kernel = private unnamed_addr constant [3 x i32] [i32 5, i32 6, i32 5], align 4
@dr_intra_derivative = internal constant [90 x i16] [i16 0, i16 0, i16 0, i16 1023, i16 0, i16 0, i16 547, i16 0, i16 0, i16 372, i16 0, i16 0, i16 0, i16 0, i16 273, i16 0, i16 0, i16 215, i16 0, i16 0, i16 178, i16 0, i16 0, i16 151, i16 0, i16 0, i16 132, i16 0, i16 0, i16 116, i16 0, i16 0, i16 102, i16 0, i16 0, i16 0, i16 90, i16 0, i16 0, i16 80, i16 0, i16 0, i16 71, i16 0, i16 0, i16 64, i16 0, i16 0, i16 57, i16 0, i16 0, i16 51, i16 0, i16 0, i16 45, i16 0, i16 0, i16 0, i16 40, i16 0, i16 0, i16 35, i16 0, i16 0, i16 31, i16 0, i16 0, i16 27, i16 0, i16 0, i16 23, i16 0, i16 0, i16 19, i16 0, i16 0, i16 15, i16 0, i16 0, i16 0, i16 0, i16 11, i16 0, i16 0, i16 7, i16 0, i16 0, i16 3, i16 0, i16 0], align 16
@dc_pred = internal global [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]] zeroinitializer, align 16
@pred = internal global [13 x [19 x void (i8*, i32, i8*, i8*)*]] zeroinitializer, align 16
@__const.filter_intra_edge_corner.kernel = private unnamed_addr constant [3 x i32] [i32 5, i32 6, i32 5], align 4
@get_uv_mode.uv2y = internal constant [16 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B\0C\00\19\19", align 16
@aom_once.done = internal global i32 0, align 4

; Function Attrs: nounwind
define hidden void @av1_dr_prediction_z1_c(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left, i32 %upsample_above, i32 %dx, i32 %dy) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %upsample_above.addr = alloca i32, align 4
  %dx.addr = alloca i32, align 4
  %dy.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %x = alloca i32, align 4
  %base = alloca i32, align 4
  %shift = alloca i32, align 4
  %val = alloca i32, align 4
  %max_base_x = alloca i32, align 4
  %frac_bits = alloca i32, align 4
  %base_inc = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  store i32 %upsample_above, i32* %upsample_above.addr, align 4, !tbaa !8
  store i32 %dx, i32* %dx.addr, align 4, !tbaa !8
  store i32 %dy, i32* %dy.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %7 = load i32, i32* %dy.addr, align 4, !tbaa !8
  %8 = bitcast i32* %max_base_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %10 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add = add nsw i32 %9, %10
  %sub = sub nsw i32 %add, 1
  %11 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl = shl i32 %sub, %11
  store i32 %shl, i32* %max_base_x, align 4, !tbaa !8
  %12 = bitcast i32* %frac_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %sub1 = sub nsw i32 6, %13
  store i32 %sub1, i32* %frac_bits, align 4, !tbaa !8
  %14 = bitcast i32* %base_inc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl2 = shl i32 1, %15
  store i32 %shl2, i32* %base_inc, align 4, !tbaa !8
  %16 = load i32, i32* %dx.addr, align 4, !tbaa !8
  store i32 %16, i32* %x, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc36, %entry
  %17 = load i32, i32* %r, align 4, !tbaa !8
  %18 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.end40

for.body:                                         ; preds = %for.cond
  %19 = load i32, i32* %x, align 4, !tbaa !8
  %20 = load i32, i32* %frac_bits, align 4, !tbaa !8
  %shr = ashr i32 %19, %20
  store i32 %shr, i32* %base, align 4, !tbaa !8
  %21 = load i32, i32* %x, align 4, !tbaa !8
  %22 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl3 = shl i32 %21, %22
  %and = and i32 %shl3, 63
  %shr4 = ashr i32 %and, 1
  store i32 %shr4, i32* %shift, align 4, !tbaa !8
  %23 = load i32, i32* %base, align 4, !tbaa !8
  %24 = load i32, i32* %max_base_x, align 4, !tbaa !8
  %cmp5 = icmp sge i32 %23, %24
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load i32, i32* %r, align 4, !tbaa !8
  store i32 %26, i32* %i, align 4, !tbaa !8
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %if.then
  %27 = load i32, i32* %i, align 4, !tbaa !8
  %28 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp7 = icmp slt i32 %27, %28
  br i1 %cmp7, label %for.body8, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond6
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  br label %for.end

for.body8:                                        ; preds = %for.cond6
  %30 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %31 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %32 = load i32, i32* %max_base_x, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i8, i8* %31, i32 %32
  %33 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %33 to i32
  %34 = trunc i32 %conv to i8
  %35 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %mul = mul i32 %35, 1
  call void @llvm.memset.p0i8.i32(i8* align 1 %30, i8 %34, i32 %mul, i1 false)
  %36 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %37 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %37, i32 %36
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %38 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %38, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond6

for.end:                                          ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc32, %if.end
  %39 = load i32, i32* %c, align 4, !tbaa !8
  %40 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp10 = icmp slt i32 %39, %40
  br i1 %cmp10, label %for.body12, label %for.end35

for.body12:                                       ; preds = %for.cond9
  %41 = load i32, i32* %base, align 4, !tbaa !8
  %42 = load i32, i32* %max_base_x, align 4, !tbaa !8
  %cmp13 = icmp slt i32 %41, %42
  br i1 %cmp13, label %if.then15, label %if.else

if.then15:                                        ; preds = %for.body12
  %43 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %44 = load i32, i32* %base, align 4, !tbaa !8
  %arrayidx16 = getelementptr inbounds i8, i8* %43, i32 %44
  %45 = load i8, i8* %arrayidx16, align 1, !tbaa !10
  %conv17 = zext i8 %45 to i32
  %46 = load i32, i32* %shift, align 4, !tbaa !8
  %sub18 = sub nsw i32 32, %46
  %mul19 = mul nsw i32 %conv17, %sub18
  %47 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %48 = load i32, i32* %base, align 4, !tbaa !8
  %add20 = add nsw i32 %48, 1
  %arrayidx21 = getelementptr inbounds i8, i8* %47, i32 %add20
  %49 = load i8, i8* %arrayidx21, align 1, !tbaa !10
  %conv22 = zext i8 %49 to i32
  %50 = load i32, i32* %shift, align 4, !tbaa !8
  %mul23 = mul nsw i32 %conv22, %50
  %add24 = add nsw i32 %mul19, %mul23
  store i32 %add24, i32* %val, align 4, !tbaa !8
  %51 = load i32, i32* %val, align 4, !tbaa !8
  %add25 = add nsw i32 %51, 16
  %shr26 = ashr i32 %add25, 5
  %conv27 = trunc i32 %shr26 to i8
  %52 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %53 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx28 = getelementptr inbounds i8, i8* %52, i32 %53
  store i8 %conv27, i8* %arrayidx28, align 1, !tbaa !10
  br label %if.end31

if.else:                                          ; preds = %for.body12
  %54 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %55 = load i32, i32* %max_base_x, align 4, !tbaa !8
  %arrayidx29 = getelementptr inbounds i8, i8* %54, i32 %55
  %56 = load i8, i8* %arrayidx29, align 1, !tbaa !10
  %57 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %58 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx30 = getelementptr inbounds i8, i8* %57, i32 %58
  store i8 %56, i8* %arrayidx30, align 1, !tbaa !10
  br label %if.end31

if.end31:                                         ; preds = %if.else, %if.then15
  br label %for.inc32

for.inc32:                                        ; preds = %if.end31
  %59 = load i32, i32* %c, align 4, !tbaa !8
  %inc33 = add nsw i32 %59, 1
  store i32 %inc33, i32* %c, align 4, !tbaa !8
  %60 = load i32, i32* %base_inc, align 4, !tbaa !8
  %61 = load i32, i32* %base, align 4, !tbaa !8
  %add34 = add nsw i32 %61, %60
  store i32 %add34, i32* %base, align 4, !tbaa !8
  br label %for.cond9

for.end35:                                        ; preds = %for.cond9
  br label %for.inc36

for.inc36:                                        ; preds = %for.end35
  %62 = load i32, i32* %r, align 4, !tbaa !8
  %inc37 = add nsw i32 %62, 1
  store i32 %inc37, i32* %r, align 4, !tbaa !8
  %63 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %64 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr38 = getelementptr inbounds i8, i8* %64, i32 %63
  store i8* %add.ptr38, i8** %dst.addr, align 4, !tbaa !2
  %65 = load i32, i32* %dx.addr, align 4, !tbaa !8
  %66 = load i32, i32* %x, align 4, !tbaa !8
  %add39 = add nsw i32 %66, %65
  store i32 %add39, i32* %x, align 4, !tbaa !8
  br label %for.cond

for.end40:                                        ; preds = %for.cond
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end40, %for.end
  %67 = bitcast i32* %base_inc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast i32* %frac_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast i32* %max_base_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  %71 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  %72 = bitcast i32* %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #6
  %73 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #6
  %74 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #6
  %75 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_dr_prediction_z2_c(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left, i32 %upsample_above, i32 %upsample_left, i32 %dx, i32 %dy) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %upsample_above.addr = alloca i32, align 4
  %upsample_left.addr = alloca i32, align 4
  %dx.addr = alloca i32, align 4
  %dy.addr = alloca i32, align 4
  %min_base_x = alloca i32, align 4
  %min_base_y = alloca i32, align 4
  %frac_bits_x = alloca i32, align 4
  %frac_bits_y = alloca i32, align 4
  %r = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %c = alloca i32, align 4
  %val = alloca i32, align 4
  %y = alloca i32, align 4
  %x = alloca i32, align 4
  %base_x = alloca i32, align 4
  %shift = alloca i32, align 4
  %base_y = alloca i32, align 4
  %shift29 = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  store i32 %upsample_above, i32* %upsample_above.addr, align 4, !tbaa !8
  store i32 %upsample_left, i32* %upsample_left.addr, align 4, !tbaa !8
  store i32 %dx, i32* %dx.addr, align 4, !tbaa !8
  store i32 %dy, i32* %dy.addr, align 4, !tbaa !8
  %0 = bitcast i32* %min_base_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl = shl i32 1, %1
  %sub = sub nsw i32 0, %shl
  store i32 %sub, i32* %min_base_x, align 4, !tbaa !8
  %2 = bitcast i32* %min_base_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl1 = shl i32 1, %3
  %sub2 = sub nsw i32 0, %shl1
  store i32 %sub2, i32* %min_base_y, align 4, !tbaa !8
  %4 = load i32, i32* %min_base_y, align 4, !tbaa !8
  %5 = bitcast i32* %frac_bits_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %sub3 = sub nsw i32 6, %6
  store i32 %sub3, i32* %frac_bits_x, align 4, !tbaa !8
  %7 = bitcast i32* %frac_bits_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %sub4 = sub nsw i32 6, %8
  store i32 %sub4, i32* %frac_bits_y, align 4, !tbaa !8
  %9 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc47, %entry
  %10 = load i32, i32* %r, align 4, !tbaa !8
  %11 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %12 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  br label %for.end49

for.body:                                         ; preds = %for.cond
  %13 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body
  %14 = load i32, i32* %c, align 4, !tbaa !8
  %15 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp6 = icmp slt i32 %14, %15
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond5
  store i32 5, i32* %cleanup.dest.slot, align 4
  %16 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  br label %for.end

for.body8:                                        ; preds = %for.cond5
  %17 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load i32, i32* %r, align 4, !tbaa !8
  %add = add nsw i32 %19, 1
  store i32 %add, i32* %y, align 4, !tbaa !8
  %20 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load i32, i32* %c, align 4, !tbaa !8
  %shl9 = shl i32 %21, 6
  %22 = load i32, i32* %y, align 4, !tbaa !8
  %23 = load i32, i32* %dx.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %22, %23
  %sub10 = sub nsw i32 %shl9, %mul
  store i32 %sub10, i32* %x, align 4, !tbaa !8
  %24 = bitcast i32* %base_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load i32, i32* %x, align 4, !tbaa !8
  %26 = load i32, i32* %frac_bits_x, align 4, !tbaa !8
  %shr = ashr i32 %25, %26
  store i32 %shr, i32* %base_x, align 4, !tbaa !8
  %27 = load i32, i32* %base_x, align 4, !tbaa !8
  %28 = load i32, i32* %min_base_x, align 4, !tbaa !8
  %cmp11 = icmp sge i32 %27, %28
  br i1 %cmp11, label %if.then, label %if.else

if.then:                                          ; preds = %for.body8
  %29 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load i32, i32* %x, align 4, !tbaa !8
  %31 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl12 = shl i32 1, %31
  %mul13 = mul nsw i32 %30, %shl12
  %and = and i32 %mul13, 63
  %shr14 = ashr i32 %and, 1
  store i32 %shr14, i32* %shift, align 4, !tbaa !8
  %32 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %33 = load i32, i32* %base_x, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i8, i8* %32, i32 %33
  %34 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %34 to i32
  %35 = load i32, i32* %shift, align 4, !tbaa !8
  %sub15 = sub nsw i32 32, %35
  %mul16 = mul nsw i32 %conv, %sub15
  %36 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %37 = load i32, i32* %base_x, align 4, !tbaa !8
  %add17 = add nsw i32 %37, 1
  %arrayidx18 = getelementptr inbounds i8, i8* %36, i32 %add17
  %38 = load i8, i8* %arrayidx18, align 1, !tbaa !10
  %conv19 = zext i8 %38 to i32
  %39 = load i32, i32* %shift, align 4, !tbaa !8
  %mul20 = mul nsw i32 %conv19, %39
  %add21 = add nsw i32 %mul16, %mul20
  store i32 %add21, i32* %val, align 4, !tbaa !8
  %40 = load i32, i32* %val, align 4, !tbaa !8
  %add22 = add nsw i32 %40, 16
  %shr23 = ashr i32 %add22, 5
  store i32 %shr23, i32* %val, align 4, !tbaa !8
  %41 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #6
  br label %if.end

if.else:                                          ; preds = %for.body8
  %42 = load i32, i32* %c, align 4, !tbaa !8
  %add24 = add nsw i32 %42, 1
  store i32 %add24, i32* %x, align 4, !tbaa !8
  %43 = load i32, i32* %r, align 4, !tbaa !8
  %shl25 = shl i32 %43, 6
  %44 = load i32, i32* %x, align 4, !tbaa !8
  %45 = load i32, i32* %dy.addr, align 4, !tbaa !8
  %mul26 = mul nsw i32 %44, %45
  %sub27 = sub nsw i32 %shl25, %mul26
  store i32 %sub27, i32* %y, align 4, !tbaa !8
  %46 = bitcast i32* %base_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #6
  %47 = load i32, i32* %y, align 4, !tbaa !8
  %48 = load i32, i32* %frac_bits_y, align 4, !tbaa !8
  %shr28 = ashr i32 %47, %48
  store i32 %shr28, i32* %base_y, align 4, !tbaa !8
  %49 = bitcast i32* %shift29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #6
  %50 = load i32, i32* %y, align 4, !tbaa !8
  %51 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl30 = shl i32 1, %51
  %mul31 = mul nsw i32 %50, %shl30
  %and32 = and i32 %mul31, 63
  %shr33 = ashr i32 %and32, 1
  store i32 %shr33, i32* %shift29, align 4, !tbaa !8
  %52 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %53 = load i32, i32* %base_y, align 4, !tbaa !8
  %arrayidx34 = getelementptr inbounds i8, i8* %52, i32 %53
  %54 = load i8, i8* %arrayidx34, align 1, !tbaa !10
  %conv35 = zext i8 %54 to i32
  %55 = load i32, i32* %shift29, align 4, !tbaa !8
  %sub36 = sub nsw i32 32, %55
  %mul37 = mul nsw i32 %conv35, %sub36
  %56 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %57 = load i32, i32* %base_y, align 4, !tbaa !8
  %add38 = add nsw i32 %57, 1
  %arrayidx39 = getelementptr inbounds i8, i8* %56, i32 %add38
  %58 = load i8, i8* %arrayidx39, align 1, !tbaa !10
  %conv40 = zext i8 %58 to i32
  %59 = load i32, i32* %shift29, align 4, !tbaa !8
  %mul41 = mul nsw i32 %conv40, %59
  %add42 = add nsw i32 %mul37, %mul41
  store i32 %add42, i32* %val, align 4, !tbaa !8
  %60 = load i32, i32* %val, align 4, !tbaa !8
  %add43 = add nsw i32 %60, 16
  %shr44 = ashr i32 %add43, 5
  store i32 %shr44, i32* %val, align 4, !tbaa !8
  %61 = bitcast i32* %shift29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i32* %base_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %63 = load i32, i32* %val, align 4, !tbaa !8
  %conv45 = trunc i32 %63 to i8
  %64 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %65 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx46 = getelementptr inbounds i8, i8* %64, i32 %65
  store i8 %conv45, i8* %arrayidx46, align 1, !tbaa !10
  %66 = bitcast i32* %base_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %70 = load i32, i32* %c, align 4, !tbaa !8
  %inc = add nsw i32 %70, 1
  store i32 %inc, i32* %c, align 4, !tbaa !8
  br label %for.cond5

for.end:                                          ; preds = %for.cond.cleanup7
  %71 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %72 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %72, i32 %71
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc47

for.inc47:                                        ; preds = %for.end
  %73 = load i32, i32* %r, align 4, !tbaa !8
  %inc48 = add nsw i32 %73, 1
  store i32 %inc48, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end49:                                        ; preds = %for.cond.cleanup
  %74 = bitcast i32* %frac_bits_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #6
  %75 = bitcast i32* %frac_bits_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #6
  %76 = bitcast i32* %min_base_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  %77 = bitcast i32* %min_base_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_dr_prediction_z3_c(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left, i32 %upsample_left, i32 %dx, i32 %dy) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %upsample_left.addr = alloca i32, align 4
  %dx.addr = alloca i32, align 4
  %dy.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %y = alloca i32, align 4
  %base = alloca i32, align 4
  %shift = alloca i32, align 4
  %val = alloca i32, align 4
  %max_base_y = alloca i32, align 4
  %frac_bits = alloca i32, align 4
  %base_inc = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  store i32 %upsample_left, i32* %upsample_left.addr, align 4, !tbaa !8
  store i32 %dx, i32* %dx.addr, align 4, !tbaa !8
  store i32 %dy, i32* %dy.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %7 = load i32, i32* %dx.addr, align 4, !tbaa !8
  %8 = bitcast i32* %max_base_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %10 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add = add nsw i32 %9, %10
  %sub = sub nsw i32 %add, 1
  %11 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl = shl i32 %sub, %11
  store i32 %shl, i32* %max_base_y, align 4, !tbaa !8
  %12 = bitcast i32* %frac_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %sub1 = sub nsw i32 6, %13
  store i32 %sub1, i32* %frac_bits, align 4, !tbaa !8
  %14 = bitcast i32* %base_inc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl2 = shl i32 1, %15
  store i32 %shl2, i32* %base_inc, align 4, !tbaa !8
  %16 = load i32, i32* %dy.addr, align 4, !tbaa !8
  store i32 %16, i32* %y, align 4, !tbaa !8
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc33, %entry
  %17 = load i32, i32* %c, align 4, !tbaa !8
  %18 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.end36

for.body:                                         ; preds = %for.cond
  %19 = load i32, i32* %y, align 4, !tbaa !8
  %20 = load i32, i32* %frac_bits, align 4, !tbaa !8
  %shr = ashr i32 %19, %20
  store i32 %shr, i32* %base, align 4, !tbaa !8
  %21 = load i32, i32* %y, align 4, !tbaa !8
  %22 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl3 = shl i32 %21, %22
  %and = and i32 %shl3, 63
  %shr4 = ashr i32 %and, 1
  store i32 %shr4, i32* %shift, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc29, %for.body
  %23 = load i32, i32* %r, align 4, !tbaa !8
  %24 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp6 = icmp slt i32 %23, %24
  br i1 %cmp6, label %for.body7, label %for.end32

for.body7:                                        ; preds = %for.cond5
  %25 = load i32, i32* %base, align 4, !tbaa !8
  %26 = load i32, i32* %max_base_y, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %25, %26
  br i1 %cmp8, label %if.then, label %if.else

if.then:                                          ; preds = %for.body7
  %27 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %28 = load i32, i32* %base, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i8, i8* %27, i32 %28
  %29 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %29 to i32
  %30 = load i32, i32* %shift, align 4, !tbaa !8
  %sub9 = sub nsw i32 32, %30
  %mul = mul nsw i32 %conv, %sub9
  %31 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %32 = load i32, i32* %base, align 4, !tbaa !8
  %add10 = add nsw i32 %32, 1
  %arrayidx11 = getelementptr inbounds i8, i8* %31, i32 %add10
  %33 = load i8, i8* %arrayidx11, align 1, !tbaa !10
  %conv12 = zext i8 %33 to i32
  %34 = load i32, i32* %shift, align 4, !tbaa !8
  %mul13 = mul nsw i32 %conv12, %34
  %add14 = add nsw i32 %mul, %mul13
  store i32 %add14, i32* %val, align 4, !tbaa !8
  %35 = load i32, i32* %val, align 4, !tbaa !8
  %add15 = add nsw i32 %35, 16
  %shr16 = ashr i32 %add15, 5
  store i32 %shr16, i32* %val, align 4, !tbaa !8
  %conv17 = trunc i32 %shr16 to i8
  %36 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %37 = load i32, i32* %r, align 4, !tbaa !8
  %38 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 %37, %38
  %39 = load i32, i32* %c, align 4, !tbaa !8
  %add19 = add nsw i32 %mul18, %39
  %arrayidx20 = getelementptr inbounds i8, i8* %36, i32 %add19
  store i8 %conv17, i8* %arrayidx20, align 1, !tbaa !10
  br label %if.end

if.else:                                          ; preds = %for.body7
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc, %if.else
  %40 = load i32, i32* %r, align 4, !tbaa !8
  %41 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp22 = icmp slt i32 %40, %41
  br i1 %cmp22, label %for.body24, label %for.end

for.body24:                                       ; preds = %for.cond21
  %42 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %43 = load i32, i32* %max_base_y, align 4, !tbaa !8
  %arrayidx25 = getelementptr inbounds i8, i8* %42, i32 %43
  %44 = load i8, i8* %arrayidx25, align 1, !tbaa !10
  %45 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %46 = load i32, i32* %r, align 4, !tbaa !8
  %47 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %46, %47
  %48 = load i32, i32* %c, align 4, !tbaa !8
  %add27 = add nsw i32 %mul26, %48
  %arrayidx28 = getelementptr inbounds i8, i8* %45, i32 %add27
  store i8 %44, i8* %arrayidx28, align 1, !tbaa !10
  br label %for.inc

for.inc:                                          ; preds = %for.body24
  %49 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond21

for.end:                                          ; preds = %for.cond21
  br label %for.end32

if.end:                                           ; preds = %if.then
  br label %for.inc29

for.inc29:                                        ; preds = %if.end
  %50 = load i32, i32* %r, align 4, !tbaa !8
  %inc30 = add nsw i32 %50, 1
  store i32 %inc30, i32* %r, align 4, !tbaa !8
  %51 = load i32, i32* %base_inc, align 4, !tbaa !8
  %52 = load i32, i32* %base, align 4, !tbaa !8
  %add31 = add nsw i32 %52, %51
  store i32 %add31, i32* %base, align 4, !tbaa !8
  br label %for.cond5

for.end32:                                        ; preds = %for.end, %for.cond5
  br label %for.inc33

for.inc33:                                        ; preds = %for.end32
  %53 = load i32, i32* %c, align 4, !tbaa !8
  %inc34 = add nsw i32 %53, 1
  store i32 %inc34, i32* %c, align 4, !tbaa !8
  %54 = load i32, i32* %dy.addr, align 4, !tbaa !8
  %55 = load i32, i32* %y, align 4, !tbaa !8
  %add35 = add nsw i32 %55, %54
  store i32 %add35, i32* %y, align 4, !tbaa !8
  br label %for.cond

for.end36:                                        ; preds = %for.cond
  %56 = bitcast i32* %base_inc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  %57 = bitcast i32* %frac_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  %58 = bitcast i32* %max_base_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_dr_prediction_z1_c(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %upsample_above, i32 %dx, i32 %dy, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %upsample_above.addr = alloca i32, align 4
  %dx.addr = alloca i32, align 4
  %dy.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %x = alloca i32, align 4
  %base = alloca i32, align 4
  %shift = alloca i32, align 4
  %val = alloca i32, align 4
  %max_base_x = alloca i32, align 4
  %frac_bits = alloca i32, align 4
  %base_inc = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %upsample_above, i32* %upsample_above.addr, align 4, !tbaa !8
  store i32 %dx, i32* %dx.addr, align 4, !tbaa !8
  store i32 %dy, i32* %dy.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %7 = load i32, i32* %dy.addr, align 4, !tbaa !8
  %8 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %9 = bitcast i32* %max_base_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %11 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add = add nsw i32 %10, %11
  %sub = sub nsw i32 %add, 1
  %12 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl = shl i32 %sub, %12
  store i32 %shl, i32* %max_base_x, align 4, !tbaa !8
  %13 = bitcast i32* %frac_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %sub1 = sub nsw i32 6, %14
  store i32 %sub1, i32* %frac_bits, align 4, !tbaa !8
  %15 = bitcast i32* %base_inc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl2 = shl i32 1, %16
  store i32 %shl2, i32* %base_inc, align 4, !tbaa !8
  %17 = load i32, i32* %dx.addr, align 4, !tbaa !8
  store i32 %17, i32* %x, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc35, %entry
  %18 = load i32, i32* %r, align 4, !tbaa !8
  %19 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %18, %19
  br i1 %cmp, label %for.body, label %for.end39

for.body:                                         ; preds = %for.cond
  %20 = load i32, i32* %x, align 4, !tbaa !8
  %21 = load i32, i32* %frac_bits, align 4, !tbaa !8
  %shr = ashr i32 %20, %21
  store i32 %shr, i32* %base, align 4, !tbaa !8
  %22 = load i32, i32* %x, align 4, !tbaa !8
  %23 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl3 = shl i32 %22, %23
  %and = and i32 %shl3, 63
  %shr4 = ashr i32 %and, 1
  store i32 %shr4, i32* %shift, align 4, !tbaa !8
  %24 = load i32, i32* %base, align 4, !tbaa !8
  %25 = load i32, i32* %max_base_x, align 4, !tbaa !8
  %cmp5 = icmp sge i32 %24, %25
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %26 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load i32, i32* %r, align 4, !tbaa !8
  store i32 %27, i32* %i, align 4, !tbaa !8
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %if.then
  %28 = load i32, i32* %i, align 4, !tbaa !8
  %29 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp7 = icmp slt i32 %28, %29
  br i1 %cmp7, label %for.body8, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond6
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  br label %for.end

for.body8:                                        ; preds = %for.cond6
  %31 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %32 = bitcast i16* %31 to i8*
  %33 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %34 = load i32, i32* %max_base_x, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %33, i32 %34
  %35 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %35 to i32
  %36 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %call = call i8* @aom_memset16(i8* %32, i32 %conv, i32 %36)
  %37 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %38 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %38, i32 %37
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %39 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %39, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond6

for.end:                                          ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc31, %if.end
  %40 = load i32, i32* %c, align 4, !tbaa !8
  %41 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp10 = icmp slt i32 %40, %41
  br i1 %cmp10, label %for.body12, label %for.end34

for.body12:                                       ; preds = %for.cond9
  %42 = load i32, i32* %base, align 4, !tbaa !8
  %43 = load i32, i32* %max_base_x, align 4, !tbaa !8
  %cmp13 = icmp slt i32 %42, %43
  br i1 %cmp13, label %if.then15, label %if.else

if.then15:                                        ; preds = %for.body12
  %44 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %45 = load i32, i32* %base, align 4, !tbaa !8
  %arrayidx16 = getelementptr inbounds i16, i16* %44, i32 %45
  %46 = load i16, i16* %arrayidx16, align 2, !tbaa !11
  %conv17 = zext i16 %46 to i32
  %47 = load i32, i32* %shift, align 4, !tbaa !8
  %sub18 = sub nsw i32 32, %47
  %mul = mul nsw i32 %conv17, %sub18
  %48 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %49 = load i32, i32* %base, align 4, !tbaa !8
  %add19 = add nsw i32 %49, 1
  %arrayidx20 = getelementptr inbounds i16, i16* %48, i32 %add19
  %50 = load i16, i16* %arrayidx20, align 2, !tbaa !11
  %conv21 = zext i16 %50 to i32
  %51 = load i32, i32* %shift, align 4, !tbaa !8
  %mul22 = mul nsw i32 %conv21, %51
  %add23 = add nsw i32 %mul, %mul22
  store i32 %add23, i32* %val, align 4, !tbaa !8
  %52 = load i32, i32* %val, align 4, !tbaa !8
  %add24 = add nsw i32 %52, 16
  %shr25 = ashr i32 %add24, 5
  %conv26 = trunc i32 %shr25 to i16
  %53 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %54 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx27 = getelementptr inbounds i16, i16* %53, i32 %54
  store i16 %conv26, i16* %arrayidx27, align 2, !tbaa !11
  br label %if.end30

if.else:                                          ; preds = %for.body12
  %55 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %56 = load i32, i32* %max_base_x, align 4, !tbaa !8
  %arrayidx28 = getelementptr inbounds i16, i16* %55, i32 %56
  %57 = load i16, i16* %arrayidx28, align 2, !tbaa !11
  %58 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %59 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx29 = getelementptr inbounds i16, i16* %58, i32 %59
  store i16 %57, i16* %arrayidx29, align 2, !tbaa !11
  br label %if.end30

if.end30:                                         ; preds = %if.else, %if.then15
  br label %for.inc31

for.inc31:                                        ; preds = %if.end30
  %60 = load i32, i32* %c, align 4, !tbaa !8
  %inc32 = add nsw i32 %60, 1
  store i32 %inc32, i32* %c, align 4, !tbaa !8
  %61 = load i32, i32* %base_inc, align 4, !tbaa !8
  %62 = load i32, i32* %base, align 4, !tbaa !8
  %add33 = add nsw i32 %62, %61
  store i32 %add33, i32* %base, align 4, !tbaa !8
  br label %for.cond9

for.end34:                                        ; preds = %for.cond9
  br label %for.inc35

for.inc35:                                        ; preds = %for.end34
  %63 = load i32, i32* %r, align 4, !tbaa !8
  %inc36 = add nsw i32 %63, 1
  store i32 %inc36, i32* %r, align 4, !tbaa !8
  %64 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %65 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr37 = getelementptr inbounds i16, i16* %65, i32 %64
  store i16* %add.ptr37, i16** %dst.addr, align 4, !tbaa !2
  %66 = load i32, i32* %dx.addr, align 4, !tbaa !8
  %67 = load i32, i32* %x, align 4, !tbaa !8
  %add38 = add nsw i32 %67, %66
  store i32 %add38, i32* %x, align 4, !tbaa !8
  br label %for.cond

for.end39:                                        ; preds = %for.cond
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end39, %for.end
  %68 = bitcast i32* %base_inc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast i32* %frac_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast i32* %max_base_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  %71 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  %72 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #6
  %73 = bitcast i32* %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #6
  %74 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #6
  %75 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #6
  %76 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare i8* @aom_memset16(i8*, i32, i32) #3

; Function Attrs: nounwind
define hidden void @av1_highbd_dr_prediction_z2_c(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %upsample_above, i32 %upsample_left, i32 %dx, i32 %dy, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %upsample_above.addr = alloca i32, align 4
  %upsample_left.addr = alloca i32, align 4
  %dx.addr = alloca i32, align 4
  %dy.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %min_base_x = alloca i32, align 4
  %min_base_y = alloca i32, align 4
  %frac_bits_x = alloca i32, align 4
  %frac_bits_y = alloca i32, align 4
  %r = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %c = alloca i32, align 4
  %val = alloca i32, align 4
  %y = alloca i32, align 4
  %x = alloca i32, align 4
  %base_x = alloca i32, align 4
  %shift = alloca i32, align 4
  %base_y = alloca i32, align 4
  %shift29 = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %upsample_above, i32* %upsample_above.addr, align 4, !tbaa !8
  store i32 %upsample_left, i32* %upsample_left.addr, align 4, !tbaa !8
  store i32 %dx, i32* %dx.addr, align 4, !tbaa !8
  store i32 %dy, i32* %dy.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %1 = bitcast i32* %min_base_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl = shl i32 1, %2
  %sub = sub nsw i32 0, %shl
  store i32 %sub, i32* %min_base_x, align 4, !tbaa !8
  %3 = bitcast i32* %min_base_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl1 = shl i32 1, %4
  %sub2 = sub nsw i32 0, %shl1
  store i32 %sub2, i32* %min_base_y, align 4, !tbaa !8
  %5 = load i32, i32* %min_base_y, align 4, !tbaa !8
  %6 = bitcast i32* %frac_bits_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %sub3 = sub nsw i32 6, %7
  store i32 %sub3, i32* %frac_bits_x, align 4, !tbaa !8
  %8 = bitcast i32* %frac_bits_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %sub4 = sub nsw i32 6, %9
  store i32 %sub4, i32* %frac_bits_y, align 4, !tbaa !8
  %10 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc47, %entry
  %11 = load i32, i32* %r, align 4, !tbaa !8
  %12 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  br label %for.end49

for.body:                                         ; preds = %for.cond
  %14 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body
  %15 = load i32, i32* %c, align 4, !tbaa !8
  %16 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp6 = icmp slt i32 %15, %16
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond5
  store i32 5, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %for.end

for.body8:                                        ; preds = %for.cond5
  %18 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i32, i32* %r, align 4, !tbaa !8
  %add = add nsw i32 %20, 1
  store i32 %add, i32* %y, align 4, !tbaa !8
  %21 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load i32, i32* %c, align 4, !tbaa !8
  %shl9 = shl i32 %22, 6
  %23 = load i32, i32* %y, align 4, !tbaa !8
  %24 = load i32, i32* %dx.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %23, %24
  %sub10 = sub nsw i32 %shl9, %mul
  store i32 %sub10, i32* %x, align 4, !tbaa !8
  %25 = bitcast i32* %base_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load i32, i32* %x, align 4, !tbaa !8
  %27 = load i32, i32* %frac_bits_x, align 4, !tbaa !8
  %shr = ashr i32 %26, %27
  store i32 %shr, i32* %base_x, align 4, !tbaa !8
  %28 = load i32, i32* %base_x, align 4, !tbaa !8
  %29 = load i32, i32* %min_base_x, align 4, !tbaa !8
  %cmp11 = icmp sge i32 %28, %29
  br i1 %cmp11, label %if.then, label %if.else

if.then:                                          ; preds = %for.body8
  %30 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load i32, i32* %x, align 4, !tbaa !8
  %32 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %shl12 = shl i32 1, %32
  %mul13 = mul nsw i32 %31, %shl12
  %and = and i32 %mul13, 63
  %shr14 = ashr i32 %and, 1
  store i32 %shr14, i32* %shift, align 4, !tbaa !8
  %33 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %34 = load i32, i32* %base_x, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %33, i32 %34
  %35 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %35 to i32
  %36 = load i32, i32* %shift, align 4, !tbaa !8
  %sub15 = sub nsw i32 32, %36
  %mul16 = mul nsw i32 %conv, %sub15
  %37 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %38 = load i32, i32* %base_x, align 4, !tbaa !8
  %add17 = add nsw i32 %38, 1
  %arrayidx18 = getelementptr inbounds i16, i16* %37, i32 %add17
  %39 = load i16, i16* %arrayidx18, align 2, !tbaa !11
  %conv19 = zext i16 %39 to i32
  %40 = load i32, i32* %shift, align 4, !tbaa !8
  %mul20 = mul nsw i32 %conv19, %40
  %add21 = add nsw i32 %mul16, %mul20
  store i32 %add21, i32* %val, align 4, !tbaa !8
  %41 = load i32, i32* %val, align 4, !tbaa !8
  %add22 = add nsw i32 %41, 16
  %shr23 = ashr i32 %add22, 5
  store i32 %shr23, i32* %val, align 4, !tbaa !8
  %42 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  br label %if.end

if.else:                                          ; preds = %for.body8
  %43 = load i32, i32* %c, align 4, !tbaa !8
  %add24 = add nsw i32 %43, 1
  store i32 %add24, i32* %x, align 4, !tbaa !8
  %44 = load i32, i32* %r, align 4, !tbaa !8
  %shl25 = shl i32 %44, 6
  %45 = load i32, i32* %x, align 4, !tbaa !8
  %46 = load i32, i32* %dy.addr, align 4, !tbaa !8
  %mul26 = mul nsw i32 %45, %46
  %sub27 = sub nsw i32 %shl25, %mul26
  store i32 %sub27, i32* %y, align 4, !tbaa !8
  %47 = bitcast i32* %base_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  %48 = load i32, i32* %y, align 4, !tbaa !8
  %49 = load i32, i32* %frac_bits_y, align 4, !tbaa !8
  %shr28 = ashr i32 %48, %49
  store i32 %shr28, i32* %base_y, align 4, !tbaa !8
  %50 = bitcast i32* %shift29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #6
  %51 = load i32, i32* %y, align 4, !tbaa !8
  %52 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl30 = shl i32 1, %52
  %mul31 = mul nsw i32 %51, %shl30
  %and32 = and i32 %mul31, 63
  %shr33 = ashr i32 %and32, 1
  store i32 %shr33, i32* %shift29, align 4, !tbaa !8
  %53 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %54 = load i32, i32* %base_y, align 4, !tbaa !8
  %arrayidx34 = getelementptr inbounds i16, i16* %53, i32 %54
  %55 = load i16, i16* %arrayidx34, align 2, !tbaa !11
  %conv35 = zext i16 %55 to i32
  %56 = load i32, i32* %shift29, align 4, !tbaa !8
  %sub36 = sub nsw i32 32, %56
  %mul37 = mul nsw i32 %conv35, %sub36
  %57 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %58 = load i32, i32* %base_y, align 4, !tbaa !8
  %add38 = add nsw i32 %58, 1
  %arrayidx39 = getelementptr inbounds i16, i16* %57, i32 %add38
  %59 = load i16, i16* %arrayidx39, align 2, !tbaa !11
  %conv40 = zext i16 %59 to i32
  %60 = load i32, i32* %shift29, align 4, !tbaa !8
  %mul41 = mul nsw i32 %conv40, %60
  %add42 = add nsw i32 %mul37, %mul41
  store i32 %add42, i32* %val, align 4, !tbaa !8
  %61 = load i32, i32* %val, align 4, !tbaa !8
  %add43 = add nsw i32 %61, 16
  %shr44 = ashr i32 %add43, 5
  store i32 %shr44, i32* %val, align 4, !tbaa !8
  %62 = bitcast i32* %shift29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast i32* %base_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %64 = load i32, i32* %val, align 4, !tbaa !8
  %conv45 = trunc i32 %64 to i16
  %65 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %66 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx46 = getelementptr inbounds i16, i16* %65, i32 %66
  store i16 %conv45, i16* %arrayidx46, align 2, !tbaa !11
  %67 = bitcast i32* %base_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %71 = load i32, i32* %c, align 4, !tbaa !8
  %inc = add nsw i32 %71, 1
  store i32 %inc, i32* %c, align 4, !tbaa !8
  br label %for.cond5

for.end:                                          ; preds = %for.cond.cleanup7
  %72 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %73 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %73, i32 %72
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc47

for.inc47:                                        ; preds = %for.end
  %74 = load i32, i32* %r, align 4, !tbaa !8
  %inc48 = add nsw i32 %74, 1
  store i32 %inc48, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end49:                                        ; preds = %for.cond.cleanup
  %75 = bitcast i32* %frac_bits_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #6
  %76 = bitcast i32* %frac_bits_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  %77 = bitcast i32* %min_base_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #6
  %78 = bitcast i32* %min_base_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_dr_prediction_z3_c(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %upsample_left, i32 %dx, i32 %dy, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %upsample_left.addr = alloca i32, align 4
  %dx.addr = alloca i32, align 4
  %dy.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %y = alloca i32, align 4
  %base = alloca i32, align 4
  %shift = alloca i32, align 4
  %val = alloca i32, align 4
  %max_base_y = alloca i32, align 4
  %frac_bits = alloca i32, align 4
  %base_inc = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %upsample_left, i32* %upsample_left.addr, align 4, !tbaa !8
  store i32 %dx, i32* %dx.addr, align 4, !tbaa !8
  store i32 %dy, i32* %dy.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %7 = load i32, i32* %dx.addr, align 4, !tbaa !8
  %8 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %9 = bitcast i32* %max_base_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %11 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add = add nsw i32 %10, %11
  %sub = sub nsw i32 %add, 1
  %12 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl = shl i32 %sub, %12
  store i32 %shl, i32* %max_base_y, align 4, !tbaa !8
  %13 = bitcast i32* %frac_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %sub1 = sub nsw i32 6, %14
  store i32 %sub1, i32* %frac_bits, align 4, !tbaa !8
  %15 = bitcast i32* %base_inc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl2 = shl i32 1, %16
  store i32 %shl2, i32* %base_inc, align 4, !tbaa !8
  %17 = load i32, i32* %dy.addr, align 4, !tbaa !8
  store i32 %17, i32* %y, align 4, !tbaa !8
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc33, %entry
  %18 = load i32, i32* %c, align 4, !tbaa !8
  %19 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %18, %19
  br i1 %cmp, label %for.body, label %for.end36

for.body:                                         ; preds = %for.cond
  %20 = load i32, i32* %y, align 4, !tbaa !8
  %21 = load i32, i32* %frac_bits, align 4, !tbaa !8
  %shr = ashr i32 %20, %21
  store i32 %shr, i32* %base, align 4, !tbaa !8
  %22 = load i32, i32* %y, align 4, !tbaa !8
  %23 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %shl3 = shl i32 %22, %23
  %and = and i32 %shl3, 63
  %shr4 = ashr i32 %and, 1
  store i32 %shr4, i32* %shift, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc29, %for.body
  %24 = load i32, i32* %r, align 4, !tbaa !8
  %25 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp6 = icmp slt i32 %24, %25
  br i1 %cmp6, label %for.body7, label %for.end32

for.body7:                                        ; preds = %for.cond5
  %26 = load i32, i32* %base, align 4, !tbaa !8
  %27 = load i32, i32* %max_base_y, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %26, %27
  br i1 %cmp8, label %if.then, label %if.else

if.then:                                          ; preds = %for.body7
  %28 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %29 = load i32, i32* %base, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %28, i32 %29
  %30 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %30 to i32
  %31 = load i32, i32* %shift, align 4, !tbaa !8
  %sub9 = sub nsw i32 32, %31
  %mul = mul nsw i32 %conv, %sub9
  %32 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %33 = load i32, i32* %base, align 4, !tbaa !8
  %add10 = add nsw i32 %33, 1
  %arrayidx11 = getelementptr inbounds i16, i16* %32, i32 %add10
  %34 = load i16, i16* %arrayidx11, align 2, !tbaa !11
  %conv12 = zext i16 %34 to i32
  %35 = load i32, i32* %shift, align 4, !tbaa !8
  %mul13 = mul nsw i32 %conv12, %35
  %add14 = add nsw i32 %mul, %mul13
  store i32 %add14, i32* %val, align 4, !tbaa !8
  %36 = load i32, i32* %val, align 4, !tbaa !8
  %add15 = add nsw i32 %36, 16
  %shr16 = ashr i32 %add15, 5
  %conv17 = trunc i32 %shr16 to i16
  %37 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %38 = load i32, i32* %r, align 4, !tbaa !8
  %39 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 %38, %39
  %40 = load i32, i32* %c, align 4, !tbaa !8
  %add19 = add nsw i32 %mul18, %40
  %arrayidx20 = getelementptr inbounds i16, i16* %37, i32 %add19
  store i16 %conv17, i16* %arrayidx20, align 2, !tbaa !11
  br label %if.end

if.else:                                          ; preds = %for.body7
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc, %if.else
  %41 = load i32, i32* %r, align 4, !tbaa !8
  %42 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp22 = icmp slt i32 %41, %42
  br i1 %cmp22, label %for.body24, label %for.end

for.body24:                                       ; preds = %for.cond21
  %43 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %44 = load i32, i32* %max_base_y, align 4, !tbaa !8
  %arrayidx25 = getelementptr inbounds i16, i16* %43, i32 %44
  %45 = load i16, i16* %arrayidx25, align 2, !tbaa !11
  %46 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %47 = load i32, i32* %r, align 4, !tbaa !8
  %48 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %47, %48
  %49 = load i32, i32* %c, align 4, !tbaa !8
  %add27 = add nsw i32 %mul26, %49
  %arrayidx28 = getelementptr inbounds i16, i16* %46, i32 %add27
  store i16 %45, i16* %arrayidx28, align 2, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body24
  %50 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %50, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond21

for.end:                                          ; preds = %for.cond21
  br label %for.end32

if.end:                                           ; preds = %if.then
  br label %for.inc29

for.inc29:                                        ; preds = %if.end
  %51 = load i32, i32* %r, align 4, !tbaa !8
  %inc30 = add nsw i32 %51, 1
  store i32 %inc30, i32* %r, align 4, !tbaa !8
  %52 = load i32, i32* %base_inc, align 4, !tbaa !8
  %53 = load i32, i32* %base, align 4, !tbaa !8
  %add31 = add nsw i32 %53, %52
  store i32 %add31, i32* %base, align 4, !tbaa !8
  br label %for.cond5

for.end32:                                        ; preds = %for.end, %for.cond5
  br label %for.inc33

for.inc33:                                        ; preds = %for.end32
  %54 = load i32, i32* %c, align 4, !tbaa !8
  %inc34 = add nsw i32 %54, 1
  store i32 %inc34, i32* %c, align 4, !tbaa !8
  %55 = load i32, i32* %dy.addr, align 4, !tbaa !8
  %56 = load i32, i32* %y, align 4, !tbaa !8
  %add35 = add nsw i32 %56, %55
  store i32 %add35, i32* %y, align 4, !tbaa !8
  br label %for.cond

for.end36:                                        ; preds = %for.cond
  %57 = bitcast i32* %base_inc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  %58 = bitcast i32* %frac_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast i32* %max_base_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i32* %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_filter_intra_predictor_c(i8* %dst, i32 %stride, i8 zeroext %tx_size, i8* %above, i8* %left, i32 %mode) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %mode.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %buffer = alloca [33 x [33 x i8]], align 16
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %p0 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p2 = alloca i8, align 1
  %p3 = alloca i8, align 1
  %p4 = alloca i8, align 1
  %p5 = alloca i8, align 1
  %p6 = alloca i8, align 1
  %k = alloca i32, align 4
  %r_offset = alloca i32, align 4
  %c_offset = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  store i32 %mode, i32* %mode.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast [33 x [33 x i8]]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 1089, i8* %2) #6
  %3 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %4 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %5 = load i32, i32* %arrayidx, align 4, !tbaa !8
  store i32 %5, i32* %bw, align 4, !tbaa !8
  %6 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom1 = zext i8 %7 to i32
  %arrayidx2 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom1
  %8 = load i32, i32* %arrayidx2, align 4, !tbaa !8
  store i32 %8, i32* %bh, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %r, align 4, !tbaa !8
  %10 = load i32, i32* %bh, align 4, !tbaa !8
  %add = add nsw i32 %10, 1
  %cmp = icmp slt i32 %9, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %11
  %arraydecay = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx3, i32 0, i32 0
  %12 = load i32, i32* %bw, align 4, !tbaa !8
  %add4 = add nsw i32 %12, 1
  %mul = mul i32 %add4, 1
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay, i8 0, i32 %mul, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc12, %for.end
  %14 = load i32, i32* %r, align 4, !tbaa !8
  %15 = load i32, i32* %bh, align 4, !tbaa !8
  %cmp6 = icmp slt i32 %14, %15
  br i1 %cmp6, label %for.body7, label %for.end14

for.body7:                                        ; preds = %for.cond5
  %16 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %17 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx8 = getelementptr inbounds i8, i8* %16, i32 %17
  %18 = load i8, i8* %arrayidx8, align 1, !tbaa !10
  %19 = load i32, i32* %r, align 4, !tbaa !8
  %add9 = add nsw i32 %19, 1
  %arrayidx10 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %add9
  %arrayidx11 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx10, i32 0, i32 0
  store i8 %18, i8* %arrayidx11, align 1, !tbaa !10
  br label %for.inc12

for.inc12:                                        ; preds = %for.body7
  %20 = load i32, i32* %r, align 4, !tbaa !8
  %inc13 = add nsw i32 %20, 1
  store i32 %inc13, i32* %r, align 4, !tbaa !8
  br label %for.cond5

for.end14:                                        ; preds = %for.cond5
  %arrayidx15 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 0
  %arraydecay16 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx15, i32 0, i32 0
  %21 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i8, i8* %21, i32 -1
  %22 = load i32, i32* %bw, align 4, !tbaa !8
  %add18 = add nsw i32 %22, 1
  %mul19 = mul i32 %add18, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay16, i8* align 1 %arrayidx17, i32 %mul19, i1 false)
  store i32 1, i32* %r, align 4, !tbaa !8
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc217, %for.end14
  %23 = load i32, i32* %r, align 4, !tbaa !8
  %24 = load i32, i32* %bh, align 4, !tbaa !8
  %add21 = add nsw i32 %24, 1
  %cmp22 = icmp slt i32 %23, %add21
  br i1 %cmp22, label %for.body23, label %for.end219

for.body23:                                       ; preds = %for.cond20
  store i32 1, i32* %c, align 4, !tbaa !8
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc214, %for.body23
  %25 = load i32, i32* %c, align 4, !tbaa !8
  %26 = load i32, i32* %bw, align 4, !tbaa !8
  %add25 = add nsw i32 %26, 1
  %cmp26 = icmp slt i32 %25, %add25
  br i1 %cmp26, label %for.body27, label %for.end216

for.body27:                                       ; preds = %for.cond24
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #6
  %27 = load i32, i32* %r, align 4, !tbaa !8
  %sub = sub nsw i32 %27, 1
  %arrayidx28 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %sub
  %28 = load i32, i32* %c, align 4, !tbaa !8
  %sub29 = sub nsw i32 %28, 1
  %arrayidx30 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx28, i32 0, i32 %sub29
  %29 = load i8, i8* %arrayidx30, align 1, !tbaa !10
  store i8 %29, i8* %p0, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #6
  %30 = load i32, i32* %r, align 4, !tbaa !8
  %sub31 = sub nsw i32 %30, 1
  %arrayidx32 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %sub31
  %31 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx33 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx32, i32 0, i32 %31
  %32 = load i8, i8* %arrayidx33, align 1, !tbaa !10
  store i8 %32, i8* %p1, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #6
  %33 = load i32, i32* %r, align 4, !tbaa !8
  %sub34 = sub nsw i32 %33, 1
  %arrayidx35 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %sub34
  %34 = load i32, i32* %c, align 4, !tbaa !8
  %add36 = add nsw i32 %34, 1
  %arrayidx37 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx35, i32 0, i32 %add36
  %35 = load i8, i8* %arrayidx37, align 1, !tbaa !10
  store i8 %35, i8* %p2, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p3) #6
  %36 = load i32, i32* %r, align 4, !tbaa !8
  %sub38 = sub nsw i32 %36, 1
  %arrayidx39 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %sub38
  %37 = load i32, i32* %c, align 4, !tbaa !8
  %add40 = add nsw i32 %37, 2
  %arrayidx41 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx39, i32 0, i32 %add40
  %38 = load i8, i8* %arrayidx41, align 1, !tbaa !10
  store i8 %38, i8* %p3, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p4) #6
  %39 = load i32, i32* %r, align 4, !tbaa !8
  %sub42 = sub nsw i32 %39, 1
  %arrayidx43 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %sub42
  %40 = load i32, i32* %c, align 4, !tbaa !8
  %add44 = add nsw i32 %40, 3
  %arrayidx45 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx43, i32 0, i32 %add44
  %41 = load i8, i8* %arrayidx45, align 1, !tbaa !10
  store i8 %41, i8* %p4, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p5) #6
  %42 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx46 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %42
  %43 = load i32, i32* %c, align 4, !tbaa !8
  %sub47 = sub nsw i32 %43, 1
  %arrayidx48 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx46, i32 0, i32 %sub47
  %44 = load i8, i8* %arrayidx48, align 1, !tbaa !10
  store i8 %44, i8* %p5, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p6) #6
  %45 = load i32, i32* %r, align 4, !tbaa !8
  %add49 = add nsw i32 %45, 1
  %arrayidx50 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %add49
  %46 = load i32, i32* %c, align 4, !tbaa !8
  %sub51 = sub nsw i32 %46, 1
  %arrayidx52 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx50, i32 0, i32 %sub51
  %47 = load i8, i8* %arrayidx52, align 1, !tbaa !10
  store i8 %47, i8* %p6, align 1, !tbaa !10
  %48 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #6
  store i32 0, i32* %k, align 4, !tbaa !8
  br label %for.cond53

for.cond53:                                       ; preds = %for.inc211, %for.body27
  %49 = load i32, i32* %k, align 4, !tbaa !8
  %cmp54 = icmp slt i32 %49, 8
  br i1 %cmp54, label %for.body55, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond53
  %50 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  br label %for.end213

for.body55:                                       ; preds = %for.cond53
  %51 = bitcast i32* %r_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #6
  %52 = load i32, i32* %k, align 4, !tbaa !8
  %shr = ashr i32 %52, 2
  store i32 %shr, i32* %r_offset, align 4, !tbaa !8
  %53 = bitcast i32* %c_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #6
  %54 = load i32, i32* %k, align 4, !tbaa !8
  %and = and i32 %54, 3
  store i32 %and, i32* %c_offset, align 4, !tbaa !8
  %55 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx56 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %55
  %56 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx57 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx56, i32 0, i32 %56
  %arrayidx58 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx57, i32 0, i32 0
  %57 = load i8, i8* %arrayidx58, align 8, !tbaa !10
  %conv = sext i8 %57 to i32
  %58 = load i8, i8* %p0, align 1, !tbaa !10
  %conv59 = zext i8 %58 to i32
  %mul60 = mul nsw i32 %conv, %conv59
  %59 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx61 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %59
  %60 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx62 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx61, i32 0, i32 %60
  %arrayidx63 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx62, i32 0, i32 1
  %61 = load i8, i8* %arrayidx63, align 1, !tbaa !10
  %conv64 = sext i8 %61 to i32
  %62 = load i8, i8* %p1, align 1, !tbaa !10
  %conv65 = zext i8 %62 to i32
  %mul66 = mul nsw i32 %conv64, %conv65
  %add67 = add nsw i32 %mul60, %mul66
  %63 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx68 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %63
  %64 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx69 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx68, i32 0, i32 %64
  %arrayidx70 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx69, i32 0, i32 2
  %65 = load i8, i8* %arrayidx70, align 2, !tbaa !10
  %conv71 = sext i8 %65 to i32
  %66 = load i8, i8* %p2, align 1, !tbaa !10
  %conv72 = zext i8 %66 to i32
  %mul73 = mul nsw i32 %conv71, %conv72
  %add74 = add nsw i32 %add67, %mul73
  %67 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx75 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %67
  %68 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx76 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx75, i32 0, i32 %68
  %arrayidx77 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx76, i32 0, i32 3
  %69 = load i8, i8* %arrayidx77, align 1, !tbaa !10
  %conv78 = sext i8 %69 to i32
  %70 = load i8, i8* %p3, align 1, !tbaa !10
  %conv79 = zext i8 %70 to i32
  %mul80 = mul nsw i32 %conv78, %conv79
  %add81 = add nsw i32 %add74, %mul80
  %71 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx82 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %71
  %72 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx83 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx82, i32 0, i32 %72
  %arrayidx84 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx83, i32 0, i32 4
  %73 = load i8, i8* %arrayidx84, align 4, !tbaa !10
  %conv85 = sext i8 %73 to i32
  %74 = load i8, i8* %p4, align 1, !tbaa !10
  %conv86 = zext i8 %74 to i32
  %mul87 = mul nsw i32 %conv85, %conv86
  %add88 = add nsw i32 %add81, %mul87
  %75 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx89 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %75
  %76 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx90 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx89, i32 0, i32 %76
  %arrayidx91 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx90, i32 0, i32 5
  %77 = load i8, i8* %arrayidx91, align 1, !tbaa !10
  %conv92 = sext i8 %77 to i32
  %78 = load i8, i8* %p5, align 1, !tbaa !10
  %conv93 = zext i8 %78 to i32
  %mul94 = mul nsw i32 %conv92, %conv93
  %add95 = add nsw i32 %add88, %mul94
  %79 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx96 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %79
  %80 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx97 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx96, i32 0, i32 %80
  %arrayidx98 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx97, i32 0, i32 6
  %81 = load i8, i8* %arrayidx98, align 2, !tbaa !10
  %conv99 = sext i8 %81 to i32
  %82 = load i8, i8* %p6, align 1, !tbaa !10
  %conv100 = zext i8 %82 to i32
  %mul101 = mul nsw i32 %conv99, %conv100
  %add102 = add nsw i32 %add95, %mul101
  %cmp103 = icmp slt i32 %add102, 0
  br i1 %cmp103, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body55
  %83 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx105 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %83
  %84 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx106 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx105, i32 0, i32 %84
  %arrayidx107 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx106, i32 0, i32 0
  %85 = load i8, i8* %arrayidx107, align 8, !tbaa !10
  %conv108 = sext i8 %85 to i32
  %86 = load i8, i8* %p0, align 1, !tbaa !10
  %conv109 = zext i8 %86 to i32
  %mul110 = mul nsw i32 %conv108, %conv109
  %87 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx111 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %87
  %88 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx112 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx111, i32 0, i32 %88
  %arrayidx113 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx112, i32 0, i32 1
  %89 = load i8, i8* %arrayidx113, align 1, !tbaa !10
  %conv114 = sext i8 %89 to i32
  %90 = load i8, i8* %p1, align 1, !tbaa !10
  %conv115 = zext i8 %90 to i32
  %mul116 = mul nsw i32 %conv114, %conv115
  %add117 = add nsw i32 %mul110, %mul116
  %91 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx118 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %91
  %92 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx119 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx118, i32 0, i32 %92
  %arrayidx120 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx119, i32 0, i32 2
  %93 = load i8, i8* %arrayidx120, align 2, !tbaa !10
  %conv121 = sext i8 %93 to i32
  %94 = load i8, i8* %p2, align 1, !tbaa !10
  %conv122 = zext i8 %94 to i32
  %mul123 = mul nsw i32 %conv121, %conv122
  %add124 = add nsw i32 %add117, %mul123
  %95 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx125 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %95
  %96 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx126 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx125, i32 0, i32 %96
  %arrayidx127 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx126, i32 0, i32 3
  %97 = load i8, i8* %arrayidx127, align 1, !tbaa !10
  %conv128 = sext i8 %97 to i32
  %98 = load i8, i8* %p3, align 1, !tbaa !10
  %conv129 = zext i8 %98 to i32
  %mul130 = mul nsw i32 %conv128, %conv129
  %add131 = add nsw i32 %add124, %mul130
  %99 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx132 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %99
  %100 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx133 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx132, i32 0, i32 %100
  %arrayidx134 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx133, i32 0, i32 4
  %101 = load i8, i8* %arrayidx134, align 4, !tbaa !10
  %conv135 = sext i8 %101 to i32
  %102 = load i8, i8* %p4, align 1, !tbaa !10
  %conv136 = zext i8 %102 to i32
  %mul137 = mul nsw i32 %conv135, %conv136
  %add138 = add nsw i32 %add131, %mul137
  %103 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx139 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %103
  %104 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx140 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx139, i32 0, i32 %104
  %arrayidx141 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx140, i32 0, i32 5
  %105 = load i8, i8* %arrayidx141, align 1, !tbaa !10
  %conv142 = sext i8 %105 to i32
  %106 = load i8, i8* %p5, align 1, !tbaa !10
  %conv143 = zext i8 %106 to i32
  %mul144 = mul nsw i32 %conv142, %conv143
  %add145 = add nsw i32 %add138, %mul144
  %107 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx146 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %107
  %108 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx147 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx146, i32 0, i32 %108
  %arrayidx148 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx147, i32 0, i32 6
  %109 = load i8, i8* %arrayidx148, align 2, !tbaa !10
  %conv149 = sext i8 %109 to i32
  %110 = load i8, i8* %p6, align 1, !tbaa !10
  %conv150 = zext i8 %110 to i32
  %mul151 = mul nsw i32 %conv149, %conv150
  %add152 = add nsw i32 %add145, %mul151
  %sub153 = sub nsw i32 0, %add152
  %add154 = add nsw i32 %sub153, 8
  %shr155 = ashr i32 %add154, 4
  %sub156 = sub nsw i32 0, %shr155
  br label %cond.end

cond.false:                                       ; preds = %for.body55
  %111 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx157 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %111
  %112 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx158 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx157, i32 0, i32 %112
  %arrayidx159 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx158, i32 0, i32 0
  %113 = load i8, i8* %arrayidx159, align 8, !tbaa !10
  %conv160 = sext i8 %113 to i32
  %114 = load i8, i8* %p0, align 1, !tbaa !10
  %conv161 = zext i8 %114 to i32
  %mul162 = mul nsw i32 %conv160, %conv161
  %115 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx163 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %115
  %116 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx164 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx163, i32 0, i32 %116
  %arrayidx165 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx164, i32 0, i32 1
  %117 = load i8, i8* %arrayidx165, align 1, !tbaa !10
  %conv166 = sext i8 %117 to i32
  %118 = load i8, i8* %p1, align 1, !tbaa !10
  %conv167 = zext i8 %118 to i32
  %mul168 = mul nsw i32 %conv166, %conv167
  %add169 = add nsw i32 %mul162, %mul168
  %119 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx170 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %119
  %120 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx171 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx170, i32 0, i32 %120
  %arrayidx172 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx171, i32 0, i32 2
  %121 = load i8, i8* %arrayidx172, align 2, !tbaa !10
  %conv173 = sext i8 %121 to i32
  %122 = load i8, i8* %p2, align 1, !tbaa !10
  %conv174 = zext i8 %122 to i32
  %mul175 = mul nsw i32 %conv173, %conv174
  %add176 = add nsw i32 %add169, %mul175
  %123 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx177 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %123
  %124 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx178 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx177, i32 0, i32 %124
  %arrayidx179 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx178, i32 0, i32 3
  %125 = load i8, i8* %arrayidx179, align 1, !tbaa !10
  %conv180 = sext i8 %125 to i32
  %126 = load i8, i8* %p3, align 1, !tbaa !10
  %conv181 = zext i8 %126 to i32
  %mul182 = mul nsw i32 %conv180, %conv181
  %add183 = add nsw i32 %add176, %mul182
  %127 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx184 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %127
  %128 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx185 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx184, i32 0, i32 %128
  %arrayidx186 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx185, i32 0, i32 4
  %129 = load i8, i8* %arrayidx186, align 4, !tbaa !10
  %conv187 = sext i8 %129 to i32
  %130 = load i8, i8* %p4, align 1, !tbaa !10
  %conv188 = zext i8 %130 to i32
  %mul189 = mul nsw i32 %conv187, %conv188
  %add190 = add nsw i32 %add183, %mul189
  %131 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx191 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %131
  %132 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx192 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx191, i32 0, i32 %132
  %arrayidx193 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx192, i32 0, i32 5
  %133 = load i8, i8* %arrayidx193, align 1, !tbaa !10
  %conv194 = sext i8 %133 to i32
  %134 = load i8, i8* %p5, align 1, !tbaa !10
  %conv195 = zext i8 %134 to i32
  %mul196 = mul nsw i32 %conv194, %conv195
  %add197 = add nsw i32 %add190, %mul196
  %135 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx198 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %135
  %136 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx199 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx198, i32 0, i32 %136
  %arrayidx200 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx199, i32 0, i32 6
  %137 = load i8, i8* %arrayidx200, align 2, !tbaa !10
  %conv201 = sext i8 %137 to i32
  %138 = load i8, i8* %p6, align 1, !tbaa !10
  %conv202 = zext i8 %138 to i32
  %mul203 = mul nsw i32 %conv201, %conv202
  %add204 = add nsw i32 %add197, %mul203
  %add205 = add nsw i32 %add204, 8
  %shr206 = ashr i32 %add205, 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub156, %cond.true ], [ %shr206, %cond.false ]
  %call = call zeroext i8 @clip_pixel(i32 %cond)
  %139 = load i32, i32* %r, align 4, !tbaa !8
  %140 = load i32, i32* %r_offset, align 4, !tbaa !8
  %add207 = add nsw i32 %139, %140
  %arrayidx208 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %add207
  %141 = load i32, i32* %c, align 4, !tbaa !8
  %142 = load i32, i32* %c_offset, align 4, !tbaa !8
  %add209 = add nsw i32 %141, %142
  %arrayidx210 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx208, i32 0, i32 %add209
  store i8 %call, i8* %arrayidx210, align 1, !tbaa !10
  %143 = bitcast i32* %c_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #6
  %144 = bitcast i32* %r_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #6
  br label %for.inc211

for.inc211:                                       ; preds = %cond.end
  %145 = load i32, i32* %k, align 4, !tbaa !8
  %inc212 = add nsw i32 %145, 1
  store i32 %inc212, i32* %k, align 4, !tbaa !8
  br label %for.cond53

for.end213:                                       ; preds = %for.cond.cleanup
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p6) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p5) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p4) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p3) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #6
  br label %for.inc214

for.inc214:                                       ; preds = %for.end213
  %146 = load i32, i32* %c, align 4, !tbaa !8
  %add215 = add nsw i32 %146, 4
  store i32 %add215, i32* %c, align 4, !tbaa !8
  br label %for.cond24

for.end216:                                       ; preds = %for.cond24
  br label %for.inc217

for.inc217:                                       ; preds = %for.end216
  %147 = load i32, i32* %r, align 4, !tbaa !8
  %add218 = add nsw i32 %147, 2
  store i32 %add218, i32* %r, align 4, !tbaa !8
  br label %for.cond20

for.end219:                                       ; preds = %for.cond20
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond220

for.cond220:                                      ; preds = %for.inc228, %for.end219
  %148 = load i32, i32* %r, align 4, !tbaa !8
  %149 = load i32, i32* %bh, align 4, !tbaa !8
  %cmp221 = icmp slt i32 %148, %149
  br i1 %cmp221, label %for.body223, label %for.end230

for.body223:                                      ; preds = %for.cond220
  %150 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %151 = load i32, i32* %r, align 4, !tbaa !8
  %add224 = add nsw i32 %151, 1
  %arrayidx225 = getelementptr inbounds [33 x [33 x i8]], [33 x [33 x i8]]* %buffer, i32 0, i32 %add224
  %arrayidx226 = getelementptr inbounds [33 x i8], [33 x i8]* %arrayidx225, i32 0, i32 1
  %152 = load i32, i32* %bw, align 4, !tbaa !8
  %mul227 = mul i32 %152, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %150, i8* align 1 %arrayidx226, i32 %mul227, i1 false)
  %153 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %154 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %154, i32 %153
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc228

for.inc228:                                       ; preds = %for.body223
  %155 = load i32, i32* %r, align 4, !tbaa !8
  %inc229 = add nsw i32 %155, 1
  store i32 %inc229, i32* %r, align 4, !tbaa !8
  br label %for.cond220

for.end230:                                       ; preds = %for.cond220
  %156 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #6
  %157 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #6
  %158 = bitcast [33 x [33 x i8]]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 1089, i8* %158) #6
  %159 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #6
  %160 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @clip_pixel(i32 %val) #4 {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !8
  %0 = load i32, i32* %val.addr, align 4, !tbaa !8
  %cmp = icmp sgt i32 %0, 255
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4, !tbaa !8
  %cmp1 = icmp slt i32 %1, 0
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %2 = load i32, i32* %val.addr, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ 0, %cond.true2 ], [ %2, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ 255, %cond.true ], [ %cond, %cond.end ]
  %conv = trunc i32 %cond5 to i8
  ret i8 %conv
}

; Function Attrs: nounwind
define hidden void @av1_filter_intra_edge_c(i8* %p, i32 %sz, i32 %strength) #0 {
entry:
  %p.addr = alloca i8*, align 4
  %sz.addr = alloca i32, align 4
  %strength.addr = alloca i32, align 4
  %kernel = alloca [3 x [5 x i32]], align 16
  %filt = alloca i32, align 4
  %edge = alloca [129 x i8], align 16
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %s = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  store i8* %p, i8** %p.addr, align 4, !tbaa !2
  store i32 %sz, i32* %sz.addr, align 4, !tbaa !8
  store i32 %strength, i32* %strength.addr, align 4, !tbaa !8
  %0 = load i32, i32* %strength.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast [3 x [5 x i32]]* %kernel to i8*
  call void @llvm.lifetime.start.p0i8(i64 60, i8* %1) #6
  %2 = bitcast [3 x [5 x i32]]* %kernel to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %2, i8* align 16 bitcast ([3 x [5 x i32]]* @__const.av1_filter_intra_edge_c.kernel to i8*), i32 60, i1 false)
  %3 = bitcast i32* %filt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %strength.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %4, 1
  store i32 %sub, i32* %filt, align 4, !tbaa !8
  %5 = bitcast [129 x i8]* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 129, i8* %5) #6
  %arraydecay = getelementptr inbounds [129 x i8], [129 x i8]* %edge, i32 0, i32 0
  %6 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %7 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %mul = mul i32 %7, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay, i8* align 1 %6, i32 %mul, i1 false)
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store i32 1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %10 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  br label %for.end23

for.body:                                         ; preds = %for.cond
  %12 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  store i32 0, i32* %s, align 4, !tbaa !8
  %13 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %j, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %14 = load i32, i32* %j, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %14, 5
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %15 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %16 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %i, align 4, !tbaa !8
  %sub5 = sub nsw i32 %17, 2
  %18 = load i32, i32* %j, align 4, !tbaa !8
  %add = add nsw i32 %sub5, %18
  store i32 %add, i32* %k, align 4, !tbaa !8
  %19 = load i32, i32* %k, align 4, !tbaa !8
  %cmp6 = icmp slt i32 %19, 0
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body4
  br label %cond.end

cond.false:                                       ; preds = %for.body4
  %20 = load i32, i32* %k, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %20, %cond.false ]
  store i32 %cond, i32* %k, align 4, !tbaa !8
  %21 = load i32, i32* %k, align 4, !tbaa !8
  %22 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %sub7 = sub nsw i32 %22, 1
  %cmp8 = icmp sgt i32 %21, %sub7
  br i1 %cmp8, label %cond.true9, label %cond.false11

cond.true9:                                       ; preds = %cond.end
  %23 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %sub10 = sub nsw i32 %23, 1
  br label %cond.end12

cond.false11:                                     ; preds = %cond.end
  %24 = load i32, i32* %k, align 4, !tbaa !8
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false11, %cond.true9
  %cond13 = phi i32 [ %sub10, %cond.true9 ], [ %24, %cond.false11 ]
  store i32 %cond13, i32* %k, align 4, !tbaa !8
  %25 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [129 x i8], [129 x i8]* %edge, i32 0, i32 %25
  %26 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %26 to i32
  %27 = load i32, i32* %filt, align 4, !tbaa !8
  %arrayidx14 = getelementptr inbounds [3 x [5 x i32]], [3 x [5 x i32]]* %kernel, i32 0, i32 %27
  %28 = load i32, i32* %j, align 4, !tbaa !8
  %arrayidx15 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx14, i32 0, i32 %28
  %29 = load i32, i32* %arrayidx15, align 4, !tbaa !8
  %mul16 = mul nsw i32 %conv, %29
  %30 = load i32, i32* %s, align 4, !tbaa !8
  %add17 = add nsw i32 %30, %mul16
  store i32 %add17, i32* %s, align 4, !tbaa !8
  %31 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  br label %for.inc

for.inc:                                          ; preds = %cond.end12
  %32 = load i32, i32* %j, align 4, !tbaa !8
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %j, align 4, !tbaa !8
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %33 = load i32, i32* %s, align 4, !tbaa !8
  %add18 = add nsw i32 %33, 8
  %shr = ashr i32 %add18, 4
  store i32 %shr, i32* %s, align 4, !tbaa !8
  %34 = load i32, i32* %s, align 4, !tbaa !8
  %conv19 = trunc i32 %34 to i8
  %35 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %36 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx20 = getelementptr inbounds i8, i8* %35, i32 %36
  store i8 %conv19, i8* %arrayidx20, align 1, !tbaa !10
  %37 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  br label %for.inc21

for.inc21:                                        ; preds = %for.end
  %38 = load i32, i32* %i, align 4, !tbaa !8
  %inc22 = add nsw i32 %38, 1
  store i32 %inc22, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end23:                                        ; preds = %for.cond.cleanup
  %39 = bitcast [129 x i8]* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 129, i8* %39) #6
  %40 = bitcast i32* %filt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #6
  %41 = bitcast [3 x [5 x i32]]* %kernel to i8*
  call void @llvm.lifetime.end.p0i8(i64 60, i8* %41) #6
  br label %return

return:                                           ; preds = %for.end23, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_filter_intra_edge_high_c(i16* %p, i32 %sz, i32 %strength) #0 {
entry:
  %p.addr = alloca i16*, align 4
  %sz.addr = alloca i32, align 4
  %strength.addr = alloca i32, align 4
  %kernel = alloca [3 x [5 x i32]], align 16
  %filt = alloca i32, align 4
  %edge = alloca [129 x i16], align 16
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %s = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  store i16* %p, i16** %p.addr, align 4, !tbaa !2
  store i32 %sz, i32* %sz.addr, align 4, !tbaa !8
  store i32 %strength, i32* %strength.addr, align 4, !tbaa !8
  %0 = load i32, i32* %strength.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast [3 x [5 x i32]]* %kernel to i8*
  call void @llvm.lifetime.start.p0i8(i64 60, i8* %1) #6
  %2 = bitcast [3 x [5 x i32]]* %kernel to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %2, i8* align 16 bitcast ([3 x [5 x i32]]* @__const.av1_filter_intra_edge_high_c.kernel to i8*), i32 60, i1 false)
  %3 = bitcast i32* %filt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %strength.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %4, 1
  store i32 %sub, i32* %filt, align 4, !tbaa !8
  %5 = bitcast [129 x i16]* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 258, i8* %5) #6
  %arraydecay = getelementptr inbounds [129 x i16], [129 x i16]* %edge, i32 0, i32 0
  %6 = bitcast i16* %arraydecay to i8*
  %7 = load i16*, i16** %p.addr, align 4, !tbaa !2
  %8 = bitcast i16* %7 to i8*
  %9 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %mul = mul i32 %9, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %6, i8* align 2 %8, i32 %mul, i1 false)
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store i32 1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %if.end
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %12 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  br label %for.end23

for.body:                                         ; preds = %for.cond
  %14 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  store i32 0, i32* %s, align 4, !tbaa !8
  %15 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  store i32 0, i32* %j, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %16 = load i32, i32* %j, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %16, 5
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %18 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load i32, i32* %i, align 4, !tbaa !8
  %sub5 = sub nsw i32 %19, 2
  %20 = load i32, i32* %j, align 4, !tbaa !8
  %add = add nsw i32 %sub5, %20
  store i32 %add, i32* %k, align 4, !tbaa !8
  %21 = load i32, i32* %k, align 4, !tbaa !8
  %cmp6 = icmp slt i32 %21, 0
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body4
  br label %cond.end

cond.false:                                       ; preds = %for.body4
  %22 = load i32, i32* %k, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %22, %cond.false ]
  store i32 %cond, i32* %k, align 4, !tbaa !8
  %23 = load i32, i32* %k, align 4, !tbaa !8
  %24 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %sub7 = sub nsw i32 %24, 1
  %cmp8 = icmp sgt i32 %23, %sub7
  br i1 %cmp8, label %cond.true9, label %cond.false11

cond.true9:                                       ; preds = %cond.end
  %25 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %sub10 = sub nsw i32 %25, 1
  br label %cond.end12

cond.false11:                                     ; preds = %cond.end
  %26 = load i32, i32* %k, align 4, !tbaa !8
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false11, %cond.true9
  %cond13 = phi i32 [ %sub10, %cond.true9 ], [ %26, %cond.false11 ]
  store i32 %cond13, i32* %k, align 4, !tbaa !8
  %27 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [129 x i16], [129 x i16]* %edge, i32 0, i32 %27
  %28 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %28 to i32
  %29 = load i32, i32* %filt, align 4, !tbaa !8
  %arrayidx14 = getelementptr inbounds [3 x [5 x i32]], [3 x [5 x i32]]* %kernel, i32 0, i32 %29
  %30 = load i32, i32* %j, align 4, !tbaa !8
  %arrayidx15 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx14, i32 0, i32 %30
  %31 = load i32, i32* %arrayidx15, align 4, !tbaa !8
  %mul16 = mul nsw i32 %conv, %31
  %32 = load i32, i32* %s, align 4, !tbaa !8
  %add17 = add nsw i32 %32, %mul16
  store i32 %add17, i32* %s, align 4, !tbaa !8
  %33 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  br label %for.inc

for.inc:                                          ; preds = %cond.end12
  %34 = load i32, i32* %j, align 4, !tbaa !8
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %j, align 4, !tbaa !8
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %35 = load i32, i32* %s, align 4, !tbaa !8
  %add18 = add nsw i32 %35, 8
  %shr = ashr i32 %add18, 4
  store i32 %shr, i32* %s, align 4, !tbaa !8
  %36 = load i32, i32* %s, align 4, !tbaa !8
  %conv19 = trunc i32 %36 to i16
  %37 = load i16*, i16** %p.addr, align 4, !tbaa !2
  %38 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx20 = getelementptr inbounds i16, i16* %37, i32 %38
  store i16 %conv19, i16* %arrayidx20, align 2, !tbaa !11
  %39 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  br label %for.inc21

for.inc21:                                        ; preds = %for.end
  %40 = load i32, i32* %i, align 4, !tbaa !8
  %inc22 = add nsw i32 %40, 1
  store i32 %inc22, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end23:                                        ; preds = %for.cond.cleanup
  %41 = bitcast [129 x i16]* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 258, i8* %41) #6
  %42 = bitcast i32* %filt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  %43 = bitcast [3 x [5 x i32]]* %kernel to i8*
  call void @llvm.lifetime.end.p0i8(i64 60, i8* %43) #6
  br label %return

return:                                           ; preds = %for.end23, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_upsample_intra_edge_c(i8* %p, i32 %sz) #0 {
entry:
  %p.addr = alloca i8*, align 4
  %sz.addr = alloca i32, align 4
  %in = alloca [19 x i8], align 16
  %i = alloca i32, align 4
  %i11 = alloca i32, align 4
  %s = alloca i32, align 4
  store i8* %p, i8** %p.addr, align 4, !tbaa !2
  store i32 %sz, i32* %sz.addr, align 4, !tbaa !8
  %0 = bitcast [19 x i8]* %in to i8*
  call void @llvm.lifetime.start.p0i8(i64 19, i8* %0) #6
  %1 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 -1
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %arrayidx1 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 0
  store i8 %2, i8* %arrayidx1, align 16, !tbaa !10
  %3 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %3, i32 -1
  %4 = load i8, i8* %arrayidx2, align 1, !tbaa !10
  %arrayidx3 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 1
  store i8 %4, i8* %arrayidx3, align 1, !tbaa !10
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %7 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx4, align 1, !tbaa !10
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %add = add nsw i32 %12, 2
  %arrayidx5 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 %add
  store i8 %11, i8* %arrayidx5, align 1, !tbaa !10
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %15 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %15, 1
  %arrayidx6 = getelementptr inbounds i8, i8* %14, i32 %sub
  %16 = load i8, i8* %arrayidx6, align 1, !tbaa !10
  %17 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %add7 = add nsw i32 %17, 2
  %arrayidx8 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 %add7
  store i8 %16, i8* %arrayidx8, align 1, !tbaa !10
  %arrayidx9 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 0
  %18 = load i8, i8* %arrayidx9, align 16, !tbaa !10
  %19 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %19, i32 -2
  store i8 %18, i8* %arrayidx10, align 1, !tbaa !10
  %20 = bitcast i32* %i11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  store i32 0, i32* %i11, align 4, !tbaa !8
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc41, %for.end
  %21 = load i32, i32* %i11, align 4, !tbaa !8
  %22 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %cmp13 = icmp slt i32 %21, %22
  br i1 %cmp13, label %for.body15, label %for.cond.cleanup14

for.cond.cleanup14:                               ; preds = %for.cond12
  %23 = bitcast i32* %i11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  br label %for.end43

for.body15:                                       ; preds = %for.cond12
  %24 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load i32, i32* %i11, align 4, !tbaa !8
  %arrayidx16 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 %25
  %26 = load i8, i8* %arrayidx16, align 1, !tbaa !10
  %conv = zext i8 %26 to i32
  %sub17 = sub nsw i32 0, %conv
  %27 = load i32, i32* %i11, align 4, !tbaa !8
  %add18 = add nsw i32 %27, 1
  %arrayidx19 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 %add18
  %28 = load i8, i8* %arrayidx19, align 1, !tbaa !10
  %conv20 = zext i8 %28 to i32
  %mul = mul nsw i32 9, %conv20
  %add21 = add nsw i32 %sub17, %mul
  %29 = load i32, i32* %i11, align 4, !tbaa !8
  %add22 = add nsw i32 %29, 2
  %arrayidx23 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 %add22
  %30 = load i8, i8* %arrayidx23, align 1, !tbaa !10
  %conv24 = zext i8 %30 to i32
  %mul25 = mul nsw i32 9, %conv24
  %add26 = add nsw i32 %add21, %mul25
  %31 = load i32, i32* %i11, align 4, !tbaa !8
  %add27 = add nsw i32 %31, 3
  %arrayidx28 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 %add27
  %32 = load i8, i8* %arrayidx28, align 1, !tbaa !10
  %conv29 = zext i8 %32 to i32
  %sub30 = sub nsw i32 %add26, %conv29
  store i32 %sub30, i32* %s, align 4, !tbaa !8
  %33 = load i32, i32* %s, align 4, !tbaa !8
  %add31 = add nsw i32 %33, 8
  %shr = ashr i32 %add31, 4
  %call = call zeroext i8 @clip_pixel(i32 %shr)
  %conv32 = zext i8 %call to i32
  store i32 %conv32, i32* %s, align 4, !tbaa !8
  %34 = load i32, i32* %s, align 4, !tbaa !8
  %conv33 = trunc i32 %34 to i8
  %35 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %36 = load i32, i32* %i11, align 4, !tbaa !8
  %mul34 = mul nsw i32 2, %36
  %sub35 = sub nsw i32 %mul34, 1
  %arrayidx36 = getelementptr inbounds i8, i8* %35, i32 %sub35
  store i8 %conv33, i8* %arrayidx36, align 1, !tbaa !10
  %37 = load i32, i32* %i11, align 4, !tbaa !8
  %add37 = add nsw i32 %37, 2
  %arrayidx38 = getelementptr inbounds [19 x i8], [19 x i8]* %in, i32 0, i32 %add37
  %38 = load i8, i8* %arrayidx38, align 1, !tbaa !10
  %39 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %40 = load i32, i32* %i11, align 4, !tbaa !8
  %mul39 = mul nsw i32 2, %40
  %arrayidx40 = getelementptr inbounds i8, i8* %39, i32 %mul39
  store i8 %38, i8* %arrayidx40, align 1, !tbaa !10
  %41 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #6
  br label %for.inc41

for.inc41:                                        ; preds = %for.body15
  %42 = load i32, i32* %i11, align 4, !tbaa !8
  %inc42 = add nsw i32 %42, 1
  store i32 %inc42, i32* %i11, align 4, !tbaa !8
  br label %for.cond12

for.end43:                                        ; preds = %for.cond.cleanup14
  %43 = bitcast [19 x i8]* %in to i8*
  call void @llvm.lifetime.end.p0i8(i64 19, i8* %43) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_upsample_intra_edge_high_c(i16* %p, i32 %sz, i32 %bd) #0 {
entry:
  %p.addr = alloca i16*, align 4
  %sz.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %in = alloca [19 x i16], align 16
  %i = alloca i32, align 4
  %i11 = alloca i32, align 4
  %s = alloca i32, align 4
  store i16* %p, i16** %p.addr, align 4, !tbaa !2
  store i32 %sz, i32* %sz.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast [19 x i16]* %in to i8*
  call void @llvm.lifetime.start.p0i8(i64 38, i8* %0) #6
  %1 = load i16*, i16** %p.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 -1
  %2 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %arrayidx1 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 0
  store i16 %2, i16* %arrayidx1, align 16, !tbaa !11
  %3 = load i16*, i16** %p.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %3, i32 -1
  %4 = load i16, i16* %arrayidx2, align 2, !tbaa !11
  %arrayidx3 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 1
  store i16 %4, i16* %arrayidx3, align 2, !tbaa !11
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %7 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i16*, i16** %p.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds i16, i16* %9, i32 %10
  %11 = load i16, i16* %arrayidx4, align 2, !tbaa !11
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %add = add nsw i32 %12, 2
  %arrayidx5 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 %add
  store i16 %11, i16* %arrayidx5, align 2, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = load i16*, i16** %p.addr, align 4, !tbaa !2
  %15 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %15, 1
  %arrayidx6 = getelementptr inbounds i16, i16* %14, i32 %sub
  %16 = load i16, i16* %arrayidx6, align 2, !tbaa !11
  %17 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %add7 = add nsw i32 %17, 2
  %arrayidx8 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 %add7
  store i16 %16, i16* %arrayidx8, align 2, !tbaa !11
  %arrayidx9 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 0
  %18 = load i16, i16* %arrayidx9, align 16, !tbaa !11
  %19 = load i16*, i16** %p.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %19, i32 -2
  store i16 %18, i16* %arrayidx10, align 2, !tbaa !11
  %20 = bitcast i32* %i11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  store i32 0, i32* %i11, align 4, !tbaa !8
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc41, %for.end
  %21 = load i32, i32* %i11, align 4, !tbaa !8
  %22 = load i32, i32* %sz.addr, align 4, !tbaa !8
  %cmp13 = icmp slt i32 %21, %22
  br i1 %cmp13, label %for.body15, label %for.cond.cleanup14

for.cond.cleanup14:                               ; preds = %for.cond12
  %23 = bitcast i32* %i11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  br label %for.end43

for.body15:                                       ; preds = %for.cond12
  %24 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load i32, i32* %i11, align 4, !tbaa !8
  %arrayidx16 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 %25
  %26 = load i16, i16* %arrayidx16, align 2, !tbaa !11
  %conv = zext i16 %26 to i32
  %sub17 = sub nsw i32 0, %conv
  %27 = load i32, i32* %i11, align 4, !tbaa !8
  %add18 = add nsw i32 %27, 1
  %arrayidx19 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 %add18
  %28 = load i16, i16* %arrayidx19, align 2, !tbaa !11
  %conv20 = zext i16 %28 to i32
  %mul = mul nsw i32 9, %conv20
  %add21 = add nsw i32 %sub17, %mul
  %29 = load i32, i32* %i11, align 4, !tbaa !8
  %add22 = add nsw i32 %29, 2
  %arrayidx23 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 %add22
  %30 = load i16, i16* %arrayidx23, align 2, !tbaa !11
  %conv24 = zext i16 %30 to i32
  %mul25 = mul nsw i32 9, %conv24
  %add26 = add nsw i32 %add21, %mul25
  %31 = load i32, i32* %i11, align 4, !tbaa !8
  %add27 = add nsw i32 %31, 3
  %arrayidx28 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 %add27
  %32 = load i16, i16* %arrayidx28, align 2, !tbaa !11
  %conv29 = zext i16 %32 to i32
  %sub30 = sub nsw i32 %add26, %conv29
  store i32 %sub30, i32* %s, align 4, !tbaa !8
  %33 = load i32, i32* %s, align 4, !tbaa !8
  %add31 = add nsw i32 %33, 8
  %shr = ashr i32 %add31, 4
  store i32 %shr, i32* %s, align 4, !tbaa !8
  %34 = load i32, i32* %s, align 4, !tbaa !8
  %35 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %call = call zeroext i16 @clip_pixel_highbd(i32 %34, i32 %35)
  %conv32 = zext i16 %call to i32
  store i32 %conv32, i32* %s, align 4, !tbaa !8
  %36 = load i32, i32* %s, align 4, !tbaa !8
  %conv33 = trunc i32 %36 to i16
  %37 = load i16*, i16** %p.addr, align 4, !tbaa !2
  %38 = load i32, i32* %i11, align 4, !tbaa !8
  %mul34 = mul nsw i32 2, %38
  %sub35 = sub nsw i32 %mul34, 1
  %arrayidx36 = getelementptr inbounds i16, i16* %37, i32 %sub35
  store i16 %conv33, i16* %arrayidx36, align 2, !tbaa !11
  %39 = load i32, i32* %i11, align 4, !tbaa !8
  %add37 = add nsw i32 %39, 2
  %arrayidx38 = getelementptr inbounds [19 x i16], [19 x i16]* %in, i32 0, i32 %add37
  %40 = load i16, i16* %arrayidx38, align 2, !tbaa !11
  %41 = load i16*, i16** %p.addr, align 4, !tbaa !2
  %42 = load i32, i32* %i11, align 4, !tbaa !8
  %mul39 = mul nsw i32 2, %42
  %arrayidx40 = getelementptr inbounds i16, i16* %41, i32 %mul39
  store i16 %40, i16* %arrayidx40, align 2, !tbaa !11
  %43 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  br label %for.inc41

for.inc41:                                        ; preds = %for.body15
  %44 = load i32, i32* %i11, align 4, !tbaa !8
  %inc42 = add nsw i32 %44, 1
  store i32 %inc42, i32* %i11, align 4, !tbaa !8
  br label %for.cond12

for.end43:                                        ; preds = %for.cond.cleanup14
  %45 = bitcast [19 x i16]* %in to i8*
  call void @llvm.lifetime.end.p0i8(i64 38, i8* %45) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @clip_pixel_highbd(i32 %val, i32 %bd) #4 {
entry:
  %retval = alloca i16, align 2
  %val.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !8
  switch i32 %0, label %sw.default [
    i32 8, label %sw.bb
    i32 10, label %sw.bb1
    i32 12, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb
  %1 = load i32, i32* %val.addr, align 4, !tbaa !8
  %call = call i32 @clamp(i32 %1, i32 0, i32 255)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %retval, align 2
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !8
  %call2 = call i32 @clamp(i32 %2, i32 0, i32 1023)
  %conv3 = trunc i32 %call2 to i16
  store i16 %conv3, i16* %retval, align 2
  br label %return

sw.bb4:                                           ; preds = %entry
  %3 = load i32, i32* %val.addr, align 4, !tbaa !8
  %call5 = call i32 @clamp(i32 %3, i32 0, i32 4095)
  %conv6 = trunc i32 %call5 to i16
  store i16 %conv6, i16* %retval, align 2
  br label %return

return:                                           ; preds = %sw.bb4, %sw.bb1, %sw.default
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: nounwind
define hidden void @av1_predict_intra_block(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %wpx, i32 %hpx, i8 zeroext %tx_size, i8 zeroext %mode, i32 %angle_delta, i32 %use_palette, i8 zeroext %filter_intra_mode, i8* %ref, i32 %ref_stride, i8* %dst, i32 %dst_stride, i32 %col_off, i32 %row_off, i32 %plane) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %wpx.addr = alloca i32, align 4
  %hpx.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %mode.addr = alloca i8, align 1
  %angle_delta.addr = alloca i32, align 4
  %use_palette.addr = alloca i32, align 4
  %filter_intra_mode.addr = alloca i8, align 1
  %ref.addr = alloca i8*, align 4
  %ref_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %col_off.addr = alloca i32, align 4
  %row_off.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %txwpx = alloca i32, align 4
  %txhpx = alloca i32, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %map = alloca i8*, align 4
  %palette = alloca i16*, align 4
  %dst16 = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %txw = alloca i32, align 4
  %txh = alloca i32, align 4
  %ss_x = alloca i32, align 4
  %ss_y = alloca i32, align 4
  %have_top = alloca i32, align 4
  %have_left = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %xr_chr_offset = alloca i32, align 4
  %yd_chr_offset = alloca i32, align 4
  %xr = alloca i32, align 4
  %yd = alloca i32, align 4
  %right_available = alloca i32, align 4
  %bottom_available = alloca i32, align 4
  %partition = alloca i8, align 1
  %bsize = alloca i8, align 1
  %have_top_right = alloca i32, align 4
  %have_bottom_left = alloca i32, align 4
  %disable_edge_filter = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %wpx, i32* %wpx.addr, align 4, !tbaa !8
  store i32 %hpx, i32* %hpx.addr, align 4, !tbaa !8
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !10
  store i32 %angle_delta, i32* %angle_delta.addr, align 4, !tbaa !8
  store i32 %use_palette, i32* %use_palette.addr, align 4, !tbaa !8
  store i8 %filter_intra_mode, i8* %filter_intra_mode.addr, align 1, !tbaa !10
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %ref_stride, i32* %ref_stride.addr, align 4, !tbaa !8
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !8
  store i32 %col_off, i32* %col_off.addr, align 4, !tbaa !8
  store i32 %row_off, i32* %row_off.addr, align 4, !tbaa !8
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !8
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %4 = bitcast i32* %txwpx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %5 to i32
  %arrayidx1 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %6 = load i32, i32* %arrayidx1, align 4, !tbaa !8
  store i32 %6, i32* %txwpx, align 4, !tbaa !8
  %7 = bitcast i32* %txhpx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom2 = zext i8 %8 to i32
  %arrayidx3 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom2
  %9 = load i32, i32* %arrayidx3, align 4, !tbaa !8
  store i32 %9, i32* %txhpx, align 4, !tbaa !8
  %10 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %shl = shl i32 %11, 2
  store i32 %shl, i32* %x, align 4, !tbaa !8
  %12 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %shl4 = shl i32 %13, 2
  store i32 %shl4, i32* %y, align 4, !tbaa !8
  %14 = load i32, i32* %use_palette.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.then, label %if.end58

if.then:                                          ; preds = %entry
  %15 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = bitcast i8** %map to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane5 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %18, i32 0, i32 4
  %19 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %cmp = icmp ne i32 %19, 0
  %conv = zext i1 %cmp to i32
  %arrayidx6 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane5, i32 0, i32 %conv
  %color_index_map = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx6, i32 0, i32 11
  %20 = load i8*, i8** %color_index_map, align 4, !tbaa !19
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %color_index_map_offset = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %21, i32 0, i32 60
  %22 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %cmp7 = icmp ne i32 %22, 0
  %conv8 = zext i1 %cmp7 to i32
  %arrayidx9 = getelementptr inbounds [2 x i16], [2 x i16]* %color_index_map_offset, i32 0, i32 %conv8
  %23 = load i16, i16* %arrayidx9, align 2, !tbaa !11
  %conv10 = zext i16 %23 to i32
  %add.ptr = getelementptr inbounds i8, i8* %20, i32 %conv10
  store i8* %add.ptr, i8** %map, align 4, !tbaa !2
  %24 = bitcast i16** %palette to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %25, i32 0, i32 5
  %palette_colors = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 0
  %arraydecay = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors, i32 0, i32 0
  %26 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %26, 8
  %add.ptr11 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul
  store i16* %add.ptr11, i16** %palette, align 4, !tbaa !2
  %27 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @is_cur_buf_hbd(%struct.macroblockd* %27)
  %tobool12 = icmp ne i32 %call, 0
  br i1 %tobool12, label %if.then13, label %if.else

if.then13:                                        ; preds = %if.then
  %28 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %29 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %30 = ptrtoint i8* %29 to i32
  %shl14 = shl i32 %30, 1
  %31 = inttoptr i32 %shl14 to i16*
  store i16* %31, i16** %dst16, align 4, !tbaa !2
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc30, %if.then13
  %32 = load i32, i32* %r, align 4, !tbaa !8
  %33 = load i32, i32* %txhpx, align 4, !tbaa !8
  %cmp15 = icmp slt i32 %32, %33
  br i1 %cmp15, label %for.body, label %for.end32

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc, %for.body
  %34 = load i32, i32* %c, align 4, !tbaa !8
  %35 = load i32, i32* %txwpx, align 4, !tbaa !8
  %cmp18 = icmp slt i32 %34, %35
  br i1 %cmp18, label %for.body20, label %for.end

for.body20:                                       ; preds = %for.cond17
  %36 = load i16*, i16** %palette, align 4, !tbaa !2
  %37 = load i8*, i8** %map, align 4, !tbaa !2
  %38 = load i32, i32* %r, align 4, !tbaa !8
  %39 = load i32, i32* %y, align 4, !tbaa !8
  %add = add nsw i32 %38, %39
  %40 = load i32, i32* %wpx.addr, align 4, !tbaa !8
  %mul21 = mul nsw i32 %add, %40
  %41 = load i32, i32* %c, align 4, !tbaa !8
  %add22 = add nsw i32 %mul21, %41
  %42 = load i32, i32* %x, align 4, !tbaa !8
  %add23 = add nsw i32 %add22, %42
  %arrayidx24 = getelementptr inbounds i8, i8* %37, i32 %add23
  %43 = load i8, i8* %arrayidx24, align 1, !tbaa !10
  %idxprom25 = zext i8 %43 to i32
  %arrayidx26 = getelementptr inbounds i16, i16* %36, i32 %idxprom25
  %44 = load i16, i16* %arrayidx26, align 2, !tbaa !11
  %45 = load i16*, i16** %dst16, align 4, !tbaa !2
  %46 = load i32, i32* %r, align 4, !tbaa !8
  %47 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %mul27 = mul nsw i32 %46, %47
  %48 = load i32, i32* %c, align 4, !tbaa !8
  %add28 = add nsw i32 %mul27, %48
  %arrayidx29 = getelementptr inbounds i16, i16* %45, i32 %add28
  store i16 %44, i16* %arrayidx29, align 2, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body20
  %49 = load i32, i32* %c, align 4, !tbaa !8
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %c, align 4, !tbaa !8
  br label %for.cond17

for.end:                                          ; preds = %for.cond17
  br label %for.inc30

for.inc30:                                        ; preds = %for.end
  %50 = load i32, i32* %r, align 4, !tbaa !8
  %inc31 = add nsw i32 %50, 1
  store i32 %inc31, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end32:                                        ; preds = %for.cond
  %51 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  br label %if.end

if.else:                                          ; preds = %if.then
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc55, %if.else
  %52 = load i32, i32* %r, align 4, !tbaa !8
  %53 = load i32, i32* %txhpx, align 4, !tbaa !8
  %cmp34 = icmp slt i32 %52, %53
  br i1 %cmp34, label %for.body36, label %for.end57

for.body36:                                       ; preds = %for.cond33
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc52, %for.body36
  %54 = load i32, i32* %c, align 4, !tbaa !8
  %55 = load i32, i32* %txwpx, align 4, !tbaa !8
  %cmp38 = icmp slt i32 %54, %55
  br i1 %cmp38, label %for.body40, label %for.end54

for.body40:                                       ; preds = %for.cond37
  %56 = load i16*, i16** %palette, align 4, !tbaa !2
  %57 = load i8*, i8** %map, align 4, !tbaa !2
  %58 = load i32, i32* %r, align 4, !tbaa !8
  %59 = load i32, i32* %y, align 4, !tbaa !8
  %add41 = add nsw i32 %58, %59
  %60 = load i32, i32* %wpx.addr, align 4, !tbaa !8
  %mul42 = mul nsw i32 %add41, %60
  %61 = load i32, i32* %c, align 4, !tbaa !8
  %add43 = add nsw i32 %mul42, %61
  %62 = load i32, i32* %x, align 4, !tbaa !8
  %add44 = add nsw i32 %add43, %62
  %arrayidx45 = getelementptr inbounds i8, i8* %57, i32 %add44
  %63 = load i8, i8* %arrayidx45, align 1, !tbaa !10
  %idxprom46 = zext i8 %63 to i32
  %arrayidx47 = getelementptr inbounds i16, i16* %56, i32 %idxprom46
  %64 = load i16, i16* %arrayidx47, align 2, !tbaa !11
  %conv48 = trunc i16 %64 to i8
  %65 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %66 = load i32, i32* %r, align 4, !tbaa !8
  %67 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %mul49 = mul nsw i32 %66, %67
  %68 = load i32, i32* %c, align 4, !tbaa !8
  %add50 = add nsw i32 %mul49, %68
  %arrayidx51 = getelementptr inbounds i8, i8* %65, i32 %add50
  store i8 %conv48, i8* %arrayidx51, align 1, !tbaa !10
  br label %for.inc52

for.inc52:                                        ; preds = %for.body40
  %69 = load i32, i32* %c, align 4, !tbaa !8
  %inc53 = add nsw i32 %69, 1
  store i32 %inc53, i32* %c, align 4, !tbaa !8
  br label %for.cond37

for.end54:                                        ; preds = %for.cond37
  br label %for.inc55

for.inc55:                                        ; preds = %for.end54
  %70 = load i32, i32* %r, align 4, !tbaa !8
  %inc56 = add nsw i32 %70, 1
  store i32 %inc56, i32* %r, align 4, !tbaa !8
  br label %for.cond33

for.end57:                                        ; preds = %for.cond33
  br label %if.end

if.end:                                           ; preds = %for.end57, %for.end32
  store i32 1, i32* %cleanup.dest.slot, align 4
  %71 = bitcast i16** %palette to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  %72 = bitcast i8** %map to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #6
  %73 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #6
  %74 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #6
  br label %cleanup241

if.end58:                                         ; preds = %entry
  %75 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #6
  %76 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane59 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %76, i32 0, i32 4
  %77 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %arrayidx60 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane59, i32 0, i32 %77
  store %struct.macroblockd_plane* %arrayidx60, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %78 = bitcast i32* %txw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #6
  %79 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom61 = zext i8 %79 to i32
  %arrayidx62 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_unit, i32 0, i32 %idxprom61
  %80 = load i32, i32* %arrayidx62, align 4, !tbaa !8
  store i32 %80, i32* %txw, align 4, !tbaa !8
  %81 = bitcast i32* %txh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #6
  %82 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom63 = zext i8 %82 to i32
  %arrayidx64 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_unit, i32 0, i32 %idxprom63
  %83 = load i32, i32* %arrayidx64, align 4, !tbaa !8
  store i32 %83, i32* %txh, align 4, !tbaa !8
  %84 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #6
  %85 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %85, i32 0, i32 4
  %86 = load i32, i32* %subsampling_x, align 4, !tbaa !22
  store i32 %86, i32* %ss_x, align 4, !tbaa !8
  %87 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #6
  %88 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %88, i32 0, i32 5
  %89 = load i32, i32* %subsampling_y, align 4, !tbaa !23
  store i32 %89, i32* %ss_y, align 4, !tbaa !8
  %90 = bitcast i32* %have_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  %91 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %tobool65 = icmp ne i32 %91, 0
  br i1 %tobool65, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end58
  %92 = load i32, i32* %ss_y, align 4, !tbaa !8
  %tobool66 = icmp ne i32 %92, 0
  br i1 %tobool66, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.rhs
  %93 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %chroma_up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %93, i32 0, i32 9
  %94 = load i8, i8* %chroma_up_available, align 2, !tbaa !24, !range !25
  %tobool67 = trunc i8 %94 to i1
  %conv68 = zext i1 %tobool67 to i32
  br label %cond.end

cond.false:                                       ; preds = %lor.rhs
  %95 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %95, i32 0, i32 7
  %96 = load i8, i8* %up_available, align 8, !tbaa !26, !range !25
  %tobool69 = trunc i8 %96 to i1
  %conv70 = zext i1 %tobool69 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv68, %cond.true ], [ %conv70, %cond.false ]
  %tobool71 = icmp ne i32 %cond, 0
  br label %lor.end

lor.end:                                          ; preds = %cond.end, %if.end58
  %97 = phi i1 [ true, %if.end58 ], [ %tobool71, %cond.end ]
  %lor.ext = zext i1 %97 to i32
  store i32 %lor.ext, i32* %have_top, align 4, !tbaa !8
  %98 = bitcast i32* %have_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #6
  %99 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %tobool72 = icmp ne i32 %99, 0
  br i1 %tobool72, label %lor.end84, label %lor.rhs73

lor.rhs73:                                        ; preds = %lor.end
  %100 = load i32, i32* %ss_x, align 4, !tbaa !8
  %tobool74 = icmp ne i32 %100, 0
  br i1 %tobool74, label %cond.true75, label %cond.false78

cond.true75:                                      ; preds = %lor.rhs73
  %101 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %chroma_left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %101, i32 0, i32 10
  %102 = load i8, i8* %chroma_left_available, align 1, !tbaa !27, !range !25
  %tobool76 = trunc i8 %102 to i1
  %conv77 = zext i1 %tobool76 to i32
  br label %cond.end81

cond.false78:                                     ; preds = %lor.rhs73
  %103 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %103, i32 0, i32 8
  %104 = load i8, i8* %left_available, align 1, !tbaa !28, !range !25
  %tobool79 = trunc i8 %104 to i1
  %conv80 = zext i1 %tobool79 to i32
  br label %cond.end81

cond.end81:                                       ; preds = %cond.false78, %cond.true75
  %cond82 = phi i32 [ %conv77, %cond.true75 ], [ %conv80, %cond.false78 ]
  %tobool83 = icmp ne i32 %cond82, 0
  br label %lor.end84

lor.end84:                                        ; preds = %cond.end81, %lor.end
  %105 = phi i1 [ true, %lor.end ], [ %tobool83, %cond.end81 ]
  %lor.ext85 = zext i1 %105 to i32
  store i32 %lor.ext85, i32* %have_left, align 4, !tbaa !8
  %106 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #6
  %107 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_top_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %107, i32 0, i32 19
  %108 = load i32, i32* %mb_to_top_edge, align 4, !tbaa !29
  %sub = sub nsw i32 0, %108
  %shr = ashr i32 %sub, 5
  store i32 %shr, i32* %mi_row, align 4, !tbaa !8
  %109 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #6
  %110 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_left_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %110, i32 0, i32 17
  %111 = load i32, i32* %mb_to_left_edge, align 4, !tbaa !30
  %sub86 = sub nsw i32 0, %111
  %shr87 = ashr i32 %sub86, 5
  store i32 %shr87, i32* %mi_col, align 4, !tbaa !8
  %112 = bitcast i32* %xr_chr_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #6
  store i32 0, i32* %xr_chr_offset, align 4, !tbaa !8
  %113 = bitcast i32* %yd_chr_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #6
  store i32 0, i32* %yd_chr_offset, align 4, !tbaa !8
  %114 = bitcast i32* %xr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #6
  %115 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %115, i32 0, i32 18
  %116 = load i32, i32* %mb_to_right_edge, align 8, !tbaa !31
  %117 = load i32, i32* %ss_x, align 4, !tbaa !8
  %add88 = add nsw i32 3, %117
  %shr89 = ashr i32 %116, %add88
  %118 = load i32, i32* %wpx.addr, align 4, !tbaa !8
  %119 = load i32, i32* %x, align 4, !tbaa !8
  %sub90 = sub nsw i32 %118, %119
  %120 = load i32, i32* %txwpx, align 4, !tbaa !8
  %sub91 = sub nsw i32 %sub90, %120
  %add92 = add nsw i32 %shr89, %sub91
  %sub93 = sub nsw i32 %add92, 0
  store i32 %sub93, i32* %xr, align 4, !tbaa !8
  %121 = bitcast i32* %yd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #6
  %122 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %122, i32 0, i32 20
  %123 = load i32, i32* %mb_to_bottom_edge, align 16, !tbaa !32
  %124 = load i32, i32* %ss_y, align 4, !tbaa !8
  %add94 = add nsw i32 3, %124
  %shr95 = ashr i32 %123, %add94
  %125 = load i32, i32* %hpx.addr, align 4, !tbaa !8
  %126 = load i32, i32* %y, align 4, !tbaa !8
  %sub96 = sub nsw i32 %125, %126
  %127 = load i32, i32* %txhpx, align 4, !tbaa !8
  %sub97 = sub nsw i32 %sub96, %127
  %add98 = add nsw i32 %shr95, %sub97
  %sub99 = sub nsw i32 %add98, 0
  store i32 %sub99, i32* %yd, align 4, !tbaa !8
  %128 = bitcast i32* %right_available to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #6
  %129 = load i32, i32* %mi_col, align 4, !tbaa !8
  %130 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %131 = load i32, i32* %txw, align 4, !tbaa !8
  %add100 = add nsw i32 %130, %131
  %132 = load i32, i32* %ss_x, align 4, !tbaa !8
  %shl101 = shl i32 %add100, %132
  %add102 = add nsw i32 %129, %shl101
  %133 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tile = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %133, i32 0, i32 5
  %mi_col_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile, i32 0, i32 3
  %134 = load i32, i32* %mi_col_end, align 4, !tbaa !33
  %cmp103 = icmp slt i32 %add102, %134
  %conv104 = zext i1 %cmp103 to i32
  store i32 %conv104, i32* %right_available, align 4, !tbaa !8
  %135 = bitcast i32* %bottom_available to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #6
  %136 = load i32, i32* %yd, align 4, !tbaa !8
  %cmp105 = icmp sgt i32 %136, 0
  br i1 %cmp105, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %lor.end84
  %137 = load i32, i32* %mi_row, align 4, !tbaa !8
  %138 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %139 = load i32, i32* %txh, align 4, !tbaa !8
  %add107 = add nsw i32 %138, %139
  %140 = load i32, i32* %ss_y, align 4, !tbaa !8
  %shl108 = shl i32 %add107, %140
  %add109 = add nsw i32 %137, %shl108
  %141 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tile110 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %141, i32 0, i32 5
  %mi_row_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile110, i32 0, i32 1
  %142 = load i32, i32* %mi_row_end, align 4, !tbaa !34
  %cmp111 = icmp slt i32 %add109, %142
  br label %land.end

land.end:                                         ; preds = %land.rhs, %lor.end84
  %143 = phi i1 [ false, %lor.end84 ], [ %cmp111, %land.rhs ]
  %land.ext = zext i1 %143 to i32
  store i32 %land.ext, i32* %bottom_available, align 4, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %partition) #6
  %144 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %partition113 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %144, i32 0, i32 11
  %145 = load i8, i8* %partition113, align 1, !tbaa !35
  store i8 %145, i8* %partition, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %146 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %146, i32 0, i32 6
  %147 = load i8, i8* %sb_type, align 2, !tbaa !41
  store i8 %147, i8* %bsize, align 1, !tbaa !10
  %148 = load i32, i32* %ss_x, align 4, !tbaa !8
  %tobool114 = icmp ne i32 %148, 0
  br i1 %tobool114, label %if.then116, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.end
  %149 = load i32, i32* %ss_y, align 4, !tbaa !8
  %tobool115 = icmp ne i32 %149, 0
  br i1 %tobool115, label %if.then116, label %if.end118

if.then116:                                       ; preds = %lor.lhs.false, %land.end
  %150 = load i8, i8* %bsize, align 1, !tbaa !10
  %151 = load i32, i32* %ss_x, align 4, !tbaa !8
  %152 = load i32, i32* %ss_y, align 4, !tbaa !8
  %call117 = call zeroext i8 @scale_chroma_bsize(i8 zeroext %150, i32 %151, i32 %152)
  store i8 %call117, i8* %bsize, align 1, !tbaa !10
  br label %if.end118

if.end118:                                        ; preds = %if.then116, %lor.lhs.false
  %153 = bitcast i32* %have_top_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %153) #6
  %154 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %155 = load i8, i8* %bsize, align 1, !tbaa !10
  %156 = load i32, i32* %mi_row, align 4, !tbaa !8
  %157 = load i32, i32* %mi_col, align 4, !tbaa !8
  %158 = load i32, i32* %have_top, align 4, !tbaa !8
  %159 = load i32, i32* %right_available, align 4, !tbaa !8
  %160 = load i8, i8* %partition, align 1, !tbaa !10
  %161 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %162 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %163 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %164 = load i32, i32* %ss_x, align 4, !tbaa !8
  %165 = load i32, i32* %ss_y, align 4, !tbaa !8
  %call119 = call i32 @has_top_right(%struct.AV1Common* %154, i8 zeroext %155, i32 %156, i32 %157, i32 %158, i32 %159, i8 zeroext %160, i8 zeroext %161, i32 %162, i32 %163, i32 %164, i32 %165)
  store i32 %call119, i32* %have_top_right, align 4, !tbaa !8
  %166 = bitcast i32* %have_bottom_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %166) #6
  %167 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %168 = load i8, i8* %bsize, align 1, !tbaa !10
  %169 = load i32, i32* %mi_row, align 4, !tbaa !8
  %170 = load i32, i32* %mi_col, align 4, !tbaa !8
  %171 = load i32, i32* %bottom_available, align 4, !tbaa !8
  %172 = load i32, i32* %have_left, align 4, !tbaa !8
  %173 = load i8, i8* %partition, align 1, !tbaa !10
  %174 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %175 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %176 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %177 = load i32, i32* %ss_x, align 4, !tbaa !8
  %178 = load i32, i32* %ss_y, align 4, !tbaa !8
  %call120 = call i32 @has_bottom_left(%struct.AV1Common* %167, i8 zeroext %168, i32 %169, i32 %170, i32 %171, i32 %172, i8 zeroext %173, i8 zeroext %174, i32 %175, i32 %176, i32 %177, i32 %178)
  store i32 %call120, i32* %have_bottom_left, align 4, !tbaa !8
  %179 = bitcast i32* %disable_edge_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %179) #6
  %180 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %180, i32 0, i32 37
  %enable_intra_edge_filter = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 16
  %181 = load i8, i8* %enable_intra_edge_filter, align 1, !tbaa !42
  %tobool121 = icmp ne i8 %181, 0
  %lnot = xor i1 %tobool121, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %disable_edge_filter, align 4, !tbaa !8
  %182 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call122 = call i32 @is_cur_buf_hbd(%struct.macroblockd* %182)
  %tobool123 = icmp ne i32 %call122, 0
  br i1 %tobool123, label %if.then124, label %if.end173

if.then124:                                       ; preds = %if.end118
  %183 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %184 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %185 = load i32, i32* %ref_stride.addr, align 4, !tbaa !8
  %186 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %187 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %188 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %189 = load i32, i32* %angle_delta.addr, align 4, !tbaa !8
  %190 = load i8, i8* %filter_intra_mode.addr, align 1, !tbaa !10
  %191 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %192 = load i32, i32* %disable_edge_filter, align 4, !tbaa !8
  %193 = load i32, i32* %have_top, align 4, !tbaa !8
  %tobool125 = icmp ne i32 %193, 0
  br i1 %tobool125, label %cond.true126, label %cond.false135

cond.true126:                                     ; preds = %if.then124
  %194 = load i32, i32* %txwpx, align 4, !tbaa !8
  %195 = load i32, i32* %xr, align 4, !tbaa !8
  %196 = load i32, i32* %txwpx, align 4, !tbaa !8
  %add127 = add nsw i32 %195, %196
  %cmp128 = icmp slt i32 %194, %add127
  br i1 %cmp128, label %cond.true130, label %cond.false131

cond.true130:                                     ; preds = %cond.true126
  %197 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end133

cond.false131:                                    ; preds = %cond.true126
  %198 = load i32, i32* %xr, align 4, !tbaa !8
  %199 = load i32, i32* %txwpx, align 4, !tbaa !8
  %add132 = add nsw i32 %198, %199
  br label %cond.end133

cond.end133:                                      ; preds = %cond.false131, %cond.true130
  %cond134 = phi i32 [ %197, %cond.true130 ], [ %add132, %cond.false131 ]
  br label %cond.end136

cond.false135:                                    ; preds = %if.then124
  br label %cond.end136

cond.end136:                                      ; preds = %cond.false135, %cond.end133
  %cond137 = phi i32 [ %cond134, %cond.end133 ], [ 0, %cond.false135 ]
  %200 = load i32, i32* %have_top_right, align 4, !tbaa !8
  %tobool138 = icmp ne i32 %200, 0
  br i1 %tobool138, label %cond.true139, label %cond.false146

cond.true139:                                     ; preds = %cond.end136
  %201 = load i32, i32* %txwpx, align 4, !tbaa !8
  %202 = load i32, i32* %xr, align 4, !tbaa !8
  %cmp140 = icmp slt i32 %201, %202
  br i1 %cmp140, label %cond.true142, label %cond.false143

cond.true142:                                     ; preds = %cond.true139
  %203 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end144

cond.false143:                                    ; preds = %cond.true139
  %204 = load i32, i32* %xr, align 4, !tbaa !8
  br label %cond.end144

cond.end144:                                      ; preds = %cond.false143, %cond.true142
  %cond145 = phi i32 [ %203, %cond.true142 ], [ %204, %cond.false143 ]
  br label %cond.end147

cond.false146:                                    ; preds = %cond.end136
  br label %cond.end147

cond.end147:                                      ; preds = %cond.false146, %cond.end144
  %cond148 = phi i32 [ %cond145, %cond.end144 ], [ 0, %cond.false146 ]
  %205 = load i32, i32* %have_left, align 4, !tbaa !8
  %tobool149 = icmp ne i32 %205, 0
  br i1 %tobool149, label %cond.true150, label %cond.false159

cond.true150:                                     ; preds = %cond.end147
  %206 = load i32, i32* %txhpx, align 4, !tbaa !8
  %207 = load i32, i32* %yd, align 4, !tbaa !8
  %208 = load i32, i32* %txhpx, align 4, !tbaa !8
  %add151 = add nsw i32 %207, %208
  %cmp152 = icmp slt i32 %206, %add151
  br i1 %cmp152, label %cond.true154, label %cond.false155

cond.true154:                                     ; preds = %cond.true150
  %209 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end157

cond.false155:                                    ; preds = %cond.true150
  %210 = load i32, i32* %yd, align 4, !tbaa !8
  %211 = load i32, i32* %txhpx, align 4, !tbaa !8
  %add156 = add nsw i32 %210, %211
  br label %cond.end157

cond.end157:                                      ; preds = %cond.false155, %cond.true154
  %cond158 = phi i32 [ %209, %cond.true154 ], [ %add156, %cond.false155 ]
  br label %cond.end160

cond.false159:                                    ; preds = %cond.end147
  br label %cond.end160

cond.end160:                                      ; preds = %cond.false159, %cond.end157
  %cond161 = phi i32 [ %cond158, %cond.end157 ], [ 0, %cond.false159 ]
  %212 = load i32, i32* %have_bottom_left, align 4, !tbaa !8
  %tobool162 = icmp ne i32 %212, 0
  br i1 %tobool162, label %cond.true163, label %cond.false170

cond.true163:                                     ; preds = %cond.end160
  %213 = load i32, i32* %txhpx, align 4, !tbaa !8
  %214 = load i32, i32* %yd, align 4, !tbaa !8
  %cmp164 = icmp slt i32 %213, %214
  br i1 %cmp164, label %cond.true166, label %cond.false167

cond.true166:                                     ; preds = %cond.true163
  %215 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end168

cond.false167:                                    ; preds = %cond.true163
  %216 = load i32, i32* %yd, align 4, !tbaa !8
  br label %cond.end168

cond.end168:                                      ; preds = %cond.false167, %cond.true166
  %cond169 = phi i32 [ %215, %cond.true166 ], [ %216, %cond.false167 ]
  br label %cond.end171

cond.false170:                                    ; preds = %cond.end160
  br label %cond.end171

cond.end171:                                      ; preds = %cond.false170, %cond.end168
  %cond172 = phi i32 [ %cond169, %cond.end168 ], [ 0, %cond.false170 ]
  %217 = load i32, i32* %plane.addr, align 4, !tbaa !8
  call void @build_intra_predictors_high(%struct.macroblockd* %183, i8* %184, i32 %185, i8* %186, i32 %187, i8 zeroext %188, i32 %189, i8 zeroext %190, i8 zeroext %191, i32 %192, i32 %cond137, i32 %cond148, i32 %cond161, i32 %cond172, i32 %217)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end173:                                        ; preds = %if.end118
  %218 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %219 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %220 = load i32, i32* %ref_stride.addr, align 4, !tbaa !8
  %221 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %222 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %223 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %224 = load i32, i32* %angle_delta.addr, align 4, !tbaa !8
  %225 = load i8, i8* %filter_intra_mode.addr, align 1, !tbaa !10
  %226 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %227 = load i32, i32* %disable_edge_filter, align 4, !tbaa !8
  %228 = load i32, i32* %have_top, align 4, !tbaa !8
  %tobool174 = icmp ne i32 %228, 0
  br i1 %tobool174, label %cond.true175, label %cond.false184

cond.true175:                                     ; preds = %if.end173
  %229 = load i32, i32* %txwpx, align 4, !tbaa !8
  %230 = load i32, i32* %xr, align 4, !tbaa !8
  %231 = load i32, i32* %txwpx, align 4, !tbaa !8
  %add176 = add nsw i32 %230, %231
  %cmp177 = icmp slt i32 %229, %add176
  br i1 %cmp177, label %cond.true179, label %cond.false180

cond.true179:                                     ; preds = %cond.true175
  %232 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end182

cond.false180:                                    ; preds = %cond.true175
  %233 = load i32, i32* %xr, align 4, !tbaa !8
  %234 = load i32, i32* %txwpx, align 4, !tbaa !8
  %add181 = add nsw i32 %233, %234
  br label %cond.end182

cond.end182:                                      ; preds = %cond.false180, %cond.true179
  %cond183 = phi i32 [ %232, %cond.true179 ], [ %add181, %cond.false180 ]
  br label %cond.end185

cond.false184:                                    ; preds = %if.end173
  br label %cond.end185

cond.end185:                                      ; preds = %cond.false184, %cond.end182
  %cond186 = phi i32 [ %cond183, %cond.end182 ], [ 0, %cond.false184 ]
  %235 = load i32, i32* %have_top_right, align 4, !tbaa !8
  %tobool187 = icmp ne i32 %235, 0
  br i1 %tobool187, label %cond.true188, label %cond.false195

cond.true188:                                     ; preds = %cond.end185
  %236 = load i32, i32* %txwpx, align 4, !tbaa !8
  %237 = load i32, i32* %xr, align 4, !tbaa !8
  %cmp189 = icmp slt i32 %236, %237
  br i1 %cmp189, label %cond.true191, label %cond.false192

cond.true191:                                     ; preds = %cond.true188
  %238 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end193

cond.false192:                                    ; preds = %cond.true188
  %239 = load i32, i32* %xr, align 4, !tbaa !8
  br label %cond.end193

cond.end193:                                      ; preds = %cond.false192, %cond.true191
  %cond194 = phi i32 [ %238, %cond.true191 ], [ %239, %cond.false192 ]
  br label %cond.end196

cond.false195:                                    ; preds = %cond.end185
  br label %cond.end196

cond.end196:                                      ; preds = %cond.false195, %cond.end193
  %cond197 = phi i32 [ %cond194, %cond.end193 ], [ 0, %cond.false195 ]
  %240 = load i32, i32* %have_left, align 4, !tbaa !8
  %tobool198 = icmp ne i32 %240, 0
  br i1 %tobool198, label %cond.true199, label %cond.false208

cond.true199:                                     ; preds = %cond.end196
  %241 = load i32, i32* %txhpx, align 4, !tbaa !8
  %242 = load i32, i32* %yd, align 4, !tbaa !8
  %243 = load i32, i32* %txhpx, align 4, !tbaa !8
  %add200 = add nsw i32 %242, %243
  %cmp201 = icmp slt i32 %241, %add200
  br i1 %cmp201, label %cond.true203, label %cond.false204

cond.true203:                                     ; preds = %cond.true199
  %244 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end206

cond.false204:                                    ; preds = %cond.true199
  %245 = load i32, i32* %yd, align 4, !tbaa !8
  %246 = load i32, i32* %txhpx, align 4, !tbaa !8
  %add205 = add nsw i32 %245, %246
  br label %cond.end206

cond.end206:                                      ; preds = %cond.false204, %cond.true203
  %cond207 = phi i32 [ %244, %cond.true203 ], [ %add205, %cond.false204 ]
  br label %cond.end209

cond.false208:                                    ; preds = %cond.end196
  br label %cond.end209

cond.end209:                                      ; preds = %cond.false208, %cond.end206
  %cond210 = phi i32 [ %cond207, %cond.end206 ], [ 0, %cond.false208 ]
  %247 = load i32, i32* %have_bottom_left, align 4, !tbaa !8
  %tobool211 = icmp ne i32 %247, 0
  br i1 %tobool211, label %cond.true212, label %cond.false219

cond.true212:                                     ; preds = %cond.end209
  %248 = load i32, i32* %txhpx, align 4, !tbaa !8
  %249 = load i32, i32* %yd, align 4, !tbaa !8
  %cmp213 = icmp slt i32 %248, %249
  br i1 %cmp213, label %cond.true215, label %cond.false216

cond.true215:                                     ; preds = %cond.true212
  %250 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end217

cond.false216:                                    ; preds = %cond.true212
  %251 = load i32, i32* %yd, align 4, !tbaa !8
  br label %cond.end217

cond.end217:                                      ; preds = %cond.false216, %cond.true215
  %cond218 = phi i32 [ %250, %cond.true215 ], [ %251, %cond.false216 ]
  br label %cond.end220

cond.false219:                                    ; preds = %cond.end209
  br label %cond.end220

cond.end220:                                      ; preds = %cond.false219, %cond.end217
  %cond221 = phi i32 [ %cond218, %cond.end217 ], [ 0, %cond.false219 ]
  %252 = load i32, i32* %plane.addr, align 4, !tbaa !8
  call void @build_intra_predictors(%struct.macroblockd* %218, i8* %219, i32 %220, i8* %221, i32 %222, i8 zeroext %223, i32 %224, i8 zeroext %225, i8 zeroext %226, i32 %227, i32 %cond186, i32 %cond197, i32 %cond210, i32 %cond221, i32 %252)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %cond.end220, %cond.end171
  %253 = bitcast i32* %disable_edge_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #6
  %254 = bitcast i32* %have_bottom_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #6
  %255 = bitcast i32* %have_top_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %partition) #6
  %256 = bitcast i32* %bottom_available to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #6
  %257 = bitcast i32* %right_available to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #6
  %258 = bitcast i32* %yd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #6
  %259 = bitcast i32* %xr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #6
  %260 = bitcast i32* %yd_chr_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #6
  %261 = bitcast i32* %xr_chr_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #6
  %262 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #6
  %263 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #6
  %264 = bitcast i32* %have_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #6
  %265 = bitcast i32* %have_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #6
  %266 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #6
  %267 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #6
  %268 = bitcast i32* %txh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #6
  %269 = bitcast i32* %txw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #6
  %270 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %270) #6
  br label %cleanup241

cleanup241:                                       ; preds = %cleanup, %if.end
  %271 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %271) #6
  %272 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #6
  %273 = bitcast i32* %txhpx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #6
  %274 = bitcast i32* %txwpx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #6
  %275 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup241, %cleanup241
  ret void

unreachable:                                      ; preds = %cleanup241
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_cur_buf_hbd(%struct.macroblockd* %xd) #4 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cur_buf = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 22
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %cur_buf, align 4, !tbaa !63
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 26
  %2 = load i32, i32* %flags, align 4, !tbaa !64
  %and = and i32 %2, 8
  %tobool = icmp ne i32 %and, 0
  %3 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @scale_chroma_bsize(i8 zeroext %bsize, i32 %subsampling_x, i32 %subsampling_y) #4 {
entry:
  %bsize.addr = alloca i8, align 1
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %bs = alloca i8, align 1
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !10
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !8
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bs) #6
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  store i8 %0, i8* %bs, align 1, !tbaa !10
  %1 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %conv = zext i8 %1 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb13
    i32 2, label %sw.bb31
    i32 16, label %sw.bb49
    i32 17, label %sw.bb67
  ]

sw.bb:                                            ; preds = %entry
  %2 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp = icmp eq i32 %2, 1
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %sw.bb
  %3 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp2 = icmp eq i32 %3, 1
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  store i8 3, i8* %bs, align 1, !tbaa !10
  br label %if.end12

if.else:                                          ; preds = %land.lhs.true, %sw.bb
  %4 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp4 = icmp eq i32 %4, 1
  br i1 %cmp4, label %if.then6, label %if.else7

if.then6:                                         ; preds = %if.else
  store i8 2, i8* %bs, align 1, !tbaa !10
  br label %if.end11

if.else7:                                         ; preds = %if.else
  %5 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp8 = icmp eq i32 %5, 1
  br i1 %cmp8, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.else7
  store i8 1, i8* %bs, align 1, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.else7
  br label %if.end11

if.end11:                                         ; preds = %if.end, %if.then6
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.then
  br label %sw.epilog

sw.bb13:                                          ; preds = %entry
  %6 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp14 = icmp eq i32 %6, 1
  br i1 %cmp14, label %land.lhs.true16, label %if.else20

land.lhs.true16:                                  ; preds = %sw.bb13
  %7 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp17 = icmp eq i32 %7, 1
  br i1 %cmp17, label %if.then19, label %if.else20

if.then19:                                        ; preds = %land.lhs.true16
  store i8 3, i8* %bs, align 1, !tbaa !10
  br label %if.end30

if.else20:                                        ; preds = %land.lhs.true16, %sw.bb13
  %8 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp21 = icmp eq i32 %8, 1
  br i1 %cmp21, label %if.then23, label %if.else24

if.then23:                                        ; preds = %if.else20
  store i8 3, i8* %bs, align 1, !tbaa !10
  br label %if.end29

if.else24:                                        ; preds = %if.else20
  %9 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp25 = icmp eq i32 %9, 1
  br i1 %cmp25, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.else24
  store i8 1, i8* %bs, align 1, !tbaa !10
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %if.else24
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.then23
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %if.then19
  br label %sw.epilog

sw.bb31:                                          ; preds = %entry
  %10 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp32 = icmp eq i32 %10, 1
  br i1 %cmp32, label %land.lhs.true34, label %if.else38

land.lhs.true34:                                  ; preds = %sw.bb31
  %11 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp35 = icmp eq i32 %11, 1
  br i1 %cmp35, label %if.then37, label %if.else38

if.then37:                                        ; preds = %land.lhs.true34
  store i8 3, i8* %bs, align 1, !tbaa !10
  br label %if.end48

if.else38:                                        ; preds = %land.lhs.true34, %sw.bb31
  %12 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp39 = icmp eq i32 %12, 1
  br i1 %cmp39, label %if.then41, label %if.else42

if.then41:                                        ; preds = %if.else38
  store i8 2, i8* %bs, align 1, !tbaa !10
  br label %if.end47

if.else42:                                        ; preds = %if.else38
  %13 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp43 = icmp eq i32 %13, 1
  br i1 %cmp43, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.else42
  store i8 3, i8* %bs, align 1, !tbaa !10
  br label %if.end46

if.end46:                                         ; preds = %if.then45, %if.else42
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.then41
  br label %if.end48

if.end48:                                         ; preds = %if.end47, %if.then37
  br label %sw.epilog

sw.bb49:                                          ; preds = %entry
  %14 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp50 = icmp eq i32 %14, 1
  br i1 %cmp50, label %land.lhs.true52, label %if.else56

land.lhs.true52:                                  ; preds = %sw.bb49
  %15 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp53 = icmp eq i32 %15, 1
  br i1 %cmp53, label %if.then55, label %if.else56

if.then55:                                        ; preds = %land.lhs.true52
  store i8 4, i8* %bs, align 1, !tbaa !10
  br label %if.end66

if.else56:                                        ; preds = %land.lhs.true52, %sw.bb49
  %16 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp57 = icmp eq i32 %16, 1
  br i1 %cmp57, label %if.then59, label %if.else60

if.then59:                                        ; preds = %if.else56
  store i8 4, i8* %bs, align 1, !tbaa !10
  br label %if.end65

if.else60:                                        ; preds = %if.else56
  %17 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp61 = icmp eq i32 %17, 1
  br i1 %cmp61, label %if.then63, label %if.end64

if.then63:                                        ; preds = %if.else60
  store i8 16, i8* %bs, align 1, !tbaa !10
  br label %if.end64

if.end64:                                         ; preds = %if.then63, %if.else60
  br label %if.end65

if.end65:                                         ; preds = %if.end64, %if.then59
  br label %if.end66

if.end66:                                         ; preds = %if.end65, %if.then55
  br label %sw.epilog

sw.bb67:                                          ; preds = %entry
  %18 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp68 = icmp eq i32 %18, 1
  br i1 %cmp68, label %land.lhs.true70, label %if.else74

land.lhs.true70:                                  ; preds = %sw.bb67
  %19 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp71 = icmp eq i32 %19, 1
  br i1 %cmp71, label %if.then73, label %if.else74

if.then73:                                        ; preds = %land.lhs.true70
  store i8 5, i8* %bs, align 1, !tbaa !10
  br label %if.end84

if.else74:                                        ; preds = %land.lhs.true70, %sw.bb67
  %20 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !8
  %cmp75 = icmp eq i32 %20, 1
  br i1 %cmp75, label %if.then77, label %if.else78

if.then77:                                        ; preds = %if.else74
  store i8 17, i8* %bs, align 1, !tbaa !10
  br label %if.end83

if.else78:                                        ; preds = %if.else74
  %21 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !8
  %cmp79 = icmp eq i32 %21, 1
  br i1 %cmp79, label %if.then81, label %if.end82

if.then81:                                        ; preds = %if.else78
  store i8 5, i8* %bs, align 1, !tbaa !10
  br label %if.end82

if.end82:                                         ; preds = %if.then81, %if.else78
  br label %if.end83

if.end83:                                         ; preds = %if.end82, %if.then77
  br label %if.end84

if.end84:                                         ; preds = %if.end83, %if.then73
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end84, %if.end66, %if.end48, %if.end30, %if.end12
  %22 = load i8, i8* %bs, align 1, !tbaa !10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bs) #6
  ret i8 %22
}

; Function Attrs: nounwind
define internal i32 @has_top_right(%struct.AV1Common* %cm, i8 zeroext %bsize, i32 %mi_row, i32 %mi_col, i32 %top_available, i32 %right_available, i8 zeroext %partition, i8 zeroext %txsz, i32 %row_off, i32 %col_off, i32 %ss_x, i32 %ss_y) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %bsize.addr = alloca i8, align 1
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %top_available.addr = alloca i32, align 4
  %right_available.addr = alloca i32, align 4
  %partition.addr = alloca i8, align 1
  %txsz.addr = alloca i8, align 1
  %row_off.addr = alloca i32, align 4
  %col_off.addr = alloca i32, align 4
  %ss_x.addr = alloca i32, align 4
  %ss_y.addr = alloca i32, align 4
  %bw_unit = alloca i32, align 4
  %plane_bw_unit = alloca i32, align 4
  %top_right_count_unit = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %plane_bw_unit_64 = alloca i32, align 4
  %col_off_64 = alloca i32, align 4
  %bw_in_mi_log2 = alloca i32, align 4
  %bh_in_mi_log2 = alloca i32, align 4
  %sb_mi_size = alloca i32, align 4
  %blk_row_in_sb = alloca i32, align 4
  %blk_col_in_sb = alloca i32, align 4
  %this_blk_index = alloca i32, align 4
  %idx1 = alloca i32, align 4
  %idx2 = alloca i32, align 4
  %has_tr_table = alloca i8*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !10
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !8
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !8
  store i32 %top_available, i32* %top_available.addr, align 4, !tbaa !8
  store i32 %right_available, i32* %right_available.addr, align 4, !tbaa !8
  store i8 %partition, i8* %partition.addr, align 1, !tbaa !10
  store i8 %txsz, i8* %txsz.addr, align 1, !tbaa !10
  store i32 %row_off, i32* %row_off.addr, align 4, !tbaa !8
  store i32 %col_off, i32* %col_off.addr, align 4, !tbaa !8
  store i32 %ss_x, i32* %ss_x.addr, align 4, !tbaa !8
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !8
  %0 = load i32, i32* %top_available.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %right_available.addr, align 4, !tbaa !8
  %tobool1 = icmp ne i32 %1, 0
  br i1 %tobool1, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = bitcast i32* %bw_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %bw_unit, align 4, !tbaa !8
  %5 = bitcast i32* %plane_bw_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i32, i32* %bw_unit, align 4, !tbaa !8
  %7 = load i32, i32* %ss_x.addr, align 4, !tbaa !8
  %shr = ashr i32 %6, %7
  %cmp = icmp sgt i32 %shr, 1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %8 = load i32, i32* %bw_unit, align 4, !tbaa !8
  %9 = load i32, i32* %ss_x.addr, align 4, !tbaa !8
  %shr3 = ashr i32 %8, %9
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shr3, %cond.true ], [ 1, %cond.false ]
  store i32 %cond, i32* %plane_bw_unit, align 4, !tbaa !8
  %10 = bitcast i32* %top_right_count_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load i8, i8* %txsz.addr, align 1, !tbaa !10
  %idxprom4 = zext i8 %11 to i32
  %arrayidx5 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_unit, i32 0, i32 %idxprom4
  %12 = load i32, i32* %arrayidx5, align 4, !tbaa !8
  store i32 %12, i32* %top_right_count_unit, align 4, !tbaa !8
  %13 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %cmp6 = icmp sgt i32 %13, 0
  br i1 %cmp6, label %if.then8, label %if.else

if.then8:                                         ; preds = %cond.end
  %14 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom9 = zext i8 %14 to i32
  %arrayidx10 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom9
  %15 = load i8, i8* %arrayidx10, align 1, !tbaa !10
  %conv11 = zext i8 %15 to i32
  %16 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 12), align 4, !tbaa !10
  %conv12 = zext i8 %16 to i32
  %cmp13 = icmp sgt i32 %conv11, %conv12
  br i1 %cmp13, label %if.then15, label %if.end31

if.then15:                                        ; preds = %if.then8
  %17 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %18 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !10
  %conv16 = zext i8 %18 to i32
  %19 = load i32, i32* %ss_y.addr, align 4, !tbaa !8
  %shr17 = ashr i32 %conv16, %19
  %cmp18 = icmp eq i32 %17, %shr17
  br i1 %cmp18, label %land.lhs.true, label %if.end25

land.lhs.true:                                    ; preds = %if.then15
  %20 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %21 = load i32, i32* %top_right_count_unit, align 4, !tbaa !8
  %add = add nsw i32 %20, %21
  %22 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !10
  %conv20 = zext i8 %22 to i32
  %23 = load i32, i32* %ss_x.addr, align 4, !tbaa !8
  %shr21 = ashr i32 %conv20, %23
  %cmp22 = icmp eq i32 %add, %shr21
  br i1 %cmp22, label %if.then24, label %if.end25

if.then24:                                        ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup76

if.end25:                                         ; preds = %land.lhs.true, %if.then15
  %24 = bitcast i32* %plane_bw_unit_64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !10
  %conv26 = zext i8 %25 to i32
  %26 = load i32, i32* %ss_x.addr, align 4, !tbaa !8
  %shr27 = ashr i32 %conv26, %26
  store i32 %shr27, i32* %plane_bw_unit_64, align 4, !tbaa !8
  %27 = bitcast i32* %col_off_64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %29 = load i32, i32* %plane_bw_unit_64, align 4, !tbaa !8
  %rem = srem i32 %28, %29
  store i32 %rem, i32* %col_off_64, align 4, !tbaa !8
  %30 = load i32, i32* %col_off_64, align 4, !tbaa !8
  %31 = load i32, i32* %top_right_count_unit, align 4, !tbaa !8
  %add28 = add nsw i32 %30, %31
  %32 = load i32, i32* %plane_bw_unit_64, align 4, !tbaa !8
  %cmp29 = icmp slt i32 %add28, %32
  %conv30 = zext i1 %cmp29 to i32
  store i32 %conv30, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %col_off_64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast i32* %plane_bw_unit_64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  br label %cleanup76

if.end31:                                         ; preds = %if.then8
  %35 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %36 = load i32, i32* %top_right_count_unit, align 4, !tbaa !8
  %add32 = add nsw i32 %35, %36
  %37 = load i32, i32* %plane_bw_unit, align 4, !tbaa !8
  %cmp33 = icmp slt i32 %add32, %37
  %conv34 = zext i1 %cmp33 to i32
  store i32 %conv34, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup76

if.else:                                          ; preds = %cond.end
  %38 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %39 = load i32, i32* %top_right_count_unit, align 4, !tbaa !8
  %add35 = add nsw i32 %38, %39
  %40 = load i32, i32* %plane_bw_unit, align 4, !tbaa !8
  %cmp36 = icmp slt i32 %add35, %40
  br i1 %cmp36, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.else
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup76

if.end39:                                         ; preds = %if.else
  %41 = bitcast i32* %bw_in_mi_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  %42 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom40 = zext i8 %42 to i32
  %arrayidx41 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide_log2, i32 0, i32 %idxprom40
  %43 = load i8, i8* %arrayidx41, align 1, !tbaa !10
  %conv42 = zext i8 %43 to i32
  store i32 %conv42, i32* %bw_in_mi_log2, align 4, !tbaa !8
  %44 = bitcast i32* %bh_in_mi_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #6
  %45 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom43 = zext i8 %45 to i32
  %arrayidx44 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high_log2, i32 0, i32 %idxprom43
  %46 = load i8, i8* %arrayidx44, align 1, !tbaa !10
  %conv45 = zext i8 %46 to i32
  store i32 %conv45, i32* %bh_in_mi_log2, align 4, !tbaa !8
  %47 = bitcast i32* %sb_mi_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  %48 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %48, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 7
  %49 = load i8, i8* %sb_size, align 4, !tbaa !65
  %idxprom46 = zext i8 %49 to i32
  %arrayidx47 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom46
  %50 = load i8, i8* %arrayidx47, align 1, !tbaa !10
  %conv48 = zext i8 %50 to i32
  store i32 %conv48, i32* %sb_mi_size, align 4, !tbaa !8
  %51 = bitcast i32* %blk_row_in_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #6
  %52 = load i32, i32* %mi_row.addr, align 4, !tbaa !8
  %53 = load i32, i32* %sb_mi_size, align 4, !tbaa !8
  %sub = sub nsw i32 %53, 1
  %and = and i32 %52, %sub
  %54 = load i32, i32* %bh_in_mi_log2, align 4, !tbaa !8
  %shr49 = ashr i32 %and, %54
  store i32 %shr49, i32* %blk_row_in_sb, align 4, !tbaa !8
  %55 = bitcast i32* %blk_col_in_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #6
  %56 = load i32, i32* %mi_col.addr, align 4, !tbaa !8
  %57 = load i32, i32* %sb_mi_size, align 4, !tbaa !8
  %sub50 = sub nsw i32 %57, 1
  %and51 = and i32 %56, %sub50
  %58 = load i32, i32* %bw_in_mi_log2, align 4, !tbaa !8
  %shr52 = ashr i32 %and51, %58
  store i32 %shr52, i32* %blk_col_in_sb, align 4, !tbaa !8
  %59 = load i32, i32* %blk_row_in_sb, align 4, !tbaa !8
  %cmp53 = icmp eq i32 %59, 0
  br i1 %cmp53, label %if.then55, label %if.end56

if.then55:                                        ; preds = %if.end39
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end56:                                         ; preds = %if.end39
  %60 = load i32, i32* %blk_col_in_sb, align 4, !tbaa !8
  %add57 = add nsw i32 %60, 1
  %61 = load i32, i32* %bw_in_mi_log2, align 4, !tbaa !8
  %shl = shl i32 %add57, %61
  %62 = load i32, i32* %sb_mi_size, align 4, !tbaa !8
  %cmp58 = icmp sge i32 %shl, %62
  br i1 %cmp58, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end56
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end61:                                         ; preds = %if.end56
  %63 = bitcast i32* %this_blk_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #6
  %64 = load i32, i32* %blk_row_in_sb, align 4, !tbaa !8
  %add62 = add nsw i32 %64, 0
  %65 = load i32, i32* %bw_in_mi_log2, align 4, !tbaa !8
  %sub63 = sub nsw i32 5, %65
  %shl64 = shl i32 %add62, %sub63
  %66 = load i32, i32* %blk_col_in_sb, align 4, !tbaa !8
  %add65 = add nsw i32 %shl64, %66
  %add66 = add nsw i32 %add65, 0
  store i32 %add66, i32* %this_blk_index, align 4, !tbaa !8
  %67 = bitcast i32* %idx1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #6
  %68 = load i32, i32* %this_blk_index, align 4, !tbaa !8
  %div = sdiv i32 %68, 8
  store i32 %div, i32* %idx1, align 4, !tbaa !8
  %69 = bitcast i32* %idx2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #6
  %70 = load i32, i32* %this_blk_index, align 4, !tbaa !8
  %rem67 = srem i32 %70, 8
  store i32 %rem67, i32* %idx2, align 4, !tbaa !8
  %71 = bitcast i8** %has_tr_table to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  %72 = load i8, i8* %partition.addr, align 1, !tbaa !10
  %73 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %call = call i8* @get_has_tr_table(i8 zeroext %72, i8 zeroext %73)
  store i8* %call, i8** %has_tr_table, align 4, !tbaa !2
  %74 = load i8*, i8** %has_tr_table, align 4, !tbaa !2
  %75 = load i32, i32* %idx1, align 4, !tbaa !8
  %arrayidx68 = getelementptr inbounds i8, i8* %74, i32 %75
  %76 = load i8, i8* %arrayidx68, align 1, !tbaa !10
  %conv69 = zext i8 %76 to i32
  %77 = load i32, i32* %idx2, align 4, !tbaa !8
  %shr70 = ashr i32 %conv69, %77
  %and71 = and i32 %shr70, 1
  store i32 %and71, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %78 = bitcast i8** %has_tr_table to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #6
  %79 = bitcast i32* %idx2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #6
  %80 = bitcast i32* %idx1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #6
  %81 = bitcast i32* %this_blk_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end61, %if.then60, %if.then55
  %82 = bitcast i32* %blk_col_in_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #6
  %83 = bitcast i32* %blk_row_in_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #6
  %84 = bitcast i32* %sb_mi_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #6
  %85 = bitcast i32* %bh_in_mi_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  %86 = bitcast i32* %bw_in_mi_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #6
  br label %cleanup76

cleanup76:                                        ; preds = %cleanup, %if.then38, %if.end31, %if.end25, %if.then24
  %87 = bitcast i32* %top_right_count_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #6
  %88 = bitcast i32* %plane_bw_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #6
  %89 = bitcast i32* %bw_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #6
  br label %return

return:                                           ; preds = %cleanup76, %if.then
  %90 = load i32, i32* %retval, align 4
  ret i32 %90
}

; Function Attrs: nounwind
define internal i32 @has_bottom_left(%struct.AV1Common* %cm, i8 zeroext %bsize, i32 %mi_row, i32 %mi_col, i32 %bottom_available, i32 %left_available, i8 zeroext %partition, i8 zeroext %txsz, i32 %row_off, i32 %col_off, i32 %ss_x, i32 %ss_y) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %bsize.addr = alloca i8, align 1
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %bottom_available.addr = alloca i32, align 4
  %left_available.addr = alloca i32, align 4
  %partition.addr = alloca i8, align 1
  %txsz.addr = alloca i8, align 1
  %row_off.addr = alloca i32, align 4
  %col_off.addr = alloca i32, align 4
  %ss_x.addr = alloca i32, align 4
  %ss_y.addr = alloca i32, align 4
  %plane_bw_unit_64 = alloca i32, align 4
  %col_off_64 = alloca i32, align 4
  %plane_bh_unit_64 = alloca i32, align 4
  %row_off_64 = alloca i32, align 4
  %plane_bh_unit = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %bh_unit = alloca i32, align 4
  %plane_bh_unit37 = alloca i32, align 4
  %bottom_left_count_unit = alloca i32, align 4
  %bw_in_mi_log2 = alloca i32, align 4
  %bh_in_mi_log2 = alloca i32, align 4
  %sb_mi_size = alloca i32, align 4
  %blk_row_in_sb = alloca i32, align 4
  %blk_col_in_sb = alloca i32, align 4
  %blk_start_row_off = alloca i32, align 4
  %row_off_in_sb = alloca i32, align 4
  %sb_height_unit = alloca i32, align 4
  %this_blk_index = alloca i32, align 4
  %idx1 = alloca i32, align 4
  %idx2 = alloca i32, align 4
  %has_bl_table = alloca i8*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !10
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !8
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !8
  store i32 %bottom_available, i32* %bottom_available.addr, align 4, !tbaa !8
  store i32 %left_available, i32* %left_available.addr, align 4, !tbaa !8
  store i8 %partition, i8* %partition.addr, align 1, !tbaa !10
  store i8 %txsz, i8* %txsz.addr, align 1, !tbaa !10
  store i32 %row_off, i32* %row_off.addr, align 4, !tbaa !8
  store i32 %col_off, i32* %col_off.addr, align 4, !tbaa !8
  store i32 %ss_x, i32* %ss_x.addr, align 4, !tbaa !8
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !8
  %0 = load i32, i32* %bottom_available.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %left_available.addr, align 4, !tbaa !8
  %tobool1 = icmp ne i32 %1, 0
  br i1 %tobool1, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom = zext i8 %2 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %3 to i32
  %4 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 12), align 4, !tbaa !10
  %conv2 = zext i8 %4 to i32
  %cmp = icmp sgt i32 %conv, %conv2
  br i1 %cmp, label %land.lhs.true, label %if.end30

land.lhs.true:                                    ; preds = %if.end
  %5 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %cmp4 = icmp sgt i32 %5, 0
  br i1 %cmp4, label %if.then6, label %if.end30

if.then6:                                         ; preds = %land.lhs.true
  %6 = bitcast i32* %plane_bw_unit_64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !10
  %conv7 = zext i8 %7 to i32
  %8 = load i32, i32* %ss_x.addr, align 4, !tbaa !8
  %shr = ashr i32 %conv7, %8
  store i32 %shr, i32* %plane_bw_unit_64, align 4, !tbaa !8
  %9 = bitcast i32* %col_off_64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %11 = load i32, i32* %plane_bw_unit_64, align 4, !tbaa !8
  %rem = srem i32 %10, %11
  store i32 %rem, i32* %col_off_64, align 4, !tbaa !8
  %12 = load i32, i32* %col_off_64, align 4, !tbaa !8
  %cmp8 = icmp eq i32 %12, 0
  br i1 %cmp8, label %if.then10, label %if.end28

if.then10:                                        ; preds = %if.then6
  %13 = bitcast i32* %plane_bh_unit_64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !10
  %conv11 = zext i8 %14 to i32
  %15 = load i32, i32* %ss_y.addr, align 4, !tbaa !8
  %shr12 = ashr i32 %conv11, %15
  store i32 %shr12, i32* %plane_bh_unit_64, align 4, !tbaa !8
  %16 = bitcast i32* %row_off_64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %18 = load i32, i32* %plane_bh_unit_64, align 4, !tbaa !8
  %rem13 = srem i32 %17, %18
  store i32 %rem13, i32* %row_off_64, align 4, !tbaa !8
  %19 = bitcast i32* %plane_bh_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom14 = zext i8 %20 to i32
  %arrayidx15 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom14
  %21 = load i8, i8* %arrayidx15, align 1, !tbaa !10
  %conv16 = zext i8 %21 to i32
  %22 = load i32, i32* %ss_y.addr, align 4, !tbaa !8
  %shr17 = ashr i32 %conv16, %22
  %23 = load i32, i32* %plane_bh_unit_64, align 4, !tbaa !8
  %cmp18 = icmp slt i32 %shr17, %23
  br i1 %cmp18, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then10
  %24 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom20 = zext i8 %24 to i32
  %arrayidx21 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom20
  %25 = load i8, i8* %arrayidx21, align 1, !tbaa !10
  %conv22 = zext i8 %25 to i32
  %26 = load i32, i32* %ss_y.addr, align 4, !tbaa !8
  %shr23 = ashr i32 %conv22, %26
  br label %cond.end

cond.false:                                       ; preds = %if.then10
  %27 = load i32, i32* %plane_bh_unit_64, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shr23, %cond.true ], [ %27, %cond.false ]
  store i32 %cond, i32* %plane_bh_unit, align 4, !tbaa !8
  %28 = load i32, i32* %row_off_64, align 4, !tbaa !8
  %29 = load i8, i8* %txsz.addr, align 1, !tbaa !10
  %idxprom24 = zext i8 %29 to i32
  %arrayidx25 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_unit, i32 0, i32 %idxprom24
  %30 = load i32, i32* %arrayidx25, align 4, !tbaa !8
  %add = add nsw i32 %28, %30
  %31 = load i32, i32* %plane_bh_unit, align 4, !tbaa !8
  %cmp26 = icmp slt i32 %add, %31
  %conv27 = zext i1 %cmp26 to i32
  store i32 %conv27, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %32 = bitcast i32* %plane_bh_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  %33 = bitcast i32* %row_off_64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast i32* %plane_bh_unit_64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  br label %cleanup

if.end28:                                         ; preds = %if.then6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end28, %cond.end
  %35 = bitcast i32* %col_off_64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = bitcast i32* %plane_bw_unit_64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end30

if.end30:                                         ; preds = %cleanup.cont, %land.lhs.true, %if.end
  %37 = load i32, i32* %col_off.addr, align 4, !tbaa !8
  %cmp31 = icmp sgt i32 %37, 0
  br i1 %cmp31, label %if.then33, label %if.else

if.then33:                                        ; preds = %if.end30
  store i32 0, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %if.end30
  %38 = bitcast i32* %bh_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  %39 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom34 = zext i8 %39 to i32
  %arrayidx35 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom34
  %40 = load i8, i8* %arrayidx35, align 1, !tbaa !10
  %conv36 = zext i8 %40 to i32
  store i32 %conv36, i32* %bh_unit, align 4, !tbaa !8
  %41 = bitcast i32* %plane_bh_unit37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  %42 = load i32, i32* %bh_unit, align 4, !tbaa !8
  %43 = load i32, i32* %ss_y.addr, align 4, !tbaa !8
  %shr38 = ashr i32 %42, %43
  %cmp39 = icmp sgt i32 %shr38, 1
  br i1 %cmp39, label %cond.true41, label %cond.false43

cond.true41:                                      ; preds = %if.else
  %44 = load i32, i32* %bh_unit, align 4, !tbaa !8
  %45 = load i32, i32* %ss_y.addr, align 4, !tbaa !8
  %shr42 = ashr i32 %44, %45
  br label %cond.end44

cond.false43:                                     ; preds = %if.else
  br label %cond.end44

cond.end44:                                       ; preds = %cond.false43, %cond.true41
  %cond45 = phi i32 [ %shr42, %cond.true41 ], [ 1, %cond.false43 ]
  store i32 %cond45, i32* %plane_bh_unit37, align 4, !tbaa !8
  %46 = bitcast i32* %bottom_left_count_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #6
  %47 = load i8, i8* %txsz.addr, align 1, !tbaa !10
  %idxprom46 = zext i8 %47 to i32
  %arrayidx47 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_unit, i32 0, i32 %idxprom46
  %48 = load i32, i32* %arrayidx47, align 4, !tbaa !8
  store i32 %48, i32* %bottom_left_count_unit, align 4, !tbaa !8
  %49 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %50 = load i32, i32* %bottom_left_count_unit, align 4, !tbaa !8
  %add48 = add nsw i32 %49, %50
  %51 = load i32, i32* %plane_bh_unit37, align 4, !tbaa !8
  %cmp49 = icmp slt i32 %add48, %51
  br i1 %cmp49, label %if.then51, label %if.end52

if.then51:                                        ; preds = %cond.end44
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup106

if.end52:                                         ; preds = %cond.end44
  %52 = bitcast i32* %bw_in_mi_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom53 = zext i8 %53 to i32
  %arrayidx54 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide_log2, i32 0, i32 %idxprom53
  %54 = load i8, i8* %arrayidx54, align 1, !tbaa !10
  %conv55 = zext i8 %54 to i32
  store i32 %conv55, i32* %bw_in_mi_log2, align 4, !tbaa !8
  %55 = bitcast i32* %bh_in_mi_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #6
  %56 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom56 = zext i8 %56 to i32
  %arrayidx57 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high_log2, i32 0, i32 %idxprom56
  %57 = load i8, i8* %arrayidx57, align 1, !tbaa !10
  %conv58 = zext i8 %57 to i32
  store i32 %conv58, i32* %bh_in_mi_log2, align 4, !tbaa !8
  %58 = bitcast i32* %sb_mi_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #6
  %59 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %59, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 7
  %60 = load i8, i8* %sb_size, align 4, !tbaa !65
  %idxprom59 = zext i8 %60 to i32
  %arrayidx60 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom59
  %61 = load i8, i8* %arrayidx60, align 1, !tbaa !10
  %conv61 = zext i8 %61 to i32
  store i32 %conv61, i32* %sb_mi_size, align 4, !tbaa !8
  %62 = bitcast i32* %blk_row_in_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #6
  %63 = load i32, i32* %mi_row.addr, align 4, !tbaa !8
  %64 = load i32, i32* %sb_mi_size, align 4, !tbaa !8
  %sub = sub nsw i32 %64, 1
  %and = and i32 %63, %sub
  %65 = load i32, i32* %bh_in_mi_log2, align 4, !tbaa !8
  %shr62 = ashr i32 %and, %65
  store i32 %shr62, i32* %blk_row_in_sb, align 4, !tbaa !8
  %66 = bitcast i32* %blk_col_in_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  %67 = load i32, i32* %mi_col.addr, align 4, !tbaa !8
  %68 = load i32, i32* %sb_mi_size, align 4, !tbaa !8
  %sub63 = sub nsw i32 %68, 1
  %and64 = and i32 %67, %sub63
  %69 = load i32, i32* %bw_in_mi_log2, align 4, !tbaa !8
  %shr65 = ashr i32 %and64, %69
  store i32 %shr65, i32* %blk_col_in_sb, align 4, !tbaa !8
  %70 = load i32, i32* %blk_col_in_sb, align 4, !tbaa !8
  %cmp66 = icmp eq i32 %70, 0
  br i1 %cmp66, label %if.then68, label %if.end80

if.then68:                                        ; preds = %if.end52
  %71 = bitcast i32* %blk_start_row_off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  %72 = load i32, i32* %blk_row_in_sb, align 4, !tbaa !8
  %73 = load i32, i32* %bh_in_mi_log2, align 4, !tbaa !8
  %add69 = add nsw i32 %73, 2
  %sub70 = sub nsw i32 %add69, 2
  %shl = shl i32 %72, %sub70
  %74 = load i32, i32* %ss_y.addr, align 4, !tbaa !8
  %shr71 = ashr i32 %shl, %74
  store i32 %shr71, i32* %blk_start_row_off, align 4, !tbaa !8
  %75 = bitcast i32* %row_off_in_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #6
  %76 = load i32, i32* %blk_start_row_off, align 4, !tbaa !8
  %77 = load i32, i32* %row_off.addr, align 4, !tbaa !8
  %add72 = add nsw i32 %76, %77
  store i32 %add72, i32* %row_off_in_sb, align 4, !tbaa !8
  %78 = bitcast i32* %sb_height_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #6
  %79 = load i32, i32* %sb_mi_size, align 4, !tbaa !8
  %80 = load i32, i32* %ss_y.addr, align 4, !tbaa !8
  %shr73 = ashr i32 %79, %80
  store i32 %shr73, i32* %sb_height_unit, align 4, !tbaa !8
  %81 = load i32, i32* %row_off_in_sb, align 4, !tbaa !8
  %82 = load i32, i32* %bottom_left_count_unit, align 4, !tbaa !8
  %add74 = add nsw i32 %81, %82
  %83 = load i32, i32* %sb_height_unit, align 4, !tbaa !8
  %cmp75 = icmp slt i32 %add74, %83
  %conv76 = zext i1 %cmp75 to i32
  store i32 %conv76, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %84 = bitcast i32* %sb_height_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #6
  %85 = bitcast i32* %row_off_in_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  %86 = bitcast i32* %blk_start_row_off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #6
  br label %cleanup101

if.end80:                                         ; preds = %if.end52
  %87 = load i32, i32* %blk_row_in_sb, align 4, !tbaa !8
  %add81 = add nsw i32 %87, 1
  %88 = load i32, i32* %bh_in_mi_log2, align 4, !tbaa !8
  %shl82 = shl i32 %add81, %88
  %89 = load i32, i32* %sb_mi_size, align 4, !tbaa !8
  %cmp83 = icmp sge i32 %shl82, %89
  br i1 %cmp83, label %if.then85, label %if.end86

if.then85:                                        ; preds = %if.end80
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

if.end86:                                         ; preds = %if.end80
  %90 = bitcast i32* %this_blk_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  %91 = load i32, i32* %blk_row_in_sb, align 4, !tbaa !8
  %add87 = add nsw i32 %91, 0
  %92 = load i32, i32* %bw_in_mi_log2, align 4, !tbaa !8
  %sub88 = sub nsw i32 5, %92
  %shl89 = shl i32 %add87, %sub88
  %93 = load i32, i32* %blk_col_in_sb, align 4, !tbaa !8
  %add90 = add nsw i32 %shl89, %93
  %add91 = add nsw i32 %add90, 0
  store i32 %add91, i32* %this_blk_index, align 4, !tbaa !8
  %94 = bitcast i32* %idx1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #6
  %95 = load i32, i32* %this_blk_index, align 4, !tbaa !8
  %div = sdiv i32 %95, 8
  store i32 %div, i32* %idx1, align 4, !tbaa !8
  %96 = bitcast i32* %idx2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #6
  %97 = load i32, i32* %this_blk_index, align 4, !tbaa !8
  %rem92 = srem i32 %97, 8
  store i32 %rem92, i32* %idx2, align 4, !tbaa !8
  %98 = bitcast i8** %has_bl_table to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #6
  %99 = load i8, i8* %partition.addr, align 1, !tbaa !10
  %100 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %call = call i8* @get_has_bl_table(i8 zeroext %99, i8 zeroext %100)
  store i8* %call, i8** %has_bl_table, align 4, !tbaa !2
  %101 = load i8*, i8** %has_bl_table, align 4, !tbaa !2
  %102 = load i32, i32* %idx1, align 4, !tbaa !8
  %arrayidx93 = getelementptr inbounds i8, i8* %101, i32 %102
  %103 = load i8, i8* %arrayidx93, align 1, !tbaa !10
  %conv94 = zext i8 %103 to i32
  %104 = load i32, i32* %idx2, align 4, !tbaa !8
  %shr95 = ashr i32 %conv94, %104
  %and96 = and i32 %shr95, 1
  store i32 %and96, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %105 = bitcast i8** %has_bl_table to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #6
  %106 = bitcast i32* %idx2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #6
  %107 = bitcast i32* %idx1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #6
  %108 = bitcast i32* %this_blk_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #6
  br label %cleanup101

cleanup101:                                       ; preds = %if.end86, %if.then85, %if.then68
  %109 = bitcast i32* %blk_col_in_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #6
  %110 = bitcast i32* %blk_row_in_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast i32* %sb_mi_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  %112 = bitcast i32* %bh_in_mi_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #6
  %113 = bitcast i32* %bw_in_mi_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #6
  br label %cleanup106

cleanup106:                                       ; preds = %cleanup101, %if.then51
  %114 = bitcast i32* %bottom_left_count_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #6
  %115 = bitcast i32* %plane_bh_unit37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #6
  %116 = bitcast i32* %bh_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #6
  br label %return

return:                                           ; preds = %cleanup106, %if.then33, %cleanup, %if.then
  %117 = load i32, i32* %retval, align 4
  ret i32 %117

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @build_intra_predictors_high(%struct.macroblockd* %xd, i8* %ref8, i32 %ref_stride, i8* %dst8, i32 %dst_stride, i8 zeroext %mode, i32 %angle_delta, i8 zeroext %filter_intra_mode, i8 zeroext %tx_size, i32 %disable_edge_filter, i32 %n_top_px, i32 %n_topright_px, i32 %n_left_px, i32 %n_bottomleft_px, i32 %plane) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref8.addr = alloca i8*, align 4
  %ref_stride.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %mode.addr = alloca i8, align 1
  %angle_delta.addr = alloca i32, align 4
  %filter_intra_mode.addr = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %disable_edge_filter.addr = alloca i32, align 4
  %n_top_px.addr = alloca i32, align 4
  %n_topright_px.addr = alloca i32, align 4
  %n_left_px.addr = alloca i32, align 4
  %n_bottomleft_px.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %dst = alloca i16*, align 4
  %ref = alloca i16*, align 4
  %left_data = alloca [160 x i16], align 16
  %above_data = alloca [160 x i16], align 16
  %above_row = alloca i16*, align 4
  %left_col = alloca i16*, align 4
  %txwpx = alloca i32, align 4
  %txhpx = alloca i32, align 4
  %need_left = alloca i32, align 4
  %need_above = alloca i32, align 4
  %need_above_left = alloca i32, align 4
  %above_ref = alloca i16*, align 4
  %left_ref = alloca i16*, align 4
  %p_angle = alloca i32, align 4
  %is_dr_mode = alloca i32, align 4
  %use_filter_intra = alloca i32, align 4
  %base = alloca i32, align 4
  %val = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %need_bottom = alloca i32, align 4
  %num_left_pixels_needed = alloca i32, align 4
  %need_right = alloca i32, align 4
  %num_top_pixels_needed = alloca i32, align 4
  %upsample_above = alloca i32, align 4
  %upsample_left = alloca i32, align 4
  %need_right236 = alloca i32, align 4
  %need_bottom239 = alloca i32, align 4
  %filt_type = alloca i32, align 4
  %ab_le = alloca i32, align 4
  %strength = alloca i32, align 4
  %n_px = alloca i32, align 4
  %strength282 = alloca i32, align 4
  %n_px285 = alloca i32, align 4
  %n_px303 = alloca i32, align 4
  %n_px318 = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8* %ref8, i8** %ref8.addr, align 4, !tbaa !2
  store i32 %ref_stride, i32* %ref_stride.addr, align 4, !tbaa !8
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !8
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !10
  store i32 %angle_delta, i32* %angle_delta.addr, align 4, !tbaa !8
  store i8 %filter_intra_mode, i8* %filter_intra_mode.addr, align 1, !tbaa !10
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i32 %disable_edge_filter, i32* %disable_edge_filter.addr, align 4, !tbaa !8
  store i32 %n_top_px, i32* %n_top_px.addr, align 4, !tbaa !8
  store i32 %n_topright_px, i32* %n_topright_px.addr, align 4, !tbaa !8
  store i32 %n_left_px, i32* %n_left_px.addr, align 4, !tbaa !8
  store i32 %n_bottomleft_px, i32* %n_bottomleft_px.addr, align 4, !tbaa !8
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !8
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %3 = ptrtoint i8* %2 to i32
  %shl = shl i32 %3, 1
  %4 = inttoptr i32 %shl to i16*
  store i16* %4, i16** %dst, align 4, !tbaa !2
  %5 = bitcast i16** %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i8*, i8** %ref8.addr, align 4, !tbaa !2
  %7 = ptrtoint i8* %6 to i32
  %shl1 = shl i32 %7, 1
  %8 = inttoptr i32 %shl1 to i16*
  store i16* %8, i16** %ref, align 4, !tbaa !2
  %9 = bitcast [160 x i16]* %left_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 320, i8* %9) #6
  %10 = bitcast [160 x i16]* %above_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 320, i8* %10) #6
  %11 = bitcast i16** %above_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %arraydecay = getelementptr inbounds [160 x i16], [160 x i16]* %above_data, i32 0, i32 0
  %add.ptr = getelementptr inbounds i16, i16* %arraydecay, i32 16
  store i16* %add.ptr, i16** %above_row, align 4, !tbaa !2
  %12 = bitcast i16** %left_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %arraydecay2 = getelementptr inbounds [160 x i16], [160 x i16]* %left_data, i32 0, i32 0
  %add.ptr3 = getelementptr inbounds i16, i16* %arraydecay2, i32 16
  store i16* %add.ptr3, i16** %left_col, align 4, !tbaa !2
  %13 = bitcast i32* %txwpx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %14 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %15 = load i32, i32* %arrayidx, align 4, !tbaa !8
  store i32 %15, i32* %txwpx, align 4, !tbaa !8
  %16 = bitcast i32* %txhpx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom4 = zext i8 %17 to i32
  %arrayidx5 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom4
  %18 = load i32, i32* %arrayidx5, align 4, !tbaa !8
  store i32 %18, i32* %txhpx, align 4, !tbaa !8
  %19 = bitcast i32* %need_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom6 = zext i8 %20 to i32
  %arrayidx7 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom6
  %21 = load i8, i8* %arrayidx7, align 1, !tbaa !10
  %conv = zext i8 %21 to i32
  %and = and i32 %conv, 2
  store i32 %and, i32* %need_left, align 4, !tbaa !8
  %22 = bitcast i32* %need_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom8 = zext i8 %23 to i32
  %arrayidx9 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom8
  %24 = load i8, i8* %arrayidx9, align 1, !tbaa !10
  %conv10 = zext i8 %24 to i32
  %and11 = and i32 %conv10, 4
  store i32 %and11, i32* %need_above, align 4, !tbaa !8
  %25 = bitcast i32* %need_above_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom12 = zext i8 %26 to i32
  %arrayidx13 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom12
  %27 = load i8, i8* %arrayidx13, align 1, !tbaa !10
  %conv14 = zext i8 %27 to i32
  %and15 = and i32 %conv14, 16
  store i32 %and15, i32* %need_above_left, align 4, !tbaa !8
  %28 = bitcast i16** %above_ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %29 = load i16*, i16** %ref, align 4, !tbaa !2
  %30 = load i32, i32* %ref_stride.addr, align 4, !tbaa !8
  %idx.neg = sub i32 0, %30
  %add.ptr16 = getelementptr inbounds i16, i16* %29, i32 %idx.neg
  store i16* %add.ptr16, i16** %above_ref, align 4, !tbaa !2
  %31 = bitcast i16** %left_ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load i16*, i16** %ref, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds i16, i16* %32, i32 -1
  store i16* %add.ptr17, i16** %left_ref, align 4, !tbaa !2
  %33 = bitcast i32* %p_angle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  store i32 0, i32* %p_angle, align 4, !tbaa !8
  %34 = bitcast i32* %is_dr_mode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %35 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %call = call i32 @av1_is_directional_mode(i8 zeroext %35)
  store i32 %call, i32* %is_dr_mode, align 4, !tbaa !8
  %36 = bitcast i32* %use_filter_intra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  %37 = load i8, i8* %filter_intra_mode.addr, align 1, !tbaa !10
  %conv18 = zext i8 %37 to i32
  %cmp = icmp ne i32 %conv18, 5
  %conv19 = zext i1 %cmp to i32
  store i32 %conv19, i32* %use_filter_intra, align 4, !tbaa !8
  %38 = bitcast i32* %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  %39 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %39, i32 0, i32 41
  %40 = load i32, i32* %bd, align 4, !tbaa !66
  %sub = sub nsw i32 %40, 8
  %shl20 = shl i32 128, %sub
  store i32 %shl20, i32* %base, align 4, !tbaa !8
  %41 = load i32, i32* %is_dr_mode, align 4, !tbaa !8
  %tobool = icmp ne i32 %41, 0
  br i1 %tobool, label %if.then, label %if.end32

if.then:                                          ; preds = %entry
  %42 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom21 = zext i8 %42 to i32
  %arrayidx22 = getelementptr inbounds [13 x i8], [13 x i8]* @mode_to_angle_map, i32 0, i32 %idxprom21
  %43 = load i8, i8* %arrayidx22, align 1, !tbaa !10
  %conv23 = zext i8 %43 to i32
  %44 = load i32, i32* %angle_delta.addr, align 4, !tbaa !8
  %add = add nsw i32 %conv23, %44
  store i32 %add, i32* %p_angle, align 4, !tbaa !8
  %45 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp24 = icmp sle i32 %45, 90
  br i1 %cmp24, label %if.then26, label %if.else

if.then26:                                        ; preds = %if.then
  store i32 1, i32* %need_above, align 4, !tbaa !8
  store i32 0, i32* %need_left, align 4, !tbaa !8
  store i32 1, i32* %need_above_left, align 4, !tbaa !8
  br label %if.end31

if.else:                                          ; preds = %if.then
  %46 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp27 = icmp slt i32 %46, 180
  br i1 %cmp27, label %if.then29, label %if.else30

if.then29:                                        ; preds = %if.else
  store i32 1, i32* %need_above, align 4, !tbaa !8
  store i32 1, i32* %need_left, align 4, !tbaa !8
  store i32 1, i32* %need_above_left, align 4, !tbaa !8
  br label %if.end

if.else30:                                        ; preds = %if.else
  store i32 0, i32* %need_above, align 4, !tbaa !8
  store i32 1, i32* %need_left, align 4, !tbaa !8
  store i32 1, i32* %need_above_left, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else30, %if.then29
  br label %if.end31

if.end31:                                         ; preds = %if.end, %if.then26
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %entry
  %47 = load i32, i32* %use_filter_intra, align 4, !tbaa !8
  %tobool33 = icmp ne i32 %47, 0
  br i1 %tobool33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end32
  store i32 1, i32* %need_above_left, align 4, !tbaa !8
  store i32 1, i32* %need_above, align 4, !tbaa !8
  store i32 1, i32* %need_left, align 4, !tbaa !8
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.end32
  %48 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool36 = icmp ne i32 %48, 0
  br i1 %tobool36, label %lor.lhs.false, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end35
  %49 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp37 = icmp eq i32 %49, 0
  br i1 %cmp37, label %if.then43, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %if.end35
  %50 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool39 = icmp ne i32 %50, 0
  br i1 %tobool39, label %if.end66, label %land.lhs.true40

land.lhs.true40:                                  ; preds = %lor.lhs.false
  %51 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp41 = icmp eq i32 %51, 0
  br i1 %cmp41, label %if.then43, label %if.end66

if.then43:                                        ; preds = %land.lhs.true40, %land.lhs.true
  %52 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool44 = icmp ne i32 %53, 0
  br i1 %tobool44, label %if.then45, label %if.else51

if.then45:                                        ; preds = %if.then43
  %54 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp46 = icmp sgt i32 %54, 0
  br i1 %cmp46, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then45
  %55 = load i16*, i16** %above_ref, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i16, i16* %55, i32 0
  %56 = load i16, i16* %arrayidx48, align 2, !tbaa !11
  %conv49 = zext i16 %56 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.then45
  %57 = load i32, i32* %base, align 4, !tbaa !8
  %add50 = add nsw i32 %57, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv49, %cond.true ], [ %add50, %cond.false ]
  store i32 %cond, i32* %val, align 4, !tbaa !8
  br label %if.end61

if.else51:                                        ; preds = %if.then43
  %58 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp52 = icmp sgt i32 %58, 0
  br i1 %cmp52, label %cond.true54, label %cond.false57

cond.true54:                                      ; preds = %if.else51
  %59 = load i16*, i16** %left_ref, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i16, i16* %59, i32 0
  %60 = load i16, i16* %arrayidx55, align 2, !tbaa !11
  %conv56 = zext i16 %60 to i32
  br label %cond.end59

cond.false57:                                     ; preds = %if.else51
  %61 = load i32, i32* %base, align 4, !tbaa !8
  %sub58 = sub nsw i32 %61, 1
  br label %cond.end59

cond.end59:                                       ; preds = %cond.false57, %cond.true54
  %cond60 = phi i32 [ %conv56, %cond.true54 ], [ %sub58, %cond.false57 ]
  store i32 %cond60, i32* %val, align 4, !tbaa !8
  br label %if.end61

if.end61:                                         ; preds = %cond.end59, %cond.end
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end61
  %62 = load i32, i32* %i, align 4, !tbaa !8
  %63 = load i32, i32* %txhpx, align 4, !tbaa !8
  %cmp62 = icmp slt i32 %62, %63
  br i1 %cmp62, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %64 = load i16*, i16** %dst, align 4, !tbaa !2
  %65 = bitcast i16* %64 to i8*
  %66 = load i32, i32* %val, align 4, !tbaa !8
  %67 = load i32, i32* %txwpx, align 4, !tbaa !8
  %call64 = call i8* @aom_memset16(i8* %65, i32 %66, i32 %67)
  %68 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %69 = load i16*, i16** %dst, align 4, !tbaa !2
  %add.ptr65 = getelementptr inbounds i16, i16* %69, i32 %68
  store i16* %add.ptr65, i16** %dst, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %70 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %70, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  %71 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  br label %cleanup

if.end66:                                         ; preds = %land.lhs.true40, %lor.lhs.false
  %72 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool67 = icmp ne i32 %72, 0
  br i1 %tobool67, label %if.then68, label %if.end138

if.then68:                                        ; preds = %if.end66
  %73 = bitcast i32* %need_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  %74 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom69 = zext i8 %74 to i32
  %arrayidx70 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom69
  %75 = load i8, i8* %arrayidx70, align 1, !tbaa !10
  %conv71 = zext i8 %75 to i32
  %and72 = and i32 %conv71, 32
  store i32 %and72, i32* %need_bottom, align 4, !tbaa !8
  %76 = load i32, i32* %use_filter_intra, align 4, !tbaa !8
  %tobool73 = icmp ne i32 %76, 0
  br i1 %tobool73, label %if.then74, label %if.end75

if.then74:                                        ; preds = %if.then68
  store i32 0, i32* %need_bottom, align 4, !tbaa !8
  br label %if.end75

if.end75:                                         ; preds = %if.then74, %if.then68
  %77 = load i32, i32* %is_dr_mode, align 4, !tbaa !8
  %tobool76 = icmp ne i32 %77, 0
  br i1 %tobool76, label %if.then77, label %if.end80

if.then77:                                        ; preds = %if.end75
  %78 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp78 = icmp sgt i32 %78, 180
  %conv79 = zext i1 %cmp78 to i32
  store i32 %conv79, i32* %need_bottom, align 4, !tbaa !8
  br label %if.end80

if.end80:                                         ; preds = %if.then77, %if.end75
  %79 = bitcast i32* %num_left_pixels_needed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #6
  %80 = load i32, i32* %txhpx, align 4, !tbaa !8
  %81 = load i32, i32* %need_bottom, align 4, !tbaa !8
  %tobool81 = icmp ne i32 %81, 0
  br i1 %tobool81, label %cond.true82, label %cond.false83

cond.true82:                                      ; preds = %if.end80
  %82 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end84

cond.false83:                                     ; preds = %if.end80
  br label %cond.end84

cond.end84:                                       ; preds = %cond.false83, %cond.true82
  %cond85 = phi i32 [ %82, %cond.true82 ], [ 0, %cond.false83 ]
  %add86 = add nsw i32 %80, %cond85
  store i32 %add86, i32* %num_left_pixels_needed, align 4, !tbaa !8
  store i32 0, i32* %i, align 4, !tbaa !8
  %83 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp87 = icmp sgt i32 %83, 0
  br i1 %cmp87, label %if.then89, label %if.else126

if.then89:                                        ; preds = %cond.end84
  br label %for.cond90

for.cond90:                                       ; preds = %for.inc96, %if.then89
  %84 = load i32, i32* %i, align 4, !tbaa !8
  %85 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp91 = icmp slt i32 %84, %85
  br i1 %cmp91, label %for.body93, label %for.end98

for.body93:                                       ; preds = %for.cond90
  %86 = load i16*, i16** %left_ref, align 4, !tbaa !2
  %87 = load i32, i32* %i, align 4, !tbaa !8
  %88 = load i32, i32* %ref_stride.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %87, %88
  %arrayidx94 = getelementptr inbounds i16, i16* %86, i32 %mul
  %89 = load i16, i16* %arrayidx94, align 2, !tbaa !11
  %90 = load i16*, i16** %left_col, align 4, !tbaa !2
  %91 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx95 = getelementptr inbounds i16, i16* %90, i32 %91
  store i16 %89, i16* %arrayidx95, align 2, !tbaa !11
  br label %for.inc96

for.inc96:                                        ; preds = %for.body93
  %92 = load i32, i32* %i, align 4, !tbaa !8
  %inc97 = add nsw i32 %92, 1
  store i32 %inc97, i32* %i, align 4, !tbaa !8
  br label %for.cond90

for.end98:                                        ; preds = %for.cond90
  %93 = load i32, i32* %need_bottom, align 4, !tbaa !8
  %tobool99 = icmp ne i32 %93, 0
  br i1 %tobool99, label %land.lhs.true100, label %if.end115

land.lhs.true100:                                 ; preds = %for.end98
  %94 = load i32, i32* %n_bottomleft_px.addr, align 4, !tbaa !8
  %cmp101 = icmp sgt i32 %94, 0
  br i1 %cmp101, label %if.then103, label %if.end115

if.then103:                                       ; preds = %land.lhs.true100
  br label %for.cond104

for.cond104:                                      ; preds = %for.inc112, %if.then103
  %95 = load i32, i32* %i, align 4, !tbaa !8
  %96 = load i32, i32* %txhpx, align 4, !tbaa !8
  %97 = load i32, i32* %n_bottomleft_px.addr, align 4, !tbaa !8
  %add105 = add nsw i32 %96, %97
  %cmp106 = icmp slt i32 %95, %add105
  br i1 %cmp106, label %for.body108, label %for.end114

for.body108:                                      ; preds = %for.cond104
  %98 = load i16*, i16** %left_ref, align 4, !tbaa !2
  %99 = load i32, i32* %i, align 4, !tbaa !8
  %100 = load i32, i32* %ref_stride.addr, align 4, !tbaa !8
  %mul109 = mul nsw i32 %99, %100
  %arrayidx110 = getelementptr inbounds i16, i16* %98, i32 %mul109
  %101 = load i16, i16* %arrayidx110, align 2, !tbaa !11
  %102 = load i16*, i16** %left_col, align 4, !tbaa !2
  %103 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx111 = getelementptr inbounds i16, i16* %102, i32 %103
  store i16 %101, i16* %arrayidx111, align 2, !tbaa !11
  br label %for.inc112

for.inc112:                                       ; preds = %for.body108
  %104 = load i32, i32* %i, align 4, !tbaa !8
  %inc113 = add nsw i32 %104, 1
  store i32 %inc113, i32* %i, align 4, !tbaa !8
  br label %for.cond104

for.end114:                                       ; preds = %for.cond104
  br label %if.end115

if.end115:                                        ; preds = %for.end114, %land.lhs.true100, %for.end98
  %105 = load i32, i32* %i, align 4, !tbaa !8
  %106 = load i32, i32* %num_left_pixels_needed, align 4, !tbaa !8
  %cmp116 = icmp slt i32 %105, %106
  br i1 %cmp116, label %if.then118, label %if.end125

if.then118:                                       ; preds = %if.end115
  %107 = load i16*, i16** %left_col, align 4, !tbaa !2
  %108 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx119 = getelementptr inbounds i16, i16* %107, i32 %108
  %109 = bitcast i16* %arrayidx119 to i8*
  %110 = load i16*, i16** %left_col, align 4, !tbaa !2
  %111 = load i32, i32* %i, align 4, !tbaa !8
  %sub120 = sub nsw i32 %111, 1
  %arrayidx121 = getelementptr inbounds i16, i16* %110, i32 %sub120
  %112 = load i16, i16* %arrayidx121, align 2, !tbaa !11
  %conv122 = zext i16 %112 to i32
  %113 = load i32, i32* %num_left_pixels_needed, align 4, !tbaa !8
  %114 = load i32, i32* %i, align 4, !tbaa !8
  %sub123 = sub nsw i32 %113, %114
  %call124 = call i8* @aom_memset16(i8* %109, i32 %conv122, i32 %sub123)
  br label %if.end125

if.end125:                                        ; preds = %if.then118, %if.end115
  br label %if.end137

if.else126:                                       ; preds = %cond.end84
  %115 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp127 = icmp sgt i32 %115, 0
  br i1 %cmp127, label %if.then129, label %if.else133

if.then129:                                       ; preds = %if.else126
  %116 = load i16*, i16** %left_col, align 4, !tbaa !2
  %117 = bitcast i16* %116 to i8*
  %118 = load i16*, i16** %above_ref, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds i16, i16* %118, i32 0
  %119 = load i16, i16* %arrayidx130, align 2, !tbaa !11
  %conv131 = zext i16 %119 to i32
  %120 = load i32, i32* %num_left_pixels_needed, align 4, !tbaa !8
  %call132 = call i8* @aom_memset16(i8* %117, i32 %conv131, i32 %120)
  br label %if.end136

if.else133:                                       ; preds = %if.else126
  %121 = load i16*, i16** %left_col, align 4, !tbaa !2
  %122 = bitcast i16* %121 to i8*
  %123 = load i32, i32* %base, align 4, !tbaa !8
  %add134 = add nsw i32 %123, 1
  %124 = load i32, i32* %num_left_pixels_needed, align 4, !tbaa !8
  %call135 = call i8* @aom_memset16(i8* %122, i32 %add134, i32 %124)
  br label %if.end136

if.end136:                                        ; preds = %if.else133, %if.then129
  br label %if.end137

if.end137:                                        ; preds = %if.end136, %if.end125
  %125 = bitcast i32* %num_left_pixels_needed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #6
  %126 = bitcast i32* %need_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #6
  br label %if.end138

if.end138:                                        ; preds = %if.end137, %if.end66
  %127 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool139 = icmp ne i32 %127, 0
  br i1 %tobool139, label %if.then140, label %if.end195

if.then140:                                       ; preds = %if.end138
  %128 = bitcast i32* %need_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #6
  %129 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom141 = zext i8 %129 to i32
  %arrayidx142 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom141
  %130 = load i8, i8* %arrayidx142, align 1, !tbaa !10
  %conv143 = zext i8 %130 to i32
  %and144 = and i32 %conv143, 8
  store i32 %and144, i32* %need_right, align 4, !tbaa !8
  %131 = load i32, i32* %use_filter_intra, align 4, !tbaa !8
  %tobool145 = icmp ne i32 %131, 0
  br i1 %tobool145, label %if.then146, label %if.end147

if.then146:                                       ; preds = %if.then140
  store i32 0, i32* %need_right, align 4, !tbaa !8
  br label %if.end147

if.end147:                                        ; preds = %if.then146, %if.then140
  %132 = load i32, i32* %is_dr_mode, align 4, !tbaa !8
  %tobool148 = icmp ne i32 %132, 0
  br i1 %tobool148, label %if.then149, label %if.end152

if.then149:                                       ; preds = %if.end147
  %133 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp150 = icmp slt i32 %133, 90
  %conv151 = zext i1 %cmp150 to i32
  store i32 %conv151, i32* %need_right, align 4, !tbaa !8
  br label %if.end152

if.end152:                                        ; preds = %if.then149, %if.end147
  %134 = bitcast i32* %num_top_pixels_needed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %134) #6
  %135 = load i32, i32* %txwpx, align 4, !tbaa !8
  %136 = load i32, i32* %need_right, align 4, !tbaa !8
  %tobool153 = icmp ne i32 %136, 0
  br i1 %tobool153, label %cond.true154, label %cond.false155

cond.true154:                                     ; preds = %if.end152
  %137 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end156

cond.false155:                                    ; preds = %if.end152
  br label %cond.end156

cond.end156:                                      ; preds = %cond.false155, %cond.true154
  %cond157 = phi i32 [ %137, %cond.true154 ], [ 0, %cond.false155 ]
  %add158 = add nsw i32 %135, %cond157
  store i32 %add158, i32* %num_top_pixels_needed, align 4, !tbaa !8
  %138 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp159 = icmp sgt i32 %138, 0
  br i1 %cmp159, label %if.then161, label %if.else183

if.then161:                                       ; preds = %cond.end156
  %139 = load i16*, i16** %above_row, align 4, !tbaa !2
  %140 = bitcast i16* %139 to i8*
  %141 = load i16*, i16** %above_ref, align 4, !tbaa !2
  %142 = bitcast i16* %141 to i8*
  %143 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %mul162 = mul i32 %143, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %140, i8* align 2 %142, i32 %mul162, i1 false)
  %144 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  store i32 %144, i32* %i, align 4, !tbaa !8
  %145 = load i32, i32* %need_right, align 4, !tbaa !8
  %tobool163 = icmp ne i32 %145, 0
  br i1 %tobool163, label %land.lhs.true164, label %if.end172

land.lhs.true164:                                 ; preds = %if.then161
  %146 = load i32, i32* %n_topright_px.addr, align 4, !tbaa !8
  %cmp165 = icmp sgt i32 %146, 0
  br i1 %cmp165, label %if.then167, label %if.end172

if.then167:                                       ; preds = %land.lhs.true164
  %147 = load i16*, i16** %above_row, align 4, !tbaa !2
  %148 = load i32, i32* %txwpx, align 4, !tbaa !8
  %add.ptr168 = getelementptr inbounds i16, i16* %147, i32 %148
  %149 = bitcast i16* %add.ptr168 to i8*
  %150 = load i16*, i16** %above_ref, align 4, !tbaa !2
  %151 = load i32, i32* %txwpx, align 4, !tbaa !8
  %add.ptr169 = getelementptr inbounds i16, i16* %150, i32 %151
  %152 = bitcast i16* %add.ptr169 to i8*
  %153 = load i32, i32* %n_topright_px.addr, align 4, !tbaa !8
  %mul170 = mul i32 %153, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %149, i8* align 2 %152, i32 %mul170, i1 false)
  %154 = load i32, i32* %n_topright_px.addr, align 4, !tbaa !8
  %155 = load i32, i32* %i, align 4, !tbaa !8
  %add171 = add nsw i32 %155, %154
  store i32 %add171, i32* %i, align 4, !tbaa !8
  br label %if.end172

if.end172:                                        ; preds = %if.then167, %land.lhs.true164, %if.then161
  %156 = load i32, i32* %i, align 4, !tbaa !8
  %157 = load i32, i32* %num_top_pixels_needed, align 4, !tbaa !8
  %cmp173 = icmp slt i32 %156, %157
  br i1 %cmp173, label %if.then175, label %if.end182

if.then175:                                       ; preds = %if.end172
  %158 = load i16*, i16** %above_row, align 4, !tbaa !2
  %159 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx176 = getelementptr inbounds i16, i16* %158, i32 %159
  %160 = bitcast i16* %arrayidx176 to i8*
  %161 = load i16*, i16** %above_row, align 4, !tbaa !2
  %162 = load i32, i32* %i, align 4, !tbaa !8
  %sub177 = sub nsw i32 %162, 1
  %arrayidx178 = getelementptr inbounds i16, i16* %161, i32 %sub177
  %163 = load i16, i16* %arrayidx178, align 2, !tbaa !11
  %conv179 = zext i16 %163 to i32
  %164 = load i32, i32* %num_top_pixels_needed, align 4, !tbaa !8
  %165 = load i32, i32* %i, align 4, !tbaa !8
  %sub180 = sub nsw i32 %164, %165
  %call181 = call i8* @aom_memset16(i8* %160, i32 %conv179, i32 %sub180)
  br label %if.end182

if.end182:                                        ; preds = %if.then175, %if.end172
  br label %if.end194

if.else183:                                       ; preds = %cond.end156
  %166 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp184 = icmp sgt i32 %166, 0
  br i1 %cmp184, label %if.then186, label %if.else190

if.then186:                                       ; preds = %if.else183
  %167 = load i16*, i16** %above_row, align 4, !tbaa !2
  %168 = bitcast i16* %167 to i8*
  %169 = load i16*, i16** %left_ref, align 4, !tbaa !2
  %arrayidx187 = getelementptr inbounds i16, i16* %169, i32 0
  %170 = load i16, i16* %arrayidx187, align 2, !tbaa !11
  %conv188 = zext i16 %170 to i32
  %171 = load i32, i32* %num_top_pixels_needed, align 4, !tbaa !8
  %call189 = call i8* @aom_memset16(i8* %168, i32 %conv188, i32 %171)
  br label %if.end193

if.else190:                                       ; preds = %if.else183
  %172 = load i16*, i16** %above_row, align 4, !tbaa !2
  %173 = bitcast i16* %172 to i8*
  %174 = load i32, i32* %base, align 4, !tbaa !8
  %sub191 = sub nsw i32 %174, 1
  %175 = load i32, i32* %num_top_pixels_needed, align 4, !tbaa !8
  %call192 = call i8* @aom_memset16(i8* %173, i32 %sub191, i32 %175)
  br label %if.end193

if.end193:                                        ; preds = %if.else190, %if.then186
  br label %if.end194

if.end194:                                        ; preds = %if.end193, %if.end182
  %176 = bitcast i32* %num_top_pixels_needed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #6
  %177 = bitcast i32* %need_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #6
  br label %if.end195

if.end195:                                        ; preds = %if.end194, %if.end138
  %178 = load i32, i32* %need_above_left, align 4, !tbaa !8
  %tobool196 = icmp ne i32 %178, 0
  br i1 %tobool196, label %if.then197, label %if.end226

if.then197:                                       ; preds = %if.end195
  %179 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp198 = icmp sgt i32 %179, 0
  br i1 %cmp198, label %land.lhs.true200, label %if.else206

land.lhs.true200:                                 ; preds = %if.then197
  %180 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp201 = icmp sgt i32 %180, 0
  br i1 %cmp201, label %if.then203, label %if.else206

if.then203:                                       ; preds = %land.lhs.true200
  %181 = load i16*, i16** %above_ref, align 4, !tbaa !2
  %arrayidx204 = getelementptr inbounds i16, i16* %181, i32 -1
  %182 = load i16, i16* %arrayidx204, align 2, !tbaa !11
  %183 = load i16*, i16** %above_row, align 4, !tbaa !2
  %arrayidx205 = getelementptr inbounds i16, i16* %183, i32 -1
  store i16 %182, i16* %arrayidx205, align 2, !tbaa !11
  br label %if.end223

if.else206:                                       ; preds = %land.lhs.true200, %if.then197
  %184 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp207 = icmp sgt i32 %184, 0
  br i1 %cmp207, label %if.then209, label %if.else212

if.then209:                                       ; preds = %if.else206
  %185 = load i16*, i16** %above_ref, align 4, !tbaa !2
  %arrayidx210 = getelementptr inbounds i16, i16* %185, i32 0
  %186 = load i16, i16* %arrayidx210, align 2, !tbaa !11
  %187 = load i16*, i16** %above_row, align 4, !tbaa !2
  %arrayidx211 = getelementptr inbounds i16, i16* %187, i32 -1
  store i16 %186, i16* %arrayidx211, align 2, !tbaa !11
  br label %if.end222

if.else212:                                       ; preds = %if.else206
  %188 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp213 = icmp sgt i32 %188, 0
  br i1 %cmp213, label %if.then215, label %if.else218

if.then215:                                       ; preds = %if.else212
  %189 = load i16*, i16** %left_ref, align 4, !tbaa !2
  %arrayidx216 = getelementptr inbounds i16, i16* %189, i32 0
  %190 = load i16, i16* %arrayidx216, align 2, !tbaa !11
  %191 = load i16*, i16** %above_row, align 4, !tbaa !2
  %arrayidx217 = getelementptr inbounds i16, i16* %191, i32 -1
  store i16 %190, i16* %arrayidx217, align 2, !tbaa !11
  br label %if.end221

if.else218:                                       ; preds = %if.else212
  %192 = load i32, i32* %base, align 4, !tbaa !8
  %conv219 = trunc i32 %192 to i16
  %193 = load i16*, i16** %above_row, align 4, !tbaa !2
  %arrayidx220 = getelementptr inbounds i16, i16* %193, i32 -1
  store i16 %conv219, i16* %arrayidx220, align 2, !tbaa !11
  br label %if.end221

if.end221:                                        ; preds = %if.else218, %if.then215
  br label %if.end222

if.end222:                                        ; preds = %if.end221, %if.then209
  br label %if.end223

if.end223:                                        ; preds = %if.end222, %if.then203
  %194 = load i16*, i16** %above_row, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds i16, i16* %194, i32 -1
  %195 = load i16, i16* %arrayidx224, align 2, !tbaa !11
  %196 = load i16*, i16** %left_col, align 4, !tbaa !2
  %arrayidx225 = getelementptr inbounds i16, i16* %196, i32 -1
  store i16 %195, i16* %arrayidx225, align 2, !tbaa !11
  br label %if.end226

if.end226:                                        ; preds = %if.end223, %if.end195
  %197 = load i32, i32* %use_filter_intra, align 4, !tbaa !8
  %tobool227 = icmp ne i32 %197, 0
  br i1 %tobool227, label %if.then228, label %if.end231

if.then228:                                       ; preds = %if.end226
  %198 = load i16*, i16** %dst, align 4, !tbaa !2
  %199 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %200 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %201 = load i16*, i16** %above_row, align 4, !tbaa !2
  %202 = load i16*, i16** %left_col, align 4, !tbaa !2
  %203 = load i8, i8* %filter_intra_mode.addr, align 1, !tbaa !10
  %conv229 = zext i8 %203 to i32
  %204 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd230 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %204, i32 0, i32 41
  %205 = load i32, i32* %bd230, align 4, !tbaa !66
  call void @highbd_filter_intra_predictor(i16* %198, i32 %199, i8 zeroext %200, i16* %201, i16* %202, i32 %conv229, i32 %205)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end231:                                        ; preds = %if.end226
  %206 = load i32, i32* %is_dr_mode, align 4, !tbaa !8
  %tobool232 = icmp ne i32 %206, 0
  br i1 %tobool232, label %if.then233, label %if.end329

if.then233:                                       ; preds = %if.end231
  %207 = bitcast i32* %upsample_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %207) #6
  store i32 0, i32* %upsample_above, align 4, !tbaa !8
  %208 = bitcast i32* %upsample_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %208) #6
  store i32 0, i32* %upsample_left, align 4, !tbaa !8
  %209 = load i32, i32* %disable_edge_filter.addr, align 4, !tbaa !8
  %tobool234 = icmp ne i32 %209, 0
  br i1 %tobool234, label %if.end327, label %if.then235

if.then235:                                       ; preds = %if.then233
  %210 = bitcast i32* %need_right236 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %210) #6
  %211 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp237 = icmp slt i32 %211, 90
  %conv238 = zext i1 %cmp237 to i32
  store i32 %conv238, i32* %need_right236, align 4, !tbaa !8
  %212 = bitcast i32* %need_bottom239 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %212) #6
  %213 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp240 = icmp sgt i32 %213, 180
  %conv241 = zext i1 %cmp240 to i32
  store i32 %conv241, i32* %need_bottom239, align 4, !tbaa !8
  %214 = bitcast i32* %filt_type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %214) #6
  %215 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %216 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %call242 = call i32 @get_filt_type(%struct.macroblockd* %215, i32 %216)
  store i32 %call242, i32* %filt_type, align 4, !tbaa !8
  %217 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp243 = icmp ne i32 %217, 90
  br i1 %cmp243, label %land.lhs.true245, label %if.end296

land.lhs.true245:                                 ; preds = %if.then235
  %218 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp246 = icmp ne i32 %218, 180
  br i1 %cmp246, label %if.then248, label %if.end296

if.then248:                                       ; preds = %land.lhs.true245
  %219 = bitcast i32* %ab_le to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %219) #6
  %220 = load i32, i32* %need_above_left, align 4, !tbaa !8
  %tobool249 = icmp ne i32 %220, 0
  %221 = zext i1 %tobool249 to i64
  %cond250 = select i1 %tobool249, i32 1, i32 0
  store i32 %cond250, i32* %ab_le, align 4, !tbaa !8
  %222 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool251 = icmp ne i32 %222, 0
  br i1 %tobool251, label %land.lhs.true252, label %if.end259

land.lhs.true252:                                 ; preds = %if.then248
  %223 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool253 = icmp ne i32 %223, 0
  br i1 %tobool253, label %land.lhs.true254, label %if.end259

land.lhs.true254:                                 ; preds = %land.lhs.true252
  %224 = load i32, i32* %txwpx, align 4, !tbaa !8
  %225 = load i32, i32* %txhpx, align 4, !tbaa !8
  %add255 = add nsw i32 %224, %225
  %cmp256 = icmp sge i32 %add255, 24
  br i1 %cmp256, label %if.then258, label %if.end259

if.then258:                                       ; preds = %land.lhs.true254
  %226 = load i16*, i16** %above_row, align 4, !tbaa !2
  %227 = load i16*, i16** %left_col, align 4, !tbaa !2
  call void @filter_intra_edge_corner_high(i16* %226, i16* %227)
  br label %if.end259

if.end259:                                        ; preds = %if.then258, %land.lhs.true254, %land.lhs.true252, %if.then248
  %228 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool260 = icmp ne i32 %228, 0
  br i1 %tobool260, label %land.lhs.true261, label %if.end276

land.lhs.true261:                                 ; preds = %if.end259
  %229 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp262 = icmp sgt i32 %229, 0
  br i1 %cmp262, label %if.then264, label %if.end276

if.then264:                                       ; preds = %land.lhs.true261
  %230 = bitcast i32* %strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %230) #6
  %231 = load i32, i32* %txwpx, align 4, !tbaa !8
  %232 = load i32, i32* %txhpx, align 4, !tbaa !8
  %233 = load i32, i32* %p_angle, align 4, !tbaa !8
  %sub265 = sub nsw i32 %233, 90
  %234 = load i32, i32* %filt_type, align 4, !tbaa !8
  %call266 = call i32 @intra_edge_filter_strength(i32 %231, i32 %232, i32 %sub265, i32 %234)
  store i32 %call266, i32* %strength, align 4, !tbaa !8
  %235 = bitcast i32* %n_px to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %235) #6
  %236 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %237 = load i32, i32* %ab_le, align 4, !tbaa !8
  %add267 = add nsw i32 %236, %237
  %238 = load i32, i32* %need_right236, align 4, !tbaa !8
  %tobool268 = icmp ne i32 %238, 0
  br i1 %tobool268, label %cond.true269, label %cond.false270

cond.true269:                                     ; preds = %if.then264
  %239 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end271

cond.false270:                                    ; preds = %if.then264
  br label %cond.end271

cond.end271:                                      ; preds = %cond.false270, %cond.true269
  %cond272 = phi i32 [ %239, %cond.true269 ], [ 0, %cond.false270 ]
  %add273 = add nsw i32 %add267, %cond272
  store i32 %add273, i32* %n_px, align 4, !tbaa !8
  %240 = load i16*, i16** %above_row, align 4, !tbaa !2
  %241 = load i32, i32* %ab_le, align 4, !tbaa !8
  %idx.neg274 = sub i32 0, %241
  %add.ptr275 = getelementptr inbounds i16, i16* %240, i32 %idx.neg274
  %242 = load i32, i32* %n_px, align 4, !tbaa !8
  %243 = load i32, i32* %strength, align 4, !tbaa !8
  call void @av1_filter_intra_edge_high_c(i16* %add.ptr275, i32 %242, i32 %243)
  %244 = bitcast i32* %n_px to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #6
  %245 = bitcast i32* %strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #6
  br label %if.end276

if.end276:                                        ; preds = %cond.end271, %land.lhs.true261, %if.end259
  %246 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool277 = icmp ne i32 %246, 0
  br i1 %tobool277, label %land.lhs.true278, label %if.end295

land.lhs.true278:                                 ; preds = %if.end276
  %247 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp279 = icmp sgt i32 %247, 0
  br i1 %cmp279, label %if.then281, label %if.end295

if.then281:                                       ; preds = %land.lhs.true278
  %248 = bitcast i32* %strength282 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %248) #6
  %249 = load i32, i32* %txhpx, align 4, !tbaa !8
  %250 = load i32, i32* %txwpx, align 4, !tbaa !8
  %251 = load i32, i32* %p_angle, align 4, !tbaa !8
  %sub283 = sub nsw i32 %251, 180
  %252 = load i32, i32* %filt_type, align 4, !tbaa !8
  %call284 = call i32 @intra_edge_filter_strength(i32 %249, i32 %250, i32 %sub283, i32 %252)
  store i32 %call284, i32* %strength282, align 4, !tbaa !8
  %253 = bitcast i32* %n_px285 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %253) #6
  %254 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %255 = load i32, i32* %ab_le, align 4, !tbaa !8
  %add286 = add nsw i32 %254, %255
  %256 = load i32, i32* %need_bottom239, align 4, !tbaa !8
  %tobool287 = icmp ne i32 %256, 0
  br i1 %tobool287, label %cond.true288, label %cond.false289

cond.true288:                                     ; preds = %if.then281
  %257 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end290

cond.false289:                                    ; preds = %if.then281
  br label %cond.end290

cond.end290:                                      ; preds = %cond.false289, %cond.true288
  %cond291 = phi i32 [ %257, %cond.true288 ], [ 0, %cond.false289 ]
  %add292 = add nsw i32 %add286, %cond291
  store i32 %add292, i32* %n_px285, align 4, !tbaa !8
  %258 = load i16*, i16** %left_col, align 4, !tbaa !2
  %259 = load i32, i32* %ab_le, align 4, !tbaa !8
  %idx.neg293 = sub i32 0, %259
  %add.ptr294 = getelementptr inbounds i16, i16* %258, i32 %idx.neg293
  %260 = load i32, i32* %n_px285, align 4, !tbaa !8
  %261 = load i32, i32* %strength282, align 4, !tbaa !8
  call void @av1_filter_intra_edge_high_c(i16* %add.ptr294, i32 %260, i32 %261)
  %262 = bitcast i32* %n_px285 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #6
  %263 = bitcast i32* %strength282 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #6
  br label %if.end295

if.end295:                                        ; preds = %cond.end290, %land.lhs.true278, %if.end276
  %264 = bitcast i32* %ab_le to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #6
  br label %if.end296

if.end296:                                        ; preds = %if.end295, %land.lhs.true245, %if.then235
  %265 = load i32, i32* %txwpx, align 4, !tbaa !8
  %266 = load i32, i32* %txhpx, align 4, !tbaa !8
  %267 = load i32, i32* %p_angle, align 4, !tbaa !8
  %sub297 = sub nsw i32 %267, 90
  %268 = load i32, i32* %filt_type, align 4, !tbaa !8
  %call298 = call i32 @av1_use_intra_edge_upsample(i32 %265, i32 %266, i32 %sub297, i32 %268)
  store i32 %call298, i32* %upsample_above, align 4, !tbaa !8
  %269 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool299 = icmp ne i32 %269, 0
  br i1 %tobool299, label %land.lhs.true300, label %if.end311

land.lhs.true300:                                 ; preds = %if.end296
  %270 = load i32, i32* %upsample_above, align 4, !tbaa !8
  %tobool301 = icmp ne i32 %270, 0
  br i1 %tobool301, label %if.then302, label %if.end311

if.then302:                                       ; preds = %land.lhs.true300
  %271 = bitcast i32* %n_px303 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %271) #6
  %272 = load i32, i32* %txwpx, align 4, !tbaa !8
  %273 = load i32, i32* %need_right236, align 4, !tbaa !8
  %tobool304 = icmp ne i32 %273, 0
  br i1 %tobool304, label %cond.true305, label %cond.false306

cond.true305:                                     ; preds = %if.then302
  %274 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end307

cond.false306:                                    ; preds = %if.then302
  br label %cond.end307

cond.end307:                                      ; preds = %cond.false306, %cond.true305
  %cond308 = phi i32 [ %274, %cond.true305 ], [ 0, %cond.false306 ]
  %add309 = add nsw i32 %272, %cond308
  store i32 %add309, i32* %n_px303, align 4, !tbaa !8
  %275 = load i16*, i16** %above_row, align 4, !tbaa !2
  %276 = load i32, i32* %n_px303, align 4, !tbaa !8
  %277 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd310 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %277, i32 0, i32 41
  %278 = load i32, i32* %bd310, align 4, !tbaa !66
  call void @av1_upsample_intra_edge_high_c(i16* %275, i32 %276, i32 %278)
  %279 = bitcast i32* %n_px303 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #6
  br label %if.end311

if.end311:                                        ; preds = %cond.end307, %land.lhs.true300, %if.end296
  %280 = load i32, i32* %txhpx, align 4, !tbaa !8
  %281 = load i32, i32* %txwpx, align 4, !tbaa !8
  %282 = load i32, i32* %p_angle, align 4, !tbaa !8
  %sub312 = sub nsw i32 %282, 180
  %283 = load i32, i32* %filt_type, align 4, !tbaa !8
  %call313 = call i32 @av1_use_intra_edge_upsample(i32 %280, i32 %281, i32 %sub312, i32 %283)
  store i32 %call313, i32* %upsample_left, align 4, !tbaa !8
  %284 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool314 = icmp ne i32 %284, 0
  br i1 %tobool314, label %land.lhs.true315, label %if.end326

land.lhs.true315:                                 ; preds = %if.end311
  %285 = load i32, i32* %upsample_left, align 4, !tbaa !8
  %tobool316 = icmp ne i32 %285, 0
  br i1 %tobool316, label %if.then317, label %if.end326

if.then317:                                       ; preds = %land.lhs.true315
  %286 = bitcast i32* %n_px318 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %286) #6
  %287 = load i32, i32* %txhpx, align 4, !tbaa !8
  %288 = load i32, i32* %need_bottom239, align 4, !tbaa !8
  %tobool319 = icmp ne i32 %288, 0
  br i1 %tobool319, label %cond.true320, label %cond.false321

cond.true320:                                     ; preds = %if.then317
  %289 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end322

cond.false321:                                    ; preds = %if.then317
  br label %cond.end322

cond.end322:                                      ; preds = %cond.false321, %cond.true320
  %cond323 = phi i32 [ %289, %cond.true320 ], [ 0, %cond.false321 ]
  %add324 = add nsw i32 %287, %cond323
  store i32 %add324, i32* %n_px318, align 4, !tbaa !8
  %290 = load i16*, i16** %left_col, align 4, !tbaa !2
  %291 = load i32, i32* %n_px318, align 4, !tbaa !8
  %292 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd325 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %292, i32 0, i32 41
  %293 = load i32, i32* %bd325, align 4, !tbaa !66
  call void @av1_upsample_intra_edge_high_c(i16* %290, i32 %291, i32 %293)
  %294 = bitcast i32* %n_px318 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %294) #6
  br label %if.end326

if.end326:                                        ; preds = %cond.end322, %land.lhs.true315, %if.end311
  %295 = bitcast i32* %filt_type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %295) #6
  %296 = bitcast i32* %need_bottom239 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #6
  %297 = bitcast i32* %need_right236 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %297) #6
  br label %if.end327

if.end327:                                        ; preds = %if.end326, %if.then233
  %298 = load i16*, i16** %dst, align 4, !tbaa !2
  %299 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %300 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %301 = load i16*, i16** %above_row, align 4, !tbaa !2
  %302 = load i16*, i16** %left_col, align 4, !tbaa !2
  %303 = load i32, i32* %upsample_above, align 4, !tbaa !8
  %304 = load i32, i32* %upsample_left, align 4, !tbaa !8
  %305 = load i32, i32* %p_angle, align 4, !tbaa !8
  %306 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd328 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %306, i32 0, i32 41
  %307 = load i32, i32* %bd328, align 4, !tbaa !66
  call void @highbd_dr_predictor(i16* %298, i32 %299, i8 zeroext %300, i16* %301, i16* %302, i32 %303, i32 %304, i32 %305, i32 %307)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %308 = bitcast i32* %upsample_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %308) #6
  %309 = bitcast i32* %upsample_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %309) #6
  br label %cleanup

if.end329:                                        ; preds = %if.end231
  %310 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %conv330 = zext i8 %310 to i32
  %cmp331 = icmp eq i32 %conv330, 0
  br i1 %cmp331, label %if.then333, label %if.else343

if.then333:                                       ; preds = %if.end329
  %311 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp334 = icmp sgt i32 %311, 0
  %conv335 = zext i1 %cmp334 to i32
  %arrayidx336 = getelementptr inbounds [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 %conv335
  %312 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp337 = icmp sgt i32 %312, 0
  %conv338 = zext i1 %cmp337 to i32
  %arrayidx339 = getelementptr inbounds [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* %arrayidx336, i32 0, i32 %conv338
  %313 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom340 = zext i8 %313 to i32
  %arrayidx341 = getelementptr inbounds [19 x void (i16*, i32, i16*, i16*, i32)*], [19 x void (i16*, i32, i16*, i16*, i32)*]* %arrayidx339, i32 0, i32 %idxprom340
  %314 = load void (i16*, i32, i16*, i16*, i32)*, void (i16*, i32, i16*, i16*, i32)** %arrayidx341, align 4, !tbaa !2
  %315 = load i16*, i16** %dst, align 4, !tbaa !2
  %316 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %317 = load i16*, i16** %above_row, align 4, !tbaa !2
  %318 = load i16*, i16** %left_col, align 4, !tbaa !2
  %319 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd342 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %319, i32 0, i32 41
  %320 = load i32, i32* %bd342, align 4, !tbaa !66
  call void %314(i16* %315, i32 %316, i16* %317, i16* %318, i32 %320)
  br label %if.end349

if.else343:                                       ; preds = %if.end329
  %321 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom344 = zext i8 %321 to i32
  %arrayidx345 = getelementptr inbounds [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 %idxprom344
  %322 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom346 = zext i8 %322 to i32
  %arrayidx347 = getelementptr inbounds [19 x void (i16*, i32, i16*, i16*, i32)*], [19 x void (i16*, i32, i16*, i16*, i32)*]* %arrayidx345, i32 0, i32 %idxprom346
  %323 = load void (i16*, i32, i16*, i16*, i32)*, void (i16*, i32, i16*, i16*, i32)** %arrayidx347, align 4, !tbaa !2
  %324 = load i16*, i16** %dst, align 4, !tbaa !2
  %325 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %326 = load i16*, i16** %above_row, align 4, !tbaa !2
  %327 = load i16*, i16** %left_col, align 4, !tbaa !2
  %328 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd348 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %328, i32 0, i32 41
  %329 = load i32, i32* %bd348, align 4, !tbaa !66
  call void %323(i16* %324, i32 %325, i16* %326, i16* %327, i32 %329)
  br label %if.end349

if.end349:                                        ; preds = %if.else343, %if.then333
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end349, %if.end327, %if.then228, %for.end
  %330 = bitcast i32* %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %330) #6
  %331 = bitcast i32* %use_filter_intra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #6
  %332 = bitcast i32* %is_dr_mode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #6
  %333 = bitcast i32* %p_angle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #6
  %334 = bitcast i16** %left_ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #6
  %335 = bitcast i16** %above_ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %335) #6
  %336 = bitcast i32* %need_above_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #6
  %337 = bitcast i32* %need_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #6
  %338 = bitcast i32* %need_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #6
  %339 = bitcast i32* %txhpx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %339) #6
  %340 = bitcast i32* %txwpx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #6
  %341 = bitcast i16** %left_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %341) #6
  %342 = bitcast i16** %above_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %342) #6
  %343 = bitcast [160 x i16]* %above_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 320, i8* %343) #6
  %344 = bitcast [160 x i16]* %left_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 320, i8* %344) #6
  %345 = bitcast i16** %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %345) #6
  %346 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %346) #6
  %347 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %347) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @build_intra_predictors(%struct.macroblockd* %xd, i8* %ref, i32 %ref_stride, i8* %dst, i32 %dst_stride, i8 zeroext %mode, i32 %angle_delta, i8 zeroext %filter_intra_mode, i8 zeroext %tx_size, i32 %disable_edge_filter, i32 %n_top_px, i32 %n_topright_px, i32 %n_left_px, i32 %n_bottomleft_px, i32 %plane) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref.addr = alloca i8*, align 4
  %ref_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %mode.addr = alloca i8, align 1
  %angle_delta.addr = alloca i32, align 4
  %filter_intra_mode.addr = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %disable_edge_filter.addr = alloca i32, align 4
  %n_top_px.addr = alloca i32, align 4
  %n_topright_px.addr = alloca i32, align 4
  %n_left_px.addr = alloca i32, align 4
  %n_bottomleft_px.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %above_ref = alloca i8*, align 4
  %left_ref = alloca i8*, align 4
  %left_data = alloca [160 x i8], align 16
  %above_data = alloca [160 x i8], align 16
  %above_row = alloca i8*, align 4
  %left_col = alloca i8*, align 4
  %txwpx = alloca i32, align 4
  %txhpx = alloca i32, align 4
  %need_left = alloca i32, align 4
  %need_above = alloca i32, align 4
  %need_above_left = alloca i32, align 4
  %p_angle = alloca i32, align 4
  %is_dr_mode = alloca i32, align 4
  %use_filter_intra = alloca i32, align 4
  %val = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %need_bottom = alloca i32, align 4
  %num_left_pixels_needed = alloca i32, align 4
  %need_right = alloca i32, align 4
  %num_top_pixels_needed = alloca i32, align 4
  %upsample_above = alloca i32, align 4
  %upsample_left = alloca i32, align 4
  %need_right218 = alloca i32, align 4
  %need_bottom221 = alloca i32, align 4
  %filt_type = alloca i32, align 4
  %ab_le = alloca i32, align 4
  %strength = alloca i32, align 4
  %n_px = alloca i32, align 4
  %strength264 = alloca i32, align 4
  %n_px267 = alloca i32, align 4
  %n_px285 = alloca i32, align 4
  %n_px299 = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %ref_stride, i32* %ref_stride.addr, align 4, !tbaa !8
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !8
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !10
  store i32 %angle_delta, i32* %angle_delta.addr, align 4, !tbaa !8
  store i8 %filter_intra_mode, i8* %filter_intra_mode.addr, align 1, !tbaa !10
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i32 %disable_edge_filter, i32* %disable_edge_filter.addr, align 4, !tbaa !8
  store i32 %n_top_px, i32* %n_top_px.addr, align 4, !tbaa !8
  store i32 %n_topright_px, i32* %n_topright_px.addr, align 4, !tbaa !8
  store i32 %n_left_px, i32* %n_left_px.addr, align 4, !tbaa !8
  store i32 %n_bottomleft_px, i32* %n_bottomleft_px.addr, align 4, !tbaa !8
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !8
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i8** %above_ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %3 = load i32, i32* %ref_stride.addr, align 4, !tbaa !8
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %idx.neg
  store i8* %add.ptr, i8** %above_ref, align 4, !tbaa !2
  %4 = bitcast i8** %left_ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %add.ptr1 = getelementptr inbounds i8, i8* %5, i32 -1
  store i8* %add.ptr1, i8** %left_ref, align 4, !tbaa !2
  %6 = bitcast [160 x i8]* %left_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 160, i8* %6) #6
  %7 = bitcast [160 x i8]* %above_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 160, i8* %7) #6
  %8 = bitcast i8** %above_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %arraydecay = getelementptr inbounds [160 x i8], [160 x i8]* %above_data, i32 0, i32 0
  %add.ptr2 = getelementptr inbounds i8, i8* %arraydecay, i32 16
  store i8* %add.ptr2, i8** %above_row, align 4, !tbaa !2
  %9 = bitcast i8** %left_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %arraydecay3 = getelementptr inbounds [160 x i8], [160 x i8]* %left_data, i32 0, i32 0
  %add.ptr4 = getelementptr inbounds i8, i8* %arraydecay3, i32 16
  store i8* %add.ptr4, i8** %left_col, align 4, !tbaa !2
  %10 = bitcast i32* %txwpx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %11 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %12 = load i32, i32* %arrayidx, align 4, !tbaa !8
  store i32 %12, i32* %txwpx, align 4, !tbaa !8
  %13 = bitcast i32* %txhpx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom5 = zext i8 %14 to i32
  %arrayidx6 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom5
  %15 = load i32, i32* %arrayidx6, align 4, !tbaa !8
  store i32 %15, i32* %txhpx, align 4, !tbaa !8
  %16 = bitcast i32* %need_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom7 = zext i8 %17 to i32
  %arrayidx8 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom7
  %18 = load i8, i8* %arrayidx8, align 1, !tbaa !10
  %conv = zext i8 %18 to i32
  %and = and i32 %conv, 2
  store i32 %and, i32* %need_left, align 4, !tbaa !8
  %19 = bitcast i32* %need_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom9 = zext i8 %20 to i32
  %arrayidx10 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom9
  %21 = load i8, i8* %arrayidx10, align 1, !tbaa !10
  %conv11 = zext i8 %21 to i32
  %and12 = and i32 %conv11, 4
  store i32 %and12, i32* %need_above, align 4, !tbaa !8
  %22 = bitcast i32* %need_above_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom13 = zext i8 %23 to i32
  %arrayidx14 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom13
  %24 = load i8, i8* %arrayidx14, align 1, !tbaa !10
  %conv15 = zext i8 %24 to i32
  %and16 = and i32 %conv15, 16
  store i32 %and16, i32* %need_above_left, align 4, !tbaa !8
  %25 = bitcast i32* %p_angle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  store i32 0, i32* %p_angle, align 4, !tbaa !8
  %26 = bitcast i32* %is_dr_mode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %call = call i32 @av1_is_directional_mode(i8 zeroext %27)
  store i32 %call, i32* %is_dr_mode, align 4, !tbaa !8
  %28 = bitcast i32* %use_filter_intra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %29 = load i8, i8* %filter_intra_mode.addr, align 1, !tbaa !10
  %conv17 = zext i8 %29 to i32
  %cmp = icmp ne i32 %conv17, 5
  %conv18 = zext i1 %cmp to i32
  store i32 %conv18, i32* %use_filter_intra, align 4, !tbaa !8
  %30 = load i32, i32* %is_dr_mode, align 4, !tbaa !8
  %tobool = icmp ne i32 %30, 0
  br i1 %tobool, label %if.then, label %if.end30

if.then:                                          ; preds = %entry
  %31 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom19 = zext i8 %31 to i32
  %arrayidx20 = getelementptr inbounds [13 x i8], [13 x i8]* @mode_to_angle_map, i32 0, i32 %idxprom19
  %32 = load i8, i8* %arrayidx20, align 1, !tbaa !10
  %conv21 = zext i8 %32 to i32
  %33 = load i32, i32* %angle_delta.addr, align 4, !tbaa !8
  %add = add nsw i32 %conv21, %33
  store i32 %add, i32* %p_angle, align 4, !tbaa !8
  %34 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp22 = icmp sle i32 %34, 90
  br i1 %cmp22, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.then
  store i32 1, i32* %need_above, align 4, !tbaa !8
  store i32 0, i32* %need_left, align 4, !tbaa !8
  store i32 1, i32* %need_above_left, align 4, !tbaa !8
  br label %if.end29

if.else:                                          ; preds = %if.then
  %35 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp25 = icmp slt i32 %35, 180
  br i1 %cmp25, label %if.then27, label %if.else28

if.then27:                                        ; preds = %if.else
  store i32 1, i32* %need_above, align 4, !tbaa !8
  store i32 1, i32* %need_left, align 4, !tbaa !8
  store i32 1, i32* %need_above_left, align 4, !tbaa !8
  br label %if.end

if.else28:                                        ; preds = %if.else
  store i32 0, i32* %need_above, align 4, !tbaa !8
  store i32 1, i32* %need_left, align 4, !tbaa !8
  store i32 1, i32* %need_above_left, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else28, %if.then27
  br label %if.end29

if.end29:                                         ; preds = %if.end, %if.then24
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %entry
  %36 = load i32, i32* %use_filter_intra, align 4, !tbaa !8
  %tobool31 = icmp ne i32 %36, 0
  br i1 %tobool31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.end30
  store i32 1, i32* %need_above_left, align 4, !tbaa !8
  store i32 1, i32* %need_above, align 4, !tbaa !8
  store i32 1, i32* %need_left, align 4, !tbaa !8
  br label %if.end33

if.end33:                                         ; preds = %if.then32, %if.end30
  %37 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool34 = icmp ne i32 %37, 0
  br i1 %tobool34, label %lor.lhs.false, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end33
  %38 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp35 = icmp eq i32 %38, 0
  br i1 %cmp35, label %if.then41, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %if.end33
  %39 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool37 = icmp ne i32 %39, 0
  br i1 %tobool37, label %if.end61, label %land.lhs.true38

land.lhs.true38:                                  ; preds = %lor.lhs.false
  %40 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp39 = icmp eq i32 %40, 0
  br i1 %cmp39, label %if.then41, label %if.end61

if.then41:                                        ; preds = %land.lhs.true38, %land.lhs.true
  %41 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  %42 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool42 = icmp ne i32 %42, 0
  br i1 %tobool42, label %if.then43, label %if.else48

if.then43:                                        ; preds = %if.then41
  %43 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp44 = icmp sgt i32 %43, 0
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then43
  %44 = load i8*, i8** %above_ref, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i8, i8* %44, i32 0
  %45 = load i8, i8* %arrayidx46, align 1, !tbaa !10
  %conv47 = zext i8 %45 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.then43
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv47, %cond.true ], [ 129, %cond.false ]
  store i32 %cond, i32* %val, align 4, !tbaa !8
  br label %if.end57

if.else48:                                        ; preds = %if.then41
  %46 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp49 = icmp sgt i32 %46, 0
  br i1 %cmp49, label %cond.true51, label %cond.false54

cond.true51:                                      ; preds = %if.else48
  %47 = load i8*, i8** %left_ref, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %47, i32 0
  %48 = load i8, i8* %arrayidx52, align 1, !tbaa !10
  %conv53 = zext i8 %48 to i32
  br label %cond.end55

cond.false54:                                     ; preds = %if.else48
  br label %cond.end55

cond.end55:                                       ; preds = %cond.false54, %cond.true51
  %cond56 = phi i32 [ %conv53, %cond.true51 ], [ 127, %cond.false54 ]
  store i32 %cond56, i32* %val, align 4, !tbaa !8
  br label %if.end57

if.end57:                                         ; preds = %cond.end55, %cond.end
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end57
  %49 = load i32, i32* %i, align 4, !tbaa !8
  %50 = load i32, i32* %txhpx, align 4, !tbaa !8
  %cmp58 = icmp slt i32 %49, %50
  br i1 %cmp58, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %51 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %52 = load i32, i32* %val, align 4, !tbaa !8
  %53 = trunc i32 %52 to i8
  %54 = load i32, i32* %txwpx, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %51, i8 %53, i32 %54, i1 false)
  %55 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %56 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr60 = getelementptr inbounds i8, i8* %56, i32 %55
  store i8* %add.ptr60, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %57 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %57, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  %58 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  br label %cleanup

if.end61:                                         ; preds = %land.lhs.true38, %lor.lhs.false
  %59 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool62 = icmp ne i32 %59, 0
  br i1 %tobool62, label %if.then63, label %if.end128

if.then63:                                        ; preds = %if.end61
  %60 = bitcast i32* %need_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #6
  %61 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom64 = zext i8 %61 to i32
  %arrayidx65 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom64
  %62 = load i8, i8* %arrayidx65, align 1, !tbaa !10
  %conv66 = zext i8 %62 to i32
  %and67 = and i32 %conv66, 32
  store i32 %and67, i32* %need_bottom, align 4, !tbaa !8
  %63 = load i32, i32* %use_filter_intra, align 4, !tbaa !8
  %tobool68 = icmp ne i32 %63, 0
  br i1 %tobool68, label %if.then69, label %if.end70

if.then69:                                        ; preds = %if.then63
  store i32 0, i32* %need_bottom, align 4, !tbaa !8
  br label %if.end70

if.end70:                                         ; preds = %if.then69, %if.then63
  %64 = load i32, i32* %is_dr_mode, align 4, !tbaa !8
  %tobool71 = icmp ne i32 %64, 0
  br i1 %tobool71, label %if.then72, label %if.end75

if.then72:                                        ; preds = %if.end70
  %65 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp73 = icmp sgt i32 %65, 180
  %conv74 = zext i1 %cmp73 to i32
  store i32 %conv74, i32* %need_bottom, align 4, !tbaa !8
  br label %if.end75

if.end75:                                         ; preds = %if.then72, %if.end70
  %66 = bitcast i32* %num_left_pixels_needed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  %67 = load i32, i32* %txhpx, align 4, !tbaa !8
  %68 = load i32, i32* %need_bottom, align 4, !tbaa !8
  %tobool76 = icmp ne i32 %68, 0
  br i1 %tobool76, label %cond.true77, label %cond.false78

cond.true77:                                      ; preds = %if.end75
  %69 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end79

cond.false78:                                     ; preds = %if.end75
  br label %cond.end79

cond.end79:                                       ; preds = %cond.false78, %cond.true77
  %cond80 = phi i32 [ %69, %cond.true77 ], [ 3, %cond.false78 ]
  %add81 = add nsw i32 %67, %cond80
  store i32 %add81, i32* %num_left_pixels_needed, align 4, !tbaa !8
  store i32 0, i32* %i, align 4, !tbaa !8
  %70 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp82 = icmp sgt i32 %70, 0
  br i1 %cmp82, label %if.then84, label %if.else119

if.then84:                                        ; preds = %cond.end79
  br label %for.cond85

for.cond85:                                       ; preds = %for.inc91, %if.then84
  %71 = load i32, i32* %i, align 4, !tbaa !8
  %72 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp86 = icmp slt i32 %71, %72
  br i1 %cmp86, label %for.body88, label %for.end93

for.body88:                                       ; preds = %for.cond85
  %73 = load i8*, i8** %left_ref, align 4, !tbaa !2
  %74 = load i32, i32* %i, align 4, !tbaa !8
  %75 = load i32, i32* %ref_stride.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %74, %75
  %arrayidx89 = getelementptr inbounds i8, i8* %73, i32 %mul
  %76 = load i8, i8* %arrayidx89, align 1, !tbaa !10
  %77 = load i8*, i8** %left_col, align 4, !tbaa !2
  %78 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx90 = getelementptr inbounds i8, i8* %77, i32 %78
  store i8 %76, i8* %arrayidx90, align 1, !tbaa !10
  br label %for.inc91

for.inc91:                                        ; preds = %for.body88
  %79 = load i32, i32* %i, align 4, !tbaa !8
  %inc92 = add nsw i32 %79, 1
  store i32 %inc92, i32* %i, align 4, !tbaa !8
  br label %for.cond85

for.end93:                                        ; preds = %for.cond85
  %80 = load i32, i32* %need_bottom, align 4, !tbaa !8
  %tobool94 = icmp ne i32 %80, 0
  br i1 %tobool94, label %land.lhs.true95, label %if.end110

land.lhs.true95:                                  ; preds = %for.end93
  %81 = load i32, i32* %n_bottomleft_px.addr, align 4, !tbaa !8
  %cmp96 = icmp sgt i32 %81, 0
  br i1 %cmp96, label %if.then98, label %if.end110

if.then98:                                        ; preds = %land.lhs.true95
  br label %for.cond99

for.cond99:                                       ; preds = %for.inc107, %if.then98
  %82 = load i32, i32* %i, align 4, !tbaa !8
  %83 = load i32, i32* %txhpx, align 4, !tbaa !8
  %84 = load i32, i32* %n_bottomleft_px.addr, align 4, !tbaa !8
  %add100 = add nsw i32 %83, %84
  %cmp101 = icmp slt i32 %82, %add100
  br i1 %cmp101, label %for.body103, label %for.end109

for.body103:                                      ; preds = %for.cond99
  %85 = load i8*, i8** %left_ref, align 4, !tbaa !2
  %86 = load i32, i32* %i, align 4, !tbaa !8
  %87 = load i32, i32* %ref_stride.addr, align 4, !tbaa !8
  %mul104 = mul nsw i32 %86, %87
  %arrayidx105 = getelementptr inbounds i8, i8* %85, i32 %mul104
  %88 = load i8, i8* %arrayidx105, align 1, !tbaa !10
  %89 = load i8*, i8** %left_col, align 4, !tbaa !2
  %90 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx106 = getelementptr inbounds i8, i8* %89, i32 %90
  store i8 %88, i8* %arrayidx106, align 1, !tbaa !10
  br label %for.inc107

for.inc107:                                       ; preds = %for.body103
  %91 = load i32, i32* %i, align 4, !tbaa !8
  %inc108 = add nsw i32 %91, 1
  store i32 %inc108, i32* %i, align 4, !tbaa !8
  br label %for.cond99

for.end109:                                       ; preds = %for.cond99
  br label %if.end110

if.end110:                                        ; preds = %for.end109, %land.lhs.true95, %for.end93
  %92 = load i32, i32* %i, align 4, !tbaa !8
  %93 = load i32, i32* %num_left_pixels_needed, align 4, !tbaa !8
  %cmp111 = icmp slt i32 %92, %93
  br i1 %cmp111, label %if.then113, label %if.end118

if.then113:                                       ; preds = %if.end110
  %94 = load i8*, i8** %left_col, align 4, !tbaa !2
  %95 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx114 = getelementptr inbounds i8, i8* %94, i32 %95
  %96 = load i8*, i8** %left_col, align 4, !tbaa !2
  %97 = load i32, i32* %i, align 4, !tbaa !8
  %sub = sub nsw i32 %97, 1
  %arrayidx115 = getelementptr inbounds i8, i8* %96, i32 %sub
  %98 = load i8, i8* %arrayidx115, align 1, !tbaa !10
  %conv116 = zext i8 %98 to i32
  %99 = trunc i32 %conv116 to i8
  %100 = load i32, i32* %num_left_pixels_needed, align 4, !tbaa !8
  %101 = load i32, i32* %i, align 4, !tbaa !8
  %sub117 = sub nsw i32 %100, %101
  call void @llvm.memset.p0i8.i32(i8* align 1 %arrayidx114, i8 %99, i32 %sub117, i1 false)
  br label %if.end118

if.end118:                                        ; preds = %if.then113, %if.end110
  br label %if.end127

if.else119:                                       ; preds = %cond.end79
  %102 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp120 = icmp sgt i32 %102, 0
  br i1 %cmp120, label %if.then122, label %if.else125

if.then122:                                       ; preds = %if.else119
  %103 = load i8*, i8** %left_col, align 4, !tbaa !2
  %104 = load i8*, i8** %above_ref, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i8, i8* %104, i32 0
  %105 = load i8, i8* %arrayidx123, align 1, !tbaa !10
  %conv124 = zext i8 %105 to i32
  %106 = trunc i32 %conv124 to i8
  %107 = load i32, i32* %num_left_pixels_needed, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %103, i8 %106, i32 %107, i1 false)
  br label %if.end126

if.else125:                                       ; preds = %if.else119
  %108 = load i8*, i8** %left_col, align 4, !tbaa !2
  %109 = load i32, i32* %num_left_pixels_needed, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %108, i8 -127, i32 %109, i1 false)
  br label %if.end126

if.end126:                                        ; preds = %if.else125, %if.then122
  br label %if.end127

if.end127:                                        ; preds = %if.end126, %if.end118
  %110 = bitcast i32* %num_left_pixels_needed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast i32* %need_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  br label %if.end128

if.end128:                                        ; preds = %if.end127, %if.end61
  %112 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool129 = icmp ne i32 %112, 0
  br i1 %tobool129, label %if.then130, label %if.end179

if.then130:                                       ; preds = %if.end128
  %113 = bitcast i32* %need_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #6
  %114 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom131 = zext i8 %114 to i32
  %arrayidx132 = getelementptr inbounds [13 x i8], [13 x i8]* @extend_modes, i32 0, i32 %idxprom131
  %115 = load i8, i8* %arrayidx132, align 1, !tbaa !10
  %conv133 = zext i8 %115 to i32
  %and134 = and i32 %conv133, 8
  store i32 %and134, i32* %need_right, align 4, !tbaa !8
  %116 = load i32, i32* %use_filter_intra, align 4, !tbaa !8
  %tobool135 = icmp ne i32 %116, 0
  br i1 %tobool135, label %if.then136, label %if.end137

if.then136:                                       ; preds = %if.then130
  store i32 0, i32* %need_right, align 4, !tbaa !8
  br label %if.end137

if.end137:                                        ; preds = %if.then136, %if.then130
  %117 = load i32, i32* %is_dr_mode, align 4, !tbaa !8
  %tobool138 = icmp ne i32 %117, 0
  br i1 %tobool138, label %if.then139, label %if.end142

if.then139:                                       ; preds = %if.end137
  %118 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp140 = icmp slt i32 %118, 90
  %conv141 = zext i1 %cmp140 to i32
  store i32 %conv141, i32* %need_right, align 4, !tbaa !8
  br label %if.end142

if.end142:                                        ; preds = %if.then139, %if.end137
  %119 = bitcast i32* %num_top_pixels_needed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #6
  %120 = load i32, i32* %txwpx, align 4, !tbaa !8
  %121 = load i32, i32* %need_right, align 4, !tbaa !8
  %tobool143 = icmp ne i32 %121, 0
  br i1 %tobool143, label %cond.true144, label %cond.false145

cond.true144:                                     ; preds = %if.end142
  %122 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end146

cond.false145:                                    ; preds = %if.end142
  br label %cond.end146

cond.end146:                                      ; preds = %cond.false145, %cond.true144
  %cond147 = phi i32 [ %122, %cond.true144 ], [ 0, %cond.false145 ]
  %add148 = add nsw i32 %120, %cond147
  store i32 %add148, i32* %num_top_pixels_needed, align 4, !tbaa !8
  %123 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp149 = icmp sgt i32 %123, 0
  br i1 %cmp149, label %if.then151, label %if.else170

if.then151:                                       ; preds = %cond.end146
  %124 = load i8*, i8** %above_row, align 4, !tbaa !2
  %125 = load i8*, i8** %above_ref, align 4, !tbaa !2
  %126 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %124, i8* align 1 %125, i32 %126, i1 false)
  %127 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  store i32 %127, i32* %i, align 4, !tbaa !8
  %128 = load i32, i32* %need_right, align 4, !tbaa !8
  %tobool152 = icmp ne i32 %128, 0
  br i1 %tobool152, label %land.lhs.true153, label %if.end160

land.lhs.true153:                                 ; preds = %if.then151
  %129 = load i32, i32* %n_topright_px.addr, align 4, !tbaa !8
  %cmp154 = icmp sgt i32 %129, 0
  br i1 %cmp154, label %if.then156, label %if.end160

if.then156:                                       ; preds = %land.lhs.true153
  %130 = load i8*, i8** %above_row, align 4, !tbaa !2
  %131 = load i32, i32* %txwpx, align 4, !tbaa !8
  %add.ptr157 = getelementptr inbounds i8, i8* %130, i32 %131
  %132 = load i8*, i8** %above_ref, align 4, !tbaa !2
  %133 = load i32, i32* %txwpx, align 4, !tbaa !8
  %add.ptr158 = getelementptr inbounds i8, i8* %132, i32 %133
  %134 = load i32, i32* %n_topright_px.addr, align 4, !tbaa !8
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr157, i8* align 1 %add.ptr158, i32 %134, i1 false)
  %135 = load i32, i32* %n_topright_px.addr, align 4, !tbaa !8
  %136 = load i32, i32* %i, align 4, !tbaa !8
  %add159 = add nsw i32 %136, %135
  store i32 %add159, i32* %i, align 4, !tbaa !8
  br label %if.end160

if.end160:                                        ; preds = %if.then156, %land.lhs.true153, %if.then151
  %137 = load i32, i32* %i, align 4, !tbaa !8
  %138 = load i32, i32* %num_top_pixels_needed, align 4, !tbaa !8
  %cmp161 = icmp slt i32 %137, %138
  br i1 %cmp161, label %if.then163, label %if.end169

if.then163:                                       ; preds = %if.end160
  %139 = load i8*, i8** %above_row, align 4, !tbaa !2
  %140 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx164 = getelementptr inbounds i8, i8* %139, i32 %140
  %141 = load i8*, i8** %above_row, align 4, !tbaa !2
  %142 = load i32, i32* %i, align 4, !tbaa !8
  %sub165 = sub nsw i32 %142, 1
  %arrayidx166 = getelementptr inbounds i8, i8* %141, i32 %sub165
  %143 = load i8, i8* %arrayidx166, align 1, !tbaa !10
  %conv167 = zext i8 %143 to i32
  %144 = trunc i32 %conv167 to i8
  %145 = load i32, i32* %num_top_pixels_needed, align 4, !tbaa !8
  %146 = load i32, i32* %i, align 4, !tbaa !8
  %sub168 = sub nsw i32 %145, %146
  call void @llvm.memset.p0i8.i32(i8* align 1 %arrayidx164, i8 %144, i32 %sub168, i1 false)
  br label %if.end169

if.end169:                                        ; preds = %if.then163, %if.end160
  br label %if.end178

if.else170:                                       ; preds = %cond.end146
  %147 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp171 = icmp sgt i32 %147, 0
  br i1 %cmp171, label %if.then173, label %if.else176

if.then173:                                       ; preds = %if.else170
  %148 = load i8*, i8** %above_row, align 4, !tbaa !2
  %149 = load i8*, i8** %left_ref, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i8, i8* %149, i32 0
  %150 = load i8, i8* %arrayidx174, align 1, !tbaa !10
  %conv175 = zext i8 %150 to i32
  %151 = trunc i32 %conv175 to i8
  %152 = load i32, i32* %num_top_pixels_needed, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %148, i8 %151, i32 %152, i1 false)
  br label %if.end177

if.else176:                                       ; preds = %if.else170
  %153 = load i8*, i8** %above_row, align 4, !tbaa !2
  %154 = load i32, i32* %num_top_pixels_needed, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %153, i8 127, i32 %154, i1 false)
  br label %if.end177

if.end177:                                        ; preds = %if.else176, %if.then173
  br label %if.end178

if.end178:                                        ; preds = %if.end177, %if.end169
  %155 = bitcast i32* %num_top_pixels_needed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #6
  %156 = bitcast i32* %need_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #6
  br label %if.end179

if.end179:                                        ; preds = %if.end178, %if.end128
  %157 = load i32, i32* %need_above_left, align 4, !tbaa !8
  %tobool180 = icmp ne i32 %157, 0
  br i1 %tobool180, label %if.then181, label %if.end209

if.then181:                                       ; preds = %if.end179
  %158 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp182 = icmp sgt i32 %158, 0
  br i1 %cmp182, label %land.lhs.true184, label %if.else190

land.lhs.true184:                                 ; preds = %if.then181
  %159 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp185 = icmp sgt i32 %159, 0
  br i1 %cmp185, label %if.then187, label %if.else190

if.then187:                                       ; preds = %land.lhs.true184
  %160 = load i8*, i8** %above_ref, align 4, !tbaa !2
  %arrayidx188 = getelementptr inbounds i8, i8* %160, i32 -1
  %161 = load i8, i8* %arrayidx188, align 1, !tbaa !10
  %162 = load i8*, i8** %above_row, align 4, !tbaa !2
  %arrayidx189 = getelementptr inbounds i8, i8* %162, i32 -1
  store i8 %161, i8* %arrayidx189, align 1, !tbaa !10
  br label %if.end206

if.else190:                                       ; preds = %land.lhs.true184, %if.then181
  %163 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp191 = icmp sgt i32 %163, 0
  br i1 %cmp191, label %if.then193, label %if.else196

if.then193:                                       ; preds = %if.else190
  %164 = load i8*, i8** %above_ref, align 4, !tbaa !2
  %arrayidx194 = getelementptr inbounds i8, i8* %164, i32 0
  %165 = load i8, i8* %arrayidx194, align 1, !tbaa !10
  %166 = load i8*, i8** %above_row, align 4, !tbaa !2
  %arrayidx195 = getelementptr inbounds i8, i8* %166, i32 -1
  store i8 %165, i8* %arrayidx195, align 1, !tbaa !10
  br label %if.end205

if.else196:                                       ; preds = %if.else190
  %167 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp197 = icmp sgt i32 %167, 0
  br i1 %cmp197, label %if.then199, label %if.else202

if.then199:                                       ; preds = %if.else196
  %168 = load i8*, i8** %left_ref, align 4, !tbaa !2
  %arrayidx200 = getelementptr inbounds i8, i8* %168, i32 0
  %169 = load i8, i8* %arrayidx200, align 1, !tbaa !10
  %170 = load i8*, i8** %above_row, align 4, !tbaa !2
  %arrayidx201 = getelementptr inbounds i8, i8* %170, i32 -1
  store i8 %169, i8* %arrayidx201, align 1, !tbaa !10
  br label %if.end204

if.else202:                                       ; preds = %if.else196
  %171 = load i8*, i8** %above_row, align 4, !tbaa !2
  %arrayidx203 = getelementptr inbounds i8, i8* %171, i32 -1
  store i8 -128, i8* %arrayidx203, align 1, !tbaa !10
  br label %if.end204

if.end204:                                        ; preds = %if.else202, %if.then199
  br label %if.end205

if.end205:                                        ; preds = %if.end204, %if.then193
  br label %if.end206

if.end206:                                        ; preds = %if.end205, %if.then187
  %172 = load i8*, i8** %above_row, align 4, !tbaa !2
  %arrayidx207 = getelementptr inbounds i8, i8* %172, i32 -1
  %173 = load i8, i8* %arrayidx207, align 1, !tbaa !10
  %174 = load i8*, i8** %left_col, align 4, !tbaa !2
  %arrayidx208 = getelementptr inbounds i8, i8* %174, i32 -1
  store i8 %173, i8* %arrayidx208, align 1, !tbaa !10
  br label %if.end209

if.end209:                                        ; preds = %if.end206, %if.end179
  %175 = load i32, i32* %use_filter_intra, align 4, !tbaa !8
  %tobool210 = icmp ne i32 %175, 0
  br i1 %tobool210, label %if.then211, label %if.end213

if.then211:                                       ; preds = %if.end209
  %176 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %177 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %178 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %179 = load i8*, i8** %above_row, align 4, !tbaa !2
  %180 = load i8*, i8** %left_col, align 4, !tbaa !2
  %181 = load i8, i8* %filter_intra_mode.addr, align 1, !tbaa !10
  %conv212 = zext i8 %181 to i32
  call void @av1_filter_intra_predictor_c(i8* %176, i32 %177, i8 zeroext %178, i8* %179, i8* %180, i32 %conv212)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end213:                                        ; preds = %if.end209
  %182 = load i32, i32* %is_dr_mode, align 4, !tbaa !8
  %tobool214 = icmp ne i32 %182, 0
  br i1 %tobool214, label %if.then215, label %if.end308

if.then215:                                       ; preds = %if.end213
  %183 = bitcast i32* %upsample_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %183) #6
  store i32 0, i32* %upsample_above, align 4, !tbaa !8
  %184 = bitcast i32* %upsample_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %184) #6
  store i32 0, i32* %upsample_left, align 4, !tbaa !8
  %185 = load i32, i32* %disable_edge_filter.addr, align 4, !tbaa !8
  %tobool216 = icmp ne i32 %185, 0
  br i1 %tobool216, label %if.end307, label %if.then217

if.then217:                                       ; preds = %if.then215
  %186 = bitcast i32* %need_right218 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %186) #6
  %187 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp219 = icmp slt i32 %187, 90
  %conv220 = zext i1 %cmp219 to i32
  store i32 %conv220, i32* %need_right218, align 4, !tbaa !8
  %188 = bitcast i32* %need_bottom221 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %188) #6
  %189 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp222 = icmp sgt i32 %189, 180
  %conv223 = zext i1 %cmp222 to i32
  store i32 %conv223, i32* %need_bottom221, align 4, !tbaa !8
  %190 = bitcast i32* %filt_type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %190) #6
  %191 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %192 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %call224 = call i32 @get_filt_type(%struct.macroblockd* %191, i32 %192)
  store i32 %call224, i32* %filt_type, align 4, !tbaa !8
  %193 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp225 = icmp ne i32 %193, 90
  br i1 %cmp225, label %land.lhs.true227, label %if.end278

land.lhs.true227:                                 ; preds = %if.then217
  %194 = load i32, i32* %p_angle, align 4, !tbaa !8
  %cmp228 = icmp ne i32 %194, 180
  br i1 %cmp228, label %if.then230, label %if.end278

if.then230:                                       ; preds = %land.lhs.true227
  %195 = bitcast i32* %ab_le to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %195) #6
  %196 = load i32, i32* %need_above_left, align 4, !tbaa !8
  %tobool231 = icmp ne i32 %196, 0
  %197 = zext i1 %tobool231 to i64
  %cond232 = select i1 %tobool231, i32 1, i32 0
  store i32 %cond232, i32* %ab_le, align 4, !tbaa !8
  %198 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool233 = icmp ne i32 %198, 0
  br i1 %tobool233, label %land.lhs.true234, label %if.end241

land.lhs.true234:                                 ; preds = %if.then230
  %199 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool235 = icmp ne i32 %199, 0
  br i1 %tobool235, label %land.lhs.true236, label %if.end241

land.lhs.true236:                                 ; preds = %land.lhs.true234
  %200 = load i32, i32* %txwpx, align 4, !tbaa !8
  %201 = load i32, i32* %txhpx, align 4, !tbaa !8
  %add237 = add nsw i32 %200, %201
  %cmp238 = icmp sge i32 %add237, 24
  br i1 %cmp238, label %if.then240, label %if.end241

if.then240:                                       ; preds = %land.lhs.true236
  %202 = load i8*, i8** %above_row, align 4, !tbaa !2
  %203 = load i8*, i8** %left_col, align 4, !tbaa !2
  call void @filter_intra_edge_corner(i8* %202, i8* %203)
  br label %if.end241

if.end241:                                        ; preds = %if.then240, %land.lhs.true236, %land.lhs.true234, %if.then230
  %204 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool242 = icmp ne i32 %204, 0
  br i1 %tobool242, label %land.lhs.true243, label %if.end258

land.lhs.true243:                                 ; preds = %if.end241
  %205 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp244 = icmp sgt i32 %205, 0
  br i1 %cmp244, label %if.then246, label %if.end258

if.then246:                                       ; preds = %land.lhs.true243
  %206 = bitcast i32* %strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %206) #6
  %207 = load i32, i32* %txwpx, align 4, !tbaa !8
  %208 = load i32, i32* %txhpx, align 4, !tbaa !8
  %209 = load i32, i32* %p_angle, align 4, !tbaa !8
  %sub247 = sub nsw i32 %209, 90
  %210 = load i32, i32* %filt_type, align 4, !tbaa !8
  %call248 = call i32 @intra_edge_filter_strength(i32 %207, i32 %208, i32 %sub247, i32 %210)
  store i32 %call248, i32* %strength, align 4, !tbaa !8
  %211 = bitcast i32* %n_px to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %211) #6
  %212 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %213 = load i32, i32* %ab_le, align 4, !tbaa !8
  %add249 = add nsw i32 %212, %213
  %214 = load i32, i32* %need_right218, align 4, !tbaa !8
  %tobool250 = icmp ne i32 %214, 0
  br i1 %tobool250, label %cond.true251, label %cond.false252

cond.true251:                                     ; preds = %if.then246
  %215 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end253

cond.false252:                                    ; preds = %if.then246
  br label %cond.end253

cond.end253:                                      ; preds = %cond.false252, %cond.true251
  %cond254 = phi i32 [ %215, %cond.true251 ], [ 0, %cond.false252 ]
  %add255 = add nsw i32 %add249, %cond254
  store i32 %add255, i32* %n_px, align 4, !tbaa !8
  %216 = load i8*, i8** %above_row, align 4, !tbaa !2
  %217 = load i32, i32* %ab_le, align 4, !tbaa !8
  %idx.neg256 = sub i32 0, %217
  %add.ptr257 = getelementptr inbounds i8, i8* %216, i32 %idx.neg256
  %218 = load i32, i32* %n_px, align 4, !tbaa !8
  %219 = load i32, i32* %strength, align 4, !tbaa !8
  call void @av1_filter_intra_edge_c(i8* %add.ptr257, i32 %218, i32 %219)
  %220 = bitcast i32* %n_px to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #6
  %221 = bitcast i32* %strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #6
  br label %if.end258

if.end258:                                        ; preds = %cond.end253, %land.lhs.true243, %if.end241
  %222 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool259 = icmp ne i32 %222, 0
  br i1 %tobool259, label %land.lhs.true260, label %if.end277

land.lhs.true260:                                 ; preds = %if.end258
  %223 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp261 = icmp sgt i32 %223, 0
  br i1 %cmp261, label %if.then263, label %if.end277

if.then263:                                       ; preds = %land.lhs.true260
  %224 = bitcast i32* %strength264 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %224) #6
  %225 = load i32, i32* %txhpx, align 4, !tbaa !8
  %226 = load i32, i32* %txwpx, align 4, !tbaa !8
  %227 = load i32, i32* %p_angle, align 4, !tbaa !8
  %sub265 = sub nsw i32 %227, 180
  %228 = load i32, i32* %filt_type, align 4, !tbaa !8
  %call266 = call i32 @intra_edge_filter_strength(i32 %225, i32 %226, i32 %sub265, i32 %228)
  store i32 %call266, i32* %strength264, align 4, !tbaa !8
  %229 = bitcast i32* %n_px267 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %229) #6
  %230 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %231 = load i32, i32* %ab_le, align 4, !tbaa !8
  %add268 = add nsw i32 %230, %231
  %232 = load i32, i32* %need_bottom221, align 4, !tbaa !8
  %tobool269 = icmp ne i32 %232, 0
  br i1 %tobool269, label %cond.true270, label %cond.false271

cond.true270:                                     ; preds = %if.then263
  %233 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end272

cond.false271:                                    ; preds = %if.then263
  br label %cond.end272

cond.end272:                                      ; preds = %cond.false271, %cond.true270
  %cond273 = phi i32 [ %233, %cond.true270 ], [ 0, %cond.false271 ]
  %add274 = add nsw i32 %add268, %cond273
  store i32 %add274, i32* %n_px267, align 4, !tbaa !8
  %234 = load i8*, i8** %left_col, align 4, !tbaa !2
  %235 = load i32, i32* %ab_le, align 4, !tbaa !8
  %idx.neg275 = sub i32 0, %235
  %add.ptr276 = getelementptr inbounds i8, i8* %234, i32 %idx.neg275
  %236 = load i32, i32* %n_px267, align 4, !tbaa !8
  %237 = load i32, i32* %strength264, align 4, !tbaa !8
  call void @av1_filter_intra_edge_c(i8* %add.ptr276, i32 %236, i32 %237)
  %238 = bitcast i32* %n_px267 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #6
  %239 = bitcast i32* %strength264 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #6
  br label %if.end277

if.end277:                                        ; preds = %cond.end272, %land.lhs.true260, %if.end258
  %240 = bitcast i32* %ab_le to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #6
  br label %if.end278

if.end278:                                        ; preds = %if.end277, %land.lhs.true227, %if.then217
  %241 = load i32, i32* %txwpx, align 4, !tbaa !8
  %242 = load i32, i32* %txhpx, align 4, !tbaa !8
  %243 = load i32, i32* %p_angle, align 4, !tbaa !8
  %sub279 = sub nsw i32 %243, 90
  %244 = load i32, i32* %filt_type, align 4, !tbaa !8
  %call280 = call i32 @av1_use_intra_edge_upsample(i32 %241, i32 %242, i32 %sub279, i32 %244)
  store i32 %call280, i32* %upsample_above, align 4, !tbaa !8
  %245 = load i32, i32* %need_above, align 4, !tbaa !8
  %tobool281 = icmp ne i32 %245, 0
  br i1 %tobool281, label %land.lhs.true282, label %if.end292

land.lhs.true282:                                 ; preds = %if.end278
  %246 = load i32, i32* %upsample_above, align 4, !tbaa !8
  %tobool283 = icmp ne i32 %246, 0
  br i1 %tobool283, label %if.then284, label %if.end292

if.then284:                                       ; preds = %land.lhs.true282
  %247 = bitcast i32* %n_px285 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %247) #6
  %248 = load i32, i32* %txwpx, align 4, !tbaa !8
  %249 = load i32, i32* %need_right218, align 4, !tbaa !8
  %tobool286 = icmp ne i32 %249, 0
  br i1 %tobool286, label %cond.true287, label %cond.false288

cond.true287:                                     ; preds = %if.then284
  %250 = load i32, i32* %txhpx, align 4, !tbaa !8
  br label %cond.end289

cond.false288:                                    ; preds = %if.then284
  br label %cond.end289

cond.end289:                                      ; preds = %cond.false288, %cond.true287
  %cond290 = phi i32 [ %250, %cond.true287 ], [ 0, %cond.false288 ]
  %add291 = add nsw i32 %248, %cond290
  store i32 %add291, i32* %n_px285, align 4, !tbaa !8
  %251 = load i8*, i8** %above_row, align 4, !tbaa !2
  %252 = load i32, i32* %n_px285, align 4, !tbaa !8
  call void @av1_upsample_intra_edge_c(i8* %251, i32 %252)
  %253 = bitcast i32* %n_px285 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #6
  br label %if.end292

if.end292:                                        ; preds = %cond.end289, %land.lhs.true282, %if.end278
  %254 = load i32, i32* %txhpx, align 4, !tbaa !8
  %255 = load i32, i32* %txwpx, align 4, !tbaa !8
  %256 = load i32, i32* %p_angle, align 4, !tbaa !8
  %sub293 = sub nsw i32 %256, 180
  %257 = load i32, i32* %filt_type, align 4, !tbaa !8
  %call294 = call i32 @av1_use_intra_edge_upsample(i32 %254, i32 %255, i32 %sub293, i32 %257)
  store i32 %call294, i32* %upsample_left, align 4, !tbaa !8
  %258 = load i32, i32* %need_left, align 4, !tbaa !8
  %tobool295 = icmp ne i32 %258, 0
  br i1 %tobool295, label %land.lhs.true296, label %if.end306

land.lhs.true296:                                 ; preds = %if.end292
  %259 = load i32, i32* %upsample_left, align 4, !tbaa !8
  %tobool297 = icmp ne i32 %259, 0
  br i1 %tobool297, label %if.then298, label %if.end306

if.then298:                                       ; preds = %land.lhs.true296
  %260 = bitcast i32* %n_px299 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %260) #6
  %261 = load i32, i32* %txhpx, align 4, !tbaa !8
  %262 = load i32, i32* %need_bottom221, align 4, !tbaa !8
  %tobool300 = icmp ne i32 %262, 0
  br i1 %tobool300, label %cond.true301, label %cond.false302

cond.true301:                                     ; preds = %if.then298
  %263 = load i32, i32* %txwpx, align 4, !tbaa !8
  br label %cond.end303

cond.false302:                                    ; preds = %if.then298
  br label %cond.end303

cond.end303:                                      ; preds = %cond.false302, %cond.true301
  %cond304 = phi i32 [ %263, %cond.true301 ], [ 0, %cond.false302 ]
  %add305 = add nsw i32 %261, %cond304
  store i32 %add305, i32* %n_px299, align 4, !tbaa !8
  %264 = load i8*, i8** %left_col, align 4, !tbaa !2
  %265 = load i32, i32* %n_px299, align 4, !tbaa !8
  call void @av1_upsample_intra_edge_c(i8* %264, i32 %265)
  %266 = bitcast i32* %n_px299 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #6
  br label %if.end306

if.end306:                                        ; preds = %cond.end303, %land.lhs.true296, %if.end292
  %267 = bitcast i32* %filt_type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #6
  %268 = bitcast i32* %need_bottom221 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #6
  %269 = bitcast i32* %need_right218 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #6
  br label %if.end307

if.end307:                                        ; preds = %if.end306, %if.then215
  %270 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %271 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %272 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %273 = load i8*, i8** %above_row, align 4, !tbaa !2
  %274 = load i8*, i8** %left_col, align 4, !tbaa !2
  %275 = load i32, i32* %upsample_above, align 4, !tbaa !8
  %276 = load i32, i32* %upsample_left, align 4, !tbaa !8
  %277 = load i32, i32* %p_angle, align 4, !tbaa !8
  call void @dr_predictor(i8* %270, i32 %271, i8 zeroext %272, i8* %273, i8* %274, i32 %275, i32 %276, i32 %277)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %278 = bitcast i32* %upsample_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #6
  %279 = bitcast i32* %upsample_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #6
  br label %cleanup

if.end308:                                        ; preds = %if.end213
  %280 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %conv309 = zext i8 %280 to i32
  %cmp310 = icmp eq i32 %conv309, 0
  br i1 %cmp310, label %if.then312, label %if.else321

if.then312:                                       ; preds = %if.end308
  %281 = load i32, i32* %n_left_px.addr, align 4, !tbaa !8
  %cmp313 = icmp sgt i32 %281, 0
  %conv314 = zext i1 %cmp313 to i32
  %arrayidx315 = getelementptr inbounds [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 %conv314
  %282 = load i32, i32* %n_top_px.addr, align 4, !tbaa !8
  %cmp316 = icmp sgt i32 %282, 0
  %conv317 = zext i1 %cmp316 to i32
  %arrayidx318 = getelementptr inbounds [2 x [19 x void (i8*, i32, i8*, i8*)*]], [2 x [19 x void (i8*, i32, i8*, i8*)*]]* %arrayidx315, i32 0, i32 %conv317
  %283 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom319 = zext i8 %283 to i32
  %arrayidx320 = getelementptr inbounds [19 x void (i8*, i32, i8*, i8*)*], [19 x void (i8*, i32, i8*, i8*)*]* %arrayidx318, i32 0, i32 %idxprom319
  %284 = load void (i8*, i32, i8*, i8*)*, void (i8*, i32, i8*, i8*)** %arrayidx320, align 4, !tbaa !2
  %285 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %286 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %287 = load i8*, i8** %above_row, align 4, !tbaa !2
  %288 = load i8*, i8** %left_col, align 4, !tbaa !2
  call void %284(i8* %285, i32 %286, i8* %287, i8* %288)
  br label %if.end326

if.else321:                                       ; preds = %if.end308
  %289 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom322 = zext i8 %289 to i32
  %arrayidx323 = getelementptr inbounds [13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 %idxprom322
  %290 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom324 = zext i8 %290 to i32
  %arrayidx325 = getelementptr inbounds [19 x void (i8*, i32, i8*, i8*)*], [19 x void (i8*, i32, i8*, i8*)*]* %arrayidx323, i32 0, i32 %idxprom324
  %291 = load void (i8*, i32, i8*, i8*)*, void (i8*, i32, i8*, i8*)** %arrayidx325, align 4, !tbaa !2
  %292 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %293 = load i32, i32* %dst_stride.addr, align 4, !tbaa !8
  %294 = load i8*, i8** %above_row, align 4, !tbaa !2
  %295 = load i8*, i8** %left_col, align 4, !tbaa !2
  call void %291(i8* %292, i32 %293, i8* %294, i8* %295)
  br label %if.end326

if.end326:                                        ; preds = %if.else321, %if.then312
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end326, %if.end307, %if.then211, %for.end
  %296 = bitcast i32* %use_filter_intra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #6
  %297 = bitcast i32* %is_dr_mode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %297) #6
  %298 = bitcast i32* %p_angle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #6
  %299 = bitcast i32* %need_above_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %299) #6
  %300 = bitcast i32* %need_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %300) #6
  %301 = bitcast i32* %need_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %301) #6
  %302 = bitcast i32* %txhpx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %302) #6
  %303 = bitcast i32* %txwpx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %303) #6
  %304 = bitcast i8** %left_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %304) #6
  %305 = bitcast i8** %above_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %305) #6
  %306 = bitcast [160 x i8]* %above_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 160, i8* %306) #6
  %307 = bitcast [160 x i8]* %left_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 160, i8* %307) #6
  %308 = bitcast i8** %left_ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %308) #6
  %309 = bitcast i8** %above_ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %309) #6
  %310 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %310) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @av1_predict_intra_block_facade(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane, i32 %blk_col, i32 %blk_row, i8 zeroext %tx_size) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %blk_col.addr = alloca i32, align 4
  %blk_row.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %dst_stride = alloca i32, align 4
  %dst3 = alloca i8*, align 4
  %mode = alloca i8, align 1
  %use_palette = alloca i32, align 4
  %filter_intra_mode = alloca i8, align 1
  %angle_delta = alloca i32, align 4
  %cfl = alloca %struct.cfl_ctx*, align 4
  %pred_plane = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !8
  store i32 %blk_col, i32* %blk_col.addr, align 4, !tbaa !8
  store i32 %blk_row, i32* %blk_row.addr, align 4, !tbaa !8
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %4 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 4
  %6 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane1, i32 0, i32 %6
  store %struct.macroblockd_plane* %arrayidx2, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %7 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %8, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 4
  %9 = load i32, i32* %stride, align 4, !tbaa !67
  store i32 %9, i32* %dst_stride, align 4, !tbaa !8
  %10 = bitcast i8** %dst3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst4 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %11, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst4, i32 0, i32 0
  %12 = load i8*, i8** %buf, align 4, !tbaa !68
  %13 = load i32, i32* %blk_row.addr, align 4, !tbaa !8
  %14 = load i32, i32* %dst_stride, align 4, !tbaa !8
  %mul = mul nsw i32 %13, %14
  %15 = load i32, i32* %blk_col.addr, align 4, !tbaa !8
  %add = add nsw i32 %mul, %15
  %shl = shl i32 %add, 2
  %arrayidx5 = getelementptr inbounds i8, i8* %12, i32 %shl
  store i8* %arrayidx5, i8** %dst3, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mode) #6
  %16 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %cmp = icmp eq i32 %16, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %17 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %mode6 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %17, i32 0, i32 7
  %18 = load i8, i8* %mode6, align 1, !tbaa !69
  %conv = zext i8 %18 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %19 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %uv_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %19, i32 0, i32 8
  %20 = load i8, i8* %uv_mode, align 4, !tbaa !70
  %call = call zeroext i8 @get_uv_mode(i8 zeroext %20)
  %conv7 = zext i8 %call to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %conv7, %cond.false ]
  %conv8 = trunc i32 %cond to i8
  store i8 %conv8, i8* %mode, align 1, !tbaa !10
  %21 = bitcast i32* %use_palette to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %22, i32 0, i32 5
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 1
  %23 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %cmp9 = icmp ne i32 %23, 0
  %conv10 = zext i1 %cmp9 to i32
  %arrayidx11 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 %conv10
  %24 = load i8, i8* %arrayidx11, align 1, !tbaa !10
  %conv12 = zext i8 %24 to i32
  %cmp13 = icmp sgt i32 %conv12, 0
  %conv14 = zext i1 %cmp13 to i32
  store i32 %conv14, i32* %use_palette, align 4, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %filter_intra_mode) #6
  %25 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %cmp15 = icmp eq i32 %25, 0
  br i1 %cmp15, label %land.lhs.true, label %cond.false22

land.lhs.true:                                    ; preds = %cond.end
  %26 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %filter_intra_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %26, i32 0, i32 13
  %use_filter_intra = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %filter_intra_mode_info, i32 0, i32 1
  %27 = load i8, i8* %use_filter_intra, align 1, !tbaa !71
  %conv17 = zext i8 %27 to i32
  %tobool = icmp ne i32 %conv17, 0
  br i1 %tobool, label %cond.true18, label %cond.false22

cond.true18:                                      ; preds = %land.lhs.true
  %28 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %filter_intra_mode_info19 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %28, i32 0, i32 13
  %filter_intra_mode20 = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %filter_intra_mode_info19, i32 0, i32 0
  %29 = load i8, i8* %filter_intra_mode20, align 2, !tbaa !72
  %conv21 = zext i8 %29 to i32
  br label %cond.end23

cond.false22:                                     ; preds = %land.lhs.true, %cond.end
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false22, %cond.true18
  %cond24 = phi i32 [ %conv21, %cond.true18 ], [ 5, %cond.false22 ]
  %conv25 = trunc i32 %cond24 to i8
  store i8 %conv25, i8* %filter_intra_mode, align 1, !tbaa !10
  %30 = bitcast i32* %angle_delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %angle_delta26 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %31, i32 0, i32 20
  %32 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %cmp27 = icmp ne i32 %32, 0
  %conv28 = zext i1 %cmp27 to i32
  %arrayidx29 = getelementptr inbounds [2 x i8], [2 x i8]* %angle_delta26, i32 0, i32 %conv28
  %33 = load i8, i8* %arrayidx29, align 1, !tbaa !10
  %conv30 = sext i8 %33 to i32
  %mul31 = mul nsw i32 %conv30, 3
  store i32 %mul31, i32* %angle_delta, align 4, !tbaa !8
  %34 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %cmp32 = icmp ne i32 %34, 0
  br i1 %cmp32, label %land.lhs.true34, label %if.end56

land.lhs.true34:                                  ; preds = %cond.end23
  %35 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %uv_mode35 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %35, i32 0, i32 8
  %36 = load i8, i8* %uv_mode35, align 4, !tbaa !70
  %conv36 = zext i8 %36 to i32
  %cmp37 = icmp eq i32 %conv36, 13
  br i1 %cmp37, label %if.then, label %if.end56

if.then:                                          ; preds = %land.lhs.true34
  %37 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #6
  %38 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cfl39 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %38, i32 0, i32 56
  store %struct.cfl_ctx* %cfl39, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %pred_plane) #6
  %39 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %conv40 = trunc i32 %39 to i8
  %call41 = call zeroext i8 @get_cfl_pred_type(i8 zeroext %conv40)
  store i8 %call41, i8* %pred_plane, align 1, !tbaa !10
  %40 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %dc_pred_is_cached = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %40, i32 0, i32 2
  %41 = load i8, i8* %pred_plane, align 1, !tbaa !10
  %idxprom = zext i8 %41 to i32
  %arrayidx42 = getelementptr inbounds [2 x i32], [2 x i32]* %dc_pred_is_cached, i32 0, i32 %idxprom
  %42 = load i32, i32* %arrayidx42, align 4, !tbaa !8
  %cmp43 = icmp eq i32 %42, 0
  br i1 %cmp43, label %if.then45, label %if.else

if.then45:                                        ; preds = %if.then
  %43 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %44 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %45 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %45, i32 0, i32 12
  %46 = load i8, i8* %width, align 4, !tbaa !73
  %conv46 = zext i8 %46 to i32
  %47 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %47, i32 0, i32 13
  %48 = load i8, i8* %height, align 1, !tbaa !74
  %conv47 = zext i8 %48 to i32
  %49 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %50 = load i8, i8* %mode, align 1, !tbaa !10
  %51 = load i32, i32* %angle_delta, align 4, !tbaa !8
  %52 = load i32, i32* %use_palette, align 4, !tbaa !8
  %53 = load i8, i8* %filter_intra_mode, align 1, !tbaa !10
  %54 = load i8*, i8** %dst3, align 4, !tbaa !2
  %55 = load i32, i32* %dst_stride, align 4, !tbaa !8
  %56 = load i8*, i8** %dst3, align 4, !tbaa !2
  %57 = load i32, i32* %dst_stride, align 4, !tbaa !8
  %58 = load i32, i32* %blk_col.addr, align 4, !tbaa !8
  %59 = load i32, i32* %blk_row.addr, align 4, !tbaa !8
  %60 = load i32, i32* %plane.addr, align 4, !tbaa !8
  call void @av1_predict_intra_block(%struct.AV1Common* %43, %struct.macroblockd* %44, i32 %conv46, i32 %conv47, i8 zeroext %49, i8 zeroext %50, i32 %51, i32 %52, i8 zeroext %53, i8* %54, i32 %55, i8* %56, i32 %57, i32 %58, i32 %59, i32 %60)
  %61 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %use_dc_pred_cache = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %61, i32 0, i32 3
  %62 = load i32, i32* %use_dc_pred_cache, align 4, !tbaa !75
  %tobool48 = icmp ne i32 %62, 0
  br i1 %tobool48, label %if.then49, label %if.end

if.then49:                                        ; preds = %if.then45
  %63 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %64 = load i8*, i8** %dst3, align 4, !tbaa !2
  %65 = load i8, i8* %pred_plane, align 1, !tbaa !10
  %66 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom50 = zext i8 %66 to i32
  %arrayidx51 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom50
  %67 = load i32, i32* %arrayidx51, align 4, !tbaa !8
  call void @cfl_store_dc_pred(%struct.macroblockd* %63, i8* %64, i8 zeroext %65, i32 %67)
  %68 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %dc_pred_is_cached52 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %68, i32 0, i32 2
  %69 = load i8, i8* %pred_plane, align 1, !tbaa !10
  %idxprom53 = zext i8 %69 to i32
  %arrayidx54 = getelementptr inbounds [2 x i32], [2 x i32]* %dc_pred_is_cached52, i32 0, i32 %idxprom53
  store i32 1, i32* %arrayidx54, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then49, %if.then45
  br label %if.end55

if.else:                                          ; preds = %if.then
  %70 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %71 = load i8*, i8** %dst3, align 4, !tbaa !2
  %72 = load i32, i32* %dst_stride, align 4, !tbaa !8
  %73 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %74 = load i8, i8* %pred_plane, align 1, !tbaa !10
  call void @cfl_load_dc_pred(%struct.macroblockd* %70, i8* %71, i32 %72, i8 zeroext %73, i8 zeroext %74)
  br label %if.end55

if.end55:                                         ; preds = %if.else, %if.end
  %75 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %76 = load i8*, i8** %dst3, align 4, !tbaa !2
  %77 = load i32, i32* %dst_stride, align 4, !tbaa !8
  %78 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %79 = load i32, i32* %plane.addr, align 4, !tbaa !8
  call void @cfl_predict_block(%struct.macroblockd* %75, i8* %76, i32 %77, i8 zeroext %78, i32 %79)
  store i32 1, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %pred_plane) #6
  %80 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #6
  br label %cleanup

if.end56:                                         ; preds = %land.lhs.true34, %cond.end23
  %81 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %82 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %83 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %width57 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %83, i32 0, i32 12
  %84 = load i8, i8* %width57, align 4, !tbaa !73
  %conv58 = zext i8 %84 to i32
  %85 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %height59 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %85, i32 0, i32 13
  %86 = load i8, i8* %height59, align 1, !tbaa !74
  %conv60 = zext i8 %86 to i32
  %87 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %88 = load i8, i8* %mode, align 1, !tbaa !10
  %89 = load i32, i32* %angle_delta, align 4, !tbaa !8
  %90 = load i32, i32* %use_palette, align 4, !tbaa !8
  %91 = load i8, i8* %filter_intra_mode, align 1, !tbaa !10
  %92 = load i8*, i8** %dst3, align 4, !tbaa !2
  %93 = load i32, i32* %dst_stride, align 4, !tbaa !8
  %94 = load i8*, i8** %dst3, align 4, !tbaa !2
  %95 = load i32, i32* %dst_stride, align 4, !tbaa !8
  %96 = load i32, i32* %blk_col.addr, align 4, !tbaa !8
  %97 = load i32, i32* %blk_row.addr, align 4, !tbaa !8
  %98 = load i32, i32* %plane.addr, align 4, !tbaa !8
  call void @av1_predict_intra_block(%struct.AV1Common* %81, %struct.macroblockd* %82, i32 %conv58, i32 %conv60, i8 zeroext %87, i8 zeroext %88, i32 %89, i32 %90, i8 zeroext %91, i8* %92, i32 %93, i8* %94, i32 %95, i32 %96, i32 %97, i32 %98)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end56, %if.end55
  %99 = bitcast i32* %angle_delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %filter_intra_mode) #6
  %100 = bitcast i32* %use_palette to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mode) #6
  %101 = bitcast i8** %dst3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #6
  %102 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  %103 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_uv_mode(i8 zeroext %mode) #4 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !10
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [16 x i8], [16 x i8]* @get_uv_mode.uv2y, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !10
  ret i8 %1
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_cfl_pred_type(i8 zeroext %plane) #4 {
entry:
  %plane.addr = alloca i8, align 1
  store i8 %plane, i8* %plane.addr, align 1, !tbaa !10
  %0 = load i8, i8* %plane.addr, align 1, !tbaa !10
  %conv = zext i8 %0 to i32
  %sub = sub nsw i32 %conv, 1
  %conv1 = trunc i32 %sub to i8
  ret i8 %conv1
}

declare void @cfl_store_dc_pred(%struct.macroblockd*, i8*, i8 zeroext, i32) #3

declare void @cfl_load_dc_pred(%struct.macroblockd*, i8*, i32, i8 zeroext, i8 zeroext) #3

declare void @cfl_predict_block(%struct.macroblockd*, i8*, i32, i8 zeroext, i32) #3

; Function Attrs: nounwind
define hidden void @av1_init_intra_predictors() #0 {
entry:
  call void @aom_once(void ()* @init_intra_predictors_internal)
  ret void
}

; Function Attrs: nounwind
define internal void @aom_once(void ()* %func) #0 {
entry:
  %func.addr = alloca void ()*, align 4
  store void ()* %func, void ()** %func.addr, align 4, !tbaa !2
  %0 = load i32, i32* @aom_once.done, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %1 = load void ()*, void ()** %func.addr, align 4, !tbaa !2
  call void %1()
  store i32 1, i32* @aom_once.done, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define internal void @init_intra_predictors_internal() #0 {
entry:
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 0), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 2), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 4), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 6), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 8), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 10), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 12), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 14), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 16), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_v_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1, i32 18), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 0), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 2), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 4), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 6), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 8), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 10), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 12), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 14), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 16), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_h_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2, i32 18), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 0), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 2), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 4), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 6), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 8), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 10), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 12), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 14), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 16), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_paeth_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 12, i32 18), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 0), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 2), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 4), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 6), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 8), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 10), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 12), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 14), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 16), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 9, i32 18), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 0), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 2), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 4), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 6), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 8), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 10), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 12), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 14), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 16), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_v_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 10, i32 18), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 0), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 2), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 4), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 6), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 8), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 10), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 12), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 14), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 16), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_smooth_h_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 11, i32 18), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 0), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 2), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 4), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 6), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 8), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 10), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 12), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 14), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 16), align 16, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_128_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 0, i32 18), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 0), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 2), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 4), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 6), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 8), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 10), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 12), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 14), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 16), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_top_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 0, i32 1, i32 18), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 0), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 2), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 4), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 6), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 8), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 10), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 12), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 14), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 16), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_left_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 0, i32 18), align 8, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_4x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 0), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_8x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 1), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_16x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 2), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_32x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 3), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_64x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 4), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_4x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 5), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_8x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 6), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_8x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 7), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_16x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 8), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_16x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 9), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_32x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 10), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_32x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 11), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_64x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 12), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_4x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 13), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_16x4_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 14), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_8x32_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 15), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_32x8_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 16), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_16x64_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 17), align 4, !tbaa !2
  store void (i8*, i32, i8*, i8*)* @aom_dc_predictor_64x16_c, void (i8*, i32, i8*, i8*)** getelementptr inbounds ([2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]], [2 x [2 x [19 x void (i8*, i32, i8*, i8*)*]]]* @dc_pred, i32 0, i32 1, i32 1, i32 18), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 0), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 2), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 4), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 6), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 8), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 10), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 12), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 14), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 16), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_v_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1, i32 18), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 0), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 2), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 4), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 6), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 8), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 10), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 12), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 14), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 16), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_h_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2, i32 18), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 0), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 2), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 4), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 6), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 8), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 10), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 12), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 14), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 16), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_paeth_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 12, i32 18), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 0), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 2), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 4), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 6), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 8), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 10), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 12), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 14), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 16), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 9, i32 18), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 0), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 2), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 4), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 6), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 8), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 10), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 12), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 14), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 16), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_v_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 10, i32 18), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 0), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 2), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 4), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 6), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 8), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 10), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 12), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 14), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 16), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_smooth_h_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 11, i32 18), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 0), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 2), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 4), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 6), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 8), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 10), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 12), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 14), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 16), align 16, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_128_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 0, i32 18), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 0), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 2), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 4), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 6), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 8), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 10), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 12), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 14), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 16), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_top_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 0, i32 1, i32 18), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 0), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 2), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 4), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 6), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 8), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 10), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 12), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 14), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 16), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_left_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 0, i32 18), align 8, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_4x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 0), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_8x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 1), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_16x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 2), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_32x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 3), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_64x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 4), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_4x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 5), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_8x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 6), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_8x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 7), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_16x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 8), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_16x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 9), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_32x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 10), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_32x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 11), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_64x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 12), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_4x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 13), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_16x4_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 14), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_8x32_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 15), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_32x8_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 16), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_16x64_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 17), align 4, !tbaa !2
  store void (i16*, i32, i16*, i16*, i32)* @aom_highbd_dc_predictor_64x16_c, void (i16*, i32, i16*, i16*, i32)** getelementptr inbounds ([2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]], [2 x [2 x [19 x void (i16*, i32, i16*, i16*, i32)*]]]* @dc_pred_high, i32 0, i32 1, i32 1, i32 18), align 4, !tbaa !2
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #4 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !8
  store i32 %low, i32* %low.addr, align 4, !tbaa !8
  store i32 %high, i32* %high.addr, align 4, !tbaa !8
  %0 = load i32, i32* %value.addr, align 4, !tbaa !8
  %1 = load i32, i32* %low.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !8
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !8
  %4 = load i32, i32* %high.addr, align 4, !tbaa !8
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !8
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: nounwind
define internal i8* @get_has_tr_table(i8 zeroext %partition, i8 zeroext %bsize) #0 {
entry:
  %partition.addr = alloca i8, align 1
  %bsize.addr = alloca i8, align 1
  %ret = alloca i8*, align 4
  store i8 %partition, i8* %partition.addr, align 1, !tbaa !10
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !10
  %0 = bitcast i8** %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i8* null, i8** %ret, align 4, !tbaa !2
  %1 = load i8, i8* %partition.addr, align 1, !tbaa !10
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 6
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i8, i8* %partition.addr, align 1, !tbaa !10
  %conv2 = zext i8 %2 to i32
  %cmp3 = icmp eq i32 %conv2, 7
  br i1 %cmp3, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %3 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [16 x i8*], [16 x i8*]* @has_tr_vert_tables, i32 0, i32 %idxprom
  %4 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %4, i8** %ret, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  %5 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom5 = zext i8 %5 to i32
  %arrayidx6 = getelementptr inbounds [22 x i8*], [22 x i8*]* @has_tr_tables, i32 0, i32 %idxprom5
  %6 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %6, i8** %ret, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %7 = load i8*, i8** %ret, align 4, !tbaa !2
  %8 = bitcast i8** %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  ret i8* %7
}

; Function Attrs: nounwind
define internal i8* @get_has_bl_table(i8 zeroext %partition, i8 zeroext %bsize) #0 {
entry:
  %partition.addr = alloca i8, align 1
  %bsize.addr = alloca i8, align 1
  %ret = alloca i8*, align 4
  store i8 %partition, i8* %partition.addr, align 1, !tbaa !10
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !10
  %0 = bitcast i8** %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i8* null, i8** %ret, align 4, !tbaa !2
  %1 = load i8, i8* %partition.addr, align 1, !tbaa !10
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 6
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i8, i8* %partition.addr, align 1, !tbaa !10
  %conv2 = zext i8 %2 to i32
  %cmp3 = icmp eq i32 %conv2, 7
  br i1 %cmp3, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %3 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [16 x i8*], [16 x i8*]* @has_bl_vert_tables, i32 0, i32 %idxprom
  %4 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %4, i8** %ret, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  %5 = load i8, i8* %bsize.addr, align 1, !tbaa !10
  %idxprom5 = zext i8 %5 to i32
  %arrayidx6 = getelementptr inbounds [22 x i8*], [22 x i8*]* @has_bl_tables, i32 0, i32 %idxprom5
  %6 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %6, i8** %ret, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %7 = load i8*, i8** %ret, align 4, !tbaa !2
  %8 = bitcast i8** %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  ret i8* %7
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_directional_mode(i8 zeroext %mode) #4 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !10
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %conv = zext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i8, i8* %mode.addr, align 1, !tbaa !10
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp sle i32 %conv2, 8
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  %land.ext = zext i1 %2 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define internal void @highbd_filter_intra_predictor(i16* %dst, i32 %stride, i8 zeroext %tx_size, i16* %above, i16* %left, i32 %mode, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %mode.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %buffer = alloca [33 x [33 x i16]], align 16
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %p0 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p2 = alloca i16, align 2
  %p3 = alloca i16, align 2
  %p4 = alloca i16, align 2
  %p5 = alloca i16, align 2
  %p6 = alloca i16, align 2
  %k = alloca i32, align 4
  %r_offset = alloca i32, align 4
  %c_offset = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %mode, i32* %mode.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast [33 x [33 x i16]]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 2178, i8* %2) #6
  %3 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %4 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %5 = load i32, i32* %arrayidx, align 4, !tbaa !8
  store i32 %5, i32* %bw, align 4, !tbaa !8
  %6 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom1 = zext i8 %7 to i32
  %arrayidx2 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom1
  %8 = load i32, i32* %arrayidx2, align 4, !tbaa !8
  store i32 %8, i32* %bh, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %r, align 4, !tbaa !8
  %10 = load i32, i32* %bh, align 4, !tbaa !8
  %add = add nsw i32 %10, 1
  %cmp = icmp slt i32 %9, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %11
  %arraydecay = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx3, i32 0, i32 0
  %12 = bitcast i16* %arraydecay to i8*
  %13 = load i32, i32* %bw, align 4, !tbaa !8
  %add4 = add nsw i32 %13, 1
  %mul = mul i32 %add4, 2
  call void @llvm.memset.p0i8.i32(i8* align 2 %12, i8 0, i32 %mul, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc12, %for.end
  %15 = load i32, i32* %r, align 4, !tbaa !8
  %16 = load i32, i32* %bh, align 4, !tbaa !8
  %cmp6 = icmp slt i32 %15, %16
  br i1 %cmp6, label %for.body7, label %for.end14

for.body7:                                        ; preds = %for.cond5
  %17 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %18 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx8 = getelementptr inbounds i16, i16* %17, i32 %18
  %19 = load i16, i16* %arrayidx8, align 2, !tbaa !11
  %20 = load i32, i32* %r, align 4, !tbaa !8
  %add9 = add nsw i32 %20, 1
  %arrayidx10 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %add9
  %arrayidx11 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx10, i32 0, i32 0
  store i16 %19, i16* %arrayidx11, align 2, !tbaa !11
  br label %for.inc12

for.inc12:                                        ; preds = %for.body7
  %21 = load i32, i32* %r, align 4, !tbaa !8
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %r, align 4, !tbaa !8
  br label %for.cond5

for.end14:                                        ; preds = %for.cond5
  %arrayidx15 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 0
  %arraydecay16 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx15, i32 0, i32 0
  %22 = bitcast i16* %arraydecay16 to i8*
  %23 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i16, i16* %23, i32 -1
  %24 = bitcast i16* %arrayidx17 to i8*
  %25 = load i32, i32* %bw, align 4, !tbaa !8
  %add18 = add nsw i32 %25, 1
  %mul19 = mul i32 %add18, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %22, i8* align 2 %24, i32 %mul19, i1 false)
  store i32 1, i32* %r, align 4, !tbaa !8
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc217, %for.end14
  %26 = load i32, i32* %r, align 4, !tbaa !8
  %27 = load i32, i32* %bh, align 4, !tbaa !8
  %add21 = add nsw i32 %27, 1
  %cmp22 = icmp slt i32 %26, %add21
  br i1 %cmp22, label %for.body23, label %for.end219

for.body23:                                       ; preds = %for.cond20
  store i32 1, i32* %c, align 4, !tbaa !8
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc214, %for.body23
  %28 = load i32, i32* %c, align 4, !tbaa !8
  %29 = load i32, i32* %bw, align 4, !tbaa !8
  %add25 = add nsw i32 %29, 1
  %cmp26 = icmp slt i32 %28, %add25
  br i1 %cmp26, label %for.body27, label %for.end216

for.body27:                                       ; preds = %for.cond24
  %30 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %30) #6
  %31 = load i32, i32* %r, align 4, !tbaa !8
  %sub = sub nsw i32 %31, 1
  %arrayidx28 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %sub
  %32 = load i32, i32* %c, align 4, !tbaa !8
  %sub29 = sub nsw i32 %32, 1
  %arrayidx30 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx28, i32 0, i32 %sub29
  %33 = load i16, i16* %arrayidx30, align 2, !tbaa !11
  store i16 %33, i16* %p0, align 2, !tbaa !11
  %34 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %34) #6
  %35 = load i32, i32* %r, align 4, !tbaa !8
  %sub31 = sub nsw i32 %35, 1
  %arrayidx32 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %sub31
  %36 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx33 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx32, i32 0, i32 %36
  %37 = load i16, i16* %arrayidx33, align 2, !tbaa !11
  store i16 %37, i16* %p1, align 2, !tbaa !11
  %38 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %38) #6
  %39 = load i32, i32* %r, align 4, !tbaa !8
  %sub34 = sub nsw i32 %39, 1
  %arrayidx35 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %sub34
  %40 = load i32, i32* %c, align 4, !tbaa !8
  %add36 = add nsw i32 %40, 1
  %arrayidx37 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx35, i32 0, i32 %add36
  %41 = load i16, i16* %arrayidx37, align 2, !tbaa !11
  store i16 %41, i16* %p2, align 2, !tbaa !11
  %42 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %42) #6
  %43 = load i32, i32* %r, align 4, !tbaa !8
  %sub38 = sub nsw i32 %43, 1
  %arrayidx39 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %sub38
  %44 = load i32, i32* %c, align 4, !tbaa !8
  %add40 = add nsw i32 %44, 2
  %arrayidx41 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx39, i32 0, i32 %add40
  %45 = load i16, i16* %arrayidx41, align 2, !tbaa !11
  store i16 %45, i16* %p3, align 2, !tbaa !11
  %46 = bitcast i16* %p4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %46) #6
  %47 = load i32, i32* %r, align 4, !tbaa !8
  %sub42 = sub nsw i32 %47, 1
  %arrayidx43 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %sub42
  %48 = load i32, i32* %c, align 4, !tbaa !8
  %add44 = add nsw i32 %48, 3
  %arrayidx45 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx43, i32 0, i32 %add44
  %49 = load i16, i16* %arrayidx45, align 2, !tbaa !11
  store i16 %49, i16* %p4, align 2, !tbaa !11
  %50 = bitcast i16* %p5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %50) #6
  %51 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx46 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %51
  %52 = load i32, i32* %c, align 4, !tbaa !8
  %sub47 = sub nsw i32 %52, 1
  %arrayidx48 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx46, i32 0, i32 %sub47
  %53 = load i16, i16* %arrayidx48, align 2, !tbaa !11
  store i16 %53, i16* %p5, align 2, !tbaa !11
  %54 = bitcast i16* %p6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %54) #6
  %55 = load i32, i32* %r, align 4, !tbaa !8
  %add49 = add nsw i32 %55, 1
  %arrayidx50 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %add49
  %56 = load i32, i32* %c, align 4, !tbaa !8
  %sub51 = sub nsw i32 %56, 1
  %arrayidx52 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx50, i32 0, i32 %sub51
  %57 = load i16, i16* %arrayidx52, align 2, !tbaa !11
  store i16 %57, i16* %p6, align 2, !tbaa !11
  %58 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #6
  store i32 0, i32* %k, align 4, !tbaa !8
  br label %for.cond53

for.cond53:                                       ; preds = %for.inc211, %for.body27
  %59 = load i32, i32* %k, align 4, !tbaa !8
  %cmp54 = icmp slt i32 %59, 8
  br i1 %cmp54, label %for.body55, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond53
  %60 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  br label %for.end213

for.body55:                                       ; preds = %for.cond53
  %61 = bitcast i32* %r_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  %62 = load i32, i32* %k, align 4, !tbaa !8
  %shr = ashr i32 %62, 2
  store i32 %shr, i32* %r_offset, align 4, !tbaa !8
  %63 = bitcast i32* %c_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #6
  %64 = load i32, i32* %k, align 4, !tbaa !8
  %and = and i32 %64, 3
  store i32 %and, i32* %c_offset, align 4, !tbaa !8
  %65 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx56 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %65
  %66 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx57 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx56, i32 0, i32 %66
  %arrayidx58 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx57, i32 0, i32 0
  %67 = load i8, i8* %arrayidx58, align 8, !tbaa !10
  %conv = sext i8 %67 to i32
  %68 = load i16, i16* %p0, align 2, !tbaa !11
  %conv59 = zext i16 %68 to i32
  %mul60 = mul nsw i32 %conv, %conv59
  %69 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx61 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %69
  %70 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx62 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx61, i32 0, i32 %70
  %arrayidx63 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx62, i32 0, i32 1
  %71 = load i8, i8* %arrayidx63, align 1, !tbaa !10
  %conv64 = sext i8 %71 to i32
  %72 = load i16, i16* %p1, align 2, !tbaa !11
  %conv65 = zext i16 %72 to i32
  %mul66 = mul nsw i32 %conv64, %conv65
  %add67 = add nsw i32 %mul60, %mul66
  %73 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx68 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %73
  %74 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx69 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx68, i32 0, i32 %74
  %arrayidx70 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx69, i32 0, i32 2
  %75 = load i8, i8* %arrayidx70, align 2, !tbaa !10
  %conv71 = sext i8 %75 to i32
  %76 = load i16, i16* %p2, align 2, !tbaa !11
  %conv72 = zext i16 %76 to i32
  %mul73 = mul nsw i32 %conv71, %conv72
  %add74 = add nsw i32 %add67, %mul73
  %77 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx75 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %77
  %78 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx76 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx75, i32 0, i32 %78
  %arrayidx77 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx76, i32 0, i32 3
  %79 = load i8, i8* %arrayidx77, align 1, !tbaa !10
  %conv78 = sext i8 %79 to i32
  %80 = load i16, i16* %p3, align 2, !tbaa !11
  %conv79 = zext i16 %80 to i32
  %mul80 = mul nsw i32 %conv78, %conv79
  %add81 = add nsw i32 %add74, %mul80
  %81 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx82 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %81
  %82 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx83 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx82, i32 0, i32 %82
  %arrayidx84 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx83, i32 0, i32 4
  %83 = load i8, i8* %arrayidx84, align 4, !tbaa !10
  %conv85 = sext i8 %83 to i32
  %84 = load i16, i16* %p4, align 2, !tbaa !11
  %conv86 = zext i16 %84 to i32
  %mul87 = mul nsw i32 %conv85, %conv86
  %add88 = add nsw i32 %add81, %mul87
  %85 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx89 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %85
  %86 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx90 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx89, i32 0, i32 %86
  %arrayidx91 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx90, i32 0, i32 5
  %87 = load i8, i8* %arrayidx91, align 1, !tbaa !10
  %conv92 = sext i8 %87 to i32
  %88 = load i16, i16* %p5, align 2, !tbaa !11
  %conv93 = zext i16 %88 to i32
  %mul94 = mul nsw i32 %conv92, %conv93
  %add95 = add nsw i32 %add88, %mul94
  %89 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx96 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %89
  %90 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx97 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx96, i32 0, i32 %90
  %arrayidx98 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx97, i32 0, i32 6
  %91 = load i8, i8* %arrayidx98, align 2, !tbaa !10
  %conv99 = sext i8 %91 to i32
  %92 = load i16, i16* %p6, align 2, !tbaa !11
  %conv100 = zext i16 %92 to i32
  %mul101 = mul nsw i32 %conv99, %conv100
  %add102 = add nsw i32 %add95, %mul101
  %cmp103 = icmp slt i32 %add102, 0
  br i1 %cmp103, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body55
  %93 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx105 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %93
  %94 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx106 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx105, i32 0, i32 %94
  %arrayidx107 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx106, i32 0, i32 0
  %95 = load i8, i8* %arrayidx107, align 8, !tbaa !10
  %conv108 = sext i8 %95 to i32
  %96 = load i16, i16* %p0, align 2, !tbaa !11
  %conv109 = zext i16 %96 to i32
  %mul110 = mul nsw i32 %conv108, %conv109
  %97 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx111 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %97
  %98 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx112 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx111, i32 0, i32 %98
  %arrayidx113 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx112, i32 0, i32 1
  %99 = load i8, i8* %arrayidx113, align 1, !tbaa !10
  %conv114 = sext i8 %99 to i32
  %100 = load i16, i16* %p1, align 2, !tbaa !11
  %conv115 = zext i16 %100 to i32
  %mul116 = mul nsw i32 %conv114, %conv115
  %add117 = add nsw i32 %mul110, %mul116
  %101 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx118 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %101
  %102 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx119 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx118, i32 0, i32 %102
  %arrayidx120 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx119, i32 0, i32 2
  %103 = load i8, i8* %arrayidx120, align 2, !tbaa !10
  %conv121 = sext i8 %103 to i32
  %104 = load i16, i16* %p2, align 2, !tbaa !11
  %conv122 = zext i16 %104 to i32
  %mul123 = mul nsw i32 %conv121, %conv122
  %add124 = add nsw i32 %add117, %mul123
  %105 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx125 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %105
  %106 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx126 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx125, i32 0, i32 %106
  %arrayidx127 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx126, i32 0, i32 3
  %107 = load i8, i8* %arrayidx127, align 1, !tbaa !10
  %conv128 = sext i8 %107 to i32
  %108 = load i16, i16* %p3, align 2, !tbaa !11
  %conv129 = zext i16 %108 to i32
  %mul130 = mul nsw i32 %conv128, %conv129
  %add131 = add nsw i32 %add124, %mul130
  %109 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx132 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %109
  %110 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx133 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx132, i32 0, i32 %110
  %arrayidx134 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx133, i32 0, i32 4
  %111 = load i8, i8* %arrayidx134, align 4, !tbaa !10
  %conv135 = sext i8 %111 to i32
  %112 = load i16, i16* %p4, align 2, !tbaa !11
  %conv136 = zext i16 %112 to i32
  %mul137 = mul nsw i32 %conv135, %conv136
  %add138 = add nsw i32 %add131, %mul137
  %113 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx139 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %113
  %114 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx140 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx139, i32 0, i32 %114
  %arrayidx141 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx140, i32 0, i32 5
  %115 = load i8, i8* %arrayidx141, align 1, !tbaa !10
  %conv142 = sext i8 %115 to i32
  %116 = load i16, i16* %p5, align 2, !tbaa !11
  %conv143 = zext i16 %116 to i32
  %mul144 = mul nsw i32 %conv142, %conv143
  %add145 = add nsw i32 %add138, %mul144
  %117 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx146 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %117
  %118 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx147 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx146, i32 0, i32 %118
  %arrayidx148 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx147, i32 0, i32 6
  %119 = load i8, i8* %arrayidx148, align 2, !tbaa !10
  %conv149 = sext i8 %119 to i32
  %120 = load i16, i16* %p6, align 2, !tbaa !11
  %conv150 = zext i16 %120 to i32
  %mul151 = mul nsw i32 %conv149, %conv150
  %add152 = add nsw i32 %add145, %mul151
  %sub153 = sub nsw i32 0, %add152
  %add154 = add nsw i32 %sub153, 8
  %shr155 = ashr i32 %add154, 4
  %sub156 = sub nsw i32 0, %shr155
  br label %cond.end

cond.false:                                       ; preds = %for.body55
  %121 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx157 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %121
  %122 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx158 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx157, i32 0, i32 %122
  %arrayidx159 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx158, i32 0, i32 0
  %123 = load i8, i8* %arrayidx159, align 8, !tbaa !10
  %conv160 = sext i8 %123 to i32
  %124 = load i16, i16* %p0, align 2, !tbaa !11
  %conv161 = zext i16 %124 to i32
  %mul162 = mul nsw i32 %conv160, %conv161
  %125 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx163 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %125
  %126 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx164 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx163, i32 0, i32 %126
  %arrayidx165 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx164, i32 0, i32 1
  %127 = load i8, i8* %arrayidx165, align 1, !tbaa !10
  %conv166 = sext i8 %127 to i32
  %128 = load i16, i16* %p1, align 2, !tbaa !11
  %conv167 = zext i16 %128 to i32
  %mul168 = mul nsw i32 %conv166, %conv167
  %add169 = add nsw i32 %mul162, %mul168
  %129 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx170 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %129
  %130 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx171 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx170, i32 0, i32 %130
  %arrayidx172 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx171, i32 0, i32 2
  %131 = load i8, i8* %arrayidx172, align 2, !tbaa !10
  %conv173 = sext i8 %131 to i32
  %132 = load i16, i16* %p2, align 2, !tbaa !11
  %conv174 = zext i16 %132 to i32
  %mul175 = mul nsw i32 %conv173, %conv174
  %add176 = add nsw i32 %add169, %mul175
  %133 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx177 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %133
  %134 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx178 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx177, i32 0, i32 %134
  %arrayidx179 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx178, i32 0, i32 3
  %135 = load i8, i8* %arrayidx179, align 1, !tbaa !10
  %conv180 = sext i8 %135 to i32
  %136 = load i16, i16* %p3, align 2, !tbaa !11
  %conv181 = zext i16 %136 to i32
  %mul182 = mul nsw i32 %conv180, %conv181
  %add183 = add nsw i32 %add176, %mul182
  %137 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx184 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %137
  %138 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx185 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx184, i32 0, i32 %138
  %arrayidx186 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx185, i32 0, i32 4
  %139 = load i8, i8* %arrayidx186, align 4, !tbaa !10
  %conv187 = sext i8 %139 to i32
  %140 = load i16, i16* %p4, align 2, !tbaa !11
  %conv188 = zext i16 %140 to i32
  %mul189 = mul nsw i32 %conv187, %conv188
  %add190 = add nsw i32 %add183, %mul189
  %141 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx191 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %141
  %142 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx192 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx191, i32 0, i32 %142
  %arrayidx193 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx192, i32 0, i32 5
  %143 = load i8, i8* %arrayidx193, align 1, !tbaa !10
  %conv194 = sext i8 %143 to i32
  %144 = load i16, i16* %p5, align 2, !tbaa !11
  %conv195 = zext i16 %144 to i32
  %mul196 = mul nsw i32 %conv194, %conv195
  %add197 = add nsw i32 %add190, %mul196
  %145 = load i32, i32* %mode.addr, align 4, !tbaa !8
  %arrayidx198 = getelementptr inbounds [5 x [8 x [8 x i8]]], [5 x [8 x [8 x i8]]]* @av1_filter_intra_taps, i32 0, i32 %145
  %146 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx199 = getelementptr inbounds [8 x [8 x i8]], [8 x [8 x i8]]* %arrayidx198, i32 0, i32 %146
  %arrayidx200 = getelementptr inbounds [8 x i8], [8 x i8]* %arrayidx199, i32 0, i32 6
  %147 = load i8, i8* %arrayidx200, align 2, !tbaa !10
  %conv201 = sext i8 %147 to i32
  %148 = load i16, i16* %p6, align 2, !tbaa !11
  %conv202 = zext i16 %148 to i32
  %mul203 = mul nsw i32 %conv201, %conv202
  %add204 = add nsw i32 %add197, %mul203
  %add205 = add nsw i32 %add204, 8
  %shr206 = ashr i32 %add205, 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub156, %cond.true ], [ %shr206, %cond.false ]
  %149 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %call = call zeroext i16 @clip_pixel_highbd(i32 %cond, i32 %149)
  %150 = load i32, i32* %r, align 4, !tbaa !8
  %151 = load i32, i32* %r_offset, align 4, !tbaa !8
  %add207 = add nsw i32 %150, %151
  %arrayidx208 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %add207
  %152 = load i32, i32* %c, align 4, !tbaa !8
  %153 = load i32, i32* %c_offset, align 4, !tbaa !8
  %add209 = add nsw i32 %152, %153
  %arrayidx210 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx208, i32 0, i32 %add209
  store i16 %call, i16* %arrayidx210, align 2, !tbaa !11
  %154 = bitcast i32* %c_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #6
  %155 = bitcast i32* %r_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #6
  br label %for.inc211

for.inc211:                                       ; preds = %cond.end
  %156 = load i32, i32* %k, align 4, !tbaa !8
  %inc212 = add nsw i32 %156, 1
  store i32 %inc212, i32* %k, align 4, !tbaa !8
  br label %for.cond53

for.end213:                                       ; preds = %for.cond.cleanup
  %157 = bitcast i16* %p6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %157) #6
  %158 = bitcast i16* %p5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %158) #6
  %159 = bitcast i16* %p4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %159) #6
  %160 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %160) #6
  %161 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %161) #6
  %162 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %162) #6
  %163 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %163) #6
  br label %for.inc214

for.inc214:                                       ; preds = %for.end213
  %164 = load i32, i32* %c, align 4, !tbaa !8
  %add215 = add nsw i32 %164, 4
  store i32 %add215, i32* %c, align 4, !tbaa !8
  br label %for.cond24

for.end216:                                       ; preds = %for.cond24
  br label %for.inc217

for.inc217:                                       ; preds = %for.end216
  %165 = load i32, i32* %r, align 4, !tbaa !8
  %add218 = add nsw i32 %165, 2
  store i32 %add218, i32* %r, align 4, !tbaa !8
  br label %for.cond20

for.end219:                                       ; preds = %for.cond20
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond220

for.cond220:                                      ; preds = %for.inc228, %for.end219
  %166 = load i32, i32* %r, align 4, !tbaa !8
  %167 = load i32, i32* %bh, align 4, !tbaa !8
  %cmp221 = icmp slt i32 %166, %167
  br i1 %cmp221, label %for.body223, label %for.end230

for.body223:                                      ; preds = %for.cond220
  %168 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %169 = bitcast i16* %168 to i8*
  %170 = load i32, i32* %r, align 4, !tbaa !8
  %add224 = add nsw i32 %170, 1
  %arrayidx225 = getelementptr inbounds [33 x [33 x i16]], [33 x [33 x i16]]* %buffer, i32 0, i32 %add224
  %arrayidx226 = getelementptr inbounds [33 x i16], [33 x i16]* %arrayidx225, i32 0, i32 1
  %171 = bitcast i16* %arrayidx226 to i8*
  %172 = load i32, i32* %bw, align 4, !tbaa !8
  %mul227 = mul i32 %172, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %169, i8* align 2 %171, i32 %mul227, i1 false)
  %173 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %174 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %174, i32 %173
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc228

for.inc228:                                       ; preds = %for.body223
  %175 = load i32, i32* %r, align 4, !tbaa !8
  %inc229 = add nsw i32 %175, 1
  store i32 %inc229, i32* %r, align 4, !tbaa !8
  br label %for.cond220

for.end230:                                       ; preds = %for.cond220
  %176 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #6
  %177 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #6
  %178 = bitcast [33 x [33 x i16]]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 2178, i8* %178) #6
  %179 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #6
  %180 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #6
  ret void
}

; Function Attrs: nounwind
define internal i32 @get_filt_type(%struct.macroblockd* %xd, i32 %plane) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %ab_sm = alloca i32, align 4
  %le_sm = alloca i32, align 4
  %ab = alloca %struct.MB_MODE_INFO*, align 4
  %le = alloca %struct.MB_MODE_INFO*, align 4
  %ab7 = alloca %struct.MB_MODE_INFO*, align 4
  %le8 = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !8
  %0 = bitcast i32* %ab_sm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %le_sm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast %struct.MB_MODE_INFO** %ab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 12
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 16, !tbaa !76
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %ab, align 4, !tbaa !2
  %6 = bitcast %struct.MB_MODE_INFO** %le to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 11
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !77
  store %struct.MB_MODE_INFO* %8, %struct.MB_MODE_INFO** %le, align 4, !tbaa !2
  %9 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ab, align 4, !tbaa !2
  %tobool = icmp ne %struct.MB_MODE_INFO* %9, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ab, align 4, !tbaa !2
  %11 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %call = call i32 @is_smooth(%struct.MB_MODE_INFO* %10, i32 %11)
  br label %cond.end

cond.false:                                       ; preds = %if.then
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %ab_sm, align 4, !tbaa !8
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %le, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.MB_MODE_INFO* %12, null
  br i1 %tobool1, label %cond.true2, label %cond.false4

cond.true2:                                       ; preds = %cond.end
  %13 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %le, align 4, !tbaa !2
  %14 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %call3 = call i32 @is_smooth(%struct.MB_MODE_INFO* %13, i32 %14)
  br label %cond.end5

cond.false4:                                      ; preds = %cond.end
  br label %cond.end5

cond.end5:                                        ; preds = %cond.false4, %cond.true2
  %cond6 = phi i32 [ %call3, %cond.true2 ], [ 0, %cond.false4 ]
  store i32 %cond6, i32* %le_sm, align 4, !tbaa !8
  %15 = bitcast %struct.MB_MODE_INFO** %le to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %struct.MB_MODE_INFO** %ab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  br label %if.end

if.else:                                          ; preds = %entry
  %17 = bitcast %struct.MB_MODE_INFO** %ab7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %chroma_above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %18, i32 0, i32 14
  %19 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %chroma_above_mbmi, align 8, !tbaa !78
  store %struct.MB_MODE_INFO* %19, %struct.MB_MODE_INFO** %ab7, align 4, !tbaa !2
  %20 = bitcast %struct.MB_MODE_INFO** %le8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %chroma_left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %21, i32 0, i32 13
  %22 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %chroma_left_mbmi, align 4, !tbaa !79
  store %struct.MB_MODE_INFO* %22, %struct.MB_MODE_INFO** %le8, align 4, !tbaa !2
  %23 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ab7, align 4, !tbaa !2
  %tobool9 = icmp ne %struct.MB_MODE_INFO* %23, null
  br i1 %tobool9, label %cond.true10, label %cond.false12

cond.true10:                                      ; preds = %if.else
  %24 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ab7, align 4, !tbaa !2
  %25 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %call11 = call i32 @is_smooth(%struct.MB_MODE_INFO* %24, i32 %25)
  br label %cond.end13

cond.false12:                                     ; preds = %if.else
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false12, %cond.true10
  %cond14 = phi i32 [ %call11, %cond.true10 ], [ 0, %cond.false12 ]
  store i32 %cond14, i32* %ab_sm, align 4, !tbaa !8
  %26 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %le8, align 4, !tbaa !2
  %tobool15 = icmp ne %struct.MB_MODE_INFO* %26, null
  br i1 %tobool15, label %cond.true16, label %cond.false18

cond.true16:                                      ; preds = %cond.end13
  %27 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %le8, align 4, !tbaa !2
  %28 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %call17 = call i32 @is_smooth(%struct.MB_MODE_INFO* %27, i32 %28)
  br label %cond.end19

cond.false18:                                     ; preds = %cond.end13
  br label %cond.end19

cond.end19:                                       ; preds = %cond.false18, %cond.true16
  %cond20 = phi i32 [ %call17, %cond.true16 ], [ 0, %cond.false18 ]
  store i32 %cond20, i32* %le_sm, align 4, !tbaa !8
  %29 = bitcast %struct.MB_MODE_INFO** %le8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  %30 = bitcast %struct.MB_MODE_INFO** %ab7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  br label %if.end

if.end:                                           ; preds = %cond.end19, %cond.end5
  %31 = load i32, i32* %ab_sm, align 4, !tbaa !8
  %tobool21 = icmp ne i32 %31, 0
  br i1 %tobool21, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end
  %32 = load i32, i32* %le_sm, align 4, !tbaa !8
  %tobool22 = icmp ne i32 %32, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.end
  %33 = phi i1 [ true, %if.end ], [ %tobool22, %lor.rhs ]
  %34 = zext i1 %33 to i64
  %cond23 = select i1 %33, i32 1, i32 0
  %35 = bitcast i32* %le_sm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = bitcast i32* %ab_sm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  ret i32 %cond23
}

; Function Attrs: nounwind
define internal void @filter_intra_edge_corner_high(i16* %p_above, i16* %p_left) #0 {
entry:
  %p_above.addr = alloca i16*, align 4
  %p_left.addr = alloca i16*, align 4
  %kernel = alloca [3 x i32], align 4
  %s = alloca i32, align 4
  store i16* %p_above, i16** %p_above.addr, align 4, !tbaa !2
  store i16* %p_left, i16** %p_left.addr, align 4, !tbaa !2
  %0 = bitcast [3 x i32]* %kernel to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #6
  %1 = bitcast [3 x i32]* %kernel to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast ([3 x i32]* @__const.filter_intra_edge_corner_high.kernel to i8*), i32 12, i1 false)
  %2 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i16*, i16** %p_left.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 0
  %4 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %4 to i32
  %arrayidx1 = getelementptr inbounds [3 x i32], [3 x i32]* %kernel, i32 0, i32 0
  %5 = load i32, i32* %arrayidx1, align 4, !tbaa !8
  %mul = mul nsw i32 %conv, %5
  %6 = load i16*, i16** %p_above.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %6, i32 -1
  %7 = load i16, i16* %arrayidx2, align 2, !tbaa !11
  %conv3 = zext i16 %7 to i32
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %kernel, i32 0, i32 1
  %8 = load i32, i32* %arrayidx4, align 4, !tbaa !8
  %mul5 = mul nsw i32 %conv3, %8
  %add = add nsw i32 %mul, %mul5
  %9 = load i16*, i16** %p_above.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %9, i32 0
  %10 = load i16, i16* %arrayidx6, align 2, !tbaa !11
  %conv7 = zext i16 %10 to i32
  %arrayidx8 = getelementptr inbounds [3 x i32], [3 x i32]* %kernel, i32 0, i32 2
  %11 = load i32, i32* %arrayidx8, align 4, !tbaa !8
  %mul9 = mul nsw i32 %conv7, %11
  %add10 = add nsw i32 %add, %mul9
  store i32 %add10, i32* %s, align 4, !tbaa !8
  %12 = load i32, i32* %s, align 4, !tbaa !8
  %add11 = add nsw i32 %12, 8
  %shr = ashr i32 %add11, 4
  store i32 %shr, i32* %s, align 4, !tbaa !8
  %13 = load i32, i32* %s, align 4, !tbaa !8
  %conv12 = trunc i32 %13 to i16
  %14 = load i16*, i16** %p_above.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i16, i16* %14, i32 -1
  store i16 %conv12, i16* %arrayidx13, align 2, !tbaa !11
  %15 = load i32, i32* %s, align 4, !tbaa !8
  %conv14 = trunc i32 %15 to i16
  %16 = load i16*, i16** %p_left.addr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i16, i16* %16, i32 -1
  store i16 %conv14, i16* %arrayidx15, align 2, !tbaa !11
  %17 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast [3 x i32]* %kernel to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %18) #6
  ret void
}

; Function Attrs: nounwind
define internal i32 @intra_edge_filter_strength(i32 %bs0, i32 %bs1, i32 %delta, i32 %type) #0 {
entry:
  %bs0.addr = alloca i32, align 4
  %bs1.addr = alloca i32, align 4
  %delta.addr = alloca i32, align 4
  %type.addr = alloca i32, align 4
  %d = alloca i32, align 4
  %strength = alloca i32, align 4
  %blk_wh = alloca i32, align 4
  store i32 %bs0, i32* %bs0.addr, align 4, !tbaa !8
  store i32 %bs1, i32* %bs1.addr, align 4, !tbaa !8
  store i32 %delta, i32* %delta.addr, align 4, !tbaa !8
  store i32 %type, i32* %type.addr, align 4, !tbaa !8
  %0 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %delta.addr, align 4, !tbaa !8
  %call = call i32 @abs(i32 %1) #7
  store i32 %call, i32* %d, align 4, !tbaa !8
  %2 = bitcast i32* %strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 0, i32* %strength, align 4, !tbaa !8
  %3 = bitcast i32* %blk_wh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %bs0.addr, align 4, !tbaa !8
  %5 = load i32, i32* %bs1.addr, align 4, !tbaa !8
  %add = add nsw i32 %4, %5
  store i32 %add, i32* %blk_wh, align 4, !tbaa !8
  %6 = load i32, i32* %type.addr, align 4, !tbaa !8
  %cmp = icmp eq i32 %6, 0
  br i1 %cmp, label %if.then, label %if.else49

if.then:                                          ; preds = %entry
  %7 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp1 = icmp sle i32 %7, 8
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %8 = load i32, i32* %d, align 4, !tbaa !8
  %cmp3 = icmp sge i32 %8, 56
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then2
  store i32 1, i32* %strength, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then2
  br label %if.end48

if.else:                                          ; preds = %if.then
  %9 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp5 = icmp sle i32 %9, 12
  br i1 %cmp5, label %if.then6, label %if.else10

if.then6:                                         ; preds = %if.else
  %10 = load i32, i32* %d, align 4, !tbaa !8
  %cmp7 = icmp sge i32 %10, 40
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.then6
  store i32 1, i32* %strength, align 4, !tbaa !8
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %if.then6
  br label %if.end47

if.else10:                                        ; preds = %if.else
  %11 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp11 = icmp sle i32 %11, 16
  br i1 %cmp11, label %if.then12, label %if.else16

if.then12:                                        ; preds = %if.else10
  %12 = load i32, i32* %d, align 4, !tbaa !8
  %cmp13 = icmp sge i32 %12, 40
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 1, i32* %strength, align 4, !tbaa !8
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %if.then12
  br label %if.end46

if.else16:                                        ; preds = %if.else10
  %13 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp17 = icmp sle i32 %13, 24
  br i1 %cmp17, label %if.then18, label %if.else28

if.then18:                                        ; preds = %if.else16
  %14 = load i32, i32* %d, align 4, !tbaa !8
  %cmp19 = icmp sge i32 %14, 8
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.then18
  store i32 1, i32* %strength, align 4, !tbaa !8
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %if.then18
  %15 = load i32, i32* %d, align 4, !tbaa !8
  %cmp22 = icmp sge i32 %15, 16
  br i1 %cmp22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end21
  store i32 2, i32* %strength, align 4, !tbaa !8
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %if.end21
  %16 = load i32, i32* %d, align 4, !tbaa !8
  %cmp25 = icmp sge i32 %16, 32
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.end24
  store i32 3, i32* %strength, align 4, !tbaa !8
  br label %if.end27

if.end27:                                         ; preds = %if.then26, %if.end24
  br label %if.end45

if.else28:                                        ; preds = %if.else16
  %17 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp29 = icmp sle i32 %17, 32
  br i1 %cmp29, label %if.then30, label %if.else40

if.then30:                                        ; preds = %if.else28
  %18 = load i32, i32* %d, align 4, !tbaa !8
  %cmp31 = icmp sge i32 %18, 1
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.then30
  store i32 1, i32* %strength, align 4, !tbaa !8
  br label %if.end33

if.end33:                                         ; preds = %if.then32, %if.then30
  %19 = load i32, i32* %d, align 4, !tbaa !8
  %cmp34 = icmp sge i32 %19, 4
  br i1 %cmp34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.end33
  store i32 2, i32* %strength, align 4, !tbaa !8
  br label %if.end36

if.end36:                                         ; preds = %if.then35, %if.end33
  %20 = load i32, i32* %d, align 4, !tbaa !8
  %cmp37 = icmp sge i32 %20, 32
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.end36
  store i32 3, i32* %strength, align 4, !tbaa !8
  br label %if.end39

if.end39:                                         ; preds = %if.then38, %if.end36
  br label %if.end44

if.else40:                                        ; preds = %if.else28
  %21 = load i32, i32* %d, align 4, !tbaa !8
  %cmp41 = icmp sge i32 %21, 1
  br i1 %cmp41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.else40
  store i32 3, i32* %strength, align 4, !tbaa !8
  br label %if.end43

if.end43:                                         ; preds = %if.then42, %if.else40
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %if.end39
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.end27
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.end15
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.end9
  br label %if.end48

if.end48:                                         ; preds = %if.end47, %if.end
  br label %if.end80

if.else49:                                        ; preds = %entry
  %22 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp50 = icmp sle i32 %22, 8
  br i1 %cmp50, label %if.then51, label %if.else58

if.then51:                                        ; preds = %if.else49
  %23 = load i32, i32* %d, align 4, !tbaa !8
  %cmp52 = icmp sge i32 %23, 40
  br i1 %cmp52, label %if.then53, label %if.end54

if.then53:                                        ; preds = %if.then51
  store i32 1, i32* %strength, align 4, !tbaa !8
  br label %if.end54

if.end54:                                         ; preds = %if.then53, %if.then51
  %24 = load i32, i32* %d, align 4, !tbaa !8
  %cmp55 = icmp sge i32 %24, 64
  br i1 %cmp55, label %if.then56, label %if.end57

if.then56:                                        ; preds = %if.end54
  store i32 2, i32* %strength, align 4, !tbaa !8
  br label %if.end57

if.end57:                                         ; preds = %if.then56, %if.end54
  br label %if.end79

if.else58:                                        ; preds = %if.else49
  %25 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp59 = icmp sle i32 %25, 16
  br i1 %cmp59, label %if.then60, label %if.else67

if.then60:                                        ; preds = %if.else58
  %26 = load i32, i32* %d, align 4, !tbaa !8
  %cmp61 = icmp sge i32 %26, 20
  br i1 %cmp61, label %if.then62, label %if.end63

if.then62:                                        ; preds = %if.then60
  store i32 1, i32* %strength, align 4, !tbaa !8
  br label %if.end63

if.end63:                                         ; preds = %if.then62, %if.then60
  %27 = load i32, i32* %d, align 4, !tbaa !8
  %cmp64 = icmp sge i32 %27, 48
  br i1 %cmp64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %if.end63
  store i32 2, i32* %strength, align 4, !tbaa !8
  br label %if.end66

if.end66:                                         ; preds = %if.then65, %if.end63
  br label %if.end78

if.else67:                                        ; preds = %if.else58
  %28 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp68 = icmp sle i32 %28, 24
  br i1 %cmp68, label %if.then69, label %if.else73

if.then69:                                        ; preds = %if.else67
  %29 = load i32, i32* %d, align 4, !tbaa !8
  %cmp70 = icmp sge i32 %29, 4
  br i1 %cmp70, label %if.then71, label %if.end72

if.then71:                                        ; preds = %if.then69
  store i32 3, i32* %strength, align 4, !tbaa !8
  br label %if.end72

if.end72:                                         ; preds = %if.then71, %if.then69
  br label %if.end77

if.else73:                                        ; preds = %if.else67
  %30 = load i32, i32* %d, align 4, !tbaa !8
  %cmp74 = icmp sge i32 %30, 1
  br i1 %cmp74, label %if.then75, label %if.end76

if.then75:                                        ; preds = %if.else73
  store i32 3, i32* %strength, align 4, !tbaa !8
  br label %if.end76

if.end76:                                         ; preds = %if.then75, %if.else73
  br label %if.end77

if.end77:                                         ; preds = %if.end76, %if.end72
  br label %if.end78

if.end78:                                         ; preds = %if.end77, %if.end66
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %if.end57
  br label %if.end80

if.end80:                                         ; preds = %if.end79, %if.end48
  %31 = load i32, i32* %strength, align 4, !tbaa !8
  %32 = bitcast i32* %blk_wh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  %33 = bitcast i32* %strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  ret i32 %31
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_use_intra_edge_upsample(i32 %bs0, i32 %bs1, i32 %delta, i32 %type) #4 {
entry:
  %retval = alloca i32, align 4
  %bs0.addr = alloca i32, align 4
  %bs1.addr = alloca i32, align 4
  %delta.addr = alloca i32, align 4
  %type.addr = alloca i32, align 4
  %d = alloca i32, align 4
  %blk_wh = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %bs0, i32* %bs0.addr, align 4, !tbaa !8
  store i32 %bs1, i32* %bs1.addr, align 4, !tbaa !8
  store i32 %delta, i32* %delta.addr, align 4, !tbaa !8
  store i32 %type, i32* %type.addr, align 4, !tbaa !8
  %0 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %delta.addr, align 4, !tbaa !8
  %call = call i32 @abs(i32 %1) #7
  store i32 %call, i32* %d, align 4, !tbaa !8
  %2 = bitcast i32* %blk_wh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %bs0.addr, align 4, !tbaa !8
  %4 = load i32, i32* %bs1.addr, align 4, !tbaa !8
  %add = add nsw i32 %3, %4
  store i32 %add, i32* %blk_wh, align 4, !tbaa !8
  %5 = load i32, i32* %d, align 4, !tbaa !8
  %cmp = icmp eq i32 %5, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %6 = load i32, i32* %d, align 4, !tbaa !8
  %cmp1 = icmp sge i32 %6, 40
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %7 = load i32, i32* %type.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %8 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp2 = icmp sle i32 %8, 8
  %conv = zext i1 %cmp2 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %9 = load i32, i32* %blk_wh, align 4, !tbaa !8
  %cmp3 = icmp sle i32 %9, 16
  %conv4 = zext i1 %cmp3 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %conv4, %cond.false ]
  store i32 %cond, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %cond.end, %if.then
  %10 = bitcast i32* %blk_wh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: nounwind
define internal void @highbd_dr_predictor(i16* %dst, i32 %stride, i8 zeroext %tx_size, i16* %above, i16* %left, i32 %upsample_above, i32 %upsample_left, i32 %angle, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %upsample_above.addr = alloca i32, align 4
  %upsample_left.addr = alloca i32, align 4
  %angle.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %dx = alloca i32, align 4
  %dy = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %upsample_above, i32* %upsample_above.addr, align 4, !tbaa !8
  store i32 %upsample_left, i32* %upsample_left.addr, align 4, !tbaa !8
  store i32 %angle, i32* %angle.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %call = call i32 @av1_get_dx(i32 %1)
  store i32 %call, i32* %dx, align 4, !tbaa !8
  %2 = bitcast i32* %dy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %call1 = call i32 @av1_get_dy(i32 %3)
  store i32 %call1, i32* %dy, align 4, !tbaa !8
  %4 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !8
  store i32 %6, i32* %bw, align 4, !tbaa !8
  %7 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom2 = zext i8 %8 to i32
  %arrayidx3 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom2
  %9 = load i32, i32* %arrayidx3, align 4, !tbaa !8
  store i32 %9, i32* %bh, align 4, !tbaa !8
  %10 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp = icmp sgt i32 %10, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %11 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp4 = icmp slt i32 %11, 90
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %12 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %13 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %14 = load i32, i32* %bw, align 4, !tbaa !8
  %15 = load i32, i32* %bh, align 4, !tbaa !8
  %16 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %17 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %18 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %19 = load i32, i32* %dx, align 4, !tbaa !8
  %20 = load i32, i32* %dy, align 4, !tbaa !8
  %21 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @av1_highbd_dr_prediction_z1_c(i16* %12, i32 %13, i32 %14, i32 %15, i16* %16, i16* %17, i32 %18, i32 %19, i32 %20, i32 %21)
  br label %if.end27

if.else:                                          ; preds = %land.lhs.true, %entry
  %22 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp5 = icmp sgt i32 %22, 90
  br i1 %cmp5, label %land.lhs.true6, label %if.else9

land.lhs.true6:                                   ; preds = %if.else
  %23 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp7 = icmp slt i32 %23, 180
  br i1 %cmp7, label %if.then8, label %if.else9

if.then8:                                         ; preds = %land.lhs.true6
  %24 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %25 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %26 = load i32, i32* %bw, align 4, !tbaa !8
  %27 = load i32, i32* %bh, align 4, !tbaa !8
  %28 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %29 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %30 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %31 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %32 = load i32, i32* %dx, align 4, !tbaa !8
  %33 = load i32, i32* %dy, align 4, !tbaa !8
  %34 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @av1_highbd_dr_prediction_z2_c(i16* %24, i32 %25, i32 %26, i32 %27, i16* %28, i16* %29, i32 %30, i32 %31, i32 %32, i32 %33, i32 %34)
  br label %if.end26

if.else9:                                         ; preds = %land.lhs.true6, %if.else
  %35 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp10 = icmp sgt i32 %35, 180
  br i1 %cmp10, label %land.lhs.true11, label %if.else14

land.lhs.true11:                                  ; preds = %if.else9
  %36 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp12 = icmp slt i32 %36, 270
  br i1 %cmp12, label %if.then13, label %if.else14

if.then13:                                        ; preds = %land.lhs.true11
  %37 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %38 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %39 = load i32, i32* %bw, align 4, !tbaa !8
  %40 = load i32, i32* %bh, align 4, !tbaa !8
  %41 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %42 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %43 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %44 = load i32, i32* %dx, align 4, !tbaa !8
  %45 = load i32, i32* %dy, align 4, !tbaa !8
  %46 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @av1_highbd_dr_prediction_z3_c(i16* %37, i32 %38, i32 %39, i32 %40, i16* %41, i16* %42, i32 %43, i32 %44, i32 %45, i32 %46)
  br label %if.end25

if.else14:                                        ; preds = %land.lhs.true11, %if.else9
  %47 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp15 = icmp eq i32 %47, 90
  br i1 %cmp15, label %if.then16, label %if.else19

if.then16:                                        ; preds = %if.else14
  %48 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom17 = zext i8 %48 to i32
  %arrayidx18 = getelementptr inbounds [19 x void (i16*, i32, i16*, i16*, i32)*], [19 x void (i16*, i32, i16*, i16*, i32)*]* getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 1), i32 0, i32 %idxprom17
  %49 = load void (i16*, i32, i16*, i16*, i32)*, void (i16*, i32, i16*, i16*, i32)** %arrayidx18, align 4, !tbaa !2
  %50 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %51 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %52 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %53 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %54 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void %49(i16* %50, i32 %51, i16* %52, i16* %53, i32 %54)
  br label %if.end24

if.else19:                                        ; preds = %if.else14
  %55 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp20 = icmp eq i32 %55, 180
  br i1 %cmp20, label %if.then21, label %if.end

if.then21:                                        ; preds = %if.else19
  %56 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom22 = zext i8 %56 to i32
  %arrayidx23 = getelementptr inbounds [19 x void (i16*, i32, i16*, i16*, i32)*], [19 x void (i16*, i32, i16*, i16*, i32)*]* getelementptr inbounds ([13 x [19 x void (i16*, i32, i16*, i16*, i32)*]], [13 x [19 x void (i16*, i32, i16*, i16*, i32)*]]* @pred_high, i32 0, i32 2), i32 0, i32 %idxprom22
  %57 = load void (i16*, i32, i16*, i16*, i32)*, void (i16*, i32, i16*, i16*, i32)** %arrayidx23, align 4, !tbaa !2
  %58 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %59 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %60 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %61 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %62 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void %57(i16* %58, i32 %59, i16* %60, i16* %61, i32 %62)
  br label %if.end

if.end:                                           ; preds = %if.then21, %if.else19
  br label %if.end24

if.end24:                                         ; preds = %if.end, %if.then16
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.then13
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.then8
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.then
  %63 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = bitcast i32* %dy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  %66 = bitcast i32* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  ret void
}

; Function Attrs: nounwind
define internal i32 @is_smooth(%struct.MB_MODE_INFO* %mbmi, i32 %plane) #0 {
entry:
  %retval = alloca i32, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %plane.addr = alloca i32, align 4
  %mode = alloca i8, align 1
  %uv_mode = alloca i8, align 1
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !8
  %0 = load i32, i32* %plane.addr, align 4, !tbaa !8
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mode) #6
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %mode1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 7
  %2 = load i8, i8* %mode1, align 1, !tbaa !69
  store i8 %2, i8* %mode, align 1, !tbaa !10
  %3 = load i8, i8* %mode, align 1, !tbaa !10
  %conv = zext i8 %3 to i32
  %cmp2 = icmp eq i32 %conv, 9
  br i1 %cmp2, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %4 = load i8, i8* %mode, align 1, !tbaa !10
  %conv4 = zext i8 %4 to i32
  %cmp5 = icmp eq i32 %conv4, 10
  br i1 %cmp5, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %lor.lhs.false
  %5 = load i8, i8* %mode, align 1, !tbaa !10
  %conv7 = zext i8 %5 to i32
  %cmp8 = icmp eq i32 %conv7, 11
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %lor.lhs.false, %if.then
  %6 = phi i1 [ true, %lor.lhs.false ], [ true, %if.then ], [ %cmp8, %lor.rhs ]
  %lor.ext = zext i1 %6 to i32
  store i32 %lor.ext, i32* %retval, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mode) #6
  br label %return

if.else:                                          ; preds = %entry
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %7)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.else
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.else
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %uv_mode) #6
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %uv_mode11 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 8
  %9 = load i8, i8* %uv_mode11, align 4, !tbaa !70
  store i8 %9, i8* %uv_mode, align 1, !tbaa !10
  %10 = load i8, i8* %uv_mode, align 1, !tbaa !10
  %conv12 = zext i8 %10 to i32
  %cmp13 = icmp eq i32 %conv12, 9
  br i1 %cmp13, label %lor.end23, label %lor.lhs.false15

lor.lhs.false15:                                  ; preds = %if.end
  %11 = load i8, i8* %uv_mode, align 1, !tbaa !10
  %conv16 = zext i8 %11 to i32
  %cmp17 = icmp eq i32 %conv16, 10
  br i1 %cmp17, label %lor.end23, label %lor.rhs19

lor.rhs19:                                        ; preds = %lor.lhs.false15
  %12 = load i8, i8* %uv_mode, align 1, !tbaa !10
  %conv20 = zext i8 %12 to i32
  %cmp21 = icmp eq i32 %conv20, 11
  br label %lor.end23

lor.end23:                                        ; preds = %lor.rhs19, %lor.lhs.false15, %if.end
  %13 = phi i1 [ true, %lor.lhs.false15 ], [ true, %if.end ], [ %cmp21, %lor.rhs19 ]
  %lor.ext24 = zext i1 %13 to i32
  store i32 %lor.ext24, i32* %retval, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %uv_mode) #6
  br label %return

return:                                           ; preds = %lor.end23, %if.then10, %lor.end
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_block(%struct.MB_MODE_INFO* %mbmi) #4 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !10
  %conv = sext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_intrabc_block(%struct.MB_MODE_INFO* %mbmi) #4 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  ret i32 %conv
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #5

; Function Attrs: inlinehint nounwind
define internal i32 @av1_get_dx(i32 %angle) #4 {
entry:
  %retval = alloca i32, align 4
  %angle.addr = alloca i32, align 4
  store i32 %angle, i32* %angle.addr, align 4, !tbaa !8
  %0 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp1 = icmp slt i32 %1, 90
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [90 x i16], [90 x i16]* @dr_intra_derivative, i32 0, i32 %2
  %3 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = sext i16 %3 to i32
  store i32 %conv, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %land.lhs.true, %entry
  %4 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp2 = icmp sgt i32 %4, 90
  br i1 %cmp2, label %land.lhs.true4, label %if.else10

land.lhs.true4:                                   ; preds = %if.else
  %5 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp5 = icmp slt i32 %5, 180
  br i1 %cmp5, label %if.then7, label %if.else10

if.then7:                                         ; preds = %land.lhs.true4
  %6 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %sub = sub nsw i32 180, %6
  %arrayidx8 = getelementptr inbounds [90 x i16], [90 x i16]* @dr_intra_derivative, i32 0, i32 %sub
  %7 = load i16, i16* %arrayidx8, align 2, !tbaa !11
  %conv9 = sext i16 %7 to i32
  store i32 %conv9, i32* %retval, align 4
  br label %return

if.else10:                                        ; preds = %land.lhs.true4, %if.else
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else10, %if.then7, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_get_dy(i32 %angle) #4 {
entry:
  %retval = alloca i32, align 4
  %angle.addr = alloca i32, align 4
  store i32 %angle, i32* %angle.addr, align 4, !tbaa !8
  %0 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp = icmp sgt i32 %0, 90
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp1 = icmp slt i32 %1, 180
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %2, 90
  %arrayidx = getelementptr inbounds [90 x i16], [90 x i16]* @dr_intra_derivative, i32 0, i32 %sub
  %3 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = sext i16 %3 to i32
  store i32 %conv, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %land.lhs.true, %entry
  %4 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp2 = icmp sgt i32 %4, 180
  br i1 %cmp2, label %land.lhs.true4, label %if.else11

land.lhs.true4:                                   ; preds = %if.else
  %5 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp5 = icmp slt i32 %5, 270
  br i1 %cmp5, label %if.then7, label %if.else11

if.then7:                                         ; preds = %land.lhs.true4
  %6 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %sub8 = sub nsw i32 270, %6
  %arrayidx9 = getelementptr inbounds [90 x i16], [90 x i16]* @dr_intra_derivative, i32 0, i32 %sub8
  %7 = load i16, i16* %arrayidx9, align 2, !tbaa !11
  %conv10 = sext i16 %7 to i32
  store i32 %conv10, i32* %retval, align 4
  br label %return

if.else11:                                        ; preds = %land.lhs.true4, %if.else
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else11, %if.then7, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: nounwind
define internal void @filter_intra_edge_corner(i8* %p_above, i8* %p_left) #0 {
entry:
  %p_above.addr = alloca i8*, align 4
  %p_left.addr = alloca i8*, align 4
  %kernel = alloca [3 x i32], align 4
  %s = alloca i32, align 4
  store i8* %p_above, i8** %p_above.addr, align 4, !tbaa !2
  store i8* %p_left, i8** %p_left.addr, align 4, !tbaa !2
  %0 = bitcast [3 x i32]* %kernel to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #6
  %1 = bitcast [3 x i32]* %kernel to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast ([3 x i32]* @__const.filter_intra_edge_corner.kernel to i8*), i32 12, i1 false)
  %2 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i8*, i8** %p_left.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %4 to i32
  %arrayidx1 = getelementptr inbounds [3 x i32], [3 x i32]* %kernel, i32 0, i32 0
  %5 = load i32, i32* %arrayidx1, align 4, !tbaa !8
  %mul = mul nsw i32 %conv, %5
  %6 = load i8*, i8** %p_above.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 -1
  %7 = load i8, i8* %arrayidx2, align 1, !tbaa !10
  %conv3 = zext i8 %7 to i32
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %kernel, i32 0, i32 1
  %8 = load i32, i32* %arrayidx4, align 4, !tbaa !8
  %mul5 = mul nsw i32 %conv3, %8
  %add = add nsw i32 %mul, %mul5
  %9 = load i8*, i8** %p_above.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %9, i32 0
  %10 = load i8, i8* %arrayidx6, align 1, !tbaa !10
  %conv7 = zext i8 %10 to i32
  %arrayidx8 = getelementptr inbounds [3 x i32], [3 x i32]* %kernel, i32 0, i32 2
  %11 = load i32, i32* %arrayidx8, align 4, !tbaa !8
  %mul9 = mul nsw i32 %conv7, %11
  %add10 = add nsw i32 %add, %mul9
  store i32 %add10, i32* %s, align 4, !tbaa !8
  %12 = load i32, i32* %s, align 4, !tbaa !8
  %add11 = add nsw i32 %12, 8
  %shr = ashr i32 %add11, 4
  store i32 %shr, i32* %s, align 4, !tbaa !8
  %13 = load i32, i32* %s, align 4, !tbaa !8
  %conv12 = trunc i32 %13 to i8
  %14 = load i8*, i8** %p_above.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i8, i8* %14, i32 -1
  store i8 %conv12, i8* %arrayidx13, align 1, !tbaa !10
  %15 = load i32, i32* %s, align 4, !tbaa !8
  %conv14 = trunc i32 %15 to i8
  %16 = load i8*, i8** %p_left.addr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %16, i32 -1
  store i8 %conv14, i8* %arrayidx15, align 1, !tbaa !10
  %17 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast [3 x i32]* %kernel to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %18) #6
  ret void
}

; Function Attrs: nounwind
define internal void @dr_predictor(i8* %dst, i32 %stride, i8 zeroext %tx_size, i8* %above, i8* %left, i32 %upsample_above, i32 %upsample_left, i32 %angle) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %upsample_above.addr = alloca i32, align 4
  %upsample_left.addr = alloca i32, align 4
  %angle.addr = alloca i32, align 4
  %dx = alloca i32, align 4
  %dy = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  store i32 %upsample_above, i32* %upsample_above.addr, align 4, !tbaa !8
  store i32 %upsample_left, i32* %upsample_left.addr, align 4, !tbaa !8
  store i32 %angle, i32* %angle.addr, align 4, !tbaa !8
  %0 = bitcast i32* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %call = call i32 @av1_get_dx(i32 %1)
  store i32 %call, i32* %dx, align 4, !tbaa !8
  %2 = bitcast i32* %dy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %call1 = call i32 @av1_get_dy(i32 %3)
  store i32 %call1, i32* %dy, align 4, !tbaa !8
  %4 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !8
  store i32 %6, i32* %bw, align 4, !tbaa !8
  %7 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom2 = zext i8 %8 to i32
  %arrayidx3 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom2
  %9 = load i32, i32* %arrayidx3, align 4, !tbaa !8
  store i32 %9, i32* %bh, align 4, !tbaa !8
  %10 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp = icmp sgt i32 %10, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %11 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp4 = icmp slt i32 %11, 90
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %12 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %13 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %14 = load i32, i32* %bw, align 4, !tbaa !8
  %15 = load i32, i32* %bh, align 4, !tbaa !8
  %16 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %17 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %18 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %19 = load i32, i32* %dx, align 4, !tbaa !8
  %20 = load i32, i32* %dy, align 4, !tbaa !8
  call void @av1_dr_prediction_z1_c(i8* %12, i32 %13, i32 %14, i32 %15, i8* %16, i8* %17, i32 %18, i32 %19, i32 %20)
  br label %if.end27

if.else:                                          ; preds = %land.lhs.true, %entry
  %21 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp5 = icmp sgt i32 %21, 90
  br i1 %cmp5, label %land.lhs.true6, label %if.else9

land.lhs.true6:                                   ; preds = %if.else
  %22 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp7 = icmp slt i32 %22, 180
  br i1 %cmp7, label %if.then8, label %if.else9

if.then8:                                         ; preds = %land.lhs.true6
  %23 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %24 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %25 = load i32, i32* %bw, align 4, !tbaa !8
  %26 = load i32, i32* %bh, align 4, !tbaa !8
  %27 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %28 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %29 = load i32, i32* %upsample_above.addr, align 4, !tbaa !8
  %30 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %31 = load i32, i32* %dx, align 4, !tbaa !8
  %32 = load i32, i32* %dy, align 4, !tbaa !8
  call void @av1_dr_prediction_z2_c(i8* %23, i32 %24, i32 %25, i32 %26, i8* %27, i8* %28, i32 %29, i32 %30, i32 %31, i32 %32)
  br label %if.end26

if.else9:                                         ; preds = %land.lhs.true6, %if.else
  %33 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp10 = icmp sgt i32 %33, 180
  br i1 %cmp10, label %land.lhs.true11, label %if.else14

land.lhs.true11:                                  ; preds = %if.else9
  %34 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp12 = icmp slt i32 %34, 270
  br i1 %cmp12, label %if.then13, label %if.else14

if.then13:                                        ; preds = %land.lhs.true11
  %35 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %36 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %37 = load i32, i32* %bw, align 4, !tbaa !8
  %38 = load i32, i32* %bh, align 4, !tbaa !8
  %39 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %40 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %41 = load i32, i32* %upsample_left.addr, align 4, !tbaa !8
  %42 = load i32, i32* %dx, align 4, !tbaa !8
  %43 = load i32, i32* %dy, align 4, !tbaa !8
  call void @av1_dr_prediction_z3_c(i8* %35, i32 %36, i32 %37, i32 %38, i8* %39, i8* %40, i32 %41, i32 %42, i32 %43)
  br label %if.end25

if.else14:                                        ; preds = %land.lhs.true11, %if.else9
  %44 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp15 = icmp eq i32 %44, 90
  br i1 %cmp15, label %if.then16, label %if.else19

if.then16:                                        ; preds = %if.else14
  %45 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom17 = zext i8 %45 to i32
  %arrayidx18 = getelementptr inbounds [19 x void (i8*, i32, i8*, i8*)*], [19 x void (i8*, i32, i8*, i8*)*]* getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 1), i32 0, i32 %idxprom17
  %46 = load void (i8*, i32, i8*, i8*)*, void (i8*, i32, i8*, i8*)** %arrayidx18, align 4, !tbaa !2
  %47 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %48 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %49 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %50 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void %46(i8* %47, i32 %48, i8* %49, i8* %50)
  br label %if.end24

if.else19:                                        ; preds = %if.else14
  %51 = load i32, i32* %angle.addr, align 4, !tbaa !8
  %cmp20 = icmp eq i32 %51, 180
  br i1 %cmp20, label %if.then21, label %if.end

if.then21:                                        ; preds = %if.else19
  %52 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom22 = zext i8 %52 to i32
  %arrayidx23 = getelementptr inbounds [19 x void (i8*, i32, i8*, i8*)*], [19 x void (i8*, i32, i8*, i8*)*]* getelementptr inbounds ([13 x [19 x void (i8*, i32, i8*, i8*)*]], [13 x [19 x void (i8*, i32, i8*, i8*)*]]* @pred, i32 0, i32 2), i32 0, i32 %idxprom22
  %53 = load void (i8*, i32, i8*, i8*)*, void (i8*, i32, i8*, i8*)** %arrayidx23, align 4, !tbaa !2
  %54 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %55 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %56 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %57 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void %53(i8* %54, i32 %55, i8* %56, i8* %57)
  br label %if.end

if.end:                                           ; preds = %if.then21, %if.else19
  br label %if.end24

if.end24:                                         ; preds = %if.end, %if.then16
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.then13
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.then8
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.then
  %58 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %dy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  ret void
}

declare void @aom_v_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_v_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_h_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_paeth_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_v_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_smooth_h_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_128_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_top_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_left_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_4x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_8x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_16x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_32x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_64x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_4x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_8x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_8x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_16x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_16x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_32x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_32x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_64x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_4x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_16x4_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_8x32_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_32x8_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_16x64_c(i8*, i32, i8*, i8*) #3

declare void @aom_dc_predictor_64x16_c(i8*, i32, i8*, i8*) #3

declare void @aom_highbd_v_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_v_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_h_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_paeth_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_v_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_smooth_h_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_128_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_top_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_left_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_4x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_8x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_16x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_32x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_64x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_4x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_8x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_8x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_16x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_16x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_32x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_32x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_64x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_4x16_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_16x4_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_8x32_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_32x8_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_16x64_c(i16*, i32, i16*, i16*, i32) #3

declare void @aom_highbd_dc_predictor_64x16_c(i16*, i32, i16*, i16*, i32) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!4, !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"short", !4, i64 0}
!13 = !{!14, !3, i64 4084}
!14 = !{!"macroblockd", !9, i64 0, !9, i64 4, !9, i64 8, !15, i64 12, !4, i64 16, !16, i64 4060, !3, i64 4084, !15, i64 4088, !15, i64 4089, !15, i64 4090, !15, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !9, i64 4112, !9, i64 4116, !9, i64 4120, !9, i64 4124, !9, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !9, i64 6836, !4, i64 6840, !4, i64 6872, !9, i64 6904, !9, i64 6908, !3, i64 6912, !3, i64 6916, !9, i64 6920, !9, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !17, i64 39720, !18, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!15 = !{!"_Bool", !4, i64 0}
!16 = !{!"TileInfo", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20}
!17 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !9, i64 4104, !4, i64 4108, !9, i64 4236, !9, i64 4240, !9, i64 4244, !9, i64 4248, !9, i64 4252, !9, i64 4256}
!18 = !{!"dist_wtd_comp_params", !9, i64 0, !9, i64 4, !9, i64 8}
!19 = !{!20, !3, i64 124}
!20 = !{!"macroblockd_plane", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !9, i64 16, !9, i64 20, !21, i64 24, !4, i64 44, !3, i64 84, !3, i64 88, !4, i64 92, !3, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!21 = !{!"buf_2d", !3, i64 0, !3, i64 4, !9, i64 8, !9, i64 12, !9, i64 16}
!22 = !{!20, !9, i64 16}
!23 = !{!20, !9, i64 20}
!24 = !{!14, !15, i64 4090}
!25 = !{i8 0, i8 2}
!26 = !{!14, !15, i64 4088}
!27 = !{!14, !15, i64 4091}
!28 = !{!14, !15, i64 4089}
!29 = !{!14, !9, i64 4124}
!30 = !{!14, !9, i64 4116}
!31 = !{!14, !9, i64 4120}
!32 = !{!14, !9, i64 4128}
!33 = !{!14, !9, i64 4072}
!34 = !{!14, !9, i64 4064}
!35 = !{!36, !4, i64 123}
!36 = !{!"MB_MODE_INFO", !37, i64 0, !38, i64 8, !4, i64 52, !9, i64 60, !4, i64 64, !39, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !40, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!37 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!38 = !{!"", !4, i64 0, !12, i64 32, !12, i64 34, !12, i64 36, !12, i64 38, !4, i64 40, !4, i64 41}
!39 = !{!"", !4, i64 0, !4, i64 48}
!40 = !{!"", !4, i64 0, !4, i64 1}
!41 = !{!36, !4, i64 118}
!42 = !{!43, !4, i64 16253}
!43 = !{!"AV1Common", !44, i64 0, !46, i64 40, !9, i64 288, !9, i64 292, !9, i64 296, !9, i64 300, !9, i64 304, !9, i64 308, !4, i64 312, !15, i64 313, !4, i64 316, !9, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !47, i64 492, !4, i64 580, !4, i64 1284, !9, i64 1316, !9, i64 1320, !9, i64 1324, !48, i64 1328, !49, i64 1356, !50, i64 1420, !51, i64 10676, !3, i64 10848, !52, i64 10864, !53, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !54, i64 14880, !55, i64 15028, !56, i64 15168, !57, i64 15816, !4, i64 15836, !58, i64 16192, !3, i64 18128, !3, i64 18132, !61, i64 18136, !3, i64 18724, !62, i64 18728, !9, i64 18760, !4, i64 18764, !3, i64 18796, !9, i64 18800, !4, i64 18804, !4, i64 18836, !9, i64 18844, !9, i64 18848, !9, i64 18852, !9, i64 18856}
!44 = !{!"", !4, i64 0, !4, i64 1, !9, i64 4, !9, i64 8, !9, i64 12, !45, i64 16, !9, i64 32, !9, i64 36}
!45 = !{!"", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!46 = !{!"aom_internal_error_info", !4, i64 0, !9, i64 4, !4, i64 8, !9, i64 88, !4, i64 92}
!47 = !{!"scale_factors", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!48 = !{!"", !15, i64 0, !15, i64 1, !15, i64 2, !15, i64 3, !15, i64 4, !15, i64 5, !15, i64 6, !15, i64 7, !15, i64 8, !15, i64 9, !15, i64 10, !15, i64 11, !4, i64 12, !4, i64 13, !9, i64 16, !9, i64 20, !4, i64 24}
!49 = !{!"CommonModeInfoParams", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !3, i64 20, !9, i64 24, !9, i64 28, !4, i64 32, !3, i64 36, !9, i64 40, !9, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!50 = !{!"CommonQuantParams", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !15, i64 9240, !9, i64 9244, !9, i64 9248, !9, i64 9252}
!51 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !9, i64 164, !4, i64 168}
!52 = !{!"", !4, i64 0, !4, i64 3072}
!53 = !{!"loopfilter", !4, i64 0, !9, i64 8, !9, i64 12, !9, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !9, i64 32}
!54 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !9, i64 52, !4, i64 56, !3, i64 68, !9, i64 72, !3, i64 76, !7, i64 80, !9, i64 84, !7, i64 88, !9, i64 92, !9, i64 96, !9, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !9, i64 128, !9, i64 132, !9, i64 136, !9, i64 140, !3, i64 144}
!55 = !{!"", !9, i64 0, !9, i64 4, !4, i64 8, !4, i64 72, !9, i64 136}
!56 = !{!"", !9, i64 0, !9, i64 4, !4, i64 8, !9, i64 120, !4, i64 124, !9, i64 204, !4, i64 208, !9, i64 288, !9, i64 292, !9, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !9, i64 596, !9, i64 600, !9, i64 604, !9, i64 608, !9, i64 612, !9, i64 616, !9, i64 620, !9, i64 624, !9, i64 628, !9, i64 632, !9, i64 636, !9, i64 640, !12, i64 644}
!57 = !{!"", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16}
!58 = !{!"SequenceHeader", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !4, i64 16, !9, i64 20, !9, i64 24, !4, i64 28, !9, i64 32, !9, i64 36, !45, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !9, i64 92, !9, i64 96, !9, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !9, i64 112, !4, i64 116, !9, i64 244, !59, i64 248, !4, i64 264, !60, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!59 = !{!"aom_timing", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!60 = !{!"aom_dec_model_info", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!61 = !{!"CommonTileParams", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28, !9, i64 32, !9, i64 36, !9, i64 40, !9, i64 44, !9, i64 48, !9, i64 52, !9, i64 56, !4, i64 60, !4, i64 320, !9, i64 580, !9, i64 584}
!62 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !9, i64 20, !9, i64 24, !9, i64 28}
!63 = !{!14, !3, i64 4140}
!64 = !{!54, !9, i64 140}
!65 = !{!43, !4, i64 16220}
!66 = !{!14, !9, i64 6836}
!67 = !{!20, !9, i64 40}
!68 = !{!20, !3, i64 24}
!69 = !{!36, !4, i64 119}
!70 = !{!36, !4, i64 120}
!71 = !{!36, !4, i64 127}
!72 = !{!36, !4, i64 126}
!73 = !{!20, !4, i64 128}
!74 = !{!20, !4, i64 129}
!75 = !{!17, !9, i64 4104}
!76 = !{!14, !3, i64 4096}
!77 = !{!14, !3, i64 4092}
!78 = !{!14, !3, i64 4104}
!79 = !{!14, !3, i64 4100}
