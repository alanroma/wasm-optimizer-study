; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/thread_common.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/thread_common.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.AV1LfSyncData = type { [3 x i32*], i32, i32, %struct.LoopFilterWorkerData*, i32, %struct.AV1LfMTInfo*, i32, i32 }
%struct.LoopFilterWorkerData = type { %struct.yv12_buffer_config*, %struct.AV1Common*, [3 x %struct.macroblockd_plane], %struct.macroblockd* }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.AV1LfMTInfo = type { i32, i32, i32 }
%struct.AVxWorker = type { %struct.AVxWorkerImpl*, i32, i8*, i32 (i8*, i8*)*, i8*, i8*, i32 }
%struct.AVxWorkerImpl = type opaque
%struct.AVxWorkerInterface = type { void (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)* }
%struct.AV1LrSyncData = type { [3 x i32*], i32, i32, i32, i32, %struct.LoopRestorationWorkerData*, %struct.AV1LrMTInfo*, i32, i32 }
%struct.LoopRestorationWorkerData = type { i32*, i8*, i8* }
%struct.AV1LrMTInfo = type { i32, i32, i32, i32, i32, i32, i32 }
%struct.AV1LrStruct = type { void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, [3 x %struct.FilterFrameCtxt], %struct.yv12_buffer_config*, %struct.yv12_buffer_config* }
%struct.RestorationTileLimits = type { i32, i32, i32, i32 }
%struct.AV1PixelRect = type { i32, i32, i32, i32 }
%struct.FilterFrameCtxt = type { %struct.RestorationInfo*, i32, i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.AV1PixelRect }

@.str = private unnamed_addr constant [35 x i8] c"Failed to allocate lf_sync->lfdata\00", align 1
@.str.1 = private unnamed_addr constant [42 x i8] c"Failed to allocate lf_sync->cur_sb_col[j]\00", align 1
@.str.2 = private unnamed_addr constant [38 x i8] c"Failed to allocate lf_sync->job_queue\00", align 1
@.str.3 = private unnamed_addr constant [41 x i8] c"Failed to allocate lr_sync->lrworkerdata\00", align 1
@.str.4 = private unnamed_addr constant [64 x i8] c"Failed to allocate lr_sync->lrworkerdata[worker_idx].rst_tmpbuf\00", align 1
@.str.5 = private unnamed_addr constant [58 x i8] c"Failed to allocate lr_sync->lrworkerdata[worker_idx].rlbs\00", align 1
@.str.6 = private unnamed_addr constant [42 x i8] c"Failed to allocate lr_sync->cur_sb_col[j]\00", align 1
@.str.7 = private unnamed_addr constant [38 x i8] c"Failed to allocate lr_sync->job_queue\00", align 1
@loop_restoration_row_worker.copy_funs = internal constant [3 x void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)*] [void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)* @aom_yv12_partial_coloc_copy_y_c, void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)* @aom_yv12_partial_coloc_copy_u_c, void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)* @aom_yv12_partial_coloc_copy_v_c], align 4

; Function Attrs: nounwind
define hidden void @av1_loop_filter_dealloc(%struct.AV1LfSyncData* %lf_sync) #0 {
entry:
  %lf_sync.addr = alloca %struct.AV1LfSyncData*, align 4
  %j = alloca i32, align 4
  store %struct.AV1LfSyncData* %lf_sync, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %0 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %cmp = icmp ne %struct.AV1LfSyncData* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %lfdata = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %2, i32 0, i32 3
  %3 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lfdata, align 4, !tbaa !6
  %4 = bitcast %struct.LoopFilterWorkerData* %3 to i8*
  call void @aom_free(i8* %4)
  store i32 0, i32* %j, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %j, align 4, !tbaa !9
  %cmp1 = icmp slt i32 %5, 3
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %cur_sb_col = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %6, i32 0, i32 0
  %7 = load i32, i32* %j, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds [3 x i32*], [3 x i32*]* %cur_sb_col, i32 0, i32 %7
  %8 = load i32*, i32** %arrayidx, align 4, !tbaa !2
  %9 = bitcast i32* %8 to i8*
  call void @aom_free(i8* %9)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %j, align 4, !tbaa !9
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %j, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %job_queue = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %11, i32 0, i32 5
  %12 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %job_queue, align 4, !tbaa !10
  %13 = bitcast %struct.AV1LfMTInfo* %12 to i8*
  call void @aom_free(i8* %13)
  %14 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %15 = bitcast %struct.AV1LfSyncData* %14 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %15, i8 0, i32 40, i1 false)
  %16 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare void @aom_free(i8*) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_loop_filter_frame_mt(%struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane_start, i32 %plane_end, i32 %partial_frame, %struct.AVxWorker* %workers, i32 %num_workers, %struct.AV1LfSyncData* %lf_sync) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane_start.addr = alloca i32, align 4
  %plane_end.addr = alloca i32, align 4
  %partial_frame.addr = alloca i32, align 4
  %workers.addr = alloca %struct.AVxWorker*, align 4
  %num_workers.addr = alloca i32, align 4
  %lf_sync.addr = alloca %struct.AV1LfSyncData*, align 4
  %start_mi_row = alloca i32, align 4
  %end_mi_row = alloca i32, align 4
  %mi_rows_to_filter = alloca i32, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane_start, i32* %plane_start.addr, align 4, !tbaa !9
  store i32 %plane_end, i32* %plane_end.addr, align 4, !tbaa !9
  store i32 %partial_frame, i32* %partial_frame.addr, align 4, !tbaa !9
  store %struct.AVxWorker* %workers, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  store i32 %num_workers, i32* %num_workers.addr, align 4, !tbaa !9
  store %struct.AV1LfSyncData* %lf_sync, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %0 = bitcast i32* %start_mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %end_mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %mi_rows_to_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %start_mi_row, align 4, !tbaa !9
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %4 = load i32, i32* %mi_rows, align 4, !tbaa !11
  store i32 %4, i32* %mi_rows_to_filter, align 4, !tbaa !9
  %5 = load i32, i32* %partial_frame.addr, align 4, !tbaa !9
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 22
  %mi_rows2 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params1, i32 0, i32 3
  %7 = load i32, i32* %mi_rows2, align 4, !tbaa !11
  %cmp = icmp sgt i32 %7, 8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params3 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 22
  %mi_rows4 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params3, i32 0, i32 3
  %9 = load i32, i32* %mi_rows4, align 4, !tbaa !11
  %shr = ashr i32 %9, 1
  store i32 %shr, i32* %start_mi_row, align 4, !tbaa !9
  %10 = load i32, i32* %start_mi_row, align 4, !tbaa !9
  %and = and i32 %10, -8
  store i32 %and, i32* %start_mi_row, align 4, !tbaa !9
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params5 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 22
  %mi_rows6 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params5, i32 0, i32 3
  %12 = load i32, i32* %mi_rows6, align 4, !tbaa !11
  %div = sdiv i32 %12, 8
  %cmp7 = icmp sgt i32 %div, 8
  br i1 %cmp7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 22
  %mi_rows9 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params8, i32 0, i32 3
  %14 = load i32, i32* %mi_rows9, align 4, !tbaa !11
  %div10 = sdiv i32 %14, 8
  br label %cond.end

cond.false:                                       ; preds = %if.then
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %div10, %cond.true ], [ 8, %cond.false ]
  store i32 %cond, i32* %mi_rows_to_filter, align 4, !tbaa !9
  br label %if.end

if.end:                                           ; preds = %cond.end, %land.lhs.true, %entry
  %15 = load i32, i32* %start_mi_row, align 4, !tbaa !9
  %16 = load i32, i32* %mi_rows_to_filter, align 4, !tbaa !9
  %add = add nsw i32 %15, %16
  store i32 %add, i32* %end_mi_row, align 4, !tbaa !9
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %18 = load i32, i32* %plane_start.addr, align 4, !tbaa !9
  %19 = load i32, i32* %plane_end.addr, align 4, !tbaa !9
  call void @av1_loop_filter_frame_init(%struct.AV1Common* %17, i32 %18, i32 %19)
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %22 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %23 = load i32, i32* %start_mi_row, align 4, !tbaa !9
  %24 = load i32, i32* %end_mi_row, align 4, !tbaa !9
  %25 = load i32, i32* %plane_start.addr, align 4, !tbaa !9
  %26 = load i32, i32* %plane_end.addr, align 4, !tbaa !9
  %27 = load %struct.AVxWorker*, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  %28 = load i32, i32* %num_workers.addr, align 4, !tbaa !9
  %29 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  call void @loop_filter_rows_mt(%struct.yv12_buffer_config* %20, %struct.AV1Common* %21, %struct.macroblockd* %22, i32 %23, i32 %24, i32 %25, i32 %26, %struct.AVxWorker* %27, i32 %28, %struct.AV1LfSyncData* %29)
  %30 = bitcast i32* %mi_rows_to_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  %31 = bitcast i32* %end_mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #5
  %32 = bitcast i32* %start_mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #5
  ret void
}

declare void @av1_loop_filter_frame_init(%struct.AV1Common*, i32, i32) #2

; Function Attrs: nounwind
define internal void @loop_filter_rows_mt(%struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %start, i32 %stop, i32 %plane_start, i32 %plane_end, %struct.AVxWorker* %workers, i32 %nworkers, %struct.AV1LfSyncData* %lf_sync) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %start.addr = alloca i32, align 4
  %stop.addr = alloca i32, align 4
  %plane_start.addr = alloca i32, align 4
  %plane_end.addr = alloca i32, align 4
  %workers.addr = alloca %struct.AVxWorker*, align 4
  %nworkers.addr = alloca i32, align 4
  %lf_sync.addr = alloca %struct.AV1LfSyncData*, align 4
  %winterface = alloca %struct.AVxWorkerInterface*, align 4
  %sb_rows = alloca i32, align 4
  %num_workers = alloca i32, align 4
  %i = alloca i32, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %lf_data = alloca %struct.LoopFilterWorkerData*, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !9
  store i32 %stop, i32* %stop.addr, align 4, !tbaa !9
  store i32 %plane_start, i32* %plane_start.addr, align 4, !tbaa !9
  store i32 %plane_end, i32* %plane_end.addr, align 4, !tbaa !9
  store %struct.AVxWorker* %workers, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  store i32 %nworkers, i32* %nworkers.addr, align 4, !tbaa !9
  store %struct.AV1LfSyncData* %lf_sync, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  store %struct.AVxWorkerInterface* %call, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %1 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %3 = load i32, i32* %mi_rows, align 4, !tbaa !11
  %add = add nsw i32 %3, 31
  %and = and i32 %add, -32
  %shr = ashr i32 %and, 5
  store i32 %shr, i32* %sb_rows, align 4, !tbaa !9
  %4 = bitcast i32* %num_workers to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %nworkers.addr, align 4, !tbaa !9
  store i32 %5, i32* %num_workers, align 4, !tbaa !9
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %sync_range = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %7, i32 0, i32 1
  %8 = load i32, i32* %sync_range, align 4, !tbaa !35
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %9 = load i32, i32* %sb_rows, align 4, !tbaa !9
  %10 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %10, i32 0, i32 2
  %11 = load i32, i32* %rows, align 4, !tbaa !36
  %cmp = icmp ne i32 %9, %11
  br i1 %cmp, label %if.then, label %lor.lhs.false1

lor.lhs.false1:                                   ; preds = %lor.lhs.false
  %12 = load i32, i32* %num_workers, align 4, !tbaa !9
  %13 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %num_workers2 = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %13, i32 0, i32 4
  %14 = load i32, i32* %num_workers2, align 4, !tbaa !37
  %cmp3 = icmp sgt i32 %12, %14
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false1, %lor.lhs.false, %entry
  %15 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  call void @av1_loop_filter_dealloc(%struct.AV1LfSyncData* %15)
  %16 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %18 = load i32, i32* %sb_rows, align 4, !tbaa !9
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %19, i32 0, i32 2
  %20 = load i32, i32* %width, align 16, !tbaa !38
  %21 = load i32, i32* %num_workers, align 4, !tbaa !9
  call void @loop_filter_alloc(%struct.AV1LfSyncData* %16, %struct.AV1Common* %17, i32 %18, i32 %20, i32 %21)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false1
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !9
  %cmp4 = icmp slt i32 %22, 3
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %cur_sb_col = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %23, i32 0, i32 0
  %24 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds [3 x i32*], [3 x i32*]* %cur_sb_col, i32 0, i32 %24
  %25 = load i32*, i32** %arrayidx, align 4, !tbaa !2
  %26 = bitcast i32* %25 to i8*
  %27 = load i32, i32* %sb_rows, align 4, !tbaa !9
  %mul = mul i32 4, %27
  call void @llvm.memset.p0i8.i32(i8* align 4 %26, i8 -1, i32 %mul, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %28 = load i32, i32* %i, align 4, !tbaa !9
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %29 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %30 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %31 = load i32, i32* %start.addr, align 4, !tbaa !9
  %32 = load i32, i32* %stop.addr, align 4, !tbaa !9
  %33 = load i32, i32* %plane_start.addr, align 4, !tbaa !9
  %34 = load i32, i32* %plane_end.addr, align 4, !tbaa !9
  call void @enqueue_lf_jobs(%struct.AV1LfSyncData* %29, %struct.AV1Common* %30, i32 %31, i32 %32, i32 %33, i32 %34)
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc13, %for.end
  %35 = load i32, i32* %i, align 4, !tbaa !9
  %36 = load i32, i32* %num_workers, align 4, !tbaa !9
  %cmp6 = icmp slt i32 %35, %36
  br i1 %cmp6, label %for.body7, label %for.end15

for.body7:                                        ; preds = %for.cond5
  %37 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #5
  %38 = load %struct.AVxWorker*, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  %39 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx8 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %38, i32 %39
  store %struct.AVxWorker* %arrayidx8, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %40 = bitcast %struct.LoopFilterWorkerData** %lf_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  %41 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %lfdata = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %41, i32 0, i32 3
  %42 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lfdata, align 4, !tbaa !6
  %43 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx9 = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %42, i32 %43
  store %struct.LoopFilterWorkerData* %arrayidx9, %struct.LoopFilterWorkerData** %lf_data, align 4, !tbaa !2
  %44 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %hook = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %44, i32 0, i32 3
  store i32 (i8*, i8*)* @loop_filter_row_worker, i32 (i8*, i8*)** %hook, align 4, !tbaa !39
  %45 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %46 = bitcast %struct.AV1LfSyncData* %45 to i8*
  %47 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %47, i32 0, i32 4
  store i8* %46, i8** %data1, align 4, !tbaa !41
  %48 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data, align 4, !tbaa !2
  %49 = bitcast %struct.LoopFilterWorkerData* %48 to i8*
  %50 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data2 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %50, i32 0, i32 5
  store i8* %49, i8** %data2, align 4, !tbaa !42
  %51 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data, align 4, !tbaa !2
  %52 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %53 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %54 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  call void @loop_filter_data_reset(%struct.LoopFilterWorkerData* %51, %struct.yv12_buffer_config* %52, %struct.AV1Common* %53, %struct.macroblockd* %54)
  %55 = load i32, i32* %i, align 4, !tbaa !9
  %56 = load i32, i32* %num_workers, align 4, !tbaa !9
  %sub = sub nsw i32 %56, 1
  %cmp10 = icmp eq i32 %55, %sub
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %for.body7
  %57 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %execute = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %57, i32 0, i32 4
  %58 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %execute, align 4, !tbaa !43
  %59 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  call void %58(%struct.AVxWorker* %59)
  br label %if.end12

if.else:                                          ; preds = %for.body7
  %60 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %launch = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %60, i32 0, i32 3
  %61 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %launch, align 4, !tbaa !45
  %62 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  call void %61(%struct.AVxWorker* %62)
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then11
  %63 = bitcast %struct.LoopFilterWorkerData** %lf_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  %64 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  br label %for.inc13

for.inc13:                                        ; preds = %if.end12
  %65 = load i32, i32* %i, align 4, !tbaa !9
  %inc14 = add nsw i32 %65, 1
  store i32 %inc14, i32* %i, align 4, !tbaa !9
  br label %for.cond5

for.end15:                                        ; preds = %for.cond5
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc21, %for.end15
  %66 = load i32, i32* %i, align 4, !tbaa !9
  %67 = load i32, i32* %num_workers, align 4, !tbaa !9
  %cmp17 = icmp slt i32 %66, %67
  br i1 %cmp17, label %for.body18, label %for.end23

for.body18:                                       ; preds = %for.cond16
  %68 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %68, i32 0, i32 2
  %69 = load i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)** %sync, align 4, !tbaa !46
  %70 = load %struct.AVxWorker*, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  %71 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx19 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %70, i32 %71
  %call20 = call i32 %69(%struct.AVxWorker* %arrayidx19)
  br label %for.inc21

for.inc21:                                        ; preds = %for.body18
  %72 = load i32, i32* %i, align 4, !tbaa !9
  %inc22 = add nsw i32 %72, 1
  store i32 %inc22, i32* %i, align 4, !tbaa !9
  br label %for.cond16

for.end23:                                        ; preds = %for.cond16
  %73 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #5
  %74 = bitcast i32* %num_workers to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  %75 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #5
  %76 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_loop_restoration_dealloc(%struct.AV1LrSyncData* %lr_sync, i32 %num_workers) #0 {
entry:
  %lr_sync.addr = alloca %struct.AV1LrSyncData*, align 4
  %num_workers.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %worker_idx = alloca i32, align 4
  %workerdata_data = alloca %struct.LoopRestorationWorkerData*, align 4
  store %struct.AV1LrSyncData* %lr_sync, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  store i32 %num_workers, i32* %num_workers.addr, align 4, !tbaa !9
  %0 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %cmp = icmp ne %struct.AV1LrSyncData* %0, null
  br i1 %cmp, label %if.then, label %if.end11

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %j, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %2 = load i32, i32* %j, align 4, !tbaa !9
  %cmp1 = icmp slt i32 %2, 3
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %cur_sb_col = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %3, i32 0, i32 0
  %4 = load i32, i32* %j, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds [3 x i32*], [3 x i32*]* %cur_sb_col, i32 0, i32 %4
  %5 = load i32*, i32** %arrayidx, align 4, !tbaa !2
  %6 = bitcast i32* %5 to i8*
  call void @aom_free(i8* %6)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %j, align 4, !tbaa !9
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %j, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %job_queue = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %8, i32 0, i32 6
  %9 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %job_queue, align 4, !tbaa !47
  %10 = bitcast %struct.AV1LrMTInfo* %9 to i8*
  call void @aom_free(i8* %10)
  %11 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %11, i32 0, i32 5
  %12 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata, align 4, !tbaa !49
  %tobool = icmp ne %struct.LoopRestorationWorkerData* %12, null
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %for.end
  %13 = bitcast i32* %worker_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  store i32 0, i32* %worker_idx, align 4, !tbaa !9
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc7, %if.then2
  %14 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %15 = load i32, i32* %num_workers.addr, align 4, !tbaa !9
  %sub = sub nsw i32 %15, 1
  %cmp4 = icmp slt i32 %14, %sub
  br i1 %cmp4, label %for.body5, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond3
  %16 = bitcast i32* %worker_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  br label %for.end9

for.body5:                                        ; preds = %for.cond3
  %17 = bitcast %struct.LoopRestorationWorkerData** %workerdata_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata6 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %18, i32 0, i32 5
  %19 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata6, align 4, !tbaa !49
  %20 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %add.ptr = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %19, i32 %20
  store %struct.LoopRestorationWorkerData* %add.ptr, %struct.LoopRestorationWorkerData** %workerdata_data, align 4, !tbaa !2
  %21 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %workerdata_data, align 4, !tbaa !2
  %rst_tmpbuf = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %21, i32 0, i32 0
  %22 = load i32*, i32** %rst_tmpbuf, align 4, !tbaa !50
  %23 = bitcast i32* %22 to i8*
  call void @aom_free(i8* %23)
  %24 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %workerdata_data, align 4, !tbaa !2
  %rlbs = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %24, i32 0, i32 1
  %25 = load i8*, i8** %rlbs, align 4, !tbaa !52
  call void @aom_free(i8* %25)
  %26 = bitcast %struct.LoopRestorationWorkerData** %workerdata_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  br label %for.inc7

for.inc7:                                         ; preds = %for.body5
  %27 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %inc8 = add nsw i32 %27, 1
  store i32 %inc8, i32* %worker_idx, align 4, !tbaa !9
  br label %for.cond3

for.end9:                                         ; preds = %for.cond.cleanup
  %28 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata10 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %28, i32 0, i32 5
  %29 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata10, align 4, !tbaa !49
  %30 = bitcast %struct.LoopRestorationWorkerData* %29 to i8*
  call void @aom_free(i8* %30)
  br label %if.end

if.end:                                           ; preds = %for.end9, %for.end
  %31 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %32 = bitcast %struct.AV1LrSyncData* %31 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %32, i8 0, i32 44, i1 false)
  %33 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  br label %if.end11

if.end11:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_loop_restoration_filter_frame_mt(%struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, i32 %optimized_lr, %struct.AVxWorker* %workers, i32 %num_workers, %struct.AV1LrSyncData* %lr_sync, i8* %lr_ctxt) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %optimized_lr.addr = alloca i32, align 4
  %workers.addr = alloca %struct.AVxWorker*, align 4
  %num_workers.addr = alloca i32, align 4
  %lr_sync.addr = alloca %struct.AV1LrSyncData*, align 4
  %lr_ctxt.addr = alloca i8*, align 4
  %num_planes = alloca i32, align 4
  %loop_rest_ctxt = alloca %struct.AV1LrStruct*, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %optimized_lr, i32* %optimized_lr.addr, align 4, !tbaa !9
  store %struct.AVxWorker* %workers, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  store i32 %num_workers, i32* %num_workers.addr, align 4, !tbaa !9
  store %struct.AV1LrSyncData* %lr_sync, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  store i8* %lr_ctxt, i8** %lr_ctxt.addr, align 4, !tbaa !2
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !9
  %2 = bitcast %struct.AV1LrStruct** %loop_rest_ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i8*, i8** %lr_ctxt.addr, align 4, !tbaa !2
  %4 = bitcast i8* %3 to %struct.AV1LrStruct*
  store %struct.AV1LrStruct* %4, %struct.AV1LrStruct** %loop_rest_ctxt, align 4, !tbaa !2
  %5 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %loop_rest_ctxt, align 4, !tbaa !2
  %6 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %8 = load i32, i32* %optimized_lr.addr, align 4, !tbaa !9
  %9 = load i32, i32* %num_planes, align 4, !tbaa !9
  call void @av1_loop_restoration_filter_frame_init(%struct.AV1LrStruct* %5, %struct.yv12_buffer_config* %6, %struct.AV1Common* %7, i32 %8, i32 %9)
  %10 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %loop_rest_ctxt, align 4, !tbaa !2
  %11 = load %struct.AVxWorker*, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  %12 = load i32, i32* %num_workers.addr, align 4, !tbaa !9
  %13 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  call void @foreach_rest_unit_in_planes_mt(%struct.AV1LrStruct* %10, %struct.AVxWorker* %11, i32 %12, %struct.AV1LrSyncData* %13, %struct.AV1Common* %14)
  %15 = bitcast %struct.AV1LrStruct** %loop_rest_ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %16 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_num_planes(%struct.AV1Common* %cm) #4 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %1 = load i8, i8* %monochrome, align 1, !tbaa !53
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 3
  ret i32 %cond
}

declare void @av1_loop_restoration_filter_frame_init(%struct.AV1LrStruct*, %struct.yv12_buffer_config*, %struct.AV1Common*, i32, i32) #2

; Function Attrs: nounwind
define internal void @foreach_rest_unit_in_planes_mt(%struct.AV1LrStruct* %lr_ctxt, %struct.AVxWorker* %workers, i32 %nworkers, %struct.AV1LrSyncData* %lr_sync, %struct.AV1Common* %cm) #0 {
entry:
  %lr_ctxt.addr = alloca %struct.AV1LrStruct*, align 4
  %workers.addr = alloca %struct.AVxWorker*, align 4
  %nworkers.addr = alloca i32, align 4
  %lr_sync.addr = alloca %struct.AV1LrSyncData*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ctxt = alloca %struct.FilterFrameCtxt*, align 4
  %num_planes = alloca i32, align 4
  %winterface = alloca %struct.AVxWorkerInterface*, align 4
  %num_rows_lr = alloca i32, align 4
  %plane = alloca i32, align 4
  %tile_rect = alloca %struct.AV1PixelRect, align 4
  %max_tile_h = alloca i32, align 4
  %unit_size = alloca i32, align 4
  %num_workers = alloca i32, align 4
  %i = alloca i32, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  store %struct.AV1LrStruct* %lr_ctxt, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  store %struct.AVxWorker* %workers, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  store i32 %nworkers, i32* %nworkers.addr, align 4, !tbaa !9
  store %struct.AV1LrSyncData* %lr_sync, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %ctxt1 = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [3 x %struct.FilterFrameCtxt], [3 x %struct.FilterFrameCtxt]* %ctxt1, i32 0, i32 0
  store %struct.FilterFrameCtxt* %arraydecay, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %2 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %3)
  store i32 %call, i32* %num_planes, align 4, !tbaa !9
  %4 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %call2 = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  store %struct.AVxWorkerInterface* %call2, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %5 = bitcast i32* %num_rows_lr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  store i32 0, i32* %num_rows_lr, align 4, !tbaa !9
  %6 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  store i32 0, i32* %plane, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %plane, align 4, !tbaa !9
  %8 = load i32, i32* %num_planes, align 4, !tbaa !9
  %cmp = icmp slt i32 %7, %8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %9 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 29
  %11 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %11
  %frame_restoration_type = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx, i32 0, i32 0
  %12 = load i8, i8* %frame_restoration_type, align 4, !tbaa !54
  %conv = zext i8 %12 to i32
  %cmp3 = icmp eq i32 %conv, 0
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %13 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #5
  %14 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %15 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx5 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %14, i32 %15
  %tile_rect6 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx5, i32 0, i32 10
  %16 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  %17 = bitcast %struct.AV1PixelRect* %tile_rect6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !57
  %18 = bitcast i32* %max_tile_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %19 = load i32, i32* %bottom, align 4, !tbaa !58
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %20 = load i32, i32* %top, align 4, !tbaa !59
  %sub = sub nsw i32 %19, %20
  store i32 %sub, i32* %max_tile_h, align 4, !tbaa !9
  %21 = bitcast i32* %unit_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %22, i32 0, i32 29
  %23 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx8 = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info7, i32 0, i32 %23
  %restoration_unit_size = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx8, i32 0, i32 1
  %24 = load i32, i32* %restoration_unit_size, align 4, !tbaa !60
  store i32 %24, i32* %unit_size, align 4, !tbaa !9
  %25 = load i32, i32* %num_rows_lr, align 4, !tbaa !9
  %26 = load i32, i32* %unit_size, align 4, !tbaa !9
  %27 = load i32, i32* %max_tile_h, align 4, !tbaa !9
  %call9 = call i32 @av1_lr_count_units_in_tile(i32 %26, i32 %27)
  %cmp10 = icmp sgt i32 %25, %call9
  br i1 %cmp10, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %28 = load i32, i32* %num_rows_lr, align 4, !tbaa !9
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %29 = load i32, i32* %unit_size, align 4, !tbaa !9
  %30 = load i32, i32* %max_tile_h, align 4, !tbaa !9
  %call12 = call i32 @av1_lr_count_units_in_tile(i32 %29, i32 %30)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %28, %cond.true ], [ %call12, %cond.false ]
  store i32 %cond, i32* %num_rows_lr, align 4, !tbaa !9
  %31 = bitcast i32* %unit_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #5
  %32 = bitcast i32* %max_tile_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #5
  %33 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end, %if.then
  %34 = load i32, i32* %plane, align 4, !tbaa !9
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %35 = bitcast i32* %num_workers to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #5
  %36 = load i32, i32* %nworkers.addr, align 4, !tbaa !9
  store i32 %36, i32* %num_workers, align 4, !tbaa !9
  %37 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #5
  %38 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %sync_range = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %38, i32 0, i32 1
  %39 = load i32, i32* %sync_range, align 4, !tbaa !61
  %tobool = icmp ne i32 %39, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then23

lor.lhs.false:                                    ; preds = %for.end
  %40 = load i32, i32* %num_rows_lr, align 4, !tbaa !9
  %41 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %41, i32 0, i32 2
  %42 = load i32, i32* %rows, align 4, !tbaa !62
  %cmp13 = icmp ne i32 %40, %42
  br i1 %cmp13, label %if.then23, label %lor.lhs.false15

lor.lhs.false15:                                  ; preds = %lor.lhs.false
  %43 = load i32, i32* %num_workers, align 4, !tbaa !9
  %44 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %num_workers16 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %44, i32 0, i32 4
  %45 = load i32, i32* %num_workers16, align 4, !tbaa !63
  %cmp17 = icmp sgt i32 %43, %45
  br i1 %cmp17, label %if.then23, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %lor.lhs.false15
  %46 = load i32, i32* %num_planes, align 4, !tbaa !9
  %47 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %num_planes20 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %47, i32 0, i32 3
  %48 = load i32, i32* %num_planes20, align 4, !tbaa !64
  %cmp21 = icmp ne i32 %46, %48
  br i1 %cmp21, label %if.then23, label %if.end24

if.then23:                                        ; preds = %lor.lhs.false19, %lor.lhs.false15, %lor.lhs.false, %for.end
  %49 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %50 = load i32, i32* %num_workers, align 4, !tbaa !9
  call void @av1_loop_restoration_dealloc(%struct.AV1LrSyncData* %49, i32 %50)
  %51 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %52 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %53 = load i32, i32* %num_workers, align 4, !tbaa !9
  %54 = load i32, i32* %num_rows_lr, align 4, !tbaa !9
  %55 = load i32, i32* %num_planes, align 4, !tbaa !9
  %56 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %56, i32 0, i32 2
  %57 = load i32, i32* %width, align 16, !tbaa !38
  call void @loop_restoration_alloc(%struct.AV1LrSyncData* %51, %struct.AV1Common* %52, i32 %53, i32 %54, i32 %55, i32 %57)
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %lor.lhs.false19
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc30, %if.end24
  %58 = load i32, i32* %i, align 4, !tbaa !9
  %59 = load i32, i32* %num_planes, align 4, !tbaa !9
  %cmp26 = icmp slt i32 %58, %59
  br i1 %cmp26, label %for.body28, label %for.end32

for.body28:                                       ; preds = %for.cond25
  %60 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %cur_sb_col = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %60, i32 0, i32 0
  %61 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx29 = getelementptr inbounds [3 x i32*], [3 x i32*]* %cur_sb_col, i32 0, i32 %61
  %62 = load i32*, i32** %arrayidx29, align 4, !tbaa !2
  %63 = bitcast i32* %62 to i8*
  %64 = load i32, i32* %num_rows_lr, align 4, !tbaa !9
  %mul = mul i32 4, %64
  call void @llvm.memset.p0i8.i32(i8* align 4 %63, i8 -1, i32 %mul, i1 false)
  br label %for.inc30

for.inc30:                                        ; preds = %for.body28
  %65 = load i32, i32* %i, align 4, !tbaa !9
  %inc31 = add nsw i32 %65, 1
  store i32 %inc31, i32* %i, align 4, !tbaa !9
  br label %for.cond25

for.end32:                                        ; preds = %for.cond25
  %66 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %67 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %68 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  call void @enqueue_lr_jobs(%struct.AV1LrSyncData* %66, %struct.AV1LrStruct* %67, %struct.AV1Common* %68)
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc47, %for.end32
  %69 = load i32, i32* %i, align 4, !tbaa !9
  %70 = load i32, i32* %num_workers, align 4, !tbaa !9
  %cmp34 = icmp slt i32 %69, %70
  br i1 %cmp34, label %for.body36, label %for.end49

for.body36:                                       ; preds = %for.cond33
  %71 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #5
  %72 = load %struct.AVxWorker*, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  %73 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx37 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %72, i32 %73
  store %struct.AVxWorker* %arrayidx37, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %74 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %75 = bitcast %struct.AV1LrStruct* %74 to i8*
  %76 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %76, i32 0, i32 5
  %77 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata, align 4, !tbaa !49
  %78 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx38 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %77, i32 %78
  %lr_ctxt39 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %arrayidx38, i32 0, i32 2
  store i8* %75, i8** %lr_ctxt39, align 4, !tbaa !65
  %79 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %hook = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %79, i32 0, i32 3
  store i32 (i8*, i8*)* @loop_restoration_row_worker, i32 (i8*, i8*)** %hook, align 4, !tbaa !39
  %80 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %81 = bitcast %struct.AV1LrSyncData* %80 to i8*
  %82 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %82, i32 0, i32 4
  store i8* %81, i8** %data1, align 4, !tbaa !41
  %83 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata40 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %83, i32 0, i32 5
  %84 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata40, align 4, !tbaa !49
  %85 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx41 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %84, i32 %85
  %86 = bitcast %struct.LoopRestorationWorkerData* %arrayidx41 to i8*
  %87 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data2 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %87, i32 0, i32 5
  store i8* %86, i8** %data2, align 4, !tbaa !42
  %88 = load i32, i32* %i, align 4, !tbaa !9
  %89 = load i32, i32* %num_workers, align 4, !tbaa !9
  %sub42 = sub nsw i32 %89, 1
  %cmp43 = icmp eq i32 %88, %sub42
  br i1 %cmp43, label %if.then45, label %if.else

if.then45:                                        ; preds = %for.body36
  %90 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %execute = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %90, i32 0, i32 4
  %91 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %execute, align 4, !tbaa !43
  %92 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  call void %91(%struct.AVxWorker* %92)
  br label %if.end46

if.else:                                          ; preds = %for.body36
  %93 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %launch = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %93, i32 0, i32 3
  %94 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %launch, align 4, !tbaa !45
  %95 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  call void %94(%struct.AVxWorker* %95)
  br label %if.end46

if.end46:                                         ; preds = %if.else, %if.then45
  %96 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #5
  br label %for.inc47

for.inc47:                                        ; preds = %if.end46
  %97 = load i32, i32* %i, align 4, !tbaa !9
  %inc48 = add nsw i32 %97, 1
  store i32 %inc48, i32* %i, align 4, !tbaa !9
  br label %for.cond33

for.end49:                                        ; preds = %for.cond33
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %for.cond50

for.cond50:                                       ; preds = %for.inc56, %for.end49
  %98 = load i32, i32* %i, align 4, !tbaa !9
  %99 = load i32, i32* %num_workers, align 4, !tbaa !9
  %cmp51 = icmp slt i32 %98, %99
  br i1 %cmp51, label %for.body53, label %for.end58

for.body53:                                       ; preds = %for.cond50
  %100 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %100, i32 0, i32 2
  %101 = load i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)** %sync, align 4, !tbaa !46
  %102 = load %struct.AVxWorker*, %struct.AVxWorker** %workers.addr, align 4, !tbaa !2
  %103 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx54 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %102, i32 %103
  %call55 = call i32 %101(%struct.AVxWorker* %arrayidx54)
  br label %for.inc56

for.inc56:                                        ; preds = %for.body53
  %104 = load i32, i32* %i, align 4, !tbaa !9
  %inc57 = add nsw i32 %104, 1
  store i32 %inc57, i32* %i, align 4, !tbaa !9
  br label %for.cond50

for.end58:                                        ; preds = %for.cond50
  %105 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #5
  %106 = bitcast i32* %num_workers to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #5
  %107 = bitcast i32* %num_rows_lr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #5
  %108 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  %109 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #5
  %110 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #5
  ret void
}

declare %struct.AVxWorkerInterface* @aom_get_worker_interface() #2

; Function Attrs: nounwind
define internal void @loop_filter_alloc(%struct.AV1LfSyncData* %lf_sync, %struct.AV1Common* %cm, i32 %rows, i32 %width, i32 %num_workers) #0 {
entry:
  %lf_sync.addr = alloca %struct.AV1LfSyncData*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %rows.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %num_workers.addr = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.AV1LfSyncData* %lf_sync, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !9
  store i32 %width, i32* %width.addr, align 4, !tbaa !9
  store i32 %num_workers, i32* %num_workers.addr, align 4, !tbaa !9
  %0 = load i32, i32* %rows.addr, align 4, !tbaa !9
  %1 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %rows1 = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %1, i32 0, i32 2
  store i32 %0, i32* %rows1, align 4, !tbaa !36
  br label %do.body

do.body:                                          ; preds = %entry
  %2 = load i32, i32* %num_workers.addr, align 4, !tbaa !9
  %mul = mul i32 %2, 4056
  %call = call i8* @aom_malloc(i32 %mul)
  %3 = bitcast i8* %call to %struct.LoopFilterWorkerData*
  %4 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %lfdata = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %4, i32 0, i32 3
  store %struct.LoopFilterWorkerData* %3, %struct.LoopFilterWorkerData** %lfdata, align 4, !tbaa !6
  %5 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %lfdata2 = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %5, i32 0, i32 3
  %6 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lfdata2, align 4, !tbaa !6
  %tobool = icmp ne %struct.LoopFilterWorkerData* %6, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 2, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %do.body
  br label %do.end

do.end:                                           ; preds = %if.end
  %8 = load i32, i32* %num_workers.addr, align 4, !tbaa !9
  %9 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %num_workers3 = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %9, i32 0, i32 4
  store i32 %8, i32* %num_workers3, align 4, !tbaa !37
  %10 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  store i32 0, i32* %j, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end
  %11 = load i32, i32* %j, align 4, !tbaa !9
  %cmp = icmp slt i32 %11, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  br label %do.body4

do.body4:                                         ; preds = %for.body
  %13 = load i32, i32* %rows.addr, align 4, !tbaa !9
  %mul5 = mul i32 4, %13
  %call6 = call i8* @aom_malloc(i32 %mul5)
  %14 = bitcast i8* %call6 to i32*
  %15 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %cur_sb_col = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %15, i32 0, i32 0
  %16 = load i32, i32* %j, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds [3 x i32*], [3 x i32*]* %cur_sb_col, i32 0, i32 %16
  store i32* %14, i32** %arrayidx, align 4, !tbaa !2
  %17 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %cur_sb_col7 = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %17, i32 0, i32 0
  %18 = load i32, i32* %j, align 4, !tbaa !9
  %arrayidx8 = getelementptr inbounds [3 x i32*], [3 x i32*]* %cur_sb_col7, i32 0, i32 %18
  %19 = load i32*, i32** %arrayidx8, align 4, !tbaa !2
  %tobool9 = icmp ne i32* %19, null
  br i1 %tobool9, label %if.end12, label %if.then10

if.then10:                                        ; preds = %do.body4
  %20 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error11 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %20, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error11, i32 2, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %do.body4
  br label %do.cond

do.cond:                                          ; preds = %if.end12
  br label %do.end13

do.end13:                                         ; preds = %do.cond
  br label %for.inc

for.inc:                                          ; preds = %do.end13
  %21 = load i32, i32* %j, align 4, !tbaa !9
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %j, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %do.body14

do.body14:                                        ; preds = %for.end
  %22 = load i32, i32* %rows.addr, align 4, !tbaa !9
  %mul15 = mul i32 12, %22
  %mul16 = mul i32 %mul15, 3
  %mul17 = mul i32 %mul16, 2
  %call18 = call i8* @aom_malloc(i32 %mul17)
  %23 = bitcast i8* %call18 to %struct.AV1LfMTInfo*
  %24 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %job_queue = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %24, i32 0, i32 5
  store %struct.AV1LfMTInfo* %23, %struct.AV1LfMTInfo** %job_queue, align 4, !tbaa !10
  %25 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %job_queue19 = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %25, i32 0, i32 5
  %26 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %job_queue19, align 4, !tbaa !10
  %tobool20 = icmp ne %struct.AV1LfMTInfo* %26, null
  br i1 %tobool20, label %if.end23, label %if.then21

if.then21:                                        ; preds = %do.body14
  %27 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error22 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %27, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error22, i32 2, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end23

if.end23:                                         ; preds = %if.then21, %do.body14
  br label %do.end25

do.end25:                                         ; preds = %if.end23
  %28 = load i32, i32* %width.addr, align 4, !tbaa !9
  %call26 = call i32 @get_sync_range(i32 %28)
  %29 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %sync_range = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %29, i32 0, i32 1
  store i32 %call26, i32* %sync_range, align 4, !tbaa !35
  ret void
}

; Function Attrs: nounwind
define internal void @enqueue_lf_jobs(%struct.AV1LfSyncData* %lf_sync, %struct.AV1Common* %cm, i32 %start, i32 %stop, i32 %plane_start, i32 %plane_end) #0 {
entry:
  %lf_sync.addr = alloca %struct.AV1LfSyncData*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %start.addr = alloca i32, align 4
  %stop.addr = alloca i32, align 4
  %plane_start.addr = alloca i32, align 4
  %plane_end.addr = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %plane = alloca i32, align 4
  %dir = alloca i32, align 4
  %lf_job_queue = alloca %struct.AV1LfMTInfo*, align 4
  store %struct.AV1LfSyncData* %lf_sync, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !9
  store i32 %stop, i32* %stop.addr, align 4, !tbaa !9
  store i32 %plane_start, i32* %plane_start.addr, align 4, !tbaa !9
  store i32 %plane_end, i32* %plane_end.addr, align 4, !tbaa !9
  %0 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast %struct.AV1LfMTInfo** %lf_job_queue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %job_queue = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %4, i32 0, i32 5
  %5 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %job_queue, align 4, !tbaa !10
  store %struct.AV1LfMTInfo* %5, %struct.AV1LfMTInfo** %lf_job_queue, align 4, !tbaa !2
  %6 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %jobs_enqueued = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %6, i32 0, i32 6
  store i32 0, i32* %jobs_enqueued, align 4, !tbaa !66
  %7 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %jobs_dequeued = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %7, i32 0, i32 7
  store i32 0, i32* %jobs_dequeued, align 4, !tbaa !67
  store i32 0, i32* %dir, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc33, %entry
  %8 = load i32, i32* %dir, align 4, !tbaa !9
  %cmp = icmp slt i32 %8, 2
  br i1 %cmp, label %for.body, label %for.end35

for.body:                                         ; preds = %for.cond
  %9 = load i32, i32* %plane_start.addr, align 4, !tbaa !9
  store i32 %9, i32* %plane, align 4, !tbaa !9
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc30, %for.body
  %10 = load i32, i32* %plane, align 4, !tbaa !9
  %11 = load i32, i32* %plane_end.addr, align 4, !tbaa !9
  %cmp2 = icmp slt i32 %10, %11
  br i1 %cmp2, label %for.body3, label %for.end32

for.body3:                                        ; preds = %for.cond1
  %12 = load i32, i32* %plane, align 4, !tbaa !9
  %cmp4 = icmp eq i32 %12, 0
  br i1 %cmp4, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.body3
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 28
  %filter_level = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %filter_level, i32 0, i32 0
  %14 = load i32, i32* %arrayidx, align 16, !tbaa !9
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.else, label %land.lhs.true5

land.lhs.true5:                                   ; preds = %land.lhs.true
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 28
  %filter_level7 = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf6, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [2 x i32], [2 x i32]* %filter_level7, i32 0, i32 1
  %16 = load i32, i32* %arrayidx8, align 4, !tbaa !9
  %tobool9 = icmp ne i32 %16, 0
  br i1 %tobool9, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true5
  br label %for.end32

if.else:                                          ; preds = %land.lhs.true5, %land.lhs.true, %for.body3
  %17 = load i32, i32* %plane, align 4, !tbaa !9
  %cmp10 = icmp eq i32 %17, 1
  br i1 %cmp10, label %land.lhs.true11, label %if.else15

land.lhs.true11:                                  ; preds = %if.else
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf12 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 28
  %filter_level_u = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf12, i32 0, i32 1
  %19 = load i32, i32* %filter_level_u, align 8, !tbaa !68
  %tobool13 = icmp ne i32 %19, 0
  br i1 %tobool13, label %if.else15, label %if.then14

if.then14:                                        ; preds = %land.lhs.true11
  br label %for.inc30

if.else15:                                        ; preds = %land.lhs.true11, %if.else
  %20 = load i32, i32* %plane, align 4, !tbaa !9
  %cmp16 = icmp eq i32 %20, 2
  br i1 %cmp16, label %land.lhs.true17, label %if.end

land.lhs.true17:                                  ; preds = %if.else15
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf18 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 28
  %filter_level_v = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf18, i32 0, i32 2
  %22 = load i32, i32* %filter_level_v, align 4, !tbaa !69
  %tobool19 = icmp ne i32 %22, 0
  br i1 %tobool19, label %if.end, label %if.then20

if.then20:                                        ; preds = %land.lhs.true17
  br label %for.inc30

if.end:                                           ; preds = %land.lhs.true17, %if.else15
  br label %if.end21

if.end21:                                         ; preds = %if.end
  br label %if.end22

if.end22:                                         ; preds = %if.end21
  %23 = load i32, i32* %start.addr, align 4, !tbaa !9
  store i32 %23, i32* %mi_row, align 4, !tbaa !9
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc, %if.end22
  %24 = load i32, i32* %mi_row, align 4, !tbaa !9
  %25 = load i32, i32* %stop.addr, align 4, !tbaa !9
  %cmp24 = icmp slt i32 %24, %25
  br i1 %cmp24, label %for.body25, label %for.end

for.body25:                                       ; preds = %for.cond23
  %26 = load i32, i32* %mi_row, align 4, !tbaa !9
  %27 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %lf_job_queue, align 4, !tbaa !2
  %mi_row26 = getelementptr inbounds %struct.AV1LfMTInfo, %struct.AV1LfMTInfo* %27, i32 0, i32 0
  store i32 %26, i32* %mi_row26, align 4, !tbaa !70
  %28 = load i32, i32* %plane, align 4, !tbaa !9
  %29 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %lf_job_queue, align 4, !tbaa !2
  %plane27 = getelementptr inbounds %struct.AV1LfMTInfo, %struct.AV1LfMTInfo* %29, i32 0, i32 1
  store i32 %28, i32* %plane27, align 4, !tbaa !72
  %30 = load i32, i32* %dir, align 4, !tbaa !9
  %31 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %lf_job_queue, align 4, !tbaa !2
  %dir28 = getelementptr inbounds %struct.AV1LfMTInfo, %struct.AV1LfMTInfo* %31, i32 0, i32 2
  store i32 %30, i32* %dir28, align 4, !tbaa !73
  %32 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %lf_job_queue, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.AV1LfMTInfo, %struct.AV1LfMTInfo* %32, i32 1
  store %struct.AV1LfMTInfo* %incdec.ptr, %struct.AV1LfMTInfo** %lf_job_queue, align 4, !tbaa !2
  %33 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %jobs_enqueued29 = getelementptr inbounds %struct.AV1LfSyncData, %struct.AV1LfSyncData* %33, i32 0, i32 6
  %34 = load i32, i32* %jobs_enqueued29, align 4, !tbaa !66
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %jobs_enqueued29, align 4, !tbaa !66
  br label %for.inc

for.inc:                                          ; preds = %for.body25
  %35 = load i32, i32* %mi_row, align 4, !tbaa !9
  %add = add nsw i32 %35, 32
  store i32 %add, i32* %mi_row, align 4, !tbaa !9
  br label %for.cond23

for.end:                                          ; preds = %for.cond23
  br label %for.inc30

for.inc30:                                        ; preds = %for.end, %if.then20, %if.then14
  %36 = load i32, i32* %plane, align 4, !tbaa !9
  %inc31 = add nsw i32 %36, 1
  store i32 %inc31, i32* %plane, align 4, !tbaa !9
  br label %for.cond1

for.end32:                                        ; preds = %if.then, %for.cond1
  br label %for.inc33

for.inc33:                                        ; preds = %for.end32
  %37 = load i32, i32* %dir, align 4, !tbaa !9
  %inc34 = add nsw i32 %37, 1
  store i32 %inc34, i32* %dir, align 4, !tbaa !9
  br label %for.cond

for.end35:                                        ; preds = %for.cond
  %38 = bitcast %struct.AV1LfMTInfo** %lf_job_queue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #5
  %39 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #5
  %40 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #5
  %41 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #5
  ret void
}

; Function Attrs: nounwind
define internal i32 @loop_filter_row_worker(i8* %arg1, i8* %arg2) #0 {
entry:
  %arg1.addr = alloca i8*, align 4
  %arg2.addr = alloca i8*, align 4
  %lf_sync = alloca %struct.AV1LfSyncData*, align 4
  %lf_data = alloca %struct.LoopFilterWorkerData*, align 4
  store i8* %arg1, i8** %arg1.addr, align 4, !tbaa !2
  store i8* %arg2, i8** %arg2.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AV1LfSyncData** %lf_sync to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %arg1.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.AV1LfSyncData*
  store %struct.AV1LfSyncData* %2, %struct.AV1LfSyncData** %lf_sync, align 4, !tbaa !2
  %3 = bitcast %struct.LoopFilterWorkerData** %lf_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i8*, i8** %arg2.addr, align 4, !tbaa !2
  %5 = bitcast i8* %4 to %struct.LoopFilterWorkerData*
  store %struct.LoopFilterWorkerData* %5, %struct.LoopFilterWorkerData** %lf_data, align 4, !tbaa !2
  %6 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data, align 4, !tbaa !2
  %frame_buffer = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %6, i32 0, i32 0
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_buffer, align 4, !tbaa !74
  %8 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data, align 4, !tbaa !2
  %cm = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %8, i32 0, i32 1
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !76
  %10 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data, align 4, !tbaa !2
  %planes = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %10, i32 0, i32 2
  %arraydecay = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %planes, i32 0, i32 0
  %11 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data, align 4, !tbaa !2
  %xd = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %11, i32 0, i32 3
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd, align 4, !tbaa !77
  %13 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync, align 4, !tbaa !2
  call void @thread_loop_filter_rows(%struct.yv12_buffer_config* %7, %struct.AV1Common* %9, %struct.macroblockd_plane* %arraydecay, %struct.macroblockd* %12, %struct.AV1LfSyncData* %13)
  %14 = bitcast %struct.LoopFilterWorkerData** %lf_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  %15 = bitcast %struct.AV1LfSyncData** %lf_sync to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  ret i32 1
}

; Function Attrs: nounwind
define internal void @loop_filter_data_reset(%struct.LoopFilterWorkerData* %lf_data, %struct.yv12_buffer_config* %frame_buffer, %struct.AV1Common* %cm, %struct.macroblockd* %xd) #0 {
entry:
  %lf_data.addr = alloca %struct.LoopFilterWorkerData*, align 4
  %frame_buffer.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %i = alloca i32, align 4
  store %struct.LoopFilterWorkerData* %lf_data, %struct.LoopFilterWorkerData** %lf_data.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %frame_buffer, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 4
  %arraydecay = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 0
  store %struct.macroblockd_plane* %arraydecay, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  %3 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data.addr, align 4, !tbaa !2
  %frame_buffer1 = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %3, i32 0, i32 0
  store %struct.yv12_buffer_config* %2, %struct.yv12_buffer_config** %frame_buffer1, align 4, !tbaa !74
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %5 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data.addr, align 4, !tbaa !2
  %cm2 = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %5, i32 0, i32 1
  store %struct.AV1Common* %4, %struct.AV1Common** %cm2, align 4, !tbaa !76
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %7 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data.addr, align 4, !tbaa !2
  %xd3 = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %7, i32 0, i32 3
  store %struct.macroblockd* %6, %struct.macroblockd** %xd3, align 4, !tbaa !77
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %i, align 4, !tbaa !9
  %cmp = icmp slt i32 %9, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data.addr, align 4, !tbaa !2
  %planes = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %11, i32 0, i32 2
  %12 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %planes, i32 0, i32 %12
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx, i32 0, i32 6
  %13 = bitcast %struct.buf_2d* %dst to i8*
  %14 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %15 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx4 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %14, i32 %15
  %dst5 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx4, i32 0, i32 6
  %16 = bitcast %struct.buf_2d* %dst5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %16, i32 20, i1 false)
  %17 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx6 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %17, i32 %18
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx6, i32 0, i32 4
  %19 = load i32, i32* %subsampling_x, align 4, !tbaa !78
  %20 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data.addr, align 4, !tbaa !2
  %planes7 = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %20, i32 0, i32 2
  %21 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx8 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %planes7, i32 0, i32 %21
  %subsampling_x9 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx8, i32 0, i32 4
  store i32 %19, i32* %subsampling_x9, align 4, !tbaa !78
  %22 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx10 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %22, i32 %23
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx10, i32 0, i32 5
  %24 = load i32, i32* %subsampling_y, align 4, !tbaa !81
  %25 = load %struct.LoopFilterWorkerData*, %struct.LoopFilterWorkerData** %lf_data.addr, align 4, !tbaa !2
  %planes11 = getelementptr inbounds %struct.LoopFilterWorkerData, %struct.LoopFilterWorkerData* %25, i32 0, i32 2
  %26 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx12 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %planes11, i32 0, i32 %26
  %subsampling_y13 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx12, i32 0, i32 5
  store i32 %24, i32* %subsampling_y13, align 4, !tbaa !81
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %i, align 4, !tbaa !9
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %28 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  ret void
}

declare i8* @aom_malloc(i32) #2

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #2

; Function Attrs: inlinehint nounwind
define internal i32 @get_sync_range(i32 %width) #4 {
entry:
  %retval = alloca i32, align 4
  %width.addr = alloca i32, align 4
  store i32 %width, i32* %width.addr, align 4, !tbaa !9
  %0 = load i32, i32* %width.addr, align 4, !tbaa !9
  %cmp = icmp slt i32 %0, 640
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %1 = load i32, i32* %width.addr, align 4, !tbaa !9
  %cmp1 = icmp sle i32 %1, 1280
  br i1 %cmp1, label %if.then2, label %if.else3

if.then2:                                         ; preds = %if.else
  store i32 2, i32* %retval, align 4
  br label %return

if.else3:                                         ; preds = %if.else
  %2 = load i32, i32* %width.addr, align 4, !tbaa !9
  %cmp4 = icmp sle i32 %2, 4096
  br i1 %cmp4, label %if.then5, label %if.else6

if.then5:                                         ; preds = %if.else3
  store i32 4, i32* %retval, align 4
  br label %return

if.else6:                                         ; preds = %if.else3
  store i32 8, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else6, %if.then5, %if.then2, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: inlinehint nounwind
define internal void @thread_loop_filter_rows(%struct.yv12_buffer_config* %frame_buffer, %struct.AV1Common* %cm, %struct.macroblockd_plane* %planes, %struct.macroblockd* %xd, %struct.AV1LfSyncData* %lf_sync) #4 {
entry:
  %frame_buffer.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %planes.addr = alloca %struct.macroblockd_plane*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %lf_sync.addr = alloca %struct.AV1LfSyncData*, align 4
  %sb_cols = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %plane = alloca i32, align 4
  %dir = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %cur_job_info = alloca %struct.AV1LfMTInfo*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %frame_buffer, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd_plane* %planes, %struct.macroblockd_plane** %planes.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store %struct.AV1LfSyncData* %lf_sync, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sb_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %2 = load i32, i32* %mi_cols, align 4, !tbaa !82
  %add = add nsw i32 %2, 31
  %and = and i32 %add, -32
  %shr = ashr i32 %and, 5
  store i32 %shr, i32* %sb_cols, align 4, !tbaa !9
  %3 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  br label %while.body

while.body:                                       ; preds = %while.cond
  %9 = bitcast %struct.AV1LfMTInfo** %cur_job_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %call = call %struct.AV1LfMTInfo* @get_lf_job_info(%struct.AV1LfSyncData* %10)
  store %struct.AV1LfMTInfo* %call, %struct.AV1LfMTInfo** %cur_job_info, align 4, !tbaa !2
  %11 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %cur_job_info, align 4, !tbaa !2
  %cmp = icmp ne %struct.AV1LfMTInfo* %11, null
  br i1 %cmp, label %if.then, label %if.else30

if.then:                                          ; preds = %while.body
  %12 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %cur_job_info, align 4, !tbaa !2
  %mi_row1 = getelementptr inbounds %struct.AV1LfMTInfo, %struct.AV1LfMTInfo* %12, i32 0, i32 0
  %13 = load i32, i32* %mi_row1, align 4, !tbaa !70
  store i32 %13, i32* %mi_row, align 4, !tbaa !9
  %14 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %cur_job_info, align 4, !tbaa !2
  %plane2 = getelementptr inbounds %struct.AV1LfMTInfo, %struct.AV1LfMTInfo* %14, i32 0, i32 1
  %15 = load i32, i32* %plane2, align 4, !tbaa !72
  store i32 %15, i32* %plane, align 4, !tbaa !9
  %16 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %cur_job_info, align 4, !tbaa !2
  %dir3 = getelementptr inbounds %struct.AV1LfMTInfo, %struct.AV1LfMTInfo* %16, i32 0, i32 2
  %17 = load i32, i32* %dir3, align 4, !tbaa !73
  store i32 %17, i32* %dir, align 4, !tbaa !9
  %18 = load i32, i32* %mi_row, align 4, !tbaa !9
  %shr4 = ashr i32 %18, 5
  store i32 %shr4, i32* %r, align 4, !tbaa !9
  %19 = load i32, i32* %dir, align 4, !tbaa !9
  %cmp5 = icmp eq i32 %19, 0
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then
  store i32 0, i32* %mi_col, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then6
  %20 = load i32, i32* %mi_col, align 4, !tbaa !9
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 22
  %mi_cols8 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params7, i32 0, i32 4
  %22 = load i32, i32* %mi_cols8, align 4, !tbaa !82
  %cmp9 = icmp slt i32 %20, %22
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load i32, i32* %mi_col, align 4, !tbaa !9
  %shr10 = ashr i32 %23, 5
  store i32 %shr10, i32* %c, align 4, !tbaa !9
  %24 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %planes.addr, align 4, !tbaa !2
  %25 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %25, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 7
  %26 = load i8, i8* %sb_size, align 4, !tbaa !83
  %27 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  %28 = load i32, i32* %mi_row, align 4, !tbaa !9
  %29 = load i32, i32* %mi_col, align 4, !tbaa !9
  %30 = load i32, i32* %plane, align 4, !tbaa !9
  %31 = load i32, i32* %plane, align 4, !tbaa !9
  %add11 = add nsw i32 %31, 1
  call void @av1_setup_dst_planes(%struct.macroblockd_plane* %24, i8 zeroext %26, %struct.yv12_buffer_config* %27, i32 %28, i32 %29, i32 %30, i32 %add11)
  %32 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %33 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %34 = load i32, i32* %plane, align 4, !tbaa !9
  %35 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %planes.addr, align 4, !tbaa !2
  %36 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %35, i32 %36
  %37 = load i32, i32* %mi_row, align 4, !tbaa !9
  %38 = load i32, i32* %mi_col, align 4, !tbaa !9
  call void @av1_filter_block_plane_vert(%struct.AV1Common* %32, %struct.macroblockd* %33, i32 %34, %struct.macroblockd_plane* %arrayidx, i32 %37, i32 %38)
  %39 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %40 = load i32, i32* %r, align 4, !tbaa !9
  %41 = load i32, i32* %c, align 4, !tbaa !9
  %42 = load i32, i32* %sb_cols, align 4, !tbaa !9
  %43 = load i32, i32* %plane, align 4, !tbaa !9
  call void @sync_write(%struct.AV1LfSyncData* %39, i32 %40, i32 %41, i32 %42, i32 %43)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %mi_col, align 4, !tbaa !9
  %add12 = add nsw i32 %44, 32
  store i32 %add12, i32* %mi_col, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end29

if.else:                                          ; preds = %if.then
  %45 = load i32, i32* %dir, align 4, !tbaa !9
  %cmp13 = icmp eq i32 %45, 1
  br i1 %cmp13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.else
  store i32 0, i32* %mi_col, align 4, !tbaa !9
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc26, %if.then14
  %46 = load i32, i32* %mi_col, align 4, !tbaa !9
  %47 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params16 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %47, i32 0, i32 22
  %mi_cols17 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params16, i32 0, i32 4
  %48 = load i32, i32* %mi_cols17, align 4, !tbaa !82
  %cmp18 = icmp slt i32 %46, %48
  br i1 %cmp18, label %for.body19, label %for.end28

for.body19:                                       ; preds = %for.cond15
  %49 = load i32, i32* %mi_col, align 4, !tbaa !9
  %shr20 = ashr i32 %49, 5
  store i32 %shr20, i32* %c, align 4, !tbaa !9
  %50 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %51 = load i32, i32* %r, align 4, !tbaa !9
  %52 = load i32, i32* %c, align 4, !tbaa !9
  %53 = load i32, i32* %plane, align 4, !tbaa !9
  call void @sync_read(%struct.AV1LfSyncData* %50, i32 %51, i32 %52, i32 %53)
  %54 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %55 = load i32, i32* %r, align 4, !tbaa !9
  %add21 = add nsw i32 %55, 1
  %56 = load i32, i32* %c, align 4, !tbaa !9
  %57 = load i32, i32* %plane, align 4, !tbaa !9
  call void @sync_read(%struct.AV1LfSyncData* %54, i32 %add21, i32 %56, i32 %57)
  %58 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %planes.addr, align 4, !tbaa !2
  %59 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params22 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %59, i32 0, i32 37
  %sb_size23 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params22, i32 0, i32 7
  %60 = load i8, i8* %sb_size23, align 4, !tbaa !83
  %61 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  %62 = load i32, i32* %mi_row, align 4, !tbaa !9
  %63 = load i32, i32* %mi_col, align 4, !tbaa !9
  %64 = load i32, i32* %plane, align 4, !tbaa !9
  %65 = load i32, i32* %plane, align 4, !tbaa !9
  %add24 = add nsw i32 %65, 1
  call void @av1_setup_dst_planes(%struct.macroblockd_plane* %58, i8 zeroext %60, %struct.yv12_buffer_config* %61, i32 %62, i32 %63, i32 %64, i32 %add24)
  %66 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %67 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %68 = load i32, i32* %plane, align 4, !tbaa !9
  %69 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %planes.addr, align 4, !tbaa !2
  %70 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx25 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %69, i32 %70
  %71 = load i32, i32* %mi_row, align 4, !tbaa !9
  %72 = load i32, i32* %mi_col, align 4, !tbaa !9
  call void @av1_filter_block_plane_horz(%struct.AV1Common* %66, %struct.macroblockd* %67, i32 %68, %struct.macroblockd_plane* %arrayidx25, i32 %71, i32 %72)
  br label %for.inc26

for.inc26:                                        ; preds = %for.body19
  %73 = load i32, i32* %mi_col, align 4, !tbaa !9
  %add27 = add nsw i32 %73, 32
  store i32 %add27, i32* %mi_col, align 4, !tbaa !9
  br label %for.cond15

for.end28:                                        ; preds = %for.cond15
  br label %if.end

if.end:                                           ; preds = %for.end28, %if.else
  br label %if.end29

if.end29:                                         ; preds = %if.end, %for.end
  br label %if.end31

if.else30:                                        ; preds = %while.body
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %if.end29
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end31, %if.else30
  %74 = bitcast %struct.AV1LfMTInfo** %cur_job_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup
  %75 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #5
  %76 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #5
  %77 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #5
  %78 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  %79 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #5
  %80 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #5
  %81 = bitcast i32* %sb_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #5
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal %struct.AV1LfMTInfo* @get_lf_job_info(%struct.AV1LfSyncData* %lf_sync) #0 {
entry:
  %lf_sync.addr = alloca %struct.AV1LfSyncData*, align 4
  %cur_job_info = alloca %struct.AV1LfMTInfo*, align 4
  store %struct.AV1LfSyncData* %lf_sync, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AV1LfMTInfo** %cur_job_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store %struct.AV1LfMTInfo* null, %struct.AV1LfMTInfo** %cur_job_info, align 4, !tbaa !2
  %1 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %2 = load %struct.AV1LfMTInfo*, %struct.AV1LfMTInfo** %cur_job_info, align 4, !tbaa !2
  %3 = bitcast %struct.AV1LfMTInfo** %cur_job_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  ret %struct.AV1LfMTInfo* %2
}

declare void @av1_setup_dst_planes(%struct.macroblockd_plane*, i8 zeroext, %struct.yv12_buffer_config*, i32, i32, i32, i32) #2

declare void @av1_filter_block_plane_vert(%struct.AV1Common*, %struct.macroblockd*, i32, %struct.macroblockd_plane*, i32, i32) #2

; Function Attrs: inlinehint nounwind
define internal void @sync_write(%struct.AV1LfSyncData* %lf_sync, i32 %r, i32 %c, i32 %sb_cols, i32 %plane) #4 {
entry:
  %lf_sync.addr = alloca %struct.AV1LfSyncData*, align 4
  %r.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  %sb_cols.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  store %struct.AV1LfSyncData* %lf_sync, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  store i32 %r, i32* %r.addr, align 4, !tbaa !9
  store i32 %c, i32* %c.addr, align 4, !tbaa !9
  store i32 %sb_cols, i32* %sb_cols.addr, align 4, !tbaa !9
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !9
  %0 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %1 = load i32, i32* %r.addr, align 4, !tbaa !9
  %2 = load i32, i32* %c.addr, align 4, !tbaa !9
  %3 = load i32, i32* %sb_cols.addr, align 4, !tbaa !9
  %4 = load i32, i32* %plane.addr, align 4, !tbaa !9
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @sync_read(%struct.AV1LfSyncData* %lf_sync, i32 %r, i32 %c, i32 %plane) #4 {
entry:
  %lf_sync.addr = alloca %struct.AV1LfSyncData*, align 4
  %r.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  store %struct.AV1LfSyncData* %lf_sync, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  store i32 %r, i32* %r.addr, align 4, !tbaa !9
  store i32 %c, i32* %c.addr, align 4, !tbaa !9
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !9
  %0 = load %struct.AV1LfSyncData*, %struct.AV1LfSyncData** %lf_sync.addr, align 4, !tbaa !2
  %1 = load i32, i32* %r.addr, align 4, !tbaa !9
  %2 = load i32, i32* %c.addr, align 4, !tbaa !9
  %3 = load i32, i32* %plane.addr, align 4, !tbaa !9
  ret void
}

declare void @av1_filter_block_plane_horz(%struct.AV1Common*, %struct.macroblockd*, i32, %struct.macroblockd_plane*, i32, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare i32 @av1_lr_count_units_in_tile(i32, i32) #2

; Function Attrs: nounwind
define internal void @loop_restoration_alloc(%struct.AV1LrSyncData* %lr_sync, %struct.AV1Common* %cm, i32 %num_workers, i32 %num_rows_lr, i32 %num_planes, i32 %width) #0 {
entry:
  %lr_sync.addr = alloca %struct.AV1LrSyncData*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %num_workers.addr = alloca i32, align 4
  %num_rows_lr.addr = alloca i32, align 4
  %num_planes.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %worker_idx = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.AV1LrSyncData* %lr_sync, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %num_workers, i32* %num_workers.addr, align 4, !tbaa !9
  store i32 %num_rows_lr, i32* %num_rows_lr.addr, align 4, !tbaa !9
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !9
  store i32 %width, i32* %width.addr, align 4, !tbaa !9
  %0 = load i32, i32* %num_rows_lr.addr, align 4, !tbaa !9
  %1 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %1, i32 0, i32 2
  store i32 %0, i32* %rows, align 4, !tbaa !62
  %2 = load i32, i32* %num_planes.addr, align 4, !tbaa !9
  %3 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %num_planes1 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %3, i32 0, i32 3
  store i32 %2, i32* %num_planes1, align 4, !tbaa !64
  br label %do.body

do.body:                                          ; preds = %entry
  %4 = load i32, i32* %num_workers.addr, align 4, !tbaa !9
  %mul = mul i32 %4, 12
  %call = call i8* @aom_malloc(i32 %mul)
  %5 = bitcast i8* %call to %struct.LoopRestorationWorkerData*
  %6 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %6, i32 0, i32 5
  store %struct.LoopRestorationWorkerData* %5, %struct.LoopRestorationWorkerData** %lrworkerdata, align 4, !tbaa !49
  %7 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata2 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %7, i32 0, i32 5
  %8 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata2, align 4, !tbaa !49
  %tobool = icmp ne %struct.LoopRestorationWorkerData* %8, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 2, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.3, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %do.body
  br label %do.end

do.end:                                           ; preds = %if.end
  %10 = bitcast i32* %worker_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  store i32 0, i32* %worker_idx, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end
  %11 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %12 = load i32, i32* %num_workers.addr, align 4, !tbaa !9
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %worker_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %15 = load i32, i32* %num_workers.addr, align 4, !tbaa !9
  %sub = sub nsw i32 %15, 1
  %cmp3 = icmp slt i32 %14, %sub
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %for.body
  br label %do.body5

do.body5:                                         ; preds = %if.then4
  %call6 = call i8* @aom_memalign(i32 16, i32 1292704)
  %16 = bitcast i8* %call6 to i32*
  %17 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata7 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %17, i32 0, i32 5
  %18 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata7, align 4, !tbaa !49
  %19 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %18, i32 %19
  %rst_tmpbuf = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %arrayidx, i32 0, i32 0
  store i32* %16, i32** %rst_tmpbuf, align 4, !tbaa !50
  %20 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata8 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %20, i32 0, i32 5
  %21 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata8, align 4, !tbaa !49
  %22 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %arrayidx9 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %21, i32 %22
  %rst_tmpbuf10 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %arrayidx9, i32 0, i32 0
  %23 = load i32*, i32** %rst_tmpbuf10, align 4, !tbaa !50
  %tobool11 = icmp ne i32* %23, null
  br i1 %tobool11, label %if.end14, label %if.then12

if.then12:                                        ; preds = %do.body5
  %24 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error13 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %24, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error13, i32 2, i8* getelementptr inbounds ([64 x i8], [64 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %do.body5
  br label %do.cond

do.cond:                                          ; preds = %if.end14
  br label %do.end15

do.end15:                                         ; preds = %do.cond
  br label %do.body16

do.body16:                                        ; preds = %do.end15
  %call17 = call i8* @aom_malloc(i32 4704)
  %25 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata18 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %25, i32 0, i32 5
  %26 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata18, align 4, !tbaa !49
  %27 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %arrayidx19 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %26, i32 %27
  %rlbs = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %arrayidx19, i32 0, i32 1
  store i8* %call17, i8** %rlbs, align 4, !tbaa !52
  %28 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata20 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %28, i32 0, i32 5
  %29 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata20, align 4, !tbaa !49
  %30 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %arrayidx21 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %29, i32 %30
  %rlbs22 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %arrayidx21, i32 0, i32 1
  %31 = load i8*, i8** %rlbs22, align 4, !tbaa !52
  %tobool23 = icmp ne i8* %31, null
  br i1 %tobool23, label %if.end26, label %if.then24

if.then24:                                        ; preds = %do.body16
  %32 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error25 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %32, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error25, i32 2, i8* getelementptr inbounds ([58 x i8], [58 x i8]* @.str.5, i32 0, i32 0))
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %do.body16
  br label %do.cond27

do.cond27:                                        ; preds = %if.end26
  br label %do.end28

do.end28:                                         ; preds = %do.cond27
  br label %if.end37

if.else:                                          ; preds = %for.body
  %33 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_tmpbuf29 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %33, i32 0, i32 30
  %34 = load i32*, i32** %rst_tmpbuf29, align 8, !tbaa !84
  %35 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata30 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %35, i32 0, i32 5
  %36 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata30, align 4, !tbaa !49
  %37 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %arrayidx31 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %36, i32 %37
  %rst_tmpbuf32 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %arrayidx31, i32 0, i32 0
  store i32* %34, i32** %rst_tmpbuf32, align 4, !tbaa !50
  %38 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rlbs33 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %38, i32 0, i32 31
  %39 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs33, align 4, !tbaa !85
  %40 = bitcast %struct.RestorationLineBuffers* %39 to i8*
  %41 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %lrworkerdata34 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %41, i32 0, i32 5
  %42 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata34, align 4, !tbaa !49
  %43 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %arrayidx35 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %42, i32 %43
  %rlbs36 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %arrayidx35, i32 0, i32 1
  store i8* %40, i8** %rlbs36, align 4, !tbaa !52
  br label %if.end37

if.end37:                                         ; preds = %if.else, %do.end28
  br label %for.inc

for.inc:                                          ; preds = %if.end37
  %44 = load i32, i32* %worker_idx, align 4, !tbaa !9
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %worker_idx, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %45 = load i32, i32* %num_workers.addr, align 4, !tbaa !9
  %46 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %num_workers38 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %46, i32 0, i32 4
  store i32 %45, i32* %num_workers38, align 4, !tbaa !63
  %47 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  store i32 0, i32* %j, align 4, !tbaa !9
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc55, %for.end
  %48 = load i32, i32* %j, align 4, !tbaa !9
  %49 = load i32, i32* %num_planes.addr, align 4, !tbaa !9
  %cmp40 = icmp slt i32 %48, %49
  br i1 %cmp40, label %for.body42, label %for.cond.cleanup41

for.cond.cleanup41:                               ; preds = %for.cond39
  %50 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  br label %for.end57

for.body42:                                       ; preds = %for.cond39
  br label %do.body43

do.body43:                                        ; preds = %for.body42
  %51 = load i32, i32* %num_rows_lr.addr, align 4, !tbaa !9
  %mul44 = mul i32 4, %51
  %call45 = call i8* @aom_malloc(i32 %mul44)
  %52 = bitcast i8* %call45 to i32*
  %53 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %cur_sb_col = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %53, i32 0, i32 0
  %54 = load i32, i32* %j, align 4, !tbaa !9
  %arrayidx46 = getelementptr inbounds [3 x i32*], [3 x i32*]* %cur_sb_col, i32 0, i32 %54
  store i32* %52, i32** %arrayidx46, align 4, !tbaa !2
  %55 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %cur_sb_col47 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %55, i32 0, i32 0
  %56 = load i32, i32* %j, align 4, !tbaa !9
  %arrayidx48 = getelementptr inbounds [3 x i32*], [3 x i32*]* %cur_sb_col47, i32 0, i32 %56
  %57 = load i32*, i32** %arrayidx48, align 4, !tbaa !2
  %tobool49 = icmp ne i32* %57, null
  br i1 %tobool49, label %if.end52, label %if.then50

if.then50:                                        ; preds = %do.body43
  %58 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error51 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %58, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error51, i32 2, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.6, i32 0, i32 0))
  br label %if.end52

if.end52:                                         ; preds = %if.then50, %do.body43
  br label %do.cond53

do.cond53:                                        ; preds = %if.end52
  br label %do.end54

do.end54:                                         ; preds = %do.cond53
  br label %for.inc55

for.inc55:                                        ; preds = %do.end54
  %59 = load i32, i32* %j, align 4, !tbaa !9
  %inc56 = add nsw i32 %59, 1
  store i32 %inc56, i32* %j, align 4, !tbaa !9
  br label %for.cond39

for.end57:                                        ; preds = %for.cond.cleanup41
  br label %do.body58

do.body58:                                        ; preds = %for.end57
  %60 = load i32, i32* %num_rows_lr.addr, align 4, !tbaa !9
  %mul59 = mul i32 28, %60
  %61 = load i32, i32* %num_planes.addr, align 4, !tbaa !9
  %mul60 = mul i32 %mul59, %61
  %call61 = call i8* @aom_malloc(i32 %mul60)
  %62 = bitcast i8* %call61 to %struct.AV1LrMTInfo*
  %63 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %job_queue = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %63, i32 0, i32 6
  store %struct.AV1LrMTInfo* %62, %struct.AV1LrMTInfo** %job_queue, align 4, !tbaa !47
  %64 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %job_queue62 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %64, i32 0, i32 6
  %65 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %job_queue62, align 4, !tbaa !47
  %tobool63 = icmp ne %struct.AV1LrMTInfo* %65, null
  br i1 %tobool63, label %if.end66, label %if.then64

if.then64:                                        ; preds = %do.body58
  %66 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error65 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %66, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error65, i32 2, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.7, i32 0, i32 0))
  br label %if.end66

if.end66:                                         ; preds = %if.then64, %do.body58
  br label %do.end68

do.end68:                                         ; preds = %if.end66
  %67 = load i32, i32* %width.addr, align 4, !tbaa !9
  %call69 = call i32 @get_lr_sync_range(i32 %67)
  %68 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %sync_range = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %68, i32 0, i32 1
  store i32 %call69, i32* %sync_range, align 4, !tbaa !61
  ret void
}

; Function Attrs: nounwind
define internal void @enqueue_lr_jobs(%struct.AV1LrSyncData* %lr_sync, %struct.AV1LrStruct* %lr_ctxt, %struct.AV1Common* %cm) #0 {
entry:
  %lr_sync.addr = alloca %struct.AV1LrSyncData*, align 4
  %lr_ctxt.addr = alloca %struct.AV1LrStruct*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ctxt = alloca %struct.FilterFrameCtxt*, align 4
  %num_planes = alloca i32, align 4
  %lr_job_queue = alloca %struct.AV1LrMTInfo*, align 4
  %lr_job_counter = alloca [2 x i32], align 4
  %num_even_lr_jobs = alloca i32, align 4
  %plane = alloca i32, align 4
  %plane8 = alloca i32, align 4
  %is_uv = alloca i32, align 4
  %ss_y = alloca i32, align 4
  %tile_rect = alloca %struct.AV1PixelRect, align 4
  %unit_size = alloca i32, align 4
  %tile_h = alloca i32, align 4
  %ext_size = alloca i32, align 4
  %y0 = alloca i32, align 4
  %i = alloca i32, align 4
  %remaining_h = alloca i32, align 4
  %h = alloca i32, align 4
  %limits = alloca %struct.RestorationTileLimits, align 4
  %voffset = alloca i32, align 4
  store %struct.AV1LrSyncData* %lr_sync, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  store %struct.AV1LrStruct* %lr_ctxt, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt.addr, align 4, !tbaa !2
  %ctxt1 = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [3 x %struct.FilterFrameCtxt], [3 x %struct.FilterFrameCtxt]* %ctxt1, i32 0, i32 0
  store %struct.FilterFrameCtxt* %arraydecay, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %2 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %3)
  store i32 %call, i32* %num_planes, align 4, !tbaa !9
  %4 = bitcast %struct.AV1LrMTInfo** %lr_job_queue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %job_queue = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %5, i32 0, i32 6
  %6 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %job_queue, align 4, !tbaa !47
  store %struct.AV1LrMTInfo* %6, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %7 = bitcast [2 x i32]* %lr_job_counter to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #5
  %8 = bitcast i32* %num_even_lr_jobs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  store i32 0, i32* %num_even_lr_jobs, align 4, !tbaa !9
  %9 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %jobs_enqueued = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %9, i32 0, i32 7
  store i32 0, i32* %jobs_enqueued, align 4, !tbaa !86
  %10 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %jobs_dequeued = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %10, i32 0, i32 8
  store i32 0, i32* %jobs_dequeued, align 4, !tbaa !87
  %11 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  store i32 0, i32* %plane, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %12 = load i32, i32* %plane, align 4, !tbaa !9
  %13 = load i32, i32* %num_planes, align 4, !tbaa !9
  %cmp = icmp slt i32 %12, %13
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %14 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 29
  %16 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %16
  %frame_restoration_type = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx, i32 0, i32 0
  %17 = load i8, i8* %frame_restoration_type, align 4, !tbaa !54
  %conv = zext i8 %17 to i32
  %cmp2 = icmp eq i32 %conv, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %18 = load i32, i32* %num_even_lr_jobs, align 4, !tbaa !9
  %19 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %20 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx4 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %19, i32 %20
  %rsi = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx4, i32 0, i32 0
  %21 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !88
  %vert_units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %21, i32 0, i32 3
  %22 = load i32, i32* %vert_units_per_tile, align 4, !tbaa !90
  %add = add nsw i32 %22, 1
  %shr = ashr i32 %add, 1
  %add5 = add nsw i32 %18, %shr
  store i32 %add5, i32* %num_even_lr_jobs, align 4, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %if.end, %if.then
  %23 = load i32, i32* %plane, align 4, !tbaa !9
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %arrayidx6 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 0
  store i32 0, i32* %arrayidx6, align 4, !tbaa !9
  %24 = load i32, i32* %num_even_lr_jobs, align 4, !tbaa !9
  %arrayidx7 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 1
  store i32 %24, i32* %arrayidx7, align 4, !tbaa !9
  %25 = bitcast i32* %plane8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  store i32 0, i32* %plane8, align 4, !tbaa !9
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc157, %for.end
  %26 = load i32, i32* %plane8, align 4, !tbaa !9
  %27 = load i32, i32* %num_planes, align 4, !tbaa !9
  %cmp10 = icmp slt i32 %26, %27
  br i1 %cmp10, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond9
  %28 = bitcast i32* %plane8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  br label %for.end159

for.body13:                                       ; preds = %for.cond9
  %29 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %rst_info14 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %29, i32 0, i32 29
  %30 = load i32, i32* %plane8, align 4, !tbaa !9
  %arrayidx15 = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info14, i32 0, i32 %30
  %frame_restoration_type16 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx15, i32 0, i32 0
  %31 = load i8, i8* %frame_restoration_type16, align 4, !tbaa !54
  %conv17 = zext i8 %31 to i32
  %cmp18 = icmp eq i32 %conv17, 0
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %for.body13
  br label %for.inc157

if.end21:                                         ; preds = %for.body13
  %32 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  %33 = load i32, i32* %plane8, align 4, !tbaa !9
  %cmp22 = icmp sgt i32 %33, 0
  %conv23 = zext i1 %cmp22 to i32
  store i32 %conv23, i32* %is_uv, align 4, !tbaa !9
  %34 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #5
  %35 = load i32, i32* %is_uv, align 4, !tbaa !9
  %tobool = icmp ne i32 %35, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end21
  %36 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %36, i32 0, i32 37
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 33
  %37 = load i32, i32* %subsampling_y, align 4, !tbaa !91
  %tobool24 = icmp ne i32 %37, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end21
  %38 = phi i1 [ false, %if.end21 ], [ %tobool24, %land.rhs ]
  %land.ext = zext i1 %38 to i32
  store i32 %land.ext, i32* %ss_y, align 4, !tbaa !9
  %39 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %39) #5
  %40 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %41 = load i32, i32* %plane8, align 4, !tbaa !9
  %arrayidx25 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %40, i32 %41
  %tile_rect26 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx25, i32 0, i32 10
  %42 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  %43 = bitcast %struct.AV1PixelRect* %tile_rect26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 16, i1 false), !tbaa.struct !57
  %44 = bitcast i32* %unit_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  %45 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %46 = load i32, i32* %plane8, align 4, !tbaa !9
  %arrayidx27 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %45, i32 %46
  %rsi28 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx27, i32 0, i32 0
  %47 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi28, align 4, !tbaa !88
  %restoration_unit_size = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %47, i32 0, i32 1
  %48 = load i32, i32* %restoration_unit_size, align 4, !tbaa !60
  store i32 %48, i32* %unit_size, align 4, !tbaa !9
  %49 = bitcast i32* %tile_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #5
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %50 = load i32, i32* %bottom, align 4, !tbaa !58
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %51 = load i32, i32* %top, align 4, !tbaa !59
  %sub = sub nsw i32 %50, %51
  store i32 %sub, i32* %tile_h, align 4, !tbaa !9
  %52 = bitcast i32* %ext_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #5
  %53 = load i32, i32* %unit_size, align 4, !tbaa !9
  %mul = mul nsw i32 %53, 3
  %div = sdiv i32 %mul, 2
  store i32 %div, i32* %ext_size, align 4, !tbaa !9
  %54 = bitcast i32* %y0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #5
  store i32 0, i32* %y0, align 4, !tbaa !9
  %55 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #5
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %while.cond

while.cond:                                       ; preds = %if.end149, %land.end
  %56 = load i32, i32* %y0, align 4, !tbaa !9
  %57 = load i32, i32* %tile_h, align 4, !tbaa !9
  %cmp29 = icmp slt i32 %56, %57
  br i1 %cmp29, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %58 = bitcast i32* %remaining_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #5
  %59 = load i32, i32* %tile_h, align 4, !tbaa !9
  %60 = load i32, i32* %y0, align 4, !tbaa !9
  %sub31 = sub nsw i32 %59, %60
  store i32 %sub31, i32* %remaining_h, align 4, !tbaa !9
  %61 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #5
  %62 = load i32, i32* %remaining_h, align 4, !tbaa !9
  %63 = load i32, i32* %ext_size, align 4, !tbaa !9
  %cmp32 = icmp slt i32 %62, %63
  br i1 %cmp32, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %64 = load i32, i32* %remaining_h, align 4, !tbaa !9
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %65 = load i32, i32* %unit_size, align 4, !tbaa !9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %64, %cond.true ], [ %65, %cond.false ]
  store i32 %cond, i32* %h, align 4, !tbaa !9
  %66 = bitcast %struct.RestorationTileLimits* %limits to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %66) #5
  %top34 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %67 = load i32, i32* %top34, align 4, !tbaa !59
  %68 = load i32, i32* %y0, align 4, !tbaa !9
  %add35 = add nsw i32 %67, %68
  %v_start = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  store i32 %add35, i32* %v_start, align 4, !tbaa !92
  %top36 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %69 = load i32, i32* %top36, align 4, !tbaa !59
  %70 = load i32, i32* %y0, align 4, !tbaa !9
  %add37 = add nsw i32 %69, %70
  %71 = load i32, i32* %h, align 4, !tbaa !9
  %add38 = add nsw i32 %add37, %71
  %v_end = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  store i32 %add38, i32* %v_end, align 4, !tbaa !58
  %72 = bitcast i32* %voffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #5
  %73 = load i32, i32* %ss_y, align 4, !tbaa !9
  %shr39 = ashr i32 8, %73
  store i32 %shr39, i32* %voffset, align 4, !tbaa !9
  %top40 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %74 = load i32, i32* %top40, align 4, !tbaa !59
  %v_start41 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  %75 = load i32, i32* %v_start41, align 4, !tbaa !92
  %76 = load i32, i32* %voffset, align 4, !tbaa !9
  %sub42 = sub nsw i32 %75, %76
  %cmp43 = icmp sgt i32 %74, %sub42
  br i1 %cmp43, label %cond.true45, label %cond.false47

cond.true45:                                      ; preds = %cond.end
  %top46 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %77 = load i32, i32* %top46, align 4, !tbaa !59
  br label %cond.end50

cond.false47:                                     ; preds = %cond.end
  %v_start48 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  %78 = load i32, i32* %v_start48, align 4, !tbaa !92
  %79 = load i32, i32* %voffset, align 4, !tbaa !9
  %sub49 = sub nsw i32 %78, %79
  br label %cond.end50

cond.end50:                                       ; preds = %cond.false47, %cond.true45
  %cond51 = phi i32 [ %77, %cond.true45 ], [ %sub49, %cond.false47 ]
  %v_start52 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  store i32 %cond51, i32* %v_start52, align 4, !tbaa !92
  %v_end53 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  %80 = load i32, i32* %v_end53, align 4, !tbaa !58
  %bottom54 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %81 = load i32, i32* %bottom54, align 4, !tbaa !58
  %cmp55 = icmp slt i32 %80, %81
  br i1 %cmp55, label %if.then57, label %if.end60

if.then57:                                        ; preds = %cond.end50
  %82 = load i32, i32* %voffset, align 4, !tbaa !9
  %v_end58 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  %83 = load i32, i32* %v_end58, align 4, !tbaa !58
  %sub59 = sub nsw i32 %83, %82
  store i32 %sub59, i32* %v_end58, align 4, !tbaa !58
  br label %if.end60

if.end60:                                         ; preds = %if.then57, %cond.end50
  %84 = load i32, i32* %i, align 4, !tbaa !9
  %85 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %86 = load i32, i32* %i, align 4, !tbaa !9
  %and = and i32 %86, 1
  %arrayidx61 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and
  %87 = load i32, i32* %arrayidx61, align 4, !tbaa !9
  %arrayidx62 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %85, i32 %87
  %lr_unit_row = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx62, i32 0, i32 2
  store i32 %84, i32* %lr_unit_row, align 4, !tbaa !93
  %88 = load i32, i32* %plane8, align 4, !tbaa !9
  %89 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %90 = load i32, i32* %i, align 4, !tbaa !9
  %and63 = and i32 %90, 1
  %arrayidx64 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and63
  %91 = load i32, i32* %arrayidx64, align 4, !tbaa !9
  %arrayidx65 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %89, i32 %91
  %plane66 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx65, i32 0, i32 3
  store i32 %88, i32* %plane66, align 4, !tbaa !95
  %v_start67 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  %92 = load i32, i32* %v_start67, align 4, !tbaa !92
  %93 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %94 = load i32, i32* %i, align 4, !tbaa !9
  %and68 = and i32 %94, 1
  %arrayidx69 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and68
  %95 = load i32, i32* %arrayidx69, align 4, !tbaa !9
  %arrayidx70 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %93, i32 %95
  %v_start71 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx70, i32 0, i32 0
  store i32 %92, i32* %v_start71, align 4, !tbaa !96
  %v_end72 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  %96 = load i32, i32* %v_end72, align 4, !tbaa !58
  %97 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %98 = load i32, i32* %i, align 4, !tbaa !9
  %and73 = and i32 %98, 1
  %arrayidx74 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and73
  %99 = load i32, i32* %arrayidx74, align 4, !tbaa !9
  %arrayidx75 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %97, i32 %99
  %v_end76 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx75, i32 0, i32 1
  store i32 %96, i32* %v_end76, align 4, !tbaa !97
  %100 = load i32, i32* %i, align 4, !tbaa !9
  %and77 = and i32 %100, 1
  %101 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %102 = load i32, i32* %i, align 4, !tbaa !9
  %and78 = and i32 %102, 1
  %arrayidx79 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and78
  %103 = load i32, i32* %arrayidx79, align 4, !tbaa !9
  %arrayidx80 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %101, i32 %103
  %sync_mode = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx80, i32 0, i32 4
  store i32 %and77, i32* %sync_mode, align 4, !tbaa !98
  %104 = load i32, i32* %i, align 4, !tbaa !9
  %and81 = and i32 %104, 1
  %cmp82 = icmp eq i32 %and81, 0
  br i1 %cmp82, label %if.then84, label %if.else

if.then84:                                        ; preds = %if.end60
  %v_start85 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  %105 = load i32, i32* %v_start85, align 4, !tbaa !92
  %add86 = add nsw i32 %105, 3
  %106 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %107 = load i32, i32* %i, align 4, !tbaa !9
  %and87 = and i32 %107, 1
  %arrayidx88 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and87
  %108 = load i32, i32* %arrayidx88, align 4, !tbaa !9
  %arrayidx89 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %106, i32 %108
  %v_copy_start = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx89, i32 0, i32 5
  store i32 %add86, i32* %v_copy_start, align 4, !tbaa !99
  %v_end90 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  %109 = load i32, i32* %v_end90, align 4, !tbaa !58
  %sub91 = sub nsw i32 %109, 3
  %110 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %111 = load i32, i32* %i, align 4, !tbaa !9
  %and92 = and i32 %111, 1
  %arrayidx93 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and92
  %112 = load i32, i32* %arrayidx93, align 4, !tbaa !9
  %arrayidx94 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %110, i32 %112
  %v_copy_end = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx94, i32 0, i32 6
  store i32 %sub91, i32* %v_copy_end, align 4, !tbaa !100
  %113 = load i32, i32* %i, align 4, !tbaa !9
  %cmp95 = icmp eq i32 %113, 0
  br i1 %cmp95, label %if.then97, label %if.end103

if.then97:                                        ; preds = %if.then84
  %top98 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %114 = load i32, i32* %top98, align 4, !tbaa !59
  %115 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %116 = load i32, i32* %i, align 4, !tbaa !9
  %and99 = and i32 %116, 1
  %arrayidx100 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and99
  %117 = load i32, i32* %arrayidx100, align 4, !tbaa !9
  %arrayidx101 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %115, i32 %117
  %v_copy_start102 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx101, i32 0, i32 5
  store i32 %114, i32* %v_copy_start102, align 4, !tbaa !99
  br label %if.end103

if.end103:                                        ; preds = %if.then97, %if.then84
  %118 = load i32, i32* %i, align 4, !tbaa !9
  %119 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %120 = load i32, i32* %plane8, align 4, !tbaa !9
  %arrayidx104 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %119, i32 %120
  %rsi105 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx104, i32 0, i32 0
  %121 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi105, align 4, !tbaa !88
  %vert_units_per_tile106 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %121, i32 0, i32 3
  %122 = load i32, i32* %vert_units_per_tile106, align 4, !tbaa !90
  %sub107 = sub nsw i32 %122, 1
  %cmp108 = icmp eq i32 %118, %sub107
  br i1 %cmp108, label %if.then110, label %if.end116

if.then110:                                       ; preds = %if.end103
  %bottom111 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %123 = load i32, i32* %bottom111, align 4, !tbaa !58
  %124 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %125 = load i32, i32* %i, align 4, !tbaa !9
  %and112 = and i32 %125, 1
  %arrayidx113 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and112
  %126 = load i32, i32* %arrayidx113, align 4, !tbaa !9
  %arrayidx114 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %124, i32 %126
  %v_copy_end115 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx114, i32 0, i32 6
  store i32 %123, i32* %v_copy_end115, align 4, !tbaa !100
  br label %if.end116

if.end116:                                        ; preds = %if.then110, %if.end103
  br label %if.end149

if.else:                                          ; preds = %if.end60
  %v_start117 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  %127 = load i32, i32* %v_start117, align 4, !tbaa !92
  %sub118 = sub nsw i32 %127, 3
  %top119 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %128 = load i32, i32* %top119, align 4, !tbaa !59
  %cmp120 = icmp sgt i32 %sub118, %128
  br i1 %cmp120, label %cond.true122, label %cond.false125

cond.true122:                                     ; preds = %if.else
  %v_start123 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  %129 = load i32, i32* %v_start123, align 4, !tbaa !92
  %sub124 = sub nsw i32 %129, 3
  br label %cond.end127

cond.false125:                                    ; preds = %if.else
  %top126 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 1
  %130 = load i32, i32* %top126, align 4, !tbaa !59
  br label %cond.end127

cond.end127:                                      ; preds = %cond.false125, %cond.true122
  %cond128 = phi i32 [ %sub124, %cond.true122 ], [ %130, %cond.false125 ]
  %131 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %132 = load i32, i32* %i, align 4, !tbaa !9
  %and129 = and i32 %132, 1
  %arrayidx130 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and129
  %133 = load i32, i32* %arrayidx130, align 4, !tbaa !9
  %arrayidx131 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %131, i32 %133
  %v_copy_start132 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx131, i32 0, i32 5
  store i32 %cond128, i32* %v_copy_start132, align 4, !tbaa !99
  %v_end133 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  %134 = load i32, i32* %v_end133, align 4, !tbaa !58
  %add134 = add nsw i32 %134, 3
  %bottom135 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %135 = load i32, i32* %bottom135, align 4, !tbaa !58
  %cmp136 = icmp slt i32 %add134, %135
  br i1 %cmp136, label %cond.true138, label %cond.false141

cond.true138:                                     ; preds = %cond.end127
  %v_end139 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  %136 = load i32, i32* %v_end139, align 4, !tbaa !58
  %add140 = add nsw i32 %136, 3
  br label %cond.end143

cond.false141:                                    ; preds = %cond.end127
  %bottom142 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect, i32 0, i32 3
  %137 = load i32, i32* %bottom142, align 4, !tbaa !58
  br label %cond.end143

cond.end143:                                      ; preds = %cond.false141, %cond.true138
  %cond144 = phi i32 [ %add140, %cond.true138 ], [ %137, %cond.false141 ]
  %138 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %lr_job_queue, align 4, !tbaa !2
  %139 = load i32, i32* %i, align 4, !tbaa !9
  %and145 = and i32 %139, 1
  %arrayidx146 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and145
  %140 = load i32, i32* %arrayidx146, align 4, !tbaa !9
  %arrayidx147 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %138, i32 %140
  %v_copy_end148 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %arrayidx147, i32 0, i32 6
  store i32 %cond144, i32* %v_copy_end148, align 4, !tbaa !100
  br label %if.end149

if.end149:                                        ; preds = %cond.end143, %if.end116
  %141 = load i32, i32* %i, align 4, !tbaa !9
  %and150 = and i32 %141, 1
  %arrayidx151 = getelementptr inbounds [2 x i32], [2 x i32]* %lr_job_counter, i32 0, i32 %and150
  %142 = load i32, i32* %arrayidx151, align 4, !tbaa !9
  %inc152 = add nsw i32 %142, 1
  store i32 %inc152, i32* %arrayidx151, align 4, !tbaa !9
  %143 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %jobs_enqueued153 = getelementptr inbounds %struct.AV1LrSyncData, %struct.AV1LrSyncData* %143, i32 0, i32 7
  %144 = load i32, i32* %jobs_enqueued153, align 4, !tbaa !86
  %inc154 = add nsw i32 %144, 1
  store i32 %inc154, i32* %jobs_enqueued153, align 4, !tbaa !86
  %145 = load i32, i32* %h, align 4, !tbaa !9
  %146 = load i32, i32* %y0, align 4, !tbaa !9
  %add155 = add nsw i32 %146, %145
  store i32 %add155, i32* %y0, align 4, !tbaa !9
  %147 = load i32, i32* %i, align 4, !tbaa !9
  %inc156 = add nsw i32 %147, 1
  store i32 %inc156, i32* %i, align 4, !tbaa !9
  %148 = bitcast i32* %voffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #5
  %149 = bitcast %struct.RestorationTileLimits* %limits to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %149) #5
  %150 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #5
  %151 = bitcast i32* %remaining_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #5
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %152 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #5
  %153 = bitcast i32* %y0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #5
  %154 = bitcast i32* %ext_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #5
  %155 = bitcast i32* %tile_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #5
  %156 = bitcast i32* %unit_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #5
  %157 = bitcast %struct.AV1PixelRect* %tile_rect to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %157) #5
  %158 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #5
  %159 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #5
  br label %for.inc157

for.inc157:                                       ; preds = %while.end, %if.then20
  %160 = load i32, i32* %plane8, align 4, !tbaa !9
  %inc158 = add nsw i32 %160, 1
  store i32 %inc158, i32* %plane8, align 4, !tbaa !9
  br label %for.cond9

for.end159:                                       ; preds = %for.cond.cleanup12
  %161 = bitcast i32* %num_even_lr_jobs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #5
  %162 = bitcast [2 x i32]* %lr_job_counter to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %162) #5
  %163 = bitcast %struct.AV1LrMTInfo** %lr_job_queue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #5
  %164 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #5
  %165 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #5
  ret void
}

; Function Attrs: nounwind
define internal i32 @loop_restoration_row_worker(i8* %arg1, i8* %arg2) #0 {
entry:
  %arg1.addr = alloca i8*, align 4
  %arg2.addr = alloca i8*, align 4
  %lr_sync = alloca %struct.AV1LrSyncData*, align 4
  %lrworkerdata = alloca %struct.LoopRestorationWorkerData*, align 4
  %lr_ctxt = alloca %struct.AV1LrStruct*, align 4
  %ctxt = alloca %struct.FilterFrameCtxt*, align 4
  %lr_unit_row = alloca i32, align 4
  %plane = alloca i32, align 4
  %tile_row = alloca i32, align 4
  %tile_col = alloca i32, align 4
  %tile_cols = alloca i32, align 4
  %tile_idx = alloca i32, align 4
  %cur_job_info = alloca %struct.AV1LrMTInfo*, align 4
  %limits = alloca %struct.RestorationTileLimits, align 4
  %on_sync_read = alloca void (i8*, i32, i32, i32)*, align 4
  %on_sync_write = alloca void (i8*, i32, i32, i32, i32)*, align 4
  %unit_idx0 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %arg1, i8** %arg1.addr, align 4, !tbaa !2
  store i8* %arg2, i8** %arg2.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AV1LrSyncData** %lr_sync to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %arg1.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.AV1LrSyncData*
  store %struct.AV1LrSyncData* %2, %struct.AV1LrSyncData** %lr_sync, align 4, !tbaa !2
  %3 = bitcast %struct.LoopRestorationWorkerData** %lrworkerdata to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i8*, i8** %arg2.addr, align 4, !tbaa !2
  %5 = bitcast i8* %4 to %struct.LoopRestorationWorkerData*
  store %struct.LoopRestorationWorkerData* %5, %struct.LoopRestorationWorkerData** %lrworkerdata, align 4, !tbaa !2
  %6 = bitcast %struct.AV1LrStruct** %lr_ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata, align 4, !tbaa !2
  %lr_ctxt1 = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %7, i32 0, i32 2
  %8 = load i8*, i8** %lr_ctxt1, align 4, !tbaa !65
  %9 = bitcast i8* %8 to %struct.AV1LrStruct*
  store %struct.AV1LrStruct* %9, %struct.AV1LrStruct** %lr_ctxt, align 4, !tbaa !2
  %10 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt, align 4, !tbaa !2
  %ctxt2 = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %11, i32 0, i32 1
  %arraydecay = getelementptr inbounds [3 x %struct.FilterFrameCtxt], [3 x %struct.FilterFrameCtxt]* %ctxt2, i32 0, i32 0
  store %struct.FilterFrameCtxt* %arraydecay, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %12 = bitcast i32* %lr_unit_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = bitcast i32* %tile_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  store i32 0, i32* %tile_row, align 4, !tbaa !9
  %15 = bitcast i32* %tile_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  store i32 0, i32* %tile_col, align 4, !tbaa !9
  %16 = bitcast i32* %tile_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  store i32 1, i32* %tile_cols, align 4, !tbaa !9
  %17 = bitcast i32* %tile_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  store i32 0, i32* %tile_idx, align 4, !tbaa !9
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  br label %while.body

while.body:                                       ; preds = %while.cond
  %18 = bitcast %struct.AV1LrMTInfo** %cur_job_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync, align 4, !tbaa !2
  %call = call %struct.AV1LrMTInfo* @get_lr_job_info(%struct.AV1LrSyncData* %19)
  store %struct.AV1LrMTInfo* %call, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %20 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %cmp = icmp ne %struct.AV1LrMTInfo* %20, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %21 = bitcast %struct.RestorationTileLimits* %limits to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #5
  %22 = bitcast void (i8*, i32, i32, i32)** %on_sync_read to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = bitcast void (i8*, i32, i32, i32, i32)** %on_sync_write to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  %24 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %v_start = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %24, i32 0, i32 0
  %25 = load i32, i32* %v_start, align 4, !tbaa !96
  %v_start3 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 2
  store i32 %25, i32* %v_start3, align 4, !tbaa !92
  %26 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %v_end = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %26, i32 0, i32 1
  %27 = load i32, i32* %v_end, align 4, !tbaa !97
  %v_end4 = getelementptr inbounds %struct.RestorationTileLimits, %struct.RestorationTileLimits* %limits, i32 0, i32 3
  store i32 %27, i32* %v_end4, align 4, !tbaa !58
  %28 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %lr_unit_row5 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %28, i32 0, i32 2
  %29 = load i32, i32* %lr_unit_row5, align 4, !tbaa !93
  store i32 %29, i32* %lr_unit_row, align 4, !tbaa !9
  %30 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %plane6 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %30, i32 0, i32 3
  %31 = load i32, i32* %plane6, align 4, !tbaa !95
  store i32 %31, i32* %plane, align 4, !tbaa !9
  %32 = bitcast i32* %unit_idx0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  %33 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %34 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %33, i32 %34
  %rsi = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx, i32 0, i32 0
  %35 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi, align 4, !tbaa !88
  %units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %35, i32 0, i32 2
  %36 = load i32, i32* %units_per_tile, align 4, !tbaa !101
  %mul = mul nsw i32 0, %36
  store i32 %mul, i32* %unit_idx0, align 4, !tbaa !9
  %37 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %sync_mode = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %37, i32 0, i32 4
  %38 = load i32, i32* %sync_mode, align 4, !tbaa !98
  %cmp7 = icmp eq i32 %38, 1
  %39 = zext i1 %cmp7 to i64
  %cond = select i1 %cmp7, void (i8*, i32, i32, i32)* @lr_sync_read, void (i8*, i32, i32, i32)* @av1_lr_sync_read_dummy
  store void (i8*, i32, i32, i32)* %cond, void (i8*, i32, i32, i32)** %on_sync_read, align 4, !tbaa !2
  %40 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %sync_mode8 = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %40, i32 0, i32 4
  %41 = load i32, i32* %sync_mode8, align 4, !tbaa !98
  %cmp9 = icmp eq i32 %41, 0
  %42 = zext i1 %cmp9 to i64
  %cond10 = select i1 %cmp9, void (i8*, i32, i32, i32, i32)* @lr_sync_write, void (i8*, i32, i32, i32, i32)* @av1_lr_sync_write_dummy
  store void (i8*, i32, i32, i32, i32)* %cond10, void (i8*, i32, i32, i32, i32)** %on_sync_write, align 4, !tbaa !2
  %43 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %44 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx11 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %43, i32 %44
  %tile_rect = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx11, i32 0, i32 10
  %45 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt, align 4, !tbaa !2
  %on_rest_unit = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %45, i32 0, i32 0
  %46 = load void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)** %on_rest_unit, align 4, !tbaa !102
  %47 = load i32, i32* %lr_unit_row, align 4, !tbaa !9
  %48 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %49 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx12 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %48, i32 %49
  %rsi13 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx12, i32 0, i32 0
  %50 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi13, align 4, !tbaa !88
  %restoration_unit_size = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %50, i32 0, i32 1
  %51 = load i32, i32* %restoration_unit_size, align 4, !tbaa !60
  %52 = load i32, i32* %unit_idx0, align 4, !tbaa !9
  %53 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %54 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx14 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %53, i32 %54
  %rsi15 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx14, i32 0, i32 0
  %55 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi15, align 4, !tbaa !88
  %horz_units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %55, i32 0, i32 4
  %56 = load i32, i32* %horz_units_per_tile, align 4, !tbaa !104
  %57 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %58 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx16 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %57, i32 %58
  %rsi17 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx16, i32 0, i32 0
  %59 = load %struct.RestorationInfo*, %struct.RestorationInfo** %rsi17, align 4, !tbaa !88
  %vert_units_per_tile = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %59, i32 0, i32 3
  %60 = load i32, i32* %vert_units_per_tile, align 4, !tbaa !90
  %61 = load i32, i32* %plane, align 4, !tbaa !9
  %62 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %63 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx18 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %62, i32 %63
  %64 = bitcast %struct.FilterFrameCtxt* %arrayidx18 to i8*
  %65 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata, align 4, !tbaa !2
  %rst_tmpbuf = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %65, i32 0, i32 0
  %66 = load i32*, i32** %rst_tmpbuf, align 4, !tbaa !50
  %67 = load %struct.LoopRestorationWorkerData*, %struct.LoopRestorationWorkerData** %lrworkerdata, align 4, !tbaa !2
  %rlbs = getelementptr inbounds %struct.LoopRestorationWorkerData, %struct.LoopRestorationWorkerData* %67, i32 0, i32 1
  %68 = load i8*, i8** %rlbs, align 4, !tbaa !52
  %69 = bitcast i8* %68 to %struct.RestorationLineBuffers*
  %70 = load void (i8*, i32, i32, i32)*, void (i8*, i32, i32, i32)** %on_sync_read, align 4, !tbaa !2
  %71 = load void (i8*, i32, i32, i32, i32)*, void (i8*, i32, i32, i32, i32)** %on_sync_write, align 4, !tbaa !2
  %72 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync, align 4, !tbaa !2
  call void @av1_foreach_rest_unit_in_row(%struct.RestorationTileLimits* %limits, %struct.AV1PixelRect* %tile_rect, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)* %46, i32 %47, i32 %51, i32 %52, i32 %56, i32 %60, i32 %61, i8* %64, i32* %66, %struct.RestorationLineBuffers* %69, void (i8*, i32, i32, i32)* %70, void (i8*, i32, i32, i32, i32)* %71, %struct.AV1LrSyncData* %72)
  %73 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx19 = getelementptr inbounds [3 x void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)*], [3 x void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)*]* @loop_restoration_row_worker.copy_funs, i32 0, i32 %73
  %74 = load void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)*, void (%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32)** %arrayidx19, align 4, !tbaa !2
  %75 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %75, i32 0, i32 3
  %76 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst, align 4, !tbaa !105
  %77 = load %struct.AV1LrStruct*, %struct.AV1LrStruct** %lr_ctxt, align 4, !tbaa !2
  %frame = getelementptr inbounds %struct.AV1LrStruct, %struct.AV1LrStruct* %77, i32 0, i32 2
  %78 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame, align 4, !tbaa !106
  %79 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %80 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx20 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %79, i32 %80
  %tile_rect21 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx20, i32 0, i32 10
  %left = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect21, i32 0, i32 0
  %81 = load i32, i32* %left, align 4, !tbaa !107
  %82 = load %struct.FilterFrameCtxt*, %struct.FilterFrameCtxt** %ctxt, align 4, !tbaa !2
  %83 = load i32, i32* %plane, align 4, !tbaa !9
  %arrayidx22 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %82, i32 %83
  %tile_rect23 = getelementptr inbounds %struct.FilterFrameCtxt, %struct.FilterFrameCtxt* %arrayidx22, i32 0, i32 10
  %right = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %tile_rect23, i32 0, i32 2
  %84 = load i32, i32* %right, align 4, !tbaa !108
  %85 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %v_copy_start = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %85, i32 0, i32 5
  %86 = load i32, i32* %v_copy_start, align 4, !tbaa !99
  %87 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %v_copy_end = getelementptr inbounds %struct.AV1LrMTInfo, %struct.AV1LrMTInfo* %87, i32 0, i32 6
  %88 = load i32, i32* %v_copy_end, align 4, !tbaa !100
  call void %74(%struct.yv12_buffer_config* %76, %struct.yv12_buffer_config* %78, i32 %81, i32 %84, i32 %86, i32 %88)
  %89 = bitcast i32* %unit_idx0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #5
  %90 = bitcast void (i8*, i32, i32, i32, i32)** %on_sync_write to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #5
  %91 = bitcast void (i8*, i32, i32, i32)** %on_sync_read to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #5
  %92 = bitcast %struct.RestorationTileLimits* %limits to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %92) #5
  br label %if.end

if.else:                                          ; preds = %while.body
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else
  %93 = bitcast %struct.AV1LrMTInfo** %cur_job_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %94 = bitcast i32* %tile_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  %95 = bitcast i32* %tile_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #5
  %96 = bitcast i32* %tile_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #5
  %97 = bitcast i32* %tile_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #5
  %98 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #5
  %99 = bitcast i32* %lr_unit_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #5
  %100 = bitcast %struct.FilterFrameCtxt** %ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #5
  %101 = bitcast %struct.AV1LrStruct** %lr_ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #5
  %102 = bitcast %struct.LoopRestorationWorkerData** %lrworkerdata to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #5
  %103 = bitcast %struct.AV1LrSyncData** %lr_sync to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #5
  ret i32 1

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare i8* @aom_memalign(i32, i32) #2

; Function Attrs: inlinehint nounwind
define internal i32 @get_lr_sync_range(i32 %width) #4 {
entry:
  %width.addr = alloca i32, align 4
  store i32 %width, i32* %width.addr, align 4, !tbaa !9
  %0 = load i32, i32* %width.addr, align 4, !tbaa !9
  ret i32 1
}

declare void @aom_yv12_partial_coloc_copy_y_c(%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32) #2

declare void @aom_yv12_partial_coloc_copy_u_c(%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32) #2

declare void @aom_yv12_partial_coloc_copy_v_c(%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32, i32, i32, i32) #2

; Function Attrs: nounwind
define internal %struct.AV1LrMTInfo* @get_lr_job_info(%struct.AV1LrSyncData* %lr_sync) #0 {
entry:
  %lr_sync.addr = alloca %struct.AV1LrSyncData*, align 4
  %cur_job_info = alloca %struct.AV1LrMTInfo*, align 4
  store %struct.AV1LrSyncData* %lr_sync, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AV1LrMTInfo** %cur_job_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store %struct.AV1LrMTInfo* null, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %1 = load %struct.AV1LrSyncData*, %struct.AV1LrSyncData** %lr_sync.addr, align 4, !tbaa !2
  %2 = load %struct.AV1LrMTInfo*, %struct.AV1LrMTInfo** %cur_job_info, align 4, !tbaa !2
  %3 = bitcast %struct.AV1LrMTInfo** %cur_job_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  ret %struct.AV1LrMTInfo* %2
}

; Function Attrs: inlinehint nounwind
define internal void @lr_sync_read(i8* %lr_sync, i32 %r, i32 %c, i32 %plane) #4 {
entry:
  %lr_sync.addr = alloca i8*, align 4
  %r.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  store i8* %lr_sync, i8** %lr_sync.addr, align 4, !tbaa !2
  store i32 %r, i32* %r.addr, align 4, !tbaa !9
  store i32 %c, i32* %c.addr, align 4, !tbaa !9
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !9
  %0 = load i8*, i8** %lr_sync.addr, align 4, !tbaa !2
  %1 = load i32, i32* %r.addr, align 4, !tbaa !9
  %2 = load i32, i32* %c.addr, align 4, !tbaa !9
  %3 = load i32, i32* %plane.addr, align 4, !tbaa !9
  ret void
}

declare void @av1_lr_sync_read_dummy(i8*, i32, i32, i32) #2

; Function Attrs: inlinehint nounwind
define internal void @lr_sync_write(i8* %lr_sync, i32 %r, i32 %c, i32 %sb_cols, i32 %plane) #4 {
entry:
  %lr_sync.addr = alloca i8*, align 4
  %r.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  %sb_cols.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  store i8* %lr_sync, i8** %lr_sync.addr, align 4, !tbaa !2
  store i32 %r, i32* %r.addr, align 4, !tbaa !9
  store i32 %c, i32* %c.addr, align 4, !tbaa !9
  store i32 %sb_cols, i32* %sb_cols.addr, align 4, !tbaa !9
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !9
  %0 = load i8*, i8** %lr_sync.addr, align 4, !tbaa !2
  %1 = load i32, i32* %r.addr, align 4, !tbaa !9
  %2 = load i32, i32* %c.addr, align 4, !tbaa !9
  %3 = load i32, i32* %sb_cols.addr, align 4, !tbaa !9
  %4 = load i32, i32* %plane.addr, align 4, !tbaa !9
  ret void
}

declare void @av1_lr_sync_write_dummy(i8*, i32, i32, i32, i32) #2

declare void @av1_foreach_rest_unit_in_row(%struct.RestorationTileLimits*, %struct.AV1PixelRect*, void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, i32, i32, i32, i32, i32, i32, i8*, i32*, %struct.RestorationLineBuffers*, void (i8*, i32, i32, i32)*, void (i8*, i32, i32, i32, i32)*, %struct.AV1LrSyncData*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 20}
!7 = !{!"AV1LfSyncData", !4, i64 0, !8, i64 12, !8, i64 16, !3, i64 20, !8, i64 24, !3, i64 28, !8, i64 32, !8, i64 36}
!8 = !{!"int", !4, i64 0}
!9 = !{!8, !8, i64 0}
!10 = !{!7, !3, i64 28}
!11 = !{!12, !8, i64 1368}
!12 = !{!"AV1Common", !13, i64 0, !15, i64 40, !8, i64 288, !8, i64 292, !8, i64 296, !8, i64 300, !8, i64 304, !8, i64 308, !4, i64 312, !16, i64 313, !4, i64 316, !8, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !17, i64 492, !4, i64 580, !4, i64 1284, !8, i64 1316, !8, i64 1320, !8, i64 1324, !18, i64 1328, !19, i64 1356, !20, i64 1420, !21, i64 10676, !3, i64 10848, !22, i64 10864, !23, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !24, i64 14880, !26, i64 15028, !27, i64 15168, !29, i64 15816, !4, i64 15836, !30, i64 16192, !3, i64 18128, !3, i64 18132, !33, i64 18136, !3, i64 18724, !34, i64 18728, !8, i64 18760, !4, i64 18764, !3, i64 18796, !8, i64 18800, !4, i64 18804, !4, i64 18836, !8, i64 18844, !8, i64 18848, !8, i64 18852, !8, i64 18856}
!13 = !{!"", !4, i64 0, !4, i64 1, !8, i64 4, !8, i64 8, !8, i64 12, !14, i64 16, !8, i64 32, !8, i64 36}
!14 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!15 = !{!"aom_internal_error_info", !4, i64 0, !8, i64 4, !4, i64 8, !8, i64 88, !4, i64 92}
!16 = !{!"_Bool", !4, i64 0}
!17 = !{!"scale_factors", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!18 = !{!"", !16, i64 0, !16, i64 1, !16, i64 2, !16, i64 3, !16, i64 4, !16, i64 5, !16, i64 6, !16, i64 7, !16, i64 8, !16, i64 9, !16, i64 10, !16, i64 11, !4, i64 12, !4, i64 13, !8, i64 16, !8, i64 20, !4, i64 24}
!19 = !{!"CommonModeInfoParams", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !3, i64 20, !8, i64 24, !8, i64 28, !4, i64 32, !3, i64 36, !8, i64 40, !8, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!20 = !{!"CommonQuantParams", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !16, i64 9240, !8, i64 9244, !8, i64 9248, !8, i64 9252}
!21 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !8, i64 164, !4, i64 168}
!22 = !{!"", !4, i64 0, !4, i64 3072}
!23 = !{!"loopfilter", !4, i64 0, !8, i64 8, !8, i64 12, !8, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !8, i64 32}
!24 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !8, i64 52, !4, i64 56, !3, i64 68, !8, i64 72, !3, i64 76, !25, i64 80, !8, i64 84, !25, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !8, i64 128, !8, i64 132, !8, i64 136, !8, i64 140, !3, i64 144}
!25 = !{!"long", !4, i64 0}
!26 = !{!"", !8, i64 0, !8, i64 4, !4, i64 8, !4, i64 72, !8, i64 136}
!27 = !{!"", !8, i64 0, !8, i64 4, !4, i64 8, !8, i64 120, !4, i64 124, !8, i64 204, !4, i64 208, !8, i64 288, !8, i64 292, !8, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !8, i64 596, !8, i64 600, !8, i64 604, !8, i64 608, !8, i64 612, !8, i64 616, !8, i64 620, !8, i64 624, !8, i64 628, !8, i64 632, !8, i64 636, !8, i64 640, !28, i64 644}
!28 = !{!"short", !4, i64 0}
!29 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16}
!30 = !{!"SequenceHeader", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !4, i64 16, !8, i64 20, !8, i64 24, !4, i64 28, !8, i64 32, !8, i64 36, !14, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !8, i64 112, !4, i64 116, !8, i64 244, !31, i64 248, !4, i64 264, !32, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!31 = !{!"aom_timing", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!32 = !{!"aom_dec_model_info", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!33 = !{!"CommonTileParams", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !4, i64 60, !4, i64 320, !8, i64 580, !8, i64 584}
!34 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !8, i64 20, !8, i64 24, !8, i64 28}
!35 = !{!7, !8, i64 12}
!36 = !{!7, !8, i64 16}
!37 = !{!7, !8, i64 24}
!38 = !{!12, !8, i64 288}
!39 = !{!40, !3, i64 12}
!40 = !{!"", !3, i64 0, !4, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !8, i64 24}
!41 = !{!40, !3, i64 16}
!42 = !{!40, !3, i64 20}
!43 = !{!44, !3, i64 16}
!44 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20}
!45 = !{!44, !3, i64 12}
!46 = !{!44, !3, i64 8}
!47 = !{!48, !3, i64 32}
!48 = !{!"AV1LrSyncData", !4, i64 0, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !3, i64 28, !3, i64 32, !8, i64 36, !8, i64 40}
!49 = !{!48, !3, i64 28}
!50 = !{!51, !3, i64 0}
!51 = !{!"LoopRestorationWorkerData", !3, i64 0, !3, i64 4, !3, i64 8}
!52 = !{!51, !3, i64 4}
!53 = !{!12, !4, i64 16269}
!54 = !{!55, !4, i64 0}
!55 = !{!"", !4, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !3, i64 20, !56, i64 24, !8, i64 40}
!56 = !{!"", !3, i64 0, !3, i64 4, !8, i64 8, !8, i64 12}
!57 = !{i64 0, i64 4, !9, i64 4, i64 4, !9, i64 8, i64 4, !9, i64 12, i64 4, !9}
!58 = !{!14, !8, i64 12}
!59 = !{!14, !8, i64 4}
!60 = !{!55, !8, i64 4}
!61 = !{!48, !8, i64 12}
!62 = !{!48, !8, i64 16}
!63 = !{!48, !8, i64 24}
!64 = !{!48, !8, i64 20}
!65 = !{!51, !3, i64 8}
!66 = !{!7, !8, i64 32}
!67 = !{!7, !8, i64 36}
!68 = !{!12, !8, i64 14712}
!69 = !{!12, !8, i64 14716}
!70 = !{!71, !8, i64 0}
!71 = !{!"AV1LfMTInfo", !8, i64 0, !8, i64 4, !8, i64 8}
!72 = !{!71, !8, i64 4}
!73 = !{!71, !8, i64 8}
!74 = !{!75, !3, i64 0}
!75 = !{!"LoopFilterWorkerData", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 4052}
!76 = !{!75, !3, i64 4}
!77 = !{!75, !3, i64 4052}
!78 = !{!79, !8, i64 16}
!79 = !{!"macroblockd_plane", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !8, i64 16, !8, i64 20, !80, i64 24, !4, i64 44, !3, i64 84, !3, i64 88, !4, i64 92, !3, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!80 = !{!"buf_2d", !3, i64 0, !3, i64 4, !8, i64 8, !8, i64 12, !8, i64 16}
!81 = !{!79, !8, i64 20}
!82 = !{!12, !8, i64 1372}
!83 = !{!12, !4, i64 16220}
!84 = !{!12, !3, i64 14872}
!85 = !{!12, !3, i64 14876}
!86 = !{!48, !8, i64 36}
!87 = !{!48, !8, i64 40}
!88 = !{!89, !3, i64 0}
!89 = !{!"FilterFrameCtxt", !3, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !3, i64 28, !8, i64 32, !8, i64 36, !14, i64 40}
!90 = !{!55, !8, i64 12}
!91 = !{!12, !8, i64 16292}
!92 = !{!14, !8, i64 8}
!93 = !{!94, !8, i64 8}
!94 = !{!"AV1LrMTInfo", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24}
!95 = !{!94, !8, i64 12}
!96 = !{!94, !8, i64 0}
!97 = !{!94, !8, i64 4}
!98 = !{!94, !8, i64 16}
!99 = !{!94, !8, i64 20}
!100 = !{!94, !8, i64 24}
!101 = !{!55, !8, i64 8}
!102 = !{!103, !3, i64 0}
!103 = !{!"AV1LrStruct", !3, i64 0, !4, i64 4, !3, i64 172, !3, i64 176}
!104 = !{!55, !8, i64 16}
!105 = !{!103, !3, i64 176}
!106 = !{!103, !3, i64 172}
!107 = !{!89, !8, i64 40}
!108 = !{!89, !8, i64 48}
