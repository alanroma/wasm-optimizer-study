; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/mvref_common.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/mvref_common.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.mv = type { i16, i16 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.position = type { i32, i32 }
%struct.REF_FRAME_INFO = type { i32, %struct.RefCntBuffer*, i32 }
%struct.SubpelMvLimits = type { i32, i32, i32, i32 }

@block_size_wide = internal constant [22 x i8] c"\04\04\08\08\08\10\10\10   @@@\80\80\04\10\08 \10@", align 16
@block_size_high = internal constant [22 x i8] c"\04\08\04\08\10\08\10 \10 @ @\80@\80\10\04 \08@\10", align 16
@mi_size_wide = internal constant [22 x i8] c"\01\01\02\02\02\04\04\04\08\08\08\10\10\10  \01\04\02\08\04\10", align 16
@mi_size_high = internal constant [22 x i8] c"\01\02\01\02\04\02\04\08\04\08\10\08\10 \10 \04\01\08\02\10\04", align 16
@__const.av1_setup_skip_mode_allowed.ref_order_hints = private unnamed_addr constant [2 x i32] [i32 -1, i32 2147483647], align 4
@__const.av1_setup_skip_mode_allowed.ref_idx = private unnamed_addr constant [2 x i32] [i32 -1, i32 -1], align 4
@.str = private unnamed_addr constant [48 x i8] c"Inter frame requests a look-ahead frame as LAST\00", align 1
@.str.1 = private unnamed_addr constant [50 x i8] c"Inter frame requests a look-ahead frame as GOLDEN\00", align 1
@av1_set_frame_refs.ref_frame_list = internal constant [5 x i8] c"\02\03\05\06\07", align 1
@ref_frame_map = internal global [21 x [2 x i8]] [[2 x i8] c"\01\05", [2 x i8] c"\02\05", [2 x i8] c"\03\05", [2 x i8] c"\04\05", [2 x i8] c"\01\06", [2 x i8] c"\02\06", [2 x i8] c"\03\06", [2 x i8] c"\04\06", [2 x i8] c"\01\07", [2 x i8] c"\02\07", [2 x i8] c"\03\07", [2 x i8] c"\04\07", [2 x i8] c"\01\02", [2 x i8] c"\01\03", [2 x i8] c"\01\04", [2 x i8] c"\05\07", [2 x i8] c"\02\03", [2 x i8] c"\02\04", [2 x i8] c"\03\04", [2 x i8] c"\05\06", [2 x i8] c"\06\07"], align 16
@div_mult = internal global [32 x i32] [i32 0, i32 16384, i32 8192, i32 5461, i32 4096, i32 3276, i32 2730, i32 2340, i32 2048, i32 1820, i32 1638, i32 1489, i32 1365, i32 1260, i32 1170, i32 1092, i32 1024, i32 963, i32 910, i32 862, i32 819, i32 780, i32 744, i32 712, i32 682, i32 655, i32 630, i32 606, i32 585, i32 564, i32 546, i32 528], align 16

; Function Attrs: nounwind
define hidden void @av1_copy_frame_mvs(%struct.AV1Common* %cm, %struct.MB_MODE_INFO* %mi, i32 %mi_row, i32 %mi_col, i32 %x_mis, i32 %y_mis) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %x_mis.addr = alloca i32, align 4
  %y_mis.addr = alloca i32, align 4
  %frame_mvs_stride = alloca i32, align 4
  %frame_mvs = alloca %struct.MV_REF*, align 4
  %w = alloca i32, align 4
  %h = alloca i32, align 4
  %mv = alloca %struct.MV_REF*, align 4
  %idx = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref_frame15 = alloca i8, align 1
  %ref_idx = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %mi, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i32 %x_mis, i32* %x_mis.addr, align 4, !tbaa !6
  store i32 %y_mis, i32* %y_mis.addr, align 4, !tbaa !6
  %0 = bitcast i32* %frame_mvs_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %2 = load i32, i32* %mi_cols, align 4, !tbaa !8
  %add = add nsw i32 %2, 1
  %shr = ashr i32 %add, 1
  store i32 %shr, i32* %frame_mvs_stride, align 4, !tbaa !6
  %3 = bitcast %struct.MV_REF** %frame_mvs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 13
  %5 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !32
  %mvs = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %5, i32 0, i32 5
  %6 = load %struct.MV_REF*, %struct.MV_REF** %mvs, align 4, !tbaa !33
  %7 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %shr1 = ashr i32 %7, 1
  %8 = load i32, i32* %frame_mvs_stride, align 4, !tbaa !6
  %mul = mul nsw i32 %shr1, %8
  %add.ptr = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %6, i32 %mul
  %9 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %shr2 = ashr i32 %9, 1
  %add.ptr3 = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %add.ptr, i32 %shr2
  store %struct.MV_REF* %add.ptr3, %struct.MV_REF** %frame_mvs, align 4, !tbaa !2
  %10 = load i32, i32* %x_mis.addr, align 4, !tbaa !6
  %add4 = add nsw i32 %10, 1
  %shr5 = ashr i32 %add4, 1
  store i32 %shr5, i32* %x_mis.addr, align 4, !tbaa !6
  %11 = load i32, i32* %y_mis.addr, align 4, !tbaa !6
  %add6 = add nsw i32 %11, 1
  %shr7 = ashr i32 %add6, 1
  store i32 %shr7, i32* %y_mis.addr, align 4, !tbaa !6
  %12 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %h, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc50, %entry
  %14 = load i32, i32* %h, align 4, !tbaa !6
  %15 = load i32, i32* %y_mis.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %14, %15
  br i1 %cmp, label %for.body, label %for.end52

for.body:                                         ; preds = %for.cond
  %16 = bitcast %struct.MV_REF** %mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.MV_REF*, %struct.MV_REF** %frame_mvs, align 4, !tbaa !2
  store %struct.MV_REF* %17, %struct.MV_REF** %mv, align 4, !tbaa !2
  store i32 0, i32* %w, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc46, %for.body
  %18 = load i32, i32* %w, align 4, !tbaa !6
  %19 = load i32, i32* %x_mis.addr, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %18, %19
  br i1 %cmp9, label %for.body10, label %for.end48

for.body10:                                       ; preds = %for.cond8
  %20 = load %struct.MV_REF*, %struct.MV_REF** %mv, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %20, i32 0, i32 1
  store i8 -1, i8* %ref_frame, align 4, !tbaa !39
  %21 = load %struct.MV_REF*, %struct.MV_REF** %mv, align 4, !tbaa !2
  %mv11 = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %21, i32 0, i32 0
  %as_int = bitcast %union.int_mv* %mv11 to i32*
  store i32 0, i32* %as_int, align 4, !tbaa !41
  %22 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %for.body10
  %23 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %23, 2
  br i1 %cmp13, label %for.body14, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond12
  store i32 8, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  br label %for.end

for.body14:                                       ; preds = %for.cond12
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_frame15) #6
  %25 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %ref_frame16 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %25, i32 0, i32 12
  %26 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame16, i32 0, i32 %26
  %27 = load i8, i8* %arrayidx, align 1, !tbaa !41
  store i8 %27, i8* %ref_frame15, align 1, !tbaa !41
  %28 = load i8, i8* %ref_frame15, align 1, !tbaa !41
  %conv = sext i8 %28 to i32
  %cmp17 = icmp sgt i32 %conv, 0
  br i1 %cmp17, label %if.then, label %if.end41

if.then:                                          ; preds = %for.body14
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_idx) #6
  %29 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_side = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %29, i32 0, i32 48
  %30 = load i8, i8* %ref_frame15, align 1, !tbaa !41
  %idxprom = sext i8 %30 to i32
  %arrayidx19 = getelementptr inbounds [8 x i8], [8 x i8]* %ref_frame_side, i32 0, i32 %idxprom
  %31 = load i8, i8* %arrayidx19, align 1, !tbaa !41
  store i8 %31, i8* %ref_idx, align 1, !tbaa !41
  %32 = load i8, i8* %ref_idx, align 1, !tbaa !41
  %tobool = icmp ne i8 %32, 0
  br i1 %tobool, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %33 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %mv21 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %33, i32 0, i32 2
  %34 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv21, i32 0, i32 %34
  %as_mv = bitcast %union.int_mv* %arrayidx22 to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv, i32 0, i32 0
  %35 = load i16, i16* %row, align 4, !tbaa !41
  %conv23 = sext i16 %35 to i32
  %call = call i32 @abs(i32 %conv23) #7
  %cmp24 = icmp sgt i32 %call, 4095
  br i1 %cmp24, label %if.then33, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %36 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %mv26 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %36, i32 0, i32 2
  %37 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv26, i32 0, i32 %37
  %as_mv28 = bitcast %union.int_mv* %arrayidx27 to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv28, i32 0, i32 1
  %38 = load i16, i16* %col, align 2, !tbaa !41
  %conv29 = sext i16 %38 to i32
  %call30 = call i32 @abs(i32 %conv29) #7
  %cmp31 = icmp sgt i32 %call30, 4095
  br i1 %cmp31, label %if.then33, label %if.end34

if.then33:                                        ; preds = %lor.lhs.false, %if.end
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end34:                                         ; preds = %lor.lhs.false
  %39 = load i8, i8* %ref_frame15, align 1, !tbaa !41
  %40 = load %struct.MV_REF*, %struct.MV_REF** %mv, align 4, !tbaa !2
  %ref_frame35 = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %40, i32 0, i32 1
  store i8 %39, i8* %ref_frame35, align 4, !tbaa !39
  %41 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %mv36 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %41, i32 0, i32 2
  %42 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv36, i32 0, i32 %42
  %as_int38 = bitcast %union.int_mv* %arrayidx37 to i32*
  %43 = load i32, i32* %as_int38, align 4, !tbaa !41
  %44 = load %struct.MV_REF*, %struct.MV_REF** %mv, align 4, !tbaa !2
  %mv39 = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %44, i32 0, i32 0
  %as_int40 = bitcast %union.int_mv* %mv39 to i32*
  store i32 %43, i32* %as_int40, align 4, !tbaa !41
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end34, %if.then33, %if.then20
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_idx) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup42 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end41

if.end41:                                         ; preds = %cleanup.cont, %for.body14
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup42

cleanup42:                                        ; preds = %if.end41, %cleanup
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_frame15) #6
  %cleanup.dest43 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest43, label %unreachable [
    i32 0, label %cleanup.cont44
    i32 10, label %for.inc
  ]

cleanup.cont44:                                   ; preds = %cleanup42
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont44, %cleanup42
  %45 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond12

for.end:                                          ; preds = %for.cond.cleanup
  %46 = load %struct.MV_REF*, %struct.MV_REF** %mv, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %46, i32 1
  store %struct.MV_REF* %incdec.ptr, %struct.MV_REF** %mv, align 4, !tbaa !2
  br label %for.inc46

for.inc46:                                        ; preds = %for.end
  %47 = load i32, i32* %w, align 4, !tbaa !6
  %inc47 = add nsw i32 %47, 1
  store i32 %inc47, i32* %w, align 4, !tbaa !6
  br label %for.cond8

for.end48:                                        ; preds = %for.cond8
  %48 = load i32, i32* %frame_mvs_stride, align 4, !tbaa !6
  %49 = load %struct.MV_REF*, %struct.MV_REF** %frame_mvs, align 4, !tbaa !2
  %add.ptr49 = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %49, i32 %48
  store %struct.MV_REF* %add.ptr49, %struct.MV_REF** %frame_mvs, align 4, !tbaa !2
  %50 = bitcast %struct.MV_REF** %mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  br label %for.inc50

for.inc50:                                        ; preds = %for.end48
  %51 = load i32, i32* %h, align 4, !tbaa !6
  %inc51 = add nsw i32 %51, 1
  store i32 %inc51, i32* %h, align 4, !tbaa !6
  br label %for.cond

for.end52:                                        ; preds = %for.cond
  %52 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  %53 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  %54 = bitcast %struct.MV_REF** %frame_mvs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast i32* %frame_mvs_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #6
  ret void

unreachable:                                      ; preds = %cleanup42
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_find_mv_refs(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.MB_MODE_INFO* %mi, i8 signext %ref_frame, i8* %ref_mv_count, [8 x %struct.candidate_mv]* %ref_mv_stack, [8 x i16]* %ref_mv_weight, [2 x %union.int_mv]* %mv_ref_list, %union.int_mv* %global_mvs, i16* %mode_context) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %ref_frame.addr = alloca i8, align 1
  %ref_mv_count.addr = alloca i8*, align 4
  %ref_mv_stack.addr = alloca [8 x %struct.candidate_mv]*, align 4
  %ref_mv_weight.addr = alloca [8 x i16]*, align 4
  %mv_ref_list.addr = alloca [2 x %union.int_mv]*, align 4
  %global_mvs.addr = alloca %union.int_mv*, align 4
  %mode_context.addr = alloca i16*, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %gm_mv = alloca [2 x %union.int_mv], align 4
  %bsize = alloca i8, align 1
  %allow_high_precision_mv = alloca i32, align 4
  %force_integer_mv = alloca i32, align 4
  %tmp = alloca %union.int_mv, align 4
  %rf = alloca [2 x i8], align 1
  %tmp34 = alloca %union.int_mv, align 4
  %tmp40 = alloca %union.int_mv, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %mi, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !41
  store i8* %ref_mv_count, i8** %ref_mv_count.addr, align 4, !tbaa !2
  store [8 x %struct.candidate_mv]* %ref_mv_stack, [8 x %struct.candidate_mv]** %ref_mv_stack.addr, align 4, !tbaa !2
  store [8 x i16]* %ref_mv_weight, [8 x i16]** %ref_mv_weight.addr, align 4, !tbaa !2
  store [2 x %union.int_mv]* %mv_ref_list, [2 x %union.int_mv]** %mv_ref_list.addr, align 4, !tbaa !2
  store %union.int_mv* %global_mvs, %union.int_mv** %global_mvs.addr, align 4, !tbaa !2
  store i16* %mode_context, i16** %mode_context.addr, align 4, !tbaa !2
  %0 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_row1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 0
  %2 = load i32, i32* %mi_row1, align 16, !tbaa !42
  store i32 %2, i32* %mi_row, align 4, !tbaa !6
  %3 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_col2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 1
  %5 = load i32, i32* %mi_col2, align 4, !tbaa !47
  store i32 %5, i32* %mi_col, align 4, !tbaa !6
  %6 = bitcast [2 x %union.int_mv]* %gm_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #6
  %7 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %conv = sext i8 %7 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %arrayidx = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %gm_mv, i32 0, i32 1
  %as_int = bitcast %union.int_mv* %arrayidx to i32*
  store i32 0, i32* %as_int, align 4, !tbaa !41
  %arrayidx4 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %gm_mv, i32 0, i32 0
  %as_int5 = bitcast %union.int_mv* %arrayidx4 to i32*
  store i32 0, i32* %as_int5, align 4, !tbaa !41
  %8 = load %union.int_mv*, %union.int_mv** %global_mvs.addr, align 4, !tbaa !2
  %cmp6 = icmp ne %union.int_mv* %8, null
  br i1 %cmp6, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.then
  %9 = load %union.int_mv*, %union.int_mv** %global_mvs.addr, align 4, !tbaa !2
  %10 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom = sext i8 %10 to i32
  %arrayidx9 = getelementptr inbounds %union.int_mv, %union.int_mv* %9, i32 %idxprom
  %as_int10 = bitcast %union.int_mv* %arrayidx9 to i32*
  store i32 -2147450880, i32* %as_int10, align 4, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then8, %if.then
  br label %if.end46

if.else:                                          ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %11, i32 0, i32 6
  %12 = load i8, i8* %sb_type, align 2, !tbaa !48
  store i8 %12, i8* %bsize, align 1, !tbaa !41
  %13 = bitcast i32* %allow_high_precision_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 21
  %allow_high_precision_mv11 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 1
  %15 = load i8, i8* %allow_high_precision_mv11, align 1, !tbaa !54, !range !55
  %tobool = trunc i8 %15 to i1
  %conv12 = zext i1 %tobool to i32
  store i32 %conv12, i32* %allow_high_precision_mv, align 4, !tbaa !6
  %16 = bitcast i32* %force_integer_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %features13 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 21
  %cur_frame_force_integer_mv = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features13, i32 0, i32 2
  %18 = load i8, i8* %cur_frame_force_integer_mv, align 2, !tbaa !56, !range !55
  %tobool14 = trunc i8 %18 to i1
  %conv15 = zext i1 %tobool14 to i32
  store i32 %conv15, i32* %force_integer_mv, align 4, !tbaa !6
  %19 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %conv16 = sext i8 %19 to i32
  %cmp17 = icmp slt i32 %conv16, 8
  br i1 %cmp17, label %if.then19, label %if.else32

if.then19:                                        ; preds = %if.else
  %arrayidx20 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %gm_mv, i32 0, i32 0
  %20 = bitcast %union.int_mv* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %global_motion = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 36
  %22 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom21 = sext i8 %22 to i32
  %arrayidx22 = getelementptr inbounds [8 x %struct.WarpedMotionParams], [8 x %struct.WarpedMotionParams]* %global_motion, i32 0, i32 %idxprom21
  %23 = load i32, i32* %allow_high_precision_mv, align 4, !tbaa !6
  %24 = load i8, i8* %bsize, align 1, !tbaa !41
  %25 = load i32, i32* %mi_col, align 4, !tbaa !6
  %26 = load i32, i32* %mi_row, align 4, !tbaa !6
  %27 = load i32, i32* %force_integer_mv, align 4, !tbaa !6
  call void @gm_get_motion_vector(%union.int_mv* sret align 4 %tmp, %struct.WarpedMotionParams* %arrayidx22, i32 %23, i8 zeroext %24, i32 %25, i32 %26, i32 %27)
  %28 = bitcast %union.int_mv* %arrayidx20 to i8*
  %29 = bitcast %union.int_mv* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 4, i1 false), !tbaa.struct !57
  %30 = bitcast %union.int_mv* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  %arrayidx23 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %gm_mv, i32 0, i32 1
  %as_int24 = bitcast %union.int_mv* %arrayidx23 to i32*
  store i32 0, i32* %as_int24, align 4, !tbaa !41
  %31 = load %union.int_mv*, %union.int_mv** %global_mvs.addr, align 4, !tbaa !2
  %cmp25 = icmp ne %union.int_mv* %31, null
  br i1 %cmp25, label %if.then27, label %if.end31

if.then27:                                        ; preds = %if.then19
  %32 = load %union.int_mv*, %union.int_mv** %global_mvs.addr, align 4, !tbaa !2
  %33 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom28 = sext i8 %33 to i32
  %arrayidx29 = getelementptr inbounds %union.int_mv, %union.int_mv* %32, i32 %idxprom28
  %arrayidx30 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %gm_mv, i32 0, i32 0
  %34 = bitcast %union.int_mv* %arrayidx29 to i8*
  %35 = bitcast %union.int_mv* %arrayidx30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 4, i1 false), !tbaa.struct !57
  br label %if.end31

if.end31:                                         ; preds = %if.then27, %if.then19
  br label %if.end45

if.else32:                                        ; preds = %if.else
  %36 = bitcast [2 x i8]* %rf to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %36) #6
  %arraydecay = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %37 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  call void @av1_set_ref_frame(i8* %arraydecay, i8 signext %37)
  %arrayidx33 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %gm_mv, i32 0, i32 0
  %38 = bitcast %union.int_mv* %tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  %39 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %global_motion35 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %39, i32 0, i32 36
  %arrayidx36 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %40 = load i8, i8* %arrayidx36, align 1, !tbaa !41
  %idxprom37 = sext i8 %40 to i32
  %arrayidx38 = getelementptr inbounds [8 x %struct.WarpedMotionParams], [8 x %struct.WarpedMotionParams]* %global_motion35, i32 0, i32 %idxprom37
  %41 = load i32, i32* %allow_high_precision_mv, align 4, !tbaa !6
  %42 = load i8, i8* %bsize, align 1, !tbaa !41
  %43 = load i32, i32* %mi_col, align 4, !tbaa !6
  %44 = load i32, i32* %mi_row, align 4, !tbaa !6
  %45 = load i32, i32* %force_integer_mv, align 4, !tbaa !6
  call void @gm_get_motion_vector(%union.int_mv* sret align 4 %tmp34, %struct.WarpedMotionParams* %arrayidx38, i32 %41, i8 zeroext %42, i32 %43, i32 %44, i32 %45)
  %46 = bitcast %union.int_mv* %arrayidx33 to i8*
  %47 = bitcast %union.int_mv* %tmp34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 4, i1 false), !tbaa.struct !57
  %48 = bitcast %union.int_mv* %tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #6
  %arrayidx39 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %gm_mv, i32 0, i32 1
  %49 = bitcast %union.int_mv* %tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #6
  %50 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %global_motion41 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %50, i32 0, i32 36
  %arrayidx42 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 1
  %51 = load i8, i8* %arrayidx42, align 1, !tbaa !41
  %idxprom43 = sext i8 %51 to i32
  %arrayidx44 = getelementptr inbounds [8 x %struct.WarpedMotionParams], [8 x %struct.WarpedMotionParams]* %global_motion41, i32 0, i32 %idxprom43
  %52 = load i32, i32* %allow_high_precision_mv, align 4, !tbaa !6
  %53 = load i8, i8* %bsize, align 1, !tbaa !41
  %54 = load i32, i32* %mi_col, align 4, !tbaa !6
  %55 = load i32, i32* %mi_row, align 4, !tbaa !6
  %56 = load i32, i32* %force_integer_mv, align 4, !tbaa !6
  call void @gm_get_motion_vector(%union.int_mv* sret align 4 %tmp40, %struct.WarpedMotionParams* %arrayidx44, i32 %52, i8 zeroext %53, i32 %54, i32 %55, i32 %56)
  %57 = bitcast %union.int_mv* %arrayidx39 to i8*
  %58 = bitcast %union.int_mv* %tmp40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 4, i1 false), !tbaa.struct !57
  %59 = bitcast %union.int_mv* %tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast [2 x i8]* %rf to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %60) #6
  br label %if.end45

if.end45:                                         ; preds = %if.else32, %if.end31
  %61 = bitcast i32* %force_integer_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i32* %allow_high_precision_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.end
  %63 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %64 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %65 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %66 = load i8*, i8** %ref_mv_count.addr, align 4, !tbaa !2
  %67 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom47 = sext i8 %67 to i32
  %arrayidx48 = getelementptr inbounds i8, i8* %66, i32 %idxprom47
  %68 = load [8 x %struct.candidate_mv]*, [8 x %struct.candidate_mv]** %ref_mv_stack.addr, align 4, !tbaa !2
  %69 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom49 = sext i8 %69 to i32
  %arrayidx50 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %68, i32 %idxprom49
  %arraydecay51 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %arrayidx50, i32 0, i32 0
  %70 = load [8 x i16]*, [8 x i16]** %ref_mv_weight.addr, align 4, !tbaa !2
  %71 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom52 = sext i8 %71 to i32
  %arrayidx53 = getelementptr inbounds [8 x i16], [8 x i16]* %70, i32 %idxprom52
  %arraydecay54 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx53, i32 0, i32 0
  %72 = load [2 x %union.int_mv]*, [2 x %union.int_mv]** %mv_ref_list.addr, align 4, !tbaa !2
  %tobool55 = icmp ne [2 x %union.int_mv]* %72, null
  br i1 %tobool55, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end46
  %73 = load [2 x %union.int_mv]*, [2 x %union.int_mv]** %mv_ref_list.addr, align 4, !tbaa !2
  %74 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom56 = sext i8 %74 to i32
  %arrayidx57 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %73, i32 %idxprom56
  %arraydecay58 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx57, i32 0, i32 0
  br label %cond.end

cond.false:                                       ; preds = %if.end46
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %union.int_mv* [ %arraydecay58, %cond.true ], [ null, %cond.false ]
  %arraydecay59 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %gm_mv, i32 0, i32 0
  %75 = load i32, i32* %mi_row, align 4, !tbaa !6
  %76 = load i32, i32* %mi_col, align 4, !tbaa !6
  %77 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  call void @setup_ref_mv_list(%struct.AV1Common* %63, %struct.macroblockd* %64, i8 signext %65, i8* %arrayidx48, %struct.candidate_mv* %arraydecay51, i16* %arraydecay54, %union.int_mv* %cond, %union.int_mv* %arraydecay59, i32 %75, i32 %76, i16* %77)
  %78 = bitcast [2 x %union.int_mv]* %gm_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %78) #6
  %79 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #6
  %80 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @gm_get_motion_vector(%union.int_mv* noalias sret align 4 %agg.result, %struct.WarpedMotionParams* %gm, i32 %allow_hp, i8 zeroext %bsize, i32 %mi_col, i32 %mi_row, i32 %is_integer) #3 {
entry:
  %gm.addr = alloca %struct.WarpedMotionParams*, align 4
  %allow_hp.addr = alloca i32, align 4
  %bsize.addr = alloca i8, align 1
  %mi_col.addr = alloca i32, align 4
  %mi_row.addr = alloca i32, align 4
  %is_integer.addr = alloca i32, align 4
  %mat = alloca i32*, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %tx = alloca i32, align 4
  %ty = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %xc = alloca i32, align 4
  %yc = alloca i32, align 4
  store %struct.WarpedMotionParams* %gm, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !2
  store i32 %allow_hp, i32* %allow_hp.addr, align 4, !tbaa !6
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !41
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %is_integer, i32* %is_integer.addr, align 4, !tbaa !6
  %0 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !2
  %wmtype = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %0, i32 0, i32 5
  %1 = load i8, i8* %wmtype, align 4, !tbaa !59
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %as_int = bitcast %union.int_mv* %agg.result to i32*
  store i32 0, i32* %as_int, align 4, !tbaa !41
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !2
  %wmmat = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %3, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat, i32 0, i32 0
  store i32* %arraydecay, i32** %mat, align 4, !tbaa !2
  %4 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = bitcast i32* %tx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = bitcast i32* %ty to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !2
  %wmtype2 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %8, i32 0, i32 5
  %9 = load i8, i8* %wmtype2, align 4, !tbaa !59
  %conv3 = zext i8 %9 to i32
  %cmp4 = icmp eq i32 %conv3, 1
  br i1 %cmp4, label %if.then6, label %if.end17

if.then6:                                         ; preds = %if.end
  %10 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !2
  %wmmat7 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %10, i32 0, i32 0
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat7, i32 0, i32 0
  %11 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %shr = ashr i32 %11, 13
  %conv8 = trunc i32 %shr to i16
  %as_mv = bitcast %union.int_mv* %agg.result to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv, i32 0, i32 0
  store i16 %conv8, i16* %row, align 4, !tbaa !41
  %12 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !2
  %wmmat9 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %12, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat9, i32 0, i32 1
  %13 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %shr11 = ashr i32 %13, 13
  %conv12 = trunc i32 %shr11 to i16
  %as_mv13 = bitcast %union.int_mv* %agg.result to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv13, i32 0, i32 1
  store i16 %conv12, i16* %col, align 2, !tbaa !41
  %14 = load i32, i32* %is_integer.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.then6
  %as_mv15 = bitcast %union.int_mv* %agg.result to %struct.mv*
  call void @integer_mv_precision(%struct.mv* %as_mv15)
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.then6
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %if.end
  %15 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %16 = load i8, i8* %bsize.addr, align 1, !tbaa !41
  %call = call i32 @block_center_x(i32 %15, i8 zeroext %16)
  store i32 %call, i32* %x, align 4, !tbaa !6
  %17 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %18 = load i8, i8* %bsize.addr, align 1, !tbaa !41
  %call18 = call i32 @block_center_y(i32 %17, i8 zeroext %18)
  store i32 %call18, i32* %y, align 4, !tbaa !6
  %19 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !2
  %wmtype19 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %19, i32 0, i32 5
  %20 = load i8, i8* %wmtype19, align 4, !tbaa !59
  %conv20 = zext i8 %20 to i32
  %cmp21 = icmp eq i32 %conv20, 2
  br i1 %cmp21, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end17
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %if.end17
  %21 = bitcast i32* %xc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %22, i32 2
  %23 = load i32, i32* %arrayidx25, align 4, !tbaa !6
  %sub = sub nsw i32 %23, 65536
  %24 = load i32, i32* %x, align 4, !tbaa !6
  %mul = mul nsw i32 %sub, %24
  %25 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i32, i32* %25, i32 3
  %26 = load i32, i32* %arrayidx26, align 4, !tbaa !6
  %27 = load i32, i32* %y, align 4, !tbaa !6
  %mul27 = mul nsw i32 %26, %27
  %add = add nsw i32 %mul, %mul27
  %28 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i32, i32* %28, i32 0
  %29 = load i32, i32* %arrayidx28, align 4, !tbaa !6
  %add29 = add nsw i32 %add, %29
  store i32 %add29, i32* %xc, align 4, !tbaa !6
  %30 = bitcast i32* %yc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %31, i32 4
  %32 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %33 = load i32, i32* %x, align 4, !tbaa !6
  %mul31 = mul nsw i32 %32, %33
  %34 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %34, i32 5
  %35 = load i32, i32* %arrayidx32, align 4, !tbaa !6
  %sub33 = sub nsw i32 %35, 65536
  %36 = load i32, i32* %y, align 4, !tbaa !6
  %mul34 = mul nsw i32 %sub33, %36
  %add35 = add nsw i32 %mul31, %mul34
  %37 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %37, i32 1
  %38 = load i32, i32* %arrayidx36, align 4, !tbaa !6
  %add37 = add nsw i32 %add35, %38
  store i32 %add37, i32* %yc, align 4, !tbaa !6
  %39 = load i32, i32* %allow_hp.addr, align 4, !tbaa !6
  %40 = load i32, i32* %xc, align 4, !tbaa !6
  %call38 = call i32 @convert_to_trans_prec(i32 %39, i32 %40)
  store i32 %call38, i32* %tx, align 4, !tbaa !6
  %41 = load i32, i32* %allow_hp.addr, align 4, !tbaa !6
  %42 = load i32, i32* %yc, align 4, !tbaa !6
  %call39 = call i32 @convert_to_trans_prec(i32 %41, i32 %42)
  store i32 %call39, i32* %ty, align 4, !tbaa !6
  %43 = load i32, i32* %ty, align 4, !tbaa !6
  %conv40 = trunc i32 %43 to i16
  %as_mv41 = bitcast %union.int_mv* %agg.result to %struct.mv*
  %row42 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv41, i32 0, i32 0
  store i16 %conv40, i16* %row42, align 4, !tbaa !41
  %44 = load i32, i32* %tx, align 4, !tbaa !6
  %conv43 = trunc i32 %44 to i16
  %as_mv44 = bitcast %union.int_mv* %agg.result to %struct.mv*
  %col45 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv44, i32 0, i32 1
  store i16 %conv43, i16* %col45, align 2, !tbaa !41
  %45 = load i32, i32* %is_integer.addr, align 4, !tbaa !6
  %tobool46 = icmp ne i32 %45, 0
  br i1 %tobool46, label %if.then47, label %if.end49

if.then47:                                        ; preds = %if.end24
  %as_mv48 = bitcast %union.int_mv* %agg.result to %struct.mv*
  call void @integer_mv_precision(%struct.mv* %as_mv48)
  br label %if.end49

if.end49:                                         ; preds = %if.then47, %if.end24
  store i32 1, i32* %cleanup.dest.slot, align 4
  %46 = bitcast i32* %yc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  %47 = bitcast i32* %xc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end49, %if.end16
  %48 = bitcast i32* %ty to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #6
  %49 = bitcast i32* %tx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  %50 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  %51 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  %52 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  br label %return

return:                                           ; preds = %cleanup, %if.then
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint nounwind
define internal void @av1_set_ref_frame(i8* %rf, i8 signext %ref_frame_type) #3 {
entry:
  %rf.addr = alloca i8*, align 4
  %ref_frame_type.addr = alloca i8, align 1
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !2
  store i8 %ref_frame_type, i8* %ref_frame_type.addr, align 1, !tbaa !41
  %0 = load i8, i8* %ref_frame_type.addr, align 1, !tbaa !41
  %conv = sext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 8
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i8, i8* %ref_frame_type.addr, align 1, !tbaa !41
  %conv2 = sext i8 %1 to i32
  %sub = sub nsw i32 %conv2, 8
  %arrayidx = getelementptr inbounds [21 x [2 x i8]], [21 x [2 x i8]]* @ref_frame_map, i32 0, i32 %sub
  %arrayidx3 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx, i32 0, i32 0
  %2 = load i8, i8* %arrayidx3, align 2, !tbaa !41
  %3 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %3, i32 0
  store i8 %2, i8* %arrayidx4, align 1, !tbaa !41
  %4 = load i8, i8* %ref_frame_type.addr, align 1, !tbaa !41
  %conv5 = sext i8 %4 to i32
  %sub6 = sub nsw i32 %conv5, 8
  %arrayidx7 = getelementptr inbounds [21 x [2 x i8]], [21 x [2 x i8]]* @ref_frame_map, i32 0, i32 %sub6
  %arrayidx8 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx7, i32 0, i32 1
  %5 = load i8, i8* %arrayidx8, align 1, !tbaa !41
  %6 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %6, i32 1
  store i8 %5, i8* %arrayidx9, align 1, !tbaa !41
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = load i8, i8* %ref_frame_type.addr, align 1, !tbaa !41
  %8 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %8, i32 0
  store i8 %7, i8* %arrayidx10, align 1, !tbaa !41
  %9 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %9, i32 1
  store i8 -1, i8* %arrayidx11, align 1, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @setup_ref_mv_list(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i8 signext %ref_frame, i8* %refmv_count, %struct.candidate_mv* %ref_mv_stack, i16* %ref_mv_weight, %union.int_mv* %mv_ref_list, %union.int_mv* %gm_mv_candidates, i32 %mi_row, i32 %mi_col, i16* %mode_context) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_frame.addr = alloca i8, align 1
  %refmv_count.addr = alloca i8*, align 4
  %ref_mv_stack.addr = alloca %struct.candidate_mv*, align 4
  %ref_mv_weight.addr = alloca i16*, align 4
  %mv_ref_list.addr = alloca %union.int_mv*, align 4
  %gm_mv_candidates.addr = alloca %union.int_mv*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %mode_context.addr = alloca i16*, align 4
  %bs = alloca i32, align 4
  %has_tr = alloca i32, align 4
  %rf = alloca [2 x i8], align 1
  %tile = alloca %struct.TileInfo*, align 4
  %max_row_offset = alloca i32, align 4
  %max_col_offset = alloca i32, align 4
  %row_adj = alloca i32, align 4
  %col_adj = alloca i32, align 4
  %processed_rows = alloca i32, align 4
  %processed_cols = alloca i32, align 4
  %col_match_count = alloca i8, align 1
  %row_match_count = alloca i8, align 1
  %newmv_count = alloca i8, align 1
  %nearest_match = alloca i8, align 1
  %nearest_refmv_count = alloca i8, align 1
  %idx = alloca i32, align 4
  %is_available = alloca i32, align 4
  %voffset = alloca i32, align 4
  %hoffset = alloca i32, align 4
  %blk_row_end = alloca i32, align 4
  %blk_col_end = alloca i32, align 4
  %tpl_sample_pos = alloca [3 x [2 x i32]], align 16
  %allow_extension = alloca i32, align 4
  %step_h = alloca i32, align 4
  %step_w = alloca i32, align 4
  %blk_row = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %blk_col = alloca i32, align 4
  %ret = alloca i32, align 4
  %i = alloca i32, align 4
  %blk_row223 = alloca i32, align 4
  %blk_col226 = alloca i32, align 4
  %dummy_newmv_count = alloca i8, align 1
  %idx241 = alloca i32, align 4
  %row_offset = alloca i32, align 4
  %col_offset = alloca i32, align 4
  %ref_match_count = alloca i8, align 1
  %len = alloca i32, align 4
  %nr_len = alloca i32, align 4
  %idx375 = alloca i32, align 4
  %tmp_mv = alloca %struct.candidate_mv, align 4
  %tmp_ref_mv_weight = alloca i16, align 2
  %nr_len412 = alloca i32, align 4
  %idx414 = alloca i32, align 4
  %tmp_mv430 = alloca %struct.candidate_mv, align 4
  %tmp_ref_mv_weight433 = alloca i16, align 2
  %mi_width = alloca i32, align 4
  %mi_height = alloca i32, align 4
  %mi_size = alloca i32, align 4
  %ref_id = alloca [2 x [2 x %union.int_mv]], align 16
  %ref_diff = alloca [2 x [2 x %union.int_mv]], align 16
  %ref_id_count = alloca [2 x i32], align 4
  %ref_diff_count = alloca [2 x i32], align 4
  %idx510 = alloca i32, align 4
  %candidate = alloca %struct.MB_MODE_INFO*, align 4
  %idx536 = alloca i32, align 4
  %candidate548 = alloca %struct.MB_MODE_INFO*, align 4
  %comp_list = alloca [2 x [2 x %union.int_mv]], align 16
  %idx565 = alloca i32, align 4
  %comp_idx = alloca i32, align 4
  %list_idx = alloca i32, align 4
  %list_idx591 = alloca i32, align 4
  %idx669 = alloca i32, align 4
  %idx694 = alloca i32, align 4
  %idx723 = alloca i32, align 4
  %candidate739 = alloca %struct.MB_MODE_INFO*, align 4
  %idx752 = alloca i32, align 4
  %candidate768 = alloca %struct.MB_MODE_INFO*, align 4
  %idx781 = alloca i32, align 4
  %idx804 = alloca i32, align 4
  %idx819 = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !41
  store i8* %refmv_count, i8** %refmv_count.addr, align 4, !tbaa !2
  store %struct.candidate_mv* %ref_mv_stack, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  store i16* %ref_mv_weight, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  store %union.int_mv* %mv_ref_list, %union.int_mv** %mv_ref_list.addr, align 4, !tbaa !2
  store %union.int_mv* %gm_mv_candidates, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i16* %mode_context, i16** %mode_context.addr, align 4, !tbaa !2
  %0 = bitcast i32* %bs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 33
  %2 = load i8, i8* %width, align 4, !tbaa !60
  %conv = zext i8 %2 to i32
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 34
  %4 = load i8, i8* %height, align 1, !tbaa !61
  %conv1 = zext i8 %4 to i32
  %cmp = icmp sgt i32 %conv, %conv1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width3 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 33
  %6 = load i8, i8* %width3, align 4, !tbaa !60
  %conv4 = zext i8 %6 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height5 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 34
  %8 = load i8, i8* %height5, align 1, !tbaa !61
  %conv6 = zext i8 %8 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv4, %cond.true ], [ %conv6, %cond.false ]
  store i32 %cond, i32* %bs, align 4, !tbaa !6
  %9 = bitcast i32* %has_tr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %12 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %13 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %14 = load i32, i32* %bs, align 4, !tbaa !6
  %call = call i32 @has_top_right(%struct.AV1Common* %10, %struct.macroblockd* %11, i32 %12, i32 %13, i32 %14)
  store i32 %call, i32* %has_tr, align 4, !tbaa !6
  %15 = bitcast [2 x i8]* %rf to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %15) #6
  %16 = bitcast %struct.TileInfo** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tile7 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %17, i32 0, i32 5
  store %struct.TileInfo* %tile7, %struct.TileInfo** %tile, align 4, !tbaa !2
  %18 = bitcast i32* %max_row_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  store i32 0, i32* %max_row_offset, align 4, !tbaa !6
  %19 = bitcast i32* %max_col_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  store i32 0, i32* %max_col_offset, align 4, !tbaa !6
  %20 = bitcast i32* %row_adj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %21, i32 0, i32 34
  %22 = load i8, i8* %height8, align 1, !tbaa !61
  %conv9 = zext i8 %22 to i32
  %23 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 3), align 1, !tbaa !41
  %conv10 = zext i8 %23 to i32
  %cmp11 = icmp slt i32 %conv9, %conv10
  br i1 %cmp11, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %cond.end
  %24 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %and = and i32 %24, 1
  %tobool = icmp ne i32 %and, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %cond.end
  %25 = phi i1 [ false, %cond.end ], [ %tobool, %land.rhs ]
  %land.ext = zext i1 %25 to i32
  store i32 %land.ext, i32* %row_adj, align 4, !tbaa !6
  %26 = bitcast i32* %col_adj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width13 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %27, i32 0, i32 33
  %28 = load i8, i8* %width13, align 4, !tbaa !60
  %conv14 = zext i8 %28 to i32
  %29 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 3), align 1, !tbaa !41
  %conv15 = zext i8 %29 to i32
  %cmp16 = icmp slt i32 %conv14, %conv15
  br i1 %cmp16, label %land.rhs18, label %land.end21

land.rhs18:                                       ; preds = %land.end
  %30 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %and19 = and i32 %30, 1
  %tobool20 = icmp ne i32 %and19, 0
  br label %land.end21

land.end21:                                       ; preds = %land.rhs18, %land.end
  %31 = phi i1 [ false, %land.end ], [ %tobool20, %land.rhs18 ]
  %land.ext22 = zext i1 %31 to i32
  store i32 %land.ext22, i32* %col_adj, align 4, !tbaa !6
  %32 = bitcast i32* %processed_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  store i32 0, i32* %processed_rows, align 4, !tbaa !6
  %33 = bitcast i32* %processed_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  store i32 0, i32* %processed_cols, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %34 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  call void @av1_set_ref_frame(i8* %arraydecay, i8 signext %34)
  %35 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %36 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom = sext i8 %36 to i32
  %arrayidx = getelementptr inbounds i16, i16* %35, i32 %idxprom
  store i16 0, i16* %arrayidx, align 2, !tbaa !58
  %37 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  store i8 0, i8* %37, align 1, !tbaa !41
  %38 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %38, i32 0, i32 7
  %39 = load i8, i8* %up_available, align 8, !tbaa !62, !range !55
  %tobool23 = trunc i8 %39 to i1
  br i1 %tobool23, label %if.then, label %if.end32

if.then:                                          ; preds = %land.end21
  %40 = load i32, i32* %row_adj, align 4, !tbaa !6
  %add = add nsw i32 -6, %40
  store i32 %add, i32* %max_row_offset, align 4, !tbaa !6
  %41 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height24 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %41, i32 0, i32 34
  %42 = load i8, i8* %height24, align 1, !tbaa !61
  %conv25 = zext i8 %42 to i32
  %43 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 3), align 1, !tbaa !41
  %conv26 = zext i8 %43 to i32
  %cmp27 = icmp slt i32 %conv25, %conv26
  br i1 %cmp27, label %if.then29, label %if.end

if.then29:                                        ; preds = %if.then
  %44 = load i32, i32* %row_adj, align 4, !tbaa !6
  %add30 = add nsw i32 -4, %44
  store i32 %add30, i32* %max_row_offset, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then29, %if.then
  %45 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !2
  %46 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %47 = load i32, i32* %max_row_offset, align 4, !tbaa !6
  %call31 = call i32 @find_valid_row_offset(%struct.TileInfo* %45, i32 %46, i32 %47)
  store i32 %call31, i32* %max_row_offset, align 4, !tbaa !6
  br label %if.end32

if.end32:                                         ; preds = %if.end, %land.end21
  %48 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %48, i32 0, i32 8
  %49 = load i8, i8* %left_available, align 1, !tbaa !63, !range !55
  %tobool33 = trunc i8 %49 to i1
  br i1 %tobool33, label %if.then34, label %if.end45

if.then34:                                        ; preds = %if.end32
  %50 = load i32, i32* %col_adj, align 4, !tbaa !6
  %add35 = add nsw i32 -6, %50
  store i32 %add35, i32* %max_col_offset, align 4, !tbaa !6
  %51 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width36 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %51, i32 0, i32 33
  %52 = load i8, i8* %width36, align 4, !tbaa !60
  %conv37 = zext i8 %52 to i32
  %53 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 3), align 1, !tbaa !41
  %conv38 = zext i8 %53 to i32
  %cmp39 = icmp slt i32 %conv37, %conv38
  br i1 %cmp39, label %if.then41, label %if.end43

if.then41:                                        ; preds = %if.then34
  %54 = load i32, i32* %col_adj, align 4, !tbaa !6
  %add42 = add nsw i32 -4, %54
  store i32 %add42, i32* %max_col_offset, align 4, !tbaa !6
  br label %if.end43

if.end43:                                         ; preds = %if.then41, %if.then34
  %55 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !2
  %56 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %57 = load i32, i32* %max_col_offset, align 4, !tbaa !6
  %call44 = call i32 @find_valid_col_offset(%struct.TileInfo* %55, i32 %56, i32 %57)
  store i32 %call44, i32* %max_col_offset, align 4, !tbaa !6
  br label %if.end45

if.end45:                                         ; preds = %if.end43, %if.end32
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %col_match_count) #6
  store i8 0, i8* %col_match_count, align 1, !tbaa !41
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %row_match_count) #6
  store i8 0, i8* %row_match_count, align 1, !tbaa !41
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %newmv_count) #6
  store i8 0, i8* %newmv_count, align 1, !tbaa !41
  %58 = load i32, i32* %max_row_offset, align 4, !tbaa !6
  %call46 = call i32 @abs(i32 %58) #7
  %cmp47 = icmp sge i32 %call46, 1
  br i1 %cmp47, label %if.then49, label %if.end51

if.then49:                                        ; preds = %if.end45
  %59 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %60 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %61 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %arraydecay50 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %62 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %63 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %64 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %65 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %66 = load i32, i32* %max_row_offset, align 4, !tbaa !6
  call void @scan_row_mbmi(%struct.AV1Common* %59, %struct.macroblockd* %60, i32 %61, i8* %arraydecay50, i32 -1, %struct.candidate_mv* %62, i16* %63, i8* %64, i8* %row_match_count, i8* %newmv_count, %union.int_mv* %65, i32 %66, i32* %processed_rows)
  br label %if.end51

if.end51:                                         ; preds = %if.then49, %if.end45
  %67 = load i32, i32* %max_col_offset, align 4, !tbaa !6
  %call52 = call i32 @abs(i32 %67) #7
  %cmp53 = icmp sge i32 %call52, 1
  br i1 %cmp53, label %if.then55, label %if.end57

if.then55:                                        ; preds = %if.end51
  %68 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %69 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %70 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %arraydecay56 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %71 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %72 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %73 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %74 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %75 = load i32, i32* %max_col_offset, align 4, !tbaa !6
  call void @scan_col_mbmi(%struct.AV1Common* %68, %struct.macroblockd* %69, i32 %70, i8* %arraydecay56, i32 -1, %struct.candidate_mv* %71, i16* %72, i8* %73, i8* %col_match_count, i8* %newmv_count, %union.int_mv* %74, i32 %75, i32* %processed_cols)
  br label %if.end57

if.end57:                                         ; preds = %if.then55, %if.end51
  %76 = load i32, i32* %has_tr, align 4, !tbaa !6
  %tobool58 = icmp ne i32 %76, 0
  br i1 %tobool58, label %if.then59, label %if.end63

if.then59:                                        ; preds = %if.end57
  %77 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %78 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %79 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %80 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %arraydecay60 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %81 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width61 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %81, i32 0, i32 33
  %82 = load i8, i8* %width61, align 4, !tbaa !60
  %conv62 = zext i8 %82 to i32
  %83 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %84 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %85 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %86 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  call void @scan_blk_mbmi(%struct.AV1Common* %77, %struct.macroblockd* %78, i32 %79, i32 %80, i8* %arraydecay60, i32 -1, i32 %conv62, %struct.candidate_mv* %83, i16* %84, i8* %row_match_count, i8* %newmv_count, %union.int_mv* %85, i8* %86)
  br label %if.end63

if.end63:                                         ; preds = %if.then59, %if.end57
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %nearest_match) #6
  %87 = load i8, i8* %row_match_count, align 1, !tbaa !41
  %conv64 = zext i8 %87 to i32
  %cmp65 = icmp sgt i32 %conv64, 0
  %conv66 = zext i1 %cmp65 to i32
  %88 = load i8, i8* %col_match_count, align 1, !tbaa !41
  %conv67 = zext i8 %88 to i32
  %cmp68 = icmp sgt i32 %conv67, 0
  %conv69 = zext i1 %cmp68 to i32
  %add70 = add nsw i32 %conv66, %conv69
  %conv71 = trunc i32 %add70 to i8
  store i8 %conv71, i8* %nearest_match, align 1, !tbaa !41
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %nearest_refmv_count) #6
  %89 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %90 = load i8, i8* %89, align 1, !tbaa !41
  store i8 %90, i8* %nearest_refmv_count, align 1, !tbaa !41
  %91 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #6
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end63
  %92 = load i32, i32* %idx, align 4, !tbaa !6
  %93 = load i8, i8* %nearest_refmv_count, align 1, !tbaa !41
  %conv72 = zext i8 %93 to i32
  %cmp73 = icmp slt i32 %92, %conv72
  br i1 %cmp73, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %94 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %95 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %96 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds i16, i16* %95, i32 %96
  %97 = load i16, i16* %arrayidx75, align 2, !tbaa !58
  %conv76 = zext i16 %97 to i32
  %add77 = add nsw i32 %conv76, 640
  %conv78 = trunc i32 %add77 to i16
  store i16 %conv78, i16* %arrayidx75, align 2, !tbaa !58
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %98 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %98, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %99 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %99, i32 0, i32 21
  %allow_ref_frame_mvs = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 6
  %100 = load i8, i8* %allow_ref_frame_mvs, align 2, !tbaa !64, !range !55
  %tobool79 = trunc i8 %100 to i1
  br i1 %tobool79, label %if.then80, label %if.end239

if.then80:                                        ; preds = %for.end
  %101 = bitcast i32* %is_available to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #6
  store i32 0, i32* %is_available, align 4, !tbaa !6
  %102 = bitcast i32* %voffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #6
  %103 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 3), align 1, !tbaa !41
  %conv81 = zext i8 %103 to i32
  %104 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height82 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %104, i32 0, i32 34
  %105 = load i8, i8* %height82, align 1, !tbaa !61
  %conv83 = zext i8 %105 to i32
  %cmp84 = icmp sgt i32 %conv81, %conv83
  br i1 %cmp84, label %cond.true86, label %cond.false88

cond.true86:                                      ; preds = %if.then80
  %106 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 3), align 1, !tbaa !41
  %conv87 = zext i8 %106 to i32
  br label %cond.end91

cond.false88:                                     ; preds = %if.then80
  %107 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height89 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %107, i32 0, i32 34
  %108 = load i8, i8* %height89, align 1, !tbaa !61
  %conv90 = zext i8 %108 to i32
  br label %cond.end91

cond.end91:                                       ; preds = %cond.false88, %cond.true86
  %cond92 = phi i32 [ %conv87, %cond.true86 ], [ %conv90, %cond.false88 ]
  store i32 %cond92, i32* %voffset, align 4, !tbaa !6
  %109 = bitcast i32* %hoffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #6
  %110 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 3), align 1, !tbaa !41
  %conv93 = zext i8 %110 to i32
  %111 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width94 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %111, i32 0, i32 33
  %112 = load i8, i8* %width94, align 4, !tbaa !60
  %conv95 = zext i8 %112 to i32
  %cmp96 = icmp sgt i32 %conv93, %conv95
  br i1 %cmp96, label %cond.true98, label %cond.false100

cond.true98:                                      ; preds = %cond.end91
  %113 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 3), align 1, !tbaa !41
  %conv99 = zext i8 %113 to i32
  br label %cond.end103

cond.false100:                                    ; preds = %cond.end91
  %114 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width101 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %114, i32 0, i32 33
  %115 = load i8, i8* %width101, align 4, !tbaa !60
  %conv102 = zext i8 %115 to i32
  br label %cond.end103

cond.end103:                                      ; preds = %cond.false100, %cond.true98
  %cond104 = phi i32 [ %conv99, %cond.true98 ], [ %conv102, %cond.false100 ]
  store i32 %cond104, i32* %hoffset, align 4, !tbaa !6
  %116 = bitcast i32* %blk_row_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #6
  %117 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height105 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %117, i32 0, i32 34
  %118 = load i8, i8* %height105, align 1, !tbaa !61
  %conv106 = zext i8 %118 to i32
  %119 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !41
  %conv107 = zext i8 %119 to i32
  %cmp108 = icmp slt i32 %conv106, %conv107
  br i1 %cmp108, label %cond.true110, label %cond.false113

cond.true110:                                     ; preds = %cond.end103
  %120 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height111 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %120, i32 0, i32 34
  %121 = load i8, i8* %height111, align 1, !tbaa !61
  %conv112 = zext i8 %121 to i32
  br label %cond.end115

cond.false113:                                    ; preds = %cond.end103
  %122 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !41
  %conv114 = zext i8 %122 to i32
  br label %cond.end115

cond.end115:                                      ; preds = %cond.false113, %cond.true110
  %cond116 = phi i32 [ %conv112, %cond.true110 ], [ %conv114, %cond.false113 ]
  store i32 %cond116, i32* %blk_row_end, align 4, !tbaa !6
  %123 = bitcast i32* %blk_col_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #6
  %124 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width117 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %124, i32 0, i32 33
  %125 = load i8, i8* %width117, align 4, !tbaa !60
  %conv118 = zext i8 %125 to i32
  %126 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv119 = zext i8 %126 to i32
  %cmp120 = icmp slt i32 %conv118, %conv119
  br i1 %cmp120, label %cond.true122, label %cond.false125

cond.true122:                                     ; preds = %cond.end115
  %127 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width123 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %127, i32 0, i32 33
  %128 = load i8, i8* %width123, align 4, !tbaa !60
  %conv124 = zext i8 %128 to i32
  br label %cond.end127

cond.false125:                                    ; preds = %cond.end115
  %129 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv126 = zext i8 %129 to i32
  br label %cond.end127

cond.end127:                                      ; preds = %cond.false125, %cond.true122
  %cond128 = phi i32 [ %conv124, %cond.true122 ], [ %conv126, %cond.false125 ]
  store i32 %cond128, i32* %blk_col_end, align 4, !tbaa !6
  %130 = bitcast [3 x [2 x i32]]* %tpl_sample_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %130) #6
  %arrayinit.begin = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %tpl_sample_pos, i32 0, i32 0
  %arrayinit.begin129 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayinit.begin, i32 0, i32 0
  %131 = load i32, i32* %voffset, align 4, !tbaa !6
  store i32 %131, i32* %arrayinit.begin129, align 4, !tbaa !6
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin129, i32 1
  store i32 -2, i32* %arrayinit.element, align 4, !tbaa !6
  %arrayinit.element130 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayinit.begin, i32 1
  %arrayinit.begin131 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayinit.element130, i32 0, i32 0
  %132 = load i32, i32* %voffset, align 4, !tbaa !6
  store i32 %132, i32* %arrayinit.begin131, align 4, !tbaa !6
  %arrayinit.element132 = getelementptr inbounds i32, i32* %arrayinit.begin131, i32 1
  %133 = load i32, i32* %hoffset, align 4, !tbaa !6
  store i32 %133, i32* %arrayinit.element132, align 4, !tbaa !6
  %arrayinit.element133 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayinit.element130, i32 1
  %arrayinit.begin134 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayinit.element133, i32 0, i32 0
  %134 = load i32, i32* %voffset, align 4, !tbaa !6
  %sub = sub nsw i32 %134, 2
  store i32 %sub, i32* %arrayinit.begin134, align 4, !tbaa !6
  %arrayinit.element135 = getelementptr inbounds i32, i32* %arrayinit.begin134, i32 1
  %135 = load i32, i32* %hoffset, align 4, !tbaa !6
  store i32 %135, i32* %arrayinit.element135, align 4, !tbaa !6
  %136 = bitcast i32* %allow_extension to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #6
  %137 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height136 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %137, i32 0, i32 34
  %138 = load i8, i8* %height136, align 1, !tbaa !61
  %conv137 = zext i8 %138 to i32
  %139 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 3), align 1, !tbaa !41
  %conv138 = zext i8 %139 to i32
  %cmp139 = icmp sge i32 %conv137, %conv138
  br i1 %cmp139, label %land.lhs.true, label %land.end158

land.lhs.true:                                    ; preds = %cond.end127
  %140 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height141 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %140, i32 0, i32 34
  %141 = load i8, i8* %height141, align 1, !tbaa !61
  %conv142 = zext i8 %141 to i32
  %142 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !41
  %conv143 = zext i8 %142 to i32
  %cmp144 = icmp slt i32 %conv142, %conv143
  br i1 %cmp144, label %land.lhs.true146, label %land.end158

land.lhs.true146:                                 ; preds = %land.lhs.true
  %143 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width147 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %143, i32 0, i32 33
  %144 = load i8, i8* %width147, align 4, !tbaa !60
  %conv148 = zext i8 %144 to i32
  %145 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 3), align 1, !tbaa !41
  %conv149 = zext i8 %145 to i32
  %cmp150 = icmp sge i32 %conv148, %conv149
  br i1 %cmp150, label %land.rhs152, label %land.end158

land.rhs152:                                      ; preds = %land.lhs.true146
  %146 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width153 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %146, i32 0, i32 33
  %147 = load i8, i8* %width153, align 4, !tbaa !60
  %conv154 = zext i8 %147 to i32
  %148 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv155 = zext i8 %148 to i32
  %cmp156 = icmp slt i32 %conv154, %conv155
  br label %land.end158

land.end158:                                      ; preds = %land.rhs152, %land.lhs.true146, %land.lhs.true, %cond.end127
  %149 = phi i1 [ false, %land.lhs.true146 ], [ false, %land.lhs.true ], [ false, %cond.end127 ], [ %cmp156, %land.rhs152 ]
  %land.ext159 = zext i1 %149 to i32
  store i32 %land.ext159, i32* %allow_extension, align 4, !tbaa !6
  %150 = bitcast i32* %step_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #6
  %151 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height160 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %151, i32 0, i32 34
  %152 = load i8, i8* %height160, align 1, !tbaa !61
  %conv161 = zext i8 %152 to i32
  %153 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !41
  %conv162 = zext i8 %153 to i32
  %cmp163 = icmp sge i32 %conv161, %conv162
  br i1 %cmp163, label %cond.true165, label %cond.false167

cond.true165:                                     ; preds = %land.end158
  %154 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 6), align 2, !tbaa !41
  %conv166 = zext i8 %154 to i32
  br label %cond.end169

cond.false167:                                    ; preds = %land.end158
  %155 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 3), align 1, !tbaa !41
  %conv168 = zext i8 %155 to i32
  br label %cond.end169

cond.end169:                                      ; preds = %cond.false167, %cond.true165
  %cond170 = phi i32 [ %conv166, %cond.true165 ], [ %conv168, %cond.false167 ]
  store i32 %cond170, i32* %step_h, align 4, !tbaa !6
  %156 = bitcast i32* %step_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %156) #6
  %157 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width171 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %157, i32 0, i32 33
  %158 = load i8, i8* %width171, align 4, !tbaa !60
  %conv172 = zext i8 %158 to i32
  %159 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv173 = zext i8 %159 to i32
  %cmp174 = icmp sge i32 %conv172, %conv173
  br i1 %cmp174, label %cond.true176, label %cond.false178

cond.true176:                                     ; preds = %cond.end169
  %160 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 6), align 2, !tbaa !41
  %conv177 = zext i8 %160 to i32
  br label %cond.end180

cond.false178:                                    ; preds = %cond.end169
  %161 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 3), align 1, !tbaa !41
  %conv179 = zext i8 %161 to i32
  br label %cond.end180

cond.end180:                                      ; preds = %cond.false178, %cond.true176
  %cond181 = phi i32 [ %conv177, %cond.true176 ], [ %conv179, %cond.false178 ]
  store i32 %cond181, i32* %step_w, align 4, !tbaa !6
  %162 = bitcast i32* %blk_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #6
  store i32 0, i32* %blk_row, align 4, !tbaa !6
  br label %for.cond182

for.cond182:                                      ; preds = %for.inc203, %cond.end180
  %163 = load i32, i32* %blk_row, align 4, !tbaa !6
  %164 = load i32, i32* %blk_row_end, align 4, !tbaa !6
  %cmp183 = icmp slt i32 %163, %164
  br i1 %cmp183, label %for.body186, label %for.cond.cleanup185

for.cond.cleanup185:                              ; preds = %for.cond182
  store i32 5, i32* %cleanup.dest.slot, align 4
  %165 = bitcast i32* %blk_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #6
  br label %for.end205

for.body186:                                      ; preds = %for.cond182
  %166 = bitcast i32* %blk_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %166) #6
  store i32 0, i32* %blk_col, align 4, !tbaa !6
  br label %for.cond187

for.cond187:                                      ; preds = %for.inc200, %for.body186
  %167 = load i32, i32* %blk_col, align 4, !tbaa !6
  %168 = load i32, i32* %blk_col_end, align 4, !tbaa !6
  %cmp188 = icmp slt i32 %167, %168
  br i1 %cmp188, label %for.body191, label %for.cond.cleanup190

for.cond.cleanup190:                              ; preds = %for.cond187
  store i32 8, i32* %cleanup.dest.slot, align 4
  %169 = bitcast i32* %blk_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #6
  br label %for.end202

for.body191:                                      ; preds = %for.cond187
  %170 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %170) #6
  %171 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %172 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %173 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %174 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %175 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %176 = load i32, i32* %blk_row, align 4, !tbaa !6
  %177 = load i32, i32* %blk_col, align 4, !tbaa !6
  %178 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %179 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %180 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %181 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %182 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %call192 = call i32 @add_tpl_ref_mv(%struct.AV1Common* %171, %struct.macroblockd* %172, i32 %173, i32 %174, i8 signext %175, i32 %176, i32 %177, %union.int_mv* %178, i8* %179, %struct.candidate_mv* %180, i16* %181, i16* %182)
  store i32 %call192, i32* %ret, align 4, !tbaa !6
  %183 = load i32, i32* %blk_row, align 4, !tbaa !6
  %cmp193 = icmp eq i32 %183, 0
  br i1 %cmp193, label %land.lhs.true195, label %if.end199

land.lhs.true195:                                 ; preds = %for.body191
  %184 = load i32, i32* %blk_col, align 4, !tbaa !6
  %cmp196 = icmp eq i32 %184, 0
  br i1 %cmp196, label %if.then198, label %if.end199

if.then198:                                       ; preds = %land.lhs.true195
  %185 = load i32, i32* %ret, align 4, !tbaa !6
  store i32 %185, i32* %is_available, align 4, !tbaa !6
  br label %if.end199

if.end199:                                        ; preds = %if.then198, %land.lhs.true195, %for.body191
  %186 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #6
  br label %for.inc200

for.inc200:                                       ; preds = %if.end199
  %187 = load i32, i32* %step_w, align 4, !tbaa !6
  %188 = load i32, i32* %blk_col, align 4, !tbaa !6
  %add201 = add nsw i32 %188, %187
  store i32 %add201, i32* %blk_col, align 4, !tbaa !6
  br label %for.cond187

for.end202:                                       ; preds = %for.cond.cleanup190
  br label %for.inc203

for.inc203:                                       ; preds = %for.end202
  %189 = load i32, i32* %step_h, align 4, !tbaa !6
  %190 = load i32, i32* %blk_row, align 4, !tbaa !6
  %add204 = add nsw i32 %190, %189
  store i32 %add204, i32* %blk_row, align 4, !tbaa !6
  br label %for.cond182

for.end205:                                       ; preds = %for.cond.cleanup185
  %191 = load i32, i32* %is_available, align 4, !tbaa !6
  %cmp206 = icmp eq i32 %191, 0
  br i1 %cmp206, label %if.then208, label %if.end213

if.then208:                                       ; preds = %for.end205
  %192 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %193 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom209 = sext i8 %193 to i32
  %arrayidx210 = getelementptr inbounds i16, i16* %192, i32 %idxprom209
  %194 = load i16, i16* %arrayidx210, align 2, !tbaa !58
  %conv211 = sext i16 %194 to i32
  %or = or i32 %conv211, 8
  %conv212 = trunc i32 %or to i16
  store i16 %conv212, i16* %arrayidx210, align 2, !tbaa !58
  br label %if.end213

if.end213:                                        ; preds = %if.then208, %for.end205
  %195 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %195) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond214

for.cond214:                                      ; preds = %for.inc235, %if.end213
  %196 = load i32, i32* %i, align 4, !tbaa !6
  %cmp215 = icmp slt i32 %196, 3
  br i1 %cmp215, label %land.rhs217, label %land.end219

land.rhs217:                                      ; preds = %for.cond214
  %197 = load i32, i32* %allow_extension, align 4, !tbaa !6
  %tobool218 = icmp ne i32 %197, 0
  br label %land.end219

land.end219:                                      ; preds = %land.rhs217, %for.cond214
  %198 = phi i1 [ false, %for.cond214 ], [ %tobool218, %land.rhs217 ]
  br i1 %198, label %for.body222, label %for.cond.cleanup221

for.cond.cleanup221:                              ; preds = %land.end219
  store i32 11, i32* %cleanup.dest.slot, align 4
  %199 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #6
  br label %for.end238

for.body222:                                      ; preds = %land.end219
  %200 = bitcast i32* %blk_row223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #6
  %201 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx224 = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %tpl_sample_pos, i32 0, i32 %201
  %arrayidx225 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx224, i32 0, i32 0
  %202 = load i32, i32* %arrayidx225, align 8, !tbaa !6
  store i32 %202, i32* %blk_row223, align 4, !tbaa !6
  %203 = bitcast i32* %blk_col226 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %203) #6
  %204 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx227 = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %tpl_sample_pos, i32 0, i32 %204
  %arrayidx228 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx227, i32 0, i32 1
  %205 = load i32, i32* %arrayidx228, align 4, !tbaa !6
  store i32 %205, i32* %blk_col226, align 4, !tbaa !6
  %206 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %207 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %208 = load i32, i32* %blk_row223, align 4, !tbaa !6
  %209 = load i32, i32* %blk_col226, align 4, !tbaa !6
  %call229 = call i32 @check_sb_border(i32 %206, i32 %207, i32 %208, i32 %209)
  %tobool230 = icmp ne i32 %call229, 0
  br i1 %tobool230, label %if.end232, label %if.then231

if.then231:                                       ; preds = %for.body222
  store i32 13, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end232:                                        ; preds = %for.body222
  %210 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %211 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %212 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %213 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %214 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %215 = load i32, i32* %blk_row223, align 4, !tbaa !6
  %216 = load i32, i32* %blk_col226, align 4, !tbaa !6
  %217 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %218 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %219 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %220 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %221 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %call233 = call i32 @add_tpl_ref_mv(%struct.AV1Common* %210, %struct.macroblockd* %211, i32 %212, i32 %213, i8 signext %214, i32 %215, i32 %216, %union.int_mv* %217, i8* %218, %struct.candidate_mv* %219, i16* %220, i16* %221)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end232, %if.then231
  %222 = bitcast i32* %blk_col226 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #6
  %223 = bitcast i32* %blk_row223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 13, label %for.inc235
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc235

for.inc235:                                       ; preds = %cleanup.cont, %cleanup
  %224 = load i32, i32* %i, align 4, !tbaa !6
  %inc236 = add nsw i32 %224, 1
  store i32 %inc236, i32* %i, align 4, !tbaa !6
  br label %for.cond214

for.end238:                                       ; preds = %for.cond.cleanup221
  %225 = bitcast i32* %step_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #6
  %226 = bitcast i32* %step_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #6
  %227 = bitcast i32* %allow_extension to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #6
  %228 = bitcast [3 x [2 x i32]]* %tpl_sample_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %228) #6
  %229 = bitcast i32* %blk_col_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #6
  %230 = bitcast i32* %blk_row_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #6
  %231 = bitcast i32* %hoffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #6
  %232 = bitcast i32* %voffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #6
  %233 = bitcast i32* %is_available to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #6
  br label %if.end239

if.end239:                                        ; preds = %for.end238, %for.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %dummy_newmv_count) #6
  store i8 0, i8* %dummy_newmv_count, align 1, !tbaa !41
  %234 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %235 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %236 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %237 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %arraydecay240 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %238 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %239 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %240 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %241 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  call void @scan_blk_mbmi(%struct.AV1Common* %234, %struct.macroblockd* %235, i32 %236, i32 %237, i8* %arraydecay240, i32 -1, i32 -1, %struct.candidate_mv* %238, i16* %239, i8* %row_match_count, i8* %dummy_newmv_count, %union.int_mv* %240, i8* %241)
  %242 = bitcast i32* %idx241 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %242) #6
  store i32 2, i32* %idx241, align 4, !tbaa !6
  br label %for.cond242

for.cond242:                                      ; preds = %for.inc276, %if.end239
  %243 = load i32, i32* %idx241, align 4, !tbaa !6
  %cmp243 = icmp sle i32 %243, 3
  br i1 %cmp243, label %for.body246, label %for.cond.cleanup245

for.cond.cleanup245:                              ; preds = %for.cond242
  store i32 14, i32* %cleanup.dest.slot, align 4
  %244 = bitcast i32* %idx241 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #6
  br label %for.end279

for.body246:                                      ; preds = %for.cond242
  %245 = bitcast i32* %row_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %245) #6
  %246 = load i32, i32* %idx241, align 4, !tbaa !6
  %shl = shl i32 %246, 1
  %sub247 = sub nsw i32 0, %shl
  %add248 = add nsw i32 %sub247, 1
  %247 = load i32, i32* %row_adj, align 4, !tbaa !6
  %add249 = add nsw i32 %add248, %247
  store i32 %add249, i32* %row_offset, align 4, !tbaa !6
  %248 = bitcast i32* %col_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %248) #6
  %249 = load i32, i32* %idx241, align 4, !tbaa !6
  %shl250 = shl i32 %249, 1
  %sub251 = sub nsw i32 0, %shl250
  %add252 = add nsw i32 %sub251, 1
  %250 = load i32, i32* %col_adj, align 4, !tbaa !6
  %add253 = add nsw i32 %add252, %250
  store i32 %add253, i32* %col_offset, align 4, !tbaa !6
  %251 = load i32, i32* %row_offset, align 4, !tbaa !6
  %call254 = call i32 @abs(i32 %251) #7
  %252 = load i32, i32* %max_row_offset, align 4, !tbaa !6
  %call255 = call i32 @abs(i32 %252) #7
  %cmp256 = icmp sle i32 %call254, %call255
  br i1 %cmp256, label %land.lhs.true258, label %if.end264

land.lhs.true258:                                 ; preds = %for.body246
  %253 = load i32, i32* %row_offset, align 4, !tbaa !6
  %call259 = call i32 @abs(i32 %253) #7
  %254 = load i32, i32* %processed_rows, align 4, !tbaa !6
  %cmp260 = icmp sgt i32 %call259, %254
  br i1 %cmp260, label %if.then262, label %if.end264

if.then262:                                       ; preds = %land.lhs.true258
  %255 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %256 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %257 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %arraydecay263 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %258 = load i32, i32* %row_offset, align 4, !tbaa !6
  %259 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %260 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %261 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %262 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %263 = load i32, i32* %max_row_offset, align 4, !tbaa !6
  call void @scan_row_mbmi(%struct.AV1Common* %255, %struct.macroblockd* %256, i32 %257, i8* %arraydecay263, i32 %258, %struct.candidate_mv* %259, i16* %260, i8* %261, i8* %row_match_count, i8* %dummy_newmv_count, %union.int_mv* %262, i32 %263, i32* %processed_rows)
  br label %if.end264

if.end264:                                        ; preds = %if.then262, %land.lhs.true258, %for.body246
  %264 = load i32, i32* %col_offset, align 4, !tbaa !6
  %call265 = call i32 @abs(i32 %264) #7
  %265 = load i32, i32* %max_col_offset, align 4, !tbaa !6
  %call266 = call i32 @abs(i32 %265) #7
  %cmp267 = icmp sle i32 %call265, %call266
  br i1 %cmp267, label %land.lhs.true269, label %if.end275

land.lhs.true269:                                 ; preds = %if.end264
  %266 = load i32, i32* %col_offset, align 4, !tbaa !6
  %call270 = call i32 @abs(i32 %266) #7
  %267 = load i32, i32* %processed_cols, align 4, !tbaa !6
  %cmp271 = icmp sgt i32 %call270, %267
  br i1 %cmp271, label %if.then273, label %if.end275

if.then273:                                       ; preds = %land.lhs.true269
  %268 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %269 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %270 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %arraydecay274 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %271 = load i32, i32* %col_offset, align 4, !tbaa !6
  %272 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %273 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %274 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %275 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %276 = load i32, i32* %max_col_offset, align 4, !tbaa !6
  call void @scan_col_mbmi(%struct.AV1Common* %268, %struct.macroblockd* %269, i32 %270, i8* %arraydecay274, i32 %271, %struct.candidate_mv* %272, i16* %273, i8* %274, i8* %col_match_count, i8* %dummy_newmv_count, %union.int_mv* %275, i32 %276, i32* %processed_cols)
  br label %if.end275

if.end275:                                        ; preds = %if.then273, %land.lhs.true269, %if.end264
  %277 = bitcast i32* %col_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #6
  %278 = bitcast i32* %row_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #6
  br label %for.inc276

for.inc276:                                       ; preds = %if.end275
  %279 = load i32, i32* %idx241, align 4, !tbaa !6
  %inc277 = add nsw i32 %279, 1
  store i32 %inc277, i32* %idx241, align 4, !tbaa !6
  br label %for.cond242

for.end279:                                       ; preds = %for.cond.cleanup245
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_match_count) #6
  %280 = load i8, i8* %row_match_count, align 1, !tbaa !41
  %conv280 = zext i8 %280 to i32
  %cmp281 = icmp sgt i32 %conv280, 0
  %conv282 = zext i1 %cmp281 to i32
  %281 = load i8, i8* %col_match_count, align 1, !tbaa !41
  %conv283 = zext i8 %281 to i32
  %cmp284 = icmp sgt i32 %conv283, 0
  %conv285 = zext i1 %cmp284 to i32
  %add286 = add nsw i32 %conv282, %conv285
  %conv287 = trunc i32 %add286 to i8
  store i8 %conv287, i8* %ref_match_count, align 1, !tbaa !41
  %282 = load i8, i8* %nearest_match, align 1, !tbaa !41
  %conv288 = zext i8 %282 to i32
  switch i32 %conv288, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb319
    i32 2, label %sw.bb350
  ]

sw.bb:                                            ; preds = %for.end279
  %283 = load i8, i8* %ref_match_count, align 1, !tbaa !41
  %conv289 = zext i8 %283 to i32
  %cmp290 = icmp sge i32 %conv289, 1
  br i1 %cmp290, label %if.then292, label %if.end298

if.then292:                                       ; preds = %sw.bb
  %284 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %285 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom293 = sext i8 %285 to i32
  %arrayidx294 = getelementptr inbounds i16, i16* %284, i32 %idxprom293
  %286 = load i16, i16* %arrayidx294, align 2, !tbaa !58
  %conv295 = sext i16 %286 to i32
  %or296 = or i32 %conv295, 1
  %conv297 = trunc i32 %or296 to i16
  store i16 %conv297, i16* %arrayidx294, align 2, !tbaa !58
  br label %if.end298

if.end298:                                        ; preds = %if.then292, %sw.bb
  %287 = load i8, i8* %ref_match_count, align 1, !tbaa !41
  %conv299 = zext i8 %287 to i32
  %cmp300 = icmp eq i32 %conv299, 1
  br i1 %cmp300, label %if.then302, label %if.else

if.then302:                                       ; preds = %if.end298
  %288 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %289 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom303 = sext i8 %289 to i32
  %arrayidx304 = getelementptr inbounds i16, i16* %288, i32 %idxprom303
  %290 = load i16, i16* %arrayidx304, align 2, !tbaa !58
  %conv305 = sext i16 %290 to i32
  %or306 = or i32 %conv305, 16
  %conv307 = trunc i32 %or306 to i16
  store i16 %conv307, i16* %arrayidx304, align 2, !tbaa !58
  br label %if.end318

if.else:                                          ; preds = %if.end298
  %291 = load i8, i8* %ref_match_count, align 1, !tbaa !41
  %conv308 = zext i8 %291 to i32
  %cmp309 = icmp sge i32 %conv308, 2
  br i1 %cmp309, label %if.then311, label %if.end317

if.then311:                                       ; preds = %if.else
  %292 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %293 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom312 = sext i8 %293 to i32
  %arrayidx313 = getelementptr inbounds i16, i16* %292, i32 %idxprom312
  %294 = load i16, i16* %arrayidx313, align 2, !tbaa !58
  %conv314 = sext i16 %294 to i32
  %or315 = or i32 %conv314, 32
  %conv316 = trunc i32 %or315 to i16
  store i16 %conv316, i16* %arrayidx313, align 2, !tbaa !58
  br label %if.end317

if.end317:                                        ; preds = %if.then311, %if.else
  br label %if.end318

if.end318:                                        ; preds = %if.end317, %if.then302
  br label %sw.epilog

sw.bb319:                                         ; preds = %for.end279
  %295 = load i8, i8* %newmv_count, align 1, !tbaa !41
  %conv320 = zext i8 %295 to i32
  %cmp321 = icmp sgt i32 %conv320, 0
  %296 = zext i1 %cmp321 to i64
  %cond323 = select i1 %cmp321, i32 2, i32 3
  %297 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %298 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom324 = sext i8 %298 to i32
  %arrayidx325 = getelementptr inbounds i16, i16* %297, i32 %idxprom324
  %299 = load i16, i16* %arrayidx325, align 2, !tbaa !58
  %conv326 = sext i16 %299 to i32
  %or327 = or i32 %conv326, %cond323
  %conv328 = trunc i32 %or327 to i16
  store i16 %conv328, i16* %arrayidx325, align 2, !tbaa !58
  %300 = load i8, i8* %ref_match_count, align 1, !tbaa !41
  %conv329 = zext i8 %300 to i32
  %cmp330 = icmp eq i32 %conv329, 1
  br i1 %cmp330, label %if.then332, label %if.else338

if.then332:                                       ; preds = %sw.bb319
  %301 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %302 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom333 = sext i8 %302 to i32
  %arrayidx334 = getelementptr inbounds i16, i16* %301, i32 %idxprom333
  %303 = load i16, i16* %arrayidx334, align 2, !tbaa !58
  %conv335 = sext i16 %303 to i32
  %or336 = or i32 %conv335, 48
  %conv337 = trunc i32 %or336 to i16
  store i16 %conv337, i16* %arrayidx334, align 2, !tbaa !58
  br label %if.end349

if.else338:                                       ; preds = %sw.bb319
  %304 = load i8, i8* %ref_match_count, align 1, !tbaa !41
  %conv339 = zext i8 %304 to i32
  %cmp340 = icmp sge i32 %conv339, 2
  br i1 %cmp340, label %if.then342, label %if.end348

if.then342:                                       ; preds = %if.else338
  %305 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %306 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom343 = sext i8 %306 to i32
  %arrayidx344 = getelementptr inbounds i16, i16* %305, i32 %idxprom343
  %307 = load i16, i16* %arrayidx344, align 2, !tbaa !58
  %conv345 = sext i16 %307 to i32
  %or346 = or i32 %conv345, 64
  %conv347 = trunc i32 %or346 to i16
  store i16 %conv347, i16* %arrayidx344, align 2, !tbaa !58
  br label %if.end348

if.end348:                                        ; preds = %if.then342, %if.else338
  br label %if.end349

if.end349:                                        ; preds = %if.end348, %if.then332
  br label %sw.epilog

sw.bb350:                                         ; preds = %for.end279
  br label %sw.default

sw.default:                                       ; preds = %for.end279, %sw.bb350
  %308 = load i8, i8* %newmv_count, align 1, !tbaa !41
  %conv351 = zext i8 %308 to i32
  %cmp352 = icmp sge i32 %conv351, 1
  br i1 %cmp352, label %if.then354, label %if.else360

if.then354:                                       ; preds = %sw.default
  %309 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %310 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom355 = sext i8 %310 to i32
  %arrayidx356 = getelementptr inbounds i16, i16* %309, i32 %idxprom355
  %311 = load i16, i16* %arrayidx356, align 2, !tbaa !58
  %conv357 = sext i16 %311 to i32
  %or358 = or i32 %conv357, 4
  %conv359 = trunc i32 %or358 to i16
  store i16 %conv359, i16* %arrayidx356, align 2, !tbaa !58
  br label %if.end366

if.else360:                                       ; preds = %sw.default
  %312 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %313 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom361 = sext i8 %313 to i32
  %arrayidx362 = getelementptr inbounds i16, i16* %312, i32 %idxprom361
  %314 = load i16, i16* %arrayidx362, align 2, !tbaa !58
  %conv363 = sext i16 %314 to i32
  %or364 = or i32 %conv363, 5
  %conv365 = trunc i32 %or364 to i16
  store i16 %conv365, i16* %arrayidx362, align 2, !tbaa !58
  br label %if.end366

if.end366:                                        ; preds = %if.else360, %if.then354
  %315 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %316 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom367 = sext i8 %316 to i32
  %arrayidx368 = getelementptr inbounds i16, i16* %315, i32 %idxprom367
  %317 = load i16, i16* %arrayidx368, align 2, !tbaa !58
  %conv369 = sext i16 %317 to i32
  %or370 = or i32 %conv369, 80
  %conv371 = trunc i32 %or370 to i16
  store i16 %conv371, i16* %arrayidx368, align 2, !tbaa !58
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end366, %if.end349, %if.end318
  %318 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %318) #6
  %319 = load i8, i8* %nearest_refmv_count, align 1, !tbaa !41
  %conv372 = zext i8 %319 to i32
  store i32 %conv372, i32* %len, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %for.end405, %sw.epilog
  %320 = load i32, i32* %len, align 4, !tbaa !6
  %cmp373 = icmp sgt i32 %320, 0
  br i1 %cmp373, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %321 = bitcast i32* %nr_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %321) #6
  store i32 0, i32* %nr_len, align 4, !tbaa !6
  %322 = bitcast i32* %idx375 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %322) #6
  store i32 1, i32* %idx375, align 4, !tbaa !6
  br label %for.cond376

for.cond376:                                      ; preds = %for.inc402, %while.body
  %323 = load i32, i32* %idx375, align 4, !tbaa !6
  %324 = load i32, i32* %len, align 4, !tbaa !6
  %cmp377 = icmp slt i32 %323, %324
  br i1 %cmp377, label %for.body380, label %for.cond.cleanup379

for.cond.cleanup379:                              ; preds = %for.cond376
  store i32 20, i32* %cleanup.dest.slot, align 4
  %325 = bitcast i32* %idx375 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %325) #6
  br label %for.end405

for.body380:                                      ; preds = %for.cond376
  %326 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %327 = load i32, i32* %idx375, align 4, !tbaa !6
  %sub381 = sub nsw i32 %327, 1
  %arrayidx382 = getelementptr inbounds i16, i16* %326, i32 %sub381
  %328 = load i16, i16* %arrayidx382, align 2, !tbaa !58
  %conv383 = zext i16 %328 to i32
  %329 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %330 = load i32, i32* %idx375, align 4, !tbaa !6
  %arrayidx384 = getelementptr inbounds i16, i16* %329, i32 %330
  %331 = load i16, i16* %arrayidx384, align 2, !tbaa !58
  %conv385 = zext i16 %331 to i32
  %cmp386 = icmp slt i32 %conv383, %conv385
  br i1 %cmp386, label %if.then388, label %if.end401

if.then388:                                       ; preds = %for.body380
  %332 = bitcast %struct.candidate_mv* %tmp_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %332) #6
  %333 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %334 = load i32, i32* %idx375, align 4, !tbaa !6
  %sub389 = sub nsw i32 %334, 1
  %arrayidx390 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %333, i32 %sub389
  %335 = bitcast %struct.candidate_mv* %tmp_mv to i8*
  %336 = bitcast %struct.candidate_mv* %arrayidx390 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %335, i8* align 4 %336, i32 8, i1 false), !tbaa.struct !65
  %337 = bitcast i16* %tmp_ref_mv_weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %337) #6
  %338 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %339 = load i32, i32* %idx375, align 4, !tbaa !6
  %sub391 = sub nsw i32 %339, 1
  %arrayidx392 = getelementptr inbounds i16, i16* %338, i32 %sub391
  %340 = load i16, i16* %arrayidx392, align 2, !tbaa !58
  store i16 %340, i16* %tmp_ref_mv_weight, align 2, !tbaa !58
  %341 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %342 = load i32, i32* %idx375, align 4, !tbaa !6
  %sub393 = sub nsw i32 %342, 1
  %arrayidx394 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %341, i32 %sub393
  %343 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %344 = load i32, i32* %idx375, align 4, !tbaa !6
  %arrayidx395 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %343, i32 %344
  %345 = bitcast %struct.candidate_mv* %arrayidx394 to i8*
  %346 = bitcast %struct.candidate_mv* %arrayidx395 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %345, i8* align 4 %346, i32 8, i1 false), !tbaa.struct !65
  %347 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %348 = load i32, i32* %idx375, align 4, !tbaa !6
  %arrayidx396 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %347, i32 %348
  %349 = bitcast %struct.candidate_mv* %arrayidx396 to i8*
  %350 = bitcast %struct.candidate_mv* %tmp_mv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %349, i8* align 4 %350, i32 8, i1 false), !tbaa.struct !65
  %351 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %352 = load i32, i32* %idx375, align 4, !tbaa !6
  %arrayidx397 = getelementptr inbounds i16, i16* %351, i32 %352
  %353 = load i16, i16* %arrayidx397, align 2, !tbaa !58
  %354 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %355 = load i32, i32* %idx375, align 4, !tbaa !6
  %sub398 = sub nsw i32 %355, 1
  %arrayidx399 = getelementptr inbounds i16, i16* %354, i32 %sub398
  store i16 %353, i16* %arrayidx399, align 2, !tbaa !58
  %356 = load i16, i16* %tmp_ref_mv_weight, align 2, !tbaa !58
  %357 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %358 = load i32, i32* %idx375, align 4, !tbaa !6
  %arrayidx400 = getelementptr inbounds i16, i16* %357, i32 %358
  store i16 %356, i16* %arrayidx400, align 2, !tbaa !58
  %359 = load i32, i32* %idx375, align 4, !tbaa !6
  store i32 %359, i32* %nr_len, align 4, !tbaa !6
  %360 = bitcast i16* %tmp_ref_mv_weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %360) #6
  %361 = bitcast %struct.candidate_mv* %tmp_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %361) #6
  br label %if.end401

if.end401:                                        ; preds = %if.then388, %for.body380
  br label %for.inc402

for.inc402:                                       ; preds = %if.end401
  %362 = load i32, i32* %idx375, align 4, !tbaa !6
  %inc403 = add nsw i32 %362, 1
  store i32 %inc403, i32* %idx375, align 4, !tbaa !6
  br label %for.cond376

for.end405:                                       ; preds = %for.cond.cleanup379
  %363 = load i32, i32* %nr_len, align 4, !tbaa !6
  store i32 %363, i32* %len, align 4, !tbaa !6
  %364 = bitcast i32* %nr_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %364) #6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %365 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %366 = load i8, i8* %365, align 1, !tbaa !41
  %conv406 = zext i8 %366 to i32
  store i32 %conv406, i32* %len, align 4, !tbaa !6
  br label %while.cond407

while.cond407:                                    ; preds = %for.end448, %while.end
  %367 = load i32, i32* %len, align 4, !tbaa !6
  %368 = load i8, i8* %nearest_refmv_count, align 1, !tbaa !41
  %conv408 = zext i8 %368 to i32
  %cmp409 = icmp sgt i32 %367, %conv408
  br i1 %cmp409, label %while.body411, label %while.end449

while.body411:                                    ; preds = %while.cond407
  %369 = bitcast i32* %nr_len412 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %369) #6
  %370 = load i8, i8* %nearest_refmv_count, align 1, !tbaa !41
  %conv413 = zext i8 %370 to i32
  store i32 %conv413, i32* %nr_len412, align 4, !tbaa !6
  %371 = bitcast i32* %idx414 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %371) #6
  %372 = load i8, i8* %nearest_refmv_count, align 1, !tbaa !41
  %conv415 = zext i8 %372 to i32
  %add416 = add nsw i32 %conv415, 1
  store i32 %add416, i32* %idx414, align 4, !tbaa !6
  br label %for.cond417

for.cond417:                                      ; preds = %for.inc445, %while.body411
  %373 = load i32, i32* %idx414, align 4, !tbaa !6
  %374 = load i32, i32* %len, align 4, !tbaa !6
  %cmp418 = icmp slt i32 %373, %374
  br i1 %cmp418, label %for.body421, label %for.cond.cleanup420

for.cond.cleanup420:                              ; preds = %for.cond417
  store i32 25, i32* %cleanup.dest.slot, align 4
  %375 = bitcast i32* %idx414 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %375) #6
  br label %for.end448

for.body421:                                      ; preds = %for.cond417
  %376 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %377 = load i32, i32* %idx414, align 4, !tbaa !6
  %sub422 = sub nsw i32 %377, 1
  %arrayidx423 = getelementptr inbounds i16, i16* %376, i32 %sub422
  %378 = load i16, i16* %arrayidx423, align 2, !tbaa !58
  %conv424 = zext i16 %378 to i32
  %379 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %380 = load i32, i32* %idx414, align 4, !tbaa !6
  %arrayidx425 = getelementptr inbounds i16, i16* %379, i32 %380
  %381 = load i16, i16* %arrayidx425, align 2, !tbaa !58
  %conv426 = zext i16 %381 to i32
  %cmp427 = icmp slt i32 %conv424, %conv426
  br i1 %cmp427, label %if.then429, label %if.end444

if.then429:                                       ; preds = %for.body421
  %382 = bitcast %struct.candidate_mv* %tmp_mv430 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %382) #6
  %383 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %384 = load i32, i32* %idx414, align 4, !tbaa !6
  %sub431 = sub nsw i32 %384, 1
  %arrayidx432 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %383, i32 %sub431
  %385 = bitcast %struct.candidate_mv* %tmp_mv430 to i8*
  %386 = bitcast %struct.candidate_mv* %arrayidx432 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %385, i8* align 4 %386, i32 8, i1 false), !tbaa.struct !65
  %387 = bitcast i16* %tmp_ref_mv_weight433 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %387) #6
  %388 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %389 = load i32, i32* %idx414, align 4, !tbaa !6
  %sub434 = sub nsw i32 %389, 1
  %arrayidx435 = getelementptr inbounds i16, i16* %388, i32 %sub434
  %390 = load i16, i16* %arrayidx435, align 2, !tbaa !58
  store i16 %390, i16* %tmp_ref_mv_weight433, align 2, !tbaa !58
  %391 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %392 = load i32, i32* %idx414, align 4, !tbaa !6
  %sub436 = sub nsw i32 %392, 1
  %arrayidx437 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %391, i32 %sub436
  %393 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %394 = load i32, i32* %idx414, align 4, !tbaa !6
  %arrayidx438 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %393, i32 %394
  %395 = bitcast %struct.candidate_mv* %arrayidx437 to i8*
  %396 = bitcast %struct.candidate_mv* %arrayidx438 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %395, i8* align 4 %396, i32 8, i1 false), !tbaa.struct !65
  %397 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %398 = load i32, i32* %idx414, align 4, !tbaa !6
  %arrayidx439 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %397, i32 %398
  %399 = bitcast %struct.candidate_mv* %arrayidx439 to i8*
  %400 = bitcast %struct.candidate_mv* %tmp_mv430 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %399, i8* align 4 %400, i32 8, i1 false), !tbaa.struct !65
  %401 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %402 = load i32, i32* %idx414, align 4, !tbaa !6
  %arrayidx440 = getelementptr inbounds i16, i16* %401, i32 %402
  %403 = load i16, i16* %arrayidx440, align 2, !tbaa !58
  %404 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %405 = load i32, i32* %idx414, align 4, !tbaa !6
  %sub441 = sub nsw i32 %405, 1
  %arrayidx442 = getelementptr inbounds i16, i16* %404, i32 %sub441
  store i16 %403, i16* %arrayidx442, align 2, !tbaa !58
  %406 = load i16, i16* %tmp_ref_mv_weight433, align 2, !tbaa !58
  %407 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %408 = load i32, i32* %idx414, align 4, !tbaa !6
  %arrayidx443 = getelementptr inbounds i16, i16* %407, i32 %408
  store i16 %406, i16* %arrayidx443, align 2, !tbaa !58
  %409 = load i32, i32* %idx414, align 4, !tbaa !6
  store i32 %409, i32* %nr_len412, align 4, !tbaa !6
  %410 = bitcast i16* %tmp_ref_mv_weight433 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %410) #6
  %411 = bitcast %struct.candidate_mv* %tmp_mv430 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %411) #6
  br label %if.end444

if.end444:                                        ; preds = %if.then429, %for.body421
  br label %for.inc445

for.inc445:                                       ; preds = %if.end444
  %412 = load i32, i32* %idx414, align 4, !tbaa !6
  %inc446 = add nsw i32 %412, 1
  store i32 %inc446, i32* %idx414, align 4, !tbaa !6
  br label %for.cond417

for.end448:                                       ; preds = %for.cond.cleanup420
  %413 = load i32, i32* %nr_len412, align 4, !tbaa !6
  store i32 %413, i32* %len, align 4, !tbaa !6
  %414 = bitcast i32* %nr_len412 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %414) #6
  br label %while.cond407

while.end449:                                     ; preds = %while.cond407
  %415 = bitcast i32* %mi_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %415) #6
  %416 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv450 = zext i8 %416 to i32
  %417 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width451 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %417, i32 0, i32 33
  %418 = load i8, i8* %width451, align 4, !tbaa !60
  %conv452 = zext i8 %418 to i32
  %cmp453 = icmp slt i32 %conv450, %conv452
  br i1 %cmp453, label %cond.true455, label %cond.false457

cond.true455:                                     ; preds = %while.end449
  %419 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv456 = zext i8 %419 to i32
  br label %cond.end460

cond.false457:                                    ; preds = %while.end449
  %420 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width458 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %420, i32 0, i32 33
  %421 = load i8, i8* %width458, align 4, !tbaa !60
  %conv459 = zext i8 %421 to i32
  br label %cond.end460

cond.end460:                                      ; preds = %cond.false457, %cond.true455
  %cond461 = phi i32 [ %conv456, %cond.true455 ], [ %conv459, %cond.false457 ]
  store i32 %cond461, i32* %mi_width, align 4, !tbaa !6
  %422 = load i32, i32* %mi_width, align 4, !tbaa !6
  %423 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %423, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %424 = load i32, i32* %mi_cols, align 4, !tbaa !8
  %425 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %sub462 = sub nsw i32 %424, %425
  %cmp463 = icmp slt i32 %422, %sub462
  br i1 %cmp463, label %cond.true465, label %cond.false466

cond.true465:                                     ; preds = %cond.end460
  %426 = load i32, i32* %mi_width, align 4, !tbaa !6
  br label %cond.end470

cond.false466:                                    ; preds = %cond.end460
  %427 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params467 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %427, i32 0, i32 22
  %mi_cols468 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params467, i32 0, i32 4
  %428 = load i32, i32* %mi_cols468, align 4, !tbaa !8
  %429 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %sub469 = sub nsw i32 %428, %429
  br label %cond.end470

cond.end470:                                      ; preds = %cond.false466, %cond.true465
  %cond471 = phi i32 [ %426, %cond.true465 ], [ %sub469, %cond.false466 ]
  store i32 %cond471, i32* %mi_width, align 4, !tbaa !6
  %430 = bitcast i32* %mi_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %430) #6
  %431 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !41
  %conv472 = zext i8 %431 to i32
  %432 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height473 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %432, i32 0, i32 34
  %433 = load i8, i8* %height473, align 1, !tbaa !61
  %conv474 = zext i8 %433 to i32
  %cmp475 = icmp slt i32 %conv472, %conv474
  br i1 %cmp475, label %cond.true477, label %cond.false479

cond.true477:                                     ; preds = %cond.end470
  %434 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !41
  %conv478 = zext i8 %434 to i32
  br label %cond.end482

cond.false479:                                    ; preds = %cond.end470
  %435 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height480 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %435, i32 0, i32 34
  %436 = load i8, i8* %height480, align 1, !tbaa !61
  %conv481 = zext i8 %436 to i32
  br label %cond.end482

cond.end482:                                      ; preds = %cond.false479, %cond.true477
  %cond483 = phi i32 [ %conv478, %cond.true477 ], [ %conv481, %cond.false479 ]
  store i32 %cond483, i32* %mi_height, align 4, !tbaa !6
  %437 = load i32, i32* %mi_height, align 4, !tbaa !6
  %438 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params484 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %438, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params484, i32 0, i32 3
  %439 = load i32, i32* %mi_rows, align 4, !tbaa !66
  %440 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %sub485 = sub nsw i32 %439, %440
  %cmp486 = icmp slt i32 %437, %sub485
  br i1 %cmp486, label %cond.true488, label %cond.false489

cond.true488:                                     ; preds = %cond.end482
  %441 = load i32, i32* %mi_height, align 4, !tbaa !6
  br label %cond.end493

cond.false489:                                    ; preds = %cond.end482
  %442 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params490 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %442, i32 0, i32 22
  %mi_rows491 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params490, i32 0, i32 3
  %443 = load i32, i32* %mi_rows491, align 4, !tbaa !66
  %444 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %sub492 = sub nsw i32 %443, %444
  br label %cond.end493

cond.end493:                                      ; preds = %cond.false489, %cond.true488
  %cond494 = phi i32 [ %441, %cond.true488 ], [ %sub492, %cond.false489 ]
  store i32 %cond494, i32* %mi_height, align 4, !tbaa !6
  %445 = bitcast i32* %mi_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %445) #6
  %446 = load i32, i32* %mi_width, align 4, !tbaa !6
  %447 = load i32, i32* %mi_height, align 4, !tbaa !6
  %cmp495 = icmp slt i32 %446, %447
  br i1 %cmp495, label %cond.true497, label %cond.false498

cond.true497:                                     ; preds = %cond.end493
  %448 = load i32, i32* %mi_width, align 4, !tbaa !6
  br label %cond.end499

cond.false498:                                    ; preds = %cond.end493
  %449 = load i32, i32* %mi_height, align 4, !tbaa !6
  br label %cond.end499

cond.end499:                                      ; preds = %cond.false498, %cond.true497
  %cond500 = phi i32 [ %448, %cond.true497 ], [ %449, %cond.false498 ]
  store i32 %cond500, i32* %mi_size, align 4, !tbaa !6
  %arrayidx501 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 1
  %450 = load i8, i8* %arrayidx501, align 1, !tbaa !41
  %conv502 = sext i8 %450 to i32
  %cmp503 = icmp sgt i32 %conv502, -1
  br i1 %cmp503, label %if.then505, label %if.else722

if.then505:                                       ; preds = %cond.end499
  %451 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %452 = load i8, i8* %451, align 1, !tbaa !41
  %conv506 = zext i8 %452 to i32
  %cmp507 = icmp slt i32 %conv506, 2
  br i1 %cmp507, label %if.then509, label %if.end693

if.then509:                                       ; preds = %if.then505
  %453 = bitcast [2 x [2 x %union.int_mv]]* %ref_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %453) #6
  %454 = bitcast [2 x [2 x %union.int_mv]]* %ref_diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %454) #6
  %455 = bitcast [2 x i32]* %ref_id_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %455) #6
  %456 = bitcast [2 x i32]* %ref_id_count to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %456, i8 0, i32 8, i1 false)
  %457 = bitcast [2 x i32]* %ref_diff_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %457) #6
  %458 = bitcast [2 x i32]* %ref_diff_count to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %458, i8 0, i32 8, i1 false)
  %459 = bitcast i32* %idx510 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %459) #6
  store i32 0, i32* %idx510, align 4, !tbaa !6
  br label %for.cond511

for.cond511:                                      ; preds = %for.body521, %if.then509
  %460 = load i32, i32* %max_row_offset, align 4, !tbaa !6
  %call512 = call i32 @abs(i32 %460) #7
  %cmp513 = icmp sge i32 %call512, 1
  br i1 %cmp513, label %land.rhs515, label %land.end518

land.rhs515:                                      ; preds = %for.cond511
  %461 = load i32, i32* %idx510, align 4, !tbaa !6
  %462 = load i32, i32* %mi_size, align 4, !tbaa !6
  %cmp516 = icmp slt i32 %461, %462
  br label %land.end518

land.end518:                                      ; preds = %land.rhs515, %for.cond511
  %463 = phi i1 [ false, %for.cond511 ], [ %cmp516, %land.rhs515 ]
  br i1 %463, label %for.body521, label %for.cond.cleanup520

for.cond.cleanup520:                              ; preds = %land.end518
  store i32 28, i32* %cleanup.dest.slot, align 4
  %464 = bitcast i32* %idx510 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %464) #6
  br label %for.end535

for.body521:                                      ; preds = %land.end518
  %465 = bitcast %struct.MB_MODE_INFO** %candidate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %465) #6
  %466 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %466, i32 0, i32 6
  %467 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !67
  %468 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %468, i32 0, i32 2
  %469 = load i32, i32* %mi_stride, align 8, !tbaa !68
  %sub522 = sub nsw i32 0, %469
  %470 = load i32, i32* %idx510, align 4, !tbaa !6
  %add523 = add nsw i32 %sub522, %470
  %arrayidx524 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %467, i32 %add523
  %471 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx524, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %471, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %472 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %473 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %arraydecay525 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %arraydecay526 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %ref_id, i32 0, i32 0
  %arraydecay527 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_id_count, i32 0, i32 0
  %arraydecay528 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %ref_diff, i32 0, i32 0
  %arraydecay529 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_diff_count, i32 0, i32 0
  call void @process_compound_ref_mv_candidate(%struct.MB_MODE_INFO* %472, %struct.AV1Common* %473, i8* %arraydecay525, [2 x %union.int_mv]* %arraydecay526, i32* %arraydecay527, [2 x %union.int_mv]* %arraydecay528, i32* %arraydecay529)
  %474 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %474, i32 0, i32 6
  %475 = load i8, i8* %sb_type, align 2, !tbaa !48
  %idxprom530 = zext i8 %475 to i32
  %arrayidx531 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom530
  %476 = load i8, i8* %arrayidx531, align 1, !tbaa !41
  %conv532 = zext i8 %476 to i32
  %477 = load i32, i32* %idx510, align 4, !tbaa !6
  %add533 = add nsw i32 %477, %conv532
  store i32 %add533, i32* %idx510, align 4, !tbaa !6
  %478 = bitcast %struct.MB_MODE_INFO** %candidate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %478) #6
  br label %for.cond511

for.end535:                                       ; preds = %for.cond.cleanup520
  %479 = bitcast i32* %idx536 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %479) #6
  store i32 0, i32* %idx536, align 4, !tbaa !6
  br label %for.cond537

for.cond537:                                      ; preds = %for.body547, %for.end535
  %480 = load i32, i32* %max_col_offset, align 4, !tbaa !6
  %call538 = call i32 @abs(i32 %480) #7
  %cmp539 = icmp sge i32 %call538, 1
  br i1 %cmp539, label %land.rhs541, label %land.end544

land.rhs541:                                      ; preds = %for.cond537
  %481 = load i32, i32* %idx536, align 4, !tbaa !6
  %482 = load i32, i32* %mi_size, align 4, !tbaa !6
  %cmp542 = icmp slt i32 %481, %482
  br label %land.end544

land.end544:                                      ; preds = %land.rhs541, %for.cond537
  %483 = phi i1 [ false, %for.cond537 ], [ %cmp542, %land.rhs541 ]
  br i1 %483, label %for.body547, label %for.cond.cleanup546

for.cond.cleanup546:                              ; preds = %land.end544
  store i32 30, i32* %cleanup.dest.slot, align 4
  %484 = bitcast i32* %idx536 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %484) #6
  br label %for.end564

for.body547:                                      ; preds = %land.end544
  %485 = bitcast %struct.MB_MODE_INFO** %candidate548 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %485) #6
  %486 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi549 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %486, i32 0, i32 6
  %487 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi549, align 4, !tbaa !67
  %488 = load i32, i32* %idx536, align 4, !tbaa !6
  %489 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride550 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %489, i32 0, i32 2
  %490 = load i32, i32* %mi_stride550, align 8, !tbaa !68
  %mul = mul nsw i32 %488, %490
  %sub551 = sub nsw i32 %mul, 1
  %arrayidx552 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %487, i32 %sub551
  %491 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx552, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %491, %struct.MB_MODE_INFO** %candidate548, align 4, !tbaa !2
  %492 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate548, align 4, !tbaa !2
  %493 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %arraydecay553 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %arraydecay554 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %ref_id, i32 0, i32 0
  %arraydecay555 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_id_count, i32 0, i32 0
  %arraydecay556 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %ref_diff, i32 0, i32 0
  %arraydecay557 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_diff_count, i32 0, i32 0
  call void @process_compound_ref_mv_candidate(%struct.MB_MODE_INFO* %492, %struct.AV1Common* %493, i8* %arraydecay553, [2 x %union.int_mv]* %arraydecay554, i32* %arraydecay555, [2 x %union.int_mv]* %arraydecay556, i32* %arraydecay557)
  %494 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate548, align 4, !tbaa !2
  %sb_type558 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %494, i32 0, i32 6
  %495 = load i8, i8* %sb_type558, align 2, !tbaa !48
  %idxprom559 = zext i8 %495 to i32
  %arrayidx560 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom559
  %496 = load i8, i8* %arrayidx560, align 1, !tbaa !41
  %conv561 = zext i8 %496 to i32
  %497 = load i32, i32* %idx536, align 4, !tbaa !6
  %add562 = add nsw i32 %497, %conv561
  store i32 %add562, i32* %idx536, align 4, !tbaa !6
  %498 = bitcast %struct.MB_MODE_INFO** %candidate548 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %498) #6
  br label %for.cond537

for.end564:                                       ; preds = %for.cond.cleanup546
  %499 = bitcast [2 x [2 x %union.int_mv]]* %comp_list to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %499) #6
  %500 = bitcast i32* %idx565 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %500) #6
  store i32 0, i32* %idx565, align 4, !tbaa !6
  br label %for.cond566

for.cond566:                                      ; preds = %for.inc622, %for.end564
  %501 = load i32, i32* %idx565, align 4, !tbaa !6
  %cmp567 = icmp slt i32 %501, 2
  br i1 %cmp567, label %for.body570, label %for.cond.cleanup569

for.cond.cleanup569:                              ; preds = %for.cond566
  store i32 32, i32* %cleanup.dest.slot, align 4
  %502 = bitcast i32* %idx565 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %502) #6
  br label %for.end625

for.body570:                                      ; preds = %for.cond566
  %503 = bitcast i32* %comp_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %503) #6
  store i32 0, i32* %comp_idx, align 4, !tbaa !6
  %504 = bitcast i32* %list_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %504) #6
  store i32 0, i32* %list_idx, align 4, !tbaa !6
  br label %for.cond571

for.cond571:                                      ; preds = %for.inc586, %for.body570
  %505 = load i32, i32* %list_idx, align 4, !tbaa !6
  %506 = load i32, i32* %idx565, align 4, !tbaa !6
  %arrayidx572 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_id_count, i32 0, i32 %506
  %507 = load i32, i32* %arrayidx572, align 4, !tbaa !6
  %cmp573 = icmp slt i32 %505, %507
  br i1 %cmp573, label %land.rhs575, label %land.end578

land.rhs575:                                      ; preds = %for.cond571
  %508 = load i32, i32* %comp_idx, align 4, !tbaa !6
  %cmp576 = icmp slt i32 %508, 2
  br label %land.end578

land.end578:                                      ; preds = %land.rhs575, %for.cond571
  %509 = phi i1 [ false, %for.cond571 ], [ %cmp576, %land.rhs575 ]
  br i1 %509, label %for.body581, label %for.cond.cleanup580

for.cond.cleanup580:                              ; preds = %land.end578
  store i32 35, i32* %cleanup.dest.slot, align 4
  %510 = bitcast i32* %list_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %510) #6
  br label %for.end590

for.body581:                                      ; preds = %land.end578
  %511 = load i32, i32* %comp_idx, align 4, !tbaa !6
  %arrayidx582 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 %511
  %512 = load i32, i32* %idx565, align 4, !tbaa !6
  %arrayidx583 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx582, i32 0, i32 %512
  %513 = load i32, i32* %idx565, align 4, !tbaa !6
  %arrayidx584 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %ref_id, i32 0, i32 %513
  %514 = load i32, i32* %list_idx, align 4, !tbaa !6
  %arrayidx585 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx584, i32 0, i32 %514
  %515 = bitcast %union.int_mv* %arrayidx583 to i8*
  %516 = bitcast %union.int_mv* %arrayidx585 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %515, i8* align 4 %516, i32 4, i1 false), !tbaa.struct !57
  br label %for.inc586

for.inc586:                                       ; preds = %for.body581
  %517 = load i32, i32* %list_idx, align 4, !tbaa !6
  %inc587 = add nsw i32 %517, 1
  store i32 %inc587, i32* %list_idx, align 4, !tbaa !6
  %518 = load i32, i32* %comp_idx, align 4, !tbaa !6
  %inc588 = add nsw i32 %518, 1
  store i32 %inc588, i32* %comp_idx, align 4, !tbaa !6
  br label %for.cond571

for.end590:                                       ; preds = %for.cond.cleanup580
  %519 = bitcast i32* %list_idx591 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %519) #6
  store i32 0, i32* %list_idx591, align 4, !tbaa !6
  br label %for.cond592

for.cond592:                                      ; preds = %for.inc607, %for.end590
  %520 = load i32, i32* %list_idx591, align 4, !tbaa !6
  %521 = load i32, i32* %idx565, align 4, !tbaa !6
  %arrayidx593 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_diff_count, i32 0, i32 %521
  %522 = load i32, i32* %arrayidx593, align 4, !tbaa !6
  %cmp594 = icmp slt i32 %520, %522
  br i1 %cmp594, label %land.rhs596, label %land.end599

land.rhs596:                                      ; preds = %for.cond592
  %523 = load i32, i32* %comp_idx, align 4, !tbaa !6
  %cmp597 = icmp slt i32 %523, 2
  br label %land.end599

land.end599:                                      ; preds = %land.rhs596, %for.cond592
  %524 = phi i1 [ false, %for.cond592 ], [ %cmp597, %land.rhs596 ]
  br i1 %524, label %for.body602, label %for.cond.cleanup601

for.cond.cleanup601:                              ; preds = %land.end599
  store i32 38, i32* %cleanup.dest.slot, align 4
  %525 = bitcast i32* %list_idx591 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %525) #6
  br label %for.end611

for.body602:                                      ; preds = %land.end599
  %526 = load i32, i32* %comp_idx, align 4, !tbaa !6
  %arrayidx603 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 %526
  %527 = load i32, i32* %idx565, align 4, !tbaa !6
  %arrayidx604 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx603, i32 0, i32 %527
  %528 = load i32, i32* %idx565, align 4, !tbaa !6
  %arrayidx605 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %ref_diff, i32 0, i32 %528
  %529 = load i32, i32* %list_idx591, align 4, !tbaa !6
  %arrayidx606 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx605, i32 0, i32 %529
  %530 = bitcast %union.int_mv* %arrayidx604 to i8*
  %531 = bitcast %union.int_mv* %arrayidx606 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %530, i8* align 4 %531, i32 4, i1 false), !tbaa.struct !57
  br label %for.inc607

for.inc607:                                       ; preds = %for.body602
  %532 = load i32, i32* %list_idx591, align 4, !tbaa !6
  %inc608 = add nsw i32 %532, 1
  store i32 %inc608, i32* %list_idx591, align 4, !tbaa !6
  %533 = load i32, i32* %comp_idx, align 4, !tbaa !6
  %inc609 = add nsw i32 %533, 1
  store i32 %inc609, i32* %comp_idx, align 4, !tbaa !6
  br label %for.cond592

for.end611:                                       ; preds = %for.cond.cleanup601
  br label %for.cond612

for.cond612:                                      ; preds = %for.inc619, %for.end611
  %534 = load i32, i32* %comp_idx, align 4, !tbaa !6
  %cmp613 = icmp slt i32 %534, 2
  br i1 %cmp613, label %for.body615, label %for.end621

for.body615:                                      ; preds = %for.cond612
  %535 = load i32, i32* %comp_idx, align 4, !tbaa !6
  %arrayidx616 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 %535
  %536 = load i32, i32* %idx565, align 4, !tbaa !6
  %arrayidx617 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx616, i32 0, i32 %536
  %537 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %538 = load i32, i32* %idx565, align 4, !tbaa !6
  %arrayidx618 = getelementptr inbounds %union.int_mv, %union.int_mv* %537, i32 %538
  %539 = bitcast %union.int_mv* %arrayidx617 to i8*
  %540 = bitcast %union.int_mv* %arrayidx618 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %539, i8* align 4 %540, i32 4, i1 false), !tbaa.struct !57
  br label %for.inc619

for.inc619:                                       ; preds = %for.body615
  %541 = load i32, i32* %comp_idx, align 4, !tbaa !6
  %inc620 = add nsw i32 %541, 1
  store i32 %inc620, i32* %comp_idx, align 4, !tbaa !6
  br label %for.cond612

for.end621:                                       ; preds = %for.cond612
  %542 = bitcast i32* %comp_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %542) #6
  br label %for.inc622

for.inc622:                                       ; preds = %for.end621
  %543 = load i32, i32* %idx565, align 4, !tbaa !6
  %inc623 = add nsw i32 %543, 1
  store i32 %inc623, i32* %idx565, align 4, !tbaa !6
  br label %for.cond566

for.end625:                                       ; preds = %for.cond.cleanup569
  %544 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %545 = load i8, i8* %544, align 1, !tbaa !41
  %tobool626 = icmp ne i8 %545, 0
  br i1 %tobool626, label %if.then627, label %if.else668

if.then627:                                       ; preds = %for.end625
  %arrayidx628 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 0
  %arrayidx629 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx628, i32 0, i32 0
  %as_int = bitcast %union.int_mv* %arrayidx629 to i32*
  %546 = load i32, i32* %as_int, align 16, !tbaa !41
  %547 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %arrayidx630 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %547, i32 0
  %this_mv = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx630, i32 0, i32 0
  %as_int631 = bitcast %union.int_mv* %this_mv to i32*
  %548 = load i32, i32* %as_int631, align 4, !tbaa !41
  %cmp632 = icmp eq i32 %546, %548
  br i1 %cmp632, label %land.lhs.true634, label %if.else653

land.lhs.true634:                                 ; preds = %if.then627
  %arrayidx635 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 0
  %arrayidx636 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx635, i32 0, i32 1
  %as_int637 = bitcast %union.int_mv* %arrayidx636 to i32*
  %549 = load i32, i32* %as_int637, align 4, !tbaa !41
  %550 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %arrayidx638 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %550, i32 0
  %comp_mv = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx638, i32 0, i32 1
  %as_int639 = bitcast %union.int_mv* %comp_mv to i32*
  %551 = load i32, i32* %as_int639, align 4, !tbaa !41
  %cmp640 = icmp eq i32 %549, %551
  br i1 %cmp640, label %if.then642, label %if.else653

if.then642:                                       ; preds = %land.lhs.true634
  %552 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %553 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %554 = load i8, i8* %553, align 1, !tbaa !41
  %idxprom643 = zext i8 %554 to i32
  %arrayidx644 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %552, i32 %idxprom643
  %this_mv645 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx644, i32 0, i32 0
  %arrayidx646 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 1
  %arrayidx647 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx646, i32 0, i32 0
  %555 = bitcast %union.int_mv* %this_mv645 to i8*
  %556 = bitcast %union.int_mv* %arrayidx647 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %555, i8* align 8 %556, i32 4, i1 false), !tbaa.struct !57
  %557 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %558 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %559 = load i8, i8* %558, align 1, !tbaa !41
  %idxprom648 = zext i8 %559 to i32
  %arrayidx649 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %557, i32 %idxprom648
  %comp_mv650 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx649, i32 0, i32 1
  %arrayidx651 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 1
  %arrayidx652 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx651, i32 0, i32 1
  %560 = bitcast %union.int_mv* %comp_mv650 to i8*
  %561 = bitcast %union.int_mv* %arrayidx652 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %560, i8* align 4 %561, i32 4, i1 false), !tbaa.struct !57
  br label %if.end664

if.else653:                                       ; preds = %land.lhs.true634, %if.then627
  %562 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %563 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %564 = load i8, i8* %563, align 1, !tbaa !41
  %idxprom654 = zext i8 %564 to i32
  %arrayidx655 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %562, i32 %idxprom654
  %this_mv656 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx655, i32 0, i32 0
  %arrayidx657 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 0
  %arrayidx658 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx657, i32 0, i32 0
  %565 = bitcast %union.int_mv* %this_mv656 to i8*
  %566 = bitcast %union.int_mv* %arrayidx658 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %565, i8* align 16 %566, i32 4, i1 false), !tbaa.struct !57
  %567 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %568 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %569 = load i8, i8* %568, align 1, !tbaa !41
  %idxprom659 = zext i8 %569 to i32
  %arrayidx660 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %567, i32 %idxprom659
  %comp_mv661 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx660, i32 0, i32 1
  %arrayidx662 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 0
  %arrayidx663 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx662, i32 0, i32 1
  %570 = bitcast %union.int_mv* %comp_mv661 to i8*
  %571 = bitcast %union.int_mv* %arrayidx663 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %570, i8* align 4 %571, i32 4, i1 false), !tbaa.struct !57
  br label %if.end664

if.end664:                                        ; preds = %if.else653, %if.then642
  %572 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %573 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %574 = load i8, i8* %573, align 1, !tbaa !41
  %idxprom665 = zext i8 %574 to i32
  %arrayidx666 = getelementptr inbounds i16, i16* %572, i32 %idxprom665
  store i16 2, i16* %arrayidx666, align 2, !tbaa !58
  %575 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %576 = load i8, i8* %575, align 1, !tbaa !41
  %inc667 = add i8 %576, 1
  store i8 %inc667, i8* %575, align 1, !tbaa !41
  br label %if.end692

if.else668:                                       ; preds = %for.end625
  %577 = bitcast i32* %idx669 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %577) #6
  store i32 0, i32* %idx669, align 4, !tbaa !6
  br label %for.cond670

for.cond670:                                      ; preds = %for.inc688, %if.else668
  %578 = load i32, i32* %idx669, align 4, !tbaa !6
  %cmp671 = icmp slt i32 %578, 2
  br i1 %cmp671, label %for.body674, label %for.cond.cleanup673

for.cond.cleanup673:                              ; preds = %for.cond670
  store i32 44, i32* %cleanup.dest.slot, align 4
  %579 = bitcast i32* %idx669 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %579) #6
  br label %for.end691

for.body674:                                      ; preds = %for.cond670
  %580 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %581 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %582 = load i8, i8* %581, align 1, !tbaa !41
  %idxprom675 = zext i8 %582 to i32
  %arrayidx676 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %580, i32 %idxprom675
  %this_mv677 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx676, i32 0, i32 0
  %583 = load i32, i32* %idx669, align 4, !tbaa !6
  %arrayidx678 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 %583
  %arrayidx679 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx678, i32 0, i32 0
  %584 = bitcast %union.int_mv* %this_mv677 to i8*
  %585 = bitcast %union.int_mv* %arrayidx679 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %584, i8* align 8 %585, i32 4, i1 false), !tbaa.struct !57
  %586 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %587 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %588 = load i8, i8* %587, align 1, !tbaa !41
  %idxprom680 = zext i8 %588 to i32
  %arrayidx681 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %586, i32 %idxprom680
  %comp_mv682 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx681, i32 0, i32 1
  %589 = load i32, i32* %idx669, align 4, !tbaa !6
  %arrayidx683 = getelementptr inbounds [2 x [2 x %union.int_mv]], [2 x [2 x %union.int_mv]]* %comp_list, i32 0, i32 %589
  %arrayidx684 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx683, i32 0, i32 1
  %590 = bitcast %union.int_mv* %comp_mv682 to i8*
  %591 = bitcast %union.int_mv* %arrayidx684 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %590, i8* align 4 %591, i32 4, i1 false), !tbaa.struct !57
  %592 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %593 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %594 = load i8, i8* %593, align 1, !tbaa !41
  %idxprom685 = zext i8 %594 to i32
  %arrayidx686 = getelementptr inbounds i16, i16* %592, i32 %idxprom685
  store i16 2, i16* %arrayidx686, align 2, !tbaa !58
  %595 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %596 = load i8, i8* %595, align 1, !tbaa !41
  %inc687 = add i8 %596, 1
  store i8 %inc687, i8* %595, align 1, !tbaa !41
  br label %for.inc688

for.inc688:                                       ; preds = %for.body674
  %597 = load i32, i32* %idx669, align 4, !tbaa !6
  %inc689 = add nsw i32 %597, 1
  store i32 %inc689, i32* %idx669, align 4, !tbaa !6
  br label %for.cond670

for.end691:                                       ; preds = %for.cond.cleanup673
  br label %if.end692

if.end692:                                        ; preds = %for.end691, %if.end664
  %598 = bitcast [2 x [2 x %union.int_mv]]* %comp_list to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %598) #6
  %599 = bitcast [2 x i32]* %ref_diff_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %599) #6
  %600 = bitcast [2 x i32]* %ref_id_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %600) #6
  %601 = bitcast [2 x [2 x %union.int_mv]]* %ref_diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %601) #6
  %602 = bitcast [2 x [2 x %union.int_mv]]* %ref_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %602) #6
  br label %if.end693

if.end693:                                        ; preds = %if.end692, %if.then505
  %603 = bitcast i32* %idx694 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %603) #6
  store i32 0, i32* %idx694, align 4, !tbaa !6
  br label %for.cond695

for.cond695:                                      ; preds = %for.inc718, %if.end693
  %604 = load i32, i32* %idx694, align 4, !tbaa !6
  %605 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %606 = load i8, i8* %605, align 1, !tbaa !41
  %conv696 = zext i8 %606 to i32
  %cmp697 = icmp slt i32 %604, %conv696
  br i1 %cmp697, label %for.body700, label %for.cond.cleanup699

for.cond.cleanup699:                              ; preds = %for.cond695
  store i32 47, i32* %cleanup.dest.slot, align 4
  %607 = bitcast i32* %idx694 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %607) #6
  br label %for.end721

for.body700:                                      ; preds = %for.cond695
  %608 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %609 = load i32, i32* %idx694, align 4, !tbaa !6
  %arrayidx701 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %608, i32 %609
  %this_mv702 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx701, i32 0, i32 0
  %as_mv = bitcast %union.int_mv* %this_mv702 to %struct.mv*
  %610 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width703 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %610, i32 0, i32 33
  %611 = load i8, i8* %width703, align 4, !tbaa !60
  %conv704 = zext i8 %611 to i32
  %shl705 = shl i32 %conv704, 2
  %612 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height706 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %612, i32 0, i32 34
  %613 = load i8, i8* %height706, align 1, !tbaa !61
  %conv707 = zext i8 %613 to i32
  %shl708 = shl i32 %conv707, 2
  %614 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  call void @clamp_mv_ref(%struct.mv* %as_mv, i32 %shl705, i32 %shl708, %struct.macroblockd* %614)
  %615 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %616 = load i32, i32* %idx694, align 4, !tbaa !6
  %arrayidx709 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %615, i32 %616
  %comp_mv710 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx709, i32 0, i32 1
  %as_mv711 = bitcast %union.int_mv* %comp_mv710 to %struct.mv*
  %617 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width712 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %617, i32 0, i32 33
  %618 = load i8, i8* %width712, align 4, !tbaa !60
  %conv713 = zext i8 %618 to i32
  %shl714 = shl i32 %conv713, 2
  %619 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height715 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %619, i32 0, i32 34
  %620 = load i8, i8* %height715, align 1, !tbaa !61
  %conv716 = zext i8 %620 to i32
  %shl717 = shl i32 %conv716, 2
  %621 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  call void @clamp_mv_ref(%struct.mv* %as_mv711, i32 %shl714, i32 %shl717, %struct.macroblockd* %621)
  br label %for.inc718

for.inc718:                                       ; preds = %for.body700
  %622 = load i32, i32* %idx694, align 4, !tbaa !6
  %inc719 = add nsw i32 %622, 1
  store i32 %inc719, i32* %idx694, align 4, !tbaa !6
  br label %for.cond695

for.end721:                                       ; preds = %for.cond.cleanup699
  br label %if.end843

if.else722:                                       ; preds = %cond.end499
  %623 = bitcast i32* %idx723 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %623) #6
  store i32 0, i32* %idx723, align 4, !tbaa !6
  br label %for.cond724

for.cond724:                                      ; preds = %for.body738, %if.else722
  %624 = load i32, i32* %max_row_offset, align 4, !tbaa !6
  %call725 = call i32 @abs(i32 %624) #7
  %cmp726 = icmp sge i32 %call725, 1
  br i1 %cmp726, label %land.lhs.true728, label %land.end735

land.lhs.true728:                                 ; preds = %for.cond724
  %625 = load i32, i32* %idx723, align 4, !tbaa !6
  %626 = load i32, i32* %mi_size, align 4, !tbaa !6
  %cmp729 = icmp slt i32 %625, %626
  br i1 %cmp729, label %land.rhs731, label %land.end735

land.rhs731:                                      ; preds = %land.lhs.true728
  %627 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %628 = load i8, i8* %627, align 1, !tbaa !41
  %conv732 = zext i8 %628 to i32
  %cmp733 = icmp slt i32 %conv732, 2
  br label %land.end735

land.end735:                                      ; preds = %land.rhs731, %land.lhs.true728, %for.cond724
  %629 = phi i1 [ false, %land.lhs.true728 ], [ false, %for.cond724 ], [ %cmp733, %land.rhs731 ]
  br i1 %629, label %for.body738, label %for.cond.cleanup737

for.cond.cleanup737:                              ; preds = %land.end735
  store i32 50, i32* %cleanup.dest.slot, align 4
  %630 = bitcast i32* %idx723 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %630) #6
  br label %for.end751

for.body738:                                      ; preds = %land.end735
  %631 = bitcast %struct.MB_MODE_INFO** %candidate739 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %631) #6
  %632 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi740 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %632, i32 0, i32 6
  %633 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi740, align 4, !tbaa !67
  %634 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride741 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %634, i32 0, i32 2
  %635 = load i32, i32* %mi_stride741, align 8, !tbaa !68
  %sub742 = sub nsw i32 0, %635
  %636 = load i32, i32* %idx723, align 4, !tbaa !6
  %add743 = add nsw i32 %sub742, %636
  %arrayidx744 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %633, i32 %add743
  %637 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx744, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %637, %struct.MB_MODE_INFO** %candidate739, align 4, !tbaa !2
  %638 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate739, align 4, !tbaa !2
  %639 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %640 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %641 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %642 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %643 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  call void @process_single_ref_mv_candidate(%struct.MB_MODE_INFO* %638, %struct.AV1Common* %639, i8 signext %640, i8* %641, %struct.candidate_mv* %642, i16* %643)
  %644 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate739, align 4, !tbaa !2
  %sb_type745 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %644, i32 0, i32 6
  %645 = load i8, i8* %sb_type745, align 2, !tbaa !48
  %idxprom746 = zext i8 %645 to i32
  %arrayidx747 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom746
  %646 = load i8, i8* %arrayidx747, align 1, !tbaa !41
  %conv748 = zext i8 %646 to i32
  %647 = load i32, i32* %idx723, align 4, !tbaa !6
  %add749 = add nsw i32 %647, %conv748
  store i32 %add749, i32* %idx723, align 4, !tbaa !6
  %648 = bitcast %struct.MB_MODE_INFO** %candidate739 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %648) #6
  br label %for.cond724

for.end751:                                       ; preds = %for.cond.cleanup737
  %649 = bitcast i32* %idx752 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %649) #6
  store i32 0, i32* %idx752, align 4, !tbaa !6
  br label %for.cond753

for.cond753:                                      ; preds = %for.body767, %for.end751
  %650 = load i32, i32* %max_col_offset, align 4, !tbaa !6
  %call754 = call i32 @abs(i32 %650) #7
  %cmp755 = icmp sge i32 %call754, 1
  br i1 %cmp755, label %land.lhs.true757, label %land.end764

land.lhs.true757:                                 ; preds = %for.cond753
  %651 = load i32, i32* %idx752, align 4, !tbaa !6
  %652 = load i32, i32* %mi_size, align 4, !tbaa !6
  %cmp758 = icmp slt i32 %651, %652
  br i1 %cmp758, label %land.rhs760, label %land.end764

land.rhs760:                                      ; preds = %land.lhs.true757
  %653 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %654 = load i8, i8* %653, align 1, !tbaa !41
  %conv761 = zext i8 %654 to i32
  %cmp762 = icmp slt i32 %conv761, 2
  br label %land.end764

land.end764:                                      ; preds = %land.rhs760, %land.lhs.true757, %for.cond753
  %655 = phi i1 [ false, %land.lhs.true757 ], [ false, %for.cond753 ], [ %cmp762, %land.rhs760 ]
  br i1 %655, label %for.body767, label %for.cond.cleanup766

for.cond.cleanup766:                              ; preds = %land.end764
  store i32 52, i32* %cleanup.dest.slot, align 4
  %656 = bitcast i32* %idx752 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %656) #6
  br label %for.end780

for.body767:                                      ; preds = %land.end764
  %657 = bitcast %struct.MB_MODE_INFO** %candidate768 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %657) #6
  %658 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi769 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %658, i32 0, i32 6
  %659 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi769, align 4, !tbaa !67
  %660 = load i32, i32* %idx752, align 4, !tbaa !6
  %661 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride770 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %661, i32 0, i32 2
  %662 = load i32, i32* %mi_stride770, align 8, !tbaa !68
  %mul771 = mul nsw i32 %660, %662
  %sub772 = sub nsw i32 %mul771, 1
  %arrayidx773 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %659, i32 %sub772
  %663 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx773, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %663, %struct.MB_MODE_INFO** %candidate768, align 4, !tbaa !2
  %664 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate768, align 4, !tbaa !2
  %665 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %666 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %667 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %668 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %669 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  call void @process_single_ref_mv_candidate(%struct.MB_MODE_INFO* %664, %struct.AV1Common* %665, i8 signext %666, i8* %667, %struct.candidate_mv* %668, i16* %669)
  %670 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate768, align 4, !tbaa !2
  %sb_type774 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %670, i32 0, i32 6
  %671 = load i8, i8* %sb_type774, align 2, !tbaa !48
  %idxprom775 = zext i8 %671 to i32
  %arrayidx776 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom775
  %672 = load i8, i8* %arrayidx776, align 1, !tbaa !41
  %conv777 = zext i8 %672 to i32
  %673 = load i32, i32* %idx752, align 4, !tbaa !6
  %add778 = add nsw i32 %673, %conv777
  store i32 %add778, i32* %idx752, align 4, !tbaa !6
  %674 = bitcast %struct.MB_MODE_INFO** %candidate768 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %674) #6
  br label %for.cond753

for.end780:                                       ; preds = %for.cond.cleanup766
  %675 = bitcast i32* %idx781 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %675) #6
  store i32 0, i32* %idx781, align 4, !tbaa !6
  br label %for.cond782

for.cond782:                                      ; preds = %for.inc797, %for.end780
  %676 = load i32, i32* %idx781, align 4, !tbaa !6
  %677 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %678 = load i8, i8* %677, align 1, !tbaa !41
  %conv783 = zext i8 %678 to i32
  %cmp784 = icmp slt i32 %676, %conv783
  br i1 %cmp784, label %for.body787, label %for.cond.cleanup786

for.cond.cleanup786:                              ; preds = %for.cond782
  store i32 54, i32* %cleanup.dest.slot, align 4
  %679 = bitcast i32* %idx781 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %679) #6
  br label %for.end800

for.body787:                                      ; preds = %for.cond782
  %680 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %681 = load i32, i32* %idx781, align 4, !tbaa !6
  %arrayidx788 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %680, i32 %681
  %this_mv789 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx788, i32 0, i32 0
  %as_mv790 = bitcast %union.int_mv* %this_mv789 to %struct.mv*
  %682 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width791 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %682, i32 0, i32 33
  %683 = load i8, i8* %width791, align 4, !tbaa !60
  %conv792 = zext i8 %683 to i32
  %shl793 = shl i32 %conv792, 2
  %684 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height794 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %684, i32 0, i32 34
  %685 = load i8, i8* %height794, align 1, !tbaa !61
  %conv795 = zext i8 %685 to i32
  %shl796 = shl i32 %conv795, 2
  %686 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  call void @clamp_mv_ref(%struct.mv* %as_mv790, i32 %shl793, i32 %shl796, %struct.macroblockd* %686)
  br label %for.inc797

for.inc797:                                       ; preds = %for.body787
  %687 = load i32, i32* %idx781, align 4, !tbaa !6
  %inc798 = add nsw i32 %687, 1
  store i32 %inc798, i32* %idx781, align 4, !tbaa !6
  br label %for.cond782

for.end800:                                       ; preds = %for.cond.cleanup786
  %688 = load %union.int_mv*, %union.int_mv** %mv_ref_list.addr, align 4, !tbaa !2
  %cmp801 = icmp ne %union.int_mv* %688, null
  br i1 %cmp801, label %if.then803, label %if.end842

if.then803:                                       ; preds = %for.end800
  %689 = bitcast i32* %idx804 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %689) #6
  %690 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %691 = load i8, i8* %690, align 1, !tbaa !41
  %conv805 = zext i8 %691 to i32
  store i32 %conv805, i32* %idx804, align 4, !tbaa !6
  br label %for.cond806

for.cond806:                                      ; preds = %for.inc815, %if.then803
  %692 = load i32, i32* %idx804, align 4, !tbaa !6
  %cmp807 = icmp slt i32 %692, 2
  br i1 %cmp807, label %for.body810, label %for.cond.cleanup809

for.cond.cleanup809:                              ; preds = %for.cond806
  store i32 57, i32* %cleanup.dest.slot, align 4
  %693 = bitcast i32* %idx804 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %693) #6
  br label %for.end818

for.body810:                                      ; preds = %for.cond806
  %694 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %arrayidx811 = getelementptr inbounds %union.int_mv, %union.int_mv* %694, i32 0
  %as_int812 = bitcast %union.int_mv* %arrayidx811 to i32*
  %695 = load i32, i32* %as_int812, align 4, !tbaa !41
  %696 = load %union.int_mv*, %union.int_mv** %mv_ref_list.addr, align 4, !tbaa !2
  %697 = load i32, i32* %idx804, align 4, !tbaa !6
  %arrayidx813 = getelementptr inbounds %union.int_mv, %union.int_mv* %696, i32 %697
  %as_int814 = bitcast %union.int_mv* %arrayidx813 to i32*
  store i32 %695, i32* %as_int814, align 4, !tbaa !41
  br label %for.inc815

for.inc815:                                       ; preds = %for.body810
  %698 = load i32, i32* %idx804, align 4, !tbaa !6
  %inc816 = add nsw i32 %698, 1
  store i32 %inc816, i32* %idx804, align 4, !tbaa !6
  br label %for.cond806

for.end818:                                       ; preds = %for.cond.cleanup809
  %699 = bitcast i32* %idx819 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %699) #6
  store i32 0, i32* %idx819, align 4, !tbaa !6
  br label %for.cond820

for.cond820:                                      ; preds = %for.inc838, %for.end818
  %700 = load i32, i32* %idx819, align 4, !tbaa !6
  %701 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %702 = load i8, i8* %701, align 1, !tbaa !41
  %conv821 = zext i8 %702 to i32
  %cmp822 = icmp slt i32 2, %conv821
  br i1 %cmp822, label %cond.true824, label %cond.false825

cond.true824:                                     ; preds = %for.cond820
  br label %cond.end827

cond.false825:                                    ; preds = %for.cond820
  %703 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %704 = load i8, i8* %703, align 1, !tbaa !41
  %conv826 = zext i8 %704 to i32
  br label %cond.end827

cond.end827:                                      ; preds = %cond.false825, %cond.true824
  %cond828 = phi i32 [ 2, %cond.true824 ], [ %conv826, %cond.false825 ]
  %cmp829 = icmp slt i32 %700, %cond828
  br i1 %cmp829, label %for.body832, label %for.cond.cleanup831

for.cond.cleanup831:                              ; preds = %cond.end827
  store i32 60, i32* %cleanup.dest.slot, align 4
  %705 = bitcast i32* %idx819 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %705) #6
  br label %for.end841

for.body832:                                      ; preds = %cond.end827
  %706 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %707 = load i32, i32* %idx819, align 4, !tbaa !6
  %arrayidx833 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %706, i32 %707
  %this_mv834 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx833, i32 0, i32 0
  %as_int835 = bitcast %union.int_mv* %this_mv834 to i32*
  %708 = load i32, i32* %as_int835, align 4, !tbaa !41
  %709 = load %union.int_mv*, %union.int_mv** %mv_ref_list.addr, align 4, !tbaa !2
  %710 = load i32, i32* %idx819, align 4, !tbaa !6
  %arrayidx836 = getelementptr inbounds %union.int_mv, %union.int_mv* %709, i32 %710
  %as_int837 = bitcast %union.int_mv* %arrayidx836 to i32*
  store i32 %708, i32* %as_int837, align 4, !tbaa !41
  br label %for.inc838

for.inc838:                                       ; preds = %for.body832
  %711 = load i32, i32* %idx819, align 4, !tbaa !6
  %inc839 = add nsw i32 %711, 1
  store i32 %inc839, i32* %idx819, align 4, !tbaa !6
  br label %for.cond820

for.end841:                                       ; preds = %for.cond.cleanup831
  br label %if.end842

if.end842:                                        ; preds = %for.end841, %for.end800
  br label %if.end843

if.end843:                                        ; preds = %if.end842, %for.end721
  %712 = bitcast i32* %mi_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %712) #6
  %713 = bitcast i32* %mi_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %713) #6
  %714 = bitcast i32* %mi_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %714) #6
  %715 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %715) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_match_count) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %dummy_newmv_count) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %nearest_refmv_count) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %nearest_match) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %newmv_count) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %row_match_count) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %col_match_count) #6
  %716 = bitcast i32* %processed_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %716) #6
  %717 = bitcast i32* %processed_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %717) #6
  %718 = bitcast i32* %col_adj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %718) #6
  %719 = bitcast i32* %row_adj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %719) #6
  %720 = bitcast i32* %max_col_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %720) #6
  %721 = bitcast i32* %max_row_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %721) #6
  %722 = bitcast %struct.TileInfo** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %722) #6
  %723 = bitcast [2 x i8]* %rf to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %723) #6
  %724 = bitcast i32* %has_tr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %724) #6
  %725 = bitcast i32* %bs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %725) #6
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @av1_find_best_ref_mvs(i32 %allow_hp, %union.int_mv* %mvlist, %union.int_mv* %nearest_mv, %union.int_mv* %near_mv, i32 %is_integer) #0 {
entry:
  %allow_hp.addr = alloca i32, align 4
  %mvlist.addr = alloca %union.int_mv*, align 4
  %nearest_mv.addr = alloca %union.int_mv*, align 4
  %near_mv.addr = alloca %union.int_mv*, align 4
  %is_integer.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %allow_hp, i32* %allow_hp.addr, align 4, !tbaa !6
  store %union.int_mv* %mvlist, %union.int_mv** %mvlist.addr, align 4, !tbaa !2
  store %union.int_mv* %nearest_mv, %union.int_mv** %nearest_mv.addr, align 4, !tbaa !2
  store %union.int_mv* %near_mv, %union.int_mv** %near_mv.addr, align 4, !tbaa !2
  store i32 %is_integer, i32* %is_integer.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %union.int_mv*, %union.int_mv** %mvlist.addr, align 4, !tbaa !2
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %union.int_mv, %union.int_mv* %2, i32 %3
  %as_mv = bitcast %union.int_mv* %arrayidx to %struct.mv*
  %4 = load i32, i32* %allow_hp.addr, align 4, !tbaa !6
  %5 = load i32, i32* %is_integer.addr, align 4, !tbaa !6
  call void @lower_mv_precision(%struct.mv* %as_mv, i32 %4, i32 %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load %union.int_mv*, %union.int_mv** %nearest_mv.addr, align 4, !tbaa !2
  %8 = load %union.int_mv*, %union.int_mv** %mvlist.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds %union.int_mv, %union.int_mv* %8, i32 0
  %9 = bitcast %union.int_mv* %7 to i8*
  %10 = bitcast %union.int_mv* %arrayidx1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 4, i1 false), !tbaa.struct !57
  %11 = load %union.int_mv*, %union.int_mv** %near_mv.addr, align 4, !tbaa !2
  %12 = load %union.int_mv*, %union.int_mv** %mvlist.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds %union.int_mv, %union.int_mv* %12, i32 1
  %13 = bitcast %union.int_mv* %11 to i8*
  %14 = bitcast %union.int_mv* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 4, i1 false), !tbaa.struct !57
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @lower_mv_precision(%struct.mv* %mv, i32 %allow_hp, i32 %is_integer) #3 {
entry:
  %mv.addr = alloca %struct.mv*, align 4
  %allow_hp.addr = alloca i32, align 4
  %is_integer.addr = alloca i32, align 4
  store %struct.mv* %mv, %struct.mv** %mv.addr, align 4, !tbaa !2
  store i32 %allow_hp, i32* %allow_hp.addr, align 4, !tbaa !6
  store i32 %is_integer, i32* %is_integer.addr, align 4, !tbaa !6
  %0 = load i32, i32* %is_integer.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  call void @integer_mv_precision(%struct.mv* %1)
  br label %if.end26

if.else:                                          ; preds = %entry
  %2 = load i32, i32* %allow_hp.addr, align 4, !tbaa !6
  %tobool1 = icmp ne i32 %2, 0
  br i1 %tobool1, label %if.end25, label %if.then2

if.then2:                                         ; preds = %if.else
  %3 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row = getelementptr inbounds %struct.mv, %struct.mv* %3, i32 0, i32 0
  %4 = load i16, i16* %row, align 2, !tbaa !69
  %conv = sext i16 %4 to i32
  %and = and i32 %conv, 1
  %tobool3 = icmp ne i32 %and, 0
  br i1 %tobool3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then2
  %5 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row5 = getelementptr inbounds %struct.mv, %struct.mv* %5, i32 0, i32 0
  %6 = load i16, i16* %row5, align 2, !tbaa !69
  %conv6 = sext i16 %6 to i32
  %cmp = icmp sgt i32 %conv6, 0
  %7 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 -1, i32 1
  %8 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row8 = getelementptr inbounds %struct.mv, %struct.mv* %8, i32 0, i32 0
  %9 = load i16, i16* %row8, align 2, !tbaa !69
  %conv9 = sext i16 %9 to i32
  %add = add nsw i32 %conv9, %cond
  %conv10 = trunc i32 %add to i16
  store i16 %conv10, i16* %row8, align 2, !tbaa !69
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then2
  %10 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col = getelementptr inbounds %struct.mv, %struct.mv* %10, i32 0, i32 1
  %11 = load i16, i16* %col, align 2, !tbaa !71
  %conv11 = sext i16 %11 to i32
  %and12 = and i32 %conv11, 1
  %tobool13 = icmp ne i32 %and12, 0
  br i1 %tobool13, label %if.then14, label %if.end24

if.then14:                                        ; preds = %if.end
  %12 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col15 = getelementptr inbounds %struct.mv, %struct.mv* %12, i32 0, i32 1
  %13 = load i16, i16* %col15, align 2, !tbaa !71
  %conv16 = sext i16 %13 to i32
  %cmp17 = icmp sgt i32 %conv16, 0
  %14 = zext i1 %cmp17 to i64
  %cond19 = select i1 %cmp17, i32 -1, i32 1
  %15 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col20 = getelementptr inbounds %struct.mv, %struct.mv* %15, i32 0, i32 1
  %16 = load i16, i16* %col20, align 2, !tbaa !71
  %conv21 = sext i16 %16 to i32
  %add22 = add nsw i32 %conv21, %cond19
  %conv23 = trunc i32 %add22 to i16
  store i16 %conv23, i16* %col20, align 2, !tbaa !71
  br label %if.end24

if.end24:                                         ; preds = %if.then14, %if.end
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.else
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_setup_frame_buf_refs(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame = alloca i8, align 1
  %buf = alloca %struct.RefCntBuffer*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 0
  %order_hint = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 2
  %1 = load i32, i32* %order_hint, align 4, !tbaa !72
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 13
  %3 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !32
  %order_hint1 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %3, i32 0, i32 1
  store i32 %1, i32* %order_hint1, align 4, !tbaa !73
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 0
  %display_order_hint = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame2, i32 0, i32 3
  %5 = load i32, i32* %display_order_hint, align 8, !tbaa !74
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame3 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 13
  %7 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame3, align 8, !tbaa !32
  %display_order_hint4 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %7, i32 0, i32 3
  store i32 %5, i32* %display_order_hint4, align 4, !tbaa !75
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_frame) #6
  store i8 1, i8* %ref_frame, align 1, !tbaa !41
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %conv = sext i8 %8 to i32
  %cmp = icmp sle i32 %conv, 7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %11 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %call = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %10, i8 signext %11)
  store %struct.RefCntBuffer* %call, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %12 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %cmp6 = icmp ne %struct.RefCntBuffer* %12, null
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %13 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %order_hint8 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %13, i32 0, i32 1
  %14 = load i32, i32* %order_hint8, align 4, !tbaa !73
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame9 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 13
  %16 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame9, align 8, !tbaa !32
  %ref_order_hints = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %16, i32 0, i32 2
  %17 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %conv10 = sext i8 %17 to i32
  %sub = sub nsw i32 %conv10, 1
  %arrayidx = getelementptr inbounds [7 x i32], [7 x i32]* %ref_order_hints, i32 0, i32 %sub
  store i32 %14, i32* %arrayidx, align 4, !tbaa !6
  %18 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %display_order_hint11 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %18, i32 0, i32 3
  %19 = load i32, i32* %display_order_hint11, align 4, !tbaa !75
  %20 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame12 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %20, i32 0, i32 13
  %21 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame12, align 8, !tbaa !32
  %ref_display_order_hint = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %21, i32 0, i32 4
  %22 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %conv13 = sext i8 %22 to i32
  %sub14 = sub nsw i32 %conv13, 1
  %arrayidx15 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_display_order_hint, i32 0, i32 %sub14
  store i32 %19, i32* %arrayidx15, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %23 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %24 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %inc = add i8 %24, 1
  store i8 %inc, i8* %ref_frame, align 1, !tbaa !41
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_frame) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %cm, i8 signext %ref_frame) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  %map_idx = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !41
  %0 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %2 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %call = call i32 @get_ref_frame_map_idx(%struct.AV1Common* %1, i8 signext %2)
  store i32 %call, i32* %map_idx, align 4, !tbaa !6
  %3 = load i32, i32* %map_idx, align 4, !tbaa !6
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 17
  %5 = load i32, i32* %map_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %5
  %6 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.RefCntBuffer* [ %6, %cond.true ], [ null, %cond.false ]
  %7 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret %struct.RefCntBuffer* %cond
}

; Function Attrs: nounwind
define hidden void @av1_setup_frame_sign_bias(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame = alloca i8, align 1
  %buf = alloca %struct.RefCntBuffer*, align 4
  %ref_order_hint = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_frame) #6
  store i8 1, i8* %ref_frame, align 1, !tbaa !41
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %conv = sext i8 %0 to i32
  %cmp = icmp sle i32 %conv, 7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %3 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %call = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %2, i8 signext %3)
  store %struct.RefCntBuffer* %call, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 37
  %order_hint_info = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 10
  %enable_order_hint = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %order_hint_info, i32 0, i32 0
  %5 = load i32, i32* %enable_order_hint, align 8, !tbaa !76
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.body
  %6 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %cmp2 = icmp ne %struct.RefCntBuffer* %6, null
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %7 = bitcast i32* %ref_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %order_hint = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %8, i32 0, i32 1
  %9 = load i32, i32* %order_hint, align 4, !tbaa !73
  store i32 %9, i32* %ref_order_hint, align 4, !tbaa !6
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 37
  %order_hint_info5 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params4, i32 0, i32 10
  %11 = load i32, i32* %ref_order_hint, align 4, !tbaa !6
  %12 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %12, i32 0, i32 0
  %order_hint6 = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 2
  %13 = load i32, i32* %order_hint6, align 4, !tbaa !72
  %call7 = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info5, i32 %11, i32 %13)
  %cmp8 = icmp sle i32 %call7, 0
  %14 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 0, i32 1
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_sign_bias = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 47
  %16 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %idxprom = sext i8 %16 to i32
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %ref_frame_sign_bias, i32 0, i32 %idxprom
  store i32 %cond, i32* %arrayidx, align 4, !tbaa !6
  %17 = bitcast i32* %ref_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %for.body
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_sign_bias10 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 47
  %19 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %idxprom11 = sext i8 %19 to i32
  %arrayidx12 = getelementptr inbounds [8 x i32], [8 x i32]* %ref_frame_sign_bias10, i32 0, i32 %idxprom11
  store i32 0, i32* %arrayidx12, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %20 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %21 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %inc = add i8 %21, 1
  store i8 %inc, i8* %ref_frame, align 1, !tbaa !41
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_frame) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_relative_dist(%struct.OrderHintInfo* %oh, i32 %a, i32 %b) #3 {
entry:
  %retval = alloca i32, align 4
  %oh.addr = alloca %struct.OrderHintInfo*, align 4
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  %bits = alloca i32, align 4
  %diff = alloca i32, align 4
  %m = alloca i32, align 4
  store %struct.OrderHintInfo* %oh, %struct.OrderHintInfo** %oh.addr, align 4, !tbaa !2
  store i32 %a, i32* %a.addr, align 4, !tbaa !6
  store i32 %b, i32* %b.addr, align 4, !tbaa !6
  %0 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %oh.addr, align 4, !tbaa !2
  %enable_order_hint = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %0, i32 0, i32 0
  %1 = load i32, i32* %enable_order_hint, align 4, !tbaa !77
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %oh.addr, align 4, !tbaa !2
  %order_hint_bits_minus_1 = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %3, i32 0, i32 1
  %4 = load i32, i32* %order_hint_bits_minus_1, align 4, !tbaa !78
  %add = add nsw i32 %4, 1
  store i32 %add, i32* %bits, align 4, !tbaa !6
  %5 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i32, i32* %a.addr, align 4, !tbaa !6
  %7 = load i32, i32* %b.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %6, %7
  store i32 %sub, i32* %diff, align 4, !tbaa !6
  %8 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %bits, align 4, !tbaa !6
  %sub1 = sub nsw i32 %9, 1
  %shl = shl i32 1, %sub1
  store i32 %shl, i32* %m, align 4, !tbaa !6
  %10 = load i32, i32* %diff, align 4, !tbaa !6
  %11 = load i32, i32* %m, align 4, !tbaa !6
  %sub2 = sub nsw i32 %11, 1
  %and = and i32 %10, %sub2
  %12 = load i32, i32* %diff, align 4, !tbaa !6
  %13 = load i32, i32* %m, align 4, !tbaa !6
  %and3 = and i32 %12, %13
  %sub4 = sub nsw i32 %and, %and3
  store i32 %sub4, i32* %diff, align 4, !tbaa !6
  %14 = load i32, i32* %diff, align 4, !tbaa !6
  store i32 %14, i32* %retval, align 4
  %15 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %return

return:                                           ; preds = %if.end, %if.then
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

; Function Attrs: nounwind
define hidden void @av1_setup_motion_field(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %order_hint_info = alloca %struct.OrderHintInfo*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tpl_mvs_base = alloca %struct.TPL_MV_REF*, align 4
  %size = alloca i32, align 4
  %idx = alloca i32, align 4
  %cur_order_hint = alloca i32, align 4
  %ref_buf = alloca [7 x %struct.RefCntBuffer*], align 16
  %ref_order_hint = alloca [7 x i32], align 16
  %ref_frame = alloca i32, align 4
  %ref_idx = alloca i32, align 4
  %buf = alloca %struct.RefCntBuffer*, align 4
  %order_hint9 = alloca i32, align 4
  %ref_stamp = alloca i32, align 4
  %alt_of_lst_order_hint = alloca i32, align 4
  %is_lst_overlay = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = bitcast %struct.OrderHintInfo** %order_hint_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 37
  %order_hint_info1 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 10
  store %struct.OrderHintInfo* %order_hint_info1, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_side = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 48
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %ref_frame_side, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 4 %arraydecay, i8 0, i32 8, i1 false)
  %3 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %enable_order_hint = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %3, i32 0, i32 0
  %4 = load i32, i32* %enable_order_hint, align 4, !tbaa !77
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = bitcast %struct.TPL_MV_REF** %tpl_mvs_base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tpl_mvs = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 45
  %7 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %tpl_mvs, align 4, !tbaa !79
  store %struct.TPL_MV_REF* %7, %struct.TPL_MV_REF** %tpl_mvs_base, align 4, !tbaa !2
  %8 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %10 = load i32, i32* %mi_rows, align 4, !tbaa !66
  %add = add nsw i32 %10, 32
  %shr = ashr i32 %add, 1
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 22
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params2, i32 0, i32 11
  %12 = load i32, i32* %mi_stride, align 4, !tbaa !80
  %shr3 = ashr i32 %12, 1
  %mul = mul nsw i32 %shr, %shr3
  store i32 %mul, i32* %size, align 4, !tbaa !6
  %13 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %14 = load i32, i32* %idx, align 4, !tbaa !6
  %15 = load i32, i32* %size, align 4, !tbaa !6
  %cmp = icmp slt i32 %14, %15
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %16 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %tpl_mvs_base, align 4, !tbaa !2
  %18 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %17, i32 %18
  %mfmv0 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %arrayidx, i32 0, i32 0
  %as_int = bitcast %union.int_mv* %mfmv0 to i32*
  store i32 -2147450880, i32* %as_int, align 4, !tbaa !41
  %19 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %tpl_mvs_base, align 4, !tbaa !2
  %20 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %19, i32 %20
  %ref_frame_offset = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %arrayidx4, i32 0, i32 1
  store i8 0, i8* %ref_frame_offset, align 4, !tbaa !39
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %22 = bitcast i32* %cur_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %23, i32 0, i32 13
  %24 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !32
  %order_hint = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %24, i32 0, i32 1
  %25 = load i32, i32* %order_hint, align 4, !tbaa !73
  store i32 %25, i32* %cur_order_hint, align 4, !tbaa !6
  %26 = bitcast [7 x %struct.RefCntBuffer*]* %ref_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 28, i8* %26) #6
  %27 = bitcast [7 x i32]* %ref_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 28, i8* %27) #6
  %28 = bitcast i32* %ref_frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  store i32 1, i32* %ref_frame, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc30, %for.end
  %29 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %cmp6 = icmp sle i32 %29, 7
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond5
  store i32 5, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i32* %ref_frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  br label %for.end32

for.body8:                                        ; preds = %for.cond5
  %31 = bitcast i32* %ref_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %sub = sub nsw i32 %32, 1
  store i32 %sub, i32* %ref_idx, align 4, !tbaa !6
  %33 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  %34 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %35 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %conv = trunc i32 %35 to i8
  %call = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %34, i8 signext %conv)
  store %struct.RefCntBuffer* %call, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %36 = bitcast i32* %order_hint9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  store i32 0, i32* %order_hint9, align 4, !tbaa !6
  %37 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %cmp10 = icmp ne %struct.RefCntBuffer* %37, null
  br i1 %cmp10, label %if.then12, label %if.end14

if.then12:                                        ; preds = %for.body8
  %38 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %order_hint13 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %38, i32 0, i32 1
  %39 = load i32, i32* %order_hint13, align 4, !tbaa !73
  store i32 %39, i32* %order_hint9, align 4, !tbaa !6
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %for.body8
  %40 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %41 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [7 x %struct.RefCntBuffer*], [7 x %struct.RefCntBuffer*]* %ref_buf, i32 0, i32 %41
  store %struct.RefCntBuffer* %40, %struct.RefCntBuffer** %arrayidx15, align 4, !tbaa !2
  %42 = load i32, i32* %order_hint9, align 4, !tbaa !6
  %43 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_order_hint, i32 0, i32 %43
  store i32 %42, i32* %arrayidx16, align 4, !tbaa !6
  %44 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %45 = load i32, i32* %order_hint9, align 4, !tbaa !6
  %46 = load i32, i32* %cur_order_hint, align 4, !tbaa !6
  %call17 = call i32 @get_relative_dist(%struct.OrderHintInfo* %44, i32 %45, i32 %46)
  %cmp18 = icmp sgt i32 %call17, 0
  br i1 %cmp18, label %if.then20, label %if.else

if.then20:                                        ; preds = %if.end14
  %47 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_side21 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %47, i32 0, i32 48
  %48 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [8 x i8], [8 x i8]* %ref_frame_side21, i32 0, i32 %48
  store i8 1, i8* %arrayidx22, align 1, !tbaa !41
  br label %if.end29

if.else:                                          ; preds = %if.end14
  %49 = load i32, i32* %order_hint9, align 4, !tbaa !6
  %50 = load i32, i32* %cur_order_hint, align 4, !tbaa !6
  %cmp23 = icmp eq i32 %49, %50
  br i1 %cmp23, label %if.then25, label %if.end28

if.then25:                                        ; preds = %if.else
  %51 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_side26 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %51, i32 0, i32 48
  %52 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds [8 x i8], [8 x i8]* %ref_frame_side26, i32 0, i32 %52
  store i8 -1, i8* %arrayidx27, align 1, !tbaa !41
  br label %if.end28

if.end28:                                         ; preds = %if.then25, %if.else
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.then20
  %53 = bitcast i32* %order_hint9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  %54 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast i32* %ref_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #6
  br label %for.inc30

for.inc30:                                        ; preds = %if.end29
  %56 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %inc31 = add nsw i32 %56, 1
  store i32 %inc31, i32* %ref_frame, align 4, !tbaa !6
  br label %for.cond5

for.end32:                                        ; preds = %for.cond.cleanup7
  %57 = bitcast i32* %ref_stamp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #6
  store i32 2, i32* %ref_stamp, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [7 x %struct.RefCntBuffer*], [7 x %struct.RefCntBuffer*]* %ref_buf, i32 0, i32 0
  %58 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx33, align 16, !tbaa !2
  %cmp34 = icmp ne %struct.RefCntBuffer* %58, null
  br i1 %cmp34, label %if.then36, label %if.end46

if.then36:                                        ; preds = %for.end32
  %59 = bitcast i32* %alt_of_lst_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #6
  %arrayidx37 = getelementptr inbounds [7 x %struct.RefCntBuffer*], [7 x %struct.RefCntBuffer*]* %ref_buf, i32 0, i32 0
  %60 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx37, align 16, !tbaa !2
  %ref_order_hints = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %60, i32 0, i32 2
  %arrayidx38 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_order_hints, i32 0, i32 6
  %61 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  store i32 %61, i32* %alt_of_lst_order_hint, align 4, !tbaa !6
  %62 = bitcast i32* %is_lst_overlay to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #6
  %63 = load i32, i32* %alt_of_lst_order_hint, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_order_hint, i32 0, i32 3
  %64 = load i32, i32* %arrayidx39, align 4, !tbaa !6
  %cmp40 = icmp eq i32 %63, %64
  %conv41 = zext i1 %cmp40 to i32
  store i32 %conv41, i32* %is_lst_overlay, align 4, !tbaa !6
  %65 = load i32, i32* %is_lst_overlay, align 4, !tbaa !6
  %tobool42 = icmp ne i32 %65, 0
  br i1 %tobool42, label %if.end45, label %if.then43

if.then43:                                        ; preds = %if.then36
  %66 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call44 = call i32 @motion_field_projection(%struct.AV1Common* %66, i8 signext 1, i32 2)
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %if.then36
  %67 = load i32, i32* %ref_stamp, align 4, !tbaa !6
  %dec = add nsw i32 %67, -1
  store i32 %dec, i32* %ref_stamp, align 4, !tbaa !6
  %68 = bitcast i32* %is_lst_overlay to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast i32* %alt_of_lst_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %for.end32
  %70 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_order_hint, i32 0, i32 4
  %71 = load i32, i32* %arrayidx47, align 16, !tbaa !6
  %72 = load i32, i32* %cur_order_hint, align 4, !tbaa !6
  %call48 = call i32 @get_relative_dist(%struct.OrderHintInfo* %70, i32 %71, i32 %72)
  %cmp49 = icmp sgt i32 %call48, 0
  br i1 %cmp49, label %if.then51, label %if.end57

if.then51:                                        ; preds = %if.end46
  %73 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call52 = call i32 @motion_field_projection(%struct.AV1Common* %73, i8 signext 5, i32 0)
  %tobool53 = icmp ne i32 %call52, 0
  br i1 %tobool53, label %if.then54, label %if.end56

if.then54:                                        ; preds = %if.then51
  %74 = load i32, i32* %ref_stamp, align 4, !tbaa !6
  %dec55 = add nsw i32 %74, -1
  store i32 %dec55, i32* %ref_stamp, align 4, !tbaa !6
  br label %if.end56

if.end56:                                         ; preds = %if.then54, %if.then51
  br label %if.end57

if.end57:                                         ; preds = %if.end56, %if.end46
  %75 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_order_hint, i32 0, i32 5
  %76 = load i32, i32* %arrayidx58, align 4, !tbaa !6
  %77 = load i32, i32* %cur_order_hint, align 4, !tbaa !6
  %call59 = call i32 @get_relative_dist(%struct.OrderHintInfo* %75, i32 %76, i32 %77)
  %cmp60 = icmp sgt i32 %call59, 0
  br i1 %cmp60, label %if.then62, label %if.end68

if.then62:                                        ; preds = %if.end57
  %78 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call63 = call i32 @motion_field_projection(%struct.AV1Common* %78, i8 signext 6, i32 0)
  %tobool64 = icmp ne i32 %call63, 0
  br i1 %tobool64, label %if.then65, label %if.end67

if.then65:                                        ; preds = %if.then62
  %79 = load i32, i32* %ref_stamp, align 4, !tbaa !6
  %dec66 = add nsw i32 %79, -1
  store i32 %dec66, i32* %ref_stamp, align 4, !tbaa !6
  br label %if.end67

if.end67:                                         ; preds = %if.then65, %if.then62
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %if.end57
  %80 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_order_hint, i32 0, i32 6
  %81 = load i32, i32* %arrayidx69, align 8, !tbaa !6
  %82 = load i32, i32* %cur_order_hint, align 4, !tbaa !6
  %call70 = call i32 @get_relative_dist(%struct.OrderHintInfo* %80, i32 %81, i32 %82)
  %cmp71 = icmp sgt i32 %call70, 0
  br i1 %cmp71, label %land.lhs.true, label %if.end81

land.lhs.true:                                    ; preds = %if.end68
  %83 = load i32, i32* %ref_stamp, align 4, !tbaa !6
  %cmp73 = icmp sge i32 %83, 0
  br i1 %cmp73, label %if.then75, label %if.end81

if.then75:                                        ; preds = %land.lhs.true
  %84 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call76 = call i32 @motion_field_projection(%struct.AV1Common* %84, i8 signext 7, i32 0)
  %tobool77 = icmp ne i32 %call76, 0
  br i1 %tobool77, label %if.then78, label %if.end80

if.then78:                                        ; preds = %if.then75
  %85 = load i32, i32* %ref_stamp, align 4, !tbaa !6
  %dec79 = add nsw i32 %85, -1
  store i32 %dec79, i32* %ref_stamp, align 4, !tbaa !6
  br label %if.end80

if.end80:                                         ; preds = %if.then78, %if.then75
  br label %if.end81

if.end81:                                         ; preds = %if.end80, %land.lhs.true, %if.end68
  %86 = load i32, i32* %ref_stamp, align 4, !tbaa !6
  %cmp82 = icmp sge i32 %86, 0
  br i1 %cmp82, label %if.then84, label %if.end86

if.then84:                                        ; preds = %if.end81
  %87 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call85 = call i32 @motion_field_projection(%struct.AV1Common* %87, i8 signext 2, i32 2)
  br label %if.end86

if.end86:                                         ; preds = %if.then84, %if.end81
  %88 = bitcast i32* %ref_stamp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #6
  %89 = bitcast [7 x i32]* %ref_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 28, i8* %89) #6
  %90 = bitcast [7 x %struct.RefCntBuffer*]* %ref_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 28, i8* %90) #6
  %91 = bitcast i32* %cur_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #6
  %92 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #6
  %93 = bitcast %struct.TPL_MV_REF** %tpl_mvs_base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end86, %if.then
  %94 = bitcast %struct.OrderHintInfo** %order_hint_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: nounwind
define internal i32 @motion_field_projection(%struct.AV1Common* %cm, i8 signext %start_frame, i32 %dir) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %start_frame.addr = alloca i8, align 1
  %dir.addr = alloca i32, align 4
  %tpl_mvs_base = alloca %struct.TPL_MV_REF*, align 4
  %ref_offset = alloca [8 x i32], align 16
  %start_frame_buf = alloca %struct.RefCntBuffer*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %start_frame_order_hint = alloca i32, align 4
  %ref_order_hints = alloca i32*, align 4
  %cur_order_hint = alloca i32, align 4
  %start_to_current_frame_offset = alloca i32, align 4
  %rf = alloca i8, align 1
  %mv_ref_base = alloca %struct.MV_REF*, align 4
  %mvs_rows = alloca i32, align 4
  %mvs_cols = alloca i32, align 4
  %blk_row = alloca i32, align 4
  %blk_col = alloca i32, align 4
  %mv_ref = alloca %struct.MV_REF*, align 4
  %fwd_mv = alloca %struct.mv, align 2
  %this_mv = alloca %union.int_mv, align 4
  %mi_r = alloca i32, align 4
  %mi_c = alloca i32, align 4
  %ref_frame_offset = alloca i32, align 4
  %pos_valid = alloca i32, align 4
  %mi_offset = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %start_frame, i8* %start_frame.addr, align 1, !tbaa !41
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  %0 = bitcast %struct.TPL_MV_REF** %tpl_mvs_base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tpl_mvs = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 45
  %2 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %tpl_mvs, align 4, !tbaa !79
  store %struct.TPL_MV_REF* %2, %struct.TPL_MV_REF** %tpl_mvs_base, align 4, !tbaa !2
  %3 = bitcast [8 x i32]* %ref_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %3) #6
  %4 = bitcast [8 x i32]* %ref_offset to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %4, i8 0, i32 32, i1 false)
  %5 = bitcast %struct.RefCntBuffer** %start_frame_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %7 = load i8, i8* %start_frame.addr, align 1, !tbaa !41
  %call = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %6, i8 signext %7)
  store %struct.RefCntBuffer* %call, %struct.RefCntBuffer** %start_frame_buf, align 4, !tbaa !2
  %8 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %start_frame_buf, align 4, !tbaa !2
  %cmp = icmp eq %struct.RefCntBuffer* %8, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %9 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %start_frame_buf, align 4, !tbaa !2
  %frame_type = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %9, i32 0, i32 18
  %10 = load i8, i8* %frame_type, align 4, !tbaa !81
  %conv = zext i8 %10 to i32
  %cmp1 = icmp eq i32 %conv, 0
  br i1 %cmp1, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %11 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %start_frame_buf, align 4, !tbaa !2
  %frame_type3 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %11, i32 0, i32 18
  %12 = load i8, i8* %frame_type3, align 4, !tbaa !81
  %conv4 = zext i8 %12 to i32
  %cmp5 = icmp eq i32 %conv4, 2
  br i1 %cmp5, label %if.then7, label %if.end8

if.then7:                                         ; preds = %lor.lhs.false, %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %lor.lhs.false
  %13 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %start_frame_buf, align 4, !tbaa !2
  %mi_rows = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %13, i32 0, i32 8
  %14 = load i32, i32* %mi_rows, align 4, !tbaa !82
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 22
  %mi_rows9 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %16 = load i32, i32* %mi_rows9, align 4, !tbaa !66
  %cmp10 = icmp ne i32 %14, %16
  br i1 %cmp10, label %if.then17, label %lor.lhs.false12

lor.lhs.false12:                                  ; preds = %if.end8
  %17 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %start_frame_buf, align 4, !tbaa !2
  %mi_cols = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %17, i32 0, i32 9
  %18 = load i32, i32* %mi_cols, align 4, !tbaa !83
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params13 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %19, i32 0, i32 22
  %mi_cols14 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params13, i32 0, i32 4
  %20 = load i32, i32* %mi_cols14, align 4, !tbaa !8
  %cmp15 = icmp ne i32 %18, %20
  br i1 %cmp15, label %if.then17, label %if.end18

if.then17:                                        ; preds = %lor.lhs.false12, %if.end8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %lor.lhs.false12
  %21 = bitcast i32* %start_frame_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %start_frame_buf, align 4, !tbaa !2
  %order_hint = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %22, i32 0, i32 1
  %23 = load i32, i32* %order_hint, align 4, !tbaa !73
  store i32 %23, i32* %start_frame_order_hint, align 4, !tbaa !6
  %24 = bitcast i32** %ref_order_hints to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %start_frame_buf, align 4, !tbaa !2
  %ref_order_hints19 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %25, i32 0, i32 2
  %arrayidx = getelementptr inbounds [7 x i32], [7 x i32]* %ref_order_hints19, i32 0, i32 0
  store i32* %arrayidx, i32** %ref_order_hints, align 4, !tbaa !2
  %26 = bitcast i32* %cur_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %27, i32 0, i32 13
  %28 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !32
  %order_hint20 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %28, i32 0, i32 1
  %29 = load i32, i32* %order_hint20, align 4, !tbaa !73
  store i32 %29, i32* %cur_order_hint, align 4, !tbaa !6
  %30 = bitcast i32* %start_to_current_frame_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %31, i32 0, i32 37
  %order_hint_info = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 10
  %32 = load i32, i32* %start_frame_order_hint, align 4, !tbaa !6
  %33 = load i32, i32* %cur_order_hint, align 4, !tbaa !6
  %call21 = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info, i32 %32, i32 %33)
  store i32 %call21, i32* %start_to_current_frame_offset, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %rf) #6
  store i8 1, i8* %rf, align 1, !tbaa !41
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end18
  %34 = load i8, i8* %rf, align 1, !tbaa !41
  %conv22 = sext i8 %34 to i32
  %cmp23 = icmp sle i32 %conv22, 7
  br i1 %cmp23, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %rf) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %35 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params25 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %35, i32 0, i32 37
  %order_hint_info26 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params25, i32 0, i32 10
  %36 = load i32, i32* %start_frame_order_hint, align 4, !tbaa !6
  %37 = load i32*, i32** %ref_order_hints, align 4, !tbaa !2
  %38 = load i8, i8* %rf, align 1, !tbaa !41
  %conv27 = sext i8 %38 to i32
  %sub = sub nsw i32 %conv27, 1
  %arrayidx28 = getelementptr inbounds i32, i32* %37, i32 %sub
  %39 = load i32, i32* %arrayidx28, align 4, !tbaa !6
  %call29 = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info26, i32 %36, i32 %39)
  %40 = load i8, i8* %rf, align 1, !tbaa !41
  %idxprom = sext i8 %40 to i32
  %arrayidx30 = getelementptr inbounds [8 x i32], [8 x i32]* %ref_offset, i32 0, i32 %idxprom
  store i32 %call29, i32* %arrayidx30, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %41 = load i8, i8* %rf, align 1, !tbaa !41
  %inc = add i8 %41, 1
  store i8 %inc, i8* %rf, align 1, !tbaa !41
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %42 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %cmp31 = icmp eq i32 %42, 2
  br i1 %cmp31, label %if.then33, label %if.end35

if.then33:                                        ; preds = %for.end
  %43 = load i32, i32* %start_to_current_frame_offset, align 4, !tbaa !6
  %sub34 = sub nsw i32 0, %43
  store i32 %sub34, i32* %start_to_current_frame_offset, align 4, !tbaa !6
  br label %if.end35

if.end35:                                         ; preds = %if.then33, %for.end
  %44 = bitcast %struct.MV_REF** %mv_ref_base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #6
  %45 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %start_frame_buf, align 4, !tbaa !2
  %mvs = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %45, i32 0, i32 5
  %46 = load %struct.MV_REF*, %struct.MV_REF** %mvs, align 4, !tbaa !33
  store %struct.MV_REF* %46, %struct.MV_REF** %mv_ref_base, align 4, !tbaa !2
  %47 = bitcast i32* %mvs_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  %48 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params36 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %48, i32 0, i32 22
  %mi_rows37 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params36, i32 0, i32 3
  %49 = load i32, i32* %mi_rows37, align 4, !tbaa !66
  %add = add nsw i32 %49, 1
  %shr = ashr i32 %add, 1
  store i32 %shr, i32* %mvs_rows, align 4, !tbaa !6
  %50 = bitcast i32* %mvs_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #6
  %51 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params38 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %51, i32 0, i32 22
  %mi_cols39 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params38, i32 0, i32 4
  %52 = load i32, i32* %mi_cols39, align 4, !tbaa !8
  %add40 = add nsw i32 %52, 1
  %shr41 = ashr i32 %add40, 1
  store i32 %shr41, i32* %mvs_cols, align 4, !tbaa !6
  %53 = bitcast i32* %blk_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #6
  store i32 0, i32* %blk_row, align 4, !tbaa !6
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc96, %if.end35
  %54 = load i32, i32* %blk_row, align 4, !tbaa !6
  %55 = load i32, i32* %mvs_rows, align 4, !tbaa !6
  %cmp43 = icmp slt i32 %54, %55
  br i1 %cmp43, label %for.body46, label %for.cond.cleanup45

for.cond.cleanup45:                               ; preds = %for.cond42
  store i32 5, i32* %cleanup.dest.slot, align 4
  %56 = bitcast i32* %blk_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  br label %for.end98

for.body46:                                       ; preds = %for.cond42
  %57 = bitcast i32* %blk_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #6
  store i32 0, i32* %blk_col, align 4, !tbaa !6
  br label %for.cond47

for.cond47:                                       ; preds = %for.inc93, %for.body46
  %58 = load i32, i32* %blk_col, align 4, !tbaa !6
  %59 = load i32, i32* %mvs_cols, align 4, !tbaa !6
  %cmp48 = icmp slt i32 %58, %59
  br i1 %cmp48, label %for.body51, label %for.cond.cleanup50

for.cond.cleanup50:                               ; preds = %for.cond47
  store i32 8, i32* %cleanup.dest.slot, align 4
  %60 = bitcast i32* %blk_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  br label %for.end95

for.body51:                                       ; preds = %for.cond47
  %61 = bitcast %struct.MV_REF** %mv_ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  %62 = load %struct.MV_REF*, %struct.MV_REF** %mv_ref_base, align 4, !tbaa !2
  %63 = load i32, i32* %blk_row, align 4, !tbaa !6
  %64 = load i32, i32* %mvs_cols, align 4, !tbaa !6
  %mul = mul nsw i32 %63, %64
  %65 = load i32, i32* %blk_col, align 4, !tbaa !6
  %add52 = add nsw i32 %mul, %65
  %arrayidx53 = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %62, i32 %add52
  store %struct.MV_REF* %arrayidx53, %struct.MV_REF** %mv_ref, align 4, !tbaa !2
  %66 = bitcast %struct.mv* %fwd_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  %67 = load %struct.MV_REF*, %struct.MV_REF** %mv_ref, align 4, !tbaa !2
  %mv = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %67, i32 0, i32 0
  %as_mv = bitcast %union.int_mv* %mv to %struct.mv*
  %68 = bitcast %struct.mv* %fwd_mv to i8*
  %69 = bitcast %struct.mv* %as_mv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %68, i8* align 4 %69, i32 4, i1 false), !tbaa.struct !84
  %70 = load %struct.MV_REF*, %struct.MV_REF** %mv_ref, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %70, i32 0, i32 1
  %71 = load i8, i8* %ref_frame, align 4, !tbaa !39
  %conv54 = sext i8 %71 to i32
  %cmp55 = icmp sgt i32 %conv54, 0
  br i1 %cmp55, label %if.then57, label %if.end92

if.then57:                                        ; preds = %for.body51
  %72 = bitcast %union.int_mv* %this_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #6
  %73 = bitcast i32* %mi_r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  %74 = bitcast i32* %mi_c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #6
  %75 = bitcast i32* %ref_frame_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #6
  %76 = load %struct.MV_REF*, %struct.MV_REF** %mv_ref, align 4, !tbaa !2
  %ref_frame58 = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %76, i32 0, i32 1
  %77 = load i8, i8* %ref_frame58, align 4, !tbaa !39
  %idxprom59 = sext i8 %77 to i32
  %arrayidx60 = getelementptr inbounds [8 x i32], [8 x i32]* %ref_offset, i32 0, i32 %idxprom59
  %78 = load i32, i32* %arrayidx60, align 4, !tbaa !6
  store i32 %78, i32* %ref_frame_offset, align 4, !tbaa !6
  %79 = bitcast i32* %pos_valid to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #6
  %80 = load i32, i32* %ref_frame_offset, align 4, !tbaa !6
  %call61 = call i32 @abs(i32 %80) #7
  %cmp62 = icmp sle i32 %call61, 31
  br i1 %cmp62, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %if.then57
  %81 = load i32, i32* %ref_frame_offset, align 4, !tbaa !6
  %cmp64 = icmp sgt i32 %81, 0
  br i1 %cmp64, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %82 = load i32, i32* %start_to_current_frame_offset, align 4, !tbaa !6
  %call66 = call i32 @abs(i32 %82) #7
  %cmp67 = icmp sle i32 %call66, 31
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %if.then57
  %83 = phi i1 [ false, %land.lhs.true ], [ false, %if.then57 ], [ %cmp67, %land.rhs ]
  %land.ext = zext i1 %83 to i32
  store i32 %land.ext, i32* %pos_valid, align 4, !tbaa !6
  %84 = load i32, i32* %pos_valid, align 4, !tbaa !6
  %tobool = icmp ne i32 %84, 0
  br i1 %tobool, label %if.then69, label %if.end74

if.then69:                                        ; preds = %land.end
  %as_mv70 = bitcast %union.int_mv* %this_mv to %struct.mv*
  %85 = load i32, i32* %start_to_current_frame_offset, align 4, !tbaa !6
  %86 = load i32, i32* %ref_frame_offset, align 4, !tbaa !6
  call void @get_mv_projection(%struct.mv* %as_mv70, %struct.mv* byval(%struct.mv) align 2 %fwd_mv, i32 %85, i32 %86)
  %87 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %88 = load i32, i32* %blk_row, align 4, !tbaa !6
  %89 = load i32, i32* %blk_col, align 4, !tbaa !6
  %as_mv71 = bitcast %union.int_mv* %this_mv to %struct.mv*
  %90 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %shr72 = ashr i32 %90, 1
  %call73 = call i32 @get_block_position(%struct.AV1Common* %87, i32* %mi_r, i32* %mi_c, i32 %88, i32 %89, %struct.mv* byval(%struct.mv) align 2 %as_mv71, i32 %shr72)
  store i32 %call73, i32* %pos_valid, align 4, !tbaa !6
  br label %if.end74

if.end74:                                         ; preds = %if.then69, %land.end
  %91 = load i32, i32* %pos_valid, align 4, !tbaa !6
  %tobool75 = icmp ne i32 %91, 0
  br i1 %tobool75, label %if.then76, label %if.end91

if.then76:                                        ; preds = %if.end74
  %92 = bitcast i32* %mi_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #6
  %93 = load i32, i32* %mi_r, align 4, !tbaa !6
  %94 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params77 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %94, i32 0, i32 22
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params77, i32 0, i32 11
  %95 = load i32, i32* %mi_stride, align 4, !tbaa !80
  %shr78 = ashr i32 %95, 1
  %mul79 = mul nsw i32 %93, %shr78
  %96 = load i32, i32* %mi_c, align 4, !tbaa !6
  %add80 = add nsw i32 %mul79, %96
  store i32 %add80, i32* %mi_offset, align 4, !tbaa !6
  %row = getelementptr inbounds %struct.mv, %struct.mv* %fwd_mv, i32 0, i32 0
  %97 = load i16, i16* %row, align 2, !tbaa !69
  %98 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %tpl_mvs_base, align 4, !tbaa !2
  %99 = load i32, i32* %mi_offset, align 4, !tbaa !6
  %arrayidx81 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %98, i32 %99
  %mfmv0 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %arrayidx81, i32 0, i32 0
  %as_mv82 = bitcast %union.int_mv* %mfmv0 to %struct.mv*
  %row83 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv82, i32 0, i32 0
  store i16 %97, i16* %row83, align 4, !tbaa !41
  %col = getelementptr inbounds %struct.mv, %struct.mv* %fwd_mv, i32 0, i32 1
  %100 = load i16, i16* %col, align 2, !tbaa !71
  %101 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %tpl_mvs_base, align 4, !tbaa !2
  %102 = load i32, i32* %mi_offset, align 4, !tbaa !6
  %arrayidx84 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %101, i32 %102
  %mfmv085 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %arrayidx84, i32 0, i32 0
  %as_mv86 = bitcast %union.int_mv* %mfmv085 to %struct.mv*
  %col87 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv86, i32 0, i32 1
  store i16 %100, i16* %col87, align 2, !tbaa !41
  %103 = load i32, i32* %ref_frame_offset, align 4, !tbaa !6
  %conv88 = trunc i32 %103 to i8
  %104 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %tpl_mvs_base, align 4, !tbaa !2
  %105 = load i32, i32* %mi_offset, align 4, !tbaa !6
  %arrayidx89 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %104, i32 %105
  %ref_frame_offset90 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %arrayidx89, i32 0, i32 1
  store i8 %conv88, i8* %ref_frame_offset90, align 4, !tbaa !39
  %106 = bitcast i32* %mi_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #6
  br label %if.end91

if.end91:                                         ; preds = %if.then76, %if.end74
  %107 = bitcast i32* %pos_valid to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #6
  %108 = bitcast i32* %ref_frame_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #6
  %109 = bitcast i32* %mi_c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #6
  %110 = bitcast i32* %mi_r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast %union.int_mv* %this_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  br label %if.end92

if.end92:                                         ; preds = %if.end91, %for.body51
  %112 = bitcast %struct.mv* %fwd_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #6
  %113 = bitcast %struct.MV_REF** %mv_ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #6
  br label %for.inc93

for.inc93:                                        ; preds = %if.end92
  %114 = load i32, i32* %blk_col, align 4, !tbaa !6
  %inc94 = add nsw i32 %114, 1
  store i32 %inc94, i32* %blk_col, align 4, !tbaa !6
  br label %for.cond47

for.end95:                                        ; preds = %for.cond.cleanup50
  br label %for.inc96

for.inc96:                                        ; preds = %for.end95
  %115 = load i32, i32* %blk_row, align 4, !tbaa !6
  %inc97 = add nsw i32 %115, 1
  store i32 %inc97, i32* %blk_row, align 4, !tbaa !6
  br label %for.cond42

for.end98:                                        ; preds = %for.cond.cleanup45
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %116 = bitcast i32* %mvs_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #6
  %117 = bitcast i32* %mvs_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #6
  %118 = bitcast %struct.MV_REF** %mv_ref_base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #6
  %119 = bitcast i32* %start_to_current_frame_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #6
  %120 = bitcast i32* %cur_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #6
  %121 = bitcast i32** %ref_order_hints to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #6
  %122 = bitcast i32* %start_frame_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #6
  br label %cleanup

cleanup:                                          ; preds = %for.end98, %if.then17, %if.then7, %if.then
  %123 = bitcast %struct.RefCntBuffer** %start_frame_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #6
  %124 = bitcast [8 x i32]* %ref_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %124) #6
  %125 = bitcast %struct.TPL_MV_REF** %tpl_mvs_base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #6
  %126 = load i32, i32* %retval, align 4
  ret i32 %126
}

; Function Attrs: nounwind
define hidden zeroext i8 @av1_selectSamples(%struct.mv* %mv, i32* %pts, i32* %pts_inref, i32 %len, i8 zeroext %bsize) #0 {
entry:
  %retval = alloca i8, align 1
  %mv.addr = alloca %struct.mv*, align 4
  %pts.addr = alloca i32*, align 4
  %pts_inref.addr = alloca i32*, align 4
  %len.addr = alloca i32, align 4
  %bsize.addr = alloca i8, align 1
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %thresh = alloca i32, align 4
  %pts_mvd = alloca [16 x i32], align 16
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %l = alloca i32, align 4
  %ret = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.mv* %mv, %struct.mv** %mv.addr, align 4, !tbaa !2
  store i32* %pts, i32** %pts.addr, align 4, !tbaa !2
  store i32* %pts_inref, i32** %pts_inref.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !6
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !41
  %0 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %bsize.addr, align 1, !tbaa !41
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !6
  %3 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8, i8* %bsize.addr, align 1, !tbaa !41
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom1
  %5 = load i8, i8* %arrayidx2, align 1, !tbaa !41
  %conv3 = zext i8 %5 to i32
  store i32 %conv3, i32* %bh, align 4, !tbaa !6
  %6 = bitcast i32* %thresh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i32, i32* %bw, align 4, !tbaa !6
  %8 = load i32, i32* %bh, align 4, !tbaa !6
  %cmp = icmp sgt i32 %7, %8
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %9 = load i32, i32* %bw, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %10 = load i32, i32* %bh, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %9, %cond.true ], [ %10, %cond.false ]
  %call = call i32 @clamp(i32 %cond, i32 16, i32 112)
  store i32 %call, i32* %thresh, align 4, !tbaa !6
  %11 = bitcast [16 x i32]* %pts_mvd to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %11) #6
  %12 = bitcast [16 x i32]* %pts_mvd to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %12, i8 0, i32 64, i1 false)
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %len.addr, align 4, !tbaa !6
  store i32 %17, i32* %l, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ret) #6
  store i8 0, i8* %ret, align 1, !tbaa !41
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %19 = load i32, i32* %len.addr, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %18, %19
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %20 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul nsw i32 2, %21
  %arrayidx7 = getelementptr inbounds i32, i32* %20, i32 %mul
  %22 = load i32, i32* %arrayidx7, align 4, !tbaa !6
  %23 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %mul8 = mul nsw i32 2, %24
  %arrayidx9 = getelementptr inbounds i32, i32* %23, i32 %mul8
  %25 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %sub = sub nsw i32 %22, %25
  %26 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col = getelementptr inbounds %struct.mv, %struct.mv* %26, i32 0, i32 1
  %27 = load i16, i16* %col, align 2, !tbaa !71
  %conv10 = sext i16 %27 to i32
  %sub11 = sub nsw i32 %sub, %conv10
  %call12 = call i32 @abs(i32 %sub11) #7
  %28 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %mul13 = mul nsw i32 2, %29
  %add = add nsw i32 %mul13, 1
  %arrayidx14 = getelementptr inbounds i32, i32* %28, i32 %add
  %30 = load i32, i32* %arrayidx14, align 4, !tbaa !6
  %31 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %mul15 = mul nsw i32 2, %32
  %add16 = add nsw i32 %mul15, 1
  %arrayidx17 = getelementptr inbounds i32, i32* %31, i32 %add16
  %33 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %sub18 = sub nsw i32 %30, %33
  %34 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row = getelementptr inbounds %struct.mv, %struct.mv* %34, i32 0, i32 0
  %35 = load i16, i16* %row, align 2, !tbaa !69
  %conv19 = sext i16 %35 to i32
  %sub20 = sub nsw i32 %sub18, %conv19
  %call21 = call i32 @abs(i32 %sub20) #7
  %add22 = add nsw i32 %call12, %call21
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_mvd, i32 0, i32 %36
  store i32 %add22, i32* %arrayidx23, align 4, !tbaa !6
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_mvd, i32 0, i32 %37
  %38 = load i32, i32* %arrayidx24, align 4, !tbaa !6
  %39 = load i32, i32* %thresh, align 4, !tbaa !6
  %cmp25 = icmp sgt i32 %38, %39
  br i1 %cmp25, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_mvd, i32 0, i32 %40
  store i32 -1, i32* %arrayidx27, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %for.body
  %41 = load i8, i8* %ret, align 1, !tbaa !41
  %inc = add i8 %41, 1
  store i8 %inc, i8* %ret, align 1, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %inc28 = add nsw i32 %42, 1
  store i32 %inc28, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %43 = load i8, i8* %ret, align 1, !tbaa !41
  %tobool = icmp ne i8 %43, 0
  br i1 %tobool, label %if.end30, label %if.then29

if.then29:                                        ; preds = %for.end
  store i8 1, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end30:                                         ; preds = %for.end
  store i32 0, i32* %i, align 4, !tbaa !6
  %44 = load i32, i32* %l, align 4, !tbaa !6
  %sub31 = sub nsw i32 %44, 1
  store i32 %sub31, i32* %j, align 4, !tbaa !6
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc76, %if.end30
  %45 = load i32, i32* %k, align 4, !tbaa !6
  %46 = load i32, i32* %l, align 4, !tbaa !6
  %47 = load i8, i8* %ret, align 1, !tbaa !41
  %conv33 = zext i8 %47 to i32
  %sub34 = sub nsw i32 %46, %conv33
  %cmp35 = icmp slt i32 %45, %sub34
  br i1 %cmp35, label %for.body37, label %for.end78

for.body37:                                       ; preds = %for.cond32
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body37
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_mvd, i32 0, i32 %48
  %49 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %cmp39 = icmp ne i32 %49, -1
  br i1 %cmp39, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %inc41 = add nsw i32 %50, 1
  store i32 %inc41, i32* %i, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond42

while.cond42:                                     ; preds = %while.body46, %while.end
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_mvd, i32 0, i32 %51
  %52 = load i32, i32* %arrayidx43, align 4, !tbaa !6
  %cmp44 = icmp eq i32 %52, -1
  br i1 %cmp44, label %while.body46, label %while.end47

while.body46:                                     ; preds = %while.cond42
  %53 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %53, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %while.cond42

while.end47:                                      ; preds = %while.cond42
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %55 = load i32, i32* %j, align 4, !tbaa !6
  %cmp48 = icmp sgt i32 %54, %55
  br i1 %cmp48, label %if.then50, label %if.end51

if.then50:                                        ; preds = %while.end47
  br label %for.end78

if.end51:                                         ; preds = %while.end47
  %56 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx52 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_mvd, i32 0, i32 %56
  %57 = load i32, i32* %arrayidx52, align 4, !tbaa !6
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx53 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_mvd, i32 0, i32 %58
  store i32 %57, i32* %arrayidx53, align 4, !tbaa !6
  %59 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %60 = load i32, i32* %j, align 4, !tbaa !6
  %mul54 = mul nsw i32 2, %60
  %arrayidx55 = getelementptr inbounds i32, i32* %59, i32 %mul54
  %61 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %62 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %63 = load i32, i32* %i, align 4, !tbaa !6
  %mul56 = mul nsw i32 2, %63
  %arrayidx57 = getelementptr inbounds i32, i32* %62, i32 %mul56
  store i32 %61, i32* %arrayidx57, align 4, !tbaa !6
  %64 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %65 = load i32, i32* %j, align 4, !tbaa !6
  %mul58 = mul nsw i32 2, %65
  %add59 = add nsw i32 %mul58, 1
  %arrayidx60 = getelementptr inbounds i32, i32* %64, i32 %add59
  %66 = load i32, i32* %arrayidx60, align 4, !tbaa !6
  %67 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %68 = load i32, i32* %i, align 4, !tbaa !6
  %mul61 = mul nsw i32 2, %68
  %add62 = add nsw i32 %mul61, 1
  %arrayidx63 = getelementptr inbounds i32, i32* %67, i32 %add62
  store i32 %66, i32* %arrayidx63, align 4, !tbaa !6
  %69 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %70 = load i32, i32* %j, align 4, !tbaa !6
  %mul64 = mul nsw i32 2, %70
  %arrayidx65 = getelementptr inbounds i32, i32* %69, i32 %mul64
  %71 = load i32, i32* %arrayidx65, align 4, !tbaa !6
  %72 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %73 = load i32, i32* %i, align 4, !tbaa !6
  %mul66 = mul nsw i32 2, %73
  %arrayidx67 = getelementptr inbounds i32, i32* %72, i32 %mul66
  store i32 %71, i32* %arrayidx67, align 4, !tbaa !6
  %74 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %75 = load i32, i32* %j, align 4, !tbaa !6
  %mul68 = mul nsw i32 2, %75
  %add69 = add nsw i32 %mul68, 1
  %arrayidx70 = getelementptr inbounds i32, i32* %74, i32 %add69
  %76 = load i32, i32* %arrayidx70, align 4, !tbaa !6
  %77 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %78 = load i32, i32* %i, align 4, !tbaa !6
  %mul71 = mul nsw i32 2, %78
  %add72 = add nsw i32 %mul71, 1
  %arrayidx73 = getelementptr inbounds i32, i32* %77, i32 %add72
  store i32 %76, i32* %arrayidx73, align 4, !tbaa !6
  %79 = load i32, i32* %i, align 4, !tbaa !6
  %inc74 = add nsw i32 %79, 1
  store i32 %inc74, i32* %i, align 4, !tbaa !6
  %80 = load i32, i32* %j, align 4, !tbaa !6
  %dec75 = add nsw i32 %80, -1
  store i32 %dec75, i32* %j, align 4, !tbaa !6
  br label %for.inc76

for.inc76:                                        ; preds = %if.end51
  %81 = load i32, i32* %k, align 4, !tbaa !6
  %inc77 = add nsw i32 %81, 1
  store i32 %inc77, i32* %k, align 4, !tbaa !6
  br label %for.cond32

for.end78:                                        ; preds = %if.then50, %for.cond32
  %82 = load i8, i8* %ret, align 1, !tbaa !41
  store i8 %82, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end78, %if.then29
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ret) #6
  %83 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #6
  %84 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #6
  %85 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  %86 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #6
  %87 = bitcast [16 x i32]* %pts_mvd to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %87) #6
  %88 = bitcast i32* %thresh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #6
  %89 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #6
  %90 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #6
  %91 = load i8, i8* %retval, align 1
  ret i8 %91
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #3 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: nounwind
define hidden zeroext i8 @av1_findSamples(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32* %pts, i32* %pts_inref) #0 {
entry:
  %retval = alloca i8, align 1
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pts.addr = alloca i32*, align 4
  %pts_inref.addr = alloca i32*, align 4
  %mbmi0 = alloca %struct.MB_MODE_INFO*, align 4
  %ref_frame = alloca i32, align 4
  %up_available = alloca i32, align 4
  %left_available = alloca i32, align 4
  %i = alloca i32, align 4
  %mi_step = alloca i32, align 4
  %np = alloca i8, align 1
  %do_tl = alloca i32, align 4
  %do_tr = alloca i32, align 4
  %mi_stride = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %mi_row_offset = alloca i32, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %superblock_width = alloca i8, align 1
  %col_offset = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %mi_col_offset = alloca i32, align 4
  %mbmi110 = alloca %struct.MB_MODE_INFO*, align 4
  %superblock_height = alloca i8, align 1
  %row_offset = alloca i32, align 4
  %mi_row_offset228 = alloca i32, align 4
  %mi_col_offset229 = alloca i32, align 4
  %mbmi230 = alloca %struct.MB_MODE_INFO*, align 4
  %trb_pos = alloca %struct.position, align 4
  %tile = alloca %struct.TileInfo*, align 4
  %mi_row_offset286 = alloca i32, align 4
  %mi_col_offset287 = alloca i32, align 4
  %mbmi290 = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32* %pts, i32** %pts.addr, align 4, !tbaa !2
  store i32* %pts_inref, i32** %pts_inref.addr, align 4, !tbaa !2
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !67
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi0, align 4, !tbaa !2
  %4 = bitcast i32* %ref_frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi0, align 4, !tbaa !2
  %ref_frame1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 12
  %arrayidx2 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame1, i32 0, i32 0
  %6 = load i8, i8* %arrayidx2, align 4, !tbaa !41
  %conv = sext i8 %6 to i32
  store i32 %conv, i32* %ref_frame, align 4, !tbaa !6
  %7 = bitcast i32* %up_available to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %up_available3 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 7
  %9 = load i8, i8* %up_available3, align 8, !tbaa !62, !range !55
  %tobool = trunc i8 %9 to i1
  %conv4 = zext i1 %tobool to i32
  store i32 %conv4, i32* %up_available, align 4, !tbaa !6
  %10 = bitcast i32* %left_available to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_available5 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %11, i32 0, i32 8
  %12 = load i8, i8* %left_available5, align 1, !tbaa !63, !range !55
  %tobool6 = trunc i8 %12 to i1
  %conv7 = zext i1 %tobool6 to i32
  store i32 %conv7, i32* %left_available, align 4, !tbaa !6
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = bitcast i32* %mi_step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %np) #6
  store i8 0, i8* %np, align 1, !tbaa !41
  %15 = bitcast i32* %do_tl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  store i32 1, i32* %do_tl, align 4, !tbaa !6
  %16 = bitcast i32* %do_tr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  store i32 1, i32* %do_tr, align 4, !tbaa !6
  %17 = bitcast i32* %mi_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %18, i32 0, i32 2
  %19 = load i32, i32* %mi_stride8, align 8, !tbaa !68
  store i32 %19, i32* %mi_stride, align 4, !tbaa !6
  %20 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_row9 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %21, i32 0, i32 0
  %22 = load i32, i32* %mi_row9, align 16, !tbaa !42
  store i32 %22, i32* %mi_row, align 4, !tbaa !6
  %23 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_col10 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %24, i32 0, i32 1
  %25 = load i32, i32* %mi_col10, align 4, !tbaa !47
  store i32 %25, i32* %mi_col, align 4, !tbaa !6
  %26 = load i32, i32* %up_available, align 4, !tbaa !6
  %tobool11 = icmp ne i32 %26, 0
  br i1 %tobool11, label %if.then, label %if.end107

if.then:                                          ; preds = %entry
  %27 = bitcast i32* %mi_row_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  store i32 -1, i32* %mi_row_offset, align 4, !tbaa !6
  %28 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %29 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi12 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %29, i32 0, i32 6
  %30 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi12, align 4, !tbaa !67
  %31 = load i32, i32* %mi_stride, align 4, !tbaa !6
  %mul = mul nsw i32 -1, %31
  %arrayidx13 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %30, i32 %mul
  %32 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx13, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %32, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %superblock_width) #6
  %33 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %33, i32 0, i32 6
  %34 = load i8, i8* %sb_type, align 2, !tbaa !48
  %idxprom = zext i8 %34 to i32
  %arrayidx14 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom
  %35 = load i8, i8* %arrayidx14, align 1, !tbaa !41
  store i8 %35, i8* %superblock_width, align 1, !tbaa !41
  %36 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %36, i32 0, i32 33
  %37 = load i8, i8* %width, align 4, !tbaa !60
  %conv15 = zext i8 %37 to i32
  %38 = load i8, i8* %superblock_width, align 1, !tbaa !41
  %conv16 = zext i8 %38 to i32
  %cmp = icmp sle i32 %conv15, %conv16
  br i1 %cmp, label %if.then18, label %if.else

if.then18:                                        ; preds = %if.then
  %39 = bitcast i32* %col_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  %40 = load i32, i32* %mi_col, align 4, !tbaa !6
  %sub = sub nsw i32 0, %40
  %41 = load i8, i8* %superblock_width, align 1, !tbaa !41
  %conv19 = zext i8 %41 to i32
  %rem = srem i32 %sub, %conv19
  store i32 %rem, i32* %col_offset, align 4, !tbaa !6
  %42 = load i32, i32* %col_offset, align 4, !tbaa !6
  %cmp20 = icmp slt i32 %42, 0
  br i1 %cmp20, label %if.then22, label %if.end

if.then22:                                        ; preds = %if.then18
  store i32 0, i32* %do_tl, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then22, %if.then18
  %43 = load i32, i32* %col_offset, align 4, !tbaa !6
  %44 = load i8, i8* %superblock_width, align 1, !tbaa !41
  %conv23 = zext i8 %44 to i32
  %add = add nsw i32 %43, %conv23
  %45 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width24 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %45, i32 0, i32 33
  %46 = load i8, i8* %width24, align 4, !tbaa !60
  %conv25 = zext i8 %46 to i32
  %cmp26 = icmp sgt i32 %add, %conv25
  br i1 %cmp26, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.end
  store i32 0, i32* %do_tr, align 4, !tbaa !6
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %if.end
  %47 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %ref_frame30 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %47, i32 0, i32 12
  %arrayidx31 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame30, i32 0, i32 0
  %48 = load i8, i8* %arrayidx31, align 4, !tbaa !41
  %conv32 = sext i8 %48 to i32
  %49 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %cmp33 = icmp eq i32 %conv32, %49
  br i1 %cmp33, label %land.lhs.true, label %if.end47

land.lhs.true:                                    ; preds = %if.end29
  %50 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %ref_frame35 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %50, i32 0, i32 12
  %arrayidx36 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame35, i32 0, i32 1
  %51 = load i8, i8* %arrayidx36, align 1, !tbaa !41
  %conv37 = sext i8 %51 to i32
  %cmp38 = icmp eq i32 %conv37, -1
  br i1 %cmp38, label %if.then40, label %if.end47

if.then40:                                        ; preds = %land.lhs.true
  %52 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %53 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %54 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %55 = load i32, i32* %col_offset, align 4, !tbaa !6
  call void @record_samples(%struct.MB_MODE_INFO* %52, i32* %53, i32* %54, i32 0, i32 -1, i32 %55, i32 1)
  %56 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %56, i32 2
  store i32* %add.ptr, i32** %pts.addr, align 4, !tbaa !2
  %57 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %add.ptr41 = getelementptr inbounds i32, i32* %57, i32 2
  store i32* %add.ptr41, i32** %pts_inref.addr, align 4, !tbaa !2
  %58 = load i8, i8* %np, align 1, !tbaa !41
  %inc = add i8 %58, 1
  store i8 %inc, i8* %np, align 1, !tbaa !41
  %59 = load i8, i8* %np, align 1, !tbaa !41
  %conv42 = zext i8 %59 to i32
  %cmp43 = icmp sge i32 %conv42, 8
  br i1 %cmp43, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.then40
  store i8 8, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %if.then40
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %land.lhs.true, %if.end29
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end47, %if.then45
  %60 = bitcast i32* %col_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup102 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end101

if.else:                                          ; preds = %if.then
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %61 = load i32, i32* %i, align 4, !tbaa !6
  %62 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width48 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %62, i32 0, i32 33
  %63 = load i8, i8* %width48, align 4, !tbaa !60
  %conv49 = zext i8 %63 to i32
  %64 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %64, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %65 = load i32, i32* %mi_cols, align 4, !tbaa !8
  %66 = load i32, i32* %mi_col, align 4, !tbaa !6
  %sub50 = sub nsw i32 %65, %66
  %cmp51 = icmp slt i32 %conv49, %sub50
  br i1 %cmp51, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.cond
  %67 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width53 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %67, i32 0, i32 33
  %68 = load i8, i8* %width53, align 4, !tbaa !60
  %conv54 = zext i8 %68 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.cond
  %69 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params55 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %69, i32 0, i32 22
  %mi_cols56 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params55, i32 0, i32 4
  %70 = load i32, i32* %mi_cols56, align 4, !tbaa !8
  %71 = load i32, i32* %mi_col, align 4, !tbaa !6
  %sub57 = sub nsw i32 %70, %71
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv54, %cond.true ], [ %sub57, %cond.false ]
  %cmp58 = icmp slt i32 %61, %cond
  br i1 %cmp58, label %for.body, label %for.end

for.body:                                         ; preds = %cond.end
  %72 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi60 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %72, i32 0, i32 6
  %73 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi60, align 4, !tbaa !67
  %74 = load i32, i32* %i, align 4, !tbaa !6
  %75 = load i32, i32* %mi_stride, align 4, !tbaa !6
  %mul61 = mul nsw i32 -1, %75
  %add62 = add nsw i32 %74, %mul61
  %arrayidx63 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %73, i32 %add62
  %76 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx63, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %76, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %77 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %sb_type64 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %77, i32 0, i32 6
  %78 = load i8, i8* %sb_type64, align 2, !tbaa !48
  %idxprom65 = zext i8 %78 to i32
  %arrayidx66 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom65
  %79 = load i8, i8* %arrayidx66, align 1, !tbaa !41
  store i8 %79, i8* %superblock_width, align 1, !tbaa !41
  %80 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width67 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %80, i32 0, i32 33
  %81 = load i8, i8* %width67, align 4, !tbaa !60
  %conv68 = zext i8 %81 to i32
  %82 = load i8, i8* %superblock_width, align 1, !tbaa !41
  %conv69 = zext i8 %82 to i32
  %cmp70 = icmp slt i32 %conv68, %conv69
  br i1 %cmp70, label %cond.true72, label %cond.false75

cond.true72:                                      ; preds = %for.body
  %83 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width73 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %83, i32 0, i32 33
  %84 = load i8, i8* %width73, align 4, !tbaa !60
  %conv74 = zext i8 %84 to i32
  br label %cond.end77

cond.false75:                                     ; preds = %for.body
  %85 = load i8, i8* %superblock_width, align 1, !tbaa !41
  %conv76 = zext i8 %85 to i32
  br label %cond.end77

cond.end77:                                       ; preds = %cond.false75, %cond.true72
  %cond78 = phi i32 [ %conv74, %cond.true72 ], [ %conv76, %cond.false75 ]
  store i32 %cond78, i32* %mi_step, align 4, !tbaa !6
  %86 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %ref_frame79 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %86, i32 0, i32 12
  %arrayidx80 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame79, i32 0, i32 0
  %87 = load i8, i8* %arrayidx80, align 4, !tbaa !41
  %conv81 = sext i8 %87 to i32
  %88 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %cmp82 = icmp eq i32 %conv81, %88
  br i1 %cmp82, label %land.lhs.true84, label %if.end99

land.lhs.true84:                                  ; preds = %cond.end77
  %89 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %ref_frame85 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %89, i32 0, i32 12
  %arrayidx86 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame85, i32 0, i32 1
  %90 = load i8, i8* %arrayidx86, align 1, !tbaa !41
  %conv87 = sext i8 %90 to i32
  %cmp88 = icmp eq i32 %conv87, -1
  br i1 %cmp88, label %if.then90, label %if.end99

if.then90:                                        ; preds = %land.lhs.true84
  %91 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %92 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %93 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %94 = load i32, i32* %i, align 4, !tbaa !6
  call void @record_samples(%struct.MB_MODE_INFO* %91, i32* %92, i32* %93, i32 0, i32 -1, i32 %94, i32 1)
  %95 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %add.ptr91 = getelementptr inbounds i32, i32* %95, i32 2
  store i32* %add.ptr91, i32** %pts.addr, align 4, !tbaa !2
  %96 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %add.ptr92 = getelementptr inbounds i32, i32* %96, i32 2
  store i32* %add.ptr92, i32** %pts_inref.addr, align 4, !tbaa !2
  %97 = load i8, i8* %np, align 1, !tbaa !41
  %inc93 = add i8 %97, 1
  store i8 %inc93, i8* %np, align 1, !tbaa !41
  %98 = load i8, i8* %np, align 1, !tbaa !41
  %conv94 = zext i8 %98 to i32
  %cmp95 = icmp sge i32 %conv94, 8
  br i1 %cmp95, label %if.then97, label %if.end98

if.then97:                                        ; preds = %if.then90
  store i8 8, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup102

if.end98:                                         ; preds = %if.then90
  br label %if.end99

if.end99:                                         ; preds = %if.end98, %land.lhs.true84, %cond.end77
  br label %for.inc

for.inc:                                          ; preds = %if.end99
  %99 = load i32, i32* %mi_step, align 4, !tbaa !6
  %100 = load i32, i32* %i, align 4, !tbaa !6
  %add100 = add nsw i32 %100, %99
  store i32 %add100, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %cond.end
  br label %if.end101

if.end101:                                        ; preds = %for.end, %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup102

cleanup102:                                       ; preds = %if.end101, %if.then97, %cleanup
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %superblock_width) #6
  %101 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #6
  %102 = bitcast i32* %mi_row_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  %cleanup.dest105 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest105, label %cleanup327 [
    i32 0, label %cleanup.cont106
  ]

cleanup.cont106:                                  ; preds = %cleanup102
  br label %if.end107

if.end107:                                        ; preds = %cleanup.cont106, %entry
  %103 = load i32, i32* %left_available, align 4, !tbaa !6
  %tobool108 = icmp ne i32 %103, 0
  br i1 %tobool108, label %if.then109, label %if.end221

if.then109:                                       ; preds = %if.end107
  %104 = bitcast i32* %mi_col_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #6
  store i32 -1, i32* %mi_col_offset, align 4, !tbaa !6
  %105 = bitcast %struct.MB_MODE_INFO** %mbmi110 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #6
  %106 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi111 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %106, i32 0, i32 6
  %107 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi111, align 4, !tbaa !67
  %arrayidx112 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %107, i32 -1
  %108 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx112, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %108, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %superblock_height) #6
  %109 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  %sb_type113 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %109, i32 0, i32 6
  %110 = load i8, i8* %sb_type113, align 2, !tbaa !48
  %idxprom114 = zext i8 %110 to i32
  %arrayidx115 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom114
  %111 = load i8, i8* %arrayidx115, align 1, !tbaa !41
  store i8 %111, i8* %superblock_height, align 1, !tbaa !41
  %112 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %112, i32 0, i32 34
  %113 = load i8, i8* %height, align 1, !tbaa !61
  %conv116 = zext i8 %113 to i32
  %114 = load i8, i8* %superblock_height, align 1, !tbaa !41
  %conv117 = zext i8 %114 to i32
  %cmp118 = icmp sle i32 %conv116, %conv117
  br i1 %cmp118, label %if.then120, label %if.else152

if.then120:                                       ; preds = %if.then109
  %115 = bitcast i32* %row_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %115) #6
  %116 = load i32, i32* %mi_row, align 4, !tbaa !6
  %sub121 = sub nsw i32 0, %116
  %117 = load i8, i8* %superblock_height, align 1, !tbaa !41
  %conv122 = zext i8 %117 to i32
  %rem123 = srem i32 %sub121, %conv122
  store i32 %rem123, i32* %row_offset, align 4, !tbaa !6
  %118 = load i32, i32* %row_offset, align 4, !tbaa !6
  %cmp124 = icmp slt i32 %118, 0
  br i1 %cmp124, label %if.then126, label %if.end127

if.then126:                                       ; preds = %if.then120
  store i32 0, i32* %do_tl, align 4, !tbaa !6
  br label %if.end127

if.end127:                                        ; preds = %if.then126, %if.then120
  %119 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  %ref_frame128 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %119, i32 0, i32 12
  %arrayidx129 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame128, i32 0, i32 0
  %120 = load i8, i8* %arrayidx129, align 4, !tbaa !41
  %conv130 = sext i8 %120 to i32
  %121 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %cmp131 = icmp eq i32 %conv130, %121
  br i1 %cmp131, label %land.lhs.true133, label %if.end148

land.lhs.true133:                                 ; preds = %if.end127
  %122 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  %ref_frame134 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %122, i32 0, i32 12
  %arrayidx135 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame134, i32 0, i32 1
  %123 = load i8, i8* %arrayidx135, align 1, !tbaa !41
  %conv136 = sext i8 %123 to i32
  %cmp137 = icmp eq i32 %conv136, -1
  br i1 %cmp137, label %if.then139, label %if.end148

if.then139:                                       ; preds = %land.lhs.true133
  %124 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  %125 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %126 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %127 = load i32, i32* %row_offset, align 4, !tbaa !6
  call void @record_samples(%struct.MB_MODE_INFO* %124, i32* %125, i32* %126, i32 %127, i32 1, i32 0, i32 -1)
  %128 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %add.ptr140 = getelementptr inbounds i32, i32* %128, i32 2
  store i32* %add.ptr140, i32** %pts.addr, align 4, !tbaa !2
  %129 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %add.ptr141 = getelementptr inbounds i32, i32* %129, i32 2
  store i32* %add.ptr141, i32** %pts_inref.addr, align 4, !tbaa !2
  %130 = load i8, i8* %np, align 1, !tbaa !41
  %inc142 = add i8 %130, 1
  store i8 %inc142, i8* %np, align 1, !tbaa !41
  %131 = load i8, i8* %np, align 1, !tbaa !41
  %conv143 = zext i8 %131 to i32
  %cmp144 = icmp sge i32 %conv143, 8
  br i1 %cmp144, label %if.then146, label %if.end147

if.then146:                                       ; preds = %if.then139
  store i8 8, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup149

if.end147:                                        ; preds = %if.then139
  br label %if.end148

if.end148:                                        ; preds = %if.end147, %land.lhs.true133, %if.end127
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup149

cleanup149:                                       ; preds = %if.end148, %if.then146
  %132 = bitcast i32* %row_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #6
  %cleanup.dest150 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest150, label %cleanup216 [
    i32 0, label %cleanup.cont151
  ]

cleanup.cont151:                                  ; preds = %cleanup149
  br label %if.end215

if.else152:                                       ; preds = %if.then109
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond153

for.cond153:                                      ; preds = %for.inc212, %if.else152
  %133 = load i32, i32* %i, align 4, !tbaa !6
  %134 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height154 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %134, i32 0, i32 34
  %135 = load i8, i8* %height154, align 1, !tbaa !61
  %conv155 = zext i8 %135 to i32
  %136 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params156 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %136, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params156, i32 0, i32 3
  %137 = load i32, i32* %mi_rows, align 4, !tbaa !66
  %138 = load i32, i32* %mi_row, align 4, !tbaa !6
  %sub157 = sub nsw i32 %137, %138
  %cmp158 = icmp slt i32 %conv155, %sub157
  br i1 %cmp158, label %cond.true160, label %cond.false163

cond.true160:                                     ; preds = %for.cond153
  %139 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height161 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %139, i32 0, i32 34
  %140 = load i8, i8* %height161, align 1, !tbaa !61
  %conv162 = zext i8 %140 to i32
  br label %cond.end167

cond.false163:                                    ; preds = %for.cond153
  %141 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params164 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %141, i32 0, i32 22
  %mi_rows165 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params164, i32 0, i32 3
  %142 = load i32, i32* %mi_rows165, align 4, !tbaa !66
  %143 = load i32, i32* %mi_row, align 4, !tbaa !6
  %sub166 = sub nsw i32 %142, %143
  br label %cond.end167

cond.end167:                                      ; preds = %cond.false163, %cond.true160
  %cond168 = phi i32 [ %conv162, %cond.true160 ], [ %sub166, %cond.false163 ]
  %cmp169 = icmp slt i32 %133, %cond168
  br i1 %cmp169, label %for.body171, label %for.end214

for.body171:                                      ; preds = %cond.end167
  %144 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi172 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %144, i32 0, i32 6
  %145 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi172, align 4, !tbaa !67
  %146 = load i32, i32* %i, align 4, !tbaa !6
  %147 = load i32, i32* %mi_stride, align 4, !tbaa !6
  %mul173 = mul nsw i32 %146, %147
  %add174 = add nsw i32 -1, %mul173
  %arrayidx175 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %145, i32 %add174
  %148 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx175, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %148, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  %149 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  %sb_type176 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %149, i32 0, i32 6
  %150 = load i8, i8* %sb_type176, align 2, !tbaa !48
  %idxprom177 = zext i8 %150 to i32
  %arrayidx178 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom177
  %151 = load i8, i8* %arrayidx178, align 1, !tbaa !41
  store i8 %151, i8* %superblock_height, align 1, !tbaa !41
  %152 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height179 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %152, i32 0, i32 34
  %153 = load i8, i8* %height179, align 1, !tbaa !61
  %conv180 = zext i8 %153 to i32
  %154 = load i8, i8* %superblock_height, align 1, !tbaa !41
  %conv181 = zext i8 %154 to i32
  %cmp182 = icmp slt i32 %conv180, %conv181
  br i1 %cmp182, label %cond.true184, label %cond.false187

cond.true184:                                     ; preds = %for.body171
  %155 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height185 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %155, i32 0, i32 34
  %156 = load i8, i8* %height185, align 1, !tbaa !61
  %conv186 = zext i8 %156 to i32
  br label %cond.end189

cond.false187:                                    ; preds = %for.body171
  %157 = load i8, i8* %superblock_height, align 1, !tbaa !41
  %conv188 = zext i8 %157 to i32
  br label %cond.end189

cond.end189:                                      ; preds = %cond.false187, %cond.true184
  %cond190 = phi i32 [ %conv186, %cond.true184 ], [ %conv188, %cond.false187 ]
  store i32 %cond190, i32* %mi_step, align 4, !tbaa !6
  %158 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  %ref_frame191 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %158, i32 0, i32 12
  %arrayidx192 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame191, i32 0, i32 0
  %159 = load i8, i8* %arrayidx192, align 4, !tbaa !41
  %conv193 = sext i8 %159 to i32
  %160 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %cmp194 = icmp eq i32 %conv193, %160
  br i1 %cmp194, label %land.lhs.true196, label %if.end211

land.lhs.true196:                                 ; preds = %cond.end189
  %161 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  %ref_frame197 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %161, i32 0, i32 12
  %arrayidx198 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame197, i32 0, i32 1
  %162 = load i8, i8* %arrayidx198, align 1, !tbaa !41
  %conv199 = sext i8 %162 to i32
  %cmp200 = icmp eq i32 %conv199, -1
  br i1 %cmp200, label %if.then202, label %if.end211

if.then202:                                       ; preds = %land.lhs.true196
  %163 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi110, align 4, !tbaa !2
  %164 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %165 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %166 = load i32, i32* %i, align 4, !tbaa !6
  call void @record_samples(%struct.MB_MODE_INFO* %163, i32* %164, i32* %165, i32 %166, i32 1, i32 0, i32 -1)
  %167 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %add.ptr203 = getelementptr inbounds i32, i32* %167, i32 2
  store i32* %add.ptr203, i32** %pts.addr, align 4, !tbaa !2
  %168 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %add.ptr204 = getelementptr inbounds i32, i32* %168, i32 2
  store i32* %add.ptr204, i32** %pts_inref.addr, align 4, !tbaa !2
  %169 = load i8, i8* %np, align 1, !tbaa !41
  %inc205 = add i8 %169, 1
  store i8 %inc205, i8* %np, align 1, !tbaa !41
  %170 = load i8, i8* %np, align 1, !tbaa !41
  %conv206 = zext i8 %170 to i32
  %cmp207 = icmp sge i32 %conv206, 8
  br i1 %cmp207, label %if.then209, label %if.end210

if.then209:                                       ; preds = %if.then202
  store i8 8, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup216

if.end210:                                        ; preds = %if.then202
  br label %if.end211

if.end211:                                        ; preds = %if.end210, %land.lhs.true196, %cond.end189
  br label %for.inc212

for.inc212:                                       ; preds = %if.end211
  %171 = load i32, i32* %mi_step, align 4, !tbaa !6
  %172 = load i32, i32* %i, align 4, !tbaa !6
  %add213 = add nsw i32 %172, %171
  store i32 %add213, i32* %i, align 4, !tbaa !6
  br label %for.cond153

for.end214:                                       ; preds = %cond.end167
  br label %if.end215

if.end215:                                        ; preds = %for.end214, %cleanup.cont151
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup216

cleanup216:                                       ; preds = %if.end215, %if.then209, %cleanup149
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %superblock_height) #6
  %173 = bitcast %struct.MB_MODE_INFO** %mbmi110 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #6
  %174 = bitcast i32* %mi_col_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #6
  %cleanup.dest219 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest219, label %cleanup327 [
    i32 0, label %cleanup.cont220
  ]

cleanup.cont220:                                  ; preds = %cleanup216
  br label %if.end221

if.end221:                                        ; preds = %cleanup.cont220, %if.end107
  %175 = load i32, i32* %do_tl, align 4, !tbaa !6
  %tobool222 = icmp ne i32 %175, 0
  br i1 %tobool222, label %land.lhs.true223, label %if.end261

land.lhs.true223:                                 ; preds = %if.end221
  %176 = load i32, i32* %left_available, align 4, !tbaa !6
  %tobool224 = icmp ne i32 %176, 0
  br i1 %tobool224, label %land.lhs.true225, label %if.end261

land.lhs.true225:                                 ; preds = %land.lhs.true223
  %177 = load i32, i32* %up_available, align 4, !tbaa !6
  %tobool226 = icmp ne i32 %177, 0
  br i1 %tobool226, label %if.then227, label %if.end261

if.then227:                                       ; preds = %land.lhs.true225
  %178 = bitcast i32* %mi_row_offset228 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %178) #6
  store i32 -1, i32* %mi_row_offset228, align 4, !tbaa !6
  %179 = bitcast i32* %mi_col_offset229 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %179) #6
  store i32 -1, i32* %mi_col_offset229, align 4, !tbaa !6
  %180 = bitcast %struct.MB_MODE_INFO** %mbmi230 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %180) #6
  %181 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi231 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %181, i32 0, i32 6
  %182 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi231, align 4, !tbaa !67
  %183 = load i32, i32* %mi_stride, align 4, !tbaa !6
  %mul232 = mul nsw i32 -1, %183
  %add233 = add nsw i32 -1, %mul232
  %arrayidx234 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %182, i32 %add233
  %184 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx234, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %184, %struct.MB_MODE_INFO** %mbmi230, align 4, !tbaa !2
  %185 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi230, align 4, !tbaa !2
  %ref_frame235 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %185, i32 0, i32 12
  %arrayidx236 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame235, i32 0, i32 0
  %186 = load i8, i8* %arrayidx236, align 4, !tbaa !41
  %conv237 = sext i8 %186 to i32
  %187 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %cmp238 = icmp eq i32 %conv237, %187
  br i1 %cmp238, label %land.lhs.true240, label %if.end255

land.lhs.true240:                                 ; preds = %if.then227
  %188 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi230, align 4, !tbaa !2
  %ref_frame241 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %188, i32 0, i32 12
  %arrayidx242 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame241, i32 0, i32 1
  %189 = load i8, i8* %arrayidx242, align 1, !tbaa !41
  %conv243 = sext i8 %189 to i32
  %cmp244 = icmp eq i32 %conv243, -1
  br i1 %cmp244, label %if.then246, label %if.end255

if.then246:                                       ; preds = %land.lhs.true240
  %190 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi230, align 4, !tbaa !2
  %191 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %192 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  call void @record_samples(%struct.MB_MODE_INFO* %190, i32* %191, i32* %192, i32 0, i32 -1, i32 0, i32 -1)
  %193 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %add.ptr247 = getelementptr inbounds i32, i32* %193, i32 2
  store i32* %add.ptr247, i32** %pts.addr, align 4, !tbaa !2
  %194 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %add.ptr248 = getelementptr inbounds i32, i32* %194, i32 2
  store i32* %add.ptr248, i32** %pts_inref.addr, align 4, !tbaa !2
  %195 = load i8, i8* %np, align 1, !tbaa !41
  %inc249 = add i8 %195, 1
  store i8 %inc249, i8* %np, align 1, !tbaa !41
  %196 = load i8, i8* %np, align 1, !tbaa !41
  %conv250 = zext i8 %196 to i32
  %cmp251 = icmp sge i32 %conv250, 8
  br i1 %cmp251, label %if.then253, label %if.end254

if.then253:                                       ; preds = %if.then246
  store i8 8, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup256

if.end254:                                        ; preds = %if.then246
  br label %if.end255

if.end255:                                        ; preds = %if.end254, %land.lhs.true240, %if.then227
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup256

cleanup256:                                       ; preds = %if.end255, %if.then253
  %197 = bitcast %struct.MB_MODE_INFO** %mbmi230 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #6
  %198 = bitcast i32* %mi_col_offset229 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #6
  %199 = bitcast i32* %mi_row_offset228 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #6
  %cleanup.dest259 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest259, label %cleanup327 [
    i32 0, label %cleanup.cont260
  ]

cleanup.cont260:                                  ; preds = %cleanup256
  br label %if.end261

if.end261:                                        ; preds = %cleanup.cont260, %land.lhs.true225, %land.lhs.true223, %if.end221
  %200 = load i32, i32* %do_tr, align 4, !tbaa !6
  %tobool262 = icmp ne i32 %200, 0
  br i1 %tobool262, label %land.lhs.true263, label %if.end326

land.lhs.true263:                                 ; preds = %if.end261
  %201 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %202 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %203 = load i32, i32* %mi_row, align 4, !tbaa !6
  %204 = load i32, i32* %mi_col, align 4, !tbaa !6
  %205 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width264 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %205, i32 0, i32 33
  %206 = load i8, i8* %width264, align 4, !tbaa !60
  %conv265 = zext i8 %206 to i32
  %207 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height266 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %207, i32 0, i32 34
  %208 = load i8, i8* %height266, align 1, !tbaa !61
  %conv267 = zext i8 %208 to i32
  %cmp268 = icmp sgt i32 %conv265, %conv267
  br i1 %cmp268, label %cond.true270, label %cond.false273

cond.true270:                                     ; preds = %land.lhs.true263
  %209 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width271 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %209, i32 0, i32 33
  %210 = load i8, i8* %width271, align 4, !tbaa !60
  %conv272 = zext i8 %210 to i32
  br label %cond.end276

cond.false273:                                    ; preds = %land.lhs.true263
  %211 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height274 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %211, i32 0, i32 34
  %212 = load i8, i8* %height274, align 1, !tbaa !61
  %conv275 = zext i8 %212 to i32
  br label %cond.end276

cond.end276:                                      ; preds = %cond.false273, %cond.true270
  %cond277 = phi i32 [ %conv272, %cond.true270 ], [ %conv275, %cond.false273 ]
  %call = call i32 @has_top_right(%struct.AV1Common* %201, %struct.macroblockd* %202, i32 %203, i32 %204, i32 %cond277)
  %tobool278 = icmp ne i32 %call, 0
  br i1 %tobool278, label %if.then279, label %if.end326

if.then279:                                       ; preds = %cond.end276
  %213 = bitcast %struct.position* %trb_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %213) #6
  %row = getelementptr inbounds %struct.position, %struct.position* %trb_pos, i32 0, i32 0
  store i32 -1, i32* %row, align 4, !tbaa !85
  %col = getelementptr inbounds %struct.position, %struct.position* %trb_pos, i32 0, i32 1
  %214 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width280 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %214, i32 0, i32 33
  %215 = load i8, i8* %width280, align 4, !tbaa !60
  %conv281 = zext i8 %215 to i32
  store i32 %conv281, i32* %col, align 4, !tbaa !87
  %216 = bitcast %struct.TileInfo** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %216) #6
  %217 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tile282 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %217, i32 0, i32 5
  store %struct.TileInfo* %tile282, %struct.TileInfo** %tile, align 4, !tbaa !2
  %218 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !2
  %219 = load i32, i32* %mi_col, align 4, !tbaa !6
  %220 = load i32, i32* %mi_row, align 4, !tbaa !6
  %call283 = call i32 @is_inside(%struct.TileInfo* %218, i32 %219, i32 %220, %struct.position* %trb_pos)
  %tobool284 = icmp ne i32 %call283, 0
  br i1 %tobool284, label %if.then285, label %if.end321

if.then285:                                       ; preds = %if.then279
  %221 = bitcast i32* %mi_row_offset286 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %221) #6
  store i32 -1, i32* %mi_row_offset286, align 4, !tbaa !6
  %222 = bitcast i32* %mi_col_offset287 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %222) #6
  %223 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width288 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %223, i32 0, i32 33
  %224 = load i8, i8* %width288, align 4, !tbaa !60
  %conv289 = zext i8 %224 to i32
  store i32 %conv289, i32* %mi_col_offset287, align 4, !tbaa !6
  %225 = bitcast %struct.MB_MODE_INFO** %mbmi290 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %225) #6
  %226 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi291 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %226, i32 0, i32 6
  %227 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi291, align 4, !tbaa !67
  %228 = load i32, i32* %mi_col_offset287, align 4, !tbaa !6
  %229 = load i32, i32* %mi_stride, align 4, !tbaa !6
  %mul292 = mul nsw i32 -1, %229
  %add293 = add nsw i32 %228, %mul292
  %arrayidx294 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %227, i32 %add293
  %230 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx294, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %230, %struct.MB_MODE_INFO** %mbmi290, align 4, !tbaa !2
  %231 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi290, align 4, !tbaa !2
  %ref_frame295 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %231, i32 0, i32 12
  %arrayidx296 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame295, i32 0, i32 0
  %232 = load i8, i8* %arrayidx296, align 4, !tbaa !41
  %conv297 = sext i8 %232 to i32
  %233 = load i32, i32* %ref_frame, align 4, !tbaa !6
  %cmp298 = icmp eq i32 %conv297, %233
  br i1 %cmp298, label %land.lhs.true300, label %if.end315

land.lhs.true300:                                 ; preds = %if.then285
  %234 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi290, align 4, !tbaa !2
  %ref_frame301 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %234, i32 0, i32 12
  %arrayidx302 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame301, i32 0, i32 1
  %235 = load i8, i8* %arrayidx302, align 1, !tbaa !41
  %conv303 = sext i8 %235 to i32
  %cmp304 = icmp eq i32 %conv303, -1
  br i1 %cmp304, label %if.then306, label %if.end315

if.then306:                                       ; preds = %land.lhs.true300
  %236 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi290, align 4, !tbaa !2
  %237 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %238 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %239 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width307 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %239, i32 0, i32 33
  %240 = load i8, i8* %width307, align 4, !tbaa !60
  %conv308 = zext i8 %240 to i32
  call void @record_samples(%struct.MB_MODE_INFO* %236, i32* %237, i32* %238, i32 0, i32 -1, i32 %conv308, i32 1)
  %241 = load i8, i8* %np, align 1, !tbaa !41
  %inc309 = add i8 %241, 1
  store i8 %inc309, i8* %np, align 1, !tbaa !41
  %242 = load i8, i8* %np, align 1, !tbaa !41
  %conv310 = zext i8 %242 to i32
  %cmp311 = icmp sge i32 %conv310, 8
  br i1 %cmp311, label %if.then313, label %if.end314

if.then313:                                       ; preds = %if.then306
  store i8 8, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup316

if.end314:                                        ; preds = %if.then306
  br label %if.end315

if.end315:                                        ; preds = %if.end314, %land.lhs.true300, %if.then285
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup316

cleanup316:                                       ; preds = %if.end315, %if.then313
  %243 = bitcast %struct.MB_MODE_INFO** %mbmi290 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #6
  %244 = bitcast i32* %mi_col_offset287 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #6
  %245 = bitcast i32* %mi_row_offset286 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #6
  %cleanup.dest319 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest319, label %cleanup322 [
    i32 0, label %cleanup.cont320
  ]

cleanup.cont320:                                  ; preds = %cleanup316
  br label %if.end321

if.end321:                                        ; preds = %cleanup.cont320, %if.then279
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup322

cleanup322:                                       ; preds = %if.end321, %cleanup316
  %246 = bitcast %struct.TileInfo** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #6
  %247 = bitcast %struct.position* %trb_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %247) #6
  %cleanup.dest324 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest324, label %cleanup327 [
    i32 0, label %cleanup.cont325
  ]

cleanup.cont325:                                  ; preds = %cleanup322
  br label %if.end326

if.end326:                                        ; preds = %cleanup.cont325, %cond.end276, %if.end261
  %248 = load i8, i8* %np, align 1, !tbaa !41
  store i8 %248, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup327

cleanup327:                                       ; preds = %if.end326, %cleanup322, %cleanup256, %cleanup216, %cleanup102
  %249 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #6
  %250 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %250) #6
  %251 = bitcast i32* %mi_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #6
  %252 = bitcast i32* %do_tr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #6
  %253 = bitcast i32* %do_tl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %np) #6
  %254 = bitcast i32* %mi_step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #6
  %255 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #6
  %256 = bitcast i32* %left_available to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #6
  %257 = bitcast i32* %up_available to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #6
  %258 = bitcast i32* %ref_frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #6
  %259 = bitcast %struct.MB_MODE_INFO** %mbmi0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #6
  %260 = load i8, i8* %retval, align 1
  ret i8 %260
}

; Function Attrs: inlinehint nounwind
define internal void @record_samples(%struct.MB_MODE_INFO* %mbmi, i32* %pts, i32* %pts_inref, i32 %row_offset, i32 %sign_r, i32 %col_offset, i32 %sign_c) #3 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %pts.addr = alloca i32*, align 4
  %pts_inref.addr = alloca i32*, align 4
  %row_offset.addr = alloca i32, align 4
  %sign_r.addr = alloca i32, align 4
  %col_offset.addr = alloca i32, align 4
  %sign_c.addr = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  store i32* %pts, i32** %pts.addr, align 4, !tbaa !2
  store i32* %pts_inref, i32** %pts_inref.addr, align 4, !tbaa !2
  store i32 %row_offset, i32* %row_offset.addr, align 4, !tbaa !6
  store i32 %sign_r, i32* %sign_r.addr, align 4, !tbaa !6
  store i32 %col_offset, i32* %col_offset.addr, align 4, !tbaa !6
  store i32 %sign_c, i32* %sign_c.addr, align 4, !tbaa !6
  %0 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 6
  %2 = load i8, i8* %sb_type, align 2, !tbaa !48
  %idxprom = zext i8 %2 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %conv = zext i8 %3 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !6
  %4 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %sb_type1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 6
  %6 = load i8, i8* %sb_type1, align 2, !tbaa !48
  %idxprom2 = zext i8 %6 to i32
  %arrayidx3 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom2
  %7 = load i8, i8* %arrayidx3, align 1, !tbaa !41
  %conv4 = zext i8 %7 to i32
  store i32 %conv4, i32* %bh, align 4, !tbaa !6
  %8 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %9, 4
  %10 = load i32, i32* %sign_c.addr, align 4, !tbaa !6
  %11 = load i32, i32* %bw, align 4, !tbaa !6
  %cmp = icmp sgt i32 %11, 4
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %12 = load i32, i32* %bw, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %12, %cond.true ], [ 4, %cond.false ]
  %mul6 = mul nsw i32 %10, %cond
  %div = sdiv i32 %mul6, 2
  %add = add nsw i32 %mul, %div
  %sub = sub nsw i32 %add, 1
  store i32 %sub, i32* %x, align 4, !tbaa !6
  %13 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 %14, 4
  %15 = load i32, i32* %sign_r.addr, align 4, !tbaa !6
  %16 = load i32, i32* %bh, align 4, !tbaa !6
  %cmp8 = icmp sgt i32 %16, 4
  br i1 %cmp8, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %cond.end
  %17 = load i32, i32* %bh, align 4, !tbaa !6
  br label %cond.end12

cond.false11:                                     ; preds = %cond.end
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false11, %cond.true10
  %cond13 = phi i32 [ %17, %cond.true10 ], [ 4, %cond.false11 ]
  %mul14 = mul nsw i32 %15, %cond13
  %div15 = sdiv i32 %mul14, 2
  %add16 = add nsw i32 %mul7, %div15
  %sub17 = sub nsw i32 %add16, 1
  store i32 %sub17, i32* %y, align 4, !tbaa !6
  %18 = load i32, i32* %x, align 4, !tbaa !6
  %mul18 = mul nsw i32 %18, 8
  %19 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %19, i32 0
  store i32 %mul18, i32* %arrayidx19, align 4, !tbaa !6
  %20 = load i32, i32* %y, align 4, !tbaa !6
  %mul20 = mul nsw i32 %20, 8
  %21 = load i32*, i32** %pts.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %21, i32 1
  store i32 %mul20, i32* %arrayidx21, align 4, !tbaa !6
  %22 = load i32, i32* %x, align 4, !tbaa !6
  %mul22 = mul nsw i32 %22, 8
  %23 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %mv = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %23, i32 0, i32 2
  %arrayidx23 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv, i32 0, i32 0
  %as_mv = bitcast %union.int_mv* %arrayidx23 to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv, i32 0, i32 1
  %24 = load i16, i16* %col, align 2, !tbaa !41
  %conv24 = sext i16 %24 to i32
  %add25 = add nsw i32 %mul22, %conv24
  %25 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i32, i32* %25, i32 0
  store i32 %add25, i32* %arrayidx26, align 4, !tbaa !6
  %26 = load i32, i32* %y, align 4, !tbaa !6
  %mul27 = mul nsw i32 %26, 8
  %27 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %mv28 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %27, i32 0, i32 2
  %arrayidx29 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv28, i32 0, i32 0
  %as_mv30 = bitcast %union.int_mv* %arrayidx29 to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv30, i32 0, i32 0
  %28 = load i16, i16* %row, align 4, !tbaa !41
  %conv31 = sext i16 %28 to i32
  %add32 = add nsw i32 %mul27, %conv31
  %29 = load i32*, i32** %pts_inref.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %29, i32 1
  store i32 %add32, i32* %arrayidx33, align 4, !tbaa !6
  %30 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  %31 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  %32 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  %33 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  ret void
}

; Function Attrs: nounwind
define internal i32 @has_top_right(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %mi_row, i32 %mi_col, i32 %bs) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %bs.addr = alloca i32, align 4
  %sb_mi_size = alloca i32, align 4
  %mask_row = alloca i32, align 4
  %mask_col = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %has_tr = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i32 %bs, i32* %bs.addr, align 4, !tbaa !6
  %0 = bitcast i32* %sb_mi_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 7
  %2 = load i8, i8* %sb_size, align 4, !tbaa !88
  %idxprom = zext i8 %2 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %conv = zext i8 %3 to i32
  store i32 %conv, i32* %sb_mi_size, align 4, !tbaa !6
  %4 = bitcast i32* %mask_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %6 = load i32, i32* %sb_mi_size, align 4, !tbaa !6
  %sub = sub nsw i32 %6, 1
  %and = and i32 %5, %sub
  store i32 %and, i32* %mask_row, align 4, !tbaa !6
  %7 = bitcast i32* %mask_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %9 = load i32, i32* %sb_mi_size, align 4, !tbaa !6
  %sub1 = sub nsw i32 %9, 1
  %and2 = and i32 %8, %sub1
  store i32 %and2, i32* %mask_col, align 4, !tbaa !6
  %10 = load i32, i32* %bs.addr, align 4, !tbaa !6
  %11 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv3 = zext i8 %11 to i32
  %cmp = icmp sgt i32 %10, %conv3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %12 = bitcast i32* %has_tr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i32, i32* %mask_row, align 4, !tbaa !6
  %14 = load i32, i32* %bs.addr, align 4, !tbaa !6
  %and5 = and i32 %13, %14
  %tobool = icmp ne i32 %and5, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end
  %15 = load i32, i32* %mask_col, align 4, !tbaa !6
  %16 = load i32, i32* %bs.addr, align 4, !tbaa !6
  %and6 = and i32 %15, %16
  %tobool7 = icmp ne i32 %and6, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end
  %17 = phi i1 [ false, %if.end ], [ %tobool7, %land.rhs ]
  %lnot = xor i1 %17, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %has_tr, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end20, %land.end
  %18 = load i32, i32* %bs.addr, align 4, !tbaa !6
  %19 = load i32, i32* %sb_mi_size, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %18, %19
  br i1 %cmp8, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %20 = load i32, i32* %mask_col, align 4, !tbaa !6
  %21 = load i32, i32* %bs.addr, align 4, !tbaa !6
  %and10 = and i32 %20, %21
  %tobool11 = icmp ne i32 %and10, 0
  br i1 %tobool11, label %if.then12, label %if.else

if.then12:                                        ; preds = %while.body
  %22 = load i32, i32* %mask_col, align 4, !tbaa !6
  %23 = load i32, i32* %bs.addr, align 4, !tbaa !6
  %mul = mul nsw i32 2, %23
  %and13 = and i32 %22, %mul
  %tobool14 = icmp ne i32 %and13, 0
  br i1 %tobool14, label %land.lhs.true, label %if.end19

land.lhs.true:                                    ; preds = %if.then12
  %24 = load i32, i32* %mask_row, align 4, !tbaa !6
  %25 = load i32, i32* %bs.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 2, %25
  %and16 = and i32 %24, %mul15
  %tobool17 = icmp ne i32 %and16, 0
  br i1 %tobool17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %land.lhs.true
  store i32 0, i32* %has_tr, align 4, !tbaa !6
  br label %while.end

if.end19:                                         ; preds = %land.lhs.true, %if.then12
  br label %if.end20

if.else:                                          ; preds = %while.body
  br label %while.end

if.end20:                                         ; preds = %if.end19
  %26 = load i32, i32* %bs.addr, align 4, !tbaa !6
  %shl = shl i32 %26, 1
  store i32 %shl, i32* %bs.addr, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %if.else, %if.then18, %while.cond
  %27 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %27, i32 0, i32 33
  %28 = load i8, i8* %width, align 4, !tbaa !60
  %conv21 = zext i8 %28 to i32
  %29 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %29, i32 0, i32 34
  %30 = load i8, i8* %height, align 1, !tbaa !61
  %conv22 = zext i8 %30 to i32
  %cmp23 = icmp slt i32 %conv21, %conv22
  br i1 %cmp23, label %if.then25, label %if.end29

if.then25:                                        ; preds = %while.end
  %31 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %is_sec_rect = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %31, i32 0, i32 38
  %32 = load i8, i8* %is_sec_rect, align 4, !tbaa !89
  %tobool26 = icmp ne i8 %32, 0
  br i1 %tobool26, label %if.end28, label %if.then27

if.then27:                                        ; preds = %if.then25
  store i32 1, i32* %has_tr, align 4, !tbaa !6
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %if.then25
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %while.end
  %33 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width30 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %33, i32 0, i32 33
  %34 = load i8, i8* %width30, align 4, !tbaa !60
  %conv31 = zext i8 %34 to i32
  %35 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height32 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %35, i32 0, i32 34
  %36 = load i8, i8* %height32, align 1, !tbaa !61
  %conv33 = zext i8 %36 to i32
  %cmp34 = icmp sgt i32 %conv31, %conv33
  br i1 %cmp34, label %if.then36, label %if.end41

if.then36:                                        ; preds = %if.end29
  %37 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %is_sec_rect37 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %37, i32 0, i32 38
  %38 = load i8, i8* %is_sec_rect37, align 4, !tbaa !89
  %tobool38 = icmp ne i8 %38, 0
  br i1 %tobool38, label %if.then39, label %if.end40

if.then39:                                        ; preds = %if.then36
  store i32 0, i32* %has_tr, align 4, !tbaa !6
  br label %if.end40

if.end40:                                         ; preds = %if.then39, %if.then36
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %if.end29
  %39 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %39, i32 0, i32 6
  %40 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !67
  %arrayidx42 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %40, i32 0
  %41 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx42, align 4, !tbaa !2
  %partition = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %41, i32 0, i32 11
  %42 = load i8, i8* %partition, align 1, !tbaa !90
  %conv43 = zext i8 %42 to i32
  %cmp44 = icmp eq i32 %conv43, 6
  br i1 %cmp44, label %if.then46, label %if.end59

if.then46:                                        ; preds = %if.end41
  %43 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width47 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %43, i32 0, i32 33
  %44 = load i8, i8* %width47, align 4, !tbaa !60
  %conv48 = zext i8 %44 to i32
  %45 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height49 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %45, i32 0, i32 34
  %46 = load i8, i8* %height49, align 1, !tbaa !61
  %conv50 = zext i8 %46 to i32
  %cmp51 = icmp eq i32 %conv48, %conv50
  br i1 %cmp51, label %if.then53, label %if.end58

if.then53:                                        ; preds = %if.then46
  %47 = load i32, i32* %mask_row, align 4, !tbaa !6
  %48 = load i32, i32* %bs.addr, align 4, !tbaa !6
  %and54 = and i32 %47, %48
  %tobool55 = icmp ne i32 %and54, 0
  br i1 %tobool55, label %if.then56, label %if.end57

if.then56:                                        ; preds = %if.then53
  store i32 0, i32* %has_tr, align 4, !tbaa !6
  br label %if.end57

if.end57:                                         ; preds = %if.then56, %if.then53
  br label %if.end58

if.end58:                                         ; preds = %if.end57, %if.then46
  br label %if.end59

if.end59:                                         ; preds = %if.end58, %if.end41
  %49 = load i32, i32* %has_tr, align 4, !tbaa !6
  store i32 %49, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %50 = bitcast i32* %has_tr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end59, %if.then
  %51 = bitcast i32* %mask_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  %52 = bitcast i32* %mask_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  %53 = bitcast i32* %sb_mi_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  %54 = load i32, i32* %retval, align 4
  ret i32 %54
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inside(%struct.TileInfo* %tile, i32 %mi_col, i32 %mi_row, %struct.position* %mi_pos) #3 {
entry:
  %tile.addr = alloca %struct.TileInfo*, align 4
  %mi_col.addr = alloca i32, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_pos.addr = alloca %struct.position*, align 4
  store %struct.TileInfo* %tile, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store %struct.position* %mi_pos, %struct.position** %mi_pos.addr, align 4, !tbaa !2
  %0 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %1 = load %struct.position*, %struct.position** %mi_pos.addr, align 4, !tbaa !2
  %row = getelementptr inbounds %struct.position, %struct.position* %1, i32 0, i32 0
  %2 = load i32, i32* %row, align 4, !tbaa !85
  %add = add nsw i32 %0, %2
  %3 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_row_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %3, i32 0, i32 0
  %4 = load i32, i32* %mi_row_start, align 4, !tbaa !91
  %cmp = icmp slt i32 %add, %4
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %5 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %6 = load %struct.position*, %struct.position** %mi_pos.addr, align 4, !tbaa !2
  %col = getelementptr inbounds %struct.position, %struct.position* %6, i32 0, i32 1
  %7 = load i32, i32* %col, align 4, !tbaa !87
  %add1 = add nsw i32 %5, %7
  %8 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_col_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %8, i32 0, i32 2
  %9 = load i32, i32* %mi_col_start, align 4, !tbaa !92
  %cmp2 = icmp slt i32 %add1, %9
  br i1 %cmp2, label %lor.end, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %10 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %11 = load %struct.position*, %struct.position** %mi_pos.addr, align 4, !tbaa !2
  %row4 = getelementptr inbounds %struct.position, %struct.position* %11, i32 0, i32 0
  %12 = load i32, i32* %row4, align 4, !tbaa !85
  %add5 = add nsw i32 %10, %12
  %13 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_row_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %13, i32 0, i32 1
  %14 = load i32, i32* %mi_row_end, align 4, !tbaa !93
  %cmp6 = icmp sge i32 %add5, %14
  br i1 %cmp6, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %lor.lhs.false3
  %15 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %16 = load %struct.position*, %struct.position** %mi_pos.addr, align 4, !tbaa !2
  %col7 = getelementptr inbounds %struct.position, %struct.position* %16, i32 0, i32 1
  %17 = load i32, i32* %col7, align 4, !tbaa !87
  %add8 = add nsw i32 %15, %17
  %18 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_col_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %18, i32 0, i32 3
  %19 = load i32, i32* %mi_col_end, align 4, !tbaa !94
  %cmp9 = icmp sge i32 %add8, %19
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %lor.lhs.false3, %lor.lhs.false, %entry
  %20 = phi i1 [ true, %lor.lhs.false3 ], [ true, %lor.lhs.false ], [ true, %entry ], [ %cmp9, %lor.rhs ]
  %lnot = xor i1 %20, true
  %lnot.ext = zext i1 %lnot to i32
  ret i32 %lnot.ext
}

; Function Attrs: nounwind
define hidden void @av1_setup_skip_mode_allowed(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %order_hint_info = alloca %struct.OrderHintInfo*, align 4
  %skip_mode_info = alloca %struct.SkipModeInfo*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %cur_order_hint = alloca i32, align 4
  %ref_order_hints = alloca [2 x i32], align 4
  %ref_idx = alloca [2 x i32], align 4
  %i = alloca i32, align 4
  %buf = alloca %struct.RefCntBuffer*, align 4
  %ref_order_hint = alloca i32, align 4
  %i87 = alloca i32, align 4
  %buf93 = alloca %struct.RefCntBuffer*, align 4
  %ref_order_hint101 = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = bitcast %struct.OrderHintInfo** %order_hint_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 37
  %order_hint_info1 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 10
  store %struct.OrderHintInfo* %order_hint_info1, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %2 = bitcast %struct.SkipModeInfo** %skip_mode_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 0
  %skip_mode_info2 = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 5
  store %struct.SkipModeInfo* %skip_mode_info2, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %4 = load %struct.SkipModeInfo*, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %skip_mode_allowed = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %4, i32 0, i32 0
  store i32 0, i32* %skip_mode_allowed, align 4, !tbaa !77
  %5 = load %struct.SkipModeInfo*, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %ref_frame_idx_0 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %5, i32 0, i32 2
  store i32 -1, i32* %ref_frame_idx_0, align 4, !tbaa !95
  %6 = load %struct.SkipModeInfo*, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %ref_frame_idx_1 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %6, i32 0, i32 3
  store i32 -1, i32* %ref_frame_idx_1, align 4, !tbaa !96
  %7 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %enable_order_hint = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %7, i32 0, i32 0
  %8 = load i32, i32* %enable_order_hint, align 4, !tbaa !77
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @frame_is_intra_only(%struct.AV1Common* %9)
  %tobool3 = icmp ne i32 %call, 0
  br i1 %tobool3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame5 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 0
  %reference_mode = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame5, i32 0, i32 1
  %11 = load i8, i8* %reference_mode, align 1, !tbaa !97
  %conv = zext i8 %11 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false, %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup161

if.end:                                           ; preds = %lor.lhs.false4
  %12 = bitcast i32* %cur_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 0
  %order_hint = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame7, i32 0, i32 2
  %14 = load i32, i32* %order_hint, align 4, !tbaa !72
  store i32 %14, i32* %cur_order_hint, align 4, !tbaa !6
  %15 = bitcast [2 x i32]* %ref_order_hints to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %15) #6
  %16 = bitcast [2 x i32]* %ref_order_hints to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 bitcast ([2 x i32]* @__const.av1_setup_skip_mode_allowed.ref_order_hints to i8*), i32 8, i1 false)
  %17 = bitcast [2 x i32]* %ref_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %17) #6
  %18 = bitcast [2 x i32]* %ref_idx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 bitcast ([2 x i32]* @__const.av1_setup_skip_mode_allowed.ref_idx to i8*), i32 8, i1 false)
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %20, 7
  br i1 %cmp8, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %22 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 1, %24
  %conv10 = trunc i32 %add to i8
  %call11 = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %23, i8 signext %conv10)
  store %struct.RefCntBuffer* %call11, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %25 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %cmp12 = icmp eq %struct.RefCntBuffer* %25, null
  br i1 %cmp12, label %if.then14, label %if.end15

if.then14:                                        ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %for.body
  %26 = bitcast i32* %ref_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %order_hint16 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %27, i32 0, i32 1
  %28 = load i32, i32* %order_hint16, align 4, !tbaa !73
  store i32 %28, i32* %ref_order_hint, align 4, !tbaa !6
  %29 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %30 = load i32, i32* %ref_order_hint, align 4, !tbaa !6
  %31 = load i32, i32* %cur_order_hint, align 4, !tbaa !6
  %call17 = call i32 @get_relative_dist(%struct.OrderHintInfo* %29, i32 %30, i32 %31)
  %cmp18 = icmp slt i32 %call17, 0
  br i1 %cmp18, label %if.then20, label %if.else

if.then20:                                        ; preds = %if.end15
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 0
  %32 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %cmp21 = icmp eq i32 %32, -1
  br i1 %cmp21, label %if.then28, label %lor.lhs.false23

lor.lhs.false23:                                  ; preds = %if.then20
  %33 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %34 = load i32, i32* %ref_order_hint, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 0
  %35 = load i32, i32* %arrayidx24, align 4, !tbaa !6
  %call25 = call i32 @get_relative_dist(%struct.OrderHintInfo* %33, i32 %34, i32 %35)
  %cmp26 = icmp sgt i32 %call25, 0
  br i1 %cmp26, label %if.then28, label %if.end31

if.then28:                                        ; preds = %lor.lhs.false23, %if.then20
  %36 = load i32, i32* %ref_order_hint, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 0
  store i32 %36, i32* %arrayidx29, align 4, !tbaa !6
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  store i32 %37, i32* %arrayidx30, align 4, !tbaa !6
  br label %if.end31

if.end31:                                         ; preds = %if.then28, %lor.lhs.false23
  br label %if.end49

if.else:                                          ; preds = %if.end15
  %38 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %39 = load i32, i32* %ref_order_hint, align 4, !tbaa !6
  %40 = load i32, i32* %cur_order_hint, align 4, !tbaa !6
  %call32 = call i32 @get_relative_dist(%struct.OrderHintInfo* %38, i32 %39, i32 %40)
  %cmp33 = icmp sgt i32 %call32, 0
  br i1 %cmp33, label %if.then35, label %if.end48

if.then35:                                        ; preds = %if.else
  %arrayidx36 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 1
  %41 = load i32, i32* %arrayidx36, align 4, !tbaa !6
  %cmp37 = icmp eq i32 %41, 2147483647
  br i1 %cmp37, label %if.then44, label %lor.lhs.false39

lor.lhs.false39:                                  ; preds = %if.then35
  %42 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %43 = load i32, i32* %ref_order_hint, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 1
  %44 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %call41 = call i32 @get_relative_dist(%struct.OrderHintInfo* %42, i32 %43, i32 %44)
  %cmp42 = icmp slt i32 %call41, 0
  br i1 %cmp42, label %if.then44, label %if.end47

if.then44:                                        ; preds = %lor.lhs.false39, %if.then35
  %45 = load i32, i32* %ref_order_hint, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 1
  store i32 %45, i32* %arrayidx45, align 4, !tbaa !6
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  store i32 %46, i32* %arrayidx46, align 4, !tbaa !6
  br label %if.end47

if.end47:                                         ; preds = %if.then44, %lor.lhs.false39
  br label %if.end48

if.end48:                                         ; preds = %if.end47, %if.else
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.end31
  %47 = bitcast i32* %ref_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end49, %if.then14
  %48 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %arrayidx51 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %50 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  %cmp52 = icmp ne i32 %50, -1
  br i1 %cmp52, label %land.lhs.true, label %if.else77

land.lhs.true:                                    ; preds = %for.end
  %arrayidx54 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %51 = load i32, i32* %arrayidx54, align 4, !tbaa !6
  %cmp55 = icmp ne i32 %51, -1
  br i1 %cmp55, label %if.then57, label %if.else77

if.then57:                                        ; preds = %land.lhs.true
  %52 = load %struct.SkipModeInfo*, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %skip_mode_allowed58 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %52, i32 0, i32 0
  store i32 1, i32* %skip_mode_allowed58, align 4, !tbaa !77
  %arrayidx59 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %53 = load i32, i32* %arrayidx59, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %54 = load i32, i32* %arrayidx60, align 4, !tbaa !6
  %cmp61 = icmp slt i32 %53, %54
  br i1 %cmp61, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then57
  %arrayidx63 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %55 = load i32, i32* %arrayidx63, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.then57
  %arrayidx64 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %56 = load i32, i32* %arrayidx64, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %55, %cond.true ], [ %56, %cond.false ]
  %57 = load %struct.SkipModeInfo*, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %ref_frame_idx_065 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %57, i32 0, i32 2
  store i32 %cond, i32* %ref_frame_idx_065, align 4, !tbaa !95
  %arrayidx66 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %58 = load i32, i32* %arrayidx66, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %59 = load i32, i32* %arrayidx67, align 4, !tbaa !6
  %cmp68 = icmp sgt i32 %58, %59
  br i1 %cmp68, label %cond.true70, label %cond.false72

cond.true70:                                      ; preds = %cond.end
  %arrayidx71 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %60 = load i32, i32* %arrayidx71, align 4, !tbaa !6
  br label %cond.end74

cond.false72:                                     ; preds = %cond.end
  %arrayidx73 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %61 = load i32, i32* %arrayidx73, align 4, !tbaa !6
  br label %cond.end74

cond.end74:                                       ; preds = %cond.false72, %cond.true70
  %cond75 = phi i32 [ %60, %cond.true70 ], [ %61, %cond.false72 ]
  %62 = load %struct.SkipModeInfo*, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %ref_frame_idx_176 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %62, i32 0, i32 3
  store i32 %cond75, i32* %ref_frame_idx_176, align 4, !tbaa !96
  br label %if.end160

if.else77:                                        ; preds = %land.lhs.true, %for.end
  %arrayidx78 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %63 = load i32, i32* %arrayidx78, align 4, !tbaa !6
  %cmp79 = icmp ne i32 %63, -1
  br i1 %cmp79, label %land.lhs.true81, label %if.end159

land.lhs.true81:                                  ; preds = %if.else77
  %arrayidx82 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %64 = load i32, i32* %arrayidx82, align 4, !tbaa !6
  %cmp83 = icmp eq i32 %64, -1
  br i1 %cmp83, label %if.then85, label %if.end159

if.then85:                                        ; preds = %land.lhs.true81
  %arrayidx86 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 1
  store i32 -1, i32* %arrayidx86, align 4, !tbaa !6
  %65 = bitcast i32* %i87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #6
  store i32 0, i32* %i87, align 4, !tbaa !6
  br label %for.cond88

for.cond88:                                       ; preds = %for.inc127, %if.then85
  %66 = load i32, i32* %i87, align 4, !tbaa !6
  %cmp89 = icmp slt i32 %66, 7
  br i1 %cmp89, label %for.body92, label %for.cond.cleanup91

for.cond.cleanup91:                               ; preds = %for.cond88
  store i32 5, i32* %cleanup.dest.slot, align 4
  %67 = bitcast i32* %i87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  br label %for.end130

for.body92:                                       ; preds = %for.cond88
  %68 = bitcast %struct.RefCntBuffer** %buf93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #6
  %69 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %70 = load i32, i32* %i87, align 4, !tbaa !6
  %add94 = add nsw i32 1, %70
  %conv95 = trunc i32 %add94 to i8
  %call96 = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %69, i8 signext %conv95)
  store %struct.RefCntBuffer* %call96, %struct.RefCntBuffer** %buf93, align 4, !tbaa !2
  %71 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf93, align 4, !tbaa !2
  %cmp97 = icmp eq %struct.RefCntBuffer* %71, null
  br i1 %cmp97, label %if.then99, label %if.end100

if.then99:                                        ; preds = %for.body92
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup124

if.end100:                                        ; preds = %for.body92
  %72 = bitcast i32* %ref_order_hint101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #6
  %73 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf93, align 4, !tbaa !2
  %order_hint102 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %73, i32 0, i32 1
  %74 = load i32, i32* %order_hint102, align 4, !tbaa !73
  store i32 %74, i32* %ref_order_hint101, align 4, !tbaa !6
  %arrayidx103 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 0
  %75 = load i32, i32* %arrayidx103, align 4, !tbaa !6
  %cmp104 = icmp ne i32 %75, -1
  br i1 %cmp104, label %land.lhs.true106, label %if.end123

land.lhs.true106:                                 ; preds = %if.end100
  %76 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %77 = load i32, i32* %ref_order_hint101, align 4, !tbaa !6
  %arrayidx107 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 0
  %78 = load i32, i32* %arrayidx107, align 4, !tbaa !6
  %call108 = call i32 @get_relative_dist(%struct.OrderHintInfo* %76, i32 %77, i32 %78)
  %cmp109 = icmp slt i32 %call108, 0
  br i1 %cmp109, label %land.lhs.true111, label %if.end123

land.lhs.true111:                                 ; preds = %land.lhs.true106
  %arrayidx112 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 1
  %79 = load i32, i32* %arrayidx112, align 4, !tbaa !6
  %cmp113 = icmp eq i32 %79, -1
  br i1 %cmp113, label %if.then120, label %lor.lhs.false115

lor.lhs.false115:                                 ; preds = %land.lhs.true111
  %80 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %order_hint_info, align 4, !tbaa !2
  %81 = load i32, i32* %ref_order_hint101, align 4, !tbaa !6
  %arrayidx116 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 1
  %82 = load i32, i32* %arrayidx116, align 4, !tbaa !6
  %call117 = call i32 @get_relative_dist(%struct.OrderHintInfo* %80, i32 %81, i32 %82)
  %cmp118 = icmp sgt i32 %call117, 0
  br i1 %cmp118, label %if.then120, label %if.end123

if.then120:                                       ; preds = %lor.lhs.false115, %land.lhs.true111
  %83 = load i32, i32* %ref_order_hint101, align 4, !tbaa !6
  %arrayidx121 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 1
  store i32 %83, i32* %arrayidx121, align 4, !tbaa !6
  %84 = load i32, i32* %i87, align 4, !tbaa !6
  %arrayidx122 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  store i32 %84, i32* %arrayidx122, align 4, !tbaa !6
  br label %if.end123

if.end123:                                        ; preds = %if.then120, %lor.lhs.false115, %land.lhs.true106, %if.end100
  %85 = bitcast i32* %ref_order_hint101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup124

cleanup124:                                       ; preds = %if.end123, %if.then99
  %86 = bitcast %struct.RefCntBuffer** %buf93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #6
  %cleanup.dest125 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest125, label %unreachable [
    i32 0, label %cleanup.cont126
    i32 7, label %for.inc127
  ]

cleanup.cont126:                                  ; preds = %cleanup124
  br label %for.inc127

for.inc127:                                       ; preds = %cleanup.cont126, %cleanup124
  %87 = load i32, i32* %i87, align 4, !tbaa !6
  %inc128 = add nsw i32 %87, 1
  store i32 %inc128, i32* %i87, align 4, !tbaa !6
  br label %for.cond88

for.end130:                                       ; preds = %for.cond.cleanup91
  %arrayidx131 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_order_hints, i32 0, i32 1
  %88 = load i32, i32* %arrayidx131, align 4, !tbaa !6
  %cmp132 = icmp ne i32 %88, -1
  br i1 %cmp132, label %if.then134, label %if.end158

if.then134:                                       ; preds = %for.end130
  %89 = load %struct.SkipModeInfo*, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %skip_mode_allowed135 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %89, i32 0, i32 0
  store i32 1, i32* %skip_mode_allowed135, align 4, !tbaa !77
  %arrayidx136 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %90 = load i32, i32* %arrayidx136, align 4, !tbaa !6
  %arrayidx137 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %91 = load i32, i32* %arrayidx137, align 4, !tbaa !6
  %cmp138 = icmp slt i32 %90, %91
  br i1 %cmp138, label %cond.true140, label %cond.false142

cond.true140:                                     ; preds = %if.then134
  %arrayidx141 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %92 = load i32, i32* %arrayidx141, align 4, !tbaa !6
  br label %cond.end144

cond.false142:                                    ; preds = %if.then134
  %arrayidx143 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %93 = load i32, i32* %arrayidx143, align 4, !tbaa !6
  br label %cond.end144

cond.end144:                                      ; preds = %cond.false142, %cond.true140
  %cond145 = phi i32 [ %92, %cond.true140 ], [ %93, %cond.false142 ]
  %94 = load %struct.SkipModeInfo*, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %ref_frame_idx_0146 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %94, i32 0, i32 2
  store i32 %cond145, i32* %ref_frame_idx_0146, align 4, !tbaa !95
  %arrayidx147 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %95 = load i32, i32* %arrayidx147, align 4, !tbaa !6
  %arrayidx148 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %96 = load i32, i32* %arrayidx148, align 4, !tbaa !6
  %cmp149 = icmp sgt i32 %95, %96
  br i1 %cmp149, label %cond.true151, label %cond.false153

cond.true151:                                     ; preds = %cond.end144
  %arrayidx152 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 0
  %97 = load i32, i32* %arrayidx152, align 4, !tbaa !6
  br label %cond.end155

cond.false153:                                    ; preds = %cond.end144
  %arrayidx154 = getelementptr inbounds [2 x i32], [2 x i32]* %ref_idx, i32 0, i32 1
  %98 = load i32, i32* %arrayidx154, align 4, !tbaa !6
  br label %cond.end155

cond.end155:                                      ; preds = %cond.false153, %cond.true151
  %cond156 = phi i32 [ %97, %cond.true151 ], [ %98, %cond.false153 ]
  %99 = load %struct.SkipModeInfo*, %struct.SkipModeInfo** %skip_mode_info, align 4, !tbaa !2
  %ref_frame_idx_1157 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %99, i32 0, i32 3
  store i32 %cond156, i32* %ref_frame_idx_1157, align 4, !tbaa !96
  br label %if.end158

if.end158:                                        ; preds = %cond.end155, %for.end130
  br label %if.end159

if.end159:                                        ; preds = %if.end158, %land.lhs.true81, %if.else77
  br label %if.end160

if.end160:                                        ; preds = %if.end159, %cond.end74
  %100 = bitcast [2 x i32]* %ref_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %100) #6
  %101 = bitcast [2 x i32]* %ref_order_hints to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %101) #6
  %102 = bitcast i32* %cur_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup161

cleanup161:                                       ; preds = %if.end160, %if.then
  %103 = bitcast %struct.SkipModeInfo** %skip_mode_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast %struct.OrderHintInfo** %order_hint_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  %cleanup.dest163 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest163, label %unreachable [
    i32 0, label %cleanup.cont164
    i32 1, label %cleanup.cont164
  ]

cleanup.cont164:                                  ; preds = %cleanup161, %cleanup161
  ret void

unreachable:                                      ; preds = %cleanup161, %cleanup124, %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32 @frame_is_intra_only(%struct.AV1Common* %cm) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 0
  %frame_type = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 0
  %1 = load i8, i8* %frame_type, align 16, !tbaa !98
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 0
  %frame_type3 = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame2, i32 0, i32 0
  %3 = load i8, i8* %frame_type3, align 16, !tbaa !98
  %conv4 = zext i8 %3 to i32
  %cmp5 = icmp eq i32 %conv4, 2
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %4 = phi i1 [ true, %entry ], [ %cmp5, %lor.rhs ]
  %lor.ext = zext i1 %4 to i32
  ret i32 %lor.ext
}

; Function Attrs: nounwind
define hidden void @av1_set_frame_refs(%struct.AV1Common* %cm, i32* %remapped_ref_idx, i32 %lst_map_idx, i32 %gld_map_idx) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %remapped_ref_idx.addr = alloca i32*, align 4
  %lst_map_idx.addr = alloca i32, align 4
  %gld_map_idx.addr = alloca i32, align 4
  %lst_frame_sort_idx = alloca i32, align 4
  %gld_frame_sort_idx = alloca i32, align 4
  %cur_order_hint = alloca i32, align 4
  %cur_frame_sort_idx = alloca i32, align 4
  %ref_frame_info = alloca [8 x %struct.REF_FRAME_INFO], align 16
  %ref_flag_list = alloca [7 x i32], align 16
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %map_idx = alloca i32, align 4
  %buf = alloca %struct.RefCntBuffer*, align 4
  %offset = alloca i32, align 4
  %fwd_start_idx = alloca i32, align 4
  %fwd_end_idx = alloca i32, align 4
  %i35 = alloca i32, align 4
  %bwd_start_idx = alloca i32, align 4
  %bwd_end_idx = alloca i32, align 4
  %i72 = alloca i32, align 4
  %ref_idx = alloca i32, align 4
  %ref_frame = alloca i8, align 1
  %ref_frame137 = alloca i8, align 1
  %i158 = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32* %remapped_ref_idx, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  store i32 %lst_map_idx, i32* %lst_map_idx.addr, align 4, !tbaa !6
  store i32 %gld_map_idx, i32* %gld_map_idx.addr, align 4, !tbaa !6
  %0 = bitcast i32* %lst_frame_sort_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 -1, i32* %lst_frame_sort_idx, align 4, !tbaa !6
  %1 = bitcast i32* %gld_frame_sort_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 -1, i32* %gld_frame_sort_idx, align 4, !tbaa !6
  %2 = bitcast i32* %cur_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 0
  %order_hint = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 2
  %4 = load i32, i32* %order_hint, align 4, !tbaa !72
  store i32 %4, i32* %cur_order_hint, align 4, !tbaa !6
  %5 = bitcast i32* %cur_frame_sort_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 37
  %order_hint_info = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 10
  %order_hint_bits_minus_1 = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %order_hint_info, i32 0, i32 1
  %7 = load i32, i32* %order_hint_bits_minus_1, align 4, !tbaa !99
  %shl = shl i32 1, %7
  store i32 %shl, i32* %cur_frame_sort_idx, align 4, !tbaa !6
  %8 = bitcast [8 x %struct.REF_FRAME_INFO]* %ref_frame_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 96, i8* %8) #6
  %9 = bitcast [7 x i32]* %ref_flag_list to i8*
  call void @llvm.lifetime.start.p0i8(i64 28, i8* %9) #6
  %10 = bitcast [7 x i32]* %ref_flag_list to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %10, i8 0, i32 28, i1 false)
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %12, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %15, i32* %map_idx, align 4, !tbaa !6
  %16 = load i32, i32* %map_idx, align 4, !tbaa !6
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %17
  %map_idx1 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx, i32 0, i32 0
  store i32 %16, i32* %map_idx1, align 4, !tbaa !100
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %18
  %sort_idx = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx2, i32 0, i32 2
  store i32 -1, i32* %sort_idx, align 4, !tbaa !102
  %19 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %20, i32 0, i32 17
  %21 = load i32, i32* %map_idx, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %21
  %22 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx3, align 4, !tbaa !2
  store %struct.RefCntBuffer* %22, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %23 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %24
  %buf5 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx4, i32 0, i32 1
  store %struct.RefCntBuffer* %23, %struct.RefCntBuffer** %buf5, align 4, !tbaa !103
  %25 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %cmp6 = icmp eq %struct.RefCntBuffer* %25, null
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %26 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %order_hint7 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %27, i32 0, i32 1
  %28 = load i32, i32* %order_hint7, align 4, !tbaa !73
  store i32 %28, i32* %offset, align 4, !tbaa !6
  %29 = load i32, i32* %offset, align 4, !tbaa !6
  %cmp8 = icmp eq i32 %29, -1
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %30 = load i32, i32* %cur_frame_sort_idx, align 4, !tbaa !6
  %31 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params9 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %31, i32 0, i32 37
  %order_hint_info10 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params9, i32 0, i32 10
  %32 = load i32, i32* %offset, align 4, !tbaa !6
  %33 = load i32, i32* %cur_order_hint, align 4, !tbaa !6
  %call = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info10, i32 %32, i32 %33)
  %add = add nsw i32 %30, %call
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ -1, %cond.true ], [ %add, %cond.false ]
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %34
  %sort_idx12 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx11, i32 0, i32 2
  store i32 %cond, i32* %sort_idx12, align 4, !tbaa !102
  %35 = load i32, i32* %map_idx, align 4, !tbaa !6
  %36 = load i32, i32* %lst_map_idx.addr, align 4, !tbaa !6
  %cmp13 = icmp eq i32 %35, %36
  br i1 %cmp13, label %if.then14, label %if.end17

if.then14:                                        ; preds = %cond.end
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %37
  %sort_idx16 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx15, i32 0, i32 2
  %38 = load i32, i32* %sort_idx16, align 4, !tbaa !102
  store i32 %38, i32* %lst_frame_sort_idx, align 4, !tbaa !6
  br label %if.end17

if.end17:                                         ; preds = %if.then14, %cond.end
  %39 = load i32, i32* %map_idx, align 4, !tbaa !6
  %40 = load i32, i32* %gld_map_idx.addr, align 4, !tbaa !6
  %cmp18 = icmp eq i32 %39, %40
  br i1 %cmp18, label %if.then19, label %if.end22

if.then19:                                        ; preds = %if.end17
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %41
  %sort_idx21 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx20, i32 0, i32 2
  %42 = load i32, i32* %sort_idx21, align 4, !tbaa !102
  store i32 %42, i32* %gld_frame_sort_idx, align 4, !tbaa !6
  br label %if.end22

if.end22:                                         ; preds = %if.then19, %if.end17
  %43 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end22, %if.then
  %44 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  %45 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %47 = load i32, i32* %lst_frame_sort_idx, align 4, !tbaa !6
  %cmp25 = icmp eq i32 %47, -1
  br i1 %cmp25, label %if.then27, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %48 = load i32, i32* %lst_frame_sort_idx, align 4, !tbaa !6
  %49 = load i32, i32* %cur_frame_sort_idx, align 4, !tbaa !6
  %cmp26 = icmp sge i32 %48, %49
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %lor.lhs.false, %for.end
  %50 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %50, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 7, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str, i32 0, i32 0))
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %lor.lhs.false
  %51 = load i32, i32* %gld_frame_sort_idx, align 4, !tbaa !6
  %cmp29 = icmp eq i32 %51, -1
  br i1 %cmp29, label %if.then32, label %lor.lhs.false30

lor.lhs.false30:                                  ; preds = %if.end28
  %52 = load i32, i32* %gld_frame_sort_idx, align 4, !tbaa !6
  %53 = load i32, i32* %cur_frame_sort_idx, align 4, !tbaa !6
  %cmp31 = icmp sge i32 %52, %53
  br i1 %cmp31, label %if.then32, label %if.end34

if.then32:                                        ; preds = %lor.lhs.false30, %if.end28
  %54 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %error33 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %54, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error33, i32 7, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %lor.lhs.false30
  %arraydecay = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 0
  %55 = bitcast %struct.REF_FRAME_INFO* %arraydecay to i8*
  call void @qsort(i8* %55, i32 8, i32 12, i32 (i8*, i8*)* @compare_ref_frame_info)
  %56 = bitcast i32* %fwd_start_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #6
  store i32 0, i32* %fwd_start_idx, align 4, !tbaa !6
  %57 = bitcast i32* %fwd_end_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #6
  store i32 7, i32* %fwd_end_idx, align 4, !tbaa !6
  %58 = bitcast i32* %i35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #6
  store i32 0, i32* %i35, align 4, !tbaa !6
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc51, %if.end34
  %59 = load i32, i32* %i35, align 4, !tbaa !6
  %cmp37 = icmp slt i32 %59, 8
  br i1 %cmp37, label %for.body39, label %for.cond.cleanup38

for.cond.cleanup38:                               ; preds = %for.cond36
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

for.body39:                                       ; preds = %for.cond36
  %60 = load i32, i32* %i35, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %60
  %sort_idx41 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx40, i32 0, i32 2
  %61 = load i32, i32* %sort_idx41, align 4, !tbaa !102
  %cmp42 = icmp eq i32 %61, -1
  br i1 %cmp42, label %if.then43, label %if.end45

if.then43:                                        ; preds = %for.body39
  %62 = load i32, i32* %fwd_start_idx, align 4, !tbaa !6
  %inc44 = add nsw i32 %62, 1
  store i32 %inc44, i32* %fwd_start_idx, align 4, !tbaa !6
  br label %for.inc51

if.end45:                                         ; preds = %for.body39
  %63 = load i32, i32* %i35, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %63
  %sort_idx47 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx46, i32 0, i32 2
  %64 = load i32, i32* %sort_idx47, align 4, !tbaa !102
  %65 = load i32, i32* %cur_frame_sort_idx, align 4, !tbaa !6
  %cmp48 = icmp sge i32 %64, %65
  br i1 %cmp48, label %if.then49, label %if.end50

if.then49:                                        ; preds = %if.end45
  %66 = load i32, i32* %i35, align 4, !tbaa !6
  %sub = sub nsw i32 %66, 1
  store i32 %sub, i32* %fwd_end_idx, align 4, !tbaa !6
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

if.end50:                                         ; preds = %if.end45
  br label %for.inc51

for.inc51:                                        ; preds = %if.end50, %if.then43
  %67 = load i32, i32* %i35, align 4, !tbaa !6
  %inc52 = add nsw i32 %67, 1
  store i32 %inc52, i32* %i35, align 4, !tbaa !6
  br label %for.cond36

cleanup53:                                        ; preds = %if.then49, %for.cond.cleanup38
  %68 = bitcast i32* %i35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  br label %for.end54

for.end54:                                        ; preds = %cleanup53
  %69 = bitcast i32* %bwd_start_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #6
  %70 = load i32, i32* %fwd_end_idx, align 4, !tbaa !6
  %add55 = add nsw i32 %70, 1
  store i32 %add55, i32* %bwd_start_idx, align 4, !tbaa !6
  %71 = bitcast i32* %bwd_end_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  store i32 7, i32* %bwd_end_idx, align 4, !tbaa !6
  %72 = load i32, i32* %bwd_start_idx, align 4, !tbaa !6
  %73 = load i32, i32* %bwd_end_idx, align 4, !tbaa !6
  %cmp56 = icmp sle i32 %72, %73
  br i1 %cmp56, label %if.then57, label %if.end60

if.then57:                                        ; preds = %for.end54
  %74 = load i32*, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  %75 = load i32, i32* %bwd_end_idx, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %75
  call void @set_ref_frame_info(i32* %74, i32 6, %struct.REF_FRAME_INFO* %arrayidx58)
  %arrayidx59 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_flag_list, i32 0, i32 6
  store i32 1, i32* %arrayidx59, align 8, !tbaa !6
  %76 = load i32, i32* %bwd_end_idx, align 4, !tbaa !6
  %dec = add nsw i32 %76, -1
  store i32 %dec, i32* %bwd_end_idx, align 4, !tbaa !6
  br label %if.end60

if.end60:                                         ; preds = %if.then57, %for.end54
  %77 = load i32, i32* %bwd_start_idx, align 4, !tbaa !6
  %78 = load i32, i32* %bwd_end_idx, align 4, !tbaa !6
  %cmp61 = icmp sle i32 %77, %78
  br i1 %cmp61, label %if.then62, label %if.end66

if.then62:                                        ; preds = %if.end60
  %79 = load i32*, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  %80 = load i32, i32* %bwd_start_idx, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %80
  call void @set_ref_frame_info(i32* %79, i32 4, %struct.REF_FRAME_INFO* %arrayidx63)
  %arrayidx64 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_flag_list, i32 0, i32 4
  store i32 1, i32* %arrayidx64, align 16, !tbaa !6
  %81 = load i32, i32* %bwd_start_idx, align 4, !tbaa !6
  %inc65 = add nsw i32 %81, 1
  store i32 %inc65, i32* %bwd_start_idx, align 4, !tbaa !6
  br label %if.end66

if.end66:                                         ; preds = %if.then62, %if.end60
  %82 = load i32, i32* %bwd_start_idx, align 4, !tbaa !6
  %83 = load i32, i32* %bwd_end_idx, align 4, !tbaa !6
  %cmp67 = icmp sle i32 %82, %83
  br i1 %cmp67, label %if.then68, label %if.end71

if.then68:                                        ; preds = %if.end66
  %84 = load i32*, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  %85 = load i32, i32* %bwd_start_idx, align 4, !tbaa !6
  %arrayidx69 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %85
  call void @set_ref_frame_info(i32* %84, i32 5, %struct.REF_FRAME_INFO* %arrayidx69)
  %arrayidx70 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_flag_list, i32 0, i32 5
  store i32 1, i32* %arrayidx70, align 4, !tbaa !6
  br label %if.end71

if.end71:                                         ; preds = %if.then68, %if.end66
  %86 = bitcast i32* %i72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #6
  %87 = load i32, i32* %fwd_start_idx, align 4, !tbaa !6
  store i32 %87, i32* %i72, align 4, !tbaa !6
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc91, %if.end71
  %88 = load i32, i32* %i72, align 4, !tbaa !6
  %89 = load i32, i32* %fwd_end_idx, align 4, !tbaa !6
  %cmp74 = icmp sle i32 %88, %89
  br i1 %cmp74, label %for.body76, label %for.cond.cleanup75

for.cond.cleanup75:                               ; preds = %for.cond73
  store i32 8, i32* %cleanup.dest.slot, align 4
  %90 = bitcast i32* %i72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #6
  br label %for.end94

for.body76:                                       ; preds = %for.cond73
  %91 = load i32, i32* %i72, align 4, !tbaa !6
  %arrayidx77 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %91
  %map_idx78 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx77, i32 0, i32 0
  %92 = load i32, i32* %map_idx78, align 4, !tbaa !100
  %93 = load i32, i32* %lst_map_idx.addr, align 4, !tbaa !6
  %cmp79 = icmp eq i32 %92, %93
  br i1 %cmp79, label %if.then80, label %if.end83

if.then80:                                        ; preds = %for.body76
  %94 = load i32*, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  %95 = load i32, i32* %i72, align 4, !tbaa !6
  %arrayidx81 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %95
  call void @set_ref_frame_info(i32* %94, i32 0, %struct.REF_FRAME_INFO* %arrayidx81)
  %arrayidx82 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_flag_list, i32 0, i32 0
  store i32 1, i32* %arrayidx82, align 16, !tbaa !6
  br label %if.end83

if.end83:                                         ; preds = %if.then80, %for.body76
  %96 = load i32, i32* %i72, align 4, !tbaa !6
  %arrayidx84 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %96
  %map_idx85 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx84, i32 0, i32 0
  %97 = load i32, i32* %map_idx85, align 4, !tbaa !100
  %98 = load i32, i32* %gld_map_idx.addr, align 4, !tbaa !6
  %cmp86 = icmp eq i32 %97, %98
  br i1 %cmp86, label %if.then87, label %if.end90

if.then87:                                        ; preds = %if.end83
  %99 = load i32*, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  %100 = load i32, i32* %i72, align 4, !tbaa !6
  %arrayidx88 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %100
  call void @set_ref_frame_info(i32* %99, i32 3, %struct.REF_FRAME_INFO* %arrayidx88)
  %arrayidx89 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_flag_list, i32 0, i32 3
  store i32 1, i32* %arrayidx89, align 4, !tbaa !6
  br label %if.end90

if.end90:                                         ; preds = %if.then87, %if.end83
  br label %for.inc91

for.inc91:                                        ; preds = %if.end90
  %101 = load i32, i32* %i72, align 4, !tbaa !6
  %inc92 = add nsw i32 %101, 1
  store i32 %inc92, i32* %i72, align 4, !tbaa !6
  br label %for.cond73

for.end94:                                        ; preds = %for.cond.cleanup75
  %102 = bitcast i32* %ref_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #6
  store i32 0, i32* %ref_idx, align 4, !tbaa !6
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc130, %for.end94
  %103 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %cmp96 = icmp slt i32 %103, 5
  br i1 %cmp96, label %for.body97, label %for.end132

for.body97:                                       ; preds = %for.cond95
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_frame) #6
  %104 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds [5 x i8], [5 x i8]* @av1_set_frame_refs.ref_frame_list, i32 0, i32 %104
  %105 = load i8, i8* %arrayidx98, align 1, !tbaa !41
  store i8 %105, i8* %ref_frame, align 1, !tbaa !41
  %106 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %conv = sext i8 %106 to i32
  %sub99 = sub nsw i32 %conv, 1
  %arrayidx100 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_flag_list, i32 0, i32 %sub99
  %107 = load i32, i32* %arrayidx100, align 4, !tbaa !6
  %cmp101 = icmp eq i32 %107, 1
  br i1 %cmp101, label %if.then103, label %if.end104

if.then103:                                       ; preds = %for.body97
  store i32 13, i32* %cleanup.dest.slot, align 4
  br label %cleanup127

if.end104:                                        ; preds = %for.body97
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end104
  %108 = load i32, i32* %fwd_start_idx, align 4, !tbaa !6
  %109 = load i32, i32* %fwd_end_idx, align 4, !tbaa !6
  %cmp105 = icmp sle i32 %108, %109
  br i1 %cmp105, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %110 = load i32, i32* %fwd_end_idx, align 4, !tbaa !6
  %arrayidx107 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %110
  %map_idx108 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx107, i32 0, i32 0
  %111 = load i32, i32* %map_idx108, align 4, !tbaa !100
  %112 = load i32, i32* %lst_map_idx.addr, align 4, !tbaa !6
  %cmp109 = icmp eq i32 %111, %112
  br i1 %cmp109, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.rhs
  %113 = load i32, i32* %fwd_end_idx, align 4, !tbaa !6
  %arrayidx111 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %113
  %map_idx112 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %arrayidx111, i32 0, i32 0
  %114 = load i32, i32* %map_idx112, align 4, !tbaa !100
  %115 = load i32, i32* %gld_map_idx.addr, align 4, !tbaa !6
  %cmp113 = icmp eq i32 %114, %115
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.rhs
  %116 = phi i1 [ true, %land.rhs ], [ %cmp113, %lor.rhs ]
  br label %land.end

land.end:                                         ; preds = %lor.end, %while.cond
  %117 = phi i1 [ false, %while.cond ], [ %116, %lor.end ]
  br i1 %117, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %118 = load i32, i32* %fwd_end_idx, align 4, !tbaa !6
  %dec115 = add nsw i32 %118, -1
  store i32 %dec115, i32* %fwd_end_idx, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %land.end
  %119 = load i32, i32* %fwd_start_idx, align 4, !tbaa !6
  %120 = load i32, i32* %fwd_end_idx, align 4, !tbaa !6
  %cmp116 = icmp sgt i32 %119, %120
  br i1 %cmp116, label %if.then118, label %if.end119

if.then118:                                       ; preds = %while.end
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup127

if.end119:                                        ; preds = %while.end
  %121 = load i32*, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  %122 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %conv120 = sext i8 %122 to i32
  %sub121 = sub nsw i32 %conv120, 1
  %123 = load i32, i32* %fwd_end_idx, align 4, !tbaa !6
  %arrayidx122 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %123
  call void @set_ref_frame_info(i32* %121, i32 %sub121, %struct.REF_FRAME_INFO* %arrayidx122)
  %124 = load i8, i8* %ref_frame, align 1, !tbaa !41
  %conv123 = sext i8 %124 to i32
  %sub124 = sub nsw i32 %conv123, 1
  %arrayidx125 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_flag_list, i32 0, i32 %sub124
  store i32 1, i32* %arrayidx125, align 4, !tbaa !6
  %125 = load i32, i32* %fwd_end_idx, align 4, !tbaa !6
  %dec126 = add nsw i32 %125, -1
  store i32 %dec126, i32* %fwd_end_idx, align 4, !tbaa !6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup127

cleanup127:                                       ; preds = %if.end119, %if.then118, %if.then103
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_frame) #6
  %cleanup.dest128 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest128, label %unreachable [
    i32 0, label %cleanup.cont129
    i32 13, label %for.inc130
    i32 11, label %for.end132
  ]

cleanup.cont129:                                  ; preds = %cleanup127
  br label %for.inc130

for.inc130:                                       ; preds = %cleanup.cont129, %cleanup127
  %126 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %inc131 = add nsw i32 %126, 1
  store i32 %inc131, i32* %ref_idx, align 4, !tbaa !6
  br label %for.cond95

for.end132:                                       ; preds = %cleanup127, %for.cond95
  br label %for.cond133

for.cond133:                                      ; preds = %for.inc155, %for.end132
  %127 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %cmp134 = icmp slt i32 %127, 5
  br i1 %cmp134, label %for.body136, label %for.end157

for.body136:                                      ; preds = %for.cond133
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_frame137) #6
  %128 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %arrayidx138 = getelementptr inbounds [5 x i8], [5 x i8]* @av1_set_frame_refs.ref_frame_list, i32 0, i32 %128
  %129 = load i8, i8* %arrayidx138, align 1, !tbaa !41
  store i8 %129, i8* %ref_frame137, align 1, !tbaa !41
  %130 = load i8, i8* %ref_frame137, align 1, !tbaa !41
  %conv139 = sext i8 %130 to i32
  %sub140 = sub nsw i32 %conv139, 1
  %arrayidx141 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_flag_list, i32 0, i32 %sub140
  %131 = load i32, i32* %arrayidx141, align 4, !tbaa !6
  %cmp142 = icmp eq i32 %131, 1
  br i1 %cmp142, label %if.then144, label %if.end145

if.then144:                                       ; preds = %for.body136
  store i32 18, i32* %cleanup.dest.slot, align 4
  br label %cleanup152

if.end145:                                        ; preds = %for.body136
  %132 = load i32*, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  %133 = load i8, i8* %ref_frame137, align 1, !tbaa !41
  %conv146 = sext i8 %133 to i32
  %sub147 = sub nsw i32 %conv146, 1
  %134 = load i32, i32* %fwd_start_idx, align 4, !tbaa !6
  %arrayidx148 = getelementptr inbounds [8 x %struct.REF_FRAME_INFO], [8 x %struct.REF_FRAME_INFO]* %ref_frame_info, i32 0, i32 %134
  call void @set_ref_frame_info(i32* %132, i32 %sub147, %struct.REF_FRAME_INFO* %arrayidx148)
  %135 = load i8, i8* %ref_frame137, align 1, !tbaa !41
  %conv149 = sext i8 %135 to i32
  %sub150 = sub nsw i32 %conv149, 1
  %arrayidx151 = getelementptr inbounds [7 x i32], [7 x i32]* %ref_flag_list, i32 0, i32 %sub150
  store i32 1, i32* %arrayidx151, align 4, !tbaa !6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup152

cleanup152:                                       ; preds = %if.end145, %if.then144
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_frame137) #6
  %cleanup.dest153 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest153, label %unreachable [
    i32 0, label %cleanup.cont154
    i32 18, label %for.inc155
  ]

cleanup.cont154:                                  ; preds = %cleanup152
  br label %for.inc155

for.inc155:                                       ; preds = %cleanup.cont154, %cleanup152
  %136 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %inc156 = add nsw i32 %136, 1
  store i32 %inc156, i32* %ref_idx, align 4, !tbaa !6
  br label %for.cond133

for.end157:                                       ; preds = %for.cond133
  %137 = bitcast i32* %i158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #6
  store i32 0, i32* %i158, align 4, !tbaa !6
  br label %for.cond159

for.cond159:                                      ; preds = %for.inc164, %for.end157
  %138 = load i32, i32* %i158, align 4, !tbaa !6
  %cmp160 = icmp slt i32 %138, 7
  br i1 %cmp160, label %for.body163, label %for.cond.cleanup162

for.cond.cleanup162:                              ; preds = %for.cond159
  store i32 19, i32* %cleanup.dest.slot, align 4
  %139 = bitcast i32* %i158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #6
  br label %for.end167

for.body163:                                      ; preds = %for.cond159
  br label %for.inc164

for.inc164:                                       ; preds = %for.body163
  %140 = load i32, i32* %i158, align 4, !tbaa !6
  %inc165 = add nsw i32 %140, 1
  store i32 %inc165, i32* %i158, align 4, !tbaa !6
  br label %for.cond159

for.end167:                                       ; preds = %for.cond.cleanup162
  %141 = bitcast i32* %ref_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #6
  %142 = bitcast i32* %bwd_end_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #6
  %143 = bitcast i32* %bwd_start_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #6
  %144 = bitcast i32* %fwd_end_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #6
  %145 = bitcast i32* %fwd_start_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #6
  %146 = bitcast [7 x i32]* %ref_flag_list to i8*
  call void @llvm.lifetime.end.p0i8(i64 28, i8* %146) #6
  %147 = bitcast [8 x %struct.REF_FRAME_INFO]* %ref_frame_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 96, i8* %147) #6
  %148 = bitcast i32* %cur_frame_sort_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #6
  %149 = bitcast i32* %cur_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #6
  %150 = bitcast i32* %gld_frame_sort_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #6
  %151 = bitcast i32* %lst_frame_sort_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #6
  ret void

unreachable:                                      ; preds = %cleanup152, %cleanup127, %cleanup
  unreachable
}

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #5

declare void @qsort(i8*, i32, i32, i32 (i8*, i8*)*) #5

; Function Attrs: nounwind
define internal i32 @compare_ref_frame_info(i8* %arg_a, i8* %arg_b) #0 {
entry:
  %retval = alloca i32, align 4
  %arg_a.addr = alloca i8*, align 4
  %arg_b.addr = alloca i8*, align 4
  %info_a = alloca %struct.REF_FRAME_INFO*, align 4
  %info_b = alloca %struct.REF_FRAME_INFO*, align 4
  %sort_idx_diff = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %arg_a, i8** %arg_a.addr, align 4, !tbaa !2
  store i8* %arg_b, i8** %arg_b.addr, align 4, !tbaa !2
  %0 = bitcast %struct.REF_FRAME_INFO** %info_a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %arg_a.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.REF_FRAME_INFO*
  store %struct.REF_FRAME_INFO* %2, %struct.REF_FRAME_INFO** %info_a, align 4, !tbaa !2
  %3 = bitcast %struct.REF_FRAME_INFO** %info_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8*, i8** %arg_b.addr, align 4, !tbaa !2
  %5 = bitcast i8* %4 to %struct.REF_FRAME_INFO*
  store %struct.REF_FRAME_INFO* %5, %struct.REF_FRAME_INFO** %info_b, align 4, !tbaa !2
  %6 = bitcast i32* %sort_idx_diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.REF_FRAME_INFO*, %struct.REF_FRAME_INFO** %info_a, align 4, !tbaa !2
  %sort_idx = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %7, i32 0, i32 2
  %8 = load i32, i32* %sort_idx, align 4, !tbaa !102
  %9 = load %struct.REF_FRAME_INFO*, %struct.REF_FRAME_INFO** %info_b, align 4, !tbaa !2
  %sort_idx1 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %9, i32 0, i32 2
  %10 = load i32, i32* %sort_idx1, align 4, !tbaa !102
  %sub = sub nsw i32 %8, %10
  store i32 %sub, i32* %sort_idx_diff, align 4, !tbaa !6
  %11 = load i32, i32* %sort_idx_diff, align 4, !tbaa !6
  %cmp = icmp ne i32 %11, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load i32, i32* %sort_idx_diff, align 4, !tbaa !6
  store i32 %12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %13 = load %struct.REF_FRAME_INFO*, %struct.REF_FRAME_INFO** %info_a, align 4, !tbaa !2
  %map_idx = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %13, i32 0, i32 0
  %14 = load i32, i32* %map_idx, align 4, !tbaa !100
  %15 = load %struct.REF_FRAME_INFO*, %struct.REF_FRAME_INFO** %info_b, align 4, !tbaa !2
  %map_idx2 = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %15, i32 0, i32 0
  %16 = load i32, i32* %map_idx2, align 4, !tbaa !100
  %sub3 = sub nsw i32 %14, %16
  store i32 %sub3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %17 = bitcast i32* %sort_idx_diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast %struct.REF_FRAME_INFO** %info_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = bitcast %struct.REF_FRAME_INFO** %info_a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

; Function Attrs: inlinehint nounwind
define internal void @set_ref_frame_info(i32* %remapped_ref_idx, i32 %frame_idx, %struct.REF_FRAME_INFO* %ref_info) #3 {
entry:
  %remapped_ref_idx.addr = alloca i32*, align 4
  %frame_idx.addr = alloca i32, align 4
  %ref_info.addr = alloca %struct.REF_FRAME_INFO*, align 4
  store i32* %remapped_ref_idx, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  store i32 %frame_idx, i32* %frame_idx.addr, align 4, !tbaa !6
  store %struct.REF_FRAME_INFO* %ref_info, %struct.REF_FRAME_INFO** %ref_info.addr, align 4, !tbaa !2
  %0 = load %struct.REF_FRAME_INFO*, %struct.REF_FRAME_INFO** %ref_info.addr, align 4, !tbaa !2
  %map_idx = getelementptr inbounds %struct.REF_FRAME_INFO, %struct.REF_FRAME_INFO* %0, i32 0, i32 0
  %1 = load i32, i32* %map_idx, align 4, !tbaa !100
  %2 = load i32*, i32** %remapped_ref_idx.addr, align 4, !tbaa !2
  %3 = load i32, i32* %frame_idx.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  store i32 %1, i32* %arrayidx, align 4, !tbaa !6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @integer_mv_precision(%struct.mv* %mv) #3 {
entry:
  %mv.addr = alloca %struct.mv*, align 4
  %mod = alloca i32, align 4
  store %struct.mv* %mv, %struct.mv** %mv.addr, align 4, !tbaa !2
  %0 = bitcast i32* %mod to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row = getelementptr inbounds %struct.mv, %struct.mv* %1, i32 0, i32 0
  %2 = load i16, i16* %row, align 2, !tbaa !69
  %conv = sext i16 %2 to i32
  %rem = srem i32 %conv, 8
  store i32 %rem, i32* %mod, align 4, !tbaa !6
  %3 = load i32, i32* %mod, align 4, !tbaa !6
  %cmp = icmp ne i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end19

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %mod, align 4, !tbaa !6
  %5 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row2 = getelementptr inbounds %struct.mv, %struct.mv* %5, i32 0, i32 0
  %6 = load i16, i16* %row2, align 2, !tbaa !69
  %conv3 = sext i16 %6 to i32
  %sub = sub nsw i32 %conv3, %4
  %conv4 = trunc i32 %sub to i16
  store i16 %conv4, i16* %row2, align 2, !tbaa !69
  %7 = load i32, i32* %mod, align 4, !tbaa !6
  %call = call i32 @abs(i32 %7) #7
  %cmp5 = icmp sgt i32 %call, 4
  br i1 %cmp5, label %if.then7, label %if.end18

if.then7:                                         ; preds = %if.then
  %8 = load i32, i32* %mod, align 4, !tbaa !6
  %cmp8 = icmp sgt i32 %8, 0
  br i1 %cmp8, label %if.then10, label %if.else

if.then10:                                        ; preds = %if.then7
  %9 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row11 = getelementptr inbounds %struct.mv, %struct.mv* %9, i32 0, i32 0
  %10 = load i16, i16* %row11, align 2, !tbaa !69
  %conv12 = sext i16 %10 to i32
  %add = add nsw i32 %conv12, 8
  %conv13 = trunc i32 %add to i16
  store i16 %conv13, i16* %row11, align 2, !tbaa !69
  br label %if.end

if.else:                                          ; preds = %if.then7
  %11 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row14 = getelementptr inbounds %struct.mv, %struct.mv* %11, i32 0, i32 0
  %12 = load i16, i16* %row14, align 2, !tbaa !69
  %conv15 = sext i16 %12 to i32
  %sub16 = sub nsw i32 %conv15, 8
  %conv17 = trunc i32 %sub16 to i16
  store i16 %conv17, i16* %row14, align 2, !tbaa !69
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then10
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %entry
  %13 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col = getelementptr inbounds %struct.mv, %struct.mv* %13, i32 0, i32 1
  %14 = load i16, i16* %col, align 2, !tbaa !71
  %conv20 = sext i16 %14 to i32
  %rem21 = srem i32 %conv20, 8
  store i32 %rem21, i32* %mod, align 4, !tbaa !6
  %15 = load i32, i32* %mod, align 4, !tbaa !6
  %cmp22 = icmp ne i32 %15, 0
  br i1 %cmp22, label %if.then24, label %if.end47

if.then24:                                        ; preds = %if.end19
  %16 = load i32, i32* %mod, align 4, !tbaa !6
  %17 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col25 = getelementptr inbounds %struct.mv, %struct.mv* %17, i32 0, i32 1
  %18 = load i16, i16* %col25, align 2, !tbaa !71
  %conv26 = sext i16 %18 to i32
  %sub27 = sub nsw i32 %conv26, %16
  %conv28 = trunc i32 %sub27 to i16
  store i16 %conv28, i16* %col25, align 2, !tbaa !71
  %19 = load i32, i32* %mod, align 4, !tbaa !6
  %call29 = call i32 @abs(i32 %19) #7
  %cmp30 = icmp sgt i32 %call29, 4
  br i1 %cmp30, label %if.then32, label %if.end46

if.then32:                                        ; preds = %if.then24
  %20 = load i32, i32* %mod, align 4, !tbaa !6
  %cmp33 = icmp sgt i32 %20, 0
  br i1 %cmp33, label %if.then35, label %if.else40

if.then35:                                        ; preds = %if.then32
  %21 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col36 = getelementptr inbounds %struct.mv, %struct.mv* %21, i32 0, i32 1
  %22 = load i16, i16* %col36, align 2, !tbaa !71
  %conv37 = sext i16 %22 to i32
  %add38 = add nsw i32 %conv37, 8
  %conv39 = trunc i32 %add38 to i16
  store i16 %conv39, i16* %col36, align 2, !tbaa !71
  br label %if.end45

if.else40:                                        ; preds = %if.then32
  %23 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col41 = getelementptr inbounds %struct.mv, %struct.mv* %23, i32 0, i32 1
  %24 = load i16, i16* %col41, align 2, !tbaa !71
  %conv42 = sext i16 %24 to i32
  %sub43 = sub nsw i32 %conv42, 8
  %conv44 = trunc i32 %sub43 to i16
  store i16 %conv44, i16* %col41, align 2, !tbaa !71
  br label %if.end45

if.end45:                                         ; preds = %if.else40, %if.then35
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.then24
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.end19
  %25 = bitcast i32* %mod to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @block_center_x(i32 %mi_col, i8 zeroext %bs) #3 {
entry:
  %mi_col.addr = alloca i32, align 4
  %bs.addr = alloca i8, align 1
  %bw = alloca i32, align 4
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i8 %bs, i8* %bs.addr, align 1, !tbaa !41
  %0 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %bs.addr, align 1, !tbaa !41
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !6
  %3 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %3, 4
  %4 = load i32, i32* %bw, align 4, !tbaa !6
  %div = sdiv i32 %4, 2
  %add = add nsw i32 %mul, %div
  %sub = sub nsw i32 %add, 1
  %5 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i32 %sub
}

; Function Attrs: inlinehint nounwind
define internal i32 @block_center_y(i32 %mi_row, i8 zeroext %bs) #3 {
entry:
  %mi_row.addr = alloca i32, align 4
  %bs.addr = alloca i8, align 1
  %bh = alloca i32, align 4
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i8 %bs, i8* %bs.addr, align 1, !tbaa !41
  %0 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %bs.addr, align 1, !tbaa !41
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %bh, align 4, !tbaa !6
  %3 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %3, 4
  %4 = load i32, i32* %bh, align 4, !tbaa !6
  %div = sdiv i32 %4, 2
  %add = add nsw i32 %mul, %div
  %sub = sub nsw i32 %add, 1
  %5 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i32 %sub
}

; Function Attrs: inlinehint nounwind
define internal i32 @convert_to_trans_prec(i32 %allow_hp, i32 %coor) #3 {
entry:
  %retval = alloca i32, align 4
  %allow_hp.addr = alloca i32, align 4
  %coor.addr = alloca i32, align 4
  store i32 %allow_hp, i32* %allow_hp.addr, align 4, !tbaa !6
  store i32 %coor, i32* %coor.addr, align 4, !tbaa !6
  %0 = load i32, i32* %allow_hp.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %coor.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %2 = load i32, i32* %coor.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %2
  %add = add nsw i32 %sub, 4096
  %shr = ashr i32 %add, 13
  %sub1 = sub nsw i32 0, %shr
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %3 = load i32, i32* %coor.addr, align 4, !tbaa !6
  %add2 = add nsw i32 %3, 4096
  %shr3 = ashr i32 %add2, 13
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub1, %cond.true ], [ %shr3, %cond.false ]
  store i32 %cond, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %coor.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %4, 0
  br i1 %cmp4, label %cond.true5, label %cond.false10

cond.true5:                                       ; preds = %if.else
  %5 = load i32, i32* %coor.addr, align 4, !tbaa !6
  %sub6 = sub nsw i32 0, %5
  %add7 = add nsw i32 %sub6, 8192
  %shr8 = ashr i32 %add7, 14
  %sub9 = sub nsw i32 0, %shr8
  br label %cond.end13

cond.false10:                                     ; preds = %if.else
  %6 = load i32, i32* %coor.addr, align 4, !tbaa !6
  %add11 = add nsw i32 %6, 8192
  %shr12 = ashr i32 %add11, 14
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false10, %cond.true5
  %cond14 = phi i32 [ %sub9, %cond.true5 ], [ %shr12, %cond.false10 ]
  %mul = mul nsw i32 %cond14, 2
  store i32 %mul, i32* %retval, align 4
  br label %return

return:                                           ; preds = %cond.end13, %cond.end
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: inlinehint nounwind
define internal i32 @find_valid_row_offset(%struct.TileInfo* %tile, i32 %mi_row, i32 %row_offset) #3 {
entry:
  %tile.addr = alloca %struct.TileInfo*, align 4
  %mi_row.addr = alloca i32, align 4
  %row_offset.addr = alloca i32, align 4
  store %struct.TileInfo* %tile, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %row_offset, i32* %row_offset.addr, align 4, !tbaa !6
  %0 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %1 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_row_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %1, i32 0, i32 0
  %2 = load i32, i32* %mi_row_start, align 4, !tbaa !91
  %3 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, %3
  %4 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_row_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %4, i32 0, i32 1
  %5 = load i32, i32* %mi_row_end, align 4, !tbaa !93
  %6 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 %5, %6
  %sub2 = sub nsw i32 %sub1, 1
  %call = call i32 @clamp(i32 %0, i32 %sub, i32 %sub2)
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define internal i32 @find_valid_col_offset(%struct.TileInfo* %tile, i32 %mi_col, i32 %col_offset) #3 {
entry:
  %tile.addr = alloca %struct.TileInfo*, align 4
  %mi_col.addr = alloca i32, align 4
  %col_offset.addr = alloca i32, align 4
  store %struct.TileInfo* %tile, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i32 %col_offset, i32* %col_offset.addr, align 4, !tbaa !6
  %0 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %1 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_col_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %1, i32 0, i32 2
  %2 = load i32, i32* %mi_col_start, align 4, !tbaa !92
  %3 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, %3
  %4 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_col_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %4, i32 0, i32 3
  %5 = load i32, i32* %mi_col_end, align 4, !tbaa !94
  %6 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 %5, %6
  %sub2 = sub nsw i32 %sub1, 1
  %call = call i32 @clamp(i32 %0, i32 %sub, i32 %sub2)
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define internal void @scan_row_mbmi(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %mi_col, i8* %rf, i32 %row_offset, %struct.candidate_mv* %ref_mv_stack, i16* %ref_mv_weight, i8* %refmv_count, i8* %ref_match_count, i8* %newmv_count, %union.int_mv* %gm_mv_candidates, i32 %max_row_offset, i32* %processed_rows) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi_col.addr = alloca i32, align 4
  %rf.addr = alloca i8*, align 4
  %row_offset.addr = alloca i32, align 4
  %ref_mv_stack.addr = alloca %struct.candidate_mv*, align 4
  %ref_mv_weight.addr = alloca i16*, align 4
  %refmv_count.addr = alloca i8*, align 4
  %ref_match_count.addr = alloca i8*, align 4
  %newmv_count.addr = alloca i8*, align 4
  %gm_mv_candidates.addr = alloca %union.int_mv*, align 4
  %max_row_offset.addr = alloca i32, align 4
  %processed_rows.addr = alloca i32*, align 4
  %end_mi = alloca i32, align 4
  %width_8x8 = alloca i32, align 4
  %width_16x16 = alloca i32, align 4
  %col_offset = alloca i32, align 4
  %use_step_16 = alloca i32, align 4
  %candidate_mi0 = alloca %struct.MB_MODE_INFO**, align 4
  %i = alloca i32, align 4
  %candidate = alloca %struct.MB_MODE_INFO*, align 4
  %candidate_bsize = alloca i32, align 4
  %n4_w = alloca i32, align 4
  %len = alloca i32, align 4
  %weight = alloca i16, align 2
  %inc = alloca i16, align 2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !2
  store i32 %row_offset, i32* %row_offset.addr, align 4, !tbaa !6
  store %struct.candidate_mv* %ref_mv_stack, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  store i16* %ref_mv_weight, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  store i8* %refmv_count, i8** %refmv_count.addr, align 4, !tbaa !2
  store i8* %ref_match_count, i8** %ref_match_count.addr, align 4, !tbaa !2
  store i8* %newmv_count, i8** %newmv_count.addr, align 4, !tbaa !2
  store %union.int_mv* %gm_mv_candidates, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  store i32 %max_row_offset, i32* %max_row_offset.addr, align 4, !tbaa !6
  store i32* %processed_rows, i32** %processed_rows.addr, align 4, !tbaa !2
  %0 = bitcast i32* %end_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 33
  %2 = load i8, i8* %width, align 4, !tbaa !60
  %conv = zext i8 %2 to i32
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %4 = load i32, i32* %mi_cols, align 4, !tbaa !8
  %5 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %4, %5
  %cmp = icmp slt i32 %conv, %sub
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 33
  %7 = load i8, i8* %width2, align 4, !tbaa !60
  %conv3 = zext i8 %7 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 22
  %mi_cols5 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params4, i32 0, i32 4
  %9 = load i32, i32* %mi_cols5, align 4, !tbaa !8
  %10 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %sub6 = sub nsw i32 %9, %10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv3, %cond.true ], [ %sub6, %cond.false ]
  store i32 %cond, i32* %end_mi, align 4, !tbaa !6
  %11 = load i32, i32* %end_mi, align 4, !tbaa !6
  %12 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv7 = zext i8 %12 to i32
  %cmp8 = icmp slt i32 %11, %conv7
  br i1 %cmp8, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %cond.end
  %13 = load i32, i32* %end_mi, align 4, !tbaa !6
  br label %cond.end13

cond.false11:                                     ; preds = %cond.end
  %14 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv12 = zext i8 %14 to i32
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false11, %cond.true10
  %cond14 = phi i32 [ %13, %cond.true10 ], [ %conv12, %cond.false11 ]
  store i32 %cond14, i32* %end_mi, align 4, !tbaa !6
  %15 = bitcast i32* %width_8x8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 3), align 1, !tbaa !41
  %conv15 = zext i8 %16 to i32
  store i32 %conv15, i32* %width_8x8, align 4, !tbaa !6
  %17 = bitcast i32* %width_16x16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 6), align 2, !tbaa !41
  %conv16 = zext i8 %18 to i32
  store i32 %conv16, i32* %width_16x16, align 4, !tbaa !6
  %19 = bitcast i32* %col_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  store i32 0, i32* %col_offset, align 4, !tbaa !6
  %20 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %call = call i32 @abs(i32 %20) #7
  %cmp17 = icmp sgt i32 %call, 1
  br i1 %cmp17, label %if.then, label %if.end24

if.then:                                          ; preds = %cond.end13
  store i32 1, i32* %col_offset, align 4, !tbaa !6
  %21 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %and = and i32 %21, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %22 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width19 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %22, i32 0, i32 33
  %23 = load i8, i8* %width19, align 4, !tbaa !60
  %conv20 = zext i8 %23 to i32
  %24 = load i32, i32* %width_8x8, align 4, !tbaa !6
  %cmp21 = icmp slt i32 %conv20, %24
  br i1 %cmp21, label %if.then23, label %if.end

if.then23:                                        ; preds = %land.lhs.true
  %25 = load i32, i32* %col_offset, align 4, !tbaa !6
  %dec = add nsw i32 %25, -1
  store i32 %dec, i32* %col_offset, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then23, %land.lhs.true, %if.then
  br label %if.end24

if.end24:                                         ; preds = %if.end, %cond.end13
  %26 = bitcast i32* %use_step_16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width25 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %27, i32 0, i32 33
  %28 = load i8, i8* %width25, align 4, !tbaa !60
  %conv26 = zext i8 %28 to i32
  %cmp27 = icmp sge i32 %conv26, 16
  %conv28 = zext i1 %cmp27 to i32
  store i32 %conv28, i32* %use_step_16, align 4, !tbaa !6
  %29 = bitcast %struct.MB_MODE_INFO*** %candidate_mi0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %30, i32 0, i32 6
  %31 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !67
  %32 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %33 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %33, i32 0, i32 2
  %34 = load i32, i32* %mi_stride, align 8, !tbaa !68
  %mul = mul nsw i32 %32, %34
  %add.ptr = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %31, i32 %mul
  store %struct.MB_MODE_INFO** %add.ptr, %struct.MB_MODE_INFO*** %candidate_mi0, align 4, !tbaa !2
  %35 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %if.end105, %if.end24
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %end_mi, align 4, !tbaa !6
  %cmp29 = icmp slt i32 %36, %37
  br i1 %cmp29, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %38 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %39 = bitcast %struct.MB_MODE_INFO** %candidate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  %40 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %candidate_mi0, align 4, !tbaa !2
  %41 = load i32, i32* %col_offset, align 4, !tbaa !6
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %41, %42
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %40, i32 %add
  %43 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %43, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %44 = bitcast i32* %candidate_bsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #6
  %45 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %45, i32 0, i32 6
  %46 = load i8, i8* %sb_type, align 2, !tbaa !48
  %conv31 = zext i8 %46 to i32
  store i32 %conv31, i32* %candidate_bsize, align 4, !tbaa !6
  %47 = bitcast i32* %n4_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  %48 = load i32, i32* %candidate_bsize, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %48
  %49 = load i8, i8* %arrayidx32, align 1, !tbaa !41
  %conv33 = zext i8 %49 to i32
  store i32 %conv33, i32* %n4_w, align 4, !tbaa !6
  %50 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #6
  %51 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width34 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %51, i32 0, i32 33
  %52 = load i8, i8* %width34, align 4, !tbaa !60
  %conv35 = zext i8 %52 to i32
  %53 = load i32, i32* %n4_w, align 4, !tbaa !6
  %cmp36 = icmp slt i32 %conv35, %53
  br i1 %cmp36, label %cond.true38, label %cond.false41

cond.true38:                                      ; preds = %for.body
  %54 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width39 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %54, i32 0, i32 33
  %55 = load i8, i8* %width39, align 4, !tbaa !60
  %conv40 = zext i8 %55 to i32
  br label %cond.end42

cond.false41:                                     ; preds = %for.body
  %56 = load i32, i32* %n4_w, align 4, !tbaa !6
  br label %cond.end42

cond.end42:                                       ; preds = %cond.false41, %cond.true38
  %cond43 = phi i32 [ %conv40, %cond.true38 ], [ %56, %cond.false41 ]
  store i32 %cond43, i32* %len, align 4, !tbaa !6
  %57 = load i32, i32* %use_step_16, align 4, !tbaa !6
  %tobool44 = icmp ne i32 %57, 0
  br i1 %tobool44, label %if.then45, label %if.else

if.then45:                                        ; preds = %cond.end42
  %58 = load i32, i32* %width_16x16, align 4, !tbaa !6
  %59 = load i32, i32* %len, align 4, !tbaa !6
  %cmp46 = icmp sgt i32 %58, %59
  br i1 %cmp46, label %cond.true48, label %cond.false49

cond.true48:                                      ; preds = %if.then45
  %60 = load i32, i32* %width_16x16, align 4, !tbaa !6
  br label %cond.end50

cond.false49:                                     ; preds = %if.then45
  %61 = load i32, i32* %len, align 4, !tbaa !6
  br label %cond.end50

cond.end50:                                       ; preds = %cond.false49, %cond.true48
  %cond51 = phi i32 [ %60, %cond.true48 ], [ %61, %cond.false49 ]
  store i32 %cond51, i32* %len, align 4, !tbaa !6
  br label %if.end63

if.else:                                          ; preds = %cond.end42
  %62 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %call52 = call i32 @abs(i32 %62) #7
  %cmp53 = icmp sgt i32 %call52, 1
  br i1 %cmp53, label %if.then55, label %if.end62

if.then55:                                        ; preds = %if.else
  %63 = load i32, i32* %len, align 4, !tbaa !6
  %64 = load i32, i32* %width_8x8, align 4, !tbaa !6
  %cmp56 = icmp sgt i32 %63, %64
  br i1 %cmp56, label %cond.true58, label %cond.false59

cond.true58:                                      ; preds = %if.then55
  %65 = load i32, i32* %len, align 4, !tbaa !6
  br label %cond.end60

cond.false59:                                     ; preds = %if.then55
  %66 = load i32, i32* %width_8x8, align 4, !tbaa !6
  br label %cond.end60

cond.end60:                                       ; preds = %cond.false59, %cond.true58
  %cond61 = phi i32 [ %65, %cond.true58 ], [ %66, %cond.false59 ]
  store i32 %cond61, i32* %len, align 4, !tbaa !6
  br label %if.end62

if.end62:                                         ; preds = %cond.end60, %if.else
  br label %if.end63

if.end63:                                         ; preds = %if.end62, %cond.end50
  %67 = bitcast i16* %weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %67) #6
  store i16 2, i16* %weight, align 2, !tbaa !58
  %68 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width64 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %68, i32 0, i32 33
  %69 = load i8, i8* %width64, align 4, !tbaa !60
  %conv65 = zext i8 %69 to i32
  %70 = load i32, i32* %width_8x8, align 4, !tbaa !6
  %cmp66 = icmp sge i32 %conv65, %70
  br i1 %cmp66, label %land.lhs.true68, label %if.end105

land.lhs.true68:                                  ; preds = %if.end63
  %71 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width69 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %71, i32 0, i32 33
  %72 = load i8, i8* %width69, align 4, !tbaa !60
  %conv70 = zext i8 %72 to i32
  %73 = load i32, i32* %n4_w, align 4, !tbaa !6
  %cmp71 = icmp sle i32 %conv70, %73
  br i1 %cmp71, label %if.then73, label %if.end105

if.then73:                                        ; preds = %land.lhs.true68
  %74 = bitcast i16* %inc to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %74) #6
  %75 = load i32, i32* %max_row_offset.addr, align 4, !tbaa !6
  %sub74 = sub nsw i32 0, %75
  %76 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %add75 = add nsw i32 %sub74, %76
  %add76 = add nsw i32 %add75, 1
  %77 = load i32, i32* %candidate_bsize, align 4, !tbaa !6
  %arrayidx77 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %77
  %78 = load i8, i8* %arrayidx77, align 1, !tbaa !41
  %conv78 = zext i8 %78 to i32
  %cmp79 = icmp slt i32 %add76, %conv78
  br i1 %cmp79, label %cond.true81, label %cond.false85

cond.true81:                                      ; preds = %if.then73
  %79 = load i32, i32* %max_row_offset.addr, align 4, !tbaa !6
  %sub82 = sub nsw i32 0, %79
  %80 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %add83 = add nsw i32 %sub82, %80
  %add84 = add nsw i32 %add83, 1
  br label %cond.end88

cond.false85:                                     ; preds = %if.then73
  %81 = load i32, i32* %candidate_bsize, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %81
  %82 = load i8, i8* %arrayidx86, align 1, !tbaa !41
  %conv87 = zext i8 %82 to i32
  br label %cond.end88

cond.end88:                                       ; preds = %cond.false85, %cond.true81
  %cond89 = phi i32 [ %add84, %cond.true81 ], [ %conv87, %cond.false85 ]
  %conv90 = trunc i32 %cond89 to i16
  store i16 %conv90, i16* %inc, align 2, !tbaa !58
  %83 = load i16, i16* %weight, align 2, !tbaa !58
  %conv91 = zext i16 %83 to i32
  %84 = load i16, i16* %inc, align 2, !tbaa !58
  %conv92 = zext i16 %84 to i32
  %cmp93 = icmp sgt i32 %conv91, %conv92
  br i1 %cmp93, label %cond.true95, label %cond.false97

cond.true95:                                      ; preds = %cond.end88
  %85 = load i16, i16* %weight, align 2, !tbaa !58
  %conv96 = zext i16 %85 to i32
  br label %cond.end99

cond.false97:                                     ; preds = %cond.end88
  %86 = load i16, i16* %inc, align 2, !tbaa !58
  %conv98 = zext i16 %86 to i32
  br label %cond.end99

cond.end99:                                       ; preds = %cond.false97, %cond.true95
  %cond100 = phi i32 [ %conv96, %cond.true95 ], [ %conv98, %cond.false97 ]
  %conv101 = trunc i32 %cond100 to i16
  store i16 %conv101, i16* %weight, align 2, !tbaa !58
  %87 = load i16, i16* %inc, align 2, !tbaa !58
  %conv102 = zext i16 %87 to i32
  %88 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %sub103 = sub nsw i32 %conv102, %88
  %sub104 = sub nsw i32 %sub103, 1
  %89 = load i32*, i32** %processed_rows.addr, align 4, !tbaa !2
  store i32 %sub104, i32* %89, align 4, !tbaa !6
  %90 = bitcast i16* %inc to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %90) #6
  br label %if.end105

if.end105:                                        ; preds = %cond.end99, %land.lhs.true68, %if.end63
  %91 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %92 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %93 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %94 = load i8*, i8** %ref_match_count.addr, align 4, !tbaa !2
  %95 = load i8*, i8** %newmv_count.addr, align 4, !tbaa !2
  %96 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %97 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %98 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %99 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %global_motion = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %99, i32 0, i32 36
  %arraydecay = getelementptr inbounds [8 x %struct.WarpedMotionParams], [8 x %struct.WarpedMotionParams]* %global_motion, i32 0, i32 0
  %100 = load i32, i32* %len, align 4, !tbaa !6
  %101 = load i16, i16* %weight, align 2, !tbaa !58
  %conv106 = zext i16 %101 to i32
  %mul107 = mul nsw i32 %100, %conv106
  %conv108 = trunc i32 %mul107 to i16
  call void @add_ref_mv_candidate(%struct.MB_MODE_INFO* %91, i8* %92, i8* %93, i8* %94, i8* %95, %struct.candidate_mv* %96, i16* %97, %union.int_mv* %98, %struct.WarpedMotionParams* %arraydecay, i16 zeroext %conv108)
  %102 = load i32, i32* %len, align 4, !tbaa !6
  %103 = load i32, i32* %i, align 4, !tbaa !6
  %add109 = add nsw i32 %103, %102
  store i32 %add109, i32* %i, align 4, !tbaa !6
  %104 = bitcast i16* %weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %104) #6
  %105 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #6
  %106 = bitcast i32* %n4_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #6
  %107 = bitcast i32* %candidate_bsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #6
  %108 = bitcast %struct.MB_MODE_INFO** %candidate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %109 = bitcast %struct.MB_MODE_INFO*** %candidate_mi0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #6
  %110 = bitcast i32* %use_step_16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast i32* %col_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  %112 = bitcast i32* %width_16x16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #6
  %113 = bitcast i32* %width_8x8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #6
  %114 = bitcast i32* %end_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @scan_col_mbmi(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %mi_row, i8* %rf, i32 %col_offset, %struct.candidate_mv* %ref_mv_stack, i16* %ref_mv_weight, i8* %refmv_count, i8* %ref_match_count, i8* %newmv_count, %union.int_mv* %gm_mv_candidates, i32 %max_col_offset, i32* %processed_cols) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi_row.addr = alloca i32, align 4
  %rf.addr = alloca i8*, align 4
  %col_offset.addr = alloca i32, align 4
  %ref_mv_stack.addr = alloca %struct.candidate_mv*, align 4
  %ref_mv_weight.addr = alloca i16*, align 4
  %refmv_count.addr = alloca i8*, align 4
  %ref_match_count.addr = alloca i8*, align 4
  %newmv_count.addr = alloca i8*, align 4
  %gm_mv_candidates.addr = alloca %union.int_mv*, align 4
  %max_col_offset.addr = alloca i32, align 4
  %processed_cols.addr = alloca i32*, align 4
  %end_mi = alloca i32, align 4
  %n8_h_8 = alloca i32, align 4
  %n8_h_16 = alloca i32, align 4
  %i = alloca i32, align 4
  %row_offset = alloca i32, align 4
  %use_step_16 = alloca i32, align 4
  %candidate = alloca %struct.MB_MODE_INFO*, align 4
  %candidate_bsize = alloca i32, align 4
  %n4_h = alloca i32, align 4
  %len = alloca i32, align 4
  %weight = alloca i32, align 4
  %inc = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !2
  store i32 %col_offset, i32* %col_offset.addr, align 4, !tbaa !6
  store %struct.candidate_mv* %ref_mv_stack, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  store i16* %ref_mv_weight, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  store i8* %refmv_count, i8** %refmv_count.addr, align 4, !tbaa !2
  store i8* %ref_match_count, i8** %ref_match_count.addr, align 4, !tbaa !2
  store i8* %newmv_count, i8** %newmv_count.addr, align 4, !tbaa !2
  store %union.int_mv* %gm_mv_candidates, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  store i32 %max_col_offset, i32* %max_col_offset.addr, align 4, !tbaa !6
  store i32* %processed_cols, i32** %processed_cols.addr, align 4, !tbaa !2
  %0 = bitcast i32* %end_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 34
  %2 = load i8, i8* %height, align 1, !tbaa !61
  %conv = zext i8 %2 to i32
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %4 = load i32, i32* %mi_rows, align 4, !tbaa !66
  %5 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %4, %5
  %cmp = icmp slt i32 %conv, %sub
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 34
  %7 = load i8, i8* %height2, align 1, !tbaa !61
  %conv3 = zext i8 %7 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 22
  %mi_rows5 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params4, i32 0, i32 3
  %9 = load i32, i32* %mi_rows5, align 4, !tbaa !66
  %10 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %sub6 = sub nsw i32 %9, %10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv3, %cond.true ], [ %sub6, %cond.false ]
  store i32 %cond, i32* %end_mi, align 4, !tbaa !6
  %11 = load i32, i32* %end_mi, align 4, !tbaa !6
  %12 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !41
  %conv7 = zext i8 %12 to i32
  %cmp8 = icmp slt i32 %11, %conv7
  br i1 %cmp8, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %cond.end
  %13 = load i32, i32* %end_mi, align 4, !tbaa !6
  br label %cond.end13

cond.false11:                                     ; preds = %cond.end
  %14 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !41
  %conv12 = zext i8 %14 to i32
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false11, %cond.true10
  %cond14 = phi i32 [ %13, %cond.true10 ], [ %conv12, %cond.false11 ]
  store i32 %cond14, i32* %end_mi, align 4, !tbaa !6
  %15 = bitcast i32* %n8_h_8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 3), align 1, !tbaa !41
  %conv15 = zext i8 %16 to i32
  store i32 %conv15, i32* %n8_h_8, align 4, !tbaa !6
  %17 = bitcast i32* %n8_h_16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 6), align 2, !tbaa !41
  %conv16 = zext i8 %18 to i32
  store i32 %conv16, i32* %n8_h_16, align 4, !tbaa !6
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = bitcast i32* %row_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  store i32 0, i32* %row_offset, align 4, !tbaa !6
  %21 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %call = call i32 @abs(i32 %21) #7
  %cmp17 = icmp sgt i32 %call, 1
  br i1 %cmp17, label %if.then, label %if.end24

if.then:                                          ; preds = %cond.end13
  store i32 1, i32* %row_offset, align 4, !tbaa !6
  %22 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %and = and i32 %22, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %23 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height19 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %23, i32 0, i32 34
  %24 = load i8, i8* %height19, align 1, !tbaa !61
  %conv20 = zext i8 %24 to i32
  %25 = load i32, i32* %n8_h_8, align 4, !tbaa !6
  %cmp21 = icmp slt i32 %conv20, %25
  br i1 %cmp21, label %if.then23, label %if.end

if.then23:                                        ; preds = %land.lhs.true
  %26 = load i32, i32* %row_offset, align 4, !tbaa !6
  %dec = add nsw i32 %26, -1
  store i32 %dec, i32* %row_offset, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then23, %land.lhs.true, %if.then
  br label %if.end24

if.end24:                                         ; preds = %if.end, %cond.end13
  %27 = bitcast i32* %use_step_16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height25 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %28, i32 0, i32 34
  %29 = load i8, i8* %height25, align 1, !tbaa !61
  %conv26 = zext i8 %29 to i32
  %cmp27 = icmp sge i32 %conv26, 16
  %conv28 = zext i1 %cmp27 to i32
  store i32 %conv28, i32* %use_step_16, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %if.end99, %if.end24
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %31 = load i32, i32* %end_mi, align 4, !tbaa !6
  %cmp29 = icmp slt i32 %30, %31
  br i1 %cmp29, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %32 = bitcast %struct.MB_MODE_INFO** %candidate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  %33 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %33, i32 0, i32 6
  %34 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !67
  %35 = load i32, i32* %row_offset, align 4, !tbaa !6
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %35, %36
  %37 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %37, i32 0, i32 2
  %38 = load i32, i32* %mi_stride, align 8, !tbaa !68
  %mul = mul nsw i32 %add, %38
  %39 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %add31 = add nsw i32 %mul, %39
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %34, i32 %add31
  %40 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %40, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %41 = bitcast i32* %candidate_bsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  %42 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %42, i32 0, i32 6
  %43 = load i8, i8* %sb_type, align 2, !tbaa !48
  %conv32 = zext i8 %43 to i32
  store i32 %conv32, i32* %candidate_bsize, align 4, !tbaa !6
  %44 = bitcast i32* %n4_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #6
  %45 = load i32, i32* %candidate_bsize, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %45
  %46 = load i8, i8* %arrayidx33, align 1, !tbaa !41
  %conv34 = zext i8 %46 to i32
  store i32 %conv34, i32* %n4_h, align 4, !tbaa !6
  %47 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  %48 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height35 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %48, i32 0, i32 34
  %49 = load i8, i8* %height35, align 1, !tbaa !61
  %conv36 = zext i8 %49 to i32
  %50 = load i32, i32* %n4_h, align 4, !tbaa !6
  %cmp37 = icmp slt i32 %conv36, %50
  br i1 %cmp37, label %cond.true39, label %cond.false42

cond.true39:                                      ; preds = %for.body
  %51 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height40 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %51, i32 0, i32 34
  %52 = load i8, i8* %height40, align 1, !tbaa !61
  %conv41 = zext i8 %52 to i32
  br label %cond.end43

cond.false42:                                     ; preds = %for.body
  %53 = load i32, i32* %n4_h, align 4, !tbaa !6
  br label %cond.end43

cond.end43:                                       ; preds = %cond.false42, %cond.true39
  %cond44 = phi i32 [ %conv41, %cond.true39 ], [ %53, %cond.false42 ]
  store i32 %cond44, i32* %len, align 4, !tbaa !6
  %54 = load i32, i32* %use_step_16, align 4, !tbaa !6
  %tobool45 = icmp ne i32 %54, 0
  br i1 %tobool45, label %if.then46, label %if.else

if.then46:                                        ; preds = %cond.end43
  %55 = load i32, i32* %n8_h_16, align 4, !tbaa !6
  %56 = load i32, i32* %len, align 4, !tbaa !6
  %cmp47 = icmp sgt i32 %55, %56
  br i1 %cmp47, label %cond.true49, label %cond.false50

cond.true49:                                      ; preds = %if.then46
  %57 = load i32, i32* %n8_h_16, align 4, !tbaa !6
  br label %cond.end51

cond.false50:                                     ; preds = %if.then46
  %58 = load i32, i32* %len, align 4, !tbaa !6
  br label %cond.end51

cond.end51:                                       ; preds = %cond.false50, %cond.true49
  %cond52 = phi i32 [ %57, %cond.true49 ], [ %58, %cond.false50 ]
  store i32 %cond52, i32* %len, align 4, !tbaa !6
  br label %if.end64

if.else:                                          ; preds = %cond.end43
  %59 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %call53 = call i32 @abs(i32 %59) #7
  %cmp54 = icmp sgt i32 %call53, 1
  br i1 %cmp54, label %if.then56, label %if.end63

if.then56:                                        ; preds = %if.else
  %60 = load i32, i32* %len, align 4, !tbaa !6
  %61 = load i32, i32* %n8_h_8, align 4, !tbaa !6
  %cmp57 = icmp sgt i32 %60, %61
  br i1 %cmp57, label %cond.true59, label %cond.false60

cond.true59:                                      ; preds = %if.then56
  %62 = load i32, i32* %len, align 4, !tbaa !6
  br label %cond.end61

cond.false60:                                     ; preds = %if.then56
  %63 = load i32, i32* %n8_h_8, align 4, !tbaa !6
  br label %cond.end61

cond.end61:                                       ; preds = %cond.false60, %cond.true59
  %cond62 = phi i32 [ %62, %cond.true59 ], [ %63, %cond.false60 ]
  store i32 %cond62, i32* %len, align 4, !tbaa !6
  br label %if.end63

if.end63:                                         ; preds = %cond.end61, %if.else
  br label %if.end64

if.end64:                                         ; preds = %if.end63, %cond.end51
  %64 = bitcast i32* %weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #6
  store i32 2, i32* %weight, align 4, !tbaa !6
  %65 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height65 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %65, i32 0, i32 34
  %66 = load i8, i8* %height65, align 1, !tbaa !61
  %conv66 = zext i8 %66 to i32
  %67 = load i32, i32* %n8_h_8, align 4, !tbaa !6
  %cmp67 = icmp sge i32 %conv66, %67
  br i1 %cmp67, label %land.lhs.true69, label %if.end99

land.lhs.true69:                                  ; preds = %if.end64
  %68 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height70 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %68, i32 0, i32 34
  %69 = load i8, i8* %height70, align 1, !tbaa !61
  %conv71 = zext i8 %69 to i32
  %70 = load i32, i32* %n4_h, align 4, !tbaa !6
  %cmp72 = icmp sle i32 %conv71, %70
  br i1 %cmp72, label %if.then74, label %if.end99

if.then74:                                        ; preds = %land.lhs.true69
  %71 = bitcast i32* %inc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  %72 = load i32, i32* %max_col_offset.addr, align 4, !tbaa !6
  %sub75 = sub nsw i32 0, %72
  %73 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %add76 = add nsw i32 %sub75, %73
  %add77 = add nsw i32 %add76, 1
  %74 = load i32, i32* %candidate_bsize, align 4, !tbaa !6
  %arrayidx78 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %74
  %75 = load i8, i8* %arrayidx78, align 1, !tbaa !41
  %conv79 = zext i8 %75 to i32
  %cmp80 = icmp slt i32 %add77, %conv79
  br i1 %cmp80, label %cond.true82, label %cond.false86

cond.true82:                                      ; preds = %if.then74
  %76 = load i32, i32* %max_col_offset.addr, align 4, !tbaa !6
  %sub83 = sub nsw i32 0, %76
  %77 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %add84 = add nsw i32 %sub83, %77
  %add85 = add nsw i32 %add84, 1
  br label %cond.end89

cond.false86:                                     ; preds = %if.then74
  %78 = load i32, i32* %candidate_bsize, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %78
  %79 = load i8, i8* %arrayidx87, align 1, !tbaa !41
  %conv88 = zext i8 %79 to i32
  br label %cond.end89

cond.end89:                                       ; preds = %cond.false86, %cond.true82
  %cond90 = phi i32 [ %add85, %cond.true82 ], [ %conv88, %cond.false86 ]
  store i32 %cond90, i32* %inc, align 4, !tbaa !6
  %80 = load i32, i32* %weight, align 4, !tbaa !6
  %81 = load i32, i32* %inc, align 4, !tbaa !6
  %cmp91 = icmp sgt i32 %80, %81
  br i1 %cmp91, label %cond.true93, label %cond.false94

cond.true93:                                      ; preds = %cond.end89
  %82 = load i32, i32* %weight, align 4, !tbaa !6
  br label %cond.end95

cond.false94:                                     ; preds = %cond.end89
  %83 = load i32, i32* %inc, align 4, !tbaa !6
  br label %cond.end95

cond.end95:                                       ; preds = %cond.false94, %cond.true93
  %cond96 = phi i32 [ %82, %cond.true93 ], [ %83, %cond.false94 ]
  store i32 %cond96, i32* %weight, align 4, !tbaa !6
  %84 = load i32, i32* %inc, align 4, !tbaa !6
  %85 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %sub97 = sub nsw i32 %84, %85
  %sub98 = sub nsw i32 %sub97, 1
  %86 = load i32*, i32** %processed_cols.addr, align 4, !tbaa !2
  store i32 %sub98, i32* %86, align 4, !tbaa !6
  %87 = bitcast i32* %inc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #6
  br label %if.end99

if.end99:                                         ; preds = %cond.end95, %land.lhs.true69, %if.end64
  %88 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %89 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %90 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %91 = load i8*, i8** %ref_match_count.addr, align 4, !tbaa !2
  %92 = load i8*, i8** %newmv_count.addr, align 4, !tbaa !2
  %93 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %94 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %95 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %96 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %global_motion = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %96, i32 0, i32 36
  %arraydecay = getelementptr inbounds [8 x %struct.WarpedMotionParams], [8 x %struct.WarpedMotionParams]* %global_motion, i32 0, i32 0
  %97 = load i32, i32* %len, align 4, !tbaa !6
  %98 = load i32, i32* %weight, align 4, !tbaa !6
  %mul100 = mul nsw i32 %97, %98
  %conv101 = trunc i32 %mul100 to i16
  call void @add_ref_mv_candidate(%struct.MB_MODE_INFO* %88, i8* %89, i8* %90, i8* %91, i8* %92, %struct.candidate_mv* %93, i16* %94, %union.int_mv* %95, %struct.WarpedMotionParams* %arraydecay, i16 zeroext %conv101)
  %99 = load i32, i32* %len, align 4, !tbaa !6
  %100 = load i32, i32* %i, align 4, !tbaa !6
  %add102 = add nsw i32 %100, %99
  store i32 %add102, i32* %i, align 4, !tbaa !6
  %101 = bitcast i32* %weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #6
  %102 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  %103 = bitcast i32* %n4_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast i32* %candidate_bsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  %105 = bitcast %struct.MB_MODE_INFO** %candidate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %106 = bitcast i32* %use_step_16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #6
  %107 = bitcast i32* %row_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #6
  %108 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #6
  %109 = bitcast i32* %n8_h_16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #6
  %110 = bitcast i32* %n8_h_8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast i32* %end_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @scan_blk_mbmi(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %mi_row, i32 %mi_col, i8* %rf, i32 %row_offset, i32 %col_offset, %struct.candidate_mv* %ref_mv_stack, i16* %ref_mv_weight, i8* %ref_match_count, i8* %newmv_count, %union.int_mv* %gm_mv_candidates, i8* %refmv_count) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %rf.addr = alloca i8*, align 4
  %row_offset.addr = alloca i32, align 4
  %col_offset.addr = alloca i32, align 4
  %ref_mv_stack.addr = alloca %struct.candidate_mv*, align 4
  %ref_mv_weight.addr = alloca i16*, align 4
  %ref_match_count.addr = alloca i8*, align 4
  %newmv_count.addr = alloca i8*, align 4
  %gm_mv_candidates.addr = alloca %union.int_mv*, align 4
  %refmv_count.addr = alloca i8*, align 4
  %tile = alloca %struct.TileInfo*, align 4
  %mi_pos = alloca %struct.position, align 4
  %candidate = alloca %struct.MB_MODE_INFO*, align 4
  %len = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !2
  store i32 %row_offset, i32* %row_offset.addr, align 4, !tbaa !6
  store i32 %col_offset, i32* %col_offset.addr, align 4, !tbaa !6
  store %struct.candidate_mv* %ref_mv_stack, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  store i16* %ref_mv_weight, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  store i8* %ref_match_count, i8** %ref_match_count.addr, align 4, !tbaa !2
  store i8* %newmv_count, i8** %newmv_count.addr, align 4, !tbaa !2
  store %union.int_mv* %gm_mv_candidates, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  store i8* %refmv_count, i8** %refmv_count.addr, align 4, !tbaa !2
  %0 = bitcast %struct.TileInfo** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tile1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 5
  store %struct.TileInfo* %tile1, %struct.TileInfo** %tile, align 4, !tbaa !2
  %2 = bitcast %struct.position* %mi_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #6
  %3 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %row = getelementptr inbounds %struct.position, %struct.position* %mi_pos, i32 0, i32 0
  store i32 %3, i32* %row, align 4, !tbaa !85
  %4 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %col = getelementptr inbounds %struct.position, %struct.position* %mi_pos, i32 0, i32 1
  store i32 %4, i32* %col, align 4, !tbaa !87
  %5 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !2
  %6 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %7 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %call = call i32 @is_inside(%struct.TileInfo* %5, i32 %6, i32 %7, %struct.position* %mi_pos)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = bitcast %struct.MB_MODE_INFO** %candidate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %9, i32 0, i32 6
  %10 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !67
  %row2 = getelementptr inbounds %struct.position, %struct.position* %mi_pos, i32 0, i32 0
  %11 = load i32, i32* %row2, align 4, !tbaa !85
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %12, i32 0, i32 2
  %13 = load i32, i32* %mi_stride, align 8, !tbaa !68
  %mul = mul nsw i32 %11, %13
  %col3 = getelementptr inbounds %struct.position, %struct.position* %mi_pos, i32 0, i32 1
  %14 = load i32, i32* %col3, align 4, !tbaa !87
  %add = add nsw i32 %mul, %14
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %10, i32 %add
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %15, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %16 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 3), align 1, !tbaa !41
  %conv = zext i8 %17 to i32
  store i32 %conv, i32* %len, align 4, !tbaa !6
  %18 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate, align 4, !tbaa !2
  %19 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %21 = load i8*, i8** %ref_match_count.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %newmv_count.addr, align 4, !tbaa !2
  %23 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %24 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %25 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %26 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %global_motion = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %26, i32 0, i32 36
  %arraydecay = getelementptr inbounds [8 x %struct.WarpedMotionParams], [8 x %struct.WarpedMotionParams]* %global_motion, i32 0, i32 0
  %27 = load i32, i32* %len, align 4, !tbaa !6
  %mul4 = mul nsw i32 2, %27
  %conv5 = trunc i32 %mul4 to i16
  call void @add_ref_mv_candidate(%struct.MB_MODE_INFO* %18, i8* %19, i8* %20, i8* %21, i8* %22, %struct.candidate_mv* %23, i16* %24, %union.int_mv* %25, %struct.WarpedMotionParams* %arraydecay, i16 zeroext %conv5)
  %28 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  %29 = bitcast %struct.MB_MODE_INFO** %candidate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %30 = bitcast %struct.position* %mi_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %30) #6
  %31 = bitcast %struct.TileInfo** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  ret void
}

; Function Attrs: nounwind
define internal i32 @add_tpl_ref_mv(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %mi_row, i32 %mi_col, i8 signext %ref_frame, i32 %blk_row, i32 %blk_col, %union.int_mv* %gm_mv_candidates, i8* %refmv_count, %struct.candidate_mv* %ref_mv_stack, i16* %ref_mv_weight, i16* %mode_context) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %ref_frame.addr = alloca i8, align 1
  %blk_row.addr = alloca i32, align 4
  %blk_col.addr = alloca i32, align 4
  %gm_mv_candidates.addr = alloca %union.int_mv*, align 4
  %refmv_count.addr = alloca i8*, align 4
  %ref_mv_stack.addr = alloca %struct.candidate_mv*, align 4
  %ref_mv_weight.addr = alloca i16*, align 4
  %mode_context.addr = alloca i16*, align 4
  %mi_pos = alloca %struct.position, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %prev_frame_mvs = alloca %struct.TPL_MV_REF*, align 4
  %rf = alloca [2 x i8], align 1
  %weight_unit = alloca i16, align 2
  %cur_frame_index = alloca i32, align 4
  %buf_0 = alloca %struct.RefCntBuffer*, align 4
  %frame0_index = alloca i32, align 4
  %cur_offset_0 = alloca i32, align 4
  %idx = alloca i32, align 4
  %allow_high_precision_mv = alloca i32, align 4
  %force_integer_mv = alloca i32, align 4
  %this_refmv = alloca %union.int_mv, align 4
  %buf_1 = alloca %struct.RefCntBuffer*, align 4
  %frame1_index = alloca i32, align 4
  %cur_offset_1 = alloca i32, align 4
  %comp_refmv = alloca %union.int_mv, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !41
  store i32 %blk_row, i32* %blk_row.addr, align 4, !tbaa !6
  store i32 %blk_col, i32* %blk_col.addr, align 4, !tbaa !6
  store %union.int_mv* %gm_mv_candidates, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  store i8* %refmv_count, i8** %refmv_count.addr, align 4, !tbaa !2
  store %struct.candidate_mv* %ref_mv_stack, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  store i16* %ref_mv_weight, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  store i16* %mode_context, i16** %mode_context.addr, align 4, !tbaa !2
  %0 = bitcast %struct.position* %mi_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %and = and i32 %1, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %add = add nsw i32 %3, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %2, %cond.true ], [ %add, %cond.false ]
  %row = getelementptr inbounds %struct.position, %struct.position* %mi_pos, i32 0, i32 0
  store i32 %cond, i32* %row, align 4, !tbaa !85
  %4 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %and1 = and i32 %4, 1
  %tobool2 = icmp ne i32 %and1, 0
  br i1 %tobool2, label %cond.true3, label %cond.false4

cond.true3:                                       ; preds = %cond.end
  %5 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  br label %cond.end6

cond.false4:                                      ; preds = %cond.end
  %6 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %add5 = add nsw i32 %6, 1
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false4, %cond.true3
  %cond7 = phi i32 [ %5, %cond.true3 ], [ %add5, %cond.false4 ]
  %col = getelementptr inbounds %struct.position, %struct.position* %mi_pos, i32 0, i32 1
  store i32 %cond7, i32* %col, align 4, !tbaa !87
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tile = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 5
  %8 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %9 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %call = call i32 @is_inside(%struct.TileInfo* %tile, i32 %8, i32 %9, %struct.position* %mi_pos)
  %tobool8 = icmp ne i32 %call, 0
  br i1 %tobool8, label %if.end, label %if.then

if.then:                                          ; preds = %cond.end6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup225

if.end:                                           ; preds = %cond.end6
  %10 = bitcast %struct.TPL_MV_REF** %prev_frame_mvs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tpl_mvs = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 45
  %12 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %tpl_mvs, align 4, !tbaa !79
  %13 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %row9 = getelementptr inbounds %struct.position, %struct.position* %mi_pos, i32 0, i32 0
  %14 = load i32, i32* %row9, align 4, !tbaa !85
  %add10 = add nsw i32 %13, %14
  %shr = ashr i32 %add10, 1
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 22
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 11
  %16 = load i32, i32* %mi_stride, align 4, !tbaa !80
  %shr11 = ashr i32 %16, 1
  %mul = mul nsw i32 %shr, %shr11
  %add.ptr = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %12, i32 %mul
  %17 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %col12 = getelementptr inbounds %struct.position, %struct.position* %mi_pos, i32 0, i32 1
  %18 = load i32, i32* %col12, align 4, !tbaa !87
  %add13 = add nsw i32 %17, %18
  %shr14 = ashr i32 %add13, 1
  %add.ptr15 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %add.ptr, i32 %shr14
  store %struct.TPL_MV_REF* %add.ptr15, %struct.TPL_MV_REF** %prev_frame_mvs, align 4, !tbaa !2
  %19 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %prev_frame_mvs, align 4, !tbaa !2
  %mfmv0 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %19, i32 0, i32 0
  %as_int = bitcast %union.int_mv* %mfmv0 to i32*
  %20 = load i32, i32* %as_int, align 4, !tbaa !41
  %cmp = icmp eq i32 %20, -2147450880
  br i1 %cmp, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %if.end
  %21 = bitcast [2 x i8]* %rf to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %21) #6
  %arraydecay = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %22 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  call void @av1_set_ref_frame(i8* %arraydecay, i8 signext %22)
  %23 = bitcast i16* %weight_unit to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %23) #6
  store i16 1, i16* %weight_unit, align 2, !tbaa !58
  %24 = bitcast i32* %cur_frame_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %25, i32 0, i32 13
  %26 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !32
  %order_hint = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %26, i32 0, i32 1
  %27 = load i32, i32* %order_hint, align 4, !tbaa !73
  store i32 %27, i32* %cur_frame_index, align 4, !tbaa !6
  %28 = bitcast %struct.RefCntBuffer** %buf_0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %29 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 0
  %30 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %call18 = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %29, i8 signext %30)
  store %struct.RefCntBuffer* %call18, %struct.RefCntBuffer** %buf_0, align 4, !tbaa !2
  %31 = bitcast i32* %frame0_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf_0, align 4, !tbaa !2
  %order_hint19 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %32, i32 0, i32 1
  %33 = load i32, i32* %order_hint19, align 4, !tbaa !73
  store i32 %33, i32* %frame0_index, align 4, !tbaa !6
  %34 = bitcast i32* %cur_offset_0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %35 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %35, i32 0, i32 37
  %order_hint_info = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 10
  %36 = load i32, i32* %cur_frame_index, align 4, !tbaa !6
  %37 = load i32, i32* %frame0_index, align 4, !tbaa !6
  %call20 = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info, i32 %36, i32 %37)
  store i32 %call20, i32* %cur_offset_0, align 4, !tbaa !6
  %38 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  %39 = bitcast i32* %allow_high_precision_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  %40 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %40, i32 0, i32 21
  %allow_high_precision_mv21 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 1
  %41 = load i8, i8* %allow_high_precision_mv21, align 1, !tbaa !54, !range !55
  %tobool22 = trunc i8 %41 to i1
  %conv = zext i1 %tobool22 to i32
  store i32 %conv, i32* %allow_high_precision_mv, align 4, !tbaa !6
  %42 = bitcast i32* %force_integer_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #6
  %43 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %features23 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %43, i32 0, i32 21
  %cur_frame_force_integer_mv = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features23, i32 0, i32 2
  %44 = load i8, i8* %cur_frame_force_integer_mv, align 2, !tbaa !56, !range !55
  %tobool24 = trunc i8 %44 to i1
  %conv25 = zext i1 %tobool24 to i32
  store i32 %conv25, i32* %force_integer_mv, align 4, !tbaa !6
  %45 = bitcast %union.int_mv* %this_refmv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #6
  %as_mv = bitcast %union.int_mv* %this_refmv to %struct.mv*
  %46 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %prev_frame_mvs, align 4, !tbaa !2
  %mfmv026 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %46, i32 0, i32 0
  %as_mv27 = bitcast %union.int_mv* %mfmv026 to %struct.mv*
  %47 = load i32, i32* %cur_offset_0, align 4, !tbaa !6
  %48 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %prev_frame_mvs, align 4, !tbaa !2
  %ref_frame_offset = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %48, i32 0, i32 1
  %49 = load i8, i8* %ref_frame_offset, align 4, !tbaa !39
  %conv28 = zext i8 %49 to i32
  call void @get_mv_projection(%struct.mv* %as_mv, %struct.mv* byval(%struct.mv) align 2 %as_mv27, i32 %47, i32 %conv28)
  %as_mv29 = bitcast %union.int_mv* %this_refmv to %struct.mv*
  %50 = load i32, i32* %allow_high_precision_mv, align 4, !tbaa !6
  %51 = load i32, i32* %force_integer_mv, align 4, !tbaa !6
  call void @lower_mv_precision(%struct.mv* %as_mv29, i32 %50, i32 %51)
  %arrayidx30 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 1
  %52 = load i8, i8* %arrayidx30, align 1, !tbaa !41
  %conv31 = sext i8 %52 to i32
  %cmp32 = icmp eq i32 %conv31, -1
  br i1 %cmp32, label %if.then34, label %if.else

if.then34:                                        ; preds = %if.end17
  %53 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %cmp35 = icmp eq i32 %53, 0
  br i1 %cmp35, label %land.lhs.true, label %if.end66

land.lhs.true:                                    ; preds = %if.then34
  %54 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %cmp37 = icmp eq i32 %54, 0
  br i1 %cmp37, label %if.then39, label %if.end66

if.then39:                                        ; preds = %land.lhs.true
  %as_mv40 = bitcast %union.int_mv* %this_refmv to %struct.mv*
  %row41 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv40, i32 0, i32 0
  %55 = load i16, i16* %row41, align 4, !tbaa !41
  %conv42 = sext i16 %55 to i32
  %56 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds %union.int_mv, %union.int_mv* %56, i32 0
  %as_mv44 = bitcast %union.int_mv* %arrayidx43 to %struct.mv*
  %row45 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv44, i32 0, i32 0
  %57 = load i16, i16* %row45, align 4, !tbaa !41
  %conv46 = sext i16 %57 to i32
  %sub = sub nsw i32 %conv42, %conv46
  %call47 = call i32 @abs(i32 %sub) #7
  %cmp48 = icmp sge i32 %call47, 16
  br i1 %cmp48, label %if.then61, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then39
  %as_mv50 = bitcast %union.int_mv* %this_refmv to %struct.mv*
  %col51 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv50, i32 0, i32 1
  %58 = load i16, i16* %col51, align 2, !tbaa !41
  %conv52 = sext i16 %58 to i32
  %59 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds %union.int_mv, %union.int_mv* %59, i32 0
  %as_mv54 = bitcast %union.int_mv* %arrayidx53 to %struct.mv*
  %col55 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv54, i32 0, i32 1
  %60 = load i16, i16* %col55, align 2, !tbaa !41
  %conv56 = sext i16 %60 to i32
  %sub57 = sub nsw i32 %conv52, %conv56
  %call58 = call i32 @abs(i32 %sub57) #7
  %cmp59 = icmp sge i32 %call58, 16
  br i1 %cmp59, label %if.then61, label %if.end65

if.then61:                                        ; preds = %lor.lhs.false, %if.then39
  %61 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %62 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom = sext i8 %62 to i32
  %arrayidx62 = getelementptr inbounds i16, i16* %61, i32 %idxprom
  %63 = load i16, i16* %arrayidx62, align 2, !tbaa !58
  %conv63 = sext i16 %63 to i32
  %or = or i32 %conv63, 8
  %conv64 = trunc i32 %or to i16
  store i16 %conv64, i16* %arrayidx62, align 2, !tbaa !58
  br label %if.end65

if.end65:                                         ; preds = %if.then61, %lor.lhs.false
  br label %if.end66

if.end66:                                         ; preds = %if.end65, %land.lhs.true, %if.then34
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end66
  %64 = load i32, i32* %idx, align 4, !tbaa !6
  %65 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %66 = load i8, i8* %65, align 1, !tbaa !41
  %conv67 = zext i8 %66 to i32
  %cmp68 = icmp slt i32 %64, %conv67
  br i1 %cmp68, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %as_int70 = bitcast %union.int_mv* %this_refmv to i32*
  %67 = load i32, i32* %as_int70, align 4, !tbaa !41
  %68 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %69 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx71 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %68, i32 %69
  %this_mv = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx71, i32 0, i32 0
  %as_int72 = bitcast %union.int_mv* %this_mv to i32*
  %70 = load i32, i32* %as_int72, align 4, !tbaa !41
  %cmp73 = icmp eq i32 %67, %70
  br i1 %cmp73, label %if.then75, label %if.end76

if.then75:                                        ; preds = %for.body
  br label %for.end

if.end76:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end76
  %71 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %71, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %if.then75, %for.cond
  %72 = load i32, i32* %idx, align 4, !tbaa !6
  %73 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %74 = load i8, i8* %73, align 1, !tbaa !41
  %conv77 = zext i8 %74 to i32
  %cmp78 = icmp slt i32 %72, %conv77
  br i1 %cmp78, label %if.then80, label %if.end85

if.then80:                                        ; preds = %for.end
  %75 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %76 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx81 = getelementptr inbounds i16, i16* %75, i32 %76
  %77 = load i16, i16* %arrayidx81, align 2, !tbaa !58
  %conv82 = zext i16 %77 to i32
  %add83 = add nsw i32 %conv82, 2
  %conv84 = trunc i32 %add83 to i16
  store i16 %conv84, i16* %arrayidx81, align 2, !tbaa !58
  br label %if.end85

if.end85:                                         ; preds = %if.then80, %for.end
  %78 = load i32, i32* %idx, align 4, !tbaa !6
  %79 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %80 = load i8, i8* %79, align 1, !tbaa !41
  %conv86 = zext i8 %80 to i32
  %cmp87 = icmp eq i32 %78, %conv86
  br i1 %cmp87, label %land.lhs.true89, label %if.end100

land.lhs.true89:                                  ; preds = %if.end85
  %81 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %82 = load i8, i8* %81, align 1, !tbaa !41
  %conv90 = zext i8 %82 to i32
  %cmp91 = icmp slt i32 %conv90, 8
  br i1 %cmp91, label %if.then93, label %if.end100

if.then93:                                        ; preds = %land.lhs.true89
  %as_int94 = bitcast %union.int_mv* %this_refmv to i32*
  %83 = load i32, i32* %as_int94, align 4, !tbaa !41
  %84 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %85 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx95 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %84, i32 %85
  %this_mv96 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx95, i32 0, i32 0
  %as_int97 = bitcast %union.int_mv* %this_mv96 to i32*
  store i32 %83, i32* %as_int97, align 4, !tbaa !41
  %86 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %87 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds i16, i16* %86, i32 %87
  store i16 2, i16* %arrayidx98, align 2, !tbaa !58
  %88 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %89 = load i8, i8* %88, align 1, !tbaa !41
  %inc99 = add i8 %89, 1
  store i8 %inc99, i8* %88, align 1, !tbaa !41
  br label %if.end100

if.end100:                                        ; preds = %if.then93, %land.lhs.true89, %if.end85
  br label %if.end224

if.else:                                          ; preds = %if.end17
  %90 = bitcast %struct.RefCntBuffer** %buf_1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  %91 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds [2 x i8], [2 x i8]* %rf, i32 0, i32 1
  %92 = load i8, i8* %arrayidx101, align 1, !tbaa !41
  %call102 = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %91, i8 signext %92)
  store %struct.RefCntBuffer* %call102, %struct.RefCntBuffer** %buf_1, align 4, !tbaa !2
  %93 = bitcast i32* %frame1_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #6
  %94 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf_1, align 4, !tbaa !2
  %order_hint103 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %94, i32 0, i32 1
  %95 = load i32, i32* %order_hint103, align 4, !tbaa !73
  store i32 %95, i32* %frame1_index, align 4, !tbaa !6
  %96 = bitcast i32* %cur_offset_1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #6
  %97 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params104 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %97, i32 0, i32 37
  %order_hint_info105 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params104, i32 0, i32 10
  %98 = load i32, i32* %cur_frame_index, align 4, !tbaa !6
  %99 = load i32, i32* %frame1_index, align 4, !tbaa !6
  %call106 = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info105, i32 %98, i32 %99)
  store i32 %call106, i32* %cur_offset_1, align 4, !tbaa !6
  %100 = bitcast %union.int_mv* %comp_refmv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #6
  %as_mv107 = bitcast %union.int_mv* %comp_refmv to %struct.mv*
  %101 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %prev_frame_mvs, align 4, !tbaa !2
  %mfmv0108 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %101, i32 0, i32 0
  %as_mv109 = bitcast %union.int_mv* %mfmv0108 to %struct.mv*
  %102 = load i32, i32* %cur_offset_1, align 4, !tbaa !6
  %103 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %prev_frame_mvs, align 4, !tbaa !2
  %ref_frame_offset110 = getelementptr inbounds %struct.TPL_MV_REF, %struct.TPL_MV_REF* %103, i32 0, i32 1
  %104 = load i8, i8* %ref_frame_offset110, align 4, !tbaa !39
  %conv111 = zext i8 %104 to i32
  call void @get_mv_projection(%struct.mv* %as_mv107, %struct.mv* byval(%struct.mv) align 2 %as_mv109, i32 %102, i32 %conv111)
  %as_mv112 = bitcast %union.int_mv* %comp_refmv to %struct.mv*
  %105 = load i32, i32* %allow_high_precision_mv, align 4, !tbaa !6
  %106 = load i32, i32* %force_integer_mv, align 4, !tbaa !6
  call void @lower_mv_precision(%struct.mv* %as_mv112, i32 %105, i32 %106)
  %107 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %cmp113 = icmp eq i32 %107, 0
  br i1 %cmp113, label %land.lhs.true115, label %if.end173

land.lhs.true115:                                 ; preds = %if.else
  %108 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %cmp116 = icmp eq i32 %108, 0
  br i1 %cmp116, label %if.then118, label %if.end173

if.then118:                                       ; preds = %land.lhs.true115
  %as_mv119 = bitcast %union.int_mv* %this_refmv to %struct.mv*
  %row120 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv119, i32 0, i32 0
  %109 = load i16, i16* %row120, align 4, !tbaa !41
  %conv121 = sext i16 %109 to i32
  %110 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds %union.int_mv, %union.int_mv* %110, i32 0
  %as_mv123 = bitcast %union.int_mv* %arrayidx122 to %struct.mv*
  %row124 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv123, i32 0, i32 0
  %111 = load i16, i16* %row124, align 4, !tbaa !41
  %conv125 = sext i16 %111 to i32
  %sub126 = sub nsw i32 %conv121, %conv125
  %call127 = call i32 @abs(i32 %sub126) #7
  %cmp128 = icmp sge i32 %call127, 16
  br i1 %cmp128, label %if.then166, label %lor.lhs.false130

lor.lhs.false130:                                 ; preds = %if.then118
  %as_mv131 = bitcast %union.int_mv* %this_refmv to %struct.mv*
  %col132 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv131, i32 0, i32 1
  %112 = load i16, i16* %col132, align 2, !tbaa !41
  %conv133 = sext i16 %112 to i32
  %113 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %arrayidx134 = getelementptr inbounds %union.int_mv, %union.int_mv* %113, i32 0
  %as_mv135 = bitcast %union.int_mv* %arrayidx134 to %struct.mv*
  %col136 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv135, i32 0, i32 1
  %114 = load i16, i16* %col136, align 2, !tbaa !41
  %conv137 = sext i16 %114 to i32
  %sub138 = sub nsw i32 %conv133, %conv137
  %call139 = call i32 @abs(i32 %sub138) #7
  %cmp140 = icmp sge i32 %call139, 16
  br i1 %cmp140, label %if.then166, label %lor.lhs.false142

lor.lhs.false142:                                 ; preds = %lor.lhs.false130
  %as_mv143 = bitcast %union.int_mv* %comp_refmv to %struct.mv*
  %row144 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv143, i32 0, i32 0
  %115 = load i16, i16* %row144, align 4, !tbaa !41
  %conv145 = sext i16 %115 to i32
  %116 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds %union.int_mv, %union.int_mv* %116, i32 1
  %as_mv147 = bitcast %union.int_mv* %arrayidx146 to %struct.mv*
  %row148 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv147, i32 0, i32 0
  %117 = load i16, i16* %row148, align 4, !tbaa !41
  %conv149 = sext i16 %117 to i32
  %sub150 = sub nsw i32 %conv145, %conv149
  %call151 = call i32 @abs(i32 %sub150) #7
  %cmp152 = icmp sge i32 %call151, 16
  br i1 %cmp152, label %if.then166, label %lor.lhs.false154

lor.lhs.false154:                                 ; preds = %lor.lhs.false142
  %as_mv155 = bitcast %union.int_mv* %comp_refmv to %struct.mv*
  %col156 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv155, i32 0, i32 1
  %118 = load i16, i16* %col156, align 2, !tbaa !41
  %conv157 = sext i16 %118 to i32
  %119 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %arrayidx158 = getelementptr inbounds %union.int_mv, %union.int_mv* %119, i32 1
  %as_mv159 = bitcast %union.int_mv* %arrayidx158 to %struct.mv*
  %col160 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv159, i32 0, i32 1
  %120 = load i16, i16* %col160, align 2, !tbaa !41
  %conv161 = sext i16 %120 to i32
  %sub162 = sub nsw i32 %conv157, %conv161
  %call163 = call i32 @abs(i32 %sub162) #7
  %cmp164 = icmp sge i32 %call163, 16
  br i1 %cmp164, label %if.then166, label %if.end172

if.then166:                                       ; preds = %lor.lhs.false154, %lor.lhs.false142, %lor.lhs.false130, %if.then118
  %121 = load i16*, i16** %mode_context.addr, align 4, !tbaa !2
  %122 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom167 = sext i8 %122 to i32
  %arrayidx168 = getelementptr inbounds i16, i16* %121, i32 %idxprom167
  %123 = load i16, i16* %arrayidx168, align 2, !tbaa !58
  %conv169 = sext i16 %123 to i32
  %or170 = or i32 %conv169, 8
  %conv171 = trunc i32 %or170 to i16
  store i16 %conv171, i16* %arrayidx168, align 2, !tbaa !58
  br label %if.end172

if.end172:                                        ; preds = %if.then166, %lor.lhs.false154
  br label %if.end173

if.end173:                                        ; preds = %if.end172, %land.lhs.true115, %if.else
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond174

for.cond174:                                      ; preds = %for.inc193, %if.end173
  %124 = load i32, i32* %idx, align 4, !tbaa !6
  %125 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %126 = load i8, i8* %125, align 1, !tbaa !41
  %conv175 = zext i8 %126 to i32
  %cmp176 = icmp slt i32 %124, %conv175
  br i1 %cmp176, label %for.body178, label %for.end195

for.body178:                                      ; preds = %for.cond174
  %as_int179 = bitcast %union.int_mv* %this_refmv to i32*
  %127 = load i32, i32* %as_int179, align 4, !tbaa !41
  %128 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %129 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx180 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %128, i32 %129
  %this_mv181 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx180, i32 0, i32 0
  %as_int182 = bitcast %union.int_mv* %this_mv181 to i32*
  %130 = load i32, i32* %as_int182, align 4, !tbaa !41
  %cmp183 = icmp eq i32 %127, %130
  br i1 %cmp183, label %land.lhs.true185, label %if.end192

land.lhs.true185:                                 ; preds = %for.body178
  %as_int186 = bitcast %union.int_mv* %comp_refmv to i32*
  %131 = load i32, i32* %as_int186, align 4, !tbaa !41
  %132 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %133 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx187 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %132, i32 %133
  %comp_mv = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx187, i32 0, i32 1
  %as_int188 = bitcast %union.int_mv* %comp_mv to i32*
  %134 = load i32, i32* %as_int188, align 4, !tbaa !41
  %cmp189 = icmp eq i32 %131, %134
  br i1 %cmp189, label %if.then191, label %if.end192

if.then191:                                       ; preds = %land.lhs.true185
  br label %for.end195

if.end192:                                        ; preds = %land.lhs.true185, %for.body178
  br label %for.inc193

for.inc193:                                       ; preds = %if.end192
  %135 = load i32, i32* %idx, align 4, !tbaa !6
  %inc194 = add nsw i32 %135, 1
  store i32 %inc194, i32* %idx, align 4, !tbaa !6
  br label %for.cond174

for.end195:                                       ; preds = %if.then191, %for.cond174
  %136 = load i32, i32* %idx, align 4, !tbaa !6
  %137 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %138 = load i8, i8* %137, align 1, !tbaa !41
  %conv196 = zext i8 %138 to i32
  %cmp197 = icmp slt i32 %136, %conv196
  br i1 %cmp197, label %if.then199, label %if.end204

if.then199:                                       ; preds = %for.end195
  %139 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %140 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx200 = getelementptr inbounds i16, i16* %139, i32 %140
  %141 = load i16, i16* %arrayidx200, align 2, !tbaa !58
  %conv201 = zext i16 %141 to i32
  %add202 = add nsw i32 %conv201, 2
  %conv203 = trunc i32 %add202 to i16
  store i16 %conv203, i16* %arrayidx200, align 2, !tbaa !58
  br label %if.end204

if.end204:                                        ; preds = %if.then199, %for.end195
  %142 = load i32, i32* %idx, align 4, !tbaa !6
  %143 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %144 = load i8, i8* %143, align 1, !tbaa !41
  %conv205 = zext i8 %144 to i32
  %cmp206 = icmp eq i32 %142, %conv205
  br i1 %cmp206, label %land.lhs.true208, label %if.end223

land.lhs.true208:                                 ; preds = %if.end204
  %145 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %146 = load i8, i8* %145, align 1, !tbaa !41
  %conv209 = zext i8 %146 to i32
  %cmp210 = icmp slt i32 %conv209, 8
  br i1 %cmp210, label %if.then212, label %if.end223

if.then212:                                       ; preds = %land.lhs.true208
  %as_int213 = bitcast %union.int_mv* %this_refmv to i32*
  %147 = load i32, i32* %as_int213, align 4, !tbaa !41
  %148 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %149 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx214 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %148, i32 %149
  %this_mv215 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx214, i32 0, i32 0
  %as_int216 = bitcast %union.int_mv* %this_mv215 to i32*
  store i32 %147, i32* %as_int216, align 4, !tbaa !41
  %as_int217 = bitcast %union.int_mv* %comp_refmv to i32*
  %150 = load i32, i32* %as_int217, align 4, !tbaa !41
  %151 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %152 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx218 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %151, i32 %152
  %comp_mv219 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx218, i32 0, i32 1
  %as_int220 = bitcast %union.int_mv* %comp_mv219 to i32*
  store i32 %150, i32* %as_int220, align 4, !tbaa !41
  %153 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %154 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx221 = getelementptr inbounds i16, i16* %153, i32 %154
  store i16 2, i16* %arrayidx221, align 2, !tbaa !58
  %155 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %156 = load i8, i8* %155, align 1, !tbaa !41
  %inc222 = add i8 %156, 1
  store i8 %inc222, i8* %155, align 1, !tbaa !41
  br label %if.end223

if.end223:                                        ; preds = %if.then212, %land.lhs.true208, %if.end204
  %157 = bitcast %union.int_mv* %comp_refmv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #6
  %158 = bitcast i32* %cur_offset_1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #6
  %159 = bitcast i32* %frame1_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #6
  %160 = bitcast %struct.RefCntBuffer** %buf_1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #6
  br label %if.end224

if.end224:                                        ; preds = %if.end223, %if.end100
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %161 = bitcast %union.int_mv* %this_refmv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #6
  %162 = bitcast i32* %force_integer_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #6
  %163 = bitcast i32* %allow_high_precision_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #6
  %164 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #6
  %165 = bitcast i32* %cur_offset_0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #6
  %166 = bitcast i32* %frame0_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #6
  %167 = bitcast %struct.RefCntBuffer** %buf_0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #6
  %168 = bitcast i32* %cur_frame_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #6
  %169 = bitcast i16* %weight_unit to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %169) #6
  %170 = bitcast [2 x i8]* %rf to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %170) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end224, %if.then16
  %171 = bitcast %struct.TPL_MV_REF** %prev_frame_mvs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #6
  br label %cleanup225

cleanup225:                                       ; preds = %cleanup, %if.then
  %172 = bitcast %struct.position* %mi_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %172) #6
  %173 = load i32, i32* %retval, align 4
  ret i32 %173
}

; Function Attrs: nounwind
define internal i32 @check_sb_border(i32 %mi_row, i32 %mi_col, i32 %row_offset, i32 %col_offset) #0 {
entry:
  %retval = alloca i32, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %row_offset.addr = alloca i32, align 4
  %col_offset.addr = alloca i32, align 4
  %sb_mi_size = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i32 %row_offset, i32* %row_offset.addr, align 4, !tbaa !6
  store i32 %col_offset, i32* %col_offset.addr, align 4, !tbaa !6
  %0 = bitcast i32* %sb_mi_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !41
  %conv = zext i8 %1 to i32
  store i32 %conv, i32* %sb_mi_size, align 4, !tbaa !6
  %2 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %4 = load i32, i32* %sb_mi_size, align 4, !tbaa !6
  %sub = sub nsw i32 %4, 1
  %and = and i32 %3, %sub
  store i32 %and, i32* %row, align 4, !tbaa !6
  %5 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %7 = load i32, i32* %sb_mi_size, align 4, !tbaa !6
  %sub1 = sub nsw i32 %7, 1
  %and2 = and i32 %6, %sub1
  store i32 %and2, i32* %col, align 4, !tbaa !6
  %8 = load i32, i32* %row, align 4, !tbaa !6
  %9 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %add = add nsw i32 %8, %9
  %cmp = icmp slt i32 %add, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %10 = load i32, i32* %row, align 4, !tbaa !6
  %11 = load i32, i32* %row_offset.addr, align 4, !tbaa !6
  %add4 = add nsw i32 %10, %11
  %12 = load i32, i32* %sb_mi_size, align 4, !tbaa !6
  %cmp5 = icmp sge i32 %add4, %12
  br i1 %cmp5, label %if.then, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %lor.lhs.false
  %13 = load i32, i32* %col, align 4, !tbaa !6
  %14 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %add8 = add nsw i32 %13, %14
  %cmp9 = icmp slt i32 %add8, 0
  br i1 %cmp9, label %if.then, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %lor.lhs.false7
  %15 = load i32, i32* %col, align 4, !tbaa !6
  %16 = load i32, i32* %col_offset.addr, align 4, !tbaa !6
  %add12 = add nsw i32 %15, %16
  %17 = load i32, i32* %sb_mi_size, align 4, !tbaa !6
  %cmp13 = icmp sge i32 %add12, %17
  br i1 %cmp13, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false11, %lor.lhs.false7, %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false11
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %18 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = bitcast i32* %sb_mi_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: inlinehint nounwind
define internal void @process_compound_ref_mv_candidate(%struct.MB_MODE_INFO* %candidate, %struct.AV1Common* %cm, i8* %rf, [2 x %union.int_mv]* %ref_id, i32* %ref_id_count, [2 x %union.int_mv]* %ref_diff, i32* %ref_diff_count) #3 {
entry:
  %candidate.addr = alloca %struct.MB_MODE_INFO*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %rf.addr = alloca i8*, align 4
  %ref_id.addr = alloca [2 x %union.int_mv]*, align 4
  %ref_id_count.addr = alloca i32*, align 4
  %ref_diff.addr = alloca [2 x %union.int_mv]*, align 4
  %ref_diff_count.addr = alloca i32*, align 4
  %rf_idx = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %can_rf = alloca i8, align 1
  %cmp_idx = alloca i32, align 4
  %this_mv = alloca %union.int_mv, align 4
  store %struct.MB_MODE_INFO* %candidate, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !2
  store [2 x %union.int_mv]* %ref_id, [2 x %union.int_mv]** %ref_id.addr, align 4, !tbaa !2
  store i32* %ref_id_count, i32** %ref_id_count.addr, align 4, !tbaa !2
  store [2 x %union.int_mv]* %ref_diff, [2 x %union.int_mv]** %ref_diff.addr, align 4, !tbaa !2
  store i32* %ref_diff_count, i32** %ref_diff_count.addr, align 4, !tbaa !2
  %0 = bitcast i32* %rf_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %rf_idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc53, %entry
  %1 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %2 = bitcast i32* %rf_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  br label %for.end55

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %can_rf) #6
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %3, i32 0, i32 12
  %4 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 %4
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !41
  store i8 %5, i8* %can_rf, align 1, !tbaa !41
  %6 = bitcast i32* %cmp_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 0, i32* %cmp_idx, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %7 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %7, 2
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %8 = bitcast i32* %cmp_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %9 = load i8, i8* %can_rf, align 1, !tbaa !41
  %conv = sext i8 %9 to i32
  %10 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %11 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i8, i8* %10, i32 %11
  %12 = load i8, i8* %arrayidx5, align 1, !tbaa !41
  %conv6 = sext i8 %12 to i32
  %cmp7 = icmp eq i32 %conv, %conv6
  br i1 %cmp7, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.body4
  %13 = load i32*, i32** %ref_id_count.addr, align 4, !tbaa !2
  %14 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds i32, i32* %13, i32 %14
  %15 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %15, 2
  br i1 %cmp10, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %16 = load [2 x %union.int_mv]*, [2 x %union.int_mv]** %ref_id.addr, align 4, !tbaa !2
  %17 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %16, i32 %17
  %18 = load i32*, i32** %ref_id_count.addr, align 4, !tbaa !2
  %19 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = load i32, i32* %arrayidx13, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx12, i32 0, i32 %20
  %21 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %mv = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %21, i32 0, i32 2
  %22 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv, i32 0, i32 %22
  %23 = bitcast %union.int_mv* %arrayidx14 to i8*
  %24 = bitcast %union.int_mv* %arrayidx15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 4, i1 false), !tbaa.struct !57
  %25 = load i32*, i32** %ref_id_count.addr, align 4, !tbaa !2
  %26 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds i32, i32* %25, i32 %26
  %27 = load i32, i32* %arrayidx16, align 4, !tbaa !6
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %arrayidx16, align 4, !tbaa !6
  br label %if.end51

if.else:                                          ; preds = %land.lhs.true, %for.body4
  %28 = load i8, i8* %can_rf, align 1, !tbaa !41
  %conv17 = sext i8 %28 to i32
  %cmp18 = icmp sgt i32 %conv17, 0
  br i1 %cmp18, label %land.lhs.true20, label %if.end50

land.lhs.true20:                                  ; preds = %if.else
  %29 = load i32*, i32** %ref_diff_count.addr, align 4, !tbaa !2
  %30 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds i32, i32* %29, i32 %30
  %31 = load i32, i32* %arrayidx21, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %31, 2
  br i1 %cmp22, label %if.then24, label %if.end50

if.then24:                                        ; preds = %land.lhs.true20
  %32 = bitcast %union.int_mv* %this_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  %33 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %mv25 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %33, i32 0, i32 2
  %34 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv25, i32 0, i32 %34
  %35 = bitcast %union.int_mv* %this_mv to i8*
  %36 = bitcast %union.int_mv* %arrayidx26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 4, i1 false), !tbaa.struct !57
  %37 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_sign_bias = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %37, i32 0, i32 47
  %38 = load i8, i8* %can_rf, align 1, !tbaa !41
  %idxprom = sext i8 %38 to i32
  %arrayidx27 = getelementptr inbounds [8 x i32], [8 x i32]* %ref_frame_sign_bias, i32 0, i32 %idxprom
  %39 = load i32, i32* %arrayidx27, align 4, !tbaa !6
  %40 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_sign_bias28 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %40, i32 0, i32 47
  %41 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %42 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds i8, i8* %41, i32 %42
  %43 = load i8, i8* %arrayidx29, align 1, !tbaa !41
  %idxprom30 = sext i8 %43 to i32
  %arrayidx31 = getelementptr inbounds [8 x i32], [8 x i32]* %ref_frame_sign_bias28, i32 0, i32 %idxprom30
  %44 = load i32, i32* %arrayidx31, align 4, !tbaa !6
  %cmp32 = icmp ne i32 %39, %44
  br i1 %cmp32, label %if.then34, label %if.end

if.then34:                                        ; preds = %if.then24
  %as_mv = bitcast %union.int_mv* %this_mv to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv, i32 0, i32 0
  %45 = load i16, i16* %row, align 4, !tbaa !41
  %conv35 = sext i16 %45 to i32
  %sub = sub nsw i32 0, %conv35
  %conv36 = trunc i32 %sub to i16
  %as_mv37 = bitcast %union.int_mv* %this_mv to %struct.mv*
  %row38 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv37, i32 0, i32 0
  store i16 %conv36, i16* %row38, align 4, !tbaa !41
  %as_mv39 = bitcast %union.int_mv* %this_mv to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv39, i32 0, i32 1
  %46 = load i16, i16* %col, align 2, !tbaa !41
  %conv40 = sext i16 %46 to i32
  %sub41 = sub nsw i32 0, %conv40
  %conv42 = trunc i32 %sub41 to i16
  %as_mv43 = bitcast %union.int_mv* %this_mv to %struct.mv*
  %col44 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv43, i32 0, i32 1
  store i16 %conv42, i16* %col44, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then34, %if.then24
  %47 = load [2 x %union.int_mv]*, [2 x %union.int_mv]** %ref_diff.addr, align 4, !tbaa !2
  %48 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %47, i32 %48
  %49 = load i32*, i32** %ref_diff_count.addr, align 4, !tbaa !2
  %50 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds i32, i32* %49, i32 %50
  %51 = load i32, i32* %arrayidx46, align 4, !tbaa !6
  %arrayidx47 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx45, i32 0, i32 %51
  %52 = bitcast %union.int_mv* %arrayidx47 to i8*
  %53 = bitcast %union.int_mv* %this_mv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 4, i1 false), !tbaa.struct !57
  %54 = load i32*, i32** %ref_diff_count.addr, align 4, !tbaa !2
  %55 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds i32, i32* %54, i32 %55
  %56 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %inc49 = add nsw i32 %56, 1
  store i32 %inc49, i32* %arrayidx48, align 4, !tbaa !6
  %57 = bitcast %union.int_mv* %this_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  br label %if.end50

if.end50:                                         ; preds = %if.end, %land.lhs.true20, %if.else
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end51
  %58 = load i32, i32* %cmp_idx, align 4, !tbaa !6
  %inc52 = add nsw i32 %58, 1
  store i32 %inc52, i32* %cmp_idx, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %can_rf) #6
  br label %for.inc53

for.inc53:                                        ; preds = %for.end
  %59 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %inc54 = add nsw i32 %59, 1
  store i32 %inc54, i32* %rf_idx, align 4, !tbaa !6
  br label %for.cond

for.end55:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @clamp_mv_ref(%struct.mv* %mv, i32 %bw, i32 %bh, %struct.macroblockd* %xd) #3 {
entry:
  %mv.addr = alloca %struct.mv*, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mv_limits = alloca %struct.SubpelMvLimits, align 4
  store %struct.mv* %mv, %struct.mv** %mv.addr, align 4, !tbaa !2
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !6
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast %struct.SubpelMvLimits* %mv_limits to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %col_min = getelementptr inbounds %struct.SubpelMvLimits, %struct.SubpelMvLimits* %mv_limits, i32 0, i32 0
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_left_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 17
  %2 = load i32, i32* %mb_to_left_edge, align 4, !tbaa !104
  %3 = load i32, i32* %bw.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %3, 8
  %sub = sub nsw i32 %2, %mul
  %sub1 = sub nsw i32 %sub, 128
  store i32 %sub1, i32* %col_min, align 4, !tbaa !77
  %col_max = getelementptr inbounds %struct.SubpelMvLimits, %struct.SubpelMvLimits* %mv_limits, i32 0, i32 1
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 18
  %5 = load i32, i32* %mb_to_right_edge, align 8, !tbaa !105
  %6 = load i32, i32* %bw.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 %6, 8
  %add = add nsw i32 %5, %mul2
  %add3 = add nsw i32 %add, 128
  store i32 %add3, i32* %col_max, align 4, !tbaa !78
  %row_min = getelementptr inbounds %struct.SubpelMvLimits, %struct.SubpelMvLimits* %mv_limits, i32 0, i32 2
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_top_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 19
  %8 = load i32, i32* %mb_to_top_edge, align 4, !tbaa !106
  %9 = load i32, i32* %bh.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 %9, 8
  %sub5 = sub nsw i32 %8, %mul4
  %sub6 = sub nsw i32 %sub5, 128
  store i32 %sub6, i32* %row_min, align 4, !tbaa !95
  %row_max = getelementptr inbounds %struct.SubpelMvLimits, %struct.SubpelMvLimits* %mv_limits, i32 0, i32 3
  %10 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %10, i32 0, i32 20
  %11 = load i32, i32* %mb_to_bottom_edge, align 16, !tbaa !107
  %12 = load i32, i32* %bh.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 %12, 8
  %add8 = add nsw i32 %11, %mul7
  %add9 = add nsw i32 %add8, 128
  store i32 %add9, i32* %row_max, align 4, !tbaa !96
  %13 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  call void @clamp_mv(%struct.mv* %13, %struct.SubpelMvLimits* %mv_limits)
  %14 = bitcast %struct.SubpelMvLimits* %mv_limits to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @process_single_ref_mv_candidate(%struct.MB_MODE_INFO* %candidate, %struct.AV1Common* %cm, i8 signext %ref_frame, i8* %refmv_count, %struct.candidate_mv* %ref_mv_stack, i16* %ref_mv_weight) #3 {
entry:
  %candidate.addr = alloca %struct.MB_MODE_INFO*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  %refmv_count.addr = alloca i8*, align 4
  %ref_mv_stack.addr = alloca %struct.candidate_mv*, align 4
  %ref_mv_weight.addr = alloca i16*, align 4
  %rf_idx = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %this_mv = alloca %union.int_mv, align 4
  %stack_idx = alloca i32, align 4
  %stack_mv = alloca %union.int_mv, align 4
  store %struct.MB_MODE_INFO* %candidate, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !41
  store i8* %refmv_count, i8** %refmv_count.addr, align 4, !tbaa !2
  store %struct.candidate_mv* %ref_mv_stack, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  store i16* %ref_mv_weight, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %0 = bitcast i32* %rf_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %rf_idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc46, %entry
  %1 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %2 = bitcast i32* %rf_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  br label %for.end49

for.body:                                         ; preds = %for.cond
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %ref_frame1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %3, i32 0, i32 12
  %4 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame1, i32 0, i32 %4
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %conv = sext i8 %5 to i32
  %cmp2 = icmp sgt i32 %conv, 0
  br i1 %cmp2, label %if.then, label %if.end45

if.then:                                          ; preds = %for.body
  %6 = bitcast %union.int_mv* %this_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %mv = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 2
  %8 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv, i32 0, i32 %8
  %9 = bitcast %union.int_mv* %this_mv to i8*
  %10 = bitcast %union.int_mv* %arrayidx4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 4, i1 false), !tbaa.struct !57
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_sign_bias = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 47
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %ref_frame5 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %12, i32 0, i32 12
  %13 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame5, i32 0, i32 %13
  %14 = load i8, i8* %arrayidx6, align 1, !tbaa !41
  %idxprom = sext i8 %14 to i32
  %arrayidx7 = getelementptr inbounds [8 x i32], [8 x i32]* %ref_frame_sign_bias, i32 0, i32 %idxprom
  %15 = load i32, i32* %arrayidx7, align 4, !tbaa !6
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_sign_bias8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %16, i32 0, i32 47
  %17 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %idxprom9 = sext i8 %17 to i32
  %arrayidx10 = getelementptr inbounds [8 x i32], [8 x i32]* %ref_frame_sign_bias8, i32 0, i32 %idxprom9
  %18 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %cmp11 = icmp ne i32 %15, %18
  br i1 %cmp11, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.then
  %as_mv = bitcast %union.int_mv* %this_mv to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv, i32 0, i32 0
  %19 = load i16, i16* %row, align 4, !tbaa !41
  %conv14 = sext i16 %19 to i32
  %sub = sub nsw i32 0, %conv14
  %conv15 = trunc i32 %sub to i16
  %as_mv16 = bitcast %union.int_mv* %this_mv to %struct.mv*
  %row17 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv16, i32 0, i32 0
  store i16 %conv15, i16* %row17, align 4, !tbaa !41
  %as_mv18 = bitcast %union.int_mv* %this_mv to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv18, i32 0, i32 1
  %20 = load i16, i16* %col, align 2, !tbaa !41
  %conv19 = sext i16 %20 to i32
  %sub20 = sub nsw i32 0, %conv19
  %conv21 = trunc i32 %sub20 to i16
  %as_mv22 = bitcast %union.int_mv* %this_mv to %struct.mv*
  %col23 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv22, i32 0, i32 1
  store i16 %conv21, i16* %col23, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then13, %if.then
  %21 = bitcast i32* %stack_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  store i32 0, i32* %stack_idx, align 4, !tbaa !6
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc, %if.end
  %22 = load i32, i32* %stack_idx, align 4, !tbaa !6
  %23 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %24 = load i8, i8* %23, align 1, !tbaa !41
  %conv25 = zext i8 %24 to i32
  %cmp26 = icmp slt i32 %22, %conv25
  br i1 %cmp26, label %for.body28, label %for.end

for.body28:                                       ; preds = %for.cond24
  %25 = bitcast %union.int_mv* %stack_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %27 = load i32, i32* %stack_idx, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %26, i32 %27
  %this_mv30 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx29, i32 0, i32 0
  %28 = bitcast %union.int_mv* %stack_mv to i8*
  %29 = bitcast %union.int_mv* %this_mv30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 4, i1 false), !tbaa.struct !57
  %as_int = bitcast %union.int_mv* %this_mv to i32*
  %30 = load i32, i32* %as_int, align 4, !tbaa !41
  %as_int31 = bitcast %union.int_mv* %stack_mv to i32*
  %31 = load i32, i32* %as_int31, align 4, !tbaa !41
  %cmp32 = icmp eq i32 %30, %31
  br i1 %cmp32, label %if.then34, label %if.end35

if.then34:                                        ; preds = %for.body28
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end35:                                         ; preds = %for.body28
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end35, %if.then34
  %32 = bitcast %union.int_mv* %stack_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 5, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %33 = load i32, i32* %stack_idx, align 4, !tbaa !6
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %stack_idx, align 4, !tbaa !6
  br label %for.cond24

for.end:                                          ; preds = %cleanup, %for.cond24
  %34 = load i32, i32* %stack_idx, align 4, !tbaa !6
  %35 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %36 = load i8, i8* %35, align 1, !tbaa !41
  %conv36 = zext i8 %36 to i32
  %cmp37 = icmp eq i32 %34, %conv36
  br i1 %cmp37, label %if.then39, label %if.end44

if.then39:                                        ; preds = %for.end
  %37 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %38 = load i32, i32* %stack_idx, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %37, i32 %38
  %this_mv41 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx40, i32 0, i32 0
  %39 = bitcast %union.int_mv* %this_mv41 to i8*
  %40 = bitcast %union.int_mv* %this_mv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 4, i1 false), !tbaa.struct !57
  %41 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %42 = load i32, i32* %stack_idx, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds i16, i16* %41, i32 %42
  store i16 2, i16* %arrayidx42, align 2, !tbaa !58
  %43 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %44 = load i8, i8* %43, align 1, !tbaa !41
  %inc43 = add i8 %44, 1
  store i8 %inc43, i8* %43, align 1, !tbaa !41
  br label %if.end44

if.end44:                                         ; preds = %if.then39, %for.end
  %45 = bitcast i32* %stack_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  %46 = bitcast %union.int_mv* %this_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %for.body
  br label %for.inc46

for.inc46:                                        ; preds = %if.end45
  %47 = load i32, i32* %rf_idx, align 4, !tbaa !6
  %inc47 = add nsw i32 %47, 1
  store i32 %inc47, i32* %rf_idx, align 4, !tbaa !6
  br label %for.cond

for.end49:                                        ; preds = %for.cond.cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal void @add_ref_mv_candidate(%struct.MB_MODE_INFO* %candidate, i8* %rf, i8* %refmv_count, i8* %ref_match_count, i8* %newmv_count, %struct.candidate_mv* %ref_mv_stack, i16* %ref_mv_weight, %union.int_mv* %gm_mv_candidates, %struct.WarpedMotionParams* %gm_params, i16 zeroext %weight) #3 {
entry:
  %candidate.addr = alloca %struct.MB_MODE_INFO*, align 4
  %rf.addr = alloca i8*, align 4
  %refmv_count.addr = alloca i8*, align 4
  %ref_match_count.addr = alloca i8*, align 4
  %newmv_count.addr = alloca i8*, align 4
  %ref_mv_stack.addr = alloca %struct.candidate_mv*, align 4
  %ref_mv_weight.addr = alloca i16*, align 4
  %gm_mv_candidates.addr = alloca %union.int_mv*, align 4
  %gm_params.addr = alloca %struct.WarpedMotionParams*, align 4
  %weight.addr = alloca i16, align 2
  %index = alloca i32, align 4
  %ref = alloca i32, align 4
  %is_gm_block = alloca i32, align 4
  %this_refmv = alloca %union.int_mv, align 4
  %this_refmv70 = alloca [2 x %union.int_mv], align 4
  %tmp = alloca %union.int_mv, align 4
  store %struct.MB_MODE_INFO* %candidate, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !2
  store i8* %refmv_count, i8** %refmv_count.addr, align 4, !tbaa !2
  store i8* %ref_match_count, i8** %ref_match_count.addr, align 4, !tbaa !2
  store i8* %newmv_count, i8** %newmv_count.addr, align 4, !tbaa !2
  store %struct.candidate_mv* %ref_mv_stack, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  store i16* %ref_mv_weight, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  store %union.int_mv* %gm_mv_candidates, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  store %struct.WarpedMotionParams* %gm_params, %struct.WarpedMotionParams** %gm_params.addr, align 4, !tbaa !2
  store i16 %weight, i16* %weight.addr, align 2, !tbaa !58
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %conv = sext i8 %4 to i32
  %cmp = icmp eq i32 %conv, -1
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %ref, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc51, %if.then2
  %5 = load i32, i32* %ref, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %5, 2
  br i1 %cmp3, label %for.body, label %for.end53

for.body:                                         ; preds = %for.cond
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %6, i32 0, i32 12
  %7 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 %7
  %8 = load i8, i8* %arrayidx5, align 1, !tbaa !41
  %conv6 = sext i8 %8 to i32
  %9 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %9, i32 0
  %10 = load i8, i8* %arrayidx7, align 1, !tbaa !41
  %conv8 = sext i8 %10 to i32
  %cmp9 = icmp eq i32 %conv6, %conv8
  br i1 %cmp9, label %if.then11, label %if.end50

if.then11:                                        ; preds = %for.body
  %11 = bitcast i32* %is_gm_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %13 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm_params.addr, align 4, !tbaa !2
  %14 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %14, i32 0
  %15 = load i8, i8* %arrayidx12, align 1, !tbaa !41
  %idxprom = sext i8 %15 to i32
  %arrayidx13 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %13, i32 %idxprom
  %wmtype = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %arrayidx13, i32 0, i32 5
  %16 = load i8, i8* %wmtype, align 4, !tbaa !59
  %call14 = call i32 @is_global_mv_block(%struct.MB_MODE_INFO* %12, i8 zeroext %16)
  store i32 %call14, i32* %is_gm_block, align 4, !tbaa !6
  %17 = bitcast %union.int_mv* %this_refmv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load i32, i32* %is_gm_block, align 4, !tbaa !6
  %tobool15 = icmp ne i32 %18, 0
  br i1 %tobool15, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then11
  %19 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds %union.int_mv, %union.int_mv* %19, i32 0
  %20 = bitcast %union.int_mv* %this_refmv to i8*
  %21 = bitcast %union.int_mv* %arrayidx16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 4, i1 false), !tbaa.struct !57
  br label %cond.end

cond.false:                                       ; preds = %if.then11
  %22 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %23 = load i32, i32* %ref, align 4, !tbaa !6
  call void @get_block_mv(%union.int_mv* sret align 4 %this_refmv, %struct.MB_MODE_INFO* %22, i32 %23)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  store i32 0, i32* %index, align 4, !tbaa !6
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc, %cond.end
  %24 = load i32, i32* %index, align 4, !tbaa !6
  %25 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %26 = load i8, i8* %25, align 1, !tbaa !41
  %conv18 = zext i8 %26 to i32
  %cmp19 = icmp slt i32 %24, %conv18
  br i1 %cmp19, label %for.body21, label %for.end

for.body21:                                       ; preds = %for.cond17
  %27 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %28 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %27, i32 %28
  %this_mv = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx22, i32 0, i32 0
  %as_int = bitcast %union.int_mv* %this_mv to i32*
  %29 = load i32, i32* %as_int, align 4, !tbaa !41
  %as_int23 = bitcast %union.int_mv* %this_refmv to i32*
  %30 = load i32, i32* %as_int23, align 4, !tbaa !41
  %cmp24 = icmp eq i32 %29, %30
  br i1 %cmp24, label %if.then26, label %if.end31

if.then26:                                        ; preds = %for.body21
  %31 = load i16, i16* %weight.addr, align 2, !tbaa !58
  %conv27 = zext i16 %31 to i32
  %32 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %33 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds i16, i16* %32, i32 %33
  %34 = load i16, i16* %arrayidx28, align 2, !tbaa !58
  %conv29 = zext i16 %34 to i32
  %add = add nsw i32 %conv29, %conv27
  %conv30 = trunc i32 %add to i16
  store i16 %conv30, i16* %arrayidx28, align 2, !tbaa !58
  br label %for.end

if.end31:                                         ; preds = %for.body21
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %35 = load i32, i32* %index, align 4, !tbaa !6
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %index, align 4, !tbaa !6
  br label %for.cond17

for.end:                                          ; preds = %if.then26, %for.cond17
  %36 = load i32, i32* %index, align 4, !tbaa !6
  %37 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %38 = load i8, i8* %37, align 1, !tbaa !41
  %conv32 = zext i8 %38 to i32
  %cmp33 = icmp eq i32 %36, %conv32
  br i1 %cmp33, label %land.lhs.true, label %if.end43

land.lhs.true:                                    ; preds = %for.end
  %39 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %40 = load i8, i8* %39, align 1, !tbaa !41
  %conv35 = zext i8 %40 to i32
  %cmp36 = icmp slt i32 %conv35, 8
  br i1 %cmp36, label %if.then38, label %if.end43

if.then38:                                        ; preds = %land.lhs.true
  %41 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %42 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %41, i32 %42
  %this_mv40 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx39, i32 0, i32 0
  %43 = bitcast %union.int_mv* %this_mv40 to i8*
  %44 = bitcast %union.int_mv* %this_refmv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 4, i1 false), !tbaa.struct !57
  %45 = load i16, i16* %weight.addr, align 2, !tbaa !58
  %46 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %47 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds i16, i16* %46, i32 %47
  store i16 %45, i16* %arrayidx41, align 2, !tbaa !58
  %48 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %49 = load i8, i8* %48, align 1, !tbaa !41
  %inc42 = add i8 %49, 1
  store i8 %inc42, i8* %48, align 1, !tbaa !41
  br label %if.end43

if.end43:                                         ; preds = %if.then38, %land.lhs.true, %for.end
  %50 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %50, i32 0, i32 7
  %51 = load i8, i8* %mode, align 1, !tbaa !108
  %call44 = call i32 @have_newmv_in_inter_mode(i8 zeroext %51)
  %tobool45 = icmp ne i32 %call44, 0
  br i1 %tobool45, label %if.then46, label %if.end48

if.then46:                                        ; preds = %if.end43
  %52 = load i8*, i8** %newmv_count.addr, align 4, !tbaa !2
  %53 = load i8, i8* %52, align 1, !tbaa !41
  %inc47 = add i8 %53, 1
  store i8 %inc47, i8* %52, align 1, !tbaa !41
  br label %if.end48

if.end48:                                         ; preds = %if.then46, %if.end43
  %54 = load i8*, i8** %ref_match_count.addr, align 4, !tbaa !2
  %55 = load i8, i8* %54, align 1, !tbaa !41
  %inc49 = add i8 %55, 1
  store i8 %inc49, i8* %54, align 1, !tbaa !41
  %56 = bitcast %union.int_mv* %this_refmv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  %57 = bitcast i32* %is_gm_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  br label %if.end50

if.end50:                                         ; preds = %if.end48, %for.body
  br label %for.inc51

for.inc51:                                        ; preds = %if.end50
  %58 = load i32, i32* %ref, align 4, !tbaa !6
  %inc52 = add nsw i32 %58, 1
  store i32 %inc52, i32* %ref, align 4, !tbaa !6
  br label %for.cond

for.end53:                                        ; preds = %for.cond
  br label %if.end144

if.else:                                          ; preds = %if.end
  %59 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %ref_frame54 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %59, i32 0, i32 12
  %arrayidx55 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame54, i32 0, i32 0
  %60 = load i8, i8* %arrayidx55, align 4, !tbaa !41
  %conv56 = sext i8 %60 to i32
  %61 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i8, i8* %61, i32 0
  %62 = load i8, i8* %arrayidx57, align 1, !tbaa !41
  %conv58 = sext i8 %62 to i32
  %cmp59 = icmp eq i32 %conv56, %conv58
  br i1 %cmp59, label %land.lhs.true61, label %if.end143

land.lhs.true61:                                  ; preds = %if.else
  %63 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %ref_frame62 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %63, i32 0, i32 12
  %arrayidx63 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame62, i32 0, i32 1
  %64 = load i8, i8* %arrayidx63, align 1, !tbaa !41
  %conv64 = sext i8 %64 to i32
  %65 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %65, i32 1
  %66 = load i8, i8* %arrayidx65, align 1, !tbaa !41
  %conv66 = sext i8 %66 to i32
  %cmp67 = icmp eq i32 %conv64, %conv66
  br i1 %cmp67, label %if.then69, label %if.end143

if.then69:                                        ; preds = %land.lhs.true61
  %67 = bitcast [2 x %union.int_mv]* %this_refmv70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %67) #6
  store i32 0, i32* %ref, align 4, !tbaa !6
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc87, %if.then69
  %68 = load i32, i32* %ref, align 4, !tbaa !6
  %cmp72 = icmp slt i32 %68, 2
  br i1 %cmp72, label %for.body74, label %for.end89

for.body74:                                       ; preds = %for.cond71
  %69 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %70 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm_params.addr, align 4, !tbaa !2
  %71 = load i8*, i8** %rf.addr, align 4, !tbaa !2
  %72 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds i8, i8* %71, i32 %72
  %73 = load i8, i8* %arrayidx75, align 1, !tbaa !41
  %idxprom76 = sext i8 %73 to i32
  %arrayidx77 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %70, i32 %idxprom76
  %wmtype78 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %arrayidx77, i32 0, i32 5
  %74 = load i8, i8* %wmtype78, align 4, !tbaa !59
  %call79 = call i32 @is_global_mv_block(%struct.MB_MODE_INFO* %69, i8 zeroext %74)
  %tobool80 = icmp ne i32 %call79, 0
  br i1 %tobool80, label %if.then81, label %if.else84

if.then81:                                        ; preds = %for.body74
  %75 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx82 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %this_refmv70, i32 0, i32 %75
  %76 = load %union.int_mv*, %union.int_mv** %gm_mv_candidates.addr, align 4, !tbaa !2
  %77 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx83 = getelementptr inbounds %union.int_mv, %union.int_mv* %76, i32 %77
  %78 = bitcast %union.int_mv* %arrayidx82 to i8*
  %79 = bitcast %union.int_mv* %arrayidx83 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %78, i8* align 4 %79, i32 4, i1 false), !tbaa.struct !57
  br label %if.end86

if.else84:                                        ; preds = %for.body74
  %80 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx85 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %this_refmv70, i32 0, i32 %80
  %81 = bitcast %union.int_mv* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #6
  %82 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %83 = load i32, i32* %ref, align 4, !tbaa !6
  call void @get_block_mv(%union.int_mv* sret align 4 %tmp, %struct.MB_MODE_INFO* %82, i32 %83)
  %84 = bitcast %union.int_mv* %arrayidx85 to i8*
  %85 = bitcast %union.int_mv* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %84, i8* align 4 %85, i32 4, i1 false), !tbaa.struct !57
  %86 = bitcast %union.int_mv* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #6
  br label %if.end86

if.end86:                                         ; preds = %if.else84, %if.then81
  br label %for.inc87

for.inc87:                                        ; preds = %if.end86
  %87 = load i32, i32* %ref, align 4, !tbaa !6
  %inc88 = add nsw i32 %87, 1
  store i32 %inc88, i32* %ref, align 4, !tbaa !6
  br label %for.cond71

for.end89:                                        ; preds = %for.cond71
  store i32 0, i32* %index, align 4, !tbaa !6
  br label %for.cond90

for.cond90:                                       ; preds = %for.inc116, %for.end89
  %88 = load i32, i32* %index, align 4, !tbaa !6
  %89 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %90 = load i8, i8* %89, align 1, !tbaa !41
  %conv91 = zext i8 %90 to i32
  %cmp92 = icmp slt i32 %88, %conv91
  br i1 %cmp92, label %for.body94, label %for.end118

for.body94:                                       ; preds = %for.cond90
  %91 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %92 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx95 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %91, i32 %92
  %this_mv96 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx95, i32 0, i32 0
  %as_int97 = bitcast %union.int_mv* %this_mv96 to i32*
  %93 = load i32, i32* %as_int97, align 4, !tbaa !41
  %arrayidx98 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %this_refmv70, i32 0, i32 0
  %as_int99 = bitcast %union.int_mv* %arrayidx98 to i32*
  %94 = load i32, i32* %as_int99, align 4, !tbaa !41
  %cmp100 = icmp eq i32 %93, %94
  br i1 %cmp100, label %land.lhs.true102, label %if.end115

land.lhs.true102:                                 ; preds = %for.body94
  %95 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %96 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx103 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %95, i32 %96
  %comp_mv = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx103, i32 0, i32 1
  %as_int104 = bitcast %union.int_mv* %comp_mv to i32*
  %97 = load i32, i32* %as_int104, align 4, !tbaa !41
  %arrayidx105 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %this_refmv70, i32 0, i32 1
  %as_int106 = bitcast %union.int_mv* %arrayidx105 to i32*
  %98 = load i32, i32* %as_int106, align 4, !tbaa !41
  %cmp107 = icmp eq i32 %97, %98
  br i1 %cmp107, label %if.then109, label %if.end115

if.then109:                                       ; preds = %land.lhs.true102
  %99 = load i16, i16* %weight.addr, align 2, !tbaa !58
  %conv110 = zext i16 %99 to i32
  %100 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %101 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx111 = getelementptr inbounds i16, i16* %100, i32 %101
  %102 = load i16, i16* %arrayidx111, align 2, !tbaa !58
  %conv112 = zext i16 %102 to i32
  %add113 = add nsw i32 %conv112, %conv110
  %conv114 = trunc i32 %add113 to i16
  store i16 %conv114, i16* %arrayidx111, align 2, !tbaa !58
  br label %for.end118

if.end115:                                        ; preds = %land.lhs.true102, %for.body94
  br label %for.inc116

for.inc116:                                       ; preds = %if.end115
  %103 = load i32, i32* %index, align 4, !tbaa !6
  %inc117 = add nsw i32 %103, 1
  store i32 %inc117, i32* %index, align 4, !tbaa !6
  br label %for.cond90

for.end118:                                       ; preds = %if.then109, %for.cond90
  %104 = load i32, i32* %index, align 4, !tbaa !6
  %105 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %106 = load i8, i8* %105, align 1, !tbaa !41
  %conv119 = zext i8 %106 to i32
  %cmp120 = icmp eq i32 %104, %conv119
  br i1 %cmp120, label %land.lhs.true122, label %if.end135

land.lhs.true122:                                 ; preds = %for.end118
  %107 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %108 = load i8, i8* %107, align 1, !tbaa !41
  %conv123 = zext i8 %108 to i32
  %cmp124 = icmp slt i32 %conv123, 8
  br i1 %cmp124, label %if.then126, label %if.end135

if.then126:                                       ; preds = %land.lhs.true122
  %109 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %110 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx127 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %109, i32 %110
  %this_mv128 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx127, i32 0, i32 0
  %arrayidx129 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %this_refmv70, i32 0, i32 0
  %111 = bitcast %union.int_mv* %this_mv128 to i8*
  %112 = bitcast %union.int_mv* %arrayidx129 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %111, i8* align 4 %112, i32 4, i1 false), !tbaa.struct !57
  %113 = load %struct.candidate_mv*, %struct.candidate_mv** %ref_mv_stack.addr, align 4, !tbaa !2
  %114 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx130 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %113, i32 %114
  %comp_mv131 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx130, i32 0, i32 1
  %arrayidx132 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %this_refmv70, i32 0, i32 1
  %115 = bitcast %union.int_mv* %comp_mv131 to i8*
  %116 = bitcast %union.int_mv* %arrayidx132 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %115, i8* align 4 %116, i32 4, i1 false), !tbaa.struct !57
  %117 = load i16, i16* %weight.addr, align 2, !tbaa !58
  %118 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !2
  %119 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx133 = getelementptr inbounds i16, i16* %118, i32 %119
  store i16 %117, i16* %arrayidx133, align 2, !tbaa !58
  %120 = load i8*, i8** %refmv_count.addr, align 4, !tbaa !2
  %121 = load i8, i8* %120, align 1, !tbaa !41
  %inc134 = add i8 %121, 1
  store i8 %inc134, i8* %120, align 1, !tbaa !41
  br label %if.end135

if.end135:                                        ; preds = %if.then126, %land.lhs.true122, %for.end118
  %122 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %mode136 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %122, i32 0, i32 7
  %123 = load i8, i8* %mode136, align 1, !tbaa !108
  %call137 = call i32 @have_newmv_in_inter_mode(i8 zeroext %123)
  %tobool138 = icmp ne i32 %call137, 0
  br i1 %tobool138, label %if.then139, label %if.end141

if.then139:                                       ; preds = %if.end135
  %124 = load i8*, i8** %newmv_count.addr, align 4, !tbaa !2
  %125 = load i8, i8* %124, align 1, !tbaa !41
  %inc140 = add i8 %125, 1
  store i8 %inc140, i8* %124, align 1, !tbaa !41
  br label %if.end141

if.end141:                                        ; preds = %if.then139, %if.end135
  %126 = load i8*, i8** %ref_match_count.addr, align 4, !tbaa !2
  %127 = load i8, i8* %126, align 1, !tbaa !41
  %inc142 = add i8 %127, 1
  store i8 %inc142, i8* %126, align 1, !tbaa !41
  %128 = bitcast [2 x %union.int_mv]* %this_refmv70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %128) #6
  br label %if.end143

if.end143:                                        ; preds = %if.end141, %land.lhs.true61, %if.else
  br label %if.end144

if.end144:                                        ; preds = %if.end143, %for.end53
  %129 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #6
  %130 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #6
  br label %return

return:                                           ; preds = %if.end144, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_block(%struct.MB_MODE_INFO* %mbmi) #3 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !41
  %conv = sext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_global_mv_block(%struct.MB_MODE_INFO* %mbmi, i8 zeroext %type) #3 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %type.addr = alloca i8, align 1
  %mode = alloca i8, align 1
  %bsize = alloca i8, align 1
  %block_size_allowed = alloca i32, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  store i8 %type, i8* %type.addr, align 1, !tbaa !41
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mode) #6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %mode1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 7
  %1 = load i8, i8* %mode1, align 1, !tbaa !108
  store i8 %1, i8* %mode, align 1, !tbaa !41
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 6
  %3 = load i8, i8* %sb_type, align 2, !tbaa !48
  store i8 %3, i8* %bsize, align 1, !tbaa !41
  %4 = bitcast i32* %block_size_allowed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i8, i8* %bsize, align 1, !tbaa !41
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %conv = zext i8 %6 to i32
  %7 = load i8, i8* %bsize, align 1, !tbaa !41
  %idxprom2 = zext i8 %7 to i32
  %arrayidx3 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom2
  %8 = load i8, i8* %arrayidx3, align 1, !tbaa !41
  %conv4 = zext i8 %8 to i32
  %cmp = icmp slt i32 %conv, %conv4
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %9 = load i8, i8* %bsize, align 1, !tbaa !41
  %idxprom6 = zext i8 %9 to i32
  %arrayidx7 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom6
  %10 = load i8, i8* %arrayidx7, align 1, !tbaa !41
  %conv8 = zext i8 %10 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i8, i8* %bsize, align 1, !tbaa !41
  %idxprom9 = zext i8 %11 to i32
  %arrayidx10 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom9
  %12 = load i8, i8* %arrayidx10, align 1, !tbaa !41
  %conv11 = zext i8 %12 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv8, %cond.true ], [ %conv11, %cond.false ]
  %cmp12 = icmp sge i32 %cond, 8
  %conv13 = zext i1 %cmp12 to i32
  store i32 %conv13, i32* %block_size_allowed, align 4, !tbaa !6
  %13 = load i8, i8* %mode, align 1, !tbaa !41
  %conv14 = zext i8 %13 to i32
  %cmp15 = icmp eq i32 %conv14, 15
  br i1 %cmp15, label %land.lhs.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end
  %14 = load i8, i8* %mode, align 1, !tbaa !41
  %conv17 = zext i8 %14 to i32
  %cmp18 = icmp eq i32 %conv17, 23
  br i1 %cmp18, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %lor.lhs.false, %cond.end
  %15 = load i8, i8* %type.addr, align 1, !tbaa !41
  %conv20 = zext i8 %15 to i32
  %cmp21 = icmp sgt i32 %conv20, 1
  br i1 %cmp21, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %16 = load i32, i32* %block_size_allowed, align 4, !tbaa !6
  %tobool = icmp ne i32 %16, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %lor.lhs.false
  %17 = phi i1 [ false, %land.lhs.true ], [ false, %lor.lhs.false ], [ %tobool, %land.rhs ]
  %land.ext = zext i1 %17 to i32
  %18 = bitcast i32* %block_size_allowed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mode) #6
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal void @get_block_mv(%union.int_mv* noalias sret align 4 %agg.result, %struct.MB_MODE_INFO* %candidate, i32 %which_mv) #3 {
entry:
  %candidate.addr = alloca %struct.MB_MODE_INFO*, align 4
  %which_mv.addr = alloca i32, align 4
  store %struct.MB_MODE_INFO* %candidate, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  store i32 %which_mv, i32* %which_mv.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %candidate.addr, align 4, !tbaa !2
  %mv = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 2
  %1 = load i32, i32* %which_mv.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv, i32 0, i32 %1
  %2 = bitcast %union.int_mv* %agg.result to i8*
  %3 = bitcast %union.int_mv* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false), !tbaa.struct !57
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @have_newmv_in_inter_mode(i8 zeroext %mode) #3 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !41
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !41
  %conv = zext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 16
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8, i8* %mode.addr, align 1, !tbaa !41
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp eq i32 %conv2, 24
  br i1 %cmp3, label %lor.end, label %lor.lhs.false5

lor.lhs.false5:                                   ; preds = %lor.lhs.false
  %2 = load i8, i8* %mode.addr, align 1, !tbaa !41
  %conv6 = zext i8 %2 to i32
  %cmp7 = icmp eq i32 %conv6, 19
  br i1 %cmp7, label %lor.end, label %lor.lhs.false9

lor.lhs.false9:                                   ; preds = %lor.lhs.false5
  %3 = load i8, i8* %mode.addr, align 1, !tbaa !41
  %conv10 = zext i8 %3 to i32
  %cmp11 = icmp eq i32 %conv10, 20
  br i1 %cmp11, label %lor.end, label %lor.lhs.false13

lor.lhs.false13:                                  ; preds = %lor.lhs.false9
  %4 = load i8, i8* %mode.addr, align 1, !tbaa !41
  %conv14 = zext i8 %4 to i32
  %cmp15 = icmp eq i32 %conv14, 21
  br i1 %cmp15, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %lor.lhs.false13
  %5 = load i8, i8* %mode.addr, align 1, !tbaa !41
  %conv17 = zext i8 %5 to i32
  %cmp18 = icmp eq i32 %conv17, 22
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %lor.lhs.false13, %lor.lhs.false9, %lor.lhs.false5, %lor.lhs.false, %entry
  %6 = phi i1 [ true, %lor.lhs.false13 ], [ true, %lor.lhs.false9 ], [ true, %lor.lhs.false5 ], [ true, %lor.lhs.false ], [ true, %entry ], [ %cmp18, %lor.rhs ]
  %lor.ext = zext i1 %6 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_intrabc_block(%struct.MB_MODE_INFO* %mbmi) #3 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  ret i32 %conv
}

; Function Attrs: inlinehint nounwind
define internal void @get_mv_projection(%struct.mv* %output, %struct.mv* byval(%struct.mv) align 2 %ref, i32 %num, i32 %den) #3 {
entry:
  %output.addr = alloca %struct.mv*, align 4
  %num.addr = alloca i32, align 4
  %den.addr = alloca i32, align 4
  %mv_row = alloca i32, align 4
  %mv_col = alloca i32, align 4
  %clamp_max = alloca i32, align 4
  %clamp_min = alloca i32, align 4
  store %struct.mv* %output, %struct.mv** %output.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !6
  store i32 %den, i32* %den.addr, align 4, !tbaa !6
  %0 = load i32, i32* %den.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, 31
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %den.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %1, %cond.true ], [ 31, %cond.false ]
  store i32 %cond, i32* %den.addr, align 4, !tbaa !6
  %2 = load i32, i32* %num.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %2, 0
  br i1 %cmp1, label %cond.true2, label %cond.false8

cond.true2:                                       ; preds = %cond.end
  %3 = load i32, i32* %num.addr, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %3, 31
  br i1 %cmp3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.true2
  %4 = load i32, i32* %num.addr, align 4, !tbaa !6
  br label %cond.end6

cond.false5:                                      ; preds = %cond.true2
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi i32 [ %4, %cond.true4 ], [ 31, %cond.false5 ]
  br label %cond.end14

cond.false8:                                      ; preds = %cond.end
  %5 = load i32, i32* %num.addr, align 4, !tbaa !6
  %cmp9 = icmp sgt i32 %5, -31
  br i1 %cmp9, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %cond.false8
  %6 = load i32, i32* %num.addr, align 4, !tbaa !6
  br label %cond.end12

cond.false11:                                     ; preds = %cond.false8
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false11, %cond.true10
  %cond13 = phi i32 [ %6, %cond.true10 ], [ -31, %cond.false11 ]
  br label %cond.end14

cond.end14:                                       ; preds = %cond.end12, %cond.end6
  %cond15 = phi i32 [ %cond7, %cond.end6 ], [ %cond13, %cond.end12 ]
  store i32 %cond15, i32* %num.addr, align 4, !tbaa !6
  %7 = bitcast i32* %mv_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %row = getelementptr inbounds %struct.mv, %struct.mv* %ref, i32 0, i32 0
  %8 = load i16, i16* %row, align 2, !tbaa !69
  %conv = sext i16 %8 to i32
  %9 = load i32, i32* %num.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %9
  %10 = load i32, i32* %den.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [32 x i32], [32 x i32]* @div_mult, i32 0, i32 %10
  %11 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %mul16 = mul nsw i32 %mul, %11
  %cmp17 = icmp slt i32 %mul16, 0
  br i1 %cmp17, label %cond.true19, label %cond.false26

cond.true19:                                      ; preds = %cond.end14
  %row20 = getelementptr inbounds %struct.mv, %struct.mv* %ref, i32 0, i32 0
  %12 = load i16, i16* %row20, align 2, !tbaa !69
  %conv21 = sext i16 %12 to i32
  %13 = load i32, i32* %num.addr, align 4, !tbaa !6
  %mul22 = mul nsw i32 %conv21, %13
  %14 = load i32, i32* %den.addr, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds [32 x i32], [32 x i32]* @div_mult, i32 0, i32 %14
  %15 = load i32, i32* %arrayidx23, align 4, !tbaa !6
  %mul24 = mul nsw i32 %mul22, %15
  %sub = sub nsw i32 0, %mul24
  %add = add nsw i32 %sub, 8192
  %shr = ashr i32 %add, 14
  %sub25 = sub nsw i32 0, %shr
  br label %cond.end34

cond.false26:                                     ; preds = %cond.end14
  %row27 = getelementptr inbounds %struct.mv, %struct.mv* %ref, i32 0, i32 0
  %16 = load i16, i16* %row27, align 2, !tbaa !69
  %conv28 = sext i16 %16 to i32
  %17 = load i32, i32* %num.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 %conv28, %17
  %18 = load i32, i32* %den.addr, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds [32 x i32], [32 x i32]* @div_mult, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %mul31 = mul nsw i32 %mul29, %19
  %add32 = add nsw i32 %mul31, 8192
  %shr33 = ashr i32 %add32, 14
  br label %cond.end34

cond.end34:                                       ; preds = %cond.false26, %cond.true19
  %cond35 = phi i32 [ %sub25, %cond.true19 ], [ %shr33, %cond.false26 ]
  store i32 %cond35, i32* %mv_row, align 4, !tbaa !6
  %20 = bitcast i32* %mv_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %col = getelementptr inbounds %struct.mv, %struct.mv* %ref, i32 0, i32 1
  %21 = load i16, i16* %col, align 2, !tbaa !71
  %conv36 = sext i16 %21 to i32
  %22 = load i32, i32* %num.addr, align 4, !tbaa !6
  %mul37 = mul nsw i32 %conv36, %22
  %23 = load i32, i32* %den.addr, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds [32 x i32], [32 x i32]* @div_mult, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %mul39 = mul nsw i32 %mul37, %24
  %cmp40 = icmp slt i32 %mul39, 0
  br i1 %cmp40, label %cond.true42, label %cond.false52

cond.true42:                                      ; preds = %cond.end34
  %col43 = getelementptr inbounds %struct.mv, %struct.mv* %ref, i32 0, i32 1
  %25 = load i16, i16* %col43, align 2, !tbaa !71
  %conv44 = sext i16 %25 to i32
  %26 = load i32, i32* %num.addr, align 4, !tbaa !6
  %mul45 = mul nsw i32 %conv44, %26
  %27 = load i32, i32* %den.addr, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds [32 x i32], [32 x i32]* @div_mult, i32 0, i32 %27
  %28 = load i32, i32* %arrayidx46, align 4, !tbaa !6
  %mul47 = mul nsw i32 %mul45, %28
  %sub48 = sub nsw i32 0, %mul47
  %add49 = add nsw i32 %sub48, 8192
  %shr50 = ashr i32 %add49, 14
  %sub51 = sub nsw i32 0, %shr50
  br label %cond.end60

cond.false52:                                     ; preds = %cond.end34
  %col53 = getelementptr inbounds %struct.mv, %struct.mv* %ref, i32 0, i32 1
  %29 = load i16, i16* %col53, align 2, !tbaa !71
  %conv54 = sext i16 %29 to i32
  %30 = load i32, i32* %num.addr, align 4, !tbaa !6
  %mul55 = mul nsw i32 %conv54, %30
  %31 = load i32, i32* %den.addr, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds [32 x i32], [32 x i32]* @div_mult, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx56, align 4, !tbaa !6
  %mul57 = mul nsw i32 %mul55, %32
  %add58 = add nsw i32 %mul57, 8192
  %shr59 = ashr i32 %add58, 14
  br label %cond.end60

cond.end60:                                       ; preds = %cond.false52, %cond.true42
  %cond61 = phi i32 [ %sub51, %cond.true42 ], [ %shr59, %cond.false52 ]
  store i32 %cond61, i32* %mv_col, align 4, !tbaa !6
  %33 = bitcast i32* %clamp_max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  store i32 16383, i32* %clamp_max, align 4, !tbaa !6
  %34 = bitcast i32* %clamp_min to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  store i32 -16383, i32* %clamp_min, align 4, !tbaa !6
  %35 = load i32, i32* %mv_row, align 4, !tbaa !6
  %call = call i32 @clamp(i32 %35, i32 -16383, i32 16383)
  %conv62 = trunc i32 %call to i16
  %36 = load %struct.mv*, %struct.mv** %output.addr, align 4, !tbaa !2
  %row63 = getelementptr inbounds %struct.mv, %struct.mv* %36, i32 0, i32 0
  store i16 %conv62, i16* %row63, align 2, !tbaa !69
  %37 = load i32, i32* %mv_col, align 4, !tbaa !6
  %call64 = call i32 @clamp(i32 %37, i32 -16383, i32 16383)
  %conv65 = trunc i32 %call64 to i16
  %38 = load %struct.mv*, %struct.mv** %output.addr, align 4, !tbaa !2
  %col66 = getelementptr inbounds %struct.mv, %struct.mv* %38, i32 0, i32 1
  store i16 %conv65, i16* %col66, align 2, !tbaa !71
  %39 = bitcast i32* %clamp_min to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  %40 = bitcast i32* %clamp_max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #6
  %41 = bitcast i32* %mv_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #6
  %42 = bitcast i32* %mv_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @clamp_mv(%struct.mv* %mv, %struct.SubpelMvLimits* %mv_limits) #3 {
entry:
  %mv.addr = alloca %struct.mv*, align 4
  %mv_limits.addr = alloca %struct.SubpelMvLimits*, align 4
  store %struct.mv* %mv, %struct.mv** %mv.addr, align 4, !tbaa !2
  store %struct.SubpelMvLimits* %mv_limits, %struct.SubpelMvLimits** %mv_limits.addr, align 4, !tbaa !2
  %0 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col = getelementptr inbounds %struct.mv, %struct.mv* %0, i32 0, i32 1
  %1 = load i16, i16* %col, align 2, !tbaa !71
  %conv = sext i16 %1 to i32
  %2 = load %struct.SubpelMvLimits*, %struct.SubpelMvLimits** %mv_limits.addr, align 4, !tbaa !2
  %col_min = getelementptr inbounds %struct.SubpelMvLimits, %struct.SubpelMvLimits* %2, i32 0, i32 0
  %3 = load i32, i32* %col_min, align 4, !tbaa !77
  %4 = load %struct.SubpelMvLimits*, %struct.SubpelMvLimits** %mv_limits.addr, align 4, !tbaa !2
  %col_max = getelementptr inbounds %struct.SubpelMvLimits, %struct.SubpelMvLimits* %4, i32 0, i32 1
  %5 = load i32, i32* %col_max, align 4, !tbaa !78
  %call = call i32 @clamp(i32 %conv, i32 %3, i32 %5)
  %conv1 = trunc i32 %call to i16
  %6 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %col2 = getelementptr inbounds %struct.mv, %struct.mv* %6, i32 0, i32 1
  store i16 %conv1, i16* %col2, align 2, !tbaa !71
  %7 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row = getelementptr inbounds %struct.mv, %struct.mv* %7, i32 0, i32 0
  %8 = load i16, i16* %row, align 2, !tbaa !69
  %conv3 = sext i16 %8 to i32
  %9 = load %struct.SubpelMvLimits*, %struct.SubpelMvLimits** %mv_limits.addr, align 4, !tbaa !2
  %row_min = getelementptr inbounds %struct.SubpelMvLimits, %struct.SubpelMvLimits* %9, i32 0, i32 2
  %10 = load i32, i32* %row_min, align 4, !tbaa !95
  %11 = load %struct.SubpelMvLimits*, %struct.SubpelMvLimits** %mv_limits.addr, align 4, !tbaa !2
  %row_max = getelementptr inbounds %struct.SubpelMvLimits, %struct.SubpelMvLimits* %11, i32 0, i32 3
  %12 = load i32, i32* %row_max, align 4, !tbaa !96
  %call4 = call i32 @clamp(i32 %conv3, i32 %10, i32 %12)
  %conv5 = trunc i32 %call4 to i16
  %13 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !2
  %row6 = getelementptr inbounds %struct.mv, %struct.mv* %13, i32 0, i32 0
  store i16 %conv5, i16* %row6, align 2, !tbaa !69
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_ref_frame_map_idx(%struct.AV1Common* %cm, i8 signext %ref_frame) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !41
  %0 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %conv = sext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 1
  br i1 %cmp, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %conv2 = sext i8 %1 to i32
  %cmp3 = icmp sle i32 %conv2, 8
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %remapped_ref_idx = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 14
  %3 = load i8, i8* %ref_frame.addr, align 1, !tbaa !41
  %conv5 = sext i8 %3 to i32
  %sub = sub nsw i32 %conv5, 1
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %remapped_ref_idx, i32 0, i32 %sub
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ -1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define internal i32 @get_block_position(%struct.AV1Common* %cm, i32* %mi_r, i32* %mi_c, i32 %blk_row, i32 %blk_col, %struct.mv* byval(%struct.mv) align 2 %mv, i32 %sign_bias) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %mi_r.addr = alloca i32*, align 4
  %mi_c.addr = alloca i32*, align 4
  %blk_row.addr = alloca i32, align 4
  %blk_col.addr = alloca i32, align 4
  %sign_bias.addr = alloca i32, align 4
  %base_blk_row = alloca i32, align 4
  %base_blk_col = alloca i32, align 4
  %row_offset = alloca i32, align 4
  %col_offset = alloca i32, align 4
  %row26 = alloca i32, align 4
  %col34 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32* %mi_r, i32** %mi_r.addr, align 4, !tbaa !2
  store i32* %mi_c, i32** %mi_c.addr, align 4, !tbaa !2
  store i32 %blk_row, i32* %blk_row.addr, align 4, !tbaa !6
  store i32 %blk_col, i32* %blk_col.addr, align 4, !tbaa !6
  store i32 %sign_bias, i32* %sign_bias.addr, align 4, !tbaa !6
  %0 = bitcast i32* %base_blk_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %shr = ashr i32 %1, 3
  %shl = shl i32 %shr, 3
  store i32 %shl, i32* %base_blk_row, align 4, !tbaa !6
  %2 = bitcast i32* %base_blk_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %shr1 = ashr i32 %3, 3
  %shl2 = shl i32 %shr1, 3
  store i32 %shl2, i32* %base_blk_col, align 4, !tbaa !6
  %4 = bitcast i32* %row_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %row = getelementptr inbounds %struct.mv, %struct.mv* %mv, i32 0, i32 0
  %5 = load i16, i16* %row, align 2, !tbaa !69
  %conv = sext i16 %5 to i32
  %cmp = icmp sge i32 %conv, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %row4 = getelementptr inbounds %struct.mv, %struct.mv* %mv, i32 0, i32 0
  %6 = load i16, i16* %row4, align 2, !tbaa !69
  %conv5 = sext i16 %6 to i32
  %shr6 = ashr i32 %conv5, 6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %row7 = getelementptr inbounds %struct.mv, %struct.mv* %mv, i32 0, i32 0
  %7 = load i16, i16* %row7, align 2, !tbaa !69
  %conv8 = sext i16 %7 to i32
  %sub = sub nsw i32 0, %conv8
  %shr9 = ashr i32 %sub, 6
  %sub10 = sub nsw i32 0, %shr9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shr6, %cond.true ], [ %sub10, %cond.false ]
  store i32 %cond, i32* %row_offset, align 4, !tbaa !6
  %8 = bitcast i32* %col_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %col = getelementptr inbounds %struct.mv, %struct.mv* %mv, i32 0, i32 1
  %9 = load i16, i16* %col, align 2, !tbaa !71
  %conv11 = sext i16 %9 to i32
  %cmp12 = icmp sge i32 %conv11, 0
  br i1 %cmp12, label %cond.true14, label %cond.false18

cond.true14:                                      ; preds = %cond.end
  %col15 = getelementptr inbounds %struct.mv, %struct.mv* %mv, i32 0, i32 1
  %10 = load i16, i16* %col15, align 2, !tbaa !71
  %conv16 = sext i16 %10 to i32
  %shr17 = ashr i32 %conv16, 6
  br label %cond.end24

cond.false18:                                     ; preds = %cond.end
  %col19 = getelementptr inbounds %struct.mv, %struct.mv* %mv, i32 0, i32 1
  %11 = load i16, i16* %col19, align 2, !tbaa !71
  %conv20 = sext i16 %11 to i32
  %sub21 = sub nsw i32 0, %conv20
  %shr22 = ashr i32 %sub21, 6
  %sub23 = sub nsw i32 0, %shr22
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false18, %cond.true14
  %cond25 = phi i32 [ %shr17, %cond.true14 ], [ %sub23, %cond.false18 ]
  store i32 %cond25, i32* %col_offset, align 4, !tbaa !6
  %12 = bitcast i32* %row26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i32, i32* %sign_bias.addr, align 4, !tbaa !6
  %cmp27 = icmp eq i32 %13, 1
  br i1 %cmp27, label %cond.true29, label %cond.false31

cond.true29:                                      ; preds = %cond.end24
  %14 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %15 = load i32, i32* %row_offset, align 4, !tbaa !6
  %sub30 = sub nsw i32 %14, %15
  br label %cond.end32

cond.false31:                                     ; preds = %cond.end24
  %16 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %17 = load i32, i32* %row_offset, align 4, !tbaa !6
  %add = add nsw i32 %16, %17
  br label %cond.end32

cond.end32:                                       ; preds = %cond.false31, %cond.true29
  %cond33 = phi i32 [ %sub30, %cond.true29 ], [ %add, %cond.false31 ]
  store i32 %cond33, i32* %row26, align 4, !tbaa !6
  %18 = bitcast i32* %col34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load i32, i32* %sign_bias.addr, align 4, !tbaa !6
  %cmp35 = icmp eq i32 %19, 1
  br i1 %cmp35, label %cond.true37, label %cond.false39

cond.true37:                                      ; preds = %cond.end32
  %20 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %21 = load i32, i32* %col_offset, align 4, !tbaa !6
  %sub38 = sub nsw i32 %20, %21
  br label %cond.end41

cond.false39:                                     ; preds = %cond.end32
  %22 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %23 = load i32, i32* %col_offset, align 4, !tbaa !6
  %add40 = add nsw i32 %22, %23
  br label %cond.end41

cond.end41:                                       ; preds = %cond.false39, %cond.true37
  %cond42 = phi i32 [ %sub38, %cond.true37 ], [ %add40, %cond.false39 ]
  store i32 %cond42, i32* %col34, align 4, !tbaa !6
  %24 = load i32, i32* %row26, align 4, !tbaa !6
  %cmp43 = icmp slt i32 %24, 0
  br i1 %cmp43, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end41
  %25 = load i32, i32* %row26, align 4, !tbaa !6
  %26 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %26, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %27 = load i32, i32* %mi_rows, align 4, !tbaa !66
  %shr45 = ashr i32 %27, 1
  %cmp46 = icmp sge i32 %25, %shr45
  br i1 %cmp46, label %if.then, label %lor.lhs.false48

lor.lhs.false48:                                  ; preds = %lor.lhs.false
  %28 = load i32, i32* %col34, align 4, !tbaa !6
  %cmp49 = icmp slt i32 %28, 0
  br i1 %cmp49, label %if.then, label %lor.lhs.false51

lor.lhs.false51:                                  ; preds = %lor.lhs.false48
  %29 = load i32, i32* %col34, align 4, !tbaa !6
  %30 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params52 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %30, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params52, i32 0, i32 4
  %31 = load i32, i32* %mi_cols, align 4, !tbaa !8
  %shr53 = ashr i32 %31, 1
  %cmp54 = icmp sge i32 %29, %shr53
  br i1 %cmp54, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false51, %lor.lhs.false48, %lor.lhs.false, %cond.end41
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false51
  %32 = load i32, i32* %row26, align 4, !tbaa !6
  %33 = load i32, i32* %base_blk_row, align 4, !tbaa !6
  %sub56 = sub nsw i32 %33, 0
  %cmp57 = icmp slt i32 %32, %sub56
  br i1 %cmp57, label %if.then73, label %lor.lhs.false59

lor.lhs.false59:                                  ; preds = %if.end
  %34 = load i32, i32* %row26, align 4, !tbaa !6
  %35 = load i32, i32* %base_blk_row, align 4, !tbaa !6
  %add60 = add nsw i32 %35, 8
  %add61 = add nsw i32 %add60, 0
  %cmp62 = icmp sge i32 %34, %add61
  br i1 %cmp62, label %if.then73, label %lor.lhs.false64

lor.lhs.false64:                                  ; preds = %lor.lhs.false59
  %36 = load i32, i32* %col34, align 4, !tbaa !6
  %37 = load i32, i32* %base_blk_col, align 4, !tbaa !6
  %sub65 = sub nsw i32 %37, 8
  %cmp66 = icmp slt i32 %36, %sub65
  br i1 %cmp66, label %if.then73, label %lor.lhs.false68

lor.lhs.false68:                                  ; preds = %lor.lhs.false64
  %38 = load i32, i32* %col34, align 4, !tbaa !6
  %39 = load i32, i32* %base_blk_col, align 4, !tbaa !6
  %add69 = add nsw i32 %39, 8
  %add70 = add nsw i32 %add69, 8
  %cmp71 = icmp sge i32 %38, %add70
  br i1 %cmp71, label %if.then73, label %if.end74

if.then73:                                        ; preds = %lor.lhs.false68, %lor.lhs.false64, %lor.lhs.false59, %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end74:                                         ; preds = %lor.lhs.false68
  %40 = load i32, i32* %row26, align 4, !tbaa !6
  %41 = load i32*, i32** %mi_r.addr, align 4, !tbaa !2
  store i32 %40, i32* %41, align 4, !tbaa !6
  %42 = load i32, i32* %col34, align 4, !tbaa !6
  %43 = load i32*, i32** %mi_c.addr, align 4, !tbaa !2
  store i32 %42, i32* %43, align 4, !tbaa !6
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end74, %if.then73, %if.then
  %44 = bitcast i32* %col34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  %45 = bitcast i32* %row26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  %46 = bitcast i32* %col_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  %47 = bitcast i32* %row_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  %48 = bitcast i32* %base_blk_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #6
  %49 = bitcast i32* %base_blk_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  %50 = load i32, i32* %retval, align 4
  ret i32 %50
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 1372}
!9 = !{!"AV1Common", !10, i64 0, !12, i64 40, !7, i64 288, !7, i64 292, !7, i64 296, !7, i64 300, !7, i64 304, !7, i64 308, !4, i64 312, !13, i64 313, !4, i64 316, !7, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !14, i64 492, !4, i64 580, !4, i64 1284, !7, i64 1316, !7, i64 1320, !7, i64 1324, !15, i64 1328, !16, i64 1356, !17, i64 1420, !18, i64 10676, !3, i64 10848, !19, i64 10864, !20, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !21, i64 14880, !23, i64 15028, !24, i64 15168, !26, i64 15816, !4, i64 15836, !27, i64 16192, !3, i64 18128, !3, i64 18132, !30, i64 18136, !3, i64 18724, !31, i64 18728, !7, i64 18760, !4, i64 18764, !3, i64 18796, !7, i64 18800, !4, i64 18804, !4, i64 18836, !7, i64 18844, !7, i64 18848, !7, i64 18852, !7, i64 18856}
!10 = !{!"", !4, i64 0, !4, i64 1, !7, i64 4, !7, i64 8, !7, i64 12, !11, i64 16, !7, i64 32, !7, i64 36}
!11 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!12 = !{!"aom_internal_error_info", !4, i64 0, !7, i64 4, !4, i64 8, !7, i64 88, !4, i64 92}
!13 = !{!"_Bool", !4, i64 0}
!14 = !{!"scale_factors", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!15 = !{!"", !13, i64 0, !13, i64 1, !13, i64 2, !13, i64 3, !13, i64 4, !13, i64 5, !13, i64 6, !13, i64 7, !13, i64 8, !13, i64 9, !13, i64 10, !13, i64 11, !4, i64 12, !4, i64 13, !7, i64 16, !7, i64 20, !4, i64 24}
!16 = !{!"CommonModeInfoParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !7, i64 28, !4, i64 32, !3, i64 36, !7, i64 40, !7, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!17 = !{!"CommonQuantParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !13, i64 9240, !7, i64 9244, !7, i64 9248, !7, i64 9252}
!18 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !7, i64 164, !4, i64 168}
!19 = !{!"", !4, i64 0, !4, i64 3072}
!20 = !{!"loopfilter", !4, i64 0, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !7, i64 32}
!21 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !22, i64 80, !7, i64 84, !22, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!22 = !{!"long", !4, i64 0}
!23 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !4, i64 72, !7, i64 136}
!24 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !25, i64 644}
!25 = !{!"short", !4, i64 0}
!26 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!27 = !{!"SequenceHeader", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !4, i64 16, !7, i64 20, !7, i64 24, !4, i64 28, !7, i64 32, !7, i64 36, !11, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !7, i64 112, !4, i64 116, !7, i64 244, !28, i64 248, !4, i64 264, !29, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!28 = !{!"aom_timing", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!29 = !{!"aom_dec_model_info", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!30 = !{!"CommonTileParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !4, i64 60, !4, i64 320, !7, i64 580, !7, i64 584}
!31 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !7, i64 20, !7, i64 24, !7, i64 28}
!32 = !{!9, !3, i64 456}
!33 = !{!34, !3, i64 68}
!34 = !{!"RefCntBuffer", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 36, !4, i64 40, !3, i64 68, !3, i64 72, !18, i64 76, !7, i64 248, !7, i64 252, !7, i64 256, !7, i64 260, !4, i64 264, !7, i64 616, !4, i64 620, !24, i64 624, !35, i64 1272, !21, i64 1284, !4, i64 1432, !4, i64 1436, !4, i64 1452, !4, i64 1460, !36, i64 1464}
!35 = !{!"aom_codec_frame_buffer", !3, i64 0, !22, i64 4, !3, i64 8}
!36 = !{!"frame_contexts", !4, i64 0, !4, i64 390, !4, i64 930, !4, i64 966, !4, i64 1014, !4, i64 1070, !4, i64 1134, !4, i64 1206, !4, i64 1286, !4, i64 1374, !4, i64 1470, !4, i64 1790, !4, i64 5990, !4, i64 8090, !4, i64 8126, !4, i64 8138, !4, i64 8174, !4, i64 8192, !4, i64 8336, !4, i64 8468, !4, i64 9216, !4, i64 9240, !4, i64 9372, !4, i64 9412, !4, i64 9588, !4, i64 9720, !4, i64 9832, !4, i64 9944, !4, i64 10574, !4, i64 11204, !4, i64 11330, !4, i64 11342, !4, i64 11372, !4, i64 11480, !4, i64 11510, !4, i64 11564, !4, i64 11618, !4, i64 11654, !4, i64 11780, !4, i64 11816, !4, i64 11852, !4, i64 11870, !4, i64 11888, !37, i64 11912, !37, i64 12198, !4, i64 12484, !38, i64 12490, !4, i64 12580, !4, i64 12712, !4, i64 12724, !4, i64 12732, !4, i64 12738, !4, i64 12744, !4, i64 12856, !4, i64 13636, !4, i64 14076, !4, i64 14204, !4, i64 14904, !4, i64 15032, !4, i64 15128, !4, i64 15138, !4, i64 15178, !4, i64 15188, !4, i64 20492, !4, i64 21036, !4, i64 21054, !7, i64 21260}
!37 = !{!"", !4, i64 0, !4, i64 10}
!38 = !{!"segmentation_probs", !4, i64 0, !4, i64 18, !4, i64 36}
!39 = !{!40, !4, i64 4}
!40 = !{!"", !4, i64 0, !4, i64 4}
!41 = !{!4, !4, i64 0}
!42 = !{!43, !7, i64 0}
!43 = !{!"macroblockd", !7, i64 0, !7, i64 4, !7, i64 8, !13, i64 12, !4, i64 16, !44, i64 4060, !3, i64 4084, !13, i64 4088, !13, i64 4089, !13, i64 4090, !13, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !7, i64 4112, !7, i64 4116, !7, i64 4120, !7, i64 4124, !7, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !7, i64 6836, !4, i64 6840, !4, i64 6872, !7, i64 6904, !7, i64 6908, !3, i64 6912, !3, i64 6916, !7, i64 6920, !7, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !45, i64 39720, !46, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!44 = !{!"TileInfo", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20}
!45 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !7, i64 4104, !4, i64 4108, !7, i64 4236, !7, i64 4240, !7, i64 4244, !7, i64 4248, !7, i64 4252, !7, i64 4256}
!46 = !{!"dist_wtd_comp_params", !7, i64 0, !7, i64 4, !7, i64 8}
!47 = !{!43, !7, i64 4}
!48 = !{!49, !4, i64 118}
!49 = !{!"MB_MODE_INFO", !50, i64 0, !51, i64 8, !4, i64 52, !7, i64 60, !4, i64 64, !52, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !53, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!50 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!51 = !{!"", !4, i64 0, !25, i64 32, !25, i64 34, !25, i64 36, !25, i64 38, !4, i64 40, !4, i64 41}
!52 = !{!"", !4, i64 0, !4, i64 48}
!53 = !{!"", !4, i64 0, !4, i64 1}
!54 = !{!9, !13, i64 1329}
!55 = !{i8 0, i8 2}
!56 = !{!9, !13, i64 1330}
!57 = !{i64 0, i64 4, !6, i64 0, i64 2, !58, i64 2, i64 2, !58, i64 0, i64 2, !58, i64 2, i64 2, !58}
!58 = !{!25, !25, i64 0}
!59 = !{!51, !4, i64 40}
!60 = !{!43, !4, i64 4468}
!61 = !{!43, !4, i64 4469}
!62 = !{!43, !13, i64 4088}
!63 = !{!43, !13, i64 4089}
!64 = !{!9, !13, i64 1334}
!65 = !{i64 0, i64 4, !6, i64 0, i64 2, !58, i64 2, i64 2, !58, i64 0, i64 2, !58, i64 2, i64 2, !58, i64 4, i64 4, !6, i64 4, i64 2, !58, i64 6, i64 2, !58, i64 4, i64 2, !58, i64 6, i64 2, !58}
!66 = !{!9, !7, i64 1368}
!67 = !{!43, !3, i64 4084}
!68 = !{!43, !7, i64 8}
!69 = !{!70, !25, i64 0}
!70 = !{!"mv", !25, i64 0, !25, i64 2}
!71 = !{!70, !25, i64 2}
!72 = !{!9, !7, i64 4}
!73 = !{!34, !7, i64 4}
!74 = !{!9, !7, i64 8}
!75 = !{!34, !7, i64 36}
!76 = !{!9, !7, i64 16232}
!77 = !{!11, !7, i64 0}
!78 = !{!11, !7, i64 4}
!79 = !{!9, !3, i64 18796}
!80 = !{!9, !7, i64 1400}
!81 = !{!34, !4, i64 1432}
!82 = !{!34, !7, i64 248}
!83 = !{!34, !7, i64 252}
!84 = !{i64 0, i64 2, !58, i64 2, i64 2, !58}
!85 = !{!86, !7, i64 0}
!86 = !{!"position", !7, i64 0, !7, i64 4}
!87 = !{!86, !7, i64 4}
!88 = !{!9, !4, i64 16220}
!89 = !{!43, !4, i64 6820}
!90 = !{!49, !4, i64 123}
!91 = !{!44, !7, i64 0}
!92 = !{!44, !7, i64 8}
!93 = !{!44, !7, i64 4}
!94 = !{!44, !7, i64 12}
!95 = !{!11, !7, i64 8}
!96 = !{!11, !7, i64 12}
!97 = !{!9, !4, i64 1}
!98 = !{!9, !4, i64 0}
!99 = !{!9, !7, i64 16236}
!100 = !{!101, !7, i64 0}
!101 = !{!"", !7, i64 0, !3, i64 4, !7, i64 8}
!102 = !{!101, !7, i64 8}
!103 = !{!101, !3, i64 4}
!104 = !{!43, !7, i64 4116}
!105 = !{!43, !7, i64 4120}
!106 = !{!43, !7, i64 4124}
!107 = !{!43, !7, i64 4128}
!108 = !{!49, !4, i64 119}
