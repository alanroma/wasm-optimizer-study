; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/debugmodes.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/debugmodes.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct._IO_FILE = type opaque
%struct.mv = type { i16, i16 }

@.str = private unnamed_addr constant [2 x i8] c"a\00", align 1
@.str.1 = private unnamed_addr constant [12 x i8] c"Partitions:\00", align 1
@.str.2 = private unnamed_addr constant [7 x i8] c"Modes:\00", align 1
@.str.3 = private unnamed_addr constant [11 x i8] c"Ref frame:\00", align 1
@.str.4 = private unnamed_addr constant [11 x i8] c"Transform:\00", align 1
@.str.5 = private unnamed_addr constant [10 x i8] c"UV Modes:\00", align 1
@.str.6 = private unnamed_addr constant [7 x i8] c"Skips:\00", align 1
@.str.7 = private unnamed_addr constant [3 x i8] c"S \00", align 1
@.str.8 = private unnamed_addr constant [5 x i8] c"%2d \00", align 1
@.str.9 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.10 = private unnamed_addr constant [9 x i8] c"Vectors \00", align 1
@.str.11 = private unnamed_addr constant [3 x i8] c"V \00", align 1
@.str.12 = private unnamed_addr constant [9 x i8] c"%4d:%4d \00", align 1
@.str.13 = private unnamed_addr constant [2 x i8] c"w\00", align 1
@.str.14 = private unnamed_addr constant [4 x i8] c"%d \00", align 1
@.str.15 = private unnamed_addr constant [4 x i8] c"%c \00", align 1
@.str.16 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str.17 = private unnamed_addr constant [29 x i8] c"(Frame %d, Show:%d, Q:%d): \0A\00", align 1

; Function Attrs: nounwind
define hidden void @av1_print_modes_and_motion_vectors(%struct.AV1Common* %cm, i8* %file) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %file.addr = alloca i8*, align 4
  %mi_params = alloca %struct.CommonModeInfoParams*, align 4
  %mvs = alloca %struct._IO_FILE*, align 4
  %mi = alloca %struct.MB_MODE_INFO**, align 4
  %rows = alloca i32, align 4
  %cols = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %mi_row14 = alloca i32, align 4
  %mi_col21 = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8* %file, i8** %file.addr, align 4, !tbaa !2
  %0 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 22
  store %struct.CommonModeInfoParams* %mi_params1, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %2 = bitcast %struct._IO_FILE** %mvs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %file.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @fopen(i8* %3, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  store %struct._IO_FILE* %call, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %4 = bitcast %struct.MB_MODE_INFO*** %mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_grid_base = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %5, i32 0, i32 9
  %6 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base, align 4, !tbaa !6
  store %struct.MB_MODE_INFO** %6, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %7 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %8, i32 0, i32 3
  %9 = load i32, i32* %mi_rows, align 4, !tbaa !9
  store i32 %9, i32* %rows, align 4, !tbaa !10
  %10 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %11, i32 0, i32 4
  %12 = load i32, i32* %mi_cols, align 4, !tbaa !11
  store i32 %12, i32* %cols, align 4, !tbaa !10
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  call void @print_mi_data(%struct.AV1Common* %13, %struct._IO_FILE* %14, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i32 0, i32 0), i32 118)
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  call void @print_mi_data(%struct.AV1Common* %15, %struct._IO_FILE* %16, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.2, i32 0, i32 0), i32 119)
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  call void @print_mi_data(%struct.AV1Common* %17, %struct._IO_FILE* %18, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.3, i32 0, i32 0), i32 124)
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  call void @print_mi_data(%struct.AV1Common* %19, %struct._IO_FILE* %20, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.4, i32 0, i32 0), i32 145)
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  call void @print_mi_data(%struct.AV1Common* %21, %struct._IO_FILE* %22, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.5, i32 0, i32 0), i32 120)
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  call void @log_frame_info(%struct.AV1Common* %23, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.6, i32 0, i32 0), %struct._IO_FILE* %24)
  %25 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  store i32 0, i32* %mi_row, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc9, %entry
  %26 = load i32, i32* %mi_row, align 4, !tbaa !10
  %27 = load i32, i32* %rows, align 4, !tbaa !10
  %cmp = icmp slt i32 %26, %27
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %28 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  br label %for.end11

for.body:                                         ; preds = %for.cond
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %call2 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.7, i32 0, i32 0))
  %30 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  store i32 0, i32* %mi_col, align 4, !tbaa !10
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %31 = load i32, i32* %mi_col, align 4, !tbaa !10
  %32 = load i32, i32* %cols, align 4, !tbaa !10
  %cmp4 = icmp slt i32 %31, %32
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #3
  br label %for.end

for.body6:                                        ; preds = %for.cond3
  %34 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %35 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %35, i32 0
  %36 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %36, i32 0, i32 14
  %37 = load i8, i8* %skip, align 4, !tbaa !12
  %conv = sext i8 %37 to i32
  %call7 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %34, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.8, i32 0, i32 0), i32 %conv)
  %38 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %38, i32 1
  store %struct.MB_MODE_INFO** %incdec.ptr, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %39 = load i32, i32* %mi_col, align 4, !tbaa !10
  %inc = add nsw i32 %39, 1
  store i32 %inc, i32* %mi_col, align 4, !tbaa !10
  br label %for.cond3

for.end:                                          ; preds = %for.cond.cleanup5
  %40 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %40, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.9, i32 0, i32 0))
  %41 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %41, i32 0, i32 11
  %42 = load i32, i32* %mi_stride, align 4, !tbaa !19
  %43 = load i32, i32* %cols, align 4, !tbaa !10
  %sub = sub nsw i32 %42, %43
  %44 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %44, i32 %sub
  store %struct.MB_MODE_INFO** %add.ptr, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  br label %for.inc9

for.inc9:                                         ; preds = %for.end
  %45 = load i32, i32* %mi_row, align 4, !tbaa !10
  %inc10 = add nsw i32 %45, 1
  store i32 %inc10, i32* %mi_row, align 4, !tbaa !10
  br label %for.cond

for.end11:                                        ; preds = %for.cond.cleanup
  %46 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %46, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.9, i32 0, i32 0))
  %47 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %48 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  call void @log_frame_info(%struct.AV1Common* %47, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.10, i32 0, i32 0), %struct._IO_FILE* %48)
  %49 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_grid_base13 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %49, i32 0, i32 9
  %50 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base13, align 4, !tbaa !6
  store %struct.MB_MODE_INFO** %50, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %51 = bitcast i32* %mi_row14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #3
  store i32 0, i32* %mi_row14, align 4, !tbaa !10
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc44, %for.end11
  %52 = load i32, i32* %mi_row14, align 4, !tbaa !10
  %53 = load i32, i32* %rows, align 4, !tbaa !10
  %cmp16 = icmp slt i32 %52, %53
  br i1 %cmp16, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond15
  store i32 8, i32* %cleanup.dest.slot, align 4
  %54 = bitcast i32* %mi_row14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  br label %for.end46

for.body19:                                       ; preds = %for.cond15
  %55 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %call20 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %55, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.11, i32 0, i32 0))
  %56 = bitcast i32* %mi_col21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #3
  store i32 0, i32* %mi_col21, align 4, !tbaa !10
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc37, %for.body19
  %57 = load i32, i32* %mi_col21, align 4, !tbaa !10
  %58 = load i32, i32* %cols, align 4, !tbaa !10
  %cmp23 = icmp slt i32 %57, %58
  br i1 %cmp23, label %for.body26, label %for.cond.cleanup25

for.cond.cleanup25:                               ; preds = %for.cond22
  store i32 11, i32* %cleanup.dest.slot, align 4
  %59 = bitcast i32* %mi_col21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  br label %for.end39

for.body26:                                       ; preds = %for.cond22
  %60 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %61 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %61, i32 0
  %62 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx27, align 4, !tbaa !2
  %mv = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %62, i32 0, i32 2
  %arrayidx28 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv, i32 0, i32 0
  %as_mv = bitcast %union.int_mv* %arrayidx28 to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv, i32 0, i32 0
  %63 = load i16, i16* %row, align 4, !tbaa !20
  %conv29 = sext i16 %63 to i32
  %64 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %64, i32 0
  %65 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx30, align 4, !tbaa !2
  %mv31 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %65, i32 0, i32 2
  %arrayidx32 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv31, i32 0, i32 0
  %as_mv33 = bitcast %union.int_mv* %arrayidx32 to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv33, i32 0, i32 1
  %66 = load i16, i16* %col, align 2, !tbaa !20
  %conv34 = sext i16 %66 to i32
  %call35 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %60, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.12, i32 0, i32 0), i32 %conv29, i32 %conv34)
  %67 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %incdec.ptr36 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %67, i32 1
  store %struct.MB_MODE_INFO** %incdec.ptr36, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  br label %for.inc37

for.inc37:                                        ; preds = %for.body26
  %68 = load i32, i32* %mi_col21, align 4, !tbaa !10
  %inc38 = add nsw i32 %68, 1
  store i32 %inc38, i32* %mi_col21, align 4, !tbaa !10
  br label %for.cond22

for.end39:                                        ; preds = %for.cond.cleanup25
  %69 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %call40 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %69, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.9, i32 0, i32 0))
  %70 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_stride41 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %70, i32 0, i32 11
  %71 = load i32, i32* %mi_stride41, align 4, !tbaa !19
  %72 = load i32, i32* %cols, align 4, !tbaa !10
  %sub42 = sub nsw i32 %71, %72
  %73 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %add.ptr43 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %73, i32 %sub42
  store %struct.MB_MODE_INFO** %add.ptr43, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  br label %for.inc44

for.inc44:                                        ; preds = %for.end39
  %74 = load i32, i32* %mi_row14, align 4, !tbaa !10
  %inc45 = add nsw i32 %74, 1
  store i32 %inc45, i32* %mi_row14, align 4, !tbaa !10
  br label %for.cond15

for.end46:                                        ; preds = %for.cond.cleanup18
  %75 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %call47 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %75, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.9, i32 0, i32 0))
  %76 = load %struct._IO_FILE*, %struct._IO_FILE** %mvs, align 4, !tbaa !2
  %call48 = call i32 @fclose(%struct._IO_FILE* %76)
  %77 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  %78 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  %79 = bitcast %struct.MB_MODE_INFO*** %mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  %80 = bitcast %struct._IO_FILE** %mvs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #3
  %81 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare %struct._IO_FILE* @fopen(i8*, i8*) #2

; Function Attrs: nounwind
define internal void @print_mi_data(%struct.AV1Common* %cm, %struct._IO_FILE* %file, i8* %descriptor, i32 %member_offset) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %file.addr = alloca %struct._IO_FILE*, align 4
  %descriptor.addr = alloca i8*, align 4
  %member_offset.addr = alloca i32, align 4
  %mi_params = alloca %struct.CommonModeInfoParams*, align 4
  %mi = alloca %struct.MB_MODE_INFO**, align 4
  %rows = alloca i32, align 4
  %cols = alloca i32, align 4
  %prefix = alloca i8, align 1
  %mi_row = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %mi_col = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %file, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  store i8* %descriptor, i8** %descriptor.addr, align 4, !tbaa !2
  store i32 %member_offset, i32* %member_offset.addr, align 4, !tbaa !21
  %0 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 22
  store %struct.CommonModeInfoParams* %mi_params1, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %2 = bitcast %struct.MB_MODE_INFO*** %mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_grid_base = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %3, i32 0, i32 9
  %4 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base, align 4, !tbaa !6
  store %struct.MB_MODE_INFO** %4, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %5 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %6, i32 0, i32 3
  %7 = load i32, i32* %mi_rows, align 4, !tbaa !9
  store i32 %7, i32* %rows, align 4, !tbaa !10
  %8 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %9, i32 0, i32 4
  %10 = load i32, i32* %mi_cols, align 4, !tbaa !11
  store i32 %10, i32* %cols, align 4, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %prefix) #3
  %11 = load i8*, i8** %descriptor.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %11, i32 0
  %12 = load i8, i8* %arrayidx, align 1, !tbaa !20
  store i8 %12, i8* %prefix, align 1, !tbaa !20
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %14 = load i8*, i8** %descriptor.addr, align 4, !tbaa !2
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  call void @log_frame_info(%struct.AV1Common* %13, i8* %14, %struct._IO_FILE* %15)
  %16 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  store i32 0, i32* %mi_row, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %entry
  %17 = load i32, i32* %mi_row, align 4, !tbaa !10
  %18 = load i32, i32* %rows, align 4, !tbaa !10
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  br label %for.end14

for.body:                                         ; preds = %for.cond
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %21 = load i8, i8* %prefix, align 1, !tbaa !20
  %conv = sext i8 %21 to i32
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %20, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.15, i32 0, i32 0), i32 %conv)
  %22 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  store i32 0, i32* %mi_col, align 4, !tbaa !10
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %23 = load i32, i32* %mi_col, align 4, !tbaa !10
  %24 = load i32, i32* %cols, align 4, !tbaa !10
  %cmp3 = icmp slt i32 %23, %24
  br i1 %cmp3, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %25 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #3
  br label %for.end

for.body6:                                        ; preds = %for.cond2
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %27 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %27, i32 0
  %28 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx7, align 4, !tbaa !2
  %29 = bitcast %struct.MB_MODE_INFO* %28 to i8*
  %30 = load i32, i32* %member_offset.addr, align 4, !tbaa !21
  %add.ptr = getelementptr inbounds i8, i8* %29, i32 %30
  %31 = load i8, i8* %add.ptr, align 1, !tbaa !20
  %conv8 = sext i8 %31 to i32
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %26, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.8, i32 0, i32 0), i32 %conv8)
  %32 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %32, i32 1
  store %struct.MB_MODE_INFO** %incdec.ptr, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %33 = load i32, i32* %mi_col, align 4, !tbaa !10
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %mi_col, align 4, !tbaa !10
  br label %for.cond2

for.end:                                          ; preds = %for.cond.cleanup5
  %34 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %34, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.9, i32 0, i32 0))
  %35 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %35, i32 0, i32 11
  %36 = load i32, i32* %mi_stride, align 4, !tbaa !19
  %37 = load i32, i32* %cols, align 4, !tbaa !10
  %sub = sub nsw i32 %36, %37
  %38 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %38, i32 %sub
  store %struct.MB_MODE_INFO** %add.ptr11, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %39 = load i32, i32* %mi_row, align 4, !tbaa !10
  %inc13 = add nsw i32 %39, 1
  store i32 %inc13, i32* %mi_row, align 4, !tbaa !10
  br label %for.cond

for.end14:                                        ; preds = %for.cond.cleanup
  %40 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call15 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %40, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.9, i32 0, i32 0))
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %prefix) #3
  %41 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #3
  %42 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #3
  %43 = bitcast %struct.MB_MODE_INFO*** %mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  %44 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  ret void
}

; Function Attrs: nounwind
define internal void @log_frame_info(%struct.AV1Common* %cm, i8* %str, %struct._IO_FILE* %f) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %str.addr = alloca i8*, align 4
  %f.addr = alloca %struct._IO_FILE*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8* %str, i8** %str.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %f, %struct._IO_FILE** %f.addr, align 4, !tbaa !2
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %f.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %0, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.16, i32 0, i32 0), i8* %1)
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %f.addr, align 4, !tbaa !2
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 0
  %frame_number = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 4
  %4 = load i32, i32* %frame_number, align 4, !tbaa !23
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %show_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 18
  %6 = load i32, i32* %show_frame, align 4, !tbaa !44
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %quant_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 23
  %base_qindex = getelementptr inbounds %struct.CommonQuantParams, %struct.CommonQuantParams* %quant_params, i32 0, i32 0
  %8 = load i32, i32* %base_qindex, align 4, !tbaa !45
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %2, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.17, i32 0, i32 0), i32 %4, i32 %6, i32 %8)
  ret void
}

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare i32 @fclose(%struct._IO_FILE*) #2

; Function Attrs: nounwind
define hidden void @av1_print_uncompressed_frame_header(i8* %data, i32 %size, i8* %filename) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %filename.addr = alloca i8*, align 4
  %hdrFile = alloca %struct._IO_FILE*, align 4
  %zero = alloca i8, align 1
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  store i8* %filename, i8** %filename.addr, align 4, !tbaa !2
  %0 = bitcast %struct._IO_FILE** %hdrFile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @fopen(i8* %1, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.13, i32 0, i32 0))
  store %struct._IO_FILE* %call, %struct._IO_FILE** %hdrFile, align 4, !tbaa !2
  %2 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %3 = load i32, i32* %size.addr, align 4, !tbaa !10
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** %hdrFile, align 4, !tbaa !2
  %call1 = call i32 @fwrite(i8* %2, i32 %3, i32 1, %struct._IO_FILE* %4)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %zero) #3
  store i8 0, i8* %zero, align 1, !tbaa !20
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** %hdrFile, align 4, !tbaa !2
  %call2 = call i32 @fseek(%struct._IO_FILE* %5, i32 1, i32 0)
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** %hdrFile, align 4, !tbaa !2
  %call3 = call i32 @fwrite(i8* %zero, i32 1, i32 1, %struct._IO_FILE* %6)
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** %hdrFile, align 4, !tbaa !2
  %call4 = call i32 @fclose(%struct._IO_FILE* %7)
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %zero) #3
  %8 = bitcast %struct._IO_FILE** %hdrFile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #3
  ret void
}

declare i32 @fwrite(i8*, i32, i32, %struct._IO_FILE*) #2

declare i32 @fseek(%struct._IO_FILE*, i32, i32) #2

; Function Attrs: nounwind
define hidden void @av1_print_frame_contexts(%struct.frame_contexts* %fc, i8* %filename) #0 {
entry:
  %fc.addr = alloca %struct.frame_contexts*, align 4
  %filename.addr = alloca i8*, align 4
  %fcFile = alloca %struct._IO_FILE*, align 4
  %fcp = alloca i16*, align 4
  %n_contexts = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.frame_contexts* %fc, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  store i8* %filename, i8** %filename.addr, align 4, !tbaa !2
  %0 = bitcast %struct._IO_FILE** %fcFile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @fopen(i8* %1, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.13, i32 0, i32 0))
  store %struct._IO_FILE* %call, %struct._IO_FILE** %fcFile, align 4, !tbaa !2
  %2 = bitcast i16** %fcp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %4 = bitcast %struct.frame_contexts* %3 to i16*
  store i16* %4, i16** %fcp, align 4, !tbaa !2
  %5 = bitcast i32* %n_contexts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  store i32 10632, i32* %n_contexts, align 4, !tbaa !10
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !10
  %cmp = icmp ult i32 %7, 10632
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** %fcFile, align 4, !tbaa !2
  %9 = load i16*, i16** %fcp, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %9, i32 1
  store i16* %incdec.ptr, i16** %fcp, align 4, !tbaa !2
  %10 = load i16, i16* %9, align 2, !tbaa !46
  %conv = zext i16 %10 to i32
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.14, i32 0, i32 0), i32 %conv)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** %fcFile, align 4, !tbaa !2
  %call2 = call i32 @fclose(%struct._IO_FILE* %12)
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %n_contexts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i16** %fcp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %16 = bitcast %struct._IO_FILE** %fcFile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 36}
!7 = !{!"CommonModeInfoParams", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !3, i64 20, !8, i64 24, !8, i64 28, !4, i64 32, !3, i64 36, !8, i64 40, !8, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!8 = !{!"int", !4, i64 0}
!9 = !{!7, !8, i64 12}
!10 = !{!8, !8, i64 0}
!11 = !{!7, !8, i64 16}
!12 = !{!13, !4, i64 128}
!13 = !{!"MB_MODE_INFO", !14, i64 0, !15, i64 8, !4, i64 52, !8, i64 60, !4, i64 64, !17, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !18, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!14 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!15 = !{!"", !4, i64 0, !16, i64 32, !16, i64 34, !16, i64 36, !16, i64 38, !4, i64 40, !4, i64 41}
!16 = !{!"short", !4, i64 0}
!17 = !{!"", !4, i64 0, !4, i64 48}
!18 = !{!"", !4, i64 0, !4, i64 1}
!19 = !{!7, !8, i64 44}
!20 = !{!4, !4, i64 0}
!21 = !{!22, !22, i64 0}
!22 = !{!"long", !4, i64 0}
!23 = !{!24, !8, i64 12}
!24 = !{!"AV1Common", !25, i64 0, !27, i64 40, !8, i64 288, !8, i64 292, !8, i64 296, !8, i64 300, !8, i64 304, !8, i64 308, !4, i64 312, !28, i64 313, !4, i64 316, !8, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !29, i64 492, !4, i64 580, !4, i64 1284, !8, i64 1316, !8, i64 1320, !8, i64 1324, !30, i64 1328, !7, i64 1356, !31, i64 1420, !32, i64 10676, !3, i64 10848, !33, i64 10864, !34, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !35, i64 14880, !36, i64 15028, !37, i64 15168, !38, i64 15816, !4, i64 15836, !39, i64 16192, !3, i64 18128, !3, i64 18132, !42, i64 18136, !3, i64 18724, !43, i64 18728, !8, i64 18760, !4, i64 18764, !3, i64 18796, !8, i64 18800, !4, i64 18804, !4, i64 18836, !8, i64 18844, !8, i64 18848, !8, i64 18852, !8, i64 18856}
!25 = !{!"", !4, i64 0, !4, i64 1, !8, i64 4, !8, i64 8, !8, i64 12, !26, i64 16, !8, i64 32, !8, i64 36}
!26 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!27 = !{!"aom_internal_error_info", !4, i64 0, !8, i64 4, !4, i64 8, !8, i64 88, !4, i64 92}
!28 = !{!"_Bool", !4, i64 0}
!29 = !{!"scale_factors", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!30 = !{!"", !28, i64 0, !28, i64 1, !28, i64 2, !28, i64 3, !28, i64 4, !28, i64 5, !28, i64 6, !28, i64 7, !28, i64 8, !28, i64 9, !28, i64 10, !28, i64 11, !4, i64 12, !4, i64 13, !8, i64 16, !8, i64 20, !4, i64 24}
!31 = !{!"CommonQuantParams", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !28, i64 9240, !8, i64 9244, !8, i64 9248, !8, i64 9252}
!32 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !8, i64 164, !4, i64 168}
!33 = !{!"", !4, i64 0, !4, i64 3072}
!34 = !{!"loopfilter", !4, i64 0, !8, i64 8, !8, i64 12, !8, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !8, i64 32}
!35 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !8, i64 52, !4, i64 56, !3, i64 68, !8, i64 72, !3, i64 76, !22, i64 80, !8, i64 84, !22, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !8, i64 128, !8, i64 132, !8, i64 136, !8, i64 140, !3, i64 144}
!36 = !{!"", !8, i64 0, !8, i64 4, !4, i64 8, !4, i64 72, !8, i64 136}
!37 = !{!"", !8, i64 0, !8, i64 4, !4, i64 8, !8, i64 120, !4, i64 124, !8, i64 204, !4, i64 208, !8, i64 288, !8, i64 292, !8, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !8, i64 596, !8, i64 600, !8, i64 604, !8, i64 608, !8, i64 612, !8, i64 616, !8, i64 620, !8, i64 624, !8, i64 628, !8, i64 632, !8, i64 636, !8, i64 640, !16, i64 644}
!38 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16}
!39 = !{!"SequenceHeader", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !4, i64 16, !8, i64 20, !8, i64 24, !4, i64 28, !8, i64 32, !8, i64 36, !26, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !8, i64 112, !4, i64 116, !8, i64 244, !40, i64 248, !4, i64 264, !41, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!40 = !{!"aom_timing", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!41 = !{!"aom_dec_model_info", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!42 = !{!"CommonTileParams", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !4, i64 60, !4, i64 320, !8, i64 580, !8, i64 584}
!43 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !8, i64 20, !8, i64 24, !8, i64 28}
!44 = !{!24, !8, i64 1316}
!45 = !{!24, !8, i64 1420}
!46 = !{!16, !16, i64 0}
