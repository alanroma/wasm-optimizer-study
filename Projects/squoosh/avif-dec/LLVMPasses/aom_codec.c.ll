; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_codec.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_codec.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_codec_iface = type { i8*, i32, i32, i32 (%struct.aom_codec_ctx*)*, i32 (%struct.aom_codec_alg_priv*)*, %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_dec_iface, %struct.aom_codec_enc_iface }
%struct.aom_codec_ctx = type { i8*, %struct.aom_codec_iface*, i32, i8*, i32, %union.anon, %struct.aom_codec_priv* }
%union.anon = type { %struct.aom_codec_dec_cfg* }
%struct.aom_codec_dec_cfg = type { i32, i32, i32, i32 }
%struct.aom_codec_priv = type { i8*, i32, %struct.anon }
%struct.anon = type { %struct.aom_fixed_buf, i32, i32, %struct.aom_codec_cx_pkt }
%struct.aom_fixed_buf = type { i8*, i32 }
%struct.aom_codec_cx_pkt = type { i32, %union.anon.0 }
%union.anon.0 = type { %struct.aom_psnr_pkt, [48 x i8] }
%struct.aom_psnr_pkt = type { [4 x i32], [4 x i64], [4 x double] }
%struct.aom_codec_alg_priv = type opaque
%struct.aom_codec_ctrl_fn_map = type { i32, i32 (%struct.aom_codec_alg_priv*, i8*)* }
%struct.aom_codec_dec_iface = type { i32 (i8*, i32, %struct.aom_codec_stream_info*)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_stream_info*)*, i32 (%struct.aom_codec_alg_priv*, i8*, i32, i8*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*, i8**)*, i32 (%struct.aom_codec_alg_priv*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, i8*)* }
%struct.aom_codec_stream_info = type { i32, i32, i32, i32, i32, i32 }
%struct.aom_image = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.aom_metadata_array*, i8* }
%struct.aom_metadata_array = type opaque
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.aom_codec_enc_iface = type { i32, %struct.aom_codec_enc_cfg*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_image*, i64, i32, i32)*, %struct.aom_codec_cx_pkt* (%struct.aom_codec_alg_priv*, i8**)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_enc_cfg*)*, %struct.aom_fixed_buf* (%struct.aom_codec_alg_priv*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*)* }
%struct.aom_codec_enc_cfg = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_rational, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_fixed_buf, %struct.aom_fixed_buf, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [64 x i32], [64 x i32], i32, [5 x i32], %struct.cfg_options }
%struct.aom_rational = type { i32, i32 }
%struct.cfg_options = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }

@.str = private unnamed_addr constant [7 x i8] c"v2.0.0\00", align 1
@.str.1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@.str.2 = private unnamed_addr constant [20 x i8] c"<invalid interface>\00", align 1
@.str.3 = private unnamed_addr constant [8 x i8] c"Success\00", align 1
@.str.4 = private unnamed_addr constant [27 x i8] c"Unspecified internal error\00", align 1
@.str.5 = private unnamed_addr constant [24 x i8] c"Memory allocation error\00", align 1
@.str.6 = private unnamed_addr constant [21 x i8] c"ABI version mismatch\00", align 1
@.str.7 = private unnamed_addr constant [46 x i8] c"Codec does not implement requested capability\00", align 1
@.str.8 = private unnamed_addr constant [40 x i8] c"Bitstream not supported by this decoder\00", align 1
@.str.9 = private unnamed_addr constant [57 x i8] c"Bitstream required feature not supported by this decoder\00", align 1
@.str.10 = private unnamed_addr constant [23 x i8] c"Corrupt frame detected\00", align 1
@.str.11 = private unnamed_addr constant [18 x i8] c"Invalid parameter\00", align 1
@.str.12 = private unnamed_addr constant [21 x i8] c"End of iterated list\00", align 1
@.str.13 = private unnamed_addr constant [24 x i8] c"Unrecognized error code\00", align 1
@.str.14 = private unnamed_addr constant [20 x i8] c"OBU_SEQUENCE_HEADER\00", align 1
@.str.15 = private unnamed_addr constant [23 x i8] c"OBU_TEMPORAL_DELIMITER\00", align 1
@.str.16 = private unnamed_addr constant [17 x i8] c"OBU_FRAME_HEADER\00", align 1
@.str.17 = private unnamed_addr constant [27 x i8] c"OBU_REDUNDANT_FRAME_HEADER\00", align 1
@.str.18 = private unnamed_addr constant [10 x i8] c"OBU_FRAME\00", align 1
@.str.19 = private unnamed_addr constant [15 x i8] c"OBU_TILE_GROUP\00", align 1
@.str.20 = private unnamed_addr constant [13 x i8] c"OBU_METADATA\00", align 1
@.str.21 = private unnamed_addr constant [14 x i8] c"OBU_TILE_LIST\00", align 1
@.str.22 = private unnamed_addr constant [12 x i8] c"OBU_PADDING\00", align 1
@.str.23 = private unnamed_addr constant [19 x i8] c"<Invalid OBU Type>\00", align 1

; Function Attrs: nounwind
define hidden i32 @aom_codec_version() #0 {
entry:
  ret i32 131072
}

; Function Attrs: nounwind
define hidden i8* @aom_codec_version_str() #0 {
entry:
  ret i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: nounwind
define hidden i8* @aom_codec_version_extra_str() #0 {
entry:
  ret i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.1, i32 0, i32 0)
}

; Function Attrs: nounwind
define hidden i8* @aom_codec_iface_name(%struct.aom_codec_iface* %iface) #0 {
entry:
  %iface.addr = alloca %struct.aom_codec_iface*, align 4
  store %struct.aom_codec_iface* %iface, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_iface* %0, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %name = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %1, i32 0, i32 0
  %2 = load i8*, i8** %name, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %2, %cond.true ], [ getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0), %cond.false ]
  ret i8* %cond
}

; Function Attrs: nounwind
define hidden i8* @aom_codec_err_to_string(i32 %err) #0 {
entry:
  %retval = alloca i8*, align 4
  %err.addr = alloca i32, align 4
  store i32 %err, i32* %err.addr, align 4, !tbaa !12
  %0 = load i32, i32* %err.addr, align 4, !tbaa !12
  switch i32 %0, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
    i32 2, label %sw.bb2
    i32 3, label %sw.bb3
    i32 4, label %sw.bb4
    i32 5, label %sw.bb5
    i32 6, label %sw.bb6
    i32 7, label %sw.bb7
    i32 8, label %sw.bb8
    i32 9, label %sw.bb9
  ]

sw.bb:                                            ; preds = %entry
  store i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.3, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  store i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.4, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.5, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.6, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.7, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  store i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.8, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry
  store i8* getelementptr inbounds ([57 x i8], [57 x i8]* @.str.9, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb7:                                           ; preds = %entry
  store i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.10, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb8:                                           ; preds = %entry
  store i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.11, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb9:                                           ; preds = %entry
  store i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.12, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %entry
  store i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.13, i32 0, i32 0), i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb9, %sw.bb8, %sw.bb7, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i8*, i8** %retval, align 4
  ret i8* %1
}

; Function Attrs: nounwind
define hidden i8* @aom_codec_error(%struct.aom_codec_ctx* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %0, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %1, i32 0, i32 2
  %2 = load i32, i32* %err, align 4, !tbaa !13
  %call = call i8* @aom_codec_err_to_string(i32 %2)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call1 = call i8* @aom_codec_err_to_string(i32 8)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call, %cond.true ], [ %call1, %cond.false ]
  ret i8* %cond
}

; Function Attrs: nounwind
define hidden i8* @aom_codec_error_detail(%struct.aom_codec_ctx* %ctx) #0 {
entry:
  %retval = alloca i8*, align 4
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %1, i32 0, i32 2
  %2 = load i32, i32* %err, align 4, !tbaa !13
  %tobool1 = icmp ne i32 %2, 0
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %3, i32 0, i32 6
  %4 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !15
  %tobool2 = icmp ne %struct.aom_codec_priv* %4, null
  br i1 %tobool2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %5 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv3 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %5, i32 0, i32 6
  %6 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv3, align 4, !tbaa !15
  %err_detail = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %6, i32 0, i32 0
  %7 = load i8*, i8** %err_detail, align 8, !tbaa !16
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %8 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err_detail4 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %8, i32 0, i32 3
  %9 = load i8*, i8** %err_detail4, align 4, !tbaa !21
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %7, %cond.true ], [ %9, %cond.false ]
  store i8* %cond, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %cond.end
  %10 = load i8*, i8** %retval, align 4
  ret i8* %10
}

; Function Attrs: nounwind
define hidden i32 @aom_codec_destroy(%struct.aom_codec_ctx* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %res = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  store i32 8, i32* %res, align 4, !tbaa !12
  br label %if.end9

if.else:                                          ; preds = %entry
  %2 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %2, i32 0, i32 1
  %3 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !22
  %tobool1 = icmp ne %struct.aom_codec_iface* %3, null
  br i1 %tobool1, label %lor.lhs.false, label %if.then3

lor.lhs.false:                                    ; preds = %if.else
  %4 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %4, i32 0, i32 6
  %5 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !15
  %tobool2 = icmp ne %struct.aom_codec_priv* %5, null
  br i1 %tobool2, label %if.else4, label %if.then3

if.then3:                                         ; preds = %lor.lhs.false, %if.else
  store i32 1, i32* %res, align 4, !tbaa !12
  br label %if.end

if.else4:                                         ; preds = %lor.lhs.false
  %6 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface5 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %6, i32 0, i32 1
  %7 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface5, align 4, !tbaa !22
  %destroy = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %7, i32 0, i32 4
  %8 = load i32 (%struct.aom_codec_alg_priv*)*, i32 (%struct.aom_codec_alg_priv*)** %destroy, align 4, !tbaa !23
  %9 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv6 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %9, i32 0, i32 6
  %10 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv6, align 4, !tbaa !15
  %11 = bitcast %struct.aom_codec_priv* %10 to %struct.aom_codec_alg_priv*
  %call = call i32 %8(%struct.aom_codec_alg_priv* %11)
  %12 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface7 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %12, i32 0, i32 1
  store %struct.aom_codec_iface* null, %struct.aom_codec_iface** %iface7, align 4, !tbaa !22
  %13 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %name = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %13, i32 0, i32 0
  store i8* null, i8** %name, align 4, !tbaa !24
  %14 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv8 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %14, i32 0, i32 6
  store %struct.aom_codec_priv* null, %struct.aom_codec_priv** %priv8, align 4, !tbaa !15
  store i32 0, i32* %res, align 4, !tbaa !12
  br label %if.end

if.end:                                           ; preds = %if.else4, %if.then3
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  %15 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool10 = icmp ne %struct.aom_codec_ctx* %15, null
  br i1 %tobool10, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end9
  %16 = load i32, i32* %res, align 4, !tbaa !12
  %17 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %17, i32 0, i32 2
  store i32 %16, i32* %err, align 4, !tbaa !13
  br label %cond.end

cond.false:                                       ; preds = %if.end9
  %18 = load i32, i32* %res, align 4, !tbaa !12
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %16, %cond.true ], [ %18, %cond.false ]
  %19 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #2
  ret i32 %cond
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @aom_codec_get_caps(%struct.aom_codec_iface* %iface) #0 {
entry:
  %iface.addr = alloca %struct.aom_codec_iface*, align 4
  store %struct.aom_codec_iface* %iface, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_iface* %0, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %1, i32 0, i32 2
  %2 = load i32, i32* %caps, align 4, !tbaa !25
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %2, %cond.true ], [ 0, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define hidden i32 @aom_codec_control(%struct.aom_codec_ctx* %ctx, i32 %ctrl_id, ...) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %ctrl_id.addr = alloca i32, align 4
  %res = alloca i32, align 4
  %entry10 = alloca %struct.aom_codec_ctrl_fn_map*, align 4
  %ap = alloca i8*, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store i32 %ctrl_id, i32* %ctrl_id.addr, align 4, !tbaa !26
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load i32, i32* %ctrl_id.addr, align 4, !tbaa !26
  %tobool1 = icmp ne i32 %2, 0
  br i1 %tobool1, label %if.else, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 8, i32* %res, align 4, !tbaa !12
  br label %if.end25

if.else:                                          ; preds = %lor.lhs.false
  %3 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %3, i32 0, i32 1
  %4 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !22
  %tobool2 = icmp ne %struct.aom_codec_iface* %4, null
  br i1 %tobool2, label %lor.lhs.false3, label %if.then8

lor.lhs.false3:                                   ; preds = %if.else
  %5 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %5, i32 0, i32 6
  %6 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !15
  %tobool4 = icmp ne %struct.aom_codec_priv* %6, null
  br i1 %tobool4, label %lor.lhs.false5, label %if.then8

lor.lhs.false5:                                   ; preds = %lor.lhs.false3
  %7 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface6 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %7, i32 0, i32 1
  %8 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface6, align 4, !tbaa !22
  %ctrl_maps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %8, i32 0, i32 5
  %9 = load %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_ctrl_fn_map** %ctrl_maps, align 4, !tbaa !27
  %tobool7 = icmp ne %struct.aom_codec_ctrl_fn_map* %9, null
  br i1 %tobool7, label %if.else9, label %if.then8

if.then8:                                         ; preds = %lor.lhs.false5, %lor.lhs.false3, %if.else
  store i32 1, i32* %res, align 4, !tbaa !12
  br label %if.end24

if.else9:                                         ; preds = %lor.lhs.false5
  %10 = bitcast %struct.aom_codec_ctrl_fn_map** %entry10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  store i32 1, i32* %res, align 4, !tbaa !12
  %11 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface11 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %11, i32 0, i32 1
  %12 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface11, align 4, !tbaa !22
  %ctrl_maps12 = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %12, i32 0, i32 5
  %13 = load %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_ctrl_fn_map** %ctrl_maps12, align 4, !tbaa !27
  store %struct.aom_codec_ctrl_fn_map* %13, %struct.aom_codec_ctrl_fn_map** %entry10, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else9
  %14 = load %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_ctrl_fn_map** %entry10, align 4, !tbaa !2
  %tobool13 = icmp ne %struct.aom_codec_ctrl_fn_map* %14, null
  br i1 %tobool13, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %15 = load %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_ctrl_fn_map** %entry10, align 4, !tbaa !2
  %fn = getelementptr inbounds %struct.aom_codec_ctrl_fn_map, %struct.aom_codec_ctrl_fn_map* %15, i32 0, i32 1
  %16 = load i32 (%struct.aom_codec_alg_priv*, i8*)*, i32 (%struct.aom_codec_alg_priv*, i8*)** %fn, align 4, !tbaa !28
  %tobool14 = icmp ne i32 (%struct.aom_codec_alg_priv*, i8*)* %16, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %17 = phi i1 [ false, %for.cond ], [ %tobool14, %land.rhs ]
  br i1 %17, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %18 = load %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_ctrl_fn_map** %entry10, align 4, !tbaa !2
  %ctrl_id15 = getelementptr inbounds %struct.aom_codec_ctrl_fn_map, %struct.aom_codec_ctrl_fn_map* %18, i32 0, i32 0
  %19 = load i32, i32* %ctrl_id15, align 4, !tbaa !30
  %tobool16 = icmp ne i32 %19, 0
  br i1 %tobool16, label %lor.lhs.false17, label %if.then19

lor.lhs.false17:                                  ; preds = %for.body
  %20 = load %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_ctrl_fn_map** %entry10, align 4, !tbaa !2
  %ctrl_id18 = getelementptr inbounds %struct.aom_codec_ctrl_fn_map, %struct.aom_codec_ctrl_fn_map* %20, i32 0, i32 0
  %21 = load i32, i32* %ctrl_id18, align 4, !tbaa !30
  %22 = load i32, i32* %ctrl_id.addr, align 4, !tbaa !26
  %cmp = icmp eq i32 %21, %22
  br i1 %cmp, label %if.then19, label %if.end

if.then19:                                        ; preds = %lor.lhs.false17, %for.body
  %23 = bitcast i8** %ap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #2
  %ap20 = bitcast i8** %ap to i8*
  call void @llvm.va_start(i8* %ap20)
  %24 = load %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_ctrl_fn_map** %entry10, align 4, !tbaa !2
  %fn21 = getelementptr inbounds %struct.aom_codec_ctrl_fn_map, %struct.aom_codec_ctrl_fn_map* %24, i32 0, i32 1
  %25 = load i32 (%struct.aom_codec_alg_priv*, i8*)*, i32 (%struct.aom_codec_alg_priv*, i8*)** %fn21, align 4, !tbaa !28
  %26 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv22 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %26, i32 0, i32 6
  %27 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv22, align 4, !tbaa !15
  %28 = bitcast %struct.aom_codec_priv* %27 to %struct.aom_codec_alg_priv*
  %29 = load i8*, i8** %ap, align 4, !tbaa !2
  %call = call i32 %25(%struct.aom_codec_alg_priv* %28, i8* %29)
  store i32 %call, i32* %res, align 4, !tbaa !12
  %ap23 = bitcast i8** %ap to i8*
  call void @llvm.va_end(i8* %ap23)
  %30 = bitcast i8** %ap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #2
  br label %for.end

if.end:                                           ; preds = %lor.lhs.false17
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %31 = load %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_ctrl_fn_map** %entry10, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.aom_codec_ctrl_fn_map, %struct.aom_codec_ctrl_fn_map* %31, i32 1
  store %struct.aom_codec_ctrl_fn_map* %incdec.ptr, %struct.aom_codec_ctrl_fn_map** %entry10, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %if.then19, %land.end
  %32 = bitcast %struct.aom_codec_ctrl_fn_map** %entry10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #2
  br label %if.end24

if.end24:                                         ; preds = %for.end, %if.then8
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.then
  %33 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool26 = icmp ne %struct.aom_codec_ctx* %33, null
  br i1 %tobool26, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end25
  %34 = load i32, i32* %res, align 4, !tbaa !12
  %35 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %35, i32 0, i32 2
  store i32 %34, i32* %err, align 4, !tbaa !13
  br label %cond.end

cond.false:                                       ; preds = %if.end25
  %36 = load i32, i32* %res, align 4, !tbaa !12
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %34, %cond.true ], [ %36, %cond.false ]
  %37 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #2
  ret i32 %cond
}

; Function Attrs: nounwind
declare void @llvm.va_start(i8*) #2

; Function Attrs: nounwind
declare void @llvm.va_end(i8*) #2

; Function Attrs: nounwind
define hidden void @aom_internal_error(%struct.aom_internal_error_info* %info, i32 %error, i8* %fmt, ...) #0 {
entry:
  %info.addr = alloca %struct.aom_internal_error_info*, align 4
  %error.addr = alloca i32, align 4
  %fmt.addr = alloca i8*, align 4
  %ap = alloca i8*, align 4
  %sz = alloca i32, align 4
  store %struct.aom_internal_error_info* %info, %struct.aom_internal_error_info** %info.addr, align 4, !tbaa !2
  store i32 %error, i32* %error.addr, align 4, !tbaa !12
  store i8* %fmt, i8** %fmt.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load i32, i32* %error.addr, align 4, !tbaa !12
  %2 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %info.addr, align 4, !tbaa !2
  %error_code = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %2, i32 0, i32 0
  store i32 %1, i32* %error_code, align 4, !tbaa !31
  %3 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %info.addr, align 4, !tbaa !2
  %has_detail = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %3, i32 0, i32 1
  store i32 0, i32* %has_detail, align 4, !tbaa !33
  %4 = load i8*, i8** %fmt.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %4, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  store i32 80, i32* %sz, align 4, !tbaa !34
  %6 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %info.addr, align 4, !tbaa !2
  %has_detail1 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %6, i32 0, i32 1
  store i32 1, i32* %has_detail1, align 4, !tbaa !33
  %ap2 = bitcast i8** %ap to i8*
  call void @llvm.va_start(i8* %ap2)
  %7 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %info.addr, align 4, !tbaa !2
  %detail = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %7, i32 0, i32 2
  %arraydecay = getelementptr inbounds [80 x i8], [80 x i8]* %detail, i32 0, i32 0
  %8 = load i32, i32* %sz, align 4, !tbaa !34
  %sub = sub i32 %8, 1
  %9 = load i8*, i8** %fmt.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %ap, align 4, !tbaa !2
  %call = call i32 @vsnprintf(i8* %arraydecay, i32 %sub, i8* %9, i8* %10)
  %ap3 = bitcast i8** %ap to i8*
  call void @llvm.va_end(i8* %ap3)
  %11 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %info.addr, align 4, !tbaa !2
  %detail4 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %11, i32 0, i32 2
  %12 = load i32, i32* %sz, align 4, !tbaa !34
  %sub5 = sub i32 %12, 1
  %arrayidx = getelementptr inbounds [80 x i8], [80 x i8]* %detail4, i32 0, i32 %sub5
  store i8 0, i8* %arrayidx, align 1, !tbaa !12
  %13 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %info.addr, align 4, !tbaa !2
  %setjmp = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %14, i32 0, i32 3
  %15 = load i32, i32* %setjmp, align 4, !tbaa !35
  %tobool6 = icmp ne i32 %15, 0
  br i1 %tobool6, label %if.then7, label %if.end10

if.then7:                                         ; preds = %if.end
  %16 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %info.addr, align 4, !tbaa !2
  %jmp = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %16, i32 0, i32 4
  %arraydecay8 = getelementptr inbounds [1 x %struct.__jmp_buf_tag], [1 x %struct.__jmp_buf_tag]* %jmp, i32 0, i32 0
  %17 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %info.addr, align 4, !tbaa !2
  %error_code9 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %17, i32 0, i32 0
  %18 = load i32, i32* %error_code9, align 4, !tbaa !31
  call void @longjmp(%struct.__jmp_buf_tag* %arraydecay8, i32 %18) #5
  unreachable

if.end10:                                         ; preds = %if.end
  %19 = bitcast i8** %ap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #2
  ret void
}

declare i32 @vsnprintf(i8*, i32, i8*, i8*) #3

; Function Attrs: noreturn
declare void @longjmp(%struct.__jmp_buf_tag*, i32) #4

; Function Attrs: nounwind
define hidden void @aom_merge_corrupted_flag(i32* %corrupted, i32 %value) #0 {
entry:
  %corrupted.addr = alloca i32*, align 4
  %value.addr = alloca i32, align 4
  store i32* %corrupted, i32** %corrupted.addr, align 4, !tbaa !2
  store i32 %value, i32* %value.addr, align 4, !tbaa !26
  %0 = load i32, i32* %value.addr, align 4, !tbaa !26
  %1 = load i32*, i32** %corrupted.addr, align 4, !tbaa !2
  %2 = load i32, i32* %1, align 4, !tbaa !26
  %or = or i32 %2, %0
  store i32 %or, i32* %1, align 4, !tbaa !26
  ret void
}

; Function Attrs: nounwind
define hidden i8* @aom_obu_type_to_string(i8 zeroext %type) #0 {
entry:
  %retval = alloca i8*, align 4
  %type.addr = alloca i8, align 1
  store i8 %type, i8* %type.addr, align 1, !tbaa !12
  %0 = load i8, i8* %type.addr, align 1, !tbaa !12
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
    i32 3, label %sw.bb2
    i32 7, label %sw.bb3
    i32 6, label %sw.bb4
    i32 4, label %sw.bb5
    i32 5, label %sw.bb6
    i32 8, label %sw.bb7
    i32 15, label %sw.bb8
  ]

sw.bb:                                            ; preds = %entry
  store i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.14, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  store i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.15, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.16, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.17, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.18, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  store i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.19, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry
  store i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.20, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb7:                                           ; preds = %entry
  store i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.21, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb8:                                           ; preds = %entry
  store i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.22, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  store i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.23, i32 0, i32 0), i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb8, %sw.bb7, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i8*, i8** %retval, align 4
  ret i8* %1
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 0}
!7 = !{!"aom_codec_iface", !3, i64 0, !8, i64 4, !9, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !10, i64 24, !11, i64 44}
!8 = !{!"int", !4, i64 0}
!9 = !{!"long", !4, i64 0}
!10 = !{!"aom_codec_dec_iface", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!11 = !{!"aom_codec_enc_iface", !8, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!12 = !{!4, !4, i64 0}
!13 = !{!14, !4, i64 8}
!14 = !{!"aom_codec_ctx", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 12, !9, i64 16, !4, i64 20, !3, i64 24}
!15 = !{!14, !3, i64 24}
!16 = !{!17, !3, i64 0}
!17 = !{!"aom_codec_priv", !3, i64 0, !9, i64 4, !18, i64 8}
!18 = !{!"", !19, i64 0, !8, i64 8, !8, i64 12, !20, i64 16}
!19 = !{!"aom_fixed_buf", !3, i64 0, !9, i64 4}
!20 = !{!"aom_codec_cx_pkt", !4, i64 0, !4, i64 8}
!21 = !{!14, !3, i64 12}
!22 = !{!14, !3, i64 4}
!23 = !{!7, !3, i64 16}
!24 = !{!14, !3, i64 0}
!25 = !{!7, !9, i64 8}
!26 = !{!8, !8, i64 0}
!27 = !{!7, !3, i64 20}
!28 = !{!29, !3, i64 4}
!29 = !{!"aom_codec_ctrl_fn_map", !8, i64 0, !3, i64 4}
!30 = !{!29, !8, i64 0}
!31 = !{!32, !4, i64 0}
!32 = !{!"aom_internal_error_info", !4, i64 0, !8, i64 4, !4, i64 8, !8, i64 88, !4, i64 92}
!33 = !{!32, !8, i64 4}
!34 = !{!9, !9, i64 0}
!35 = !{!32, !8, i64 88}
