; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/av1_loopfilter.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/av1_loopfilter.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.AV1_DEBLOCKING_PARAMETERS = type { i32, i8*, i8*, i8* }

@delta_lf_id_lut = internal constant [3 x [2 x i32]] [[2 x i32] [i32 0, i32 1], [2 x i32] [i32 2, i32 2], [2 x i32] [i32 3, i32 3]], align 16
@seg_lvl_lf_lut = internal constant [3 x [2 x i8]] [[2 x i8] c"\01\02", [2 x i8] c"\03\03", [2 x i8] c"\04\04"], align 1
@mode_lf_lut = internal constant [25 x i32] [i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 1, i32 1, i32 0, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 0, i32 1], align 16
@tx_size_wide_unit = internal constant [19 x i32] [i32 1, i32 2, i32 4, i32 8, i32 16, i32 1, i32 2, i32 2, i32 4, i32 4, i32 8, i32 8, i32 16, i32 1, i32 4, i32 2, i32 8, i32 4, i32 16], align 16
@tx_size_high_unit = internal constant [19 x i32] [i32 1, i32 2, i32 4, i32 8, i32 16, i32 2, i32 1, i32 4, i32 2, i32 8, i32 4, i32 16, i32 8, i32 4, i32 1, i32 8, i32 2, i32 16, i32 4], align 16
@tx_size_wide = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 4, i32 8, i32 8, i32 16, i32 16, i32 32, i32 32, i32 64, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64], align 16
@tx_size_high = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 8, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64, i32 32, i32 16, i32 4, i32 32, i32 8, i32 64, i32 16], align 16
@block_size_wide = internal constant [22 x i8] c"\04\04\08\08\08\10\10\10   @@@\80\80\04\10\08 \10@", align 16
@block_size_high = internal constant [22 x i8] c"\04\08\04\08\10\08\10 \10 @ @\80@\80\10\04 \08@\10", align 16
@mi_size_high = internal constant [22 x i8] c"\01\02\01\02\04\02\04\08\04\08\10\08\10 \10 \04\01\08\02\10\04", align 16
@mi_size_wide = internal constant [22 x i8] c"\01\01\02\02\02\04\04\04\08\08\08\10\10\10  \01\04\02\08\04\10", align 16
@txsize_horz_map = internal constant [19 x i8] c"\00\01\02\03\04\00\01\01\02\02\03\03\04\00\02\01\03\02\04", align 16
@txsize_vert_map = internal constant [19 x i8] c"\00\01\02\03\04\01\00\02\01\03\02\04\03\02\00\03\01\04\02", align 16
@max_txsize_rect_lookup = internal constant [22 x i8] c"\00\05\06\01\07\08\02\09\0A\03\0B\0C\04\04\04\04\0D\0E\0F\10\11\12", align 16
@av1_get_txb_size_index.tw_w_log2_table = internal constant [22 x i8] c"\00\00\00\00\01\01\01\02\02\02\03\03\03\03\03\03\00\01\01\02\02\03", align 16
@av1_get_txb_size_index.tw_h_log2_table = internal constant [22 x i8] c"\00\00\00\00\01\01\01\02\02\02\03\03\03\03\03\03\01\00\02\01\03\02", align 16
@av1_get_txb_size_index.stride_log2_table = internal constant [22 x i8] c"\00\00\01\01\00\01\01\00\01\01\00\01\01\01\02\02\00\01\00\01\00\01", align 16
@ss_size_lookup = internal constant [22 x [2 x [2 x i8]]] [[2 x [2 x i8]] zeroinitializer, [2 x [2 x i8]] [[2 x i8] c"\01\00", [2 x i8] c"\FF\00"], [2 x [2 x i8]] [[2 x i8] c"\02\FF", [2 x i8] zeroinitializer], [2 x [2 x i8]] [[2 x i8] c"\03\02", [2 x i8] c"\01\00"], [2 x [2 x i8]] [[2 x i8] c"\04\03", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\05\FF", [2 x i8] c"\03\02"], [2 x [2 x i8]] [[2 x i8] c"\06\05", [2 x i8] c"\04\03"], [2 x [2 x i8]] [[2 x i8] c"\07\06", [2 x i8] c"\FF\04"], [2 x [2 x i8]] [[2 x i8] c"\08\FF", [2 x i8] c"\06\05"], [2 x [2 x i8]] [[2 x i8] c"\09\08", [2 x i8] c"\07\06"], [2 x [2 x i8]] [[2 x i8] c"\0A\09", [2 x i8] c"\FF\07"], [2 x [2 x i8]] [[2 x i8] c"\0B\FF", [2 x i8] c"\09\08"], [2 x [2 x i8]] [[2 x i8] c"\0C\0B", [2 x i8] c"\0A\09"], [2 x [2 x i8]] [[2 x i8] c"\0D\0C", [2 x i8] c"\FF\0A"], [2 x [2 x i8]] [[2 x i8] c"\0E\FF", [2 x i8] c"\0C\0B"], [2 x [2 x i8]] [[2 x i8] c"\0F\0E", [2 x i8] c"\0D\0C"], [2 x [2 x i8]] [[2 x i8] c"\10\01", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\11\FF", [2 x i8] c"\02\02"], [2 x [2 x i8]] [[2 x i8] c"\12\04", [2 x i8] c"\FF\10"], [2 x [2 x i8]] [[2 x i8] c"\13\FF", [2 x i8] c"\05\11"], [2 x [2 x i8]] [[2 x i8] c"\14\07", [2 x i8] c"\FF\12"], [2 x [2 x i8]] [[2 x i8] c"\15\FF", [2 x i8] c"\08\13"]], align 16

; Function Attrs: nounwind
define hidden zeroext i8 @av1_get_filter_level(%struct.AV1Common* %cm, %struct.loop_filter_info_n* %lfi_n, i32 %dir_idx, i32 %plane, %struct.MB_MODE_INFO* %mbmi) #0 {
entry:
  %retval = alloca i8, align 1
  %cm.addr = alloca %struct.AV1Common*, align 4
  %lfi_n.addr = alloca %struct.loop_filter_info_n*, align 4
  %dir_idx.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %segment_id = alloca i32, align 4
  %delta_lf = alloca i8, align 1
  %delta_lf_idx = alloca i32, align 4
  %base_level = alloca i32, align 4
  %lvl_seg = alloca i32, align 4
  %seg_lf_feature_id = alloca i32, align 4
  %data = alloca i32, align 4
  %scale = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.loop_filter_info_n* %lfi_n, %struct.loop_filter_info_n** %lfi_n.addr, align 4, !tbaa !2
  store i32 %dir_idx, i32* %dir_idx.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %segment_id1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id1, align 4
  %bf.lshr = lshr i8 %bf.load, 1
  %bf.clear = and i8 %bf.lshr, 7
  %conv = zext i8 %bf.clear to i32
  store i32 %conv, i32* %segment_id, align 4, !tbaa !6
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %delta_q_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 35
  %delta_lf_present_flag = getelementptr inbounds %struct.DeltaQInfo, %struct.DeltaQInfo* %delta_q_info, i32 0, i32 2
  %3 = load i32, i32* %delta_lf_present_flag, align 8, !tbaa !8
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.else59

if.then:                                          ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %delta_lf) #5
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %delta_q_info2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 35
  %delta_lf_multi = getelementptr inbounds %struct.DeltaQInfo, %struct.DeltaQInfo* %delta_q_info2, i32 0, i32 4
  %5 = load i32, i32* %delta_lf_multi, align 8, !tbaa !32
  %tobool3 = icmp ne i32 %5, 0
  br i1 %tobool3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then
  %6 = bitcast i32* %delta_lf_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* @delta_lf_id_lut, i32 0, i32 %7
  %8 = load i32, i32* %dir_idx.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 %8
  %9 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  store i32 %9, i32* %delta_lf_idx, align 4, !tbaa !6
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %delta_lf6 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %10, i32 0, i32 18
  %11 = load i32, i32* %delta_lf_idx, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [4 x i8], [4 x i8]* %delta_lf6, i32 0, i32 %11
  %12 = load i8, i8* %arrayidx7, align 1, !tbaa !33
  store i8 %12, i8* %delta_lf, align 1, !tbaa !33
  %13 = bitcast i32* %delta_lf_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  br label %if.end

if.else:                                          ; preds = %if.then
  %14 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %delta_lf_from_base = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %14, i32 0, i32 17
  %15 = load i8, i8* %delta_lf_from_base, align 2, !tbaa !34
  store i8 %15, i8* %delta_lf, align 1, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then4
  %16 = bitcast i32* %base_level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %17, 0
  br i1 %cmp, label %if.then9, label %if.else11

if.then9:                                         ; preds = %if.end
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 28
  %filter_level = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf, i32 0, i32 0
  %19 = load i32, i32* %dir_idx.addr, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [2 x i32], [2 x i32]* %filter_level, i32 0, i32 %19
  %20 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  store i32 %20, i32* %base_level, align 4, !tbaa !6
  br label %if.end19

if.else11:                                        ; preds = %if.end
  %21 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp12 = icmp eq i32 %21, 1
  br i1 %cmp12, label %if.then14, label %if.else16

if.then14:                                        ; preds = %if.else11
  %22 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf15 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %22, i32 0, i32 28
  %filter_level_u = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf15, i32 0, i32 1
  %23 = load i32, i32* %filter_level_u, align 8, !tbaa !40
  store i32 %23, i32* %base_level, align 4, !tbaa !6
  br label %if.end18

if.else16:                                        ; preds = %if.else11
  %24 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf17 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %24, i32 0, i32 28
  %filter_level_v = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf17, i32 0, i32 2
  %25 = load i32, i32* %filter_level_v, align 4, !tbaa !41
  store i32 %25, i32* %base_level, align 4, !tbaa !6
  br label %if.end18

if.end18:                                         ; preds = %if.else16, %if.then14
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.then9
  %26 = bitcast i32* %lvl_seg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #5
  %27 = load i8, i8* %delta_lf, align 1, !tbaa !33
  %conv20 = sext i8 %27 to i32
  %28 = load i32, i32* %base_level, align 4, !tbaa !6
  %add = add nsw i32 %conv20, %28
  %call = call i32 @clamp(i32 %add, i32 0, i32 63)
  store i32 %call, i32* %lvl_seg, align 4, !tbaa !6
  %29 = bitcast i32* %seg_lf_feature_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #5
  %30 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [3 x [2 x i8]], [3 x [2 x i8]]* @seg_lvl_lf_lut, i32 0, i32 %30
  %31 = load i32, i32* %dir_idx.addr, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx21, i32 0, i32 %31
  %32 = load i8, i8* %arrayidx22, align 1, !tbaa !33
  %conv23 = zext i8 %32 to i32
  store i32 %conv23, i32* %seg_lf_feature_id, align 4, !tbaa !6
  %33 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %33, i32 0, i32 24
  %34 = load i32, i32* %segment_id, align 4, !tbaa !6
  %35 = load i32, i32* %seg_lf_feature_id, align 4, !tbaa !6
  %conv24 = trunc i32 %35 to i8
  %call25 = call i32 @segfeature_active(%struct.segmentation* %seg, i32 %34, i8 zeroext %conv24)
  %tobool26 = icmp ne i32 %call25, 0
  br i1 %tobool26, label %if.then27, label %if.end33

if.then27:                                        ; preds = %if.end19
  %36 = bitcast i32* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #5
  %37 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seg28 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %37, i32 0, i32 24
  %38 = load i32, i32* %segment_id, align 4, !tbaa !6
  %39 = load i32, i32* %seg_lf_feature_id, align 4, !tbaa !6
  %conv29 = trunc i32 %39 to i8
  %call30 = call i32 @get_segdata(%struct.segmentation* %seg28, i32 %38, i8 zeroext %conv29)
  store i32 %call30, i32* %data, align 4, !tbaa !6
  %40 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %41 = load i32, i32* %data, align 4, !tbaa !6
  %add31 = add nsw i32 %40, %41
  %call32 = call i32 @clamp(i32 %add31, i32 0, i32 63)
  store i32 %call32, i32* %lvl_seg, align 4, !tbaa !6
  %42 = bitcast i32* %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #5
  br label %if.end33

if.end33:                                         ; preds = %if.then27, %if.end19
  %43 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf34 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %43, i32 0, i32 28
  %mode_ref_delta_enabled = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf34, i32 0, i32 4
  %44 = load i8, i8* %mode_ref_delta_enabled, align 4, !tbaa !42
  %tobool35 = icmp ne i8 %44, 0
  br i1 %tobool35, label %if.then36, label %if.end57

if.then36:                                        ; preds = %if.end33
  %45 = bitcast i32* %scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #5
  %46 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %shr = ashr i32 %46, 5
  %shl = shl i32 1, %shr
  store i32 %shl, i32* %scale, align 4, !tbaa !6
  %47 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf37 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %47, i32 0, i32 28
  %ref_deltas = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf37, i32 0, i32 6
  %48 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %48, i32 0, i32 12
  %arrayidx38 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %49 = load i8, i8* %arrayidx38, align 4, !tbaa !33
  %idxprom = sext i8 %49 to i32
  %arrayidx39 = getelementptr inbounds [8 x i8], [8 x i8]* %ref_deltas, i32 0, i32 %idxprom
  %50 = load i8, i8* %arrayidx39, align 1, !tbaa !33
  %conv40 = sext i8 %50 to i32
  %51 = load i32, i32* %scale, align 4, !tbaa !6
  %mul = mul nsw i32 %conv40, %51
  %52 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %add41 = add nsw i32 %52, %mul
  store i32 %add41, i32* %lvl_seg, align 4, !tbaa !6
  %53 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame42 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %53, i32 0, i32 12
  %arrayidx43 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame42, i32 0, i32 0
  %54 = load i8, i8* %arrayidx43, align 4, !tbaa !33
  %conv44 = sext i8 %54 to i32
  %cmp45 = icmp sgt i32 %conv44, 0
  br i1 %cmp45, label %if.then47, label %if.end55

if.then47:                                        ; preds = %if.then36
  %55 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf48 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %55, i32 0, i32 28
  %mode_deltas = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf48, i32 0, i32 7
  %56 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %56, i32 0, i32 7
  %57 = load i8, i8* %mode, align 1, !tbaa !43
  %idxprom49 = zext i8 %57 to i32
  %arrayidx50 = getelementptr inbounds [25 x i32], [25 x i32]* @mode_lf_lut, i32 0, i32 %idxprom49
  %58 = load i32, i32* %arrayidx50, align 4, !tbaa !6
  %arrayidx51 = getelementptr inbounds [2 x i8], [2 x i8]* %mode_deltas, i32 0, i32 %58
  %59 = load i8, i8* %arrayidx51, align 1, !tbaa !33
  %conv52 = sext i8 %59 to i32
  %60 = load i32, i32* %scale, align 4, !tbaa !6
  %mul53 = mul nsw i32 %conv52, %60
  %61 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %add54 = add nsw i32 %61, %mul53
  store i32 %add54, i32* %lvl_seg, align 4, !tbaa !6
  br label %if.end55

if.end55:                                         ; preds = %if.then47, %if.then36
  %62 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %call56 = call i32 @clamp(i32 %62, i32 0, i32 63)
  store i32 %call56, i32* %lvl_seg, align 4, !tbaa !6
  %63 = bitcast i32* %scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  br label %if.end57

if.end57:                                         ; preds = %if.end55, %if.end33
  %64 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %conv58 = trunc i32 %64 to i8
  store i8 %conv58, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %65 = bitcast i32* %seg_lf_feature_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %66 = bitcast i32* %lvl_seg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = bitcast i32* %base_level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %delta_lf) #5
  br label %cleanup

if.else59:                                        ; preds = %entry
  %68 = load %struct.loop_filter_info_n*, %struct.loop_filter_info_n** %lfi_n.addr, align 4, !tbaa !2
  %lvl = getelementptr inbounds %struct.loop_filter_info_n, %struct.loop_filter_info_n* %68, i32 0, i32 1
  %69 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [3 x [8 x [2 x [8 x [2 x i8]]]]], [3 x [8 x [2 x [8 x [2 x i8]]]]]* %lvl, i32 0, i32 %69
  %70 = load i32, i32* %segment_id, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds [8 x [2 x [8 x [2 x i8]]]], [8 x [2 x [8 x [2 x i8]]]]* %arrayidx60, i32 0, i32 %70
  %71 = load i32, i32* %dir_idx.addr, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds [2 x [8 x [2 x i8]]], [2 x [8 x [2 x i8]]]* %arrayidx61, i32 0, i32 %71
  %72 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame63 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %72, i32 0, i32 12
  %arrayidx64 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame63, i32 0, i32 0
  %73 = load i8, i8* %arrayidx64, align 4, !tbaa !33
  %idxprom65 = sext i8 %73 to i32
  %arrayidx66 = getelementptr inbounds [8 x [2 x i8]], [8 x [2 x i8]]* %arrayidx62, i32 0, i32 %idxprom65
  %74 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %mode67 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %74, i32 0, i32 7
  %75 = load i8, i8* %mode67, align 1, !tbaa !43
  %idxprom68 = zext i8 %75 to i32
  %arrayidx69 = getelementptr inbounds [25 x i32], [25 x i32]* @mode_lf_lut, i32 0, i32 %idxprom68
  %76 = load i32, i32* %arrayidx69, align 4, !tbaa !6
  %arrayidx70 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx66, i32 0, i32 %76
  %77 = load i8, i8* %arrayidx70, align 1, !tbaa !33
  store i8 %77, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else59, %if.end57
  %78 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  %79 = load i8, i8* %retval, align 1
  ret i8 %79
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal i32 @segfeature_active(%struct.segmentation* %seg, i32 %segment_id, i8 zeroext %feature_id) #2 {
entry:
  %seg.addr = alloca %struct.segmentation*, align 4
  %segment_id.addr = alloca i32, align 4
  %feature_id.addr = alloca i8, align 1
  store %struct.segmentation* %seg, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !6
  store i8 %feature_id, i8* %feature_id.addr, align 1, !tbaa !33
  %0 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %enabled = getelementptr inbounds %struct.segmentation, %struct.segmentation* %0, i32 0, i32 0
  %1 = load i8, i8* %enabled, align 4, !tbaa !44
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %feature_mask = getelementptr inbounds %struct.segmentation, %struct.segmentation* %2, i32 0, i32 5
  %3 = load i32, i32* %segment_id.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %feature_mask, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %5 = load i8, i8* %feature_id.addr, align 1, !tbaa !33
  %conv1 = zext i8 %5 to i32
  %shl = shl i32 1, %conv1
  %and = and i32 %4, %shl
  %tobool2 = icmp ne i32 %and, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %tobool2, %land.rhs ]
  %land.ext = zext i1 %6 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_segdata(%struct.segmentation* %seg, i32 %segment_id, i8 zeroext %feature_id) #2 {
entry:
  %seg.addr = alloca %struct.segmentation*, align 4
  %segment_id.addr = alloca i32, align 4
  %feature_id.addr = alloca i8, align 1
  store %struct.segmentation* %seg, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !6
  store i8 %feature_id, i8* %feature_id.addr, align 1, !tbaa !33
  %0 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %feature_data = getelementptr inbounds %struct.segmentation, %struct.segmentation* %0, i32 0, i32 4
  %1 = load i32, i32* %segment_id.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x [8 x i16]], [8 x [8 x i16]]* %feature_data, i32 0, i32 %1
  %2 = load i8, i8* %feature_id.addr, align 1, !tbaa !33
  %idxprom = zext i8 %2 to i32
  %arrayidx1 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx, i32 0, i32 %idxprom
  %3 = load i16, i16* %arrayidx1, align 2, !tbaa !45
  %conv = sext i16 %3 to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define hidden void @av1_loop_filter_init(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %lfi = alloca %struct.loop_filter_info_n*, align 4
  %lf = alloca %struct.loopfilter*, align 4
  %lvl = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = bitcast %struct.loop_filter_info_n** %lfi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 27
  store %struct.loop_filter_info_n* %lf_info, %struct.loop_filter_info_n** %lfi, align 4, !tbaa !2
  %2 = bitcast %struct.loopfilter** %lf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 28
  store %struct.loopfilter* %lf1, %struct.loopfilter** %lf, align 4, !tbaa !2
  %4 = bitcast i32* %lvl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.loopfilter*, %struct.loopfilter** %lf, align 4, !tbaa !2
  %combine_vert_horz_lf = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %5, i32 0, i32 8
  store i32 1, i32* %combine_vert_horz_lf, align 4, !tbaa !46
  %6 = load %struct.loop_filter_info_n*, %struct.loop_filter_info_n** %lfi, align 4, !tbaa !2
  %7 = load %struct.loopfilter*, %struct.loopfilter** %lf, align 4, !tbaa !2
  %sharpness_level = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %7, i32 0, i32 3
  %8 = load i32, i32* %sharpness_level, align 4, !tbaa !47
  call void @update_sharpness(%struct.loop_filter_info_n* %6, i32 %8)
  store i32 0, i32* %lvl, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %lvl, align 4, !tbaa !6
  %cmp = icmp sle i32 %9, 63
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load %struct.loop_filter_info_n*, %struct.loop_filter_info_n** %lfi, align 4, !tbaa !2
  %lfthr = getelementptr inbounds %struct.loop_filter_info_n, %struct.loop_filter_info_n* %10, i32 0, i32 0
  %11 = load i32, i32* %lvl, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [64 x %struct.loop_filter_thresh], [64 x %struct.loop_filter_thresh]* %lfthr, i32 0, i32 %11
  %hev_thr = getelementptr inbounds %struct.loop_filter_thresh, %struct.loop_filter_thresh* %arrayidx, i32 0, i32 2
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %hev_thr, i32 0, i32 0
  %12 = load i32, i32* %lvl, align 4, !tbaa !6
  %shr = ashr i32 %12, 4
  %13 = trunc i32 %shr to i8
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay, i8 %13, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %lvl, align 4, !tbaa !6
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %lvl, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = bitcast i32* %lvl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %16 = bitcast %struct.loopfilter** %lf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast %struct.loop_filter_info_n** %lfi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  ret void
}

; Function Attrs: nounwind
define internal void @update_sharpness(%struct.loop_filter_info_n* %lfi, i32 %sharpness_lvl) #0 {
entry:
  %lfi.addr = alloca %struct.loop_filter_info_n*, align 4
  %sharpness_lvl.addr = alloca i32, align 4
  %lvl = alloca i32, align 4
  %block_inside_limit = alloca i32, align 4
  store %struct.loop_filter_info_n* %lfi, %struct.loop_filter_info_n** %lfi.addr, align 4, !tbaa !2
  store i32 %sharpness_lvl, i32* %sharpness_lvl.addr, align 4, !tbaa !6
  %0 = bitcast i32* %lvl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %lvl, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %lvl, align 4, !tbaa !6
  %cmp = icmp sle i32 %1, 63
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast i32* %block_inside_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %lvl, align 4, !tbaa !6
  %4 = load i32, i32* %sharpness_lvl.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %4, 0
  %conv = zext i1 %cmp1 to i32
  %5 = load i32, i32* %sharpness_lvl.addr, align 4, !tbaa !6
  %cmp2 = icmp sgt i32 %5, 4
  %conv3 = zext i1 %cmp2 to i32
  %add = add nsw i32 %conv, %conv3
  %shr = ashr i32 %3, %add
  store i32 %shr, i32* %block_inside_limit, align 4, !tbaa !6
  %6 = load i32, i32* %sharpness_lvl.addr, align 4, !tbaa !6
  %cmp4 = icmp sgt i32 %6, 0
  br i1 %cmp4, label %if.then, label %if.end10

if.then:                                          ; preds = %for.body
  %7 = load i32, i32* %block_inside_limit, align 4, !tbaa !6
  %8 = load i32, i32* %sharpness_lvl.addr, align 4, !tbaa !6
  %sub = sub nsw i32 9, %8
  %cmp6 = icmp sgt i32 %7, %sub
  br i1 %cmp6, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.then
  %9 = load i32, i32* %sharpness_lvl.addr, align 4, !tbaa !6
  %sub9 = sub nsw i32 9, %9
  store i32 %sub9, i32* %block_inside_limit, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then8, %if.then
  br label %if.end10

if.end10:                                         ; preds = %if.end, %for.body
  %10 = load i32, i32* %block_inside_limit, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %10, 1
  br i1 %cmp11, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.end10
  store i32 1, i32* %block_inside_limit, align 4, !tbaa !6
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.end10
  %11 = load %struct.loop_filter_info_n*, %struct.loop_filter_info_n** %lfi.addr, align 4, !tbaa !2
  %lfthr = getelementptr inbounds %struct.loop_filter_info_n, %struct.loop_filter_info_n* %11, i32 0, i32 0
  %12 = load i32, i32* %lvl, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [64 x %struct.loop_filter_thresh], [64 x %struct.loop_filter_thresh]* %lfthr, i32 0, i32 %12
  %lim = getelementptr inbounds %struct.loop_filter_thresh, %struct.loop_filter_thresh* %arrayidx, i32 0, i32 1
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %lim, i32 0, i32 0
  %13 = load i32, i32* %block_inside_limit, align 4, !tbaa !6
  %14 = trunc i32 %13 to i8
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay, i8 %14, i32 16, i1 false)
  %15 = load %struct.loop_filter_info_n*, %struct.loop_filter_info_n** %lfi.addr, align 4, !tbaa !2
  %lfthr15 = getelementptr inbounds %struct.loop_filter_info_n, %struct.loop_filter_info_n* %15, i32 0, i32 0
  %16 = load i32, i32* %lvl, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [64 x %struct.loop_filter_thresh], [64 x %struct.loop_filter_thresh]* %lfthr15, i32 0, i32 %16
  %mblim = getelementptr inbounds %struct.loop_filter_thresh, %struct.loop_filter_thresh* %arrayidx16, i32 0, i32 0
  %arraydecay17 = getelementptr inbounds [16 x i8], [16 x i8]* %mblim, i32 0, i32 0
  %17 = load i32, i32* %lvl, align 4, !tbaa !6
  %add18 = add nsw i32 %17, 2
  %mul = mul nsw i32 2, %add18
  %18 = load i32, i32* %block_inside_limit, align 4, !tbaa !6
  %add19 = add nsw i32 %mul, %18
  %19 = trunc i32 %add19 to i8
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay17, i8 %19, i32 16, i1 false)
  %20 = bitcast i32* %block_inside_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end14
  %21 = load i32, i32* %lvl, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %lvl, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = bitcast i32* %lvl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define hidden void @av1_loop_filter_frame_init(%struct.AV1Common* %cm, i32 %plane_start, i32 %plane_end) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %plane_start.addr = alloca i32, align 4
  %plane_end.addr = alloca i32, align 4
  %filt_lvl = alloca [3 x i32], align 4
  %filt_lvl_r = alloca [3 x i32], align 4
  %plane = alloca i32, align 4
  %seg_id = alloca i32, align 4
  %lfi = alloca %struct.loop_filter_info_n*, align 4
  %lf = alloca %struct.loopfilter*, align 4
  %seg = alloca %struct.segmentation*, align 4
  %dir = alloca i32, align 4
  %lvl_seg = alloca i32, align 4
  %seg_lf_feature_id = alloca i32, align 4
  %data = alloca i32, align 4
  %ref = alloca i32, align 4
  %mode = alloca i32, align 4
  %scale = alloca i32, align 4
  %intra_lvl = alloca i32, align 4
  %inter_lvl = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %plane_start, i32* %plane_start.addr, align 4, !tbaa !6
  store i32 %plane_end, i32* %plane_end.addr, align 4, !tbaa !6
  %0 = bitcast [3 x i32]* %filt_lvl to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #5
  %1 = bitcast [3 x i32]* %filt_lvl_r to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %1) #5
  %2 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %seg_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast %struct.loop_filter_info_n** %lfi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 27
  store %struct.loop_filter_info_n* %lf_info, %struct.loop_filter_info_n** %lfi, align 4, !tbaa !2
  %6 = bitcast %struct.loopfilter** %lf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 28
  store %struct.loopfilter* %lf1, %struct.loopfilter** %lf, align 4, !tbaa !2
  %8 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seg2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 24
  store %struct.segmentation* %seg2, %struct.segmentation** %seg, align 4, !tbaa !2
  %10 = load %struct.loop_filter_info_n*, %struct.loop_filter_info_n** %lfi, align 4, !tbaa !2
  %11 = load %struct.loopfilter*, %struct.loopfilter** %lf, align 4, !tbaa !2
  %sharpness_level = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %11, i32 0, i32 3
  %12 = load i32, i32* %sharpness_level, align 4, !tbaa !47
  call void @update_sharpness(%struct.loop_filter_info_n* %10, i32 %12)
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf3 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 28
  %filter_level = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf3, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %filter_level, i32 0, i32 0
  %14 = load i32, i32* %arrayidx, align 16, !tbaa !6
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl, i32 0, i32 0
  store i32 %14, i32* %arrayidx4, align 4, !tbaa !6
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf5 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 28
  %filter_level_u = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf5, i32 0, i32 1
  %16 = load i32, i32* %filter_level_u, align 8, !tbaa !40
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl, i32 0, i32 1
  store i32 %16, i32* %arrayidx6, align 4, !tbaa !6
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 28
  %filter_level_v = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf7, i32 0, i32 2
  %18 = load i32, i32* %filter_level_v, align 4, !tbaa !41
  %arrayidx8 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl, i32 0, i32 2
  store i32 %18, i32* %arrayidx8, align 4, !tbaa !6
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf9 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %19, i32 0, i32 28
  %filter_level10 = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf9, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [2 x i32], [2 x i32]* %filter_level10, i32 0, i32 1
  %20 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl_r, i32 0, i32 0
  store i32 %20, i32* %arrayidx12, align 4, !tbaa !6
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf13 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 28
  %filter_level_u14 = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf13, i32 0, i32 1
  %22 = load i32, i32* %filter_level_u14, align 8, !tbaa !40
  %arrayidx15 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl_r, i32 0, i32 1
  store i32 %22, i32* %arrayidx15, align 4, !tbaa !6
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf16 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %23, i32 0, i32 28
  %filter_level_v17 = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf16, i32 0, i32 2
  %24 = load i32, i32* %filter_level_v17, align 4, !tbaa !41
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl_r, i32 0, i32 2
  store i32 %24, i32* %arrayidx18, align 4, !tbaa !6
  %25 = load i32, i32* %plane_start.addr, align 4, !tbaa !6
  store i32 %25, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc108, %entry
  %26 = load i32, i32* %plane, align 4, !tbaa !6
  %27 = load i32, i32* %plane_end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %26, %27
  br i1 %cmp, label %for.body, label %for.end110

for.body:                                         ; preds = %for.cond
  %28 = load i32, i32* %plane, align 4, !tbaa !6
  %cmp19 = icmp eq i32 %28, 0
  br i1 %cmp19, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.body
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl, i32 0, i32 0
  %29 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %tobool = icmp ne i32 %29, 0
  br i1 %tobool, label %if.else, label %land.lhs.true21

land.lhs.true21:                                  ; preds = %land.lhs.true
  %arrayidx22 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl_r, i32 0, i32 0
  %30 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  %tobool23 = icmp ne i32 %30, 0
  br i1 %tobool23, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true21
  br label %for.end110

if.else:                                          ; preds = %land.lhs.true21, %land.lhs.true, %for.body
  %31 = load i32, i32* %plane, align 4, !tbaa !6
  %cmp24 = icmp eq i32 %31, 1
  br i1 %cmp24, label %land.lhs.true25, label %if.else29

land.lhs.true25:                                  ; preds = %if.else
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl, i32 0, i32 1
  %32 = load i32, i32* %arrayidx26, align 4, !tbaa !6
  %tobool27 = icmp ne i32 %32, 0
  br i1 %tobool27, label %if.else29, label %if.then28

if.then28:                                        ; preds = %land.lhs.true25
  br label %for.inc108

if.else29:                                        ; preds = %land.lhs.true25, %if.else
  %33 = load i32, i32* %plane, align 4, !tbaa !6
  %cmp30 = icmp eq i32 %33, 2
  br i1 %cmp30, label %land.lhs.true31, label %if.end

land.lhs.true31:                                  ; preds = %if.else29
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl, i32 0, i32 2
  %34 = load i32, i32* %arrayidx32, align 4, !tbaa !6
  %tobool33 = icmp ne i32 %34, 0
  br i1 %tobool33, label %if.end, label %if.then34

if.then34:                                        ; preds = %land.lhs.true31
  br label %for.inc108

if.end:                                           ; preds = %land.lhs.true31, %if.else29
  br label %if.end35

if.end35:                                         ; preds = %if.end
  br label %if.end36

if.end36:                                         ; preds = %if.end35
  store i32 0, i32* %seg_id, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc105, %if.end36
  %35 = load i32, i32* %seg_id, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %35, 8
  br i1 %cmp38, label %for.body39, label %for.end107

for.body39:                                       ; preds = %for.cond37
  %36 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #5
  store i32 0, i32* %dir, align 4, !tbaa !6
  br label %for.cond40

for.cond40:                                       ; preds = %for.inc102, %for.body39
  %37 = load i32, i32* %dir, align 4, !tbaa !6
  %cmp41 = icmp slt i32 %37, 2
  br i1 %cmp41, label %for.body42, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond40
  %38 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #5
  br label %for.end104

for.body42:                                       ; preds = %for.cond40
  %39 = bitcast i32* %lvl_seg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #5
  %40 = load i32, i32* %dir, align 4, !tbaa !6
  %cmp43 = icmp eq i32 %40, 0
  br i1 %cmp43, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body42
  %41 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body42
  %43 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds [3 x i32], [3 x i32]* %filt_lvl_r, i32 0, i32 %43
  %44 = load i32, i32* %arrayidx45, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %42, %cond.true ], [ %44, %cond.false ]
  store i32 %cond, i32* %lvl_seg, align 4, !tbaa !6
  %45 = bitcast i32* %seg_lf_feature_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #5
  %46 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds [3 x [2 x i8]], [3 x [2 x i8]]* @seg_lvl_lf_lut, i32 0, i32 %46
  %47 = load i32, i32* %dir, align 4, !tbaa !6
  %arrayidx47 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx46, i32 0, i32 %47
  %48 = load i8, i8* %arrayidx47, align 1, !tbaa !33
  %conv = zext i8 %48 to i32
  store i32 %conv, i32* %seg_lf_feature_id, align 4, !tbaa !6
  %49 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !2
  %50 = load i32, i32* %seg_id, align 4, !tbaa !6
  %51 = load i32, i32* %seg_lf_feature_id, align 4, !tbaa !6
  %conv48 = trunc i32 %51 to i8
  %call = call i32 @segfeature_active(%struct.segmentation* %49, i32 %50, i8 zeroext %conv48)
  %tobool49 = icmp ne i32 %call, 0
  br i1 %tobool49, label %if.then50, label %if.end55

if.then50:                                        ; preds = %cond.end
  %52 = bitcast i32* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #5
  %53 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seg51 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %53, i32 0, i32 24
  %54 = load i32, i32* %seg_id, align 4, !tbaa !6
  %55 = load i32, i32* %seg_lf_feature_id, align 4, !tbaa !6
  %conv52 = trunc i32 %55 to i8
  %call53 = call i32 @get_segdata(%struct.segmentation* %seg51, i32 %54, i8 zeroext %conv52)
  store i32 %call53, i32* %data, align 4, !tbaa !6
  %56 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %57 = load i32, i32* %data, align 4, !tbaa !6
  %add = add nsw i32 %56, %57
  %call54 = call i32 @clamp(i32 %add, i32 0, i32 63)
  store i32 %call54, i32* %lvl_seg, align 4, !tbaa !6
  %58 = bitcast i32* %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #5
  br label %if.end55

if.end55:                                         ; preds = %if.then50, %cond.end
  %59 = load %struct.loopfilter*, %struct.loopfilter** %lf, align 4, !tbaa !2
  %mode_ref_delta_enabled = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %59, i32 0, i32 4
  %60 = load i8, i8* %mode_ref_delta_enabled, align 4, !tbaa !48
  %tobool56 = icmp ne i8 %60, 0
  br i1 %tobool56, label %if.else61, label %if.then57

if.then57:                                        ; preds = %if.end55
  %61 = load %struct.loop_filter_info_n*, %struct.loop_filter_info_n** %lfi, align 4, !tbaa !2
  %lvl = getelementptr inbounds %struct.loop_filter_info_n, %struct.loop_filter_info_n* %61, i32 0, i32 1
  %62 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds [3 x [8 x [2 x [8 x [2 x i8]]]]], [3 x [8 x [2 x [8 x [2 x i8]]]]]* %lvl, i32 0, i32 %62
  %63 = load i32, i32* %seg_id, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds [8 x [2 x [8 x [2 x i8]]]], [8 x [2 x [8 x [2 x i8]]]]* %arrayidx58, i32 0, i32 %63
  %64 = load i32, i32* %dir, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [2 x [8 x [2 x i8]]], [2 x [8 x [2 x i8]]]* %arrayidx59, i32 0, i32 %64
  %arraydecay = getelementptr inbounds [8 x [2 x i8]], [8 x [2 x i8]]* %arrayidx60, i32 0, i32 0
  %65 = bitcast [2 x i8]* %arraydecay to i8*
  %66 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %67 = trunc i32 %66 to i8
  call void @llvm.memset.p0i8.i32(i8* align 16 %65, i8 %67, i32 16, i1 false)
  br label %if.end101

if.else61:                                        ; preds = %if.end55
  %68 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #5
  %69 = bitcast i32* %mode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #5
  %70 = bitcast i32* %scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #5
  %71 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %shr = ashr i32 %71, 5
  %shl = shl i32 1, %shr
  store i32 %shl, i32* %scale, align 4, !tbaa !6
  %72 = bitcast i32* %intra_lvl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #5
  %73 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %74 = load %struct.loopfilter*, %struct.loopfilter** %lf, align 4, !tbaa !2
  %ref_deltas = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %74, i32 0, i32 6
  %arrayidx62 = getelementptr inbounds [8 x i8], [8 x i8]* %ref_deltas, i32 0, i32 0
  %75 = load i8, i8* %arrayidx62, align 2, !tbaa !33
  %conv63 = sext i8 %75 to i32
  %76 = load i32, i32* %scale, align 4, !tbaa !6
  %mul = mul nsw i32 %conv63, %76
  %add64 = add nsw i32 %73, %mul
  store i32 %add64, i32* %intra_lvl, align 4, !tbaa !6
  %77 = load i32, i32* %intra_lvl, align 4, !tbaa !6
  %call65 = call i32 @clamp(i32 %77, i32 0, i32 63)
  %conv66 = trunc i32 %call65 to i8
  %78 = load %struct.loop_filter_info_n*, %struct.loop_filter_info_n** %lfi, align 4, !tbaa !2
  %lvl67 = getelementptr inbounds %struct.loop_filter_info_n, %struct.loop_filter_info_n* %78, i32 0, i32 1
  %79 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx68 = getelementptr inbounds [3 x [8 x [2 x [8 x [2 x i8]]]]], [3 x [8 x [2 x [8 x [2 x i8]]]]]* %lvl67, i32 0, i32 %79
  %80 = load i32, i32* %seg_id, align 4, !tbaa !6
  %arrayidx69 = getelementptr inbounds [8 x [2 x [8 x [2 x i8]]]], [8 x [2 x [8 x [2 x i8]]]]* %arrayidx68, i32 0, i32 %80
  %81 = load i32, i32* %dir, align 4, !tbaa !6
  %arrayidx70 = getelementptr inbounds [2 x [8 x [2 x i8]]], [2 x [8 x [2 x i8]]]* %arrayidx69, i32 0, i32 %81
  %arrayidx71 = getelementptr inbounds [8 x [2 x i8]], [8 x [2 x i8]]* %arrayidx70, i32 0, i32 0
  %arrayidx72 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx71, i32 0, i32 0
  store i8 %conv66, i8* %arrayidx72, align 16, !tbaa !33
  store i32 1, i32* %ref, align 4, !tbaa !6
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc98, %if.else61
  %82 = load i32, i32* %ref, align 4, !tbaa !6
  %cmp74 = icmp slt i32 %82, 8
  br i1 %cmp74, label %for.body76, label %for.end100

for.body76:                                       ; preds = %for.cond73
  store i32 0, i32* %mode, align 4, !tbaa !6
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc, %for.body76
  %83 = load i32, i32* %mode, align 4, !tbaa !6
  %cmp78 = icmp slt i32 %83, 2
  br i1 %cmp78, label %for.body80, label %for.end

for.body80:                                       ; preds = %for.cond77
  %84 = bitcast i32* %inter_lvl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #5
  %85 = load i32, i32* %lvl_seg, align 4, !tbaa !6
  %86 = load %struct.loopfilter*, %struct.loopfilter** %lf, align 4, !tbaa !2
  %ref_deltas81 = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %86, i32 0, i32 6
  %87 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx82 = getelementptr inbounds [8 x i8], [8 x i8]* %ref_deltas81, i32 0, i32 %87
  %88 = load i8, i8* %arrayidx82, align 1, !tbaa !33
  %conv83 = sext i8 %88 to i32
  %89 = load i32, i32* %scale, align 4, !tbaa !6
  %mul84 = mul nsw i32 %conv83, %89
  %add85 = add nsw i32 %85, %mul84
  %90 = load %struct.loopfilter*, %struct.loopfilter** %lf, align 4, !tbaa !2
  %mode_deltas = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %90, i32 0, i32 7
  %91 = load i32, i32* %mode, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds [2 x i8], [2 x i8]* %mode_deltas, i32 0, i32 %91
  %92 = load i8, i8* %arrayidx86, align 1, !tbaa !33
  %conv87 = sext i8 %92 to i32
  %93 = load i32, i32* %scale, align 4, !tbaa !6
  %mul88 = mul nsw i32 %conv87, %93
  %add89 = add nsw i32 %add85, %mul88
  store i32 %add89, i32* %inter_lvl, align 4, !tbaa !6
  %94 = load i32, i32* %inter_lvl, align 4, !tbaa !6
  %call90 = call i32 @clamp(i32 %94, i32 0, i32 63)
  %conv91 = trunc i32 %call90 to i8
  %95 = load %struct.loop_filter_info_n*, %struct.loop_filter_info_n** %lfi, align 4, !tbaa !2
  %lvl92 = getelementptr inbounds %struct.loop_filter_info_n, %struct.loop_filter_info_n* %95, i32 0, i32 1
  %96 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx93 = getelementptr inbounds [3 x [8 x [2 x [8 x [2 x i8]]]]], [3 x [8 x [2 x [8 x [2 x i8]]]]]* %lvl92, i32 0, i32 %96
  %97 = load i32, i32* %seg_id, align 4, !tbaa !6
  %arrayidx94 = getelementptr inbounds [8 x [2 x [8 x [2 x i8]]]], [8 x [2 x [8 x [2 x i8]]]]* %arrayidx93, i32 0, i32 %97
  %98 = load i32, i32* %dir, align 4, !tbaa !6
  %arrayidx95 = getelementptr inbounds [2 x [8 x [2 x i8]]], [2 x [8 x [2 x i8]]]* %arrayidx94, i32 0, i32 %98
  %99 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx96 = getelementptr inbounds [8 x [2 x i8]], [8 x [2 x i8]]* %arrayidx95, i32 0, i32 %99
  %100 = load i32, i32* %mode, align 4, !tbaa !6
  %arrayidx97 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx96, i32 0, i32 %100
  store i8 %conv91, i8* %arrayidx97, align 1, !tbaa !33
  %101 = bitcast i32* %inter_lvl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #5
  br label %for.inc

for.inc:                                          ; preds = %for.body80
  %102 = load i32, i32* %mode, align 4, !tbaa !6
  %inc = add nsw i32 %102, 1
  store i32 %inc, i32* %mode, align 4, !tbaa !6
  br label %for.cond77

for.end:                                          ; preds = %for.cond77
  br label %for.inc98

for.inc98:                                        ; preds = %for.end
  %103 = load i32, i32* %ref, align 4, !tbaa !6
  %inc99 = add nsw i32 %103, 1
  store i32 %inc99, i32* %ref, align 4, !tbaa !6
  br label %for.cond73

for.end100:                                       ; preds = %for.cond73
  %104 = bitcast i32* %intra_lvl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #5
  %105 = bitcast i32* %scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #5
  %106 = bitcast i32* %mode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #5
  %107 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #5
  br label %if.end101

if.end101:                                        ; preds = %for.end100, %if.then57
  %108 = bitcast i32* %seg_lf_feature_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  %109 = bitcast i32* %lvl_seg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #5
  br label %for.inc102

for.inc102:                                       ; preds = %if.end101
  %110 = load i32, i32* %dir, align 4, !tbaa !6
  %inc103 = add nsw i32 %110, 1
  store i32 %inc103, i32* %dir, align 4, !tbaa !6
  br label %for.cond40

for.end104:                                       ; preds = %for.cond.cleanup
  br label %for.inc105

for.inc105:                                       ; preds = %for.end104
  %111 = load i32, i32* %seg_id, align 4, !tbaa !6
  %inc106 = add nsw i32 %111, 1
  store i32 %inc106, i32* %seg_id, align 4, !tbaa !6
  br label %for.cond37

for.end107:                                       ; preds = %for.cond37
  br label %for.inc108

for.inc108:                                       ; preds = %for.end107, %if.then34, %if.then28
  %112 = load i32, i32* %plane, align 4, !tbaa !6
  %inc109 = add nsw i32 %112, 1
  store i32 %inc109, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end110:                                       ; preds = %if.then, %for.cond
  %113 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #5
  %114 = bitcast %struct.loopfilter** %lf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #5
  %115 = bitcast %struct.loop_filter_info_n** %lfi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #5
  %116 = bitcast i32* %seg_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #5
  %117 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast [3 x i32]* %filt_lvl_r to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %118) #5
  %119 = bitcast [3 x i32]* %filt_lvl to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %119) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_filter_block_plane_vert(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane, %struct.macroblockd_plane* %plane_ptr, i32 %mi_row, i32 %mi_col) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %plane_ptr.addr = alloca %struct.macroblockd_plane*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %scale_horz = alloca i32, align 4
  %scale_vert = alloca i32, align 4
  %dst_ptr = alloca i8*, align 4
  %dst_stride = alloca i32, align 4
  %y_range = alloca i32, align 4
  %x_range = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %p = alloca i8*, align 4
  %x = alloca i32, align 4
  %curr_x = alloca i32, align 4
  %curr_y = alloca i32, align 4
  %advance_units = alloca i32, align 4
  %tx_size = alloca i8, align 1
  %params = alloca %struct.AV1_DEBLOCKING_PARAMETERS, align 4
  %use_highbitdepth = alloca i32, align 4
  %bit_depth = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.macroblockd_plane* %plane_ptr, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %1, i32 0, i32 4
  %2 = load i32, i32* %subsampling_x, align 4, !tbaa !49
  store i32 %2, i32* %scale_horz, align 4, !tbaa !6
  %3 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %4, i32 0, i32 5
  %5 = load i32, i32* %subsampling_y, align 4, !tbaa !52
  store i32 %5, i32* %scale_vert, align 4, !tbaa !6
  %6 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %7, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 0
  %8 = load i8*, i8** %buf, align 4, !tbaa !53
  store i8* %8, i8** %dst_ptr, align 4, !tbaa !2
  %9 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %10, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst1, i32 0, i32 4
  %11 = load i32, i32* %stride, align 4, !tbaa !54
  store i32 %11, i32* %dst_stride, align 4, !tbaa !6
  %12 = bitcast i32* %y_range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shr = ashr i32 32, %13
  store i32 %shr, i32* %y_range, align 4, !tbaa !6
  %14 = bitcast i32* %x_range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shr2 = ashr i32 32, %15
  store i32 %shr2, i32* %x_range, align 4, !tbaa !6
  %16 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %17 = load i32, i32* %y, align 4, !tbaa !6
  %18 = load i32, i32* %y_range, align 4, !tbaa !6
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  br label %for.end67

for.body:                                         ; preds = %for.cond
  %20 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  %21 = load i8*, i8** %dst_ptr, align 4, !tbaa !2
  %22 = load i32, i32* %y, align 4, !tbaa !6
  %mul = mul nsw i32 %22, 4
  %23 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mul3 = mul nsw i32 %mul, %23
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 %mul3
  store i8* %add.ptr, i8** %p, align 4, !tbaa !2
  %24 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %sw.epilog, %for.body
  %25 = load i32, i32* %x, align 4, !tbaa !6
  %26 = load i32, i32* %x_range, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %25, %26
  br i1 %cmp5, label %for.body7, label %for.cond.cleanup6

for.cond.cleanup6:                                ; preds = %for.cond4
  store i32 5, i32* %cleanup.dest.slot, align 4
  %27 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  br label %for.end

for.body7:                                        ; preds = %for.cond4
  %28 = bitcast i32* %curr_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  %29 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %mul8 = mul i32 %29, 4
  %30 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shr9 = lshr i32 %mul8, %30
  %31 = load i32, i32* %x, align 4, !tbaa !6
  %mul10 = mul nsw i32 %31, 4
  %add = add i32 %shr9, %mul10
  store i32 %add, i32* %curr_x, align 4, !tbaa !6
  %32 = bitcast i32* %curr_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  %33 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %mul11 = mul i32 %33, 4
  %34 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shr12 = lshr i32 %mul11, %34
  %35 = load i32, i32* %y, align 4, !tbaa !6
  %mul13 = mul nsw i32 %35, 4
  %add14 = add i32 %shr12, %mul13
  store i32 %add14, i32* %curr_y, align 4, !tbaa !6
  %36 = bitcast i32* %advance_units to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size) #5
  %37 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #5
  %38 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %38, i8 0, i32 16, i1 false)
  %39 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shl = shl i32 1, %39
  %40 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %41 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %42 = load i32, i32* %curr_x, align 4, !tbaa !6
  %43 = load i32, i32* %curr_y, align 4, !tbaa !6
  %44 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %45 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %call = call zeroext i8 @set_lpf_parameters(%struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 %shl, %struct.AV1Common* %40, %struct.macroblockd* %41, i8 zeroext 0, i32 %42, i32 %43, i32 %44, %struct.macroblockd_plane* %45)
  store i8 %call, i8* %tx_size, align 1, !tbaa !33
  %46 = load i8, i8* %tx_size, align 1, !tbaa !33
  %conv = zext i8 %46 to i32
  %cmp15 = icmp eq i32 %conv, 255
  br i1 %cmp15, label %if.then, label %if.end

if.then:                                          ; preds = %for.body7
  %filter_length = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 0
  store i32 0, i32* %filter_length, align 4, !tbaa !55
  store i8 0, i8* %tx_size, align 1, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body7
  %47 = bitcast i32* %use_highbitdepth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  %48 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %48, i32 0, i32 37
  %use_highbitdepth17 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 26
  %49 = load i8, i8* %use_highbitdepth17, align 4, !tbaa !57
  %conv18 = zext i8 %49 to i32
  store i32 %conv18, i32* %use_highbitdepth, align 4, !tbaa !6
  %50 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #5
  %51 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params19 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %51, i32 0, i32 37
  %bit_depth20 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params19, i32 0, i32 25
  %52 = load i32, i32* %bit_depth20, align 8, !tbaa !58
  store i32 %52, i32* %bit_depth, align 4, !tbaa !33
  %filter_length21 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 0
  %53 = load i32, i32* %filter_length21, align 4, !tbaa !55
  switch i32 %53, label %sw.default [
    i32 4, label %sw.bb
    i32 6, label %sw.bb28
    i32 8, label %sw.bb40
    i32 14, label %sw.bb52
  ]

sw.bb:                                            ; preds = %if.end
  %54 = load i32, i32* %use_highbitdepth, align 4, !tbaa !6
  %tobool = icmp ne i32 %54, 0
  br i1 %tobool, label %if.then22, label %if.else

if.then22:                                        ; preds = %sw.bb
  %55 = load i8*, i8** %p, align 4, !tbaa !2
  %56 = ptrtoint i8* %55 to i32
  %shl23 = shl i32 %56, 1
  %57 = inttoptr i32 %shl23 to i16*
  %58 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %59 = load i8*, i8** %mblim, align 4, !tbaa !59
  %lim = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %60 = load i8*, i8** %lim, align 4, !tbaa !60
  %hev_thr = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %61 = load i8*, i8** %hev_thr, align 4, !tbaa !61
  %62 = load i32, i32* %bit_depth, align 4, !tbaa !33
  call void @aom_highbd_lpf_vertical_4_c(i16* %57, i32 %58, i8* %59, i8* %60, i8* %61, i32 %62)
  br label %if.end27

if.else:                                          ; preds = %sw.bb
  %63 = load i8*, i8** %p, align 4, !tbaa !2
  %64 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim24 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %65 = load i8*, i8** %mblim24, align 4, !tbaa !59
  %lim25 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %66 = load i8*, i8** %lim25, align 4, !tbaa !60
  %hev_thr26 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %67 = load i8*, i8** %hev_thr26, align 4, !tbaa !61
  call void @aom_lpf_vertical_4_c(i8* %63, i32 %64, i8* %65, i8* %66, i8* %67)
  br label %if.end27

if.end27:                                         ; preds = %if.else, %if.then22
  br label %sw.epilog

sw.bb28:                                          ; preds = %if.end
  %68 = load i32, i32* %use_highbitdepth, align 4, !tbaa !6
  %tobool29 = icmp ne i32 %68, 0
  br i1 %tobool29, label %if.then30, label %if.else35

if.then30:                                        ; preds = %sw.bb28
  %69 = load i8*, i8** %p, align 4, !tbaa !2
  %70 = ptrtoint i8* %69 to i32
  %shl31 = shl i32 %70, 1
  %71 = inttoptr i32 %shl31 to i16*
  %72 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim32 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %73 = load i8*, i8** %mblim32, align 4, !tbaa !59
  %lim33 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %74 = load i8*, i8** %lim33, align 4, !tbaa !60
  %hev_thr34 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %75 = load i8*, i8** %hev_thr34, align 4, !tbaa !61
  %76 = load i32, i32* %bit_depth, align 4, !tbaa !33
  call void @aom_highbd_lpf_vertical_6_c(i16* %71, i32 %72, i8* %73, i8* %74, i8* %75, i32 %76)
  br label %if.end39

if.else35:                                        ; preds = %sw.bb28
  %77 = load i8*, i8** %p, align 4, !tbaa !2
  %78 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim36 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %79 = load i8*, i8** %mblim36, align 4, !tbaa !59
  %lim37 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %80 = load i8*, i8** %lim37, align 4, !tbaa !60
  %hev_thr38 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %81 = load i8*, i8** %hev_thr38, align 4, !tbaa !61
  call void @aom_lpf_vertical_6_c(i8* %77, i32 %78, i8* %79, i8* %80, i8* %81)
  br label %if.end39

if.end39:                                         ; preds = %if.else35, %if.then30
  br label %sw.epilog

sw.bb40:                                          ; preds = %if.end
  %82 = load i32, i32* %use_highbitdepth, align 4, !tbaa !6
  %tobool41 = icmp ne i32 %82, 0
  br i1 %tobool41, label %if.then42, label %if.else47

if.then42:                                        ; preds = %sw.bb40
  %83 = load i8*, i8** %p, align 4, !tbaa !2
  %84 = ptrtoint i8* %83 to i32
  %shl43 = shl i32 %84, 1
  %85 = inttoptr i32 %shl43 to i16*
  %86 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim44 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %87 = load i8*, i8** %mblim44, align 4, !tbaa !59
  %lim45 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %88 = load i8*, i8** %lim45, align 4, !tbaa !60
  %hev_thr46 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %89 = load i8*, i8** %hev_thr46, align 4, !tbaa !61
  %90 = load i32, i32* %bit_depth, align 4, !tbaa !33
  call void @aom_highbd_lpf_vertical_8_c(i16* %85, i32 %86, i8* %87, i8* %88, i8* %89, i32 %90)
  br label %if.end51

if.else47:                                        ; preds = %sw.bb40
  %91 = load i8*, i8** %p, align 4, !tbaa !2
  %92 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim48 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %93 = load i8*, i8** %mblim48, align 4, !tbaa !59
  %lim49 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %94 = load i8*, i8** %lim49, align 4, !tbaa !60
  %hev_thr50 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %95 = load i8*, i8** %hev_thr50, align 4, !tbaa !61
  call void @aom_lpf_vertical_8_c(i8* %91, i32 %92, i8* %93, i8* %94, i8* %95)
  br label %if.end51

if.end51:                                         ; preds = %if.else47, %if.then42
  br label %sw.epilog

sw.bb52:                                          ; preds = %if.end
  %96 = load i32, i32* %use_highbitdepth, align 4, !tbaa !6
  %tobool53 = icmp ne i32 %96, 0
  br i1 %tobool53, label %if.then54, label %if.else59

if.then54:                                        ; preds = %sw.bb52
  %97 = load i8*, i8** %p, align 4, !tbaa !2
  %98 = ptrtoint i8* %97 to i32
  %shl55 = shl i32 %98, 1
  %99 = inttoptr i32 %shl55 to i16*
  %100 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim56 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %101 = load i8*, i8** %mblim56, align 4, !tbaa !59
  %lim57 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %102 = load i8*, i8** %lim57, align 4, !tbaa !60
  %hev_thr58 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %103 = load i8*, i8** %hev_thr58, align 4, !tbaa !61
  %104 = load i32, i32* %bit_depth, align 4, !tbaa !33
  call void @aom_highbd_lpf_vertical_14_c(i16* %99, i32 %100, i8* %101, i8* %102, i8* %103, i32 %104)
  br label %if.end63

if.else59:                                        ; preds = %sw.bb52
  %105 = load i8*, i8** %p, align 4, !tbaa !2
  %106 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim60 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %107 = load i8*, i8** %mblim60, align 4, !tbaa !59
  %lim61 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %108 = load i8*, i8** %lim61, align 4, !tbaa !60
  %hev_thr62 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %109 = load i8*, i8** %hev_thr62, align 4, !tbaa !61
  call void @aom_lpf_vertical_14_c(i8* %105, i32 %106, i8* %107, i8* %108, i8* %109)
  br label %if.end63

if.end63:                                         ; preds = %if.else59, %if.then54
  br label %sw.epilog

sw.default:                                       ; preds = %if.end
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end63, %if.end51, %if.end39, %if.end27
  %110 = load i8, i8* %tx_size, align 1, !tbaa !33
  %idxprom = zext i8 %110 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_unit, i32 0, i32 %idxprom
  %111 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %111, i32* %advance_units, align 4, !tbaa !6
  %112 = load i32, i32* %advance_units, align 4, !tbaa !6
  %113 = load i32, i32* %x, align 4, !tbaa !6
  %add64 = add i32 %113, %112
  store i32 %add64, i32* %x, align 4, !tbaa !6
  %114 = load i32, i32* %advance_units, align 4, !tbaa !6
  %mul65 = mul i32 %114, 4
  %115 = load i8*, i8** %p, align 4, !tbaa !2
  %add.ptr66 = getelementptr inbounds i8, i8* %115, i32 %mul65
  store i8* %add.ptr66, i8** %p, align 4, !tbaa !2
  %116 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #5
  %117 = bitcast i32* %use_highbitdepth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %118) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size) #5
  %119 = bitcast i32* %advance_units to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i32* %curr_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  %121 = bitcast i32* %curr_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  br label %for.cond4

for.end:                                          ; preds = %for.cond.cleanup6
  %122 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  br label %for.inc

for.inc:                                          ; preds = %for.end
  %123 = load i32, i32* %y, align 4, !tbaa !6
  %inc = add nsw i32 %123, 1
  store i32 %inc, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end67:                                        ; preds = %for.cond.cleanup
  %124 = bitcast i32* %x_range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  %125 = bitcast i32* %y_range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #5
  %126 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #5
  %127 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #5
  %128 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #5
  %129 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #5
  ret void
}

; Function Attrs: nounwind
define internal zeroext i8 @set_lpf_parameters(%struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 %mode_step, %struct.AV1Common* %cm, %struct.macroblockd* %xd, i8 zeroext %edge_dir, i32 %x, i32 %y, i32 %plane, %struct.macroblockd_plane* %plane_ptr) #0 {
entry:
  %retval = alloca i8, align 1
  %params.addr = alloca %struct.AV1_DEBLOCKING_PARAMETERS*, align 4
  %mode_step.addr = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %edge_dir.addr = alloca i8, align 1
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %plane_ptr.addr = alloca %struct.macroblockd_plane*, align 4
  %width = alloca i32, align 4
  %height = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %scale_horz = alloca i32, align 4
  %scale_vert = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %mi = alloca %struct.MB_MODE_INFO**, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %ts = alloca i8, align 1
  %coord = alloca i32, align 4
  %transform_masks = alloca i32, align 4
  %tu_edge = alloca i32, align 4
  %curr_level = alloca i32, align 4
  %curr_skipped = alloca i32, align 4
  %level = alloca i32, align 4
  %mi_prev = alloca %struct.MB_MODE_INFO*, align 4
  %pv_row = alloca i32, align 4
  %pv_col = alloca i32, align 4
  %pv_ts = alloca i8, align 1
  %pv_lvl = alloca i32, align 4
  %pv_skip = alloca i32, align 4
  %bsize = alloca i8, align 1
  %prediction_masks = alloca i32, align 4
  %pu_edge = alloca i32, align 4
  %min_ts = alloca i8, align 1
  %limits = alloca %struct.loop_filter_thresh*, align 4
  store %struct.AV1_DEBLOCKING_PARAMETERS* %params, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  store i32 %mode_step, i32* %mode_step.addr, align 4, !tbaa !62
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %edge_dir, i8* %edge_dir.addr, align 1, !tbaa !33
  store i32 %x, i32* %x.addr, align 4, !tbaa !6
  store i32 %y, i32* %y.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.macroblockd_plane* %plane_ptr, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %0 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %filter_length = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %0, i32 0, i32 0
  store i32 0, i32* %filter_length, align 4, !tbaa !55
  %1 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %2, i32 0, i32 6
  %width1 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 2
  %3 = load i32, i32* %width1, align 4, !tbaa !63
  store i32 %3, i32* %width, align 4, !tbaa !6
  %4 = bitcast i32* %height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst2 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %5, i32 0, i32 6
  %height3 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst2, i32 0, i32 3
  %6 = load i32, i32* %height3, align 4, !tbaa !64
  store i32 %6, i32* %height, align 4, !tbaa !6
  %7 = load i32, i32* %width, align 4, !tbaa !6
  %8 = load i32, i32* %x.addr, align 4, !tbaa !6
  %cmp = icmp ule i32 %7, %8
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %9 = load i32, i32* %height, align 4, !tbaa !6
  %10 = load i32, i32* %y.addr, align 4, !tbaa !6
  %cmp4 = icmp ule i32 %9, %10
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i8 0, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup177

if.end:                                           ; preds = %lor.lhs.false
  %11 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %12, i32 0, i32 4
  %13 = load i32, i32* %subsampling_x, align 4, !tbaa !49
  store i32 %13, i32* %scale_horz, align 4, !tbaa !6
  %14 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %15, i32 0, i32 5
  %16 = load i32, i32* %subsampling_y, align 4, !tbaa !52
  store i32 %16, i32* %scale_vert, align 4, !tbaa !6
  %17 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %19 = load i32, i32* %y.addr, align 4, !tbaa !6
  %20 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shl = shl i32 %19, %20
  %shr = lshr i32 %shl, 2
  %or = or i32 %18, %shr
  store i32 %or, i32* %mi_row, align 4, !tbaa !6
  %21 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %23 = load i32, i32* %x.addr, align 4, !tbaa !6
  %24 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shl5 = shl i32 %23, %24
  %shr6 = lshr i32 %shl5, 2
  %or7 = or i32 %22, %shr6
  store i32 %or7, i32* %mi_col, align 4, !tbaa !6
  %25 = bitcast %struct.MB_MODE_INFO*** %mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  %26 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %26, i32 0, i32 22
  %mi_grid_base = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 9
  %27 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base, align 4, !tbaa !65
  %28 = load i32, i32* %mi_row, align 4, !tbaa !6
  %29 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %29, i32 0, i32 22
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params8, i32 0, i32 11
  %30 = load i32, i32* %mi_stride, align 4, !tbaa !66
  %mul = mul nsw i32 %28, %30
  %add.ptr = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %27, i32 %mul
  %31 = load i32, i32* %mi_col, align 4, !tbaa !6
  %add.ptr9 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %add.ptr, i32 %31
  store %struct.MB_MODE_INFO** %add.ptr9, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %32 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  %33 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %33, i32 0
  %34 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %34, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %35 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %cmp10 = icmp eq %struct.MB_MODE_INFO* %35, null
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end
  store i8 -1, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

if.end12:                                         ; preds = %if.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ts) #5
  %36 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %37 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %37, i32 0
  %38 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx13, align 4, !tbaa !2
  %39 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %40 = load i32, i32* %mi_row, align 4, !tbaa !6
  %41 = load i32, i32* %mi_col, align 4, !tbaa !6
  %42 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %43 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %call = call zeroext i8 @get_transform_size(%struct.macroblockd* %36, %struct.MB_MODE_INFO* %38, i8 zeroext %39, i32 %40, i32 %41, i32 %42, %struct.macroblockd_plane* %43)
  store i8 %call, i8* %ts, align 1, !tbaa !33
  %44 = bitcast i32* %coord to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  %45 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %conv = zext i8 %45 to i32
  %cmp14 = icmp eq i32 0, %conv
  br i1 %cmp14, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end12
  %46 = load i32, i32* %x.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.end12
  %47 = load i32, i32* %y.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %46, %cond.true ], [ %47, %cond.false ]
  store i32 %cond, i32* %coord, align 4, !tbaa !6
  %48 = bitcast i32* %transform_masks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #5
  %49 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %conv16 = zext i8 %49 to i32
  %cmp17 = icmp eq i32 %conv16, 0
  br i1 %cmp17, label %cond.true19, label %cond.false21

cond.true19:                                      ; preds = %cond.end
  %50 = load i8, i8* %ts, align 1, !tbaa !33
  %idxprom = zext i8 %50 to i32
  %arrayidx20 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %51 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %sub = sub nsw i32 %51, 1
  br label %cond.end25

cond.false21:                                     ; preds = %cond.end
  %52 = load i8, i8* %ts, align 1, !tbaa !33
  %idxprom22 = zext i8 %52 to i32
  %arrayidx23 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom22
  %53 = load i32, i32* %arrayidx23, align 4, !tbaa !6
  %sub24 = sub nsw i32 %53, 1
  br label %cond.end25

cond.end25:                                       ; preds = %cond.false21, %cond.true19
  %cond26 = phi i32 [ %sub, %cond.true19 ], [ %sub24, %cond.false21 ]
  store i32 %cond26, i32* %transform_masks, align 4, !tbaa !6
  %54 = bitcast i32* %tu_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #5
  %55 = load i32, i32* %coord, align 4, !tbaa !6
  %56 = load i32, i32* %transform_masks, align 4, !tbaa !6
  %and = and i32 %55, %56
  %tobool = icmp ne i32 %and, 0
  %57 = zext i1 %tobool to i64
  %cond27 = select i1 %tobool, i32 0, i32 1
  store i32 %cond27, i32* %tu_edge, align 4, !tbaa !6
  %58 = load i32, i32* %tu_edge, align 4, !tbaa !6
  %tobool28 = icmp ne i32 %58, 0
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %cond.end25
  %59 = load i8, i8* %ts, align 1, !tbaa !33
  store i8 %59, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup165

if.end30:                                         ; preds = %cond.end25
  %60 = bitcast i32* %curr_level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #5
  %61 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %62 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %62, i32 0, i32 27
  %63 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %conv31 = zext i8 %63 to i32
  %64 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %65 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %call32 = call zeroext i8 @av1_get_filter_level(%struct.AV1Common* %61, %struct.loop_filter_info_n* %lf_info, i32 %conv31, i32 %64, %struct.MB_MODE_INFO* %65)
  %conv33 = zext i8 %call32 to i32
  store i32 %conv33, i32* %curr_level, align 4, !tbaa !6
  %66 = bitcast i32* %curr_skipped to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #5
  %67 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %67, i32 0, i32 14
  %68 = load i8, i8* %skip, align 4, !tbaa !67
  %conv34 = sext i8 %68 to i32
  %tobool35 = icmp ne i32 %conv34, 0
  br i1 %tobool35, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end30
  %69 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %call36 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %69)
  %tobool37 = icmp ne i32 %call36, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end30
  %70 = phi i1 [ false, %if.end30 ], [ %tobool37, %land.rhs ]
  %land.ext = zext i1 %70 to i32
  store i32 %land.ext, i32* %curr_skipped, align 4, !tbaa !6
  %71 = bitcast i32* %level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #5
  %72 = load i32, i32* %curr_level, align 4, !tbaa !6
  store i32 %72, i32* %level, align 4, !tbaa !6
  %73 = load i32, i32* %coord, align 4, !tbaa !6
  %tobool38 = icmp ne i32 %73, 0
  br i1 %tobool38, label %if.then39, label %if.end147

if.then39:                                        ; preds = %land.end
  %74 = bitcast %struct.MB_MODE_INFO** %mi_prev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #5
  %75 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !2
  %76 = load i32, i32* %mode_step.addr, align 4, !tbaa !62
  %idx.neg = sub i32 0, %76
  %add.ptr40 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %75, i32 %idx.neg
  %77 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %add.ptr40, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %77, %struct.MB_MODE_INFO** %mi_prev, align 4, !tbaa !2
  %78 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi_prev, align 4, !tbaa !2
  %cmp41 = icmp eq %struct.MB_MODE_INFO* %78, null
  br i1 %cmp41, label %if.then43, label %if.end44

if.then43:                                        ; preds = %if.then39
  store i8 -1, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end44:                                         ; preds = %if.then39
  %79 = bitcast i32* %pv_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #5
  %80 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %conv45 = zext i8 %80 to i32
  %cmp46 = icmp eq i32 0, %conv45
  br i1 %cmp46, label %cond.true48, label %cond.false49

cond.true48:                                      ; preds = %if.end44
  %81 = load i32, i32* %mi_row, align 4, !tbaa !6
  br label %cond.end52

cond.false49:                                     ; preds = %if.end44
  %82 = load i32, i32* %mi_row, align 4, !tbaa !6
  %83 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shl50 = shl i32 1, %83
  %sub51 = sub nsw i32 %82, %shl50
  br label %cond.end52

cond.end52:                                       ; preds = %cond.false49, %cond.true48
  %cond53 = phi i32 [ %81, %cond.true48 ], [ %sub51, %cond.false49 ]
  store i32 %cond53, i32* %pv_row, align 4, !tbaa !6
  %84 = bitcast i32* %pv_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #5
  %85 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %conv54 = zext i8 %85 to i32
  %cmp55 = icmp eq i32 0, %conv54
  br i1 %cmp55, label %cond.true57, label %cond.false60

cond.true57:                                      ; preds = %cond.end52
  %86 = load i32, i32* %mi_col, align 4, !tbaa !6
  %87 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shl58 = shl i32 1, %87
  %sub59 = sub nsw i32 %86, %shl58
  br label %cond.end61

cond.false60:                                     ; preds = %cond.end52
  %88 = load i32, i32* %mi_col, align 4, !tbaa !6
  br label %cond.end61

cond.end61:                                       ; preds = %cond.false60, %cond.true57
  %cond62 = phi i32 [ %sub59, %cond.true57 ], [ %88, %cond.false60 ]
  store i32 %cond62, i32* %pv_col, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %pv_ts) #5
  %89 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %90 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi_prev, align 4, !tbaa !2
  %91 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %92 = load i32, i32* %pv_row, align 4, !tbaa !6
  %93 = load i32, i32* %pv_col, align 4, !tbaa !6
  %94 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %95 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %call63 = call zeroext i8 @get_transform_size(%struct.macroblockd* %89, %struct.MB_MODE_INFO* %90, i8 zeroext %91, i32 %92, i32 %93, i32 %94, %struct.macroblockd_plane* %95)
  store i8 %call63, i8* %pv_ts, align 1, !tbaa !33
  %96 = bitcast i32* %pv_lvl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #5
  %97 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %98 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf_info64 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %98, i32 0, i32 27
  %99 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %conv65 = zext i8 %99 to i32
  %100 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %101 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi_prev, align 4, !tbaa !2
  %call66 = call zeroext i8 @av1_get_filter_level(%struct.AV1Common* %97, %struct.loop_filter_info_n* %lf_info64, i32 %conv65, i32 %100, %struct.MB_MODE_INFO* %101)
  %conv67 = zext i8 %call66 to i32
  store i32 %conv67, i32* %pv_lvl, align 4, !tbaa !6
  %102 = bitcast i32* %pv_skip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #5
  %103 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi_prev, align 4, !tbaa !2
  %skip68 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %103, i32 0, i32 14
  %104 = load i8, i8* %skip68, align 4, !tbaa !67
  %conv69 = sext i8 %104 to i32
  %tobool70 = icmp ne i32 %conv69, 0
  br i1 %tobool70, label %land.rhs71, label %land.end74

land.rhs71:                                       ; preds = %cond.end61
  %105 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi_prev, align 4, !tbaa !2
  %call72 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %105)
  %tobool73 = icmp ne i32 %call72, 0
  br label %land.end74

land.end74:                                       ; preds = %land.rhs71, %cond.end61
  %106 = phi i1 [ false, %cond.end61 ], [ %tobool73, %land.rhs71 ]
  %land.ext75 = zext i1 %106 to i32
  store i32 %land.ext75, i32* %pv_skip, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #5
  %107 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %107, i32 0, i32 6
  %108 = load i8, i8* %sb_type, align 2, !tbaa !68
  %109 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_x76 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %109, i32 0, i32 4
  %110 = load i32, i32* %subsampling_x76, align 4, !tbaa !49
  %111 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_y77 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %111, i32 0, i32 5
  %112 = load i32, i32* %subsampling_y77, align 4, !tbaa !52
  %call78 = call zeroext i8 @get_plane_block_size(i8 zeroext %108, i32 %110, i32 %112)
  store i8 %call78, i8* %bsize, align 1, !tbaa !33
  %113 = bitcast i32* %prediction_masks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #5
  %114 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %conv79 = zext i8 %114 to i32
  %cmp80 = icmp eq i32 %conv79, 0
  br i1 %cmp80, label %cond.true82, label %cond.false87

cond.true82:                                      ; preds = %land.end74
  %115 = load i8, i8* %bsize, align 1, !tbaa !33
  %idxprom83 = zext i8 %115 to i32
  %arrayidx84 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom83
  %116 = load i8, i8* %arrayidx84, align 1, !tbaa !33
  %conv85 = zext i8 %116 to i32
  %sub86 = sub nsw i32 %conv85, 1
  br label %cond.end92

cond.false87:                                     ; preds = %land.end74
  %117 = load i8, i8* %bsize, align 1, !tbaa !33
  %idxprom88 = zext i8 %117 to i32
  %arrayidx89 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom88
  %118 = load i8, i8* %arrayidx89, align 1, !tbaa !33
  %conv90 = zext i8 %118 to i32
  %sub91 = sub nsw i32 %conv90, 1
  br label %cond.end92

cond.end92:                                       ; preds = %cond.false87, %cond.true82
  %cond93 = phi i32 [ %sub86, %cond.true82 ], [ %sub91, %cond.false87 ]
  store i32 %cond93, i32* %prediction_masks, align 4, !tbaa !6
  %119 = bitcast i32* %pu_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #5
  %120 = load i32, i32* %coord, align 4, !tbaa !6
  %121 = load i32, i32* %prediction_masks, align 4, !tbaa !6
  %and94 = and i32 %120, %121
  %tobool95 = icmp ne i32 %and94, 0
  %lnot = xor i1 %tobool95, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %pu_edge, align 4, !tbaa !6
  %122 = load i32, i32* %curr_level, align 4, !tbaa !6
  %tobool96 = icmp ne i32 %122, 0
  br i1 %tobool96, label %land.lhs.true, label %lor.lhs.false97

lor.lhs.false97:                                  ; preds = %cond.end92
  %123 = load i32, i32* %pv_lvl, align 4, !tbaa !6
  %tobool98 = icmp ne i32 %123, 0
  br i1 %tobool98, label %land.lhs.true, label %if.end146

land.lhs.true:                                    ; preds = %lor.lhs.false97, %cond.end92
  %124 = load i32, i32* %pv_skip, align 4, !tbaa !6
  %tobool99 = icmp ne i32 %124, 0
  br i1 %tobool99, label %lor.lhs.false100, label %if.then104

lor.lhs.false100:                                 ; preds = %land.lhs.true
  %125 = load i32, i32* %curr_skipped, align 4, !tbaa !6
  %tobool101 = icmp ne i32 %125, 0
  br i1 %tobool101, label %lor.lhs.false102, label %if.then104

lor.lhs.false102:                                 ; preds = %lor.lhs.false100
  %126 = load i32, i32* %pu_edge, align 4, !tbaa !6
  %tobool103 = icmp ne i32 %126, 0
  br i1 %tobool103, label %if.then104, label %if.end146

if.then104:                                       ; preds = %lor.lhs.false102, %lor.lhs.false100, %land.lhs.true
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %min_ts) #5
  %127 = load i8, i8* %ts, align 1, !tbaa !33
  %conv105 = zext i8 %127 to i32
  %128 = load i8, i8* %pv_ts, align 1, !tbaa !33
  %conv106 = zext i8 %128 to i32
  %cmp107 = icmp slt i32 %conv105, %conv106
  br i1 %cmp107, label %cond.true109, label %cond.false111

cond.true109:                                     ; preds = %if.then104
  %129 = load i8, i8* %ts, align 1, !tbaa !33
  %conv110 = zext i8 %129 to i32
  br label %cond.end113

cond.false111:                                    ; preds = %if.then104
  %130 = load i8, i8* %pv_ts, align 1, !tbaa !33
  %conv112 = zext i8 %130 to i32
  br label %cond.end113

cond.end113:                                      ; preds = %cond.false111, %cond.true109
  %cond114 = phi i32 [ %conv110, %cond.true109 ], [ %conv112, %cond.false111 ]
  %conv115 = trunc i32 %cond114 to i8
  store i8 %conv115, i8* %min_ts, align 1, !tbaa !33
  %131 = load i8, i8* %min_ts, align 1, !tbaa !33
  %conv116 = zext i8 %131 to i32
  %cmp117 = icmp sge i32 0, %conv116
  br i1 %cmp117, label %if.then119, label %if.else

if.then119:                                       ; preds = %cond.end113
  %132 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %filter_length120 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %132, i32 0, i32 0
  store i32 4, i32* %filter_length120, align 4, !tbaa !55
  br label %if.end140

if.else:                                          ; preds = %cond.end113
  %133 = load i8, i8* %min_ts, align 1, !tbaa !33
  %conv121 = zext i8 %133 to i32
  %cmp122 = icmp eq i32 1, %conv121
  br i1 %cmp122, label %if.then124, label %if.else132

if.then124:                                       ; preds = %if.else
  %134 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp125 = icmp ne i32 %134, 0
  br i1 %cmp125, label %if.then127, label %if.else129

if.then127:                                       ; preds = %if.then124
  %135 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %filter_length128 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %135, i32 0, i32 0
  store i32 6, i32* %filter_length128, align 4, !tbaa !55
  br label %if.end131

if.else129:                                       ; preds = %if.then124
  %136 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %filter_length130 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %136, i32 0, i32 0
  store i32 8, i32* %filter_length130, align 4, !tbaa !55
  br label %if.end131

if.end131:                                        ; preds = %if.else129, %if.then127
  br label %if.end139

if.else132:                                       ; preds = %if.else
  %137 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %filter_length133 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %137, i32 0, i32 0
  store i32 14, i32* %filter_length133, align 4, !tbaa !55
  %138 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp134 = icmp ne i32 %138, 0
  br i1 %cmp134, label %if.then136, label %if.end138

if.then136:                                       ; preds = %if.else132
  %139 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %filter_length137 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %139, i32 0, i32 0
  store i32 6, i32* %filter_length137, align 4, !tbaa !55
  br label %if.end138

if.end138:                                        ; preds = %if.then136, %if.else132
  br label %if.end139

if.end139:                                        ; preds = %if.end138, %if.end131
  br label %if.end140

if.end140:                                        ; preds = %if.end139, %if.then119
  %140 = load i32, i32* %curr_level, align 4, !tbaa !6
  %tobool141 = icmp ne i32 %140, 0
  br i1 %tobool141, label %cond.true142, label %cond.false143

cond.true142:                                     ; preds = %if.end140
  %141 = load i32, i32* %curr_level, align 4, !tbaa !6
  br label %cond.end144

cond.false143:                                    ; preds = %if.end140
  %142 = load i32, i32* %pv_lvl, align 4, !tbaa !6
  br label %cond.end144

cond.end144:                                      ; preds = %cond.false143, %cond.true142
  %cond145 = phi i32 [ %141, %cond.true142 ], [ %142, %cond.false143 ]
  store i32 %cond145, i32* %level, align 4, !tbaa !6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %min_ts) #5
  br label %if.end146

if.end146:                                        ; preds = %cond.end144, %lor.lhs.false102, %lor.lhs.false97
  %143 = bitcast i32* %pu_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #5
  %144 = bitcast i32* %prediction_masks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #5
  %145 = bitcast i32* %pv_skip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #5
  %146 = bitcast i32* %pv_lvl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %pv_ts) #5
  %147 = bitcast i32* %pv_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #5
  %148 = bitcast i32* %pv_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end146, %if.then43
  %149 = bitcast %struct.MB_MODE_INFO** %mi_prev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup160 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end147

if.end147:                                        ; preds = %cleanup.cont, %land.end
  %150 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %filter_length148 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %150, i32 0, i32 0
  %151 = load i32, i32* %filter_length148, align 4, !tbaa !55
  %tobool149 = icmp ne i32 %151, 0
  br i1 %tobool149, label %if.then150, label %if.end159

if.then150:                                       ; preds = %if.end147
  %152 = bitcast %struct.loop_filter_thresh** %limits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %152) #5
  %153 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf_info151 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %153, i32 0, i32 27
  %lfthr = getelementptr inbounds %struct.loop_filter_info_n, %struct.loop_filter_info_n* %lf_info151, i32 0, i32 0
  %arraydecay = getelementptr inbounds [64 x %struct.loop_filter_thresh], [64 x %struct.loop_filter_thresh]* %lfthr, i32 0, i32 0
  %154 = load i32, i32* %level, align 4, !tbaa !6
  %add.ptr152 = getelementptr inbounds %struct.loop_filter_thresh, %struct.loop_filter_thresh* %arraydecay, i32 %154
  store %struct.loop_filter_thresh* %add.ptr152, %struct.loop_filter_thresh** %limits, align 4, !tbaa !2
  %155 = load %struct.loop_filter_thresh*, %struct.loop_filter_thresh** %limits, align 4, !tbaa !2
  %lim = getelementptr inbounds %struct.loop_filter_thresh, %struct.loop_filter_thresh* %155, i32 0, i32 1
  %arraydecay153 = getelementptr inbounds [16 x i8], [16 x i8]* %lim, i32 0, i32 0
  %156 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %lim154 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %156, i32 0, i32 1
  store i8* %arraydecay153, i8** %lim154, align 4, !tbaa !60
  %157 = load %struct.loop_filter_thresh*, %struct.loop_filter_thresh** %limits, align 4, !tbaa !2
  %mblim = getelementptr inbounds %struct.loop_filter_thresh, %struct.loop_filter_thresh* %157, i32 0, i32 0
  %arraydecay155 = getelementptr inbounds [16 x i8], [16 x i8]* %mblim, i32 0, i32 0
  %158 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %mblim156 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %158, i32 0, i32 2
  store i8* %arraydecay155, i8** %mblim156, align 4, !tbaa !59
  %159 = load %struct.loop_filter_thresh*, %struct.loop_filter_thresh** %limits, align 4, !tbaa !2
  %hev_thr = getelementptr inbounds %struct.loop_filter_thresh, %struct.loop_filter_thresh* %159, i32 0, i32 2
  %arraydecay157 = getelementptr inbounds [16 x i8], [16 x i8]* %hev_thr, i32 0, i32 0
  %160 = load %struct.AV1_DEBLOCKING_PARAMETERS*, %struct.AV1_DEBLOCKING_PARAMETERS** %params.addr, align 4, !tbaa !2
  %hev_thr158 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %160, i32 0, i32 3
  store i8* %arraydecay157, i8** %hev_thr158, align 4, !tbaa !61
  %161 = bitcast %struct.loop_filter_thresh** %limits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #5
  br label %if.end159

if.end159:                                        ; preds = %if.then150, %if.end147
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup160

cleanup160:                                       ; preds = %if.end159, %cleanup
  %162 = bitcast i32* %level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #5
  %163 = bitcast i32* %curr_skipped to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #5
  %164 = bitcast i32* %curr_level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #5
  %cleanup.dest163 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest163, label %cleanup165 [
    i32 0, label %cleanup.cont164
  ]

cleanup.cont164:                                  ; preds = %cleanup160
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup165

cleanup165:                                       ; preds = %cleanup.cont164, %cleanup160, %if.then29
  %165 = bitcast i32* %tu_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #5
  %166 = bitcast i32* %transform_masks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #5
  %167 = bitcast i32* %coord to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #5
  %cleanup.dest168 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest168, label %cleanup170 [
    i32 0, label %cleanup.cont169
  ]

cleanup.cont169:                                  ; preds = %cleanup165
  %168 = load i8, i8* %ts, align 1, !tbaa !33
  store i8 %168, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup170

cleanup170:                                       ; preds = %cleanup.cont169, %cleanup165
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ts) #5
  br label %cleanup171

cleanup171:                                       ; preds = %cleanup170, %if.then11
  %169 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #5
  %170 = bitcast %struct.MB_MODE_INFO*** %mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #5
  %171 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #5
  %172 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #5
  %173 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #5
  %174 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #5
  br label %cleanup177

cleanup177:                                       ; preds = %cleanup171, %if.then
  %175 = bitcast i32* %height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #5
  %176 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #5
  %177 = load i8, i8* %retval, align 1
  ret i8 %177
}

declare void @aom_highbd_lpf_vertical_4_c(i16*, i32, i8*, i8*, i8*, i32) #4

declare void @aom_lpf_vertical_4_c(i8*, i32, i8*, i8*, i8*) #4

declare void @aom_highbd_lpf_vertical_6_c(i16*, i32, i8*, i8*, i8*, i32) #4

declare void @aom_lpf_vertical_6_c(i8*, i32, i8*, i8*, i8*) #4

declare void @aom_highbd_lpf_vertical_8_c(i16*, i32, i8*, i8*, i8*, i32) #4

declare void @aom_lpf_vertical_8_c(i8*, i32, i8*, i8*, i8*) #4

declare void @aom_highbd_lpf_vertical_14_c(i16*, i32, i8*, i8*, i8*, i32) #4

declare void @aom_lpf_vertical_14_c(i8*, i32, i8*, i8*, i8*) #4

; Function Attrs: nounwind
define hidden void @av1_filter_block_plane_horz(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane, %struct.macroblockd_plane* %plane_ptr, i32 %mi_row, i32 %mi_col) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %plane_ptr.addr = alloca %struct.macroblockd_plane*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %scale_horz = alloca i32, align 4
  %scale_vert = alloca i32, align 4
  %dst_ptr = alloca i8*, align 4
  %dst_stride = alloca i32, align 4
  %y_range = alloca i32, align 4
  %x_range = alloca i32, align 4
  %x = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %p = alloca i8*, align 4
  %y = alloca i32, align 4
  %curr_x = alloca i32, align 4
  %curr_y = alloca i32, align 4
  %advance_units = alloca i32, align 4
  %tx_size = alloca i8, align 1
  %params = alloca %struct.AV1_DEBLOCKING_PARAMETERS, align 4
  %use_highbitdepth = alloca i32, align 4
  %bit_depth = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.macroblockd_plane* %plane_ptr, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %1, i32 0, i32 4
  %2 = load i32, i32* %subsampling_x, align 4, !tbaa !49
  store i32 %2, i32* %scale_horz, align 4, !tbaa !6
  %3 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %4, i32 0, i32 5
  %5 = load i32, i32* %subsampling_y, align 4, !tbaa !52
  store i32 %5, i32* %scale_vert, align 4, !tbaa !6
  %6 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %7, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 0
  %8 = load i8*, i8** %buf, align 4, !tbaa !53
  store i8* %8, i8** %dst_ptr, align 4, !tbaa !2
  %9 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %10, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst1, i32 0, i32 4
  %11 = load i32, i32* %stride, align 4, !tbaa !54
  store i32 %11, i32* %dst_stride, align 4, !tbaa !6
  %12 = bitcast i32* %y_range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shr = ashr i32 32, %13
  store i32 %shr, i32* %y_range, align 4, !tbaa !6
  %14 = bitcast i32* %x_range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shr2 = ashr i32 32, %15
  store i32 %shr2, i32* %x_range, align 4, !tbaa !6
  %16 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %17 = load i32, i32* %x, align 4, !tbaa !6
  %18 = load i32, i32* %x_range, align 4, !tbaa !6
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  br label %for.end67

for.body:                                         ; preds = %for.cond
  %20 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  %21 = load i8*, i8** %dst_ptr, align 4, !tbaa !2
  %22 = load i32, i32* %x, align 4, !tbaa !6
  %mul = mul nsw i32 %22, 4
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 %mul
  store i8* %add.ptr, i8** %p, align 4, !tbaa !2
  %23 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %sw.epilog, %for.body
  %24 = load i32, i32* %y, align 4, !tbaa !6
  %25 = load i32, i32* %y_range, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %24, %25
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  %26 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  br label %for.end

for.body6:                                        ; preds = %for.cond3
  %27 = bitcast i32* %curr_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  %28 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %mul7 = mul i32 %28, 4
  %29 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shr8 = lshr i32 %mul7, %29
  %30 = load i32, i32* %x, align 4, !tbaa !6
  %mul9 = mul nsw i32 %30, 4
  %add = add i32 %shr8, %mul9
  store i32 %add, i32* %curr_x, align 4, !tbaa !6
  %31 = bitcast i32* %curr_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #5
  %32 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %mul10 = mul i32 %32, 4
  %33 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shr11 = lshr i32 %mul10, %33
  %34 = load i32, i32* %y, align 4, !tbaa !6
  %mul12 = mul nsw i32 %34, 4
  %add13 = add i32 %shr11, %mul12
  store i32 %add13, i32* %curr_y, align 4, !tbaa !6
  %35 = bitcast i32* %advance_units to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size) #5
  %36 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #5
  %37 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %37, i8 0, i32 16, i1 false)
  %38 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %38, i32 0, i32 22
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 11
  %39 = load i32, i32* %mi_stride, align 4, !tbaa !66
  %40 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shl = shl i32 %39, %40
  %41 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %42 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %43 = load i32, i32* %curr_x, align 4, !tbaa !6
  %44 = load i32, i32* %curr_y, align 4, !tbaa !6
  %45 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %46 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %call = call zeroext i8 @set_lpf_parameters(%struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 %shl, %struct.AV1Common* %41, %struct.macroblockd* %42, i8 zeroext 1, i32 %43, i32 %44, i32 %45, %struct.macroblockd_plane* %46)
  store i8 %call, i8* %tx_size, align 1, !tbaa !33
  %47 = load i8, i8* %tx_size, align 1, !tbaa !33
  %conv = zext i8 %47 to i32
  %cmp14 = icmp eq i32 %conv, 255
  br i1 %cmp14, label %if.then, label %if.end

if.then:                                          ; preds = %for.body6
  %filter_length = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 0
  store i32 0, i32* %filter_length, align 4, !tbaa !55
  store i8 0, i8* %tx_size, align 1, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body6
  %48 = bitcast i32* %use_highbitdepth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #5
  %49 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %49, i32 0, i32 37
  %use_highbitdepth16 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 26
  %50 = load i8, i8* %use_highbitdepth16, align 4, !tbaa !57
  %conv17 = zext i8 %50 to i32
  store i32 %conv17, i32* %use_highbitdepth, align 4, !tbaa !6
  %51 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #5
  %52 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params18 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %52, i32 0, i32 37
  %bit_depth19 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params18, i32 0, i32 25
  %53 = load i32, i32* %bit_depth19, align 8, !tbaa !58
  store i32 %53, i32* %bit_depth, align 4, !tbaa !33
  %filter_length20 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 0
  %54 = load i32, i32* %filter_length20, align 4, !tbaa !55
  switch i32 %54, label %sw.default [
    i32 4, label %sw.bb
    i32 6, label %sw.bb27
    i32 8, label %sw.bb39
    i32 14, label %sw.bb51
  ]

sw.bb:                                            ; preds = %if.end
  %55 = load i32, i32* %use_highbitdepth, align 4, !tbaa !6
  %tobool = icmp ne i32 %55, 0
  br i1 %tobool, label %if.then21, label %if.else

if.then21:                                        ; preds = %sw.bb
  %56 = load i8*, i8** %p, align 4, !tbaa !2
  %57 = ptrtoint i8* %56 to i32
  %shl22 = shl i32 %57, 1
  %58 = inttoptr i32 %shl22 to i16*
  %59 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %60 = load i8*, i8** %mblim, align 4, !tbaa !59
  %lim = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %61 = load i8*, i8** %lim, align 4, !tbaa !60
  %hev_thr = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %62 = load i8*, i8** %hev_thr, align 4, !tbaa !61
  %63 = load i32, i32* %bit_depth, align 4, !tbaa !33
  call void @aom_highbd_lpf_horizontal_4_c(i16* %58, i32 %59, i8* %60, i8* %61, i8* %62, i32 %63)
  br label %if.end26

if.else:                                          ; preds = %sw.bb
  %64 = load i8*, i8** %p, align 4, !tbaa !2
  %65 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim23 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %66 = load i8*, i8** %mblim23, align 4, !tbaa !59
  %lim24 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %67 = load i8*, i8** %lim24, align 4, !tbaa !60
  %hev_thr25 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %68 = load i8*, i8** %hev_thr25, align 4, !tbaa !61
  call void @aom_lpf_horizontal_4_c(i8* %64, i32 %65, i8* %66, i8* %67, i8* %68)
  br label %if.end26

if.end26:                                         ; preds = %if.else, %if.then21
  br label %sw.epilog

sw.bb27:                                          ; preds = %if.end
  %69 = load i32, i32* %use_highbitdepth, align 4, !tbaa !6
  %tobool28 = icmp ne i32 %69, 0
  br i1 %tobool28, label %if.then29, label %if.else34

if.then29:                                        ; preds = %sw.bb27
  %70 = load i8*, i8** %p, align 4, !tbaa !2
  %71 = ptrtoint i8* %70 to i32
  %shl30 = shl i32 %71, 1
  %72 = inttoptr i32 %shl30 to i16*
  %73 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim31 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %74 = load i8*, i8** %mblim31, align 4, !tbaa !59
  %lim32 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %75 = load i8*, i8** %lim32, align 4, !tbaa !60
  %hev_thr33 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %76 = load i8*, i8** %hev_thr33, align 4, !tbaa !61
  %77 = load i32, i32* %bit_depth, align 4, !tbaa !33
  call void @aom_highbd_lpf_horizontal_6_c(i16* %72, i32 %73, i8* %74, i8* %75, i8* %76, i32 %77)
  br label %if.end38

if.else34:                                        ; preds = %sw.bb27
  %78 = load i8*, i8** %p, align 4, !tbaa !2
  %79 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim35 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %80 = load i8*, i8** %mblim35, align 4, !tbaa !59
  %lim36 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %81 = load i8*, i8** %lim36, align 4, !tbaa !60
  %hev_thr37 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %82 = load i8*, i8** %hev_thr37, align 4, !tbaa !61
  call void @aom_lpf_horizontal_6_c(i8* %78, i32 %79, i8* %80, i8* %81, i8* %82)
  br label %if.end38

if.end38:                                         ; preds = %if.else34, %if.then29
  br label %sw.epilog

sw.bb39:                                          ; preds = %if.end
  %83 = load i32, i32* %use_highbitdepth, align 4, !tbaa !6
  %tobool40 = icmp ne i32 %83, 0
  br i1 %tobool40, label %if.then41, label %if.else46

if.then41:                                        ; preds = %sw.bb39
  %84 = load i8*, i8** %p, align 4, !tbaa !2
  %85 = ptrtoint i8* %84 to i32
  %shl42 = shl i32 %85, 1
  %86 = inttoptr i32 %shl42 to i16*
  %87 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim43 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %88 = load i8*, i8** %mblim43, align 4, !tbaa !59
  %lim44 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %89 = load i8*, i8** %lim44, align 4, !tbaa !60
  %hev_thr45 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %90 = load i8*, i8** %hev_thr45, align 4, !tbaa !61
  %91 = load i32, i32* %bit_depth, align 4, !tbaa !33
  call void @aom_highbd_lpf_horizontal_8_c(i16* %86, i32 %87, i8* %88, i8* %89, i8* %90, i32 %91)
  br label %if.end50

if.else46:                                        ; preds = %sw.bb39
  %92 = load i8*, i8** %p, align 4, !tbaa !2
  %93 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim47 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %94 = load i8*, i8** %mblim47, align 4, !tbaa !59
  %lim48 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %95 = load i8*, i8** %lim48, align 4, !tbaa !60
  %hev_thr49 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %96 = load i8*, i8** %hev_thr49, align 4, !tbaa !61
  call void @aom_lpf_horizontal_8_c(i8* %92, i32 %93, i8* %94, i8* %95, i8* %96)
  br label %if.end50

if.end50:                                         ; preds = %if.else46, %if.then41
  br label %sw.epilog

sw.bb51:                                          ; preds = %if.end
  %97 = load i32, i32* %use_highbitdepth, align 4, !tbaa !6
  %tobool52 = icmp ne i32 %97, 0
  br i1 %tobool52, label %if.then53, label %if.else58

if.then53:                                        ; preds = %sw.bb51
  %98 = load i8*, i8** %p, align 4, !tbaa !2
  %99 = ptrtoint i8* %98 to i32
  %shl54 = shl i32 %99, 1
  %100 = inttoptr i32 %shl54 to i16*
  %101 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim55 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %102 = load i8*, i8** %mblim55, align 4, !tbaa !59
  %lim56 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %103 = load i8*, i8** %lim56, align 4, !tbaa !60
  %hev_thr57 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %104 = load i8*, i8** %hev_thr57, align 4, !tbaa !61
  %105 = load i32, i32* %bit_depth, align 4, !tbaa !33
  call void @aom_highbd_lpf_horizontal_14_c(i16* %100, i32 %101, i8* %102, i8* %103, i8* %104, i32 %105)
  br label %if.end62

if.else58:                                        ; preds = %sw.bb51
  %106 = load i8*, i8** %p, align 4, !tbaa !2
  %107 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mblim59 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 2
  %108 = load i8*, i8** %mblim59, align 4, !tbaa !59
  %lim60 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 1
  %109 = load i8*, i8** %lim60, align 4, !tbaa !60
  %hev_thr61 = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 3
  %110 = load i8*, i8** %hev_thr61, align 4, !tbaa !61
  call void @aom_lpf_horizontal_14_c(i8* %106, i32 %107, i8* %108, i8* %109, i8* %110)
  br label %if.end62

if.end62:                                         ; preds = %if.else58, %if.then53
  br label %sw.epilog

sw.default:                                       ; preds = %if.end
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end62, %if.end50, %if.end38, %if.end26
  %111 = load i8, i8* %tx_size, align 1, !tbaa !33
  %idxprom = zext i8 %111 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_unit, i32 0, i32 %idxprom
  %112 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %112, i32* %advance_units, align 4, !tbaa !6
  %113 = load i32, i32* %advance_units, align 4, !tbaa !6
  %114 = load i32, i32* %y, align 4, !tbaa !6
  %add63 = add i32 %114, %113
  store i32 %add63, i32* %y, align 4, !tbaa !6
  %115 = load i32, i32* %advance_units, align 4, !tbaa !6
  %116 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mul64 = mul i32 %115, %116
  %mul65 = mul i32 %mul64, 4
  %117 = load i8*, i8** %p, align 4, !tbaa !2
  %add.ptr66 = getelementptr inbounds i8, i8* %117, i32 %mul65
  store i8* %add.ptr66, i8** %p, align 4, !tbaa !2
  %118 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %use_highbitdepth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %120) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size) #5
  %121 = bitcast i32* %advance_units to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  %122 = bitcast i32* %curr_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  %123 = bitcast i32* %curr_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  br label %for.cond3

for.end:                                          ; preds = %for.cond.cleanup5
  %124 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  br label %for.inc

for.inc:                                          ; preds = %for.end
  %125 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %125, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.end67:                                        ; preds = %for.cond.cleanup
  %126 = bitcast i32* %x_range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #5
  %127 = bitcast i32* %y_range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #5
  %128 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #5
  %129 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #5
  %130 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #5
  %131 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #5
  ret void
}

declare void @aom_highbd_lpf_horizontal_4_c(i16*, i32, i8*, i8*, i8*, i32) #4

declare void @aom_lpf_horizontal_4_c(i8*, i32, i8*, i8*, i8*) #4

declare void @aom_highbd_lpf_horizontal_6_c(i16*, i32, i8*, i8*, i8*, i32) #4

declare void @aom_lpf_horizontal_6_c(i8*, i32, i8*, i8*, i8*) #4

declare void @aom_highbd_lpf_horizontal_8_c(i16*, i32, i8*, i8*, i8*, i32) #4

declare void @aom_lpf_horizontal_8_c(i8*, i32, i8*, i8*, i8*) #4

declare void @aom_highbd_lpf_horizontal_14_c(i16*, i32, i8*, i8*, i8*, i32) #4

declare void @aom_lpf_horizontal_14_c(i8*, i32, i8*, i8*, i8*) #4

; Function Attrs: nounwind
define hidden void @av1_filter_block_plane_vert_test(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane, %struct.macroblockd_plane* %plane_ptr, i32 %mi_row, i32 %mi_col) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %plane_ptr.addr = alloca %struct.macroblockd_plane*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %scale_horz = alloca i32, align 4
  %scale_vert = alloca i32, align 4
  %dst_ptr = alloca i8*, align 4
  %dst_stride = alloca i32, align 4
  %y_range = alloca i32, align 4
  %x_range = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %p = alloca i8*, align 4
  %x = alloca i32, align 4
  %curr_x = alloca i32, align 4
  %curr_y = alloca i32, align 4
  %advance_units = alloca i32, align 4
  %tx_size = alloca i8, align 1
  %params = alloca %struct.AV1_DEBLOCKING_PARAMETERS, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.macroblockd_plane* %plane_ptr, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %1, i32 0, i32 4
  %2 = load i32, i32* %subsampling_x, align 4, !tbaa !49
  store i32 %2, i32* %scale_horz, align 4, !tbaa !6
  %3 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %4, i32 0, i32 5
  %5 = load i32, i32* %subsampling_y, align 4, !tbaa !52
  store i32 %5, i32* %scale_vert, align 4, !tbaa !6
  %6 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %7, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 0
  %8 = load i8*, i8** %buf, align 4, !tbaa !53
  store i8* %8, i8** %dst_ptr, align 4, !tbaa !2
  %9 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %10, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst1, i32 0, i32 4
  %11 = load i32, i32* %stride, align 4, !tbaa !54
  store i32 %11, i32* %dst_stride, align 4, !tbaa !6
  %12 = bitcast i32* %y_range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %14 = load i32, i32* %mi_rows, align 4, !tbaa !69
  %15 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shr = ashr i32 %14, %15
  store i32 %shr, i32* %y_range, align 4, !tbaa !6
  %16 = bitcast i32* %x_range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params2, i32 0, i32 4
  %18 = load i32, i32* %mi_cols, align 4, !tbaa !70
  %19 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shr3 = ashr i32 %18, %19
  store i32 %shr3, i32* %x_range, align 4, !tbaa !6
  %20 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %21 = load i32, i32* %y, align 4, !tbaa !6
  %22 = load i32, i32* %y_range, align 4, !tbaa !6
  %cmp = icmp slt i32 %21, %22
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  br label %for.end21

for.body:                                         ; preds = %for.cond
  %24 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  %25 = load i8*, i8** %dst_ptr, align 4, !tbaa !2
  %26 = load i32, i32* %y, align 4, !tbaa !6
  %mul = mul nsw i32 %26, 4
  %27 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mul4 = mul nsw i32 %mul, %27
  %add.ptr = getelementptr inbounds i8, i8* %25, i32 %mul4
  store i8* %add.ptr, i8** %p, align 4, !tbaa !2
  %28 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %if.end, %for.body
  %29 = load i32, i32* %x, align 4, !tbaa !6
  %30 = load i32, i32* %x_range, align 4, !tbaa !6
  %cmp6 = icmp slt i32 %29, %30
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond5
  store i32 5, i32* %cleanup.dest.slot, align 4
  %31 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #5
  br label %for.end

for.body8:                                        ; preds = %for.cond5
  %32 = bitcast i32* %curr_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  %33 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %mul9 = mul i32 %33, 4
  %34 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shr10 = lshr i32 %mul9, %34
  %35 = load i32, i32* %x, align 4, !tbaa !6
  %mul11 = mul nsw i32 %35, 4
  %add = add i32 %shr10, %mul11
  store i32 %add, i32* %curr_x, align 4, !tbaa !6
  %36 = bitcast i32* %curr_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #5
  %37 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %mul12 = mul i32 %37, 4
  %38 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shr13 = lshr i32 %mul12, %38
  %39 = load i32, i32* %y, align 4, !tbaa !6
  %mul14 = mul nsw i32 %39, 4
  %add15 = add i32 %shr13, %mul14
  store i32 %add15, i32* %curr_y, align 4, !tbaa !6
  %40 = bitcast i32* %advance_units to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size) #5
  %41 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #5
  %42 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %42, i8 0, i32 16, i1 false)
  %43 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shl = shl i32 1, %43
  %44 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %45 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %46 = load i32, i32* %curr_x, align 4, !tbaa !6
  %47 = load i32, i32* %curr_y, align 4, !tbaa !6
  %48 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %49 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %call = call zeroext i8 @set_lpf_parameters(%struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 %shl, %struct.AV1Common* %44, %struct.macroblockd* %45, i8 zeroext 0, i32 %46, i32 %47, i32 %48, %struct.macroblockd_plane* %49)
  store i8 %call, i8* %tx_size, align 1, !tbaa !33
  %50 = load i8, i8* %tx_size, align 1, !tbaa !33
  %conv = zext i8 %50 to i32
  %cmp16 = icmp eq i32 %conv, 255
  br i1 %cmp16, label %if.then, label %if.end

if.then:                                          ; preds = %for.body8
  %filter_length = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 0
  store i32 0, i32* %filter_length, align 4, !tbaa !55
  store i8 0, i8* %tx_size, align 1, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body8
  %51 = load i8, i8* %tx_size, align 1, !tbaa !33
  %idxprom = zext i8 %51 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_unit, i32 0, i32 %idxprom
  %52 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %52, i32* %advance_units, align 4, !tbaa !6
  %53 = load i32, i32* %advance_units, align 4, !tbaa !6
  %54 = load i32, i32* %x, align 4, !tbaa !6
  %add18 = add i32 %54, %53
  store i32 %add18, i32* %x, align 4, !tbaa !6
  %55 = load i32, i32* %advance_units, align 4, !tbaa !6
  %mul19 = mul i32 %55, 4
  %56 = load i8*, i8** %p, align 4, !tbaa !2
  %add.ptr20 = getelementptr inbounds i8, i8* %56, i32 %mul19
  store i8* %add.ptr20, i8** %p, align 4, !tbaa !2
  %57 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size) #5
  %58 = bitcast i32* %advance_units to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #5
  %59 = bitcast i32* %curr_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #5
  %60 = bitcast i32* %curr_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #5
  br label %for.cond5

for.end:                                          ; preds = %for.cond.cleanup7
  %61 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #5
  br label %for.inc

for.inc:                                          ; preds = %for.end
  %62 = load i32, i32* %y, align 4, !tbaa !6
  %inc = add nsw i32 %62, 1
  store i32 %inc, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end21:                                        ; preds = %for.cond.cleanup
  %63 = bitcast i32* %x_range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  %64 = bitcast i32* %y_range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  %65 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %66 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_filter_block_plane_horz_test(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane, %struct.macroblockd_plane* %plane_ptr, i32 %mi_row, i32 %mi_col) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %plane_ptr.addr = alloca %struct.macroblockd_plane*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %scale_horz = alloca i32, align 4
  %scale_vert = alloca i32, align 4
  %dst_ptr = alloca i8*, align 4
  %dst_stride = alloca i32, align 4
  %y_range = alloca i32, align 4
  %x_range = alloca i32, align 4
  %x = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %p = alloca i8*, align 4
  %y = alloca i32, align 4
  %curr_x = alloca i32, align 4
  %curr_y = alloca i32, align 4
  %advance_units = alloca i32, align 4
  %tx_size = alloca i8, align 1
  %params = alloca %struct.AV1_DEBLOCKING_PARAMETERS, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.macroblockd_plane* %plane_ptr, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %1, i32 0, i32 4
  %2 = load i32, i32* %subsampling_x, align 4, !tbaa !49
  store i32 %2, i32* %scale_horz, align 4, !tbaa !6
  %3 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %4, i32 0, i32 5
  %5 = load i32, i32* %subsampling_y, align 4, !tbaa !52
  store i32 %5, i32* %scale_vert, align 4, !tbaa !6
  %6 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %7, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 0
  %8 = load i8*, i8** %buf, align 4, !tbaa !53
  store i8* %8, i8** %dst_ptr, align 4, !tbaa !2
  %9 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %10, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst1, i32 0, i32 4
  %11 = load i32, i32* %stride, align 4, !tbaa !54
  store i32 %11, i32* %dst_stride, align 4, !tbaa !6
  %12 = bitcast i32* %y_range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %14 = load i32, i32* %mi_rows, align 4, !tbaa !69
  %15 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shr = ashr i32 %14, %15
  store i32 %shr, i32* %y_range, align 4, !tbaa !6
  %16 = bitcast i32* %x_range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params2, i32 0, i32 4
  %18 = load i32, i32* %mi_cols, align 4, !tbaa !70
  %19 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shr3 = ashr i32 %18, %19
  store i32 %shr3, i32* %x_range, align 4, !tbaa !6
  %20 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %21 = load i32, i32* %x, align 4, !tbaa !6
  %22 = load i32, i32* %x_range, align 4, !tbaa !6
  %cmp = icmp slt i32 %21, %22
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  br label %for.end22

for.body:                                         ; preds = %for.cond
  %24 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  %25 = load i8*, i8** %dst_ptr, align 4, !tbaa !2
  %26 = load i32, i32* %x, align 4, !tbaa !6
  %mul = mul nsw i32 %26, 4
  %add.ptr = getelementptr inbounds i8, i8* %25, i32 %mul
  store i8* %add.ptr, i8** %p, align 4, !tbaa !2
  %27 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %if.end, %for.body
  %28 = load i32, i32* %y, align 4, !tbaa !6
  %29 = load i32, i32* %y_range, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %28, %29
  br i1 %cmp5, label %for.body7, label %for.cond.cleanup6

for.cond.cleanup6:                                ; preds = %for.cond4
  store i32 5, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  br label %for.end

for.body7:                                        ; preds = %for.cond4
  %31 = bitcast i32* %curr_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #5
  %32 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %mul8 = mul i32 %32, 4
  %33 = load i32, i32* %scale_horz, align 4, !tbaa !6
  %shr9 = lshr i32 %mul8, %33
  %34 = load i32, i32* %x, align 4, !tbaa !6
  %mul10 = mul nsw i32 %34, 4
  %add = add i32 %shr9, %mul10
  store i32 %add, i32* %curr_x, align 4, !tbaa !6
  %35 = bitcast i32* %curr_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #5
  %36 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %mul11 = mul i32 %36, 4
  %37 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shr12 = lshr i32 %mul11, %37
  %38 = load i32, i32* %y, align 4, !tbaa !6
  %mul13 = mul nsw i32 %38, 4
  %add14 = add i32 %shr12, %mul13
  store i32 %add14, i32* %curr_y, align 4, !tbaa !6
  %39 = bitcast i32* %advance_units to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size) #5
  %40 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #5
  %41 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %41, i8 0, i32 16, i1 false)
  %42 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params15 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %42, i32 0, i32 22
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params15, i32 0, i32 11
  %43 = load i32, i32* %mi_stride, align 4, !tbaa !66
  %44 = load i32, i32* %scale_vert, align 4, !tbaa !6
  %shl = shl i32 %43, %44
  %45 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %46 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %47 = load i32, i32* %curr_x, align 4, !tbaa !6
  %48 = load i32, i32* %curr_y, align 4, !tbaa !6
  %49 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %50 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %call = call zeroext i8 @set_lpf_parameters(%struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 %shl, %struct.AV1Common* %45, %struct.macroblockd* %46, i8 zeroext 1, i32 %47, i32 %48, i32 %49, %struct.macroblockd_plane* %50)
  store i8 %call, i8* %tx_size, align 1, !tbaa !33
  %51 = load i8, i8* %tx_size, align 1, !tbaa !33
  %conv = zext i8 %51 to i32
  %cmp16 = icmp eq i32 %conv, 255
  br i1 %cmp16, label %if.then, label %if.end

if.then:                                          ; preds = %for.body7
  %filter_length = getelementptr inbounds %struct.AV1_DEBLOCKING_PARAMETERS, %struct.AV1_DEBLOCKING_PARAMETERS* %params, i32 0, i32 0
  store i32 0, i32* %filter_length, align 4, !tbaa !55
  store i8 0, i8* %tx_size, align 1, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body7
  %52 = load i8, i8* %tx_size, align 1, !tbaa !33
  %idxprom = zext i8 %52 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_unit, i32 0, i32 %idxprom
  %53 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %53, i32* %advance_units, align 4, !tbaa !6
  %54 = load i32, i32* %advance_units, align 4, !tbaa !6
  %55 = load i32, i32* %y, align 4, !tbaa !6
  %add18 = add i32 %55, %54
  store i32 %add18, i32* %y, align 4, !tbaa !6
  %56 = load i32, i32* %advance_units, align 4, !tbaa !6
  %57 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mul19 = mul i32 %56, %57
  %mul20 = mul i32 %mul19, 4
  %58 = load i8*, i8** %p, align 4, !tbaa !2
  %add.ptr21 = getelementptr inbounds i8, i8* %58, i32 %mul20
  store i8* %add.ptr21, i8** %p, align 4, !tbaa !2
  %59 = bitcast %struct.AV1_DEBLOCKING_PARAMETERS* %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %59) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size) #5
  %60 = bitcast i32* %advance_units to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #5
  %61 = bitcast i32* %curr_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #5
  %62 = bitcast i32* %curr_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #5
  br label %for.cond4

for.end:                                          ; preds = %for.cond.cleanup6
  %63 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  br label %for.inc

for.inc:                                          ; preds = %for.end
  %64 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.end22:                                        ; preds = %for.cond.cleanup
  %65 = bitcast i32* %x_range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %66 = bitcast i32* %y_range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  %69 = bitcast i32* %scale_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #5
  %70 = bitcast i32* %scale_horz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_loop_filter_frame(%struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane_start, i32 %plane_end, i32 %partial_frame) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane_start.addr = alloca i32, align 4
  %plane_end.addr = alloca i32, align 4
  %partial_frame.addr = alloca i32, align 4
  %start_mi_row = alloca i32, align 4
  %end_mi_row = alloca i32, align 4
  %mi_rows_to_filter = alloca i32, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane_start, i32* %plane_start.addr, align 4, !tbaa !6
  store i32 %plane_end, i32* %plane_end.addr, align 4, !tbaa !6
  store i32 %partial_frame, i32* %partial_frame.addr, align 4, !tbaa !6
  %0 = bitcast i32* %start_mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %end_mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %mi_rows_to_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %start_mi_row, align 4, !tbaa !6
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %4 = load i32, i32* %mi_rows, align 4, !tbaa !69
  store i32 %4, i32* %mi_rows_to_filter, align 4, !tbaa !6
  %5 = load i32, i32* %partial_frame.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 22
  %mi_rows2 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params1, i32 0, i32 3
  %7 = load i32, i32* %mi_rows2, align 4, !tbaa !69
  %cmp = icmp sgt i32 %7, 8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params3 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 22
  %mi_rows4 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params3, i32 0, i32 3
  %9 = load i32, i32* %mi_rows4, align 4, !tbaa !69
  %shr = ashr i32 %9, 1
  store i32 %shr, i32* %start_mi_row, align 4, !tbaa !6
  %10 = load i32, i32* %start_mi_row, align 4, !tbaa !6
  %and = and i32 %10, -8
  store i32 %and, i32* %start_mi_row, align 4, !tbaa !6
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params5 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 22
  %mi_rows6 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params5, i32 0, i32 3
  %12 = load i32, i32* %mi_rows6, align 4, !tbaa !69
  %div = sdiv i32 %12, 8
  %cmp7 = icmp sgt i32 %div, 8
  br i1 %cmp7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 22
  %mi_rows9 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params8, i32 0, i32 3
  %14 = load i32, i32* %mi_rows9, align 4, !tbaa !69
  %div10 = sdiv i32 %14, 8
  br label %cond.end

cond.false:                                       ; preds = %if.then
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %div10, %cond.true ], [ 8, %cond.false ]
  store i32 %cond, i32* %mi_rows_to_filter, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %cond.end, %land.lhs.true, %entry
  %15 = load i32, i32* %start_mi_row, align 4, !tbaa !6
  %16 = load i32, i32* %mi_rows_to_filter, align 4, !tbaa !6
  %add = add nsw i32 %15, %16
  store i32 %add, i32* %end_mi_row, align 4, !tbaa !6
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %18 = load i32, i32* %plane_start.addr, align 4, !tbaa !6
  %19 = load i32, i32* %plane_end.addr, align 4, !tbaa !6
  call void @av1_loop_filter_frame_init(%struct.AV1Common* %17, i32 %18, i32 %19)
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %22 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %23 = load i32, i32* %start_mi_row, align 4, !tbaa !6
  %24 = load i32, i32* %end_mi_row, align 4, !tbaa !6
  %25 = load i32, i32* %plane_start.addr, align 4, !tbaa !6
  %26 = load i32, i32* %plane_end.addr, align 4, !tbaa !6
  call void @loop_filter_rows(%struct.yv12_buffer_config* %20, %struct.AV1Common* %21, %struct.macroblockd* %22, i32 %23, i32 %24, i32 %25, i32 %26)
  %27 = bitcast i32* %mi_rows_to_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %28 = bitcast i32* %end_mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i32* %start_mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  ret void
}

; Function Attrs: nounwind
define internal void @loop_filter_rows(%struct.yv12_buffer_config* %frame_buffer, %struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %start, i32 %stop, i32 %plane_start, i32 %plane_end) #0 {
entry:
  %frame_buffer.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %start.addr = alloca i32, align 4
  %stop.addr = alloca i32, align 4
  %plane_start.addr = alloca i32, align 4
  %plane_end.addr = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %col_start = alloca i32, align 4
  %col_end = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %plane1 = alloca i32, align 4
  store %struct.yv12_buffer_config* %frame_buffer, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %stop, i32* %stop.addr, align 4, !tbaa !6
  store i32 %plane_start, i32* %plane_start.addr, align 4, !tbaa !6
  store i32 %plane_end, i32* %plane_end.addr, align 4, !tbaa !6
  %0 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 4
  %arraydecay = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 0
  store %struct.macroblockd_plane* %arraydecay, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %2 = bitcast i32* %col_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %col_start, align 4, !tbaa !6
  %3 = bitcast i32* %col_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %5 = load i32, i32* %mi_cols, align 4, !tbaa !70
  store i32 %5, i32* %col_end, align 4, !tbaa !6
  %6 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = bitcast i32* %plane1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i32, i32* %plane_start.addr, align 4, !tbaa !6
  store i32 %9, i32* %plane1, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc84, %entry
  %10 = load i32, i32* %plane1, align 4, !tbaa !6
  %11 = load i32, i32* %plane_end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.end85

for.body:                                         ; preds = %for.cond
  %12 = load i32, i32* %plane1, align 4, !tbaa !6
  %cmp2 = icmp eq i32 %12, 0
  br i1 %cmp2, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.body
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 28
  %filter_level = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %filter_level, i32 0, i32 0
  %14 = load i32, i32* %arrayidx, align 16, !tbaa !6
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.else, label %land.lhs.true3

land.lhs.true3:                                   ; preds = %land.lhs.true
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 28
  %filter_level5 = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf4, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [2 x i32], [2 x i32]* %filter_level5, i32 0, i32 1
  %16 = load i32, i32* %arrayidx6, align 4, !tbaa !6
  %tobool7 = icmp ne i32 %16, 0
  br i1 %tobool7, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true3
  br label %for.end85

if.else:                                          ; preds = %land.lhs.true3, %land.lhs.true, %for.body
  %17 = load i32, i32* %plane1, align 4, !tbaa !6
  %cmp8 = icmp eq i32 %17, 1
  br i1 %cmp8, label %land.lhs.true9, label %if.else13

land.lhs.true9:                                   ; preds = %if.else
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf10 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 28
  %filter_level_u = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf10, i32 0, i32 1
  %19 = load i32, i32* %filter_level_u, align 8, !tbaa !40
  %tobool11 = icmp ne i32 %19, 0
  br i1 %tobool11, label %if.else13, label %if.then12

if.then12:                                        ; preds = %land.lhs.true9
  br label %for.inc84

if.else13:                                        ; preds = %land.lhs.true9, %if.else
  %20 = load i32, i32* %plane1, align 4, !tbaa !6
  %cmp14 = icmp eq i32 %20, 2
  br i1 %cmp14, label %land.lhs.true15, label %if.end

land.lhs.true15:                                  ; preds = %if.else13
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf16 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 28
  %filter_level_v = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf16, i32 0, i32 2
  %22 = load i32, i32* %filter_level_v, align 4, !tbaa !41
  %tobool17 = icmp ne i32 %22, 0
  br i1 %tobool17, label %if.end, label %if.then18

if.then18:                                        ; preds = %land.lhs.true15
  br label %for.inc84

if.end:                                           ; preds = %land.lhs.true15, %if.else13
  br label %if.end19

if.end19:                                         ; preds = %if.end
  br label %if.end20

if.end20:                                         ; preds = %if.end19
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf21 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %23, i32 0, i32 28
  %combine_vert_horz_lf = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %lf21, i32 0, i32 8
  %24 = load i32, i32* %combine_vert_horz_lf, align 16, !tbaa !71
  %tobool22 = icmp ne i32 %24, 0
  br i1 %tobool22, label %if.then23, label %if.else50

if.then23:                                        ; preds = %if.end20
  %25 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %25, i32* %mi_row, align 4, !tbaa !6
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc47, %if.then23
  %26 = load i32, i32* %mi_row, align 4, !tbaa !6
  %27 = load i32, i32* %stop.addr, align 4, !tbaa !6
  %cmp25 = icmp slt i32 %26, %27
  br i1 %cmp25, label %for.body26, label %for.end49

for.body26:                                       ; preds = %for.cond24
  store i32 0, i32* %mi_col, align 4, !tbaa !6
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc, %for.body26
  %28 = load i32, i32* %mi_col, align 4, !tbaa !6
  %29 = load i32, i32* %col_end, align 4, !tbaa !6
  %cmp28 = icmp slt i32 %28, %29
  br i1 %cmp28, label %for.body29, label %for.end

for.body29:                                       ; preds = %for.cond27
  %30 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %31 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %31, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 7
  %32 = load i8, i8* %sb_size, align 4, !tbaa !72
  %33 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  %34 = load i32, i32* %mi_row, align 4, !tbaa !6
  %35 = load i32, i32* %mi_col, align 4, !tbaa !6
  %36 = load i32, i32* %plane1, align 4, !tbaa !6
  %37 = load i32, i32* %plane1, align 4, !tbaa !6
  %add = add nsw i32 %37, 1
  call void @av1_setup_dst_planes(%struct.macroblockd_plane* %30, i8 zeroext %32, %struct.yv12_buffer_config* %33, i32 %34, i32 %35, i32 %36, i32 %add)
  %38 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %39 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %40 = load i32, i32* %plane1, align 4, !tbaa !6
  %41 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %42 = load i32, i32* %plane1, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %41, i32 %42
  %43 = load i32, i32* %mi_row, align 4, !tbaa !6
  %44 = load i32, i32* %mi_col, align 4, !tbaa !6
  call void @av1_filter_block_plane_vert(%struct.AV1Common* %38, %struct.macroblockd* %39, i32 %40, %struct.macroblockd_plane* %arrayidx30, i32 %43, i32 %44)
  %45 = load i32, i32* %mi_col, align 4, !tbaa !6
  %sub = sub nsw i32 %45, 32
  %cmp31 = icmp sge i32 %sub, 0
  br i1 %cmp31, label %if.then32, label %if.end39

if.then32:                                        ; preds = %for.body29
  %46 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %47 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params33 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %47, i32 0, i32 37
  %sb_size34 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params33, i32 0, i32 7
  %48 = load i8, i8* %sb_size34, align 4, !tbaa !72
  %49 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  %50 = load i32, i32* %mi_row, align 4, !tbaa !6
  %51 = load i32, i32* %mi_col, align 4, !tbaa !6
  %sub35 = sub nsw i32 %51, 32
  %52 = load i32, i32* %plane1, align 4, !tbaa !6
  %53 = load i32, i32* %plane1, align 4, !tbaa !6
  %add36 = add nsw i32 %53, 1
  call void @av1_setup_dst_planes(%struct.macroblockd_plane* %46, i8 zeroext %48, %struct.yv12_buffer_config* %49, i32 %50, i32 %sub35, i32 %52, i32 %add36)
  %54 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %55 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %56 = load i32, i32* %plane1, align 4, !tbaa !6
  %57 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %58 = load i32, i32* %plane1, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %57, i32 %58
  %59 = load i32, i32* %mi_row, align 4, !tbaa !6
  %60 = load i32, i32* %mi_col, align 4, !tbaa !6
  %sub38 = sub nsw i32 %60, 32
  call void @av1_filter_block_plane_horz(%struct.AV1Common* %54, %struct.macroblockd* %55, i32 %56, %struct.macroblockd_plane* %arrayidx37, i32 %59, i32 %sub38)
  br label %if.end39

if.end39:                                         ; preds = %if.then32, %for.body29
  br label %for.inc

for.inc:                                          ; preds = %if.end39
  %61 = load i32, i32* %mi_col, align 4, !tbaa !6
  %add40 = add nsw i32 %61, 32
  store i32 %add40, i32* %mi_col, align 4, !tbaa !6
  br label %for.cond27

for.end:                                          ; preds = %for.cond27
  %62 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %63 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params41 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %63, i32 0, i32 37
  %sb_size42 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params41, i32 0, i32 7
  %64 = load i8, i8* %sb_size42, align 4, !tbaa !72
  %65 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  %66 = load i32, i32* %mi_row, align 4, !tbaa !6
  %67 = load i32, i32* %mi_col, align 4, !tbaa !6
  %sub43 = sub nsw i32 %67, 32
  %68 = load i32, i32* %plane1, align 4, !tbaa !6
  %69 = load i32, i32* %plane1, align 4, !tbaa !6
  %add44 = add nsw i32 %69, 1
  call void @av1_setup_dst_planes(%struct.macroblockd_plane* %62, i8 zeroext %64, %struct.yv12_buffer_config* %65, i32 %66, i32 %sub43, i32 %68, i32 %add44)
  %70 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %71 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %72 = load i32, i32* %plane1, align 4, !tbaa !6
  %73 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %74 = load i32, i32* %plane1, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %73, i32 %74
  %75 = load i32, i32* %mi_row, align 4, !tbaa !6
  %76 = load i32, i32* %mi_col, align 4, !tbaa !6
  %sub46 = sub nsw i32 %76, 32
  call void @av1_filter_block_plane_horz(%struct.AV1Common* %70, %struct.macroblockd* %71, i32 %72, %struct.macroblockd_plane* %arrayidx45, i32 %75, i32 %sub46)
  br label %for.inc47

for.inc47:                                        ; preds = %for.end
  %77 = load i32, i32* %mi_row, align 4, !tbaa !6
  %add48 = add nsw i32 %77, 32
  store i32 %add48, i32* %mi_row, align 4, !tbaa !6
  br label %for.cond24

for.end49:                                        ; preds = %for.cond24
  br label %if.end83

if.else50:                                        ; preds = %if.end20
  %78 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %78, i32* %mi_row, align 4, !tbaa !6
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc64, %if.else50
  %79 = load i32, i32* %mi_row, align 4, !tbaa !6
  %80 = load i32, i32* %stop.addr, align 4, !tbaa !6
  %cmp52 = icmp slt i32 %79, %80
  br i1 %cmp52, label %for.body53, label %for.end66

for.body53:                                       ; preds = %for.cond51
  store i32 0, i32* %mi_col, align 4, !tbaa !6
  br label %for.cond54

for.cond54:                                       ; preds = %for.inc61, %for.body53
  %81 = load i32, i32* %mi_col, align 4, !tbaa !6
  %82 = load i32, i32* %col_end, align 4, !tbaa !6
  %cmp55 = icmp slt i32 %81, %82
  br i1 %cmp55, label %for.body56, label %for.end63

for.body56:                                       ; preds = %for.cond54
  %83 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %84 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params57 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %84, i32 0, i32 37
  %sb_size58 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params57, i32 0, i32 7
  %85 = load i8, i8* %sb_size58, align 4, !tbaa !72
  %86 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  %87 = load i32, i32* %mi_row, align 4, !tbaa !6
  %88 = load i32, i32* %mi_col, align 4, !tbaa !6
  %89 = load i32, i32* %plane1, align 4, !tbaa !6
  %90 = load i32, i32* %plane1, align 4, !tbaa !6
  %add59 = add nsw i32 %90, 1
  call void @av1_setup_dst_planes(%struct.macroblockd_plane* %83, i8 zeroext %85, %struct.yv12_buffer_config* %86, i32 %87, i32 %88, i32 %89, i32 %add59)
  %91 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %92 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %93 = load i32, i32* %plane1, align 4, !tbaa !6
  %94 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %95 = load i32, i32* %plane1, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %94, i32 %95
  %96 = load i32, i32* %mi_row, align 4, !tbaa !6
  %97 = load i32, i32* %mi_col, align 4, !tbaa !6
  call void @av1_filter_block_plane_vert(%struct.AV1Common* %91, %struct.macroblockd* %92, i32 %93, %struct.macroblockd_plane* %arrayidx60, i32 %96, i32 %97)
  br label %for.inc61

for.inc61:                                        ; preds = %for.body56
  %98 = load i32, i32* %mi_col, align 4, !tbaa !6
  %add62 = add nsw i32 %98, 32
  store i32 %add62, i32* %mi_col, align 4, !tbaa !6
  br label %for.cond54

for.end63:                                        ; preds = %for.cond54
  br label %for.inc64

for.inc64:                                        ; preds = %for.end63
  %99 = load i32, i32* %mi_row, align 4, !tbaa !6
  %add65 = add nsw i32 %99, 32
  store i32 %add65, i32* %mi_row, align 4, !tbaa !6
  br label %for.cond51

for.end66:                                        ; preds = %for.cond51
  %100 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %100, i32* %mi_row, align 4, !tbaa !6
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc80, %for.end66
  %101 = load i32, i32* %mi_row, align 4, !tbaa !6
  %102 = load i32, i32* %stop.addr, align 4, !tbaa !6
  %cmp68 = icmp slt i32 %101, %102
  br i1 %cmp68, label %for.body69, label %for.end82

for.body69:                                       ; preds = %for.cond67
  store i32 0, i32* %mi_col, align 4, !tbaa !6
  br label %for.cond70

for.cond70:                                       ; preds = %for.inc77, %for.body69
  %103 = load i32, i32* %mi_col, align 4, !tbaa !6
  %104 = load i32, i32* %col_end, align 4, !tbaa !6
  %cmp71 = icmp slt i32 %103, %104
  br i1 %cmp71, label %for.body72, label %for.end79

for.body72:                                       ; preds = %for.cond70
  %105 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %106 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params73 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %106, i32 0, i32 37
  %sb_size74 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params73, i32 0, i32 7
  %107 = load i8, i8* %sb_size74, align 4, !tbaa !72
  %108 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_buffer.addr, align 4, !tbaa !2
  %109 = load i32, i32* %mi_row, align 4, !tbaa !6
  %110 = load i32, i32* %mi_col, align 4, !tbaa !6
  %111 = load i32, i32* %plane1, align 4, !tbaa !6
  %112 = load i32, i32* %plane1, align 4, !tbaa !6
  %add75 = add nsw i32 %112, 1
  call void @av1_setup_dst_planes(%struct.macroblockd_plane* %105, i8 zeroext %107, %struct.yv12_buffer_config* %108, i32 %109, i32 %110, i32 %111, i32 %add75)
  %113 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %114 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %115 = load i32, i32* %plane1, align 4, !tbaa !6
  %116 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %117 = load i32, i32* %plane1, align 4, !tbaa !6
  %arrayidx76 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %116, i32 %117
  %118 = load i32, i32* %mi_row, align 4, !tbaa !6
  %119 = load i32, i32* %mi_col, align 4, !tbaa !6
  call void @av1_filter_block_plane_horz(%struct.AV1Common* %113, %struct.macroblockd* %114, i32 %115, %struct.macroblockd_plane* %arrayidx76, i32 %118, i32 %119)
  br label %for.inc77

for.inc77:                                        ; preds = %for.body72
  %120 = load i32, i32* %mi_col, align 4, !tbaa !6
  %add78 = add nsw i32 %120, 32
  store i32 %add78, i32* %mi_col, align 4, !tbaa !6
  br label %for.cond70

for.end79:                                        ; preds = %for.cond70
  br label %for.inc80

for.inc80:                                        ; preds = %for.end79
  %121 = load i32, i32* %mi_row, align 4, !tbaa !6
  %add81 = add nsw i32 %121, 32
  store i32 %add81, i32* %mi_row, align 4, !tbaa !6
  br label %for.cond67

for.end82:                                        ; preds = %for.cond67
  br label %if.end83

if.end83:                                         ; preds = %for.end82, %for.end49
  br label %for.inc84

for.inc84:                                        ; preds = %if.end83, %if.then18, %if.then12
  %122 = load i32, i32* %plane1, align 4, !tbaa !6
  %inc = add nsw i32 %122, 1
  store i32 %inc, i32* %plane1, align 4, !tbaa !6
  br label %for.cond

for.end85:                                        ; preds = %if.then, %for.cond
  %123 = bitcast i32* %plane1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  %124 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  %125 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #5
  %126 = bitcast i32* %col_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #5
  %127 = bitcast i32* %col_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #5
  %128 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #5
  ret void
}

; Function Attrs: nounwind
define internal zeroext i8 @get_transform_size(%struct.macroblockd* %xd, %struct.MB_MODE_INFO* %mbmi, i8 zeroext %edge_dir, i32 %mi_row, i32 %mi_col, i32 %plane, %struct.macroblockd_plane* %plane_ptr) #0 {
entry:
  %retval = alloca i8, align 1
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %edge_dir.addr = alloca i8, align 1
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %plane_ptr.addr = alloca %struct.macroblockd_plane*, align 4
  %tx_size = alloca i8, align 1
  %sb_type13 = alloca i8, align 1
  %blk_row = alloca i32, align 4
  %blk_col = alloca i32, align 4
  %mb_tx_size = alloca i8, align 1
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  store i8 %edge_dir, i8* %edge_dir.addr, align 1, !tbaa !33
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.macroblockd_plane* %plane_ptr, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.macroblockd* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %lossless = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 43
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %segment_id = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id, align 4
  %bf.lshr = lshr i8 %bf.load, 1
  %bf.clear = and i8 %bf.lshr, 7
  %idxprom = zext i8 %bf.clear to i32
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %lossless, i32 0, i32 %idxprom
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %tobool1 = icmp ne i32 %3, 0
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i8 0, i8* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size) #5
  %4 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %4, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %tx_size2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 16
  %6 = load i8, i8* %tx_size2, align 1, !tbaa !73
  %conv = zext i8 %6 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 6
  %8 = load i8, i8* %sb_type, align 2, !tbaa !68
  %9 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %9, i32 0, i32 4
  %10 = load i32, i32* %subsampling_x, align 4, !tbaa !49
  %11 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %plane_ptr.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %11, i32 0, i32 5
  %12 = load i32, i32* %subsampling_y, align 4, !tbaa !52
  %call = call zeroext i8 @av1_get_max_uv_txsize(i8 zeroext %8, i32 %10, i32 %12)
  %conv3 = zext i8 %call to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %conv3, %cond.false ]
  %conv4 = trunc i32 %cond to i8
  store i8 %conv4, i8* %tx_size, align 1, !tbaa !33
  %13 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp5 = icmp eq i32 %13, 0
  br i1 %cmp5, label %land.lhs.true7, label %if.end25

land.lhs.true7:                                   ; preds = %cond.end
  %14 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call8 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %14)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %land.lhs.true10, label %if.end25

land.lhs.true10:                                  ; preds = %land.lhs.true7
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %15, i32 0, i32 14
  %16 = load i8, i8* %skip, align 4, !tbaa !67
  %tobool11 = icmp ne i8 %16, 0
  br i1 %tobool11, label %if.end25, label %if.then12

if.then12:                                        ; preds = %land.lhs.true10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %sb_type13) #5
  %17 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %sb_type14 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %17, i32 0, i32 6
  %18 = load i8, i8* %sb_type14, align 2, !tbaa !68
  store i8 %18, i8* %sb_type13, align 1, !tbaa !33
  %19 = bitcast i32* %blk_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %21 = load i8, i8* %sb_type13, align 1, !tbaa !33
  %idxprom15 = zext i8 %21 to i32
  %arrayidx16 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom15
  %22 = load i8, i8* %arrayidx16, align 1, !tbaa !33
  %conv17 = zext i8 %22 to i32
  %sub = sub nsw i32 %conv17, 1
  %and = and i32 %20, %sub
  store i32 %and, i32* %blk_row, align 4, !tbaa !6
  %23 = bitcast i32* %blk_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  %24 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %25 = load i8, i8* %sb_type13, align 1, !tbaa !33
  %idxprom18 = zext i8 %25 to i32
  %arrayidx19 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom18
  %26 = load i8, i8* %arrayidx19, align 1, !tbaa !33
  %conv20 = zext i8 %26 to i32
  %sub21 = sub nsw i32 %conv20, 1
  %and22 = and i32 %24, %sub21
  store i32 %and22, i32* %blk_col, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mb_tx_size) #5
  %27 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %inter_tx_size = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %27, i32 0, i32 15
  %28 = load i8, i8* %sb_type13, align 1, !tbaa !33
  %29 = load i32, i32* %blk_row, align 4, !tbaa !6
  %30 = load i32, i32* %blk_col, align 4, !tbaa !6
  %call23 = call i32 @av1_get_txb_size_index(i8 zeroext %28, i32 %29, i32 %30)
  %arrayidx24 = getelementptr inbounds [16 x i8], [16 x i8]* %inter_tx_size, i32 0, i32 %call23
  %31 = load i8, i8* %arrayidx24, align 1, !tbaa !33
  store i8 %31, i8* %mb_tx_size, align 1, !tbaa !33
  %32 = load i8, i8* %mb_tx_size, align 1, !tbaa !33
  store i8 %32, i8* %tx_size, align 1, !tbaa !33
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mb_tx_size) #5
  %33 = bitcast i32* %blk_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  %34 = bitcast i32* %blk_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %sb_type13) #5
  br label %if.end25

if.end25:                                         ; preds = %if.then12, %land.lhs.true10, %land.lhs.true7, %cond.end
  %35 = load i8, i8* %edge_dir.addr, align 1, !tbaa !33
  %conv26 = zext i8 %35 to i32
  %cmp27 = icmp eq i32 0, %conv26
  br i1 %cmp27, label %cond.true29, label %cond.false33

cond.true29:                                      ; preds = %if.end25
  %36 = load i8, i8* %tx_size, align 1, !tbaa !33
  %idxprom30 = zext i8 %36 to i32
  %arrayidx31 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_horz_map, i32 0, i32 %idxprom30
  %37 = load i8, i8* %arrayidx31, align 1, !tbaa !33
  %conv32 = zext i8 %37 to i32
  br label %cond.end37

cond.false33:                                     ; preds = %if.end25
  %38 = load i8, i8* %tx_size, align 1, !tbaa !33
  %idxprom34 = zext i8 %38 to i32
  %arrayidx35 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_vert_map, i32 0, i32 %idxprom34
  %39 = load i8, i8* %arrayidx35, align 1, !tbaa !33
  %conv36 = zext i8 %39 to i32
  br label %cond.end37

cond.end37:                                       ; preds = %cond.false33, %cond.true29
  %cond38 = phi i32 [ %conv32, %cond.true29 ], [ %conv36, %cond.false33 ]
  %conv39 = trunc i32 %cond38 to i8
  store i8 %conv39, i8* %tx_size, align 1, !tbaa !33
  %40 = load i8, i8* %tx_size, align 1, !tbaa !33
  store i8 %40, i8* %retval, align 1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size) #5
  br label %return

return:                                           ; preds = %cond.end37, %if.then
  %41 = load i8, i8* %retval, align 1
  ret i8 %41
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_block(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !33
  %conv = sext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_plane_block_size(i8 zeroext %bsize, i32 %subsampling_x, i32 %subsampling_y) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !33
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !33
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x [2 x [2 x i8]]], [22 x [2 x [2 x i8]]]* @ss_size_lookup, i32 0, i32 %idxprom
  %1 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds [2 x [2 x i8]], [2 x [2 x i8]]* %arrayidx, i32 0, i32 %1
  %2 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx1, i32 0, i32 %2
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !33
  ret i8 %3
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_get_max_uv_txsize(i8 zeroext %bsize, i32 %subsampling_x, i32 %subsampling_y) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %plane_bsize = alloca i8, align 1
  %uv_tx = alloca i8, align 1
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !33
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %plane_bsize) #5
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !33
  %1 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %2 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %call = call zeroext i8 @get_plane_block_size(i8 zeroext %0, i32 %1, i32 %2)
  store i8 %call, i8* %plane_bsize, align 1, !tbaa !33
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %uv_tx) #5
  %3 = load i8, i8* %plane_bsize, align 1, !tbaa !33
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @max_txsize_rect_lookup, i32 0, i32 %idxprom
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !33
  store i8 %4, i8* %uv_tx, align 1, !tbaa !33
  %5 = load i8, i8* %uv_tx, align 1, !tbaa !33
  %call1 = call zeroext i8 @av1_get_adjusted_tx_size(i8 zeroext %5)
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %uv_tx) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %plane_bsize) #5
  ret i8 %call1
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_get_txb_size_index(i8 zeroext %bsize, i32 %blk_row, i32 %blk_col) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  %blk_row.addr = alloca i32, align 4
  %blk_col.addr = alloca i32, align 4
  %index = alloca i32, align 4
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !33
  store i32 %blk_row, i32* %blk_row.addr, align 4, !tbaa !6
  store i32 %blk_col, i32* %blk_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %blk_row.addr, align 4, !tbaa !6
  %2 = load i8, i8* %bsize.addr, align 1, !tbaa !33
  %idxprom = zext i8 %2 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @av1_get_txb_size_index.tw_h_log2_table, i32 0, i32 %idxprom
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !33
  %conv = zext i8 %3 to i32
  %shr = ashr i32 %1, %conv
  %4 = load i8, i8* %bsize.addr, align 1, !tbaa !33
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @av1_get_txb_size_index.stride_log2_table, i32 0, i32 %idxprom1
  %5 = load i8, i8* %arrayidx2, align 1, !tbaa !33
  %conv3 = zext i8 %5 to i32
  %shl = shl i32 %shr, %conv3
  %6 = load i32, i32* %blk_col.addr, align 4, !tbaa !6
  %7 = load i8, i8* %bsize.addr, align 1, !tbaa !33
  %idxprom4 = zext i8 %7 to i32
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @av1_get_txb_size_index.tw_w_log2_table, i32 0, i32 %idxprom4
  %8 = load i8, i8* %arrayidx5, align 1, !tbaa !33
  %conv6 = zext i8 %8 to i32
  %shr7 = ashr i32 %6, %conv6
  %add = add nsw i32 %shl, %shr7
  store i32 %add, i32* %index, align 4, !tbaa !6
  %9 = load i32, i32* %index, align 4, !tbaa !6
  %10 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  ret i32 %9
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_get_adjusted_tx_size(i8 zeroext %tx_size) #2 {
entry:
  %retval = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !33
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !33
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 4, label %sw.bb
    i32 12, label %sw.bb
    i32 11, label %sw.bb
    i32 18, label %sw.bb1
    i32 17, label %sw.bb2
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry
  store i8 3, i8* %retval, align 1
  br label %return

sw.bb1:                                           ; preds = %entry
  store i8 10, i8* %retval, align 1
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8 9, i8* %retval, align 1
  br label %return

sw.default:                                       ; preds = %entry
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !33
  store i8 %1, i8* %retval, align 1
  br label %return

return:                                           ; preds = %sw.default, %sw.bb2, %sw.bb1, %sw.bb
  %2 = load i8, i8* %retval, align 1
  ret i8 %2
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_intrabc_block(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  ret i32 %conv
}

declare void @av1_setup_dst_planes(%struct.macroblockd_plane*, i8 zeroext, %struct.yv12_buffer_config*, i32, i32, i32, i32) #4

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 15824}
!9 = !{!"AV1Common", !10, i64 0, !12, i64 40, !7, i64 288, !7, i64 292, !7, i64 296, !7, i64 300, !7, i64 304, !7, i64 308, !4, i64 312, !13, i64 313, !4, i64 316, !7, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !14, i64 492, !4, i64 580, !4, i64 1284, !7, i64 1316, !7, i64 1320, !7, i64 1324, !15, i64 1328, !16, i64 1356, !17, i64 1420, !18, i64 10676, !3, i64 10848, !19, i64 10864, !20, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !21, i64 14880, !23, i64 15028, !24, i64 15168, !26, i64 15816, !4, i64 15836, !27, i64 16192, !3, i64 18128, !3, i64 18132, !30, i64 18136, !3, i64 18724, !31, i64 18728, !7, i64 18760, !4, i64 18764, !3, i64 18796, !7, i64 18800, !4, i64 18804, !4, i64 18836, !7, i64 18844, !7, i64 18848, !7, i64 18852, !7, i64 18856}
!10 = !{!"", !4, i64 0, !4, i64 1, !7, i64 4, !7, i64 8, !7, i64 12, !11, i64 16, !7, i64 32, !7, i64 36}
!11 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!12 = !{!"aom_internal_error_info", !4, i64 0, !7, i64 4, !4, i64 8, !7, i64 88, !4, i64 92}
!13 = !{!"_Bool", !4, i64 0}
!14 = !{!"scale_factors", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!15 = !{!"", !13, i64 0, !13, i64 1, !13, i64 2, !13, i64 3, !13, i64 4, !13, i64 5, !13, i64 6, !13, i64 7, !13, i64 8, !13, i64 9, !13, i64 10, !13, i64 11, !4, i64 12, !4, i64 13, !7, i64 16, !7, i64 20, !4, i64 24}
!16 = !{!"CommonModeInfoParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !7, i64 28, !4, i64 32, !3, i64 36, !7, i64 40, !7, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!17 = !{!"CommonQuantParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !13, i64 9240, !7, i64 9244, !7, i64 9248, !7, i64 9252}
!18 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !7, i64 164, !4, i64 168}
!19 = !{!"", !4, i64 0, !4, i64 3072}
!20 = !{!"loopfilter", !4, i64 0, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !7, i64 32}
!21 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !22, i64 80, !7, i64 84, !22, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!22 = !{!"long", !4, i64 0}
!23 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !4, i64 72, !7, i64 136}
!24 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !25, i64 644}
!25 = !{!"short", !4, i64 0}
!26 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!27 = !{!"SequenceHeader", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !4, i64 16, !7, i64 20, !7, i64 24, !4, i64 28, !7, i64 32, !7, i64 36, !11, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !7, i64 112, !4, i64 116, !7, i64 244, !28, i64 248, !4, i64 264, !29, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!28 = !{!"aom_timing", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!29 = !{!"aom_dec_model_info", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!30 = !{!"CommonTileParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !4, i64 60, !4, i64 320, !7, i64 580, !7, i64 584}
!31 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !7, i64 20, !7, i64 24, !7, i64 28}
!32 = !{!9, !7, i64 15832}
!33 = !{!4, !4, i64 0}
!34 = !{!35, !4, i64 146}
!35 = !{!"MB_MODE_INFO", !36, i64 0, !37, i64 8, !4, i64 52, !7, i64 60, !4, i64 64, !38, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !39, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!36 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!37 = !{!"", !4, i64 0, !25, i64 32, !25, i64 34, !25, i64 36, !25, i64 38, !4, i64 40, !4, i64 41}
!38 = !{!"", !4, i64 0, !4, i64 48}
!39 = !{!"", !4, i64 0, !4, i64 1}
!40 = !{!9, !7, i64 14712}
!41 = !{!9, !7, i64 14716}
!42 = !{!9, !4, i64 14724}
!43 = !{!35, !4, i64 119}
!44 = !{!18, !4, i64 0}
!45 = !{!25, !25, i64 0}
!46 = !{!20, !7, i64 32}
!47 = !{!20, !7, i64 16}
!48 = !{!20, !4, i64 20}
!49 = !{!50, !7, i64 16}
!50 = !{!"macroblockd_plane", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !7, i64 16, !7, i64 20, !51, i64 24, !4, i64 44, !3, i64 84, !3, i64 88, !4, i64 92, !3, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!51 = !{!"buf_2d", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!52 = !{!50, !7, i64 20}
!53 = !{!50, !3, i64 24}
!54 = !{!50, !7, i64 40}
!55 = !{!56, !7, i64 0}
!56 = !{!"AV1_DEBLOCKING_PARAMETERS", !7, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!57 = !{!9, !4, i64 16268}
!58 = !{!9, !4, i64 16264}
!59 = !{!56, !3, i64 8}
!60 = !{!56, !3, i64 4}
!61 = !{!56, !3, i64 12}
!62 = !{!22, !22, i64 0}
!63 = !{!50, !7, i64 32}
!64 = !{!50, !7, i64 36}
!65 = !{!9, !3, i64 1392}
!66 = !{!9, !7, i64 1400}
!67 = !{!35, !4, i64 128}
!68 = !{!35, !4, i64 118}
!69 = !{!9, !7, i64 1368}
!70 = !{!9, !7, i64 1372}
!71 = !{!9, !7, i64 14736}
!72 = !{!9, !4, i64 16220}
!73 = !{!35, !4, i64 145}
