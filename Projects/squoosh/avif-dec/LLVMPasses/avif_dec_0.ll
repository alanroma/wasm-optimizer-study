; ModuleID = 'dec/avif_dec.cpp'
source_filename = "dec/avif_dec.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.emscripten::val" = type { %"struct.emscripten::internal::_EM_VAL"* }
%"struct.emscripten::internal::_EM_VAL" = type opaque
%struct.EmscriptenBindingInitializer_my_module = type { i8 }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%struct.avifROData = type { i8*, i32 }
%struct.avifImage = type { i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i8*, i32, i32, %struct.avifRWData, i32, i32, i32, i32, %struct.avifPixelAspectRatioBox, %struct.avifCleanApertureBox, %struct.avifImageRotation, %struct.avifImageMirror, %struct.avifRWData, %struct.avifRWData }
%struct.avifPixelAspectRatioBox = type { i32, i32 }
%struct.avifCleanApertureBox = type { i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifImageRotation = type { i8 }
%struct.avifImageMirror = type { i8 }
%struct.avifRWData = type { i8*, i32 }
%struct.avifDecoder = type { i32, i32, %struct.avifImage*, i32, i32, %struct.avifImageTiming, i64, double, i64, i32, %struct.avifIOStats, %struct.avifDecoderData* }
%struct.avifImageTiming = type { i64, double, i64, double, i64 }
%struct.avifIOStats = type { i32, i32 }
%struct.avifDecoderData = type opaque
%struct.avifRGBImage = type { i32, i32, i32, i32, i32, i32, i8*, i32 }
%"struct.emscripten::memory_view" = type { i32, i8* }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5" = type { i8 }
%struct.anon.6 = type { i32, [1 x i8] }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList" = type { i8 }
%"struct.emscripten::internal::WireTypePack" = type { %"struct.std::__2::array" }
%"struct.std::__2::array" = type { [1 x %"union.emscripten::internal::GenericWireType"] }
%"union.emscripten::internal::GenericWireType" = type { double }
%union.anon.1 = type { i32 }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2" = type { i8 }
%"struct.emscripten::internal::WireTypePack.3" = type { %"struct.std::__2::array.4" }
%"struct.std::__2::array.4" = type { [3 x %"union.emscripten::internal::GenericWireType"] }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::allocator" = type { i8 }

$_ZN10emscripten3val6globalEPKc = comdat any

$_ZN10emscripten3valD2Ev = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6lengthEv = comdat any

$_ZN10emscripten3val4nullEv = comdat any

$_ZNK10emscripten3val4new_IJS0_RjS2_EEES0_DpOT_ = comdat any

$_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_ = comdat any

$_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_ = comdat any

$_ZN10emscripten3valaSEOS0_ = comdat any

$_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEEJEEEvPKcPFT_DpT0_EDpT1_ = comdat any

$_ZN10emscripten3valC2EPNS_8internal7_EM_VALE = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv = comdat any

$_ZN10emscripten11memory_viewIhEC2EmPKh = comdat any

$_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_ = comdat any

$_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_ = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv = comdat any

$_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv = comdat any

$_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_ = comdat any

$_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE = comdat any

$_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_ = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv = comdat any

$_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv = comdat any

$_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJS0_RjSB_EEES0_T_DpOT0_ = comdat any

$_ZNSt3__27forwardIN10emscripten3valEEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZN10emscripten8internal12WireTypePackIJNS_3valERjS3_EEC2EOS2_S3_S3_ = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valERjS5_EE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valERjS5_EE8getTypesEv = comdat any

$_ZNK10emscripten8internal12WireTypePackIJNS_3valERjS3_EEcvPKvEv = comdat any

$_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm3EE4dataEv = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesINS_3valEJRjS3_EEEvRPNS0_15GenericWireTypeEOT_DpOT0_ = comdat any

$_ZN10emscripten8internal20writeGenericWireTypeINS0_7_EM_VALEEEvRPNS0_15GenericWireTypeEPT_ = comdat any

$_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_ = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesIRjJS2_EEEvRPNS0_15GenericWireTypeEOT_DpOT0_ = comdat any

$_ZN10emscripten8internal20writeGenericWireTypeIjEEvRPNS0_15GenericWireTypeET_ = comdat any

$_ZN10emscripten8internal11BindingTypeIjvE10toWireTypeERKj = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesIRjJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_ = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valERjS4_EEEE3getEv = comdat any

$_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm3EE4dataEv = comdat any

$_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEEE6invokeEPFS2_S9_EPNS0_11BindingTypeIS9_vEUt_E = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEE8getTypesEv = comdat any

$_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEPNS0_11BindingTypeISB_vEUt_EEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEEEEE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEPNS0_11BindingTypeISB_vEUt_EEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types = comdat any

$_ZTSN10emscripten11memory_viewIhEE = comdat any

$_ZTIN10emscripten11memory_viewIhEE = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valERjS4_EEEE3getEvE5types = comdat any

$_ZTSN10emscripten3valE = comdat any

$_ZTIN10emscripten3valE = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEEEEE3getEvE5types = comdat any

$_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTSNSt3__221__basic_string_commonILb1EEE = comdat any

$_ZTINSt3__221__basic_string_commonILb1EEE = comdat any

$_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcvE9signature = comdat any

@_ZL17Uint8ClampedArray = internal thread_local global %"class.emscripten::val" zeroinitializer, align 4
@.str = private unnamed_addr constant [18 x i8] c"Uint8ClampedArray\00", align 1
@__dso_handle = external hidden global i8
@_ZL9ImageData = internal thread_local global %"class.emscripten::val" zeroinitializer, align 4
@.str.2 = private unnamed_addr constant [10 x i8] c"ImageData\00", align 1
@_ZL47EmscriptenBindingInitializer_my_module_instance = internal global %struct.EmscriptenBindingInitializer_my_module zeroinitializer, align 1
@.str.5 = private unnamed_addr constant [7 x i8] c"decode\00", align 1
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types = linkonce_odr hidden constant [1 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten11memory_viewIhEE to i8*)], comdat, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN10emscripten11memory_viewIhEE = linkonce_odr hidden constant [31 x i8] c"N10emscripten11memory_viewIhEE\00", comdat, align 1
@_ZTIN10emscripten11memory_viewIhEE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTSN10emscripten11memory_viewIhEE, i32 0, i32 0) }, comdat, align 4
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valERjS4_EEEE3getEvE5types = linkonce_odr hidden constant [3 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten3valE to i8*), i8* bitcast (i8** @_ZTIj to i8*), i8* bitcast (i8** @_ZTIj to i8*)], comdat, align 4
@_ZTSN10emscripten3valE = linkonce_odr hidden constant [19 x i8] c"N10emscripten3valE\00", comdat, align 1
@_ZTIN10emscripten3valE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN10emscripten3valE, i32 0, i32 0) }, comdat, align 4
@_ZTIj = external constant i8*
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEEEEE3getEvE5types = linkonce_odr hidden constant [2 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten3valE to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i8*, i32 }* @_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE to i8*)], comdat, align 4
@_ZTVN10__cxxabiv121__vmi_class_type_infoE = external global i8*
@_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant [63 x i8] c"NSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE\00", comdat, align 1
@_ZTSNSt3__221__basic_string_commonILb1EEE = linkonce_odr constant [38 x i8] c"NSt3__221__basic_string_commonILb1EEE\00", comdat, align 1
@_ZTINSt3__221__basic_string_commonILb1EEE = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([38 x i8], [38 x i8]* @_ZTSNSt3__221__basic_string_commonILb1EEE, i32 0, i32 0) }, comdat, align 4
@_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant { i8*, i8*, i32, i32, i8*, i32 } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv121__vmi_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([63 x i8], [63 x i8]* @_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, i32 0), i32 0, i32 1, i8* bitcast ({ i8*, i8* }* @_ZTINSt3__221__basic_string_commonILb1EEE to i8*), i32 0 }, comdat, align 4
@_ZZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcvE9signature = linkonce_odr hidden constant [4 x i8] c"iii\00", comdat, align 1
@__tls_guard = internal thread_local global i8 0, align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_avif_dec.cpp, i8* null }]

@_ZN38EmscriptenBindingInitializer_my_moduleC1Ev = hidden unnamed_addr alias %struct.EmscriptenBindingInitializer_my_module* (%struct.EmscriptenBindingInitializer_my_module*), %struct.EmscriptenBindingInitializer_my_module* (%struct.EmscriptenBindingInitializer_my_module*)* @_ZN38EmscriptenBindingInitializer_my_moduleC2Ev
@_ZTHL9ImageData = internal alias void (), void ()* @__tls_init
@_ZTHL17Uint8ClampedArray = internal alias void (), void ()* @__tls_init

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZN10emscripten3val6globalEPKc(%"class.emscripten::val"* sret align 4 @_ZL17Uint8ClampedArray, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str, i32 0, i32 0))
  %0 = call i32 @__cxa_thread_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #3
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten3val6globalEPKc(%"class.emscripten::val"* noalias sret align 4 %agg.result, i8* %name) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %name.addr = alloca i8*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i8* %name, i8** %name.addr, align 4
  %1 = load i8*, i8** %name.addr, align 4
  %call = call %"struct.emscripten::internal::_EM_VAL"* @_emval_get_global(i8* %1)
  %call1 = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* %call)
  ret void
}

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* @_ZL17Uint8ClampedArray) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"* %0)
  ret %"class.emscripten::val"* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_thread_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: noinline
define internal void @__cxx_global_var_init.1() #0 {
entry:
  call void @_ZN10emscripten3val6globalEPKc(%"class.emscripten::val"* sret align 4 @_ZL9ImageData, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.2, i32 0, i32 0))
  %0 = call i32 @__cxa_thread_atexit(void (i8*)* @__cxx_global_array_dtor.3, i8* null, i8* @__dso_handle) #3
  ret void
}

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor.3(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* @_ZL9ImageData) #3
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_Z6decodeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.std::__2::basic_string"* %avifimage) #1 {
entry:
  %result.ptr = alloca i8*, align 4
  %raw = alloca %struct.avifROData, align 4
  %image = alloca %struct.avifImage*, align 4
  %decoder = alloca %struct.avifDecoder*, align 4
  %decodeResult = alloca i32, align 4
  %nrvo = alloca i1, align 1
  %rgb = alloca %struct.avifRGBImage, align 4
  %ref.tmp = alloca %"class.emscripten::val", align 4
  %ref.tmp6 = alloca %"class.emscripten::val", align 4
  %ref.tmp7 = alloca %"struct.emscripten::memory_view", align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %raw, i32 0, i32 0
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %avifimage) #3
  store i8* %call, i8** %data, align 4
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %raw, i32 0, i32 1
  %call1 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6lengthEv(%"class.std::__2::basic_string"* %avifimage) #3
  store i32 %call1, i32* %size, align 4
  %call2 = call %struct.avifImage* @avifImageCreateEmpty()
  store %struct.avifImage* %call2, %struct.avifImage** %image, align 4
  %call3 = call %struct.avifDecoder* @avifDecoderCreate()
  store %struct.avifDecoder* %call3, %struct.avifDecoder** %decoder, align 4
  %1 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder, align 4
  %2 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  %call4 = call i32 @avifDecoderRead(%struct.avifDecoder* %1, %struct.avifImage* %2, %struct.avifROData* %raw)
  store i32 %call4, i32* %decodeResult, align 4
  %3 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder, align 4
  call void @avifDecoderDestroy(%struct.avifDecoder* %3)
  store i1 false, i1* %nrvo, align 1
  call void @_ZN10emscripten3val4nullEv(%"class.emscripten::val"* sret align 4 %agg.result)
  %4 = load i32, i32* %decodeResult, align 4
  %cmp = icmp eq i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  call void @avifRGBImageSetDefaults(%struct.avifRGBImage* %rgb, %struct.avifImage* %5)
  %depth = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %rgb, i32 0, i32 2
  store i32 8, i32* %depth, align 4
  call void @avifRGBImageAllocatePixels(%struct.avifRGBImage* %rgb)
  %6 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  %call5 = call i32 @avifImageYUVToRGB(%struct.avifImage* %6, %struct.avifRGBImage* %rgb)
  %7 = call %"class.emscripten::val"* @_ZTWL9ImageData()
  %8 = call %"class.emscripten::val"* @_ZTWL17Uint8ClampedArray()
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %rgb, i32 0, i32 7
  %9 = load i32, i32* %rowBytes, align 4
  %height = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %rgb, i32 0, i32 1
  %10 = load i32, i32* %height, align 4
  %mul = mul i32 %9, %10
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %rgb, i32 0, i32 6
  %11 = load i8*, i8** %pixels, align 4
  call void @_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_(%"struct.emscripten::memory_view"* sret align 4 %ref.tmp7, i32 %mul, i8* %11)
  call void @_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_(%"class.emscripten::val"* sret align 4 %ref.tmp6, %"class.emscripten::val"* %8, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %ref.tmp7)
  %width = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %rgb, i32 0, i32 0
  %height8 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %rgb, i32 0, i32 1
  call void @_ZNK10emscripten3val4new_IJS0_RjS2_EEES0_DpOT_(%"class.emscripten::val"* sret align 4 %ref.tmp, %"class.emscripten::val"* %7, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %ref.tmp6, i32* nonnull align 4 dereferenceable(4) %width, i32* nonnull align 4 dereferenceable(4) %height8)
  %call9 = call nonnull align 4 dereferenceable(4) %"class.emscripten::val"* @_ZN10emscripten3valaSEOS0_(%"class.emscripten::val"* %agg.result, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call10 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %ref.tmp) #3
  %call11 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %ref.tmp6) #3
  call void @avifRGBImageFreePixels(%struct.avifRGBImage* %rgb)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  call void @avifImageDestroy(%struct.avifImage* %12)
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %if.end
  %call12 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %agg.result) #3
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6lengthEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this1) #3
  ret i32 %call
}

declare %struct.avifImage* @avifImageCreateEmpty() #4

declare %struct.avifDecoder* @avifDecoderCreate() #4

declare i32 @avifDecoderRead(%struct.avifDecoder*, %struct.avifImage*, %struct.avifROData*) #4

declare void @avifDecoderDestroy(%struct.avifDecoder*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten3val4nullEv(%"class.emscripten::val"* noalias sret align 4 %agg.result) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %call = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* inttoptr (i32 2 to %"struct.emscripten::internal::_EM_VAL"*))
  ret void
}

declare void @avifRGBImageSetDefaults(%struct.avifRGBImage*, %struct.avifImage*) #4

declare void @avifRGBImageAllocatePixels(%struct.avifRGBImage*) #4

declare i32 @avifImageYUVToRGB(%struct.avifImage*, %struct.avifRGBImage*) #4

; Function Attrs: noinline
define internal %"class.emscripten::val"* @_ZTWL9ImageData() #5 {
  call void @_ZTHL9ImageData()
  ret %"class.emscripten::val"* @_ZL9ImageData
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val4new_IJS0_RjS2_EEES0_DpOT_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %args, i32* nonnull align 4 dereferenceable(4) %args1, i32* nonnull align 4 dereferenceable(4) %args3) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %args.addr = alloca %"class.emscripten::val"*, align 4
  %args.addr2 = alloca i32*, align 4
  %args.addr4 = alloca i32*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"class.emscripten::val"* %args, %"class.emscripten::val"** %args.addr, align 4
  store i32* %args1, i32** %args.addr2, align 4
  store i32* %args3, i32** %args.addr4, align 4
  %this5 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.emscripten::val"* @_ZNSt3__27forwardIN10emscripten3valEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %1) #3
  %2 = load i32*, i32** %args.addr2, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %2) #3
  %3 = load i32*, i32** %args.addr4, align 4
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #3
  call void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJS0_RjSB_EEES0_T_DpOT0_(%"class.emscripten::val"* sret align 4 %agg.result, %"class.emscripten::val"* %this5, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* @_emval_new, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %call, i32* nonnull align 4 dereferenceable(4) %call6, i32* nonnull align 4 dereferenceable(4) %call7)
  ret void
}

; Function Attrs: noinline
define internal %"class.emscripten::val"* @_ZTWL17Uint8ClampedArray() #5 {
  call void @_ZTHL17Uint8ClampedArray()
  ret %"class.emscripten::val"* @_ZL17Uint8ClampedArray
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #3
  call void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_(%"class.emscripten::val"* sret align 4 %agg.result, %"class.emscripten::val"* %this1, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* @_emval_new, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_(%"struct.emscripten::memory_view"* noalias sret align 4 %agg.result, i32 %size, i8* %data) #1 comdat {
entry:
  %size.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %1 = load i8*, i8** %data.addr, align 4
  %call = call %"struct.emscripten::memory_view"* @_ZN10emscripten11memory_viewIhEC2EmPKh(%"struct.emscripten::memory_view"* %agg.result, i32 %0, i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.emscripten::val"* @_ZN10emscripten3valaSEOS0_(%"class.emscripten::val"* %this, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %v) #1 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %v.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"class.emscripten::val"* %v, %"class.emscripten::val"** %v.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"* %0)
  %1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle2 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %1, i32 0, i32 0
  %2 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle2, align 4
  %handle3 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  store %"struct.emscripten::internal::_EM_VAL"* %2, %"struct.emscripten::internal::_EM_VAL"** %handle3, align 4
  %3 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle4 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %3, i32 0, i32 0
  store %"struct.emscripten::internal::_EM_VAL"* null, %"struct.emscripten::internal::_EM_VAL"** %handle4, align 4
  ret %"class.emscripten::val"* %this1
}

declare void @avifRGBImageFreePixels(%struct.avifRGBImage*) #4

declare void @avifImageDestroy(%struct.avifImage*) #4

; Function Attrs: noinline
define internal void @__cxx_global_var_init.4() #0 {
entry:
  %call = call %struct.EmscriptenBindingInitializer_my_module* @_ZN38EmscriptenBindingInitializer_my_moduleC1Ev(%struct.EmscriptenBindingInitializer_my_module* @_ZL47EmscriptenBindingInitializer_my_module_instance)
  ret void
}

; Function Attrs: noinline optnone
define hidden %struct.EmscriptenBindingInitializer_my_module* @_ZN38EmscriptenBindingInitializer_my_moduleC2Ev(%struct.EmscriptenBindingInitializer_my_module* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.EmscriptenBindingInitializer_my_module*, align 4
  store %struct.EmscriptenBindingInitializer_my_module* %this, %struct.EmscriptenBindingInitializer_my_module** %this.addr, align 4
  %this1 = load %struct.EmscriptenBindingInitializer_my_module*, %struct.EmscriptenBindingInitializer_my_module** %this.addr, align 4
  call void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEEJEEEvPKcPFT_DpT0_EDpT1_(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i32 0, i32 0), void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)* @_Z6decodeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE)
  ret %struct.EmscriptenBindingInitializer_my_module* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEEJEEEvPKcPFT_DpT0_EDpT1_(i8* %name, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)* %fn) #1 comdat {
entry:
  %name.addr = alloca i8*, align 4
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, align 4
  %args = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5", align 1
  %invoker = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)*, align 4
  store i8* %name, i8** %name.addr, align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)** %fn.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEEE6invokeEPFS2_S9_EPNS0_11BindingTypeIS9_vEUt_E, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)** %invoker, align 4
  %0 = load i8*, i8** %name.addr, align 4
  %call = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"* %args)
  %call1 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"* %args)
  %1 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)** %invoker, align 4
  %call2 = call i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEPNS0_11BindingTypeISB_vEUt_EEEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)* %1)
  %2 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)** %invoker, align 4
  %3 = bitcast %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)* %2 to i8*
  %4 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)** %fn.addr, align 4
  %5 = bitcast void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)* %4 to i8*
  call void @_embind_register_function(i8* %0, i32 %call, i8** %call1, i8* %call2, i8* %3, i8* %5)
  ret void
}

declare %"struct.emscripten::internal::_EM_VAL"* @_emval_get_global(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* returned %this, %"struct.emscripten::internal::_EM_VAL"* %handle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %handle.addr = alloca %"struct.emscripten::internal::_EM_VAL"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* %handle, %"struct.emscripten::internal::_EM_VAL"** %handle.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle2 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* %0, %"struct.emscripten::internal::_EM_VAL"** %handle2, align 4
  ret %"class.emscripten::val"* %this1
}

declare void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #3
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #2 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #3
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #3
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #3
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #2 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #2 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #3
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__size_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.emscripten::memory_view"* @_ZN10emscripten11memory_viewIhEC2EmPKh(%"struct.emscripten::memory_view"* returned %this, i32 %size, i8* %data) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %size.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  store %"struct.emscripten::memory_view"* %this, %"struct.emscripten::memory_view"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %this1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %this.addr, align 4
  %size2 = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  store i32 %0, i32* %size2, align 4
  %data3 = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %this1, i32 0, i32 1
  %1 = load i8*, i8** %data.addr, align 4
  store i8* %1, i8** %data3, align 4
  ret %"struct.emscripten::memory_view"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %impl.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %argList = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList", align 1
  %argv = alloca %"struct.emscripten::internal::WireTypePack", align 8
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #3
  %call2 = call %"struct.emscripten::internal::WireTypePack"* @_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_(%"struct.emscripten::internal::WireTypePack"* %argv, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  %2 = load %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %3 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  %call3 = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %argList)
  %call4 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %argList)
  %call5 = call i8* @_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv(%"struct.emscripten::internal::WireTypePack"* %argv)
  %call6 = call %"struct.emscripten::internal::_EM_VAL"* %2(%"struct.emscripten::internal::_EM_VAL"* %3, i32 %call3, i8** %call4, i8* %call5)
  %call7 = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* %call6)
  ret void
}

declare %"struct.emscripten::internal::_EM_VAL"* @_emval_new(%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %__t) #2 comdat {
entry:
  %__t.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"struct.emscripten::memory_view"* %__t, %"struct.emscripten::memory_view"** %__t.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %__t.addr, align 4
  ret %"struct.emscripten::memory_view"* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::WireTypePack"* @_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_(%"struct.emscripten::internal::WireTypePack"* returned %this, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack"*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %cursor = alloca %"union.emscripten::internal::GenericWireType"*, align 4
  store %"struct.emscripten::internal::WireTypePack"* %this, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"struct.emscripten::internal::WireTypePack"*, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %elements2 = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %elements2) #3
  store %"union.emscripten::internal::GenericWireType"* %call, %"union.emscripten::internal::GenericWireType"** %cursor, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %0) #3
  call void @_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call3)
  ret %"struct.emscripten::internal::WireTypePack"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv()
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv(%"struct.emscripten::internal::WireTypePack"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack"*, align 4
  store %"struct.emscripten::internal::WireTypePack"* %this, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WireTypePack"*, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %elements) #3
  %0 = bitcast %"union.emscripten::internal::GenericWireType"* %call to i8*
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1 x %"union.emscripten::internal::GenericWireType"], [1 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: alwaysinline
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %first) #6 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %first.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %ref.tmp = alloca %"struct.emscripten::memory_view", align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"struct.emscripten::memory_view"* %first, %"struct.emscripten::memory_view"** %first.addr, align 4
  %0 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %first.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #3
  call void @_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_(%"struct.emscripten::memory_view"* sret align 4 %ref.tmp, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  call void @_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %ref.tmp)
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  call void @_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %wt) #2 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %wt.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"struct.emscripten::memory_view"* %wt, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %size = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %0, i32 0, i32 0
  %1 = load i32, i32* %size, align 4
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %3 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %2, align 4
  %w = bitcast %"union.emscripten::internal::GenericWireType"* %3 to [2 x %union.anon.1]*
  %arrayidx = getelementptr inbounds [2 x %union.anon.1], [2 x %union.anon.1]* %w, i32 0, i32 0
  %u = bitcast %union.anon.1* %arrayidx to i32*
  store i32 %1, i32* %u, align 8
  %4 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %data = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %4, i32 0, i32 1
  %5 = load i8*, i8** %data, align 4
  %6 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %7 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %6, align 4
  %w1 = bitcast %"union.emscripten::internal::GenericWireType"* %7 to [2 x %union.anon.1]*
  %arrayidx2 = getelementptr inbounds [2 x %union.anon.1], [2 x %union.anon.1]* %w1, i32 0, i32 1
  %p = bitcast %union.anon.1* %arrayidx2 to i8**
  store i8* %5, i8** %p, align 4
  %8 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %9 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %8, align 4
  %incdec.ptr = getelementptr inbounds %"union.emscripten::internal::GenericWireType", %"union.emscripten::internal::GenericWireType"* %9, i32 1
  store %"union.emscripten::internal::GenericWireType"* %incdec.ptr, %"union.emscripten::internal::GenericWireType"** %8, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_(%"struct.emscripten::memory_view"* noalias sret align 4 %agg.result, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %mv) #2 comdat {
entry:
  %mv.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"struct.emscripten::memory_view"* %mv, %"struct.emscripten::memory_view"** %mv.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %mv.addr, align 4
  %1 = bitcast %"struct.emscripten::memory_view"* %agg.result to i8*
  %2 = bitcast %"struct.emscripten::memory_view"* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0) #2 comdat {
entry:
  %.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  store %"union.emscripten::internal::GenericWireType"** %0, %"union.emscripten::internal::GenericWireType"*** %.addr, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv() #2 comdat {
entry:
  ret i8** getelementptr inbounds ([1 x i8*], [1 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1 x %"union.emscripten::internal::GenericWireType"], [1 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJS0_RjSB_EEES0_T_DpOT0_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %args, i32* nonnull align 4 dereferenceable(4) %args1, i32* nonnull align 4 dereferenceable(4) %args3) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %impl.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, align 4
  %args.addr = alloca %"class.emscripten::val"*, align 4
  %args.addr2 = alloca i32*, align 4
  %args.addr4 = alloca i32*, align 4
  %argList = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2", align 1
  %argv = alloca %"struct.emscripten::internal::WireTypePack.3", align 8
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  store %"class.emscripten::val"* %args, %"class.emscripten::val"** %args.addr, align 4
  store i32* %args1, i32** %args.addr2, align 4
  store i32* %args3, i32** %args.addr4, align 4
  %this5 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.emscripten::val"* @_ZNSt3__27forwardIN10emscripten3valEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %1) #3
  %2 = load i32*, i32** %args.addr2, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %2) #3
  %3 = load i32*, i32** %args.addr4, align 4
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #3
  %call8 = call %"struct.emscripten::internal::WireTypePack.3"* @_ZN10emscripten8internal12WireTypePackIJNS_3valERjS3_EEC2EOS2_S3_S3_(%"struct.emscripten::internal::WireTypePack.3"* %argv, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %call, i32* nonnull align 4 dereferenceable(4) %call6, i32* nonnull align 4 dereferenceable(4) %call7)
  %4 = load %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this5, i32 0, i32 0
  %5 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  %call9 = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valERjS5_EE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %argList)
  %call10 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valERjS5_EE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %argList)
  %call11 = call i8* @_ZNK10emscripten8internal12WireTypePackIJNS_3valERjS3_EEcvPKvEv(%"struct.emscripten::internal::WireTypePack.3"* %argv)
  %call12 = call %"struct.emscripten::internal::_EM_VAL"* %4(%"struct.emscripten::internal::_EM_VAL"* %5, i32 %call9, i8** %call10, i8* %call11)
  %call13 = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* %call12)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.emscripten::val"* @_ZNSt3__27forwardIN10emscripten3valEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %__t) #2 comdat {
entry:
  %__t.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %__t, %"class.emscripten::val"** %__t.addr, align 4
  %0 = load %"class.emscripten::val"*, %"class.emscripten::val"** %__t.addr, align 4
  ret %"class.emscripten::val"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #2 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::WireTypePack.3"* @_ZN10emscripten8internal12WireTypePackIJNS_3valERjS3_EEC2EOS2_S3_S3_(%"struct.emscripten::internal::WireTypePack.3"* returned %this, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %args, i32* nonnull align 4 dereferenceable(4) %args1, i32* nonnull align 4 dereferenceable(4) %args3) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack.3"*, align 4
  %args.addr = alloca %"class.emscripten::val"*, align 4
  %args.addr2 = alloca i32*, align 4
  %args.addr4 = alloca i32*, align 4
  %cursor = alloca %"union.emscripten::internal::GenericWireType"*, align 4
  store %"struct.emscripten::internal::WireTypePack.3"* %this, %"struct.emscripten::internal::WireTypePack.3"** %this.addr, align 4
  store %"class.emscripten::val"* %args, %"class.emscripten::val"** %args.addr, align 4
  store i32* %args1, i32** %args.addr2, align 4
  store i32* %args3, i32** %args.addr4, align 4
  %this5 = load %"struct.emscripten::internal::WireTypePack.3"*, %"struct.emscripten::internal::WireTypePack.3"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack.3", %"struct.emscripten::internal::WireTypePack.3"* %this5, i32 0, i32 0
  %elements6 = getelementptr inbounds %"struct.emscripten::internal::WireTypePack.3", %"struct.emscripten::internal::WireTypePack.3"* %this5, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm3EE4dataEv(%"struct.std::__2::array.4"* %elements6) #3
  store %"union.emscripten::internal::GenericWireType"* %call, %"union.emscripten::internal::GenericWireType"** %cursor, align 4
  %0 = load %"class.emscripten::val"*, %"class.emscripten::val"** %args.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.emscripten::val"* @_ZNSt3__27forwardIN10emscripten3valEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %0) #3
  %1 = load i32*, i32** %args.addr2, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #3
  %2 = load i32*, i32** %args.addr4, align 4
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %2) #3
  call void @_ZN10emscripten8internal21writeGenericWireTypesINS_3valEJRjS3_EEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %call7, i32* nonnull align 4 dereferenceable(4) %call8, i32* nonnull align 4 dereferenceable(4) %call9)
  ret %"struct.emscripten::internal::WireTypePack.3"* %this5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valERjS5_EE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"** %this.addr, align 4
  ret i32 3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valERjS5_EE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valERjS4_EEEE3getEv()
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK10emscripten8internal12WireTypePackIJNS_3valERjS3_EEcvPKvEv(%"struct.emscripten::internal::WireTypePack.3"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack.3"*, align 4
  store %"struct.emscripten::internal::WireTypePack.3"* %this, %"struct.emscripten::internal::WireTypePack.3"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WireTypePack.3"*, %"struct.emscripten::internal::WireTypePack.3"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack.3", %"struct.emscripten::internal::WireTypePack.3"* %this1, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm3EE4dataEv(%"struct.std::__2::array.4"* %elements) #3
  %0 = bitcast %"union.emscripten::internal::GenericWireType"* %call to i8*
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm3EE4dataEv(%"struct.std::__2::array.4"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array.4"*, align 4
  store %"struct.std::__2::array.4"* %this, %"struct.std::__2::array.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array.4"*, %"struct.std::__2::array.4"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.4", %"struct.std::__2::array.4"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x %"union.emscripten::internal::GenericWireType"], [3 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: alwaysinline
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesINS_3valEJRjS3_EEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %first, i32* nonnull align 4 dereferenceable(4) %rest, i32* nonnull align 4 dereferenceable(4) %rest1) #6 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %first.addr = alloca %"class.emscripten::val"*, align 4
  %rest.addr = alloca i32*, align 4
  %rest.addr2 = alloca i32*, align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"class.emscripten::val"* %first, %"class.emscripten::val"** %first.addr, align 4
  store i32* %rest, i32** %rest.addr, align 4
  store i32* %rest1, i32** %rest.addr2, align 4
  %0 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %first.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.emscripten::val"* @_ZNSt3__27forwardIN10emscripten3valEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %1) #3
  %call3 = call %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %call)
  call void @_ZN10emscripten8internal20writeGenericWireTypeINS0_7_EM_VALEEEvRPNS0_15GenericWireTypeEPT_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0, %"struct.emscripten::internal::_EM_VAL"* %call3)
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %3 = load i32*, i32** %rest.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #3
  %4 = load i32*, i32** %rest.addr2, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %4) #3
  call void @_ZN10emscripten8internal21writeGenericWireTypesIRjJS2_EEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %2, i32* nonnull align 4 dereferenceable(4) %call4, i32* nonnull align 4 dereferenceable(4) %call5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal20writeGenericWireTypeINS0_7_EM_VALEEEvRPNS0_15GenericWireTypeEPT_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::internal::_EM_VAL"* %wt) #2 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %wt.addr = alloca %"struct.emscripten::internal::_EM_VAL"*, align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* %wt, %"struct.emscripten::internal::_EM_VAL"** %wt.addr, align 4
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %wt.addr, align 4
  %1 = bitcast %"struct.emscripten::internal::_EM_VAL"* %0 to i8*
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %3 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %2, align 4
  %w = bitcast %"union.emscripten::internal::GenericWireType"* %3 to [2 x %union.anon.1]*
  %arrayidx = getelementptr inbounds [2 x %union.anon.1], [2 x %union.anon.1]* %w, i32 0, i32 0
  %p = bitcast %union.anon.1* %arrayidx to i8**
  store i8* %1, i8** %p, align 8
  %4 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %5 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %4, align 4
  %incdec.ptr = getelementptr inbounds %"union.emscripten::internal::GenericWireType", %"union.emscripten::internal::GenericWireType"* %5, i32 1
  store %"union.emscripten::internal::GenericWireType"* %incdec.ptr, %"union.emscripten::internal::GenericWireType"** %4, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %v) #1 comdat {
entry:
  %v.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %v, %"class.emscripten::val"** %v.addr, align 4
  %0 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %0, i32 0, i32 0
  %1 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_incref(%"struct.emscripten::internal::_EM_VAL"* %1)
  %2 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle1 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %2, i32 0, i32 0
  %3 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle1, align 4
  ret %"struct.emscripten::internal::_EM_VAL"* %3
}

; Function Attrs: alwaysinline
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesIRjJS2_EEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, i32* nonnull align 4 dereferenceable(4) %first, i32* nonnull align 4 dereferenceable(4) %rest) #6 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %first.addr = alloca i32*, align 4
  %rest.addr = alloca i32*, align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store i32* %first, i32** %first.addr, align 4
  store i32* %rest, i32** %rest.addr, align 4
  %0 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %1 = load i32*, i32** %first.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #3
  %call1 = call i32 @_ZN10emscripten8internal11BindingTypeIjvE10toWireTypeERKj(i32* nonnull align 4 dereferenceable(4) %call)
  call void @_ZN10emscripten8internal20writeGenericWireTypeIjEEvRPNS0_15GenericWireTypeET_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0, i32 %call1)
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %3 = load i32*, i32** %rest.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #3
  call void @_ZN10emscripten8internal21writeGenericWireTypesIRjJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %2, i32* nonnull align 4 dereferenceable(4) %call2)
  ret void
}

declare void @_emval_incref(%"struct.emscripten::internal::_EM_VAL"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal20writeGenericWireTypeIjEEvRPNS0_15GenericWireTypeET_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, i32 %wt) #2 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %wt.addr = alloca i32, align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store i32 %wt, i32* %wt.addr, align 4
  %0 = load i32, i32* %wt.addr, align 4
  %1 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %2 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %1, align 4
  %w = bitcast %"union.emscripten::internal::GenericWireType"* %2 to [2 x %union.anon.1]*
  %arrayidx = getelementptr inbounds [2 x %union.anon.1], [2 x %union.anon.1]* %w, i32 0, i32 0
  %u = bitcast %union.anon.1* %arrayidx to i32*
  store i32 %0, i32* %u, align 8
  %3 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %4 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %3, align 4
  %incdec.ptr = getelementptr inbounds %"union.emscripten::internal::GenericWireType", %"union.emscripten::internal::GenericWireType"* %4, i32 1
  store %"union.emscripten::internal::GenericWireType"* %incdec.ptr, %"union.emscripten::internal::GenericWireType"** %3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal11BindingTypeIjvE10toWireTypeERKj(i32* nonnull align 4 dereferenceable(4) %v) #2 comdat {
entry:
  %v.addr = alloca i32*, align 4
  store i32* %v, i32** %v.addr, align 4
  %0 = load i32*, i32** %v.addr, align 4
  %1 = load i32, i32* %0, align 4
  ret i32 %1
}

; Function Attrs: alwaysinline nounwind
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesIRjJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, i32* nonnull align 4 dereferenceable(4) %first) #8 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %first.addr = alloca i32*, align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store i32* %first, i32** %first.addr, align 4
  %0 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %1 = load i32*, i32** %first.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRjEEOT_RNS_16remove_referenceIS2_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #3
  %call1 = call i32 @_ZN10emscripten8internal11BindingTypeIjvE10toWireTypeERKj(i32* nonnull align 4 dereferenceable(4) %call)
  call void @_ZN10emscripten8internal20writeGenericWireTypeIjEEvRPNS0_15GenericWireTypeET_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0, i32 %call1)
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  call void @_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valERjS4_EEEE3getEv() #2 comdat {
entry:
  ret i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valERjS4_EEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm3EE4dataEv(%"struct.std::__2::array.4"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array.4"*, align 4
  store %"struct.std::__2::array.4"* %this, %"struct.std::__2::array.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array.4"*, %"struct.std::__2::array.4"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.4", %"struct.std::__2::array.4"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x %"union.emscripten::internal::GenericWireType"], [3 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEEE6invokeEPFS2_S9_EPNS0_11BindingTypeIS9_vEUt_E(void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)* %fn, %struct.anon.6* %args) #1 comdat {
entry:
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, align 4
  %args.addr = alloca %struct.anon.6*, align 4
  %ref.tmp = alloca %"class.emscripten::val", align 4
  %agg.tmp = alloca %"class.std::__2::basic_string", align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)** %fn.addr, align 4
  store %struct.anon.6* %args, %struct.anon.6** %args.addr, align 4
  %0 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)** %fn.addr, align 4
  %1 = load %struct.anon.6*, %struct.anon.6** %args.addr, align 4
  call void @_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E(%"class.std::__2::basic_string"* sret align 4 %agg.tmp, %struct.anon.6* %1)
  call void %0(%"class.emscripten::val"* sret align 4 %ref.tmp, %"class.std::__2::basic_string"* %agg.tmp)
  %call = call %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call1 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %ref.tmp) #3
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %agg.tmp) #3
  ret %"struct.emscripten::internal::_EM_VAL"* %call
}

declare void @_embind_register_function(i8*, i32, i8**, i8*, i8*, i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"** %this.addr, align 4
  ret i32 2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.5"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEEEEE3getEv()
  ret i8** %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEPNS0_11BindingTypeISB_vEUt_EEEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)* %0) #6 comdat {
entry:
  %.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)*, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)* %0, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*)*, %struct.anon.6*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEPNS0_11BindingTypeISB_vEUt_EEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, %struct.anon.6* %v) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %v.addr = alloca %struct.anon.6*, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.anon.6* %v, %struct.anon.6** %v.addr, align 4
  %1 = load %struct.anon.6*, %struct.anon.6** %v.addr, align 4
  %data = getelementptr inbounds %struct.anon.6, %struct.anon.6* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [1 x i8], [1 x i8]* %data, i32 0, i32 0
  %2 = load %struct.anon.6*, %struct.anon.6** %v.addr, align 4
  %length = getelementptr inbounds %struct.anon.6, %struct.anon.6* %2, i32 0, i32 0
  %3 = load i32, i32* %length, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm(%"class.std::__2::basic_string"* %agg.result, i8* %arraydecay, i32 %3)
  ret void
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #9

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm(%"class.std::__2::basic_string"* returned %this, i8* %__s, i32 %__n) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i8*, i8** %__s.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"* %this1, i8* %1, i32 %2)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call5 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #2 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* returned %this) unnamed_addr #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* %1) #3
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEEEEE3getEv() #2 comdat {
entry:
  ret i8** getelementptr inbounds ([2 x i8*], [2 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEEPNS0_11BindingTypeISB_vEUt_EEEEPKcv() #6 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv() #2 comdat {
entry:
  ret i8* getelementptr inbounds ([4 x i8], [4 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_avif_dec.cpp() #0 {
entry:
  call void @__cxx_global_var_init.4()
  ret void
}

; Function Attrs: noinline
define internal void @__tls_init() #0 {
entry:
  %0 = load i8, i8* @__tls_guard, align 1
  %guard.uninitialized = icmp eq i8 %0, 0
  br i1 %guard.uninitialized, label %init, label %exit, !prof !2

init:                                             ; preds = %entry
  store i8 1, i8* @__tls_guard, align 1
  call void @__cxx_global_var_init()
  call void @__cxx_global_var_init.1()
  br label %exit

exit:                                             ; preds = %init, %entry
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { alwaysinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn }
attributes #8 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1023}
