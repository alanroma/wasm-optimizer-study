; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/loopfilter.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/loopfilter.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @aom_lpf_horizontal_4_c(i8* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %mask = alloca i8, align 1
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %4 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %5 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul = mul nsw i32 -2, %5
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %mul
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %6, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %7 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %8 = load i32, i32* %p.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %8
  %arrayidx1 = getelementptr inbounds i8, i8* %7, i32 %sub
  %9 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  store i8 %9, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %10 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %11 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 0, %11
  %arrayidx3 = getelementptr inbounds i8, i8* %10, i32 %mul2
  %12 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  store i8 %12, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %13 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %14 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 1, %14
  %arrayidx5 = getelementptr inbounds i8, i8* %13, i32 %mul4
  %15 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  store i8 %15, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %16 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %17 = load i8, i8* %16, align 1, !tbaa !8
  %18 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %19 = load i8, i8* %18, align 1, !tbaa !8
  %20 = load i8, i8* %p1, align 1, !tbaa !8
  %21 = load i8, i8* %p0, align 1, !tbaa !8
  %22 = load i8, i8* %q0, align 1, !tbaa !8
  %23 = load i8, i8* %q1, align 1, !tbaa !8
  %call = call signext i8 @filter_mask2(i8 zeroext %17, i8 zeroext %19, i8 zeroext %20, i8 zeroext %21, i8 zeroext %22, i8 zeroext %23)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  %24 = load i8, i8* %mask, align 1, !tbaa !8
  %25 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %26 = load i8, i8* %25, align 1, !tbaa !8
  %27 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %28 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 2, %28
  %idx.neg = sub i32 0, %mul6
  %add.ptr = getelementptr inbounds i8, i8* %27, i32 %idx.neg
  %29 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %30 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 1, %30
  %idx.neg8 = sub i32 0, %mul7
  %add.ptr9 = getelementptr inbounds i8, i8* %29, i32 %idx.neg8
  %31 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %32 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %33 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 1, %33
  %add.ptr11 = getelementptr inbounds i8, i8* %32, i32 %mul10
  call void @filter4(i8 signext %24, i8 zeroext %26, i8* %add.ptr, i8* %add.ptr9, i8* %31, i8* %add.ptr11)
  %34 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %34, i32 1
  store i8* %incdec.ptr, i8** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %36 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal signext i8 @filter_mask2(i8 zeroext %limit, i8 zeroext %blimit, i8 zeroext %p1, i8 zeroext %p0, i8 zeroext %q0, i8 zeroext %q1) #2 {
entry:
  %limit.addr = alloca i8, align 1
  %blimit.addr = alloca i8, align 1
  %p1.addr = alloca i8, align 1
  %p0.addr = alloca i8, align 1
  %q0.addr = alloca i8, align 1
  %q1.addr = alloca i8, align 1
  %mask = alloca i8, align 1
  store i8 %limit, i8* %limit.addr, align 1, !tbaa !8
  store i8 %blimit, i8* %blimit.addr, align 1, !tbaa !8
  store i8 %p1, i8* %p1.addr, align 1, !tbaa !8
  store i8 %p0, i8* %p0.addr, align 1, !tbaa !8
  store i8 %q0, i8* %q0.addr, align 1, !tbaa !8
  store i8 %q1, i8* %q1.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %1 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv1 = zext i8 %1 to i32
  %sub = sub nsw i32 %conv, %conv1
  %call = call i32 @abs(i32 %sub) #5
  %2 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv2 = zext i8 %2 to i32
  %cmp = icmp sgt i32 %call, %conv2
  %conv3 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv3, -1
  %3 = load i8, i8* %mask, align 1, !tbaa !8
  %conv4 = sext i8 %3 to i32
  %or = or i32 %conv4, %mul
  %conv5 = trunc i32 %or to i8
  store i8 %conv5, i8* %mask, align 1, !tbaa !8
  %4 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv6 = zext i8 %4 to i32
  %5 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv7 = zext i8 %5 to i32
  %sub8 = sub nsw i32 %conv6, %conv7
  %call9 = call i32 @abs(i32 %sub8) #5
  %6 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv10 = zext i8 %6 to i32
  %cmp11 = icmp sgt i32 %call9, %conv10
  %conv12 = zext i1 %cmp11 to i32
  %mul13 = mul nsw i32 %conv12, -1
  %7 = load i8, i8* %mask, align 1, !tbaa !8
  %conv14 = sext i8 %7 to i32
  %or15 = or i32 %conv14, %mul13
  %conv16 = trunc i32 %or15 to i8
  store i8 %conv16, i8* %mask, align 1, !tbaa !8
  %8 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv17 = zext i8 %8 to i32
  %9 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv18 = zext i8 %9 to i32
  %sub19 = sub nsw i32 %conv17, %conv18
  %call20 = call i32 @abs(i32 %sub19) #5
  %mul21 = mul nsw i32 %call20, 2
  %10 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv22 = zext i8 %10 to i32
  %11 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv23 = zext i8 %11 to i32
  %sub24 = sub nsw i32 %conv22, %conv23
  %call25 = call i32 @abs(i32 %sub24) #5
  %div = sdiv i32 %call25, 2
  %add = add nsw i32 %mul21, %div
  %12 = load i8, i8* %blimit.addr, align 1, !tbaa !8
  %conv26 = zext i8 %12 to i32
  %cmp27 = icmp sgt i32 %add, %conv26
  %conv28 = zext i1 %cmp27 to i32
  %mul29 = mul nsw i32 %conv28, -1
  %13 = load i8, i8* %mask, align 1, !tbaa !8
  %conv30 = sext i8 %13 to i32
  %or31 = or i32 %conv30, %mul29
  %conv32 = trunc i32 %or31 to i8
  store i8 %conv32, i8* %mask, align 1, !tbaa !8
  %14 = load i8, i8* %mask, align 1, !tbaa !8
  %conv33 = sext i8 %14 to i32
  %neg = xor i32 %conv33, -1
  %conv34 = trunc i32 %neg to i8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv34
}

; Function Attrs: inlinehint nounwind
define internal void @filter4(i8 signext %mask, i8 zeroext %thresh, i8* %op1, i8* %op0, i8* %oq0, i8* %oq1) #2 {
entry:
  %mask.addr = alloca i8, align 1
  %thresh.addr = alloca i8, align 1
  %op1.addr = alloca i8*, align 4
  %op0.addr = alloca i8*, align 4
  %oq0.addr = alloca i8*, align 4
  %oq1.addr = alloca i8*, align 4
  %filter1 = alloca i8, align 1
  %filter2 = alloca i8, align 1
  %ps1 = alloca i8, align 1
  %ps0 = alloca i8, align 1
  %qs0 = alloca i8, align 1
  %qs1 = alloca i8, align 1
  %hev = alloca i8, align 1
  %filter = alloca i8, align 1
  store i8 %mask, i8* %mask.addr, align 1, !tbaa !8
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8* %op1, i8** %op1.addr, align 4, !tbaa !2
  store i8* %op0, i8** %op0.addr, align 4, !tbaa !2
  store i8* %oq0, i8** %oq0.addr, align 4, !tbaa !2
  store i8* %oq1, i8** %oq1.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %filter1) #4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %filter2) #4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ps1) #4
  %0 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  %1 = load i8, i8* %0, align 1, !tbaa !8
  %conv = zext i8 %1 to i32
  %xor = xor i32 %conv, 128
  %conv1 = trunc i32 %xor to i8
  store i8 %conv1, i8* %ps1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ps0) #4
  %2 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  %3 = load i8, i8* %2, align 1, !tbaa !8
  %conv2 = zext i8 %3 to i32
  %xor3 = xor i32 %conv2, 128
  %conv4 = trunc i32 %xor3 to i8
  store i8 %conv4, i8* %ps0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %qs0) #4
  %4 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  %5 = load i8, i8* %4, align 1, !tbaa !8
  %conv5 = zext i8 %5 to i32
  %xor6 = xor i32 %conv5, 128
  %conv7 = trunc i32 %xor6 to i8
  store i8 %conv7, i8* %qs0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %qs1) #4
  %6 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  %7 = load i8, i8* %6, align 1, !tbaa !8
  %conv8 = zext i8 %7 to i32
  %xor9 = xor i32 %conv8, 128
  %conv10 = trunc i32 %xor9 to i8
  store i8 %conv10, i8* %qs1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hev) #4
  %8 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %9 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  %10 = load i8, i8* %9, align 1, !tbaa !8
  %11 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  %12 = load i8, i8* %11, align 1, !tbaa !8
  %13 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  %14 = load i8, i8* %13, align 1, !tbaa !8
  %15 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  %16 = load i8, i8* %15, align 1, !tbaa !8
  %call = call signext i8 @hev_mask(i8 zeroext %8, i8 zeroext %10, i8 zeroext %12, i8 zeroext %14, i8 zeroext %16)
  store i8 %call, i8* %hev, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %filter) #4
  %17 = load i8, i8* %ps1, align 1, !tbaa !8
  %conv11 = sext i8 %17 to i32
  %18 = load i8, i8* %qs1, align 1, !tbaa !8
  %conv12 = sext i8 %18 to i32
  %sub = sub nsw i32 %conv11, %conv12
  %call13 = call signext i8 @signed_char_clamp(i32 %sub)
  %conv14 = sext i8 %call13 to i32
  %19 = load i8, i8* %hev, align 1, !tbaa !8
  %conv15 = sext i8 %19 to i32
  %and = and i32 %conv14, %conv15
  %conv16 = trunc i32 %and to i8
  store i8 %conv16, i8* %filter, align 1, !tbaa !8
  %20 = load i8, i8* %filter, align 1, !tbaa !8
  %conv17 = sext i8 %20 to i32
  %21 = load i8, i8* %qs0, align 1, !tbaa !8
  %conv18 = sext i8 %21 to i32
  %22 = load i8, i8* %ps0, align 1, !tbaa !8
  %conv19 = sext i8 %22 to i32
  %sub20 = sub nsw i32 %conv18, %conv19
  %mul = mul nsw i32 3, %sub20
  %add = add nsw i32 %conv17, %mul
  %call21 = call signext i8 @signed_char_clamp(i32 %add)
  %conv22 = sext i8 %call21 to i32
  %23 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %conv23 = sext i8 %23 to i32
  %and24 = and i32 %conv22, %conv23
  %conv25 = trunc i32 %and24 to i8
  store i8 %conv25, i8* %filter, align 1, !tbaa !8
  %24 = load i8, i8* %filter, align 1, !tbaa !8
  %conv26 = sext i8 %24 to i32
  %add27 = add nsw i32 %conv26, 4
  %call28 = call signext i8 @signed_char_clamp(i32 %add27)
  %conv29 = sext i8 %call28 to i32
  %shr = ashr i32 %conv29, 3
  %conv30 = trunc i32 %shr to i8
  store i8 %conv30, i8* %filter1, align 1, !tbaa !8
  %25 = load i8, i8* %filter, align 1, !tbaa !8
  %conv31 = sext i8 %25 to i32
  %add32 = add nsw i32 %conv31, 3
  %call33 = call signext i8 @signed_char_clamp(i32 %add32)
  %conv34 = sext i8 %call33 to i32
  %shr35 = ashr i32 %conv34, 3
  %conv36 = trunc i32 %shr35 to i8
  store i8 %conv36, i8* %filter2, align 1, !tbaa !8
  %26 = load i8, i8* %qs0, align 1, !tbaa !8
  %conv37 = sext i8 %26 to i32
  %27 = load i8, i8* %filter1, align 1, !tbaa !8
  %conv38 = sext i8 %27 to i32
  %sub39 = sub nsw i32 %conv37, %conv38
  %call40 = call signext i8 @signed_char_clamp(i32 %sub39)
  %conv41 = sext i8 %call40 to i32
  %xor42 = xor i32 %conv41, 128
  %conv43 = trunc i32 %xor42 to i8
  %28 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  store i8 %conv43, i8* %28, align 1, !tbaa !8
  %29 = load i8, i8* %ps0, align 1, !tbaa !8
  %conv44 = sext i8 %29 to i32
  %30 = load i8, i8* %filter2, align 1, !tbaa !8
  %conv45 = sext i8 %30 to i32
  %add46 = add nsw i32 %conv44, %conv45
  %call47 = call signext i8 @signed_char_clamp(i32 %add46)
  %conv48 = sext i8 %call47 to i32
  %xor49 = xor i32 %conv48, 128
  %conv50 = trunc i32 %xor49 to i8
  %31 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  store i8 %conv50, i8* %31, align 1, !tbaa !8
  %32 = load i8, i8* %filter1, align 1, !tbaa !8
  %conv51 = sext i8 %32 to i32
  %add52 = add nsw i32 %conv51, 1
  %shr53 = ashr i32 %add52, 1
  %33 = load i8, i8* %hev, align 1, !tbaa !8
  %conv54 = sext i8 %33 to i32
  %neg = xor i32 %conv54, -1
  %and55 = and i32 %shr53, %neg
  %conv56 = trunc i32 %and55 to i8
  store i8 %conv56, i8* %filter, align 1, !tbaa !8
  %34 = load i8, i8* %qs1, align 1, !tbaa !8
  %conv57 = sext i8 %34 to i32
  %35 = load i8, i8* %filter, align 1, !tbaa !8
  %conv58 = sext i8 %35 to i32
  %sub59 = sub nsw i32 %conv57, %conv58
  %call60 = call signext i8 @signed_char_clamp(i32 %sub59)
  %conv61 = sext i8 %call60 to i32
  %xor62 = xor i32 %conv61, 128
  %conv63 = trunc i32 %xor62 to i8
  %36 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  store i8 %conv63, i8* %36, align 1, !tbaa !8
  %37 = load i8, i8* %ps1, align 1, !tbaa !8
  %conv64 = sext i8 %37 to i32
  %38 = load i8, i8* %filter, align 1, !tbaa !8
  %conv65 = sext i8 %38 to i32
  %add66 = add nsw i32 %conv64, %conv65
  %call67 = call signext i8 @signed_char_clamp(i32 %add66)
  %conv68 = sext i8 %call67 to i32
  %xor69 = xor i32 %conv68, 128
  %conv70 = trunc i32 %xor69 to i8
  %39 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  store i8 %conv70, i8* %39, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %filter) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hev) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %qs1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %qs0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ps0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ps1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %filter2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %filter1) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_lpf_horizontal_4_dual_c(i8* %s, i32 %p, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  call void @aom_lpf_horizontal_4_c(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4)
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 4
  %6 = load i32, i32* %p.addr, align 4, !tbaa !6
  %7 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %8 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  call void @aom_lpf_horizontal_4_c(i8* %add.ptr, i32 %6, i8* %7, i8* %8, i8* %9)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_vertical_4_c(i8* %s, i32 %pitch, i8* %blimit, i8* %limit, i8* %thresh) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %mask = alloca i8, align 1
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %4 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 -2
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %5, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %6 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %6, i32 -1
  %7 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  store i8 %7, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %8 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %8, i32 0
  %9 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  store i8 %9, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %10 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %10, i32 1
  %11 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  store i8 %11, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %12 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %13 = load i8, i8* %12, align 1, !tbaa !8
  %14 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %15 = load i8, i8* %14, align 1, !tbaa !8
  %16 = load i8, i8* %p1, align 1, !tbaa !8
  %17 = load i8, i8* %p0, align 1, !tbaa !8
  %18 = load i8, i8* %q0, align 1, !tbaa !8
  %19 = load i8, i8* %q1, align 1, !tbaa !8
  %call = call signext i8 @filter_mask2(i8 zeroext %13, i8 zeroext %15, i8 zeroext %16, i8 zeroext %17, i8 zeroext %18, i8 zeroext %19)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  %20 = load i8, i8* %mask, align 1, !tbaa !8
  %21 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %22 = load i8, i8* %21, align 1, !tbaa !8
  %23 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %23, i32 -2
  %24 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr4 = getelementptr inbounds i8, i8* %24, i32 -1
  %25 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %26 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr5 = getelementptr inbounds i8, i8* %26, i32 1
  call void @filter4(i8 signext %20, i8 zeroext %22, i8* %add.ptr, i8* %add.ptr4, i8* %25, i8* %add.ptr5)
  %27 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %28 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr6 = getelementptr inbounds i8, i8* %28, i32 %27
  store i8* %add.ptr6, i8** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %30 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  %31 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_vertical_4_dual_c(i8* %s, i32 %pitch, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  call void @aom_lpf_vertical_4_c(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4)
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 4, %6
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  %7 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  call void @aom_lpf_vertical_4_c(i8* %add.ptr, i32 %7, i8* %8, i8* %9, i8* %10)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_horizontal_6_c(i8* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p2 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %q2 = alloca i8, align 1
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #4
  %4 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %5 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul = mul nsw i32 -3, %5
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %mul
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %6, i8* %p2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %7 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %8 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 -2, %8
  %arrayidx2 = getelementptr inbounds i8, i8* %7, i32 %mul1
  %9 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  store i8 %9, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %10 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %11 = load i32, i32* %p.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %11
  %arrayidx3 = getelementptr inbounds i8, i8* %10, i32 %sub
  %12 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  store i8 %12, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %13 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %14 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 0, %14
  %arrayidx5 = getelementptr inbounds i8, i8* %13, i32 %mul4
  %15 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  store i8 %15, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %16 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %17 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 1, %17
  %arrayidx7 = getelementptr inbounds i8, i8* %16, i32 %mul6
  %18 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  store i8 %18, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q2) #4
  %19 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %20 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul8 = mul nsw i32 2, %20
  %arrayidx9 = getelementptr inbounds i8, i8* %19, i32 %mul8
  %21 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  store i8 %21, i8* %q2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %22 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %23 = load i8, i8* %22, align 1, !tbaa !8
  %24 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %25 = load i8, i8* %24, align 1, !tbaa !8
  %26 = load i8, i8* %p2, align 1, !tbaa !8
  %27 = load i8, i8* %p1, align 1, !tbaa !8
  %28 = load i8, i8* %p0, align 1, !tbaa !8
  %29 = load i8, i8* %q0, align 1, !tbaa !8
  %30 = load i8, i8* %q1, align 1, !tbaa !8
  %31 = load i8, i8* %q2, align 1, !tbaa !8
  %call = call signext i8 @filter_mask3_chroma(i8 zeroext %23, i8 zeroext %25, i8 zeroext %26, i8 zeroext %27, i8 zeroext %28, i8 zeroext %29, i8 zeroext %30, i8 zeroext %31)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %32 = load i8, i8* %p2, align 1, !tbaa !8
  %33 = load i8, i8* %p1, align 1, !tbaa !8
  %34 = load i8, i8* %p0, align 1, !tbaa !8
  %35 = load i8, i8* %q0, align 1, !tbaa !8
  %36 = load i8, i8* %q1, align 1, !tbaa !8
  %37 = load i8, i8* %q2, align 1, !tbaa !8
  %call10 = call signext i8 @flat_mask3_chroma(i8 zeroext 1, i8 zeroext %32, i8 zeroext %33, i8 zeroext %34, i8 zeroext %35, i8 zeroext %36, i8 zeroext %37)
  store i8 %call10, i8* %flat, align 1, !tbaa !8
  %38 = load i8, i8* %mask, align 1, !tbaa !8
  %39 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %40 = load i8, i8* %39, align 1, !tbaa !8
  %41 = load i8, i8* %flat, align 1, !tbaa !8
  %42 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %43 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 3, %43
  %idx.neg = sub i32 0, %mul11
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 %idx.neg
  %44 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %45 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul12 = mul nsw i32 2, %45
  %idx.neg13 = sub i32 0, %mul12
  %add.ptr14 = getelementptr inbounds i8, i8* %44, i32 %idx.neg13
  %46 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %47 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 1, %47
  %idx.neg16 = sub i32 0, %mul15
  %add.ptr17 = getelementptr inbounds i8, i8* %46, i32 %idx.neg16
  %48 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %49 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %50 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 1, %50
  %add.ptr19 = getelementptr inbounds i8, i8* %49, i32 %mul18
  %51 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %52 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 2, %52
  %add.ptr21 = getelementptr inbounds i8, i8* %51, i32 %mul20
  call void @filter6(i8 signext %38, i8 zeroext %40, i8 signext %41, i8* %add.ptr, i8* %add.ptr14, i8* %add.ptr17, i8* %48, i8* %add.ptr19, i8* %add.ptr21)
  %53 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %53, i32 1
  store i8* %incdec.ptr, i8** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %55 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  %56 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @filter_mask3_chroma(i8 zeroext %limit, i8 zeroext %blimit, i8 zeroext %p2, i8 zeroext %p1, i8 zeroext %p0, i8 zeroext %q0, i8 zeroext %q1, i8 zeroext %q2) #2 {
entry:
  %limit.addr = alloca i8, align 1
  %blimit.addr = alloca i8, align 1
  %p2.addr = alloca i8, align 1
  %p1.addr = alloca i8, align 1
  %p0.addr = alloca i8, align 1
  %q0.addr = alloca i8, align 1
  %q1.addr = alloca i8, align 1
  %q2.addr = alloca i8, align 1
  %mask = alloca i8, align 1
  store i8 %limit, i8* %limit.addr, align 1, !tbaa !8
  store i8 %blimit, i8* %blimit.addr, align 1, !tbaa !8
  store i8 %p2, i8* %p2.addr, align 1, !tbaa !8
  store i8 %p1, i8* %p1.addr, align 1, !tbaa !8
  store i8 %p0, i8* %p0.addr, align 1, !tbaa !8
  store i8 %q0, i8* %q0.addr, align 1, !tbaa !8
  store i8 %q1, i8* %q1.addr, align 1, !tbaa !8
  store i8 %q2, i8* %q2.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = load i8, i8* %p2.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %1 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv1 = zext i8 %1 to i32
  %sub = sub nsw i32 %conv, %conv1
  %call = call i32 @abs(i32 %sub) #5
  %2 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv2 = zext i8 %2 to i32
  %cmp = icmp sgt i32 %call, %conv2
  %conv3 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv3, -1
  %3 = load i8, i8* %mask, align 1, !tbaa !8
  %conv4 = sext i8 %3 to i32
  %or = or i32 %conv4, %mul
  %conv5 = trunc i32 %or to i8
  store i8 %conv5, i8* %mask, align 1, !tbaa !8
  %4 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv6 = zext i8 %4 to i32
  %5 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv7 = zext i8 %5 to i32
  %sub8 = sub nsw i32 %conv6, %conv7
  %call9 = call i32 @abs(i32 %sub8) #5
  %6 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv10 = zext i8 %6 to i32
  %cmp11 = icmp sgt i32 %call9, %conv10
  %conv12 = zext i1 %cmp11 to i32
  %mul13 = mul nsw i32 %conv12, -1
  %7 = load i8, i8* %mask, align 1, !tbaa !8
  %conv14 = sext i8 %7 to i32
  %or15 = or i32 %conv14, %mul13
  %conv16 = trunc i32 %or15 to i8
  store i8 %conv16, i8* %mask, align 1, !tbaa !8
  %8 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv17 = zext i8 %8 to i32
  %9 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv18 = zext i8 %9 to i32
  %sub19 = sub nsw i32 %conv17, %conv18
  %call20 = call i32 @abs(i32 %sub19) #5
  %10 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv21 = zext i8 %10 to i32
  %cmp22 = icmp sgt i32 %call20, %conv21
  %conv23 = zext i1 %cmp22 to i32
  %mul24 = mul nsw i32 %conv23, -1
  %11 = load i8, i8* %mask, align 1, !tbaa !8
  %conv25 = sext i8 %11 to i32
  %or26 = or i32 %conv25, %mul24
  %conv27 = trunc i32 %or26 to i8
  store i8 %conv27, i8* %mask, align 1, !tbaa !8
  %12 = load i8, i8* %q2.addr, align 1, !tbaa !8
  %conv28 = zext i8 %12 to i32
  %13 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv29 = zext i8 %13 to i32
  %sub30 = sub nsw i32 %conv28, %conv29
  %call31 = call i32 @abs(i32 %sub30) #5
  %14 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv32 = zext i8 %14 to i32
  %cmp33 = icmp sgt i32 %call31, %conv32
  %conv34 = zext i1 %cmp33 to i32
  %mul35 = mul nsw i32 %conv34, -1
  %15 = load i8, i8* %mask, align 1, !tbaa !8
  %conv36 = sext i8 %15 to i32
  %or37 = or i32 %conv36, %mul35
  %conv38 = trunc i32 %or37 to i8
  store i8 %conv38, i8* %mask, align 1, !tbaa !8
  %16 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv39 = zext i8 %16 to i32
  %17 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv40 = zext i8 %17 to i32
  %sub41 = sub nsw i32 %conv39, %conv40
  %call42 = call i32 @abs(i32 %sub41) #5
  %mul43 = mul nsw i32 %call42, 2
  %18 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv44 = zext i8 %18 to i32
  %19 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv45 = zext i8 %19 to i32
  %sub46 = sub nsw i32 %conv44, %conv45
  %call47 = call i32 @abs(i32 %sub46) #5
  %div = sdiv i32 %call47, 2
  %add = add nsw i32 %mul43, %div
  %20 = load i8, i8* %blimit.addr, align 1, !tbaa !8
  %conv48 = zext i8 %20 to i32
  %cmp49 = icmp sgt i32 %add, %conv48
  %conv50 = zext i1 %cmp49 to i32
  %mul51 = mul nsw i32 %conv50, -1
  %21 = load i8, i8* %mask, align 1, !tbaa !8
  %conv52 = sext i8 %21 to i32
  %or53 = or i32 %conv52, %mul51
  %conv54 = trunc i32 %or53 to i8
  store i8 %conv54, i8* %mask, align 1, !tbaa !8
  %22 = load i8, i8* %mask, align 1, !tbaa !8
  %conv55 = sext i8 %22 to i32
  %neg = xor i32 %conv55, -1
  %conv56 = trunc i32 %neg to i8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv56
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @flat_mask3_chroma(i8 zeroext %thresh, i8 zeroext %p2, i8 zeroext %p1, i8 zeroext %p0, i8 zeroext %q0, i8 zeroext %q1, i8 zeroext %q2) #2 {
entry:
  %thresh.addr = alloca i8, align 1
  %p2.addr = alloca i8, align 1
  %p1.addr = alloca i8, align 1
  %p0.addr = alloca i8, align 1
  %q0.addr = alloca i8, align 1
  %q1.addr = alloca i8, align 1
  %q2.addr = alloca i8, align 1
  %mask = alloca i8, align 1
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8 %p2, i8* %p2.addr, align 1, !tbaa !8
  store i8 %p1, i8* %p1.addr, align 1, !tbaa !8
  store i8 %p0, i8* %p0.addr, align 1, !tbaa !8
  store i8 %q0, i8* %q0.addr, align 1, !tbaa !8
  store i8 %q1, i8* %q1.addr, align 1, !tbaa !8
  store i8 %q2, i8* %q2.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %1 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv1 = zext i8 %1 to i32
  %sub = sub nsw i32 %conv, %conv1
  %call = call i32 @abs(i32 %sub) #5
  %2 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv2 = zext i8 %2 to i32
  %cmp = icmp sgt i32 %call, %conv2
  %conv3 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv3, -1
  %3 = load i8, i8* %mask, align 1, !tbaa !8
  %conv4 = sext i8 %3 to i32
  %or = or i32 %conv4, %mul
  %conv5 = trunc i32 %or to i8
  store i8 %conv5, i8* %mask, align 1, !tbaa !8
  %4 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv6 = zext i8 %4 to i32
  %5 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv7 = zext i8 %5 to i32
  %sub8 = sub nsw i32 %conv6, %conv7
  %call9 = call i32 @abs(i32 %sub8) #5
  %6 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv10 = zext i8 %6 to i32
  %cmp11 = icmp sgt i32 %call9, %conv10
  %conv12 = zext i1 %cmp11 to i32
  %mul13 = mul nsw i32 %conv12, -1
  %7 = load i8, i8* %mask, align 1, !tbaa !8
  %conv14 = sext i8 %7 to i32
  %or15 = or i32 %conv14, %mul13
  %conv16 = trunc i32 %or15 to i8
  store i8 %conv16, i8* %mask, align 1, !tbaa !8
  %8 = load i8, i8* %p2.addr, align 1, !tbaa !8
  %conv17 = zext i8 %8 to i32
  %9 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv18 = zext i8 %9 to i32
  %sub19 = sub nsw i32 %conv17, %conv18
  %call20 = call i32 @abs(i32 %sub19) #5
  %10 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv21 = zext i8 %10 to i32
  %cmp22 = icmp sgt i32 %call20, %conv21
  %conv23 = zext i1 %cmp22 to i32
  %mul24 = mul nsw i32 %conv23, -1
  %11 = load i8, i8* %mask, align 1, !tbaa !8
  %conv25 = sext i8 %11 to i32
  %or26 = or i32 %conv25, %mul24
  %conv27 = trunc i32 %or26 to i8
  store i8 %conv27, i8* %mask, align 1, !tbaa !8
  %12 = load i8, i8* %q2.addr, align 1, !tbaa !8
  %conv28 = zext i8 %12 to i32
  %13 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv29 = zext i8 %13 to i32
  %sub30 = sub nsw i32 %conv28, %conv29
  %call31 = call i32 @abs(i32 %sub30) #5
  %14 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv32 = zext i8 %14 to i32
  %cmp33 = icmp sgt i32 %call31, %conv32
  %conv34 = zext i1 %cmp33 to i32
  %mul35 = mul nsw i32 %conv34, -1
  %15 = load i8, i8* %mask, align 1, !tbaa !8
  %conv36 = sext i8 %15 to i32
  %or37 = or i32 %conv36, %mul35
  %conv38 = trunc i32 %or37 to i8
  store i8 %conv38, i8* %mask, align 1, !tbaa !8
  %16 = load i8, i8* %mask, align 1, !tbaa !8
  %conv39 = sext i8 %16 to i32
  %neg = xor i32 %conv39, -1
  %conv40 = trunc i32 %neg to i8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv40
}

; Function Attrs: inlinehint nounwind
define internal void @filter6(i8 signext %mask, i8 zeroext %thresh, i8 signext %flat, i8* %op2, i8* %op1, i8* %op0, i8* %oq0, i8* %oq1, i8* %oq2) #2 {
entry:
  %mask.addr = alloca i8, align 1
  %thresh.addr = alloca i8, align 1
  %flat.addr = alloca i8, align 1
  %op2.addr = alloca i8*, align 4
  %op1.addr = alloca i8*, align 4
  %op0.addr = alloca i8*, align 4
  %oq0.addr = alloca i8*, align 4
  %oq1.addr = alloca i8*, align 4
  %oq2.addr = alloca i8*, align 4
  %p2 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %q2 = alloca i8, align 1
  store i8 %mask, i8* %mask.addr, align 1, !tbaa !8
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8 %flat, i8* %flat.addr, align 1, !tbaa !8
  store i8* %op2, i8** %op2.addr, align 4, !tbaa !2
  store i8* %op1, i8** %op1.addr, align 4, !tbaa !2
  store i8* %op0, i8** %op0.addr, align 4, !tbaa !2
  store i8* %oq0, i8** %oq0.addr, align 4, !tbaa !2
  store i8* %oq1, i8** %oq1.addr, align 4, !tbaa !2
  store i8* %oq2, i8** %oq2.addr, align 4, !tbaa !2
  %0 = load i8, i8* %flat.addr, align 1, !tbaa !8
  %conv = sext i8 %0 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %conv1 = sext i8 %1 to i32
  %tobool2 = icmp ne i32 %conv1, 0
  br i1 %tobool2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #4
  %2 = load i8*, i8** %op2.addr, align 4, !tbaa !2
  %3 = load i8, i8* %2, align 1, !tbaa !8
  store i8 %3, i8* %p2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %4 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  %5 = load i8, i8* %4, align 1, !tbaa !8
  store i8 %5, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %6 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  %7 = load i8, i8* %6, align 1, !tbaa !8
  store i8 %7, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %8 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  %9 = load i8, i8* %8, align 1, !tbaa !8
  store i8 %9, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %10 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  %11 = load i8, i8* %10, align 1, !tbaa !8
  store i8 %11, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q2) #4
  %12 = load i8*, i8** %oq2.addr, align 4, !tbaa !2
  %13 = load i8, i8* %12, align 1, !tbaa !8
  store i8 %13, i8* %q2, align 1, !tbaa !8
  %14 = load i8, i8* %p2, align 1, !tbaa !8
  %conv3 = zext i8 %14 to i32
  %mul = mul nsw i32 %conv3, 3
  %15 = load i8, i8* %p1, align 1, !tbaa !8
  %conv4 = zext i8 %15 to i32
  %mul5 = mul nsw i32 %conv4, 2
  %add = add nsw i32 %mul, %mul5
  %16 = load i8, i8* %p0, align 1, !tbaa !8
  %conv6 = zext i8 %16 to i32
  %mul7 = mul nsw i32 %conv6, 2
  %add8 = add nsw i32 %add, %mul7
  %17 = load i8, i8* %q0, align 1, !tbaa !8
  %conv9 = zext i8 %17 to i32
  %add10 = add nsw i32 %add8, %conv9
  %add11 = add nsw i32 %add10, 4
  %shr = ashr i32 %add11, 3
  %conv12 = trunc i32 %shr to i8
  %18 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  store i8 %conv12, i8* %18, align 1, !tbaa !8
  %19 = load i8, i8* %p2, align 1, !tbaa !8
  %conv13 = zext i8 %19 to i32
  %20 = load i8, i8* %p1, align 1, !tbaa !8
  %conv14 = zext i8 %20 to i32
  %mul15 = mul nsw i32 %conv14, 2
  %add16 = add nsw i32 %conv13, %mul15
  %21 = load i8, i8* %p0, align 1, !tbaa !8
  %conv17 = zext i8 %21 to i32
  %mul18 = mul nsw i32 %conv17, 2
  %add19 = add nsw i32 %add16, %mul18
  %22 = load i8, i8* %q0, align 1, !tbaa !8
  %conv20 = zext i8 %22 to i32
  %mul21 = mul nsw i32 %conv20, 2
  %add22 = add nsw i32 %add19, %mul21
  %23 = load i8, i8* %q1, align 1, !tbaa !8
  %conv23 = zext i8 %23 to i32
  %add24 = add nsw i32 %add22, %conv23
  %add25 = add nsw i32 %add24, 4
  %shr26 = ashr i32 %add25, 3
  %conv27 = trunc i32 %shr26 to i8
  %24 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  store i8 %conv27, i8* %24, align 1, !tbaa !8
  %25 = load i8, i8* %p1, align 1, !tbaa !8
  %conv28 = zext i8 %25 to i32
  %26 = load i8, i8* %p0, align 1, !tbaa !8
  %conv29 = zext i8 %26 to i32
  %mul30 = mul nsw i32 %conv29, 2
  %add31 = add nsw i32 %conv28, %mul30
  %27 = load i8, i8* %q0, align 1, !tbaa !8
  %conv32 = zext i8 %27 to i32
  %mul33 = mul nsw i32 %conv32, 2
  %add34 = add nsw i32 %add31, %mul33
  %28 = load i8, i8* %q1, align 1, !tbaa !8
  %conv35 = zext i8 %28 to i32
  %mul36 = mul nsw i32 %conv35, 2
  %add37 = add nsw i32 %add34, %mul36
  %29 = load i8, i8* %q2, align 1, !tbaa !8
  %conv38 = zext i8 %29 to i32
  %add39 = add nsw i32 %add37, %conv38
  %add40 = add nsw i32 %add39, 4
  %shr41 = ashr i32 %add40, 3
  %conv42 = trunc i32 %shr41 to i8
  %30 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  store i8 %conv42, i8* %30, align 1, !tbaa !8
  %31 = load i8, i8* %p0, align 1, !tbaa !8
  %conv43 = zext i8 %31 to i32
  %32 = load i8, i8* %q0, align 1, !tbaa !8
  %conv44 = zext i8 %32 to i32
  %mul45 = mul nsw i32 %conv44, 2
  %add46 = add nsw i32 %conv43, %mul45
  %33 = load i8, i8* %q1, align 1, !tbaa !8
  %conv47 = zext i8 %33 to i32
  %mul48 = mul nsw i32 %conv47, 2
  %add49 = add nsw i32 %add46, %mul48
  %34 = load i8, i8* %q2, align 1, !tbaa !8
  %conv50 = zext i8 %34 to i32
  %mul51 = mul nsw i32 %conv50, 3
  %add52 = add nsw i32 %add49, %mul51
  %add53 = add nsw i32 %add52, 4
  %shr54 = ashr i32 %add53, 3
  %conv55 = trunc i32 %shr54 to i8
  %35 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  store i8 %conv55, i8* %35, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %entry
  %36 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %37 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %38 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  %39 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  %40 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  %41 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  call void @filter4(i8 signext %36, i8 zeroext %37, i8* %38, i8* %39, i8* %40, i8* %41)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_horizontal_6_dual_c(i8* %s, i32 %p, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  call void @aom_lpf_horizontal_6_c(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4)
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 4
  %6 = load i32, i32* %p.addr, align 4, !tbaa !6
  %7 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %8 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  call void @aom_lpf_horizontal_6_c(i8* %add.ptr, i32 %6, i8* %7, i8* %8, i8* %9)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_horizontal_8_c(i8* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p3 = alloca i8, align 1
  %p2 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %q2 = alloca i8, align 1
  %q3 = alloca i8, align 1
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p3) #4
  %4 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %5 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul = mul nsw i32 -4, %5
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %mul
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %6, i8* %p3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #4
  %7 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %8 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 -3, %8
  %arrayidx2 = getelementptr inbounds i8, i8* %7, i32 %mul1
  %9 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  store i8 %9, i8* %p2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %10 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %11 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 -2, %11
  %arrayidx4 = getelementptr inbounds i8, i8* %10, i32 %mul3
  %12 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  store i8 %12, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %13 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %14 = load i32, i32* %p.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %14
  %arrayidx5 = getelementptr inbounds i8, i8* %13, i32 %sub
  %15 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  store i8 %15, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %16 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %17 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 0, %17
  %arrayidx7 = getelementptr inbounds i8, i8* %16, i32 %mul6
  %18 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  store i8 %18, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %19 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %20 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul8 = mul nsw i32 1, %20
  %arrayidx9 = getelementptr inbounds i8, i8* %19, i32 %mul8
  %21 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  store i8 %21, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q2) #4
  %22 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %23 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 2, %23
  %arrayidx11 = getelementptr inbounds i8, i8* %22, i32 %mul10
  %24 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  store i8 %24, i8* %q2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q3) #4
  %25 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %26 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul12 = mul nsw i32 3, %26
  %arrayidx13 = getelementptr inbounds i8, i8* %25, i32 %mul12
  %27 = load i8, i8* %arrayidx13, align 1, !tbaa !8
  store i8 %27, i8* %q3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %28 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %29 = load i8, i8* %28, align 1, !tbaa !8
  %30 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !8
  %32 = load i8, i8* %p3, align 1, !tbaa !8
  %33 = load i8, i8* %p2, align 1, !tbaa !8
  %34 = load i8, i8* %p1, align 1, !tbaa !8
  %35 = load i8, i8* %p0, align 1, !tbaa !8
  %36 = load i8, i8* %q0, align 1, !tbaa !8
  %37 = load i8, i8* %q1, align 1, !tbaa !8
  %38 = load i8, i8* %q2, align 1, !tbaa !8
  %39 = load i8, i8* %q3, align 1, !tbaa !8
  %call = call signext i8 @filter_mask(i8 zeroext %29, i8 zeroext %31, i8 zeroext %32, i8 zeroext %33, i8 zeroext %34, i8 zeroext %35, i8 zeroext %36, i8 zeroext %37, i8 zeroext %38, i8 zeroext %39)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %40 = load i8, i8* %p3, align 1, !tbaa !8
  %41 = load i8, i8* %p2, align 1, !tbaa !8
  %42 = load i8, i8* %p1, align 1, !tbaa !8
  %43 = load i8, i8* %p0, align 1, !tbaa !8
  %44 = load i8, i8* %q0, align 1, !tbaa !8
  %45 = load i8, i8* %q1, align 1, !tbaa !8
  %46 = load i8, i8* %q2, align 1, !tbaa !8
  %47 = load i8, i8* %q3, align 1, !tbaa !8
  %call14 = call signext i8 @flat_mask4(i8 zeroext 1, i8 zeroext %40, i8 zeroext %41, i8 zeroext %42, i8 zeroext %43, i8 zeroext %44, i8 zeroext %45, i8 zeroext %46, i8 zeroext %47)
  store i8 %call14, i8* %flat, align 1, !tbaa !8
  %48 = load i8, i8* %mask, align 1, !tbaa !8
  %49 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !8
  %51 = load i8, i8* %flat, align 1, !tbaa !8
  %52 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %53 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 4, %53
  %idx.neg = sub i32 0, %mul15
  %add.ptr = getelementptr inbounds i8, i8* %52, i32 %idx.neg
  %54 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %55 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul16 = mul nsw i32 3, %55
  %idx.neg17 = sub i32 0, %mul16
  %add.ptr18 = getelementptr inbounds i8, i8* %54, i32 %idx.neg17
  %56 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %57 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul19 = mul nsw i32 2, %57
  %idx.neg20 = sub i32 0, %mul19
  %add.ptr21 = getelementptr inbounds i8, i8* %56, i32 %idx.neg20
  %58 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %59 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul22 = mul nsw i32 1, %59
  %idx.neg23 = sub i32 0, %mul22
  %add.ptr24 = getelementptr inbounds i8, i8* %58, i32 %idx.neg23
  %60 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %61 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %62 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 1, %62
  %add.ptr26 = getelementptr inbounds i8, i8* %61, i32 %mul25
  %63 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %64 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul27 = mul nsw i32 2, %64
  %add.ptr28 = getelementptr inbounds i8, i8* %63, i32 %mul27
  %65 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %66 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 3, %66
  %add.ptr30 = getelementptr inbounds i8, i8* %65, i32 %mul29
  call void @filter8(i8 signext %48, i8 zeroext %50, i8 signext %51, i8* %add.ptr, i8* %add.ptr18, i8* %add.ptr21, i8* %add.ptr24, i8* %60, i8* %add.ptr26, i8* %add.ptr28, i8* %add.ptr30)
  %67 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %67, i32 1
  store i8* %incdec.ptr, i8** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q3) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p3) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %68 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %68, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %69 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %70 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @filter_mask(i8 zeroext %limit, i8 zeroext %blimit, i8 zeroext %p3, i8 zeroext %p2, i8 zeroext %p1, i8 zeroext %p0, i8 zeroext %q0, i8 zeroext %q1, i8 zeroext %q2, i8 zeroext %q3) #2 {
entry:
  %limit.addr = alloca i8, align 1
  %blimit.addr = alloca i8, align 1
  %p3.addr = alloca i8, align 1
  %p2.addr = alloca i8, align 1
  %p1.addr = alloca i8, align 1
  %p0.addr = alloca i8, align 1
  %q0.addr = alloca i8, align 1
  %q1.addr = alloca i8, align 1
  %q2.addr = alloca i8, align 1
  %q3.addr = alloca i8, align 1
  %mask = alloca i8, align 1
  store i8 %limit, i8* %limit.addr, align 1, !tbaa !8
  store i8 %blimit, i8* %blimit.addr, align 1, !tbaa !8
  store i8 %p3, i8* %p3.addr, align 1, !tbaa !8
  store i8 %p2, i8* %p2.addr, align 1, !tbaa !8
  store i8 %p1, i8* %p1.addr, align 1, !tbaa !8
  store i8 %p0, i8* %p0.addr, align 1, !tbaa !8
  store i8 %q0, i8* %q0.addr, align 1, !tbaa !8
  store i8 %q1, i8* %q1.addr, align 1, !tbaa !8
  store i8 %q2, i8* %q2.addr, align 1, !tbaa !8
  store i8 %q3, i8* %q3.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = load i8, i8* %p3.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %1 = load i8, i8* %p2.addr, align 1, !tbaa !8
  %conv1 = zext i8 %1 to i32
  %sub = sub nsw i32 %conv, %conv1
  %call = call i32 @abs(i32 %sub) #5
  %2 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv2 = zext i8 %2 to i32
  %cmp = icmp sgt i32 %call, %conv2
  %conv3 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv3, -1
  %3 = load i8, i8* %mask, align 1, !tbaa !8
  %conv4 = sext i8 %3 to i32
  %or = or i32 %conv4, %mul
  %conv5 = trunc i32 %or to i8
  store i8 %conv5, i8* %mask, align 1, !tbaa !8
  %4 = load i8, i8* %p2.addr, align 1, !tbaa !8
  %conv6 = zext i8 %4 to i32
  %5 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv7 = zext i8 %5 to i32
  %sub8 = sub nsw i32 %conv6, %conv7
  %call9 = call i32 @abs(i32 %sub8) #5
  %6 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv10 = zext i8 %6 to i32
  %cmp11 = icmp sgt i32 %call9, %conv10
  %conv12 = zext i1 %cmp11 to i32
  %mul13 = mul nsw i32 %conv12, -1
  %7 = load i8, i8* %mask, align 1, !tbaa !8
  %conv14 = sext i8 %7 to i32
  %or15 = or i32 %conv14, %mul13
  %conv16 = trunc i32 %or15 to i8
  store i8 %conv16, i8* %mask, align 1, !tbaa !8
  %8 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv17 = zext i8 %8 to i32
  %9 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv18 = zext i8 %9 to i32
  %sub19 = sub nsw i32 %conv17, %conv18
  %call20 = call i32 @abs(i32 %sub19) #5
  %10 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv21 = zext i8 %10 to i32
  %cmp22 = icmp sgt i32 %call20, %conv21
  %conv23 = zext i1 %cmp22 to i32
  %mul24 = mul nsw i32 %conv23, -1
  %11 = load i8, i8* %mask, align 1, !tbaa !8
  %conv25 = sext i8 %11 to i32
  %or26 = or i32 %conv25, %mul24
  %conv27 = trunc i32 %or26 to i8
  store i8 %conv27, i8* %mask, align 1, !tbaa !8
  %12 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv28 = zext i8 %12 to i32
  %13 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv29 = zext i8 %13 to i32
  %sub30 = sub nsw i32 %conv28, %conv29
  %call31 = call i32 @abs(i32 %sub30) #5
  %14 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv32 = zext i8 %14 to i32
  %cmp33 = icmp sgt i32 %call31, %conv32
  %conv34 = zext i1 %cmp33 to i32
  %mul35 = mul nsw i32 %conv34, -1
  %15 = load i8, i8* %mask, align 1, !tbaa !8
  %conv36 = sext i8 %15 to i32
  %or37 = or i32 %conv36, %mul35
  %conv38 = trunc i32 %or37 to i8
  store i8 %conv38, i8* %mask, align 1, !tbaa !8
  %16 = load i8, i8* %q2.addr, align 1, !tbaa !8
  %conv39 = zext i8 %16 to i32
  %17 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv40 = zext i8 %17 to i32
  %sub41 = sub nsw i32 %conv39, %conv40
  %call42 = call i32 @abs(i32 %sub41) #5
  %18 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv43 = zext i8 %18 to i32
  %cmp44 = icmp sgt i32 %call42, %conv43
  %conv45 = zext i1 %cmp44 to i32
  %mul46 = mul nsw i32 %conv45, -1
  %19 = load i8, i8* %mask, align 1, !tbaa !8
  %conv47 = sext i8 %19 to i32
  %or48 = or i32 %conv47, %mul46
  %conv49 = trunc i32 %or48 to i8
  store i8 %conv49, i8* %mask, align 1, !tbaa !8
  %20 = load i8, i8* %q3.addr, align 1, !tbaa !8
  %conv50 = zext i8 %20 to i32
  %21 = load i8, i8* %q2.addr, align 1, !tbaa !8
  %conv51 = zext i8 %21 to i32
  %sub52 = sub nsw i32 %conv50, %conv51
  %call53 = call i32 @abs(i32 %sub52) #5
  %22 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv54 = zext i8 %22 to i32
  %cmp55 = icmp sgt i32 %call53, %conv54
  %conv56 = zext i1 %cmp55 to i32
  %mul57 = mul nsw i32 %conv56, -1
  %23 = load i8, i8* %mask, align 1, !tbaa !8
  %conv58 = sext i8 %23 to i32
  %or59 = or i32 %conv58, %mul57
  %conv60 = trunc i32 %or59 to i8
  store i8 %conv60, i8* %mask, align 1, !tbaa !8
  %24 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv61 = zext i8 %24 to i32
  %25 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv62 = zext i8 %25 to i32
  %sub63 = sub nsw i32 %conv61, %conv62
  %call64 = call i32 @abs(i32 %sub63) #5
  %mul65 = mul nsw i32 %call64, 2
  %26 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv66 = zext i8 %26 to i32
  %27 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv67 = zext i8 %27 to i32
  %sub68 = sub nsw i32 %conv66, %conv67
  %call69 = call i32 @abs(i32 %sub68) #5
  %div = sdiv i32 %call69, 2
  %add = add nsw i32 %mul65, %div
  %28 = load i8, i8* %blimit.addr, align 1, !tbaa !8
  %conv70 = zext i8 %28 to i32
  %cmp71 = icmp sgt i32 %add, %conv70
  %conv72 = zext i1 %cmp71 to i32
  %mul73 = mul nsw i32 %conv72, -1
  %29 = load i8, i8* %mask, align 1, !tbaa !8
  %conv74 = sext i8 %29 to i32
  %or75 = or i32 %conv74, %mul73
  %conv76 = trunc i32 %or75 to i8
  store i8 %conv76, i8* %mask, align 1, !tbaa !8
  %30 = load i8, i8* %mask, align 1, !tbaa !8
  %conv77 = sext i8 %30 to i32
  %neg = xor i32 %conv77, -1
  %conv78 = trunc i32 %neg to i8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv78
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @flat_mask4(i8 zeroext %thresh, i8 zeroext %p3, i8 zeroext %p2, i8 zeroext %p1, i8 zeroext %p0, i8 zeroext %q0, i8 zeroext %q1, i8 zeroext %q2, i8 zeroext %q3) #2 {
entry:
  %thresh.addr = alloca i8, align 1
  %p3.addr = alloca i8, align 1
  %p2.addr = alloca i8, align 1
  %p1.addr = alloca i8, align 1
  %p0.addr = alloca i8, align 1
  %q0.addr = alloca i8, align 1
  %q1.addr = alloca i8, align 1
  %q2.addr = alloca i8, align 1
  %q3.addr = alloca i8, align 1
  %mask = alloca i8, align 1
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8 %p3, i8* %p3.addr, align 1, !tbaa !8
  store i8 %p2, i8* %p2.addr, align 1, !tbaa !8
  store i8 %p1, i8* %p1.addr, align 1, !tbaa !8
  store i8 %p0, i8* %p0.addr, align 1, !tbaa !8
  store i8 %q0, i8* %q0.addr, align 1, !tbaa !8
  store i8 %q1, i8* %q1.addr, align 1, !tbaa !8
  store i8 %q2, i8* %q2.addr, align 1, !tbaa !8
  store i8 %q3, i8* %q3.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %1 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv1 = zext i8 %1 to i32
  %sub = sub nsw i32 %conv, %conv1
  %call = call i32 @abs(i32 %sub) #5
  %2 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv2 = zext i8 %2 to i32
  %cmp = icmp sgt i32 %call, %conv2
  %conv3 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv3, -1
  %3 = load i8, i8* %mask, align 1, !tbaa !8
  %conv4 = sext i8 %3 to i32
  %or = or i32 %conv4, %mul
  %conv5 = trunc i32 %or to i8
  store i8 %conv5, i8* %mask, align 1, !tbaa !8
  %4 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv6 = zext i8 %4 to i32
  %5 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv7 = zext i8 %5 to i32
  %sub8 = sub nsw i32 %conv6, %conv7
  %call9 = call i32 @abs(i32 %sub8) #5
  %6 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv10 = zext i8 %6 to i32
  %cmp11 = icmp sgt i32 %call9, %conv10
  %conv12 = zext i1 %cmp11 to i32
  %mul13 = mul nsw i32 %conv12, -1
  %7 = load i8, i8* %mask, align 1, !tbaa !8
  %conv14 = sext i8 %7 to i32
  %or15 = or i32 %conv14, %mul13
  %conv16 = trunc i32 %or15 to i8
  store i8 %conv16, i8* %mask, align 1, !tbaa !8
  %8 = load i8, i8* %p2.addr, align 1, !tbaa !8
  %conv17 = zext i8 %8 to i32
  %9 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv18 = zext i8 %9 to i32
  %sub19 = sub nsw i32 %conv17, %conv18
  %call20 = call i32 @abs(i32 %sub19) #5
  %10 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv21 = zext i8 %10 to i32
  %cmp22 = icmp sgt i32 %call20, %conv21
  %conv23 = zext i1 %cmp22 to i32
  %mul24 = mul nsw i32 %conv23, -1
  %11 = load i8, i8* %mask, align 1, !tbaa !8
  %conv25 = sext i8 %11 to i32
  %or26 = or i32 %conv25, %mul24
  %conv27 = trunc i32 %or26 to i8
  store i8 %conv27, i8* %mask, align 1, !tbaa !8
  %12 = load i8, i8* %q2.addr, align 1, !tbaa !8
  %conv28 = zext i8 %12 to i32
  %13 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv29 = zext i8 %13 to i32
  %sub30 = sub nsw i32 %conv28, %conv29
  %call31 = call i32 @abs(i32 %sub30) #5
  %14 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv32 = zext i8 %14 to i32
  %cmp33 = icmp sgt i32 %call31, %conv32
  %conv34 = zext i1 %cmp33 to i32
  %mul35 = mul nsw i32 %conv34, -1
  %15 = load i8, i8* %mask, align 1, !tbaa !8
  %conv36 = sext i8 %15 to i32
  %or37 = or i32 %conv36, %mul35
  %conv38 = trunc i32 %or37 to i8
  store i8 %conv38, i8* %mask, align 1, !tbaa !8
  %16 = load i8, i8* %p3.addr, align 1, !tbaa !8
  %conv39 = zext i8 %16 to i32
  %17 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv40 = zext i8 %17 to i32
  %sub41 = sub nsw i32 %conv39, %conv40
  %call42 = call i32 @abs(i32 %sub41) #5
  %18 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv43 = zext i8 %18 to i32
  %cmp44 = icmp sgt i32 %call42, %conv43
  %conv45 = zext i1 %cmp44 to i32
  %mul46 = mul nsw i32 %conv45, -1
  %19 = load i8, i8* %mask, align 1, !tbaa !8
  %conv47 = sext i8 %19 to i32
  %or48 = or i32 %conv47, %mul46
  %conv49 = trunc i32 %or48 to i8
  store i8 %conv49, i8* %mask, align 1, !tbaa !8
  %20 = load i8, i8* %q3.addr, align 1, !tbaa !8
  %conv50 = zext i8 %20 to i32
  %21 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv51 = zext i8 %21 to i32
  %sub52 = sub nsw i32 %conv50, %conv51
  %call53 = call i32 @abs(i32 %sub52) #5
  %22 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv54 = zext i8 %22 to i32
  %cmp55 = icmp sgt i32 %call53, %conv54
  %conv56 = zext i1 %cmp55 to i32
  %mul57 = mul nsw i32 %conv56, -1
  %23 = load i8, i8* %mask, align 1, !tbaa !8
  %conv58 = sext i8 %23 to i32
  %or59 = or i32 %conv58, %mul57
  %conv60 = trunc i32 %or59 to i8
  store i8 %conv60, i8* %mask, align 1, !tbaa !8
  %24 = load i8, i8* %mask, align 1, !tbaa !8
  %conv61 = sext i8 %24 to i32
  %neg = xor i32 %conv61, -1
  %conv62 = trunc i32 %neg to i8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv62
}

; Function Attrs: inlinehint nounwind
define internal void @filter8(i8 signext %mask, i8 zeroext %thresh, i8 signext %flat, i8* %op3, i8* %op2, i8* %op1, i8* %op0, i8* %oq0, i8* %oq1, i8* %oq2, i8* %oq3) #2 {
entry:
  %mask.addr = alloca i8, align 1
  %thresh.addr = alloca i8, align 1
  %flat.addr = alloca i8, align 1
  %op3.addr = alloca i8*, align 4
  %op2.addr = alloca i8*, align 4
  %op1.addr = alloca i8*, align 4
  %op0.addr = alloca i8*, align 4
  %oq0.addr = alloca i8*, align 4
  %oq1.addr = alloca i8*, align 4
  %oq2.addr = alloca i8*, align 4
  %oq3.addr = alloca i8*, align 4
  %p3 = alloca i8, align 1
  %p2 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %q2 = alloca i8, align 1
  %q3 = alloca i8, align 1
  store i8 %mask, i8* %mask.addr, align 1, !tbaa !8
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8 %flat, i8* %flat.addr, align 1, !tbaa !8
  store i8* %op3, i8** %op3.addr, align 4, !tbaa !2
  store i8* %op2, i8** %op2.addr, align 4, !tbaa !2
  store i8* %op1, i8** %op1.addr, align 4, !tbaa !2
  store i8* %op0, i8** %op0.addr, align 4, !tbaa !2
  store i8* %oq0, i8** %oq0.addr, align 4, !tbaa !2
  store i8* %oq1, i8** %oq1.addr, align 4, !tbaa !2
  store i8* %oq2, i8** %oq2.addr, align 4, !tbaa !2
  store i8* %oq3, i8** %oq3.addr, align 4, !tbaa !2
  %0 = load i8, i8* %flat.addr, align 1, !tbaa !8
  %conv = sext i8 %0 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %conv1 = sext i8 %1 to i32
  %tobool2 = icmp ne i32 %conv1, 0
  br i1 %tobool2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p3) #4
  %2 = load i8*, i8** %op3.addr, align 4, !tbaa !2
  %3 = load i8, i8* %2, align 1, !tbaa !8
  store i8 %3, i8* %p3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #4
  %4 = load i8*, i8** %op2.addr, align 4, !tbaa !2
  %5 = load i8, i8* %4, align 1, !tbaa !8
  store i8 %5, i8* %p2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %6 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  %7 = load i8, i8* %6, align 1, !tbaa !8
  store i8 %7, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %8 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  %9 = load i8, i8* %8, align 1, !tbaa !8
  store i8 %9, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %10 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  %11 = load i8, i8* %10, align 1, !tbaa !8
  store i8 %11, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %12 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  %13 = load i8, i8* %12, align 1, !tbaa !8
  store i8 %13, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q2) #4
  %14 = load i8*, i8** %oq2.addr, align 4, !tbaa !2
  %15 = load i8, i8* %14, align 1, !tbaa !8
  store i8 %15, i8* %q2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q3) #4
  %16 = load i8*, i8** %oq3.addr, align 4, !tbaa !2
  %17 = load i8, i8* %16, align 1, !tbaa !8
  store i8 %17, i8* %q3, align 1, !tbaa !8
  %18 = load i8, i8* %p3, align 1, !tbaa !8
  %conv3 = zext i8 %18 to i32
  %19 = load i8, i8* %p3, align 1, !tbaa !8
  %conv4 = zext i8 %19 to i32
  %add = add nsw i32 %conv3, %conv4
  %20 = load i8, i8* %p3, align 1, !tbaa !8
  %conv5 = zext i8 %20 to i32
  %add6 = add nsw i32 %add, %conv5
  %21 = load i8, i8* %p2, align 1, !tbaa !8
  %conv7 = zext i8 %21 to i32
  %mul = mul nsw i32 2, %conv7
  %add8 = add nsw i32 %add6, %mul
  %22 = load i8, i8* %p1, align 1, !tbaa !8
  %conv9 = zext i8 %22 to i32
  %add10 = add nsw i32 %add8, %conv9
  %23 = load i8, i8* %p0, align 1, !tbaa !8
  %conv11 = zext i8 %23 to i32
  %add12 = add nsw i32 %add10, %conv11
  %24 = load i8, i8* %q0, align 1, !tbaa !8
  %conv13 = zext i8 %24 to i32
  %add14 = add nsw i32 %add12, %conv13
  %add15 = add nsw i32 %add14, 4
  %shr = ashr i32 %add15, 3
  %conv16 = trunc i32 %shr to i8
  %25 = load i8*, i8** %op2.addr, align 4, !tbaa !2
  store i8 %conv16, i8* %25, align 1, !tbaa !8
  %26 = load i8, i8* %p3, align 1, !tbaa !8
  %conv17 = zext i8 %26 to i32
  %27 = load i8, i8* %p3, align 1, !tbaa !8
  %conv18 = zext i8 %27 to i32
  %add19 = add nsw i32 %conv17, %conv18
  %28 = load i8, i8* %p2, align 1, !tbaa !8
  %conv20 = zext i8 %28 to i32
  %add21 = add nsw i32 %add19, %conv20
  %29 = load i8, i8* %p1, align 1, !tbaa !8
  %conv22 = zext i8 %29 to i32
  %mul23 = mul nsw i32 2, %conv22
  %add24 = add nsw i32 %add21, %mul23
  %30 = load i8, i8* %p0, align 1, !tbaa !8
  %conv25 = zext i8 %30 to i32
  %add26 = add nsw i32 %add24, %conv25
  %31 = load i8, i8* %q0, align 1, !tbaa !8
  %conv27 = zext i8 %31 to i32
  %add28 = add nsw i32 %add26, %conv27
  %32 = load i8, i8* %q1, align 1, !tbaa !8
  %conv29 = zext i8 %32 to i32
  %add30 = add nsw i32 %add28, %conv29
  %add31 = add nsw i32 %add30, 4
  %shr32 = ashr i32 %add31, 3
  %conv33 = trunc i32 %shr32 to i8
  %33 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  store i8 %conv33, i8* %33, align 1, !tbaa !8
  %34 = load i8, i8* %p3, align 1, !tbaa !8
  %conv34 = zext i8 %34 to i32
  %35 = load i8, i8* %p2, align 1, !tbaa !8
  %conv35 = zext i8 %35 to i32
  %add36 = add nsw i32 %conv34, %conv35
  %36 = load i8, i8* %p1, align 1, !tbaa !8
  %conv37 = zext i8 %36 to i32
  %add38 = add nsw i32 %add36, %conv37
  %37 = load i8, i8* %p0, align 1, !tbaa !8
  %conv39 = zext i8 %37 to i32
  %mul40 = mul nsw i32 2, %conv39
  %add41 = add nsw i32 %add38, %mul40
  %38 = load i8, i8* %q0, align 1, !tbaa !8
  %conv42 = zext i8 %38 to i32
  %add43 = add nsw i32 %add41, %conv42
  %39 = load i8, i8* %q1, align 1, !tbaa !8
  %conv44 = zext i8 %39 to i32
  %add45 = add nsw i32 %add43, %conv44
  %40 = load i8, i8* %q2, align 1, !tbaa !8
  %conv46 = zext i8 %40 to i32
  %add47 = add nsw i32 %add45, %conv46
  %add48 = add nsw i32 %add47, 4
  %shr49 = ashr i32 %add48, 3
  %conv50 = trunc i32 %shr49 to i8
  %41 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  store i8 %conv50, i8* %41, align 1, !tbaa !8
  %42 = load i8, i8* %p2, align 1, !tbaa !8
  %conv51 = zext i8 %42 to i32
  %43 = load i8, i8* %p1, align 1, !tbaa !8
  %conv52 = zext i8 %43 to i32
  %add53 = add nsw i32 %conv51, %conv52
  %44 = load i8, i8* %p0, align 1, !tbaa !8
  %conv54 = zext i8 %44 to i32
  %add55 = add nsw i32 %add53, %conv54
  %45 = load i8, i8* %q0, align 1, !tbaa !8
  %conv56 = zext i8 %45 to i32
  %mul57 = mul nsw i32 2, %conv56
  %add58 = add nsw i32 %add55, %mul57
  %46 = load i8, i8* %q1, align 1, !tbaa !8
  %conv59 = zext i8 %46 to i32
  %add60 = add nsw i32 %add58, %conv59
  %47 = load i8, i8* %q2, align 1, !tbaa !8
  %conv61 = zext i8 %47 to i32
  %add62 = add nsw i32 %add60, %conv61
  %48 = load i8, i8* %q3, align 1, !tbaa !8
  %conv63 = zext i8 %48 to i32
  %add64 = add nsw i32 %add62, %conv63
  %add65 = add nsw i32 %add64, 4
  %shr66 = ashr i32 %add65, 3
  %conv67 = trunc i32 %shr66 to i8
  %49 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  store i8 %conv67, i8* %49, align 1, !tbaa !8
  %50 = load i8, i8* %p1, align 1, !tbaa !8
  %conv68 = zext i8 %50 to i32
  %51 = load i8, i8* %p0, align 1, !tbaa !8
  %conv69 = zext i8 %51 to i32
  %add70 = add nsw i32 %conv68, %conv69
  %52 = load i8, i8* %q0, align 1, !tbaa !8
  %conv71 = zext i8 %52 to i32
  %add72 = add nsw i32 %add70, %conv71
  %53 = load i8, i8* %q1, align 1, !tbaa !8
  %conv73 = zext i8 %53 to i32
  %mul74 = mul nsw i32 2, %conv73
  %add75 = add nsw i32 %add72, %mul74
  %54 = load i8, i8* %q2, align 1, !tbaa !8
  %conv76 = zext i8 %54 to i32
  %add77 = add nsw i32 %add75, %conv76
  %55 = load i8, i8* %q3, align 1, !tbaa !8
  %conv78 = zext i8 %55 to i32
  %add79 = add nsw i32 %add77, %conv78
  %56 = load i8, i8* %q3, align 1, !tbaa !8
  %conv80 = zext i8 %56 to i32
  %add81 = add nsw i32 %add79, %conv80
  %add82 = add nsw i32 %add81, 4
  %shr83 = ashr i32 %add82, 3
  %conv84 = trunc i32 %shr83 to i8
  %57 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  store i8 %conv84, i8* %57, align 1, !tbaa !8
  %58 = load i8, i8* %p0, align 1, !tbaa !8
  %conv85 = zext i8 %58 to i32
  %59 = load i8, i8* %q0, align 1, !tbaa !8
  %conv86 = zext i8 %59 to i32
  %add87 = add nsw i32 %conv85, %conv86
  %60 = load i8, i8* %q1, align 1, !tbaa !8
  %conv88 = zext i8 %60 to i32
  %add89 = add nsw i32 %add87, %conv88
  %61 = load i8, i8* %q2, align 1, !tbaa !8
  %conv90 = zext i8 %61 to i32
  %mul91 = mul nsw i32 2, %conv90
  %add92 = add nsw i32 %add89, %mul91
  %62 = load i8, i8* %q3, align 1, !tbaa !8
  %conv93 = zext i8 %62 to i32
  %add94 = add nsw i32 %add92, %conv93
  %63 = load i8, i8* %q3, align 1, !tbaa !8
  %conv95 = zext i8 %63 to i32
  %add96 = add nsw i32 %add94, %conv95
  %64 = load i8, i8* %q3, align 1, !tbaa !8
  %conv97 = zext i8 %64 to i32
  %add98 = add nsw i32 %add96, %conv97
  %add99 = add nsw i32 %add98, 4
  %shr100 = ashr i32 %add99, 3
  %conv101 = trunc i32 %shr100 to i8
  %65 = load i8*, i8** %oq2.addr, align 4, !tbaa !2
  store i8 %conv101, i8* %65, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q3) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p3) #4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %entry
  %66 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %67 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %68 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  %69 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  %70 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  %71 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  call void @filter4(i8 signext %66, i8 zeroext %67, i8* %68, i8* %69, i8* %70, i8* %71)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_horizontal_8_dual_c(i8* %s, i32 %p, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  call void @aom_lpf_horizontal_8_c(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4)
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 4
  %6 = load i32, i32* %p.addr, align 4, !tbaa !6
  %7 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %8 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  call void @aom_lpf_horizontal_8_c(i8* %add.ptr, i32 %6, i8* %7, i8* %8, i8* %9)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_vertical_6_c(i8* %s, i32 %pitch, i8* %blimit, i8* %limit, i8* %thresh) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p2 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %q2 = alloca i8, align 1
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #4
  %4 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 -3
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %5, i8* %p2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %6 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %6, i32 -2
  %7 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  store i8 %7, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %8 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %8, i32 -1
  %9 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  store i8 %9, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %10 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %10, i32 0
  %11 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  store i8 %11, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %12 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %12, i32 1
  %13 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  store i8 %13, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q2) #4
  %14 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %14, i32 2
  %15 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  store i8 %15, i8* %q2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %16 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %17 = load i8, i8* %16, align 1, !tbaa !8
  %18 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %19 = load i8, i8* %18, align 1, !tbaa !8
  %20 = load i8, i8* %p2, align 1, !tbaa !8
  %21 = load i8, i8* %p1, align 1, !tbaa !8
  %22 = load i8, i8* %p0, align 1, !tbaa !8
  %23 = load i8, i8* %q0, align 1, !tbaa !8
  %24 = load i8, i8* %q1, align 1, !tbaa !8
  %25 = load i8, i8* %q2, align 1, !tbaa !8
  %call = call signext i8 @filter_mask3_chroma(i8 zeroext %17, i8 zeroext %19, i8 zeroext %20, i8 zeroext %21, i8 zeroext %22, i8 zeroext %23, i8 zeroext %24, i8 zeroext %25)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %26 = load i8, i8* %p2, align 1, !tbaa !8
  %27 = load i8, i8* %p1, align 1, !tbaa !8
  %28 = load i8, i8* %p0, align 1, !tbaa !8
  %29 = load i8, i8* %q0, align 1, !tbaa !8
  %30 = load i8, i8* %q1, align 1, !tbaa !8
  %31 = load i8, i8* %q2, align 1, !tbaa !8
  %call6 = call signext i8 @flat_mask3_chroma(i8 zeroext 1, i8 zeroext %26, i8 zeroext %27, i8 zeroext %28, i8 zeroext %29, i8 zeroext %30, i8 zeroext %31)
  store i8 %call6, i8* %flat, align 1, !tbaa !8
  %32 = load i8, i8* %mask, align 1, !tbaa !8
  %33 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %34 = load i8, i8* %33, align 1, !tbaa !8
  %35 = load i8, i8* %flat, align 1, !tbaa !8
  %36 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %36, i32 -3
  %37 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr7 = getelementptr inbounds i8, i8* %37, i32 -2
  %38 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds i8, i8* %38, i32 -1
  %39 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %40 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i8, i8* %40, i32 1
  %41 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i8, i8* %41, i32 2
  call void @filter6(i8 signext %32, i8 zeroext %34, i8 signext %35, i8* %add.ptr, i8* %add.ptr7, i8* %add.ptr8, i8* %39, i8* %add.ptr9, i8* %add.ptr10)
  %42 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %43 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i8, i8* %43, i32 %42
  store i8* %add.ptr11, i8** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %45 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_vertical_6_dual_c(i8* %s, i32 %pitch, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  call void @aom_lpf_vertical_6_c(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4)
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 4, %6
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  %7 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  call void @aom_lpf_vertical_6_c(i8* %add.ptr, i32 %7, i8* %8, i8* %9, i8* %10)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_vertical_8_c(i8* %s, i32 %pitch, i8* %blimit, i8* %limit, i8* %thresh) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p3 = alloca i8, align 1
  %p2 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %q2 = alloca i8, align 1
  %q3 = alloca i8, align 1
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p3) #4
  %4 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 -4
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %5, i8* %p3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #4
  %6 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %6, i32 -3
  %7 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  store i8 %7, i8* %p2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %8 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %8, i32 -2
  %9 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  store i8 %9, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %10 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %10, i32 -1
  %11 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  store i8 %11, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %12 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %12, i32 0
  %13 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  store i8 %13, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %14 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %14, i32 1
  %15 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  store i8 %15, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q2) #4
  %16 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %16, i32 2
  %17 = load i8, i8* %arrayidx6, align 1, !tbaa !8
  store i8 %17, i8* %q2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q3) #4
  %18 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %18, i32 3
  %19 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  store i8 %19, i8* %q3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %20 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %21 = load i8, i8* %20, align 1, !tbaa !8
  %22 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %23 = load i8, i8* %22, align 1, !tbaa !8
  %24 = load i8, i8* %p3, align 1, !tbaa !8
  %25 = load i8, i8* %p2, align 1, !tbaa !8
  %26 = load i8, i8* %p1, align 1, !tbaa !8
  %27 = load i8, i8* %p0, align 1, !tbaa !8
  %28 = load i8, i8* %q0, align 1, !tbaa !8
  %29 = load i8, i8* %q1, align 1, !tbaa !8
  %30 = load i8, i8* %q2, align 1, !tbaa !8
  %31 = load i8, i8* %q3, align 1, !tbaa !8
  %call = call signext i8 @filter_mask(i8 zeroext %21, i8 zeroext %23, i8 zeroext %24, i8 zeroext %25, i8 zeroext %26, i8 zeroext %27, i8 zeroext %28, i8 zeroext %29, i8 zeroext %30, i8 zeroext %31)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %32 = load i8, i8* %p3, align 1, !tbaa !8
  %33 = load i8, i8* %p2, align 1, !tbaa !8
  %34 = load i8, i8* %p1, align 1, !tbaa !8
  %35 = load i8, i8* %p0, align 1, !tbaa !8
  %36 = load i8, i8* %q0, align 1, !tbaa !8
  %37 = load i8, i8* %q1, align 1, !tbaa !8
  %38 = load i8, i8* %q2, align 1, !tbaa !8
  %39 = load i8, i8* %q3, align 1, !tbaa !8
  %call8 = call signext i8 @flat_mask4(i8 zeroext 1, i8 zeroext %32, i8 zeroext %33, i8 zeroext %34, i8 zeroext %35, i8 zeroext %36, i8 zeroext %37, i8 zeroext %38, i8 zeroext %39)
  store i8 %call8, i8* %flat, align 1, !tbaa !8
  %40 = load i8, i8* %mask, align 1, !tbaa !8
  %41 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %42 = load i8, i8* %41, align 1, !tbaa !8
  %43 = load i8, i8* %flat, align 1, !tbaa !8
  %44 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %44, i32 -4
  %45 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i8, i8* %45, i32 -3
  %46 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i8, i8* %46, i32 -2
  %47 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i8, i8* %47, i32 -1
  %48 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %49 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr12 = getelementptr inbounds i8, i8* %49, i32 1
  %50 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr13 = getelementptr inbounds i8, i8* %50, i32 2
  %51 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr14 = getelementptr inbounds i8, i8* %51, i32 3
  call void @filter8(i8 signext %40, i8 zeroext %42, i8 signext %43, i8* %add.ptr, i8* %add.ptr9, i8* %add.ptr10, i8* %add.ptr11, i8* %48, i8* %add.ptr12, i8* %add.ptr13, i8* %add.ptr14)
  %52 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %53 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr15 = getelementptr inbounds i8, i8* %53, i32 %52
  store i8* %add.ptr15, i8** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q3) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p3) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %55 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  %56 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_vertical_8_dual_c(i8* %s, i32 %pitch, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  call void @aom_lpf_vertical_8_c(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4)
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 4, %6
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  %7 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  call void @aom_lpf_vertical_8_c(i8* %add.ptr, i32 %7, i8* %8, i8* %9, i8* %10)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_horizontal_14_c(i8* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  call void @mb_lpf_horizontal_edge_w(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 1)
  ret void
}

; Function Attrs: nounwind
define internal void @mb_lpf_horizontal_edge_w(i8* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh, i32 %count) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %count.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %step = alloca i32, align 4
  %p6 = alloca i8, align 1
  %p5 = alloca i8, align 1
  %p4 = alloca i8, align 1
  %p3 = alloca i8, align 1
  %p2 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %q2 = alloca i8, align 1
  %q3 = alloca i8, align 1
  %q4 = alloca i8, align 1
  %q5 = alloca i8, align 1
  %q6 = alloca i8, align 1
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  %flat2 = alloca i8, align 1
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %count, i32* %count.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %step, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %step, align 4, !tbaa !6
  %4 = load i32, i32* %count.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %3, %4
  %cmp = icmp slt i32 %2, %mul
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p6) #4
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 -7, %6
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %mul1
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %7, i8* %p6, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p5) #4
  %8 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %9 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 -6, %9
  %arrayidx3 = getelementptr inbounds i8, i8* %8, i32 %mul2
  %10 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  store i8 %10, i8* %p5, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p4) #4
  %11 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %12 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 -5, %12
  %arrayidx5 = getelementptr inbounds i8, i8* %11, i32 %mul4
  %13 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  store i8 %13, i8* %p4, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p3) #4
  %14 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %15 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 -4, %15
  %arrayidx7 = getelementptr inbounds i8, i8* %14, i32 %mul6
  %16 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  store i8 %16, i8* %p3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #4
  %17 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %18 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul8 = mul nsw i32 -3, %18
  %arrayidx9 = getelementptr inbounds i8, i8* %17, i32 %mul8
  %19 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  store i8 %19, i8* %p2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %20 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %21 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 -2, %21
  %arrayidx11 = getelementptr inbounds i8, i8* %20, i32 %mul10
  %22 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  store i8 %22, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %23 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %24 = load i32, i32* %p.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %24
  %arrayidx12 = getelementptr inbounds i8, i8* %23, i32 %sub
  %25 = load i8, i8* %arrayidx12, align 1, !tbaa !8
  store i8 %25, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %26 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %27 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 0, %27
  %arrayidx14 = getelementptr inbounds i8, i8* %26, i32 %mul13
  %28 = load i8, i8* %arrayidx14, align 1, !tbaa !8
  store i8 %28, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %29 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %30 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 1, %30
  %arrayidx16 = getelementptr inbounds i8, i8* %29, i32 %mul15
  %31 = load i8, i8* %arrayidx16, align 1, !tbaa !8
  store i8 %31, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q2) #4
  %32 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %33 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 2, %33
  %arrayidx18 = getelementptr inbounds i8, i8* %32, i32 %mul17
  %34 = load i8, i8* %arrayidx18, align 1, !tbaa !8
  store i8 %34, i8* %q2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q3) #4
  %35 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %36 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul19 = mul nsw i32 3, %36
  %arrayidx20 = getelementptr inbounds i8, i8* %35, i32 %mul19
  %37 = load i8, i8* %arrayidx20, align 1, !tbaa !8
  store i8 %37, i8* %q3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q4) #4
  %38 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %39 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul21 = mul nsw i32 4, %39
  %arrayidx22 = getelementptr inbounds i8, i8* %38, i32 %mul21
  %40 = load i8, i8* %arrayidx22, align 1, !tbaa !8
  store i8 %40, i8* %q4, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q5) #4
  %41 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %42 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 5, %42
  %arrayidx24 = getelementptr inbounds i8, i8* %41, i32 %mul23
  %43 = load i8, i8* %arrayidx24, align 1, !tbaa !8
  store i8 %43, i8* %q5, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q6) #4
  %44 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %45 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 6, %45
  %arrayidx26 = getelementptr inbounds i8, i8* %44, i32 %mul25
  %46 = load i8, i8* %arrayidx26, align 1, !tbaa !8
  store i8 %46, i8* %q6, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %47 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !8
  %49 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !8
  %51 = load i8, i8* %p3, align 1, !tbaa !8
  %52 = load i8, i8* %p2, align 1, !tbaa !8
  %53 = load i8, i8* %p1, align 1, !tbaa !8
  %54 = load i8, i8* %p0, align 1, !tbaa !8
  %55 = load i8, i8* %q0, align 1, !tbaa !8
  %56 = load i8, i8* %q1, align 1, !tbaa !8
  %57 = load i8, i8* %q2, align 1, !tbaa !8
  %58 = load i8, i8* %q3, align 1, !tbaa !8
  %call = call signext i8 @filter_mask(i8 zeroext %48, i8 zeroext %50, i8 zeroext %51, i8 zeroext %52, i8 zeroext %53, i8 zeroext %54, i8 zeroext %55, i8 zeroext %56, i8 zeroext %57, i8 zeroext %58)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %59 = load i8, i8* %p3, align 1, !tbaa !8
  %60 = load i8, i8* %p2, align 1, !tbaa !8
  %61 = load i8, i8* %p1, align 1, !tbaa !8
  %62 = load i8, i8* %p0, align 1, !tbaa !8
  %63 = load i8, i8* %q0, align 1, !tbaa !8
  %64 = load i8, i8* %q1, align 1, !tbaa !8
  %65 = load i8, i8* %q2, align 1, !tbaa !8
  %66 = load i8, i8* %q3, align 1, !tbaa !8
  %call27 = call signext i8 @flat_mask4(i8 zeroext 1, i8 zeroext %59, i8 zeroext %60, i8 zeroext %61, i8 zeroext %62, i8 zeroext %63, i8 zeroext %64, i8 zeroext %65, i8 zeroext %66)
  store i8 %call27, i8* %flat, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat2) #4
  %67 = load i8, i8* %p6, align 1, !tbaa !8
  %68 = load i8, i8* %p5, align 1, !tbaa !8
  %69 = load i8, i8* %p4, align 1, !tbaa !8
  %70 = load i8, i8* %p0, align 1, !tbaa !8
  %71 = load i8, i8* %q0, align 1, !tbaa !8
  %72 = load i8, i8* %q4, align 1, !tbaa !8
  %73 = load i8, i8* %q5, align 1, !tbaa !8
  %74 = load i8, i8* %q6, align 1, !tbaa !8
  %call28 = call signext i8 @flat_mask4(i8 zeroext 1, i8 zeroext %67, i8 zeroext %68, i8 zeroext %69, i8 zeroext %70, i8 zeroext %71, i8 zeroext %72, i8 zeroext %73, i8 zeroext %74)
  store i8 %call28, i8* %flat2, align 1, !tbaa !8
  %75 = load i8, i8* %mask, align 1, !tbaa !8
  %76 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %77 = load i8, i8* %76, align 1, !tbaa !8
  %78 = load i8, i8* %flat, align 1, !tbaa !8
  %79 = load i8, i8* %flat2, align 1, !tbaa !8
  %80 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %81 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 7, %81
  %idx.neg = sub i32 0, %mul29
  %add.ptr = getelementptr inbounds i8, i8* %80, i32 %idx.neg
  %82 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %83 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul30 = mul nsw i32 6, %83
  %idx.neg31 = sub i32 0, %mul30
  %add.ptr32 = getelementptr inbounds i8, i8* %82, i32 %idx.neg31
  %84 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %85 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul33 = mul nsw i32 5, %85
  %idx.neg34 = sub i32 0, %mul33
  %add.ptr35 = getelementptr inbounds i8, i8* %84, i32 %idx.neg34
  %86 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %87 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul36 = mul nsw i32 4, %87
  %idx.neg37 = sub i32 0, %mul36
  %add.ptr38 = getelementptr inbounds i8, i8* %86, i32 %idx.neg37
  %88 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %89 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul39 = mul nsw i32 3, %89
  %idx.neg40 = sub i32 0, %mul39
  %add.ptr41 = getelementptr inbounds i8, i8* %88, i32 %idx.neg40
  %90 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %91 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul42 = mul nsw i32 2, %91
  %idx.neg43 = sub i32 0, %mul42
  %add.ptr44 = getelementptr inbounds i8, i8* %90, i32 %idx.neg43
  %92 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %93 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul45 = mul nsw i32 1, %93
  %idx.neg46 = sub i32 0, %mul45
  %add.ptr47 = getelementptr inbounds i8, i8* %92, i32 %idx.neg46
  %94 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %95 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %96 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul48 = mul nsw i32 1, %96
  %add.ptr49 = getelementptr inbounds i8, i8* %95, i32 %mul48
  %97 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %98 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul50 = mul nsw i32 2, %98
  %add.ptr51 = getelementptr inbounds i8, i8* %97, i32 %mul50
  %99 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %100 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul52 = mul nsw i32 3, %100
  %add.ptr53 = getelementptr inbounds i8, i8* %99, i32 %mul52
  %101 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %102 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul54 = mul nsw i32 4, %102
  %add.ptr55 = getelementptr inbounds i8, i8* %101, i32 %mul54
  %103 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %104 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul56 = mul nsw i32 5, %104
  %add.ptr57 = getelementptr inbounds i8, i8* %103, i32 %mul56
  %105 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %106 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul58 = mul nsw i32 6, %106
  %add.ptr59 = getelementptr inbounds i8, i8* %105, i32 %mul58
  call void @filter14(i8 signext %75, i8 zeroext %77, i8 signext %78, i8 signext %79, i8* %add.ptr, i8* %add.ptr32, i8* %add.ptr35, i8* %add.ptr38, i8* %add.ptr41, i8* %add.ptr44, i8* %add.ptr47, i8* %94, i8* %add.ptr49, i8* %add.ptr51, i8* %add.ptr53, i8* %add.ptr55, i8* %add.ptr57, i8* %add.ptr59)
  %107 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %107, i32 1
  store i8* %incdec.ptr, i8** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q6) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q5) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q4) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q3) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p3) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p4) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p5) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p6) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %108 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %108, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %109 = bitcast i32* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #4
  %110 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_horizontal_14_dual_c(i8* %s, i32 %p, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  call void @mb_lpf_horizontal_edge_w(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 1)
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 4
  %6 = load i32, i32* %p.addr, align 4, !tbaa !6
  %7 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %8 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  call void @mb_lpf_horizontal_edge_w(i8* %add.ptr, i32 %6, i8* %7, i8* %8, i8* %9, i32 1)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_vertical_14_c(i8* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  call void @mb_lpf_vertical_edge_w(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 4)
  ret void
}

; Function Attrs: nounwind
define internal void @mb_lpf_vertical_edge_w(i8* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh, i32 %count) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %count.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %p6 = alloca i8, align 1
  %p5 = alloca i8, align 1
  %p4 = alloca i8, align 1
  %p3 = alloca i8, align 1
  %p2 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %q2 = alloca i8, align 1
  %q3 = alloca i8, align 1
  %q4 = alloca i8, align 1
  %q5 = alloca i8, align 1
  %q6 = alloca i8, align 1
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  %flat2 = alloca i8, align 1
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %count, i32* %count.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p6) #4
  %3 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 -7
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %4, i8* %p6, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p5) #4
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %5, i32 -6
  %6 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  store i8 %6, i8* %p5, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p4) #4
  %7 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %7, i32 -5
  %8 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  store i8 %8, i8* %p4, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p3) #4
  %9 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %9, i32 -4
  %10 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  store i8 %10, i8* %p3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #4
  %11 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %11, i32 -3
  %12 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  store i8 %12, i8* %p2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %13 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %13, i32 -2
  %14 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  store i8 %14, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %15 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %15, i32 -1
  %16 = load i8, i8* %arrayidx6, align 1, !tbaa !8
  store i8 %16, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %17 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %17, i32 0
  %18 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  store i8 %18, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %19 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %19, i32 1
  %20 = load i8, i8* %arrayidx8, align 1, !tbaa !8
  store i8 %20, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q2) #4
  %21 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %21, i32 2
  %22 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  store i8 %22, i8* %q2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q3) #4
  %23 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %23, i32 3
  %24 = load i8, i8* %arrayidx10, align 1, !tbaa !8
  store i8 %24, i8* %q3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q4) #4
  %25 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %25, i32 4
  %26 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  store i8 %26, i8* %q4, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q5) #4
  %27 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %27, i32 5
  %28 = load i8, i8* %arrayidx12, align 1, !tbaa !8
  store i8 %28, i8* %q5, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q6) #4
  %29 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i8, i8* %29, i32 6
  %30 = load i8, i8* %arrayidx13, align 1, !tbaa !8
  store i8 %30, i8* %q6, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %31 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %32 = load i8, i8* %31, align 1, !tbaa !8
  %33 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %34 = load i8, i8* %33, align 1, !tbaa !8
  %35 = load i8, i8* %p3, align 1, !tbaa !8
  %36 = load i8, i8* %p2, align 1, !tbaa !8
  %37 = load i8, i8* %p1, align 1, !tbaa !8
  %38 = load i8, i8* %p0, align 1, !tbaa !8
  %39 = load i8, i8* %q0, align 1, !tbaa !8
  %40 = load i8, i8* %q1, align 1, !tbaa !8
  %41 = load i8, i8* %q2, align 1, !tbaa !8
  %42 = load i8, i8* %q3, align 1, !tbaa !8
  %call = call signext i8 @filter_mask(i8 zeroext %32, i8 zeroext %34, i8 zeroext %35, i8 zeroext %36, i8 zeroext %37, i8 zeroext %38, i8 zeroext %39, i8 zeroext %40, i8 zeroext %41, i8 zeroext %42)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %43 = load i8, i8* %p3, align 1, !tbaa !8
  %44 = load i8, i8* %p2, align 1, !tbaa !8
  %45 = load i8, i8* %p1, align 1, !tbaa !8
  %46 = load i8, i8* %p0, align 1, !tbaa !8
  %47 = load i8, i8* %q0, align 1, !tbaa !8
  %48 = load i8, i8* %q1, align 1, !tbaa !8
  %49 = load i8, i8* %q2, align 1, !tbaa !8
  %50 = load i8, i8* %q3, align 1, !tbaa !8
  %call14 = call signext i8 @flat_mask4(i8 zeroext 1, i8 zeroext %43, i8 zeroext %44, i8 zeroext %45, i8 zeroext %46, i8 zeroext %47, i8 zeroext %48, i8 zeroext %49, i8 zeroext %50)
  store i8 %call14, i8* %flat, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat2) #4
  %51 = load i8, i8* %p6, align 1, !tbaa !8
  %52 = load i8, i8* %p5, align 1, !tbaa !8
  %53 = load i8, i8* %p4, align 1, !tbaa !8
  %54 = load i8, i8* %p0, align 1, !tbaa !8
  %55 = load i8, i8* %q0, align 1, !tbaa !8
  %56 = load i8, i8* %q4, align 1, !tbaa !8
  %57 = load i8, i8* %q5, align 1, !tbaa !8
  %58 = load i8, i8* %q6, align 1, !tbaa !8
  %call15 = call signext i8 @flat_mask4(i8 zeroext 1, i8 zeroext %51, i8 zeroext %52, i8 zeroext %53, i8 zeroext %54, i8 zeroext %55, i8 zeroext %56, i8 zeroext %57, i8 zeroext %58)
  store i8 %call15, i8* %flat2, align 1, !tbaa !8
  %59 = load i8, i8* %mask, align 1, !tbaa !8
  %60 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %61 = load i8, i8* %60, align 1, !tbaa !8
  %62 = load i8, i8* %flat, align 1, !tbaa !8
  %63 = load i8, i8* %flat2, align 1, !tbaa !8
  %64 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %64, i32 -7
  %65 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr16 = getelementptr inbounds i8, i8* %65, i32 -6
  %66 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds i8, i8* %66, i32 -5
  %67 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr18 = getelementptr inbounds i8, i8* %67, i32 -4
  %68 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr19 = getelementptr inbounds i8, i8* %68, i32 -3
  %69 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr20 = getelementptr inbounds i8, i8* %69, i32 -2
  %70 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr21 = getelementptr inbounds i8, i8* %70, i32 -1
  %71 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %72 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i8, i8* %72, i32 1
  %73 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr23 = getelementptr inbounds i8, i8* %73, i32 2
  %74 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr24 = getelementptr inbounds i8, i8* %74, i32 3
  %75 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr25 = getelementptr inbounds i8, i8* %75, i32 4
  %76 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr26 = getelementptr inbounds i8, i8* %76, i32 5
  %77 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr27 = getelementptr inbounds i8, i8* %77, i32 6
  call void @filter14(i8 signext %59, i8 zeroext %61, i8 signext %62, i8 signext %63, i8* %add.ptr, i8* %add.ptr16, i8* %add.ptr17, i8* %add.ptr18, i8* %add.ptr19, i8* %add.ptr20, i8* %add.ptr21, i8* %71, i8* %add.ptr22, i8* %add.ptr23, i8* %add.ptr24, i8* %add.ptr25, i8* %add.ptr26, i8* %add.ptr27)
  %78 = load i32, i32* %p.addr, align 4, !tbaa !6
  %79 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds i8, i8* %79, i32 %78
  store i8* %add.ptr28, i8** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q6) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q5) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q4) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q3) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p3) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p4) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p5) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p6) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %80 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %80, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %81 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_lpf_vertical_14_dual_c(i8* %s, i32 %pitch, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  call void @mb_lpf_vertical_edge_w(i8* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 4)
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 4, %6
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  %7 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  call void @mb_lpf_vertical_edge_w(i8* %add.ptr, i32 %7, i8* %8, i8* %9, i8* %10, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_horizontal_4_c(i16* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %mask = alloca i8, align 1
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  %5 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul = mul nsw i32 -2, %6
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 %mul
  %7 = load i16, i16* %arrayidx, align 2, !tbaa !9
  store i16 %7, i16* %p1, align 2, !tbaa !9
  %8 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %8) #4
  %9 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %10 = load i32, i32* %p.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %10
  %arrayidx1 = getelementptr inbounds i16, i16* %9, i32 %sub
  %11 = load i16, i16* %arrayidx1, align 2, !tbaa !9
  store i16 %11, i16* %p0, align 2, !tbaa !9
  %12 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %12) #4
  %13 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %14 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 0, %14
  %arrayidx3 = getelementptr inbounds i16, i16* %13, i32 %mul2
  %15 = load i16, i16* %arrayidx3, align 2, !tbaa !9
  store i16 %15, i16* %q0, align 2, !tbaa !9
  %16 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #4
  %17 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %18 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 1, %18
  %arrayidx5 = getelementptr inbounds i16, i16* %17, i32 %mul4
  %19 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  store i16 %19, i16* %q1, align 2, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %20 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %21 = load i8, i8* %20, align 1, !tbaa !8
  %22 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %23 = load i8, i8* %22, align 1, !tbaa !8
  %24 = load i16, i16* %p1, align 2, !tbaa !9
  %25 = load i16, i16* %p0, align 2, !tbaa !9
  %26 = load i16, i16* %q0, align 2, !tbaa !9
  %27 = load i16, i16* %q1, align 2, !tbaa !9
  %28 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call signext i8 @highbd_filter_mask2(i8 zeroext %21, i8 zeroext %23, i16 zeroext %24, i16 zeroext %25, i16 zeroext %26, i16 zeroext %27, i32 %28)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  %29 = load i8, i8* %mask, align 1, !tbaa !8
  %30 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !8
  %32 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %33 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 2, %33
  %idx.neg = sub i32 0, %mul6
  %add.ptr = getelementptr inbounds i16, i16* %32, i32 %idx.neg
  %34 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %35 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 1, %35
  %idx.neg8 = sub i32 0, %mul7
  %add.ptr9 = getelementptr inbounds i16, i16* %34, i32 %idx.neg8
  %36 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %37 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %38 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 1, %38
  %add.ptr11 = getelementptr inbounds i16, i16* %37, i32 %mul10
  %39 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter4(i8 signext %29, i8 zeroext %31, i16* %add.ptr, i16* %add.ptr9, i16* %36, i16* %add.ptr11, i32 %39)
  %40 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %40, i32 1
  store i16* %incdec.ptr, i16** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  %41 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %41) #4
  %42 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %42) #4
  %43 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %43) #4
  %44 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %44) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %46 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @highbd_filter_mask2(i8 zeroext %limit, i8 zeroext %blimit, i16 zeroext %p1, i16 zeroext %p0, i16 zeroext %q0, i16 zeroext %q1, i32 %bd) #2 {
entry:
  %limit.addr = alloca i8, align 1
  %blimit.addr = alloca i8, align 1
  %p1.addr = alloca i16, align 2
  %p0.addr = alloca i16, align 2
  %q0.addr = alloca i16, align 2
  %q1.addr = alloca i16, align 2
  %bd.addr = alloca i32, align 4
  %mask = alloca i8, align 1
  %limit16 = alloca i16, align 2
  %blimit16 = alloca i16, align 2
  store i8 %limit, i8* %limit.addr, align 1, !tbaa !8
  store i8 %blimit, i8* %blimit.addr, align 1, !tbaa !8
  store i16 %p1, i16* %p1.addr, align 2, !tbaa !9
  store i16 %p0, i16* %p0.addr, align 2, !tbaa !9
  store i16 %q0, i16* %q0.addr, align 2, !tbaa !9
  store i16 %q1, i16* %q1.addr, align 2, !tbaa !9
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = bitcast i16* %limit16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #4
  %1 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv = zext i8 %1 to i16
  %conv1 = zext i16 %conv to i32
  %2 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 8
  %shl = shl i32 %conv1, %sub
  %conv2 = trunc i32 %shl to i16
  store i16 %conv2, i16* %limit16, align 2, !tbaa !9
  %3 = bitcast i16* %blimit16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %3) #4
  %4 = load i8, i8* %blimit.addr, align 1, !tbaa !8
  %conv3 = zext i8 %4 to i16
  %conv4 = zext i16 %conv3 to i32
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub5 = sub nsw i32 %5, 8
  %shl6 = shl i32 %conv4, %sub5
  %conv7 = trunc i32 %shl6 to i16
  store i16 %conv7, i16* %blimit16, align 2, !tbaa !9
  %6 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv8 = zext i16 %6 to i32
  %7 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv9 = zext i16 %7 to i32
  %sub10 = sub nsw i32 %conv8, %conv9
  %call = call i32 @abs(i32 %sub10) #5
  %8 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv11 = sext i16 %8 to i32
  %cmp = icmp sgt i32 %call, %conv11
  %conv12 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv12, -1
  %9 = load i8, i8* %mask, align 1, !tbaa !8
  %conv13 = sext i8 %9 to i32
  %or = or i32 %conv13, %mul
  %conv14 = trunc i32 %or to i8
  store i8 %conv14, i8* %mask, align 1, !tbaa !8
  %10 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv15 = zext i16 %10 to i32
  %11 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv16 = zext i16 %11 to i32
  %sub17 = sub nsw i32 %conv15, %conv16
  %call18 = call i32 @abs(i32 %sub17) #5
  %12 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv19 = sext i16 %12 to i32
  %cmp20 = icmp sgt i32 %call18, %conv19
  %conv21 = zext i1 %cmp20 to i32
  %mul22 = mul nsw i32 %conv21, -1
  %13 = load i8, i8* %mask, align 1, !tbaa !8
  %conv23 = sext i8 %13 to i32
  %or24 = or i32 %conv23, %mul22
  %conv25 = trunc i32 %or24 to i8
  store i8 %conv25, i8* %mask, align 1, !tbaa !8
  %14 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv26 = zext i16 %14 to i32
  %15 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv27 = zext i16 %15 to i32
  %sub28 = sub nsw i32 %conv26, %conv27
  %call29 = call i32 @abs(i32 %sub28) #5
  %mul30 = mul nsw i32 %call29, 2
  %16 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv31 = zext i16 %16 to i32
  %17 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv32 = zext i16 %17 to i32
  %sub33 = sub nsw i32 %conv31, %conv32
  %call34 = call i32 @abs(i32 %sub33) #5
  %div = sdiv i32 %call34, 2
  %add = add nsw i32 %mul30, %div
  %18 = load i16, i16* %blimit16, align 2, !tbaa !9
  %conv35 = sext i16 %18 to i32
  %cmp36 = icmp sgt i32 %add, %conv35
  %conv37 = zext i1 %cmp36 to i32
  %mul38 = mul nsw i32 %conv37, -1
  %19 = load i8, i8* %mask, align 1, !tbaa !8
  %conv39 = sext i8 %19 to i32
  %or40 = or i32 %conv39, %mul38
  %conv41 = trunc i32 %or40 to i8
  store i8 %conv41, i8* %mask, align 1, !tbaa !8
  %20 = load i8, i8* %mask, align 1, !tbaa !8
  %conv42 = sext i8 %20 to i32
  %neg = xor i32 %conv42, -1
  %conv43 = trunc i32 %neg to i8
  %21 = bitcast i16* %blimit16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %21) #4
  %22 = bitcast i16* %limit16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %22) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv43
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_filter4(i8 signext %mask, i8 zeroext %thresh, i16* %op1, i16* %op0, i16* %oq0, i16* %oq1, i32 %bd) #2 {
entry:
  %mask.addr = alloca i8, align 1
  %thresh.addr = alloca i8, align 1
  %op1.addr = alloca i16*, align 4
  %op0.addr = alloca i16*, align 4
  %oq0.addr = alloca i16*, align 4
  %oq1.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %filter1 = alloca i16, align 2
  %filter2 = alloca i16, align 2
  %shift = alloca i32, align 4
  %ps1 = alloca i16, align 2
  %ps0 = alloca i16, align 2
  %qs0 = alloca i16, align 2
  %qs1 = alloca i16, align 2
  %hev = alloca i16, align 2
  %filter = alloca i16, align 2
  store i8 %mask, i8* %mask.addr, align 1, !tbaa !8
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i16* %op1, i16** %op1.addr, align 4, !tbaa !2
  store i16* %op0, i16** %op0.addr, align 4, !tbaa !2
  store i16* %oq0, i16** %oq0.addr, align 4, !tbaa !2
  store i16* %oq1, i16** %oq1.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i16* %filter1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #4
  %1 = bitcast i16* %filter2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %1) #4
  %2 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %3, 8
  store i32 %sub, i32* %shift, align 4, !tbaa !6
  %4 = bitcast i16* %ps1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  %5 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  %6 = load i16, i16* %5, align 2, !tbaa !9
  %conv = sext i16 %6 to i32
  %7 = load i32, i32* %shift, align 4, !tbaa !6
  %shl = shl i32 128, %7
  %sub1 = sub nsw i32 %conv, %shl
  %conv2 = trunc i32 %sub1 to i16
  store i16 %conv2, i16* %ps1, align 2, !tbaa !9
  %8 = bitcast i16* %ps0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %8) #4
  %9 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  %10 = load i16, i16* %9, align 2, !tbaa !9
  %conv3 = sext i16 %10 to i32
  %11 = load i32, i32* %shift, align 4, !tbaa !6
  %shl4 = shl i32 128, %11
  %sub5 = sub nsw i32 %conv3, %shl4
  %conv6 = trunc i32 %sub5 to i16
  store i16 %conv6, i16* %ps0, align 2, !tbaa !9
  %12 = bitcast i16* %qs0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %12) #4
  %13 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  %14 = load i16, i16* %13, align 2, !tbaa !9
  %conv7 = sext i16 %14 to i32
  %15 = load i32, i32* %shift, align 4, !tbaa !6
  %shl8 = shl i32 128, %15
  %sub9 = sub nsw i32 %conv7, %shl8
  %conv10 = trunc i32 %sub9 to i16
  store i16 %conv10, i16* %qs0, align 2, !tbaa !9
  %16 = bitcast i16* %qs1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #4
  %17 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  %18 = load i16, i16* %17, align 2, !tbaa !9
  %conv11 = sext i16 %18 to i32
  %19 = load i32, i32* %shift, align 4, !tbaa !6
  %shl12 = shl i32 128, %19
  %sub13 = sub nsw i32 %conv11, %shl12
  %conv14 = trunc i32 %sub13 to i16
  store i16 %conv14, i16* %qs1, align 2, !tbaa !9
  %20 = bitcast i16* %hev to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %20) #4
  %21 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %22 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  %23 = load i16, i16* %22, align 2, !tbaa !9
  %24 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  %25 = load i16, i16* %24, align 2, !tbaa !9
  %26 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  %27 = load i16, i16* %26, align 2, !tbaa !9
  %28 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  %29 = load i16, i16* %28, align 2, !tbaa !9
  %30 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call signext i16 @highbd_hev_mask(i8 zeroext %21, i16 zeroext %23, i16 zeroext %25, i16 zeroext %27, i16 zeroext %29, i32 %30)
  store i16 %call, i16* %hev, align 2, !tbaa !9
  %31 = bitcast i16* %filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %31) #4
  %32 = load i16, i16* %ps1, align 2, !tbaa !9
  %conv15 = sext i16 %32 to i32
  %33 = load i16, i16* %qs1, align 2, !tbaa !9
  %conv16 = sext i16 %33 to i32
  %sub17 = sub nsw i32 %conv15, %conv16
  %34 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call18 = call signext i16 @signed_char_clamp_high(i32 %sub17, i32 %34)
  %conv19 = sext i16 %call18 to i32
  %35 = load i16, i16* %hev, align 2, !tbaa !9
  %conv20 = sext i16 %35 to i32
  %and = and i32 %conv19, %conv20
  %conv21 = trunc i32 %and to i16
  store i16 %conv21, i16* %filter, align 2, !tbaa !9
  %36 = load i16, i16* %filter, align 2, !tbaa !9
  %conv22 = sext i16 %36 to i32
  %37 = load i16, i16* %qs0, align 2, !tbaa !9
  %conv23 = sext i16 %37 to i32
  %38 = load i16, i16* %ps0, align 2, !tbaa !9
  %conv24 = sext i16 %38 to i32
  %sub25 = sub nsw i32 %conv23, %conv24
  %mul = mul nsw i32 3, %sub25
  %add = add nsw i32 %conv22, %mul
  %39 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call26 = call signext i16 @signed_char_clamp_high(i32 %add, i32 %39)
  %conv27 = sext i16 %call26 to i32
  %40 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %conv28 = sext i8 %40 to i32
  %and29 = and i32 %conv27, %conv28
  %conv30 = trunc i32 %and29 to i16
  store i16 %conv30, i16* %filter, align 2, !tbaa !9
  %41 = load i16, i16* %filter, align 2, !tbaa !9
  %conv31 = sext i16 %41 to i32
  %add32 = add nsw i32 %conv31, 4
  %42 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call33 = call signext i16 @signed_char_clamp_high(i32 %add32, i32 %42)
  %conv34 = sext i16 %call33 to i32
  %shr = ashr i32 %conv34, 3
  %conv35 = trunc i32 %shr to i16
  store i16 %conv35, i16* %filter1, align 2, !tbaa !9
  %43 = load i16, i16* %filter, align 2, !tbaa !9
  %conv36 = sext i16 %43 to i32
  %add37 = add nsw i32 %conv36, 3
  %44 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call38 = call signext i16 @signed_char_clamp_high(i32 %add37, i32 %44)
  %conv39 = sext i16 %call38 to i32
  %shr40 = ashr i32 %conv39, 3
  %conv41 = trunc i32 %shr40 to i16
  store i16 %conv41, i16* %filter2, align 2, !tbaa !9
  %45 = load i16, i16* %qs0, align 2, !tbaa !9
  %conv42 = sext i16 %45 to i32
  %46 = load i16, i16* %filter1, align 2, !tbaa !9
  %conv43 = sext i16 %46 to i32
  %sub44 = sub nsw i32 %conv42, %conv43
  %47 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call45 = call signext i16 @signed_char_clamp_high(i32 %sub44, i32 %47)
  %conv46 = sext i16 %call45 to i32
  %48 = load i32, i32* %shift, align 4, !tbaa !6
  %shl47 = shl i32 128, %48
  %add48 = add nsw i32 %conv46, %shl47
  %conv49 = trunc i32 %add48 to i16
  %49 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  store i16 %conv49, i16* %49, align 2, !tbaa !9
  %50 = load i16, i16* %ps0, align 2, !tbaa !9
  %conv50 = sext i16 %50 to i32
  %51 = load i16, i16* %filter2, align 2, !tbaa !9
  %conv51 = sext i16 %51 to i32
  %add52 = add nsw i32 %conv50, %conv51
  %52 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call53 = call signext i16 @signed_char_clamp_high(i32 %add52, i32 %52)
  %conv54 = sext i16 %call53 to i32
  %53 = load i32, i32* %shift, align 4, !tbaa !6
  %shl55 = shl i32 128, %53
  %add56 = add nsw i32 %conv54, %shl55
  %conv57 = trunc i32 %add56 to i16
  %54 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  store i16 %conv57, i16* %54, align 2, !tbaa !9
  %55 = load i16, i16* %filter1, align 2, !tbaa !9
  %conv58 = sext i16 %55 to i32
  %add59 = add nsw i32 %conv58, 1
  %shr60 = ashr i32 %add59, 1
  %56 = load i16, i16* %hev, align 2, !tbaa !9
  %conv61 = sext i16 %56 to i32
  %neg = xor i32 %conv61, -1
  %and62 = and i32 %shr60, %neg
  %conv63 = trunc i32 %and62 to i16
  store i16 %conv63, i16* %filter, align 2, !tbaa !9
  %57 = load i16, i16* %qs1, align 2, !tbaa !9
  %conv64 = sext i16 %57 to i32
  %58 = load i16, i16* %filter, align 2, !tbaa !9
  %conv65 = sext i16 %58 to i32
  %sub66 = sub nsw i32 %conv64, %conv65
  %59 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call67 = call signext i16 @signed_char_clamp_high(i32 %sub66, i32 %59)
  %conv68 = sext i16 %call67 to i32
  %60 = load i32, i32* %shift, align 4, !tbaa !6
  %shl69 = shl i32 128, %60
  %add70 = add nsw i32 %conv68, %shl69
  %conv71 = trunc i32 %add70 to i16
  %61 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  store i16 %conv71, i16* %61, align 2, !tbaa !9
  %62 = load i16, i16* %ps1, align 2, !tbaa !9
  %conv72 = sext i16 %62 to i32
  %63 = load i16, i16* %filter, align 2, !tbaa !9
  %conv73 = sext i16 %63 to i32
  %add74 = add nsw i32 %conv72, %conv73
  %64 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call75 = call signext i16 @signed_char_clamp_high(i32 %add74, i32 %64)
  %conv76 = sext i16 %call75 to i32
  %65 = load i32, i32* %shift, align 4, !tbaa !6
  %shl77 = shl i32 128, %65
  %add78 = add nsw i32 %conv76, %shl77
  %conv79 = trunc i32 %add78 to i16
  %66 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  store i16 %conv79, i16* %66, align 2, !tbaa !9
  %67 = bitcast i16* %filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %67) #4
  %68 = bitcast i16* %hev to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %68) #4
  %69 = bitcast i16* %qs1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %69) #4
  %70 = bitcast i16* %qs0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %70) #4
  %71 = bitcast i16* %ps0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %71) #4
  %72 = bitcast i16* %ps1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %72) #4
  %73 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  %74 = bitcast i16* %filter2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %74) #4
  %75 = bitcast i16* %filter1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %75) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_horizontal_4_dual_c(i16* %s, i32 %p, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_horizontal_4_c(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 %5)
  %6 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %6, i32 4
  %7 = load i32, i32* %p.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  %11 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_horizontal_4_c(i16* %add.ptr, i32 %7, i8* %8, i8* %9, i8* %10, i32 %11)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_vertical_4_c(i16* %s, i32 %pitch, i8* %blimit, i8* %limit, i8* %thresh, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %mask = alloca i8, align 1
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  %5 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 -2
  %6 = load i16, i16* %arrayidx, align 2, !tbaa !9
  store i16 %6, i16* %p1, align 2, !tbaa !9
  %7 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %7) #4
  %8 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i16, i16* %8, i32 -1
  %9 = load i16, i16* %arrayidx1, align 2, !tbaa !9
  store i16 %9, i16* %p0, align 2, !tbaa !9
  %10 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %10) #4
  %11 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %11, i32 0
  %12 = load i16, i16* %arrayidx2, align 2, !tbaa !9
  store i16 %12, i16* %q0, align 2, !tbaa !9
  %13 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #4
  %14 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i16, i16* %14, i32 1
  %15 = load i16, i16* %arrayidx3, align 2, !tbaa !9
  store i16 %15, i16* %q1, align 2, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %16 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %17 = load i8, i8* %16, align 1, !tbaa !8
  %18 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %19 = load i8, i8* %18, align 1, !tbaa !8
  %20 = load i16, i16* %p1, align 2, !tbaa !9
  %21 = load i16, i16* %p0, align 2, !tbaa !9
  %22 = load i16, i16* %q0, align 2, !tbaa !9
  %23 = load i16, i16* %q1, align 2, !tbaa !9
  %24 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call signext i8 @highbd_filter_mask2(i8 zeroext %17, i8 zeroext %19, i16 zeroext %20, i16 zeroext %21, i16 zeroext %22, i16 zeroext %23, i32 %24)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  %25 = load i8, i8* %mask, align 1, !tbaa !8
  %26 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %27 = load i8, i8* %26, align 1, !tbaa !8
  %28 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %28, i32 -2
  %29 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr4 = getelementptr inbounds i16, i16* %29, i32 -1
  %30 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %31 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr5 = getelementptr inbounds i16, i16* %31, i32 1
  %32 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter4(i8 signext %25, i8 zeroext %27, i16* %add.ptr, i16* %add.ptr4, i16* %30, i16* %add.ptr5, i32 %32)
  %33 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %34 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr6 = getelementptr inbounds i16, i16* %34, i32 %33
  store i16* %add.ptr6, i16** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  %35 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %35) #4
  %36 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %36) #4
  %37 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %37) #4
  %38 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %38) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %39, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %40 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_vertical_4_dual_c(i16* %s, i32 %pitch, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_vertical_4_c(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 %5)
  %6 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %7 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 4, %7
  %add.ptr = getelementptr inbounds i16, i16* %6, i32 %mul
  %8 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %11 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  %12 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_vertical_4_c(i16* %add.ptr, i32 %8, i8* %9, i8* %10, i8* %11, i32 %12)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_horizontal_8_c(i16* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p3 = alloca i16, align 2
  %p2 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %q2 = alloca i16, align 2
  %q3 = alloca i16, align 2
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  %5 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul = mul nsw i32 -4, %6
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 %mul
  %7 = load i16, i16* %arrayidx, align 2, !tbaa !9
  store i16 %7, i16* %p3, align 2, !tbaa !9
  %8 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %8) #4
  %9 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %10 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 -3, %10
  %arrayidx2 = getelementptr inbounds i16, i16* %9, i32 %mul1
  %11 = load i16, i16* %arrayidx2, align 2, !tbaa !9
  store i16 %11, i16* %p2, align 2, !tbaa !9
  %12 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %12) #4
  %13 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %14 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul3 = mul nsw i32 -2, %14
  %arrayidx4 = getelementptr inbounds i16, i16* %13, i32 %mul3
  %15 = load i16, i16* %arrayidx4, align 2, !tbaa !9
  store i16 %15, i16* %p1, align 2, !tbaa !9
  %16 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #4
  %17 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %18 = load i32, i32* %p.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %18
  %arrayidx5 = getelementptr inbounds i16, i16* %17, i32 %sub
  %19 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  store i16 %19, i16* %p0, align 2, !tbaa !9
  %20 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %20) #4
  %21 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %22 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 0, %22
  %arrayidx7 = getelementptr inbounds i16, i16* %21, i32 %mul6
  %23 = load i16, i16* %arrayidx7, align 2, !tbaa !9
  store i16 %23, i16* %q0, align 2, !tbaa !9
  %24 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %24) #4
  %25 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %26 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul8 = mul nsw i32 1, %26
  %arrayidx9 = getelementptr inbounds i16, i16* %25, i32 %mul8
  %27 = load i16, i16* %arrayidx9, align 2, !tbaa !9
  store i16 %27, i16* %q1, align 2, !tbaa !9
  %28 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %28) #4
  %29 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %30 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 2, %30
  %arrayidx11 = getelementptr inbounds i16, i16* %29, i32 %mul10
  %31 = load i16, i16* %arrayidx11, align 2, !tbaa !9
  store i16 %31, i16* %q2, align 2, !tbaa !9
  %32 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %32) #4
  %33 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %34 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul12 = mul nsw i32 3, %34
  %arrayidx13 = getelementptr inbounds i16, i16* %33, i32 %mul12
  %35 = load i16, i16* %arrayidx13, align 2, !tbaa !9
  store i16 %35, i16* %q3, align 2, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %36 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %37 = load i8, i8* %36, align 1, !tbaa !8
  %38 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %39 = load i8, i8* %38, align 1, !tbaa !8
  %40 = load i16, i16* %p3, align 2, !tbaa !9
  %41 = load i16, i16* %p2, align 2, !tbaa !9
  %42 = load i16, i16* %p1, align 2, !tbaa !9
  %43 = load i16, i16* %p0, align 2, !tbaa !9
  %44 = load i16, i16* %q0, align 2, !tbaa !9
  %45 = load i16, i16* %q1, align 2, !tbaa !9
  %46 = load i16, i16* %q2, align 2, !tbaa !9
  %47 = load i16, i16* %q3, align 2, !tbaa !9
  %48 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call signext i8 @highbd_filter_mask(i8 zeroext %37, i8 zeroext %39, i16 zeroext %40, i16 zeroext %41, i16 zeroext %42, i16 zeroext %43, i16 zeroext %44, i16 zeroext %45, i16 zeroext %46, i16 zeroext %47, i32 %48)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %49 = load i16, i16* %p3, align 2, !tbaa !9
  %50 = load i16, i16* %p2, align 2, !tbaa !9
  %51 = load i16, i16* %p1, align 2, !tbaa !9
  %52 = load i16, i16* %p0, align 2, !tbaa !9
  %53 = load i16, i16* %q0, align 2, !tbaa !9
  %54 = load i16, i16* %q1, align 2, !tbaa !9
  %55 = load i16, i16* %q2, align 2, !tbaa !9
  %56 = load i16, i16* %q3, align 2, !tbaa !9
  %57 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call14 = call signext i8 @highbd_flat_mask4(i8 zeroext 1, i16 zeroext %49, i16 zeroext %50, i16 zeroext %51, i16 zeroext %52, i16 zeroext %53, i16 zeroext %54, i16 zeroext %55, i16 zeroext %56, i32 %57)
  store i8 %call14, i8* %flat, align 1, !tbaa !8
  %58 = load i8, i8* %mask, align 1, !tbaa !8
  %59 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %60 = load i8, i8* %59, align 1, !tbaa !8
  %61 = load i8, i8* %flat, align 1, !tbaa !8
  %62 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %63 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 4, %63
  %idx.neg = sub i32 0, %mul15
  %add.ptr = getelementptr inbounds i16, i16* %62, i32 %idx.neg
  %64 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %65 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul16 = mul nsw i32 3, %65
  %idx.neg17 = sub i32 0, %mul16
  %add.ptr18 = getelementptr inbounds i16, i16* %64, i32 %idx.neg17
  %66 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %67 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul19 = mul nsw i32 2, %67
  %idx.neg20 = sub i32 0, %mul19
  %add.ptr21 = getelementptr inbounds i16, i16* %66, i32 %idx.neg20
  %68 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %69 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul22 = mul nsw i32 1, %69
  %idx.neg23 = sub i32 0, %mul22
  %add.ptr24 = getelementptr inbounds i16, i16* %68, i32 %idx.neg23
  %70 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %71 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %72 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 1, %72
  %add.ptr26 = getelementptr inbounds i16, i16* %71, i32 %mul25
  %73 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %74 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul27 = mul nsw i32 2, %74
  %add.ptr28 = getelementptr inbounds i16, i16* %73, i32 %mul27
  %75 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %76 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 3, %76
  %add.ptr30 = getelementptr inbounds i16, i16* %75, i32 %mul29
  %77 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter8(i8 signext %58, i8 zeroext %60, i8 signext %61, i16* %add.ptr, i16* %add.ptr18, i16* %add.ptr21, i16* %add.ptr24, i16* %70, i16* %add.ptr26, i16* %add.ptr28, i16* %add.ptr30, i32 %77)
  %78 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %78, i32 1
  store i16* %incdec.ptr, i16** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  %79 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %79) #4
  %80 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %80) #4
  %81 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %81) #4
  %82 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %82) #4
  %83 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %83) #4
  %84 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %84) #4
  %85 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %85) #4
  %86 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %86) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %87 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %87, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %88 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @highbd_filter_mask(i8 zeroext %limit, i8 zeroext %blimit, i16 zeroext %p3, i16 zeroext %p2, i16 zeroext %p1, i16 zeroext %p0, i16 zeroext %q0, i16 zeroext %q1, i16 zeroext %q2, i16 zeroext %q3, i32 %bd) #2 {
entry:
  %limit.addr = alloca i8, align 1
  %blimit.addr = alloca i8, align 1
  %p3.addr = alloca i16, align 2
  %p2.addr = alloca i16, align 2
  %p1.addr = alloca i16, align 2
  %p0.addr = alloca i16, align 2
  %q0.addr = alloca i16, align 2
  %q1.addr = alloca i16, align 2
  %q2.addr = alloca i16, align 2
  %q3.addr = alloca i16, align 2
  %bd.addr = alloca i32, align 4
  %mask = alloca i8, align 1
  %limit16 = alloca i16, align 2
  %blimit16 = alloca i16, align 2
  store i8 %limit, i8* %limit.addr, align 1, !tbaa !8
  store i8 %blimit, i8* %blimit.addr, align 1, !tbaa !8
  store i16 %p3, i16* %p3.addr, align 2, !tbaa !9
  store i16 %p2, i16* %p2.addr, align 2, !tbaa !9
  store i16 %p1, i16* %p1.addr, align 2, !tbaa !9
  store i16 %p0, i16* %p0.addr, align 2, !tbaa !9
  store i16 %q0, i16* %q0.addr, align 2, !tbaa !9
  store i16 %q1, i16* %q1.addr, align 2, !tbaa !9
  store i16 %q2, i16* %q2.addr, align 2, !tbaa !9
  store i16 %q3, i16* %q3.addr, align 2, !tbaa !9
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = bitcast i16* %limit16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #4
  %1 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv = zext i8 %1 to i16
  %conv1 = zext i16 %conv to i32
  %2 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 8
  %shl = shl i32 %conv1, %sub
  %conv2 = trunc i32 %shl to i16
  store i16 %conv2, i16* %limit16, align 2, !tbaa !9
  %3 = bitcast i16* %blimit16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %3) #4
  %4 = load i8, i8* %blimit.addr, align 1, !tbaa !8
  %conv3 = zext i8 %4 to i16
  %conv4 = zext i16 %conv3 to i32
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub5 = sub nsw i32 %5, 8
  %shl6 = shl i32 %conv4, %sub5
  %conv7 = trunc i32 %shl6 to i16
  store i16 %conv7, i16* %blimit16, align 2, !tbaa !9
  %6 = load i16, i16* %p3.addr, align 2, !tbaa !9
  %conv8 = zext i16 %6 to i32
  %7 = load i16, i16* %p2.addr, align 2, !tbaa !9
  %conv9 = zext i16 %7 to i32
  %sub10 = sub nsw i32 %conv8, %conv9
  %call = call i32 @abs(i32 %sub10) #5
  %8 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv11 = sext i16 %8 to i32
  %cmp = icmp sgt i32 %call, %conv11
  %conv12 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv12, -1
  %9 = load i8, i8* %mask, align 1, !tbaa !8
  %conv13 = sext i8 %9 to i32
  %or = or i32 %conv13, %mul
  %conv14 = trunc i32 %or to i8
  store i8 %conv14, i8* %mask, align 1, !tbaa !8
  %10 = load i16, i16* %p2.addr, align 2, !tbaa !9
  %conv15 = zext i16 %10 to i32
  %11 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv16 = zext i16 %11 to i32
  %sub17 = sub nsw i32 %conv15, %conv16
  %call18 = call i32 @abs(i32 %sub17) #5
  %12 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv19 = sext i16 %12 to i32
  %cmp20 = icmp sgt i32 %call18, %conv19
  %conv21 = zext i1 %cmp20 to i32
  %mul22 = mul nsw i32 %conv21, -1
  %13 = load i8, i8* %mask, align 1, !tbaa !8
  %conv23 = sext i8 %13 to i32
  %or24 = or i32 %conv23, %mul22
  %conv25 = trunc i32 %or24 to i8
  store i8 %conv25, i8* %mask, align 1, !tbaa !8
  %14 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv26 = zext i16 %14 to i32
  %15 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv27 = zext i16 %15 to i32
  %sub28 = sub nsw i32 %conv26, %conv27
  %call29 = call i32 @abs(i32 %sub28) #5
  %16 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv30 = sext i16 %16 to i32
  %cmp31 = icmp sgt i32 %call29, %conv30
  %conv32 = zext i1 %cmp31 to i32
  %mul33 = mul nsw i32 %conv32, -1
  %17 = load i8, i8* %mask, align 1, !tbaa !8
  %conv34 = sext i8 %17 to i32
  %or35 = or i32 %conv34, %mul33
  %conv36 = trunc i32 %or35 to i8
  store i8 %conv36, i8* %mask, align 1, !tbaa !8
  %18 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv37 = zext i16 %18 to i32
  %19 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv38 = zext i16 %19 to i32
  %sub39 = sub nsw i32 %conv37, %conv38
  %call40 = call i32 @abs(i32 %sub39) #5
  %20 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv41 = sext i16 %20 to i32
  %cmp42 = icmp sgt i32 %call40, %conv41
  %conv43 = zext i1 %cmp42 to i32
  %mul44 = mul nsw i32 %conv43, -1
  %21 = load i8, i8* %mask, align 1, !tbaa !8
  %conv45 = sext i8 %21 to i32
  %or46 = or i32 %conv45, %mul44
  %conv47 = trunc i32 %or46 to i8
  store i8 %conv47, i8* %mask, align 1, !tbaa !8
  %22 = load i16, i16* %q2.addr, align 2, !tbaa !9
  %conv48 = zext i16 %22 to i32
  %23 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv49 = zext i16 %23 to i32
  %sub50 = sub nsw i32 %conv48, %conv49
  %call51 = call i32 @abs(i32 %sub50) #5
  %24 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv52 = sext i16 %24 to i32
  %cmp53 = icmp sgt i32 %call51, %conv52
  %conv54 = zext i1 %cmp53 to i32
  %mul55 = mul nsw i32 %conv54, -1
  %25 = load i8, i8* %mask, align 1, !tbaa !8
  %conv56 = sext i8 %25 to i32
  %or57 = or i32 %conv56, %mul55
  %conv58 = trunc i32 %or57 to i8
  store i8 %conv58, i8* %mask, align 1, !tbaa !8
  %26 = load i16, i16* %q3.addr, align 2, !tbaa !9
  %conv59 = zext i16 %26 to i32
  %27 = load i16, i16* %q2.addr, align 2, !tbaa !9
  %conv60 = zext i16 %27 to i32
  %sub61 = sub nsw i32 %conv59, %conv60
  %call62 = call i32 @abs(i32 %sub61) #5
  %28 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv63 = sext i16 %28 to i32
  %cmp64 = icmp sgt i32 %call62, %conv63
  %conv65 = zext i1 %cmp64 to i32
  %mul66 = mul nsw i32 %conv65, -1
  %29 = load i8, i8* %mask, align 1, !tbaa !8
  %conv67 = sext i8 %29 to i32
  %or68 = or i32 %conv67, %mul66
  %conv69 = trunc i32 %or68 to i8
  store i8 %conv69, i8* %mask, align 1, !tbaa !8
  %30 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv70 = zext i16 %30 to i32
  %31 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv71 = zext i16 %31 to i32
  %sub72 = sub nsw i32 %conv70, %conv71
  %call73 = call i32 @abs(i32 %sub72) #5
  %mul74 = mul nsw i32 %call73, 2
  %32 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv75 = zext i16 %32 to i32
  %33 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv76 = zext i16 %33 to i32
  %sub77 = sub nsw i32 %conv75, %conv76
  %call78 = call i32 @abs(i32 %sub77) #5
  %div = sdiv i32 %call78, 2
  %add = add nsw i32 %mul74, %div
  %34 = load i16, i16* %blimit16, align 2, !tbaa !9
  %conv79 = sext i16 %34 to i32
  %cmp80 = icmp sgt i32 %add, %conv79
  %conv81 = zext i1 %cmp80 to i32
  %mul82 = mul nsw i32 %conv81, -1
  %35 = load i8, i8* %mask, align 1, !tbaa !8
  %conv83 = sext i8 %35 to i32
  %or84 = or i32 %conv83, %mul82
  %conv85 = trunc i32 %or84 to i8
  store i8 %conv85, i8* %mask, align 1, !tbaa !8
  %36 = load i8, i8* %mask, align 1, !tbaa !8
  %conv86 = sext i8 %36 to i32
  %neg = xor i32 %conv86, -1
  %conv87 = trunc i32 %neg to i8
  %37 = bitcast i16* %blimit16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %37) #4
  %38 = bitcast i16* %limit16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %38) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv87
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @highbd_flat_mask4(i8 zeroext %thresh, i16 zeroext %p3, i16 zeroext %p2, i16 zeroext %p1, i16 zeroext %p0, i16 zeroext %q0, i16 zeroext %q1, i16 zeroext %q2, i16 zeroext %q3, i32 %bd) #2 {
entry:
  %thresh.addr = alloca i8, align 1
  %p3.addr = alloca i16, align 2
  %p2.addr = alloca i16, align 2
  %p1.addr = alloca i16, align 2
  %p0.addr = alloca i16, align 2
  %q0.addr = alloca i16, align 2
  %q1.addr = alloca i16, align 2
  %q2.addr = alloca i16, align 2
  %q3.addr = alloca i16, align 2
  %bd.addr = alloca i32, align 4
  %mask = alloca i8, align 1
  %thresh16 = alloca i16, align 2
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i16 %p3, i16* %p3.addr, align 2, !tbaa !9
  store i16 %p2, i16* %p2.addr, align 2, !tbaa !9
  store i16 %p1, i16* %p1.addr, align 2, !tbaa !9
  store i16 %p0, i16* %p0.addr, align 2, !tbaa !9
  store i16 %q0, i16* %q0.addr, align 2, !tbaa !9
  store i16 %q1, i16* %q1.addr, align 2, !tbaa !9
  store i16 %q2, i16* %q2.addr, align 2, !tbaa !9
  store i16 %q3, i16* %q3.addr, align 2, !tbaa !9
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = bitcast i16* %thresh16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #4
  %1 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv = zext i8 %1 to i16
  %conv1 = zext i16 %conv to i32
  %2 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 8
  %shl = shl i32 %conv1, %sub
  %conv2 = trunc i32 %shl to i16
  store i16 %conv2, i16* %thresh16, align 2, !tbaa !9
  %3 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv3 = zext i16 %3 to i32
  %4 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv4 = zext i16 %4 to i32
  %sub5 = sub nsw i32 %conv3, %conv4
  %call = call i32 @abs(i32 %sub5) #5
  %5 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv6 = sext i16 %5 to i32
  %cmp = icmp sgt i32 %call, %conv6
  %conv7 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv7, -1
  %6 = load i8, i8* %mask, align 1, !tbaa !8
  %conv8 = sext i8 %6 to i32
  %or = or i32 %conv8, %mul
  %conv9 = trunc i32 %or to i8
  store i8 %conv9, i8* %mask, align 1, !tbaa !8
  %7 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv10 = zext i16 %7 to i32
  %8 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv11 = zext i16 %8 to i32
  %sub12 = sub nsw i32 %conv10, %conv11
  %call13 = call i32 @abs(i32 %sub12) #5
  %9 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv14 = sext i16 %9 to i32
  %cmp15 = icmp sgt i32 %call13, %conv14
  %conv16 = zext i1 %cmp15 to i32
  %mul17 = mul nsw i32 %conv16, -1
  %10 = load i8, i8* %mask, align 1, !tbaa !8
  %conv18 = sext i8 %10 to i32
  %or19 = or i32 %conv18, %mul17
  %conv20 = trunc i32 %or19 to i8
  store i8 %conv20, i8* %mask, align 1, !tbaa !8
  %11 = load i16, i16* %p2.addr, align 2, !tbaa !9
  %conv21 = zext i16 %11 to i32
  %12 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv22 = zext i16 %12 to i32
  %sub23 = sub nsw i32 %conv21, %conv22
  %call24 = call i32 @abs(i32 %sub23) #5
  %13 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv25 = sext i16 %13 to i32
  %cmp26 = icmp sgt i32 %call24, %conv25
  %conv27 = zext i1 %cmp26 to i32
  %mul28 = mul nsw i32 %conv27, -1
  %14 = load i8, i8* %mask, align 1, !tbaa !8
  %conv29 = sext i8 %14 to i32
  %or30 = or i32 %conv29, %mul28
  %conv31 = trunc i32 %or30 to i8
  store i8 %conv31, i8* %mask, align 1, !tbaa !8
  %15 = load i16, i16* %q2.addr, align 2, !tbaa !9
  %conv32 = zext i16 %15 to i32
  %16 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv33 = zext i16 %16 to i32
  %sub34 = sub nsw i32 %conv32, %conv33
  %call35 = call i32 @abs(i32 %sub34) #5
  %17 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv36 = sext i16 %17 to i32
  %cmp37 = icmp sgt i32 %call35, %conv36
  %conv38 = zext i1 %cmp37 to i32
  %mul39 = mul nsw i32 %conv38, -1
  %18 = load i8, i8* %mask, align 1, !tbaa !8
  %conv40 = sext i8 %18 to i32
  %or41 = or i32 %conv40, %mul39
  %conv42 = trunc i32 %or41 to i8
  store i8 %conv42, i8* %mask, align 1, !tbaa !8
  %19 = load i16, i16* %p3.addr, align 2, !tbaa !9
  %conv43 = zext i16 %19 to i32
  %20 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv44 = zext i16 %20 to i32
  %sub45 = sub nsw i32 %conv43, %conv44
  %call46 = call i32 @abs(i32 %sub45) #5
  %21 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv47 = sext i16 %21 to i32
  %cmp48 = icmp sgt i32 %call46, %conv47
  %conv49 = zext i1 %cmp48 to i32
  %mul50 = mul nsw i32 %conv49, -1
  %22 = load i8, i8* %mask, align 1, !tbaa !8
  %conv51 = sext i8 %22 to i32
  %or52 = or i32 %conv51, %mul50
  %conv53 = trunc i32 %or52 to i8
  store i8 %conv53, i8* %mask, align 1, !tbaa !8
  %23 = load i16, i16* %q3.addr, align 2, !tbaa !9
  %conv54 = zext i16 %23 to i32
  %24 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv55 = zext i16 %24 to i32
  %sub56 = sub nsw i32 %conv54, %conv55
  %call57 = call i32 @abs(i32 %sub56) #5
  %25 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv58 = sext i16 %25 to i32
  %cmp59 = icmp sgt i32 %call57, %conv58
  %conv60 = zext i1 %cmp59 to i32
  %mul61 = mul nsw i32 %conv60, -1
  %26 = load i8, i8* %mask, align 1, !tbaa !8
  %conv62 = sext i8 %26 to i32
  %or63 = or i32 %conv62, %mul61
  %conv64 = trunc i32 %or63 to i8
  store i8 %conv64, i8* %mask, align 1, !tbaa !8
  %27 = load i8, i8* %mask, align 1, !tbaa !8
  %conv65 = sext i8 %27 to i32
  %neg = xor i32 %conv65, -1
  %conv66 = trunc i32 %neg to i8
  %28 = bitcast i16* %thresh16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %28) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv66
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_filter8(i8 signext %mask, i8 zeroext %thresh, i8 signext %flat, i16* %op3, i16* %op2, i16* %op1, i16* %op0, i16* %oq0, i16* %oq1, i16* %oq2, i16* %oq3, i32 %bd) #2 {
entry:
  %mask.addr = alloca i8, align 1
  %thresh.addr = alloca i8, align 1
  %flat.addr = alloca i8, align 1
  %op3.addr = alloca i16*, align 4
  %op2.addr = alloca i16*, align 4
  %op1.addr = alloca i16*, align 4
  %op0.addr = alloca i16*, align 4
  %oq0.addr = alloca i16*, align 4
  %oq1.addr = alloca i16*, align 4
  %oq2.addr = alloca i16*, align 4
  %oq3.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %p3 = alloca i16, align 2
  %p2 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %q2 = alloca i16, align 2
  %q3 = alloca i16, align 2
  store i8 %mask, i8* %mask.addr, align 1, !tbaa !8
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8 %flat, i8* %flat.addr, align 1, !tbaa !8
  store i16* %op3, i16** %op3.addr, align 4, !tbaa !2
  store i16* %op2, i16** %op2.addr, align 4, !tbaa !2
  store i16* %op1, i16** %op1.addr, align 4, !tbaa !2
  store i16* %op0, i16** %op0.addr, align 4, !tbaa !2
  store i16* %oq0, i16** %oq0.addr, align 4, !tbaa !2
  store i16* %oq1, i16** %oq1.addr, align 4, !tbaa !2
  store i16* %oq2, i16** %oq2.addr, align 4, !tbaa !2
  store i16* %oq3, i16** %oq3.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i8, i8* %flat.addr, align 1, !tbaa !8
  %conv = sext i8 %0 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %conv1 = sext i8 %1 to i32
  %tobool2 = icmp ne i32 %conv1, 0
  br i1 %tobool2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %2) #4
  %3 = load i16*, i16** %op3.addr, align 4, !tbaa !2
  %4 = load i16, i16* %3, align 2, !tbaa !9
  store i16 %4, i16* %p3, align 2, !tbaa !9
  %5 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %5) #4
  %6 = load i16*, i16** %op2.addr, align 4, !tbaa !2
  %7 = load i16, i16* %6, align 2, !tbaa !9
  store i16 %7, i16* %p2, align 2, !tbaa !9
  %8 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %8) #4
  %9 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  %10 = load i16, i16* %9, align 2, !tbaa !9
  store i16 %10, i16* %p1, align 2, !tbaa !9
  %11 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %11) #4
  %12 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  %13 = load i16, i16* %12, align 2, !tbaa !9
  store i16 %13, i16* %p0, align 2, !tbaa !9
  %14 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %14) #4
  %15 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  %16 = load i16, i16* %15, align 2, !tbaa !9
  store i16 %16, i16* %q0, align 2, !tbaa !9
  %17 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %17) #4
  %18 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  %19 = load i16, i16* %18, align 2, !tbaa !9
  store i16 %19, i16* %q1, align 2, !tbaa !9
  %20 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %20) #4
  %21 = load i16*, i16** %oq2.addr, align 4, !tbaa !2
  %22 = load i16, i16* %21, align 2, !tbaa !9
  store i16 %22, i16* %q2, align 2, !tbaa !9
  %23 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %23) #4
  %24 = load i16*, i16** %oq3.addr, align 4, !tbaa !2
  %25 = load i16, i16* %24, align 2, !tbaa !9
  store i16 %25, i16* %q3, align 2, !tbaa !9
  %26 = load i16, i16* %p3, align 2, !tbaa !9
  %conv3 = zext i16 %26 to i32
  %27 = load i16, i16* %p3, align 2, !tbaa !9
  %conv4 = zext i16 %27 to i32
  %add = add nsw i32 %conv3, %conv4
  %28 = load i16, i16* %p3, align 2, !tbaa !9
  %conv5 = zext i16 %28 to i32
  %add6 = add nsw i32 %add, %conv5
  %29 = load i16, i16* %p2, align 2, !tbaa !9
  %conv7 = zext i16 %29 to i32
  %mul = mul nsw i32 2, %conv7
  %add8 = add nsw i32 %add6, %mul
  %30 = load i16, i16* %p1, align 2, !tbaa !9
  %conv9 = zext i16 %30 to i32
  %add10 = add nsw i32 %add8, %conv9
  %31 = load i16, i16* %p0, align 2, !tbaa !9
  %conv11 = zext i16 %31 to i32
  %add12 = add nsw i32 %add10, %conv11
  %32 = load i16, i16* %q0, align 2, !tbaa !9
  %conv13 = zext i16 %32 to i32
  %add14 = add nsw i32 %add12, %conv13
  %add15 = add nsw i32 %add14, 4
  %shr = ashr i32 %add15, 3
  %conv16 = trunc i32 %shr to i16
  %33 = load i16*, i16** %op2.addr, align 4, !tbaa !2
  store i16 %conv16, i16* %33, align 2, !tbaa !9
  %34 = load i16, i16* %p3, align 2, !tbaa !9
  %conv17 = zext i16 %34 to i32
  %35 = load i16, i16* %p3, align 2, !tbaa !9
  %conv18 = zext i16 %35 to i32
  %add19 = add nsw i32 %conv17, %conv18
  %36 = load i16, i16* %p2, align 2, !tbaa !9
  %conv20 = zext i16 %36 to i32
  %add21 = add nsw i32 %add19, %conv20
  %37 = load i16, i16* %p1, align 2, !tbaa !9
  %conv22 = zext i16 %37 to i32
  %mul23 = mul nsw i32 2, %conv22
  %add24 = add nsw i32 %add21, %mul23
  %38 = load i16, i16* %p0, align 2, !tbaa !9
  %conv25 = zext i16 %38 to i32
  %add26 = add nsw i32 %add24, %conv25
  %39 = load i16, i16* %q0, align 2, !tbaa !9
  %conv27 = zext i16 %39 to i32
  %add28 = add nsw i32 %add26, %conv27
  %40 = load i16, i16* %q1, align 2, !tbaa !9
  %conv29 = zext i16 %40 to i32
  %add30 = add nsw i32 %add28, %conv29
  %add31 = add nsw i32 %add30, 4
  %shr32 = ashr i32 %add31, 3
  %conv33 = trunc i32 %shr32 to i16
  %41 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  store i16 %conv33, i16* %41, align 2, !tbaa !9
  %42 = load i16, i16* %p3, align 2, !tbaa !9
  %conv34 = zext i16 %42 to i32
  %43 = load i16, i16* %p2, align 2, !tbaa !9
  %conv35 = zext i16 %43 to i32
  %add36 = add nsw i32 %conv34, %conv35
  %44 = load i16, i16* %p1, align 2, !tbaa !9
  %conv37 = zext i16 %44 to i32
  %add38 = add nsw i32 %add36, %conv37
  %45 = load i16, i16* %p0, align 2, !tbaa !9
  %conv39 = zext i16 %45 to i32
  %mul40 = mul nsw i32 2, %conv39
  %add41 = add nsw i32 %add38, %mul40
  %46 = load i16, i16* %q0, align 2, !tbaa !9
  %conv42 = zext i16 %46 to i32
  %add43 = add nsw i32 %add41, %conv42
  %47 = load i16, i16* %q1, align 2, !tbaa !9
  %conv44 = zext i16 %47 to i32
  %add45 = add nsw i32 %add43, %conv44
  %48 = load i16, i16* %q2, align 2, !tbaa !9
  %conv46 = zext i16 %48 to i32
  %add47 = add nsw i32 %add45, %conv46
  %add48 = add nsw i32 %add47, 4
  %shr49 = ashr i32 %add48, 3
  %conv50 = trunc i32 %shr49 to i16
  %49 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  store i16 %conv50, i16* %49, align 2, !tbaa !9
  %50 = load i16, i16* %p2, align 2, !tbaa !9
  %conv51 = zext i16 %50 to i32
  %51 = load i16, i16* %p1, align 2, !tbaa !9
  %conv52 = zext i16 %51 to i32
  %add53 = add nsw i32 %conv51, %conv52
  %52 = load i16, i16* %p0, align 2, !tbaa !9
  %conv54 = zext i16 %52 to i32
  %add55 = add nsw i32 %add53, %conv54
  %53 = load i16, i16* %q0, align 2, !tbaa !9
  %conv56 = zext i16 %53 to i32
  %mul57 = mul nsw i32 2, %conv56
  %add58 = add nsw i32 %add55, %mul57
  %54 = load i16, i16* %q1, align 2, !tbaa !9
  %conv59 = zext i16 %54 to i32
  %add60 = add nsw i32 %add58, %conv59
  %55 = load i16, i16* %q2, align 2, !tbaa !9
  %conv61 = zext i16 %55 to i32
  %add62 = add nsw i32 %add60, %conv61
  %56 = load i16, i16* %q3, align 2, !tbaa !9
  %conv63 = zext i16 %56 to i32
  %add64 = add nsw i32 %add62, %conv63
  %add65 = add nsw i32 %add64, 4
  %shr66 = ashr i32 %add65, 3
  %conv67 = trunc i32 %shr66 to i16
  %57 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  store i16 %conv67, i16* %57, align 2, !tbaa !9
  %58 = load i16, i16* %p1, align 2, !tbaa !9
  %conv68 = zext i16 %58 to i32
  %59 = load i16, i16* %p0, align 2, !tbaa !9
  %conv69 = zext i16 %59 to i32
  %add70 = add nsw i32 %conv68, %conv69
  %60 = load i16, i16* %q0, align 2, !tbaa !9
  %conv71 = zext i16 %60 to i32
  %add72 = add nsw i32 %add70, %conv71
  %61 = load i16, i16* %q1, align 2, !tbaa !9
  %conv73 = zext i16 %61 to i32
  %mul74 = mul nsw i32 2, %conv73
  %add75 = add nsw i32 %add72, %mul74
  %62 = load i16, i16* %q2, align 2, !tbaa !9
  %conv76 = zext i16 %62 to i32
  %add77 = add nsw i32 %add75, %conv76
  %63 = load i16, i16* %q3, align 2, !tbaa !9
  %conv78 = zext i16 %63 to i32
  %add79 = add nsw i32 %add77, %conv78
  %64 = load i16, i16* %q3, align 2, !tbaa !9
  %conv80 = zext i16 %64 to i32
  %add81 = add nsw i32 %add79, %conv80
  %add82 = add nsw i32 %add81, 4
  %shr83 = ashr i32 %add82, 3
  %conv84 = trunc i32 %shr83 to i16
  %65 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  store i16 %conv84, i16* %65, align 2, !tbaa !9
  %66 = load i16, i16* %p0, align 2, !tbaa !9
  %conv85 = zext i16 %66 to i32
  %67 = load i16, i16* %q0, align 2, !tbaa !9
  %conv86 = zext i16 %67 to i32
  %add87 = add nsw i32 %conv85, %conv86
  %68 = load i16, i16* %q1, align 2, !tbaa !9
  %conv88 = zext i16 %68 to i32
  %add89 = add nsw i32 %add87, %conv88
  %69 = load i16, i16* %q2, align 2, !tbaa !9
  %conv90 = zext i16 %69 to i32
  %mul91 = mul nsw i32 2, %conv90
  %add92 = add nsw i32 %add89, %mul91
  %70 = load i16, i16* %q3, align 2, !tbaa !9
  %conv93 = zext i16 %70 to i32
  %add94 = add nsw i32 %add92, %conv93
  %71 = load i16, i16* %q3, align 2, !tbaa !9
  %conv95 = zext i16 %71 to i32
  %add96 = add nsw i32 %add94, %conv95
  %72 = load i16, i16* %q3, align 2, !tbaa !9
  %conv97 = zext i16 %72 to i32
  %add98 = add nsw i32 %add96, %conv97
  %add99 = add nsw i32 %add98, 4
  %shr100 = ashr i32 %add99, 3
  %conv101 = trunc i32 %shr100 to i16
  %73 = load i16*, i16** %oq2.addr, align 4, !tbaa !2
  store i16 %conv101, i16* %73, align 2, !tbaa !9
  %74 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %74) #4
  %75 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %75) #4
  %76 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %76) #4
  %77 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %77) #4
  %78 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %78) #4
  %79 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %79) #4
  %80 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %80) #4
  %81 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %81) #4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %entry
  %82 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %83 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %84 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  %85 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  %86 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  %87 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  %88 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter4(i8 signext %82, i8 zeroext %83, i16* %84, i16* %85, i16* %86, i16* %87, i32 %88)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_horizontal_6_c(i16* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p2 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %q2 = alloca i16, align 2
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  %5 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul = mul nsw i32 -3, %6
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 %mul
  %7 = load i16, i16* %arrayidx, align 2, !tbaa !9
  store i16 %7, i16* %p2, align 2, !tbaa !9
  %8 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %8) #4
  %9 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %10 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 -2, %10
  %arrayidx2 = getelementptr inbounds i16, i16* %9, i32 %mul1
  %11 = load i16, i16* %arrayidx2, align 2, !tbaa !9
  store i16 %11, i16* %p1, align 2, !tbaa !9
  %12 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %12) #4
  %13 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %14 = load i32, i32* %p.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %14
  %arrayidx3 = getelementptr inbounds i16, i16* %13, i32 %sub
  %15 = load i16, i16* %arrayidx3, align 2, !tbaa !9
  store i16 %15, i16* %p0, align 2, !tbaa !9
  %16 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #4
  %17 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %18 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 0, %18
  %arrayidx5 = getelementptr inbounds i16, i16* %17, i32 %mul4
  %19 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  store i16 %19, i16* %q0, align 2, !tbaa !9
  %20 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %20) #4
  %21 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %22 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 1, %22
  %arrayidx7 = getelementptr inbounds i16, i16* %21, i32 %mul6
  %23 = load i16, i16* %arrayidx7, align 2, !tbaa !9
  store i16 %23, i16* %q1, align 2, !tbaa !9
  %24 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %24) #4
  %25 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %26 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul8 = mul nsw i32 2, %26
  %arrayidx9 = getelementptr inbounds i16, i16* %25, i32 %mul8
  %27 = load i16, i16* %arrayidx9, align 2, !tbaa !9
  store i16 %27, i16* %q2, align 2, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %28 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %29 = load i8, i8* %28, align 1, !tbaa !8
  %30 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !8
  %32 = load i16, i16* %p2, align 2, !tbaa !9
  %33 = load i16, i16* %p1, align 2, !tbaa !9
  %34 = load i16, i16* %p0, align 2, !tbaa !9
  %35 = load i16, i16* %q0, align 2, !tbaa !9
  %36 = load i16, i16* %q1, align 2, !tbaa !9
  %37 = load i16, i16* %q2, align 2, !tbaa !9
  %38 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call signext i8 @highbd_filter_mask3_chroma(i8 zeroext %29, i8 zeroext %31, i16 zeroext %32, i16 zeroext %33, i16 zeroext %34, i16 zeroext %35, i16 zeroext %36, i16 zeroext %37, i32 %38)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %39 = load i16, i16* %p2, align 2, !tbaa !9
  %40 = load i16, i16* %p1, align 2, !tbaa !9
  %41 = load i16, i16* %p0, align 2, !tbaa !9
  %42 = load i16, i16* %q0, align 2, !tbaa !9
  %43 = load i16, i16* %q1, align 2, !tbaa !9
  %44 = load i16, i16* %q2, align 2, !tbaa !9
  %45 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call10 = call signext i8 @highbd_flat_mask3_chroma(i8 zeroext 1, i16 zeroext %39, i16 zeroext %40, i16 zeroext %41, i16 zeroext %42, i16 zeroext %43, i16 zeroext %44, i32 %45)
  store i8 %call10, i8* %flat, align 1, !tbaa !8
  %46 = load i8, i8* %mask, align 1, !tbaa !8
  %47 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !8
  %49 = load i8, i8* %flat, align 1, !tbaa !8
  %50 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %51 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 3, %51
  %idx.neg = sub i32 0, %mul11
  %add.ptr = getelementptr inbounds i16, i16* %50, i32 %idx.neg
  %52 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %53 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul12 = mul nsw i32 2, %53
  %idx.neg13 = sub i32 0, %mul12
  %add.ptr14 = getelementptr inbounds i16, i16* %52, i32 %idx.neg13
  %54 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %55 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 1, %55
  %idx.neg16 = sub i32 0, %mul15
  %add.ptr17 = getelementptr inbounds i16, i16* %54, i32 %idx.neg16
  %56 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %57 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %58 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 1, %58
  %add.ptr19 = getelementptr inbounds i16, i16* %57, i32 %mul18
  %59 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %60 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 2, %60
  %add.ptr21 = getelementptr inbounds i16, i16* %59, i32 %mul20
  %61 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter6(i8 signext %46, i8 zeroext %48, i8 signext %49, i16* %add.ptr, i16* %add.ptr14, i16* %add.ptr17, i16* %56, i16* %add.ptr19, i16* %add.ptr21, i32 %61)
  %62 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %62, i32 1
  store i16* %incdec.ptr, i16** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  %63 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %63) #4
  %64 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %64) #4
  %65 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %65) #4
  %66 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %66) #4
  %67 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %67) #4
  %68 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %68) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %69, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %70 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @highbd_filter_mask3_chroma(i8 zeroext %limit, i8 zeroext %blimit, i16 zeroext %p2, i16 zeroext %p1, i16 zeroext %p0, i16 zeroext %q0, i16 zeroext %q1, i16 zeroext %q2, i32 %bd) #2 {
entry:
  %limit.addr = alloca i8, align 1
  %blimit.addr = alloca i8, align 1
  %p2.addr = alloca i16, align 2
  %p1.addr = alloca i16, align 2
  %p0.addr = alloca i16, align 2
  %q0.addr = alloca i16, align 2
  %q1.addr = alloca i16, align 2
  %q2.addr = alloca i16, align 2
  %bd.addr = alloca i32, align 4
  %mask = alloca i8, align 1
  %limit16 = alloca i16, align 2
  %blimit16 = alloca i16, align 2
  store i8 %limit, i8* %limit.addr, align 1, !tbaa !8
  store i8 %blimit, i8* %blimit.addr, align 1, !tbaa !8
  store i16 %p2, i16* %p2.addr, align 2, !tbaa !9
  store i16 %p1, i16* %p1.addr, align 2, !tbaa !9
  store i16 %p0, i16* %p0.addr, align 2, !tbaa !9
  store i16 %q0, i16* %q0.addr, align 2, !tbaa !9
  store i16 %q1, i16* %q1.addr, align 2, !tbaa !9
  store i16 %q2, i16* %q2.addr, align 2, !tbaa !9
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = bitcast i16* %limit16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #4
  %1 = load i8, i8* %limit.addr, align 1, !tbaa !8
  %conv = zext i8 %1 to i16
  %conv1 = zext i16 %conv to i32
  %2 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 8
  %shl = shl i32 %conv1, %sub
  %conv2 = trunc i32 %shl to i16
  store i16 %conv2, i16* %limit16, align 2, !tbaa !9
  %3 = bitcast i16* %blimit16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %3) #4
  %4 = load i8, i8* %blimit.addr, align 1, !tbaa !8
  %conv3 = zext i8 %4 to i16
  %conv4 = zext i16 %conv3 to i32
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub5 = sub nsw i32 %5, 8
  %shl6 = shl i32 %conv4, %sub5
  %conv7 = trunc i32 %shl6 to i16
  store i16 %conv7, i16* %blimit16, align 2, !tbaa !9
  %6 = load i16, i16* %p2.addr, align 2, !tbaa !9
  %conv8 = zext i16 %6 to i32
  %7 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv9 = zext i16 %7 to i32
  %sub10 = sub nsw i32 %conv8, %conv9
  %call = call i32 @abs(i32 %sub10) #5
  %8 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv11 = sext i16 %8 to i32
  %cmp = icmp sgt i32 %call, %conv11
  %conv12 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv12, -1
  %9 = load i8, i8* %mask, align 1, !tbaa !8
  %conv13 = sext i8 %9 to i32
  %or = or i32 %conv13, %mul
  %conv14 = trunc i32 %or to i8
  store i8 %conv14, i8* %mask, align 1, !tbaa !8
  %10 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv15 = zext i16 %10 to i32
  %11 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv16 = zext i16 %11 to i32
  %sub17 = sub nsw i32 %conv15, %conv16
  %call18 = call i32 @abs(i32 %sub17) #5
  %12 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv19 = sext i16 %12 to i32
  %cmp20 = icmp sgt i32 %call18, %conv19
  %conv21 = zext i1 %cmp20 to i32
  %mul22 = mul nsw i32 %conv21, -1
  %13 = load i8, i8* %mask, align 1, !tbaa !8
  %conv23 = sext i8 %13 to i32
  %or24 = or i32 %conv23, %mul22
  %conv25 = trunc i32 %or24 to i8
  store i8 %conv25, i8* %mask, align 1, !tbaa !8
  %14 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv26 = zext i16 %14 to i32
  %15 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv27 = zext i16 %15 to i32
  %sub28 = sub nsw i32 %conv26, %conv27
  %call29 = call i32 @abs(i32 %sub28) #5
  %16 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv30 = sext i16 %16 to i32
  %cmp31 = icmp sgt i32 %call29, %conv30
  %conv32 = zext i1 %cmp31 to i32
  %mul33 = mul nsw i32 %conv32, -1
  %17 = load i8, i8* %mask, align 1, !tbaa !8
  %conv34 = sext i8 %17 to i32
  %or35 = or i32 %conv34, %mul33
  %conv36 = trunc i32 %or35 to i8
  store i8 %conv36, i8* %mask, align 1, !tbaa !8
  %18 = load i16, i16* %q2.addr, align 2, !tbaa !9
  %conv37 = zext i16 %18 to i32
  %19 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv38 = zext i16 %19 to i32
  %sub39 = sub nsw i32 %conv37, %conv38
  %call40 = call i32 @abs(i32 %sub39) #5
  %20 = load i16, i16* %limit16, align 2, !tbaa !9
  %conv41 = sext i16 %20 to i32
  %cmp42 = icmp sgt i32 %call40, %conv41
  %conv43 = zext i1 %cmp42 to i32
  %mul44 = mul nsw i32 %conv43, -1
  %21 = load i8, i8* %mask, align 1, !tbaa !8
  %conv45 = sext i8 %21 to i32
  %or46 = or i32 %conv45, %mul44
  %conv47 = trunc i32 %or46 to i8
  store i8 %conv47, i8* %mask, align 1, !tbaa !8
  %22 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv48 = zext i16 %22 to i32
  %23 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv49 = zext i16 %23 to i32
  %sub50 = sub nsw i32 %conv48, %conv49
  %call51 = call i32 @abs(i32 %sub50) #5
  %mul52 = mul nsw i32 %call51, 2
  %24 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv53 = zext i16 %24 to i32
  %25 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv54 = zext i16 %25 to i32
  %sub55 = sub nsw i32 %conv53, %conv54
  %call56 = call i32 @abs(i32 %sub55) #5
  %div = sdiv i32 %call56, 2
  %add = add nsw i32 %mul52, %div
  %26 = load i16, i16* %blimit16, align 2, !tbaa !9
  %conv57 = sext i16 %26 to i32
  %cmp58 = icmp sgt i32 %add, %conv57
  %conv59 = zext i1 %cmp58 to i32
  %mul60 = mul nsw i32 %conv59, -1
  %27 = load i8, i8* %mask, align 1, !tbaa !8
  %conv61 = sext i8 %27 to i32
  %or62 = or i32 %conv61, %mul60
  %conv63 = trunc i32 %or62 to i8
  store i8 %conv63, i8* %mask, align 1, !tbaa !8
  %28 = load i8, i8* %mask, align 1, !tbaa !8
  %conv64 = sext i8 %28 to i32
  %neg = xor i32 %conv64, -1
  %conv65 = trunc i32 %neg to i8
  %29 = bitcast i16* %blimit16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %29) #4
  %30 = bitcast i16* %limit16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %30) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv65
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @highbd_flat_mask3_chroma(i8 zeroext %thresh, i16 zeroext %p2, i16 zeroext %p1, i16 zeroext %p0, i16 zeroext %q0, i16 zeroext %q1, i16 zeroext %q2, i32 %bd) #2 {
entry:
  %thresh.addr = alloca i8, align 1
  %p2.addr = alloca i16, align 2
  %p1.addr = alloca i16, align 2
  %p0.addr = alloca i16, align 2
  %q0.addr = alloca i16, align 2
  %q1.addr = alloca i16, align 2
  %q2.addr = alloca i16, align 2
  %bd.addr = alloca i32, align 4
  %mask = alloca i8, align 1
  %thresh16 = alloca i16, align 2
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i16 %p2, i16* %p2.addr, align 2, !tbaa !9
  store i16 %p1, i16* %p1.addr, align 2, !tbaa !9
  store i16 %p0, i16* %p0.addr, align 2, !tbaa !9
  store i16 %q0, i16* %q0.addr, align 2, !tbaa !9
  store i16 %q1, i16* %q1.addr, align 2, !tbaa !9
  store i16 %q2, i16* %q2.addr, align 2, !tbaa !9
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  store i8 0, i8* %mask, align 1, !tbaa !8
  %0 = bitcast i16* %thresh16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #4
  %1 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv = zext i8 %1 to i16
  %conv1 = zext i16 %conv to i32
  %2 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 8
  %shl = shl i32 %conv1, %sub
  %conv2 = trunc i32 %shl to i16
  store i16 %conv2, i16* %thresh16, align 2, !tbaa !9
  %3 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv3 = zext i16 %3 to i32
  %4 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv4 = zext i16 %4 to i32
  %sub5 = sub nsw i32 %conv3, %conv4
  %call = call i32 @abs(i32 %sub5) #5
  %5 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv6 = sext i16 %5 to i32
  %cmp = icmp sgt i32 %call, %conv6
  %conv7 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv7, -1
  %6 = load i8, i8* %mask, align 1, !tbaa !8
  %conv8 = sext i8 %6 to i32
  %or = or i32 %conv8, %mul
  %conv9 = trunc i32 %or to i8
  store i8 %conv9, i8* %mask, align 1, !tbaa !8
  %7 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv10 = zext i16 %7 to i32
  %8 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv11 = zext i16 %8 to i32
  %sub12 = sub nsw i32 %conv10, %conv11
  %call13 = call i32 @abs(i32 %sub12) #5
  %9 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv14 = sext i16 %9 to i32
  %cmp15 = icmp sgt i32 %call13, %conv14
  %conv16 = zext i1 %cmp15 to i32
  %mul17 = mul nsw i32 %conv16, -1
  %10 = load i8, i8* %mask, align 1, !tbaa !8
  %conv18 = sext i8 %10 to i32
  %or19 = or i32 %conv18, %mul17
  %conv20 = trunc i32 %or19 to i8
  store i8 %conv20, i8* %mask, align 1, !tbaa !8
  %11 = load i16, i16* %p2.addr, align 2, !tbaa !9
  %conv21 = zext i16 %11 to i32
  %12 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv22 = zext i16 %12 to i32
  %sub23 = sub nsw i32 %conv21, %conv22
  %call24 = call i32 @abs(i32 %sub23) #5
  %13 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv25 = sext i16 %13 to i32
  %cmp26 = icmp sgt i32 %call24, %conv25
  %conv27 = zext i1 %cmp26 to i32
  %mul28 = mul nsw i32 %conv27, -1
  %14 = load i8, i8* %mask, align 1, !tbaa !8
  %conv29 = sext i8 %14 to i32
  %or30 = or i32 %conv29, %mul28
  %conv31 = trunc i32 %or30 to i8
  store i8 %conv31, i8* %mask, align 1, !tbaa !8
  %15 = load i16, i16* %q2.addr, align 2, !tbaa !9
  %conv32 = zext i16 %15 to i32
  %16 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv33 = zext i16 %16 to i32
  %sub34 = sub nsw i32 %conv32, %conv33
  %call35 = call i32 @abs(i32 %sub34) #5
  %17 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv36 = sext i16 %17 to i32
  %cmp37 = icmp sgt i32 %call35, %conv36
  %conv38 = zext i1 %cmp37 to i32
  %mul39 = mul nsw i32 %conv38, -1
  %18 = load i8, i8* %mask, align 1, !tbaa !8
  %conv40 = sext i8 %18 to i32
  %or41 = or i32 %conv40, %mul39
  %conv42 = trunc i32 %or41 to i8
  store i8 %conv42, i8* %mask, align 1, !tbaa !8
  %19 = load i8, i8* %mask, align 1, !tbaa !8
  %conv43 = sext i8 %19 to i32
  %neg = xor i32 %conv43, -1
  %conv44 = trunc i32 %neg to i8
  %20 = bitcast i16* %thresh16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %20) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  ret i8 %conv44
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_filter6(i8 signext %mask, i8 zeroext %thresh, i8 signext %flat, i16* %op2, i16* %op1, i16* %op0, i16* %oq0, i16* %oq1, i16* %oq2, i32 %bd) #2 {
entry:
  %mask.addr = alloca i8, align 1
  %thresh.addr = alloca i8, align 1
  %flat.addr = alloca i8, align 1
  %op2.addr = alloca i16*, align 4
  %op1.addr = alloca i16*, align 4
  %op0.addr = alloca i16*, align 4
  %oq0.addr = alloca i16*, align 4
  %oq1.addr = alloca i16*, align 4
  %oq2.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %p2 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %q2 = alloca i16, align 2
  store i8 %mask, i8* %mask.addr, align 1, !tbaa !8
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8 %flat, i8* %flat.addr, align 1, !tbaa !8
  store i16* %op2, i16** %op2.addr, align 4, !tbaa !2
  store i16* %op1, i16** %op1.addr, align 4, !tbaa !2
  store i16* %op0, i16** %op0.addr, align 4, !tbaa !2
  store i16* %oq0, i16** %oq0.addr, align 4, !tbaa !2
  store i16* %oq1, i16** %oq1.addr, align 4, !tbaa !2
  store i16* %oq2, i16** %oq2.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i8, i8* %flat.addr, align 1, !tbaa !8
  %conv = sext i8 %0 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %conv1 = sext i8 %1 to i32
  %tobool2 = icmp ne i32 %conv1, 0
  br i1 %tobool2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %2) #4
  %3 = load i16*, i16** %op2.addr, align 4, !tbaa !2
  %4 = load i16, i16* %3, align 2, !tbaa !9
  store i16 %4, i16* %p2, align 2, !tbaa !9
  %5 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %5) #4
  %6 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  %7 = load i16, i16* %6, align 2, !tbaa !9
  store i16 %7, i16* %p1, align 2, !tbaa !9
  %8 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %8) #4
  %9 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  %10 = load i16, i16* %9, align 2, !tbaa !9
  store i16 %10, i16* %p0, align 2, !tbaa !9
  %11 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %11) #4
  %12 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  %13 = load i16, i16* %12, align 2, !tbaa !9
  store i16 %13, i16* %q0, align 2, !tbaa !9
  %14 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %14) #4
  %15 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  %16 = load i16, i16* %15, align 2, !tbaa !9
  store i16 %16, i16* %q1, align 2, !tbaa !9
  %17 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %17) #4
  %18 = load i16*, i16** %oq2.addr, align 4, !tbaa !2
  %19 = load i16, i16* %18, align 2, !tbaa !9
  store i16 %19, i16* %q2, align 2, !tbaa !9
  %20 = load i16, i16* %p2, align 2, !tbaa !9
  %conv3 = zext i16 %20 to i32
  %mul = mul nsw i32 %conv3, 3
  %21 = load i16, i16* %p1, align 2, !tbaa !9
  %conv4 = zext i16 %21 to i32
  %mul5 = mul nsw i32 %conv4, 2
  %add = add nsw i32 %mul, %mul5
  %22 = load i16, i16* %p0, align 2, !tbaa !9
  %conv6 = zext i16 %22 to i32
  %mul7 = mul nsw i32 %conv6, 2
  %add8 = add nsw i32 %add, %mul7
  %23 = load i16, i16* %q0, align 2, !tbaa !9
  %conv9 = zext i16 %23 to i32
  %add10 = add nsw i32 %add8, %conv9
  %add11 = add nsw i32 %add10, 4
  %shr = ashr i32 %add11, 3
  %conv12 = trunc i32 %shr to i16
  %24 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  store i16 %conv12, i16* %24, align 2, !tbaa !9
  %25 = load i16, i16* %p2, align 2, !tbaa !9
  %conv13 = zext i16 %25 to i32
  %26 = load i16, i16* %p1, align 2, !tbaa !9
  %conv14 = zext i16 %26 to i32
  %mul15 = mul nsw i32 %conv14, 2
  %add16 = add nsw i32 %conv13, %mul15
  %27 = load i16, i16* %p0, align 2, !tbaa !9
  %conv17 = zext i16 %27 to i32
  %mul18 = mul nsw i32 %conv17, 2
  %add19 = add nsw i32 %add16, %mul18
  %28 = load i16, i16* %q0, align 2, !tbaa !9
  %conv20 = zext i16 %28 to i32
  %mul21 = mul nsw i32 %conv20, 2
  %add22 = add nsw i32 %add19, %mul21
  %29 = load i16, i16* %q1, align 2, !tbaa !9
  %conv23 = zext i16 %29 to i32
  %add24 = add nsw i32 %add22, %conv23
  %add25 = add nsw i32 %add24, 4
  %shr26 = ashr i32 %add25, 3
  %conv27 = trunc i32 %shr26 to i16
  %30 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  store i16 %conv27, i16* %30, align 2, !tbaa !9
  %31 = load i16, i16* %p1, align 2, !tbaa !9
  %conv28 = zext i16 %31 to i32
  %32 = load i16, i16* %p0, align 2, !tbaa !9
  %conv29 = zext i16 %32 to i32
  %mul30 = mul nsw i32 %conv29, 2
  %add31 = add nsw i32 %conv28, %mul30
  %33 = load i16, i16* %q0, align 2, !tbaa !9
  %conv32 = zext i16 %33 to i32
  %mul33 = mul nsw i32 %conv32, 2
  %add34 = add nsw i32 %add31, %mul33
  %34 = load i16, i16* %q1, align 2, !tbaa !9
  %conv35 = zext i16 %34 to i32
  %mul36 = mul nsw i32 %conv35, 2
  %add37 = add nsw i32 %add34, %mul36
  %35 = load i16, i16* %q2, align 2, !tbaa !9
  %conv38 = zext i16 %35 to i32
  %add39 = add nsw i32 %add37, %conv38
  %add40 = add nsw i32 %add39, 4
  %shr41 = ashr i32 %add40, 3
  %conv42 = trunc i32 %shr41 to i16
  %36 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  store i16 %conv42, i16* %36, align 2, !tbaa !9
  %37 = load i16, i16* %p0, align 2, !tbaa !9
  %conv43 = zext i16 %37 to i32
  %38 = load i16, i16* %q0, align 2, !tbaa !9
  %conv44 = zext i16 %38 to i32
  %mul45 = mul nsw i32 %conv44, 2
  %add46 = add nsw i32 %conv43, %mul45
  %39 = load i16, i16* %q1, align 2, !tbaa !9
  %conv47 = zext i16 %39 to i32
  %mul48 = mul nsw i32 %conv47, 2
  %add49 = add nsw i32 %add46, %mul48
  %40 = load i16, i16* %q2, align 2, !tbaa !9
  %conv50 = zext i16 %40 to i32
  %mul51 = mul nsw i32 %conv50, 3
  %add52 = add nsw i32 %add49, %mul51
  %add53 = add nsw i32 %add52, 4
  %shr54 = ashr i32 %add53, 3
  %conv55 = trunc i32 %shr54 to i16
  %41 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  store i16 %conv55, i16* %41, align 2, !tbaa !9
  %42 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %42) #4
  %43 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %43) #4
  %44 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %44) #4
  %45 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %45) #4
  %46 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %46) #4
  %47 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %47) #4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %entry
  %48 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %49 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %50 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  %51 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  %52 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  %53 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  %54 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter4(i8 signext %48, i8 zeroext %49, i16* %50, i16* %51, i16* %52, i16* %53, i32 %54)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_horizontal_6_dual_c(i16* %s, i32 %p, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_horizontal_6_c(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 %5)
  %6 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %6, i32 4
  %7 = load i32, i32* %p.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  %11 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_horizontal_6_c(i16* %add.ptr, i32 %7, i8* %8, i8* %9, i8* %10, i32 %11)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_horizontal_8_dual_c(i16* %s, i32 %p, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_horizontal_8_c(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 %5)
  %6 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %6, i32 4
  %7 = load i32, i32* %p.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  %11 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_horizontal_8_c(i16* %add.ptr, i32 %7, i8* %8, i8* %9, i8* %10, i32 %11)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_vertical_6_c(i16* %s, i32 %pitch, i8* %blimit, i8* %limit, i8* %thresh, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p2 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %q2 = alloca i16, align 2
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  %5 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 -3
  %6 = load i16, i16* %arrayidx, align 2, !tbaa !9
  store i16 %6, i16* %p2, align 2, !tbaa !9
  %7 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %7) #4
  %8 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i16, i16* %8, i32 -2
  %9 = load i16, i16* %arrayidx1, align 2, !tbaa !9
  store i16 %9, i16* %p1, align 2, !tbaa !9
  %10 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %10) #4
  %11 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %11, i32 -1
  %12 = load i16, i16* %arrayidx2, align 2, !tbaa !9
  store i16 %12, i16* %p0, align 2, !tbaa !9
  %13 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #4
  %14 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i16, i16* %14, i32 0
  %15 = load i16, i16* %arrayidx3, align 2, !tbaa !9
  store i16 %15, i16* %q0, align 2, !tbaa !9
  %16 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #4
  %17 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i16, i16* %17, i32 1
  %18 = load i16, i16* %arrayidx4, align 2, !tbaa !9
  store i16 %18, i16* %q1, align 2, !tbaa !9
  %19 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %19) #4
  %20 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i16, i16* %20, i32 2
  %21 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  store i16 %21, i16* %q2, align 2, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %22 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %23 = load i8, i8* %22, align 1, !tbaa !8
  %24 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %25 = load i8, i8* %24, align 1, !tbaa !8
  %26 = load i16, i16* %p2, align 2, !tbaa !9
  %27 = load i16, i16* %p1, align 2, !tbaa !9
  %28 = load i16, i16* %p0, align 2, !tbaa !9
  %29 = load i16, i16* %q0, align 2, !tbaa !9
  %30 = load i16, i16* %q1, align 2, !tbaa !9
  %31 = load i16, i16* %q2, align 2, !tbaa !9
  %32 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call signext i8 @highbd_filter_mask3_chroma(i8 zeroext %23, i8 zeroext %25, i16 zeroext %26, i16 zeroext %27, i16 zeroext %28, i16 zeroext %29, i16 zeroext %30, i16 zeroext %31, i32 %32)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %33 = load i16, i16* %p2, align 2, !tbaa !9
  %34 = load i16, i16* %p1, align 2, !tbaa !9
  %35 = load i16, i16* %p0, align 2, !tbaa !9
  %36 = load i16, i16* %q0, align 2, !tbaa !9
  %37 = load i16, i16* %q1, align 2, !tbaa !9
  %38 = load i16, i16* %q2, align 2, !tbaa !9
  %39 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call6 = call signext i8 @highbd_flat_mask3_chroma(i8 zeroext 1, i16 zeroext %33, i16 zeroext %34, i16 zeroext %35, i16 zeroext %36, i16 zeroext %37, i16 zeroext %38, i32 %39)
  store i8 %call6, i8* %flat, align 1, !tbaa !8
  %40 = load i8, i8* %mask, align 1, !tbaa !8
  %41 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %42 = load i8, i8* %41, align 1, !tbaa !8
  %43 = load i8, i8* %flat, align 1, !tbaa !8
  %44 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %44, i32 -3
  %45 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr7 = getelementptr inbounds i16, i16* %45, i32 -2
  %46 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds i16, i16* %46, i32 -1
  %47 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %48 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i16, i16* %48, i32 1
  %49 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i16, i16* %49, i32 2
  %50 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter6(i8 signext %40, i8 zeroext %42, i8 signext %43, i16* %add.ptr, i16* %add.ptr7, i16* %add.ptr8, i16* %47, i16* %add.ptr9, i16* %add.ptr10, i32 %50)
  %51 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %52 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i16, i16* %52, i32 %51
  store i16* %add.ptr11, i16** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  %53 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %53) #4
  %54 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %54) #4
  %55 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %55) #4
  %56 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %56) #4
  %57 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %57) #4
  %58 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %58) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %59, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %60 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  %61 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_vertical_6_dual_c(i16* %s, i32 %pitch, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_vertical_6_c(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 %5)
  %6 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %7 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 4, %7
  %add.ptr = getelementptr inbounds i16, i16* %6, i32 %mul
  %8 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %11 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  %12 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_vertical_6_c(i16* %add.ptr, i32 %8, i8* %9, i8* %10, i8* %11, i32 %12)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_vertical_8_c(i16* %s, i32 %pitch, i8* %blimit, i8* %limit, i8* %thresh, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %count = alloca i32, align 4
  %p3 = alloca i16, align 2
  %p2 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %q2 = alloca i16, align 2
  %q3 = alloca i16, align 2
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %count, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %count, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  %5 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 -4
  %6 = load i16, i16* %arrayidx, align 2, !tbaa !9
  store i16 %6, i16* %p3, align 2, !tbaa !9
  %7 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %7) #4
  %8 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i16, i16* %8, i32 -3
  %9 = load i16, i16* %arrayidx1, align 2, !tbaa !9
  store i16 %9, i16* %p2, align 2, !tbaa !9
  %10 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %10) #4
  %11 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %11, i32 -2
  %12 = load i16, i16* %arrayidx2, align 2, !tbaa !9
  store i16 %12, i16* %p1, align 2, !tbaa !9
  %13 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #4
  %14 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i16, i16* %14, i32 -1
  %15 = load i16, i16* %arrayidx3, align 2, !tbaa !9
  store i16 %15, i16* %p0, align 2, !tbaa !9
  %16 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #4
  %17 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i16, i16* %17, i32 0
  %18 = load i16, i16* %arrayidx4, align 2, !tbaa !9
  store i16 %18, i16* %q0, align 2, !tbaa !9
  %19 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %19) #4
  %20 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i16, i16* %20, i32 1
  %21 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  store i16 %21, i16* %q1, align 2, !tbaa !9
  %22 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %22) #4
  %23 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %23, i32 2
  %24 = load i16, i16* %arrayidx6, align 2, !tbaa !9
  store i16 %24, i16* %q2, align 2, !tbaa !9
  %25 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %25) #4
  %26 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i16, i16* %26, i32 3
  %27 = load i16, i16* %arrayidx7, align 2, !tbaa !9
  store i16 %27, i16* %q3, align 2, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %28 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %29 = load i8, i8* %28, align 1, !tbaa !8
  %30 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !8
  %32 = load i16, i16* %p3, align 2, !tbaa !9
  %33 = load i16, i16* %p2, align 2, !tbaa !9
  %34 = load i16, i16* %p1, align 2, !tbaa !9
  %35 = load i16, i16* %p0, align 2, !tbaa !9
  %36 = load i16, i16* %q0, align 2, !tbaa !9
  %37 = load i16, i16* %q1, align 2, !tbaa !9
  %38 = load i16, i16* %q2, align 2, !tbaa !9
  %39 = load i16, i16* %q3, align 2, !tbaa !9
  %40 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call signext i8 @highbd_filter_mask(i8 zeroext %29, i8 zeroext %31, i16 zeroext %32, i16 zeroext %33, i16 zeroext %34, i16 zeroext %35, i16 zeroext %36, i16 zeroext %37, i16 zeroext %38, i16 zeroext %39, i32 %40)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %41 = load i16, i16* %p3, align 2, !tbaa !9
  %42 = load i16, i16* %p2, align 2, !tbaa !9
  %43 = load i16, i16* %p1, align 2, !tbaa !9
  %44 = load i16, i16* %p0, align 2, !tbaa !9
  %45 = load i16, i16* %q0, align 2, !tbaa !9
  %46 = load i16, i16* %q1, align 2, !tbaa !9
  %47 = load i16, i16* %q2, align 2, !tbaa !9
  %48 = load i16, i16* %q3, align 2, !tbaa !9
  %49 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call8 = call signext i8 @highbd_flat_mask4(i8 zeroext 1, i16 zeroext %41, i16 zeroext %42, i16 zeroext %43, i16 zeroext %44, i16 zeroext %45, i16 zeroext %46, i16 zeroext %47, i16 zeroext %48, i32 %49)
  store i8 %call8, i8* %flat, align 1, !tbaa !8
  %50 = load i8, i8* %mask, align 1, !tbaa !8
  %51 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !8
  %53 = load i8, i8* %flat, align 1, !tbaa !8
  %54 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %54, i32 -4
  %55 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i16, i16* %55, i32 -3
  %56 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i16, i16* %56, i32 -2
  %57 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i16, i16* %57, i32 -1
  %58 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %59 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr12 = getelementptr inbounds i16, i16* %59, i32 1
  %60 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr13 = getelementptr inbounds i16, i16* %60, i32 2
  %61 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr14 = getelementptr inbounds i16, i16* %61, i32 3
  %62 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter8(i8 signext %50, i8 zeroext %52, i8 signext %53, i16* %add.ptr, i16* %add.ptr9, i16* %add.ptr10, i16* %add.ptr11, i16* %58, i16* %add.ptr12, i16* %add.ptr13, i16* %add.ptr14, i32 %62)
  %63 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %64 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr15 = getelementptr inbounds i16, i16* %64, i32 %63
  store i16* %add.ptr15, i16** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  %65 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %65) #4
  %66 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %66) #4
  %67 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %67) #4
  %68 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %68) #4
  %69 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %69) #4
  %70 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %70) #4
  %71 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %71) #4
  %72 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %72) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %73 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %73, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %74 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  %75 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_vertical_8_dual_c(i16* %s, i32 %pitch, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_vertical_8_c(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 %5)
  %6 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %7 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 4, %7
  %add.ptr = getelementptr inbounds i16, i16* %6, i32 %mul
  %8 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %11 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  %12 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_lpf_vertical_8_c(i16* %add.ptr, i32 %8, i8* %9, i8* %10, i8* %11, i32 %12)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_horizontal_14_c(i16* %s, i32 %pitch, i8* %blimit, i8* %limit, i8* %thresh, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_mb_lpf_horizontal_edge_w(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 1, i32 %5)
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_mb_lpf_horizontal_edge_w(i16* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh, i32 %count, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %count.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %step = alloca i32, align 4
  %p3 = alloca i16, align 2
  %p2 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %q2 = alloca i16, align 2
  %q3 = alloca i16, align 2
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  %flat2 = alloca i8, align 1
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %count, i32* %count.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 4, i32* %step, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %step, align 4, !tbaa !6
  %4 = load i32, i32* %count.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %3, %4
  %cmp = icmp slt i32 %2, %mul
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %5) #4
  %6 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %7 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 -4, %7
  %arrayidx = getelementptr inbounds i16, i16* %6, i32 %mul1
  %8 = load i16, i16* %arrayidx, align 2, !tbaa !9
  store i16 %8, i16* %p3, align 2, !tbaa !9
  %9 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %9) #4
  %10 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %11 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 -3, %11
  %arrayidx3 = getelementptr inbounds i16, i16* %10, i32 %mul2
  %12 = load i16, i16* %arrayidx3, align 2, !tbaa !9
  store i16 %12, i16* %p2, align 2, !tbaa !9
  %13 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #4
  %14 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %15 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 -2, %15
  %arrayidx5 = getelementptr inbounds i16, i16* %14, i32 %mul4
  %16 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  store i16 %16, i16* %p1, align 2, !tbaa !9
  %17 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %17) #4
  %18 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %19 = load i32, i32* %p.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %19
  %arrayidx6 = getelementptr inbounds i16, i16* %18, i32 %sub
  %20 = load i16, i16* %arrayidx6, align 2, !tbaa !9
  store i16 %20, i16* %p0, align 2, !tbaa !9
  %21 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %21) #4
  %22 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %23 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 0, %23
  %arrayidx8 = getelementptr inbounds i16, i16* %22, i32 %mul7
  %24 = load i16, i16* %arrayidx8, align 2, !tbaa !9
  store i16 %24, i16* %q0, align 2, !tbaa !9
  %25 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %25) #4
  %26 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %27 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul9 = mul nsw i32 1, %27
  %arrayidx10 = getelementptr inbounds i16, i16* %26, i32 %mul9
  %28 = load i16, i16* %arrayidx10, align 2, !tbaa !9
  store i16 %28, i16* %q1, align 2, !tbaa !9
  %29 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %29) #4
  %30 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %31 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 2, %31
  %arrayidx12 = getelementptr inbounds i16, i16* %30, i32 %mul11
  %32 = load i16, i16* %arrayidx12, align 2, !tbaa !9
  store i16 %32, i16* %q2, align 2, !tbaa !9
  %33 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %33) #4
  %34 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %35 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 3, %35
  %arrayidx14 = getelementptr inbounds i16, i16* %34, i32 %mul13
  %36 = load i16, i16* %arrayidx14, align 2, !tbaa !9
  store i16 %36, i16* %q3, align 2, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %37 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %38 = load i8, i8* %37, align 1, !tbaa !8
  %39 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %40 = load i8, i8* %39, align 1, !tbaa !8
  %41 = load i16, i16* %p3, align 2, !tbaa !9
  %42 = load i16, i16* %p2, align 2, !tbaa !9
  %43 = load i16, i16* %p1, align 2, !tbaa !9
  %44 = load i16, i16* %p0, align 2, !tbaa !9
  %45 = load i16, i16* %q0, align 2, !tbaa !9
  %46 = load i16, i16* %q1, align 2, !tbaa !9
  %47 = load i16, i16* %q2, align 2, !tbaa !9
  %48 = load i16, i16* %q3, align 2, !tbaa !9
  %49 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call signext i8 @highbd_filter_mask(i8 zeroext %38, i8 zeroext %40, i16 zeroext %41, i16 zeroext %42, i16 zeroext %43, i16 zeroext %44, i16 zeroext %45, i16 zeroext %46, i16 zeroext %47, i16 zeroext %48, i32 %49)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %50 = load i16, i16* %p3, align 2, !tbaa !9
  %51 = load i16, i16* %p2, align 2, !tbaa !9
  %52 = load i16, i16* %p1, align 2, !tbaa !9
  %53 = load i16, i16* %p0, align 2, !tbaa !9
  %54 = load i16, i16* %q0, align 2, !tbaa !9
  %55 = load i16, i16* %q1, align 2, !tbaa !9
  %56 = load i16, i16* %q2, align 2, !tbaa !9
  %57 = load i16, i16* %q3, align 2, !tbaa !9
  %58 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call15 = call signext i8 @highbd_flat_mask4(i8 zeroext 1, i16 zeroext %50, i16 zeroext %51, i16 zeroext %52, i16 zeroext %53, i16 zeroext %54, i16 zeroext %55, i16 zeroext %56, i16 zeroext %57, i32 %58)
  store i8 %call15, i8* %flat, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat2) #4
  %59 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %60 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul16 = mul nsw i32 -7, %60
  %arrayidx17 = getelementptr inbounds i16, i16* %59, i32 %mul16
  %61 = load i16, i16* %arrayidx17, align 2, !tbaa !9
  %62 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %63 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 -6, %63
  %arrayidx19 = getelementptr inbounds i16, i16* %62, i32 %mul18
  %64 = load i16, i16* %arrayidx19, align 2, !tbaa !9
  %65 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %66 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 -5, %66
  %arrayidx21 = getelementptr inbounds i16, i16* %65, i32 %mul20
  %67 = load i16, i16* %arrayidx21, align 2, !tbaa !9
  %68 = load i16, i16* %p0, align 2, !tbaa !9
  %69 = load i16, i16* %q0, align 2, !tbaa !9
  %70 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %71 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul22 = mul nsw i32 4, %71
  %arrayidx23 = getelementptr inbounds i16, i16* %70, i32 %mul22
  %72 = load i16, i16* %arrayidx23, align 2, !tbaa !9
  %73 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %74 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul24 = mul nsw i32 5, %74
  %arrayidx25 = getelementptr inbounds i16, i16* %73, i32 %mul24
  %75 = load i16, i16* %arrayidx25, align 2, !tbaa !9
  %76 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %77 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 6, %77
  %arrayidx27 = getelementptr inbounds i16, i16* %76, i32 %mul26
  %78 = load i16, i16* %arrayidx27, align 2, !tbaa !9
  %79 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call28 = call signext i8 @highbd_flat_mask4(i8 zeroext 1, i16 zeroext %61, i16 zeroext %64, i16 zeroext %67, i16 zeroext %68, i16 zeroext %69, i16 zeroext %72, i16 zeroext %75, i16 zeroext %78, i32 %79)
  store i8 %call28, i8* %flat2, align 1, !tbaa !8
  %80 = load i8, i8* %mask, align 1, !tbaa !8
  %81 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %82 = load i8, i8* %81, align 1, !tbaa !8
  %83 = load i8, i8* %flat, align 1, !tbaa !8
  %84 = load i8, i8* %flat2, align 1, !tbaa !8
  %85 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %86 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul29 = mul nsw i32 7, %86
  %idx.neg = sub i32 0, %mul29
  %add.ptr = getelementptr inbounds i16, i16* %85, i32 %idx.neg
  %87 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %88 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul30 = mul nsw i32 6, %88
  %idx.neg31 = sub i32 0, %mul30
  %add.ptr32 = getelementptr inbounds i16, i16* %87, i32 %idx.neg31
  %89 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %90 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul33 = mul nsw i32 5, %90
  %idx.neg34 = sub i32 0, %mul33
  %add.ptr35 = getelementptr inbounds i16, i16* %89, i32 %idx.neg34
  %91 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %92 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul36 = mul nsw i32 4, %92
  %idx.neg37 = sub i32 0, %mul36
  %add.ptr38 = getelementptr inbounds i16, i16* %91, i32 %idx.neg37
  %93 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %94 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul39 = mul nsw i32 3, %94
  %idx.neg40 = sub i32 0, %mul39
  %add.ptr41 = getelementptr inbounds i16, i16* %93, i32 %idx.neg40
  %95 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %96 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul42 = mul nsw i32 2, %96
  %idx.neg43 = sub i32 0, %mul42
  %add.ptr44 = getelementptr inbounds i16, i16* %95, i32 %idx.neg43
  %97 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %98 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul45 = mul nsw i32 1, %98
  %idx.neg46 = sub i32 0, %mul45
  %add.ptr47 = getelementptr inbounds i16, i16* %97, i32 %idx.neg46
  %99 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %100 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %101 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul48 = mul nsw i32 1, %101
  %add.ptr49 = getelementptr inbounds i16, i16* %100, i32 %mul48
  %102 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %103 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul50 = mul nsw i32 2, %103
  %add.ptr51 = getelementptr inbounds i16, i16* %102, i32 %mul50
  %104 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %105 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul52 = mul nsw i32 3, %105
  %add.ptr53 = getelementptr inbounds i16, i16* %104, i32 %mul52
  %106 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %107 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul54 = mul nsw i32 4, %107
  %add.ptr55 = getelementptr inbounds i16, i16* %106, i32 %mul54
  %108 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %109 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul56 = mul nsw i32 5, %109
  %add.ptr57 = getelementptr inbounds i16, i16* %108, i32 %mul56
  %110 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %111 = load i32, i32* %p.addr, align 4, !tbaa !6
  %mul58 = mul nsw i32 6, %111
  %add.ptr59 = getelementptr inbounds i16, i16* %110, i32 %mul58
  %112 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter14(i8 signext %80, i8 zeroext %82, i8 signext %83, i8 signext %84, i16* %add.ptr, i16* %add.ptr32, i16* %add.ptr35, i16* %add.ptr38, i16* %add.ptr41, i16* %add.ptr44, i16* %add.ptr47, i16* %99, i16* %add.ptr49, i16* %add.ptr51, i16* %add.ptr53, i16* %add.ptr55, i16* %add.ptr57, i16* %add.ptr59, i32 %112)
  %113 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %113, i32 1
  store i16* %incdec.ptr, i16** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  %114 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %114) #4
  %115 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %115) #4
  %116 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %116) #4
  %117 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %117) #4
  %118 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %118) #4
  %119 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %119) #4
  %120 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %120) #4
  %121 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %121) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %122 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %122, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %123 = bitcast i32* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #4
  %124 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_horizontal_14_dual_c(i16* %s, i32 %p, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_mb_lpf_horizontal_edge_w(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 1, i32 %5)
  %6 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %6, i32 4
  %7 = load i32, i32* %p.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  %11 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_mb_lpf_horizontal_edge_w(i16* %add.ptr, i32 %7, i8* %8, i8* %9, i8* %10, i32 1, i32 %11)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_vertical_14_c(i16* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %p.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_mb_lpf_vertical_edge_w(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 4, i32 %5)
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_mb_lpf_vertical_edge_w(i16* %s, i32 %p, i8* %blimit, i8* %limit, i8* %thresh, i32 %count, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %p.addr = alloca i32, align 4
  %blimit.addr = alloca i8*, align 4
  %limit.addr = alloca i8*, align 4
  %thresh.addr = alloca i8*, align 4
  %count.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %p3 = alloca i16, align 2
  %p2 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %q2 = alloca i16, align 2
  %q3 = alloca i16, align 2
  %mask = alloca i8, align 1
  %flat = alloca i8, align 1
  %flat2 = alloca i8, align 1
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %blimit, i8** %blimit.addr, align 4, !tbaa !2
  store i8* %limit, i8** %limit.addr, align 4, !tbaa !2
  store i8* %thresh, i8** %thresh.addr, align 4, !tbaa !2
  store i32 %count, i32* %count.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %3) #4
  %4 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %4, i32 -4
  %5 = load i16, i16* %arrayidx, align 2, !tbaa !9
  store i16 %5, i16* %p3, align 2, !tbaa !9
  %6 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %6) #4
  %7 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i16, i16* %7, i32 -3
  %8 = load i16, i16* %arrayidx1, align 2, !tbaa !9
  store i16 %8, i16* %p2, align 2, !tbaa !9
  %9 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %9) #4
  %10 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %10, i32 -2
  %11 = load i16, i16* %arrayidx2, align 2, !tbaa !9
  store i16 %11, i16* %p1, align 2, !tbaa !9
  %12 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %12) #4
  %13 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i16, i16* %13, i32 -1
  %14 = load i16, i16* %arrayidx3, align 2, !tbaa !9
  store i16 %14, i16* %p0, align 2, !tbaa !9
  %15 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %15) #4
  %16 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i16, i16* %16, i32 0
  %17 = load i16, i16* %arrayidx4, align 2, !tbaa !9
  store i16 %17, i16* %q0, align 2, !tbaa !9
  %18 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %18) #4
  %19 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i16, i16* %19, i32 1
  %20 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  store i16 %20, i16* %q1, align 2, !tbaa !9
  %21 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %21) #4
  %22 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %22, i32 2
  %23 = load i16, i16* %arrayidx6, align 2, !tbaa !9
  store i16 %23, i16* %q2, align 2, !tbaa !9
  %24 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %24) #4
  %25 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i16, i16* %25, i32 3
  %26 = load i16, i16* %arrayidx7, align 2, !tbaa !9
  store i16 %26, i16* %q3, align 2, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #4
  %27 = load i8*, i8** %limit.addr, align 4, !tbaa !2
  %28 = load i8, i8* %27, align 1, !tbaa !8
  %29 = load i8*, i8** %blimit.addr, align 4, !tbaa !2
  %30 = load i8, i8* %29, align 1, !tbaa !8
  %31 = load i16, i16* %p3, align 2, !tbaa !9
  %32 = load i16, i16* %p2, align 2, !tbaa !9
  %33 = load i16, i16* %p1, align 2, !tbaa !9
  %34 = load i16, i16* %p0, align 2, !tbaa !9
  %35 = load i16, i16* %q0, align 2, !tbaa !9
  %36 = load i16, i16* %q1, align 2, !tbaa !9
  %37 = load i16, i16* %q2, align 2, !tbaa !9
  %38 = load i16, i16* %q3, align 2, !tbaa !9
  %39 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call signext i8 @highbd_filter_mask(i8 zeroext %28, i8 zeroext %30, i16 zeroext %31, i16 zeroext %32, i16 zeroext %33, i16 zeroext %34, i16 zeroext %35, i16 zeroext %36, i16 zeroext %37, i16 zeroext %38, i32 %39)
  store i8 %call, i8* %mask, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat) #4
  %40 = load i16, i16* %p3, align 2, !tbaa !9
  %41 = load i16, i16* %p2, align 2, !tbaa !9
  %42 = load i16, i16* %p1, align 2, !tbaa !9
  %43 = load i16, i16* %p0, align 2, !tbaa !9
  %44 = load i16, i16* %q0, align 2, !tbaa !9
  %45 = load i16, i16* %q1, align 2, !tbaa !9
  %46 = load i16, i16* %q2, align 2, !tbaa !9
  %47 = load i16, i16* %q3, align 2, !tbaa !9
  %48 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call8 = call signext i8 @highbd_flat_mask4(i8 zeroext 1, i16 zeroext %40, i16 zeroext %41, i16 zeroext %42, i16 zeroext %43, i16 zeroext %44, i16 zeroext %45, i16 zeroext %46, i16 zeroext %47, i32 %48)
  store i8 %call8, i8* %flat, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flat2) #4
  %49 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i16, i16* %49, i32 -7
  %50 = load i16, i16* %arrayidx9, align 2, !tbaa !9
  %51 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %51, i32 -6
  %52 = load i16, i16* %arrayidx10, align 2, !tbaa !9
  %53 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i16, i16* %53, i32 -5
  %54 = load i16, i16* %arrayidx11, align 2, !tbaa !9
  %55 = load i16, i16* %p0, align 2, !tbaa !9
  %56 = load i16, i16* %q0, align 2, !tbaa !9
  %57 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i16, i16* %57, i32 4
  %58 = load i16, i16* %arrayidx12, align 2, !tbaa !9
  %59 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i16, i16* %59, i32 5
  %60 = load i16, i16* %arrayidx13, align 2, !tbaa !9
  %61 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i16, i16* %61, i32 6
  %62 = load i16, i16* %arrayidx14, align 2, !tbaa !9
  %63 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call15 = call signext i8 @highbd_flat_mask4(i8 zeroext 1, i16 zeroext %50, i16 zeroext %52, i16 zeroext %54, i16 zeroext %55, i16 zeroext %56, i16 zeroext %58, i16 zeroext %60, i16 zeroext %62, i32 %63)
  store i8 %call15, i8* %flat2, align 1, !tbaa !8
  %64 = load i8, i8* %mask, align 1, !tbaa !8
  %65 = load i8*, i8** %thresh.addr, align 4, !tbaa !2
  %66 = load i8, i8* %65, align 1, !tbaa !8
  %67 = load i8, i8* %flat, align 1, !tbaa !8
  %68 = load i8, i8* %flat2, align 1, !tbaa !8
  %69 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %69, i32 -7
  %70 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr16 = getelementptr inbounds i16, i16* %70, i32 -6
  %71 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds i16, i16* %71, i32 -5
  %72 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr18 = getelementptr inbounds i16, i16* %72, i32 -4
  %73 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr19 = getelementptr inbounds i16, i16* %73, i32 -3
  %74 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr20 = getelementptr inbounds i16, i16* %74, i32 -2
  %75 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr21 = getelementptr inbounds i16, i16* %75, i32 -1
  %76 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %77 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i16, i16* %77, i32 1
  %78 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr23 = getelementptr inbounds i16, i16* %78, i32 2
  %79 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr24 = getelementptr inbounds i16, i16* %79, i32 3
  %80 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr25 = getelementptr inbounds i16, i16* %80, i32 4
  %81 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr26 = getelementptr inbounds i16, i16* %81, i32 5
  %82 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr27 = getelementptr inbounds i16, i16* %82, i32 6
  %83 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter14(i8 signext %64, i8 zeroext %66, i8 signext %67, i8 signext %68, i16* %add.ptr, i16* %add.ptr16, i16* %add.ptr17, i16* %add.ptr18, i16* %add.ptr19, i16* %add.ptr20, i16* %add.ptr21, i16* %76, i16* %add.ptr22, i16* %add.ptr23, i16* %add.ptr24, i16* %add.ptr25, i16* %add.ptr26, i16* %add.ptr27, i32 %83)
  %84 = load i32, i32* %p.addr, align 4, !tbaa !6
  %85 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds i16, i16* %85, i32 %84
  store i16* %add.ptr28, i16** %s.addr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flat) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #4
  %86 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %86) #4
  %87 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %87) #4
  %88 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %88) #4
  %89 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %89) #4
  %90 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %90) #4
  %91 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %91) #4
  %92 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %92) #4
  %93 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %93) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %94 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %94, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %95 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_lpf_vertical_14_dual_c(i16* %s, i32 %pitch, i8* %blimit0, i8* %limit0, i8* %thresh0, i8* %blimit1, i8* %limit1, i8* %thresh1, i32 %bd) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %pitch.addr = alloca i32, align 4
  %blimit0.addr = alloca i8*, align 4
  %limit0.addr = alloca i8*, align 4
  %thresh0.addr = alloca i8*, align 4
  %blimit1.addr = alloca i8*, align 4
  %limit1.addr = alloca i8*, align 4
  %thresh1.addr = alloca i8*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4, !tbaa !2
  store i32 %pitch, i32* %pitch.addr, align 4, !tbaa !6
  store i8* %blimit0, i8** %blimit0.addr, align 4, !tbaa !2
  store i8* %limit0, i8** %limit0.addr, align 4, !tbaa !2
  store i8* %thresh0, i8** %thresh0.addr, align 4, !tbaa !2
  store i8* %blimit1, i8** %blimit1.addr, align 4, !tbaa !2
  store i8* %limit1, i8** %limit1.addr, align 4, !tbaa !2
  store i8* %thresh1, i8** %thresh1.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %1 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %blimit0.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %limit0.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %thresh0.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_mb_lpf_vertical_edge_w(i16* %0, i32 %1, i8* %2, i8* %3, i8* %4, i32 4, i32 %5)
  %6 = load i16*, i16** %s.addr, align 4, !tbaa !2
  %7 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 4, %7
  %add.ptr = getelementptr inbounds i16, i16* %6, i32 %mul
  %8 = load i32, i32* %pitch.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %blimit1.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %limit1.addr, align 4, !tbaa !2
  %11 = load i8*, i8** %thresh1.addr, align 4, !tbaa !2
  %12 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_mb_lpf_vertical_edge_w(i16* %add.ptr, i32 %8, i8* %9, i8* %10, i8* %11, i32 4, i32 %12)
  ret void
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #3

; Function Attrs: inlinehint nounwind
define internal signext i8 @hev_mask(i8 zeroext %thresh, i8 zeroext %p1, i8 zeroext %p0, i8 zeroext %q0, i8 zeroext %q1) #2 {
entry:
  %thresh.addr = alloca i8, align 1
  %p1.addr = alloca i8, align 1
  %p0.addr = alloca i8, align 1
  %q0.addr = alloca i8, align 1
  %q1.addr = alloca i8, align 1
  %hev = alloca i8, align 1
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8 %p1, i8* %p1.addr, align 1, !tbaa !8
  store i8 %p0, i8* %p0.addr, align 1, !tbaa !8
  store i8 %q0, i8* %q0.addr, align 1, !tbaa !8
  store i8 %q1, i8* %q1.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hev) #4
  store i8 0, i8* %hev, align 1, !tbaa !8
  %0 = load i8, i8* %p1.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %1 = load i8, i8* %p0.addr, align 1, !tbaa !8
  %conv1 = zext i8 %1 to i32
  %sub = sub nsw i32 %conv, %conv1
  %call = call i32 @abs(i32 %sub) #5
  %2 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv2 = zext i8 %2 to i32
  %cmp = icmp sgt i32 %call, %conv2
  %conv3 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv3, -1
  %3 = load i8, i8* %hev, align 1, !tbaa !8
  %conv4 = sext i8 %3 to i32
  %or = or i32 %conv4, %mul
  %conv5 = trunc i32 %or to i8
  store i8 %conv5, i8* %hev, align 1, !tbaa !8
  %4 = load i8, i8* %q1.addr, align 1, !tbaa !8
  %conv6 = zext i8 %4 to i32
  %5 = load i8, i8* %q0.addr, align 1, !tbaa !8
  %conv7 = zext i8 %5 to i32
  %sub8 = sub nsw i32 %conv6, %conv7
  %call9 = call i32 @abs(i32 %sub8) #5
  %6 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv10 = zext i8 %6 to i32
  %cmp11 = icmp sgt i32 %call9, %conv10
  %conv12 = zext i1 %cmp11 to i32
  %mul13 = mul nsw i32 %conv12, -1
  %7 = load i8, i8* %hev, align 1, !tbaa !8
  %conv14 = sext i8 %7 to i32
  %or15 = or i32 %conv14, %mul13
  %conv16 = trunc i32 %or15 to i8
  store i8 %conv16, i8* %hev, align 1, !tbaa !8
  %8 = load i8, i8* %hev, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hev) #4
  ret i8 %8
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @signed_char_clamp(i32 %t) #2 {
entry:
  %t.addr = alloca i32, align 4
  store i32 %t, i32* %t.addr, align 4, !tbaa !6
  %0 = load i32, i32* %t.addr, align 4, !tbaa !6
  %call = call i32 @clamp(i32 %0, i32 -128, i32 127)
  %conv = trunc i32 %call to i8
  ret i8 %conv
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal void @filter14(i8 signext %mask, i8 zeroext %thresh, i8 signext %flat, i8 signext %flat2, i8* %op6, i8* %op5, i8* %op4, i8* %op3, i8* %op2, i8* %op1, i8* %op0, i8* %oq0, i8* %oq1, i8* %oq2, i8* %oq3, i8* %oq4, i8* %oq5, i8* %oq6) #2 {
entry:
  %mask.addr = alloca i8, align 1
  %thresh.addr = alloca i8, align 1
  %flat.addr = alloca i8, align 1
  %flat2.addr = alloca i8, align 1
  %op6.addr = alloca i8*, align 4
  %op5.addr = alloca i8*, align 4
  %op4.addr = alloca i8*, align 4
  %op3.addr = alloca i8*, align 4
  %op2.addr = alloca i8*, align 4
  %op1.addr = alloca i8*, align 4
  %op0.addr = alloca i8*, align 4
  %oq0.addr = alloca i8*, align 4
  %oq1.addr = alloca i8*, align 4
  %oq2.addr = alloca i8*, align 4
  %oq3.addr = alloca i8*, align 4
  %oq4.addr = alloca i8*, align 4
  %oq5.addr = alloca i8*, align 4
  %oq6.addr = alloca i8*, align 4
  %p6 = alloca i8, align 1
  %p5 = alloca i8, align 1
  %p4 = alloca i8, align 1
  %p3 = alloca i8, align 1
  %p2 = alloca i8, align 1
  %p1 = alloca i8, align 1
  %p0 = alloca i8, align 1
  %q0 = alloca i8, align 1
  %q1 = alloca i8, align 1
  %q2 = alloca i8, align 1
  %q3 = alloca i8, align 1
  %q4 = alloca i8, align 1
  %q5 = alloca i8, align 1
  %q6 = alloca i8, align 1
  store i8 %mask, i8* %mask.addr, align 1, !tbaa !8
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8 %flat, i8* %flat.addr, align 1, !tbaa !8
  store i8 %flat2, i8* %flat2.addr, align 1, !tbaa !8
  store i8* %op6, i8** %op6.addr, align 4, !tbaa !2
  store i8* %op5, i8** %op5.addr, align 4, !tbaa !2
  store i8* %op4, i8** %op4.addr, align 4, !tbaa !2
  store i8* %op3, i8** %op3.addr, align 4, !tbaa !2
  store i8* %op2, i8** %op2.addr, align 4, !tbaa !2
  store i8* %op1, i8** %op1.addr, align 4, !tbaa !2
  store i8* %op0, i8** %op0.addr, align 4, !tbaa !2
  store i8* %oq0, i8** %oq0.addr, align 4, !tbaa !2
  store i8* %oq1, i8** %oq1.addr, align 4, !tbaa !2
  store i8* %oq2, i8** %oq2.addr, align 4, !tbaa !2
  store i8* %oq3, i8** %oq3.addr, align 4, !tbaa !2
  store i8* %oq4, i8** %oq4.addr, align 4, !tbaa !2
  store i8* %oq5, i8** %oq5.addr, align 4, !tbaa !2
  store i8* %oq6, i8** %oq6.addr, align 4, !tbaa !2
  %0 = load i8, i8* %flat2.addr, align 1, !tbaa !8
  %conv = sext i8 %0 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %flat.addr, align 1, !tbaa !8
  %conv1 = sext i8 %1 to i32
  %tobool2 = icmp ne i32 %conv1, 0
  br i1 %tobool2, label %land.lhs.true3, label %if.else

land.lhs.true3:                                   ; preds = %land.lhs.true
  %2 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %conv4 = sext i8 %2 to i32
  %tobool5 = icmp ne i32 %conv4, 0
  br i1 %tobool5, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true3
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p6) #4
  %3 = load i8*, i8** %op6.addr, align 4, !tbaa !2
  %4 = load i8, i8* %3, align 1, !tbaa !8
  store i8 %4, i8* %p6, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p5) #4
  %5 = load i8*, i8** %op5.addr, align 4, !tbaa !2
  %6 = load i8, i8* %5, align 1, !tbaa !8
  store i8 %6, i8* %p5, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p4) #4
  %7 = load i8*, i8** %op4.addr, align 4, !tbaa !2
  %8 = load i8, i8* %7, align 1, !tbaa !8
  store i8 %8, i8* %p4, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p3) #4
  %9 = load i8*, i8** %op3.addr, align 4, !tbaa !2
  %10 = load i8, i8* %9, align 1, !tbaa !8
  store i8 %10, i8* %p3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p2) #4
  %11 = load i8*, i8** %op2.addr, align 4, !tbaa !2
  %12 = load i8, i8* %11, align 1, !tbaa !8
  store i8 %12, i8* %p2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p1) #4
  %13 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  %14 = load i8, i8* %13, align 1, !tbaa !8
  store i8 %14, i8* %p1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %p0) #4
  %15 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  %16 = load i8, i8* %15, align 1, !tbaa !8
  store i8 %16, i8* %p0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q0) #4
  %17 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  %18 = load i8, i8* %17, align 1, !tbaa !8
  store i8 %18, i8* %q0, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q1) #4
  %19 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  %20 = load i8, i8* %19, align 1, !tbaa !8
  store i8 %20, i8* %q1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q2) #4
  %21 = load i8*, i8** %oq2.addr, align 4, !tbaa !2
  %22 = load i8, i8* %21, align 1, !tbaa !8
  store i8 %22, i8* %q2, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q3) #4
  %23 = load i8*, i8** %oq3.addr, align 4, !tbaa !2
  %24 = load i8, i8* %23, align 1, !tbaa !8
  store i8 %24, i8* %q3, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q4) #4
  %25 = load i8*, i8** %oq4.addr, align 4, !tbaa !2
  %26 = load i8, i8* %25, align 1, !tbaa !8
  store i8 %26, i8* %q4, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q5) #4
  %27 = load i8*, i8** %oq5.addr, align 4, !tbaa !2
  %28 = load i8, i8* %27, align 1, !tbaa !8
  store i8 %28, i8* %q5, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %q6) #4
  %29 = load i8*, i8** %oq6.addr, align 4, !tbaa !2
  %30 = load i8, i8* %29, align 1, !tbaa !8
  store i8 %30, i8* %q6, align 1, !tbaa !8
  %31 = load i8, i8* %p6, align 1, !tbaa !8
  %conv6 = zext i8 %31 to i32
  %mul = mul nsw i32 %conv6, 7
  %32 = load i8, i8* %p5, align 1, !tbaa !8
  %conv7 = zext i8 %32 to i32
  %mul8 = mul nsw i32 %conv7, 2
  %add = add nsw i32 %mul, %mul8
  %33 = load i8, i8* %p4, align 1, !tbaa !8
  %conv9 = zext i8 %33 to i32
  %mul10 = mul nsw i32 %conv9, 2
  %add11 = add nsw i32 %add, %mul10
  %34 = load i8, i8* %p3, align 1, !tbaa !8
  %conv12 = zext i8 %34 to i32
  %add13 = add nsw i32 %add11, %conv12
  %35 = load i8, i8* %p2, align 1, !tbaa !8
  %conv14 = zext i8 %35 to i32
  %add15 = add nsw i32 %add13, %conv14
  %36 = load i8, i8* %p1, align 1, !tbaa !8
  %conv16 = zext i8 %36 to i32
  %add17 = add nsw i32 %add15, %conv16
  %37 = load i8, i8* %p0, align 1, !tbaa !8
  %conv18 = zext i8 %37 to i32
  %add19 = add nsw i32 %add17, %conv18
  %38 = load i8, i8* %q0, align 1, !tbaa !8
  %conv20 = zext i8 %38 to i32
  %add21 = add nsw i32 %add19, %conv20
  %add22 = add nsw i32 %add21, 8
  %shr = ashr i32 %add22, 4
  %conv23 = trunc i32 %shr to i8
  %39 = load i8*, i8** %op5.addr, align 4, !tbaa !2
  store i8 %conv23, i8* %39, align 1, !tbaa !8
  %40 = load i8, i8* %p6, align 1, !tbaa !8
  %conv24 = zext i8 %40 to i32
  %mul25 = mul nsw i32 %conv24, 5
  %41 = load i8, i8* %p5, align 1, !tbaa !8
  %conv26 = zext i8 %41 to i32
  %mul27 = mul nsw i32 %conv26, 2
  %add28 = add nsw i32 %mul25, %mul27
  %42 = load i8, i8* %p4, align 1, !tbaa !8
  %conv29 = zext i8 %42 to i32
  %mul30 = mul nsw i32 %conv29, 2
  %add31 = add nsw i32 %add28, %mul30
  %43 = load i8, i8* %p3, align 1, !tbaa !8
  %conv32 = zext i8 %43 to i32
  %mul33 = mul nsw i32 %conv32, 2
  %add34 = add nsw i32 %add31, %mul33
  %44 = load i8, i8* %p2, align 1, !tbaa !8
  %conv35 = zext i8 %44 to i32
  %add36 = add nsw i32 %add34, %conv35
  %45 = load i8, i8* %p1, align 1, !tbaa !8
  %conv37 = zext i8 %45 to i32
  %add38 = add nsw i32 %add36, %conv37
  %46 = load i8, i8* %p0, align 1, !tbaa !8
  %conv39 = zext i8 %46 to i32
  %add40 = add nsw i32 %add38, %conv39
  %47 = load i8, i8* %q0, align 1, !tbaa !8
  %conv41 = zext i8 %47 to i32
  %add42 = add nsw i32 %add40, %conv41
  %48 = load i8, i8* %q1, align 1, !tbaa !8
  %conv43 = zext i8 %48 to i32
  %add44 = add nsw i32 %add42, %conv43
  %add45 = add nsw i32 %add44, 8
  %shr46 = ashr i32 %add45, 4
  %conv47 = trunc i32 %shr46 to i8
  %49 = load i8*, i8** %op4.addr, align 4, !tbaa !2
  store i8 %conv47, i8* %49, align 1, !tbaa !8
  %50 = load i8, i8* %p6, align 1, !tbaa !8
  %conv48 = zext i8 %50 to i32
  %mul49 = mul nsw i32 %conv48, 4
  %51 = load i8, i8* %p5, align 1, !tbaa !8
  %conv50 = zext i8 %51 to i32
  %add51 = add nsw i32 %mul49, %conv50
  %52 = load i8, i8* %p4, align 1, !tbaa !8
  %conv52 = zext i8 %52 to i32
  %mul53 = mul nsw i32 %conv52, 2
  %add54 = add nsw i32 %add51, %mul53
  %53 = load i8, i8* %p3, align 1, !tbaa !8
  %conv55 = zext i8 %53 to i32
  %mul56 = mul nsw i32 %conv55, 2
  %add57 = add nsw i32 %add54, %mul56
  %54 = load i8, i8* %p2, align 1, !tbaa !8
  %conv58 = zext i8 %54 to i32
  %mul59 = mul nsw i32 %conv58, 2
  %add60 = add nsw i32 %add57, %mul59
  %55 = load i8, i8* %p1, align 1, !tbaa !8
  %conv61 = zext i8 %55 to i32
  %add62 = add nsw i32 %add60, %conv61
  %56 = load i8, i8* %p0, align 1, !tbaa !8
  %conv63 = zext i8 %56 to i32
  %add64 = add nsw i32 %add62, %conv63
  %57 = load i8, i8* %q0, align 1, !tbaa !8
  %conv65 = zext i8 %57 to i32
  %add66 = add nsw i32 %add64, %conv65
  %58 = load i8, i8* %q1, align 1, !tbaa !8
  %conv67 = zext i8 %58 to i32
  %add68 = add nsw i32 %add66, %conv67
  %59 = load i8, i8* %q2, align 1, !tbaa !8
  %conv69 = zext i8 %59 to i32
  %add70 = add nsw i32 %add68, %conv69
  %add71 = add nsw i32 %add70, 8
  %shr72 = ashr i32 %add71, 4
  %conv73 = trunc i32 %shr72 to i8
  %60 = load i8*, i8** %op3.addr, align 4, !tbaa !2
  store i8 %conv73, i8* %60, align 1, !tbaa !8
  %61 = load i8, i8* %p6, align 1, !tbaa !8
  %conv74 = zext i8 %61 to i32
  %mul75 = mul nsw i32 %conv74, 3
  %62 = load i8, i8* %p5, align 1, !tbaa !8
  %conv76 = zext i8 %62 to i32
  %add77 = add nsw i32 %mul75, %conv76
  %63 = load i8, i8* %p4, align 1, !tbaa !8
  %conv78 = zext i8 %63 to i32
  %add79 = add nsw i32 %add77, %conv78
  %64 = load i8, i8* %p3, align 1, !tbaa !8
  %conv80 = zext i8 %64 to i32
  %mul81 = mul nsw i32 %conv80, 2
  %add82 = add nsw i32 %add79, %mul81
  %65 = load i8, i8* %p2, align 1, !tbaa !8
  %conv83 = zext i8 %65 to i32
  %mul84 = mul nsw i32 %conv83, 2
  %add85 = add nsw i32 %add82, %mul84
  %66 = load i8, i8* %p1, align 1, !tbaa !8
  %conv86 = zext i8 %66 to i32
  %mul87 = mul nsw i32 %conv86, 2
  %add88 = add nsw i32 %add85, %mul87
  %67 = load i8, i8* %p0, align 1, !tbaa !8
  %conv89 = zext i8 %67 to i32
  %add90 = add nsw i32 %add88, %conv89
  %68 = load i8, i8* %q0, align 1, !tbaa !8
  %conv91 = zext i8 %68 to i32
  %add92 = add nsw i32 %add90, %conv91
  %69 = load i8, i8* %q1, align 1, !tbaa !8
  %conv93 = zext i8 %69 to i32
  %add94 = add nsw i32 %add92, %conv93
  %70 = load i8, i8* %q2, align 1, !tbaa !8
  %conv95 = zext i8 %70 to i32
  %add96 = add nsw i32 %add94, %conv95
  %71 = load i8, i8* %q3, align 1, !tbaa !8
  %conv97 = zext i8 %71 to i32
  %add98 = add nsw i32 %add96, %conv97
  %add99 = add nsw i32 %add98, 8
  %shr100 = ashr i32 %add99, 4
  %conv101 = trunc i32 %shr100 to i8
  %72 = load i8*, i8** %op2.addr, align 4, !tbaa !2
  store i8 %conv101, i8* %72, align 1, !tbaa !8
  %73 = load i8, i8* %p6, align 1, !tbaa !8
  %conv102 = zext i8 %73 to i32
  %mul103 = mul nsw i32 %conv102, 2
  %74 = load i8, i8* %p5, align 1, !tbaa !8
  %conv104 = zext i8 %74 to i32
  %add105 = add nsw i32 %mul103, %conv104
  %75 = load i8, i8* %p4, align 1, !tbaa !8
  %conv106 = zext i8 %75 to i32
  %add107 = add nsw i32 %add105, %conv106
  %76 = load i8, i8* %p3, align 1, !tbaa !8
  %conv108 = zext i8 %76 to i32
  %add109 = add nsw i32 %add107, %conv108
  %77 = load i8, i8* %p2, align 1, !tbaa !8
  %conv110 = zext i8 %77 to i32
  %mul111 = mul nsw i32 %conv110, 2
  %add112 = add nsw i32 %add109, %mul111
  %78 = load i8, i8* %p1, align 1, !tbaa !8
  %conv113 = zext i8 %78 to i32
  %mul114 = mul nsw i32 %conv113, 2
  %add115 = add nsw i32 %add112, %mul114
  %79 = load i8, i8* %p0, align 1, !tbaa !8
  %conv116 = zext i8 %79 to i32
  %mul117 = mul nsw i32 %conv116, 2
  %add118 = add nsw i32 %add115, %mul117
  %80 = load i8, i8* %q0, align 1, !tbaa !8
  %conv119 = zext i8 %80 to i32
  %add120 = add nsw i32 %add118, %conv119
  %81 = load i8, i8* %q1, align 1, !tbaa !8
  %conv121 = zext i8 %81 to i32
  %add122 = add nsw i32 %add120, %conv121
  %82 = load i8, i8* %q2, align 1, !tbaa !8
  %conv123 = zext i8 %82 to i32
  %add124 = add nsw i32 %add122, %conv123
  %83 = load i8, i8* %q3, align 1, !tbaa !8
  %conv125 = zext i8 %83 to i32
  %add126 = add nsw i32 %add124, %conv125
  %84 = load i8, i8* %q4, align 1, !tbaa !8
  %conv127 = zext i8 %84 to i32
  %add128 = add nsw i32 %add126, %conv127
  %add129 = add nsw i32 %add128, 8
  %shr130 = ashr i32 %add129, 4
  %conv131 = trunc i32 %shr130 to i8
  %85 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  store i8 %conv131, i8* %85, align 1, !tbaa !8
  %86 = load i8, i8* %p6, align 1, !tbaa !8
  %conv132 = zext i8 %86 to i32
  %87 = load i8, i8* %p5, align 1, !tbaa !8
  %conv133 = zext i8 %87 to i32
  %add134 = add nsw i32 %conv132, %conv133
  %88 = load i8, i8* %p4, align 1, !tbaa !8
  %conv135 = zext i8 %88 to i32
  %add136 = add nsw i32 %add134, %conv135
  %89 = load i8, i8* %p3, align 1, !tbaa !8
  %conv137 = zext i8 %89 to i32
  %add138 = add nsw i32 %add136, %conv137
  %90 = load i8, i8* %p2, align 1, !tbaa !8
  %conv139 = zext i8 %90 to i32
  %add140 = add nsw i32 %add138, %conv139
  %91 = load i8, i8* %p1, align 1, !tbaa !8
  %conv141 = zext i8 %91 to i32
  %mul142 = mul nsw i32 %conv141, 2
  %add143 = add nsw i32 %add140, %mul142
  %92 = load i8, i8* %p0, align 1, !tbaa !8
  %conv144 = zext i8 %92 to i32
  %mul145 = mul nsw i32 %conv144, 2
  %add146 = add nsw i32 %add143, %mul145
  %93 = load i8, i8* %q0, align 1, !tbaa !8
  %conv147 = zext i8 %93 to i32
  %mul148 = mul nsw i32 %conv147, 2
  %add149 = add nsw i32 %add146, %mul148
  %94 = load i8, i8* %q1, align 1, !tbaa !8
  %conv150 = zext i8 %94 to i32
  %add151 = add nsw i32 %add149, %conv150
  %95 = load i8, i8* %q2, align 1, !tbaa !8
  %conv152 = zext i8 %95 to i32
  %add153 = add nsw i32 %add151, %conv152
  %96 = load i8, i8* %q3, align 1, !tbaa !8
  %conv154 = zext i8 %96 to i32
  %add155 = add nsw i32 %add153, %conv154
  %97 = load i8, i8* %q4, align 1, !tbaa !8
  %conv156 = zext i8 %97 to i32
  %add157 = add nsw i32 %add155, %conv156
  %98 = load i8, i8* %q5, align 1, !tbaa !8
  %conv158 = zext i8 %98 to i32
  %add159 = add nsw i32 %add157, %conv158
  %add160 = add nsw i32 %add159, 8
  %shr161 = ashr i32 %add160, 4
  %conv162 = trunc i32 %shr161 to i8
  %99 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  store i8 %conv162, i8* %99, align 1, !tbaa !8
  %100 = load i8, i8* %p5, align 1, !tbaa !8
  %conv163 = zext i8 %100 to i32
  %101 = load i8, i8* %p4, align 1, !tbaa !8
  %conv164 = zext i8 %101 to i32
  %add165 = add nsw i32 %conv163, %conv164
  %102 = load i8, i8* %p3, align 1, !tbaa !8
  %conv166 = zext i8 %102 to i32
  %add167 = add nsw i32 %add165, %conv166
  %103 = load i8, i8* %p2, align 1, !tbaa !8
  %conv168 = zext i8 %103 to i32
  %add169 = add nsw i32 %add167, %conv168
  %104 = load i8, i8* %p1, align 1, !tbaa !8
  %conv170 = zext i8 %104 to i32
  %add171 = add nsw i32 %add169, %conv170
  %105 = load i8, i8* %p0, align 1, !tbaa !8
  %conv172 = zext i8 %105 to i32
  %mul173 = mul nsw i32 %conv172, 2
  %add174 = add nsw i32 %add171, %mul173
  %106 = load i8, i8* %q0, align 1, !tbaa !8
  %conv175 = zext i8 %106 to i32
  %mul176 = mul nsw i32 %conv175, 2
  %add177 = add nsw i32 %add174, %mul176
  %107 = load i8, i8* %q1, align 1, !tbaa !8
  %conv178 = zext i8 %107 to i32
  %mul179 = mul nsw i32 %conv178, 2
  %add180 = add nsw i32 %add177, %mul179
  %108 = load i8, i8* %q2, align 1, !tbaa !8
  %conv181 = zext i8 %108 to i32
  %add182 = add nsw i32 %add180, %conv181
  %109 = load i8, i8* %q3, align 1, !tbaa !8
  %conv183 = zext i8 %109 to i32
  %add184 = add nsw i32 %add182, %conv183
  %110 = load i8, i8* %q4, align 1, !tbaa !8
  %conv185 = zext i8 %110 to i32
  %add186 = add nsw i32 %add184, %conv185
  %111 = load i8, i8* %q5, align 1, !tbaa !8
  %conv187 = zext i8 %111 to i32
  %add188 = add nsw i32 %add186, %conv187
  %112 = load i8, i8* %q6, align 1, !tbaa !8
  %conv189 = zext i8 %112 to i32
  %add190 = add nsw i32 %add188, %conv189
  %add191 = add nsw i32 %add190, 8
  %shr192 = ashr i32 %add191, 4
  %conv193 = trunc i32 %shr192 to i8
  %113 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  store i8 %conv193, i8* %113, align 1, !tbaa !8
  %114 = load i8, i8* %p4, align 1, !tbaa !8
  %conv194 = zext i8 %114 to i32
  %115 = load i8, i8* %p3, align 1, !tbaa !8
  %conv195 = zext i8 %115 to i32
  %add196 = add nsw i32 %conv194, %conv195
  %116 = load i8, i8* %p2, align 1, !tbaa !8
  %conv197 = zext i8 %116 to i32
  %add198 = add nsw i32 %add196, %conv197
  %117 = load i8, i8* %p1, align 1, !tbaa !8
  %conv199 = zext i8 %117 to i32
  %add200 = add nsw i32 %add198, %conv199
  %118 = load i8, i8* %p0, align 1, !tbaa !8
  %conv201 = zext i8 %118 to i32
  %add202 = add nsw i32 %add200, %conv201
  %119 = load i8, i8* %q0, align 1, !tbaa !8
  %conv203 = zext i8 %119 to i32
  %mul204 = mul nsw i32 %conv203, 2
  %add205 = add nsw i32 %add202, %mul204
  %120 = load i8, i8* %q1, align 1, !tbaa !8
  %conv206 = zext i8 %120 to i32
  %mul207 = mul nsw i32 %conv206, 2
  %add208 = add nsw i32 %add205, %mul207
  %121 = load i8, i8* %q2, align 1, !tbaa !8
  %conv209 = zext i8 %121 to i32
  %mul210 = mul nsw i32 %conv209, 2
  %add211 = add nsw i32 %add208, %mul210
  %122 = load i8, i8* %q3, align 1, !tbaa !8
  %conv212 = zext i8 %122 to i32
  %add213 = add nsw i32 %add211, %conv212
  %123 = load i8, i8* %q4, align 1, !tbaa !8
  %conv214 = zext i8 %123 to i32
  %add215 = add nsw i32 %add213, %conv214
  %124 = load i8, i8* %q5, align 1, !tbaa !8
  %conv216 = zext i8 %124 to i32
  %add217 = add nsw i32 %add215, %conv216
  %125 = load i8, i8* %q6, align 1, !tbaa !8
  %conv218 = zext i8 %125 to i32
  %mul219 = mul nsw i32 %conv218, 2
  %add220 = add nsw i32 %add217, %mul219
  %add221 = add nsw i32 %add220, 8
  %shr222 = ashr i32 %add221, 4
  %conv223 = trunc i32 %shr222 to i8
  %126 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  store i8 %conv223, i8* %126, align 1, !tbaa !8
  %127 = load i8, i8* %p3, align 1, !tbaa !8
  %conv224 = zext i8 %127 to i32
  %128 = load i8, i8* %p2, align 1, !tbaa !8
  %conv225 = zext i8 %128 to i32
  %add226 = add nsw i32 %conv224, %conv225
  %129 = load i8, i8* %p1, align 1, !tbaa !8
  %conv227 = zext i8 %129 to i32
  %add228 = add nsw i32 %add226, %conv227
  %130 = load i8, i8* %p0, align 1, !tbaa !8
  %conv229 = zext i8 %130 to i32
  %add230 = add nsw i32 %add228, %conv229
  %131 = load i8, i8* %q0, align 1, !tbaa !8
  %conv231 = zext i8 %131 to i32
  %add232 = add nsw i32 %add230, %conv231
  %132 = load i8, i8* %q1, align 1, !tbaa !8
  %conv233 = zext i8 %132 to i32
  %mul234 = mul nsw i32 %conv233, 2
  %add235 = add nsw i32 %add232, %mul234
  %133 = load i8, i8* %q2, align 1, !tbaa !8
  %conv236 = zext i8 %133 to i32
  %mul237 = mul nsw i32 %conv236, 2
  %add238 = add nsw i32 %add235, %mul237
  %134 = load i8, i8* %q3, align 1, !tbaa !8
  %conv239 = zext i8 %134 to i32
  %mul240 = mul nsw i32 %conv239, 2
  %add241 = add nsw i32 %add238, %mul240
  %135 = load i8, i8* %q4, align 1, !tbaa !8
  %conv242 = zext i8 %135 to i32
  %add243 = add nsw i32 %add241, %conv242
  %136 = load i8, i8* %q5, align 1, !tbaa !8
  %conv244 = zext i8 %136 to i32
  %add245 = add nsw i32 %add243, %conv244
  %137 = load i8, i8* %q6, align 1, !tbaa !8
  %conv246 = zext i8 %137 to i32
  %mul247 = mul nsw i32 %conv246, 3
  %add248 = add nsw i32 %add245, %mul247
  %add249 = add nsw i32 %add248, 8
  %shr250 = ashr i32 %add249, 4
  %conv251 = trunc i32 %shr250 to i8
  %138 = load i8*, i8** %oq2.addr, align 4, !tbaa !2
  store i8 %conv251, i8* %138, align 1, !tbaa !8
  %139 = load i8, i8* %p2, align 1, !tbaa !8
  %conv252 = zext i8 %139 to i32
  %140 = load i8, i8* %p1, align 1, !tbaa !8
  %conv253 = zext i8 %140 to i32
  %add254 = add nsw i32 %conv252, %conv253
  %141 = load i8, i8* %p0, align 1, !tbaa !8
  %conv255 = zext i8 %141 to i32
  %add256 = add nsw i32 %add254, %conv255
  %142 = load i8, i8* %q0, align 1, !tbaa !8
  %conv257 = zext i8 %142 to i32
  %add258 = add nsw i32 %add256, %conv257
  %143 = load i8, i8* %q1, align 1, !tbaa !8
  %conv259 = zext i8 %143 to i32
  %add260 = add nsw i32 %add258, %conv259
  %144 = load i8, i8* %q2, align 1, !tbaa !8
  %conv261 = zext i8 %144 to i32
  %mul262 = mul nsw i32 %conv261, 2
  %add263 = add nsw i32 %add260, %mul262
  %145 = load i8, i8* %q3, align 1, !tbaa !8
  %conv264 = zext i8 %145 to i32
  %mul265 = mul nsw i32 %conv264, 2
  %add266 = add nsw i32 %add263, %mul265
  %146 = load i8, i8* %q4, align 1, !tbaa !8
  %conv267 = zext i8 %146 to i32
  %mul268 = mul nsw i32 %conv267, 2
  %add269 = add nsw i32 %add266, %mul268
  %147 = load i8, i8* %q5, align 1, !tbaa !8
  %conv270 = zext i8 %147 to i32
  %add271 = add nsw i32 %add269, %conv270
  %148 = load i8, i8* %q6, align 1, !tbaa !8
  %conv272 = zext i8 %148 to i32
  %mul273 = mul nsw i32 %conv272, 4
  %add274 = add nsw i32 %add271, %mul273
  %add275 = add nsw i32 %add274, 8
  %shr276 = ashr i32 %add275, 4
  %conv277 = trunc i32 %shr276 to i8
  %149 = load i8*, i8** %oq3.addr, align 4, !tbaa !2
  store i8 %conv277, i8* %149, align 1, !tbaa !8
  %150 = load i8, i8* %p1, align 1, !tbaa !8
  %conv278 = zext i8 %150 to i32
  %151 = load i8, i8* %p0, align 1, !tbaa !8
  %conv279 = zext i8 %151 to i32
  %add280 = add nsw i32 %conv278, %conv279
  %152 = load i8, i8* %q0, align 1, !tbaa !8
  %conv281 = zext i8 %152 to i32
  %add282 = add nsw i32 %add280, %conv281
  %153 = load i8, i8* %q1, align 1, !tbaa !8
  %conv283 = zext i8 %153 to i32
  %add284 = add nsw i32 %add282, %conv283
  %154 = load i8, i8* %q2, align 1, !tbaa !8
  %conv285 = zext i8 %154 to i32
  %add286 = add nsw i32 %add284, %conv285
  %155 = load i8, i8* %q3, align 1, !tbaa !8
  %conv287 = zext i8 %155 to i32
  %mul288 = mul nsw i32 %conv287, 2
  %add289 = add nsw i32 %add286, %mul288
  %156 = load i8, i8* %q4, align 1, !tbaa !8
  %conv290 = zext i8 %156 to i32
  %mul291 = mul nsw i32 %conv290, 2
  %add292 = add nsw i32 %add289, %mul291
  %157 = load i8, i8* %q5, align 1, !tbaa !8
  %conv293 = zext i8 %157 to i32
  %mul294 = mul nsw i32 %conv293, 2
  %add295 = add nsw i32 %add292, %mul294
  %158 = load i8, i8* %q6, align 1, !tbaa !8
  %conv296 = zext i8 %158 to i32
  %mul297 = mul nsw i32 %conv296, 5
  %add298 = add nsw i32 %add295, %mul297
  %add299 = add nsw i32 %add298, 8
  %shr300 = ashr i32 %add299, 4
  %conv301 = trunc i32 %shr300 to i8
  %159 = load i8*, i8** %oq4.addr, align 4, !tbaa !2
  store i8 %conv301, i8* %159, align 1, !tbaa !8
  %160 = load i8, i8* %p0, align 1, !tbaa !8
  %conv302 = zext i8 %160 to i32
  %161 = load i8, i8* %q0, align 1, !tbaa !8
  %conv303 = zext i8 %161 to i32
  %add304 = add nsw i32 %conv302, %conv303
  %162 = load i8, i8* %q1, align 1, !tbaa !8
  %conv305 = zext i8 %162 to i32
  %add306 = add nsw i32 %add304, %conv305
  %163 = load i8, i8* %q2, align 1, !tbaa !8
  %conv307 = zext i8 %163 to i32
  %add308 = add nsw i32 %add306, %conv307
  %164 = load i8, i8* %q3, align 1, !tbaa !8
  %conv309 = zext i8 %164 to i32
  %add310 = add nsw i32 %add308, %conv309
  %165 = load i8, i8* %q4, align 1, !tbaa !8
  %conv311 = zext i8 %165 to i32
  %mul312 = mul nsw i32 %conv311, 2
  %add313 = add nsw i32 %add310, %mul312
  %166 = load i8, i8* %q5, align 1, !tbaa !8
  %conv314 = zext i8 %166 to i32
  %mul315 = mul nsw i32 %conv314, 2
  %add316 = add nsw i32 %add313, %mul315
  %167 = load i8, i8* %q6, align 1, !tbaa !8
  %conv317 = zext i8 %167 to i32
  %mul318 = mul nsw i32 %conv317, 7
  %add319 = add nsw i32 %add316, %mul318
  %add320 = add nsw i32 %add319, 8
  %shr321 = ashr i32 %add320, 4
  %conv322 = trunc i32 %shr321 to i8
  %168 = load i8*, i8** %oq5.addr, align 4, !tbaa !2
  store i8 %conv322, i8* %168, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q6) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q5) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q4) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q3) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %q0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p1) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p3) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p4) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p5) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %p6) #4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true3, %land.lhs.true, %entry
  %169 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %170 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %171 = load i8, i8* %flat.addr, align 1, !tbaa !8
  %172 = load i8*, i8** %op3.addr, align 4, !tbaa !2
  %173 = load i8*, i8** %op2.addr, align 4, !tbaa !2
  %174 = load i8*, i8** %op1.addr, align 4, !tbaa !2
  %175 = load i8*, i8** %op0.addr, align 4, !tbaa !2
  %176 = load i8*, i8** %oq0.addr, align 4, !tbaa !2
  %177 = load i8*, i8** %oq1.addr, align 4, !tbaa !2
  %178 = load i8*, i8** %oq2.addr, align 4, !tbaa !2
  %179 = load i8*, i8** %oq3.addr, align 4, !tbaa !2
  call void @filter8(i8 signext %169, i8 zeroext %170, i8 signext %171, i8* %172, i8* %173, i8* %174, i8* %175, i8* %176, i8* %177, i8* %178, i8* %179)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal signext i16 @highbd_hev_mask(i8 zeroext %thresh, i16 zeroext %p1, i16 zeroext %p0, i16 zeroext %q0, i16 zeroext %q1, i32 %bd) #2 {
entry:
  %thresh.addr = alloca i8, align 1
  %p1.addr = alloca i16, align 2
  %p0.addr = alloca i16, align 2
  %q0.addr = alloca i16, align 2
  %q1.addr = alloca i16, align 2
  %bd.addr = alloca i32, align 4
  %hev = alloca i16, align 2
  %thresh16 = alloca i16, align 2
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i16 %p1, i16* %p1.addr, align 2, !tbaa !9
  store i16 %p0, i16* %p0.addr, align 2, !tbaa !9
  store i16 %q0, i16* %q0.addr, align 2, !tbaa !9
  store i16 %q1, i16* %q1.addr, align 2, !tbaa !9
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i16* %hev to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #4
  store i16 0, i16* %hev, align 2, !tbaa !9
  %1 = bitcast i16* %thresh16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %1) #4
  %2 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %conv = zext i8 %2 to i16
  %conv1 = zext i16 %conv to i32
  %3 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %3, 8
  %shl = shl i32 %conv1, %sub
  %conv2 = trunc i32 %shl to i16
  store i16 %conv2, i16* %thresh16, align 2, !tbaa !9
  %4 = load i16, i16* %p1.addr, align 2, !tbaa !9
  %conv3 = zext i16 %4 to i32
  %5 = load i16, i16* %p0.addr, align 2, !tbaa !9
  %conv4 = zext i16 %5 to i32
  %sub5 = sub nsw i32 %conv3, %conv4
  %call = call i32 @abs(i32 %sub5) #5
  %6 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv6 = sext i16 %6 to i32
  %cmp = icmp sgt i32 %call, %conv6
  %conv7 = zext i1 %cmp to i32
  %mul = mul nsw i32 %conv7, -1
  %7 = load i16, i16* %hev, align 2, !tbaa !9
  %conv8 = sext i16 %7 to i32
  %or = or i32 %conv8, %mul
  %conv9 = trunc i32 %or to i16
  store i16 %conv9, i16* %hev, align 2, !tbaa !9
  %8 = load i16, i16* %q1.addr, align 2, !tbaa !9
  %conv10 = zext i16 %8 to i32
  %9 = load i16, i16* %q0.addr, align 2, !tbaa !9
  %conv11 = zext i16 %9 to i32
  %sub12 = sub nsw i32 %conv10, %conv11
  %call13 = call i32 @abs(i32 %sub12) #5
  %10 = load i16, i16* %thresh16, align 2, !tbaa !9
  %conv14 = sext i16 %10 to i32
  %cmp15 = icmp sgt i32 %call13, %conv14
  %conv16 = zext i1 %cmp15 to i32
  %mul17 = mul nsw i32 %conv16, -1
  %11 = load i16, i16* %hev, align 2, !tbaa !9
  %conv18 = sext i16 %11 to i32
  %or19 = or i32 %conv18, %mul17
  %conv20 = trunc i32 %or19 to i16
  store i16 %conv20, i16* %hev, align 2, !tbaa !9
  %12 = load i16, i16* %hev, align 2, !tbaa !9
  %13 = bitcast i16* %thresh16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %13) #4
  %14 = bitcast i16* %hev to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %14) #4
  ret i16 %12
}

; Function Attrs: inlinehint nounwind
define internal signext i16 @signed_char_clamp_high(i32 %t, i32 %bd) #2 {
entry:
  %retval = alloca i16, align 2
  %t.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32 %t, i32* %t.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 10, label %sw.bb
    i32 12, label %sw.bb1
    i32 8, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %t.addr, align 4, !tbaa !6
  %call = call i32 @clamp(i32 %1, i32 -512, i32 511)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %retval, align 2
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32, i32* %t.addr, align 4, !tbaa !6
  %call2 = call i32 @clamp(i32 %2, i32 -2048, i32 2047)
  %conv3 = trunc i32 %call2 to i16
  store i16 %conv3, i16* %retval, align 2
  br label %return

sw.bb4:                                           ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb4
  %3 = load i32, i32* %t.addr, align 4, !tbaa !6
  %call5 = call i32 @clamp(i32 %3, i32 -128, i32 127)
  %conv6 = trunc i32 %call5 to i16
  store i16 %conv6, i16* %retval, align 2
  br label %return

return:                                           ; preds = %sw.default, %sw.bb1, %sw.bb
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_filter14(i8 signext %mask, i8 zeroext %thresh, i8 signext %flat, i8 signext %flat2, i16* %op6, i16* %op5, i16* %op4, i16* %op3, i16* %op2, i16* %op1, i16* %op0, i16* %oq0, i16* %oq1, i16* %oq2, i16* %oq3, i16* %oq4, i16* %oq5, i16* %oq6, i32 %bd) #2 {
entry:
  %mask.addr = alloca i8, align 1
  %thresh.addr = alloca i8, align 1
  %flat.addr = alloca i8, align 1
  %flat2.addr = alloca i8, align 1
  %op6.addr = alloca i16*, align 4
  %op5.addr = alloca i16*, align 4
  %op4.addr = alloca i16*, align 4
  %op3.addr = alloca i16*, align 4
  %op2.addr = alloca i16*, align 4
  %op1.addr = alloca i16*, align 4
  %op0.addr = alloca i16*, align 4
  %oq0.addr = alloca i16*, align 4
  %oq1.addr = alloca i16*, align 4
  %oq2.addr = alloca i16*, align 4
  %oq3.addr = alloca i16*, align 4
  %oq4.addr = alloca i16*, align 4
  %oq5.addr = alloca i16*, align 4
  %oq6.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %p6 = alloca i16, align 2
  %p5 = alloca i16, align 2
  %p4 = alloca i16, align 2
  %p3 = alloca i16, align 2
  %p2 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %p0 = alloca i16, align 2
  %q0 = alloca i16, align 2
  %q1 = alloca i16, align 2
  %q2 = alloca i16, align 2
  %q3 = alloca i16, align 2
  %q4 = alloca i16, align 2
  %q5 = alloca i16, align 2
  %q6 = alloca i16, align 2
  store i8 %mask, i8* %mask.addr, align 1, !tbaa !8
  store i8 %thresh, i8* %thresh.addr, align 1, !tbaa !8
  store i8 %flat, i8* %flat.addr, align 1, !tbaa !8
  store i8 %flat2, i8* %flat2.addr, align 1, !tbaa !8
  store i16* %op6, i16** %op6.addr, align 4, !tbaa !2
  store i16* %op5, i16** %op5.addr, align 4, !tbaa !2
  store i16* %op4, i16** %op4.addr, align 4, !tbaa !2
  store i16* %op3, i16** %op3.addr, align 4, !tbaa !2
  store i16* %op2, i16** %op2.addr, align 4, !tbaa !2
  store i16* %op1, i16** %op1.addr, align 4, !tbaa !2
  store i16* %op0, i16** %op0.addr, align 4, !tbaa !2
  store i16* %oq0, i16** %oq0.addr, align 4, !tbaa !2
  store i16* %oq1, i16** %oq1.addr, align 4, !tbaa !2
  store i16* %oq2, i16** %oq2.addr, align 4, !tbaa !2
  store i16* %oq3, i16** %oq3.addr, align 4, !tbaa !2
  store i16* %oq4, i16** %oq4.addr, align 4, !tbaa !2
  store i16* %oq5, i16** %oq5.addr, align 4, !tbaa !2
  store i16* %oq6, i16** %oq6.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i8, i8* %flat2.addr, align 1, !tbaa !8
  %conv = sext i8 %0 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %flat.addr, align 1, !tbaa !8
  %conv1 = sext i8 %1 to i32
  %tobool2 = icmp ne i32 %conv1, 0
  br i1 %tobool2, label %land.lhs.true3, label %if.else

land.lhs.true3:                                   ; preds = %land.lhs.true
  %2 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %conv4 = sext i8 %2 to i32
  %tobool5 = icmp ne i32 %conv4, 0
  br i1 %tobool5, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true3
  %3 = bitcast i16* %p6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %3) #4
  %4 = load i16*, i16** %op6.addr, align 4, !tbaa !2
  %5 = load i16, i16* %4, align 2, !tbaa !9
  store i16 %5, i16* %p6, align 2, !tbaa !9
  %6 = bitcast i16* %p5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %6) #4
  %7 = load i16*, i16** %op5.addr, align 4, !tbaa !2
  %8 = load i16, i16* %7, align 2, !tbaa !9
  store i16 %8, i16* %p5, align 2, !tbaa !9
  %9 = bitcast i16* %p4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %9) #4
  %10 = load i16*, i16** %op4.addr, align 4, !tbaa !2
  %11 = load i16, i16* %10, align 2, !tbaa !9
  store i16 %11, i16* %p4, align 2, !tbaa !9
  %12 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %12) #4
  %13 = load i16*, i16** %op3.addr, align 4, !tbaa !2
  %14 = load i16, i16* %13, align 2, !tbaa !9
  store i16 %14, i16* %p3, align 2, !tbaa !9
  %15 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %15) #4
  %16 = load i16*, i16** %op2.addr, align 4, !tbaa !2
  %17 = load i16, i16* %16, align 2, !tbaa !9
  store i16 %17, i16* %p2, align 2, !tbaa !9
  %18 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %18) #4
  %19 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  %20 = load i16, i16* %19, align 2, !tbaa !9
  store i16 %20, i16* %p1, align 2, !tbaa !9
  %21 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %21) #4
  %22 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  %23 = load i16, i16* %22, align 2, !tbaa !9
  store i16 %23, i16* %p0, align 2, !tbaa !9
  %24 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %24) #4
  %25 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  %26 = load i16, i16* %25, align 2, !tbaa !9
  store i16 %26, i16* %q0, align 2, !tbaa !9
  %27 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %27) #4
  %28 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  %29 = load i16, i16* %28, align 2, !tbaa !9
  store i16 %29, i16* %q1, align 2, !tbaa !9
  %30 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %30) #4
  %31 = load i16*, i16** %oq2.addr, align 4, !tbaa !2
  %32 = load i16, i16* %31, align 2, !tbaa !9
  store i16 %32, i16* %q2, align 2, !tbaa !9
  %33 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %33) #4
  %34 = load i16*, i16** %oq3.addr, align 4, !tbaa !2
  %35 = load i16, i16* %34, align 2, !tbaa !9
  store i16 %35, i16* %q3, align 2, !tbaa !9
  %36 = bitcast i16* %q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %36) #4
  %37 = load i16*, i16** %oq4.addr, align 4, !tbaa !2
  %38 = load i16, i16* %37, align 2, !tbaa !9
  store i16 %38, i16* %q4, align 2, !tbaa !9
  %39 = bitcast i16* %q5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %39) #4
  %40 = load i16*, i16** %oq5.addr, align 4, !tbaa !2
  %41 = load i16, i16* %40, align 2, !tbaa !9
  store i16 %41, i16* %q5, align 2, !tbaa !9
  %42 = bitcast i16* %q6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %42) #4
  %43 = load i16*, i16** %oq6.addr, align 4, !tbaa !2
  %44 = load i16, i16* %43, align 2, !tbaa !9
  store i16 %44, i16* %q6, align 2, !tbaa !9
  %45 = load i16, i16* %p6, align 2, !tbaa !9
  %conv6 = zext i16 %45 to i32
  %mul = mul nsw i32 %conv6, 7
  %46 = load i16, i16* %p5, align 2, !tbaa !9
  %conv7 = zext i16 %46 to i32
  %mul8 = mul nsw i32 %conv7, 2
  %add = add nsw i32 %mul, %mul8
  %47 = load i16, i16* %p4, align 2, !tbaa !9
  %conv9 = zext i16 %47 to i32
  %mul10 = mul nsw i32 %conv9, 2
  %add11 = add nsw i32 %add, %mul10
  %48 = load i16, i16* %p3, align 2, !tbaa !9
  %conv12 = zext i16 %48 to i32
  %add13 = add nsw i32 %add11, %conv12
  %49 = load i16, i16* %p2, align 2, !tbaa !9
  %conv14 = zext i16 %49 to i32
  %add15 = add nsw i32 %add13, %conv14
  %50 = load i16, i16* %p1, align 2, !tbaa !9
  %conv16 = zext i16 %50 to i32
  %add17 = add nsw i32 %add15, %conv16
  %51 = load i16, i16* %p0, align 2, !tbaa !9
  %conv18 = zext i16 %51 to i32
  %add19 = add nsw i32 %add17, %conv18
  %52 = load i16, i16* %q0, align 2, !tbaa !9
  %conv20 = zext i16 %52 to i32
  %add21 = add nsw i32 %add19, %conv20
  %add22 = add nsw i32 %add21, 8
  %shr = ashr i32 %add22, 4
  %conv23 = trunc i32 %shr to i16
  %53 = load i16*, i16** %op5.addr, align 4, !tbaa !2
  store i16 %conv23, i16* %53, align 2, !tbaa !9
  %54 = load i16, i16* %p6, align 2, !tbaa !9
  %conv24 = zext i16 %54 to i32
  %mul25 = mul nsw i32 %conv24, 5
  %55 = load i16, i16* %p5, align 2, !tbaa !9
  %conv26 = zext i16 %55 to i32
  %mul27 = mul nsw i32 %conv26, 2
  %add28 = add nsw i32 %mul25, %mul27
  %56 = load i16, i16* %p4, align 2, !tbaa !9
  %conv29 = zext i16 %56 to i32
  %mul30 = mul nsw i32 %conv29, 2
  %add31 = add nsw i32 %add28, %mul30
  %57 = load i16, i16* %p3, align 2, !tbaa !9
  %conv32 = zext i16 %57 to i32
  %mul33 = mul nsw i32 %conv32, 2
  %add34 = add nsw i32 %add31, %mul33
  %58 = load i16, i16* %p2, align 2, !tbaa !9
  %conv35 = zext i16 %58 to i32
  %add36 = add nsw i32 %add34, %conv35
  %59 = load i16, i16* %p1, align 2, !tbaa !9
  %conv37 = zext i16 %59 to i32
  %add38 = add nsw i32 %add36, %conv37
  %60 = load i16, i16* %p0, align 2, !tbaa !9
  %conv39 = zext i16 %60 to i32
  %add40 = add nsw i32 %add38, %conv39
  %61 = load i16, i16* %q0, align 2, !tbaa !9
  %conv41 = zext i16 %61 to i32
  %add42 = add nsw i32 %add40, %conv41
  %62 = load i16, i16* %q1, align 2, !tbaa !9
  %conv43 = zext i16 %62 to i32
  %add44 = add nsw i32 %add42, %conv43
  %add45 = add nsw i32 %add44, 8
  %shr46 = ashr i32 %add45, 4
  %conv47 = trunc i32 %shr46 to i16
  %63 = load i16*, i16** %op4.addr, align 4, !tbaa !2
  store i16 %conv47, i16* %63, align 2, !tbaa !9
  %64 = load i16, i16* %p6, align 2, !tbaa !9
  %conv48 = zext i16 %64 to i32
  %mul49 = mul nsw i32 %conv48, 4
  %65 = load i16, i16* %p5, align 2, !tbaa !9
  %conv50 = zext i16 %65 to i32
  %add51 = add nsw i32 %mul49, %conv50
  %66 = load i16, i16* %p4, align 2, !tbaa !9
  %conv52 = zext i16 %66 to i32
  %mul53 = mul nsw i32 %conv52, 2
  %add54 = add nsw i32 %add51, %mul53
  %67 = load i16, i16* %p3, align 2, !tbaa !9
  %conv55 = zext i16 %67 to i32
  %mul56 = mul nsw i32 %conv55, 2
  %add57 = add nsw i32 %add54, %mul56
  %68 = load i16, i16* %p2, align 2, !tbaa !9
  %conv58 = zext i16 %68 to i32
  %mul59 = mul nsw i32 %conv58, 2
  %add60 = add nsw i32 %add57, %mul59
  %69 = load i16, i16* %p1, align 2, !tbaa !9
  %conv61 = zext i16 %69 to i32
  %add62 = add nsw i32 %add60, %conv61
  %70 = load i16, i16* %p0, align 2, !tbaa !9
  %conv63 = zext i16 %70 to i32
  %add64 = add nsw i32 %add62, %conv63
  %71 = load i16, i16* %q0, align 2, !tbaa !9
  %conv65 = zext i16 %71 to i32
  %add66 = add nsw i32 %add64, %conv65
  %72 = load i16, i16* %q1, align 2, !tbaa !9
  %conv67 = zext i16 %72 to i32
  %add68 = add nsw i32 %add66, %conv67
  %73 = load i16, i16* %q2, align 2, !tbaa !9
  %conv69 = zext i16 %73 to i32
  %add70 = add nsw i32 %add68, %conv69
  %add71 = add nsw i32 %add70, 8
  %shr72 = ashr i32 %add71, 4
  %conv73 = trunc i32 %shr72 to i16
  %74 = load i16*, i16** %op3.addr, align 4, !tbaa !2
  store i16 %conv73, i16* %74, align 2, !tbaa !9
  %75 = load i16, i16* %p6, align 2, !tbaa !9
  %conv74 = zext i16 %75 to i32
  %mul75 = mul nsw i32 %conv74, 3
  %76 = load i16, i16* %p5, align 2, !tbaa !9
  %conv76 = zext i16 %76 to i32
  %add77 = add nsw i32 %mul75, %conv76
  %77 = load i16, i16* %p4, align 2, !tbaa !9
  %conv78 = zext i16 %77 to i32
  %add79 = add nsw i32 %add77, %conv78
  %78 = load i16, i16* %p3, align 2, !tbaa !9
  %conv80 = zext i16 %78 to i32
  %mul81 = mul nsw i32 %conv80, 2
  %add82 = add nsw i32 %add79, %mul81
  %79 = load i16, i16* %p2, align 2, !tbaa !9
  %conv83 = zext i16 %79 to i32
  %mul84 = mul nsw i32 %conv83, 2
  %add85 = add nsw i32 %add82, %mul84
  %80 = load i16, i16* %p1, align 2, !tbaa !9
  %conv86 = zext i16 %80 to i32
  %mul87 = mul nsw i32 %conv86, 2
  %add88 = add nsw i32 %add85, %mul87
  %81 = load i16, i16* %p0, align 2, !tbaa !9
  %conv89 = zext i16 %81 to i32
  %add90 = add nsw i32 %add88, %conv89
  %82 = load i16, i16* %q0, align 2, !tbaa !9
  %conv91 = zext i16 %82 to i32
  %add92 = add nsw i32 %add90, %conv91
  %83 = load i16, i16* %q1, align 2, !tbaa !9
  %conv93 = zext i16 %83 to i32
  %add94 = add nsw i32 %add92, %conv93
  %84 = load i16, i16* %q2, align 2, !tbaa !9
  %conv95 = zext i16 %84 to i32
  %add96 = add nsw i32 %add94, %conv95
  %85 = load i16, i16* %q3, align 2, !tbaa !9
  %conv97 = zext i16 %85 to i32
  %add98 = add nsw i32 %add96, %conv97
  %add99 = add nsw i32 %add98, 8
  %shr100 = ashr i32 %add99, 4
  %conv101 = trunc i32 %shr100 to i16
  %86 = load i16*, i16** %op2.addr, align 4, !tbaa !2
  store i16 %conv101, i16* %86, align 2, !tbaa !9
  %87 = load i16, i16* %p6, align 2, !tbaa !9
  %conv102 = zext i16 %87 to i32
  %mul103 = mul nsw i32 %conv102, 2
  %88 = load i16, i16* %p5, align 2, !tbaa !9
  %conv104 = zext i16 %88 to i32
  %add105 = add nsw i32 %mul103, %conv104
  %89 = load i16, i16* %p4, align 2, !tbaa !9
  %conv106 = zext i16 %89 to i32
  %add107 = add nsw i32 %add105, %conv106
  %90 = load i16, i16* %p3, align 2, !tbaa !9
  %conv108 = zext i16 %90 to i32
  %add109 = add nsw i32 %add107, %conv108
  %91 = load i16, i16* %p2, align 2, !tbaa !9
  %conv110 = zext i16 %91 to i32
  %mul111 = mul nsw i32 %conv110, 2
  %add112 = add nsw i32 %add109, %mul111
  %92 = load i16, i16* %p1, align 2, !tbaa !9
  %conv113 = zext i16 %92 to i32
  %mul114 = mul nsw i32 %conv113, 2
  %add115 = add nsw i32 %add112, %mul114
  %93 = load i16, i16* %p0, align 2, !tbaa !9
  %conv116 = zext i16 %93 to i32
  %mul117 = mul nsw i32 %conv116, 2
  %add118 = add nsw i32 %add115, %mul117
  %94 = load i16, i16* %q0, align 2, !tbaa !9
  %conv119 = zext i16 %94 to i32
  %add120 = add nsw i32 %add118, %conv119
  %95 = load i16, i16* %q1, align 2, !tbaa !9
  %conv121 = zext i16 %95 to i32
  %add122 = add nsw i32 %add120, %conv121
  %96 = load i16, i16* %q2, align 2, !tbaa !9
  %conv123 = zext i16 %96 to i32
  %add124 = add nsw i32 %add122, %conv123
  %97 = load i16, i16* %q3, align 2, !tbaa !9
  %conv125 = zext i16 %97 to i32
  %add126 = add nsw i32 %add124, %conv125
  %98 = load i16, i16* %q4, align 2, !tbaa !9
  %conv127 = zext i16 %98 to i32
  %add128 = add nsw i32 %add126, %conv127
  %add129 = add nsw i32 %add128, 8
  %shr130 = ashr i32 %add129, 4
  %conv131 = trunc i32 %shr130 to i16
  %99 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  store i16 %conv131, i16* %99, align 2, !tbaa !9
  %100 = load i16, i16* %p6, align 2, !tbaa !9
  %conv132 = zext i16 %100 to i32
  %101 = load i16, i16* %p5, align 2, !tbaa !9
  %conv133 = zext i16 %101 to i32
  %add134 = add nsw i32 %conv132, %conv133
  %102 = load i16, i16* %p4, align 2, !tbaa !9
  %conv135 = zext i16 %102 to i32
  %add136 = add nsw i32 %add134, %conv135
  %103 = load i16, i16* %p3, align 2, !tbaa !9
  %conv137 = zext i16 %103 to i32
  %add138 = add nsw i32 %add136, %conv137
  %104 = load i16, i16* %p2, align 2, !tbaa !9
  %conv139 = zext i16 %104 to i32
  %add140 = add nsw i32 %add138, %conv139
  %105 = load i16, i16* %p1, align 2, !tbaa !9
  %conv141 = zext i16 %105 to i32
  %mul142 = mul nsw i32 %conv141, 2
  %add143 = add nsw i32 %add140, %mul142
  %106 = load i16, i16* %p0, align 2, !tbaa !9
  %conv144 = zext i16 %106 to i32
  %mul145 = mul nsw i32 %conv144, 2
  %add146 = add nsw i32 %add143, %mul145
  %107 = load i16, i16* %q0, align 2, !tbaa !9
  %conv147 = zext i16 %107 to i32
  %mul148 = mul nsw i32 %conv147, 2
  %add149 = add nsw i32 %add146, %mul148
  %108 = load i16, i16* %q1, align 2, !tbaa !9
  %conv150 = zext i16 %108 to i32
  %add151 = add nsw i32 %add149, %conv150
  %109 = load i16, i16* %q2, align 2, !tbaa !9
  %conv152 = zext i16 %109 to i32
  %add153 = add nsw i32 %add151, %conv152
  %110 = load i16, i16* %q3, align 2, !tbaa !9
  %conv154 = zext i16 %110 to i32
  %add155 = add nsw i32 %add153, %conv154
  %111 = load i16, i16* %q4, align 2, !tbaa !9
  %conv156 = zext i16 %111 to i32
  %add157 = add nsw i32 %add155, %conv156
  %112 = load i16, i16* %q5, align 2, !tbaa !9
  %conv158 = zext i16 %112 to i32
  %add159 = add nsw i32 %add157, %conv158
  %add160 = add nsw i32 %add159, 8
  %shr161 = ashr i32 %add160, 4
  %conv162 = trunc i32 %shr161 to i16
  %113 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  store i16 %conv162, i16* %113, align 2, !tbaa !9
  %114 = load i16, i16* %p5, align 2, !tbaa !9
  %conv163 = zext i16 %114 to i32
  %115 = load i16, i16* %p4, align 2, !tbaa !9
  %conv164 = zext i16 %115 to i32
  %add165 = add nsw i32 %conv163, %conv164
  %116 = load i16, i16* %p3, align 2, !tbaa !9
  %conv166 = zext i16 %116 to i32
  %add167 = add nsw i32 %add165, %conv166
  %117 = load i16, i16* %p2, align 2, !tbaa !9
  %conv168 = zext i16 %117 to i32
  %add169 = add nsw i32 %add167, %conv168
  %118 = load i16, i16* %p1, align 2, !tbaa !9
  %conv170 = zext i16 %118 to i32
  %add171 = add nsw i32 %add169, %conv170
  %119 = load i16, i16* %p0, align 2, !tbaa !9
  %conv172 = zext i16 %119 to i32
  %mul173 = mul nsw i32 %conv172, 2
  %add174 = add nsw i32 %add171, %mul173
  %120 = load i16, i16* %q0, align 2, !tbaa !9
  %conv175 = zext i16 %120 to i32
  %mul176 = mul nsw i32 %conv175, 2
  %add177 = add nsw i32 %add174, %mul176
  %121 = load i16, i16* %q1, align 2, !tbaa !9
  %conv178 = zext i16 %121 to i32
  %mul179 = mul nsw i32 %conv178, 2
  %add180 = add nsw i32 %add177, %mul179
  %122 = load i16, i16* %q2, align 2, !tbaa !9
  %conv181 = zext i16 %122 to i32
  %add182 = add nsw i32 %add180, %conv181
  %123 = load i16, i16* %q3, align 2, !tbaa !9
  %conv183 = zext i16 %123 to i32
  %add184 = add nsw i32 %add182, %conv183
  %124 = load i16, i16* %q4, align 2, !tbaa !9
  %conv185 = zext i16 %124 to i32
  %add186 = add nsw i32 %add184, %conv185
  %125 = load i16, i16* %q5, align 2, !tbaa !9
  %conv187 = zext i16 %125 to i32
  %add188 = add nsw i32 %add186, %conv187
  %126 = load i16, i16* %q6, align 2, !tbaa !9
  %conv189 = zext i16 %126 to i32
  %add190 = add nsw i32 %add188, %conv189
  %add191 = add nsw i32 %add190, 8
  %shr192 = ashr i32 %add191, 4
  %conv193 = trunc i32 %shr192 to i16
  %127 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  store i16 %conv193, i16* %127, align 2, !tbaa !9
  %128 = load i16, i16* %p4, align 2, !tbaa !9
  %conv194 = zext i16 %128 to i32
  %129 = load i16, i16* %p3, align 2, !tbaa !9
  %conv195 = zext i16 %129 to i32
  %add196 = add nsw i32 %conv194, %conv195
  %130 = load i16, i16* %p2, align 2, !tbaa !9
  %conv197 = zext i16 %130 to i32
  %add198 = add nsw i32 %add196, %conv197
  %131 = load i16, i16* %p1, align 2, !tbaa !9
  %conv199 = zext i16 %131 to i32
  %add200 = add nsw i32 %add198, %conv199
  %132 = load i16, i16* %p0, align 2, !tbaa !9
  %conv201 = zext i16 %132 to i32
  %add202 = add nsw i32 %add200, %conv201
  %133 = load i16, i16* %q0, align 2, !tbaa !9
  %conv203 = zext i16 %133 to i32
  %mul204 = mul nsw i32 %conv203, 2
  %add205 = add nsw i32 %add202, %mul204
  %134 = load i16, i16* %q1, align 2, !tbaa !9
  %conv206 = zext i16 %134 to i32
  %mul207 = mul nsw i32 %conv206, 2
  %add208 = add nsw i32 %add205, %mul207
  %135 = load i16, i16* %q2, align 2, !tbaa !9
  %conv209 = zext i16 %135 to i32
  %mul210 = mul nsw i32 %conv209, 2
  %add211 = add nsw i32 %add208, %mul210
  %136 = load i16, i16* %q3, align 2, !tbaa !9
  %conv212 = zext i16 %136 to i32
  %add213 = add nsw i32 %add211, %conv212
  %137 = load i16, i16* %q4, align 2, !tbaa !9
  %conv214 = zext i16 %137 to i32
  %add215 = add nsw i32 %add213, %conv214
  %138 = load i16, i16* %q5, align 2, !tbaa !9
  %conv216 = zext i16 %138 to i32
  %add217 = add nsw i32 %add215, %conv216
  %139 = load i16, i16* %q6, align 2, !tbaa !9
  %conv218 = zext i16 %139 to i32
  %mul219 = mul nsw i32 %conv218, 2
  %add220 = add nsw i32 %add217, %mul219
  %add221 = add nsw i32 %add220, 8
  %shr222 = ashr i32 %add221, 4
  %conv223 = trunc i32 %shr222 to i16
  %140 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  store i16 %conv223, i16* %140, align 2, !tbaa !9
  %141 = load i16, i16* %p3, align 2, !tbaa !9
  %conv224 = zext i16 %141 to i32
  %142 = load i16, i16* %p2, align 2, !tbaa !9
  %conv225 = zext i16 %142 to i32
  %add226 = add nsw i32 %conv224, %conv225
  %143 = load i16, i16* %p1, align 2, !tbaa !9
  %conv227 = zext i16 %143 to i32
  %add228 = add nsw i32 %add226, %conv227
  %144 = load i16, i16* %p0, align 2, !tbaa !9
  %conv229 = zext i16 %144 to i32
  %add230 = add nsw i32 %add228, %conv229
  %145 = load i16, i16* %q0, align 2, !tbaa !9
  %conv231 = zext i16 %145 to i32
  %add232 = add nsw i32 %add230, %conv231
  %146 = load i16, i16* %q1, align 2, !tbaa !9
  %conv233 = zext i16 %146 to i32
  %mul234 = mul nsw i32 %conv233, 2
  %add235 = add nsw i32 %add232, %mul234
  %147 = load i16, i16* %q2, align 2, !tbaa !9
  %conv236 = zext i16 %147 to i32
  %mul237 = mul nsw i32 %conv236, 2
  %add238 = add nsw i32 %add235, %mul237
  %148 = load i16, i16* %q3, align 2, !tbaa !9
  %conv239 = zext i16 %148 to i32
  %mul240 = mul nsw i32 %conv239, 2
  %add241 = add nsw i32 %add238, %mul240
  %149 = load i16, i16* %q4, align 2, !tbaa !9
  %conv242 = zext i16 %149 to i32
  %add243 = add nsw i32 %add241, %conv242
  %150 = load i16, i16* %q5, align 2, !tbaa !9
  %conv244 = zext i16 %150 to i32
  %add245 = add nsw i32 %add243, %conv244
  %151 = load i16, i16* %q6, align 2, !tbaa !9
  %conv246 = zext i16 %151 to i32
  %mul247 = mul nsw i32 %conv246, 3
  %add248 = add nsw i32 %add245, %mul247
  %add249 = add nsw i32 %add248, 8
  %shr250 = ashr i32 %add249, 4
  %conv251 = trunc i32 %shr250 to i16
  %152 = load i16*, i16** %oq2.addr, align 4, !tbaa !2
  store i16 %conv251, i16* %152, align 2, !tbaa !9
  %153 = load i16, i16* %p2, align 2, !tbaa !9
  %conv252 = zext i16 %153 to i32
  %154 = load i16, i16* %p1, align 2, !tbaa !9
  %conv253 = zext i16 %154 to i32
  %add254 = add nsw i32 %conv252, %conv253
  %155 = load i16, i16* %p0, align 2, !tbaa !9
  %conv255 = zext i16 %155 to i32
  %add256 = add nsw i32 %add254, %conv255
  %156 = load i16, i16* %q0, align 2, !tbaa !9
  %conv257 = zext i16 %156 to i32
  %add258 = add nsw i32 %add256, %conv257
  %157 = load i16, i16* %q1, align 2, !tbaa !9
  %conv259 = zext i16 %157 to i32
  %add260 = add nsw i32 %add258, %conv259
  %158 = load i16, i16* %q2, align 2, !tbaa !9
  %conv261 = zext i16 %158 to i32
  %mul262 = mul nsw i32 %conv261, 2
  %add263 = add nsw i32 %add260, %mul262
  %159 = load i16, i16* %q3, align 2, !tbaa !9
  %conv264 = zext i16 %159 to i32
  %mul265 = mul nsw i32 %conv264, 2
  %add266 = add nsw i32 %add263, %mul265
  %160 = load i16, i16* %q4, align 2, !tbaa !9
  %conv267 = zext i16 %160 to i32
  %mul268 = mul nsw i32 %conv267, 2
  %add269 = add nsw i32 %add266, %mul268
  %161 = load i16, i16* %q5, align 2, !tbaa !9
  %conv270 = zext i16 %161 to i32
  %add271 = add nsw i32 %add269, %conv270
  %162 = load i16, i16* %q6, align 2, !tbaa !9
  %conv272 = zext i16 %162 to i32
  %mul273 = mul nsw i32 %conv272, 4
  %add274 = add nsw i32 %add271, %mul273
  %add275 = add nsw i32 %add274, 8
  %shr276 = ashr i32 %add275, 4
  %conv277 = trunc i32 %shr276 to i16
  %163 = load i16*, i16** %oq3.addr, align 4, !tbaa !2
  store i16 %conv277, i16* %163, align 2, !tbaa !9
  %164 = load i16, i16* %p1, align 2, !tbaa !9
  %conv278 = zext i16 %164 to i32
  %165 = load i16, i16* %p0, align 2, !tbaa !9
  %conv279 = zext i16 %165 to i32
  %add280 = add nsw i32 %conv278, %conv279
  %166 = load i16, i16* %q0, align 2, !tbaa !9
  %conv281 = zext i16 %166 to i32
  %add282 = add nsw i32 %add280, %conv281
  %167 = load i16, i16* %q1, align 2, !tbaa !9
  %conv283 = zext i16 %167 to i32
  %add284 = add nsw i32 %add282, %conv283
  %168 = load i16, i16* %q2, align 2, !tbaa !9
  %conv285 = zext i16 %168 to i32
  %add286 = add nsw i32 %add284, %conv285
  %169 = load i16, i16* %q3, align 2, !tbaa !9
  %conv287 = zext i16 %169 to i32
  %mul288 = mul nsw i32 %conv287, 2
  %add289 = add nsw i32 %add286, %mul288
  %170 = load i16, i16* %q4, align 2, !tbaa !9
  %conv290 = zext i16 %170 to i32
  %mul291 = mul nsw i32 %conv290, 2
  %add292 = add nsw i32 %add289, %mul291
  %171 = load i16, i16* %q5, align 2, !tbaa !9
  %conv293 = zext i16 %171 to i32
  %mul294 = mul nsw i32 %conv293, 2
  %add295 = add nsw i32 %add292, %mul294
  %172 = load i16, i16* %q6, align 2, !tbaa !9
  %conv296 = zext i16 %172 to i32
  %mul297 = mul nsw i32 %conv296, 5
  %add298 = add nsw i32 %add295, %mul297
  %add299 = add nsw i32 %add298, 8
  %shr300 = ashr i32 %add299, 4
  %conv301 = trunc i32 %shr300 to i16
  %173 = load i16*, i16** %oq4.addr, align 4, !tbaa !2
  store i16 %conv301, i16* %173, align 2, !tbaa !9
  %174 = load i16, i16* %p0, align 2, !tbaa !9
  %conv302 = zext i16 %174 to i32
  %175 = load i16, i16* %q0, align 2, !tbaa !9
  %conv303 = zext i16 %175 to i32
  %add304 = add nsw i32 %conv302, %conv303
  %176 = load i16, i16* %q1, align 2, !tbaa !9
  %conv305 = zext i16 %176 to i32
  %add306 = add nsw i32 %add304, %conv305
  %177 = load i16, i16* %q2, align 2, !tbaa !9
  %conv307 = zext i16 %177 to i32
  %add308 = add nsw i32 %add306, %conv307
  %178 = load i16, i16* %q3, align 2, !tbaa !9
  %conv309 = zext i16 %178 to i32
  %add310 = add nsw i32 %add308, %conv309
  %179 = load i16, i16* %q4, align 2, !tbaa !9
  %conv311 = zext i16 %179 to i32
  %mul312 = mul nsw i32 %conv311, 2
  %add313 = add nsw i32 %add310, %mul312
  %180 = load i16, i16* %q5, align 2, !tbaa !9
  %conv314 = zext i16 %180 to i32
  %mul315 = mul nsw i32 %conv314, 2
  %add316 = add nsw i32 %add313, %mul315
  %181 = load i16, i16* %q6, align 2, !tbaa !9
  %conv317 = zext i16 %181 to i32
  %mul318 = mul nsw i32 %conv317, 7
  %add319 = add nsw i32 %add316, %mul318
  %add320 = add nsw i32 %add319, 8
  %shr321 = ashr i32 %add320, 4
  %conv322 = trunc i32 %shr321 to i16
  %182 = load i16*, i16** %oq5.addr, align 4, !tbaa !2
  store i16 %conv322, i16* %182, align 2, !tbaa !9
  %183 = bitcast i16* %q6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %183) #4
  %184 = bitcast i16* %q5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %184) #4
  %185 = bitcast i16* %q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %185) #4
  %186 = bitcast i16* %q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %186) #4
  %187 = bitcast i16* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %187) #4
  %188 = bitcast i16* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %188) #4
  %189 = bitcast i16* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %189) #4
  %190 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %190) #4
  %191 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %191) #4
  %192 = bitcast i16* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %192) #4
  %193 = bitcast i16* %p3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %193) #4
  %194 = bitcast i16* %p4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %194) #4
  %195 = bitcast i16* %p5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %195) #4
  %196 = bitcast i16* %p6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %196) #4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true3, %land.lhs.true, %entry
  %197 = load i8, i8* %mask.addr, align 1, !tbaa !8
  %198 = load i8, i8* %thresh.addr, align 1, !tbaa !8
  %199 = load i8, i8* %flat.addr, align 1, !tbaa !8
  %200 = load i16*, i16** %op3.addr, align 4, !tbaa !2
  %201 = load i16*, i16** %op2.addr, align 4, !tbaa !2
  %202 = load i16*, i16** %op1.addr, align 4, !tbaa !2
  %203 = load i16*, i16** %op0.addr, align 4, !tbaa !2
  %204 = load i16*, i16** %oq0.addr, align 4, !tbaa !2
  %205 = load i16*, i16** %oq1.addr, align 4, !tbaa !2
  %206 = load i16*, i16** %oq2.addr, align 4, !tbaa !2
  %207 = load i16*, i16** %oq3.addr, align 4, !tbaa !2
  %208 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_filter8(i8 signext %197, i8 zeroext %198, i8 signext %199, i16* %200, i16* %201, i16* %202, i16* %203, i16* %204, i16* %205, i16* %206, i16* %207, i32 %208)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"short", !4, i64 0}
