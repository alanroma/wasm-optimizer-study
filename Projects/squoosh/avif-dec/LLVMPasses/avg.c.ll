; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/avg.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/avg.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @aom_minmax_8x8_c(i8* %s, i32 %p, i8* %d, i32 %dp, i32* %min, i32* %max) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %d.addr = alloca i8*, align 4
  %dp.addr = alloca i32, align 4
  %min.addr = alloca i32*, align 4
  %max.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %diff = alloca i32, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %d, i8** %d.addr, align 4, !tbaa !2
  store i32 %dp, i32* %dp.addr, align 4, !tbaa !6
  store i32* %min, i32** %min.addr, align 4, !tbaa !2
  store i32* %max, i32** %max.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load i32*, i32** %min.addr, align 4, !tbaa !2
  store i32 255, i32* %2, align 4, !tbaa !6
  %3 = load i32*, i32** %max.addr, align 4, !tbaa !2
  store i32 0, i32* %3, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc14, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, 8
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %j, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, 8
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %6 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %8 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %7, i32 %8
  %9 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %9 to i32
  %10 = load i8*, i8** %d.addr, align 4, !tbaa !2
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds i8, i8* %10, i32 %11
  %12 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = zext i8 %12 to i32
  %sub = sub nsw i32 %conv, %conv5
  %call = call i32 @abs(i32 %sub) #4
  store i32 %call, i32* %diff, align 4, !tbaa !6
  %13 = load i32, i32* %diff, align 4, !tbaa !6
  %14 = load i32*, i32** %min.addr, align 4, !tbaa !2
  %15 = load i32, i32* %14, align 4, !tbaa !6
  %cmp6 = icmp slt i32 %13, %15
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body3
  %16 = load i32, i32* %diff, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body3
  %17 = load i32*, i32** %min.addr, align 4, !tbaa !2
  %18 = load i32, i32* %17, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %16, %cond.true ], [ %18, %cond.false ]
  %19 = load i32*, i32** %min.addr, align 4, !tbaa !2
  store i32 %cond, i32* %19, align 4, !tbaa !6
  %20 = load i32, i32* %diff, align 4, !tbaa !6
  %21 = load i32*, i32** %max.addr, align 4, !tbaa !2
  %22 = load i32, i32* %21, align 4, !tbaa !6
  %cmp8 = icmp sgt i32 %20, %22
  br i1 %cmp8, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %cond.end
  %23 = load i32, i32* %diff, align 4, !tbaa !6
  br label %cond.end12

cond.false11:                                     ; preds = %cond.end
  %24 = load i32*, i32** %max.addr, align 4, !tbaa !2
  %25 = load i32, i32* %24, align 4, !tbaa !6
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false11, %cond.true10
  %cond13 = phi i32 [ %23, %cond.true10 ], [ %25, %cond.false11 ]
  %26 = load i32*, i32** %max.addr, align 4, !tbaa !2
  store i32 %cond13, i32* %26, align 4, !tbaa !6
  %27 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end12
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc14

for.inc14:                                        ; preds = %for.end
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %inc15 = add nsw i32 %29, 1
  store i32 %inc15, i32* %i, align 4, !tbaa !6
  %30 = load i32, i32* %p.addr, align 4, !tbaa !6
  %31 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %31, i32 %30
  store i8* %add.ptr, i8** %s.addr, align 4, !tbaa !2
  %32 = load i32, i32* %dp.addr, align 4, !tbaa !6
  %33 = load i8*, i8** %d.addr, align 4, !tbaa !2
  %add.ptr16 = getelementptr inbounds i8, i8* %33, i32 %32
  store i8* %add.ptr16, i8** %d.addr, align 4, !tbaa !2
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  %34 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #3
  %35 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @aom_avg_4x4_c(i8* %s, i32 %p) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %sum = alloca i32, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %sum, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc4, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, 4
  br i1 %cmp, label %for.body, label %for.end6

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %j, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %4, 4
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %7 to i32
  %8 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add nsw i32 %8, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !6
  %9 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc4

for.inc4:                                         ; preds = %for.end
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc5 = add nsw i32 %10, 1
  store i32 %inc5, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %p.addr, align 4, !tbaa !6
  %12 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %12, i32 %11
  store i8* %add.ptr, i8** %s.addr, align 4, !tbaa !2
  br label %for.cond

for.end6:                                         ; preds = %for.cond
  %13 = load i32, i32* %sum, align 4, !tbaa !6
  %add7 = add nsw i32 %13, 8
  %shr = ashr i32 %add7, 4
  %14 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  ret i32 %shr
}

; Function Attrs: nounwind
define hidden i32 @aom_avg_8x8_c(i8* %s, i32 %p) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %sum = alloca i32, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %sum, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc4, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, 8
  br i1 %cmp, label %for.body, label %for.end6

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %j, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %4, 8
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %5 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %7 to i32
  %8 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add nsw i32 %8, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !6
  %9 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc4

for.inc4:                                         ; preds = %for.end
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc5 = add nsw i32 %10, 1
  store i32 %inc5, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %p.addr, align 4, !tbaa !6
  %12 = load i8*, i8** %s.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %12, i32 %11
  store i8* %add.ptr, i8** %s.addr, align 4, !tbaa !2
  br label %for.cond

for.end6:                                         ; preds = %for.cond
  %13 = load i32, i32* %sum, align 4, !tbaa !6
  %add7 = add nsw i32 %13, 32
  %shr = ashr i32 %add7, 6
  %14 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  ret i32 %shr
}

; Function Attrs: nounwind
define hidden i32 @aom_highbd_avg_8x8_c(i8* %s8, i32 %p) #0 {
entry:
  %s8.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %sum = alloca i32, align 4
  %s = alloca i16*, align 4
  store i8* %s8, i8** %s8.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %sum, align 4, !tbaa !6
  %3 = bitcast i16** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i8*, i8** %s8.addr, align 4, !tbaa !2
  %5 = ptrtoint i8* %4 to i32
  %shl = shl i32 %5, 1
  %6 = inttoptr i32 %shl to i16*
  store i16* %6, i16** %s, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc4, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %7, 8
  br i1 %cmp, label %for.body, label %for.end6

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %8 = load i32, i32* %j, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %8, 8
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %9 = load i16*, i16** %s, align 4, !tbaa !2
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %9, i32 %10
  %11 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = zext i16 %11 to i32
  %12 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add nsw i32 %12, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !6
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc4

for.inc4:                                         ; preds = %for.end
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %inc5 = add nsw i32 %14, 1
  store i32 %inc5, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %p.addr, align 4, !tbaa !6
  %16 = load i16*, i16** %s, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %16, i32 %15
  store i16* %add.ptr, i16** %s, align 4, !tbaa !2
  br label %for.cond

for.end6:                                         ; preds = %for.cond
  %17 = load i32, i32* %sum, align 4, !tbaa !6
  %add7 = add nsw i32 %17, 32
  %shr = ashr i32 %add7, 6
  %18 = bitcast i16** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  ret i32 %shr
}

; Function Attrs: nounwind
define hidden i32 @aom_highbd_avg_4x4_c(i8* %s8, i32 %p) #0 {
entry:
  %s8.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %sum = alloca i32, align 4
  %s = alloca i16*, align 4
  store i8* %s8, i8** %s8.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %sum, align 4, !tbaa !6
  %3 = bitcast i16** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i8*, i8** %s8.addr, align 4, !tbaa !2
  %5 = ptrtoint i8* %4 to i32
  %shl = shl i32 %5, 1
  %6 = inttoptr i32 %shl to i16*
  store i16* %6, i16** %s, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc4, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %7, 4
  br i1 %cmp, label %for.body, label %for.end6

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %8 = load i32, i32* %j, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %8, 4
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %9 = load i16*, i16** %s, align 4, !tbaa !2
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %9, i32 %10
  %11 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = zext i16 %11 to i32
  %12 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add nsw i32 %12, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !6
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc4

for.inc4:                                         ; preds = %for.end
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %inc5 = add nsw i32 %14, 1
  store i32 %inc5, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %p.addr, align 4, !tbaa !6
  %16 = load i16*, i16** %s, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %16, i32 %15
  store i16* %add.ptr, i16** %s, align 4, !tbaa !2
  br label %for.cond

for.end6:                                         ; preds = %for.cond
  %17 = load i32, i32* %sum, align 4, !tbaa !6
  %add7 = add nsw i32 %17, 8
  %shr = ashr i32 %add7, 4
  %18 = bitcast i16** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  ret i32 %shr
}

; Function Attrs: nounwind
define hidden void @aom_highbd_minmax_8x8_c(i8* %s8, i32 %p, i8* %d8, i32 %dp, i32* %min, i32* %max) #0 {
entry:
  %s8.addr = alloca i8*, align 4
  %p.addr = alloca i32, align 4
  %d8.addr = alloca i8*, align 4
  %dp.addr = alloca i32, align 4
  %min.addr = alloca i32*, align 4
  %max.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %s = alloca i16*, align 4
  %d = alloca i16*, align 4
  %diff = alloca i32, align 4
  store i8* %s8, i8** %s8.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !6
  store i8* %d8, i8** %d8.addr, align 4, !tbaa !2
  store i32 %dp, i32* %dp.addr, align 4, !tbaa !6
  store i32* %min, i32** %min.addr, align 4, !tbaa !2
  store i32* %max, i32** %max.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i16** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i8*, i8** %s8.addr, align 4, !tbaa !2
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  store i16* %5, i16** %s, align 4, !tbaa !2
  %6 = bitcast i16** %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load i8*, i8** %d8.addr, align 4, !tbaa !2
  %8 = ptrtoint i8* %7 to i32
  %shl1 = shl i32 %8, 1
  %9 = inttoptr i32 %shl1 to i16*
  store i16* %9, i16** %d, align 4, !tbaa !2
  %10 = load i32*, i32** %min.addr, align 4, !tbaa !2
  store i32 255, i32* %10, align 4, !tbaa !6
  %11 = load i32*, i32** %max.addr, align 4, !tbaa !2
  store i32 0, i32* %11, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %12, 8
  br i1 %cmp, label %for.body, label %for.end18

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %13, 8
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %14 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load i16*, i16** %s, align 4, !tbaa !2
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %15, i32 %16
  %17 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = zext i16 %17 to i32
  %18 = load i16*, i16** %d, align 4, !tbaa !2
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i16, i16* %18, i32 %19
  %20 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  %conv6 = zext i16 %20 to i32
  %sub = sub nsw i32 %conv, %conv6
  %call = call i32 @abs(i32 %sub) #4
  store i32 %call, i32* %diff, align 4, !tbaa !6
  %21 = load i32, i32* %diff, align 4, !tbaa !6
  %22 = load i32*, i32** %min.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %21, %23
  br i1 %cmp7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body4
  %24 = load i32, i32* %diff, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body4
  %25 = load i32*, i32** %min.addr, align 4, !tbaa !2
  %26 = load i32, i32* %25, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %24, %cond.true ], [ %26, %cond.false ]
  %27 = load i32*, i32** %min.addr, align 4, !tbaa !2
  store i32 %cond, i32* %27, align 4, !tbaa !6
  %28 = load i32, i32* %diff, align 4, !tbaa !6
  %29 = load i32*, i32** %max.addr, align 4, !tbaa !2
  %30 = load i32, i32* %29, align 4, !tbaa !6
  %cmp9 = icmp sgt i32 %28, %30
  br i1 %cmp9, label %cond.true11, label %cond.false12

cond.true11:                                      ; preds = %cond.end
  %31 = load i32, i32* %diff, align 4, !tbaa !6
  br label %cond.end13

cond.false12:                                     ; preds = %cond.end
  %32 = load i32*, i32** %max.addr, align 4, !tbaa !2
  %33 = load i32, i32* %32, align 4, !tbaa !6
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false12, %cond.true11
  %cond14 = phi i32 [ %31, %cond.true11 ], [ %33, %cond.false12 ]
  %34 = load i32*, i32** %max.addr, align 4, !tbaa !2
  store i32 %cond14, i32* %34, align 4, !tbaa !6
  %35 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end13
  %36 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %inc16 = add nsw i32 %37, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !6
  %38 = load i32, i32* %p.addr, align 4, !tbaa !6
  %39 = load i16*, i16** %s, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %39, i32 %38
  store i16* %add.ptr, i16** %s, align 4, !tbaa !2
  %40 = load i32, i32* %dp.addr, align 4, !tbaa !6
  %41 = load i16*, i16** %d, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds i16, i16* %41, i32 %40
  store i16* %add.ptr17, i16** %d, align 4, !tbaa !2
  br label %for.cond

for.end18:                                        ; preds = %for.cond
  %42 = bitcast i16** %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #3
  %43 = bitcast i16** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  %44 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  %45 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_hadamard_8x8_c(i16* %src_diff, i32 %src_stride, i32* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i32*, align 4
  %idx = alloca i32, align 4
  %buffer = alloca [64 x i16], align 16
  %buffer2 = alloca [64 x i16], align 16
  %tmp_buf = alloca i16*, align 4
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i32* %coeff, i32** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast [64 x i16]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %1) #3
  %2 = bitcast [64 x i16]* %buffer2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %2) #3
  %3 = bitcast i16** %tmp_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %arrayidx = getelementptr inbounds [64 x i16], [64 x i16]* %buffer, i32 0, i32 0
  store i16* %arrayidx, i16** %tmp_buf, align 4, !tbaa !2
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %6 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %7 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  call void @hadamard_col8(i16* %5, i32 %6, i16* %7)
  %8 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %8, i32 8
  store i16* %add.ptr, i16** %tmp_buf, align 4, !tbaa !2
  %9 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %9, i32 1
  store i16* %incdec.ptr, i16** %src_diff.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arrayidx1 = getelementptr inbounds [64 x i16], [64 x i16]* %buffer, i32 0, i32 0
  store i16* %arrayidx1, i16** %tmp_buf, align 4, !tbaa !2
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc7, %for.end
  %11 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %11, 8
  br i1 %cmp3, label %for.body4, label %for.end9

for.body4:                                        ; preds = %for.cond2
  %12 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [64 x i16], [64 x i16]* %buffer2, i32 0, i32 0
  %13 = load i32, i32* %idx, align 4, !tbaa !6
  %mul = mul nsw i32 8, %13
  %add.ptr5 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul
  call void @hadamard_col8(i16* %12, i32 8, i16* %add.ptr5)
  %14 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  %incdec.ptr6 = getelementptr inbounds i16, i16* %14, i32 1
  store i16* %incdec.ptr6, i16** %tmp_buf, align 4, !tbaa !2
  br label %for.inc7

for.inc7:                                         ; preds = %for.body4
  %15 = load i32, i32* %idx, align 4, !tbaa !6
  %inc8 = add nsw i32 %15, 1
  store i32 %inc8, i32* %idx, align 4, !tbaa !6
  br label %for.cond2

for.end9:                                         ; preds = %for.cond2
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end9
  %16 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %16, 64
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %17 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds [64 x i16], [64 x i16]* %buffer2, i32 0, i32 %17
  %18 = load i16, i16* %arrayidx13, align 2, !tbaa !9
  %conv = sext i16 %18 to i32
  %19 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %20 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds i32, i32* %19, i32 %20
  store i32 %conv, i32* %arrayidx14, align 4, !tbaa !6
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %21 = load i32, i32* %idx, align 4, !tbaa !6
  %inc16 = add nsw i32 %21, 1
  store i32 %inc16, i32* %idx, align 4, !tbaa !6
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  %22 = bitcast i16** %tmp_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #3
  %23 = bitcast [64 x i16]* %buffer2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %23) #3
  %24 = bitcast [64 x i16]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %24) #3
  %25 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #3
  ret void
}

; Function Attrs: nounwind
define internal void @hadamard_col8(i16* %src_diff, i32 %src_stride, i16* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i16*, align 4
  %b0 = alloca i16, align 2
  %b1 = alloca i16, align 2
  %b2 = alloca i16, align 2
  %b3 = alloca i16, align 2
  %b4 = alloca i16, align 2
  %b5 = alloca i16, align 2
  %b6 = alloca i16, align 2
  %b7 = alloca i16, align 2
  %c0 = alloca i16, align 2
  %c1 = alloca i16, align 2
  %c2 = alloca i16, align 2
  %c3 = alloca i16, align 2
  %c4 = alloca i16, align 2
  %c5 = alloca i16, align 2
  %c6 = alloca i16, align 2
  %c7 = alloca i16, align 2
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i16* %coeff, i16** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i16* %b0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #3
  %1 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %2 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul = mul nsw i32 0, %2
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 %mul
  %3 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = sext i16 %3 to i32
  %4 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %5 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul1 = mul nsw i32 1, %5
  %arrayidx2 = getelementptr inbounds i16, i16* %4, i32 %mul1
  %6 = load i16, i16* %arrayidx2, align 2, !tbaa !9
  %conv3 = sext i16 %6 to i32
  %add = add nsw i32 %conv, %conv3
  %conv4 = trunc i32 %add to i16
  store i16 %conv4, i16* %b0, align 2, !tbaa !9
  %7 = bitcast i16* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %7) #3
  %8 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %9 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul5 = mul nsw i32 0, %9
  %arrayidx6 = getelementptr inbounds i16, i16* %8, i32 %mul5
  %10 = load i16, i16* %arrayidx6, align 2, !tbaa !9
  %conv7 = sext i16 %10 to i32
  %11 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %12 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul8 = mul nsw i32 1, %12
  %arrayidx9 = getelementptr inbounds i16, i16* %11, i32 %mul8
  %13 = load i16, i16* %arrayidx9, align 2, !tbaa !9
  %conv10 = sext i16 %13 to i32
  %sub = sub nsw i32 %conv7, %conv10
  %conv11 = trunc i32 %sub to i16
  store i16 %conv11, i16* %b1, align 2, !tbaa !9
  %14 = bitcast i16* %b2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %14) #3
  %15 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %16 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul12 = mul nsw i32 2, %16
  %arrayidx13 = getelementptr inbounds i16, i16* %15, i32 %mul12
  %17 = load i16, i16* %arrayidx13, align 2, !tbaa !9
  %conv14 = sext i16 %17 to i32
  %18 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %19 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul15 = mul nsw i32 3, %19
  %arrayidx16 = getelementptr inbounds i16, i16* %18, i32 %mul15
  %20 = load i16, i16* %arrayidx16, align 2, !tbaa !9
  %conv17 = sext i16 %20 to i32
  %add18 = add nsw i32 %conv14, %conv17
  %conv19 = trunc i32 %add18 to i16
  store i16 %conv19, i16* %b2, align 2, !tbaa !9
  %21 = bitcast i16* %b3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %21) #3
  %22 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %23 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul20 = mul nsw i32 2, %23
  %arrayidx21 = getelementptr inbounds i16, i16* %22, i32 %mul20
  %24 = load i16, i16* %arrayidx21, align 2, !tbaa !9
  %conv22 = sext i16 %24 to i32
  %25 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %26 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul23 = mul nsw i32 3, %26
  %arrayidx24 = getelementptr inbounds i16, i16* %25, i32 %mul23
  %27 = load i16, i16* %arrayidx24, align 2, !tbaa !9
  %conv25 = sext i16 %27 to i32
  %sub26 = sub nsw i32 %conv22, %conv25
  %conv27 = trunc i32 %sub26 to i16
  store i16 %conv27, i16* %b3, align 2, !tbaa !9
  %28 = bitcast i16* %b4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %28) #3
  %29 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %30 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul28 = mul nsw i32 4, %30
  %arrayidx29 = getelementptr inbounds i16, i16* %29, i32 %mul28
  %31 = load i16, i16* %arrayidx29, align 2, !tbaa !9
  %conv30 = sext i16 %31 to i32
  %32 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %33 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul31 = mul nsw i32 5, %33
  %arrayidx32 = getelementptr inbounds i16, i16* %32, i32 %mul31
  %34 = load i16, i16* %arrayidx32, align 2, !tbaa !9
  %conv33 = sext i16 %34 to i32
  %add34 = add nsw i32 %conv30, %conv33
  %conv35 = trunc i32 %add34 to i16
  store i16 %conv35, i16* %b4, align 2, !tbaa !9
  %35 = bitcast i16* %b5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %35) #3
  %36 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %37 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul36 = mul nsw i32 4, %37
  %arrayidx37 = getelementptr inbounds i16, i16* %36, i32 %mul36
  %38 = load i16, i16* %arrayidx37, align 2, !tbaa !9
  %conv38 = sext i16 %38 to i32
  %39 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %40 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul39 = mul nsw i32 5, %40
  %arrayidx40 = getelementptr inbounds i16, i16* %39, i32 %mul39
  %41 = load i16, i16* %arrayidx40, align 2, !tbaa !9
  %conv41 = sext i16 %41 to i32
  %sub42 = sub nsw i32 %conv38, %conv41
  %conv43 = trunc i32 %sub42 to i16
  store i16 %conv43, i16* %b5, align 2, !tbaa !9
  %42 = bitcast i16* %b6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %42) #3
  %43 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %44 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul44 = mul nsw i32 6, %44
  %arrayidx45 = getelementptr inbounds i16, i16* %43, i32 %mul44
  %45 = load i16, i16* %arrayidx45, align 2, !tbaa !9
  %conv46 = sext i16 %45 to i32
  %46 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %47 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul47 = mul nsw i32 7, %47
  %arrayidx48 = getelementptr inbounds i16, i16* %46, i32 %mul47
  %48 = load i16, i16* %arrayidx48, align 2, !tbaa !9
  %conv49 = sext i16 %48 to i32
  %add50 = add nsw i32 %conv46, %conv49
  %conv51 = trunc i32 %add50 to i16
  store i16 %conv51, i16* %b6, align 2, !tbaa !9
  %49 = bitcast i16* %b7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %49) #3
  %50 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %51 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul52 = mul nsw i32 6, %51
  %arrayidx53 = getelementptr inbounds i16, i16* %50, i32 %mul52
  %52 = load i16, i16* %arrayidx53, align 2, !tbaa !9
  %conv54 = sext i16 %52 to i32
  %53 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %54 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul55 = mul nsw i32 7, %54
  %arrayidx56 = getelementptr inbounds i16, i16* %53, i32 %mul55
  %55 = load i16, i16* %arrayidx56, align 2, !tbaa !9
  %conv57 = sext i16 %55 to i32
  %sub58 = sub nsw i32 %conv54, %conv57
  %conv59 = trunc i32 %sub58 to i16
  store i16 %conv59, i16* %b7, align 2, !tbaa !9
  %56 = bitcast i16* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %56) #3
  %57 = load i16, i16* %b0, align 2, !tbaa !9
  %conv60 = sext i16 %57 to i32
  %58 = load i16, i16* %b2, align 2, !tbaa !9
  %conv61 = sext i16 %58 to i32
  %add62 = add nsw i32 %conv60, %conv61
  %conv63 = trunc i32 %add62 to i16
  store i16 %conv63, i16* %c0, align 2, !tbaa !9
  %59 = bitcast i16* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %59) #3
  %60 = load i16, i16* %b1, align 2, !tbaa !9
  %conv64 = sext i16 %60 to i32
  %61 = load i16, i16* %b3, align 2, !tbaa !9
  %conv65 = sext i16 %61 to i32
  %add66 = add nsw i32 %conv64, %conv65
  %conv67 = trunc i32 %add66 to i16
  store i16 %conv67, i16* %c1, align 2, !tbaa !9
  %62 = bitcast i16* %c2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %62) #3
  %63 = load i16, i16* %b0, align 2, !tbaa !9
  %conv68 = sext i16 %63 to i32
  %64 = load i16, i16* %b2, align 2, !tbaa !9
  %conv69 = sext i16 %64 to i32
  %sub70 = sub nsw i32 %conv68, %conv69
  %conv71 = trunc i32 %sub70 to i16
  store i16 %conv71, i16* %c2, align 2, !tbaa !9
  %65 = bitcast i16* %c3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %65) #3
  %66 = load i16, i16* %b1, align 2, !tbaa !9
  %conv72 = sext i16 %66 to i32
  %67 = load i16, i16* %b3, align 2, !tbaa !9
  %conv73 = sext i16 %67 to i32
  %sub74 = sub nsw i32 %conv72, %conv73
  %conv75 = trunc i32 %sub74 to i16
  store i16 %conv75, i16* %c3, align 2, !tbaa !9
  %68 = bitcast i16* %c4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %68) #3
  %69 = load i16, i16* %b4, align 2, !tbaa !9
  %conv76 = sext i16 %69 to i32
  %70 = load i16, i16* %b6, align 2, !tbaa !9
  %conv77 = sext i16 %70 to i32
  %add78 = add nsw i32 %conv76, %conv77
  %conv79 = trunc i32 %add78 to i16
  store i16 %conv79, i16* %c4, align 2, !tbaa !9
  %71 = bitcast i16* %c5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %71) #3
  %72 = load i16, i16* %b5, align 2, !tbaa !9
  %conv80 = sext i16 %72 to i32
  %73 = load i16, i16* %b7, align 2, !tbaa !9
  %conv81 = sext i16 %73 to i32
  %add82 = add nsw i32 %conv80, %conv81
  %conv83 = trunc i32 %add82 to i16
  store i16 %conv83, i16* %c5, align 2, !tbaa !9
  %74 = bitcast i16* %c6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %74) #3
  %75 = load i16, i16* %b4, align 2, !tbaa !9
  %conv84 = sext i16 %75 to i32
  %76 = load i16, i16* %b6, align 2, !tbaa !9
  %conv85 = sext i16 %76 to i32
  %sub86 = sub nsw i32 %conv84, %conv85
  %conv87 = trunc i32 %sub86 to i16
  store i16 %conv87, i16* %c6, align 2, !tbaa !9
  %77 = bitcast i16* %c7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %77) #3
  %78 = load i16, i16* %b5, align 2, !tbaa !9
  %conv88 = sext i16 %78 to i32
  %79 = load i16, i16* %b7, align 2, !tbaa !9
  %conv89 = sext i16 %79 to i32
  %sub90 = sub nsw i32 %conv88, %conv89
  %conv91 = trunc i32 %sub90 to i16
  store i16 %conv91, i16* %c7, align 2, !tbaa !9
  %80 = load i16, i16* %c0, align 2, !tbaa !9
  %conv92 = sext i16 %80 to i32
  %81 = load i16, i16* %c4, align 2, !tbaa !9
  %conv93 = sext i16 %81 to i32
  %add94 = add nsw i32 %conv92, %conv93
  %conv95 = trunc i32 %add94 to i16
  %82 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i16, i16* %82, i32 0
  store i16 %conv95, i16* %arrayidx96, align 2, !tbaa !9
  %83 = load i16, i16* %c1, align 2, !tbaa !9
  %conv97 = sext i16 %83 to i32
  %84 = load i16, i16* %c5, align 2, !tbaa !9
  %conv98 = sext i16 %84 to i32
  %add99 = add nsw i32 %conv97, %conv98
  %conv100 = trunc i32 %add99 to i16
  %85 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i16, i16* %85, i32 7
  store i16 %conv100, i16* %arrayidx101, align 2, !tbaa !9
  %86 = load i16, i16* %c2, align 2, !tbaa !9
  %conv102 = sext i16 %86 to i32
  %87 = load i16, i16* %c6, align 2, !tbaa !9
  %conv103 = sext i16 %87 to i32
  %add104 = add nsw i32 %conv102, %conv103
  %conv105 = trunc i32 %add104 to i16
  %88 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i16, i16* %88, i32 3
  store i16 %conv105, i16* %arrayidx106, align 2, !tbaa !9
  %89 = load i16, i16* %c3, align 2, !tbaa !9
  %conv107 = sext i16 %89 to i32
  %90 = load i16, i16* %c7, align 2, !tbaa !9
  %conv108 = sext i16 %90 to i32
  %add109 = add nsw i32 %conv107, %conv108
  %conv110 = trunc i32 %add109 to i16
  %91 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i16, i16* %91, i32 4
  store i16 %conv110, i16* %arrayidx111, align 2, !tbaa !9
  %92 = load i16, i16* %c0, align 2, !tbaa !9
  %conv112 = sext i16 %92 to i32
  %93 = load i16, i16* %c4, align 2, !tbaa !9
  %conv113 = sext i16 %93 to i32
  %sub114 = sub nsw i32 %conv112, %conv113
  %conv115 = trunc i32 %sub114 to i16
  %94 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx116 = getelementptr inbounds i16, i16* %94, i32 2
  store i16 %conv115, i16* %arrayidx116, align 2, !tbaa !9
  %95 = load i16, i16* %c1, align 2, !tbaa !9
  %conv117 = sext i16 %95 to i32
  %96 = load i16, i16* %c5, align 2, !tbaa !9
  %conv118 = sext i16 %96 to i32
  %sub119 = sub nsw i32 %conv117, %conv118
  %conv120 = trunc i32 %sub119 to i16
  %97 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i16, i16* %97, i32 6
  store i16 %conv120, i16* %arrayidx121, align 2, !tbaa !9
  %98 = load i16, i16* %c2, align 2, !tbaa !9
  %conv122 = sext i16 %98 to i32
  %99 = load i16, i16* %c6, align 2, !tbaa !9
  %conv123 = sext i16 %99 to i32
  %sub124 = sub nsw i32 %conv122, %conv123
  %conv125 = trunc i32 %sub124 to i16
  %100 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx126 = getelementptr inbounds i16, i16* %100, i32 1
  store i16 %conv125, i16* %arrayidx126, align 2, !tbaa !9
  %101 = load i16, i16* %c3, align 2, !tbaa !9
  %conv127 = sext i16 %101 to i32
  %102 = load i16, i16* %c7, align 2, !tbaa !9
  %conv128 = sext i16 %102 to i32
  %sub129 = sub nsw i32 %conv127, %conv128
  %conv130 = trunc i32 %sub129 to i16
  %103 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i16, i16* %103, i32 5
  store i16 %conv130, i16* %arrayidx131, align 2, !tbaa !9
  %104 = bitcast i16* %c7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %104) #3
  %105 = bitcast i16* %c6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %105) #3
  %106 = bitcast i16* %c5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %106) #3
  %107 = bitcast i16* %c4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %107) #3
  %108 = bitcast i16* %c3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %108) #3
  %109 = bitcast i16* %c2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %109) #3
  %110 = bitcast i16* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %110) #3
  %111 = bitcast i16* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %111) #3
  %112 = bitcast i16* %b7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %112) #3
  %113 = bitcast i16* %b6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %113) #3
  %114 = bitcast i16* %b5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %114) #3
  %115 = bitcast i16* %b4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %115) #3
  %116 = bitcast i16* %b3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %116) #3
  %117 = bitcast i16* %b2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %117) #3
  %118 = bitcast i16* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %118) #3
  %119 = bitcast i16* %b0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %119) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_hadamard_lp_8x8_c(i16* %src_diff, i32 %src_stride, i16* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i16*, align 4
  %buffer = alloca [64 x i16], align 16
  %buffer2 = alloca [64 x i16], align 16
  %tmp_buf = alloca i16*, align 4
  %idx = alloca i32, align 4
  %idx2 = alloca i32, align 4
  %idx12 = alloca i32, align 4
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i16* %coeff, i16** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast [64 x i16]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %0) #3
  %1 = bitcast [64 x i16]* %buffer2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %1) #3
  %2 = bitcast i16** %tmp_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %arrayidx = getelementptr inbounds [64 x i16], [64 x i16]* %buffer, i32 0, i32 0
  store i16* %arrayidx, i16** %tmp_buf, align 4, !tbaa !2
  %3 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %7 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %8 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  call void @hadamard_col8(i16* %6, i32 %7, i16* %8)
  %9 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %9, i32 8
  store i16* %add.ptr, i16** %tmp_buf, align 4, !tbaa !2
  %10 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %10, i32 1
  store i16* %incdec.ptr, i16** %src_diff.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %arrayidx1 = getelementptr inbounds [64 x i16], [64 x i16]* %buffer, i32 0, i32 0
  store i16* %arrayidx1, i16** %tmp_buf, align 4, !tbaa !2
  %12 = bitcast i32* %idx2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  store i32 0, i32* %idx2, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc9, %for.end
  %13 = load i32, i32* %idx2, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %13, 8
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond3
  %14 = bitcast i32* %idx2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  br label %for.end11

for.body6:                                        ; preds = %for.cond3
  %15 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [64 x i16], [64 x i16]* %buffer2, i32 0, i32 0
  %16 = load i32, i32* %idx2, align 4, !tbaa !6
  %mul = mul nsw i32 8, %16
  %add.ptr7 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul
  call void @hadamard_col8(i16* %15, i32 8, i16* %add.ptr7)
  %17 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i16, i16* %17, i32 1
  store i16* %incdec.ptr8, i16** %tmp_buf, align 4, !tbaa !2
  br label %for.inc9

for.inc9:                                         ; preds = %for.body6
  %18 = load i32, i32* %idx2, align 4, !tbaa !6
  %inc10 = add nsw i32 %18, 1
  store i32 %inc10, i32* %idx2, align 4, !tbaa !6
  br label %for.cond3

for.end11:                                        ; preds = %for.cond.cleanup5
  %19 = bitcast i32* %idx12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  store i32 0, i32* %idx12, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc19, %for.end11
  %20 = load i32, i32* %idx12, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %20, 64
  br i1 %cmp14, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond13
  %21 = bitcast i32* %idx12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  br label %for.end21

for.body16:                                       ; preds = %for.cond13
  %22 = load i32, i32* %idx12, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [64 x i16], [64 x i16]* %buffer2, i32 0, i32 %22
  %23 = load i16, i16* %arrayidx17, align 2, !tbaa !9
  %24 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %25 = load i32, i32* %idx12, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds i16, i16* %24, i32 %25
  store i16 %23, i16* %arrayidx18, align 2, !tbaa !9
  br label %for.inc19

for.inc19:                                        ; preds = %for.body16
  %26 = load i32, i32* %idx12, align 4, !tbaa !6
  %inc20 = add nsw i32 %26, 1
  store i32 %inc20, i32* %idx12, align 4, !tbaa !6
  br label %for.cond13

for.end21:                                        ; preds = %for.cond.cleanup15
  %27 = bitcast i16** %tmp_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  %28 = bitcast [64 x i16]* %buffer2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %28) #3
  %29 = bitcast [64 x i16]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %29) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_hadamard_16x16_c(i16* %src_diff, i32 %src_stride, i32* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i32*, align 4
  %idx = alloca i32, align 4
  %src_ptr = alloca i16*, align 4
  %a0 = alloca i32, align 4
  %a1 = alloca i32, align 4
  %a2 = alloca i32, align 4
  %a3 = alloca i32, align 4
  %b0 = alloca i32, align 4
  %b1 = alloca i32, align 4
  %b2 = alloca i32, align 4
  %b3 = alloca i32, align 4
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i32* %coeff, i32** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %4 = load i32, i32* %idx, align 4, !tbaa !6
  %shr = ashr i32 %4, 1
  %mul = mul nsw i32 %shr, 8
  %5 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul1 = mul nsw i32 %mul, %5
  %add.ptr = getelementptr inbounds i16, i16* %3, i32 %mul1
  %6 = load i32, i32* %idx, align 4, !tbaa !6
  %and = and i32 %6, 1
  %mul2 = mul nsw i32 %and, 8
  %add.ptr3 = getelementptr inbounds i16, i16* %add.ptr, i32 %mul2
  store i16* %add.ptr3, i16** %src_ptr, align 4, !tbaa !2
  %7 = load i16*, i16** %src_ptr, align 4, !tbaa !2
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %9 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %10 = load i32, i32* %idx, align 4, !tbaa !6
  %mul4 = mul nsw i32 %10, 64
  %add.ptr5 = getelementptr inbounds i32, i32* %9, i32 %mul4
  call void @aom_hadamard_8x8_c(i16* %7, i32 %8, i32* %add.ptr5)
  %11 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc26, %for.end
  %13 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %13, 64
  br i1 %cmp7, label %for.body8, label %for.end28

for.body8:                                        ; preds = %for.cond6
  %14 = bitcast i32* %a0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 0
  %16 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %16, i32* %a0, align 4, !tbaa !6
  %17 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %18, i32 64
  %19 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  store i32 %19, i32* %a1, align 4, !tbaa !6
  %20 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %21, i32 128
  %22 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  store i32 %22, i32* %a2, align 4, !tbaa !6
  %23 = bitcast i32* %a3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %24, i32 192
  %25 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  store i32 %25, i32* %a3, align 4, !tbaa !6
  %26 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load i32, i32* %a0, align 4, !tbaa !6
  %28 = load i32, i32* %a1, align 4, !tbaa !6
  %add = add nsw i32 %27, %28
  %shr12 = ashr i32 %add, 1
  store i32 %shr12, i32* %b0, align 4, !tbaa !6
  %29 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #3
  %30 = load i32, i32* %a0, align 4, !tbaa !6
  %31 = load i32, i32* %a1, align 4, !tbaa !6
  %sub = sub nsw i32 %30, %31
  %shr13 = ashr i32 %sub, 1
  store i32 %shr13, i32* %b1, align 4, !tbaa !6
  %32 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load i32, i32* %a2, align 4, !tbaa !6
  %34 = load i32, i32* %a3, align 4, !tbaa !6
  %add14 = add nsw i32 %33, %34
  %shr15 = ashr i32 %add14, 1
  store i32 %shr15, i32* %b2, align 4, !tbaa !6
  %35 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #3
  %36 = load i32, i32* %a2, align 4, !tbaa !6
  %37 = load i32, i32* %a3, align 4, !tbaa !6
  %sub16 = sub nsw i32 %36, %37
  %shr17 = ashr i32 %sub16, 1
  store i32 %shr17, i32* %b3, align 4, !tbaa !6
  %38 = load i32, i32* %b0, align 4, !tbaa !6
  %39 = load i32, i32* %b2, align 4, !tbaa !6
  %add18 = add nsw i32 %38, %39
  %40 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %40, i32 0
  store i32 %add18, i32* %arrayidx19, align 4, !tbaa !6
  %41 = load i32, i32* %b1, align 4, !tbaa !6
  %42 = load i32, i32* %b3, align 4, !tbaa !6
  %add20 = add nsw i32 %41, %42
  %43 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %43, i32 64
  store i32 %add20, i32* %arrayidx21, align 4, !tbaa !6
  %44 = load i32, i32* %b0, align 4, !tbaa !6
  %45 = load i32, i32* %b2, align 4, !tbaa !6
  %sub22 = sub nsw i32 %44, %45
  %46 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %46, i32 128
  store i32 %sub22, i32* %arrayidx23, align 4, !tbaa !6
  %47 = load i32, i32* %b1, align 4, !tbaa !6
  %48 = load i32, i32* %b3, align 4, !tbaa !6
  %sub24 = sub nsw i32 %47, %48
  %49 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %49, i32 192
  store i32 %sub24, i32* %arrayidx25, align 4, !tbaa !6
  %50 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %50, i32 1
  store i32* %incdec.ptr, i32** %coeff.addr, align 4, !tbaa !2
  %51 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  %55 = bitcast i32* %a3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %a0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  br label %for.inc26

for.inc26:                                        ; preds = %for.body8
  %59 = load i32, i32* %idx, align 4, !tbaa !6
  %inc27 = add nsw i32 %59, 1
  store i32 %inc27, i32* %idx, align 4, !tbaa !6
  br label %for.cond6

for.end28:                                        ; preds = %for.cond6
  %60 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_hadamard_lp_16x16_c(i16* %src_diff, i32 %src_stride, i16* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i16*, align 4
  %idx = alloca i32, align 4
  %src_ptr = alloca i16*, align 4
  %idx6 = alloca i32, align 4
  %a0 = alloca i16, align 2
  %a1 = alloca i16, align 2
  %a2 = alloca i16, align 2
  %a3 = alloca i16, align 2
  %b0 = alloca i16, align 2
  %b1 = alloca i16, align 2
  %b2 = alloca i16, align 2
  %b3 = alloca i16, align 2
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i16* %coeff, i16** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %5 = load i32, i32* %idx, align 4, !tbaa !6
  %shr = ashr i32 %5, 1
  %mul = mul nsw i32 %shr, 8
  %6 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul1 = mul nsw i32 %mul, %6
  %add.ptr = getelementptr inbounds i16, i16* %4, i32 %mul1
  %7 = load i32, i32* %idx, align 4, !tbaa !6
  %and = and i32 %7, 1
  %mul2 = mul nsw i32 %and, 8
  %add.ptr3 = getelementptr inbounds i16, i16* %add.ptr, i32 %mul2
  store i16* %add.ptr3, i16** %src_ptr, align 4, !tbaa !2
  %8 = load i16*, i16** %src_ptr, align 4, !tbaa !2
  %9 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %10 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %11 = load i32, i32* %idx, align 4, !tbaa !6
  %mul4 = mul nsw i32 %11, 64
  %add.ptr5 = getelementptr inbounds i16, i16* %10, i32 %mul4
  call void @aom_hadamard_lp_8x8_c(i16* %8, i32 %9, i16* %add.ptr5)
  %12 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = bitcast i32* %idx6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  store i32 0, i32* %idx6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc51, %for.end
  %15 = load i32, i32* %idx6, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, 64
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %16 = bitcast i32* %idx6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  br label %for.end53

for.body10:                                       ; preds = %for.cond7
  %17 = bitcast i16* %a0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %17) #3
  %18 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %18, i32 0
  %19 = load i16, i16* %arrayidx, align 2, !tbaa !9
  store i16 %19, i16* %a0, align 2, !tbaa !9
  %20 = bitcast i16* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %20) #3
  %21 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i16, i16* %21, i32 64
  %22 = load i16, i16* %arrayidx11, align 2, !tbaa !9
  store i16 %22, i16* %a1, align 2, !tbaa !9
  %23 = bitcast i16* %a2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %23) #3
  %24 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i16, i16* %24, i32 128
  %25 = load i16, i16* %arrayidx12, align 2, !tbaa !9
  store i16 %25, i16* %a2, align 2, !tbaa !9
  %26 = bitcast i16* %a3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %26) #3
  %27 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i16, i16* %27, i32 192
  %28 = load i16, i16* %arrayidx13, align 2, !tbaa !9
  store i16 %28, i16* %a3, align 2, !tbaa !9
  %29 = bitcast i16* %b0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %29) #3
  %30 = load i16, i16* %a0, align 2, !tbaa !9
  %conv = sext i16 %30 to i32
  %31 = load i16, i16* %a1, align 2, !tbaa !9
  %conv14 = sext i16 %31 to i32
  %add = add nsw i32 %conv, %conv14
  %shr15 = ashr i32 %add, 1
  %conv16 = trunc i32 %shr15 to i16
  store i16 %conv16, i16* %b0, align 2, !tbaa !9
  %32 = bitcast i16* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %32) #3
  %33 = load i16, i16* %a0, align 2, !tbaa !9
  %conv17 = sext i16 %33 to i32
  %34 = load i16, i16* %a1, align 2, !tbaa !9
  %conv18 = sext i16 %34 to i32
  %sub = sub nsw i32 %conv17, %conv18
  %shr19 = ashr i32 %sub, 1
  %conv20 = trunc i32 %shr19 to i16
  store i16 %conv20, i16* %b1, align 2, !tbaa !9
  %35 = bitcast i16* %b2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %35) #3
  %36 = load i16, i16* %a2, align 2, !tbaa !9
  %conv21 = sext i16 %36 to i32
  %37 = load i16, i16* %a3, align 2, !tbaa !9
  %conv22 = sext i16 %37 to i32
  %add23 = add nsw i32 %conv21, %conv22
  %shr24 = ashr i32 %add23, 1
  %conv25 = trunc i32 %shr24 to i16
  store i16 %conv25, i16* %b2, align 2, !tbaa !9
  %38 = bitcast i16* %b3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %38) #3
  %39 = load i16, i16* %a2, align 2, !tbaa !9
  %conv26 = sext i16 %39 to i32
  %40 = load i16, i16* %a3, align 2, !tbaa !9
  %conv27 = sext i16 %40 to i32
  %sub28 = sub nsw i32 %conv26, %conv27
  %shr29 = ashr i32 %sub28, 1
  %conv30 = trunc i32 %shr29 to i16
  store i16 %conv30, i16* %b3, align 2, !tbaa !9
  %41 = load i16, i16* %b0, align 2, !tbaa !9
  %conv31 = sext i16 %41 to i32
  %42 = load i16, i16* %b2, align 2, !tbaa !9
  %conv32 = sext i16 %42 to i32
  %add33 = add nsw i32 %conv31, %conv32
  %conv34 = trunc i32 %add33 to i16
  %43 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i16, i16* %43, i32 0
  store i16 %conv34, i16* %arrayidx35, align 2, !tbaa !9
  %44 = load i16, i16* %b1, align 2, !tbaa !9
  %conv36 = sext i16 %44 to i32
  %45 = load i16, i16* %b3, align 2, !tbaa !9
  %conv37 = sext i16 %45 to i32
  %add38 = add nsw i32 %conv36, %conv37
  %conv39 = trunc i32 %add38 to i16
  %46 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i16, i16* %46, i32 64
  store i16 %conv39, i16* %arrayidx40, align 2, !tbaa !9
  %47 = load i16, i16* %b0, align 2, !tbaa !9
  %conv41 = sext i16 %47 to i32
  %48 = load i16, i16* %b2, align 2, !tbaa !9
  %conv42 = sext i16 %48 to i32
  %sub43 = sub nsw i32 %conv41, %conv42
  %conv44 = trunc i32 %sub43 to i16
  %49 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i16, i16* %49, i32 128
  store i16 %conv44, i16* %arrayidx45, align 2, !tbaa !9
  %50 = load i16, i16* %b1, align 2, !tbaa !9
  %conv46 = sext i16 %50 to i32
  %51 = load i16, i16* %b3, align 2, !tbaa !9
  %conv47 = sext i16 %51 to i32
  %sub48 = sub nsw i32 %conv46, %conv47
  %conv49 = trunc i32 %sub48 to i16
  %52 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i16, i16* %52, i32 192
  store i16 %conv49, i16* %arrayidx50, align 2, !tbaa !9
  %53 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %53, i32 1
  store i16* %incdec.ptr, i16** %coeff.addr, align 4, !tbaa !2
  %54 = bitcast i16* %b3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %54) #3
  %55 = bitcast i16* %b2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %55) #3
  %56 = bitcast i16* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %56) #3
  %57 = bitcast i16* %b0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %57) #3
  %58 = bitcast i16* %a3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %58) #3
  %59 = bitcast i16* %a2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %59) #3
  %60 = bitcast i16* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %60) #3
  %61 = bitcast i16* %a0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %61) #3
  br label %for.inc51

for.inc51:                                        ; preds = %for.body10
  %62 = load i32, i32* %idx6, align 4, !tbaa !6
  %inc52 = add nsw i32 %62, 1
  store i32 %inc52, i32* %idx6, align 4, !tbaa !6
  br label %for.cond7

for.end53:                                        ; preds = %for.cond.cleanup9
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_hadamard_32x32_c(i16* %src_diff, i32 %src_stride, i32* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i32*, align 4
  %idx = alloca i32, align 4
  %src_ptr = alloca i16*, align 4
  %a0 = alloca i32, align 4
  %a1 = alloca i32, align 4
  %a2 = alloca i32, align 4
  %a3 = alloca i32, align 4
  %b0 = alloca i32, align 4
  %b1 = alloca i32, align 4
  %b2 = alloca i32, align 4
  %b3 = alloca i32, align 4
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i32* %coeff, i32** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %4 = load i32, i32* %idx, align 4, !tbaa !6
  %shr = ashr i32 %4, 1
  %mul = mul nsw i32 %shr, 16
  %5 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul1 = mul nsw i32 %mul, %5
  %add.ptr = getelementptr inbounds i16, i16* %3, i32 %mul1
  %6 = load i32, i32* %idx, align 4, !tbaa !6
  %and = and i32 %6, 1
  %mul2 = mul nsw i32 %and, 16
  %add.ptr3 = getelementptr inbounds i16, i16* %add.ptr, i32 %mul2
  store i16* %add.ptr3, i16** %src_ptr, align 4, !tbaa !2
  %7 = load i16*, i16** %src_ptr, align 4, !tbaa !2
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %9 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %10 = load i32, i32* %idx, align 4, !tbaa !6
  %mul4 = mul nsw i32 %10, 256
  %add.ptr5 = getelementptr inbounds i32, i32* %9, i32 %mul4
  call void @aom_hadamard_16x16_c(i16* %7, i32 %8, i32* %add.ptr5)
  %11 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc26, %for.end
  %13 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %13, 256
  br i1 %cmp7, label %for.body8, label %for.end28

for.body8:                                        ; preds = %for.cond6
  %14 = bitcast i32* %a0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 0
  %16 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %16, i32* %a0, align 4, !tbaa !6
  %17 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %18, i32 256
  %19 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  store i32 %19, i32* %a1, align 4, !tbaa !6
  %20 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %21, i32 512
  %22 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  store i32 %22, i32* %a2, align 4, !tbaa !6
  %23 = bitcast i32* %a3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %24, i32 768
  %25 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  store i32 %25, i32* %a3, align 4, !tbaa !6
  %26 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load i32, i32* %a0, align 4, !tbaa !6
  %28 = load i32, i32* %a1, align 4, !tbaa !6
  %add = add nsw i32 %27, %28
  %shr12 = ashr i32 %add, 2
  store i32 %shr12, i32* %b0, align 4, !tbaa !6
  %29 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #3
  %30 = load i32, i32* %a0, align 4, !tbaa !6
  %31 = load i32, i32* %a1, align 4, !tbaa !6
  %sub = sub nsw i32 %30, %31
  %shr13 = ashr i32 %sub, 2
  store i32 %shr13, i32* %b1, align 4, !tbaa !6
  %32 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load i32, i32* %a2, align 4, !tbaa !6
  %34 = load i32, i32* %a3, align 4, !tbaa !6
  %add14 = add nsw i32 %33, %34
  %shr15 = ashr i32 %add14, 2
  store i32 %shr15, i32* %b2, align 4, !tbaa !6
  %35 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #3
  %36 = load i32, i32* %a2, align 4, !tbaa !6
  %37 = load i32, i32* %a3, align 4, !tbaa !6
  %sub16 = sub nsw i32 %36, %37
  %shr17 = ashr i32 %sub16, 2
  store i32 %shr17, i32* %b3, align 4, !tbaa !6
  %38 = load i32, i32* %b0, align 4, !tbaa !6
  %39 = load i32, i32* %b2, align 4, !tbaa !6
  %add18 = add nsw i32 %38, %39
  %40 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %40, i32 0
  store i32 %add18, i32* %arrayidx19, align 4, !tbaa !6
  %41 = load i32, i32* %b1, align 4, !tbaa !6
  %42 = load i32, i32* %b3, align 4, !tbaa !6
  %add20 = add nsw i32 %41, %42
  %43 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %43, i32 256
  store i32 %add20, i32* %arrayidx21, align 4, !tbaa !6
  %44 = load i32, i32* %b0, align 4, !tbaa !6
  %45 = load i32, i32* %b2, align 4, !tbaa !6
  %sub22 = sub nsw i32 %44, %45
  %46 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %46, i32 512
  store i32 %sub22, i32* %arrayidx23, align 4, !tbaa !6
  %47 = load i32, i32* %b1, align 4, !tbaa !6
  %48 = load i32, i32* %b3, align 4, !tbaa !6
  %sub24 = sub nsw i32 %47, %48
  %49 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %49, i32 768
  store i32 %sub24, i32* %arrayidx25, align 4, !tbaa !6
  %50 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %50, i32 1
  store i32* %incdec.ptr, i32** %coeff.addr, align 4, !tbaa !2
  %51 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  %55 = bitcast i32* %a3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %a0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  br label %for.inc26

for.inc26:                                        ; preds = %for.body8
  %59 = load i32, i32* %idx, align 4, !tbaa !6
  %inc27 = add nsw i32 %59, 1
  store i32 %inc27, i32* %idx, align 4, !tbaa !6
  br label %for.cond6

for.end28:                                        ; preds = %for.cond6
  %60 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_hadamard_8x8_c(i16* %src_diff, i32 %src_stride, i32* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i32*, align 4
  %idx = alloca i32, align 4
  %buffer = alloca [64 x i16], align 16
  %buffer2 = alloca [64 x i32], align 16
  %tmp_buf = alloca i16*, align 4
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i32* %coeff, i32** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast [64 x i16]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %1) #3
  %2 = bitcast [64 x i32]* %buffer2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %2) #3
  %3 = bitcast i16** %tmp_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %arrayidx = getelementptr inbounds [64 x i16], [64 x i16]* %buffer, i32 0, i32 0
  store i16* %arrayidx, i16** %tmp_buf, align 4, !tbaa !2
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %6 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %7 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  call void @hadamard_highbd_col8_first_pass(i16* %5, i32 %6, i16* %7)
  %8 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %8, i32 8
  store i16* %add.ptr, i16** %tmp_buf, align 4, !tbaa !2
  %9 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %9, i32 1
  store i16* %incdec.ptr, i16** %src_diff.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arrayidx1 = getelementptr inbounds [64 x i16], [64 x i16]* %buffer, i32 0, i32 0
  store i16* %arrayidx1, i16** %tmp_buf, align 4, !tbaa !2
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc7, %for.end
  %11 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %11, 8
  br i1 %cmp3, label %for.body4, label %for.end9

for.body4:                                        ; preds = %for.cond2
  %12 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [64 x i32], [64 x i32]* %buffer2, i32 0, i32 0
  %13 = load i32, i32* %idx, align 4, !tbaa !6
  %mul = mul nsw i32 8, %13
  %add.ptr5 = getelementptr inbounds i32, i32* %arraydecay, i32 %mul
  call void @hadamard_highbd_col8_second_pass(i16* %12, i32 8, i32* %add.ptr5)
  %14 = load i16*, i16** %tmp_buf, align 4, !tbaa !2
  %incdec.ptr6 = getelementptr inbounds i16, i16* %14, i32 1
  store i16* %incdec.ptr6, i16** %tmp_buf, align 4, !tbaa !2
  br label %for.inc7

for.inc7:                                         ; preds = %for.body4
  %15 = load i32, i32* %idx, align 4, !tbaa !6
  %inc8 = add nsw i32 %15, 1
  store i32 %inc8, i32* %idx, align 4, !tbaa !6
  br label %for.cond2

for.end9:                                         ; preds = %for.cond2
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end9
  %16 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %16, 64
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %17 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds [64 x i32], [64 x i32]* %buffer2, i32 0, i32 %17
  %18 = load i32, i32* %arrayidx13, align 4, !tbaa !6
  %19 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %20 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds i32, i32* %19, i32 %20
  store i32 %18, i32* %arrayidx14, align 4, !tbaa !6
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %21 = load i32, i32* %idx, align 4, !tbaa !6
  %inc16 = add nsw i32 %21, 1
  store i32 %inc16, i32* %idx, align 4, !tbaa !6
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  %22 = bitcast i16** %tmp_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #3
  %23 = bitcast [64 x i32]* %buffer2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %23) #3
  %24 = bitcast [64 x i16]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %24) #3
  %25 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #3
  ret void
}

; Function Attrs: nounwind
define internal void @hadamard_highbd_col8_first_pass(i16* %src_diff, i32 %src_stride, i16* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i16*, align 4
  %b0 = alloca i16, align 2
  %b1 = alloca i16, align 2
  %b2 = alloca i16, align 2
  %b3 = alloca i16, align 2
  %b4 = alloca i16, align 2
  %b5 = alloca i16, align 2
  %b6 = alloca i16, align 2
  %b7 = alloca i16, align 2
  %c0 = alloca i16, align 2
  %c1 = alloca i16, align 2
  %c2 = alloca i16, align 2
  %c3 = alloca i16, align 2
  %c4 = alloca i16, align 2
  %c5 = alloca i16, align 2
  %c6 = alloca i16, align 2
  %c7 = alloca i16, align 2
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i16* %coeff, i16** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i16* %b0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #3
  %1 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %2 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul = mul nsw i32 0, %2
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 %mul
  %3 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = sext i16 %3 to i32
  %4 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %5 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul1 = mul nsw i32 1, %5
  %arrayidx2 = getelementptr inbounds i16, i16* %4, i32 %mul1
  %6 = load i16, i16* %arrayidx2, align 2, !tbaa !9
  %conv3 = sext i16 %6 to i32
  %add = add nsw i32 %conv, %conv3
  %conv4 = trunc i32 %add to i16
  store i16 %conv4, i16* %b0, align 2, !tbaa !9
  %7 = bitcast i16* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %7) #3
  %8 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %9 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul5 = mul nsw i32 0, %9
  %arrayidx6 = getelementptr inbounds i16, i16* %8, i32 %mul5
  %10 = load i16, i16* %arrayidx6, align 2, !tbaa !9
  %conv7 = sext i16 %10 to i32
  %11 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %12 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul8 = mul nsw i32 1, %12
  %arrayidx9 = getelementptr inbounds i16, i16* %11, i32 %mul8
  %13 = load i16, i16* %arrayidx9, align 2, !tbaa !9
  %conv10 = sext i16 %13 to i32
  %sub = sub nsw i32 %conv7, %conv10
  %conv11 = trunc i32 %sub to i16
  store i16 %conv11, i16* %b1, align 2, !tbaa !9
  %14 = bitcast i16* %b2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %14) #3
  %15 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %16 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul12 = mul nsw i32 2, %16
  %arrayidx13 = getelementptr inbounds i16, i16* %15, i32 %mul12
  %17 = load i16, i16* %arrayidx13, align 2, !tbaa !9
  %conv14 = sext i16 %17 to i32
  %18 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %19 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul15 = mul nsw i32 3, %19
  %arrayidx16 = getelementptr inbounds i16, i16* %18, i32 %mul15
  %20 = load i16, i16* %arrayidx16, align 2, !tbaa !9
  %conv17 = sext i16 %20 to i32
  %add18 = add nsw i32 %conv14, %conv17
  %conv19 = trunc i32 %add18 to i16
  store i16 %conv19, i16* %b2, align 2, !tbaa !9
  %21 = bitcast i16* %b3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %21) #3
  %22 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %23 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul20 = mul nsw i32 2, %23
  %arrayidx21 = getelementptr inbounds i16, i16* %22, i32 %mul20
  %24 = load i16, i16* %arrayidx21, align 2, !tbaa !9
  %conv22 = sext i16 %24 to i32
  %25 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %26 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul23 = mul nsw i32 3, %26
  %arrayidx24 = getelementptr inbounds i16, i16* %25, i32 %mul23
  %27 = load i16, i16* %arrayidx24, align 2, !tbaa !9
  %conv25 = sext i16 %27 to i32
  %sub26 = sub nsw i32 %conv22, %conv25
  %conv27 = trunc i32 %sub26 to i16
  store i16 %conv27, i16* %b3, align 2, !tbaa !9
  %28 = bitcast i16* %b4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %28) #3
  %29 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %30 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul28 = mul nsw i32 4, %30
  %arrayidx29 = getelementptr inbounds i16, i16* %29, i32 %mul28
  %31 = load i16, i16* %arrayidx29, align 2, !tbaa !9
  %conv30 = sext i16 %31 to i32
  %32 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %33 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul31 = mul nsw i32 5, %33
  %arrayidx32 = getelementptr inbounds i16, i16* %32, i32 %mul31
  %34 = load i16, i16* %arrayidx32, align 2, !tbaa !9
  %conv33 = sext i16 %34 to i32
  %add34 = add nsw i32 %conv30, %conv33
  %conv35 = trunc i32 %add34 to i16
  store i16 %conv35, i16* %b4, align 2, !tbaa !9
  %35 = bitcast i16* %b5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %35) #3
  %36 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %37 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul36 = mul nsw i32 4, %37
  %arrayidx37 = getelementptr inbounds i16, i16* %36, i32 %mul36
  %38 = load i16, i16* %arrayidx37, align 2, !tbaa !9
  %conv38 = sext i16 %38 to i32
  %39 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %40 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul39 = mul nsw i32 5, %40
  %arrayidx40 = getelementptr inbounds i16, i16* %39, i32 %mul39
  %41 = load i16, i16* %arrayidx40, align 2, !tbaa !9
  %conv41 = sext i16 %41 to i32
  %sub42 = sub nsw i32 %conv38, %conv41
  %conv43 = trunc i32 %sub42 to i16
  store i16 %conv43, i16* %b5, align 2, !tbaa !9
  %42 = bitcast i16* %b6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %42) #3
  %43 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %44 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul44 = mul nsw i32 6, %44
  %arrayidx45 = getelementptr inbounds i16, i16* %43, i32 %mul44
  %45 = load i16, i16* %arrayidx45, align 2, !tbaa !9
  %conv46 = sext i16 %45 to i32
  %46 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %47 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul47 = mul nsw i32 7, %47
  %arrayidx48 = getelementptr inbounds i16, i16* %46, i32 %mul47
  %48 = load i16, i16* %arrayidx48, align 2, !tbaa !9
  %conv49 = sext i16 %48 to i32
  %add50 = add nsw i32 %conv46, %conv49
  %conv51 = trunc i32 %add50 to i16
  store i16 %conv51, i16* %b6, align 2, !tbaa !9
  %49 = bitcast i16* %b7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %49) #3
  %50 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %51 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul52 = mul nsw i32 6, %51
  %arrayidx53 = getelementptr inbounds i16, i16* %50, i32 %mul52
  %52 = load i16, i16* %arrayidx53, align 2, !tbaa !9
  %conv54 = sext i16 %52 to i32
  %53 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %54 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul55 = mul nsw i32 7, %54
  %arrayidx56 = getelementptr inbounds i16, i16* %53, i32 %mul55
  %55 = load i16, i16* %arrayidx56, align 2, !tbaa !9
  %conv57 = sext i16 %55 to i32
  %sub58 = sub nsw i32 %conv54, %conv57
  %conv59 = trunc i32 %sub58 to i16
  store i16 %conv59, i16* %b7, align 2, !tbaa !9
  %56 = bitcast i16* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %56) #3
  %57 = load i16, i16* %b0, align 2, !tbaa !9
  %conv60 = sext i16 %57 to i32
  %58 = load i16, i16* %b2, align 2, !tbaa !9
  %conv61 = sext i16 %58 to i32
  %add62 = add nsw i32 %conv60, %conv61
  %conv63 = trunc i32 %add62 to i16
  store i16 %conv63, i16* %c0, align 2, !tbaa !9
  %59 = bitcast i16* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %59) #3
  %60 = load i16, i16* %b1, align 2, !tbaa !9
  %conv64 = sext i16 %60 to i32
  %61 = load i16, i16* %b3, align 2, !tbaa !9
  %conv65 = sext i16 %61 to i32
  %add66 = add nsw i32 %conv64, %conv65
  %conv67 = trunc i32 %add66 to i16
  store i16 %conv67, i16* %c1, align 2, !tbaa !9
  %62 = bitcast i16* %c2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %62) #3
  %63 = load i16, i16* %b0, align 2, !tbaa !9
  %conv68 = sext i16 %63 to i32
  %64 = load i16, i16* %b2, align 2, !tbaa !9
  %conv69 = sext i16 %64 to i32
  %sub70 = sub nsw i32 %conv68, %conv69
  %conv71 = trunc i32 %sub70 to i16
  store i16 %conv71, i16* %c2, align 2, !tbaa !9
  %65 = bitcast i16* %c3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %65) #3
  %66 = load i16, i16* %b1, align 2, !tbaa !9
  %conv72 = sext i16 %66 to i32
  %67 = load i16, i16* %b3, align 2, !tbaa !9
  %conv73 = sext i16 %67 to i32
  %sub74 = sub nsw i32 %conv72, %conv73
  %conv75 = trunc i32 %sub74 to i16
  store i16 %conv75, i16* %c3, align 2, !tbaa !9
  %68 = bitcast i16* %c4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %68) #3
  %69 = load i16, i16* %b4, align 2, !tbaa !9
  %conv76 = sext i16 %69 to i32
  %70 = load i16, i16* %b6, align 2, !tbaa !9
  %conv77 = sext i16 %70 to i32
  %add78 = add nsw i32 %conv76, %conv77
  %conv79 = trunc i32 %add78 to i16
  store i16 %conv79, i16* %c4, align 2, !tbaa !9
  %71 = bitcast i16* %c5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %71) #3
  %72 = load i16, i16* %b5, align 2, !tbaa !9
  %conv80 = sext i16 %72 to i32
  %73 = load i16, i16* %b7, align 2, !tbaa !9
  %conv81 = sext i16 %73 to i32
  %add82 = add nsw i32 %conv80, %conv81
  %conv83 = trunc i32 %add82 to i16
  store i16 %conv83, i16* %c5, align 2, !tbaa !9
  %74 = bitcast i16* %c6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %74) #3
  %75 = load i16, i16* %b4, align 2, !tbaa !9
  %conv84 = sext i16 %75 to i32
  %76 = load i16, i16* %b6, align 2, !tbaa !9
  %conv85 = sext i16 %76 to i32
  %sub86 = sub nsw i32 %conv84, %conv85
  %conv87 = trunc i32 %sub86 to i16
  store i16 %conv87, i16* %c6, align 2, !tbaa !9
  %77 = bitcast i16* %c7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %77) #3
  %78 = load i16, i16* %b5, align 2, !tbaa !9
  %conv88 = sext i16 %78 to i32
  %79 = load i16, i16* %b7, align 2, !tbaa !9
  %conv89 = sext i16 %79 to i32
  %sub90 = sub nsw i32 %conv88, %conv89
  %conv91 = trunc i32 %sub90 to i16
  store i16 %conv91, i16* %c7, align 2, !tbaa !9
  %80 = load i16, i16* %c0, align 2, !tbaa !9
  %conv92 = sext i16 %80 to i32
  %81 = load i16, i16* %c4, align 2, !tbaa !9
  %conv93 = sext i16 %81 to i32
  %add94 = add nsw i32 %conv92, %conv93
  %conv95 = trunc i32 %add94 to i16
  %82 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i16, i16* %82, i32 0
  store i16 %conv95, i16* %arrayidx96, align 2, !tbaa !9
  %83 = load i16, i16* %c1, align 2, !tbaa !9
  %conv97 = sext i16 %83 to i32
  %84 = load i16, i16* %c5, align 2, !tbaa !9
  %conv98 = sext i16 %84 to i32
  %add99 = add nsw i32 %conv97, %conv98
  %conv100 = trunc i32 %add99 to i16
  %85 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i16, i16* %85, i32 7
  store i16 %conv100, i16* %arrayidx101, align 2, !tbaa !9
  %86 = load i16, i16* %c2, align 2, !tbaa !9
  %conv102 = sext i16 %86 to i32
  %87 = load i16, i16* %c6, align 2, !tbaa !9
  %conv103 = sext i16 %87 to i32
  %add104 = add nsw i32 %conv102, %conv103
  %conv105 = trunc i32 %add104 to i16
  %88 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i16, i16* %88, i32 3
  store i16 %conv105, i16* %arrayidx106, align 2, !tbaa !9
  %89 = load i16, i16* %c3, align 2, !tbaa !9
  %conv107 = sext i16 %89 to i32
  %90 = load i16, i16* %c7, align 2, !tbaa !9
  %conv108 = sext i16 %90 to i32
  %add109 = add nsw i32 %conv107, %conv108
  %conv110 = trunc i32 %add109 to i16
  %91 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i16, i16* %91, i32 4
  store i16 %conv110, i16* %arrayidx111, align 2, !tbaa !9
  %92 = load i16, i16* %c0, align 2, !tbaa !9
  %conv112 = sext i16 %92 to i32
  %93 = load i16, i16* %c4, align 2, !tbaa !9
  %conv113 = sext i16 %93 to i32
  %sub114 = sub nsw i32 %conv112, %conv113
  %conv115 = trunc i32 %sub114 to i16
  %94 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx116 = getelementptr inbounds i16, i16* %94, i32 2
  store i16 %conv115, i16* %arrayidx116, align 2, !tbaa !9
  %95 = load i16, i16* %c1, align 2, !tbaa !9
  %conv117 = sext i16 %95 to i32
  %96 = load i16, i16* %c5, align 2, !tbaa !9
  %conv118 = sext i16 %96 to i32
  %sub119 = sub nsw i32 %conv117, %conv118
  %conv120 = trunc i32 %sub119 to i16
  %97 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i16, i16* %97, i32 6
  store i16 %conv120, i16* %arrayidx121, align 2, !tbaa !9
  %98 = load i16, i16* %c2, align 2, !tbaa !9
  %conv122 = sext i16 %98 to i32
  %99 = load i16, i16* %c6, align 2, !tbaa !9
  %conv123 = sext i16 %99 to i32
  %sub124 = sub nsw i32 %conv122, %conv123
  %conv125 = trunc i32 %sub124 to i16
  %100 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx126 = getelementptr inbounds i16, i16* %100, i32 1
  store i16 %conv125, i16* %arrayidx126, align 2, !tbaa !9
  %101 = load i16, i16* %c3, align 2, !tbaa !9
  %conv127 = sext i16 %101 to i32
  %102 = load i16, i16* %c7, align 2, !tbaa !9
  %conv128 = sext i16 %102 to i32
  %sub129 = sub nsw i32 %conv127, %conv128
  %conv130 = trunc i32 %sub129 to i16
  %103 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i16, i16* %103, i32 5
  store i16 %conv130, i16* %arrayidx131, align 2, !tbaa !9
  %104 = bitcast i16* %c7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %104) #3
  %105 = bitcast i16* %c6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %105) #3
  %106 = bitcast i16* %c5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %106) #3
  %107 = bitcast i16* %c4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %107) #3
  %108 = bitcast i16* %c3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %108) #3
  %109 = bitcast i16* %c2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %109) #3
  %110 = bitcast i16* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %110) #3
  %111 = bitcast i16* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %111) #3
  %112 = bitcast i16* %b7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %112) #3
  %113 = bitcast i16* %b6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %113) #3
  %114 = bitcast i16* %b5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %114) #3
  %115 = bitcast i16* %b4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %115) #3
  %116 = bitcast i16* %b3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %116) #3
  %117 = bitcast i16* %b2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %117) #3
  %118 = bitcast i16* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %118) #3
  %119 = bitcast i16* %b0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %119) #3
  ret void
}

; Function Attrs: nounwind
define internal void @hadamard_highbd_col8_second_pass(i16* %src_diff, i32 %src_stride, i32* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i32*, align 4
  %b0 = alloca i32, align 4
  %b1 = alloca i32, align 4
  %b2 = alloca i32, align 4
  %b3 = alloca i32, align 4
  %b4 = alloca i32, align 4
  %b5 = alloca i32, align 4
  %b6 = alloca i32, align 4
  %b7 = alloca i32, align 4
  %c0 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %c2 = alloca i32, align 4
  %c3 = alloca i32, align 4
  %c4 = alloca i32, align 4
  %c5 = alloca i32, align 4
  %c6 = alloca i32, align 4
  %c7 = alloca i32, align 4
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i32* %coeff, i32** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %2 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul = mul nsw i32 0, %2
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 %mul
  %3 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = sext i16 %3 to i32
  %4 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %5 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul1 = mul nsw i32 1, %5
  %arrayidx2 = getelementptr inbounds i16, i16* %4, i32 %mul1
  %6 = load i16, i16* %arrayidx2, align 2, !tbaa !9
  %conv3 = sext i16 %6 to i32
  %add = add nsw i32 %conv, %conv3
  store i32 %add, i32* %b0, align 4, !tbaa !6
  %7 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %9 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul4 = mul nsw i32 0, %9
  %arrayidx5 = getelementptr inbounds i16, i16* %8, i32 %mul4
  %10 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  %conv6 = sext i16 %10 to i32
  %11 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %12 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul7 = mul nsw i32 1, %12
  %arrayidx8 = getelementptr inbounds i16, i16* %11, i32 %mul7
  %13 = load i16, i16* %arrayidx8, align 2, !tbaa !9
  %conv9 = sext i16 %13 to i32
  %sub = sub nsw i32 %conv6, %conv9
  store i32 %sub, i32* %b1, align 4, !tbaa !6
  %14 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %16 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul10 = mul nsw i32 2, %16
  %arrayidx11 = getelementptr inbounds i16, i16* %15, i32 %mul10
  %17 = load i16, i16* %arrayidx11, align 2, !tbaa !9
  %conv12 = sext i16 %17 to i32
  %18 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %19 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul13 = mul nsw i32 3, %19
  %arrayidx14 = getelementptr inbounds i16, i16* %18, i32 %mul13
  %20 = load i16, i16* %arrayidx14, align 2, !tbaa !9
  %conv15 = sext i16 %20 to i32
  %add16 = add nsw i32 %conv12, %conv15
  store i32 %add16, i32* %b2, align 4, !tbaa !6
  %21 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %23 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul17 = mul nsw i32 2, %23
  %arrayidx18 = getelementptr inbounds i16, i16* %22, i32 %mul17
  %24 = load i16, i16* %arrayidx18, align 2, !tbaa !9
  %conv19 = sext i16 %24 to i32
  %25 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %26 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul20 = mul nsw i32 3, %26
  %arrayidx21 = getelementptr inbounds i16, i16* %25, i32 %mul20
  %27 = load i16, i16* %arrayidx21, align 2, !tbaa !9
  %conv22 = sext i16 %27 to i32
  %sub23 = sub nsw i32 %conv19, %conv22
  store i32 %sub23, i32* %b3, align 4, !tbaa !6
  %28 = bitcast i32* %b4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %30 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul24 = mul nsw i32 4, %30
  %arrayidx25 = getelementptr inbounds i16, i16* %29, i32 %mul24
  %31 = load i16, i16* %arrayidx25, align 2, !tbaa !9
  %conv26 = sext i16 %31 to i32
  %32 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %33 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul27 = mul nsw i32 5, %33
  %arrayidx28 = getelementptr inbounds i16, i16* %32, i32 %mul27
  %34 = load i16, i16* %arrayidx28, align 2, !tbaa !9
  %conv29 = sext i16 %34 to i32
  %add30 = add nsw i32 %conv26, %conv29
  store i32 %add30, i32* %b4, align 4, !tbaa !6
  %35 = bitcast i32* %b5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #3
  %36 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %37 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul31 = mul nsw i32 4, %37
  %arrayidx32 = getelementptr inbounds i16, i16* %36, i32 %mul31
  %38 = load i16, i16* %arrayidx32, align 2, !tbaa !9
  %conv33 = sext i16 %38 to i32
  %39 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %40 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul34 = mul nsw i32 5, %40
  %arrayidx35 = getelementptr inbounds i16, i16* %39, i32 %mul34
  %41 = load i16, i16* %arrayidx35, align 2, !tbaa !9
  %conv36 = sext i16 %41 to i32
  %sub37 = sub nsw i32 %conv33, %conv36
  store i32 %sub37, i32* %b5, align 4, !tbaa !6
  %42 = bitcast i32* %b6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #3
  %43 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %44 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul38 = mul nsw i32 6, %44
  %arrayidx39 = getelementptr inbounds i16, i16* %43, i32 %mul38
  %45 = load i16, i16* %arrayidx39, align 2, !tbaa !9
  %conv40 = sext i16 %45 to i32
  %46 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %47 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul41 = mul nsw i32 7, %47
  %arrayidx42 = getelementptr inbounds i16, i16* %46, i32 %mul41
  %48 = load i16, i16* %arrayidx42, align 2, !tbaa !9
  %conv43 = sext i16 %48 to i32
  %add44 = add nsw i32 %conv40, %conv43
  store i32 %add44, i32* %b6, align 4, !tbaa !6
  %49 = bitcast i32* %b7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #3
  %50 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %51 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul45 = mul nsw i32 6, %51
  %arrayidx46 = getelementptr inbounds i16, i16* %50, i32 %mul45
  %52 = load i16, i16* %arrayidx46, align 2, !tbaa !9
  %conv47 = sext i16 %52 to i32
  %53 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %54 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul48 = mul nsw i32 7, %54
  %arrayidx49 = getelementptr inbounds i16, i16* %53, i32 %mul48
  %55 = load i16, i16* %arrayidx49, align 2, !tbaa !9
  %conv50 = sext i16 %55 to i32
  %sub51 = sub nsw i32 %conv47, %conv50
  store i32 %sub51, i32* %b7, align 4, !tbaa !6
  %56 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #3
  %57 = load i32, i32* %b0, align 4, !tbaa !6
  %58 = load i32, i32* %b2, align 4, !tbaa !6
  %add52 = add nsw i32 %57, %58
  store i32 %add52, i32* %c0, align 4, !tbaa !6
  %59 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #3
  %60 = load i32, i32* %b1, align 4, !tbaa !6
  %61 = load i32, i32* %b3, align 4, !tbaa !6
  %add53 = add nsw i32 %60, %61
  store i32 %add53, i32* %c1, align 4, !tbaa !6
  %62 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #3
  %63 = load i32, i32* %b0, align 4, !tbaa !6
  %64 = load i32, i32* %b2, align 4, !tbaa !6
  %sub54 = sub nsw i32 %63, %64
  store i32 %sub54, i32* %c2, align 4, !tbaa !6
  %65 = bitcast i32* %c3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #3
  %66 = load i32, i32* %b1, align 4, !tbaa !6
  %67 = load i32, i32* %b3, align 4, !tbaa !6
  %sub55 = sub nsw i32 %66, %67
  store i32 %sub55, i32* %c3, align 4, !tbaa !6
  %68 = bitcast i32* %c4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #3
  %69 = load i32, i32* %b4, align 4, !tbaa !6
  %70 = load i32, i32* %b6, align 4, !tbaa !6
  %add56 = add nsw i32 %69, %70
  store i32 %add56, i32* %c4, align 4, !tbaa !6
  %71 = bitcast i32* %c5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #3
  %72 = load i32, i32* %b5, align 4, !tbaa !6
  %73 = load i32, i32* %b7, align 4, !tbaa !6
  %add57 = add nsw i32 %72, %73
  store i32 %add57, i32* %c5, align 4, !tbaa !6
  %74 = bitcast i32* %c6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #3
  %75 = load i32, i32* %b4, align 4, !tbaa !6
  %76 = load i32, i32* %b6, align 4, !tbaa !6
  %sub58 = sub nsw i32 %75, %76
  store i32 %sub58, i32* %c6, align 4, !tbaa !6
  %77 = bitcast i32* %c7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #3
  %78 = load i32, i32* %b5, align 4, !tbaa !6
  %79 = load i32, i32* %b7, align 4, !tbaa !6
  %sub59 = sub nsw i32 %78, %79
  store i32 %sub59, i32* %c7, align 4, !tbaa !6
  %80 = load i32, i32* %c0, align 4, !tbaa !6
  %81 = load i32, i32* %c4, align 4, !tbaa !6
  %add60 = add nsw i32 %80, %81
  %82 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i32, i32* %82, i32 0
  store i32 %add60, i32* %arrayidx61, align 4, !tbaa !6
  %83 = load i32, i32* %c1, align 4, !tbaa !6
  %84 = load i32, i32* %c5, align 4, !tbaa !6
  %add62 = add nsw i32 %83, %84
  %85 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i32, i32* %85, i32 7
  store i32 %add62, i32* %arrayidx63, align 4, !tbaa !6
  %86 = load i32, i32* %c2, align 4, !tbaa !6
  %87 = load i32, i32* %c6, align 4, !tbaa !6
  %add64 = add nsw i32 %86, %87
  %88 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i32, i32* %88, i32 3
  store i32 %add64, i32* %arrayidx65, align 4, !tbaa !6
  %89 = load i32, i32* %c3, align 4, !tbaa !6
  %90 = load i32, i32* %c7, align 4, !tbaa !6
  %add66 = add nsw i32 %89, %90
  %91 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i32, i32* %91, i32 4
  store i32 %add66, i32* %arrayidx67, align 4, !tbaa !6
  %92 = load i32, i32* %c0, align 4, !tbaa !6
  %93 = load i32, i32* %c4, align 4, !tbaa !6
  %sub68 = sub nsw i32 %92, %93
  %94 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i32, i32* %94, i32 2
  store i32 %sub68, i32* %arrayidx69, align 4, !tbaa !6
  %95 = load i32, i32* %c1, align 4, !tbaa !6
  %96 = load i32, i32* %c5, align 4, !tbaa !6
  %sub70 = sub nsw i32 %95, %96
  %97 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %97, i32 6
  store i32 %sub70, i32* %arrayidx71, align 4, !tbaa !6
  %98 = load i32, i32* %c2, align 4, !tbaa !6
  %99 = load i32, i32* %c6, align 4, !tbaa !6
  %sub72 = sub nsw i32 %98, %99
  %100 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %100, i32 1
  store i32 %sub72, i32* %arrayidx73, align 4, !tbaa !6
  %101 = load i32, i32* %c3, align 4, !tbaa !6
  %102 = load i32, i32* %c7, align 4, !tbaa !6
  %sub74 = sub nsw i32 %101, %102
  %103 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds i32, i32* %103, i32 5
  store i32 %sub74, i32* %arrayidx75, align 4, !tbaa !6
  %104 = bitcast i32* %c7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #3
  %105 = bitcast i32* %c6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #3
  %106 = bitcast i32* %c5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #3
  %107 = bitcast i32* %c4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  %108 = bitcast i32* %c3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %112 = bitcast i32* %b7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #3
  %113 = bitcast i32* %b6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #3
  %114 = bitcast i32* %b5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #3
  %115 = bitcast i32* %b4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #3
  %116 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  %117 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  %119 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_hadamard_16x16_c(i16* %src_diff, i32 %src_stride, i32* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i32*, align 4
  %idx = alloca i32, align 4
  %src_ptr = alloca i16*, align 4
  %a0 = alloca i32, align 4
  %a1 = alloca i32, align 4
  %a2 = alloca i32, align 4
  %a3 = alloca i32, align 4
  %b0 = alloca i32, align 4
  %b1 = alloca i32, align 4
  %b2 = alloca i32, align 4
  %b3 = alloca i32, align 4
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i32* %coeff, i32** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %4 = load i32, i32* %idx, align 4, !tbaa !6
  %shr = ashr i32 %4, 1
  %mul = mul nsw i32 %shr, 8
  %5 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul1 = mul nsw i32 %mul, %5
  %add.ptr = getelementptr inbounds i16, i16* %3, i32 %mul1
  %6 = load i32, i32* %idx, align 4, !tbaa !6
  %and = and i32 %6, 1
  %mul2 = mul nsw i32 %and, 8
  %add.ptr3 = getelementptr inbounds i16, i16* %add.ptr, i32 %mul2
  store i16* %add.ptr3, i16** %src_ptr, align 4, !tbaa !2
  %7 = load i16*, i16** %src_ptr, align 4, !tbaa !2
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %9 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %10 = load i32, i32* %idx, align 4, !tbaa !6
  %mul4 = mul nsw i32 %10, 64
  %add.ptr5 = getelementptr inbounds i32, i32* %9, i32 %mul4
  call void @aom_highbd_hadamard_8x8_c(i16* %7, i32 %8, i32* %add.ptr5)
  %11 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc26, %for.end
  %13 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %13, 64
  br i1 %cmp7, label %for.body8, label %for.end28

for.body8:                                        ; preds = %for.cond6
  %14 = bitcast i32* %a0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 0
  %16 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %16, i32* %a0, align 4, !tbaa !6
  %17 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %18, i32 64
  %19 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  store i32 %19, i32* %a1, align 4, !tbaa !6
  %20 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %21, i32 128
  %22 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  store i32 %22, i32* %a2, align 4, !tbaa !6
  %23 = bitcast i32* %a3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %24, i32 192
  %25 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  store i32 %25, i32* %a3, align 4, !tbaa !6
  %26 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load i32, i32* %a0, align 4, !tbaa !6
  %28 = load i32, i32* %a1, align 4, !tbaa !6
  %add = add nsw i32 %27, %28
  %shr12 = ashr i32 %add, 1
  store i32 %shr12, i32* %b0, align 4, !tbaa !6
  %29 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #3
  %30 = load i32, i32* %a0, align 4, !tbaa !6
  %31 = load i32, i32* %a1, align 4, !tbaa !6
  %sub = sub nsw i32 %30, %31
  %shr13 = ashr i32 %sub, 1
  store i32 %shr13, i32* %b1, align 4, !tbaa !6
  %32 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load i32, i32* %a2, align 4, !tbaa !6
  %34 = load i32, i32* %a3, align 4, !tbaa !6
  %add14 = add nsw i32 %33, %34
  %shr15 = ashr i32 %add14, 1
  store i32 %shr15, i32* %b2, align 4, !tbaa !6
  %35 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #3
  %36 = load i32, i32* %a2, align 4, !tbaa !6
  %37 = load i32, i32* %a3, align 4, !tbaa !6
  %sub16 = sub nsw i32 %36, %37
  %shr17 = ashr i32 %sub16, 1
  store i32 %shr17, i32* %b3, align 4, !tbaa !6
  %38 = load i32, i32* %b0, align 4, !tbaa !6
  %39 = load i32, i32* %b2, align 4, !tbaa !6
  %add18 = add nsw i32 %38, %39
  %40 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %40, i32 0
  store i32 %add18, i32* %arrayidx19, align 4, !tbaa !6
  %41 = load i32, i32* %b1, align 4, !tbaa !6
  %42 = load i32, i32* %b3, align 4, !tbaa !6
  %add20 = add nsw i32 %41, %42
  %43 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %43, i32 64
  store i32 %add20, i32* %arrayidx21, align 4, !tbaa !6
  %44 = load i32, i32* %b0, align 4, !tbaa !6
  %45 = load i32, i32* %b2, align 4, !tbaa !6
  %sub22 = sub nsw i32 %44, %45
  %46 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %46, i32 128
  store i32 %sub22, i32* %arrayidx23, align 4, !tbaa !6
  %47 = load i32, i32* %b1, align 4, !tbaa !6
  %48 = load i32, i32* %b3, align 4, !tbaa !6
  %sub24 = sub nsw i32 %47, %48
  %49 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %49, i32 192
  store i32 %sub24, i32* %arrayidx25, align 4, !tbaa !6
  %50 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %50, i32 1
  store i32* %incdec.ptr, i32** %coeff.addr, align 4, !tbaa !2
  %51 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  %55 = bitcast i32* %a3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %a0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  br label %for.inc26

for.inc26:                                        ; preds = %for.body8
  %59 = load i32, i32* %idx, align 4, !tbaa !6
  %inc27 = add nsw i32 %59, 1
  store i32 %inc27, i32* %idx, align 4, !tbaa !6
  br label %for.cond6

for.end28:                                        ; preds = %for.cond6
  %60 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_hadamard_32x32_c(i16* %src_diff, i32 %src_stride, i32* %coeff) #0 {
entry:
  %src_diff.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %coeff.addr = alloca i32*, align 4
  %idx = alloca i32, align 4
  %src_ptr = alloca i16*, align 4
  %a0 = alloca i32, align 4
  %a1 = alloca i32, align 4
  %a2 = alloca i32, align 4
  %a3 = alloca i32, align 4
  %b0 = alloca i32, align 4
  %b1 = alloca i32, align 4
  %b2 = alloca i32, align 4
  %b3 = alloca i32, align 4
  store i16* %src_diff, i16** %src_diff.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !11
  store i32* %coeff, i32** %coeff.addr, align 4, !tbaa !2
  %0 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i16*, i16** %src_diff.addr, align 4, !tbaa !2
  %4 = load i32, i32* %idx, align 4, !tbaa !6
  %shr = ashr i32 %4, 1
  %mul = mul nsw i32 %shr, 16
  %5 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %mul1 = mul nsw i32 %mul, %5
  %add.ptr = getelementptr inbounds i16, i16* %3, i32 %mul1
  %6 = load i32, i32* %idx, align 4, !tbaa !6
  %and = and i32 %6, 1
  %mul2 = mul nsw i32 %and, 16
  %add.ptr3 = getelementptr inbounds i16, i16* %add.ptr, i32 %mul2
  store i16* %add.ptr3, i16** %src_ptr, align 4, !tbaa !2
  %7 = load i16*, i16** %src_ptr, align 4, !tbaa !2
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !11
  %9 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %10 = load i32, i32* %idx, align 4, !tbaa !6
  %mul4 = mul nsw i32 %10, 256
  %add.ptr5 = getelementptr inbounds i32, i32* %9, i32 %mul4
  call void @aom_highbd_hadamard_16x16_c(i16* %7, i32 %8, i32* %add.ptr5)
  %11 = bitcast i16** %src_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc26, %for.end
  %13 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %13, 256
  br i1 %cmp7, label %for.body8, label %for.end28

for.body8:                                        ; preds = %for.cond6
  %14 = bitcast i32* %a0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 0
  %16 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %16, i32* %a0, align 4, !tbaa !6
  %17 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %18, i32 256
  %19 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  store i32 %19, i32* %a1, align 4, !tbaa !6
  %20 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %21, i32 512
  %22 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  store i32 %22, i32* %a2, align 4, !tbaa !6
  %23 = bitcast i32* %a3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %24, i32 768
  %25 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  store i32 %25, i32* %a3, align 4, !tbaa !6
  %26 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load i32, i32* %a0, align 4, !tbaa !6
  %28 = load i32, i32* %a1, align 4, !tbaa !6
  %add = add nsw i32 %27, %28
  %shr12 = ashr i32 %add, 2
  store i32 %shr12, i32* %b0, align 4, !tbaa !6
  %29 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #3
  %30 = load i32, i32* %a0, align 4, !tbaa !6
  %31 = load i32, i32* %a1, align 4, !tbaa !6
  %sub = sub nsw i32 %30, %31
  %shr13 = ashr i32 %sub, 2
  store i32 %shr13, i32* %b1, align 4, !tbaa !6
  %32 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load i32, i32* %a2, align 4, !tbaa !6
  %34 = load i32, i32* %a3, align 4, !tbaa !6
  %add14 = add nsw i32 %33, %34
  %shr15 = ashr i32 %add14, 2
  store i32 %shr15, i32* %b2, align 4, !tbaa !6
  %35 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #3
  %36 = load i32, i32* %a2, align 4, !tbaa !6
  %37 = load i32, i32* %a3, align 4, !tbaa !6
  %sub16 = sub nsw i32 %36, %37
  %shr17 = ashr i32 %sub16, 2
  store i32 %shr17, i32* %b3, align 4, !tbaa !6
  %38 = load i32, i32* %b0, align 4, !tbaa !6
  %39 = load i32, i32* %b2, align 4, !tbaa !6
  %add18 = add nsw i32 %38, %39
  %40 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %40, i32 0
  store i32 %add18, i32* %arrayidx19, align 4, !tbaa !6
  %41 = load i32, i32* %b1, align 4, !tbaa !6
  %42 = load i32, i32* %b3, align 4, !tbaa !6
  %add20 = add nsw i32 %41, %42
  %43 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %43, i32 256
  store i32 %add20, i32* %arrayidx21, align 4, !tbaa !6
  %44 = load i32, i32* %b0, align 4, !tbaa !6
  %45 = load i32, i32* %b2, align 4, !tbaa !6
  %sub22 = sub nsw i32 %44, %45
  %46 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %46, i32 512
  store i32 %sub22, i32* %arrayidx23, align 4, !tbaa !6
  %47 = load i32, i32* %b1, align 4, !tbaa !6
  %48 = load i32, i32* %b3, align 4, !tbaa !6
  %sub24 = sub nsw i32 %47, %48
  %49 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %49, i32 768
  store i32 %sub24, i32* %arrayidx25, align 4, !tbaa !6
  %50 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %50, i32 1
  store i32* %incdec.ptr, i32** %coeff.addr, align 4, !tbaa !2
  %51 = bitcast i32* %b3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast i32* %b2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast i32* %b0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  %55 = bitcast i32* %a3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %a0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  br label %for.inc26

for.inc26:                                        ; preds = %for.body8
  %59 = load i32, i32* %idx, align 4, !tbaa !6
  %inc27 = add nsw i32 %59, 1
  store i32 %inc27, i32* %idx, align 4, !tbaa !6
  br label %for.cond6

for.end28:                                        ; preds = %for.cond6
  %60 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_satd_c(i32* %coeff, i32 %length) #0 {
entry:
  %coeff.addr = alloca i32*, align 4
  %length.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %satd = alloca i32, align 4
  store i32* %coeff, i32** %coeff.addr, align 4, !tbaa !2
  store i32 %length, i32* %length.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %satd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %satd, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %length.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %coeff.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %call = call i32 @abs(i32 %6) #4
  %7 = load i32, i32* %satd, align 4, !tbaa !6
  %add = add nsw i32 %7, %call
  store i32 %add, i32* %satd, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i32, i32* %satd, align 4, !tbaa !6
  %10 = bitcast i32* %satd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  ret i32 %9
}

; Function Attrs: nounwind
define hidden i32 @aom_satd_lp_c(i16* %coeff, i32 %length) #0 {
entry:
  %coeff.addr = alloca i16*, align 4
  %length.addr = alloca i32, align 4
  %satd = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %coeff, i16** %coeff.addr, align 4, !tbaa !2
  store i32 %length, i32* %length.addr, align 4, !tbaa !6
  %0 = bitcast i32* %satd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %satd, align 4, !tbaa !6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %length.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i16*, i16** %coeff.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 %6
  %7 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = sext i16 %7 to i32
  %call = call i32 @abs(i32 %conv) #4
  %8 = load i32, i32* %satd, align 4, !tbaa !6
  %add = add nsw i32 %8, %call
  store i32 %add, i32* %satd, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %10 = load i32, i32* %satd, align 4, !tbaa !6
  %11 = bitcast i32* %satd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  ret i32 %10
}

; Function Attrs: nounwind
define hidden void @aom_int_pro_row_c(i16* %hbuf, i8* %ref, i32 %ref_stride, i32 %height) #0 {
entry:
  %hbuf.addr = alloca i16*, align 4
  %ref.addr = alloca i8*, align 4
  %ref_stride.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %idx = alloca i32, align 4
  %norm_factor = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %hbuf, i16** %hbuf.addr, align 4, !tbaa !2
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %ref_stride, i32* %ref_stride.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  %0 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %norm_factor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %shr = ashr i32 %2, 1
  store i32 %shr, i32* %norm_factor, align 4, !tbaa !6
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc11, %entry
  %3 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, 16
  br i1 %cmp, label %for.body, label %for.end13

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i16*, i16** %hbuf.addr, align 4, !tbaa !2
  %6 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 %6
  store i16 0, i16* %arrayidx, align 2, !tbaa !9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %8 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %7, %8
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %9 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %ref_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %10, %11
  %arrayidx4 = getelementptr inbounds i8, i8* %9, i32 %mul
  %12 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv = zext i8 %12 to i32
  %13 = load i16*, i16** %hbuf.addr, align 4, !tbaa !2
  %14 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i16, i16* %13, i32 %14
  %15 = load i16, i16* %arrayidx5, align 2, !tbaa !9
  %conv6 = sext i16 %15 to i32
  %add = add nsw i32 %conv6, %conv
  %conv7 = trunc i32 %add to i16
  store i16 %conv7, i16* %arrayidx5, align 2, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %17 = load i32, i32* %norm_factor, align 4, !tbaa !6
  %18 = load i16*, i16** %hbuf.addr, align 4, !tbaa !2
  %19 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds i16, i16* %18, i32 %19
  %20 = load i16, i16* %arrayidx8, align 2, !tbaa !9
  %conv9 = sext i16 %20 to i32
  %div = sdiv i32 %conv9, %17
  %conv10 = trunc i32 %div to i16
  store i16 %conv10, i16* %arrayidx8, align 2, !tbaa !9
  %21 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %21, i32 1
  store i8* %incdec.ptr, i8** %ref.addr, align 4, !tbaa !2
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #3
  br label %for.inc11

for.inc11:                                        ; preds = %for.end
  %23 = load i32, i32* %idx, align 4, !tbaa !6
  %inc12 = add nsw i32 %23, 1
  store i32 %inc12, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end13:                                        ; preds = %for.cond
  %24 = bitcast i32* %norm_factor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  %25 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #3
  ret void
}

; Function Attrs: nounwind
define hidden signext i16 @aom_int_pro_col_c(i8* %ref, i32 %width) #0 {
entry:
  %ref.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %idx = alloca i32, align 4
  %sum = alloca i16, align 2
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  %0 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i16* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %1) #3
  store i16 0, i16* %sum, align 2, !tbaa !9
  store i32 0, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %idx, align 4, !tbaa !6
  %3 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %5 = load i32, i32* %idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %6 to i32
  %7 = load i16, i16* %sum, align 2, !tbaa !9
  %conv1 = sext i16 %7 to i32
  %add = add nsw i32 %conv1, %conv
  %conv2 = trunc i32 %add to i16
  store i16 %conv2, i16* %sum, align 2, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %idx, align 4, !tbaa !6
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i16, i16* %sum, align 2, !tbaa !9
  %10 = bitcast i16* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %10) #3
  %11 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  ret i16 %9
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_var_c(i16* %ref, i16* %src, i32 %bwl) #0 {
entry:
  %ref.addr = alloca i16*, align 4
  %src.addr = alloca i16*, align 4
  %bwl.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %width = alloca i32, align 4
  %sse = alloca i32, align 4
  %mean = alloca i32, align 4
  %var = alloca i32, align 4
  %diff = alloca i32, align 4
  store i16* %ref, i16** %ref.addr, align 4, !tbaa !2
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %bwl, i32* %bwl.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %shl = shl i32 4, %2
  store i32 %shl, i32* %width, align 4, !tbaa !6
  %3 = bitcast i32* %sse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store i32 0, i32* %sse, align 4, !tbaa !6
  %4 = bitcast i32* %mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  store i32 0, i32* %mean, align 4, !tbaa !6
  %5 = bitcast i32* %var to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %7 = load i32, i32* %width, align 4, !tbaa !6
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load i16*, i16** %ref.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %9, i32 %10
  %11 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = sext i16 %11 to i32
  %12 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx1, align 2, !tbaa !9
  %conv2 = sext i16 %14 to i32
  %sub = sub nsw i32 %conv, %conv2
  store i32 %sub, i32* %diff, align 4, !tbaa !6
  %15 = load i32, i32* %diff, align 4, !tbaa !6
  %16 = load i32, i32* %mean, align 4, !tbaa !6
  %add = add nsw i32 %16, %15
  store i32 %add, i32* %mean, align 4, !tbaa !6
  %17 = load i32, i32* %diff, align 4, !tbaa !6
  %18 = load i32, i32* %diff, align 4, !tbaa !6
  %mul = mul nsw i32 %17, %18
  %19 = load i32, i32* %sse, align 4, !tbaa !6
  %add3 = add nsw i32 %19, %mul
  store i32 %add3, i32* %sse, align 4, !tbaa !6
  %20 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = load i32, i32* %sse, align 4, !tbaa !6
  %23 = load i32, i32* %mean, align 4, !tbaa !6
  %24 = load i32, i32* %mean, align 4, !tbaa !6
  %mul4 = mul nsw i32 %23, %24
  %25 = load i32, i32* %bwl.addr, align 4, !tbaa !6
  %add5 = add nsw i32 %25, 2
  %shr = ashr i32 %mul4, %add5
  %sub6 = sub nsw i32 %22, %shr
  store i32 %sub6, i32* %var, align 4, !tbaa !6
  %26 = load i32, i32* %var, align 4, !tbaa !6
  %27 = bitcast i32* %var to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  %28 = bitcast i32* %mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  %29 = bitcast i32* %sse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #3
  %30 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  %31 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  ret i32 %26
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"long", !4, i64 0}
