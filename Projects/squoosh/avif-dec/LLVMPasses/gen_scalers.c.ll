; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_scale/generic/gen_scalers.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_scale/generic/gen_scalers.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @aom_horizontal_line_5_4_scale_c(i8* %source, i32 %source_width, i8* %dest, i32 %dest_width) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %source_width.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_width.addr = alloca i32, align 4
  %source_end = alloca i8*, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  %d = alloca i32, align 4
  %e = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %source_width, i32* %source_width.addr, align 4, !tbaa !6
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_width, i32* %dest_width.addr, align 4, !tbaa !6
  %0 = bitcast i8** %source_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %2 = load i32, i32* %source_width.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %2
  store i8* %add.ptr, i8** %source_end, align 4, !tbaa !2
  %3 = load i32, i32* %dest_width.addr, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %source_end, align 4, !tbaa !2
  %cmp = icmp ult i8* %4, %5
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %7, i32 0
  %8 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %8 to i32
  store i32 %conv, i32* %a, align 4, !tbaa !6
  %9 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %10, i32 1
  %11 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  %conv2 = zext i8 %11 to i32
  store i32 %conv2, i32* %b, align 4, !tbaa !6
  %12 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %13, i32 2
  %14 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  %conv4 = zext i8 %14 to i32
  store i32 %conv4, i32* %c, align 4, !tbaa !6
  %15 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %16, i32 3
  %17 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv6 = zext i8 %17 to i32
  store i32 %conv6, i32* %d, align 4, !tbaa !6
  %18 = bitcast i32* %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %19, i32 4
  %20 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  %conv8 = zext i8 %20 to i32
  store i32 %conv8, i32* %e, align 4, !tbaa !6
  %21 = load i32, i32* %a, align 4, !tbaa !6
  %conv9 = trunc i32 %21 to i8
  %22 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %22, i32 0
  store i8 %conv9, i8* %arrayidx10, align 1, !tbaa !8
  %23 = load i32, i32* %b, align 4, !tbaa !6
  %mul = mul i32 %23, 192
  %24 = load i32, i32* %c, align 4, !tbaa !6
  %mul11 = mul i32 %24, 64
  %add = add i32 %mul, %mul11
  %add12 = add i32 %add, 128
  %shr = lshr i32 %add12, 8
  %conv13 = trunc i32 %shr to i8
  %25 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %25, i32 1
  store i8 %conv13, i8* %arrayidx14, align 1, !tbaa !8
  %26 = load i32, i32* %c, align 4, !tbaa !6
  %mul15 = mul i32 %26, 128
  %27 = load i32, i32* %d, align 4, !tbaa !6
  %mul16 = mul i32 %27, 128
  %add17 = add i32 %mul15, %mul16
  %add18 = add i32 %add17, 128
  %shr19 = lshr i32 %add18, 8
  %conv20 = trunc i32 %shr19 to i8
  %28 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %28, i32 2
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !8
  %29 = load i32, i32* %d, align 4, !tbaa !6
  %mul22 = mul i32 %29, 64
  %30 = load i32, i32* %e, align 4, !tbaa !6
  %mul23 = mul i32 %30, 192
  %add24 = add i32 %mul22, %mul23
  %add25 = add i32 %add24, 128
  %shr26 = lshr i32 %add25, 8
  %conv27 = trunc i32 %shr26 to i8
  %31 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i8, i8* %31, i32 3
  store i8 %conv27, i8* %arrayidx28, align 1, !tbaa !8
  %32 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr29 = getelementptr inbounds i8, i8* %32, i32 5
  store i8* %add.ptr29, i8** %source.addr, align 4, !tbaa !2
  %33 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %add.ptr30 = getelementptr inbounds i8, i8* %33, i32 4
  store i8* %add.ptr30, i8** %dest.addr, align 4, !tbaa !2
  %34 = bitcast i32* %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #2
  %35 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #2
  %36 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #2
  %37 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #2
  %38 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %39 = bitcast i8** %source_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_vertical_band_5_4_scale_c(i8* %source, i32 %src_pitch, i8* %dest, i32 %dest_pitch, i32 %dest_width) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %src_pitch.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_pitch.addr = alloca i32, align 4
  %dest_width.addr = alloca i32, align 4
  %dest_end = alloca i8*, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  %d = alloca i32, align 4
  %e = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %src_pitch, i32* %src_pitch.addr, align 4, !tbaa !6
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_pitch, i32* %dest_pitch.addr, align 4, !tbaa !6
  store i32 %dest_width, i32* %dest_width.addr, align 4, !tbaa !6
  %0 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dest_width.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %2
  store i8* %add.ptr, i8** %dest_end, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %dest_end, align 4, !tbaa !2
  %cmp = icmp ult i8* %3, %4
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %7 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %7
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %mul
  %8 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %8 to i32
  store i32 %conv, i32* %a, align 4, !tbaa !6
  %9 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %11 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %11
  %arrayidx2 = getelementptr inbounds i8, i8* %10, i32 %mul1
  %12 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = zext i8 %12 to i32
  store i32 %conv3, i32* %b, align 4, !tbaa !6
  %13 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %15 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 2, %15
  %arrayidx5 = getelementptr inbounds i8, i8* %14, i32 %mul4
  %16 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv6 = zext i8 %16 to i32
  store i32 %conv6, i32* %c, align 4, !tbaa !6
  %17 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %19 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 3, %19
  %arrayidx8 = getelementptr inbounds i8, i8* %18, i32 %mul7
  %20 = load i8, i8* %arrayidx8, align 1, !tbaa !8
  %conv9 = zext i8 %20 to i32
  store i32 %conv9, i32* %d, align 4, !tbaa !6
  %21 = bitcast i32* %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #2
  %22 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %23 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 4, %23
  %arrayidx11 = getelementptr inbounds i8, i8* %22, i32 %mul10
  %24 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  %conv12 = zext i8 %24 to i32
  store i32 %conv12, i32* %e, align 4, !tbaa !6
  %25 = load i32, i32* %a, align 4, !tbaa !6
  %conv13 = trunc i32 %25 to i8
  %26 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %27 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !6
  %mul14 = mul nsw i32 0, %27
  %arrayidx15 = getelementptr inbounds i8, i8* %26, i32 %mul14
  store i8 %conv13, i8* %arrayidx15, align 1, !tbaa !8
  %28 = load i32, i32* %b, align 4, !tbaa !6
  %mul16 = mul i32 %28, 192
  %29 = load i32, i32* %c, align 4, !tbaa !6
  %mul17 = mul i32 %29, 64
  %add = add i32 %mul16, %mul17
  %add18 = add i32 %add, 128
  %shr = lshr i32 %add18, 8
  %conv19 = trunc i32 %shr to i8
  %30 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %31 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 1, %31
  %arrayidx21 = getelementptr inbounds i8, i8* %30, i32 %mul20
  store i8 %conv19, i8* %arrayidx21, align 1, !tbaa !8
  %32 = load i32, i32* %c, align 4, !tbaa !6
  %mul22 = mul i32 %32, 128
  %33 = load i32, i32* %d, align 4, !tbaa !6
  %mul23 = mul i32 %33, 128
  %add24 = add i32 %mul22, %mul23
  %add25 = add i32 %add24, 128
  %shr26 = lshr i32 %add25, 8
  %conv27 = trunc i32 %shr26 to i8
  %34 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %35 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !6
  %mul28 = mul nsw i32 2, %35
  %arrayidx29 = getelementptr inbounds i8, i8* %34, i32 %mul28
  store i8 %conv27, i8* %arrayidx29, align 1, !tbaa !8
  %36 = load i32, i32* %d, align 4, !tbaa !6
  %mul30 = mul i32 %36, 64
  %37 = load i32, i32* %e, align 4, !tbaa !6
  %mul31 = mul i32 %37, 192
  %add32 = add i32 %mul30, %mul31
  %add33 = add i32 %add32, 128
  %shr34 = lshr i32 %add33, 8
  %conv35 = trunc i32 %shr34 to i8
  %38 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %39 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !6
  %mul36 = mul nsw i32 3, %39
  %arrayidx37 = getelementptr inbounds i8, i8* %38, i32 %mul36
  store i8 %conv35, i8* %arrayidx37, align 1, !tbaa !8
  %40 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %40, i32 1
  store i8* %incdec.ptr, i8** %source.addr, align 4, !tbaa !2
  %41 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %incdec.ptr38 = getelementptr inbounds i8, i8* %41, i32 1
  store i8* %incdec.ptr38, i8** %dest.addr, align 4, !tbaa !2
  %42 = bitcast i32* %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #2
  %43 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #2
  %44 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #2
  %45 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #2
  %46 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %47 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_horizontal_line_5_3_scale_c(i8* %source, i32 %source_width, i8* %dest, i32 %dest_width) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %source_width.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_width.addr = alloca i32, align 4
  %source_end = alloca i8*, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  %d = alloca i32, align 4
  %e = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %source_width, i32* %source_width.addr, align 4, !tbaa !6
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_width, i32* %dest_width.addr, align 4, !tbaa !6
  %0 = bitcast i8** %source_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %2 = load i32, i32* %source_width.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %2
  store i8* %add.ptr, i8** %source_end, align 4, !tbaa !2
  %3 = load i32, i32* %dest_width.addr, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %source_end, align 4, !tbaa !2
  %cmp = icmp ult i8* %4, %5
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %7, i32 0
  %8 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %8 to i32
  store i32 %conv, i32* %a, align 4, !tbaa !6
  %9 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %10, i32 1
  %11 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  %conv2 = zext i8 %11 to i32
  store i32 %conv2, i32* %b, align 4, !tbaa !6
  %12 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %13, i32 2
  %14 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  %conv4 = zext i8 %14 to i32
  store i32 %conv4, i32* %c, align 4, !tbaa !6
  %15 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %16, i32 3
  %17 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv6 = zext i8 %17 to i32
  store i32 %conv6, i32* %d, align 4, !tbaa !6
  %18 = bitcast i32* %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %19, i32 4
  %20 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  %conv8 = zext i8 %20 to i32
  store i32 %conv8, i32* %e, align 4, !tbaa !6
  %21 = load i32, i32* %a, align 4, !tbaa !6
  %conv9 = trunc i32 %21 to i8
  %22 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %22, i32 0
  store i8 %conv9, i8* %arrayidx10, align 1, !tbaa !8
  %23 = load i32, i32* %b, align 4, !tbaa !6
  %mul = mul i32 %23, 85
  %24 = load i32, i32* %c, align 4, !tbaa !6
  %mul11 = mul i32 %24, 171
  %add = add i32 %mul, %mul11
  %add12 = add i32 %add, 128
  %shr = lshr i32 %add12, 8
  %conv13 = trunc i32 %shr to i8
  %25 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %25, i32 1
  store i8 %conv13, i8* %arrayidx14, align 1, !tbaa !8
  %26 = load i32, i32* %d, align 4, !tbaa !6
  %mul15 = mul i32 %26, 171
  %27 = load i32, i32* %e, align 4, !tbaa !6
  %mul16 = mul i32 %27, 85
  %add17 = add i32 %mul15, %mul16
  %add18 = add i32 %add17, 128
  %shr19 = lshr i32 %add18, 8
  %conv20 = trunc i32 %shr19 to i8
  %28 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %28, i32 2
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !8
  %29 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i8, i8* %29, i32 5
  store i8* %add.ptr22, i8** %source.addr, align 4, !tbaa !2
  %30 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %add.ptr23 = getelementptr inbounds i8, i8* %30, i32 3
  store i8* %add.ptr23, i8** %dest.addr, align 4, !tbaa !2
  %31 = bitcast i32* %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #2
  %32 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #2
  %33 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #2
  %34 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #2
  %35 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %36 = bitcast i8** %source_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_vertical_band_5_3_scale_c(i8* %source, i32 %src_pitch, i8* %dest, i32 %dest_pitch, i32 %dest_width) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %src_pitch.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_pitch.addr = alloca i32, align 4
  %dest_width.addr = alloca i32, align 4
  %dest_end = alloca i8*, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  %d = alloca i32, align 4
  %e = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %src_pitch, i32* %src_pitch.addr, align 4, !tbaa !6
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_pitch, i32* %dest_pitch.addr, align 4, !tbaa !6
  store i32 %dest_width, i32* %dest_width.addr, align 4, !tbaa !6
  %0 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dest_width.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %2
  store i8* %add.ptr, i8** %dest_end, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %dest_end, align 4, !tbaa !2
  %cmp = icmp ult i8* %3, %4
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %7 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul = mul nsw i32 0, %7
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %mul
  %8 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %8 to i32
  store i32 %conv, i32* %a, align 4, !tbaa !6
  %9 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %11 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 1, %11
  %arrayidx2 = getelementptr inbounds i8, i8* %10, i32 %mul1
  %12 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = zext i8 %12 to i32
  store i32 %conv3, i32* %b, align 4, !tbaa !6
  %13 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %15 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 2, %15
  %arrayidx5 = getelementptr inbounds i8, i8* %14, i32 %mul4
  %16 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv6 = zext i8 %16 to i32
  store i32 %conv6, i32* %c, align 4, !tbaa !6
  %17 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %19 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 3, %19
  %arrayidx8 = getelementptr inbounds i8, i8* %18, i32 %mul7
  %20 = load i8, i8* %arrayidx8, align 1, !tbaa !8
  %conv9 = zext i8 %20 to i32
  store i32 %conv9, i32* %d, align 4, !tbaa !6
  %21 = bitcast i32* %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #2
  %22 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %23 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 4, %23
  %arrayidx11 = getelementptr inbounds i8, i8* %22, i32 %mul10
  %24 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  %conv12 = zext i8 %24 to i32
  store i32 %conv12, i32* %e, align 4, !tbaa !6
  %25 = load i32, i32* %a, align 4, !tbaa !6
  %conv13 = trunc i32 %25 to i8
  %26 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %27 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !6
  %mul14 = mul nsw i32 0, %27
  %arrayidx15 = getelementptr inbounds i8, i8* %26, i32 %mul14
  store i8 %conv13, i8* %arrayidx15, align 1, !tbaa !8
  %28 = load i32, i32* %b, align 4, !tbaa !6
  %mul16 = mul i32 %28, 85
  %29 = load i32, i32* %c, align 4, !tbaa !6
  %mul17 = mul i32 %29, 171
  %add = add i32 %mul16, %mul17
  %add18 = add i32 %add, 128
  %shr = lshr i32 %add18, 8
  %conv19 = trunc i32 %shr to i8
  %30 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %31 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 1, %31
  %arrayidx21 = getelementptr inbounds i8, i8* %30, i32 %mul20
  store i8 %conv19, i8* %arrayidx21, align 1, !tbaa !8
  %32 = load i32, i32* %d, align 4, !tbaa !6
  %mul22 = mul i32 %32, 171
  %33 = load i32, i32* %e, align 4, !tbaa !6
  %mul23 = mul i32 %33, 85
  %add24 = add i32 %mul22, %mul23
  %add25 = add i32 %add24, 128
  %shr26 = lshr i32 %add25, 8
  %conv27 = trunc i32 %shr26 to i8
  %34 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %35 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !6
  %mul28 = mul nsw i32 2, %35
  %arrayidx29 = getelementptr inbounds i8, i8* %34, i32 %mul28
  store i8 %conv27, i8* %arrayidx29, align 1, !tbaa !8
  %36 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %36, i32 1
  store i8* %incdec.ptr, i8** %source.addr, align 4, !tbaa !2
  %37 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %incdec.ptr30 = getelementptr inbounds i8, i8* %37, i32 1
  store i8* %incdec.ptr30, i8** %dest.addr, align 4, !tbaa !2
  %38 = bitcast i32* %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #2
  %39 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #2
  %40 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #2
  %41 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #2
  %42 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %43 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_horizontal_line_2_1_scale_c(i8* %source, i32 %source_width, i8* %dest, i32 %dest_width) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %source_width.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_width.addr = alloca i32, align 4
  %source_end = alloca i8*, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %source_width, i32* %source_width.addr, align 4, !tbaa !6
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_width, i32* %dest_width.addr, align 4, !tbaa !6
  %0 = bitcast i8** %source_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %2 = load i32, i32* %source_width.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %2
  store i8* %add.ptr, i8** %source_end, align 4, !tbaa !2
  %3 = load i32, i32* %dest_width.addr, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %source_end, align 4, !tbaa !2
  %cmp = icmp ult i8* %4, %5
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 0
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %8 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %8, i32 0
  store i8 %7, i8* %arrayidx1, align 1, !tbaa !8
  %9 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds i8, i8* %9, i32 2
  store i8* %add.ptr2, i8** %source.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %10, i32 1
  store i8* %incdec.ptr, i8** %dest.addr, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %11 = bitcast i8** %source_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_vertical_band_2_1_scale_c(i8* %source, i32 %src_pitch, i8* %dest, i32 %dest_pitch, i32 %dest_width) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %src_pitch.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_pitch.addr = alloca i32, align 4
  %dest_width.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %src_pitch, i32* %src_pitch.addr, align 4, !tbaa !6
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_pitch, i32* %dest_pitch.addr, align 4, !tbaa !6
  store i32 %dest_width, i32* %dest_width.addr, align 4, !tbaa !6
  %0 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !6
  %1 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %4 = load i32, i32* %dest_width.addr, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %3, i32 %4, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden void @aom_vertical_band_2_1_scale_i_c(i8* %source, i32 %src_pitch, i8* %dest, i32 %dest_pitch, i32 %dest_width) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %src_pitch.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_pitch.addr = alloca i32, align 4
  %dest_width.addr = alloca i32, align 4
  %dest_end = alloca i8*, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %src_pitch, i32* %src_pitch.addr, align 4, !tbaa !6
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_pitch, i32* %dest_pitch.addr, align 4, !tbaa !6
  store i32 %dest_width, i32* %dest_width.addr, align 4, !tbaa !6
  %0 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dest_width.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %2
  store i8* %add.ptr, i8** %dest_end, align 4, !tbaa !2
  %3 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %dest_end, align 4, !tbaa !2
  %cmp = icmp ult i8* %4, %5
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %8 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %sub = sub nsw i32 0, %8
  %arrayidx = getelementptr inbounds i8, i8* %7, i32 %sub
  %9 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %9 to i32
  %mul = mul nsw i32 %conv, 3
  store i32 %mul, i32* %a, align 4, !tbaa !6
  %10 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %11, i32 0
  %12 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  %conv2 = zext i8 %12 to i32
  %mul3 = mul nsw i32 %conv2, 10
  store i32 %mul3, i32* %b, align 4, !tbaa !6
  %13 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %15 = load i32, i32* %src_pitch.addr, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = zext i8 %16 to i32
  %mul6 = mul nsw i32 %conv5, 3
  store i32 %mul6, i32* %c, align 4, !tbaa !6
  %17 = load i32, i32* %a, align 4, !tbaa !6
  %add = add i32 8, %17
  %18 = load i32, i32* %b, align 4, !tbaa !6
  %add7 = add i32 %add, %18
  %19 = load i32, i32* %c, align 4, !tbaa !6
  %add8 = add i32 %add7, %19
  %shr = lshr i32 %add8, 4
  %conv9 = trunc i32 %shr to i8
  %20 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %20, i32 0
  store i8 %conv9, i8* %arrayidx10, align 1, !tbaa !8
  %21 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %21, i32 1
  store i8* %incdec.ptr, i8** %source.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %incdec.ptr11 = getelementptr inbounds i8, i8* %22, i32 1
  store i8* %incdec.ptr11, i8** %dest.addr, align 4, !tbaa !2
  %23 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #2
  %24 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #2
  %25 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %26 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #2
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
