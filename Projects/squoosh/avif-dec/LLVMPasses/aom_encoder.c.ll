; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_encoder.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_encoder.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_codec_ctx = type { i8*, %struct.aom_codec_iface*, i32, i8*, i32, %union.anon.0, %struct.aom_codec_priv* }
%union.anon.0 = type { %struct.aom_codec_dec_cfg* }
%struct.aom_codec_dec_cfg = type { i32, i32, i32, i32 }
%struct.aom_codec_priv = type { i8*, i32, %struct.anon.1 }
%struct.anon.1 = type { %struct.aom_fixed_buf, i32, i32, %struct.aom_codec_cx_pkt }
%struct.aom_fixed_buf = type { i8*, i32 }
%struct.aom_codec_cx_pkt = type { i32, %union.anon }
%union.anon = type { %struct.aom_psnr_pkt, [48 x i8] }
%struct.aom_psnr_pkt = type { [4 x i32], [4 x i64], [4 x double] }
%struct.aom_codec_iface = type { i8*, i32, i32, i32 (%struct.aom_codec_ctx*)*, i32 (%struct.aom_codec_alg_priv*)*, %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_dec_iface, %struct.aom_codec_enc_iface }
%struct.aom_codec_alg_priv = type opaque
%struct.aom_codec_ctrl_fn_map = type { i32, i32 (%struct.aom_codec_alg_priv*, i8*)* }
%struct.aom_codec_dec_iface = type { i32 (i8*, i32, %struct.aom_codec_stream_info*)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_stream_info*)*, i32 (%struct.aom_codec_alg_priv*, i8*, i32, i8*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*, i8**)*, i32 (%struct.aom_codec_alg_priv*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, i8*)* }
%struct.aom_codec_stream_info = type { i32, i32, i32, i32, i32, i32 }
%struct.aom_image = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.aom_metadata_array*, i8* }
%struct.aom_metadata_array = type opaque
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.aom_codec_enc_iface = type { i32, %struct.aom_codec_enc_cfg*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_image*, i64, i32, i32)*, %struct.aom_codec_cx_pkt* (%struct.aom_codec_alg_priv*, i8**)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_enc_cfg*)*, %struct.aom_fixed_buf* (%struct.aom_codec_alg_priv*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*)* }
%struct.aom_codec_enc_cfg = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_rational, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_fixed_buf, %struct.aom_fixed_buf, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [64 x i32], [64 x i32], i32, [5 x i32], %struct.cfg_options }
%struct.aom_rational = type { i32, i32 }
%struct.cfg_options = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.aom_codec_pkt_list = type { i32, i32, [1 x %struct.aom_codec_cx_pkt] }

; Function Attrs: nounwind
define hidden i32 @aom_codec_enc_init_ver(%struct.aom_codec_ctx* %ctx, %struct.aom_codec_iface* %iface, %struct.aom_codec_enc_cfg* %cfg, i32 %flags, i32 %ver) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %iface.addr = alloca %struct.aom_codec_iface*, align 4
  %cfg.addr = alloca %struct.aom_codec_enc_cfg*, align 4
  %flags.addr = alloca i32, align 4
  %ver.addr = alloca i32, align 4
  %res = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store %struct.aom_codec_iface* %iface, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  store %struct.aom_codec_enc_cfg* %cfg, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  store i32 %flags, i32* %flags.addr, align 4, !tbaa !6
  store i32 %ver, i32* %ver.addr, align 4, !tbaa !8
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %ver.addr, align 4, !tbaa !8
  %cmp = icmp ne i32 %1, 22
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 3, i32* %res, align 4, !tbaa !10
  br label %if.end33

if.else:                                          ; preds = %entry
  %2 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %2, null
  br i1 %tobool, label %lor.lhs.false, label %if.then4

lor.lhs.false:                                    ; preds = %if.else
  %3 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.aom_codec_iface* %3, null
  br i1 %tobool1, label %lor.lhs.false2, label %if.then4

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %4 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %tobool3 = icmp ne %struct.aom_codec_enc_cfg* %4, null
  br i1 %tobool3, label %if.else5, label %if.then4

if.then4:                                         ; preds = %lor.lhs.false2, %lor.lhs.false, %if.else
  store i32 8, i32* %res, align 4, !tbaa !10
  br label %if.end32

if.else5:                                         ; preds = %lor.lhs.false2
  %5 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %abi_version = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %5, i32 0, i32 1
  %6 = load i32, i32* %abi_version, align 4, !tbaa !11
  %cmp6 = icmp ne i32 %6, 7
  br i1 %cmp6, label %if.then7, label %if.else8

if.then7:                                         ; preds = %if.else5
  store i32 3, i32* %res, align 4, !tbaa !10
  br label %if.end31

if.else8:                                         ; preds = %if.else5
  %7 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %7, i32 0, i32 2
  %8 = load i32, i32* %caps, align 4, !tbaa !15
  %and = and i32 %8, 2
  %tobool9 = icmp ne i32 %and, 0
  br i1 %tobool9, label %if.else11, label %if.then10

if.then10:                                        ; preds = %if.else8
  store i32 4, i32* %res, align 4, !tbaa !10
  br label %if.end30

if.else11:                                        ; preds = %if.else8
  %9 = load i32, i32* %flags.addr, align 4, !tbaa !6
  %and12 = and i32 %9, 65536
  %tobool13 = icmp ne i32 %and12, 0
  br i1 %tobool13, label %land.lhs.true, label %if.else18

land.lhs.true:                                    ; preds = %if.else11
  %10 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %caps14 = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %10, i32 0, i32 2
  %11 = load i32, i32* %caps14, align 4, !tbaa !15
  %and15 = and i32 %11, 65536
  %tobool16 = icmp ne i32 %and15, 0
  br i1 %tobool16, label %if.else18, label %if.then17

if.then17:                                        ; preds = %land.lhs.true
  store i32 4, i32* %res, align 4, !tbaa !10
  br label %if.end29

if.else18:                                        ; preds = %land.lhs.true, %if.else11
  %12 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %13 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface19 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %13, i32 0, i32 1
  store %struct.aom_codec_iface* %12, %struct.aom_codec_iface** %iface19, align 4, !tbaa !16
  %14 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %name = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %14, i32 0, i32 0
  %15 = load i8*, i8** %name, align 4, !tbaa !18
  %16 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %name20 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %16, i32 0, i32 0
  store i8* %15, i8** %name20, align 4, !tbaa !19
  %17 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %17, i32 0, i32 6
  store %struct.aom_codec_priv* null, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %18 = load i32, i32* %flags.addr, align 4, !tbaa !6
  %19 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %init_flags = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %19, i32 0, i32 4
  store i32 %18, i32* %init_flags, align 4, !tbaa !21
  %20 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %21 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %config = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %21, i32 0, i32 5
  %enc = bitcast %union.anon.0* %config to %struct.aom_codec_enc_cfg**
  store %struct.aom_codec_enc_cfg* %20, %struct.aom_codec_enc_cfg** %enc, align 4, !tbaa !10
  %22 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface21 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %22, i32 0, i32 1
  %23 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface21, align 4, !tbaa !16
  %init = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %23, i32 0, i32 3
  %24 = load i32 (%struct.aom_codec_ctx*)*, i32 (%struct.aom_codec_ctx*)** %init, align 4, !tbaa !22
  %25 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call i32 %24(%struct.aom_codec_ctx* %25)
  store i32 %call, i32* %res, align 4, !tbaa !10
  %26 = load i32, i32* %res, align 4, !tbaa !10
  %tobool22 = icmp ne i32 %26, 0
  br i1 %tobool22, label %if.then23, label %if.end

if.then23:                                        ; preds = %if.else18
  %27 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv24 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %27, i32 0, i32 6
  %28 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv24, align 4, !tbaa !20
  %tobool25 = icmp ne %struct.aom_codec_priv* %28, null
  br i1 %tobool25, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then23
  %29 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv26 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %29, i32 0, i32 6
  %30 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv26, align 4, !tbaa !20
  %err_detail = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %30, i32 0, i32 0
  %31 = load i8*, i8** %err_detail, align 8, !tbaa !23
  br label %cond.end

cond.false:                                       ; preds = %if.then23
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %31, %cond.true ], [ null, %cond.false ]
  %32 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err_detail27 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %32, i32 0, i32 3
  store i8* %cond, i8** %err_detail27, align 4, !tbaa !28
  %33 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call28 = call i32 @aom_codec_destroy(%struct.aom_codec_ctx* %33)
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.else18
  br label %if.end29

if.end29:                                         ; preds = %if.end, %if.then17
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %if.then10
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %if.then7
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.then4
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.then
  %34 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool34 = icmp ne %struct.aom_codec_ctx* %34, null
  br i1 %tobool34, label %cond.true35, label %cond.false36

cond.true35:                                      ; preds = %if.end33
  %35 = load i32, i32* %res, align 4, !tbaa !10
  %36 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %36, i32 0, i32 2
  store i32 %35, i32* %err, align 4, !tbaa !29
  br label %cond.end37

cond.false36:                                     ; preds = %if.end33
  %37 = load i32, i32* %res, align 4, !tbaa !10
  br label %cond.end37

cond.end37:                                       ; preds = %cond.false36, %cond.true35
  %cond38 = phi i32 [ %35, %cond.true35 ], [ %37, %cond.false36 ]
  %38 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  ret i32 %cond38
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i32 @aom_codec_destroy(%struct.aom_codec_ctx*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @aom_codec_enc_config_default(%struct.aom_codec_iface* %iface, %struct.aom_codec_enc_cfg* %cfg, i32 %usage) #0 {
entry:
  %iface.addr = alloca %struct.aom_codec_iface*, align 4
  %cfg.addr = alloca %struct.aom_codec_enc_cfg*, align 4
  %usage.addr = alloca i32, align 4
  %res = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.aom_codec_iface* %iface, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  store %struct.aom_codec_enc_cfg* %cfg, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  store i32 %usage, i32* %usage.addr, align 4, !tbaa !8
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_iface* %2, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %3 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.aom_codec_enc_cfg* %3, null
  br i1 %tobool1, label %if.else, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 8, i32* %res, align 4, !tbaa !10
  br label %if.end12

if.else:                                          ; preds = %lor.lhs.false
  %4 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %4, i32 0, i32 2
  %5 = load i32, i32* %caps, align 4, !tbaa !15
  %and = and i32 %5, 2
  %tobool2 = icmp ne i32 %and, 0
  br i1 %tobool2, label %if.else4, label %if.then3

if.then3:                                         ; preds = %if.else
  store i32 4, i32* %res, align 4, !tbaa !10
  br label %if.end11

if.else4:                                         ; preds = %if.else
  store i32 8, i32* %res, align 4, !tbaa !10
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else4
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %7 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %enc = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %7, i32 0, i32 7
  %cfg_count = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc, i32 0, i32 0
  %8 = load i32, i32* %cfg_count, align 4, !tbaa !30
  %cmp = icmp slt i32 %6, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %enc5 = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %9, i32 0, i32 7
  %cfgs = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc5, i32 0, i32 1
  %10 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfgs, align 4, !tbaa !31
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %10, i32 %11
  %g_usage = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %arrayidx, i32 0, i32 0
  %12 = load i32, i32* %g_usage, align 4, !tbaa !32
  %13 = load i32, i32* %usage.addr, align 4, !tbaa !8
  %cmp6 = icmp eq i32 %12, %13
  br i1 %cmp6, label %if.then7, label %if.end

if.then7:                                         ; preds = %for.body
  %14 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %15 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %enc8 = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %15, i32 0, i32 7
  %cfgs9 = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc8, i32 0, i32 1
  %16 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfgs9, align 4, !tbaa !31
  %17 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx10 = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %16, i32 %17
  %18 = bitcast %struct.aom_codec_enc_cfg* %14 to i8*
  %19 = bitcast %struct.aom_codec_enc_cfg* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 884, i1 false), !tbaa.struct !36
  store i32 0, i32* %res, align 4, !tbaa !10
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %20 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %if.then7, %for.cond
  br label %if.end11

if.end11:                                         ; preds = %for.end, %if.then3
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.then
  %21 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %tobool13 = icmp ne %struct.aom_codec_enc_cfg* %21, null
  br i1 %tobool13, label %if.then14, label %if.end19

if.then14:                                        ; preds = %if.end12
  %22 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %encoder_cfg = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %22, i32 0, i32 53
  %23 = bitcast %struct.cfg_options* %encoder_cfg to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %23, i8 0, i32 140, i1 false)
  %24 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %encoder_cfg15 = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %24, i32 0, i32 53
  %super_block_size = getelementptr inbounds %struct.cfg_options, %struct.cfg_options* %encoder_cfg15, i32 0, i32 1
  store i32 0, i32* %super_block_size, align 4, !tbaa !37
  %25 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %encoder_cfg16 = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %25, i32 0, i32 53
  %max_partition_size = getelementptr inbounds %struct.cfg_options, %struct.cfg_options* %encoder_cfg16, i32 0, i32 2
  store i32 128, i32* %max_partition_size, align 4, !tbaa !38
  %26 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %encoder_cfg17 = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %26, i32 0, i32 53
  %min_partition_size = getelementptr inbounds %struct.cfg_options, %struct.cfg_options* %encoder_cfg17, i32 0, i32 3
  store i32 4, i32* %min_partition_size, align 4, !tbaa !39
  %27 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %encoder_cfg18 = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %27, i32 0, i32 53
  %disable_trellis_quant = getelementptr inbounds %struct.cfg_options, %struct.cfg_options* %encoder_cfg18, i32 0, i32 31
  store i32 3, i32* %disable_trellis_quant, align 4, !tbaa !40
  br label %if.end19

if.end19:                                         ; preds = %if.then14, %if.end12
  %28 = load i32, i32* %res, align 4, !tbaa !10
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  %30 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  ret i32 %28
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define hidden i32 @aom_codec_encode(%struct.aom_codec_ctx* %ctx, %struct.aom_image* %img, i64 %pts, i32 %duration, i32 %flags) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  %pts.addr = alloca i64, align 8
  %duration.addr = alloca i32, align 4
  %flags.addr = alloca i32, align 4
  %res = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i64 %pts, i64* %pts.addr, align 8, !tbaa !41
  store i32 %duration, i32* %duration.addr, align 4, !tbaa !6
  store i32 %flags, i32* %flags.addr, align 4, !tbaa !6
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %res, align 4, !tbaa !10
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.aom_image* %2, null
  br i1 %tobool1, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %lor.lhs.false
  %3 = load i32, i32* %duration.addr, align 4, !tbaa !6
  %tobool2 = icmp ne i32 %3, 0
  br i1 %tobool2, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true, %entry
  store i32 8, i32* %res, align 4, !tbaa !10
  br label %if.end15

if.else:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %4 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %4, i32 0, i32 1
  %5 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !16
  %tobool3 = icmp ne %struct.aom_codec_iface* %5, null
  br i1 %tobool3, label %lor.lhs.false4, label %if.then6

lor.lhs.false4:                                   ; preds = %if.else
  %6 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %6, i32 0, i32 6
  %7 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool5 = icmp ne %struct.aom_codec_priv* %7, null
  br i1 %tobool5, label %if.else7, label %if.then6

if.then6:                                         ; preds = %lor.lhs.false4, %if.else
  store i32 1, i32* %res, align 4, !tbaa !10
  br label %if.end14

if.else7:                                         ; preds = %lor.lhs.false4
  %8 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface8 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %8, i32 0, i32 1
  %9 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface8, align 4, !tbaa !16
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %9, i32 0, i32 2
  %10 = load i32, i32* %caps, align 4, !tbaa !15
  %and = and i32 %10, 2
  %tobool9 = icmp ne i32 %and, 0
  br i1 %tobool9, label %if.else11, label %if.then10

if.then10:                                        ; preds = %if.else7
  store i32 4, i32* %res, align 4, !tbaa !10
  br label %if.end

if.else11:                                        ; preds = %if.else7
  br label %do.body

do.body:                                          ; preds = %if.else11
  %11 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface12 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %11, i32 0, i32 1
  %12 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface12, align 4, !tbaa !16
  %enc = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %12, i32 0, i32 7
  %encode = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc, i32 0, i32 2
  %13 = load i32 (%struct.aom_codec_alg_priv*, %struct.aom_image*, i64, i32, i32)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_image*, i64, i32, i32)** %encode, align 4, !tbaa !43
  %14 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %14)
  %15 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %16 = load i64, i64* %pts.addr, align 8, !tbaa !41
  %17 = load i32, i32* %duration.addr, align 4, !tbaa !6
  %18 = load i32, i32* %flags.addr, align 4, !tbaa !6
  %call13 = call i32 %13(%struct.aom_codec_alg_priv* %call, %struct.aom_image* %15, i64 %16, i32 %17, i32 %18)
  store i32 %call13, i32* %res, align 4, !tbaa !10
  br label %do.cond

do.cond:                                          ; preds = %do.body
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end

if.end:                                           ; preds = %do.end, %if.then10
  br label %if.end14

if.end14:                                         ; preds = %if.end, %if.then6
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.then
  %19 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool16 = icmp ne %struct.aom_codec_ctx* %19, null
  br i1 %tobool16, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end15
  %20 = load i32, i32* %res, align 4, !tbaa !10
  %21 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %21, i32 0, i32 2
  store i32 %20, i32* %err, align 4, !tbaa !29
  br label %cond.end

cond.false:                                       ; preds = %if.end15
  %22 = load i32, i32* %res, align 4, !tbaa !10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %20, %cond.true ], [ %22, %cond.false ]
  %23 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  ret i32 %cond
}

; Function Attrs: nounwind
define internal %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %0, i32 0, i32 6
  %1 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %2 = bitcast %struct.aom_codec_priv* %1 to %struct.aom_codec_alg_priv*
  ret %struct.aom_codec_alg_priv* %2
}

; Function Attrs: nounwind
define hidden %struct.aom_codec_cx_pkt* @aom_codec_get_cx_data(%struct.aom_codec_ctx* %ctx, i8** %iter) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %iter.addr = alloca i8**, align 4
  %pkt = alloca %struct.aom_codec_cx_pkt*, align 4
  %priv20 = alloca %struct.aom_codec_priv*, align 4
  %dst_buf = alloca i8*, align 4
  %modified_pkt = alloca %struct.aom_codec_cx_pkt*, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store i8** %iter, i8*** %iter.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_codec_cx_pkt** %pkt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store %struct.aom_codec_cx_pkt* null, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %if.then, label %if.end17

if.then:                                          ; preds = %entry
  %2 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  %tobool1 = icmp ne i8** %2, null
  br i1 %tobool1, label %if.else, label %if.then2

if.then2:                                         ; preds = %if.then
  %3 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %3, i32 0, i32 2
  store i32 8, i32* %err, align 4, !tbaa !29
  br label %if.end16

if.else:                                          ; preds = %if.then
  %4 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %4, i32 0, i32 1
  %5 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !16
  %tobool3 = icmp ne %struct.aom_codec_iface* %5, null
  br i1 %tobool3, label %lor.lhs.false, label %if.then5

lor.lhs.false:                                    ; preds = %if.else
  %6 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %6, i32 0, i32 6
  %7 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool4 = icmp ne %struct.aom_codec_priv* %7, null
  br i1 %tobool4, label %if.else7, label %if.then5

if.then5:                                         ; preds = %lor.lhs.false, %if.else
  %8 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %8, i32 0, i32 2
  store i32 1, i32* %err6, align 4, !tbaa !29
  br label %if.end15

if.else7:                                         ; preds = %lor.lhs.false
  %9 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface8 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %9, i32 0, i32 1
  %10 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface8, align 4, !tbaa !16
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %10, i32 0, i32 2
  %11 = load i32, i32* %caps, align 4, !tbaa !15
  %and = and i32 %11, 2
  %tobool9 = icmp ne i32 %and, 0
  br i1 %tobool9, label %if.else12, label %if.then10

if.then10:                                        ; preds = %if.else7
  %12 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err11 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %12, i32 0, i32 2
  store i32 4, i32* %err11, align 4, !tbaa !29
  br label %if.end

if.else12:                                        ; preds = %if.else7
  %13 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface13 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %13, i32 0, i32 1
  %14 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface13, align 4, !tbaa !16
  %enc = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %14, i32 0, i32 7
  %get_cx_data = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc, i32 0, i32 3
  %15 = load %struct.aom_codec_cx_pkt* (%struct.aom_codec_alg_priv*, i8**)*, %struct.aom_codec_cx_pkt* (%struct.aom_codec_alg_priv*, i8**)** %get_cx_data, align 4, !tbaa !44
  %16 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %16)
  %17 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  %call14 = call %struct.aom_codec_cx_pkt* %15(%struct.aom_codec_alg_priv* %call, i8** %17)
  store %struct.aom_codec_cx_pkt* %call14, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else12, %if.then10
  br label %if.end15

if.end15:                                         ; preds = %if.end, %if.then5
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %if.then2
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %entry
  %18 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %tobool18 = icmp ne %struct.aom_codec_cx_pkt* %18, null
  br i1 %tobool18, label %land.lhs.true, label %if.end79

land.lhs.true:                                    ; preds = %if.end17
  %19 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %kind = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %19, i32 0, i32 0
  %20 = load i32, i32* %kind, align 8, !tbaa !45
  %cmp = icmp eq i32 %20, 0
  br i1 %cmp, label %if.then19, label %if.end79

if.then19:                                        ; preds = %land.lhs.true
  %21 = bitcast %struct.aom_codec_priv** %priv20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv21 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %22, i32 0, i32 6
  %23 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv21, align 4, !tbaa !20
  store %struct.aom_codec_priv* %23, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %24 = bitcast i8** %dst_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc22 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %25, i32 0, i32 2
  %cx_data_dst_buf = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc22, i32 0, i32 0
  %buf = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %cx_data_dst_buf, i32 0, i32 0
  %26 = load i8*, i8** %buf, align 8, !tbaa !46
  store i8* %26, i8** %dst_buf, align 4, !tbaa !2
  %27 = load i8*, i8** %dst_buf, align 4, !tbaa !2
  %tobool23 = icmp ne i8* %27, null
  br i1 %tobool23, label %land.lhs.true24, label %if.end59

land.lhs.true24:                                  ; preds = %if.then19
  %28 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %28, i32 0, i32 1
  %raw = bitcast %union.anon* %data to %struct.aom_fixed_buf*
  %buf25 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %raw, i32 0, i32 0
  %29 = load i8*, i8** %buf25, align 8, !tbaa !10
  %30 = load i8*, i8** %dst_buf, align 4, !tbaa !2
  %cmp26 = icmp ne i8* %29, %30
  br i1 %cmp26, label %land.lhs.true27, label %if.end59

land.lhs.true27:                                  ; preds = %land.lhs.true24
  %31 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data28 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %31, i32 0, i32 1
  %raw29 = bitcast %union.anon* %data28 to %struct.aom_fixed_buf*
  %sz = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %raw29, i32 0, i32 1
  %32 = load i32, i32* %sz, align 4, !tbaa !10
  %33 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc30 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %33, i32 0, i32 2
  %cx_data_pad_before = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc30, i32 0, i32 1
  %34 = load i32, i32* %cx_data_pad_before, align 8, !tbaa !47
  %add = add i32 %32, %34
  %35 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc31 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %35, i32 0, i32 2
  %cx_data_pad_after = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc31, i32 0, i32 2
  %36 = load i32, i32* %cx_data_pad_after, align 4, !tbaa !48
  %add32 = add i32 %add, %36
  %37 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc33 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %37, i32 0, i32 2
  %cx_data_dst_buf34 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc33, i32 0, i32 0
  %sz35 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %cx_data_dst_buf34, i32 0, i32 1
  %38 = load i32, i32* %sz35, align 4, !tbaa !49
  %cmp36 = icmp ule i32 %add32, %38
  br i1 %cmp36, label %if.then37, label %if.end59

if.then37:                                        ; preds = %land.lhs.true27
  %39 = bitcast %struct.aom_codec_cx_pkt** %modified_pkt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #4
  %40 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc38 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %40, i32 0, i32 2
  %cx_data_pkt = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc38, i32 0, i32 3
  store %struct.aom_codec_cx_pkt* %cx_data_pkt, %struct.aom_codec_cx_pkt** %modified_pkt, align 4, !tbaa !2
  %41 = load i8*, i8** %dst_buf, align 4, !tbaa !2
  %42 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc39 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %42, i32 0, i32 2
  %cx_data_pad_before40 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc39, i32 0, i32 1
  %43 = load i32, i32* %cx_data_pad_before40, align 8, !tbaa !47
  %add.ptr = getelementptr inbounds i8, i8* %41, i32 %43
  %44 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data41 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %44, i32 0, i32 1
  %raw42 = bitcast %union.anon* %data41 to %struct.aom_fixed_buf*
  %buf43 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %raw42, i32 0, i32 0
  %45 = load i8*, i8** %buf43, align 8, !tbaa !10
  %46 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data44 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %46, i32 0, i32 1
  %raw45 = bitcast %union.anon* %data44 to %struct.aom_fixed_buf*
  %sz46 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %raw45, i32 0, i32 1
  %47 = load i32, i32* %sz46, align 4, !tbaa !10
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %45, i32 %47, i1 false)
  %48 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %modified_pkt, align 4, !tbaa !2
  %49 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %50 = bitcast %struct.aom_codec_cx_pkt* %48 to i8*
  %51 = bitcast %struct.aom_codec_cx_pkt* %49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %50, i8* align 8 %51, i32 136, i1 false), !tbaa.struct !50
  %52 = load i8*, i8** %dst_buf, align 4, !tbaa !2
  %53 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %modified_pkt, align 4, !tbaa !2
  %data47 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %53, i32 0, i32 1
  %raw48 = bitcast %union.anon* %data47 to %struct.aom_fixed_buf*
  %buf49 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %raw48, i32 0, i32 0
  store i8* %52, i8** %buf49, align 8, !tbaa !10
  %54 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc50 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %54, i32 0, i32 2
  %cx_data_pad_before51 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc50, i32 0, i32 1
  %55 = load i32, i32* %cx_data_pad_before51, align 8, !tbaa !47
  %56 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc52 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %56, i32 0, i32 2
  %cx_data_pad_after53 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc52, i32 0, i32 2
  %57 = load i32, i32* %cx_data_pad_after53, align 4, !tbaa !48
  %add54 = add i32 %55, %57
  %58 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %modified_pkt, align 4, !tbaa !2
  %data55 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %58, i32 0, i32 1
  %raw56 = bitcast %union.anon* %data55 to %struct.aom_fixed_buf*
  %sz57 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %raw56, i32 0, i32 1
  %59 = load i32, i32* %sz57, align 4, !tbaa !10
  %add58 = add i32 %59, %add54
  store i32 %add58, i32* %sz57, align 4, !tbaa !10
  %60 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %modified_pkt, align 4, !tbaa !2
  store %struct.aom_codec_cx_pkt* %60, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %61 = bitcast %struct.aom_codec_cx_pkt** %modified_pkt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  br label %if.end59

if.end59:                                         ; preds = %if.then37, %land.lhs.true27, %land.lhs.true24, %if.then19
  %62 = load i8*, i8** %dst_buf, align 4, !tbaa !2
  %63 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data60 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %63, i32 0, i32 1
  %raw61 = bitcast %union.anon* %data60 to %struct.aom_fixed_buf*
  %buf62 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %raw61, i32 0, i32 0
  %64 = load i8*, i8** %buf62, align 8, !tbaa !10
  %cmp63 = icmp eq i8* %62, %64
  br i1 %cmp63, label %if.then64, label %if.end78

if.then64:                                        ; preds = %if.end59
  %65 = load i8*, i8** %dst_buf, align 4, !tbaa !2
  %66 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data65 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %66, i32 0, i32 1
  %raw66 = bitcast %union.anon* %data65 to %struct.aom_fixed_buf*
  %sz67 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %raw66, i32 0, i32 1
  %67 = load i32, i32* %sz67, align 4, !tbaa !10
  %add.ptr68 = getelementptr inbounds i8, i8* %65, i32 %67
  %68 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc69 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %68, i32 0, i32 2
  %cx_data_dst_buf70 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc69, i32 0, i32 0
  %buf71 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %cx_data_dst_buf70, i32 0, i32 0
  store i8* %add.ptr68, i8** %buf71, align 8, !tbaa !46
  %69 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data72 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %69, i32 0, i32 1
  %raw73 = bitcast %union.anon* %data72 to %struct.aom_fixed_buf*
  %sz74 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %raw73, i32 0, i32 1
  %70 = load i32, i32* %sz74, align 4, !tbaa !10
  %71 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv20, align 4, !tbaa !2
  %enc75 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %71, i32 0, i32 2
  %cx_data_dst_buf76 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc75, i32 0, i32 0
  %sz77 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %cx_data_dst_buf76, i32 0, i32 1
  %72 = load i32, i32* %sz77, align 4, !tbaa !49
  %sub = sub i32 %72, %70
  store i32 %sub, i32* %sz77, align 4, !tbaa !49
  br label %if.end78

if.end78:                                         ; preds = %if.then64, %if.end59
  %73 = bitcast i8** %dst_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  %74 = bitcast %struct.aom_codec_priv** %priv20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %land.lhs.true, %if.end17
  %75 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %76 = bitcast %struct.aom_codec_cx_pkt** %pkt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  ret %struct.aom_codec_cx_pkt* %75
}

; Function Attrs: nounwind
define hidden i32 @aom_codec_set_cx_data_buf(%struct.aom_codec_ctx* %ctx, %struct.aom_fixed_buf* %buf, i32 %pad_before, i32 %pad_after) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %buf.addr = alloca %struct.aom_fixed_buf*, align 4
  %pad_before.addr = alloca i32, align 4
  %pad_after.addr = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store %struct.aom_fixed_buf* %buf, %struct.aom_fixed_buf** %buf.addr, align 4, !tbaa !2
  store i32 %pad_before, i32* %pad_before.addr, align 4, !tbaa !8
  store i32 %pad_after, i32* %pad_after.addr, align 4, !tbaa !8
  %0 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %0, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %1, i32 0, i32 6
  %2 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool1 = icmp ne %struct.aom_codec_priv* %2, null
  br i1 %tobool1, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 8, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load %struct.aom_fixed_buf*, %struct.aom_fixed_buf** %buf.addr, align 4, !tbaa !2
  %tobool2 = icmp ne %struct.aom_fixed_buf* %3, null
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %4 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv4 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %4, i32 0, i32 6
  %5 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv4, align 4, !tbaa !20
  %enc = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %5, i32 0, i32 2
  %cx_data_dst_buf = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc, i32 0, i32 0
  %6 = load %struct.aom_fixed_buf*, %struct.aom_fixed_buf** %buf.addr, align 4, !tbaa !2
  %7 = bitcast %struct.aom_fixed_buf* %cx_data_dst_buf to i8*
  %8 = bitcast %struct.aom_fixed_buf* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %7, i8* align 4 %8, i32 8, i1 false), !tbaa.struct !51
  %9 = load i32, i32* %pad_before.addr, align 4, !tbaa !8
  %10 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv5 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %10, i32 0, i32 6
  %11 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv5, align 4, !tbaa !20
  %enc6 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %11, i32 0, i32 2
  %cx_data_pad_before = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc6, i32 0, i32 1
  store i32 %9, i32* %cx_data_pad_before, align 8, !tbaa !47
  %12 = load i32, i32* %pad_after.addr, align 4, !tbaa !8
  %13 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv7 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %13, i32 0, i32 6
  %14 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv7, align 4, !tbaa !20
  %enc8 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %14, i32 0, i32 2
  %cx_data_pad_after = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc8, i32 0, i32 2
  store i32 %12, i32* %cx_data_pad_after, align 4, !tbaa !48
  br label %if.end22

if.else:                                          ; preds = %if.end
  %15 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv9 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %15, i32 0, i32 6
  %16 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv9, align 4, !tbaa !20
  %enc10 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %16, i32 0, i32 2
  %cx_data_dst_buf11 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc10, i32 0, i32 0
  %buf12 = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %cx_data_dst_buf11, i32 0, i32 0
  store i8* null, i8** %buf12, align 8, !tbaa !46
  %17 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv13 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %17, i32 0, i32 6
  %18 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv13, align 4, !tbaa !20
  %enc14 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %18, i32 0, i32 2
  %cx_data_dst_buf15 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc14, i32 0, i32 0
  %sz = getelementptr inbounds %struct.aom_fixed_buf, %struct.aom_fixed_buf* %cx_data_dst_buf15, i32 0, i32 1
  store i32 0, i32* %sz, align 4, !tbaa !49
  %19 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv16 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %19, i32 0, i32 6
  %20 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv16, align 4, !tbaa !20
  %enc17 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %20, i32 0, i32 2
  %cx_data_pad_before18 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc17, i32 0, i32 1
  store i32 0, i32* %cx_data_pad_before18, align 8, !tbaa !47
  %21 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv19 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %21, i32 0, i32 6
  %22 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv19, align 4, !tbaa !20
  %enc20 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %22, i32 0, i32 2
  %cx_data_pad_after21 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %enc20, i32 0, i32 2
  store i32 0, i32* %cx_data_pad_after21, align 4, !tbaa !48
  br label %if.end22

if.end22:                                         ; preds = %if.else, %if.then3
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %if.then
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: nounwind
define hidden %struct.aom_image* @aom_codec_get_preview_frame(%struct.aom_codec_ctx* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %img = alloca %struct.aom_image*, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_image** %img to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store %struct.aom_image* null, %struct.aom_image** %img, align 4, !tbaa !2
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %if.then, label %if.end20

if.then:                                          ; preds = %entry
  %2 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %2, i32 0, i32 1
  %3 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !16
  %tobool1 = icmp ne %struct.aom_codec_iface* %3, null
  br i1 %tobool1, label %lor.lhs.false, label %if.then3

lor.lhs.false:                                    ; preds = %if.then
  %4 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %4, i32 0, i32 6
  %5 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool2 = icmp ne %struct.aom_codec_priv* %5, null
  br i1 %tobool2, label %if.else, label %if.then3

if.then3:                                         ; preds = %lor.lhs.false, %if.then
  %6 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %6, i32 0, i32 2
  store i32 1, i32* %err, align 4, !tbaa !29
  br label %if.end19

if.else:                                          ; preds = %lor.lhs.false
  %7 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface4 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %7, i32 0, i32 1
  %8 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface4, align 4, !tbaa !16
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %8, i32 0, i32 2
  %9 = load i32, i32* %caps, align 4, !tbaa !15
  %and = and i32 %9, 2
  %tobool5 = icmp ne i32 %and, 0
  br i1 %tobool5, label %if.else8, label %if.then6

if.then6:                                         ; preds = %if.else
  %10 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err7 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %10, i32 0, i32 2
  store i32 4, i32* %err7, align 4, !tbaa !29
  br label %if.end18

if.else8:                                         ; preds = %if.else
  %11 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface9 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %11, i32 0, i32 1
  %12 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface9, align 4, !tbaa !16
  %enc = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %12, i32 0, i32 7
  %get_preview = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc, i32 0, i32 6
  %13 = load %struct.aom_image* (%struct.aom_codec_alg_priv*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*)** %get_preview, align 4, !tbaa !52
  %tobool10 = icmp ne %struct.aom_image* (%struct.aom_codec_alg_priv*)* %13, null
  br i1 %tobool10, label %if.else13, label %if.then11

if.then11:                                        ; preds = %if.else8
  %14 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %14, i32 0, i32 2
  store i32 4, i32* %err12, align 4, !tbaa !29
  br label %if.end

if.else13:                                        ; preds = %if.else8
  %15 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface14 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %15, i32 0, i32 1
  %16 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface14, align 4, !tbaa !16
  %enc15 = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %16, i32 0, i32 7
  %get_preview16 = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc15, i32 0, i32 6
  %17 = load %struct.aom_image* (%struct.aom_codec_alg_priv*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*)** %get_preview16, align 4, !tbaa !52
  %18 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %18)
  %call17 = call %struct.aom_image* %17(%struct.aom_codec_alg_priv* %call)
  store %struct.aom_image* %call17, %struct.aom_image** %img, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else13, %if.then11
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then6
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.then3
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %entry
  %19 = load %struct.aom_image*, %struct.aom_image** %img, align 4, !tbaa !2
  %20 = bitcast %struct.aom_image** %img to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  ret %struct.aom_image* %19
}

; Function Attrs: nounwind
define hidden %struct.aom_fixed_buf* @aom_codec_get_global_headers(%struct.aom_codec_ctx* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %buf = alloca %struct.aom_fixed_buf*, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_fixed_buf** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store %struct.aom_fixed_buf* null, %struct.aom_fixed_buf** %buf, align 4, !tbaa !2
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %if.then, label %if.end20

if.then:                                          ; preds = %entry
  %2 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %2, i32 0, i32 1
  %3 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !16
  %tobool1 = icmp ne %struct.aom_codec_iface* %3, null
  br i1 %tobool1, label %lor.lhs.false, label %if.then3

lor.lhs.false:                                    ; preds = %if.then
  %4 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %4, i32 0, i32 6
  %5 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool2 = icmp ne %struct.aom_codec_priv* %5, null
  br i1 %tobool2, label %if.else, label %if.then3

if.then3:                                         ; preds = %lor.lhs.false, %if.then
  %6 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %6, i32 0, i32 2
  store i32 1, i32* %err, align 4, !tbaa !29
  br label %if.end19

if.else:                                          ; preds = %lor.lhs.false
  %7 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface4 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %7, i32 0, i32 1
  %8 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface4, align 4, !tbaa !16
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %8, i32 0, i32 2
  %9 = load i32, i32* %caps, align 4, !tbaa !15
  %and = and i32 %9, 2
  %tobool5 = icmp ne i32 %and, 0
  br i1 %tobool5, label %if.else8, label %if.then6

if.then6:                                         ; preds = %if.else
  %10 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err7 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %10, i32 0, i32 2
  store i32 4, i32* %err7, align 4, !tbaa !29
  br label %if.end18

if.else8:                                         ; preds = %if.else
  %11 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface9 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %11, i32 0, i32 1
  %12 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface9, align 4, !tbaa !16
  %enc = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %12, i32 0, i32 7
  %get_glob_hdrs = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc, i32 0, i32 5
  %13 = load %struct.aom_fixed_buf* (%struct.aom_codec_alg_priv*)*, %struct.aom_fixed_buf* (%struct.aom_codec_alg_priv*)** %get_glob_hdrs, align 4, !tbaa !53
  %tobool10 = icmp ne %struct.aom_fixed_buf* (%struct.aom_codec_alg_priv*)* %13, null
  br i1 %tobool10, label %if.else13, label %if.then11

if.then11:                                        ; preds = %if.else8
  %14 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %14, i32 0, i32 2
  store i32 4, i32* %err12, align 4, !tbaa !29
  br label %if.end

if.else13:                                        ; preds = %if.else8
  %15 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface14 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %15, i32 0, i32 1
  %16 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface14, align 4, !tbaa !16
  %enc15 = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %16, i32 0, i32 7
  %get_glob_hdrs16 = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc15, i32 0, i32 5
  %17 = load %struct.aom_fixed_buf* (%struct.aom_codec_alg_priv*)*, %struct.aom_fixed_buf* (%struct.aom_codec_alg_priv*)** %get_glob_hdrs16, align 4, !tbaa !53
  %18 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %18)
  %call17 = call %struct.aom_fixed_buf* %17(%struct.aom_codec_alg_priv* %call)
  store %struct.aom_fixed_buf* %call17, %struct.aom_fixed_buf** %buf, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else13, %if.then11
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then6
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.then3
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %entry
  %19 = load %struct.aom_fixed_buf*, %struct.aom_fixed_buf** %buf, align 4, !tbaa !2
  %20 = bitcast %struct.aom_fixed_buf** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  ret %struct.aom_fixed_buf* %19
}

; Function Attrs: nounwind
define hidden i32 @aom_codec_enc_config_set(%struct.aom_codec_ctx* %ctx, %struct.aom_codec_enc_cfg* %cfg) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %cfg.addr = alloca %struct.aom_codec_enc_cfg*, align 4
  %res = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store %struct.aom_codec_enc_cfg* %cfg, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %2, i32 0, i32 1
  %3 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !16
  %tobool1 = icmp ne %struct.aom_codec_iface* %3, null
  br i1 %tobool1, label %lor.lhs.false2, label %if.then

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %4 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %4, i32 0, i32 6
  %5 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool3 = icmp ne %struct.aom_codec_priv* %5, null
  br i1 %tobool3, label %lor.lhs.false4, label %if.then

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %6 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %tobool5 = icmp ne %struct.aom_codec_enc_cfg* %6, null
  br i1 %tobool5, label %if.else, label %if.then

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 8, i32* %res, align 4, !tbaa !10
  br label %if.end12

if.else:                                          ; preds = %lor.lhs.false4
  %7 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface6 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %7, i32 0, i32 1
  %8 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface6, align 4, !tbaa !16
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %8, i32 0, i32 2
  %9 = load i32, i32* %caps, align 4, !tbaa !15
  %and = and i32 %9, 2
  %tobool7 = icmp ne i32 %and, 0
  br i1 %tobool7, label %if.else9, label %if.then8

if.then8:                                         ; preds = %if.else
  store i32 4, i32* %res, align 4, !tbaa !10
  br label %if.end

if.else9:                                         ; preds = %if.else
  %10 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface10 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %10, i32 0, i32 1
  %11 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface10, align 4, !tbaa !16
  %enc = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %11, i32 0, i32 7
  %cfg_set = getelementptr inbounds %struct.aom_codec_enc_iface, %struct.aom_codec_enc_iface* %enc, i32 0, i32 4
  %12 = load i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_enc_cfg*)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_enc_cfg*)** %cfg_set, align 4, !tbaa !54
  %13 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %13)
  %14 = load %struct.aom_codec_enc_cfg*, %struct.aom_codec_enc_cfg** %cfg.addr, align 4, !tbaa !2
  %call11 = call i32 %12(%struct.aom_codec_alg_priv* %call, %struct.aom_codec_enc_cfg* %14)
  store i32 %call11, i32* %res, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.else9, %if.then8
  br label %if.end12

if.end12:                                         ; preds = %if.end, %if.then
  %15 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool13 = icmp ne %struct.aom_codec_ctx* %15, null
  br i1 %tobool13, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end12
  %16 = load i32, i32* %res, align 4, !tbaa !10
  %17 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %17, i32 0, i32 2
  store i32 %16, i32* %err, align 4, !tbaa !29
  br label %cond.end

cond.false:                                       ; preds = %if.end12
  %18 = load i32, i32* %res, align 4, !tbaa !10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %16, %cond.true ], [ %18, %cond.false ]
  %19 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  ret i32 %cond
}

; Function Attrs: nounwind
define hidden i32 @aom_codec_pkt_list_add(%struct.aom_codec_pkt_list* %list, %struct.aom_codec_cx_pkt* %pkt) #0 {
entry:
  %retval = alloca i32, align 4
  %list.addr = alloca %struct.aom_codec_pkt_list*, align 4
  %pkt.addr = alloca %struct.aom_codec_cx_pkt*, align 4
  store %struct.aom_codec_pkt_list* %list, %struct.aom_codec_pkt_list** %list.addr, align 4, !tbaa !2
  store %struct.aom_codec_cx_pkt* %pkt, %struct.aom_codec_cx_pkt** %pkt.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_pkt_list*, %struct.aom_codec_pkt_list** %list.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.aom_codec_pkt_list, %struct.aom_codec_pkt_list* %0, i32 0, i32 0
  %1 = load i32, i32* %cnt, align 8, !tbaa !55
  %2 = load %struct.aom_codec_pkt_list*, %struct.aom_codec_pkt_list** %list.addr, align 4, !tbaa !2
  %max = getelementptr inbounds %struct.aom_codec_pkt_list, %struct.aom_codec_pkt_list* %2, i32 0, i32 1
  %3 = load i32, i32* %max, align 4, !tbaa !57
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.aom_codec_pkt_list*, %struct.aom_codec_pkt_list** %list.addr, align 4, !tbaa !2
  %pkts = getelementptr inbounds %struct.aom_codec_pkt_list, %struct.aom_codec_pkt_list* %4, i32 0, i32 2
  %5 = load %struct.aom_codec_pkt_list*, %struct.aom_codec_pkt_list** %list.addr, align 4, !tbaa !2
  %cnt1 = getelementptr inbounds %struct.aom_codec_pkt_list, %struct.aom_codec_pkt_list* %5, i32 0, i32 0
  %6 = load i32, i32* %cnt1, align 8, !tbaa !55
  %inc = add i32 %6, 1
  store i32 %inc, i32* %cnt1, align 8, !tbaa !55
  %arrayidx = getelementptr inbounds [1 x %struct.aom_codec_cx_pkt], [1 x %struct.aom_codec_cx_pkt]* %pkts, i32 0, i32 %6
  %7 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt.addr, align 4, !tbaa !2
  %8 = bitcast %struct.aom_codec_cx_pkt* %arrayidx to i8*
  %9 = bitcast %struct.aom_codec_cx_pkt* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %8, i8* align 8 %9, i32 136, i1 false), !tbaa.struct !50
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define hidden %struct.aom_codec_cx_pkt* @aom_codec_pkt_list_get(%struct.aom_codec_pkt_list* %list, i8** %iter) #0 {
entry:
  %list.addr = alloca %struct.aom_codec_pkt_list*, align 4
  %iter.addr = alloca i8**, align 4
  %pkt = alloca %struct.aom_codec_cx_pkt*, align 4
  store %struct.aom_codec_pkt_list* %list, %struct.aom_codec_pkt_list** %list.addr, align 4, !tbaa !2
  store i8** %iter, i8*** %iter.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_codec_cx_pkt** %pkt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  %2 = load i8*, i8** %1, align 4, !tbaa !2
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %3 = load %struct.aom_codec_pkt_list*, %struct.aom_codec_pkt_list** %list.addr, align 4, !tbaa !2
  %pkts = getelementptr inbounds %struct.aom_codec_pkt_list, %struct.aom_codec_pkt_list* %3, i32 0, i32 2
  %arraydecay = getelementptr inbounds [1 x %struct.aom_codec_cx_pkt], [1 x %struct.aom_codec_cx_pkt]* %pkts, i32 0, i32 0
  %4 = bitcast %struct.aom_codec_cx_pkt* %arraydecay to i8*
  %5 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  store i8* %4, i8** %5, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  %7 = load i8*, i8** %6, align 4, !tbaa !2
  %8 = bitcast i8* %7 to %struct.aom_codec_cx_pkt*
  store %struct.aom_codec_cx_pkt* %8, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %9 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %10 = load %struct.aom_codec_pkt_list*, %struct.aom_codec_pkt_list** %list.addr, align 4, !tbaa !2
  %pkts1 = getelementptr inbounds %struct.aom_codec_pkt_list, %struct.aom_codec_pkt_list* %10, i32 0, i32 2
  %arraydecay2 = getelementptr inbounds [1 x %struct.aom_codec_cx_pkt], [1 x %struct.aom_codec_cx_pkt]* %pkts1, i32 0, i32 0
  %sub.ptr.lhs.cast = ptrtoint %struct.aom_codec_cx_pkt* %9 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.aom_codec_cx_pkt* %arraydecay2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 136
  %11 = load %struct.aom_codec_pkt_list*, %struct.aom_codec_pkt_list** %list.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.aom_codec_pkt_list, %struct.aom_codec_pkt_list* %11, i32 0, i32 0
  %12 = load i32, i32* %cnt, align 8, !tbaa !55
  %cmp = icmp ult i32 %sub.ptr.div, %12
  br i1 %cmp, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %13 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %13, i32 1
  %14 = bitcast %struct.aom_codec_cx_pkt* %add.ptr to i8*
  %15 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  store i8* %14, i8** %15, align 4, !tbaa !2
  br label %if.end4

if.else:                                          ; preds = %if.end
  store %struct.aom_codec_cx_pkt* null, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  br label %if.end4

if.end4:                                          ; preds = %if.else, %if.then3
  %16 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %17 = bitcast %struct.aom_codec_cx_pkt** %pkt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  ret %struct.aom_codec_cx_pkt* %16
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!4, !4, i64 0}
!11 = !{!12, !9, i64 4}
!12 = !{!"aom_codec_iface", !3, i64 0, !9, i64 4, !7, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !13, i64 24, !14, i64 44}
!13 = !{!"aom_codec_dec_iface", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!14 = !{!"aom_codec_enc_iface", !9, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!15 = !{!12, !7, i64 8}
!16 = !{!17, !3, i64 4}
!17 = !{!"aom_codec_ctx", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 12, !7, i64 16, !4, i64 20, !3, i64 24}
!18 = !{!12, !3, i64 0}
!19 = !{!17, !3, i64 0}
!20 = !{!17, !3, i64 24}
!21 = !{!17, !7, i64 16}
!22 = !{!12, !3, i64 12}
!23 = !{!24, !3, i64 0}
!24 = !{!"aom_codec_priv", !3, i64 0, !7, i64 4, !25, i64 8}
!25 = !{!"", !26, i64 0, !9, i64 8, !9, i64 12, !27, i64 16}
!26 = !{!"aom_fixed_buf", !3, i64 0, !7, i64 4}
!27 = !{!"aom_codec_cx_pkt", !4, i64 0, !4, i64 8}
!28 = !{!17, !3, i64 12}
!29 = !{!17, !4, i64 8}
!30 = !{!12, !9, i64 44}
!31 = !{!12, !3, i64 48}
!32 = !{!33, !9, i64 0}
!33 = !{!"aom_codec_enc_cfg", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28, !4, i64 32, !9, i64 36, !34, i64 40, !9, i64 48, !4, i64 52, !9, i64 56, !9, i64 60, !9, i64 64, !9, i64 68, !9, i64 72, !9, i64 76, !9, i64 80, !9, i64 84, !9, i64 88, !9, i64 92, !4, i64 96, !26, i64 100, !26, i64 108, !9, i64 116, !9, i64 120, !9, i64 124, !9, i64 128, !9, i64 132, !9, i64 136, !9, i64 140, !9, i64 144, !9, i64 148, !9, i64 152, !9, i64 156, !9, i64 160, !4, i64 164, !9, i64 168, !9, i64 172, !9, i64 176, !9, i64 180, !9, i64 184, !9, i64 188, !9, i64 192, !9, i64 196, !9, i64 200, !9, i64 204, !4, i64 208, !4, i64 464, !9, i64 720, !4, i64 724, !35, i64 744}
!34 = !{!"aom_rational", !9, i64 0, !9, i64 4}
!35 = !{!"cfg_options", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28, !9, i64 32, !9, i64 36, !9, i64 40, !9, i64 44, !9, i64 48, !9, i64 52, !9, i64 56, !9, i64 60, !9, i64 64, !9, i64 68, !9, i64 72, !9, i64 76, !9, i64 80, !9, i64 84, !9, i64 88, !9, i64 92, !9, i64 96, !9, i64 100, !9, i64 104, !9, i64 108, !9, i64 112, !9, i64 116, !9, i64 120, !9, i64 124, !9, i64 128, !9, i64 132, !9, i64 136}
!36 = !{i64 0, i64 4, !8, i64 4, i64 4, !8, i64 8, i64 4, !8, i64 12, i64 4, !8, i64 16, i64 4, !8, i64 20, i64 4, !8, i64 24, i64 4, !8, i64 28, i64 4, !8, i64 32, i64 4, !10, i64 36, i64 4, !8, i64 40, i64 4, !8, i64 44, i64 4, !8, i64 48, i64 4, !8, i64 52, i64 4, !10, i64 56, i64 4, !8, i64 60, i64 4, !8, i64 64, i64 4, !8, i64 68, i64 4, !8, i64 72, i64 4, !8, i64 76, i64 4, !8, i64 80, i64 4, !8, i64 84, i64 4, !8, i64 88, i64 4, !8, i64 92, i64 4, !8, i64 96, i64 4, !10, i64 100, i64 4, !2, i64 104, i64 4, !6, i64 108, i64 4, !2, i64 112, i64 4, !6, i64 116, i64 4, !8, i64 120, i64 4, !8, i64 124, i64 4, !8, i64 128, i64 4, !8, i64 132, i64 4, !8, i64 136, i64 4, !8, i64 140, i64 4, !8, i64 144, i64 4, !8, i64 148, i64 4, !8, i64 152, i64 4, !8, i64 156, i64 4, !8, i64 160, i64 4, !8, i64 164, i64 4, !10, i64 168, i64 4, !8, i64 172, i64 4, !8, i64 176, i64 4, !8, i64 180, i64 4, !8, i64 184, i64 4, !8, i64 188, i64 4, !8, i64 192, i64 4, !8, i64 196, i64 4, !8, i64 200, i64 4, !8, i64 204, i64 4, !8, i64 208, i64 256, !10, i64 464, i64 256, !10, i64 720, i64 4, !8, i64 724, i64 20, !10, i64 744, i64 4, !8, i64 748, i64 4, !8, i64 752, i64 4, !8, i64 756, i64 4, !8, i64 760, i64 4, !8, i64 764, i64 4, !8, i64 768, i64 4, !8, i64 772, i64 4, !8, i64 776, i64 4, !8, i64 780, i64 4, !8, i64 784, i64 4, !8, i64 788, i64 4, !8, i64 792, i64 4, !8, i64 796, i64 4, !8, i64 800, i64 4, !8, i64 804, i64 4, !8, i64 808, i64 4, !8, i64 812, i64 4, !8, i64 816, i64 4, !8, i64 820, i64 4, !8, i64 824, i64 4, !8, i64 828, i64 4, !8, i64 832, i64 4, !8, i64 836, i64 4, !8, i64 840, i64 4, !8, i64 844, i64 4, !8, i64 848, i64 4, !8, i64 852, i64 4, !8, i64 856, i64 4, !8, i64 860, i64 4, !8, i64 864, i64 4, !8, i64 868, i64 4, !8, i64 872, i64 4, !8, i64 876, i64 4, !8, i64 880, i64 4, !8}
!37 = !{!33, !9, i64 748}
!38 = !{!33, !9, i64 752}
!39 = !{!33, !9, i64 756}
!40 = !{!33, !9, i64 868}
!41 = !{!42, !42, i64 0}
!42 = !{!"long long", !4, i64 0}
!43 = !{!12, !3, i64 52}
!44 = !{!12, !3, i64 56}
!45 = !{!27, !4, i64 0}
!46 = !{!24, !3, i64 8}
!47 = !{!24, !9, i64 16}
!48 = !{!24, !9, i64 20}
!49 = !{!24, !7, i64 12}
!50 = !{i64 0, i64 4, !10, i64 8, i64 4, !2, i64 12, i64 4, !6, i64 16, i64 8, !41, i64 24, i64 4, !6, i64 28, i64 4, !8, i64 32, i64 4, !8, i64 36, i64 4, !6, i64 8, i64 4, !2, i64 12, i64 4, !6, i64 8, i64 4, !2, i64 12, i64 4, !6, i64 8, i64 16, !10, i64 24, i64 32, !10, i64 56, i64 32, !10, i64 8, i64 4, !2, i64 12, i64 4, !6, i64 8, i64 124, !10}
!51 = !{i64 0, i64 4, !2, i64 4, i64 4, !6}
!52 = !{!12, !3, i64 68}
!53 = !{!12, !3, i64 64}
!54 = !{!12, !3, i64 60}
!55 = !{!56, !9, i64 0}
!56 = !{!"aom_codec_pkt_list", !9, i64 0, !9, i64 4, !4, i64 8}
!57 = !{!56, !9, i64 4}
