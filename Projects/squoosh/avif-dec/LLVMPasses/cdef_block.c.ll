; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/cdef_block.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/cdef_block.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.cdef_list = type { i8, i8 }

@cdef_directions = hidden constant [8 x [2 x i32]] [[2 x i32] [i32 -143, i32 -286], [2 x i32] [i32 1, i32 -142], [2 x i32] [i32 1, i32 2], [2 x i32] [i32 1, i32 146], [2 x i32] [i32 145, i32 290], [2 x i32] [i32 144, i32 289], [2 x i32] [i32 144, i32 288], [2 x i32] [i32 144, i32 287]], align 16
@cdef_find_dir_c.div_table = internal constant [9 x i32] [i32 0, i32 840, i32 420, i32 280, i32 210, i32 168, i32 140, i32 120, i32 105], align 16
@cdef_pri_taps = hidden constant [2 x [2 x i32]] [[2 x i32] [i32 4, i32 2], [2 x i32] [i32 3, i32 3]], align 16
@cdef_sec_taps = hidden constant [2 x i32] [i32 2, i32 1], align 4
@av1_cdef_filter_fb.conv422 = internal constant [8 x i32] [i32 7, i32 0, i32 2, i32 4, i32 5, i32 6, i32 6, i32 6], align 16
@av1_cdef_filter_fb.conv440 = internal constant [8 x i32] [i32 1, i32 2, i32 2, i32 2, i32 3, i32 4, i32 6, i32 0], align 16

; Function Attrs: nounwind
define hidden i32 @cdef_find_dir_c(i16* %img, i32 %stride, i32* %var, i32 %coeff_shift) #0 {
entry:
  %img.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %var.addr = alloca i32*, align 4
  %coeff_shift.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cost = alloca [8 x i32], align 16
  %partial = alloca [8 x [15 x i32]], align 16
  %best_cost = alloca i32, align 4
  %best_dir = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca i32, align 4
  %j131 = alloca i32, align 4
  store i16* %img, i16** %img.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32* %var, i32** %var.addr, align 4, !tbaa !2
  store i32 %coeff_shift, i32* %coeff_shift.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast [8 x i32]* %cost to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %1) #6
  %2 = bitcast [8 x i32]* %cost to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %2, i8 0, i32 32, i1 false)
  %3 = bitcast [8 x [15 x i32]]* %partial to i8*
  call void @llvm.lifetime.start.p0i8(i64 480, i8* %3) #6
  %4 = bitcast [8 x [15 x i32]]* %partial to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %4, i8 0, i32 480, i1 false)
  %5 = bitcast i32* %best_cost to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store i32 0, i32* %best_cost, align 4, !tbaa !6
  %6 = bitcast i32* %best_dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 0, i32* %best_dir, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc40, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %7, 8
  br i1 %cmp, label %for.body, label %for.end42

for.body:                                         ; preds = %for.cond
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %j, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %9, 8
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %10 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load i16*, i16** %img.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %13 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %12, %13
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul, %14
  %arrayidx = getelementptr inbounds i16, i16* %11, i32 %add
  %15 = load i16, i16* %arrayidx, align 2, !tbaa !8
  %conv = zext i16 %15 to i32
  %16 = load i32, i32* %coeff_shift.addr, align 4, !tbaa !6
  %shr = ashr i32 %conv, %16
  %sub = sub nsw i32 %shr, 128
  store i32 %sub, i32* %x, align 4, !tbaa !6
  %17 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 0
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %add5 = add nsw i32 %18, %19
  %arrayidx6 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx4, i32 0, i32 %add5
  %20 = load i32, i32* %arrayidx6, align 4, !tbaa !6
  %add7 = add nsw i32 %20, %17
  store i32 %add7, i32* %arrayidx6, align 4, !tbaa !6
  %21 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 1
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %div = sdiv i32 %23, 2
  %add9 = add nsw i32 %22, %div
  %arrayidx10 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx8, i32 0, i32 %add9
  %24 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %add11 = add nsw i32 %24, %21
  store i32 %add11, i32* %arrayidx10, align 4, !tbaa !6
  %25 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx12, i32 0, i32 %26
  %27 = load i32, i32* %arrayidx13, align 4, !tbaa !6
  %add14 = add nsw i32 %27, %25
  store i32 %add14, i32* %arrayidx13, align 4, !tbaa !6
  %28 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 3
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %add16 = add nsw i32 3, %29
  %30 = load i32, i32* %j, align 4, !tbaa !6
  %div17 = sdiv i32 %30, 2
  %sub18 = sub nsw i32 %add16, %div17
  %arrayidx19 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx15, i32 0, i32 %sub18
  %31 = load i32, i32* %arrayidx19, align 4, !tbaa !6
  %add20 = add nsw i32 %31, %28
  store i32 %add20, i32* %arrayidx19, align 4, !tbaa !6
  %32 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 4
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %add22 = add nsw i32 7, %33
  %34 = load i32, i32* %j, align 4, !tbaa !6
  %sub23 = sub nsw i32 %add22, %34
  %arrayidx24 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx21, i32 0, i32 %sub23
  %35 = load i32, i32* %arrayidx24, align 4, !tbaa !6
  %add25 = add nsw i32 %35, %32
  store i32 %add25, i32* %arrayidx24, align 4, !tbaa !6
  %36 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 5
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %div27 = sdiv i32 %37, 2
  %sub28 = sub nsw i32 3, %div27
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %add29 = add nsw i32 %sub28, %38
  %arrayidx30 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx26, i32 0, i32 %add29
  %39 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %add31 = add nsw i32 %39, %36
  store i32 %add31, i32* %arrayidx30, align 4, !tbaa !6
  %40 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 6
  %41 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx32, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx33, align 4, !tbaa !6
  %add34 = add nsw i32 %42, %40
  store i32 %add34, i32* %arrayidx33, align 4, !tbaa !6
  %43 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 7
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %div36 = sdiv i32 %44, 2
  %45 = load i32, i32* %j, align 4, !tbaa !6
  %add37 = add nsw i32 %div36, %45
  %arrayidx38 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx35, i32 0, i32 %add37
  %46 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %add39 = add nsw i32 %46, %43
  store i32 %add39, i32* %arrayidx38, align 4, !tbaa !6
  %47 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %48 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %49 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  br label %for.inc40

for.inc40:                                        ; preds = %for.end
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %inc41 = add nsw i32 %50, 1
  store i32 %inc41, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end42:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc61, %for.end42
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %cmp44 = icmp slt i32 %51, 8
  br i1 %cmp44, label %for.body46, label %for.end63

for.body46:                                       ; preds = %for.cond43
  %arrayidx47 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 2
  %52 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx47, i32 0, i32 %52
  %53 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %arrayidx49 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 2
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx50 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx49, i32 0, i32 %54
  %55 = load i32, i32* %arrayidx50, align 4, !tbaa !6
  %mul51 = mul nsw i32 %53, %55
  %arrayidx52 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 2
  %56 = load i32, i32* %arrayidx52, align 8, !tbaa !6
  %add53 = add nsw i32 %56, %mul51
  store i32 %add53, i32* %arrayidx52, align 8, !tbaa !6
  %arrayidx54 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 6
  %57 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx55 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx54, i32 0, i32 %57
  %58 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 6
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx56, i32 0, i32 %59
  %60 = load i32, i32* %arrayidx57, align 4, !tbaa !6
  %mul58 = mul nsw i32 %58, %60
  %arrayidx59 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 6
  %61 = load i32, i32* %arrayidx59, align 8, !tbaa !6
  %add60 = add nsw i32 %61, %mul58
  store i32 %add60, i32* %arrayidx59, align 8, !tbaa !6
  br label %for.inc61

for.inc61:                                        ; preds = %for.body46
  %62 = load i32, i32* %i, align 4, !tbaa !6
  %inc62 = add nsw i32 %62, 1
  store i32 %inc62, i32* %i, align 4, !tbaa !6
  br label %for.cond43

for.end63:                                        ; preds = %for.cond43
  %63 = load i32, i32* getelementptr inbounds ([9 x i32], [9 x i32]* @cdef_find_dir_c.div_table, i32 0, i32 8), align 16, !tbaa !6
  %arrayidx64 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 2
  %64 = load i32, i32* %arrayidx64, align 8, !tbaa !6
  %mul65 = mul nsw i32 %64, %63
  store i32 %mul65, i32* %arrayidx64, align 8, !tbaa !6
  %65 = load i32, i32* getelementptr inbounds ([9 x i32], [9 x i32]* @cdef_find_dir_c.div_table, i32 0, i32 8), align 16, !tbaa !6
  %arrayidx66 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 6
  %66 = load i32, i32* %arrayidx66, align 8, !tbaa !6
  %mul67 = mul nsw i32 %66, %65
  store i32 %mul67, i32* %arrayidx66, align 8, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond68

for.cond68:                                       ; preds = %for.inc108, %for.end63
  %67 = load i32, i32* %i, align 4, !tbaa !6
  %cmp69 = icmp slt i32 %67, 7
  br i1 %cmp69, label %for.body71, label %for.end110

for.body71:                                       ; preds = %for.cond68
  %arrayidx72 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 0
  %68 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx73 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx72, i32 0, i32 %68
  %69 = load i32, i32* %arrayidx73, align 4, !tbaa !6
  %arrayidx74 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 0
  %70 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx74, i32 0, i32 %70
  %71 = load i32, i32* %arrayidx75, align 4, !tbaa !6
  %mul76 = mul nsw i32 %69, %71
  %arrayidx77 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 0
  %72 = load i32, i32* %i, align 4, !tbaa !6
  %sub78 = sub nsw i32 14, %72
  %arrayidx79 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx77, i32 0, i32 %sub78
  %73 = load i32, i32* %arrayidx79, align 4, !tbaa !6
  %arrayidx80 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 0
  %74 = load i32, i32* %i, align 4, !tbaa !6
  %sub81 = sub nsw i32 14, %74
  %arrayidx82 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx80, i32 0, i32 %sub81
  %75 = load i32, i32* %arrayidx82, align 4, !tbaa !6
  %mul83 = mul nsw i32 %73, %75
  %add84 = add nsw i32 %mul76, %mul83
  %76 = load i32, i32* %i, align 4, !tbaa !6
  %add85 = add nsw i32 %76, 1
  %arrayidx86 = getelementptr inbounds [9 x i32], [9 x i32]* @cdef_find_dir_c.div_table, i32 0, i32 %add85
  %77 = load i32, i32* %arrayidx86, align 4, !tbaa !6
  %mul87 = mul nsw i32 %add84, %77
  %arrayidx88 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 0
  %78 = load i32, i32* %arrayidx88, align 16, !tbaa !6
  %add89 = add nsw i32 %78, %mul87
  store i32 %add89, i32* %arrayidx88, align 16, !tbaa !6
  %arrayidx90 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 4
  %79 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx91 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx90, i32 0, i32 %79
  %80 = load i32, i32* %arrayidx91, align 4, !tbaa !6
  %arrayidx92 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 4
  %81 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx93 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx92, i32 0, i32 %81
  %82 = load i32, i32* %arrayidx93, align 4, !tbaa !6
  %mul94 = mul nsw i32 %80, %82
  %arrayidx95 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 4
  %83 = load i32, i32* %i, align 4, !tbaa !6
  %sub96 = sub nsw i32 14, %83
  %arrayidx97 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx95, i32 0, i32 %sub96
  %84 = load i32, i32* %arrayidx97, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 4
  %85 = load i32, i32* %i, align 4, !tbaa !6
  %sub99 = sub nsw i32 14, %85
  %arrayidx100 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx98, i32 0, i32 %sub99
  %86 = load i32, i32* %arrayidx100, align 4, !tbaa !6
  %mul101 = mul nsw i32 %84, %86
  %add102 = add nsw i32 %mul94, %mul101
  %87 = load i32, i32* %i, align 4, !tbaa !6
  %add103 = add nsw i32 %87, 1
  %arrayidx104 = getelementptr inbounds [9 x i32], [9 x i32]* @cdef_find_dir_c.div_table, i32 0, i32 %add103
  %88 = load i32, i32* %arrayidx104, align 4, !tbaa !6
  %mul105 = mul nsw i32 %add102, %88
  %arrayidx106 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 4
  %89 = load i32, i32* %arrayidx106, align 16, !tbaa !6
  %add107 = add nsw i32 %89, %mul105
  store i32 %add107, i32* %arrayidx106, align 16, !tbaa !6
  br label %for.inc108

for.inc108:                                       ; preds = %for.body71
  %90 = load i32, i32* %i, align 4, !tbaa !6
  %inc109 = add nsw i32 %90, 1
  store i32 %inc109, i32* %i, align 4, !tbaa !6
  br label %for.cond68

for.end110:                                       ; preds = %for.cond68
  %arrayidx111 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 0
  %arrayidx112 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx111, i32 0, i32 7
  %91 = load i32, i32* %arrayidx112, align 4, !tbaa !6
  %arrayidx113 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 0
  %arrayidx114 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx113, i32 0, i32 7
  %92 = load i32, i32* %arrayidx114, align 4, !tbaa !6
  %mul115 = mul nsw i32 %91, %92
  %93 = load i32, i32* getelementptr inbounds ([9 x i32], [9 x i32]* @cdef_find_dir_c.div_table, i32 0, i32 8), align 16, !tbaa !6
  %mul116 = mul nsw i32 %mul115, %93
  %arrayidx117 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 0
  %94 = load i32, i32* %arrayidx117, align 16, !tbaa !6
  %add118 = add nsw i32 %94, %mul116
  store i32 %add118, i32* %arrayidx117, align 16, !tbaa !6
  %arrayidx119 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 4
  %arrayidx120 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx119, i32 0, i32 7
  %95 = load i32, i32* %arrayidx120, align 4, !tbaa !6
  %arrayidx121 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 4
  %arrayidx122 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx121, i32 0, i32 7
  %96 = load i32, i32* %arrayidx122, align 4, !tbaa !6
  %mul123 = mul nsw i32 %95, %96
  %97 = load i32, i32* getelementptr inbounds ([9 x i32], [9 x i32]* @cdef_find_dir_c.div_table, i32 0, i32 8), align 16, !tbaa !6
  %mul124 = mul nsw i32 %mul123, %97
  %arrayidx125 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 4
  %98 = load i32, i32* %arrayidx125, align 16, !tbaa !6
  %add126 = add nsw i32 %98, %mul124
  store i32 %add126, i32* %arrayidx125, align 16, !tbaa !6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond127

for.cond127:                                      ; preds = %for.inc176, %for.end110
  %99 = load i32, i32* %i, align 4, !tbaa !6
  %cmp128 = icmp slt i32 %99, 8
  br i1 %cmp128, label %for.body130, label %for.end178

for.body130:                                      ; preds = %for.cond127
  %100 = bitcast i32* %j131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #6
  store i32 0, i32* %j131, align 4, !tbaa !6
  br label %for.cond132

for.cond132:                                      ; preds = %for.inc145, %for.body130
  %101 = load i32, i32* %j131, align 4, !tbaa !6
  %cmp133 = icmp slt i32 %101, 5
  br i1 %cmp133, label %for.body135, label %for.end147

for.body135:                                      ; preds = %for.cond132
  %102 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx136 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 %102
  %103 = load i32, i32* %j131, align 4, !tbaa !6
  %add137 = add nsw i32 3, %103
  %arrayidx138 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx136, i32 0, i32 %add137
  %104 = load i32, i32* %arrayidx138, align 4, !tbaa !6
  %105 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx139 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 %105
  %106 = load i32, i32* %j131, align 4, !tbaa !6
  %add140 = add nsw i32 3, %106
  %arrayidx141 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx139, i32 0, i32 %add140
  %107 = load i32, i32* %arrayidx141, align 4, !tbaa !6
  %mul142 = mul nsw i32 %104, %107
  %108 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx143 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 %108
  %109 = load i32, i32* %arrayidx143, align 4, !tbaa !6
  %add144 = add nsw i32 %109, %mul142
  store i32 %add144, i32* %arrayidx143, align 4, !tbaa !6
  br label %for.inc145

for.inc145:                                       ; preds = %for.body135
  %110 = load i32, i32* %j131, align 4, !tbaa !6
  %inc146 = add nsw i32 %110, 1
  store i32 %inc146, i32* %j131, align 4, !tbaa !6
  br label %for.cond132

for.end147:                                       ; preds = %for.cond132
  %111 = load i32, i32* getelementptr inbounds ([9 x i32], [9 x i32]* @cdef_find_dir_c.div_table, i32 0, i32 8), align 16, !tbaa !6
  %112 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx148 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 %112
  %113 = load i32, i32* %arrayidx148, align 4, !tbaa !6
  %mul149 = mul nsw i32 %113, %111
  store i32 %mul149, i32* %arrayidx148, align 4, !tbaa !6
  store i32 0, i32* %j131, align 4, !tbaa !6
  br label %for.cond150

for.cond150:                                      ; preds = %for.inc173, %for.end147
  %114 = load i32, i32* %j131, align 4, !tbaa !6
  %cmp151 = icmp slt i32 %114, 3
  br i1 %cmp151, label %for.body153, label %for.end175

for.body153:                                      ; preds = %for.cond150
  %115 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx154 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 %115
  %116 = load i32, i32* %j131, align 4, !tbaa !6
  %arrayidx155 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx154, i32 0, i32 %116
  %117 = load i32, i32* %arrayidx155, align 4, !tbaa !6
  %118 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx156 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 %118
  %119 = load i32, i32* %j131, align 4, !tbaa !6
  %arrayidx157 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx156, i32 0, i32 %119
  %120 = load i32, i32* %arrayidx157, align 4, !tbaa !6
  %mul158 = mul nsw i32 %117, %120
  %121 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx159 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 %121
  %122 = load i32, i32* %j131, align 4, !tbaa !6
  %sub160 = sub nsw i32 10, %122
  %arrayidx161 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx159, i32 0, i32 %sub160
  %123 = load i32, i32* %arrayidx161, align 4, !tbaa !6
  %124 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx162 = getelementptr inbounds [8 x [15 x i32]], [8 x [15 x i32]]* %partial, i32 0, i32 %124
  %125 = load i32, i32* %j131, align 4, !tbaa !6
  %sub163 = sub nsw i32 10, %125
  %arrayidx164 = getelementptr inbounds [15 x i32], [15 x i32]* %arrayidx162, i32 0, i32 %sub163
  %126 = load i32, i32* %arrayidx164, align 4, !tbaa !6
  %mul165 = mul nsw i32 %123, %126
  %add166 = add nsw i32 %mul158, %mul165
  %127 = load i32, i32* %j131, align 4, !tbaa !6
  %mul167 = mul nsw i32 2, %127
  %add168 = add nsw i32 %mul167, 2
  %arrayidx169 = getelementptr inbounds [9 x i32], [9 x i32]* @cdef_find_dir_c.div_table, i32 0, i32 %add168
  %128 = load i32, i32* %arrayidx169, align 4, !tbaa !6
  %mul170 = mul nsw i32 %add166, %128
  %129 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx171 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 %129
  %130 = load i32, i32* %arrayidx171, align 4, !tbaa !6
  %add172 = add nsw i32 %130, %mul170
  store i32 %add172, i32* %arrayidx171, align 4, !tbaa !6
  br label %for.inc173

for.inc173:                                       ; preds = %for.body153
  %131 = load i32, i32* %j131, align 4, !tbaa !6
  %inc174 = add nsw i32 %131, 1
  store i32 %inc174, i32* %j131, align 4, !tbaa !6
  br label %for.cond150

for.end175:                                       ; preds = %for.cond150
  %132 = bitcast i32* %j131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #6
  br label %for.inc176

for.inc176:                                       ; preds = %for.end175
  %133 = load i32, i32* %i, align 4, !tbaa !6
  %add177 = add nsw i32 %133, 2
  store i32 %add177, i32* %i, align 4, !tbaa !6
  br label %for.cond127

for.end178:                                       ; preds = %for.cond127
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond179

for.cond179:                                      ; preds = %for.inc187, %for.end178
  %134 = load i32, i32* %i, align 4, !tbaa !6
  %cmp180 = icmp slt i32 %134, 8
  br i1 %cmp180, label %for.body182, label %for.end189

for.body182:                                      ; preds = %for.cond179
  %135 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx183 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 %135
  %136 = load i32, i32* %arrayidx183, align 4, !tbaa !6
  %137 = load i32, i32* %best_cost, align 4, !tbaa !6
  %cmp184 = icmp sgt i32 %136, %137
  br i1 %cmp184, label %if.then, label %if.end

if.then:                                          ; preds = %for.body182
  %138 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx186 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 %138
  %139 = load i32, i32* %arrayidx186, align 4, !tbaa !6
  store i32 %139, i32* %best_cost, align 4, !tbaa !6
  %140 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %140, i32* %best_dir, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body182
  br label %for.inc187

for.inc187:                                       ; preds = %if.end
  %141 = load i32, i32* %i, align 4, !tbaa !6
  %inc188 = add nsw i32 %141, 1
  store i32 %inc188, i32* %i, align 4, !tbaa !6
  br label %for.cond179

for.end189:                                       ; preds = %for.cond179
  %142 = load i32, i32* %best_cost, align 4, !tbaa !6
  %143 = load i32, i32* %best_dir, align 4, !tbaa !6
  %add190 = add nsw i32 %143, 4
  %and = and i32 %add190, 7
  %arrayidx191 = getelementptr inbounds [8 x i32], [8 x i32]* %cost, i32 0, i32 %and
  %144 = load i32, i32* %arrayidx191, align 4, !tbaa !6
  %sub192 = sub nsw i32 %142, %144
  %145 = load i32*, i32** %var.addr, align 4, !tbaa !2
  store i32 %sub192, i32* %145, align 4, !tbaa !6
  %146 = load i32*, i32** %var.addr, align 4, !tbaa !2
  %147 = load i32, i32* %146, align 4, !tbaa !6
  %shr193 = ashr i32 %147, 10
  store i32 %shr193, i32* %146, align 4, !tbaa !6
  %148 = load i32, i32* %best_dir, align 4, !tbaa !6
  %149 = bitcast i32* %best_dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #6
  %150 = bitcast i32* %best_cost to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #6
  %151 = bitcast [8 x [15 x i32]]* %partial to i8*
  call void @llvm.lifetime.end.p0i8(i64 480, i8* %151) #6
  %152 = bitcast [8 x i32]* %cost to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %152) #6
  %153 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #6
  ret i32 %148
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @cdef_filter_block_c(i8* %dst8, i16* %dst16, i32 %dstride, i16* %in, i32 %pri_strength, i32 %sec_strength, i32 %dir, i32 %pri_damping, i32 %sec_damping, i32 %bsize, i32 %coeff_shift) #0 {
entry:
  %dst8.addr = alloca i8*, align 4
  %dst16.addr = alloca i16*, align 4
  %dstride.addr = alloca i32, align 4
  %in.addr = alloca i16*, align 4
  %pri_strength.addr = alloca i32, align 4
  %sec_strength.addr = alloca i32, align 4
  %dir.addr = alloca i32, align 4
  %pri_damping.addr = alloca i32, align 4
  %sec_damping.addr = alloca i32, align 4
  %bsize.addr = alloca i32, align 4
  %coeff_shift.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s = alloca i32, align 4
  %pri_taps = alloca i32*, align 4
  %sec_taps = alloca i32*, align 4
  %sum = alloca i16, align 2
  %y = alloca i16, align 2
  %x = alloca i16, align 2
  %max = alloca i32, align 4
  %min = alloca i32, align 4
  %p0 = alloca i16, align 2
  %p1 = alloca i16, align 2
  %s0 = alloca i16, align 2
  %s1 = alloca i16, align 2
  %s2 = alloca i16, align 2
  %s3 = alloca i16, align 2
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i16* %dst16, i16** %dst16.addr, align 4, !tbaa !2
  store i32 %dstride, i32* %dstride.addr, align 4, !tbaa !6
  store i16* %in, i16** %in.addr, align 4, !tbaa !2
  store i32 %pri_strength, i32* %pri_strength.addr, align 4, !tbaa !6
  store i32 %sec_strength, i32* %sec_strength.addr, align 4, !tbaa !6
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  store i32 %pri_damping, i32* %pri_damping.addr, align 4, !tbaa !6
  store i32 %sec_damping, i32* %sec_damping.addr, align 4, !tbaa !6
  store i32 %bsize, i32* %bsize.addr, align 4, !tbaa !6
  store i32 %coeff_shift, i32* %coeff_shift.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store i32 144, i32* %s, align 4, !tbaa !6
  %4 = bitcast i32** %pri_taps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32, i32* %pri_strength.addr, align 4, !tbaa !6
  %6 = load i32, i32* %coeff_shift.addr, align 4, !tbaa !6
  %shr = ashr i32 %5, %6
  %and = and i32 %shr, 1
  %arrayidx = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* @cdef_pri_taps, i32 0, i32 %and
  %arraydecay = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 0
  store i32* %arraydecay, i32** %pri_taps, align 4, !tbaa !2
  %7 = bitcast i32** %sec_taps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store i32* getelementptr inbounds ([2 x i32], [2 x i32]* @cdef_sec_taps, i32 0, i32 0), i32** %sec_taps, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc257, %entry
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %9 = load i32, i32* %bsize.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %9, 3
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %for.cond
  %10 = load i32, i32* %bsize.addr, align 4, !tbaa !6
  %cmp1 = icmp eq i32 %10, 1
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %for.cond
  %11 = phi i1 [ true, %for.cond ], [ %cmp1, %lor.rhs ]
  %lor.ext = zext i1 %11 to i32
  %shl = shl i32 4, %lor.ext
  %cmp2 = icmp slt i32 %8, %shl
  br i1 %cmp2, label %for.body, label %for.end259

for.body:                                         ; preds = %lor.end
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc254, %for.body
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %13 = load i32, i32* %bsize.addr, align 4, !tbaa !6
  %cmp4 = icmp eq i32 %13, 3
  br i1 %cmp4, label %lor.end7, label %lor.rhs5

lor.rhs5:                                         ; preds = %for.cond3
  %14 = load i32, i32* %bsize.addr, align 4, !tbaa !6
  %cmp6 = icmp eq i32 %14, 2
  br label %lor.end7

lor.end7:                                         ; preds = %lor.rhs5, %for.cond3
  %15 = phi i1 [ true, %for.cond3 ], [ %cmp6, %lor.rhs5 ]
  %lor.ext8 = zext i1 %15 to i32
  %shl9 = shl i32 4, %lor.ext8
  %cmp10 = icmp slt i32 %12, %shl9
  br i1 %cmp10, label %for.body11, label %for.end256

for.body11:                                       ; preds = %lor.end7
  %16 = bitcast i16* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #6
  store i16 0, i16* %sum, align 2, !tbaa !8
  %17 = bitcast i16* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %17) #6
  %18 = bitcast i16* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %18) #6
  %19 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul nsw i32 %20, 144
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul, %21
  %arrayidx12 = getelementptr inbounds i16, i16* %19, i32 %add
  %22 = load i16, i16* %arrayidx12, align 2, !tbaa !8
  store i16 %22, i16* %x, align 2, !tbaa !8
  %23 = bitcast i32* %max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = load i16, i16* %x, align 2, !tbaa !8
  %conv = sext i16 %24 to i32
  store i32 %conv, i32* %max, align 4, !tbaa !6
  %25 = bitcast i32* %min to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load i16, i16* %x, align 2, !tbaa !8
  %conv13 = sext i16 %26 to i32
  store i32 %conv13, i32* %min, align 4, !tbaa !6
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %for.body11
  %27 = load i32, i32* %k, align 4, !tbaa !6
  %cmp15 = icmp slt i32 %27, 2
  br i1 %cmp15, label %for.body17, label %for.end

for.body17:                                       ; preds = %for.cond14
  %28 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %28) #6
  %29 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %mul18 = mul nsw i32 %30, 144
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %add19 = add nsw i32 %mul18, %31
  %32 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds [8 x [2 x i32]], [8 x [2 x i32]]* @cdef_directions, i32 0, i32 %32
  %33 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx20, i32 0, i32 %33
  %34 = load i32, i32* %arrayidx21, align 4, !tbaa !6
  %add22 = add nsw i32 %add19, %34
  %arrayidx23 = getelementptr inbounds i16, i16* %29, i32 %add22
  %35 = load i16, i16* %arrayidx23, align 2, !tbaa !8
  store i16 %35, i16* %p0, align 2, !tbaa !8
  %36 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %36) #6
  %37 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %mul24 = mul nsw i32 %38, 144
  %39 = load i32, i32* %j, align 4, !tbaa !6
  %add25 = add nsw i32 %mul24, %39
  %40 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds [8 x [2 x i32]], [8 x [2 x i32]]* @cdef_directions, i32 0, i32 %40
  %41 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx26, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx27, align 4, !tbaa !6
  %sub = sub nsw i32 %add25, %42
  %arrayidx28 = getelementptr inbounds i16, i16* %37, i32 %sub
  %43 = load i16, i16* %arrayidx28, align 2, !tbaa !8
  store i16 %43, i16* %p1, align 2, !tbaa !8
  %44 = load i32*, i32** %pri_taps, align 4, !tbaa !2
  %45 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds i32, i32* %44, i32 %45
  %46 = load i32, i32* %arrayidx29, align 4, !tbaa !6
  %47 = load i16, i16* %p0, align 2, !tbaa !8
  %conv30 = sext i16 %47 to i32
  %48 = load i16, i16* %x, align 2, !tbaa !8
  %conv31 = sext i16 %48 to i32
  %sub32 = sub nsw i32 %conv30, %conv31
  %49 = load i32, i32* %pri_strength.addr, align 4, !tbaa !6
  %50 = load i32, i32* %pri_damping.addr, align 4, !tbaa !6
  %call = call i32 @constrain(i32 %sub32, i32 %49, i32 %50)
  %mul33 = mul nsw i32 %46, %call
  %51 = load i16, i16* %sum, align 2, !tbaa !8
  %conv34 = sext i16 %51 to i32
  %add35 = add nsw i32 %conv34, %mul33
  %conv36 = trunc i32 %add35 to i16
  store i16 %conv36, i16* %sum, align 2, !tbaa !8
  %52 = load i32*, i32** %pri_taps, align 4, !tbaa !2
  %53 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds i32, i32* %52, i32 %53
  %54 = load i32, i32* %arrayidx37, align 4, !tbaa !6
  %55 = load i16, i16* %p1, align 2, !tbaa !8
  %conv38 = sext i16 %55 to i32
  %56 = load i16, i16* %x, align 2, !tbaa !8
  %conv39 = sext i16 %56 to i32
  %sub40 = sub nsw i32 %conv38, %conv39
  %57 = load i32, i32* %pri_strength.addr, align 4, !tbaa !6
  %58 = load i32, i32* %pri_damping.addr, align 4, !tbaa !6
  %call41 = call i32 @constrain(i32 %sub40, i32 %57, i32 %58)
  %mul42 = mul nsw i32 %54, %call41
  %59 = load i16, i16* %sum, align 2, !tbaa !8
  %conv43 = sext i16 %59 to i32
  %add44 = add nsw i32 %conv43, %mul42
  %conv45 = trunc i32 %add44 to i16
  store i16 %conv45, i16* %sum, align 2, !tbaa !8
  %60 = load i16, i16* %p0, align 2, !tbaa !8
  %conv46 = sext i16 %60 to i32
  %cmp47 = icmp ne i32 %conv46, 30000
  br i1 %cmp47, label %if.then, label %if.end

if.then:                                          ; preds = %for.body17
  %61 = load i16, i16* %p0, align 2, !tbaa !8
  %conv49 = sext i16 %61 to i32
  %62 = load i32, i32* %max, align 4, !tbaa !6
  %cmp50 = icmp sgt i32 %conv49, %62
  br i1 %cmp50, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %63 = load i16, i16* %p0, align 2, !tbaa !8
  %conv52 = sext i16 %63 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %64 = load i32, i32* %max, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv52, %cond.true ], [ %64, %cond.false ]
  store i32 %cond, i32* %max, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %cond.end, %for.body17
  %65 = load i16, i16* %p1, align 2, !tbaa !8
  %conv53 = sext i16 %65 to i32
  %cmp54 = icmp ne i32 %conv53, 30000
  br i1 %cmp54, label %if.then56, label %if.end65

if.then56:                                        ; preds = %if.end
  %66 = load i16, i16* %p1, align 2, !tbaa !8
  %conv57 = sext i16 %66 to i32
  %67 = load i32, i32* %max, align 4, !tbaa !6
  %cmp58 = icmp sgt i32 %conv57, %67
  br i1 %cmp58, label %cond.true60, label %cond.false62

cond.true60:                                      ; preds = %if.then56
  %68 = load i16, i16* %p1, align 2, !tbaa !8
  %conv61 = sext i16 %68 to i32
  br label %cond.end63

cond.false62:                                     ; preds = %if.then56
  %69 = load i32, i32* %max, align 4, !tbaa !6
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false62, %cond.true60
  %cond64 = phi i32 [ %conv61, %cond.true60 ], [ %69, %cond.false62 ]
  store i32 %cond64, i32* %max, align 4, !tbaa !6
  br label %if.end65

if.end65:                                         ; preds = %cond.end63, %if.end
  %70 = load i16, i16* %p0, align 2, !tbaa !8
  %conv66 = sext i16 %70 to i32
  %71 = load i32, i32* %min, align 4, !tbaa !6
  %cmp67 = icmp slt i32 %conv66, %71
  br i1 %cmp67, label %cond.true69, label %cond.false71

cond.true69:                                      ; preds = %if.end65
  %72 = load i16, i16* %p0, align 2, !tbaa !8
  %conv70 = sext i16 %72 to i32
  br label %cond.end72

cond.false71:                                     ; preds = %if.end65
  %73 = load i32, i32* %min, align 4, !tbaa !6
  br label %cond.end72

cond.end72:                                       ; preds = %cond.false71, %cond.true69
  %cond73 = phi i32 [ %conv70, %cond.true69 ], [ %73, %cond.false71 ]
  store i32 %cond73, i32* %min, align 4, !tbaa !6
  %74 = load i16, i16* %p1, align 2, !tbaa !8
  %conv74 = sext i16 %74 to i32
  %75 = load i32, i32* %min, align 4, !tbaa !6
  %cmp75 = icmp slt i32 %conv74, %75
  br i1 %cmp75, label %cond.true77, label %cond.false79

cond.true77:                                      ; preds = %cond.end72
  %76 = load i16, i16* %p1, align 2, !tbaa !8
  %conv78 = sext i16 %76 to i32
  br label %cond.end80

cond.false79:                                     ; preds = %cond.end72
  %77 = load i32, i32* %min, align 4, !tbaa !6
  br label %cond.end80

cond.end80:                                       ; preds = %cond.false79, %cond.true77
  %cond81 = phi i32 [ %conv78, %cond.true77 ], [ %77, %cond.false79 ]
  store i32 %cond81, i32* %min, align 4, !tbaa !6
  %78 = bitcast i16* %s0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %78) #6
  %79 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %80 = load i32, i32* %i, align 4, !tbaa !6
  %mul82 = mul nsw i32 %80, 144
  %81 = load i32, i32* %j, align 4, !tbaa !6
  %add83 = add nsw i32 %mul82, %81
  %82 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %add84 = add nsw i32 %82, 2
  %and85 = and i32 %add84, 7
  %arrayidx86 = getelementptr inbounds [8 x [2 x i32]], [8 x [2 x i32]]* @cdef_directions, i32 0, i32 %and85
  %83 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx86, i32 0, i32 %83
  %84 = load i32, i32* %arrayidx87, align 4, !tbaa !6
  %add88 = add nsw i32 %add83, %84
  %arrayidx89 = getelementptr inbounds i16, i16* %79, i32 %add88
  %85 = load i16, i16* %arrayidx89, align 2, !tbaa !8
  store i16 %85, i16* %s0, align 2, !tbaa !8
  %86 = bitcast i16* %s1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %86) #6
  %87 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %88 = load i32, i32* %i, align 4, !tbaa !6
  %mul90 = mul nsw i32 %88, 144
  %89 = load i32, i32* %j, align 4, !tbaa !6
  %add91 = add nsw i32 %mul90, %89
  %90 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %add92 = add nsw i32 %90, 2
  %and93 = and i32 %add92, 7
  %arrayidx94 = getelementptr inbounds [8 x [2 x i32]], [8 x [2 x i32]]* @cdef_directions, i32 0, i32 %and93
  %91 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx95 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx94, i32 0, i32 %91
  %92 = load i32, i32* %arrayidx95, align 4, !tbaa !6
  %sub96 = sub nsw i32 %add91, %92
  %arrayidx97 = getelementptr inbounds i16, i16* %87, i32 %sub96
  %93 = load i16, i16* %arrayidx97, align 2, !tbaa !8
  store i16 %93, i16* %s1, align 2, !tbaa !8
  %94 = bitcast i16* %s2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %94) #6
  %95 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %96 = load i32, i32* %i, align 4, !tbaa !6
  %mul98 = mul nsw i32 %96, 144
  %97 = load i32, i32* %j, align 4, !tbaa !6
  %add99 = add nsw i32 %mul98, %97
  %98 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %add100 = add nsw i32 %98, 6
  %and101 = and i32 %add100, 7
  %arrayidx102 = getelementptr inbounds [8 x [2 x i32]], [8 x [2 x i32]]* @cdef_directions, i32 0, i32 %and101
  %99 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx103 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx102, i32 0, i32 %99
  %100 = load i32, i32* %arrayidx103, align 4, !tbaa !6
  %add104 = add nsw i32 %add99, %100
  %arrayidx105 = getelementptr inbounds i16, i16* %95, i32 %add104
  %101 = load i16, i16* %arrayidx105, align 2, !tbaa !8
  store i16 %101, i16* %s2, align 2, !tbaa !8
  %102 = bitcast i16* %s3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %102) #6
  %103 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %104 = load i32, i32* %i, align 4, !tbaa !6
  %mul106 = mul nsw i32 %104, 144
  %105 = load i32, i32* %j, align 4, !tbaa !6
  %add107 = add nsw i32 %mul106, %105
  %106 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %add108 = add nsw i32 %106, 6
  %and109 = and i32 %add108, 7
  %arrayidx110 = getelementptr inbounds [8 x [2 x i32]], [8 x [2 x i32]]* @cdef_directions, i32 0, i32 %and109
  %107 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx111 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx110, i32 0, i32 %107
  %108 = load i32, i32* %arrayidx111, align 4, !tbaa !6
  %sub112 = sub nsw i32 %add107, %108
  %arrayidx113 = getelementptr inbounds i16, i16* %103, i32 %sub112
  %109 = load i16, i16* %arrayidx113, align 2, !tbaa !8
  store i16 %109, i16* %s3, align 2, !tbaa !8
  %110 = load i16, i16* %s0, align 2, !tbaa !8
  %conv114 = sext i16 %110 to i32
  %cmp115 = icmp ne i32 %conv114, 30000
  br i1 %cmp115, label %if.then117, label %if.end126

if.then117:                                       ; preds = %cond.end80
  %111 = load i16, i16* %s0, align 2, !tbaa !8
  %conv118 = sext i16 %111 to i32
  %112 = load i32, i32* %max, align 4, !tbaa !6
  %cmp119 = icmp sgt i32 %conv118, %112
  br i1 %cmp119, label %cond.true121, label %cond.false123

cond.true121:                                     ; preds = %if.then117
  %113 = load i16, i16* %s0, align 2, !tbaa !8
  %conv122 = sext i16 %113 to i32
  br label %cond.end124

cond.false123:                                    ; preds = %if.then117
  %114 = load i32, i32* %max, align 4, !tbaa !6
  br label %cond.end124

cond.end124:                                      ; preds = %cond.false123, %cond.true121
  %cond125 = phi i32 [ %conv122, %cond.true121 ], [ %114, %cond.false123 ]
  store i32 %cond125, i32* %max, align 4, !tbaa !6
  br label %if.end126

if.end126:                                        ; preds = %cond.end124, %cond.end80
  %115 = load i16, i16* %s1, align 2, !tbaa !8
  %conv127 = sext i16 %115 to i32
  %cmp128 = icmp ne i32 %conv127, 30000
  br i1 %cmp128, label %if.then130, label %if.end139

if.then130:                                       ; preds = %if.end126
  %116 = load i16, i16* %s1, align 2, !tbaa !8
  %conv131 = sext i16 %116 to i32
  %117 = load i32, i32* %max, align 4, !tbaa !6
  %cmp132 = icmp sgt i32 %conv131, %117
  br i1 %cmp132, label %cond.true134, label %cond.false136

cond.true134:                                     ; preds = %if.then130
  %118 = load i16, i16* %s1, align 2, !tbaa !8
  %conv135 = sext i16 %118 to i32
  br label %cond.end137

cond.false136:                                    ; preds = %if.then130
  %119 = load i32, i32* %max, align 4, !tbaa !6
  br label %cond.end137

cond.end137:                                      ; preds = %cond.false136, %cond.true134
  %cond138 = phi i32 [ %conv135, %cond.true134 ], [ %119, %cond.false136 ]
  store i32 %cond138, i32* %max, align 4, !tbaa !6
  br label %if.end139

if.end139:                                        ; preds = %cond.end137, %if.end126
  %120 = load i16, i16* %s2, align 2, !tbaa !8
  %conv140 = sext i16 %120 to i32
  %cmp141 = icmp ne i32 %conv140, 30000
  br i1 %cmp141, label %if.then143, label %if.end152

if.then143:                                       ; preds = %if.end139
  %121 = load i16, i16* %s2, align 2, !tbaa !8
  %conv144 = sext i16 %121 to i32
  %122 = load i32, i32* %max, align 4, !tbaa !6
  %cmp145 = icmp sgt i32 %conv144, %122
  br i1 %cmp145, label %cond.true147, label %cond.false149

cond.true147:                                     ; preds = %if.then143
  %123 = load i16, i16* %s2, align 2, !tbaa !8
  %conv148 = sext i16 %123 to i32
  br label %cond.end150

cond.false149:                                    ; preds = %if.then143
  %124 = load i32, i32* %max, align 4, !tbaa !6
  br label %cond.end150

cond.end150:                                      ; preds = %cond.false149, %cond.true147
  %cond151 = phi i32 [ %conv148, %cond.true147 ], [ %124, %cond.false149 ]
  store i32 %cond151, i32* %max, align 4, !tbaa !6
  br label %if.end152

if.end152:                                        ; preds = %cond.end150, %if.end139
  %125 = load i16, i16* %s3, align 2, !tbaa !8
  %conv153 = sext i16 %125 to i32
  %cmp154 = icmp ne i32 %conv153, 30000
  br i1 %cmp154, label %if.then156, label %if.end165

if.then156:                                       ; preds = %if.end152
  %126 = load i16, i16* %s3, align 2, !tbaa !8
  %conv157 = sext i16 %126 to i32
  %127 = load i32, i32* %max, align 4, !tbaa !6
  %cmp158 = icmp sgt i32 %conv157, %127
  br i1 %cmp158, label %cond.true160, label %cond.false162

cond.true160:                                     ; preds = %if.then156
  %128 = load i16, i16* %s3, align 2, !tbaa !8
  %conv161 = sext i16 %128 to i32
  br label %cond.end163

cond.false162:                                    ; preds = %if.then156
  %129 = load i32, i32* %max, align 4, !tbaa !6
  br label %cond.end163

cond.end163:                                      ; preds = %cond.false162, %cond.true160
  %cond164 = phi i32 [ %conv161, %cond.true160 ], [ %129, %cond.false162 ]
  store i32 %cond164, i32* %max, align 4, !tbaa !6
  br label %if.end165

if.end165:                                        ; preds = %cond.end163, %if.end152
  %130 = load i16, i16* %s0, align 2, !tbaa !8
  %conv166 = sext i16 %130 to i32
  %131 = load i32, i32* %min, align 4, !tbaa !6
  %cmp167 = icmp slt i32 %conv166, %131
  br i1 %cmp167, label %cond.true169, label %cond.false171

cond.true169:                                     ; preds = %if.end165
  %132 = load i16, i16* %s0, align 2, !tbaa !8
  %conv170 = sext i16 %132 to i32
  br label %cond.end172

cond.false171:                                    ; preds = %if.end165
  %133 = load i32, i32* %min, align 4, !tbaa !6
  br label %cond.end172

cond.end172:                                      ; preds = %cond.false171, %cond.true169
  %cond173 = phi i32 [ %conv170, %cond.true169 ], [ %133, %cond.false171 ]
  store i32 %cond173, i32* %min, align 4, !tbaa !6
  %134 = load i16, i16* %s1, align 2, !tbaa !8
  %conv174 = sext i16 %134 to i32
  %135 = load i32, i32* %min, align 4, !tbaa !6
  %cmp175 = icmp slt i32 %conv174, %135
  br i1 %cmp175, label %cond.true177, label %cond.false179

cond.true177:                                     ; preds = %cond.end172
  %136 = load i16, i16* %s1, align 2, !tbaa !8
  %conv178 = sext i16 %136 to i32
  br label %cond.end180

cond.false179:                                    ; preds = %cond.end172
  %137 = load i32, i32* %min, align 4, !tbaa !6
  br label %cond.end180

cond.end180:                                      ; preds = %cond.false179, %cond.true177
  %cond181 = phi i32 [ %conv178, %cond.true177 ], [ %137, %cond.false179 ]
  store i32 %cond181, i32* %min, align 4, !tbaa !6
  %138 = load i16, i16* %s2, align 2, !tbaa !8
  %conv182 = sext i16 %138 to i32
  %139 = load i32, i32* %min, align 4, !tbaa !6
  %cmp183 = icmp slt i32 %conv182, %139
  br i1 %cmp183, label %cond.true185, label %cond.false187

cond.true185:                                     ; preds = %cond.end180
  %140 = load i16, i16* %s2, align 2, !tbaa !8
  %conv186 = sext i16 %140 to i32
  br label %cond.end188

cond.false187:                                    ; preds = %cond.end180
  %141 = load i32, i32* %min, align 4, !tbaa !6
  br label %cond.end188

cond.end188:                                      ; preds = %cond.false187, %cond.true185
  %cond189 = phi i32 [ %conv186, %cond.true185 ], [ %141, %cond.false187 ]
  store i32 %cond189, i32* %min, align 4, !tbaa !6
  %142 = load i16, i16* %s3, align 2, !tbaa !8
  %conv190 = sext i16 %142 to i32
  %143 = load i32, i32* %min, align 4, !tbaa !6
  %cmp191 = icmp slt i32 %conv190, %143
  br i1 %cmp191, label %cond.true193, label %cond.false195

cond.true193:                                     ; preds = %cond.end188
  %144 = load i16, i16* %s3, align 2, !tbaa !8
  %conv194 = sext i16 %144 to i32
  br label %cond.end196

cond.false195:                                    ; preds = %cond.end188
  %145 = load i32, i32* %min, align 4, !tbaa !6
  br label %cond.end196

cond.end196:                                      ; preds = %cond.false195, %cond.true193
  %cond197 = phi i32 [ %conv194, %cond.true193 ], [ %145, %cond.false195 ]
  store i32 %cond197, i32* %min, align 4, !tbaa !6
  %146 = load i32*, i32** %sec_taps, align 4, !tbaa !2
  %147 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx198 = getelementptr inbounds i32, i32* %146, i32 %147
  %148 = load i32, i32* %arrayidx198, align 4, !tbaa !6
  %149 = load i16, i16* %s0, align 2, !tbaa !8
  %conv199 = sext i16 %149 to i32
  %150 = load i16, i16* %x, align 2, !tbaa !8
  %conv200 = sext i16 %150 to i32
  %sub201 = sub nsw i32 %conv199, %conv200
  %151 = load i32, i32* %sec_strength.addr, align 4, !tbaa !6
  %152 = load i32, i32* %sec_damping.addr, align 4, !tbaa !6
  %call202 = call i32 @constrain(i32 %sub201, i32 %151, i32 %152)
  %mul203 = mul nsw i32 %148, %call202
  %153 = load i16, i16* %sum, align 2, !tbaa !8
  %conv204 = sext i16 %153 to i32
  %add205 = add nsw i32 %conv204, %mul203
  %conv206 = trunc i32 %add205 to i16
  store i16 %conv206, i16* %sum, align 2, !tbaa !8
  %154 = load i32*, i32** %sec_taps, align 4, !tbaa !2
  %155 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx207 = getelementptr inbounds i32, i32* %154, i32 %155
  %156 = load i32, i32* %arrayidx207, align 4, !tbaa !6
  %157 = load i16, i16* %s1, align 2, !tbaa !8
  %conv208 = sext i16 %157 to i32
  %158 = load i16, i16* %x, align 2, !tbaa !8
  %conv209 = sext i16 %158 to i32
  %sub210 = sub nsw i32 %conv208, %conv209
  %159 = load i32, i32* %sec_strength.addr, align 4, !tbaa !6
  %160 = load i32, i32* %sec_damping.addr, align 4, !tbaa !6
  %call211 = call i32 @constrain(i32 %sub210, i32 %159, i32 %160)
  %mul212 = mul nsw i32 %156, %call211
  %161 = load i16, i16* %sum, align 2, !tbaa !8
  %conv213 = sext i16 %161 to i32
  %add214 = add nsw i32 %conv213, %mul212
  %conv215 = trunc i32 %add214 to i16
  store i16 %conv215, i16* %sum, align 2, !tbaa !8
  %162 = load i32*, i32** %sec_taps, align 4, !tbaa !2
  %163 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx216 = getelementptr inbounds i32, i32* %162, i32 %163
  %164 = load i32, i32* %arrayidx216, align 4, !tbaa !6
  %165 = load i16, i16* %s2, align 2, !tbaa !8
  %conv217 = sext i16 %165 to i32
  %166 = load i16, i16* %x, align 2, !tbaa !8
  %conv218 = sext i16 %166 to i32
  %sub219 = sub nsw i32 %conv217, %conv218
  %167 = load i32, i32* %sec_strength.addr, align 4, !tbaa !6
  %168 = load i32, i32* %sec_damping.addr, align 4, !tbaa !6
  %call220 = call i32 @constrain(i32 %sub219, i32 %167, i32 %168)
  %mul221 = mul nsw i32 %164, %call220
  %169 = load i16, i16* %sum, align 2, !tbaa !8
  %conv222 = sext i16 %169 to i32
  %add223 = add nsw i32 %conv222, %mul221
  %conv224 = trunc i32 %add223 to i16
  store i16 %conv224, i16* %sum, align 2, !tbaa !8
  %170 = load i32*, i32** %sec_taps, align 4, !tbaa !2
  %171 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx225 = getelementptr inbounds i32, i32* %170, i32 %171
  %172 = load i32, i32* %arrayidx225, align 4, !tbaa !6
  %173 = load i16, i16* %s3, align 2, !tbaa !8
  %conv226 = sext i16 %173 to i32
  %174 = load i16, i16* %x, align 2, !tbaa !8
  %conv227 = sext i16 %174 to i32
  %sub228 = sub nsw i32 %conv226, %conv227
  %175 = load i32, i32* %sec_strength.addr, align 4, !tbaa !6
  %176 = load i32, i32* %sec_damping.addr, align 4, !tbaa !6
  %call229 = call i32 @constrain(i32 %sub228, i32 %175, i32 %176)
  %mul230 = mul nsw i32 %172, %call229
  %177 = load i16, i16* %sum, align 2, !tbaa !8
  %conv231 = sext i16 %177 to i32
  %add232 = add nsw i32 %conv231, %mul230
  %conv233 = trunc i32 %add232 to i16
  store i16 %conv233, i16* %sum, align 2, !tbaa !8
  %178 = bitcast i16* %s3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %178) #6
  %179 = bitcast i16* %s2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %179) #6
  %180 = bitcast i16* %s1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %180) #6
  %181 = bitcast i16* %s0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %181) #6
  %182 = bitcast i16* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %182) #6
  %183 = bitcast i16* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %183) #6
  br label %for.inc

for.inc:                                          ; preds = %cond.end196
  %184 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %184, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond14

for.end:                                          ; preds = %for.cond14
  %185 = load i16, i16* %x, align 2, !tbaa !8
  %conv234 = sext i16 %185 to i32
  %186 = load i16, i16* %sum, align 2, !tbaa !8
  %conv235 = sext i16 %186 to i32
  %add236 = add nsw i32 8, %conv235
  %187 = load i16, i16* %sum, align 2, !tbaa !8
  %conv237 = sext i16 %187 to i32
  %cmp238 = icmp slt i32 %conv237, 0
  %conv239 = zext i1 %cmp238 to i32
  %sub240 = sub nsw i32 %add236, %conv239
  %shr241 = ashr i32 %sub240, 4
  %add242 = add nsw i32 %conv234, %shr241
  %188 = load i32, i32* %min, align 4, !tbaa !6
  %189 = load i32, i32* %max, align 4, !tbaa !6
  %call243 = call i32 @clamp(i32 %add242, i32 %188, i32 %189)
  %conv244 = trunc i32 %call243 to i16
  store i16 %conv244, i16* %y, align 2, !tbaa !8
  %190 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %190, null
  br i1 %tobool, label %if.then245, label %if.else

if.then245:                                       ; preds = %for.end
  %191 = load i16, i16* %y, align 2, !tbaa !8
  %conv246 = trunc i16 %191 to i8
  %192 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %193 = load i32, i32* %i, align 4, !tbaa !6
  %194 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %mul247 = mul nsw i32 %193, %194
  %195 = load i32, i32* %j, align 4, !tbaa !6
  %add248 = add nsw i32 %mul247, %195
  %arrayidx249 = getelementptr inbounds i8, i8* %192, i32 %add248
  store i8 %conv246, i8* %arrayidx249, align 1, !tbaa !10
  br label %if.end253

if.else:                                          ; preds = %for.end
  %196 = load i16, i16* %y, align 2, !tbaa !8
  %197 = load i16*, i16** %dst16.addr, align 4, !tbaa !2
  %198 = load i32, i32* %i, align 4, !tbaa !6
  %199 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %mul250 = mul nsw i32 %198, %199
  %200 = load i32, i32* %j, align 4, !tbaa !6
  %add251 = add nsw i32 %mul250, %200
  %arrayidx252 = getelementptr inbounds i16, i16* %197, i32 %add251
  store i16 %196, i16* %arrayidx252, align 2, !tbaa !8
  br label %if.end253

if.end253:                                        ; preds = %if.else, %if.then245
  %201 = bitcast i32* %min to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #6
  %202 = bitcast i32* %max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #6
  %203 = bitcast i16* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %203) #6
  %204 = bitcast i16* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %204) #6
  %205 = bitcast i16* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %205) #6
  br label %for.inc254

for.inc254:                                       ; preds = %if.end253
  %206 = load i32, i32* %j, align 4, !tbaa !6
  %inc255 = add nsw i32 %206, 1
  store i32 %inc255, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.end256:                                       ; preds = %lor.end7
  br label %for.inc257

for.inc257:                                       ; preds = %for.end256
  %207 = load i32, i32* %i, align 4, !tbaa !6
  %inc258 = add nsw i32 %207, 1
  store i32 %inc258, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end259:                                       ; preds = %lor.end
  %208 = bitcast i32** %sec_taps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #6
  %209 = bitcast i32** %pri_taps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #6
  %210 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #6
  %211 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #6
  %212 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #6
  %213 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @constrain(i32 %diff, i32 %threshold, i32 %damping) #3 {
entry:
  %retval = alloca i32, align 4
  %diff.addr = alloca i32, align 4
  %threshold.addr = alloca i32, align 4
  %damping.addr = alloca i32, align 4
  %shift = alloca i32, align 4
  store i32 %diff, i32* %diff.addr, align 4, !tbaa !6
  store i32 %threshold, i32* %threshold.addr, align 4, !tbaa !6
  store i32 %damping, i32* %damping.addr, align 4, !tbaa !6
  %0 = load i32, i32* %threshold.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %damping.addr, align 4, !tbaa !6
  %3 = load i32, i32* %threshold.addr, align 4, !tbaa !6
  %call = call i32 @get_msb(i32 %3)
  %sub = sub nsw i32 %2, %call
  %cmp = icmp sgt i32 0, %sub
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %4 = load i32, i32* %damping.addr, align 4, !tbaa !6
  %5 = load i32, i32* %threshold.addr, align 4, !tbaa !6
  %call1 = call i32 @get_msb(i32 %5)
  %sub2 = sub nsw i32 %4, %call1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %sub2, %cond.false ]
  store i32 %cond, i32* %shift, align 4, !tbaa !6
  %6 = load i32, i32* %diff.addr, align 4, !tbaa !6
  %call3 = call i32 @sign(i32 %6)
  %7 = load i32, i32* %diff.addr, align 4, !tbaa !6
  %call4 = call i32 @abs(i32 %7) #7
  %8 = load i32, i32* %threshold.addr, align 4, !tbaa !6
  %9 = load i32, i32* %diff.addr, align 4, !tbaa !6
  %call5 = call i32 @abs(i32 %9) #7
  %10 = load i32, i32* %shift, align 4, !tbaa !6
  %shr = ashr i32 %call5, %10
  %sub6 = sub nsw i32 %8, %shr
  %cmp7 = icmp sgt i32 0, %sub6
  br i1 %cmp7, label %cond.true8, label %cond.false9

cond.true8:                                       ; preds = %cond.end
  br label %cond.end13

cond.false9:                                      ; preds = %cond.end
  %11 = load i32, i32* %threshold.addr, align 4, !tbaa !6
  %12 = load i32, i32* %diff.addr, align 4, !tbaa !6
  %call10 = call i32 @abs(i32 %12) #7
  %13 = load i32, i32* %shift, align 4, !tbaa !6
  %shr11 = ashr i32 %call10, %13
  %sub12 = sub nsw i32 %11, %shr11
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false9, %cond.true8
  %cond14 = phi i32 [ 0, %cond.true8 ], [ %sub12, %cond.false9 ]
  %cmp15 = icmp slt i32 %call4, %cond14
  br i1 %cmp15, label %cond.true16, label %cond.false18

cond.true16:                                      ; preds = %cond.end13
  %14 = load i32, i32* %diff.addr, align 4, !tbaa !6
  %call17 = call i32 @abs(i32 %14) #7
  br label %cond.end30

cond.false18:                                     ; preds = %cond.end13
  %15 = load i32, i32* %threshold.addr, align 4, !tbaa !6
  %16 = load i32, i32* %diff.addr, align 4, !tbaa !6
  %call19 = call i32 @abs(i32 %16) #7
  %17 = load i32, i32* %shift, align 4, !tbaa !6
  %shr20 = ashr i32 %call19, %17
  %sub21 = sub nsw i32 %15, %shr20
  %cmp22 = icmp sgt i32 0, %sub21
  br i1 %cmp22, label %cond.true23, label %cond.false24

cond.true23:                                      ; preds = %cond.false18
  br label %cond.end28

cond.false24:                                     ; preds = %cond.false18
  %18 = load i32, i32* %threshold.addr, align 4, !tbaa !6
  %19 = load i32, i32* %diff.addr, align 4, !tbaa !6
  %call25 = call i32 @abs(i32 %19) #7
  %20 = load i32, i32* %shift, align 4, !tbaa !6
  %shr26 = ashr i32 %call25, %20
  %sub27 = sub nsw i32 %18, %shr26
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false24, %cond.true23
  %cond29 = phi i32 [ 0, %cond.true23 ], [ %sub27, %cond.false24 ]
  br label %cond.end30

cond.end30:                                       ; preds = %cond.end28, %cond.true16
  %cond31 = phi i32 [ %call17, %cond.true16 ], [ %cond29, %cond.end28 ]
  %mul = mul nsw i32 %call3, %cond31
  store i32 %mul, i32* %retval, align 4
  %21 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  br label %return

return:                                           ; preds = %cond.end30, %if.then
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #3 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: nounwind
define hidden void @av1_cdef_filter_fb(i8* %dst8, i16* %dst16, i32 %dstride, i16* %in, i32 %xdec, i32 %ydec, [16 x i32]* %dir, i32* %dirinit, [16 x i32]* %var, i32 %pli, %struct.cdef_list* %dlist, i32 %cdef_count, i32 %level, i32 %sec_strength, i32 %damping, i32 %coeff_shift) #0 {
entry:
  %dst8.addr = alloca i8*, align 4
  %dst16.addr = alloca i16*, align 4
  %dstride.addr = alloca i32, align 4
  %in.addr = alloca i16*, align 4
  %xdec.addr = alloca i32, align 4
  %ydec.addr = alloca i32, align 4
  %dir.addr = alloca [16 x i32]*, align 4
  %dirinit.addr = alloca i32*, align 4
  %var.addr = alloca [16 x i32]*, align 4
  %pli.addr = alloca i32, align 4
  %dlist.addr = alloca %struct.cdef_list*, align 4
  %cdef_count.addr = alloca i32, align 4
  %level.addr = alloca i32, align 4
  %sec_strength.addr = alloca i32, align 4
  %damping.addr = alloca i32, align 4
  %coeff_shift.addr = alloca i32, align 4
  %bi = alloca i32, align 4
  %bx = alloca i32, align 4
  %by = alloca i32, align 4
  %pri_strength = alloca i32, align 4
  %bw_log2 = alloca i32, align 4
  %bh_log2 = alloca i32, align 4
  %iy = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %bsize = alloca i32, align 4
  %t = alloca i32, align 4
  %s = alloca i32, align 4
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i16* %dst16, i16** %dst16.addr, align 4, !tbaa !2
  store i32 %dstride, i32* %dstride.addr, align 4, !tbaa !6
  store i16* %in, i16** %in.addr, align 4, !tbaa !2
  store i32 %xdec, i32* %xdec.addr, align 4, !tbaa !6
  store i32 %ydec, i32* %ydec.addr, align 4, !tbaa !6
  store [16 x i32]* %dir, [16 x i32]** %dir.addr, align 4, !tbaa !2
  store i32* %dirinit, i32** %dirinit.addr, align 4, !tbaa !2
  store [16 x i32]* %var, [16 x i32]** %var.addr, align 4, !tbaa !2
  store i32 %pli, i32* %pli.addr, align 4, !tbaa !6
  store %struct.cdef_list* %dlist, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  store i32 %cdef_count, i32* %cdef_count.addr, align 4, !tbaa !6
  store i32 %level, i32* %level.addr, align 4, !tbaa !6
  store i32 %sec_strength, i32* %sec_strength.addr, align 4, !tbaa !6
  store i32 %damping, i32* %damping.addr, align 4, !tbaa !6
  store i32 %coeff_shift, i32* %coeff_shift.addr, align 4, !tbaa !6
  %0 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %by to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %pri_strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %level.addr, align 4, !tbaa !6
  %5 = load i32, i32* %coeff_shift.addr, align 4, !tbaa !6
  %shl = shl i32 %4, %5
  store i32 %shl, i32* %pri_strength, align 4, !tbaa !6
  %6 = load i32, i32* %coeff_shift.addr, align 4, !tbaa !6
  %7 = load i32, i32* %sec_strength.addr, align 4, !tbaa !6
  %shl1 = shl i32 %7, %6
  store i32 %shl1, i32* %sec_strength.addr, align 4, !tbaa !6
  %8 = load i32, i32* %coeff_shift.addr, align 4, !tbaa !6
  %9 = load i32, i32* %pli.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %9, 0
  %conv = zext i1 %cmp to i32
  %sub = sub nsw i32 %8, %conv
  %10 = load i32, i32* %damping.addr, align 4, !tbaa !6
  %add = add nsw i32 %10, %sub
  store i32 %add, i32* %damping.addr, align 4, !tbaa !6
  %11 = bitcast i32* %bw_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load i32, i32* %xdec.addr, align 4, !tbaa !6
  %sub2 = sub nsw i32 3, %12
  store i32 %sub2, i32* %bw_log2, align 4, !tbaa !6
  %13 = bitcast i32* %bh_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %ydec.addr, align 4, !tbaa !6
  %sub3 = sub nsw i32 3, %14
  store i32 %sub3, i32* %bh_log2, align 4, !tbaa !6
  %15 = load i32*, i32** %dirinit.addr, align 4, !tbaa !2
  %tobool = icmp ne i32* %15, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %16 = load i32, i32* %pri_strength, align 4, !tbaa !6
  %cmp4 = icmp eq i32 %16, 0
  br i1 %cmp4, label %land.lhs.true6, label %if.end

land.lhs.true6:                                   ; preds = %land.lhs.true
  %17 = load i32, i32* %sec_strength.addr, align 4, !tbaa !6
  %cmp7 = icmp eq i32 %17, 0
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true6
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc33, %if.then
  %18 = load i32, i32* %bi, align 4, !tbaa !6
  %19 = load i32, i32* %cdef_count.addr, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %18, %19
  br i1 %cmp9, label %for.body, label %for.end35

for.body:                                         ; preds = %for.cond
  %20 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %21 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %20, i32 %21
  %by11 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx, i32 0, i32 0
  %22 = load i8, i8* %by11, align 1, !tbaa !11
  %conv12 = zext i8 %22 to i32
  store i32 %conv12, i32* %by, align 4, !tbaa !6
  %23 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %24 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %23, i32 %24
  %bx14 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx13, i32 0, i32 1
  %25 = load i8, i8* %bx14, align 1, !tbaa !13
  %conv15 = zext i8 %25 to i32
  store i32 %conv15, i32* %bx, align 4, !tbaa !6
  %26 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  store i32 0, i32* %iy, align 4, !tbaa !6
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc, %for.body
  %27 = load i32, i32* %iy, align 4, !tbaa !6
  %28 = load i32, i32* %bh_log2, align 4, !tbaa !6
  %shl17 = shl i32 1, %28
  %cmp18 = icmp slt i32 %27, %shl17
  br i1 %cmp18, label %for.body20, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond16
  %29 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  br label %for.end

for.body20:                                       ; preds = %for.cond16
  %30 = load i16*, i16** %dst16.addr, align 4, !tbaa !2
  %31 = load i32, i32* %bi, align 4, !tbaa !6
  %32 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %33 = load i32, i32* %bh_log2, align 4, !tbaa !6
  %add21 = add nsw i32 %32, %33
  %shl22 = shl i32 %31, %add21
  %34 = load i32, i32* %iy, align 4, !tbaa !6
  %35 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %shl23 = shl i32 %34, %35
  %add24 = add nsw i32 %shl22, %shl23
  %arrayidx25 = getelementptr inbounds i16, i16* %30, i32 %add24
  %36 = bitcast i16* %arrayidx25 to i8*
  %37 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %38 = load i32, i32* %by, align 4, !tbaa !6
  %39 = load i32, i32* %bh_log2, align 4, !tbaa !6
  %shl26 = shl i32 %38, %39
  %40 = load i32, i32* %iy, align 4, !tbaa !6
  %add27 = add nsw i32 %shl26, %40
  %mul = mul nsw i32 %add27, 144
  %41 = load i32, i32* %bx, align 4, !tbaa !6
  %42 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %shl28 = shl i32 %41, %42
  %add29 = add nsw i32 %mul, %shl28
  %arrayidx30 = getelementptr inbounds i16, i16* %37, i32 %add29
  %43 = bitcast i16* %arrayidx30 to i8*
  %44 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %shl31 = shl i32 1, %44
  %mul32 = mul i32 %shl31, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %36, i8* align 2 %43, i32 %mul32, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body20
  %45 = load i32, i32* %iy, align 4, !tbaa !6
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %iy, align 4, !tbaa !6
  br label %for.cond16

for.end:                                          ; preds = %for.cond.cleanup
  br label %for.inc33

for.inc33:                                        ; preds = %for.end
  %46 = load i32, i32* %bi, align 4, !tbaa !6
  %inc34 = add nsw i32 %46, 1
  store i32 %inc34, i32* %bi, align 4, !tbaa !6
  br label %for.cond

for.end35:                                        ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true6, %land.lhs.true, %entry
  %47 = load i32, i32* %pli.addr, align 4, !tbaa !6
  %cmp36 = icmp eq i32 %47, 0
  br i1 %cmp36, label %if.then38, label %if.end68

if.then38:                                        ; preds = %if.end
  %48 = load i32*, i32** %dirinit.addr, align 4, !tbaa !2
  %tobool39 = icmp ne i32* %48, null
  br i1 %tobool39, label %lor.lhs.false, label %if.then41

lor.lhs.false:                                    ; preds = %if.then38
  %49 = load i32*, i32** %dirinit.addr, align 4, !tbaa !2
  %50 = load i32, i32* %49, align 4, !tbaa !6
  %tobool40 = icmp ne i32 %50, 0
  br i1 %tobool40, label %if.end67, label %if.then41

if.then41:                                        ; preds = %lor.lhs.false, %if.then38
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc61, %if.then41
  %51 = load i32, i32* %bi, align 4, !tbaa !6
  %52 = load i32, i32* %cdef_count.addr, align 4, !tbaa !6
  %cmp43 = icmp slt i32 %51, %52
  br i1 %cmp43, label %for.body45, label %for.end63

for.body45:                                       ; preds = %for.cond42
  %53 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %54 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %53, i32 %54
  %by47 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx46, i32 0, i32 0
  %55 = load i8, i8* %by47, align 1, !tbaa !11
  %conv48 = zext i8 %55 to i32
  store i32 %conv48, i32* %by, align 4, !tbaa !6
  %56 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %57 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx49 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %56, i32 %57
  %bx50 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx49, i32 0, i32 1
  %58 = load i8, i8* %bx50, align 1, !tbaa !13
  %conv51 = zext i8 %58 to i32
  store i32 %conv51, i32* %bx, align 4, !tbaa !6
  %59 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %60 = load i32, i32* %by, align 4, !tbaa !6
  %mul52 = mul nsw i32 8, %60
  %mul53 = mul nsw i32 %mul52, 144
  %61 = load i32, i32* %bx, align 4, !tbaa !6
  %mul54 = mul nsw i32 8, %61
  %add55 = add nsw i32 %mul53, %mul54
  %arrayidx56 = getelementptr inbounds i16, i16* %59, i32 %add55
  %62 = load [16 x i32]*, [16 x i32]** %var.addr, align 4, !tbaa !2
  %63 = load i32, i32* %by, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds [16 x i32], [16 x i32]* %62, i32 %63
  %64 = load i32, i32* %bx, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx57, i32 0, i32 %64
  %65 = load i32, i32* %coeff_shift.addr, align 4, !tbaa !6
  %call = call i32 @cdef_find_dir_c(i16* %arrayidx56, i32 144, i32* %arrayidx58, i32 %65)
  %66 = load [16 x i32]*, [16 x i32]** %dir.addr, align 4, !tbaa !2
  %67 = load i32, i32* %by, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds [16 x i32], [16 x i32]* %66, i32 %67
  %68 = load i32, i32* %bx, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx59, i32 0, i32 %68
  store i32 %call, i32* %arrayidx60, align 4, !tbaa !6
  br label %for.inc61

for.inc61:                                        ; preds = %for.body45
  %69 = load i32, i32* %bi, align 4, !tbaa !6
  %inc62 = add nsw i32 %69, 1
  store i32 %inc62, i32* %bi, align 4, !tbaa !6
  br label %for.cond42

for.end63:                                        ; preds = %for.cond42
  %70 = load i32*, i32** %dirinit.addr, align 4, !tbaa !2
  %tobool64 = icmp ne i32* %70, null
  br i1 %tobool64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %for.end63
  %71 = load i32*, i32** %dirinit.addr, align 4, !tbaa !2
  store i32 1, i32* %71, align 4, !tbaa !6
  br label %if.end66

if.end66:                                         ; preds = %if.then65, %for.end63
  br label %if.end67

if.end67:                                         ; preds = %if.end66, %lor.lhs.false
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %if.end
  %72 = load i32, i32* %pli.addr, align 4, !tbaa !6
  %cmp69 = icmp eq i32 %72, 1
  br i1 %cmp69, label %land.lhs.true71, label %if.end94

land.lhs.true71:                                  ; preds = %if.end68
  %73 = load i32, i32* %xdec.addr, align 4, !tbaa !6
  %74 = load i32, i32* %ydec.addr, align 4, !tbaa !6
  %cmp72 = icmp ne i32 %73, %74
  br i1 %cmp72, label %if.then74, label %if.end94

if.then74:                                        ; preds = %land.lhs.true71
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc91, %if.then74
  %75 = load i32, i32* %bi, align 4, !tbaa !6
  %76 = load i32, i32* %cdef_count.addr, align 4, !tbaa !6
  %cmp76 = icmp slt i32 %75, %76
  br i1 %cmp76, label %for.body78, label %for.end93

for.body78:                                       ; preds = %for.cond75
  %77 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %78 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx79 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %77, i32 %78
  %by80 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx79, i32 0, i32 0
  %79 = load i8, i8* %by80, align 1, !tbaa !11
  %conv81 = zext i8 %79 to i32
  store i32 %conv81, i32* %by, align 4, !tbaa !6
  %80 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %81 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx82 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %80, i32 %81
  %bx83 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx82, i32 0, i32 1
  %82 = load i8, i8* %bx83, align 1, !tbaa !13
  %conv84 = zext i8 %82 to i32
  store i32 %conv84, i32* %bx, align 4, !tbaa !6
  %83 = load i32, i32* %xdec.addr, align 4, !tbaa !6
  %tobool85 = icmp ne i32 %83, 0
  %84 = zext i1 %tobool85 to i64
  %cond = select i1 %tobool85, i32* getelementptr inbounds ([8 x i32], [8 x i32]* @av1_cdef_filter_fb.conv422, i32 0, i32 0), i32* getelementptr inbounds ([8 x i32], [8 x i32]* @av1_cdef_filter_fb.conv440, i32 0, i32 0)
  %85 = load [16 x i32]*, [16 x i32]** %dir.addr, align 4, !tbaa !2
  %86 = load i32, i32* %by, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds [16 x i32], [16 x i32]* %85, i32 %86
  %87 = load i32, i32* %bx, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx86, i32 0, i32 %87
  %88 = load i32, i32* %arrayidx87, align 4, !tbaa !6
  %arrayidx88 = getelementptr inbounds i32, i32* %cond, i32 %88
  %89 = load i32, i32* %arrayidx88, align 4, !tbaa !6
  %90 = load [16 x i32]*, [16 x i32]** %dir.addr, align 4, !tbaa !2
  %91 = load i32, i32* %by, align 4, !tbaa !6
  %arrayidx89 = getelementptr inbounds [16 x i32], [16 x i32]* %90, i32 %91
  %92 = load i32, i32* %bx, align 4, !tbaa !6
  %arrayidx90 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx89, i32 0, i32 %92
  store i32 %89, i32* %arrayidx90, align 4, !tbaa !6
  br label %for.inc91

for.inc91:                                        ; preds = %for.body78
  %93 = load i32, i32* %bi, align 4, !tbaa !6
  %inc92 = add nsw i32 %93, 1
  store i32 %inc92, i32* %bi, align 4, !tbaa !6
  br label %for.cond75

for.end93:                                        ; preds = %for.cond75
  br label %if.end94

if.end94:                                         ; preds = %for.end93, %land.lhs.true71, %if.end68
  %94 = bitcast i32* %bsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #6
  %95 = load i32, i32* %ydec.addr, align 4, !tbaa !6
  %tobool95 = icmp ne i32 %95, 0
  br i1 %tobool95, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end94
  %96 = load i32, i32* %xdec.addr, align 4, !tbaa !6
  %tobool96 = icmp ne i32 %96, 0
  %97 = zext i1 %tobool96 to i64
  %cond97 = select i1 %tobool96, i32 0, i32 2
  br label %cond.end

cond.false:                                       ; preds = %if.end94
  %98 = load i32, i32* %xdec.addr, align 4, !tbaa !6
  %tobool98 = icmp ne i32 %98, 0
  %99 = zext i1 %tobool98 to i64
  %cond99 = select i1 %tobool98, i32 1, i32 3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond100 = phi i32 [ %cond97, %cond.true ], [ %cond99, %cond.false ]
  store i32 %cond100, i32* %bsize, align 4, !tbaa !6
  %100 = bitcast i32* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #6
  %101 = load i32, i32* %pri_strength, align 4, !tbaa !6
  store i32 %101, i32* %t, align 4, !tbaa !6
  %102 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #6
  %103 = load i32, i32* %sec_strength.addr, align 4, !tbaa !6
  store i32 %103, i32* %s, align 4, !tbaa !6
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond101

for.cond101:                                      ; preds = %for.inc177, %cond.end
  %104 = load i32, i32* %bi, align 4, !tbaa !6
  %105 = load i32, i32* %cdef_count.addr, align 4, !tbaa !6
  %cmp102 = icmp slt i32 %104, %105
  br i1 %cmp102, label %for.body104, label %for.end179

for.body104:                                      ; preds = %for.cond101
  %106 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %107 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx105 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %106, i32 %107
  %by106 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx105, i32 0, i32 0
  %108 = load i8, i8* %by106, align 1, !tbaa !11
  %conv107 = zext i8 %108 to i32
  store i32 %conv107, i32* %by, align 4, !tbaa !6
  %109 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %110 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx108 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %109, i32 %110
  %bx109 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx108, i32 0, i32 1
  %111 = load i8, i8* %bx109, align 1, !tbaa !13
  %conv110 = zext i8 %111 to i32
  store i32 %conv110, i32* %bx, align 4, !tbaa !6
  %112 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %tobool111 = icmp ne i8* %112, null
  br i1 %tobool111, label %if.then112, label %if.else

if.then112:                                       ; preds = %for.body104
  %113 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %114 = load i32, i32* %by, align 4, !tbaa !6
  %115 = load i32, i32* %bh_log2, align 4, !tbaa !6
  %shl113 = shl i32 %114, %115
  %116 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %mul114 = mul nsw i32 %shl113, %116
  %117 = load i32, i32* %bx, align 4, !tbaa !6
  %118 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %shl115 = shl i32 %117, %118
  %add116 = add nsw i32 %mul114, %shl115
  %arrayidx117 = getelementptr inbounds i8, i8* %113, i32 %add116
  %119 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %120 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %121 = load i32, i32* %by, align 4, !tbaa !6
  %mul118 = mul nsw i32 %121, 144
  %122 = load i32, i32* %bh_log2, align 4, !tbaa !6
  %shl119 = shl i32 %mul118, %122
  %123 = load i32, i32* %bx, align 4, !tbaa !6
  %124 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %shl120 = shl i32 %123, %124
  %add121 = add nsw i32 %shl119, %shl120
  %arrayidx122 = getelementptr inbounds i16, i16* %120, i32 %add121
  %125 = load i32, i32* %pli.addr, align 4, !tbaa !6
  %tobool123 = icmp ne i32 %125, 0
  br i1 %tobool123, label %cond.true124, label %cond.false125

cond.true124:                                     ; preds = %if.then112
  %126 = load i32, i32* %t, align 4, !tbaa !6
  br label %cond.end129

cond.false125:                                    ; preds = %if.then112
  %127 = load i32, i32* %t, align 4, !tbaa !6
  %128 = load [16 x i32]*, [16 x i32]** %var.addr, align 4, !tbaa !2
  %129 = load i32, i32* %by, align 4, !tbaa !6
  %arrayidx126 = getelementptr inbounds [16 x i32], [16 x i32]* %128, i32 %129
  %130 = load i32, i32* %bx, align 4, !tbaa !6
  %arrayidx127 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx126, i32 0, i32 %130
  %131 = load i32, i32* %arrayidx127, align 4, !tbaa !6
  %call128 = call i32 @adjust_strength(i32 %127, i32 %131)
  br label %cond.end129

cond.end129:                                      ; preds = %cond.false125, %cond.true124
  %cond130 = phi i32 [ %126, %cond.true124 ], [ %call128, %cond.false125 ]
  %132 = load i32, i32* %s, align 4, !tbaa !6
  %133 = load i32, i32* %t, align 4, !tbaa !6
  %tobool131 = icmp ne i32 %133, 0
  br i1 %tobool131, label %cond.true132, label %cond.false135

cond.true132:                                     ; preds = %cond.end129
  %134 = load [16 x i32]*, [16 x i32]** %dir.addr, align 4, !tbaa !2
  %135 = load i32, i32* %by, align 4, !tbaa !6
  %arrayidx133 = getelementptr inbounds [16 x i32], [16 x i32]* %134, i32 %135
  %136 = load i32, i32* %bx, align 4, !tbaa !6
  %arrayidx134 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx133, i32 0, i32 %136
  %137 = load i32, i32* %arrayidx134, align 4, !tbaa !6
  br label %cond.end136

cond.false135:                                    ; preds = %cond.end129
  br label %cond.end136

cond.end136:                                      ; preds = %cond.false135, %cond.true132
  %cond137 = phi i32 [ %137, %cond.true132 ], [ 0, %cond.false135 ]
  %138 = load i32, i32* %damping.addr, align 4, !tbaa !6
  %139 = load i32, i32* %damping.addr, align 4, !tbaa !6
  %140 = load i32, i32* %bsize, align 4, !tbaa !6
  %141 = load i32, i32* %coeff_shift.addr, align 4, !tbaa !6
  call void @cdef_filter_block_c(i8* %arrayidx117, i16* null, i32 %119, i16* %arrayidx122, i32 %cond130, i32 %132, i32 %cond137, i32 %138, i32 %139, i32 %140, i32 %141)
  br label %if.end176

if.else:                                          ; preds = %for.body104
  %142 = load i16*, i16** %dst16.addr, align 4, !tbaa !2
  %143 = load i32*, i32** %dirinit.addr, align 4, !tbaa !2
  %tobool138 = icmp ne i32* %143, null
  br i1 %tobool138, label %cond.true139, label %cond.false142

cond.true139:                                     ; preds = %if.else
  %144 = load i32, i32* %bi, align 4, !tbaa !6
  %145 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %146 = load i32, i32* %bh_log2, align 4, !tbaa !6
  %add140 = add nsw i32 %145, %146
  %shl141 = shl i32 %144, %add140
  br label %cond.end147

cond.false142:                                    ; preds = %if.else
  %147 = load i32, i32* %by, align 4, !tbaa !6
  %148 = load i32, i32* %bh_log2, align 4, !tbaa !6
  %shl143 = shl i32 %147, %148
  %149 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %mul144 = mul nsw i32 %shl143, %149
  %150 = load i32, i32* %bx, align 4, !tbaa !6
  %151 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %shl145 = shl i32 %150, %151
  %add146 = add nsw i32 %mul144, %shl145
  br label %cond.end147

cond.end147:                                      ; preds = %cond.false142, %cond.true139
  %cond148 = phi i32 [ %shl141, %cond.true139 ], [ %add146, %cond.false142 ]
  %arrayidx149 = getelementptr inbounds i16, i16* %142, i32 %cond148
  %152 = load i32*, i32** %dirinit.addr, align 4, !tbaa !2
  %tobool150 = icmp ne i32* %152, null
  br i1 %tobool150, label %cond.true151, label %cond.false153

cond.true151:                                     ; preds = %cond.end147
  %153 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %shl152 = shl i32 1, %153
  br label %cond.end154

cond.false153:                                    ; preds = %cond.end147
  %154 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  br label %cond.end154

cond.end154:                                      ; preds = %cond.false153, %cond.true151
  %cond155 = phi i32 [ %shl152, %cond.true151 ], [ %154, %cond.false153 ]
  %155 = load i16*, i16** %in.addr, align 4, !tbaa !2
  %156 = load i32, i32* %by, align 4, !tbaa !6
  %mul156 = mul nsw i32 %156, 144
  %157 = load i32, i32* %bh_log2, align 4, !tbaa !6
  %shl157 = shl i32 %mul156, %157
  %158 = load i32, i32* %bx, align 4, !tbaa !6
  %159 = load i32, i32* %bw_log2, align 4, !tbaa !6
  %shl158 = shl i32 %158, %159
  %add159 = add nsw i32 %shl157, %shl158
  %arrayidx160 = getelementptr inbounds i16, i16* %155, i32 %add159
  %160 = load i32, i32* %pli.addr, align 4, !tbaa !6
  %tobool161 = icmp ne i32 %160, 0
  br i1 %tobool161, label %cond.true162, label %cond.false163

cond.true162:                                     ; preds = %cond.end154
  %161 = load i32, i32* %t, align 4, !tbaa !6
  br label %cond.end167

cond.false163:                                    ; preds = %cond.end154
  %162 = load i32, i32* %t, align 4, !tbaa !6
  %163 = load [16 x i32]*, [16 x i32]** %var.addr, align 4, !tbaa !2
  %164 = load i32, i32* %by, align 4, !tbaa !6
  %arrayidx164 = getelementptr inbounds [16 x i32], [16 x i32]* %163, i32 %164
  %165 = load i32, i32* %bx, align 4, !tbaa !6
  %arrayidx165 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx164, i32 0, i32 %165
  %166 = load i32, i32* %arrayidx165, align 4, !tbaa !6
  %call166 = call i32 @adjust_strength(i32 %162, i32 %166)
  br label %cond.end167

cond.end167:                                      ; preds = %cond.false163, %cond.true162
  %cond168 = phi i32 [ %161, %cond.true162 ], [ %call166, %cond.false163 ]
  %167 = load i32, i32* %s, align 4, !tbaa !6
  %168 = load i32, i32* %t, align 4, !tbaa !6
  %tobool169 = icmp ne i32 %168, 0
  br i1 %tobool169, label %cond.true170, label %cond.false173

cond.true170:                                     ; preds = %cond.end167
  %169 = load [16 x i32]*, [16 x i32]** %dir.addr, align 4, !tbaa !2
  %170 = load i32, i32* %by, align 4, !tbaa !6
  %arrayidx171 = getelementptr inbounds [16 x i32], [16 x i32]* %169, i32 %170
  %171 = load i32, i32* %bx, align 4, !tbaa !6
  %arrayidx172 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx171, i32 0, i32 %171
  %172 = load i32, i32* %arrayidx172, align 4, !tbaa !6
  br label %cond.end174

cond.false173:                                    ; preds = %cond.end167
  br label %cond.end174

cond.end174:                                      ; preds = %cond.false173, %cond.true170
  %cond175 = phi i32 [ %172, %cond.true170 ], [ 0, %cond.false173 ]
  %173 = load i32, i32* %damping.addr, align 4, !tbaa !6
  %174 = load i32, i32* %damping.addr, align 4, !tbaa !6
  %175 = load i32, i32* %bsize, align 4, !tbaa !6
  %176 = load i32, i32* %coeff_shift.addr, align 4, !tbaa !6
  call void @cdef_filter_block_c(i8* null, i16* %arrayidx149, i32 %cond155, i16* %arrayidx160, i32 %cond168, i32 %167, i32 %cond175, i32 %173, i32 %174, i32 %175, i32 %176)
  br label %if.end176

if.end176:                                        ; preds = %cond.end174, %cond.end136
  br label %for.inc177

for.inc177:                                       ; preds = %if.end176
  %177 = load i32, i32* %bi, align 4, !tbaa !6
  %inc178 = add nsw i32 %177, 1
  store i32 %inc178, i32* %bi, align 4, !tbaa !6
  br label %for.cond101

for.end179:                                       ; preds = %for.cond101
  %178 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #6
  %179 = bitcast i32* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #6
  %180 = bitcast i32* %bsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end179, %for.end35
  %181 = bitcast i32* %bh_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #6
  %182 = bitcast i32* %bw_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #6
  %183 = bitcast i32* %pri_strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #6
  %184 = bitcast i32* %by to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #6
  %185 = bitcast i32* %bx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #6
  %186 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint nounwind
define internal i32 @adjust_strength(i32 %strength, i32 %var) #3 {
entry:
  %strength.addr = alloca i32, align 4
  %var.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %strength, i32* %strength.addr, align 4, !tbaa !6
  store i32 %var, i32* %var.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %var.addr, align 4, !tbaa !6
  %shr = ashr i32 %1, 6
  %tobool = icmp ne i32 %shr, 0
  br i1 %tobool, label %cond.true, label %cond.false5

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %var.addr, align 4, !tbaa !6
  %shr1 = ashr i32 %2, 6
  %call = call i32 @get_msb(i32 %shr1)
  %cmp = icmp slt i32 %call, 12
  br i1 %cmp, label %cond.true2, label %cond.false

cond.true2:                                       ; preds = %cond.true
  %3 = load i32, i32* %var.addr, align 4, !tbaa !6
  %shr3 = ashr i32 %3, 6
  %call4 = call i32 @get_msb(i32 %shr3)
  br label %cond.end

cond.false:                                       ; preds = %cond.true
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true2
  %cond = phi i32 [ %call4, %cond.true2 ], [ 12, %cond.false ]
  br label %cond.end6

cond.false5:                                      ; preds = %entry
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.end
  %cond7 = phi i32 [ %cond, %cond.end ], [ 0, %cond.false5 ]
  store i32 %cond7, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %var.addr, align 4, !tbaa !6
  %tobool8 = icmp ne i32 %4, 0
  br i1 %tobool8, label %cond.true9, label %cond.false12

cond.true9:                                       ; preds = %cond.end6
  %5 = load i32, i32* %strength.addr, align 4, !tbaa !6
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 4, %6
  %mul = mul nsw i32 %5, %add
  %add10 = add nsw i32 %mul, 8
  %shr11 = ashr i32 %add10, 4
  br label %cond.end13

cond.false12:                                     ; preds = %cond.end6
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false12, %cond.true9
  %cond14 = phi i32 [ %shr11, %cond.true9 ], [ 0, %cond.false12 ]
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret i32 %cond14
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #3 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: inlinehint nounwind
define internal i32 @sign(i32 %i) #3 {
entry:
  %i.addr = alloca i32, align 4
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, 0
  %1 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 -1, i32 1
  ret i32 %cond
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #5

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind }
attributes #7 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"short", !4, i64 0}
!10 = !{!4, !4, i64 0}
!11 = !{!12, !4, i64 0}
!12 = !{!"", !4, i64 0, !4, i64 1}
!13 = !{!12, !4, i64 1}
