; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/entdec.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/entdec.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.od_ec_dec = type { i8*, i32, i8*, i8*, i32, i16, i16 }

; Function Attrs: nounwind
define hidden void @od_ec_dec_init(%struct.od_ec_dec* nonnull %dec, i8* nonnull %buf, i32 %storage) #0 {
entry:
  %dec.addr = alloca %struct.od_ec_dec*, align 4
  %buf.addr = alloca i8*, align 4
  %storage.addr = alloca i32, align 4
  store %struct.od_ec_dec* %dec, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  store i8* %buf, i8** %buf.addr, align 4, !tbaa !2
  store i32 %storage, i32* %storage.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %1 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %buf1 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %1, i32 0, i32 0
  store i8* %0, i8** %buf1, align 4, !tbaa !8
  %2 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %tell_offs = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %2, i32 0, i32 1
  store i32 -14, i32* %tell_offs, align 4, !tbaa !11
  %3 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %storage.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %4
  %5 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %end = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %5, i32 0, i32 2
  store i8* %add.ptr, i8** %end, align 4, !tbaa !12
  %6 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %7 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %bptr = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %7, i32 0, i32 3
  store i8* %6, i8** %bptr, align 4, !tbaa !13
  %8 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %dif = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %8, i32 0, i32 4
  store i32 2147483647, i32* %dif, align 4, !tbaa !14
  %9 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %rng = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %9, i32 0, i32 5
  store i16 -32768, i16* %rng, align 4, !tbaa !15
  %10 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %10, i32 0, i32 6
  store i16 -15, i16* %cnt, align 2, !tbaa !16
  %11 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  call void @od_ec_dec_refill(%struct.od_ec_dec* %11)
  ret void
}

; Function Attrs: nounwind
define internal void @od_ec_dec_refill(%struct.od_ec_dec* %dec) #0 {
entry:
  %dec.addr = alloca %struct.od_ec_dec*, align 4
  %s = alloca i32, align 4
  %dif = alloca i32, align 4
  %cnt = alloca i16, align 2
  %bptr = alloca i8*, align 4
  %end = alloca i8*, align 4
  store %struct.od_ec_dec* %dec, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %0 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %dif to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i16* %cnt to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %2) #5
  %3 = bitcast i8** %bptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i8** %end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %dif1 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %5, i32 0, i32 4
  %6 = load i32, i32* %dif1, align 4, !tbaa !14
  store i32 %6, i32* %dif, align 4, !tbaa !6
  %7 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %cnt2 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %7, i32 0, i32 6
  %8 = load i16, i16* %cnt2, align 2, !tbaa !16
  store i16 %8, i16* %cnt, align 2, !tbaa !17
  %9 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %bptr3 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %9, i32 0, i32 3
  %10 = load i8*, i8** %bptr3, align 4, !tbaa !13
  store i8* %10, i8** %bptr, align 4, !tbaa !2
  %11 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %end4 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %11, i32 0, i32 2
  %12 = load i8*, i8** %end4, align 4, !tbaa !12
  store i8* %12, i8** %end, align 4, !tbaa !2
  %13 = load i16, i16* %cnt, align 2, !tbaa !17
  %conv = sext i16 %13 to i32
  %add = add nsw i32 %conv, 15
  %sub = sub nsw i32 23, %add
  store i32 %sub, i32* %s, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %s, align 4, !tbaa !6
  %cmp = icmp sge i32 %14, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %15 = load i8*, i8** %bptr, align 4, !tbaa !2
  %16 = load i8*, i8** %end, align 4, !tbaa !2
  %cmp6 = icmp ult i8* %15, %16
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %17 = phi i1 [ false, %for.cond ], [ %cmp6, %land.rhs ]
  br i1 %17, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %18 = load i8*, i8** %bptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %18, i32 0
  %19 = load i8, i8* %arrayidx, align 1, !tbaa !18
  %conv8 = zext i8 %19 to i32
  %20 = load i32, i32* %s, align 4, !tbaa !6
  %shl = shl i32 %conv8, %20
  %21 = load i32, i32* %dif, align 4, !tbaa !6
  %xor = xor i32 %21, %shl
  store i32 %xor, i32* %dif, align 4, !tbaa !6
  %22 = load i16, i16* %cnt, align 2, !tbaa !17
  %conv9 = sext i16 %22 to i32
  %add10 = add nsw i32 %conv9, 8
  %conv11 = trunc i32 %add10 to i16
  store i16 %conv11, i16* %cnt, align 2, !tbaa !17
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %s, align 4, !tbaa !6
  %sub12 = sub nsw i32 %23, 8
  store i32 %sub12, i32* %s, align 4, !tbaa !6
  %24 = load i8*, i8** %bptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %24, i32 1
  store i8* %incdec.ptr, i8** %bptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %land.end
  %25 = load i8*, i8** %bptr, align 4, !tbaa !2
  %26 = load i8*, i8** %end, align 4, !tbaa !2
  %cmp13 = icmp uge i8* %25, %26
  br i1 %cmp13, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %27 = load i16, i16* %cnt, align 2, !tbaa !17
  %conv15 = sext i16 %27 to i32
  %sub16 = sub nsw i32 16384, %conv15
  %28 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %tell_offs = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %28, i32 0, i32 1
  %29 = load i32, i32* %tell_offs, align 4, !tbaa !11
  %add17 = add nsw i32 %29, %sub16
  store i32 %add17, i32* %tell_offs, align 4, !tbaa !11
  store i16 16384, i16* %cnt, align 2, !tbaa !17
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %30 = load i32, i32* %dif, align 4, !tbaa !6
  %31 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %dif18 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %31, i32 0, i32 4
  store i32 %30, i32* %dif18, align 4, !tbaa !14
  %32 = load i16, i16* %cnt, align 2, !tbaa !17
  %33 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %cnt19 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %33, i32 0, i32 6
  store i16 %32, i16* %cnt19, align 2, !tbaa !16
  %34 = load i8*, i8** %bptr, align 4, !tbaa !2
  %35 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %bptr20 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %35, i32 0, i32 3
  store i8* %34, i8** %bptr20, align 4, !tbaa !13
  %36 = bitcast i8** %end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  %37 = bitcast i8** %bptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  %38 = bitcast i16* %cnt to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %38) #5
  %39 = bitcast i32* %dif to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #5
  %40 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #5
  ret void
}

; Function Attrs: nounwind
define hidden i32 @od_ec_decode_bool_q15(%struct.od_ec_dec* nonnull %dec, i32 %f) #0 {
entry:
  %dec.addr = alloca %struct.od_ec_dec*, align 4
  %f.addr = alloca i32, align 4
  %dif = alloca i32, align 4
  %vw = alloca i32, align 4
  %r = alloca i32, align 4
  %r_new = alloca i32, align 4
  %v = alloca i32, align 4
  %ret = alloca i32, align 4
  store %struct.od_ec_dec* %dec, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  store i32 %f, i32* %f.addr, align 4, !tbaa !6
  %0 = bitcast i32* %dif to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %vw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %r_new to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %dif1 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %6, i32 0, i32 4
  %7 = load i32, i32* %dif1, align 4, !tbaa !14
  store i32 %7, i32* %dif, align 4, !tbaa !6
  %8 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %rng = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %8, i32 0, i32 5
  %9 = load i16, i16* %rng, align 4, !tbaa !15
  %conv = zext i16 %9 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !6
  %10 = load i32, i32* %r, align 4, !tbaa !6
  %shr = lshr i32 %10, 8
  %11 = load i32, i32* %f.addr, align 4, !tbaa !6
  %shr2 = lshr i32 %11, 6
  %mul = mul i32 %shr, %shr2
  %shr3 = lshr i32 %mul, 1
  store i32 %shr3, i32* %v, align 4, !tbaa !6
  %12 = load i32, i32* %v, align 4, !tbaa !6
  %add = add i32 %12, 4
  store i32 %add, i32* %v, align 4, !tbaa !6
  %13 = load i32, i32* %v, align 4, !tbaa !6
  %shl = shl i32 %13, 16
  store i32 %shl, i32* %vw, align 4, !tbaa !6
  store i32 1, i32* %ret, align 4, !tbaa !6
  %14 = load i32, i32* %v, align 4, !tbaa !6
  store i32 %14, i32* %r_new, align 4, !tbaa !6
  %15 = load i32, i32* %dif, align 4, !tbaa !6
  %16 = load i32, i32* %vw, align 4, !tbaa !6
  %cmp = icmp uge i32 %15, %16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %17 = load i32, i32* %r, align 4, !tbaa !6
  %18 = load i32, i32* %v, align 4, !tbaa !6
  %sub = sub i32 %17, %18
  store i32 %sub, i32* %r_new, align 4, !tbaa !6
  %19 = load i32, i32* %vw, align 4, !tbaa !6
  %20 = load i32, i32* %dif, align 4, !tbaa !6
  %sub5 = sub i32 %20, %19
  store i32 %sub5, i32* %dif, align 4, !tbaa !6
  store i32 0, i32* %ret, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %21 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %22 = load i32, i32* %dif, align 4, !tbaa !6
  %23 = load i32, i32* %r_new, align 4, !tbaa !6
  %24 = load i32, i32* %ret, align 4, !tbaa !6
  %call = call i32 @od_ec_dec_normalize(%struct.od_ec_dec* %21, i32 %22, i32 %23, i32 %24)
  %25 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast i32* %r_new to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %28 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i32* %vw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  %30 = bitcast i32* %dif to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  ret i32 %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @od_ec_dec_normalize(%struct.od_ec_dec* %dec, i32 %dif, i32 %rng, i32 %ret) #0 {
entry:
  %dec.addr = alloca %struct.od_ec_dec*, align 4
  %dif.addr = alloca i32, align 4
  %rng.addr = alloca i32, align 4
  %ret.addr = alloca i32, align 4
  %d = alloca i32, align 4
  store %struct.od_ec_dec* %dec, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  store i32 %dif, i32* %dif.addr, align 4, !tbaa !6
  store i32 %rng, i32* %rng.addr, align 4, !tbaa !6
  store i32 %ret, i32* %ret.addr, align 4, !tbaa !6
  %0 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %rng.addr, align 4, !tbaa !6
  %call = call i32 @get_msb(i32 %1)
  %add = add nsw i32 1, %call
  %sub = sub nsw i32 16, %add
  store i32 %sub, i32* %d, align 4, !tbaa !6
  %2 = load i32, i32* %d, align 4, !tbaa !6
  %3 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %3, i32 0, i32 6
  %4 = load i16, i16* %cnt, align 2, !tbaa !16
  %conv = sext i16 %4 to i32
  %sub1 = sub nsw i32 %conv, %2
  %conv2 = trunc i32 %sub1 to i16
  store i16 %conv2, i16* %cnt, align 2, !tbaa !16
  %5 = load i32, i32* %dif.addr, align 4, !tbaa !6
  %add3 = add i32 %5, 1
  %6 = load i32, i32* %d, align 4, !tbaa !6
  %shl = shl i32 %add3, %6
  %sub4 = sub i32 %shl, 1
  %7 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %dif5 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %7, i32 0, i32 4
  store i32 %sub4, i32* %dif5, align 4, !tbaa !14
  %8 = load i32, i32* %rng.addr, align 4, !tbaa !6
  %9 = load i32, i32* %d, align 4, !tbaa !6
  %shl6 = shl i32 %8, %9
  %conv7 = trunc i32 %shl6 to i16
  %10 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %rng8 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %10, i32 0, i32 5
  store i16 %conv7, i16* %rng8, align 4, !tbaa !15
  %11 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %cnt9 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %11, i32 0, i32 6
  %12 = load i16, i16* %cnt9, align 2, !tbaa !16
  %conv10 = sext i16 %12 to i32
  %cmp = icmp slt i32 %conv10, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  call void @od_ec_dec_refill(%struct.od_ec_dec* %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load i32, i32* %ret.addr, align 4, !tbaa !6
  %15 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  ret i32 %14
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @od_ec_decode_cdf_q15(%struct.od_ec_dec* nonnull %dec, i16* nonnull %icdf, i32 %nsyms) #0 {
entry:
  %dec.addr = alloca %struct.od_ec_dec*, align 4
  %icdf.addr = alloca i16*, align 4
  %nsyms.addr = alloca i32, align 4
  %dif = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %u = alloca i32, align 4
  %v = alloca i32, align 4
  %ret = alloca i32, align 4
  %N = alloca i32, align 4
  store %struct.od_ec_dec* %dec, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  store i16* %icdf, i16** %icdf.addr, align 4, !tbaa !2
  store i32 %nsyms, i32* %nsyms.addr, align 4, !tbaa !6
  %0 = bitcast i32* %dif to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %u to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %nsyms.addr, align 4, !tbaa !6
  %7 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %dif1 = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %7, i32 0, i32 4
  %8 = load i32, i32* %dif1, align 4, !tbaa !14
  store i32 %8, i32* %dif, align 4, !tbaa !6
  %9 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %rng = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %9, i32 0, i32 5
  %10 = load i16, i16* %rng, align 4, !tbaa !15
  %conv = zext i16 %10 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !6
  %11 = bitcast i32* %N to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %nsyms.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %12, 1
  store i32 %sub, i32* %N, align 4, !tbaa !6
  %13 = load i32, i32* %dif, align 4, !tbaa !6
  %shr = lshr i32 %13, 16
  store i32 %shr, i32* %c, align 4, !tbaa !6
  %14 = load i32, i32* %r, align 4, !tbaa !6
  store i32 %14, i32* %v, align 4, !tbaa !6
  store i32 -1, i32* %ret, align 4, !tbaa !6
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %15 = load i32, i32* %v, align 4, !tbaa !6
  store i32 %15, i32* %u, align 4, !tbaa !6
  %16 = load i32, i32* %r, align 4, !tbaa !6
  %shr2 = lshr i32 %16, 8
  %17 = load i16*, i16** %icdf.addr, align 4, !tbaa !2
  %18 = load i32, i32* %ret, align 4, !tbaa !6
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %ret, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %17, i32 %inc
  %19 = load i16, i16* %arrayidx, align 2, !tbaa !17
  %conv3 = zext i16 %19 to i32
  %shr4 = ashr i32 %conv3, 6
  %mul = mul i32 %shr2, %shr4
  %shr5 = lshr i32 %mul, 1
  store i32 %shr5, i32* %v, align 4, !tbaa !6
  %20 = load i32, i32* %N, align 4, !tbaa !6
  %21 = load i32, i32* %ret, align 4, !tbaa !6
  %sub6 = sub nsw i32 %20, %21
  %mul7 = mul nsw i32 4, %sub6
  %22 = load i32, i32* %v, align 4, !tbaa !6
  %add = add i32 %22, %mul7
  store i32 %add, i32* %v, align 4, !tbaa !6
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %23 = load i32, i32* %c, align 4, !tbaa !6
  %24 = load i32, i32* %v, align 4, !tbaa !6
  %cmp = icmp ult i32 %23, %24
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %25 = load i32, i32* %u, align 4, !tbaa !6
  %26 = load i32, i32* %v, align 4, !tbaa !6
  %sub9 = sub i32 %25, %26
  store i32 %sub9, i32* %r, align 4, !tbaa !6
  %27 = load i32, i32* %v, align 4, !tbaa !6
  %shl = shl i32 %27, 16
  %28 = load i32, i32* %dif, align 4, !tbaa !6
  %sub10 = sub i32 %28, %shl
  store i32 %sub10, i32* %dif, align 4, !tbaa !6
  %29 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %30 = load i32, i32* %dif, align 4, !tbaa !6
  %31 = load i32, i32* %r, align 4, !tbaa !6
  %32 = load i32, i32* %ret, align 4, !tbaa !6
  %call = call i32 @od_ec_dec_normalize(%struct.od_ec_dec* %29, i32 %30, i32 %31, i32 %32)
  %33 = bitcast i32* %N to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  %34 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  %35 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  %36 = bitcast i32* %u to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  %37 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  %38 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #5
  %39 = bitcast i32* %dif to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #5
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @od_ec_dec_tell(%struct.od_ec_dec* nonnull %dec) #0 {
entry:
  %dec.addr = alloca %struct.od_ec_dec*, align 4
  store %struct.od_ec_dec* %dec, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %0 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %bptr = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %0, i32 0, i32 3
  %1 = load i8*, i8** %bptr, align 4, !tbaa !13
  %2 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %2, i32 0, i32 0
  %3 = load i8*, i8** %buf, align 4, !tbaa !8
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %mul = mul nsw i32 %sub.ptr.sub, 8
  %4 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %4, i32 0, i32 6
  %5 = load i16, i16* %cnt, align 2, !tbaa !16
  %conv = sext i16 %5 to i32
  %sub = sub nsw i32 %mul, %conv
  %6 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %tell_offs = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %6, i32 0, i32 1
  %7 = load i32, i32* %tell_offs, align 4, !tbaa !11
  %add = add nsw i32 %sub, %7
  ret i32 %add
}

; Function Attrs: nounwind
define hidden i32 @od_ec_dec_tell_frac(%struct.od_ec_dec* nonnull %dec) #0 {
entry:
  %dec.addr = alloca %struct.od_ec_dec*, align 4
  store %struct.od_ec_dec* %dec, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %0 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %call = call i32 @od_ec_dec_tell(%struct.od_ec_dec* %0)
  %1 = load %struct.od_ec_dec*, %struct.od_ec_dec** %dec.addr, align 4, !tbaa !2
  %rng = getelementptr inbounds %struct.od_ec_dec, %struct.od_ec_dec* %1, i32 0, i32 5
  %2 = load i16, i16* %rng, align 4, !tbaa !15
  %conv = zext i16 %2 to i32
  %call1 = call i32 @od_ec_tell_frac(i32 %call, i32 %conv)
  ret i32 %call1
}

declare i32 @od_ec_tell_frac(i32, i32) #2

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #3 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #4

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 0}
!9 = !{!"od_ec_dec", !3, i64 0, !7, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !10, i64 20, !10, i64 22}
!10 = !{!"short", !4, i64 0}
!11 = !{!9, !7, i64 4}
!12 = !{!9, !3, i64 8}
!13 = !{!9, !3, i64 12}
!14 = !{!9, !7, i64 16}
!15 = !{!9, !10, i64 20}
!16 = !{!9, !10, i64 22}
!17 = !{!10, !10, i64 0}
!18 = !{!4, !4, i64 0}
