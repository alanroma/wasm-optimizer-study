; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/warped_motion.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/warped_motion.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }

@av1_warped_filter = hidden constant [193 x [8 x i16]] [[8 x i16] [i16 0, i16 0, i16 127, i16 1, i16 0, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 -1, i16 127, i16 2, i16 0, i16 0, i16 0, i16 0], [8 x i16] [i16 1, i16 -3, i16 127, i16 4, i16 -1, i16 0, i16 0, i16 0], [8 x i16] [i16 1, i16 -4, i16 126, i16 6, i16 -2, i16 1, i16 0, i16 0], [8 x i16] [i16 1, i16 -5, i16 126, i16 8, i16 -3, i16 1, i16 0, i16 0], [8 x i16] [i16 1, i16 -6, i16 125, i16 11, i16 -4, i16 1, i16 0, i16 0], [8 x i16] [i16 1, i16 -7, i16 124, i16 13, i16 -4, i16 1, i16 0, i16 0], [8 x i16] [i16 2, i16 -8, i16 123, i16 15, i16 -5, i16 1, i16 0, i16 0], [8 x i16] [i16 2, i16 -9, i16 122, i16 18, i16 -6, i16 1, i16 0, i16 0], [8 x i16] [i16 2, i16 -10, i16 121, i16 20, i16 -6, i16 1, i16 0, i16 0], [8 x i16] [i16 2, i16 -11, i16 120, i16 22, i16 -7, i16 2, i16 0, i16 0], [8 x i16] [i16 2, i16 -12, i16 119, i16 25, i16 -8, i16 2, i16 0, i16 0], [8 x i16] [i16 3, i16 -13, i16 117, i16 27, i16 -8, i16 2, i16 0, i16 0], [8 x i16] [i16 3, i16 -13, i16 116, i16 29, i16 -9, i16 2, i16 0, i16 0], [8 x i16] [i16 3, i16 -14, i16 114, i16 32, i16 -10, i16 3, i16 0, i16 0], [8 x i16] [i16 3, i16 -15, i16 113, i16 35, i16 -10, i16 2, i16 0, i16 0], [8 x i16] [i16 3, i16 -15, i16 111, i16 37, i16 -11, i16 3, i16 0, i16 0], [8 x i16] [i16 3, i16 -16, i16 109, i16 40, i16 -11, i16 3, i16 0, i16 0], [8 x i16] [i16 3, i16 -16, i16 108, i16 42, i16 -12, i16 3, i16 0, i16 0], [8 x i16] [i16 4, i16 -17, i16 106, i16 45, i16 -13, i16 3, i16 0, i16 0], [8 x i16] [i16 4, i16 -17, i16 104, i16 47, i16 -13, i16 3, i16 0, i16 0], [8 x i16] [i16 4, i16 -17, i16 102, i16 50, i16 -14, i16 3, i16 0, i16 0], [8 x i16] [i16 4, i16 -17, i16 100, i16 52, i16 -14, i16 3, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 98, i16 55, i16 -15, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 96, i16 58, i16 -15, i16 3, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 94, i16 60, i16 -16, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 91, i16 63, i16 -16, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 89, i16 65, i16 -16, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 87, i16 68, i16 -17, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 85, i16 70, i16 -17, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 82, i16 73, i16 -17, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 80, i16 75, i16 -17, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -18, i16 78, i16 78, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -17, i16 75, i16 80, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -17, i16 73, i16 82, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -17, i16 70, i16 85, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -17, i16 68, i16 87, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -16, i16 65, i16 89, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -16, i16 63, i16 91, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -16, i16 60, i16 94, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 3, i16 -15, i16 58, i16 96, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 4, i16 -15, i16 55, i16 98, i16 -18, i16 4, i16 0, i16 0], [8 x i16] [i16 3, i16 -14, i16 52, i16 100, i16 -17, i16 4, i16 0, i16 0], [8 x i16] [i16 3, i16 -14, i16 50, i16 102, i16 -17, i16 4, i16 0, i16 0], [8 x i16] [i16 3, i16 -13, i16 47, i16 104, i16 -17, i16 4, i16 0, i16 0], [8 x i16] [i16 3, i16 -13, i16 45, i16 106, i16 -17, i16 4, i16 0, i16 0], [8 x i16] [i16 3, i16 -12, i16 42, i16 108, i16 -16, i16 3, i16 0, i16 0], [8 x i16] [i16 3, i16 -11, i16 40, i16 109, i16 -16, i16 3, i16 0, i16 0], [8 x i16] [i16 3, i16 -11, i16 37, i16 111, i16 -15, i16 3, i16 0, i16 0], [8 x i16] [i16 2, i16 -10, i16 35, i16 113, i16 -15, i16 3, i16 0, i16 0], [8 x i16] [i16 3, i16 -10, i16 32, i16 114, i16 -14, i16 3, i16 0, i16 0], [8 x i16] [i16 2, i16 -9, i16 29, i16 116, i16 -13, i16 3, i16 0, i16 0], [8 x i16] [i16 2, i16 -8, i16 27, i16 117, i16 -13, i16 3, i16 0, i16 0], [8 x i16] [i16 2, i16 -8, i16 25, i16 119, i16 -12, i16 2, i16 0, i16 0], [8 x i16] [i16 2, i16 -7, i16 22, i16 120, i16 -11, i16 2, i16 0, i16 0], [8 x i16] [i16 1, i16 -6, i16 20, i16 121, i16 -10, i16 2, i16 0, i16 0], [8 x i16] [i16 1, i16 -6, i16 18, i16 122, i16 -9, i16 2, i16 0, i16 0], [8 x i16] [i16 1, i16 -5, i16 15, i16 123, i16 -8, i16 2, i16 0, i16 0], [8 x i16] [i16 1, i16 -4, i16 13, i16 124, i16 -7, i16 1, i16 0, i16 0], [8 x i16] [i16 1, i16 -4, i16 11, i16 125, i16 -6, i16 1, i16 0, i16 0], [8 x i16] [i16 1, i16 -3, i16 8, i16 126, i16 -5, i16 1, i16 0, i16 0], [8 x i16] [i16 1, i16 -2, i16 6, i16 126, i16 -4, i16 1, i16 0, i16 0], [8 x i16] [i16 0, i16 -1, i16 4, i16 127, i16 -3, i16 1, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 2, i16 127, i16 -1, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 127, i16 1, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -1, i16 127, i16 2, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 1, i16 -3, i16 127, i16 4, i16 -2, i16 1, i16 0], [8 x i16] [i16 0, i16 1, i16 -5, i16 127, i16 6, i16 -2, i16 1, i16 0], [8 x i16] [i16 0, i16 2, i16 -6, i16 126, i16 8, i16 -3, i16 1, i16 0], [8 x i16] [i16 -1, i16 2, i16 -7, i16 126, i16 11, i16 -4, i16 2, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -8, i16 125, i16 13, i16 -5, i16 2, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -10, i16 124, i16 16, i16 -6, i16 3, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -11, i16 123, i16 18, i16 -7, i16 3, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -12, i16 122, i16 20, i16 -7, i16 3, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -13, i16 121, i16 23, i16 -8, i16 3, i16 -1], [8 x i16] [i16 -2, i16 5, i16 -14, i16 120, i16 25, i16 -9, i16 4, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -15, i16 119, i16 27, i16 -10, i16 4, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -16, i16 118, i16 30, i16 -11, i16 4, i16 -1], [8 x i16] [i16 -2, i16 6, i16 -17, i16 116, i16 33, i16 -12, i16 5, i16 -1], [8 x i16] [i16 -2, i16 6, i16 -17, i16 114, i16 35, i16 -12, i16 5, i16 -1], [8 x i16] [i16 -2, i16 6, i16 -18, i16 113, i16 38, i16 -13, i16 5, i16 -1], [8 x i16] [i16 -2, i16 7, i16 -19, i16 111, i16 41, i16 -14, i16 6, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -19, i16 110, i16 43, i16 -15, i16 6, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -20, i16 108, i16 46, i16 -15, i16 6, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -20, i16 106, i16 49, i16 -16, i16 6, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -21, i16 104, i16 51, i16 -16, i16 7, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -21, i16 102, i16 54, i16 -17, i16 7, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -21, i16 100, i16 56, i16 -18, i16 7, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -22, i16 98, i16 59, i16 -18, i16 7, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -22, i16 96, i16 62, i16 -19, i16 7, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -22, i16 94, i16 64, i16 -19, i16 7, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -22, i16 91, i16 67, i16 -20, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -22, i16 89, i16 69, i16 -20, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -22, i16 87, i16 72, i16 -21, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -21, i16 84, i16 74, i16 -21, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -22, i16 82, i16 77, i16 -21, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -21, i16 79, i16 79, i16 -21, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -21, i16 77, i16 82, i16 -22, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -21, i16 74, i16 84, i16 -21, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -21, i16 72, i16 87, i16 -22, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -20, i16 69, i16 89, i16 -22, i16 8, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -20, i16 67, i16 91, i16 -22, i16 8, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -19, i16 64, i16 94, i16 -22, i16 8, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -19, i16 62, i16 96, i16 -22, i16 8, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -18, i16 59, i16 98, i16 -22, i16 8, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -18, i16 56, i16 100, i16 -21, i16 8, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -17, i16 54, i16 102, i16 -21, i16 7, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -16, i16 51, i16 104, i16 -21, i16 7, i16 -2], [8 x i16] [i16 -2, i16 6, i16 -16, i16 49, i16 106, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -2, i16 6, i16 -15, i16 46, i16 108, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -2, i16 6, i16 -15, i16 43, i16 110, i16 -19, i16 7, i16 -2], [8 x i16] [i16 -2, i16 6, i16 -14, i16 41, i16 111, i16 -19, i16 7, i16 -2], [8 x i16] [i16 -1, i16 5, i16 -13, i16 38, i16 113, i16 -18, i16 6, i16 -2], [8 x i16] [i16 -1, i16 5, i16 -12, i16 35, i16 114, i16 -17, i16 6, i16 -2], [8 x i16] [i16 -1, i16 5, i16 -12, i16 33, i16 116, i16 -17, i16 6, i16 -2], [8 x i16] [i16 -1, i16 4, i16 -11, i16 30, i16 118, i16 -16, i16 5, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -10, i16 27, i16 119, i16 -15, i16 5, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -9, i16 25, i16 120, i16 -14, i16 5, i16 -2], [8 x i16] [i16 -1, i16 3, i16 -8, i16 23, i16 121, i16 -13, i16 4, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -7, i16 20, i16 122, i16 -12, i16 4, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -7, i16 18, i16 123, i16 -11, i16 4, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -6, i16 16, i16 124, i16 -10, i16 3, i16 -1], [8 x i16] [i16 -1, i16 2, i16 -5, i16 13, i16 125, i16 -8, i16 3, i16 -1], [8 x i16] [i16 -1, i16 2, i16 -4, i16 11, i16 126, i16 -7, i16 2, i16 -1], [8 x i16] [i16 0, i16 1, i16 -3, i16 8, i16 126, i16 -6, i16 2, i16 0], [8 x i16] [i16 0, i16 1, i16 -2, i16 6, i16 127, i16 -5, i16 1, i16 0], [8 x i16] [i16 0, i16 1, i16 -2, i16 4, i16 127, i16 -3, i16 1, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 2, i16 127, i16 -1, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 1, i16 127, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 -1, i16 127, i16 2, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 1, i16 -3, i16 127, i16 4, i16 -1, i16 0], [8 x i16] [i16 0, i16 0, i16 1, i16 -4, i16 126, i16 6, i16 -2, i16 1], [8 x i16] [i16 0, i16 0, i16 1, i16 -5, i16 126, i16 8, i16 -3, i16 1], [8 x i16] [i16 0, i16 0, i16 1, i16 -6, i16 125, i16 11, i16 -4, i16 1], [8 x i16] [i16 0, i16 0, i16 1, i16 -7, i16 124, i16 13, i16 -4, i16 1], [8 x i16] [i16 0, i16 0, i16 2, i16 -8, i16 123, i16 15, i16 -5, i16 1], [8 x i16] [i16 0, i16 0, i16 2, i16 -9, i16 122, i16 18, i16 -6, i16 1], [8 x i16] [i16 0, i16 0, i16 2, i16 -10, i16 121, i16 20, i16 -6, i16 1], [8 x i16] [i16 0, i16 0, i16 2, i16 -11, i16 120, i16 22, i16 -7, i16 2], [8 x i16] [i16 0, i16 0, i16 2, i16 -12, i16 119, i16 25, i16 -8, i16 2], [8 x i16] [i16 0, i16 0, i16 3, i16 -13, i16 117, i16 27, i16 -8, i16 2], [8 x i16] [i16 0, i16 0, i16 3, i16 -13, i16 116, i16 29, i16 -9, i16 2], [8 x i16] [i16 0, i16 0, i16 3, i16 -14, i16 114, i16 32, i16 -10, i16 3], [8 x i16] [i16 0, i16 0, i16 3, i16 -15, i16 113, i16 35, i16 -10, i16 2], [8 x i16] [i16 0, i16 0, i16 3, i16 -15, i16 111, i16 37, i16 -11, i16 3], [8 x i16] [i16 0, i16 0, i16 3, i16 -16, i16 109, i16 40, i16 -11, i16 3], [8 x i16] [i16 0, i16 0, i16 3, i16 -16, i16 108, i16 42, i16 -12, i16 3], [8 x i16] [i16 0, i16 0, i16 4, i16 -17, i16 106, i16 45, i16 -13, i16 3], [8 x i16] [i16 0, i16 0, i16 4, i16 -17, i16 104, i16 47, i16 -13, i16 3], [8 x i16] [i16 0, i16 0, i16 4, i16 -17, i16 102, i16 50, i16 -14, i16 3], [8 x i16] [i16 0, i16 0, i16 4, i16 -17, i16 100, i16 52, i16 -14, i16 3], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 98, i16 55, i16 -15, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 96, i16 58, i16 -15, i16 3], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 94, i16 60, i16 -16, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 91, i16 63, i16 -16, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 89, i16 65, i16 -16, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 87, i16 68, i16 -17, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 85, i16 70, i16 -17, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 82, i16 73, i16 -17, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 80, i16 75, i16 -17, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -18, i16 78, i16 78, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -17, i16 75, i16 80, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -17, i16 73, i16 82, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -17, i16 70, i16 85, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -17, i16 68, i16 87, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -16, i16 65, i16 89, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -16, i16 63, i16 91, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -16, i16 60, i16 94, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 3, i16 -15, i16 58, i16 96, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 4, i16 -15, i16 55, i16 98, i16 -18, i16 4], [8 x i16] [i16 0, i16 0, i16 3, i16 -14, i16 52, i16 100, i16 -17, i16 4], [8 x i16] [i16 0, i16 0, i16 3, i16 -14, i16 50, i16 102, i16 -17, i16 4], [8 x i16] [i16 0, i16 0, i16 3, i16 -13, i16 47, i16 104, i16 -17, i16 4], [8 x i16] [i16 0, i16 0, i16 3, i16 -13, i16 45, i16 106, i16 -17, i16 4], [8 x i16] [i16 0, i16 0, i16 3, i16 -12, i16 42, i16 108, i16 -16, i16 3], [8 x i16] [i16 0, i16 0, i16 3, i16 -11, i16 40, i16 109, i16 -16, i16 3], [8 x i16] [i16 0, i16 0, i16 3, i16 -11, i16 37, i16 111, i16 -15, i16 3], [8 x i16] [i16 0, i16 0, i16 2, i16 -10, i16 35, i16 113, i16 -15, i16 3], [8 x i16] [i16 0, i16 0, i16 3, i16 -10, i16 32, i16 114, i16 -14, i16 3], [8 x i16] [i16 0, i16 0, i16 2, i16 -9, i16 29, i16 116, i16 -13, i16 3], [8 x i16] [i16 0, i16 0, i16 2, i16 -8, i16 27, i16 117, i16 -13, i16 3], [8 x i16] [i16 0, i16 0, i16 2, i16 -8, i16 25, i16 119, i16 -12, i16 2], [8 x i16] [i16 0, i16 0, i16 2, i16 -7, i16 22, i16 120, i16 -11, i16 2], [8 x i16] [i16 0, i16 0, i16 1, i16 -6, i16 20, i16 121, i16 -10, i16 2], [8 x i16] [i16 0, i16 0, i16 1, i16 -6, i16 18, i16 122, i16 -9, i16 2], [8 x i16] [i16 0, i16 0, i16 1, i16 -5, i16 15, i16 123, i16 -8, i16 2], [8 x i16] [i16 0, i16 0, i16 1, i16 -4, i16 13, i16 124, i16 -7, i16 1], [8 x i16] [i16 0, i16 0, i16 1, i16 -4, i16 11, i16 125, i16 -6, i16 1], [8 x i16] [i16 0, i16 0, i16 1, i16 -3, i16 8, i16 126, i16 -5, i16 1], [8 x i16] [i16 0, i16 0, i16 1, i16 -2, i16 6, i16 126, i16 -4, i16 1], [8 x i16] [i16 0, i16 0, i16 0, i16 -1, i16 4, i16 127, i16 -3, i16 1], [8 x i16] [i16 0, i16 0, i16 0, i16 0, i16 2, i16 127, i16 -1, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 0, i16 2, i16 127, i16 -1, i16 0]], align 16
@div_lut = internal constant [257 x i16] [i16 16384, i16 16320, i16 16257, i16 16194, i16 16132, i16 16070, i16 16009, i16 15948, i16 15888, i16 15828, i16 15768, i16 15709, i16 15650, i16 15592, i16 15534, i16 15477, i16 15420, i16 15364, i16 15308, i16 15252, i16 15197, i16 15142, i16 15087, i16 15033, i16 14980, i16 14926, i16 14873, i16 14821, i16 14769, i16 14717, i16 14665, i16 14614, i16 14564, i16 14513, i16 14463, i16 14413, i16 14364, i16 14315, i16 14266, i16 14218, i16 14170, i16 14122, i16 14075, i16 14028, i16 13981, i16 13935, i16 13888, i16 13843, i16 13797, i16 13752, i16 13707, i16 13662, i16 13618, i16 13574, i16 13530, i16 13487, i16 13443, i16 13400, i16 13358, i16 13315, i16 13273, i16 13231, i16 13190, i16 13148, i16 13107, i16 13066, i16 13026, i16 12985, i16 12945, i16 12906, i16 12866, i16 12827, i16 12788, i16 12749, i16 12710, i16 12672, i16 12633, i16 12596, i16 12558, i16 12520, i16 12483, i16 12446, i16 12409, i16 12373, i16 12336, i16 12300, i16 12264, i16 12228, i16 12193, i16 12157, i16 12122, i16 12087, i16 12053, i16 12018, i16 11984, i16 11950, i16 11916, i16 11882, i16 11848, i16 11815, i16 11782, i16 11749, i16 11716, i16 11683, i16 11651, i16 11619, i16 11586, i16 11555, i16 11523, i16 11491, i16 11460, i16 11429, i16 11398, i16 11367, i16 11336, i16 11305, i16 11275, i16 11245, i16 11215, i16 11185, i16 11155, i16 11125, i16 11096, i16 11067, i16 11038, i16 11009, i16 10980, i16 10951, i16 10923, i16 10894, i16 10866, i16 10838, i16 10810, i16 10782, i16 10755, i16 10727, i16 10700, i16 10673, i16 10645, i16 10618, i16 10592, i16 10565, i16 10538, i16 10512, i16 10486, i16 10460, i16 10434, i16 10408, i16 10382, i16 10356, i16 10331, i16 10305, i16 10280, i16 10255, i16 10230, i16 10205, i16 10180, i16 10156, i16 10131, i16 10107, i16 10082, i16 10058, i16 10034, i16 10010, i16 9986, i16 9963, i16 9939, i16 9916, i16 9892, i16 9869, i16 9846, i16 9823, i16 9800, i16 9777, i16 9754, i16 9732, i16 9709, i16 9687, i16 9664, i16 9642, i16 9620, i16 9598, i16 9576, i16 9554, i16 9533, i16 9511, i16 9489, i16 9468, i16 9447, i16 9425, i16 9404, i16 9383, i16 9362, i16 9341, i16 9321, i16 9300, i16 9279, i16 9259, i16 9239, i16 9218, i16 9198, i16 9178, i16 9158, i16 9138, i16 9118, i16 9098, i16 9079, i16 9059, i16 9039, i16 9020, i16 9001, i16 8981, i16 8962, i16 8943, i16 8924, i16 8905, i16 8886, i16 8867, i16 8849, i16 8830, i16 8812, i16 8793, i16 8775, i16 8756, i16 8738, i16 8720, i16 8702, i16 8684, i16 8666, i16 8648, i16 8630, i16 8613, i16 8595, i16 8577, i16 8560, i16 8542, i16 8525, i16 8508, i16 8490, i16 8473, i16 8456, i16 8439, i16 8422, i16 8405, i16 8389, i16 8372, i16 8355, i16 8339, i16 8322, i16 8306, i16 8289, i16 8273, i16 8257, i16 8240, i16 8224, i16 8208, i16 8192], align 16
@error_measure_lut = internal constant [512 x i32] [i32 16384, i32 16339, i32 16294, i32 16249, i32 16204, i32 16158, i32 16113, i32 16068, i32 16022, i32 15977, i32 15932, i32 15886, i32 15840, i32 15795, i32 15749, i32 15703, i32 15657, i32 15612, i32 15566, i32 15520, i32 15474, i32 15427, i32 15381, i32 15335, i32 15289, i32 15242, i32 15196, i32 15149, i32 15103, i32 15056, i32 15010, i32 14963, i32 14916, i32 14869, i32 14822, i32 14775, i32 14728, i32 14681, i32 14634, i32 14587, i32 14539, i32 14492, i32 14445, i32 14397, i32 14350, i32 14302, i32 14254, i32 14206, i32 14159, i32 14111, i32 14063, i32 14015, i32 13967, i32 13918, i32 13870, i32 13822, i32 13773, i32 13725, i32 13676, i32 13628, i32 13579, i32 13530, i32 13481, i32 13432, i32 13383, i32 13334, i32 13285, i32 13236, i32 13187, i32 13137, i32 13088, i32 13038, i32 12988, i32 12939, i32 12889, i32 12839, i32 12789, i32 12739, i32 12689, i32 12639, i32 12588, i32 12538, i32 12487, i32 12437, i32 12386, i32 12335, i32 12285, i32 12234, i32 12183, i32 12132, i32 12080, i32 12029, i32 11978, i32 11926, i32 11875, i32 11823, i32 11771, i32 11719, i32 11667, i32 11615, i32 11563, i32 11511, i32 11458, i32 11406, i32 11353, i32 11301, i32 11248, i32 11195, i32 11142, i32 11089, i32 11036, i32 10982, i32 10929, i32 10875, i32 10822, i32 10768, i32 10714, i32 10660, i32 10606, i32 10552, i32 10497, i32 10443, i32 10388, i32 10333, i32 10279, i32 10224, i32 10168, i32 10113, i32 10058, i32 10002, i32 9947, i32 9891, i32 9835, i32 9779, i32 9723, i32 9666, i32 9610, i32 9553, i32 9497, i32 9440, i32 9383, i32 9326, i32 9268, i32 9211, i32 9153, i32 9095, i32 9037, i32 8979, i32 8921, i32 8862, i32 8804, i32 8745, i32 8686, i32 8627, i32 8568, i32 8508, i32 8449, i32 8389, i32 8329, i32 8269, i32 8208, i32 8148, i32 8087, i32 8026, i32 7965, i32 7903, i32 7842, i32 7780, i32 7718, i32 7656, i32 7593, i32 7531, i32 7468, i32 7405, i32 7341, i32 7278, i32 7214, i32 7150, i32 7086, i32 7021, i32 6956, i32 6891, i32 6826, i32 6760, i32 6695, i32 6628, i32 6562, i32 6495, i32 6428, i32 6361, i32 6293, i32 6225, i32 6157, i32 6089, i32 6020, i32 5950, i32 5881, i32 5811, i32 5741, i32 5670, i32 5599, i32 5527, i32 5456, i32 5383, i32 5311, i32 5237, i32 5164, i32 5090, i32 5015, i32 4941, i32 4865, i32 4789, i32 4713, i32 4636, i32 4558, i32 4480, i32 4401, i32 4322, i32 4242, i32 4162, i32 4080, i32 3998, i32 3916, i32 3832, i32 3748, i32 3663, i32 3577, i32 3490, i32 3402, i32 3314, i32 3224, i32 3133, i32 3041, i32 2948, i32 2854, i32 2758, i32 2661, i32 2562, i32 2461, i32 2359, i32 2255, i32 2148, i32 2040, i32 1929, i32 1815, i32 1698, i32 1577, i32 1452, i32 1323, i32 1187, i32 1045, i32 894, i32 731, i32 550, i32 339, i32 0, i32 339, i32 550, i32 731, i32 894, i32 1045, i32 1187, i32 1323, i32 1452, i32 1577, i32 1698, i32 1815, i32 1929, i32 2040, i32 2148, i32 2255, i32 2359, i32 2461, i32 2562, i32 2661, i32 2758, i32 2854, i32 2948, i32 3041, i32 3133, i32 3224, i32 3314, i32 3402, i32 3490, i32 3577, i32 3663, i32 3748, i32 3832, i32 3916, i32 3998, i32 4080, i32 4162, i32 4242, i32 4322, i32 4401, i32 4480, i32 4558, i32 4636, i32 4713, i32 4789, i32 4865, i32 4941, i32 5015, i32 5090, i32 5164, i32 5237, i32 5311, i32 5383, i32 5456, i32 5527, i32 5599, i32 5670, i32 5741, i32 5811, i32 5881, i32 5950, i32 6020, i32 6089, i32 6157, i32 6225, i32 6293, i32 6361, i32 6428, i32 6495, i32 6562, i32 6628, i32 6695, i32 6760, i32 6826, i32 6891, i32 6956, i32 7021, i32 7086, i32 7150, i32 7214, i32 7278, i32 7341, i32 7405, i32 7468, i32 7531, i32 7593, i32 7656, i32 7718, i32 7780, i32 7842, i32 7903, i32 7965, i32 8026, i32 8087, i32 8148, i32 8208, i32 8269, i32 8329, i32 8389, i32 8449, i32 8508, i32 8568, i32 8627, i32 8686, i32 8745, i32 8804, i32 8862, i32 8921, i32 8979, i32 9037, i32 9095, i32 9153, i32 9211, i32 9268, i32 9326, i32 9383, i32 9440, i32 9497, i32 9553, i32 9610, i32 9666, i32 9723, i32 9779, i32 9835, i32 9891, i32 9947, i32 10002, i32 10058, i32 10113, i32 10168, i32 10224, i32 10279, i32 10333, i32 10388, i32 10443, i32 10497, i32 10552, i32 10606, i32 10660, i32 10714, i32 10768, i32 10822, i32 10875, i32 10929, i32 10982, i32 11036, i32 11089, i32 11142, i32 11195, i32 11248, i32 11301, i32 11353, i32 11406, i32 11458, i32 11511, i32 11563, i32 11615, i32 11667, i32 11719, i32 11771, i32 11823, i32 11875, i32 11926, i32 11978, i32 12029, i32 12080, i32 12132, i32 12183, i32 12234, i32 12285, i32 12335, i32 12386, i32 12437, i32 12487, i32 12538, i32 12588, i32 12639, i32 12689, i32 12739, i32 12789, i32 12839, i32 12889, i32 12939, i32 12988, i32 13038, i32 13088, i32 13137, i32 13187, i32 13236, i32 13285, i32 13334, i32 13383, i32 13432, i32 13481, i32 13530, i32 13579, i32 13628, i32 13676, i32 13725, i32 13773, i32 13822, i32 13870, i32 13918, i32 13967, i32 14015, i32 14063, i32 14111, i32 14159, i32 14206, i32 14254, i32 14302, i32 14350, i32 14397, i32 14445, i32 14492, i32 14539, i32 14587, i32 14634, i32 14681, i32 14728, i32 14775, i32 14822, i32 14869, i32 14916, i32 14963, i32 15010, i32 15056, i32 15103, i32 15149, i32 15196, i32 15242, i32 15289, i32 15335, i32 15381, i32 15427, i32 15474, i32 15520, i32 15566, i32 15612, i32 15657, i32 15703, i32 15749, i32 15795, i32 15840, i32 15886, i32 15932, i32 15977, i32 16022, i32 16068, i32 16113, i32 16158, i32 16204, i32 16249, i32 16294, i32 16339, i32 16384, i32 16384], align 16
@block_size_wide = internal constant [22 x i8] c"\04\04\08\08\08\10\10\10   @@@\80\80\04\10\08 \10@", align 16
@block_size_high = internal constant [22 x i8] c"\04\08\04\08\10\08\10 \10 @ @\80@\80\10\04 \08@\10", align 16

; Function Attrs: nounwind
define hidden i32 @av1_get_shear_params(%struct.WarpedMotionParams* %wm) #0 {
entry:
  %retval = alloca i32, align 4
  %wm.addr = alloca %struct.WarpedMotionParams*, align 4
  %mat = alloca i32*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %shift = alloca i16, align 2
  %y = alloca i16, align 2
  %v = alloca i64, align 8
  store %struct.WarpedMotionParams* %wm, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %0 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat, i32 0, i32 0
  store i32* %arraydecay, i32** %mat, align 4, !tbaa !2
  %2 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %call = call i32 @is_affine_valid(%struct.WarpedMotionParams* %2)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup168

if.end:                                           ; preds = %entry
  %3 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 2
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %sub = sub nsw i32 %4, 65536
  %call1 = call i32 @clamp(i32 %sub, i32 -32768, i32 32767)
  %conv = trunc i32 %call1 to i16
  %5 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %alpha = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %5, i32 0, i32 1
  store i16 %conv, i16* %alpha, align 4, !tbaa !8
  %6 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %6, i32 3
  %7 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  %call3 = call i32 @clamp(i32 %7, i32 -32768, i32 32767)
  %conv4 = trunc i32 %call3 to i16
  %8 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %beta = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %8, i32 0, i32 2
  store i16 %conv4, i16* %beta, align 2, !tbaa !11
  %9 = bitcast i16* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %9) #6
  %10 = bitcast i16* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %10) #6
  %11 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %11, i32 2
  %12 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %call6 = call i32 @abs(i32 %12) #7
  %call7 = call signext i16 @resolve_divisor_32(i32 %call6, i16* %shift)
  %conv8 = sext i16 %call7 to i32
  %13 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %13, i32 2
  %14 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %cmp = icmp slt i32 %14, 0
  %15 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 -1, i32 1
  %mul = mul nsw i32 %conv8, %cond
  %conv11 = trunc i32 %mul to i16
  store i16 %conv11, i16* %y, align 2, !tbaa !12
  %16 = bitcast i64* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %16) #6
  %17 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %17, i32 4
  %18 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %conv13 = sext i32 %18 to i64
  %mul14 = mul nsw i64 %conv13, 65536
  %19 = load i16, i16* %y, align 2, !tbaa !12
  %conv15 = sext i16 %19 to i64
  %mul16 = mul nsw i64 %mul14, %conv15
  store i64 %mul16, i64* %v, align 8, !tbaa !13
  %20 = load i64, i64* %v, align 8, !tbaa !13
  %cmp17 = icmp slt i64 %20, 0
  br i1 %cmp17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %21 = load i64, i64* %v, align 8, !tbaa !13
  %sub19 = sub nsw i64 0, %21
  %22 = load i16, i16* %shift, align 2, !tbaa !12
  %conv20 = sext i16 %22 to i32
  %sh_prom = zext i32 %conv20 to i64
  %shl = shl i64 1, %sh_prom
  %shr = ashr i64 %shl, 1
  %add = add nsw i64 %sub19, %shr
  %23 = load i16, i16* %shift, align 2, !tbaa !12
  %conv21 = sext i16 %23 to i32
  %sh_prom22 = zext i32 %conv21 to i64
  %shr23 = ashr i64 %add, %sh_prom22
  %sub24 = sub nsw i64 0, %shr23
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %24 = load i64, i64* %v, align 8, !tbaa !13
  %25 = load i16, i16* %shift, align 2, !tbaa !12
  %conv25 = sext i16 %25 to i32
  %sh_prom26 = zext i32 %conv25 to i64
  %shl27 = shl i64 1, %sh_prom26
  %shr28 = ashr i64 %shl27, 1
  %add29 = add nsw i64 %24, %shr28
  %26 = load i16, i16* %shift, align 2, !tbaa !12
  %conv30 = sext i16 %26 to i32
  %sh_prom31 = zext i32 %conv30 to i64
  %shr32 = ashr i64 %add29, %sh_prom31
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond33 = phi i64 [ %sub24, %cond.true ], [ %shr32, %cond.false ]
  %conv34 = trunc i64 %cond33 to i32
  %call35 = call i32 @clamp(i32 %conv34, i32 -32768, i32 32767)
  %conv36 = trunc i32 %call35 to i16
  %27 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %27, i32 0, i32 3
  store i16 %conv36, i16* %gamma, align 4, !tbaa !15
  %28 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %28, i32 3
  %29 = load i32, i32* %arrayidx37, align 4, !tbaa !6
  %conv38 = sext i32 %29 to i64
  %30 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %30, i32 4
  %31 = load i32, i32* %arrayidx39, align 4, !tbaa !6
  %conv40 = sext i32 %31 to i64
  %mul41 = mul nsw i64 %conv38, %conv40
  %32 = load i16, i16* %y, align 2, !tbaa !12
  %conv42 = sext i16 %32 to i64
  %mul43 = mul nsw i64 %mul41, %conv42
  store i64 %mul43, i64* %v, align 8, !tbaa !13
  %33 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %33, i32 5
  %34 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  %35 = load i64, i64* %v, align 8, !tbaa !13
  %cmp45 = icmp slt i64 %35, 0
  br i1 %cmp45, label %cond.true47, label %cond.false58

cond.true47:                                      ; preds = %cond.end
  %36 = load i64, i64* %v, align 8, !tbaa !13
  %sub48 = sub nsw i64 0, %36
  %37 = load i16, i16* %shift, align 2, !tbaa !12
  %conv49 = sext i16 %37 to i32
  %sh_prom50 = zext i32 %conv49 to i64
  %shl51 = shl i64 1, %sh_prom50
  %shr52 = ashr i64 %shl51, 1
  %add53 = add nsw i64 %sub48, %shr52
  %38 = load i16, i16* %shift, align 2, !tbaa !12
  %conv54 = sext i16 %38 to i32
  %sh_prom55 = zext i32 %conv54 to i64
  %shr56 = ashr i64 %add53, %sh_prom55
  %sub57 = sub nsw i64 0, %shr56
  br label %cond.end67

cond.false58:                                     ; preds = %cond.end
  %39 = load i64, i64* %v, align 8, !tbaa !13
  %40 = load i16, i16* %shift, align 2, !tbaa !12
  %conv59 = sext i16 %40 to i32
  %sh_prom60 = zext i32 %conv59 to i64
  %shl61 = shl i64 1, %sh_prom60
  %shr62 = ashr i64 %shl61, 1
  %add63 = add nsw i64 %39, %shr62
  %41 = load i16, i16* %shift, align 2, !tbaa !12
  %conv64 = sext i16 %41 to i32
  %sh_prom65 = zext i32 %conv64 to i64
  %shr66 = ashr i64 %add63, %sh_prom65
  br label %cond.end67

cond.end67:                                       ; preds = %cond.false58, %cond.true47
  %cond68 = phi i64 [ %sub57, %cond.true47 ], [ %shr66, %cond.false58 ]
  %conv69 = trunc i64 %cond68 to i32
  %sub70 = sub nsw i32 %34, %conv69
  %sub71 = sub nsw i32 %sub70, 65536
  %call72 = call i32 @clamp(i32 %sub71, i32 -32768, i32 32767)
  %conv73 = trunc i32 %call72 to i16
  %42 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %delta = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %42, i32 0, i32 4
  store i16 %conv73, i16* %delta, align 2, !tbaa !16
  %43 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %alpha74 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %43, i32 0, i32 1
  %44 = load i16, i16* %alpha74, align 4, !tbaa !8
  %conv75 = sext i16 %44 to i32
  %cmp76 = icmp slt i32 %conv75, 0
  br i1 %cmp76, label %cond.true78, label %cond.false85

cond.true78:                                      ; preds = %cond.end67
  %45 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %alpha79 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %45, i32 0, i32 1
  %46 = load i16, i16* %alpha79, align 4, !tbaa !8
  %conv80 = sext i16 %46 to i32
  %sub81 = sub nsw i32 0, %conv80
  %add82 = add nsw i32 %sub81, 32
  %shr83 = ashr i32 %add82, 6
  %sub84 = sub nsw i32 0, %shr83
  br label %cond.end90

cond.false85:                                     ; preds = %cond.end67
  %47 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %alpha86 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %47, i32 0, i32 1
  %48 = load i16, i16* %alpha86, align 4, !tbaa !8
  %conv87 = sext i16 %48 to i32
  %add88 = add nsw i32 %conv87, 32
  %shr89 = ashr i32 %add88, 6
  br label %cond.end90

cond.end90:                                       ; preds = %cond.false85, %cond.true78
  %cond91 = phi i32 [ %sub84, %cond.true78 ], [ %shr89, %cond.false85 ]
  %mul92 = mul nsw i32 %cond91, 64
  %conv93 = trunc i32 %mul92 to i16
  %49 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %alpha94 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %49, i32 0, i32 1
  store i16 %conv93, i16* %alpha94, align 4, !tbaa !8
  %50 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %beta95 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %50, i32 0, i32 2
  %51 = load i16, i16* %beta95, align 2, !tbaa !11
  %conv96 = sext i16 %51 to i32
  %cmp97 = icmp slt i32 %conv96, 0
  br i1 %cmp97, label %cond.true99, label %cond.false106

cond.true99:                                      ; preds = %cond.end90
  %52 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %beta100 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %52, i32 0, i32 2
  %53 = load i16, i16* %beta100, align 2, !tbaa !11
  %conv101 = sext i16 %53 to i32
  %sub102 = sub nsw i32 0, %conv101
  %add103 = add nsw i32 %sub102, 32
  %shr104 = ashr i32 %add103, 6
  %sub105 = sub nsw i32 0, %shr104
  br label %cond.end111

cond.false106:                                    ; preds = %cond.end90
  %54 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %beta107 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %54, i32 0, i32 2
  %55 = load i16, i16* %beta107, align 2, !tbaa !11
  %conv108 = sext i16 %55 to i32
  %add109 = add nsw i32 %conv108, 32
  %shr110 = ashr i32 %add109, 6
  br label %cond.end111

cond.end111:                                      ; preds = %cond.false106, %cond.true99
  %cond112 = phi i32 [ %sub105, %cond.true99 ], [ %shr110, %cond.false106 ]
  %mul113 = mul nsw i32 %cond112, 64
  %conv114 = trunc i32 %mul113 to i16
  %56 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %beta115 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %56, i32 0, i32 2
  store i16 %conv114, i16* %beta115, align 2, !tbaa !11
  %57 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %gamma116 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %57, i32 0, i32 3
  %58 = load i16, i16* %gamma116, align 4, !tbaa !15
  %conv117 = sext i16 %58 to i32
  %cmp118 = icmp slt i32 %conv117, 0
  br i1 %cmp118, label %cond.true120, label %cond.false127

cond.true120:                                     ; preds = %cond.end111
  %59 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %gamma121 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %59, i32 0, i32 3
  %60 = load i16, i16* %gamma121, align 4, !tbaa !15
  %conv122 = sext i16 %60 to i32
  %sub123 = sub nsw i32 0, %conv122
  %add124 = add nsw i32 %sub123, 32
  %shr125 = ashr i32 %add124, 6
  %sub126 = sub nsw i32 0, %shr125
  br label %cond.end132

cond.false127:                                    ; preds = %cond.end111
  %61 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %gamma128 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %61, i32 0, i32 3
  %62 = load i16, i16* %gamma128, align 4, !tbaa !15
  %conv129 = sext i16 %62 to i32
  %add130 = add nsw i32 %conv129, 32
  %shr131 = ashr i32 %add130, 6
  br label %cond.end132

cond.end132:                                      ; preds = %cond.false127, %cond.true120
  %cond133 = phi i32 [ %sub126, %cond.true120 ], [ %shr131, %cond.false127 ]
  %mul134 = mul nsw i32 %cond133, 64
  %conv135 = trunc i32 %mul134 to i16
  %63 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %gamma136 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %63, i32 0, i32 3
  store i16 %conv135, i16* %gamma136, align 4, !tbaa !15
  %64 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %delta137 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %64, i32 0, i32 4
  %65 = load i16, i16* %delta137, align 2, !tbaa !16
  %conv138 = sext i16 %65 to i32
  %cmp139 = icmp slt i32 %conv138, 0
  br i1 %cmp139, label %cond.true141, label %cond.false148

cond.true141:                                     ; preds = %cond.end132
  %66 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %delta142 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %66, i32 0, i32 4
  %67 = load i16, i16* %delta142, align 2, !tbaa !16
  %conv143 = sext i16 %67 to i32
  %sub144 = sub nsw i32 0, %conv143
  %add145 = add nsw i32 %sub144, 32
  %shr146 = ashr i32 %add145, 6
  %sub147 = sub nsw i32 0, %shr146
  br label %cond.end153

cond.false148:                                    ; preds = %cond.end132
  %68 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %delta149 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %68, i32 0, i32 4
  %69 = load i16, i16* %delta149, align 2, !tbaa !16
  %conv150 = sext i16 %69 to i32
  %add151 = add nsw i32 %conv150, 32
  %shr152 = ashr i32 %add151, 6
  br label %cond.end153

cond.end153:                                      ; preds = %cond.false148, %cond.true141
  %cond154 = phi i32 [ %sub147, %cond.true141 ], [ %shr152, %cond.false148 ]
  %mul155 = mul nsw i32 %cond154, 64
  %conv156 = trunc i32 %mul155 to i16
  %70 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %delta157 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %70, i32 0, i32 4
  store i16 %conv156, i16* %delta157, align 2, !tbaa !16
  %71 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %alpha158 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %71, i32 0, i32 1
  %72 = load i16, i16* %alpha158, align 4, !tbaa !8
  %73 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %beta159 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %73, i32 0, i32 2
  %74 = load i16, i16* %beta159, align 2, !tbaa !11
  %75 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %gamma160 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %75, i32 0, i32 3
  %76 = load i16, i16* %gamma160, align 4, !tbaa !15
  %77 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %delta161 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %77, i32 0, i32 4
  %78 = load i16, i16* %delta161, align 2, !tbaa !16
  %call162 = call i32 @is_affine_shear_allowed(i16 signext %72, i16 signext %74, i16 signext %76, i16 signext %78)
  %tobool163 = icmp ne i32 %call162, 0
  br i1 %tobool163, label %if.end165, label %if.then164

if.then164:                                       ; preds = %cond.end153
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end165:                                        ; preds = %cond.end153
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end165, %if.then164
  %79 = bitcast i64* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %79) #6
  %80 = bitcast i16* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %80) #6
  %81 = bitcast i16* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %81) #6
  br label %cleanup168

cleanup168:                                       ; preds = %cleanup, %if.then
  %82 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #6
  %83 = load i32, i32* %retval, align 4
  ret i32 %83
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @is_affine_valid(%struct.WarpedMotionParams* %wm) #0 {
entry:
  %wm.addr = alloca %struct.WarpedMotionParams*, align 4
  %mat = alloca i32*, align 4
  store %struct.WarpedMotionParams* %wm, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %0 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat, i32 0, i32 0
  store i32* %arraydecay, i32** %mat, align 4, !tbaa !2
  %2 = load i32*, i32** %mat, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 2
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %cmp = icmp sgt i32 %3, 0
  %conv = zext i1 %cmp to i32
  %4 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  ret i32 %conv
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: nounwind
define internal signext i16 @resolve_divisor_32(i32 %D, i16* %shift) #0 {
entry:
  %D.addr = alloca i32, align 4
  %shift.addr = alloca i16*, align 4
  %f = alloca i32, align 4
  %e = alloca i32, align 4
  store i32 %D, i32* %D.addr, align 4, !tbaa !6
  store i16* %shift, i16** %shift.addr, align 4, !tbaa !2
  %0 = bitcast i32* %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %D.addr, align 4, !tbaa !6
  %call = call i32 @get_msb(i32 %1)
  %conv = trunc i32 %call to i16
  %2 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  store i16 %conv, i16* %2, align 2, !tbaa !12
  %3 = bitcast i32* %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %D.addr, align 4, !tbaa !6
  %5 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %6 = load i16, i16* %5, align 2, !tbaa !12
  %conv1 = sext i16 %6 to i32
  %shl = shl i32 1, %conv1
  %sub = sub i32 %4, %shl
  store i32 %sub, i32* %e, align 4, !tbaa !6
  %7 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %8 = load i16, i16* %7, align 2, !tbaa !12
  %conv2 = sext i16 %8 to i32
  %cmp = icmp sgt i32 %conv2, 8
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %9 = load i32, i32* %e, align 4, !tbaa !6
  %10 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %11 = load i16, i16* %10, align 2, !tbaa !12
  %conv4 = sext i16 %11 to i32
  %sub5 = sub nsw i32 %conv4, 8
  %shl6 = shl i32 1, %sub5
  %shr = ashr i32 %shl6, 1
  %add = add nsw i32 %9, %shr
  %12 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %13 = load i16, i16* %12, align 2, !tbaa !12
  %conv7 = sext i16 %13 to i32
  %sub8 = sub nsw i32 %conv7, 8
  %shr9 = ashr i32 %add, %sub8
  store i32 %shr9, i32* %f, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %entry
  %14 = load i32, i32* %e, align 4, !tbaa !6
  %15 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %16 = load i16, i16* %15, align 2, !tbaa !12
  %conv10 = sext i16 %16 to i32
  %sub11 = sub nsw i32 8, %conv10
  %shl12 = shl i32 %14, %sub11
  store i32 %shl12, i32* %f, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %18 = load i16, i16* %17, align 2, !tbaa !12
  %conv13 = sext i16 %18 to i32
  %add14 = add nsw i32 %conv13, 14
  %conv15 = trunc i32 %add14 to i16
  store i16 %conv15, i16* %17, align 2, !tbaa !12
  %19 = load i32, i32* %f, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [257 x i16], [257 x i16]* @div_lut, i32 0, i32 %19
  %20 = load i16, i16* %arrayidx, align 2, !tbaa !12
  %21 = bitcast i32* %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast i32* %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  ret i16 %20
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #3

; Function Attrs: nounwind
define internal i32 @is_affine_shear_allowed(i16 signext %alpha, i16 signext %beta, i16 signext %gamma, i16 signext %delta) #0 {
entry:
  %retval = alloca i32, align 4
  %alpha.addr = alloca i16, align 2
  %beta.addr = alloca i16, align 2
  %gamma.addr = alloca i16, align 2
  %delta.addr = alloca i16, align 2
  store i16 %alpha, i16* %alpha.addr, align 2, !tbaa !12
  store i16 %beta, i16* %beta.addr, align 2, !tbaa !12
  store i16 %gamma, i16* %gamma.addr, align 2, !tbaa !12
  store i16 %delta, i16* %delta.addr, align 2, !tbaa !12
  %0 = load i16, i16* %alpha.addr, align 2, !tbaa !12
  %conv = sext i16 %0 to i32
  %call = call i32 @abs(i32 %conv) #7
  %mul = mul nsw i32 4, %call
  %1 = load i16, i16* %beta.addr, align 2, !tbaa !12
  %conv1 = sext i16 %1 to i32
  %call2 = call i32 @abs(i32 %conv1) #7
  %mul3 = mul nsw i32 7, %call2
  %add = add nsw i32 %mul, %mul3
  %cmp = icmp sge i32 %add, 65536
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i16, i16* %gamma.addr, align 2, !tbaa !12
  %conv5 = sext i16 %2 to i32
  %call6 = call i32 @abs(i32 %conv5) #7
  %mul7 = mul nsw i32 4, %call6
  %3 = load i16, i16* %delta.addr, align 2, !tbaa !12
  %conv8 = sext i16 %3 to i32
  %call9 = call i32 @abs(i32 %conv8) #7
  %mul10 = mul nsw i32 4, %call9
  %add11 = add nsw i32 %mul7, %mul10
  %cmp12 = icmp sge i32 %add11, 65536
  br i1 %cmp12, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %lor.lhs.false
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_highbd_warp_affine_c(i32* %mat, i16* %ref, i32 %width, i32 %height, i32 %stride, i16* %pred, i32 %p_col, i32 %p_row, i32 %p_width, i32 %p_height, i32 %p_stride, i32 %subsampling_x, i32 %subsampling_y, i32 %bd, %struct.ConvolveParams* %conv_params, i16 signext %alpha, i16 signext %beta, i16 signext %gamma, i16 signext %delta) #0 {
entry:
  %mat.addr = alloca i32*, align 4
  %ref.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %pred.addr = alloca i16*, align 4
  %p_col.addr = alloca i32, align 4
  %p_row.addr = alloca i32, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %alpha.addr = alloca i16, align 2
  %beta.addr = alloca i16, align 2
  %gamma.addr = alloca i16, align 2
  %delta.addr = alloca i16, align 2
  %tmp = alloca [120 x i32], align 16
  %reduce_bits_horiz = alloca i32, align 4
  %reduce_bits_vert = alloca i32, align 4
  %max_bits_horiz = alloca i32, align 4
  %offset_bits_horiz = alloca i32, align 4
  %offset_bits_vert = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %src_x = alloca i32, align 4
  %src_y = alloca i32, align 4
  %dst_x = alloca i32, align 4
  %dst_y = alloca i32, align 4
  %x4 = alloca i32, align 4
  %y4 = alloca i32, align 4
  %ix4 = alloca i32, align 4
  %sx4 = alloca i32, align 4
  %iy4 = alloca i32, align 4
  %sy4 = alloca i32, align 4
  %k = alloca i32, align 4
  %iy = alloca i32, align 4
  %sx = alloca i32, align 4
  %l = alloca i32, align 4
  %ix = alloca i32, align 4
  %offs = alloca i32, align 4
  %coeffs = alloca i16*, align 4
  %sum = alloca i32, align 4
  %m = alloca i32, align 4
  %sample_x = alloca i32, align 4
  %k122 = alloca i32, align 4
  %sy = alloca i32, align 4
  %l144 = alloca i32, align 4
  %offs162 = alloca i32, align 4
  %coeffs166 = alloca i16*, align 4
  %sum169 = alloca i32, align 4
  %m171 = alloca i32, align 4
  %p = alloca i16*, align 4
  %dst16 = alloca i16*, align 4
  %tmp32 = alloca i32, align 4
  %p243 = alloca i16*, align 4
  store i32* %mat, i32** %mat.addr, align 4, !tbaa !2
  store i16* %ref, i16** %ref.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %pred, i16** %pred.addr, align 4, !tbaa !2
  store i32 %p_col, i32* %p_col.addr, align 4, !tbaa !6
  store i32 %p_row, i32* %p_row.addr, align 4, !tbaa !6
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i16 %alpha, i16* %alpha.addr, align 2, !tbaa !12
  store i16 %beta, i16* %beta.addr, align 2, !tbaa !12
  store i16 %gamma, i16* %gamma.addr, align 2, !tbaa !12
  store i16 %delta, i16* %delta.addr, align 2, !tbaa !12
  %0 = bitcast [120 x i32]* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 480, i8* %0) #6
  %1 = bitcast i32* %reduce_bits_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %2, i32 0, i32 3
  %3 = load i32, i32* %round_0, align 4, !tbaa !17
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add = add nsw i32 %4, 7
  %5 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_01 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %5, i32 0, i32 3
  %6 = load i32, i32* %round_01, align 4, !tbaa !17
  %sub = sub nsw i32 %add, %6
  %sub2 = sub nsw i32 %sub, 14
  %cmp = icmp sgt i32 %sub2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %7 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add3 = add nsw i32 %7, 7
  %8 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_04 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %8, i32 0, i32 3
  %9 = load i32, i32* %round_04, align 4, !tbaa !17
  %sub5 = sub nsw i32 %add3, %9
  %sub6 = sub nsw i32 %sub5, 14
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub6, %cond.true ], [ 0, %cond.false ]
  %add7 = add nsw i32 %3, %cond
  store i32 %add7, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %10 = bitcast i32* %reduce_bits_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %11, i32 0, i32 6
  %12 = load i32, i32* %is_compound, align 4, !tbaa !19
  %tobool = icmp ne i32 %12, 0
  br i1 %tobool, label %cond.true8, label %cond.false9

cond.true8:                                       ; preds = %cond.end
  %13 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %13, i32 0, i32 4
  %14 = load i32, i32* %round_1, align 4, !tbaa !20
  br label %cond.end11

cond.false9:                                      ; preds = %cond.end
  %15 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %sub10 = sub nsw i32 14, %15
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false9, %cond.true8
  %cond12 = phi i32 [ %14, %cond.true8 ], [ %sub10, %cond.false9 ]
  store i32 %cond12, i32* %reduce_bits_vert, align 4, !tbaa !6
  %16 = bitcast i32* %max_bits_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add13 = add nsw i32 %17, 7
  %add14 = add nsw i32 %add13, 1
  %18 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %sub15 = sub nsw i32 %add14, %18
  store i32 %sub15, i32* %max_bits_horiz, align 4, !tbaa !6
  %19 = bitcast i32* %offset_bits_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add16 = add nsw i32 %20, 7
  %sub17 = sub nsw i32 %add16, 1
  store i32 %sub17, i32* %offset_bits_horiz, align 4, !tbaa !6
  %21 = bitcast i32* %offset_bits_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add18 = add nsw i32 %22, 14
  %23 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %sub19 = sub nsw i32 %add18, %23
  store i32 %sub19, i32* %offset_bits_vert, align 4, !tbaa !6
  %24 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_020 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %25, i32 0, i32 3
  %26 = load i32, i32* %round_020, align 4, !tbaa !17
  %sub21 = sub nsw i32 14, %26
  %27 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_122 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %27, i32 0, i32 4
  %28 = load i32, i32* %round_122, align 4, !tbaa !20
  %sub23 = sub nsw i32 %sub21, %28
  store i32 %sub23, i32* %round_bits, align 4, !tbaa !6
  %29 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add24 = add nsw i32 %30, 14
  %31 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_025 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %31, i32 0, i32 3
  %32 = load i32, i32* %round_025, align 4, !tbaa !17
  %sub26 = sub nsw i32 %add24, %32
  store i32 %sub26, i32* %offset_bits, align 4, !tbaa !6
  %33 = load i32, i32* %max_bits_horiz, align 4, !tbaa !6
  %34 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %35 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  store i32 %35, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc275, %cond.end11
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %38 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %add27 = add nsw i32 %37, %38
  %cmp28 = icmp slt i32 %36, %add27
  br i1 %cmp28, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  br label %for.end277

for.body:                                         ; preds = %for.cond
  %40 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  %41 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  store i32 %41, i32* %j, align 4, !tbaa !6
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc272, %for.body
  %42 = load i32, i32* %j, align 4, !tbaa !6
  %43 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %44 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %add30 = add nsw i32 %43, %44
  %cmp31 = icmp slt i32 %42, %add30
  br i1 %cmp31, label %for.body33, label %for.cond.cleanup32

for.cond.cleanup32:                               ; preds = %for.cond29
  store i32 5, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  br label %for.end274

for.body33:                                       ; preds = %for.cond29
  %46 = bitcast i32* %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #6
  %47 = load i32, i32* %j, align 4, !tbaa !6
  %add34 = add nsw i32 %47, 4
  %48 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %shl = shl i32 %add34, %48
  store i32 %shl, i32* %src_x, align 4, !tbaa !6
  %49 = bitcast i32* %src_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #6
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %add35 = add nsw i32 %50, 4
  %51 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %shl36 = shl i32 %add35, %51
  store i32 %shl36, i32* %src_y, align 4, !tbaa !6
  %52 = bitcast i32* %dst_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %53, i32 2
  %54 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %55 = load i32, i32* %src_x, align 4, !tbaa !6
  %mul = mul nsw i32 %54, %55
  %56 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %56, i32 3
  %57 = load i32, i32* %arrayidx37, align 4, !tbaa !6
  %58 = load i32, i32* %src_y, align 4, !tbaa !6
  %mul38 = mul nsw i32 %57, %58
  %add39 = add nsw i32 %mul, %mul38
  %59 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %59, i32 0
  %60 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %add41 = add nsw i32 %add39, %60
  store i32 %add41, i32* %dst_x, align 4, !tbaa !6
  %61 = bitcast i32* %dst_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  %62 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %62, i32 4
  %63 = load i32, i32* %arrayidx42, align 4, !tbaa !6
  %64 = load i32, i32* %src_x, align 4, !tbaa !6
  %mul43 = mul nsw i32 %63, %64
  %65 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %65, i32 5
  %66 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  %67 = load i32, i32* %src_y, align 4, !tbaa !6
  %mul45 = mul nsw i32 %66, %67
  %add46 = add nsw i32 %mul43, %mul45
  %68 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %68, i32 1
  %69 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %add48 = add nsw i32 %add46, %69
  store i32 %add48, i32* %dst_y, align 4, !tbaa !6
  %70 = bitcast i32* %x4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #6
  %71 = load i32, i32* %dst_x, align 4, !tbaa !6
  %72 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %shr = ashr i32 %71, %72
  store i32 %shr, i32* %x4, align 4, !tbaa !6
  %73 = bitcast i32* %y4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  %74 = load i32, i32* %dst_y, align 4, !tbaa !6
  %75 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %shr49 = ashr i32 %74, %75
  store i32 %shr49, i32* %y4, align 4, !tbaa !6
  %76 = bitcast i32* %ix4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #6
  %77 = load i32, i32* %x4, align 4, !tbaa !6
  %shr50 = ashr i32 %77, 16
  store i32 %shr50, i32* %ix4, align 4, !tbaa !6
  %78 = bitcast i32* %sx4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #6
  %79 = load i32, i32* %x4, align 4, !tbaa !6
  %and = and i32 %79, 65535
  store i32 %and, i32* %sx4, align 4, !tbaa !6
  %80 = bitcast i32* %iy4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #6
  %81 = load i32, i32* %y4, align 4, !tbaa !6
  %shr51 = ashr i32 %81, 16
  store i32 %shr51, i32* %iy4, align 4, !tbaa !6
  %82 = bitcast i32* %sy4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #6
  %83 = load i32, i32* %y4, align 4, !tbaa !6
  %and52 = and i32 %83, 65535
  store i32 %and52, i32* %sy4, align 4, !tbaa !6
  %84 = load i16, i16* %alpha.addr, align 2, !tbaa !12
  %conv = sext i16 %84 to i32
  %mul53 = mul nsw i32 %conv, -4
  %85 = load i16, i16* %beta.addr, align 2, !tbaa !12
  %conv54 = sext i16 %85 to i32
  %mul55 = mul nsw i32 %conv54, -4
  %add56 = add nsw i32 %mul53, %mul55
  %86 = load i32, i32* %sx4, align 4, !tbaa !6
  %add57 = add nsw i32 %86, %add56
  store i32 %add57, i32* %sx4, align 4, !tbaa !6
  %87 = load i16, i16* %gamma.addr, align 2, !tbaa !12
  %conv58 = sext i16 %87 to i32
  %mul59 = mul nsw i32 %conv58, -4
  %88 = load i16, i16* %delta.addr, align 2, !tbaa !12
  %conv60 = sext i16 %88 to i32
  %mul61 = mul nsw i32 %conv60, -4
  %add62 = add nsw i32 %mul59, %mul61
  %89 = load i32, i32* %sy4, align 4, !tbaa !6
  %add63 = add nsw i32 %89, %add62
  store i32 %add63, i32* %sy4, align 4, !tbaa !6
  %90 = load i32, i32* %sx4, align 4, !tbaa !6
  %and64 = and i32 %90, -64
  store i32 %and64, i32* %sx4, align 4, !tbaa !6
  %91 = load i32, i32* %sy4, align 4, !tbaa !6
  %and65 = and i32 %91, -64
  store i32 %and65, i32* %sy4, align 4, !tbaa !6
  %92 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #6
  store i32 -7, i32* %k, align 4, !tbaa !6
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc119, %for.body33
  %93 = load i32, i32* %k, align 4, !tbaa !6
  %cmp67 = icmp slt i32 %93, 8
  br i1 %cmp67, label %for.body70, label %for.cond.cleanup69

for.cond.cleanup69:                               ; preds = %for.cond66
  store i32 8, i32* %cleanup.dest.slot, align 4
  %94 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #6
  br label %for.end121

for.body70:                                       ; preds = %for.cond66
  %95 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #6
  %96 = load i32, i32* %iy4, align 4, !tbaa !6
  %97 = load i32, i32* %k, align 4, !tbaa !6
  %add71 = add nsw i32 %96, %97
  %98 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub72 = sub nsw i32 %98, 1
  %call = call i32 @clamp(i32 %add71, i32 0, i32 %sub72)
  store i32 %call, i32* %iy, align 4, !tbaa !6
  %99 = bitcast i32* %sx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #6
  %100 = load i32, i32* %sx4, align 4, !tbaa !6
  %101 = load i16, i16* %beta.addr, align 2, !tbaa !12
  %conv73 = sext i16 %101 to i32
  %102 = load i32, i32* %k, align 4, !tbaa !6
  %add74 = add nsw i32 %102, 4
  %mul75 = mul nsw i32 %conv73, %add74
  %add76 = add nsw i32 %100, %mul75
  store i32 %add76, i32* %sx, align 4, !tbaa !6
  %103 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #6
  store i32 -4, i32* %l, align 4, !tbaa !6
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc116, %for.body70
  %104 = load i32, i32* %l, align 4, !tbaa !6
  %cmp78 = icmp slt i32 %104, 4
  br i1 %cmp78, label %for.body81, label %for.cond.cleanup80

for.cond.cleanup80:                               ; preds = %for.cond77
  store i32 11, i32* %cleanup.dest.slot, align 4
  %105 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #6
  br label %for.end118

for.body81:                                       ; preds = %for.cond77
  %106 = bitcast i32* %ix to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #6
  %107 = load i32, i32* %ix4, align 4, !tbaa !6
  %108 = load i32, i32* %l, align 4, !tbaa !6
  %add82 = add nsw i32 %107, %108
  %sub83 = sub nsw i32 %add82, 3
  store i32 %sub83, i32* %ix, align 4, !tbaa !6
  %109 = bitcast i32* %offs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #6
  %110 = load i32, i32* %sx, align 4, !tbaa !6
  %add84 = add nsw i32 %110, 512
  %shr85 = ashr i32 %add84, 10
  %add86 = add nsw i32 %shr85, 64
  store i32 %add86, i32* %offs, align 4, !tbaa !6
  %111 = bitcast i16** %coeffs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #6
  %112 = load i32, i32* %offs, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds [193 x [8 x i16]], [193 x [8 x i16]]* @av1_warped_filter, i32 0, i32 %112
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx87, i32 0, i32 0
  store i16* %arraydecay, i16** %coeffs, align 4, !tbaa !2
  %113 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #6
  %114 = load i32, i32* %offset_bits_horiz, align 4, !tbaa !6
  %shl88 = shl i32 1, %114
  store i32 %shl88, i32* %sum, align 4, !tbaa !6
  %115 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %115) #6
  store i32 0, i32* %m, align 4, !tbaa !6
  br label %for.cond89

for.cond89:                                       ; preds = %for.inc, %for.body81
  %116 = load i32, i32* %m, align 4, !tbaa !6
  %cmp90 = icmp slt i32 %116, 8
  br i1 %cmp90, label %for.body93, label %for.cond.cleanup92

for.cond.cleanup92:                               ; preds = %for.cond89
  store i32 14, i32* %cleanup.dest.slot, align 4
  %117 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #6
  br label %for.end

for.body93:                                       ; preds = %for.cond89
  %118 = bitcast i32* %sample_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #6
  %119 = load i32, i32* %ix, align 4, !tbaa !6
  %120 = load i32, i32* %m, align 4, !tbaa !6
  %add94 = add nsw i32 %119, %120
  %121 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub95 = sub nsw i32 %121, 1
  %call96 = call i32 @clamp(i32 %add94, i32 0, i32 %sub95)
  store i32 %call96, i32* %sample_x, align 4, !tbaa !6
  %122 = load i16*, i16** %ref.addr, align 4, !tbaa !2
  %123 = load i32, i32* %iy, align 4, !tbaa !6
  %124 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul97 = mul nsw i32 %123, %124
  %125 = load i32, i32* %sample_x, align 4, !tbaa !6
  %add98 = add nsw i32 %mul97, %125
  %arrayidx99 = getelementptr inbounds i16, i16* %122, i32 %add98
  %126 = load i16, i16* %arrayidx99, align 2, !tbaa !12
  %conv100 = zext i16 %126 to i32
  %127 = load i16*, i16** %coeffs, align 4, !tbaa !2
  %128 = load i32, i32* %m, align 4, !tbaa !6
  %arrayidx101 = getelementptr inbounds i16, i16* %127, i32 %128
  %129 = load i16, i16* %arrayidx101, align 2, !tbaa !12
  %conv102 = sext i16 %129 to i32
  %mul103 = mul nsw i32 %conv100, %conv102
  %130 = load i32, i32* %sum, align 4, !tbaa !6
  %add104 = add nsw i32 %130, %mul103
  store i32 %add104, i32* %sum, align 4, !tbaa !6
  %131 = bitcast i32* %sample_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body93
  %132 = load i32, i32* %m, align 4, !tbaa !6
  %inc = add nsw i32 %132, 1
  store i32 %inc, i32* %m, align 4, !tbaa !6
  br label %for.cond89

for.end:                                          ; preds = %for.cond.cleanup92
  %133 = load i32, i32* %sum, align 4, !tbaa !6
  %134 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %shl105 = shl i32 1, %134
  %shr106 = ashr i32 %shl105, 1
  %add107 = add nsw i32 %133, %shr106
  %135 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %shr108 = ashr i32 %add107, %135
  store i32 %shr108, i32* %sum, align 4, !tbaa !6
  %136 = load i32, i32* %sum, align 4, !tbaa !6
  %137 = load i32, i32* %k, align 4, !tbaa !6
  %add109 = add nsw i32 %137, 7
  %mul110 = mul nsw i32 %add109, 8
  %138 = load i32, i32* %l, align 4, !tbaa !6
  %add111 = add nsw i32 %138, 4
  %add112 = add nsw i32 %mul110, %add111
  %arrayidx113 = getelementptr inbounds [120 x i32], [120 x i32]* %tmp, i32 0, i32 %add112
  store i32 %136, i32* %arrayidx113, align 4, !tbaa !6
  %139 = load i16, i16* %alpha.addr, align 2, !tbaa !12
  %conv114 = sext i16 %139 to i32
  %140 = load i32, i32* %sx, align 4, !tbaa !6
  %add115 = add nsw i32 %140, %conv114
  store i32 %add115, i32* %sx, align 4, !tbaa !6
  %141 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #6
  %142 = bitcast i16** %coeffs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #6
  %143 = bitcast i32* %offs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #6
  %144 = bitcast i32* %ix to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #6
  br label %for.inc116

for.inc116:                                       ; preds = %for.end
  %145 = load i32, i32* %l, align 4, !tbaa !6
  %inc117 = add nsw i32 %145, 1
  store i32 %inc117, i32* %l, align 4, !tbaa !6
  br label %for.cond77

for.end118:                                       ; preds = %for.cond.cleanup80
  %146 = bitcast i32* %sx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #6
  %147 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #6
  br label %for.inc119

for.inc119:                                       ; preds = %for.end118
  %148 = load i32, i32* %k, align 4, !tbaa !6
  %inc120 = add nsw i32 %148, 1
  store i32 %inc120, i32* %k, align 4, !tbaa !6
  br label %for.cond66

for.end121:                                       ; preds = %for.cond.cleanup69
  %149 = bitcast i32* %k122 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #6
  store i32 -4, i32* %k122, align 4, !tbaa !6
  br label %for.cond123

for.cond123:                                      ; preds = %for.inc269, %for.end121
  %150 = load i32, i32* %k122, align 4, !tbaa !6
  %151 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %152 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %add124 = add nsw i32 %151, %152
  %153 = load i32, i32* %i, align 4, !tbaa !6
  %sub125 = sub nsw i32 %add124, %153
  %sub126 = sub nsw i32 %sub125, 4
  %cmp127 = icmp slt i32 4, %sub126
  br i1 %cmp127, label %cond.true129, label %cond.false130

cond.true129:                                     ; preds = %for.cond123
  br label %cond.end134

cond.false130:                                    ; preds = %for.cond123
  %154 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %155 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %add131 = add nsw i32 %154, %155
  %156 = load i32, i32* %i, align 4, !tbaa !6
  %sub132 = sub nsw i32 %add131, %156
  %sub133 = sub nsw i32 %sub132, 4
  br label %cond.end134

cond.end134:                                      ; preds = %cond.false130, %cond.true129
  %cond135 = phi i32 [ 4, %cond.true129 ], [ %sub133, %cond.false130 ]
  %cmp136 = icmp slt i32 %150, %cond135
  br i1 %cmp136, label %for.body139, label %for.cond.cleanup138

for.cond.cleanup138:                              ; preds = %cond.end134
  store i32 17, i32* %cleanup.dest.slot, align 4
  %157 = bitcast i32* %k122 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #6
  br label %for.end271

for.body139:                                      ; preds = %cond.end134
  %158 = bitcast i32* %sy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %158) #6
  %159 = load i32, i32* %sy4, align 4, !tbaa !6
  %160 = load i16, i16* %delta.addr, align 2, !tbaa !12
  %conv140 = sext i16 %160 to i32
  %161 = load i32, i32* %k122, align 4, !tbaa !6
  %add141 = add nsw i32 %161, 4
  %mul142 = mul nsw i32 %conv140, %add141
  %add143 = add nsw i32 %159, %mul142
  store i32 %add143, i32* %sy, align 4, !tbaa !6
  %162 = bitcast i32* %l144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #6
  store i32 -4, i32* %l144, align 4, !tbaa !6
  br label %for.cond145

for.cond145:                                      ; preds = %for.inc266, %for.body139
  %163 = load i32, i32* %l144, align 4, !tbaa !6
  %164 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %165 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %add146 = add nsw i32 %164, %165
  %166 = load i32, i32* %j, align 4, !tbaa !6
  %sub147 = sub nsw i32 %add146, %166
  %sub148 = sub nsw i32 %sub147, 4
  %cmp149 = icmp slt i32 4, %sub148
  br i1 %cmp149, label %cond.true151, label %cond.false152

cond.true151:                                     ; preds = %for.cond145
  br label %cond.end156

cond.false152:                                    ; preds = %for.cond145
  %167 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %168 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %add153 = add nsw i32 %167, %168
  %169 = load i32, i32* %j, align 4, !tbaa !6
  %sub154 = sub nsw i32 %add153, %169
  %sub155 = sub nsw i32 %sub154, 4
  br label %cond.end156

cond.end156:                                      ; preds = %cond.false152, %cond.true151
  %cond157 = phi i32 [ 4, %cond.true151 ], [ %sub155, %cond.false152 ]
  %cmp158 = icmp slt i32 %163, %cond157
  br i1 %cmp158, label %for.body161, label %for.cond.cleanup160

for.cond.cleanup160:                              ; preds = %cond.end156
  store i32 20, i32* %cleanup.dest.slot, align 4
  %170 = bitcast i32* %l144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #6
  br label %for.end268

for.body161:                                      ; preds = %cond.end156
  %171 = bitcast i32* %offs162 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %171) #6
  %172 = load i32, i32* %sy, align 4, !tbaa !6
  %add163 = add nsw i32 %172, 512
  %shr164 = ashr i32 %add163, 10
  %add165 = add nsw i32 %shr164, 64
  store i32 %add165, i32* %offs162, align 4, !tbaa !6
  %173 = bitcast i16** %coeffs166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %173) #6
  %174 = load i32, i32* %offs162, align 4, !tbaa !6
  %arrayidx167 = getelementptr inbounds [193 x [8 x i16]], [193 x [8 x i16]]* @av1_warped_filter, i32 0, i32 %174
  %arraydecay168 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx167, i32 0, i32 0
  store i16* %arraydecay168, i16** %coeffs166, align 4, !tbaa !2
  %175 = bitcast i32* %sum169 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %175) #6
  %176 = load i32, i32* %offset_bits_vert, align 4, !tbaa !6
  %shl170 = shl i32 1, %176
  store i32 %shl170, i32* %sum169, align 4, !tbaa !6
  %177 = bitcast i32* %m171 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %177) #6
  store i32 0, i32* %m171, align 4, !tbaa !6
  br label %for.cond172

for.cond172:                                      ; preds = %for.inc187, %for.body161
  %178 = load i32, i32* %m171, align 4, !tbaa !6
  %cmp173 = icmp slt i32 %178, 8
  br i1 %cmp173, label %for.body176, label %for.cond.cleanup175

for.cond.cleanup175:                              ; preds = %for.cond172
  store i32 23, i32* %cleanup.dest.slot, align 4
  %179 = bitcast i32* %m171 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #6
  br label %for.end189

for.body176:                                      ; preds = %for.cond172
  %180 = load i32, i32* %k122, align 4, !tbaa !6
  %181 = load i32, i32* %m171, align 4, !tbaa !6
  %add177 = add nsw i32 %180, %181
  %add178 = add nsw i32 %add177, 4
  %mul179 = mul nsw i32 %add178, 8
  %182 = load i32, i32* %l144, align 4, !tbaa !6
  %add180 = add nsw i32 %182, 4
  %add181 = add nsw i32 %mul179, %add180
  %arrayidx182 = getelementptr inbounds [120 x i32], [120 x i32]* %tmp, i32 0, i32 %add181
  %183 = load i32, i32* %arrayidx182, align 4, !tbaa !6
  %184 = load i16*, i16** %coeffs166, align 4, !tbaa !2
  %185 = load i32, i32* %m171, align 4, !tbaa !6
  %arrayidx183 = getelementptr inbounds i16, i16* %184, i32 %185
  %186 = load i16, i16* %arrayidx183, align 2, !tbaa !12
  %conv184 = sext i16 %186 to i32
  %mul185 = mul nsw i32 %183, %conv184
  %187 = load i32, i32* %sum169, align 4, !tbaa !6
  %add186 = add nsw i32 %187, %mul185
  store i32 %add186, i32* %sum169, align 4, !tbaa !6
  br label %for.inc187

for.inc187:                                       ; preds = %for.body176
  %188 = load i32, i32* %m171, align 4, !tbaa !6
  %inc188 = add nsw i32 %188, 1
  store i32 %inc188, i32* %m171, align 4, !tbaa !6
  br label %for.cond172

for.end189:                                       ; preds = %for.cond.cleanup175
  %189 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound190 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %189, i32 0, i32 6
  %190 = load i32, i32* %is_compound190, align 4, !tbaa !19
  %tobool191 = icmp ne i32 %190, 0
  br i1 %tobool191, label %if.then, label %if.else242

if.then:                                          ; preds = %for.end189
  %191 = bitcast i16** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %191) #6
  %192 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %192, i32 0, i32 1
  %193 = load i16*, i16** %dst, align 4, !tbaa !21
  %194 = load i32, i32* %i, align 4, !tbaa !6
  %195 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %sub192 = sub nsw i32 %194, %195
  %196 = load i32, i32* %k122, align 4, !tbaa !6
  %add193 = add nsw i32 %sub192, %196
  %add194 = add nsw i32 %add193, 4
  %197 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %197, i32 0, i32 2
  %198 = load i32, i32* %dst_stride, align 4, !tbaa !22
  %mul195 = mul nsw i32 %add194, %198
  %199 = load i32, i32* %j, align 4, !tbaa !6
  %200 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %sub196 = sub nsw i32 %199, %200
  %201 = load i32, i32* %l144, align 4, !tbaa !6
  %add197 = add nsw i32 %sub196, %201
  %add198 = add nsw i32 %add197, 4
  %add199 = add nsw i32 %mul195, %add198
  %arrayidx200 = getelementptr inbounds i16, i16* %193, i32 %add199
  store i16* %arrayidx200, i16** %p, align 4, !tbaa !2
  %202 = load i32, i32* %sum169, align 4, !tbaa !6
  %203 = load i32, i32* %reduce_bits_vert, align 4, !tbaa !6
  %shl201 = shl i32 1, %203
  %shr202 = ashr i32 %shl201, 1
  %add203 = add nsw i32 %202, %shr202
  %204 = load i32, i32* %reduce_bits_vert, align 4, !tbaa !6
  %shr204 = ashr i32 %add203, %204
  store i32 %shr204, i32* %sum169, align 4, !tbaa !6
  %205 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %205, i32 0, i32 0
  %206 = load i32, i32* %do_average, align 4, !tbaa !23
  %tobool205 = icmp ne i32 %206, 0
  br i1 %tobool205, label %if.then206, label %if.else239

if.then206:                                       ; preds = %if.then
  %207 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %207) #6
  %208 = load i16*, i16** %pred.addr, align 4, !tbaa !2
  %209 = load i32, i32* %i, align 4, !tbaa !6
  %210 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %sub207 = sub nsw i32 %209, %210
  %211 = load i32, i32* %k122, align 4, !tbaa !6
  %add208 = add nsw i32 %sub207, %211
  %add209 = add nsw i32 %add208, 4
  %212 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %mul210 = mul nsw i32 %add209, %212
  %213 = load i32, i32* %j, align 4, !tbaa !6
  %214 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %sub211 = sub nsw i32 %213, %214
  %215 = load i32, i32* %l144, align 4, !tbaa !6
  %add212 = add nsw i32 %sub211, %215
  %add213 = add nsw i32 %add212, 4
  %add214 = add nsw i32 %mul210, %add213
  %arrayidx215 = getelementptr inbounds i16, i16* %208, i32 %add214
  store i16* %arrayidx215, i16** %dst16, align 4, !tbaa !2
  %216 = bitcast i32* %tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %216) #6
  %217 = load i16*, i16** %p, align 4, !tbaa !2
  %218 = load i16, i16* %217, align 2, !tbaa !12
  %conv216 = zext i16 %218 to i32
  store i32 %conv216, i32* %tmp32, align 4, !tbaa !6
  %219 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %219, i32 0, i32 8
  %220 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !24
  %tobool217 = icmp ne i32 %220, 0
  br i1 %tobool217, label %if.then218, label %if.else

if.then218:                                       ; preds = %if.then206
  %221 = load i32, i32* %tmp32, align 4, !tbaa !6
  %222 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %222, i32 0, i32 9
  %223 = load i32, i32* %fwd_offset, align 4, !tbaa !25
  %mul219 = mul nsw i32 %221, %223
  %224 = load i32, i32* %sum169, align 4, !tbaa !6
  %225 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %225, i32 0, i32 10
  %226 = load i32, i32* %bck_offset, align 4, !tbaa !26
  %mul220 = mul nsw i32 %224, %226
  %add221 = add nsw i32 %mul219, %mul220
  store i32 %add221, i32* %tmp32, align 4, !tbaa !6
  %227 = load i32, i32* %tmp32, align 4, !tbaa !6
  %shr222 = ashr i32 %227, 4
  store i32 %shr222, i32* %tmp32, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then206
  %228 = load i32, i32* %sum169, align 4, !tbaa !6
  %229 = load i32, i32* %tmp32, align 4, !tbaa !6
  %add223 = add nsw i32 %229, %228
  store i32 %add223, i32* %tmp32, align 4, !tbaa !6
  %230 = load i32, i32* %tmp32, align 4, !tbaa !6
  %shr224 = ashr i32 %230, 1
  store i32 %shr224, i32* %tmp32, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then218
  %231 = load i32, i32* %tmp32, align 4, !tbaa !6
  %232 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %233 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1225 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %233, i32 0, i32 4
  %234 = load i32, i32* %round_1225, align 4, !tbaa !20
  %sub226 = sub nsw i32 %232, %234
  %shl227 = shl i32 1, %sub226
  %sub228 = sub nsw i32 %231, %shl227
  %235 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %236 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1229 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %236, i32 0, i32 4
  %237 = load i32, i32* %round_1229, align 4, !tbaa !20
  %sub230 = sub nsw i32 %235, %237
  %sub231 = sub nsw i32 %sub230, 1
  %shl232 = shl i32 1, %sub231
  %sub233 = sub nsw i32 %sub228, %shl232
  store i32 %sub233, i32* %tmp32, align 4, !tbaa !6
  %238 = load i32, i32* %tmp32, align 4, !tbaa !6
  %239 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl234 = shl i32 1, %239
  %shr235 = ashr i32 %shl234, 1
  %add236 = add nsw i32 %238, %shr235
  %240 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr237 = ashr i32 %add236, %240
  %241 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call238 = call zeroext i16 @clip_pixel_highbd(i32 %shr237, i32 %241)
  %242 = load i16*, i16** %dst16, align 4, !tbaa !2
  store i16 %call238, i16* %242, align 2, !tbaa !12
  %243 = bitcast i32* %tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #6
  %244 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #6
  br label %if.end241

if.else239:                                       ; preds = %if.then
  %245 = load i32, i32* %sum169, align 4, !tbaa !6
  %conv240 = trunc i32 %245 to i16
  %246 = load i16*, i16** %p, align 4, !tbaa !2
  store i16 %conv240, i16* %246, align 2, !tbaa !12
  br label %if.end241

if.end241:                                        ; preds = %if.else239, %if.end
  %247 = bitcast i16** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #6
  br label %if.end263

if.else242:                                       ; preds = %for.end189
  %248 = bitcast i16** %p243 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %248) #6
  %249 = load i16*, i16** %pred.addr, align 4, !tbaa !2
  %250 = load i32, i32* %i, align 4, !tbaa !6
  %251 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %sub244 = sub nsw i32 %250, %251
  %252 = load i32, i32* %k122, align 4, !tbaa !6
  %add245 = add nsw i32 %sub244, %252
  %add246 = add nsw i32 %add245, 4
  %253 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %mul247 = mul nsw i32 %add246, %253
  %254 = load i32, i32* %j, align 4, !tbaa !6
  %255 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %sub248 = sub nsw i32 %254, %255
  %256 = load i32, i32* %l144, align 4, !tbaa !6
  %add249 = add nsw i32 %sub248, %256
  %add250 = add nsw i32 %add249, 4
  %add251 = add nsw i32 %mul247, %add250
  %arrayidx252 = getelementptr inbounds i16, i16* %249, i32 %add251
  store i16* %arrayidx252, i16** %p243, align 4, !tbaa !2
  %257 = load i32, i32* %sum169, align 4, !tbaa !6
  %258 = load i32, i32* %reduce_bits_vert, align 4, !tbaa !6
  %shl253 = shl i32 1, %258
  %shr254 = ashr i32 %shl253, 1
  %add255 = add nsw i32 %257, %shr254
  %259 = load i32, i32* %reduce_bits_vert, align 4, !tbaa !6
  %shr256 = ashr i32 %add255, %259
  store i32 %shr256, i32* %sum169, align 4, !tbaa !6
  %260 = load i32, i32* %sum169, align 4, !tbaa !6
  %261 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub257 = sub nsw i32 %261, 1
  %shl258 = shl i32 1, %sub257
  %sub259 = sub nsw i32 %260, %shl258
  %262 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %shl260 = shl i32 1, %262
  %sub261 = sub nsw i32 %sub259, %shl260
  %263 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call262 = call zeroext i16 @clip_pixel_highbd(i32 %sub261, i32 %263)
  %264 = load i16*, i16** %p243, align 4, !tbaa !2
  store i16 %call262, i16* %264, align 2, !tbaa !12
  %265 = bitcast i16** %p243 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #6
  br label %if.end263

if.end263:                                        ; preds = %if.else242, %if.end241
  %266 = load i16, i16* %gamma.addr, align 2, !tbaa !12
  %conv264 = sext i16 %266 to i32
  %267 = load i32, i32* %sy, align 4, !tbaa !6
  %add265 = add nsw i32 %267, %conv264
  store i32 %add265, i32* %sy, align 4, !tbaa !6
  %268 = bitcast i32* %sum169 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #6
  %269 = bitcast i16** %coeffs166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #6
  %270 = bitcast i32* %offs162 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %270) #6
  br label %for.inc266

for.inc266:                                       ; preds = %if.end263
  %271 = load i32, i32* %l144, align 4, !tbaa !6
  %inc267 = add nsw i32 %271, 1
  store i32 %inc267, i32* %l144, align 4, !tbaa !6
  br label %for.cond145

for.end268:                                       ; preds = %for.cond.cleanup160
  %272 = bitcast i32* %sy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #6
  br label %for.inc269

for.inc269:                                       ; preds = %for.end268
  %273 = load i32, i32* %k122, align 4, !tbaa !6
  %inc270 = add nsw i32 %273, 1
  store i32 %inc270, i32* %k122, align 4, !tbaa !6
  br label %for.cond123

for.end271:                                       ; preds = %for.cond.cleanup138
  %274 = bitcast i32* %sy4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #6
  %275 = bitcast i32* %iy4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #6
  %276 = bitcast i32* %sx4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #6
  %277 = bitcast i32* %ix4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #6
  %278 = bitcast i32* %y4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #6
  %279 = bitcast i32* %x4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #6
  %280 = bitcast i32* %dst_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #6
  %281 = bitcast i32* %dst_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #6
  %282 = bitcast i32* %src_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #6
  %283 = bitcast i32* %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %283) #6
  br label %for.inc272

for.inc272:                                       ; preds = %for.end271
  %284 = load i32, i32* %j, align 4, !tbaa !6
  %add273 = add nsw i32 %284, 8
  store i32 %add273, i32* %j, align 4, !tbaa !6
  br label %for.cond29

for.end274:                                       ; preds = %for.cond.cleanup32
  br label %for.inc275

for.inc275:                                       ; preds = %for.end274
  %285 = load i32, i32* %i, align 4, !tbaa !6
  %add276 = add nsw i32 %285, 8
  store i32 %add276, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end277:                                       ; preds = %for.cond.cleanup
  %286 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %286) #6
  %287 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #6
  %288 = bitcast i32* %offset_bits_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %288) #6
  %289 = bitcast i32* %offset_bits_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %289) #6
  %290 = bitcast i32* %max_bits_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %290) #6
  %291 = bitcast i32* %reduce_bits_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %291) #6
  %292 = bitcast i32* %reduce_bits_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %292) #6
  %293 = bitcast [120 x i32]* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 480, i8* %293) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @clip_pixel_highbd(i32 %val, i32 %bd) #2 {
entry:
  %retval = alloca i16, align 2
  %val.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 8, label %sw.bb
    i32 10, label %sw.bb1
    i32 12, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb
  %1 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call = call i32 @clamp(i32 %1, i32 0, i32 255)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %retval, align 2
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call2 = call i32 @clamp(i32 %2, i32 0, i32 1023)
  %conv3 = trunc i32 %call2 to i16
  store i16 %conv3, i16* %retval, align 2
  br label %return

sw.bb4:                                           ; preds = %entry
  %3 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call5 = call i32 @clamp(i32 %3, i32 0, i32 4095)
  %conv6 = trunc i32 %call5 to i16
  store i16 %conv6, i16* %retval, align 2
  br label %return

return:                                           ; preds = %sw.bb4, %sw.bb1, %sw.default
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: nounwind
define hidden void @highbd_warp_plane(%struct.WarpedMotionParams* %wm, i16* %ref, i32 %width, i32 %height, i32 %stride, i16* %pred, i32 %p_col, i32 %p_row, i32 %p_width, i32 %p_height, i32 %p_stride, i32 %subsampling_x, i32 %subsampling_y, i32 %bd, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %wm.addr = alloca %struct.WarpedMotionParams*, align 4
  %ref.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %pred.addr = alloca i16*, align 4
  %p_col.addr = alloca i32, align 4
  %p_row.addr = alloca i32, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %mat = alloca i32*, align 4
  %alpha = alloca i16, align 2
  %beta = alloca i16, align 2
  %gamma = alloca i16, align 2
  %delta = alloca i16, align 2
  store %struct.WarpedMotionParams* %wm, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  store i16* %ref, i16** %ref.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %pred, i16** %pred.addr, align 4, !tbaa !2
  store i32 %p_col, i32* %p_col.addr, align 4, !tbaa !6
  store i32 %p_row, i32* %p_row.addr, align 4, !tbaa !6
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmtype = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %0, i32 0, i32 5
  %1 = load i8, i8* %wmtype, align 4, !tbaa !27
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat, i32 0, i32 2
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %4 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat2 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat2, i32 0, i32 5
  store i32 %3, i32* %arrayidx3, align 4, !tbaa !6
  %5 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat4 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %5, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat4, i32 0, i32 3
  %6 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %sub = sub nsw i32 0, %6
  %7 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat6 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat6, i32 0, i32 4
  store i32 %sub, i32* %arrayidx7, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat8 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %9, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat8, i32 0, i32 0
  store i32* %arraydecay, i32** %mat, align 4, !tbaa !2
  %10 = bitcast i16* %alpha to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %10) #6
  %11 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %alpha9 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %11, i32 0, i32 1
  %12 = load i16, i16* %alpha9, align 4, !tbaa !8
  store i16 %12, i16* %alpha, align 2, !tbaa !12
  %13 = bitcast i16* %beta to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #6
  %14 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %beta10 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %14, i32 0, i32 2
  %15 = load i16, i16* %beta10, align 2, !tbaa !11
  store i16 %15, i16* %beta, align 2, !tbaa !12
  %16 = bitcast i16* %gamma to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #6
  %17 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %gamma11 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %17, i32 0, i32 3
  %18 = load i16, i16* %gamma11, align 4, !tbaa !15
  store i16 %18, i16* %gamma, align 2, !tbaa !12
  %19 = bitcast i16* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %19) #6
  %20 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %delta12 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %20, i32 0, i32 4
  %21 = load i16, i16* %delta12, align 2, !tbaa !16
  store i16 %21, i16* %delta, align 2, !tbaa !12
  %22 = load i32*, i32** %mat, align 4, !tbaa !2
  %23 = load i16*, i16** %ref.addr, align 4, !tbaa !2
  %24 = load i32, i32* %width.addr, align 4, !tbaa !6
  %25 = load i32, i32* %height.addr, align 4, !tbaa !6
  %26 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %27 = load i16*, i16** %pred.addr, align 4, !tbaa !2
  %28 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %29 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %30 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %31 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %32 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %33 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %34 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %35 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %36 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %37 = load i16, i16* %alpha, align 2, !tbaa !12
  %38 = load i16, i16* %beta, align 2, !tbaa !12
  %39 = load i16, i16* %gamma, align 2, !tbaa !12
  %40 = load i16, i16* %delta, align 2, !tbaa !12
  call void @av1_highbd_warp_affine_c(i32* %22, i16* %23, i32 %24, i32 %25, i32 %26, i16* %27, i32 %28, i32 %29, i32 %30, i32 %31, i32 %32, i32 %33, i32 %34, i32 %35, %struct.ConvolveParams* %36, i16 signext %37, i16 signext %38, i16 signext %39, i16 signext %40)
  %41 = bitcast i16* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %41) #6
  %42 = bitcast i16* %gamma to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %42) #6
  %43 = bitcast i16* %beta to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %43) #6
  %44 = bitcast i16* %alpha to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %44) #6
  %45 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  ret void
}

; Function Attrs: nounwind
define hidden i64 @av1_calc_highbd_frame_error(i16* %ref, i32 %stride, i16* %dst, i32 %p_width, i32 %p_height, i32 %p_stride, i32 %bd) #0 {
entry:
  %ref.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %sum_error = alloca i64, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  store i16* %ref, i16** %ref.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i64* %sum_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  store i64 0, i64* %sum_error, align 8, !tbaa !13
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc11, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  br label %for.end13

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %7 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %6, %7
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %9 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %11, %12
  %add = add nsw i32 %10, %mul
  %arrayidx = getelementptr inbounds i16, i16* %9, i32 %add
  %13 = load i16, i16* %arrayidx, align 2, !tbaa !12
  %conv = zext i16 %13 to i32
  %14 = load i16*, i16** %ref.addr, align 4, !tbaa !2
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 %16, %17
  %add6 = add nsw i32 %15, %mul5
  %arrayidx7 = getelementptr inbounds i16, i16* %14, i32 %add6
  %18 = load i16, i16* %arrayidx7, align 2, !tbaa !12
  %conv8 = zext i16 %18 to i32
  %sub = sub nsw i32 %conv, %conv8
  %19 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call i32 @highbd_error_measure(i32 %sub, i32 %19)
  %conv9 = sext i32 %call to i64
  %20 = load i64, i64* %sum_error, align 8, !tbaa !13
  %add10 = add nsw i64 %20, %conv9
  store i64 %add10, i64* %sum_error, align 8, !tbaa !13
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc11

for.inc11:                                        ; preds = %for.end
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %inc12 = add nsw i32 %22, 1
  store i32 %inc12, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end13:                                        ; preds = %for.cond.cleanup
  %23 = load i64, i64* %sum_error, align 8, !tbaa !13
  store i32 1, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i64* %sum_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %24) #6
  ret i64 %23
}

; Function Attrs: inlinehint nounwind
define internal i32 @highbd_error_measure(i32 %err, i32 %bd) #2 {
entry:
  %err.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %b = alloca i32, align 4
  %bmask = alloca i32, align 4
  %v = alloca i32, align 4
  %e1 = alloca i32, align 4
  %e2 = alloca i32, align 4
  store i32 %err, i32* %err.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %1, 8
  store i32 %sub, i32* %b, align 4, !tbaa !6
  %2 = bitcast i32* %bmask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %b, align 4, !tbaa !6
  %shl = shl i32 1, %3
  %sub1 = sub nsw i32 %shl, 1
  store i32 %sub1, i32* %bmask, align 4, !tbaa !6
  %4 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32, i32* %b, align 4, !tbaa !6
  %shl2 = shl i32 1, %5
  store i32 %shl2, i32* %v, align 4, !tbaa !6
  %6 = load i32, i32* %err.addr, align 4, !tbaa !6
  %call = call i32 @abs(i32 %6) #7
  store i32 %call, i32* %err.addr, align 4, !tbaa !6
  %7 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i32, i32* %err.addr, align 4, !tbaa !6
  %9 = load i32, i32* %b, align 4, !tbaa !6
  %shr = ashr i32 %8, %9
  store i32 %shr, i32* %e1, align 4, !tbaa !6
  %10 = bitcast i32* %e2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load i32, i32* %err.addr, align 4, !tbaa !6
  %12 = load i32, i32* %bmask, align 4, !tbaa !6
  %and = and i32 %11, %12
  store i32 %and, i32* %e2, align 4, !tbaa !6
  %13 = load i32, i32* %e1, align 4, !tbaa !6
  %add = add nsw i32 255, %13
  %arrayidx = getelementptr inbounds [512 x i32], [512 x i32]* @error_measure_lut, i32 0, i32 %add
  %14 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %15 = load i32, i32* %v, align 4, !tbaa !6
  %16 = load i32, i32* %e2, align 4, !tbaa !6
  %sub3 = sub nsw i32 %15, %16
  %mul = mul nsw i32 %14, %sub3
  %17 = load i32, i32* %e1, align 4, !tbaa !6
  %add4 = add nsw i32 256, %17
  %arrayidx5 = getelementptr inbounds [512 x i32], [512 x i32]* @error_measure_lut, i32 0, i32 %add4
  %18 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %19 = load i32, i32* %e2, align 4, !tbaa !6
  %mul6 = mul nsw i32 %18, %19
  %add7 = add nsw i32 %mul, %mul6
  %20 = bitcast i32* %e2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast i32* %bmask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  %24 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  ret i32 %add7
}

; Function Attrs: nounwind
define hidden void @av1_warp_affine_c(i32* %mat, i8* %ref, i32 %width, i32 %height, i32 %stride, i8* %pred, i32 %p_col, i32 %p_row, i32 %p_width, i32 %p_height, i32 %p_stride, i32 %subsampling_x, i32 %subsampling_y, %struct.ConvolveParams* %conv_params, i16 signext %alpha, i16 signext %beta, i16 signext %gamma, i16 signext %delta) #0 {
entry:
  %mat.addr = alloca i32*, align 4
  %ref.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %pred.addr = alloca i8*, align 4
  %p_col.addr = alloca i32, align 4
  %p_row.addr = alloca i32, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %alpha.addr = alloca i16, align 2
  %beta.addr = alloca i16, align 2
  %gamma.addr = alloca i16, align 2
  %delta.addr = alloca i16, align 2
  %tmp = alloca [120 x i32], align 16
  %bd = alloca i32, align 4
  %reduce_bits_horiz = alloca i32, align 4
  %reduce_bits_vert = alloca i32, align 4
  %max_bits_horiz = alloca i32, align 4
  %offset_bits_horiz = alloca i32, align 4
  %offset_bits_vert = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %src_x = alloca i32, align 4
  %src_y = alloca i32, align 4
  %dst_x = alloca i32, align 4
  %dst_y = alloca i32, align 4
  %x4 = alloca i32, align 4
  %y4 = alloca i32, align 4
  %ix4 = alloca i32, align 4
  %sx4 = alloca i32, align 4
  %iy4 = alloca i32, align 4
  %sy4 = alloca i32, align 4
  %k = alloca i32, align 4
  %iy = alloca i32, align 4
  %sx = alloca i32, align 4
  %l = alloca i32, align 4
  %ix = alloca i32, align 4
  %offs = alloca i32, align 4
  %coeffs = alloca i16*, align 4
  %sum = alloca i32, align 4
  %m = alloca i32, align 4
  %sample_x = alloca i32, align 4
  %k101 = alloca i32, align 4
  %sy = alloca i32, align 4
  %l123 = alloca i32, align 4
  %offs141 = alloca i32, align 4
  %coeffs145 = alloca i16*, align 4
  %sum148 = alloca i32, align 4
  %m150 = alloca i32, align 4
  %p = alloca i16*, align 4
  %dst8 = alloca i8*, align 4
  %tmp32 = alloca i32, align 4
  %p222 = alloca i8*, align 4
  store i32* %mat, i32** %mat.addr, align 4, !tbaa !2
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %pred, i8** %pred.addr, align 4, !tbaa !2
  store i32 %p_col, i32* %p_col.addr, align 4, !tbaa !6
  store i32 %p_row, i32* %p_row.addr, align 4, !tbaa !6
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i16 %alpha, i16* %alpha.addr, align 2, !tbaa !12
  store i16 %beta, i16* %beta.addr, align 2, !tbaa !12
  store i16 %gamma, i16* %gamma.addr, align 2, !tbaa !12
  store i16 %delta, i16* %delta.addr, align 2, !tbaa !12
  %0 = bitcast [120 x i32]* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 480, i8* %0) #6
  %1 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 8, i32* %bd, align 4, !tbaa !6
  %2 = bitcast i32* %reduce_bits_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %3, i32 0, i32 3
  %4 = load i32, i32* %round_0, align 4, !tbaa !17
  store i32 %4, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %5 = bitcast i32* %reduce_bits_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %6, i32 0, i32 6
  %7 = load i32, i32* %is_compound, align 4, !tbaa !19
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %8, i32 0, i32 4
  %9 = load i32, i32* %round_1, align 4, !tbaa !20
  br label %cond.end

cond.false:                                       ; preds = %entry
  %10 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %sub = sub nsw i32 14, %10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %9, %cond.true ], [ %sub, %cond.false ]
  store i32 %cond, i32* %reduce_bits_vert, align 4, !tbaa !6
  %11 = bitcast i32* %max_bits_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %sub1 = sub nsw i32 16, %12
  store i32 %sub1, i32* %max_bits_horiz, align 4, !tbaa !6
  %13 = bitcast i32* %offset_bits_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 14, i32* %offset_bits_horiz, align 4, !tbaa !6
  %14 = bitcast i32* %offset_bits_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %sub2 = sub nsw i32 22, %15
  store i32 %sub2, i32* %offset_bits_vert, align 4, !tbaa !6
  %16 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_03 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %17, i32 0, i32 3
  %18 = load i32, i32* %round_03, align 4, !tbaa !17
  %sub4 = sub nsw i32 14, %18
  %19 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_15 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %19, i32 0, i32 4
  %20 = load i32, i32* %round_15, align 4, !tbaa !20
  %sub6 = sub nsw i32 %sub4, %20
  store i32 %sub6, i32* %round_bits, align 4, !tbaa !6
  %21 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_07 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %22, i32 0, i32 3
  %23 = load i32, i32* %round_07, align 4, !tbaa !17
  %sub8 = sub nsw i32 22, %23
  store i32 %sub8, i32* %offset_bits, align 4, !tbaa !6
  %24 = load i32, i32* %max_bits_horiz, align 4, !tbaa !6
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  %26 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  store i32 %26, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc251, %cond.end
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %28 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %29 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %add = add nsw i32 %28, %29
  %cmp = icmp slt i32 %27, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  br label %for.end253

for.body:                                         ; preds = %for.cond
  %31 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  store i32 %32, i32* %j, align 4, !tbaa !6
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc248, %for.body
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %34 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %35 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %add10 = add nsw i32 %34, %35
  %cmp11 = icmp slt i32 %33, %add10
  br i1 %cmp11, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond9
  store i32 5, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  br label %for.end250

for.body13:                                       ; preds = %for.cond9
  %37 = bitcast i32* %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #6
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %add14 = add nsw i32 %38, 4
  %39 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %shl = shl i32 %add14, %39
  store i32 %shl, i32* %src_x, align 4, !tbaa !6
  %40 = bitcast i32* %src_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %add15 = add nsw i32 %41, 4
  %42 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %shl16 = shl i32 %add15, %42
  store i32 %shl16, i32* %src_y, align 4, !tbaa !6
  %43 = bitcast i32* %dst_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  %44 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %44, i32 2
  %45 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %46 = load i32, i32* %src_x, align 4, !tbaa !6
  %mul = mul nsw i32 %45, %46
  %47 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %47, i32 3
  %48 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %49 = load i32, i32* %src_y, align 4, !tbaa !6
  %mul18 = mul nsw i32 %48, %49
  %add19 = add nsw i32 %mul, %mul18
  %50 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %50, i32 0
  %51 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %add21 = add nsw i32 %add19, %51
  store i32 %add21, i32* %dst_x, align 4, !tbaa !6
  %52 = bitcast i32* %dst_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i32, i32* %53, i32 4
  %54 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  %55 = load i32, i32* %src_x, align 4, !tbaa !6
  %mul23 = mul nsw i32 %54, %55
  %56 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %56, i32 5
  %57 = load i32, i32* %arrayidx24, align 4, !tbaa !6
  %58 = load i32, i32* %src_y, align 4, !tbaa !6
  %mul25 = mul nsw i32 %57, %58
  %add26 = add nsw i32 %mul23, %mul25
  %59 = load i32*, i32** %mat.addr, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %59, i32 1
  %60 = load i32, i32* %arrayidx27, align 4, !tbaa !6
  %add28 = add nsw i32 %add26, %60
  store i32 %add28, i32* %dst_y, align 4, !tbaa !6
  %61 = bitcast i32* %x4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  %62 = load i32, i32* %dst_x, align 4, !tbaa !6
  %63 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %shr = ashr i32 %62, %63
  store i32 %shr, i32* %x4, align 4, !tbaa !6
  %64 = bitcast i32* %y4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #6
  %65 = load i32, i32* %dst_y, align 4, !tbaa !6
  %66 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %shr29 = ashr i32 %65, %66
  store i32 %shr29, i32* %y4, align 4, !tbaa !6
  %67 = bitcast i32* %ix4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #6
  %68 = load i32, i32* %x4, align 4, !tbaa !6
  %shr30 = ashr i32 %68, 16
  store i32 %shr30, i32* %ix4, align 4, !tbaa !6
  %69 = bitcast i32* %sx4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #6
  %70 = load i32, i32* %x4, align 4, !tbaa !6
  %and = and i32 %70, 65535
  store i32 %and, i32* %sx4, align 4, !tbaa !6
  %71 = bitcast i32* %iy4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  %72 = load i32, i32* %y4, align 4, !tbaa !6
  %shr31 = ashr i32 %72, 16
  store i32 %shr31, i32* %iy4, align 4, !tbaa !6
  %73 = bitcast i32* %sy4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  %74 = load i32, i32* %y4, align 4, !tbaa !6
  %and32 = and i32 %74, 65535
  store i32 %and32, i32* %sy4, align 4, !tbaa !6
  %75 = load i16, i16* %alpha.addr, align 2, !tbaa !12
  %conv = sext i16 %75 to i32
  %mul33 = mul nsw i32 %conv, -4
  %76 = load i16, i16* %beta.addr, align 2, !tbaa !12
  %conv34 = sext i16 %76 to i32
  %mul35 = mul nsw i32 %conv34, -4
  %add36 = add nsw i32 %mul33, %mul35
  %77 = load i32, i32* %sx4, align 4, !tbaa !6
  %add37 = add nsw i32 %77, %add36
  store i32 %add37, i32* %sx4, align 4, !tbaa !6
  %78 = load i16, i16* %gamma.addr, align 2, !tbaa !12
  %conv38 = sext i16 %78 to i32
  %mul39 = mul nsw i32 %conv38, -4
  %79 = load i16, i16* %delta.addr, align 2, !tbaa !12
  %conv40 = sext i16 %79 to i32
  %mul41 = mul nsw i32 %conv40, -4
  %add42 = add nsw i32 %mul39, %mul41
  %80 = load i32, i32* %sy4, align 4, !tbaa !6
  %add43 = add nsw i32 %80, %add42
  store i32 %add43, i32* %sy4, align 4, !tbaa !6
  %81 = load i32, i32* %sx4, align 4, !tbaa !6
  %and44 = and i32 %81, -64
  store i32 %and44, i32* %sx4, align 4, !tbaa !6
  %82 = load i32, i32* %sy4, align 4, !tbaa !6
  %and45 = and i32 %82, -64
  store i32 %and45, i32* %sy4, align 4, !tbaa !6
  %83 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #6
  store i32 -7, i32* %k, align 4, !tbaa !6
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc98, %for.body13
  %84 = load i32, i32* %k, align 4, !tbaa !6
  %cmp47 = icmp slt i32 %84, 8
  br i1 %cmp47, label %for.body50, label %for.cond.cleanup49

for.cond.cleanup49:                               ; preds = %for.cond46
  store i32 8, i32* %cleanup.dest.slot, align 4
  %85 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #6
  br label %for.end100

for.body50:                                       ; preds = %for.cond46
  %86 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #6
  %87 = load i32, i32* %iy4, align 4, !tbaa !6
  %88 = load i32, i32* %k, align 4, !tbaa !6
  %add51 = add nsw i32 %87, %88
  %89 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub52 = sub nsw i32 %89, 1
  %call = call i32 @clamp(i32 %add51, i32 0, i32 %sub52)
  store i32 %call, i32* %iy, align 4, !tbaa !6
  %90 = bitcast i32* %sx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  %91 = load i32, i32* %sx4, align 4, !tbaa !6
  %92 = load i16, i16* %beta.addr, align 2, !tbaa !12
  %conv53 = sext i16 %92 to i32
  %93 = load i32, i32* %k, align 4, !tbaa !6
  %add54 = add nsw i32 %93, 4
  %mul55 = mul nsw i32 %conv53, %add54
  %add56 = add nsw i32 %91, %mul55
  store i32 %add56, i32* %sx, align 4, !tbaa !6
  %94 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #6
  store i32 -4, i32* %l, align 4, !tbaa !6
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc95, %for.body50
  %95 = load i32, i32* %l, align 4, !tbaa !6
  %cmp58 = icmp slt i32 %95, 4
  br i1 %cmp58, label %for.body61, label %for.cond.cleanup60

for.cond.cleanup60:                               ; preds = %for.cond57
  store i32 11, i32* %cleanup.dest.slot, align 4
  %96 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #6
  br label %for.end97

for.body61:                                       ; preds = %for.cond57
  %97 = bitcast i32* %ix to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #6
  %98 = load i32, i32* %ix4, align 4, !tbaa !6
  %99 = load i32, i32* %l, align 4, !tbaa !6
  %add62 = add nsw i32 %98, %99
  %sub63 = sub nsw i32 %add62, 3
  store i32 %sub63, i32* %ix, align 4, !tbaa !6
  %100 = bitcast i32* %offs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #6
  %101 = load i32, i32* %sx, align 4, !tbaa !6
  %add64 = add nsw i32 %101, 512
  %shr65 = ashr i32 %add64, 10
  %add66 = add nsw i32 %shr65, 64
  store i32 %add66, i32* %offs, align 4, !tbaa !6
  %102 = bitcast i16** %coeffs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #6
  %103 = load i32, i32* %offs, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds [193 x [8 x i16]], [193 x [8 x i16]]* @av1_warped_filter, i32 0, i32 %103
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx67, i32 0, i32 0
  store i16* %arraydecay, i16** %coeffs, align 4, !tbaa !2
  %104 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #6
  store i32 16384, i32* %sum, align 4, !tbaa !6
  %105 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #6
  store i32 0, i32* %m, align 4, !tbaa !6
  br label %for.cond68

for.cond68:                                       ; preds = %for.inc, %for.body61
  %106 = load i32, i32* %m, align 4, !tbaa !6
  %cmp69 = icmp slt i32 %106, 8
  br i1 %cmp69, label %for.body72, label %for.cond.cleanup71

for.cond.cleanup71:                               ; preds = %for.cond68
  store i32 14, i32* %cleanup.dest.slot, align 4
  %107 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #6
  br label %for.end

for.body72:                                       ; preds = %for.cond68
  %108 = bitcast i32* %sample_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #6
  %109 = load i32, i32* %ix, align 4, !tbaa !6
  %110 = load i32, i32* %m, align 4, !tbaa !6
  %add73 = add nsw i32 %109, %110
  %111 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub74 = sub nsw i32 %111, 1
  %call75 = call i32 @clamp(i32 %add73, i32 0, i32 %sub74)
  store i32 %call75, i32* %sample_x, align 4, !tbaa !6
  %112 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %113 = load i32, i32* %iy, align 4, !tbaa !6
  %114 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul76 = mul nsw i32 %113, %114
  %115 = load i32, i32* %sample_x, align 4, !tbaa !6
  %add77 = add nsw i32 %mul76, %115
  %arrayidx78 = getelementptr inbounds i8, i8* %112, i32 %add77
  %116 = load i8, i8* %arrayidx78, align 1, !tbaa !28
  %conv79 = zext i8 %116 to i32
  %117 = load i16*, i16** %coeffs, align 4, !tbaa !2
  %118 = load i32, i32* %m, align 4, !tbaa !6
  %arrayidx80 = getelementptr inbounds i16, i16* %117, i32 %118
  %119 = load i16, i16* %arrayidx80, align 2, !tbaa !12
  %conv81 = sext i16 %119 to i32
  %mul82 = mul nsw i32 %conv79, %conv81
  %120 = load i32, i32* %sum, align 4, !tbaa !6
  %add83 = add nsw i32 %120, %mul82
  store i32 %add83, i32* %sum, align 4, !tbaa !6
  %121 = bitcast i32* %sample_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body72
  %122 = load i32, i32* %m, align 4, !tbaa !6
  %inc = add nsw i32 %122, 1
  store i32 %inc, i32* %m, align 4, !tbaa !6
  br label %for.cond68

for.end:                                          ; preds = %for.cond.cleanup71
  %123 = load i32, i32* %sum, align 4, !tbaa !6
  %124 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %shl84 = shl i32 1, %124
  %shr85 = ashr i32 %shl84, 1
  %add86 = add nsw i32 %123, %shr85
  %125 = load i32, i32* %reduce_bits_horiz, align 4, !tbaa !6
  %shr87 = ashr i32 %add86, %125
  store i32 %shr87, i32* %sum, align 4, !tbaa !6
  %126 = load i32, i32* %sum, align 4, !tbaa !6
  %127 = load i32, i32* %k, align 4, !tbaa !6
  %add88 = add nsw i32 %127, 7
  %mul89 = mul nsw i32 %add88, 8
  %128 = load i32, i32* %l, align 4, !tbaa !6
  %add90 = add nsw i32 %128, 4
  %add91 = add nsw i32 %mul89, %add90
  %arrayidx92 = getelementptr inbounds [120 x i32], [120 x i32]* %tmp, i32 0, i32 %add91
  store i32 %126, i32* %arrayidx92, align 4, !tbaa !6
  %129 = load i16, i16* %alpha.addr, align 2, !tbaa !12
  %conv93 = sext i16 %129 to i32
  %130 = load i32, i32* %sx, align 4, !tbaa !6
  %add94 = add nsw i32 %130, %conv93
  store i32 %add94, i32* %sx, align 4, !tbaa !6
  %131 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #6
  %132 = bitcast i16** %coeffs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #6
  %133 = bitcast i32* %offs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #6
  %134 = bitcast i32* %ix to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #6
  br label %for.inc95

for.inc95:                                        ; preds = %for.end
  %135 = load i32, i32* %l, align 4, !tbaa !6
  %inc96 = add nsw i32 %135, 1
  store i32 %inc96, i32* %l, align 4, !tbaa !6
  br label %for.cond57

for.end97:                                        ; preds = %for.cond.cleanup60
  %136 = bitcast i32* %sx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #6
  %137 = bitcast i32* %iy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #6
  br label %for.inc98

for.inc98:                                        ; preds = %for.end97
  %138 = load i32, i32* %k, align 4, !tbaa !6
  %inc99 = add nsw i32 %138, 1
  store i32 %inc99, i32* %k, align 4, !tbaa !6
  br label %for.cond46

for.end100:                                       ; preds = %for.cond.cleanup49
  %139 = bitcast i32* %k101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #6
  store i32 -4, i32* %k101, align 4, !tbaa !6
  br label %for.cond102

for.cond102:                                      ; preds = %for.inc245, %for.end100
  %140 = load i32, i32* %k101, align 4, !tbaa !6
  %141 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %142 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %add103 = add nsw i32 %141, %142
  %143 = load i32, i32* %i, align 4, !tbaa !6
  %sub104 = sub nsw i32 %add103, %143
  %sub105 = sub nsw i32 %sub104, 4
  %cmp106 = icmp slt i32 4, %sub105
  br i1 %cmp106, label %cond.true108, label %cond.false109

cond.true108:                                     ; preds = %for.cond102
  br label %cond.end113

cond.false109:                                    ; preds = %for.cond102
  %144 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %145 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %add110 = add nsw i32 %144, %145
  %146 = load i32, i32* %i, align 4, !tbaa !6
  %sub111 = sub nsw i32 %add110, %146
  %sub112 = sub nsw i32 %sub111, 4
  br label %cond.end113

cond.end113:                                      ; preds = %cond.false109, %cond.true108
  %cond114 = phi i32 [ 4, %cond.true108 ], [ %sub112, %cond.false109 ]
  %cmp115 = icmp slt i32 %140, %cond114
  br i1 %cmp115, label %for.body118, label %for.cond.cleanup117

for.cond.cleanup117:                              ; preds = %cond.end113
  store i32 17, i32* %cleanup.dest.slot, align 4
  %147 = bitcast i32* %k101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #6
  br label %for.end247

for.body118:                                      ; preds = %cond.end113
  %148 = bitcast i32* %sy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #6
  %149 = load i32, i32* %sy4, align 4, !tbaa !6
  %150 = load i16, i16* %delta.addr, align 2, !tbaa !12
  %conv119 = sext i16 %150 to i32
  %151 = load i32, i32* %k101, align 4, !tbaa !6
  %add120 = add nsw i32 %151, 4
  %mul121 = mul nsw i32 %conv119, %add120
  %add122 = add nsw i32 %149, %mul121
  store i32 %add122, i32* %sy, align 4, !tbaa !6
  %152 = bitcast i32* %l123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %152) #6
  store i32 -4, i32* %l123, align 4, !tbaa !6
  br label %for.cond124

for.cond124:                                      ; preds = %for.inc242, %for.body118
  %153 = load i32, i32* %l123, align 4, !tbaa !6
  %154 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %155 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %add125 = add nsw i32 %154, %155
  %156 = load i32, i32* %j, align 4, !tbaa !6
  %sub126 = sub nsw i32 %add125, %156
  %sub127 = sub nsw i32 %sub126, 4
  %cmp128 = icmp slt i32 4, %sub127
  br i1 %cmp128, label %cond.true130, label %cond.false131

cond.true130:                                     ; preds = %for.cond124
  br label %cond.end135

cond.false131:                                    ; preds = %for.cond124
  %157 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %158 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %add132 = add nsw i32 %157, %158
  %159 = load i32, i32* %j, align 4, !tbaa !6
  %sub133 = sub nsw i32 %add132, %159
  %sub134 = sub nsw i32 %sub133, 4
  br label %cond.end135

cond.end135:                                      ; preds = %cond.false131, %cond.true130
  %cond136 = phi i32 [ 4, %cond.true130 ], [ %sub134, %cond.false131 ]
  %cmp137 = icmp slt i32 %153, %cond136
  br i1 %cmp137, label %for.body140, label %for.cond.cleanup139

for.cond.cleanup139:                              ; preds = %cond.end135
  store i32 20, i32* %cleanup.dest.slot, align 4
  %160 = bitcast i32* %l123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #6
  br label %for.end244

for.body140:                                      ; preds = %cond.end135
  %161 = bitcast i32* %offs141 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %161) #6
  %162 = load i32, i32* %sy, align 4, !tbaa !6
  %add142 = add nsw i32 %162, 512
  %shr143 = ashr i32 %add142, 10
  %add144 = add nsw i32 %shr143, 64
  store i32 %add144, i32* %offs141, align 4, !tbaa !6
  %163 = bitcast i16** %coeffs145 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %163) #6
  %164 = load i32, i32* %offs141, align 4, !tbaa !6
  %arrayidx146 = getelementptr inbounds [193 x [8 x i16]], [193 x [8 x i16]]* @av1_warped_filter, i32 0, i32 %164
  %arraydecay147 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx146, i32 0, i32 0
  store i16* %arraydecay147, i16** %coeffs145, align 4, !tbaa !2
  %165 = bitcast i32* %sum148 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %165) #6
  %166 = load i32, i32* %offset_bits_vert, align 4, !tbaa !6
  %shl149 = shl i32 1, %166
  store i32 %shl149, i32* %sum148, align 4, !tbaa !6
  %167 = bitcast i32* %m150 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %167) #6
  store i32 0, i32* %m150, align 4, !tbaa !6
  br label %for.cond151

for.cond151:                                      ; preds = %for.inc166, %for.body140
  %168 = load i32, i32* %m150, align 4, !tbaa !6
  %cmp152 = icmp slt i32 %168, 8
  br i1 %cmp152, label %for.body155, label %for.cond.cleanup154

for.cond.cleanup154:                              ; preds = %for.cond151
  store i32 23, i32* %cleanup.dest.slot, align 4
  %169 = bitcast i32* %m150 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #6
  br label %for.end168

for.body155:                                      ; preds = %for.cond151
  %170 = load i32, i32* %k101, align 4, !tbaa !6
  %171 = load i32, i32* %m150, align 4, !tbaa !6
  %add156 = add nsw i32 %170, %171
  %add157 = add nsw i32 %add156, 4
  %mul158 = mul nsw i32 %add157, 8
  %172 = load i32, i32* %l123, align 4, !tbaa !6
  %add159 = add nsw i32 %172, 4
  %add160 = add nsw i32 %mul158, %add159
  %arrayidx161 = getelementptr inbounds [120 x i32], [120 x i32]* %tmp, i32 0, i32 %add160
  %173 = load i32, i32* %arrayidx161, align 4, !tbaa !6
  %174 = load i16*, i16** %coeffs145, align 4, !tbaa !2
  %175 = load i32, i32* %m150, align 4, !tbaa !6
  %arrayidx162 = getelementptr inbounds i16, i16* %174, i32 %175
  %176 = load i16, i16* %arrayidx162, align 2, !tbaa !12
  %conv163 = sext i16 %176 to i32
  %mul164 = mul nsw i32 %173, %conv163
  %177 = load i32, i32* %sum148, align 4, !tbaa !6
  %add165 = add nsw i32 %177, %mul164
  store i32 %add165, i32* %sum148, align 4, !tbaa !6
  br label %for.inc166

for.inc166:                                       ; preds = %for.body155
  %178 = load i32, i32* %m150, align 4, !tbaa !6
  %inc167 = add nsw i32 %178, 1
  store i32 %inc167, i32* %m150, align 4, !tbaa !6
  br label %for.cond151

for.end168:                                       ; preds = %for.cond.cleanup154
  %179 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound169 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %179, i32 0, i32 6
  %180 = load i32, i32* %is_compound169, align 4, !tbaa !19
  %tobool170 = icmp ne i32 %180, 0
  br i1 %tobool170, label %if.then, label %if.else221

if.then:                                          ; preds = %for.end168
  %181 = bitcast i16** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %181) #6
  %182 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %182, i32 0, i32 1
  %183 = load i16*, i16** %dst, align 4, !tbaa !21
  %184 = load i32, i32* %i, align 4, !tbaa !6
  %185 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %sub171 = sub nsw i32 %184, %185
  %186 = load i32, i32* %k101, align 4, !tbaa !6
  %add172 = add nsw i32 %sub171, %186
  %add173 = add nsw i32 %add172, 4
  %187 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %187, i32 0, i32 2
  %188 = load i32, i32* %dst_stride, align 4, !tbaa !22
  %mul174 = mul nsw i32 %add173, %188
  %189 = load i32, i32* %j, align 4, !tbaa !6
  %190 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %sub175 = sub nsw i32 %189, %190
  %191 = load i32, i32* %l123, align 4, !tbaa !6
  %add176 = add nsw i32 %sub175, %191
  %add177 = add nsw i32 %add176, 4
  %add178 = add nsw i32 %mul174, %add177
  %arrayidx179 = getelementptr inbounds i16, i16* %183, i32 %add178
  store i16* %arrayidx179, i16** %p, align 4, !tbaa !2
  %192 = load i32, i32* %sum148, align 4, !tbaa !6
  %193 = load i32, i32* %reduce_bits_vert, align 4, !tbaa !6
  %shl180 = shl i32 1, %193
  %shr181 = ashr i32 %shl180, 1
  %add182 = add nsw i32 %192, %shr181
  %194 = load i32, i32* %reduce_bits_vert, align 4, !tbaa !6
  %shr183 = ashr i32 %add182, %194
  store i32 %shr183, i32* %sum148, align 4, !tbaa !6
  %195 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %195, i32 0, i32 0
  %196 = load i32, i32* %do_average, align 4, !tbaa !23
  %tobool184 = icmp ne i32 %196, 0
  br i1 %tobool184, label %if.then185, label %if.else218

if.then185:                                       ; preds = %if.then
  %197 = bitcast i8** %dst8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %197) #6
  %198 = load i8*, i8** %pred.addr, align 4, !tbaa !2
  %199 = load i32, i32* %i, align 4, !tbaa !6
  %200 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %sub186 = sub nsw i32 %199, %200
  %201 = load i32, i32* %k101, align 4, !tbaa !6
  %add187 = add nsw i32 %sub186, %201
  %add188 = add nsw i32 %add187, 4
  %202 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %mul189 = mul nsw i32 %add188, %202
  %203 = load i32, i32* %j, align 4, !tbaa !6
  %204 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %sub190 = sub nsw i32 %203, %204
  %205 = load i32, i32* %l123, align 4, !tbaa !6
  %add191 = add nsw i32 %sub190, %205
  %add192 = add nsw i32 %add191, 4
  %add193 = add nsw i32 %mul189, %add192
  %arrayidx194 = getelementptr inbounds i8, i8* %198, i32 %add193
  store i8* %arrayidx194, i8** %dst8, align 4, !tbaa !2
  %206 = bitcast i32* %tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %206) #6
  %207 = load i16*, i16** %p, align 4, !tbaa !2
  %208 = load i16, i16* %207, align 2, !tbaa !12
  %conv195 = zext i16 %208 to i32
  store i32 %conv195, i32* %tmp32, align 4, !tbaa !6
  %209 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %209, i32 0, i32 8
  %210 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !24
  %tobool196 = icmp ne i32 %210, 0
  br i1 %tobool196, label %if.then197, label %if.else

if.then197:                                       ; preds = %if.then185
  %211 = load i32, i32* %tmp32, align 4, !tbaa !6
  %212 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %212, i32 0, i32 9
  %213 = load i32, i32* %fwd_offset, align 4, !tbaa !25
  %mul198 = mul nsw i32 %211, %213
  %214 = load i32, i32* %sum148, align 4, !tbaa !6
  %215 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %215, i32 0, i32 10
  %216 = load i32, i32* %bck_offset, align 4, !tbaa !26
  %mul199 = mul nsw i32 %214, %216
  %add200 = add nsw i32 %mul198, %mul199
  store i32 %add200, i32* %tmp32, align 4, !tbaa !6
  %217 = load i32, i32* %tmp32, align 4, !tbaa !6
  %shr201 = ashr i32 %217, 4
  store i32 %shr201, i32* %tmp32, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then185
  %218 = load i32, i32* %sum148, align 4, !tbaa !6
  %219 = load i32, i32* %tmp32, align 4, !tbaa !6
  %add202 = add nsw i32 %219, %218
  store i32 %add202, i32* %tmp32, align 4, !tbaa !6
  %220 = load i32, i32* %tmp32, align 4, !tbaa !6
  %shr203 = ashr i32 %220, 1
  store i32 %shr203, i32* %tmp32, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then197
  %221 = load i32, i32* %tmp32, align 4, !tbaa !6
  %222 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %223 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1204 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %223, i32 0, i32 4
  %224 = load i32, i32* %round_1204, align 4, !tbaa !20
  %sub205 = sub nsw i32 %222, %224
  %shl206 = shl i32 1, %sub205
  %sub207 = sub nsw i32 %221, %shl206
  %225 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %226 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1208 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %226, i32 0, i32 4
  %227 = load i32, i32* %round_1208, align 4, !tbaa !20
  %sub209 = sub nsw i32 %225, %227
  %sub210 = sub nsw i32 %sub209, 1
  %shl211 = shl i32 1, %sub210
  %sub212 = sub nsw i32 %sub207, %shl211
  store i32 %sub212, i32* %tmp32, align 4, !tbaa !6
  %228 = load i32, i32* %tmp32, align 4, !tbaa !6
  %229 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl213 = shl i32 1, %229
  %shr214 = ashr i32 %shl213, 1
  %add215 = add nsw i32 %228, %shr214
  %230 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr216 = ashr i32 %add215, %230
  %call217 = call zeroext i8 @clip_pixel(i32 %shr216)
  %231 = load i8*, i8** %dst8, align 4, !tbaa !2
  store i8 %call217, i8* %231, align 1, !tbaa !28
  %232 = bitcast i32* %tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #6
  %233 = bitcast i8** %dst8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #6
  br label %if.end220

if.else218:                                       ; preds = %if.then
  %234 = load i32, i32* %sum148, align 4, !tbaa !6
  %conv219 = trunc i32 %234 to i16
  %235 = load i16*, i16** %p, align 4, !tbaa !2
  store i16 %conv219, i16* %235, align 2, !tbaa !12
  br label %if.end220

if.end220:                                        ; preds = %if.else218, %if.end
  %236 = bitcast i16** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %236) #6
  br label %if.end239

if.else221:                                       ; preds = %for.end168
  %237 = bitcast i8** %p222 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %237) #6
  %238 = load i8*, i8** %pred.addr, align 4, !tbaa !2
  %239 = load i32, i32* %i, align 4, !tbaa !6
  %240 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %sub223 = sub nsw i32 %239, %240
  %241 = load i32, i32* %k101, align 4, !tbaa !6
  %add224 = add nsw i32 %sub223, %241
  %add225 = add nsw i32 %add224, 4
  %242 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %mul226 = mul nsw i32 %add225, %242
  %243 = load i32, i32* %j, align 4, !tbaa !6
  %244 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %sub227 = sub nsw i32 %243, %244
  %245 = load i32, i32* %l123, align 4, !tbaa !6
  %add228 = add nsw i32 %sub227, %245
  %add229 = add nsw i32 %add228, 4
  %add230 = add nsw i32 %mul226, %add229
  %arrayidx231 = getelementptr inbounds i8, i8* %238, i32 %add230
  store i8* %arrayidx231, i8** %p222, align 4, !tbaa !2
  %246 = load i32, i32* %sum148, align 4, !tbaa !6
  %247 = load i32, i32* %reduce_bits_vert, align 4, !tbaa !6
  %shl232 = shl i32 1, %247
  %shr233 = ashr i32 %shl232, 1
  %add234 = add nsw i32 %246, %shr233
  %248 = load i32, i32* %reduce_bits_vert, align 4, !tbaa !6
  %shr235 = ashr i32 %add234, %248
  store i32 %shr235, i32* %sum148, align 4, !tbaa !6
  %249 = load i32, i32* %sum148, align 4, !tbaa !6
  %sub236 = sub nsw i32 %249, 128
  %sub237 = sub nsw i32 %sub236, 256
  %call238 = call zeroext i8 @clip_pixel(i32 %sub237)
  %250 = load i8*, i8** %p222, align 4, !tbaa !2
  store i8 %call238, i8* %250, align 1, !tbaa !28
  %251 = bitcast i8** %p222 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #6
  br label %if.end239

if.end239:                                        ; preds = %if.else221, %if.end220
  %252 = load i16, i16* %gamma.addr, align 2, !tbaa !12
  %conv240 = sext i16 %252 to i32
  %253 = load i32, i32* %sy, align 4, !tbaa !6
  %add241 = add nsw i32 %253, %conv240
  store i32 %add241, i32* %sy, align 4, !tbaa !6
  %254 = bitcast i32* %sum148 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #6
  %255 = bitcast i16** %coeffs145 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #6
  %256 = bitcast i32* %offs141 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #6
  br label %for.inc242

for.inc242:                                       ; preds = %if.end239
  %257 = load i32, i32* %l123, align 4, !tbaa !6
  %inc243 = add nsw i32 %257, 1
  store i32 %inc243, i32* %l123, align 4, !tbaa !6
  br label %for.cond124

for.end244:                                       ; preds = %for.cond.cleanup139
  %258 = bitcast i32* %sy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #6
  br label %for.inc245

for.inc245:                                       ; preds = %for.end244
  %259 = load i32, i32* %k101, align 4, !tbaa !6
  %inc246 = add nsw i32 %259, 1
  store i32 %inc246, i32* %k101, align 4, !tbaa !6
  br label %for.cond102

for.end247:                                       ; preds = %for.cond.cleanup117
  %260 = bitcast i32* %sy4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #6
  %261 = bitcast i32* %iy4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #6
  %262 = bitcast i32* %sx4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #6
  %263 = bitcast i32* %ix4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #6
  %264 = bitcast i32* %y4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #6
  %265 = bitcast i32* %x4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #6
  %266 = bitcast i32* %dst_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #6
  %267 = bitcast i32* %dst_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #6
  %268 = bitcast i32* %src_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #6
  %269 = bitcast i32* %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #6
  br label %for.inc248

for.inc248:                                       ; preds = %for.end247
  %270 = load i32, i32* %j, align 4, !tbaa !6
  %add249 = add nsw i32 %270, 8
  store i32 %add249, i32* %j, align 4, !tbaa !6
  br label %for.cond9

for.end250:                                       ; preds = %for.cond.cleanup12
  br label %for.inc251

for.inc251:                                       ; preds = %for.end250
  %271 = load i32, i32* %i, align 4, !tbaa !6
  %add252 = add nsw i32 %271, 8
  store i32 %add252, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end253:                                       ; preds = %for.cond.cleanup
  %272 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #6
  %273 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #6
  %274 = bitcast i32* %offset_bits_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #6
  %275 = bitcast i32* %offset_bits_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #6
  %276 = bitcast i32* %max_bits_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #6
  %277 = bitcast i32* %reduce_bits_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #6
  %278 = bitcast i32* %reduce_bits_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #6
  %279 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #6
  %280 = bitcast [120 x i32]* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 480, i8* %280) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @clip_pixel(i32 %val) #2 {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  %0 = load i32, i32* %val.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %0, 255
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %1, 0
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %2 = load i32, i32* %val.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ 0, %cond.true2 ], [ %2, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ 255, %cond.true ], [ %cond, %cond.end ]
  %conv = trunc i32 %cond5 to i8
  ret i8 %conv
}

; Function Attrs: nounwind
define hidden void @warp_plane(%struct.WarpedMotionParams* %wm, i8* %ref, i32 %width, i32 %height, i32 %stride, i8* %pred, i32 %p_col, i32 %p_row, i32 %p_width, i32 %p_height, i32 %p_stride, i32 %subsampling_x, i32 %subsampling_y, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %wm.addr = alloca %struct.WarpedMotionParams*, align 4
  %ref.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %pred.addr = alloca i8*, align 4
  %p_col.addr = alloca i32, align 4
  %p_row.addr = alloca i32, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %mat = alloca i32*, align 4
  %alpha = alloca i16, align 2
  %beta = alloca i16, align 2
  %gamma = alloca i16, align 2
  %delta = alloca i16, align 2
  store %struct.WarpedMotionParams* %wm, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %pred, i8** %pred.addr, align 4, !tbaa !2
  store i32 %p_col, i32* %p_col.addr, align 4, !tbaa !6
  store i32 %p_row, i32* %p_row.addr, align 4, !tbaa !6
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmtype = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %0, i32 0, i32 5
  %1 = load i8, i8* %wmtype, align 4, !tbaa !27
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat, i32 0, i32 2
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %4 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat2 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat2, i32 0, i32 5
  store i32 %3, i32* %arrayidx3, align 4, !tbaa !6
  %5 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat4 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %5, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat4, i32 0, i32 3
  %6 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %sub = sub nsw i32 0, %6
  %7 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat6 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat6, i32 0, i32 4
  store i32 %sub, i32* %arrayidx7, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat8 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %9, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat8, i32 0, i32 0
  store i32* %arraydecay, i32** %mat, align 4, !tbaa !2
  %10 = bitcast i16* %alpha to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %10) #6
  %11 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %alpha9 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %11, i32 0, i32 1
  %12 = load i16, i16* %alpha9, align 4, !tbaa !8
  store i16 %12, i16* %alpha, align 2, !tbaa !12
  %13 = bitcast i16* %beta to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #6
  %14 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %beta10 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %14, i32 0, i32 2
  %15 = load i16, i16* %beta10, align 2, !tbaa !11
  store i16 %15, i16* %beta, align 2, !tbaa !12
  %16 = bitcast i16* %gamma to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #6
  %17 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %gamma11 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %17, i32 0, i32 3
  %18 = load i16, i16* %gamma11, align 4, !tbaa !15
  store i16 %18, i16* %gamma, align 2, !tbaa !12
  %19 = bitcast i16* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %19) #6
  %20 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %delta12 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %20, i32 0, i32 4
  %21 = load i16, i16* %delta12, align 2, !tbaa !16
  store i16 %21, i16* %delta, align 2, !tbaa !12
  %22 = load i32*, i32** %mat, align 4, !tbaa !2
  %23 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %24 = load i32, i32* %width.addr, align 4, !tbaa !6
  %25 = load i32, i32* %height.addr, align 4, !tbaa !6
  %26 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %27 = load i8*, i8** %pred.addr, align 4, !tbaa !2
  %28 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %29 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %30 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %31 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %32 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %33 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %34 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %35 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %36 = load i16, i16* %alpha, align 2, !tbaa !12
  %37 = load i16, i16* %beta, align 2, !tbaa !12
  %38 = load i16, i16* %gamma, align 2, !tbaa !12
  %39 = load i16, i16* %delta, align 2, !tbaa !12
  call void @av1_warp_affine_c(i32* %22, i8* %23, i32 %24, i32 %25, i32 %26, i8* %27, i32 %28, i32 %29, i32 %30, i32 %31, i32 %32, i32 %33, i32 %34, %struct.ConvolveParams* %35, i16 signext %36, i16 signext %37, i16 signext %38, i16 signext %39)
  %40 = bitcast i16* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %40) #6
  %41 = bitcast i16* %gamma to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %41) #6
  %42 = bitcast i16* %beta to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %42) #6
  %43 = bitcast i16* %alpha to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %43) #6
  %44 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  ret void
}

; Function Attrs: nounwind
define hidden i64 @av1_calc_frame_error_c(i8* %ref, i32 %stride, i8* %dst, i32 %p_width, i32 %p_height, i32 %p_stride) #0 {
entry:
  %ref.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %sum_error = alloca i64, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  %0 = bitcast i64* %sum_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  store i64 0, i64* %sum_error, align 8, !tbaa !13
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc11, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  br label %for.end13

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %7 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %6, %7
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %9 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %11, %12
  %add = add nsw i32 %10, %mul
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %add
  %13 = load i8, i8* %arrayidx, align 1, !tbaa !28
  %conv = zext i8 %13 to i32
  %14 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 %16, %17
  %add6 = add nsw i32 %15, %mul5
  %arrayidx7 = getelementptr inbounds i8, i8* %14, i32 %add6
  %18 = load i8, i8* %arrayidx7, align 1, !tbaa !28
  %conv8 = zext i8 %18 to i32
  %sub = sub nsw i32 %conv, %conv8
  %call = call i32 @error_measure(i32 %sub)
  %conv9 = sext i32 %call to i64
  %19 = load i64, i64* %sum_error, align 8, !tbaa !13
  %add10 = add nsw i64 %19, %conv9
  store i64 %add10, i64* %sum_error, align 8, !tbaa !13
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %20 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc11

for.inc11:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %inc12 = add nsw i32 %21, 1
  store i32 %inc12, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end13:                                        ; preds = %for.cond.cleanup
  %22 = load i64, i64* %sum_error, align 8, !tbaa !13
  store i32 1, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i64* %sum_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #6
  ret i64 %22
}

; Function Attrs: inlinehint nounwind
define internal i32 @error_measure(i32 %err) #2 {
entry:
  %err.addr = alloca i32, align 4
  store i32 %err, i32* %err.addr, align 4, !tbaa !6
  %0 = load i32, i32* %err.addr, align 4, !tbaa !6
  %add = add nsw i32 255, %0
  %arrayidx = getelementptr inbounds [512 x i32], [512 x i32]* @error_measure_lut, i32 0, i32 %add
  %1 = load i32, i32* %arrayidx, align 4, !tbaa !6
  ret i32 %1
}

; Function Attrs: nounwind
define hidden i64 @av1_frame_error(i32 %use_hbd, i32 %bd, i8* %ref, i32 %stride, i8* %dst, i32 %p_width, i32 %p_height, i32 %p_stride) #0 {
entry:
  %retval = alloca i64, align 8
  %use_hbd.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %ref.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  store i32 %use_hbd, i32* %use_hbd.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  %0 = load i32, i32* %use_hbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %2 = ptrtoint i8* %1 to i32
  %shl = shl i32 %2, 1
  %3 = inttoptr i32 %shl to i16*
  %4 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %5 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %6 = ptrtoint i8* %5 to i32
  %shl1 = shl i32 %6, 1
  %7 = inttoptr i32 %shl1 to i16*
  %8 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %9 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %10 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %11 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call i64 @av1_calc_highbd_frame_error(i16* %3, i32 %4, i16* %7, i32 %8, i32 %9, i32 %10, i32 %11)
  store i64 %call, i64* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %12 = load i32, i32* %use_hbd.addr, align 4, !tbaa !6
  %13 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %14 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %15 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %16 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %17 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %18 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %19 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %call2 = call i64 @av1_calc_frame_error_c(i8* %14, i32 %15, i8* %16, i32 %17, i32 %18, i32 %19)
  store i64 %call2, i64* %retval, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  %20 = load i64, i64* %retval, align 8
  ret i64 %20
}

; Function Attrs: nounwind
define hidden i64 @av1_segmented_frame_error(i32 %use_hbd, i32 %bd, i8* %ref, i32 %stride, i8* %dst, i32 %p_width, i32 %p_height, i32 %p_stride, i8* %segment_map, i32 %segment_map_stride) #0 {
entry:
  %retval = alloca i64, align 8
  %use_hbd.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %ref.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %segment_map.addr = alloca i8*, align 4
  %segment_map_stride.addr = alloca i32, align 4
  store i32 %use_hbd, i32* %use_hbd.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  store i8* %segment_map, i8** %segment_map.addr, align 4, !tbaa !2
  store i32 %segment_map_stride, i32* %segment_map_stride.addr, align 4, !tbaa !6
  %0 = load i32, i32* %use_hbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %2 = ptrtoint i8* %1 to i32
  %shl = shl i32 %2, 1
  %3 = inttoptr i32 %shl to i16*
  %4 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %5 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %6 = ptrtoint i8* %5 to i32
  %shl1 = shl i32 %6, 1
  %7 = inttoptr i32 %shl1 to i16*
  %8 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %9 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %10 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %11 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %12 = load i8*, i8** %segment_map.addr, align 4, !tbaa !2
  %13 = load i32, i32* %segment_map_stride.addr, align 4, !tbaa !6
  %call = call i64 @highbd_segmented_frame_error(i16* %3, i32 %4, i16* %7, i32 %8, i32 %9, i32 %10, i32 %11, i8* %12, i32 %13)
  store i64 %call, i64* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %14 = load i32, i32* %use_hbd.addr, align 4, !tbaa !6
  %15 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %16 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %17 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %18 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %19 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %20 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %21 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %22 = load i8*, i8** %segment_map.addr, align 4, !tbaa !2
  %23 = load i32, i32* %segment_map_stride.addr, align 4, !tbaa !6
  %call2 = call i64 @segmented_frame_error(i8* %16, i32 %17, i8* %18, i32 %19, i32 %20, i32 %21, i8* %22, i32 %23)
  store i64 %call2, i64* %retval, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  %24 = load i64, i64* %retval, align 8
  ret i64 %24
}

; Function Attrs: nounwind
define internal i64 @highbd_segmented_frame_error(i16* %ref, i32 %stride, i16* %dst, i32 %p_width, i32 %p_height, i32 %p_stride, i32 %bd, i8* %segment_map, i32 %segment_map_stride) #0 {
entry:
  %ref.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %segment_map.addr = alloca i8*, align 4
  %segment_map_stride.addr = alloca i32, align 4
  %patch_w = alloca i32, align 4
  %patch_h = alloca i32, align 4
  %error_bsize_w = alloca i32, align 4
  %error_bsize_h = alloca i32, align 4
  %sum_error = alloca i64, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %seg_x = alloca i32, align 4
  %seg_y = alloca i32, align 4
  store i16* %ref, i16** %ref.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  store i8* %segment_map, i8** %segment_map.addr, align 4, !tbaa !2
  store i32 %segment_map_stride, i32* %segment_map_stride.addr, align 4, !tbaa !6
  %0 = bitcast i32* %patch_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %patch_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %error_bsize_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, 32
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ 32, %cond.false ]
  store i32 %cond, i32* %error_bsize_w, align 4, !tbaa !6
  %5 = bitcast i32* %error_bsize_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %6, 32
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.end
  %7 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false3:                                      ; preds = %cond.end
  br label %cond.end4

cond.end4:                                        ; preds = %cond.false3, %cond.true2
  %cond5 = phi i32 [ %7, %cond.true2 ], [ 32, %cond.false3 ]
  store i32 %cond5, i32* %error_bsize_h, align 4, !tbaa !6
  %8 = bitcast i64* %sum_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #6
  store i64 0, i64* %sum_error, align 8, !tbaa !13
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc34, %cond.end4
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %cmp6 = icmp slt i32 %10, %11
  br i1 %cmp6, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  br label %for.end37

for.body:                                         ; preds = %for.cond
  %13 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %15 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %14, %15
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  store i32 5, i32* %cleanup.dest.slot, align 4
  %16 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  br label %for.end

for.body10:                                       ; preds = %for.cond7
  %17 = bitcast i32* %seg_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %shr = ashr i32 %18, 5
  store i32 %shr, i32* %seg_x, align 4, !tbaa !6
  %19 = bitcast i32* %seg_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %shr11 = ashr i32 %20, 5
  store i32 %shr11, i32* %seg_y, align 4, !tbaa !6
  %21 = load i8*, i8** %segment_map.addr, align 4, !tbaa !2
  %22 = load i32, i32* %seg_y, align 4, !tbaa !6
  %23 = load i32, i32* %segment_map_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %22, %23
  %24 = load i32, i32* %seg_x, align 4, !tbaa !6
  %add = add nsw i32 %mul, %24
  %arrayidx = getelementptr inbounds i8, i8* %21, i32 %add
  %25 = load i8, i8* %arrayidx, align 1, !tbaa !28
  %tobool = icmp ne i8 %25, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body10
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body10
  %26 = load i32, i32* %error_bsize_w, align 4, !tbaa !6
  %27 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %sub = sub nsw i32 %27, %28
  %cmp12 = icmp slt i32 %26, %sub
  br i1 %cmp12, label %cond.true13, label %cond.false14

cond.true13:                                      ; preds = %if.end
  %29 = load i32, i32* %error_bsize_w, align 4, !tbaa !6
  br label %cond.end16

cond.false14:                                     ; preds = %if.end
  %30 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %sub15 = sub nsw i32 %30, %31
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false14, %cond.true13
  %cond17 = phi i32 [ %29, %cond.true13 ], [ %sub15, %cond.false14 ]
  store i32 %cond17, i32* %patch_w, align 4, !tbaa !6
  %32 = load i32, i32* %error_bsize_h, align 4, !tbaa !6
  %33 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %sub18 = sub nsw i32 %33, %34
  %cmp19 = icmp slt i32 %32, %sub18
  br i1 %cmp19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end16
  %35 = load i32, i32* %error_bsize_h, align 4, !tbaa !6
  br label %cond.end23

cond.false21:                                     ; preds = %cond.end16
  %36 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %sub22 = sub nsw i32 %36, %37
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false21, %cond.true20
  %cond24 = phi i32 [ %35, %cond.true20 ], [ %sub22, %cond.false21 ]
  store i32 %cond24, i32* %patch_h, align 4, !tbaa !6
  %38 = load i16*, i16** %ref.addr, align 4, !tbaa !2
  %39 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i16, i16* %38, i32 %39
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 %40, %41
  %add.ptr26 = getelementptr inbounds i16, i16* %add.ptr, i32 %mul25
  %42 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %43 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %44 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr27 = getelementptr inbounds i16, i16* %43, i32 %44
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %46 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %mul28 = mul nsw i32 %45, %46
  %add.ptr29 = getelementptr inbounds i16, i16* %add.ptr27, i32 %mul28
  %47 = load i32, i32* %patch_w, align 4, !tbaa !6
  %48 = load i32, i32* %patch_h, align 4, !tbaa !6
  %49 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %50 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call i64 @av1_calc_highbd_frame_error(i16* %add.ptr26, i32 %42, i16* %add.ptr29, i32 %47, i32 %48, i32 %49, i32 %50)
  %51 = load i64, i64* %sum_error, align 8, !tbaa !13
  %add30 = add nsw i64 %51, %call
  store i64 %add30, i64* %sum_error, align 8, !tbaa !13
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %cond.end23, %if.then
  %52 = bitcast i32* %seg_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  %53 = bitcast i32* %seg_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %54 = load i32, i32* %j, align 4, !tbaa !6
  %add32 = add nsw i32 %54, 32
  store i32 %add32, i32* %j, align 4, !tbaa !6
  br label %for.cond7

for.end:                                          ; preds = %for.cond.cleanup9
  br label %for.inc34

for.inc34:                                        ; preds = %for.end
  %55 = load i32, i32* %i, align 4, !tbaa !6
  %add35 = add nsw i32 %55, 32
  store i32 %add35, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end37:                                        ; preds = %for.cond.cleanup
  %56 = load i64, i64* %sum_error, align 8, !tbaa !13
  store i32 1, i32* %cleanup.dest.slot, align 4
  %57 = bitcast i64* %sum_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %57) #6
  %58 = bitcast i32* %error_bsize_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast i32* %error_bsize_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %patch_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %patch_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  ret i64 %56

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i64 @segmented_frame_error(i8* %ref, i32 %stride, i8* %dst, i32 %p_width, i32 %p_height, i32 %p_stride, i8* %segment_map, i32 %segment_map_stride) #0 {
entry:
  %ref.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %segment_map.addr = alloca i8*, align 4
  %segment_map_stride.addr = alloca i32, align 4
  %patch_w = alloca i32, align 4
  %patch_h = alloca i32, align 4
  %error_bsize_w = alloca i32, align 4
  %error_bsize_h = alloca i32, align 4
  %sum_error = alloca i64, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %seg_x = alloca i32, align 4
  %seg_y = alloca i32, align 4
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  store i8* %segment_map, i8** %segment_map.addr, align 4, !tbaa !2
  store i32 %segment_map_stride, i32* %segment_map_stride.addr, align 4, !tbaa !6
  %0 = bitcast i32* %patch_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %patch_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %error_bsize_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, 32
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ 32, %cond.false ]
  store i32 %cond, i32* %error_bsize_w, align 4, !tbaa !6
  %5 = bitcast i32* %error_bsize_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %6, 32
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.end
  %7 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false3:                                      ; preds = %cond.end
  br label %cond.end4

cond.end4:                                        ; preds = %cond.false3, %cond.true2
  %cond5 = phi i32 [ %7, %cond.true2 ], [ 32, %cond.false3 ]
  store i32 %cond5, i32* %error_bsize_h, align 4, !tbaa !6
  %8 = bitcast i64* %sum_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #6
  store i64 0, i64* %sum_error, align 8, !tbaa !13
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc34, %cond.end4
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %cmp6 = icmp slt i32 %10, %11
  br i1 %cmp6, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  br label %for.end37

for.body:                                         ; preds = %for.cond
  %13 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %15 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %14, %15
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  store i32 5, i32* %cleanup.dest.slot, align 4
  %16 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  br label %for.end

for.body10:                                       ; preds = %for.cond7
  %17 = bitcast i32* %seg_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %shr = ashr i32 %18, 5
  store i32 %shr, i32* %seg_x, align 4, !tbaa !6
  %19 = bitcast i32* %seg_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %shr11 = ashr i32 %20, 5
  store i32 %shr11, i32* %seg_y, align 4, !tbaa !6
  %21 = load i8*, i8** %segment_map.addr, align 4, !tbaa !2
  %22 = load i32, i32* %seg_y, align 4, !tbaa !6
  %23 = load i32, i32* %segment_map_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %22, %23
  %24 = load i32, i32* %seg_x, align 4, !tbaa !6
  %add = add nsw i32 %mul, %24
  %arrayidx = getelementptr inbounds i8, i8* %21, i32 %add
  %25 = load i8, i8* %arrayidx, align 1, !tbaa !28
  %tobool = icmp ne i8 %25, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body10
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body10
  %26 = load i32, i32* %error_bsize_w, align 4, !tbaa !6
  %27 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %sub = sub nsw i32 %27, %28
  %cmp12 = icmp slt i32 %26, %sub
  br i1 %cmp12, label %cond.true13, label %cond.false14

cond.true13:                                      ; preds = %if.end
  %29 = load i32, i32* %error_bsize_w, align 4, !tbaa !6
  br label %cond.end16

cond.false14:                                     ; preds = %if.end
  %30 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %sub15 = sub nsw i32 %30, %31
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false14, %cond.true13
  %cond17 = phi i32 [ %29, %cond.true13 ], [ %sub15, %cond.false14 ]
  store i32 %cond17, i32* %patch_w, align 4, !tbaa !6
  %32 = load i32, i32* %error_bsize_h, align 4, !tbaa !6
  %33 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %sub18 = sub nsw i32 %33, %34
  %cmp19 = icmp slt i32 %32, %sub18
  br i1 %cmp19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end16
  %35 = load i32, i32* %error_bsize_h, align 4, !tbaa !6
  br label %cond.end23

cond.false21:                                     ; preds = %cond.end16
  %36 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %sub22 = sub nsw i32 %36, %37
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false21, %cond.true20
  %cond24 = phi i32 [ %35, %cond.true20 ], [ %sub22, %cond.false21 ]
  store i32 %cond24, i32* %patch_h, align 4, !tbaa !6
  %38 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %39 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 %39
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 %40, %41
  %add.ptr26 = getelementptr inbounds i8, i8* %add.ptr, i32 %mul25
  %42 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %43 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %44 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr27 = getelementptr inbounds i8, i8* %43, i32 %44
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %46 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %mul28 = mul nsw i32 %45, %46
  %add.ptr29 = getelementptr inbounds i8, i8* %add.ptr27, i32 %mul28
  %47 = load i32, i32* %patch_w, align 4, !tbaa !6
  %48 = load i32, i32* %patch_h, align 4, !tbaa !6
  %49 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %call = call i64 @av1_calc_frame_error_c(i8* %add.ptr26, i32 %42, i8* %add.ptr29, i32 %47, i32 %48, i32 %49)
  %50 = load i64, i64* %sum_error, align 8, !tbaa !13
  %add30 = add nsw i64 %50, %call
  store i64 %add30, i64* %sum_error, align 8, !tbaa !13
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %cond.end23, %if.then
  %51 = bitcast i32* %seg_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  %52 = bitcast i32* %seg_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %53 = load i32, i32* %j, align 4, !tbaa !6
  %add32 = add nsw i32 %53, 32
  store i32 %add32, i32* %j, align 4, !tbaa !6
  br label %for.cond7

for.end:                                          ; preds = %for.cond.cleanup9
  br label %for.inc34

for.inc34:                                        ; preds = %for.end
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %add35 = add nsw i32 %54, 32
  store i32 %add35, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end37:                                        ; preds = %for.cond.cleanup
  %55 = load i64, i64* %sum_error, align 8, !tbaa !13
  store i32 1, i32* %cleanup.dest.slot, align 4
  %56 = bitcast i64* %sum_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %56) #6
  %57 = bitcast i32* %error_bsize_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  %58 = bitcast i32* %error_bsize_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast i32* %patch_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %patch_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  ret i64 %55

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @av1_warp_plane(%struct.WarpedMotionParams* %wm, i32 %use_hbd, i32 %bd, i8* %ref, i32 %width, i32 %height, i32 %stride, i8* %pred, i32 %p_col, i32 %p_row, i32 %p_width, i32 %p_height, i32 %p_stride, i32 %subsampling_x, i32 %subsampling_y, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %wm.addr = alloca %struct.WarpedMotionParams*, align 4
  %use_hbd.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %ref.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %pred.addr = alloca i8*, align 4
  %p_col.addr = alloca i32, align 4
  %p_row.addr = alloca i32, align 4
  %p_width.addr = alloca i32, align 4
  %p_height.addr = alloca i32, align 4
  %p_stride.addr = alloca i32, align 4
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  store %struct.WarpedMotionParams* %wm, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  store i32 %use_hbd, i32* %use_hbd.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  store i8* %ref, i8** %ref.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %pred, i8** %pred.addr, align 4, !tbaa !2
  store i32 %p_col, i32* %p_col.addr, align 4, !tbaa !6
  store i32 %p_row, i32* %p_row.addr, align 4, !tbaa !6
  store i32 %p_width, i32* %p_width.addr, align 4, !tbaa !6
  store i32 %p_height, i32* %p_height.addr, align 4, !tbaa !6
  store i32 %p_stride, i32* %p_stride.addr, align 4, !tbaa !6
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = load i32, i32* %use_hbd.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %2 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %3 = ptrtoint i8* %2 to i32
  %shl = shl i32 %3, 1
  %4 = inttoptr i32 %shl to i16*
  %5 = load i32, i32* %width.addr, align 4, !tbaa !6
  %6 = load i32, i32* %height.addr, align 4, !tbaa !6
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %pred.addr, align 4, !tbaa !2
  %9 = ptrtoint i8* %8 to i32
  %shl1 = shl i32 %9, 1
  %10 = inttoptr i32 %shl1 to i16*
  %11 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %12 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %13 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %14 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %15 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %16 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %17 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %18 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %19 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  call void @highbd_warp_plane(%struct.WarpedMotionParams* %1, i16* %4, i32 %5, i32 %6, i32 %7, i16* %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15, i32 %16, i32 %17, i32 %18, %struct.ConvolveParams* %19)
  br label %if.end

if.else:                                          ; preds = %entry
  %20 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %21 = load i8*, i8** %ref.addr, align 4, !tbaa !2
  %22 = load i32, i32* %width.addr, align 4, !tbaa !6
  %23 = load i32, i32* %height.addr, align 4, !tbaa !6
  %24 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %25 = load i8*, i8** %pred.addr, align 4, !tbaa !2
  %26 = load i32, i32* %p_col.addr, align 4, !tbaa !6
  %27 = load i32, i32* %p_row.addr, align 4, !tbaa !6
  %28 = load i32, i32* %p_width.addr, align 4, !tbaa !6
  %29 = load i32, i32* %p_height.addr, align 4, !tbaa !6
  %30 = load i32, i32* %p_stride.addr, align 4, !tbaa !6
  %31 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %32 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %33 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  call void @warp_plane(%struct.WarpedMotionParams* %20, i8* %21, i32 %22, i32 %23, i32 %24, i8* %25, i32 %26, i32 %27, i32 %28, i32 %29, i32 %30, i32 %31, i32 %32, %struct.ConvolveParams* %33)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_find_projection(i32 %np, i32* %pts1, i32* %pts2, i8 zeroext %bsize, i32 %mvy, i32 %mvx, %struct.WarpedMotionParams* %wm_params, i32 %mi_row, i32 %mi_col) #0 {
entry:
  %retval = alloca i32, align 4
  %np.addr = alloca i32, align 4
  %pts1.addr = alloca i32*, align 4
  %pts2.addr = alloca i32*, align 4
  %bsize.addr = alloca i8, align 1
  %mvy.addr = alloca i32, align 4
  %mvx.addr = alloca i32, align 4
  %wm_params.addr = alloca %struct.WarpedMotionParams*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  store i32 %np, i32* %np.addr, align 4, !tbaa !6
  store i32* %pts1, i32** %pts1.addr, align 4, !tbaa !2
  store i32* %pts2, i32** %pts2.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !28
  store i32 %mvy, i32* %mvy.addr, align 4, !tbaa !6
  store i32 %mvx, i32* %mvx.addr, align 4, !tbaa !6
  store %struct.WarpedMotionParams* %wm_params, %struct.WarpedMotionParams** %wm_params.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  %0 = load i32, i32* %np.addr, align 4, !tbaa !6
  %1 = load i32*, i32** %pts1.addr, align 4, !tbaa !2
  %2 = load i32*, i32** %pts2.addr, align 4, !tbaa !2
  %3 = load i8, i8* %bsize.addr, align 1, !tbaa !28
  %4 = load i32, i32* %mvy.addr, align 4, !tbaa !6
  %5 = load i32, i32* %mvx.addr, align 4, !tbaa !6
  %6 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm_params.addr, align 4, !tbaa !2
  %7 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %8 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %call = call i32 @find_affine_int(i32 %0, i32* %1, i32* %2, i8 zeroext %3, i32 %4, i32 %5, %struct.WarpedMotionParams* %6, i32 %7, i32 %8)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %9 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm_params.addr, align 4, !tbaa !2
  %call1 = call i32 @av1_get_shear_params(%struct.WarpedMotionParams* %9)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.end
  store i32 1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define internal i32 @find_affine_int(i32 %np, i32* %pts1, i32* %pts2, i8 zeroext %bsize, i32 %mvy, i32 %mvx, %struct.WarpedMotionParams* %wm, i32 %mi_row, i32 %mi_col) #0 {
entry:
  %retval = alloca i32, align 4
  %np.addr = alloca i32, align 4
  %pts1.addr = alloca i32*, align 4
  %pts2.addr = alloca i32*, align 4
  %bsize.addr = alloca i8, align 1
  %mvy.addr = alloca i32, align 4
  %mvx.addr = alloca i32, align 4
  %wm.addr = alloca %struct.WarpedMotionParams*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %A = alloca [2 x [2 x i32]], align 16
  %Bx = alloca [2 x i32], align 4
  %By = alloca [2 x i32], align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %rsuy = alloca i32, align 4
  %rsux = alloca i32, align 4
  %suy = alloca i32, align 4
  %sux = alloca i32, align 4
  %duy = alloca i32, align 4
  %dux = alloca i32, align 4
  %i = alloca i32, align 4
  %dx = alloca i32, align 4
  %dy = alloca i32, align 4
  %sx = alloca i32, align 4
  %sy = alloca i32, align 4
  %Det = alloca i64, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %shift = alloca i16, align 2
  %iDet = alloca i16, align 2
  %Px = alloca [2 x i64], align 16
  %Py = alloca [2 x i64], align 16
  %isuy = alloca i32, align 4
  %isux = alloca i32, align 4
  %vx = alloca i32, align 4
  %vy = alloca i32, align 4
  store i32 %np, i32* %np.addr, align 4, !tbaa !6
  store i32* %pts1, i32** %pts1.addr, align 4, !tbaa !2
  store i32* %pts2, i32** %pts2.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !28
  store i32 %mvy, i32* %mvy.addr, align 4, !tbaa !6
  store i32 %mvx, i32* %mvx.addr, align 4, !tbaa !6
  store %struct.WarpedMotionParams* %wm, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  %0 = bitcast [2 x [2 x i32]]* %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %1 = bitcast [2 x [2 x i32]]* %A to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %1, i8 0, i32 16, i1 false)
  %2 = bitcast [2 x i32]* %Bx to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #6
  %3 = bitcast [2 x i32]* %Bx to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 8, i1 false)
  %4 = bitcast [2 x i32]* %By to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #6
  %5 = bitcast [2 x i32]* %By to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %5, i8 0, i32 8, i1 false)
  %6 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i8, i8* %bsize.addr, align 1, !tbaa !28
  %idxprom = zext i8 %7 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %8 = load i8, i8* %arrayidx, align 1, !tbaa !28
  %conv = zext i8 %8 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !6
  %9 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i8, i8* %bsize.addr, align 1, !tbaa !28
  %idxprom1 = zext i8 %10 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom1
  %11 = load i8, i8* %arrayidx2, align 1, !tbaa !28
  %conv3 = zext i8 %11 to i32
  store i32 %conv3, i32* %bh, align 4, !tbaa !6
  %12 = bitcast i32* %rsuy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i32, i32* %bh, align 4, !tbaa !6
  %div = sdiv i32 %13, 2
  %sub = sub nsw i32 %div, 1
  store i32 %sub, i32* %rsuy, align 4, !tbaa !6
  %14 = bitcast i32* %rsux to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32, i32* %bw, align 4, !tbaa !6
  %div4 = sdiv i32 %15, 2
  %sub5 = sub nsw i32 %div4, 1
  store i32 %sub5, i32* %rsux, align 4, !tbaa !6
  %16 = bitcast i32* %suy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %rsuy, align 4, !tbaa !6
  %mul = mul nsw i32 %17, 8
  store i32 %mul, i32* %suy, align 4, !tbaa !6
  %18 = bitcast i32* %sux to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load i32, i32* %rsux, align 4, !tbaa !6
  %mul6 = mul nsw i32 %19, 8
  store i32 %mul6, i32* %sux, align 4, !tbaa !6
  %20 = bitcast i32* %duy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load i32, i32* %suy, align 4, !tbaa !6
  %22 = load i32, i32* %mvy.addr, align 4, !tbaa !6
  %add = add nsw i32 %21, %22
  store i32 %add, i32* %duy, align 4, !tbaa !6
  %23 = bitcast i32* %dux to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = load i32, i32* %sux, align 4, !tbaa !6
  %25 = load i32, i32* %mvx.addr, align 4, !tbaa !6
  %add7 = add nsw i32 %24, %25
  store i32 %add7, i32* %dux, align 4, !tbaa !6
  %26 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %28 = load i32, i32* %np.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %27, %28
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %30 = bitcast i32* %dx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load i32*, i32** %pts2.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %mul9 = mul nsw i32 %32, 2
  %arrayidx10 = getelementptr inbounds i32, i32* %31, i32 %mul9
  %33 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %34 = load i32, i32* %dux, align 4, !tbaa !6
  %sub11 = sub nsw i32 %33, %34
  store i32 %sub11, i32* %dx, align 4, !tbaa !6
  %35 = bitcast i32* %dy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %36 = load i32*, i32** %pts2.addr, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %mul12 = mul nsw i32 %37, 2
  %add13 = add nsw i32 %mul12, 1
  %arrayidx14 = getelementptr inbounds i32, i32* %36, i32 %add13
  %38 = load i32, i32* %arrayidx14, align 4, !tbaa !6
  %39 = load i32, i32* %duy, align 4, !tbaa !6
  %sub15 = sub nsw i32 %38, %39
  store i32 %sub15, i32* %dy, align 4, !tbaa !6
  %40 = bitcast i32* %sx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  %41 = load i32*, i32** %pts1.addr, align 4, !tbaa !2
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %mul16 = mul nsw i32 %42, 2
  %arrayidx17 = getelementptr inbounds i32, i32* %41, i32 %mul16
  %43 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %44 = load i32, i32* %sux, align 4, !tbaa !6
  %sub18 = sub nsw i32 %43, %44
  store i32 %sub18, i32* %sx, align 4, !tbaa !6
  %45 = bitcast i32* %sy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #6
  %46 = load i32*, i32** %pts1.addr, align 4, !tbaa !2
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %mul19 = mul nsw i32 %47, 2
  %add20 = add nsw i32 %mul19, 1
  %arrayidx21 = getelementptr inbounds i32, i32* %46, i32 %add20
  %48 = load i32, i32* %arrayidx21, align 4, !tbaa !6
  %49 = load i32, i32* %suy, align 4, !tbaa !6
  %sub22 = sub nsw i32 %48, %49
  store i32 %sub22, i32* %sy, align 4, !tbaa !6
  %50 = load i32, i32* %sx, align 4, !tbaa !6
  %51 = load i32, i32* %dx, align 4, !tbaa !6
  %sub23 = sub nsw i32 %50, %51
  %call = call i32 @abs(i32 %sub23) #7
  %cmp24 = icmp slt i32 %call, 256
  br i1 %cmp24, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %52 = load i32, i32* %sy, align 4, !tbaa !6
  %53 = load i32, i32* %dy, align 4, !tbaa !6
  %sub26 = sub nsw i32 %52, %53
  %call27 = call i32 @abs(i32 %sub26) #7
  %cmp28 = icmp slt i32 %call27, 256
  br i1 %cmp28, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %54 = load i32, i32* %sx, align 4, !tbaa !6
  %55 = load i32, i32* %sx, align 4, !tbaa !6
  %mul30 = mul nsw i32 %54, %55
  %mul31 = mul nsw i32 %mul30, 4
  %56 = load i32, i32* %sx, align 4, !tbaa !6
  %mul32 = mul nsw i32 %56, 4
  %mul33 = mul nsw i32 %mul32, 8
  %add34 = add nsw i32 %mul31, %mul33
  %add35 = add nsw i32 %add34, 128
  %shr = ashr i32 %add35, 4
  %arrayidx36 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx37 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx36, i32 0, i32 0
  %57 = load i32, i32* %arrayidx37, align 16, !tbaa !6
  %add38 = add nsw i32 %57, %shr
  store i32 %add38, i32* %arrayidx37, align 16, !tbaa !6
  %58 = load i32, i32* %sx, align 4, !tbaa !6
  %59 = load i32, i32* %sy, align 4, !tbaa !6
  %mul39 = mul nsw i32 %58, %59
  %mul40 = mul nsw i32 %mul39, 4
  %60 = load i32, i32* %sx, align 4, !tbaa !6
  %61 = load i32, i32* %sy, align 4, !tbaa !6
  %add41 = add nsw i32 %60, %61
  %mul42 = mul nsw i32 %add41, 2
  %mul43 = mul nsw i32 %mul42, 8
  %add44 = add nsw i32 %mul40, %mul43
  %add45 = add nsw i32 %add44, 64
  %shr46 = ashr i32 %add45, 4
  %arrayidx47 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx48 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx47, i32 0, i32 1
  %62 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %add49 = add nsw i32 %62, %shr46
  store i32 %add49, i32* %arrayidx48, align 4, !tbaa !6
  %63 = load i32, i32* %sy, align 4, !tbaa !6
  %64 = load i32, i32* %sy, align 4, !tbaa !6
  %mul50 = mul nsw i32 %63, %64
  %mul51 = mul nsw i32 %mul50, 4
  %65 = load i32, i32* %sy, align 4, !tbaa !6
  %mul52 = mul nsw i32 %65, 4
  %mul53 = mul nsw i32 %mul52, 8
  %add54 = add nsw i32 %mul51, %mul53
  %add55 = add nsw i32 %add54, 128
  %shr56 = ashr i32 %add55, 4
  %arrayidx57 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 1
  %arrayidx58 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx57, i32 0, i32 1
  %66 = load i32, i32* %arrayidx58, align 4, !tbaa !6
  %add59 = add nsw i32 %66, %shr56
  store i32 %add59, i32* %arrayidx58, align 4, !tbaa !6
  %67 = load i32, i32* %sx, align 4, !tbaa !6
  %68 = load i32, i32* %dx, align 4, !tbaa !6
  %mul60 = mul nsw i32 %67, %68
  %mul61 = mul nsw i32 %mul60, 4
  %69 = load i32, i32* %sx, align 4, !tbaa !6
  %70 = load i32, i32* %dx, align 4, !tbaa !6
  %add62 = add nsw i32 %69, %70
  %mul63 = mul nsw i32 %add62, 2
  %mul64 = mul nsw i32 %mul63, 8
  %add65 = add nsw i32 %mul61, %mul64
  %add66 = add nsw i32 %add65, 128
  %shr67 = ashr i32 %add66, 4
  %arrayidx68 = getelementptr inbounds [2 x i32], [2 x i32]* %Bx, i32 0, i32 0
  %71 = load i32, i32* %arrayidx68, align 4, !tbaa !6
  %add69 = add nsw i32 %71, %shr67
  store i32 %add69, i32* %arrayidx68, align 4, !tbaa !6
  %72 = load i32, i32* %sy, align 4, !tbaa !6
  %73 = load i32, i32* %dx, align 4, !tbaa !6
  %mul70 = mul nsw i32 %72, %73
  %mul71 = mul nsw i32 %mul70, 4
  %74 = load i32, i32* %sy, align 4, !tbaa !6
  %75 = load i32, i32* %dx, align 4, !tbaa !6
  %add72 = add nsw i32 %74, %75
  %mul73 = mul nsw i32 %add72, 2
  %mul74 = mul nsw i32 %mul73, 8
  %add75 = add nsw i32 %mul71, %mul74
  %add76 = add nsw i32 %add75, 64
  %shr77 = ashr i32 %add76, 4
  %arrayidx78 = getelementptr inbounds [2 x i32], [2 x i32]* %Bx, i32 0, i32 1
  %76 = load i32, i32* %arrayidx78, align 4, !tbaa !6
  %add79 = add nsw i32 %76, %shr77
  store i32 %add79, i32* %arrayidx78, align 4, !tbaa !6
  %77 = load i32, i32* %sx, align 4, !tbaa !6
  %78 = load i32, i32* %dy, align 4, !tbaa !6
  %mul80 = mul nsw i32 %77, %78
  %mul81 = mul nsw i32 %mul80, 4
  %79 = load i32, i32* %sx, align 4, !tbaa !6
  %80 = load i32, i32* %dy, align 4, !tbaa !6
  %add82 = add nsw i32 %79, %80
  %mul83 = mul nsw i32 %add82, 2
  %mul84 = mul nsw i32 %mul83, 8
  %add85 = add nsw i32 %mul81, %mul84
  %add86 = add nsw i32 %add85, 64
  %shr87 = ashr i32 %add86, 4
  %arrayidx88 = getelementptr inbounds [2 x i32], [2 x i32]* %By, i32 0, i32 0
  %81 = load i32, i32* %arrayidx88, align 4, !tbaa !6
  %add89 = add nsw i32 %81, %shr87
  store i32 %add89, i32* %arrayidx88, align 4, !tbaa !6
  %82 = load i32, i32* %sy, align 4, !tbaa !6
  %83 = load i32, i32* %dy, align 4, !tbaa !6
  %mul90 = mul nsw i32 %82, %83
  %mul91 = mul nsw i32 %mul90, 4
  %84 = load i32, i32* %sy, align 4, !tbaa !6
  %85 = load i32, i32* %dy, align 4, !tbaa !6
  %add92 = add nsw i32 %84, %85
  %mul93 = mul nsw i32 %add92, 2
  %mul94 = mul nsw i32 %mul93, 8
  %add95 = add nsw i32 %mul91, %mul94
  %add96 = add nsw i32 %add95, 128
  %shr97 = ashr i32 %add96, 4
  %arrayidx98 = getelementptr inbounds [2 x i32], [2 x i32]* %By, i32 0, i32 1
  %86 = load i32, i32* %arrayidx98, align 4, !tbaa !6
  %add99 = add nsw i32 %86, %shr97
  store i32 %add99, i32* %arrayidx98, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  %87 = bitcast i32* %sy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #6
  %88 = bitcast i32* %sx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #6
  %89 = bitcast i32* %dy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #6
  %90 = bitcast i32* %dx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %91 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %91, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %92 = bitcast i64* %Det to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %92) #6
  %arrayidx100 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx101 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx100, i32 0, i32 0
  %93 = load i32, i32* %arrayidx101, align 16, !tbaa !6
  %conv102 = sext i32 %93 to i64
  %arrayidx103 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 1
  %arrayidx104 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx103, i32 0, i32 1
  %94 = load i32, i32* %arrayidx104, align 4, !tbaa !6
  %conv105 = sext i32 %94 to i64
  %mul106 = mul nsw i64 %conv102, %conv105
  %arrayidx107 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx108 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx107, i32 0, i32 1
  %95 = load i32, i32* %arrayidx108, align 4, !tbaa !6
  %conv109 = sext i32 %95 to i64
  %arrayidx110 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx111 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx110, i32 0, i32 1
  %96 = load i32, i32* %arrayidx111, align 4, !tbaa !6
  %conv112 = sext i32 %96 to i64
  %mul113 = mul nsw i64 %conv109, %conv112
  %sub114 = sub nsw i64 %mul106, %mul113
  store i64 %sub114, i64* %Det, align 8, !tbaa !13
  %97 = load i64, i64* %Det, align 8, !tbaa !13
  %cmp115 = icmp eq i64 %97, 0
  br i1 %cmp115, label %if.then117, label %if.end118

if.then117:                                       ; preds = %for.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end118:                                        ; preds = %for.end
  %98 = bitcast i16* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %98) #6
  %99 = bitcast i16* %iDet to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %99) #6
  %100 = load i64, i64* %Det, align 8, !tbaa !13
  %call119 = call i64 @llabs(i64 %100) #7
  %call120 = call signext i16 @resolve_divisor_64(i64 %call119, i16* %shift)
  %conv121 = sext i16 %call120 to i32
  %101 = load i64, i64* %Det, align 8, !tbaa !13
  %cmp122 = icmp slt i64 %101, 0
  %102 = zext i1 %cmp122 to i64
  %cond = select i1 %cmp122, i32 -1, i32 1
  %mul124 = mul nsw i32 %conv121, %cond
  %conv125 = trunc i32 %mul124 to i16
  store i16 %conv125, i16* %iDet, align 2, !tbaa !12
  %103 = load i16, i16* %shift, align 2, !tbaa !12
  %conv126 = sext i16 %103 to i32
  %sub127 = sub nsw i32 %conv126, 16
  %conv128 = trunc i32 %sub127 to i16
  store i16 %conv128, i16* %shift, align 2, !tbaa !12
  %104 = load i16, i16* %shift, align 2, !tbaa !12
  %conv129 = sext i16 %104 to i32
  %cmp130 = icmp slt i32 %conv129, 0
  br i1 %cmp130, label %if.then132, label %if.end137

if.then132:                                       ; preds = %if.end118
  %105 = load i16, i16* %shift, align 2, !tbaa !12
  %conv133 = sext i16 %105 to i32
  %sub134 = sub nsw i32 0, %conv133
  %106 = load i16, i16* %iDet, align 2, !tbaa !12
  %conv135 = sext i16 %106 to i32
  %shl = shl i32 %conv135, %sub134
  %conv136 = trunc i32 %shl to i16
  store i16 %conv136, i16* %iDet, align 2, !tbaa !12
  store i16 0, i16* %shift, align 2, !tbaa !12
  br label %if.end137

if.end137:                                        ; preds = %if.then132, %if.end118
  %107 = bitcast [2 x i64]* %Px to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %107) #6
  %108 = bitcast [2 x i64]* %Py to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %108) #6
  %arrayidx138 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 1
  %arrayidx139 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx138, i32 0, i32 1
  %109 = load i32, i32* %arrayidx139, align 4, !tbaa !6
  %conv140 = sext i32 %109 to i64
  %arrayidx141 = getelementptr inbounds [2 x i32], [2 x i32]* %Bx, i32 0, i32 0
  %110 = load i32, i32* %arrayidx141, align 4, !tbaa !6
  %conv142 = sext i32 %110 to i64
  %mul143 = mul nsw i64 %conv140, %conv142
  %arrayidx144 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx145 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx144, i32 0, i32 1
  %111 = load i32, i32* %arrayidx145, align 4, !tbaa !6
  %conv146 = sext i32 %111 to i64
  %arrayidx147 = getelementptr inbounds [2 x i32], [2 x i32]* %Bx, i32 0, i32 1
  %112 = load i32, i32* %arrayidx147, align 4, !tbaa !6
  %conv148 = sext i32 %112 to i64
  %mul149 = mul nsw i64 %conv146, %conv148
  %sub150 = sub nsw i64 %mul143, %mul149
  %arrayidx151 = getelementptr inbounds [2 x i64], [2 x i64]* %Px, i32 0, i32 0
  store i64 %sub150, i64* %arrayidx151, align 16, !tbaa !13
  %arrayidx152 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx153 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx152, i32 0, i32 1
  %113 = load i32, i32* %arrayidx153, align 4, !tbaa !6
  %conv154 = sext i32 %113 to i64
  %sub155 = sub nsw i64 0, %conv154
  %arrayidx156 = getelementptr inbounds [2 x i32], [2 x i32]* %Bx, i32 0, i32 0
  %114 = load i32, i32* %arrayidx156, align 4, !tbaa !6
  %conv157 = sext i32 %114 to i64
  %mul158 = mul nsw i64 %sub155, %conv157
  %arrayidx159 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx160 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx159, i32 0, i32 0
  %115 = load i32, i32* %arrayidx160, align 16, !tbaa !6
  %conv161 = sext i32 %115 to i64
  %arrayidx162 = getelementptr inbounds [2 x i32], [2 x i32]* %Bx, i32 0, i32 1
  %116 = load i32, i32* %arrayidx162, align 4, !tbaa !6
  %conv163 = sext i32 %116 to i64
  %mul164 = mul nsw i64 %conv161, %conv163
  %add165 = add nsw i64 %mul158, %mul164
  %arrayidx166 = getelementptr inbounds [2 x i64], [2 x i64]* %Px, i32 0, i32 1
  store i64 %add165, i64* %arrayidx166, align 8, !tbaa !13
  %arrayidx167 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 1
  %arrayidx168 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx167, i32 0, i32 1
  %117 = load i32, i32* %arrayidx168, align 4, !tbaa !6
  %conv169 = sext i32 %117 to i64
  %arrayidx170 = getelementptr inbounds [2 x i32], [2 x i32]* %By, i32 0, i32 0
  %118 = load i32, i32* %arrayidx170, align 4, !tbaa !6
  %conv171 = sext i32 %118 to i64
  %mul172 = mul nsw i64 %conv169, %conv171
  %arrayidx173 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx174 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx173, i32 0, i32 1
  %119 = load i32, i32* %arrayidx174, align 4, !tbaa !6
  %conv175 = sext i32 %119 to i64
  %arrayidx176 = getelementptr inbounds [2 x i32], [2 x i32]* %By, i32 0, i32 1
  %120 = load i32, i32* %arrayidx176, align 4, !tbaa !6
  %conv177 = sext i32 %120 to i64
  %mul178 = mul nsw i64 %conv175, %conv177
  %sub179 = sub nsw i64 %mul172, %mul178
  %arrayidx180 = getelementptr inbounds [2 x i64], [2 x i64]* %Py, i32 0, i32 0
  store i64 %sub179, i64* %arrayidx180, align 16, !tbaa !13
  %arrayidx181 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx182 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx181, i32 0, i32 1
  %121 = load i32, i32* %arrayidx182, align 4, !tbaa !6
  %conv183 = sext i32 %121 to i64
  %sub184 = sub nsw i64 0, %conv183
  %arrayidx185 = getelementptr inbounds [2 x i32], [2 x i32]* %By, i32 0, i32 0
  %122 = load i32, i32* %arrayidx185, align 4, !tbaa !6
  %conv186 = sext i32 %122 to i64
  %mul187 = mul nsw i64 %sub184, %conv186
  %arrayidx188 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %A, i32 0, i32 0
  %arrayidx189 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx188, i32 0, i32 0
  %123 = load i32, i32* %arrayidx189, align 16, !tbaa !6
  %conv190 = sext i32 %123 to i64
  %arrayidx191 = getelementptr inbounds [2 x i32], [2 x i32]* %By, i32 0, i32 1
  %124 = load i32, i32* %arrayidx191, align 4, !tbaa !6
  %conv192 = sext i32 %124 to i64
  %mul193 = mul nsw i64 %conv190, %conv192
  %add194 = add nsw i64 %mul187, %mul193
  %arrayidx195 = getelementptr inbounds [2 x i64], [2 x i64]* %Py, i32 0, i32 1
  store i64 %add194, i64* %arrayidx195, align 8, !tbaa !13
  %arrayidx196 = getelementptr inbounds [2 x i64], [2 x i64]* %Px, i32 0, i32 0
  %125 = load i64, i64* %arrayidx196, align 16, !tbaa !13
  %126 = load i16, i16* %iDet, align 2, !tbaa !12
  %127 = load i16, i16* %shift, align 2, !tbaa !12
  %conv197 = sext i16 %127 to i32
  %call198 = call i32 @get_mult_shift_diag(i64 %125, i16 signext %126, i32 %conv197)
  %128 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %128, i32 0, i32 0
  %arrayidx199 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat, i32 0, i32 2
  store i32 %call198, i32* %arrayidx199, align 4, !tbaa !6
  %arrayidx200 = getelementptr inbounds [2 x i64], [2 x i64]* %Px, i32 0, i32 1
  %129 = load i64, i64* %arrayidx200, align 8, !tbaa !13
  %130 = load i16, i16* %iDet, align 2, !tbaa !12
  %131 = load i16, i16* %shift, align 2, !tbaa !12
  %conv201 = sext i16 %131 to i32
  %call202 = call i32 @get_mult_shift_ndiag(i64 %129, i16 signext %130, i32 %conv201)
  %132 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat203 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %132, i32 0, i32 0
  %arrayidx204 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat203, i32 0, i32 3
  store i32 %call202, i32* %arrayidx204, align 4, !tbaa !6
  %arrayidx205 = getelementptr inbounds [2 x i64], [2 x i64]* %Py, i32 0, i32 0
  %133 = load i64, i64* %arrayidx205, align 16, !tbaa !13
  %134 = load i16, i16* %iDet, align 2, !tbaa !12
  %135 = load i16, i16* %shift, align 2, !tbaa !12
  %conv206 = sext i16 %135 to i32
  %call207 = call i32 @get_mult_shift_ndiag(i64 %133, i16 signext %134, i32 %conv206)
  %136 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat208 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %136, i32 0, i32 0
  %arrayidx209 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat208, i32 0, i32 4
  store i32 %call207, i32* %arrayidx209, align 4, !tbaa !6
  %arrayidx210 = getelementptr inbounds [2 x i64], [2 x i64]* %Py, i32 0, i32 1
  %137 = load i64, i64* %arrayidx210, align 8, !tbaa !13
  %138 = load i16, i16* %iDet, align 2, !tbaa !12
  %139 = load i16, i16* %shift, align 2, !tbaa !12
  %conv211 = sext i16 %139 to i32
  %call212 = call i32 @get_mult_shift_diag(i64 %137, i16 signext %138, i32 %conv211)
  %140 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat213 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %140, i32 0, i32 0
  %arrayidx214 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat213, i32 0, i32 5
  store i32 %call212, i32* %arrayidx214, align 4, !tbaa !6
  %141 = bitcast i32* %isuy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %141) #6
  %142 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %mul215 = mul nsw i32 %142, 4
  %143 = load i32, i32* %rsuy, align 4, !tbaa !6
  %add216 = add nsw i32 %mul215, %143
  store i32 %add216, i32* %isuy, align 4, !tbaa !6
  %144 = bitcast i32* %isux to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #6
  %145 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %mul217 = mul nsw i32 %145, 4
  %146 = load i32, i32* %rsux, align 4, !tbaa !6
  %add218 = add nsw i32 %mul217, %146
  store i32 %add218, i32* %isux, align 4, !tbaa !6
  %147 = bitcast i32* %vx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %147) #6
  %148 = load i32, i32* %mvx.addr, align 4, !tbaa !6
  %mul219 = mul nsw i32 %148, 8192
  %149 = load i32, i32* %isux, align 4, !tbaa !6
  %150 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat220 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %150, i32 0, i32 0
  %arrayidx221 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat220, i32 0, i32 2
  %151 = load i32, i32* %arrayidx221, align 4, !tbaa !6
  %sub222 = sub nsw i32 %151, 65536
  %mul223 = mul nsw i32 %149, %sub222
  %152 = load i32, i32* %isuy, align 4, !tbaa !6
  %153 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat224 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %153, i32 0, i32 0
  %arrayidx225 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat224, i32 0, i32 3
  %154 = load i32, i32* %arrayidx225, align 4, !tbaa !6
  %mul226 = mul nsw i32 %152, %154
  %add227 = add nsw i32 %mul223, %mul226
  %sub228 = sub nsw i32 %mul219, %add227
  store i32 %sub228, i32* %vx, align 4, !tbaa !6
  %155 = bitcast i32* %vy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %155) #6
  %156 = load i32, i32* %mvy.addr, align 4, !tbaa !6
  %mul229 = mul nsw i32 %156, 8192
  %157 = load i32, i32* %isux, align 4, !tbaa !6
  %158 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat230 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %158, i32 0, i32 0
  %arrayidx231 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat230, i32 0, i32 4
  %159 = load i32, i32* %arrayidx231, align 4, !tbaa !6
  %mul232 = mul nsw i32 %157, %159
  %160 = load i32, i32* %isuy, align 4, !tbaa !6
  %161 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat233 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %161, i32 0, i32 0
  %arrayidx234 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat233, i32 0, i32 5
  %162 = load i32, i32* %arrayidx234, align 4, !tbaa !6
  %sub235 = sub nsw i32 %162, 65536
  %mul236 = mul nsw i32 %160, %sub235
  %add237 = add nsw i32 %mul232, %mul236
  %sub238 = sub nsw i32 %mul229, %add237
  store i32 %sub238, i32* %vy, align 4, !tbaa !6
  %163 = load i32, i32* %vx, align 4, !tbaa !6
  %call239 = call i32 @clamp(i32 %163, i32 -8388608, i32 8388607)
  %164 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat240 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %164, i32 0, i32 0
  %arrayidx241 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat240, i32 0, i32 0
  store i32 %call239, i32* %arrayidx241, align 4, !tbaa !6
  %165 = load i32, i32* %vy, align 4, !tbaa !6
  %call242 = call i32 @clamp(i32 %165, i32 -8388608, i32 8388607)
  %166 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat243 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %166, i32 0, i32 0
  %arrayidx244 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat243, i32 0, i32 1
  store i32 %call242, i32* %arrayidx244, align 4, !tbaa !6
  %167 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat245 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %167, i32 0, i32 0
  %arrayidx246 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat245, i32 0, i32 7
  store i32 0, i32* %arrayidx246, align 4, !tbaa !6
  %168 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm.addr, align 4, !tbaa !2
  %wmmat247 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %168, i32 0, i32 0
  %arrayidx248 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat247, i32 0, i32 6
  store i32 0, i32* %arrayidx248, align 4, !tbaa !6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %169 = bitcast i32* %vy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #6
  %170 = bitcast i32* %vx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #6
  %171 = bitcast i32* %isux to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #6
  %172 = bitcast i32* %isuy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #6
  %173 = bitcast [2 x i64]* %Py to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %173) #6
  %174 = bitcast [2 x i64]* %Px to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %174) #6
  %175 = bitcast i16* %iDet to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %175) #6
  %176 = bitcast i16* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %176) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end137, %if.then117
  %177 = bitcast i64* %Det to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %177) #6
  %178 = bitcast i32* %dux to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #6
  %179 = bitcast i32* %duy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #6
  %180 = bitcast i32* %sux to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #6
  %181 = bitcast i32* %suy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #6
  %182 = bitcast i32* %rsux to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #6
  %183 = bitcast i32* %rsuy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #6
  %184 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #6
  %185 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #6
  %186 = bitcast [2 x i32]* %By to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %186) #6
  %187 = bitcast [2 x i32]* %Bx to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %187) #6
  %188 = bitcast [2 x [2 x i32]]* %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %188) #6
  %189 = load i32, i32* %retval, align 4
  ret i32 %189
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #2 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #4

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #5

; Function Attrs: nounwind
define internal signext i16 @resolve_divisor_64(i64 %D, i16* %shift) #0 {
entry:
  %D.addr = alloca i64, align 8
  %shift.addr = alloca i16*, align 4
  %f = alloca i64, align 8
  %e = alloca i64, align 8
  store i64 %D, i64* %D.addr, align 8, !tbaa !13
  store i16* %shift, i16** %shift.addr, align 4, !tbaa !2
  %0 = bitcast i64* %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = load i64, i64* %D.addr, align 8, !tbaa !13
  %shr = lshr i64 %1, 32
  %tobool = icmp ne i64 %shr, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i64, i64* %D.addr, align 8, !tbaa !13
  %shr1 = lshr i64 %2, 32
  %conv = trunc i64 %shr1 to i32
  %call = call i32 @get_msb(i32 %conv)
  %add = add nsw i32 %call, 32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i64, i64* %D.addr, align 8, !tbaa !13
  %conv2 = trunc i64 %3 to i32
  %call3 = call i32 @get_msb(i32 %conv2)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ %call3, %cond.false ]
  %conv4 = trunc i32 %cond to i16
  %4 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  store i16 %conv4, i16* %4, align 2, !tbaa !12
  %5 = bitcast i64* %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #6
  %6 = load i64, i64* %D.addr, align 8, !tbaa !13
  %7 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %8 = load i16, i16* %7, align 2, !tbaa !12
  %conv5 = sext i16 %8 to i32
  %sh_prom = zext i32 %conv5 to i64
  %shl = shl i64 1, %sh_prom
  %sub = sub i64 %6, %shl
  store i64 %sub, i64* %e, align 8, !tbaa !13
  %9 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %10 = load i16, i16* %9, align 2, !tbaa !12
  %conv6 = sext i16 %10 to i32
  %cmp = icmp sgt i32 %conv6, 8
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %11 = load i64, i64* %e, align 8, !tbaa !13
  %12 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %13 = load i16, i16* %12, align 2, !tbaa !12
  %conv8 = sext i16 %13 to i32
  %sub9 = sub nsw i32 %conv8, 8
  %sh_prom10 = zext i32 %sub9 to i64
  %shl11 = shl i64 1, %sh_prom10
  %shr12 = ashr i64 %shl11, 1
  %add13 = add nsw i64 %11, %shr12
  %14 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %15 = load i16, i16* %14, align 2, !tbaa !12
  %conv14 = sext i16 %15 to i32
  %sub15 = sub nsw i32 %conv14, 8
  %sh_prom16 = zext i32 %sub15 to i64
  %shr17 = ashr i64 %add13, %sh_prom16
  store i64 %shr17, i64* %f, align 8, !tbaa !13
  br label %if.end

if.else:                                          ; preds = %cond.end
  %16 = load i64, i64* %e, align 8, !tbaa !13
  %17 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %18 = load i16, i16* %17, align 2, !tbaa !12
  %conv18 = sext i16 %18 to i32
  %sub19 = sub nsw i32 8, %conv18
  %sh_prom20 = zext i32 %sub19 to i64
  %shl21 = shl i64 %16, %sh_prom20
  store i64 %shl21, i64* %f, align 8, !tbaa !13
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %19 = load i16*, i16** %shift.addr, align 4, !tbaa !2
  %20 = load i16, i16* %19, align 2, !tbaa !12
  %conv22 = sext i16 %20 to i32
  %add23 = add nsw i32 %conv22, 14
  %conv24 = trunc i32 %add23 to i16
  store i16 %conv24, i16* %19, align 2, !tbaa !12
  %21 = load i64, i64* %f, align 8, !tbaa !13
  %idxprom = trunc i64 %21 to i32
  %arrayidx = getelementptr inbounds [257 x i16], [257 x i16]* @div_lut, i32 0, i32 %idxprom
  %22 = load i16, i16* %arrayidx, align 2, !tbaa !12
  %23 = bitcast i64* %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #6
  %24 = bitcast i64* %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %24) #6
  ret i16 %22
}

; Function Attrs: nounwind readnone
declare i64 @llabs(i64) #3

; Function Attrs: nounwind
define internal i32 @get_mult_shift_diag(i64 %Px, i16 signext %iDet, i32 %shift) #0 {
entry:
  %Px.addr = alloca i64, align 8
  %iDet.addr = alloca i16, align 2
  %shift.addr = alloca i32, align 4
  %v = alloca i64, align 8
  store i64 %Px, i64* %Px.addr, align 8, !tbaa !13
  store i16 %iDet, i16* %iDet.addr, align 2, !tbaa !12
  store i32 %shift, i32* %shift.addr, align 4, !tbaa !6
  %0 = bitcast i64* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = load i64, i64* %Px.addr, align 8, !tbaa !13
  %2 = load i16, i16* %iDet.addr, align 2, !tbaa !12
  %conv = sext i16 %2 to i64
  %mul = mul nsw i64 %1, %conv
  store i64 %mul, i64* %v, align 8, !tbaa !13
  %3 = load i64, i64* %v, align 8, !tbaa !13
  %cmp = icmp slt i64 %3, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i64, i64* %v, align 8, !tbaa !13
  %sub = sub nsw i64 0, %4
  %5 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sh_prom = zext i32 %5 to i64
  %shl = shl i64 1, %sh_prom
  %shr = ashr i64 %shl, 1
  %add = add nsw i64 %sub, %shr
  %6 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sh_prom2 = zext i32 %6 to i64
  %shr3 = ashr i64 %add, %sh_prom2
  %sub4 = sub nsw i64 0, %shr3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %7 = load i64, i64* %v, align 8, !tbaa !13
  %8 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sh_prom5 = zext i32 %8 to i64
  %shl6 = shl i64 1, %sh_prom5
  %shr7 = ashr i64 %shl6, 1
  %add8 = add nsw i64 %7, %shr7
  %9 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sh_prom9 = zext i32 %9 to i64
  %shr10 = ashr i64 %add8, %sh_prom9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i64 [ %sub4, %cond.true ], [ %shr10, %cond.false ]
  %call = call i64 @clamp64(i64 %cond, i64 57345, i64 73727)
  %conv11 = trunc i64 %call to i32
  %10 = bitcast i64* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #6
  ret i32 %conv11
}

; Function Attrs: nounwind
define internal i32 @get_mult_shift_ndiag(i64 %Px, i16 signext %iDet, i32 %shift) #0 {
entry:
  %Px.addr = alloca i64, align 8
  %iDet.addr = alloca i16, align 2
  %shift.addr = alloca i32, align 4
  %v = alloca i64, align 8
  store i64 %Px, i64* %Px.addr, align 8, !tbaa !13
  store i16 %iDet, i16* %iDet.addr, align 2, !tbaa !12
  store i32 %shift, i32* %shift.addr, align 4, !tbaa !6
  %0 = bitcast i64* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = load i64, i64* %Px.addr, align 8, !tbaa !13
  %2 = load i16, i16* %iDet.addr, align 2, !tbaa !12
  %conv = sext i16 %2 to i64
  %mul = mul nsw i64 %1, %conv
  store i64 %mul, i64* %v, align 8, !tbaa !13
  %3 = load i64, i64* %v, align 8, !tbaa !13
  %cmp = icmp slt i64 %3, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i64, i64* %v, align 8, !tbaa !13
  %sub = sub nsw i64 0, %4
  %5 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sh_prom = zext i32 %5 to i64
  %shl = shl i64 1, %sh_prom
  %shr = ashr i64 %shl, 1
  %add = add nsw i64 %sub, %shr
  %6 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sh_prom2 = zext i32 %6 to i64
  %shr3 = ashr i64 %add, %sh_prom2
  %sub4 = sub nsw i64 0, %shr3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %7 = load i64, i64* %v, align 8, !tbaa !13
  %8 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sh_prom5 = zext i32 %8 to i64
  %shl6 = shl i64 1, %sh_prom5
  %shr7 = ashr i64 %shl6, 1
  %add8 = add nsw i64 %7, %shr7
  %9 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sh_prom9 = zext i32 %9 to i64
  %shr10 = ashr i64 %add8, %sh_prom9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i64 [ %sub4, %cond.true ], [ %shr10, %cond.false ]
  %call = call i64 @clamp64(i64 %cond, i64 -8191, i64 8191)
  %conv11 = trunc i64 %call to i32
  %10 = bitcast i64* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #6
  ret i32 %conv11
}

; Function Attrs: inlinehint nounwind
define internal i64 @clamp64(i64 %value, i64 %low, i64 %high) #2 {
entry:
  %value.addr = alloca i64, align 8
  %low.addr = alloca i64, align 8
  %high.addr = alloca i64, align 8
  store i64 %value, i64* %value.addr, align 8, !tbaa !13
  store i64 %low, i64* %low.addr, align 8, !tbaa !13
  store i64 %high, i64* %high.addr, align 8, !tbaa !13
  %0 = load i64, i64* %value.addr, align 8, !tbaa !13
  %1 = load i64, i64* %low.addr, align 8, !tbaa !13
  %cmp = icmp slt i64 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i64, i64* %low.addr, align 8, !tbaa !13
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i64, i64* %value.addr, align 8, !tbaa !13
  %4 = load i64, i64* %high.addr, align 8, !tbaa !13
  %cmp1 = icmp sgt i64 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i64, i64* %high.addr, align 8, !tbaa !13
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i64, i64* %value.addr, align 8, !tbaa !13
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i64 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i64 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i64 %cond5
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { argmemonly nounwind willreturn writeonly }
attributes #6 = { nounwind }
attributes #7 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !10, i64 32}
!9 = !{!"", !4, i64 0, !10, i64 32, !10, i64 34, !10, i64 36, !10, i64 38, !4, i64 40, !4, i64 41}
!10 = !{!"short", !4, i64 0}
!11 = !{!9, !10, i64 34}
!12 = !{!10, !10, i64 0}
!13 = !{!14, !14, i64 0}
!14 = !{!"long long", !4, i64 0}
!15 = !{!9, !10, i64 36}
!16 = !{!9, !10, i64 38}
!17 = !{!18, !7, i64 12}
!18 = !{!"ConvolveParams", !7, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40}
!19 = !{!18, !7, i64 24}
!20 = !{!18, !7, i64 16}
!21 = !{!18, !3, i64 4}
!22 = !{!18, !7, i64 8}
!23 = !{!18, !7, i64 0}
!24 = !{!18, !7, i64 32}
!25 = !{!18, !7, i64 36}
!26 = !{!18, !7, i64 40}
!27 = !{!9, !4, i64 40}
!28 = !{!4, !4, i64 0}
