; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/blend_a64_vmask.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/blend_a64_vmask.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @aom_blend_a64_vmask_c(i8* %dst, i32 %dst_stride, i8* %src0, i32 %src0_stride, i8* %src1, i32 %src1_stride, i8* %mask, i32 %w, i32 %h) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src0.addr = alloca i8*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i8*, align 4
  %src1_stride.addr = alloca i32, align 4
  %mask.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %m = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i8* %src0, i8** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i8* %src1, i8** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc19, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end21

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %7 to i32
  store i32 %conv, i32* %m, align 4, !tbaa !6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %8 = load i32, i32* %j, align 4, !tbaa !6
  %9 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond1
  %10 = load i32, i32* %m, align 4, !tbaa !6
  %11 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %13 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul = mul i32 %12, %13
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %add = add i32 %mul, %14
  %arrayidx5 = getelementptr inbounds i8, i8* %11, i32 %add
  %15 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv6 = zext i8 %15 to i32
  %mul7 = mul nsw i32 %10, %conv6
  %16 = load i32, i32* %m, align 4, !tbaa !6
  %sub = sub nsw i32 64, %16
  %17 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %19 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul8 = mul i32 %18, %19
  %20 = load i32, i32* %j, align 4, !tbaa !6
  %add9 = add i32 %mul8, %20
  %arrayidx10 = getelementptr inbounds i8, i8* %17, i32 %add9
  %21 = load i8, i8* %arrayidx10, align 1, !tbaa !8
  %conv11 = zext i8 %21 to i32
  %mul12 = mul nsw i32 %sub, %conv11
  %add13 = add nsw i32 %mul7, %mul12
  %add14 = add nsw i32 %add13, 32
  %shr = ashr i32 %add14, 6
  %conv15 = trunc i32 %shr to i8
  %22 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %24 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul16 = mul i32 %23, %24
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %add17 = add i32 %mul16, %25
  %arrayidx18 = getelementptr inbounds i8, i8* %22, i32 %add17
  store i8 %conv15, i8* %arrayidx18, align 1, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %27 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #2
  br label %for.inc19

for.inc19:                                        ; preds = %for.end
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %inc20 = add nsw i32 %28, 1
  store i32 %inc20, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end21:                                        ; preds = %for.cond
  %29 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #2
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_highbd_blend_a64_vmask_c(i8* %dst_8, i32 %dst_stride, i8* %src0_8, i32 %src0_stride, i8* %src1_8, i32 %src1_stride, i8* %mask, i32 %w, i32 %h, i32 %bd) #0 {
entry:
  %dst_8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src0_8.addr = alloca i8*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1_8.addr = alloca i8*, align 4
  %src1_stride.addr = alloca i32, align 4
  %mask.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %dst = alloca i16*, align 4
  %src0 = alloca i16*, align 4
  %src1 = alloca i16*, align 4
  %m = alloca i32, align 4
  store i8* %dst_8, i8** %dst_8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i8* %src0_8, i8** %src0_8.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i8* %src1_8, i8** %src1_8.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = load i8*, i8** %dst_8.addr, align 4, !tbaa !2
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  store i16* %5, i16** %dst, align 4, !tbaa !2
  %6 = bitcast i16** %src0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = load i8*, i8** %src0_8.addr, align 4, !tbaa !2
  %8 = ptrtoint i8* %7 to i32
  %shl1 = shl i32 %8, 1
  %9 = inttoptr i32 %shl1 to i16*
  store i16* %9, i16** %src0, align 4, !tbaa !2
  %10 = bitcast i16** %src1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = load i8*, i8** %src1_8.addr, align 4, !tbaa !2
  %12 = ptrtoint i8* %11 to i32
  %shl2 = shl i32 %12, 1
  %13 = inttoptr i32 %shl2 to i16*
  store i16* %13, i16** %src1, align 4, !tbaa !2
  %14 = load i32, i32* %bd.addr, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %entry
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %16 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %15, %16
  br i1 %cmp, label %for.body, label %for.end23

for.body:                                         ; preds = %for.cond
  %17 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %18, i32 %19
  %20 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %20 to i32
  store i32 %conv, i32* %m, align 4, !tbaa !6
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %22 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %21, %22
  br i1 %cmp4, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond3
  %23 = load i32, i32* %m, align 4, !tbaa !6
  %24 = load i16*, i16** %src0, align 4, !tbaa !2
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %26 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul = mul i32 %25, %26
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %add = add i32 %mul, %27
  %arrayidx7 = getelementptr inbounds i16, i16* %24, i32 %add
  %28 = load i16, i16* %arrayidx7, align 2, !tbaa !9
  %conv8 = zext i16 %28 to i32
  %mul9 = mul nsw i32 %23, %conv8
  %29 = load i32, i32* %m, align 4, !tbaa !6
  %sub = sub nsw i32 64, %29
  %30 = load i16*, i16** %src1, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %32 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul10 = mul i32 %31, %32
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %add11 = add i32 %mul10, %33
  %arrayidx12 = getelementptr inbounds i16, i16* %30, i32 %add11
  %34 = load i16, i16* %arrayidx12, align 2, !tbaa !9
  %conv13 = zext i16 %34 to i32
  %mul14 = mul nsw i32 %sub, %conv13
  %add15 = add nsw i32 %mul9, %mul14
  %add16 = add nsw i32 %add15, 32
  %shr = ashr i32 %add16, 6
  %conv17 = trunc i32 %shr to i16
  %35 = load i16*, i16** %dst, align 4, !tbaa !2
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul18 = mul i32 %36, %37
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %add19 = add i32 %mul18, %38
  %arrayidx20 = getelementptr inbounds i16, i16* %35, i32 %add19
  store i16 %conv17, i16* %arrayidx20, align 2, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %39 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %39, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  %40 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #2
  br label %for.inc21

for.inc21:                                        ; preds = %for.end
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %inc22 = add nsw i32 %41, 1
  store i32 %inc22, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end23:                                        ; preds = %for.cond
  %42 = bitcast i16** %src1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #2
  %43 = bitcast i16** %src0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #2
  %44 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #2
  %45 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #2
  %46 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #2
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"short", !4, i64 0}
