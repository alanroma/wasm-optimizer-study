; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/aom_convolve.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/aom_convolve.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @aom_convolve8_horiz_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i16* %filter_x, i32 %x_step_q4, i16* %filter_y, i32 %y_step_q4, i32 %w, i32 %h) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %filter_x.addr = alloca i16*, align 4
  %x_step_q4.addr = alloca i32, align 4
  %filter_y.addr = alloca i16*, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filters_x = alloca [8 x i16]*, align 4
  %x0_q4 = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i16* %filter_x, i16** %filter_x.addr, align 4, !tbaa !2
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !8
  store i16* %filter_y, i16** %filter_y.addr, align 4, !tbaa !2
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  %0 = bitcast [8 x i16]** %filters_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %call = call [8 x i16]* @get_filter_base(i16* %1)
  store [8 x i16]* %call, [8 x i16]** %filters_x, align 4, !tbaa !2
  %2 = bitcast i32* %x0_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %4 = load [8 x i16]*, [8 x i16]** %filters_x, align 4, !tbaa !2
  %call1 = call i32 @get_filter_offset(i16* %3, [8 x i16]* %4)
  store i32 %call1, i32* %x0_q4, align 4, !tbaa !8
  %5 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %6 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !8
  %7 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %11 = load [8 x i16]*, [8 x i16]** %filters_x, align 4, !tbaa !2
  %12 = load i32, i32* %x0_q4, align 4, !tbaa !8
  %13 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !8
  %14 = load i32, i32* %w.addr, align 4, !tbaa !8
  %15 = load i32, i32* %h.addr, align 4, !tbaa !8
  call void @convolve_horiz(i8* %7, i32 %8, i8* %9, i32 %10, [8 x i16]* %11, i32 %12, i32 %13, i32 %14, i32 %15)
  %16 = bitcast i32* %x0_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %17 = bitcast [8 x i16]** %filters_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal [8 x i16]* @get_filter_base(i16* %filter) #0 {
entry:
  %filter.addr = alloca i16*, align 4
  store i16* %filter, i16** %filter.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %filter.addr, align 4, !tbaa !2
  %1 = ptrtoint i16* %0 to i32
  %and = and i32 %1, -256
  %2 = inttoptr i32 %and to [8 x i16]*
  ret [8 x i16]* %2
}

; Function Attrs: nounwind
define internal i32 @get_filter_offset(i16* %f, [8 x i16]* %base) #0 {
entry:
  %f.addr = alloca i16*, align 4
  %base.addr = alloca [8 x i16]*, align 4
  store i16* %f, i16** %f.addr, align 4, !tbaa !2
  store [8 x i16]* %base, [8 x i16]** %base.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %f.addr, align 4, !tbaa !2
  %1 = ptrtoint i16* %0 to i32
  %2 = inttoptr i32 %1 to [8 x i16]*
  %3 = load [8 x i16]*, [8 x i16]** %base.addr, align 4, !tbaa !2
  %sub.ptr.lhs.cast = ptrtoint [8 x i16]* %2 to i32
  %sub.ptr.rhs.cast = ptrtoint [8 x i16]* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 16
  ret i32 %sub.ptr.div
}

; Function Attrs: nounwind
define internal void @convolve_horiz(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, [8 x i16]* %x_filters, i32 %x0_q4, i32 %x_step_q4, i32 %w, i32 %h) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %x_filters.addr = alloca [8 x i16]*, align 4
  %x0_q4.addr = alloca i32, align 4
  %x_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x_q4 = alloca i32, align 4
  %x = alloca i32, align 4
  %src_x = alloca i8*, align 4
  %x_filter = alloca i16*, align 4
  %sum = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store [8 x i16]* %x_filters, [8 x i16]** %x_filters.addr, align 4, !tbaa !2
  store i32 %x0_q4, i32* %x0_q4.addr, align 4, !tbaa !8
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 -3
  store i8* %add.ptr, i8** %src.addr, align 4, !tbaa !2
  %1 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %y, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %entry
  %2 = load i32, i32* %y, align 4, !tbaa !8
  %3 = load i32, i32* %h.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #3
  br label %for.end14

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %x_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load i32, i32* %x0_q4.addr, align 4, !tbaa !8
  store i32 %6, i32* %x_q4, align 4, !tbaa !8
  %7 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  store i32 0, i32* %x, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %8 = load i32, i32* %x, align 4, !tbaa !8
  %9 = load i32, i32* %w.addr, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %10 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #3
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %11 = bitcast i8** %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %13 = load i32, i32* %x_q4, align 4, !tbaa !8
  %shr = ashr i32 %13, 4
  %arrayidx = getelementptr inbounds i8, i8* %12, i32 %shr
  store i8* %arrayidx, i8** %src_x, align 4, !tbaa !2
  %14 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load [8 x i16]*, [8 x i16]** %x_filters.addr, align 4, !tbaa !2
  %16 = load i32, i32* %x_q4, align 4, !tbaa !8
  %and = and i32 %16, 15
  %arrayidx5 = getelementptr inbounds [8 x i16], [8 x i16]* %15, i32 %and
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx5, i32 0, i32 0
  store i16* %arraydecay, i16** %x_filter, align 4, !tbaa !2
  %17 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load i8*, i8** %src_x, align 4, !tbaa !2
  %19 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %call = call i32 @horz_scalar_product(i8* %18, i16* %19)
  store i32 %call, i32* %sum, align 4, !tbaa !8
  %20 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %20, 64
  %shr6 = ashr i32 %add, 7
  %call7 = call zeroext i8 @clip_pixel(i32 %shr6)
  %21 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %22 = load i32, i32* %x, align 4, !tbaa !8
  %arrayidx8 = getelementptr inbounds i8, i8* %21, i32 %22
  store i8 %call7, i8* %arrayidx8, align 1, !tbaa !10
  %23 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !8
  %24 = load i32, i32* %x_q4, align 4, !tbaa !8
  %add9 = add nsw i32 %24, %23
  store i32 %add9, i32* %x_q4, align 4, !tbaa !8
  %25 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #3
  %26 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  %27 = bitcast i8** %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %28 = load i32, i32* %x, align 4, !tbaa !8
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %x, align 4, !tbaa !8
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %29 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %30 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i8, i8* %30, i32 %29
  store i8* %add.ptr10, i8** %src.addr, align 4, !tbaa !2
  %31 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %32 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i8, i8* %32, i32 %31
  store i8* %add.ptr11, i8** %dst.addr, align 4, !tbaa !2
  %33 = bitcast i32* %x_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #3
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %34 = load i32, i32* %y, align 4, !tbaa !8
  %inc13 = add nsw i32 %34, 1
  store i32 %inc13, i32* %y, align 4, !tbaa !8
  br label %for.cond

for.end14:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_convolve8_vert_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i16* %filter_x, i32 %x_step_q4, i16* %filter_y, i32 %y_step_q4, i32 %w, i32 %h) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %filter_x.addr = alloca i16*, align 4
  %x_step_q4.addr = alloca i32, align 4
  %filter_y.addr = alloca i16*, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filters_y = alloca [8 x i16]*, align 4
  %y0_q4 = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i16* %filter_x, i16** %filter_x.addr, align 4, !tbaa !2
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !8
  store i16* %filter_y, i16** %filter_y.addr, align 4, !tbaa !2
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  %0 = bitcast [8 x i16]** %filters_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %call = call [8 x i16]* @get_filter_base(i16* %1)
  store [8 x i16]* %call, [8 x i16]** %filters_y, align 4, !tbaa !2
  %2 = bitcast i32* %y0_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %4 = load [8 x i16]*, [8 x i16]** %filters_y, align 4, !tbaa !2
  %call1 = call i32 @get_filter_offset(i16* %3, [8 x i16]* %4)
  store i32 %call1, i32* %y0_q4, align 4, !tbaa !8
  %5 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %6 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !8
  %7 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %11 = load [8 x i16]*, [8 x i16]** %filters_y, align 4, !tbaa !2
  %12 = load i32, i32* %y0_q4, align 4, !tbaa !8
  %13 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !8
  %14 = load i32, i32* %w.addr, align 4, !tbaa !8
  %15 = load i32, i32* %h.addr, align 4, !tbaa !8
  call void @convolve_vert(i8* %7, i32 %8, i8* %9, i32 %10, [8 x i16]* %11, i32 %12, i32 %13, i32 %14, i32 %15)
  %16 = bitcast i32* %y0_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %17 = bitcast [8 x i16]** %filters_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  ret void
}

; Function Attrs: nounwind
define internal void @convolve_vert(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, [8 x i16]* %y_filters, i32 %y0_q4, i32 %y_step_q4, i32 %w, i32 %h) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %y_filters.addr = alloca [8 x i16]*, align 4
  %y0_q4.addr = alloca i32, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %x = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %y_q4 = alloca i32, align 4
  %y = alloca i32, align 4
  %src_y = alloca i8*, align 4
  %y_filter = alloca i16*, align 4
  %sum = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store [8 x i16]* %y_filters, [8 x i16]** %y_filters.addr, align 4, !tbaa !2
  store i32 %y0_q4, i32* %y0_q4.addr, align 4, !tbaa !8
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  %0 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %0, 3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %idx.neg
  store i8* %add.ptr, i8** %src.addr, align 4, !tbaa !2
  %2 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %x, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %3 = load i32, i32* %x, align 4, !tbaa !8
  %4 = load i32, i32* %w.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %5 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #3
  br label %for.end15

for.body:                                         ; preds = %for.cond
  %6 = bitcast i32* %y_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load i32, i32* %y0_q4.addr, align 4, !tbaa !8
  store i32 %7, i32* %y_q4, align 4, !tbaa !8
  %8 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  store i32 0, i32* %y, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %y, align 4, !tbaa !8
  %10 = load i32, i32* %h.addr, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %9, %10
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %12 = bitcast i8** %src_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %14 = load i32, i32* %y_q4, align 4, !tbaa !8
  %shr = ashr i32 %14, 4
  %15 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 %shr, %15
  %arrayidx = getelementptr inbounds i8, i8* %13, i32 %mul5
  store i8* %arrayidx, i8** %src_y, align 4, !tbaa !2
  %16 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load [8 x i16]*, [8 x i16]** %y_filters.addr, align 4, !tbaa !2
  %18 = load i32, i32* %y_q4, align 4, !tbaa !8
  %and = and i32 %18, 15
  %arrayidx6 = getelementptr inbounds [8 x i16], [8 x i16]* %17, i32 %and
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx6, i32 0, i32 0
  store i16* %arraydecay, i16** %y_filter, align 4, !tbaa !2
  %19 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = load i8*, i8** %src_y, align 4, !tbaa !2
  %21 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %22 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %call = call i32 @vert_scalar_product(i8* %20, i32 %21, i16* %22)
  store i32 %call, i32* %sum, align 4, !tbaa !8
  %23 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %23, 64
  %shr7 = ashr i32 %add, 7
  %call8 = call zeroext i8 @clip_pixel(i32 %shr7)
  %24 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %25 = load i32, i32* %y, align 4, !tbaa !8
  %26 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul9 = mul nsw i32 %25, %26
  %arrayidx10 = getelementptr inbounds i8, i8* %24, i32 %mul9
  store i8 %call8, i8* %arrayidx10, align 1, !tbaa !10
  %27 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !8
  %28 = load i32, i32* %y_q4, align 4, !tbaa !8
  %add11 = add nsw i32 %28, %27
  store i32 %add11, i32* %y_q4, align 4, !tbaa !8
  %29 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #3
  %30 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  %31 = bitcast i8** %src_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %32 = load i32, i32* %y, align 4, !tbaa !8
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %y, align 4, !tbaa !8
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %33 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %33, i32 1
  store i8* %incdec.ptr, i8** %src.addr, align 4, !tbaa !2
  %34 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %34, i32 1
  store i8* %incdec.ptr12, i8** %dst.addr, align 4, !tbaa !2
  %35 = bitcast i32* %y_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %36 = load i32, i32* %x, align 4, !tbaa !8
  %inc14 = add nsw i32 %36, 1
  store i32 %inc14, i32* %x, align 4, !tbaa !8
  br label %for.cond

for.end15:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_convolve_copy_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i16* %filter_x, i32 %filter_x_stride, i16* %filter_y, i32 %filter_y_stride, i32 %w, i32 %h) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %filter_x.addr = alloca i16*, align 4
  %filter_x_stride.addr = alloca i32, align 4
  %filter_y.addr = alloca i16*, align 4
  %filter_y_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %r = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i16* %filter_x, i16** %filter_x.addr, align 4, !tbaa !2
  store i32 %filter_x_stride, i32* %filter_x_stride.addr, align 4, !tbaa !8
  store i16* %filter_y, i16** %filter_y.addr, align 4, !tbaa !2
  store i32 %filter_y_stride, i32* %filter_y_stride.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %2 = load i32, i32* %filter_x_stride.addr, align 4, !tbaa !8
  %3 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %4 = load i32, i32* %filter_y_stride.addr, align 4, !tbaa !8
  %5 = load i32, i32* %h.addr, align 4, !tbaa !8
  store i32 %5, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %r, align 4, !tbaa !8
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %8 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %9 = load i32, i32* %w.addr, align 4, !tbaa !8
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %7, i8* align 1 %8, i32 %9, i1 false)
  %10 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %11 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %11, i32 %10
  store i8* %add.ptr, i8** %src.addr, align 4, !tbaa !2
  %12 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %13 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr1 = getelementptr inbounds i8, i8* %13, i32 %12
  store i8* %add.ptr1, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %r, align 4, !tbaa !8
  %dec = add nsw i32 %14, -1
  store i32 %dec, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden void @aom_highbd_convolve8_horiz_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i16* %filter_x, i32 %x_step_q4, i16* %filter_y, i32 %y_step_q4, i32 %w, i32 %h, i32 %bd) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %filter_x.addr = alloca i16*, align 4
  %x_step_q4.addr = alloca i32, align 4
  %filter_y.addr = alloca i16*, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %filters_x = alloca [8 x i16]*, align 4
  %x0_q4 = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i16* %filter_x, i16** %filter_x.addr, align 4, !tbaa !2
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !8
  store i16* %filter_y, i16** %filter_y.addr, align 4, !tbaa !2
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast [8 x i16]** %filters_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %call = call [8 x i16]* @get_filter_base(i16* %1)
  store [8 x i16]* %call, [8 x i16]** %filters_x, align 4, !tbaa !2
  %2 = bitcast i32* %x0_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %4 = load [8 x i16]*, [8 x i16]** %filters_x, align 4, !tbaa !2
  %call1 = call i32 @get_filter_offset(i16* %3, [8 x i16]* %4)
  store i32 %call1, i32* %x0_q4, align 4, !tbaa !8
  %5 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %6 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !8
  %7 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %11 = load [8 x i16]*, [8 x i16]** %filters_x, align 4, !tbaa !2
  %12 = load i32, i32* %x0_q4, align 4, !tbaa !8
  %13 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !8
  %14 = load i32, i32* %w.addr, align 4, !tbaa !8
  %15 = load i32, i32* %h.addr, align 4, !tbaa !8
  %16 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_convolve_horiz(i8* %7, i32 %8, i8* %9, i32 %10, [8 x i16]* %11, i32 %12, i32 %13, i32 %14, i32 %15, i32 %16)
  %17 = bitcast i32* %x0_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast [8 x i16]** %filters_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_convolve_horiz(i8* %src8, i32 %src_stride, i8* %dst8, i32 %dst_stride, [8 x i16]* %x_filters, i32 %x0_q4, i32 %x_step_q4, i32 %w, i32 %h, i32 %bd) #0 {
entry:
  %src8.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %x_filters.addr = alloca [8 x i16]*, align 4
  %x0_q4.addr = alloca i32, align 4
  %x_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %src = alloca i16*, align 4
  %dst = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x_q4 = alloca i32, align 4
  %x = alloca i32, align 4
  %src_x = alloca i16*, align 4
  %x_filter = alloca i16*, align 4
  %sum = alloca i32, align 4
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store [8 x i16]* %x_filters, [8 x i16]** %x_filters.addr, align 4, !tbaa !2
  store i32 %x0_q4, i32* %x0_q4.addr, align 4, !tbaa !8
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i16** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %2 = ptrtoint i8* %1 to i32
  %shl = shl i32 %2, 1
  %3 = inttoptr i32 %shl to i16*
  store i16* %3, i16** %src, align 4, !tbaa !2
  %4 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %6 = ptrtoint i8* %5 to i32
  %shl1 = shl i32 %6, 1
  %7 = inttoptr i32 %shl1 to i16*
  store i16* %7, i16** %dst, align 4, !tbaa !2
  %8 = load i16*, i16** %src, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %8, i32 -3
  store i16* %add.ptr, i16** %src, align 4, !tbaa !2
  %9 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  store i32 0, i32* %y, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %10 = load i32, i32* %y, align 4, !tbaa !8
  %11 = load i32, i32* %h.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %12 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #3
  br label %for.end15

for.body:                                         ; preds = %for.cond
  %13 = bitcast i32* %x_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = load i32, i32* %x0_q4.addr, align 4, !tbaa !8
  store i32 %14, i32* %x_q4, align 4, !tbaa !8
  %15 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  store i32 0, i32* %x, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %16 = load i32, i32* %x, align 4, !tbaa !8
  %17 = load i32, i32* %w.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %16, %17
  br i1 %cmp3, label %for.body5, label %for.cond.cleanup4

for.cond.cleanup4:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %18 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  br label %for.end

for.body5:                                        ; preds = %for.cond2
  %19 = bitcast i16** %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = load i16*, i16** %src, align 4, !tbaa !2
  %21 = load i32, i32* %x_q4, align 4, !tbaa !8
  %shr = ashr i32 %21, 4
  %arrayidx = getelementptr inbounds i16, i16* %20, i32 %shr
  store i16* %arrayidx, i16** %src_x, align 4, !tbaa !2
  %22 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = load [8 x i16]*, [8 x i16]** %x_filters.addr, align 4, !tbaa !2
  %24 = load i32, i32* %x_q4, align 4, !tbaa !8
  %and = and i32 %24, 15
  %arrayidx6 = getelementptr inbounds [8 x i16], [8 x i16]* %23, i32 %and
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx6, i32 0, i32 0
  store i16* %arraydecay, i16** %x_filter, align 4, !tbaa !2
  %25 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  %26 = load i16*, i16** %src_x, align 4, !tbaa !2
  %27 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %call = call i32 @highbd_horz_scalar_product(i16* %26, i16* %27)
  store i32 %call, i32* %sum, align 4, !tbaa !8
  %28 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %28, 64
  %shr7 = ashr i32 %add, 7
  %29 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %call8 = call zeroext i16 @clip_pixel_highbd(i32 %shr7, i32 %29)
  %30 = load i16*, i16** %dst, align 4, !tbaa !2
  %31 = load i32, i32* %x, align 4, !tbaa !8
  %arrayidx9 = getelementptr inbounds i16, i16* %30, i32 %31
  store i16 %call8, i16* %arrayidx9, align 2, !tbaa !11
  %32 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !8
  %33 = load i32, i32* %x_q4, align 4, !tbaa !8
  %add10 = add nsw i32 %33, %32
  store i32 %add10, i32* %x_q4, align 4, !tbaa !8
  %34 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #3
  %35 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  %36 = bitcast i16** %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %37 = load i32, i32* %x, align 4, !tbaa !8
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %x, align 4, !tbaa !8
  br label %for.cond2

for.end:                                          ; preds = %for.cond.cleanup4
  %38 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %39 = load i16*, i16** %src, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i16, i16* %39, i32 %38
  store i16* %add.ptr11, i16** %src, align 4, !tbaa !2
  %40 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %41 = load i16*, i16** %dst, align 4, !tbaa !2
  %add.ptr12 = getelementptr inbounds i16, i16* %41, i32 %40
  store i16* %add.ptr12, i16** %dst, align 4, !tbaa !2
  %42 = bitcast i32* %x_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #3
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %43 = load i32, i32* %y, align 4, !tbaa !8
  %inc14 = add nsw i32 %43, 1
  store i32 %inc14, i32* %y, align 4, !tbaa !8
  br label %for.cond

for.end15:                                        ; preds = %for.cond.cleanup
  %44 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  %45 = bitcast i16** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_convolve8_vert_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i16* %filter_x, i32 %x_step_q4, i16* %filter_y, i32 %y_step_q4, i32 %w, i32 %h, i32 %bd) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %filter_x.addr = alloca i16*, align 4
  %x_step_q4.addr = alloca i32, align 4
  %filter_y.addr = alloca i16*, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %filters_y = alloca [8 x i16]*, align 4
  %y0_q4 = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i16* %filter_x, i16** %filter_x.addr, align 4, !tbaa !2
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !8
  store i16* %filter_y, i16** %filter_y.addr, align 4, !tbaa !2
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast [8 x i16]** %filters_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %call = call [8 x i16]* @get_filter_base(i16* %1)
  store [8 x i16]* %call, [8 x i16]** %filters_y, align 4, !tbaa !2
  %2 = bitcast i32* %y0_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %4 = load [8 x i16]*, [8 x i16]** %filters_y, align 4, !tbaa !2
  %call1 = call i32 @get_filter_offset(i16* %3, [8 x i16]* %4)
  store i32 %call1, i32* %y0_q4, align 4, !tbaa !8
  %5 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %6 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !8
  %7 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %11 = load [8 x i16]*, [8 x i16]** %filters_y, align 4, !tbaa !2
  %12 = load i32, i32* %y0_q4, align 4, !tbaa !8
  %13 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !8
  %14 = load i32, i32* %w.addr, align 4, !tbaa !8
  %15 = load i32, i32* %h.addr, align 4, !tbaa !8
  %16 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_convolve_vert(i8* %7, i32 %8, i8* %9, i32 %10, [8 x i16]* %11, i32 %12, i32 %13, i32 %14, i32 %15, i32 %16)
  %17 = bitcast i32* %y0_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast [8 x i16]** %filters_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_convolve_vert(i8* %src8, i32 %src_stride, i8* %dst8, i32 %dst_stride, [8 x i16]* %y_filters, i32 %y0_q4, i32 %y_step_q4, i32 %w, i32 %h, i32 %bd) #0 {
entry:
  %src8.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %y_filters.addr = alloca [8 x i16]*, align 4
  %y0_q4.addr = alloca i32, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %src = alloca i16*, align 4
  %dst = alloca i16*, align 4
  %x = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %y_q4 = alloca i32, align 4
  %y = alloca i32, align 4
  %src_y = alloca i16*, align 4
  %y_filter = alloca i16*, align 4
  %sum = alloca i32, align 4
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store [8 x i16]* %y_filters, [8 x i16]** %y_filters.addr, align 4, !tbaa !2
  store i32 %y0_q4, i32* %y0_q4.addr, align 4, !tbaa !8
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i16** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %2 = ptrtoint i8* %1 to i32
  %shl = shl i32 %2, 1
  %3 = inttoptr i32 %shl to i16*
  store i16* %3, i16** %src, align 4, !tbaa !2
  %4 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %6 = ptrtoint i8* %5 to i32
  %shl1 = shl i32 %6, 1
  %7 = inttoptr i32 %shl1 to i16*
  store i16* %7, i16** %dst, align 4, !tbaa !2
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %8, 3
  %9 = load i16*, i16** %src, align 4, !tbaa !2
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i16, i16* %9, i32 %idx.neg
  store i16* %add.ptr, i16** %src, align 4, !tbaa !2
  %10 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  store i32 0, i32* %x, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc14, %entry
  %11 = load i32, i32* %x, align 4, !tbaa !8
  %12 = load i32, i32* %w.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #3
  br label %for.end16

for.body:                                         ; preds = %for.cond
  %14 = bitcast i32* %y_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load i32, i32* %y0_q4.addr, align 4, !tbaa !8
  store i32 %15, i32* %y_q4, align 4, !tbaa !8
  %16 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  store i32 0, i32* %y, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %17 = load i32, i32* %y, align 4, !tbaa !8
  %18 = load i32, i32* %h.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %17, %18
  br i1 %cmp3, label %for.body5, label %for.cond.cleanup4

for.cond.cleanup4:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  br label %for.end

for.body5:                                        ; preds = %for.cond2
  %20 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load i16*, i16** %src, align 4, !tbaa !2
  %22 = load i32, i32* %y_q4, align 4, !tbaa !8
  %shr = ashr i32 %22, 4
  %23 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 %shr, %23
  %arrayidx = getelementptr inbounds i16, i16* %21, i32 %mul6
  store i16* %arrayidx, i16** %src_y, align 4, !tbaa !2
  %24 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load [8 x i16]*, [8 x i16]** %y_filters.addr, align 4, !tbaa !2
  %26 = load i32, i32* %y_q4, align 4, !tbaa !8
  %and = and i32 %26, 15
  %arrayidx7 = getelementptr inbounds [8 x i16], [8 x i16]* %25, i32 %and
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx7, i32 0, i32 0
  store i16* %arraydecay, i16** %y_filter, align 4, !tbaa !2
  %27 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #3
  %28 = load i16*, i16** %src_y, align 4, !tbaa !2
  %29 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %30 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %call = call i32 @highbd_vert_scalar_product(i16* %28, i32 %29, i16* %30)
  store i32 %call, i32* %sum, align 4, !tbaa !8
  %31 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %31, 64
  %shr8 = ashr i32 %add, 7
  %32 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %call9 = call zeroext i16 @clip_pixel_highbd(i32 %shr8, i32 %32)
  %33 = load i16*, i16** %dst, align 4, !tbaa !2
  %34 = load i32, i32* %y, align 4, !tbaa !8
  %35 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 %34, %35
  %arrayidx11 = getelementptr inbounds i16, i16* %33, i32 %mul10
  store i16 %call9, i16* %arrayidx11, align 2, !tbaa !11
  %36 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !8
  %37 = load i32, i32* %y_q4, align 4, !tbaa !8
  %add12 = add nsw i32 %37, %36
  store i32 %add12, i32* %y_q4, align 4, !tbaa !8
  %38 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  %39 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  %40 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %41 = load i32, i32* %y, align 4, !tbaa !8
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %y, align 4, !tbaa !8
  br label %for.cond2

for.end:                                          ; preds = %for.cond.cleanup4
  %42 = load i16*, i16** %src, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %42, i32 1
  store i16* %incdec.ptr, i16** %src, align 4, !tbaa !2
  %43 = load i16*, i16** %dst, align 4, !tbaa !2
  %incdec.ptr13 = getelementptr inbounds i16, i16* %43, i32 1
  store i16* %incdec.ptr13, i16** %dst, align 4, !tbaa !2
  %44 = bitcast i32* %y_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  br label %for.inc14

for.inc14:                                        ; preds = %for.end
  %45 = load i32, i32* %x, align 4, !tbaa !8
  %inc15 = add nsw i32 %45, 1
  store i32 %inc15, i32* %x, align 4, !tbaa !8
  br label %for.cond

for.end16:                                        ; preds = %for.cond.cleanup
  %46 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast i16** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_convolve_copy_c(i8* %src8, i32 %src_stride, i8* %dst8, i32 %dst_stride, i16* %filter_x, i32 %filter_x_stride, i16* %filter_y, i32 %filter_y_stride, i32 %w, i32 %h, i32 %bd) #0 {
entry:
  %src8.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %filter_x.addr = alloca i16*, align 4
  %filter_x_stride.addr = alloca i32, align 4
  %filter_y.addr = alloca i16*, align 4
  %filter_y_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %src = alloca i16*, align 4
  %dst = alloca i16*, align 4
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i16* %filter_x, i16** %filter_x.addr, align 4, !tbaa !2
  store i32 %filter_x_stride, i32* %filter_x_stride.addr, align 4, !tbaa !8
  store i16* %filter_y, i16** %filter_y.addr, align 4, !tbaa !2
  store i32 %filter_y_stride, i32* %filter_y_stride.addr, align 4, !tbaa !8
  store i32 %w, i32* %w.addr, align 4, !tbaa !8
  store i32 %h, i32* %h.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i16** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %3 = ptrtoint i8* %2 to i32
  %shl = shl i32 %3, 1
  %4 = inttoptr i32 %shl to i16*
  store i16* %4, i16** %src, align 4, !tbaa !2
  %5 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %7 = ptrtoint i8* %6 to i32
  %shl1 = shl i32 %7, 1
  %8 = inttoptr i32 %shl1 to i16*
  store i16* %8, i16** %dst, align 4, !tbaa !2
  %9 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %10 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %11 = load i32, i32* %filter_x_stride.addr, align 4, !tbaa !8
  %12 = load i32, i32* %filter_y_stride.addr, align 4, !tbaa !8
  %13 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %14 = load i32, i32* %h.addr, align 4, !tbaa !8
  store i32 %14, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %r, align 4, !tbaa !8
  %cmp = icmp sgt i32 %15, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load i16*, i16** %dst, align 4, !tbaa !2
  %17 = bitcast i16* %16 to i8*
  %18 = load i16*, i16** %src, align 4, !tbaa !2
  %19 = bitcast i16* %18 to i8*
  %20 = load i32, i32* %w.addr, align 4, !tbaa !8
  %mul = mul i32 %20, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %17, i8* align 2 %19, i32 %mul, i1 false)
  %21 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %22 = load i16*, i16** %src, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %22, i32 %21
  store i16* %add.ptr, i16** %src, align 4, !tbaa !2
  %23 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %24 = load i16*, i16** %dst, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds i16, i16* %24, i32 %23
  store i16* %add.ptr2, i16** %dst, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %r, align 4, !tbaa !8
  %dec = add nsw i32 %25, -1
  store i32 %dec, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %26 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  %27 = bitcast i16** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  %28 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @horz_scalar_product(i8* %a, i16* %b) #2 {
entry:
  %a.addr = alloca i8*, align 4
  %b.addr = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  store i8* %a, i8** %a.addr, align 4, !tbaa !2
  store i16* %b, i16** %b.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %sum, align 4, !tbaa !8
  %1 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %k, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %5 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %6 to i32
  %7 = load i16*, i16** %b.addr, align 4, !tbaa !2
  %8 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx1 = getelementptr inbounds i16, i16* %7, i32 %8
  %9 = load i16, i16* %arrayidx1, align 2, !tbaa !11
  %conv2 = sext i16 %9 to i32
  %mul = mul nsw i32 %conv, %conv2
  %10 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %10, %mul
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %k, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %12 = load i32, i32* %sum, align 4, !tbaa !8
  %13 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #3
  ret i32 %12
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @clip_pixel(i32 %val) #2 {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !8
  %0 = load i32, i32* %val.addr, align 4, !tbaa !8
  %cmp = icmp sgt i32 %0, 255
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4, !tbaa !8
  %cmp1 = icmp slt i32 %1, 0
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %2 = load i32, i32* %val.addr, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ 0, %cond.true2 ], [ %2, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ 255, %cond.true ], [ %cond, %cond.end ]
  %conv = trunc i32 %cond5 to i8
  ret i8 %conv
}

; Function Attrs: inlinehint nounwind
define internal i32 @vert_scalar_product(i8* %a, i32 %a_stride, i16* %b) #2 {
entry:
  %a.addr = alloca i8*, align 4
  %a_stride.addr = alloca i32, align 4
  %b.addr = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  store i8* %a, i8** %a.addr, align 4, !tbaa !2
  store i32 %a_stride, i32* %a_stride.addr, align 4, !tbaa !6
  store i16* %b, i16** %b.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %sum, align 4, !tbaa !8
  %1 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %k, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %5 = load i32, i32* %k, align 4, !tbaa !8
  %6 = load i32, i32* %a_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %5, %6
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %mul
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %7 to i32
  %8 = load i16*, i16** %b.addr, align 4, !tbaa !2
  %9 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx1 = getelementptr inbounds i16, i16* %8, i32 %9
  %10 = load i16, i16* %arrayidx1, align 2, !tbaa !11
  %conv2 = sext i16 %10 to i32
  %mul3 = mul nsw i32 %conv, %conv2
  %11 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %11, %mul3
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %k, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %13 = load i32, i32* %sum, align 4, !tbaa !8
  %14 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  ret i32 %13
}

; Function Attrs: inlinehint nounwind
define internal i32 @highbd_horz_scalar_product(i16* %a, i16* %b) #2 {
entry:
  %a.addr = alloca i16*, align 4
  %b.addr = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  store i16* %a, i16** %a.addr, align 4, !tbaa !2
  store i16* %b, i16** %b.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %sum, align 4, !tbaa !8
  %1 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %k, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i16*, i16** %a.addr, align 4, !tbaa !2
  %5 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %4, i32 %5
  %6 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %6 to i32
  %7 = load i16*, i16** %b.addr, align 4, !tbaa !2
  %8 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx1 = getelementptr inbounds i16, i16* %7, i32 %8
  %9 = load i16, i16* %arrayidx1, align 2, !tbaa !11
  %conv2 = sext i16 %9 to i32
  %mul = mul nsw i32 %conv, %conv2
  %10 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %10, %mul
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %k, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %12 = load i32, i32* %sum, align 4, !tbaa !8
  %13 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #3
  ret i32 %12
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @clip_pixel_highbd(i32 %val, i32 %bd) #2 {
entry:
  %retval = alloca i16, align 2
  %val.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !8
  switch i32 %0, label %sw.default [
    i32 8, label %sw.bb
    i32 10, label %sw.bb1
    i32 12, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb
  %1 = load i32, i32* %val.addr, align 4, !tbaa !8
  %call = call i32 @clamp(i32 %1, i32 0, i32 255)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %retval, align 2
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !8
  %call2 = call i32 @clamp(i32 %2, i32 0, i32 1023)
  %conv3 = trunc i32 %call2 to i16
  store i16 %conv3, i16* %retval, align 2
  br label %return

sw.bb4:                                           ; preds = %entry
  %3 = load i32, i32* %val.addr, align 4, !tbaa !8
  %call5 = call i32 @clamp(i32 %3, i32 0, i32 4095)
  %conv6 = trunc i32 %call5 to i16
  store i16 %conv6, i16* %retval, align 2
  br label %return

return:                                           ; preds = %sw.bb4, %sw.bb1, %sw.default
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !8
  store i32 %low, i32* %low.addr, align 4, !tbaa !8
  store i32 %high, i32* %high.addr, align 4, !tbaa !8
  %0 = load i32, i32* %value.addr, align 4, !tbaa !8
  %1 = load i32, i32* %low.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !8
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !8
  %4 = load i32, i32* %high.addr, align 4, !tbaa !8
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !8
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal i32 @highbd_vert_scalar_product(i16* %a, i32 %a_stride, i16* %b) #2 {
entry:
  %a.addr = alloca i16*, align 4
  %a_stride.addr = alloca i32, align 4
  %b.addr = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  store i16* %a, i16** %a.addr, align 4, !tbaa !2
  store i32 %a_stride, i32* %a_stride.addr, align 4, !tbaa !6
  store i16* %b, i16** %b.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %sum, align 4, !tbaa !8
  %1 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %k, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i16*, i16** %a.addr, align 4, !tbaa !2
  %5 = load i32, i32* %k, align 4, !tbaa !8
  %6 = load i32, i32* %a_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %5, %6
  %arrayidx = getelementptr inbounds i16, i16* %4, i32 %mul
  %7 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %7 to i32
  %8 = load i16*, i16** %b.addr, align 4, !tbaa !2
  %9 = load i32, i32* %k, align 4, !tbaa !8
  %arrayidx1 = getelementptr inbounds i16, i16* %8, i32 %9
  %10 = load i16, i16* %arrayidx1, align 2, !tbaa !11
  %conv2 = sext i16 %10 to i32
  %mul3 = mul nsw i32 %conv, %conv2
  %11 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %11, %mul3
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %k, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %k, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %13 = load i32, i32* %sum, align 4, !tbaa !8
  %14 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  ret i32 %13
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!4, !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"short", !4, i64 0}
