; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/entropymode.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/entropymode.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }

@av1_get_palette_color_index_context.weights = internal constant [3 x i32] [i32 2, i32 1, i32 2], align 4
@av1_get_palette_color_index_context.hash_multipliers = internal constant [3 x i32] [i32 1, i32 2, i32 2], align 4
@palette_color_index_context_lookup = internal constant [9 x i32] [i32 -1, i32 -1, i32 0, i32 -1, i32 -1, i32 4, i32 3, i32 2, i32 1], align 16
@default_palette_y_size_cdf = internal constant [7 x [8 x i16]] [[8 x i16] [i16 24816, i16 19768, i16 14619, i16 11290, i16 7241, i16 3527, i16 0, i16 0], [8 x i16] [i16 25629, i16 21347, i16 16573, i16 13224, i16 9102, i16 4695, i16 0, i16 0], [8 x i16] [i16 24980, i16 20027, i16 15443, i16 12268, i16 8453, i16 4238, i16 0, i16 0], [8 x i16] [i16 24497, i16 18704, i16 14522, i16 11204, i16 7697, i16 4235, i16 0, i16 0], [8 x i16] [i16 20043, i16 13588, i16 10905, i16 7929, i16 5233, i16 2648, i16 0, i16 0], [8 x i16] [i16 23057, i16 17880, i16 15845, i16 11716, i16 7107, i16 4893, i16 0, i16 0], [8 x i16] [i16 17828, i16 11971, i16 11090, i16 8582, i16 5735, i16 3769, i16 0, i16 0]], align 16
@default_palette_uv_size_cdf = internal constant [7 x [8 x i16]] [[8 x i16] [i16 24055, i16 12789, i16 5640, i16 3159, i16 1437, i16 496, i16 0, i16 0], [8 x i16] [i16 26929, i16 17195, i16 9187, i16 5821, i16 2920, i16 1068, i16 0, i16 0], [8 x i16] [i16 28342, i16 21508, i16 14769, i16 11285, i16 6905, i16 3338, i16 0, i16 0], [8 x i16] [i16 29540, i16 23304, i16 17775, i16 14679, i16 10245, i16 5348, i16 0, i16 0], [8 x i16] [i16 29000, i16 23882, i16 19677, i16 14916, i16 10273, i16 5561, i16 0, i16 0], [8 x i16] [i16 30304, i16 24317, i16 19907, i16 11136, i16 7243, i16 4213, i16 0, i16 0], [8 x i16] [i16 31499, i16 27333, i16 22335, i16 13805, i16 11068, i16 6903, i16 0, i16 0]], align 16
@default_kf_y_mode_cdf = internal constant [5 x [5 x [14 x i16]]] [[5 x [14 x i16]] [[14 x i16] [i16 17180, i16 15741, i16 13430, i16 12550, i16 12086, i16 11658, i16 10943, i16 9524, i16 8579, i16 4603, i16 3675, i16 2302, i16 0, i16 0], [14 x i16] [i16 20752, i16 14702, i16 13252, i16 12465, i16 12049, i16 11324, i16 10880, i16 9736, i16 8334, i16 4110, i16 2596, i16 1359, i16 0, i16 0], [14 x i16] [i16 22716, i16 21997, i16 10472, i16 9980, i16 9713, i16 9529, i16 8635, i16 7148, i16 6608, i16 3432, i16 2839, i16 1201, i16 0, i16 0], [14 x i16] [i16 18677, i16 17362, i16 16326, i16 13960, i16 13632, i16 13222, i16 12770, i16 10672, i16 8022, i16 3183, i16 1810, i16 306, i16 0, i16 0], [14 x i16] [i16 20646, i16 19503, i16 17165, i16 16267, i16 14159, i16 12735, i16 10377, i16 7185, i16 6331, i16 2507, i16 1695, i16 293, i16 0, i16 0]], [5 x [14 x i16]] [[14 x i16] [i16 22745, i16 13183, i16 11920, i16 11328, i16 10936, i16 10008, i16 9679, i16 8745, i16 7387, i16 3754, i16 2286, i16 1332, i16 0, i16 0], [14 x i16] [i16 26785, i16 8669, i16 8208, i16 7882, i16 7702, i16 6973, i16 6855, i16 6345, i16 5158, i16 2863, i16 1492, i16 974, i16 0, i16 0], [14 x i16] [i16 25324, i16 19987, i16 12591, i16 12040, i16 11691, i16 11161, i16 10598, i16 9363, i16 8299, i16 4853, i16 3678, i16 2276, i16 0, i16 0], [14 x i16] [i16 24231, i16 18079, i16 17336, i16 15681, i16 15360, i16 14596, i16 14360, i16 12943, i16 8119, i16 3615, i16 1672, i16 558, i16 0, i16 0], [14 x i16] [i16 25225, i16 18537, i16 17272, i16 16573, i16 14863, i16 12051, i16 10784, i16 8252, i16 6767, i16 3093, i16 1787, i16 774, i16 0, i16 0]], [5 x [14 x i16]] [[14 x i16] [i16 20155, i16 19177, i16 11385, i16 10764, i16 10456, i16 10191, i16 9367, i16 7713, i16 7039, i16 3230, i16 2463, i16 691, i16 0, i16 0], [14 x i16] [i16 23081, i16 19298, i16 14262, i16 13538, i16 13164, i16 12621, i16 12073, i16 10706, i16 9549, i16 5025, i16 3557, i16 1861, i16 0, i16 0], [14 x i16] [i16 26585, i16 26263, i16 6744, i16 6516, i16 6402, i16 6334, i16 5686, i16 4414, i16 4213, i16 2301, i16 1974, i16 682, i16 0, i16 0], [14 x i16] [i16 22050, i16 21034, i16 17814, i16 15544, i16 15203, i16 14844, i16 14207, i16 11245, i16 8890, i16 3793, i16 2481, i16 516, i16 0, i16 0], [14 x i16] [i16 23574, i16 22910, i16 16267, i16 15505, i16 14344, i16 13597, i16 11205, i16 6807, i16 6207, i16 2696, i16 2031, i16 305, i16 0, i16 0]], [5 x [14 x i16]] [[14 x i16] [i16 20166, i16 18369, i16 17280, i16 14387, i16 13990, i16 13453, i16 13044, i16 11349, i16 7708, i16 3072, i16 1851, i16 359, i16 0, i16 0], [14 x i16] [i16 24565, i16 18947, i16 18244, i16 15663, i16 15329, i16 14637, i16 14364, i16 13300, i16 7543, i16 3283, i16 1610, i16 426, i16 0, i16 0], [14 x i16] [i16 24317, i16 23037, i16 17764, i16 15125, i16 14756, i16 14343, i16 13698, i16 11230, i16 8163, i16 3650, i16 2690, i16 750, i16 0, i16 0], [14 x i16] [i16 25054, i16 23720, i16 23252, i16 16101, i16 15951, i16 15774, i16 15615, i16 14001, i16 6025, i16 2379, i16 1232, i16 240, i16 0, i16 0], [14 x i16] [i16 23925, i16 22488, i16 21272, i16 17451, i16 16116, i16 14825, i16 13660, i16 10050, i16 6999, i16 2815, i16 1785, i16 283, i16 0, i16 0]], [5 x [14 x i16]] [[14 x i16] [i16 20190, i16 19097, i16 16789, i16 15934, i16 13693, i16 11855, i16 9779, i16 7319, i16 6549, i16 2554, i16 1618, i16 291, i16 0, i16 0], [14 x i16] [i16 23205, i16 19142, i16 17688, i16 16876, i16 15012, i16 11905, i16 10561, i16 8532, i16 7388, i16 3115, i16 1625, i16 491, i16 0, i16 0], [14 x i16] [i16 24412, i16 23867, i16 15152, i16 14512, i16 13418, i16 12662, i16 10170, i16 6821, i16 6302, i16 2868, i16 2245, i16 507, i16 0, i16 0], [14 x i16] [i16 21933, i16 20953, i16 19644, i16 16726, i16 15750, i16 14729, i16 13821, i16 10015, i16 8153, i16 3279, i16 1885, i16 286, i16 0, i16 0], [14 x i16] [i16 25150, i16 24480, i16 22909, i16 22259, i16 17382, i16 14111, i16 9865, i16 3992, i16 3588, i16 1413, i16 966, i16 175, i16 0, i16 0]]], align 16
@default_angle_delta_cdf = internal constant [8 x [8 x i16]] [[8 x i16] [i16 30588, i16 27736, i16 25201, i16 9992, i16 5779, i16 2551, i16 0, i16 0], [8 x i16] [i16 30467, i16 27160, i16 23967, i16 9281, i16 5794, i16 2438, i16 0, i16 0], [8 x i16] [i16 28988, i16 21750, i16 19069, i16 13414, i16 9685, i16 1482, i16 0, i16 0], [8 x i16] [i16 28187, i16 21542, i16 17621, i16 15630, i16 10934, i16 4371, i16 0, i16 0], [8 x i16] [i16 31031, i16 21841, i16 18259, i16 13180, i16 10023, i16 3945, i16 0, i16 0], [8 x i16] [i16 30104, i16 22592, i16 20283, i16 15118, i16 11168, i16 2273, i16 0, i16 0], [8 x i16] [i16 30528, i16 21672, i16 17315, i16 12427, i16 10207, i16 3851, i16 0, i16 0], [8 x i16] [i16 29163, i16 22340, i16 20309, i16 15092, i16 11524, i16 2113, i16 0, i16 0]], align 16
@default_comp_inter_cdf = internal constant [5 x [3 x i16]] [[3 x i16] [i16 5940, i16 0, i16 0], [3 x i16] [i16 8733, i16 0, i16 0], [3 x i16] [i16 20737, i16 0, i16 0], [3 x i16] [i16 22128, i16 0, i16 0], [3 x i16] [i16 29867, i16 0, i16 0]], align 16
@default_comp_ref_type_cdf = internal constant [5 x [3 x i16]] [[3 x i16] [i16 31570, i16 0, i16 0], [3 x i16] [i16 30698, i16 0, i16 0], [3 x i16] [i16 23602, i16 0, i16 0], [3 x i16] [i16 25269, i16 0, i16 0], [3 x i16] [i16 10293, i16 0, i16 0]], align 16
@default_uni_comp_ref_cdf = internal constant [3 x [3 x [3 x i16]]] [[3 x [3 x i16]] [[3 x i16] [i16 27484, i16 0, i16 0], [3 x i16] [i16 28903, i16 0, i16 0], [3 x i16] [i16 29640, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 9616, i16 0, i16 0], [3 x i16] [i16 18595, i16 0, i16 0], [3 x i16] [i16 17498, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 994, i16 0, i16 0], [3 x i16] [i16 7648, i16 0, i16 0], [3 x i16] [i16 6058, i16 0, i16 0]]], align 16
@default_palette_y_mode_cdf = internal constant [7 x [3 x [3 x i16]]] [[3 x [3 x i16]] [[3 x i16] [i16 1092, i16 0, i16 0], [3 x i16] [i16 29349, i16 0, i16 0], [3 x i16] [i16 31507, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 856, i16 0, i16 0], [3 x i16] [i16 29909, i16 0, i16 0], [3 x i16] [i16 31788, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 945, i16 0, i16 0], [3 x i16] [i16 29368, i16 0, i16 0], [3 x i16] [i16 31987, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 738, i16 0, i16 0], [3 x i16] [i16 29207, i16 0, i16 0], [3 x i16] [i16 31864, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 459, i16 0, i16 0], [3 x i16] [i16 25431, i16 0, i16 0], [3 x i16] [i16 31306, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 503, i16 0, i16 0], [3 x i16] [i16 28753, i16 0, i16 0], [3 x i16] [i16 31247, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 318, i16 0, i16 0], [3 x i16] [i16 24822, i16 0, i16 0], [3 x i16] [i16 32639, i16 0, i16 0]]], align 16
@default_palette_uv_mode_cdf = internal constant [2 x [3 x i16]] [[3 x i16] [i16 307, i16 0, i16 0], [3 x i16] [i16 11280, i16 0, i16 0]], align 2
@default_comp_ref_cdf = internal constant [3 x [3 x [3 x i16]]] [[3 x [3 x i16]] [[3 x i16] [i16 27822, i16 0, i16 0], [3 x i16] [i16 23300, i16 0, i16 0], [3 x i16] [i16 31265, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 12877, i16 0, i16 0], [3 x i16] [i16 10327, i16 0, i16 0], [3 x i16] [i16 17608, i16 0, i16 0]], [3 x [3 x i16]] [[3 x i16] [i16 2037, i16 0, i16 0], [3 x i16] [i16 1709, i16 0, i16 0], [3 x i16] [i16 5224, i16 0, i16 0]]], align 16
@default_comp_bwdref_cdf = internal constant [3 x [2 x [3 x i16]]] [[2 x [3 x i16]] [[3 x i16] [i16 30533, i16 0, i16 0], [3 x i16] [i16 31345, i16 0, i16 0]], [2 x [3 x i16]] [[3 x i16] [i16 15586, i16 0, i16 0], [3 x i16] [i16 17593, i16 0, i16 0]], [2 x [3 x i16]] [[3 x i16] [i16 2162, i16 0, i16 0], [3 x i16] [i16 2279, i16 0, i16 0]]], align 16
@default_single_ref_cdf = internal constant [3 x [6 x [3 x i16]]] [[6 x [3 x i16]] [[3 x i16] [i16 27871, i16 0, i16 0], [3 x i16] [i16 31213, i16 0, i16 0], [3 x i16] [i16 28532, i16 0, i16 0], [3 x i16] [i16 24118, i16 0, i16 0], [3 x i16] [i16 31864, i16 0, i16 0], [3 x i16] [i16 31324, i16 0, i16 0]], [6 x [3 x i16]] [[3 x i16] [i16 15795, i16 0, i16 0], [3 x i16] [i16 16017, i16 0, i16 0], [3 x i16] [i16 13121, i16 0, i16 0], [3 x i16] [i16 7995, i16 0, i16 0], [3 x i16] [i16 21754, i16 0, i16 0], [3 x i16] [i16 17681, i16 0, i16 0]], [6 x [3 x i16]] [[3 x i16] [i16 3024, i16 0, i16 0], [3 x i16] [i16 2489, i16 0, i16 0], [3 x i16] [i16 1574, i16 0, i16 0], [3 x i16] [i16 873, i16 0, i16 0], [3 x i16] [i16 5893, i16 0, i16 0], [3 x i16] [i16 2464, i16 0, i16 0]]], align 16
@default_txfm_partition_cdf = internal constant [21 x [3 x i16]] [[3 x i16] [i16 4187, i16 0, i16 0], [3 x i16] [i16 8922, i16 0, i16 0], [3 x i16] [i16 11921, i16 0, i16 0], [3 x i16] [i16 8453, i16 0, i16 0], [3 x i16] [i16 14572, i16 0, i16 0], [3 x i16] [i16 20635, i16 0, i16 0], [3 x i16] [i16 13977, i16 0, i16 0], [3 x i16] [i16 21881, i16 0, i16 0], [3 x i16] [i16 21763, i16 0, i16 0], [3 x i16] [i16 5589, i16 0, i16 0], [3 x i16] [i16 12764, i16 0, i16 0], [3 x i16] [i16 21487, i16 0, i16 0], [3 x i16] [i16 6219, i16 0, i16 0], [3 x i16] [i16 13460, i16 0, i16 0], [3 x i16] [i16 18544, i16 0, i16 0], [3 x i16] [i16 4753, i16 0, i16 0], [3 x i16] [i16 11222, i16 0, i16 0], [3 x i16] [i16 18368, i16 0, i16 0], [3 x i16] [i16 4603, i16 0, i16 0], [3 x i16] [i16 10367, i16 0, i16 0], [3 x i16] [i16 16680, i16 0, i16 0]], align 16
@default_compound_idx_cdfs = internal constant [6 x [3 x i16]] [[3 x i16] [i16 14524, i16 0, i16 0], [3 x i16] [i16 19903, i16 0, i16 0], [3 x i16] [i16 25715, i16 0, i16 0], [3 x i16] [i16 19509, i16 0, i16 0], [3 x i16] [i16 23434, i16 0, i16 0], [3 x i16] [i16 28124, i16 0, i16 0]], align 16
@default_comp_group_idx_cdfs = internal constant [6 x [3 x i16]] [[3 x i16] [i16 6161, i16 0, i16 0], [3 x i16] [i16 9877, i16 0, i16 0], [3 x i16] [i16 13928, i16 0, i16 0], [3 x i16] [i16 8174, i16 0, i16 0], [3 x i16] [i16 12834, i16 0, i16 0], [3 x i16] [i16 10094, i16 0, i16 0]], align 16
@default_newmv_cdf = internal constant [6 x [3 x i16]] [[3 x i16] [i16 8733, i16 0, i16 0], [3 x i16] [i16 16138, i16 0, i16 0], [3 x i16] [i16 17429, i16 0, i16 0], [3 x i16] [i16 24382, i16 0, i16 0], [3 x i16] [i16 20546, i16 0, i16 0], [3 x i16] [i16 28092, i16 0, i16 0]], align 16
@default_zeromv_cdf = internal constant [2 x [3 x i16]] [[3 x i16] [i16 30593, i16 0, i16 0], [3 x i16] [i16 31714, i16 0, i16 0]], align 2
@default_refmv_cdf = internal constant [6 x [3 x i16]] [[3 x i16] [i16 8794, i16 0, i16 0], [3 x i16] [i16 8580, i16 0, i16 0], [3 x i16] [i16 14920, i16 0, i16 0], [3 x i16] [i16 4146, i16 0, i16 0], [3 x i16] [i16 8456, i16 0, i16 0], [3 x i16] [i16 12845, i16 0, i16 0]], align 16
@default_drl_cdf = internal constant [3 x [3 x i16]] [[3 x i16] [i16 19664, i16 0, i16 0], [3 x i16] [i16 8208, i16 0, i16 0], [3 x i16] [i16 13823, i16 0, i16 0]], align 16
@default_motion_mode_cdf = internal constant [22 x [4 x i16]] [[4 x i16] [i16 21845, i16 10923, i16 0, i16 0], [4 x i16] [i16 21845, i16 10923, i16 0, i16 0], [4 x i16] [i16 21845, i16 10923, i16 0, i16 0], [4 x i16] [i16 25117, i16 8008, i16 0, i16 0], [4 x i16] [i16 28030, i16 8003, i16 0, i16 0], [4 x i16] [i16 27377, i16 7240, i16 0, i16 0], [4 x i16] [i16 13349, i16 5958, i16 0, i16 0], [4 x i16] [i16 27645, i16 9162, i16 0, i16 0], [4 x i16] [i16 21162, i16 8460, i16 0, i16 0], [4 x i16] [i16 6508, i16 3652, i16 0, i16 0], [4 x i16] [i16 12408, i16 4706, i16 0, i16 0], [4 x i16] [i16 11089, i16 5938, i16 0, i16 0], [4 x i16] [i16 3252, i16 2067, i16 0, i16 0], [4 x i16] [i16 3870, i16 2371, i16 0, i16 0], [4 x i16] [i16 1890, i16 1433, i16 0, i16 0], [4 x i16] [i16 261, i16 210, i16 0, i16 0], [4 x i16] [i16 21845, i16 10923, i16 0, i16 0], [4 x i16] [i16 21845, i16 10923, i16 0, i16 0], [4 x i16] [i16 3969, i16 1378, i16 0, i16 0], [4 x i16] [i16 6337, i16 1994, i16 0, i16 0], [4 x i16] [i16 3795, i16 1174, i16 0, i16 0], [4 x i16] [i16 3026, i16 1565, i16 0, i16 0]], align 16
@default_obmc_cdf = internal constant [22 x [3 x i16]] [[3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 22331, i16 0, i16 0], [3 x i16] [i16 23397, i16 0, i16 0], [3 x i16] [i16 23467, i16 0, i16 0], [3 x i16] [i16 15336, i16 0, i16 0], [3 x i16] [i16 18345, i16 0, i16 0], [3 x i16] [i16 17626, i16 0, i16 0], [3 x i16] [i16 6951, i16 0, i16 0], [3 x i16] [i16 9945, i16 0, i16 0], [3 x i16] [i16 10685, i16 0, i16 0], [3 x i16] [i16 2640, i16 0, i16 0], [3 x i16] [i16 1754, i16 0, i16 0], [3 x i16] [i16 1208, i16 0, i16 0], [3 x i16] [i16 130, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 9104, i16 0, i16 0], [3 x i16] [i16 11867, i16 0, i16 0], [3 x i16] [i16 8760, i16 0, i16 0], [3 x i16] [i16 5889, i16 0, i16 0]], align 16
@default_inter_compound_mode_cdf = internal constant [8 x [9 x i16]] [[9 x i16] [i16 25008, i16 18945, i16 16960, i16 15127, i16 13612, i16 12102, i16 5877, i16 0, i16 0], [9 x i16] [i16 22038, i16 13316, i16 11623, i16 10019, i16 8729, i16 7637, i16 4044, i16 0, i16 0], [9 x i16] [i16 22104, i16 12547, i16 11180, i16 9862, i16 8473, i16 7381, i16 4332, i16 0, i16 0], [9 x i16] [i16 19470, i16 15784, i16 12297, i16 8586, i16 7701, i16 7032, i16 6346, i16 0, i16 0], [9 x i16] [i16 13864, i16 9443, i16 7526, i16 5336, i16 4870, i16 4510, i16 2010, i16 0, i16 0], [9 x i16] [i16 22043, i16 15314, i16 12644, i16 9948, i16 8573, i16 7600, i16 6722, i16 0, i16 0], [9 x i16] [i16 15643, i16 8495, i16 6954, i16 5276, i16 4554, i16 4064, i16 2176, i16 0, i16 0], [9 x i16] [i16 19722, i16 9554, i16 8263, i16 6826, i16 5333, i16 4326, i16 3438, i16 0, i16 0]], align 16
@default_compound_type_cdf = internal constant [22 x [3 x i16]] [[3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 9337, i16 0, i16 0], [3 x i16] [i16 19597, i16 0, i16 0], [3 x i16] [i16 21298, i16 0, i16 0], [3 x i16] [i16 22998, i16 0, i16 0], [3 x i16] [i16 23668, i16 0, i16 0], [3 x i16] [i16 24535, i16 0, i16 0], [3 x i16] [i16 26596, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 20948, i16 0, i16 0], [3 x i16] [i16 25067, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0]], align 16
@default_wedge_idx_cdf = internal constant [22 x [17 x i16]] [[17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30330, i16 28328, i16 26169, i16 24105, i16 21763, i16 19894, i16 17017, i16 14674, i16 12409, i16 10406, i16 8641, i16 7066, i16 5016, i16 3318, i16 1597, i16 0, i16 0], [17 x i16] [i16 31962, i16 29502, i16 26763, i16 26030, i16 25550, i16 25401, i16 24997, i16 18180, i16 16445, i16 15401, i16 14316, i16 13346, i16 9929, i16 6641, i16 3139, i16 0, i16 0], [17 x i16] [i16 29989, i16 29030, i16 28085, i16 25555, i16 24993, i16 24751, i16 24113, i16 18411, i16 14829, i16 11436, i16 8248, i16 5298, i16 3312, i16 2239, i16 1112, i16 0, i16 0], [17 x i16] [i16 31084, i16 29143, i16 27093, i16 25660, i16 23466, i16 21494, i16 18339, i16 15624, i16 13605, i16 11807, i16 9884, i16 8297, i16 6049, i16 4054, i16 1891, i16 0, i16 0], [17 x i16] [i16 31626, i16 29277, i16 26491, i16 25454, i16 24679, i16 24413, i16 23745, i16 19144, i16 17399, i16 16038, i16 14654, i16 13455, i16 10247, i16 6756, i16 3218, i16 0, i16 0], [17 x i16] [i16 30026, i16 28573, i16 27041, i16 24733, i16 23788, i16 23432, i16 22622, i16 18644, i16 15498, i16 12235, i16 9334, i16 6796, i16 4824, i16 3198, i16 1352, i16 0, i16 0], [17 x i16] [i16 31041, i16 28820, i16 26667, i16 24972, i16 22927, i16 20424, i16 17002, i16 13824, i16 12130, i16 10730, i16 8805, i16 7457, i16 5780, i16 4002, i16 1756, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 32614, i16 31781, i16 30843, i16 30717, i16 30680, i16 30657, i16 30617, i16 9735, i16 9065, i16 8484, i16 7783, i16 7084, i16 5509, i16 3885, i16 1857, i16 0, i16 0], [17 x i16] [i16 31633, i16 31446, i16 31275, i16 30133, i16 30072, i16 30031, i16 29998, i16 11752, i16 9833, i16 7711, i16 5517, i16 3595, i16 2679, i16 1808, i16 835, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0]], align 16
@default_interintra_cdf = internal constant [4 x [3 x i16]] [[3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 5881, i16 0, i16 0], [3 x i16] [i16 5171, i16 0, i16 0], [3 x i16] [i16 2531, i16 0, i16 0]], align 16
@default_wedge_interintra_cdf = internal constant [22 x [3 x i16]] [[3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 12732, i16 0, i16 0], [3 x i16] [i16 7811, i16 0, i16 0], [3 x i16] [i16 6064, i16 0, i16 0], [3 x i16] [i16 5238, i16 0, i16 0], [3 x i16] [i16 3204, i16 0, i16 0], [3 x i16] [i16 3324, i16 0, i16 0], [3 x i16] [i16 5896, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0]], align 16
@default_interintra_mode_cdf = internal constant [4 x [5 x i16]] [[5 x i16] [i16 24576, i16 16384, i16 8192, i16 0, i16 0], [5 x i16] [i16 30893, i16 21686, i16 5436, i16 0, i16 0], [5 x i16] [i16 30295, i16 22772, i16 6380, i16 0, i16 0], [5 x i16] [i16 28530, i16 21231, i16 6842, i16 0, i16 0]], align 16
@default_segment_pred_cdf = internal constant [3 x [3 x i16]] [[3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0]], align 16
@default_seg_tree_cdf = internal constant [9 x i16] [i16 28672, i16 24576, i16 20480, i16 16384, i16 12288, i16 8192, i16 4096, i16 0, i16 0], align 16
@default_filter_intra_cdfs = internal constant [22 x [3 x i16]] [[3 x i16] [i16 28147, i16 0, i16 0], [3 x i16] [i16 26025, i16 0, i16 0], [3 x i16] [i16 26875, i16 0, i16 0], [3 x i16] [i16 24902, i16 0, i16 0], [3 x i16] [i16 20217, i16 0, i16 0], [3 x i16] [i16 23374, i16 0, i16 0], [3 x i16] [i16 20360, i16 0, i16 0], [3 x i16] [i16 18467, i16 0, i16 0], [3 x i16] [i16 20012, i16 0, i16 0], [3 x i16] [i16 10425, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 19998, i16 0, i16 0], [3 x i16] [i16 22400, i16 0, i16 0], [3 x i16] [i16 12539, i16 0, i16 0], [3 x i16] [i16 14667, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0]], align 16
@default_filter_intra_mode_cdf = internal constant [6 x i16] [i16 23819, i16 19992, i16 15557, i16 3210, i16 0, i16 0], align 2
@default_switchable_restore_cdf = internal constant [4 x i16] [i16 23355, i16 10187, i16 0, i16 0], align 2
@default_wiener_restore_cdf = internal constant [3 x i16] [i16 21198, i16 0, i16 0], align 2
@default_sgrproj_restore_cdf = internal constant [3 x i16] [i16 15913, i16 0, i16 0], align 2
@default_if_y_mode_cdf = internal constant [4 x [14 x i16]] [[14 x i16] [i16 9967, i16 9279, i16 8475, i16 8012, i16 7167, i16 6645, i16 6162, i16 5350, i16 4823, i16 3540, i16 3083, i16 2419, i16 0, i16 0], [14 x i16] [i16 14095, i16 12923, i16 10137, i16 9450, i16 8818, i16 8119, i16 7241, i16 5404, i16 4616, i16 3067, i16 2784, i16 1916, i16 0, i16 0], [14 x i16] [i16 12998, i16 11789, i16 9372, i16 8829, i16 8527, i16 8114, i16 7632, i16 5695, i16 4938, i16 3408, i16 3038, i16 2109, i16 0, i16 0], [14 x i16] [i16 12613, i16 11467, i16 9930, i16 9590, i16 9507, i16 9235, i16 9065, i16 7964, i16 7416, i16 6193, i16 5752, i16 4719, i16 0, i16 0]], align 16
@default_uv_mode_cdf = internal constant [2 x [13 x [15 x i16]]] [[13 x [15 x i16]] [[15 x i16] [i16 10137, i16 8616, i16 7390, i16 7107, i16 6782, i16 6248, i16 5713, i16 4845, i16 4524, i16 2709, i16 1827, i16 807, i16 0, i16 0, i16 0], [15 x i16] [i16 23255, i16 5887, i16 5795, i16 5722, i16 5650, i16 5104, i16 5029, i16 4944, i16 4409, i16 3263, i16 2968, i16 972, i16 0, i16 0, i16 0], [15 x i16] [i16 22923, i16 22853, i16 4105, i16 4064, i16 4011, i16 3988, i16 3570, i16 2946, i16 2914, i16 2004, i16 991, i16 739, i16 0, i16 0, i16 0], [15 x i16] [i16 19129, i16 18871, i16 18597, i16 7437, i16 7162, i16 7041, i16 6815, i16 5620, i16 4191, i16 2156, i16 1413, i16 275, i16 0, i16 0, i16 0], [15 x i16] [i16 23004, i16 22933, i16 22838, i16 22814, i16 7382, i16 5715, i16 4810, i16 4620, i16 4525, i16 1667, i16 1024, i16 405, i16 0, i16 0, i16 0], [15 x i16] [i16 20943, i16 19179, i16 19091, i16 19048, i16 17720, i16 3555, i16 3467, i16 3310, i16 3057, i16 1607, i16 1327, i16 218, i16 0, i16 0, i16 0], [15 x i16] [i16 18593, i16 18369, i16 16160, i16 15947, i16 15050, i16 14993, i16 4217, i16 2568, i16 2523, i16 931, i16 426, i16 101, i16 0, i16 0, i16 0], [15 x i16] [i16 19883, i16 19730, i16 17790, i16 17178, i16 17095, i16 17020, i16 16592, i16 3640, i16 3501, i16 2125, i16 807, i16 307, i16 0, i16 0, i16 0], [15 x i16] [i16 20742, i16 19107, i16 18894, i16 17463, i16 17278, i16 17042, i16 16773, i16 16495, i16 4325, i16 2380, i16 2001, i16 352, i16 0, i16 0, i16 0], [15 x i16] [i16 13716, i16 12928, i16 12189, i16 11852, i16 11618, i16 11301, i16 10883, i16 10049, i16 9594, i16 3907, i16 2389, i16 593, i16 0, i16 0, i16 0], [15 x i16] [i16 14141, i16 13119, i16 11794, i16 11549, i16 11276, i16 10952, i16 10569, i16 9649, i16 9241, i16 5715, i16 1371, i16 620, i16 0, i16 0, i16 0], [15 x i16] [i16 15742, i16 13764, i16 12771, i16 12429, i16 12182, i16 11665, i16 11419, i16 10861, i16 10286, i16 6872, i16 6227, i16 949, i16 0, i16 0, i16 0], [15 x i16] [i16 20644, i16 19009, i16 17809, i16 17776, i16 17761, i16 17717, i16 17690, i16 17602, i16 17513, i16 17015, i16 16729, i16 16162, i16 0, i16 0, i16 0]], [13 x [15 x i16]] [[15 x i16] [i16 22361, i16 21560, i16 19868, i16 19587, i16 18945, i16 18593, i16 17869, i16 17112, i16 16782, i16 12682, i16 11773, i16 10313, i16 8556, i16 0, i16 0], [15 x i16] [i16 28236, i16 12988, i16 12711, i16 12553, i16 12340, i16 11697, i16 11569, i16 11317, i16 10669, i16 8540, i16 8075, i16 5736, i16 3296, i16 0, i16 0], [15 x i16] [i16 27495, i16 27389, i16 12591, i16 12498, i16 12383, i16 12329, i16 11819, i16 11073, i16 10994, i16 9630, i16 8512, i16 8065, i16 6089, i16 0, i16 0], [15 x i16] [i16 26028, i16 25601, i16 25106, i16 18616, i16 18232, i16 17983, i16 17734, i16 16027, i16 14397, i16 11248, i16 10562, i16 9379, i16 8586, i16 0, i16 0], [15 x i16] [i16 27781, i16 27400, i16 26840, i16 26700, i16 13654, i16 12453, i16 10911, i16 10515, i16 10357, i16 7857, i16 7388, i16 6741, i16 6392, i16 0, i16 0], [15 x i16] [i16 27398, i16 25879, i16 25521, i16 25375, i16 23270, i16 11654, i16 11366, i16 11015, i16 10787, i16 7988, i16 7382, i16 6251, i16 5592, i16 0, i16 0], [15 x i16] [i16 27952, i16 27807, i16 25564, i16 25442, i16 24003, i16 23838, i16 12599, i16 12086, i16 11965, i16 9580, i16 9005, i16 8313, i16 7828, i16 0, i16 0], [15 x i16] [i16 26160, i16 26028, i16 24239, i16 23719, i16 23511, i16 23412, i16 23033, i16 13941, i16 13709, i16 10432, i16 9564, i16 8804, i16 7975, i16 0, i16 0], [15 x i16] [i16 26770, i16 25349, i16 24987, i16 23835, i16 23513, i16 23219, i16 23015, i16 22351, i16 13870, i16 10274, i16 9629, i16 8004, i16 6779, i16 0, i16 0], [15 x i16] [i16 22108, i16 21470, i16 20218, i16 19811, i16 19446, i16 19144, i16 18728, i16 17764, i16 17234, i16 12054, i16 10979, i16 9325, i16 7907, i16 0, i16 0], [15 x i16] [i16 22246, i16 21238, i16 20216, i16 19805, i16 19390, i16 18989, i16 18523, i16 17533, i16 16866, i16 12666, i16 10072, i16 8994, i16 6930, i16 0, i16 0], [15 x i16] [i16 22669, i16 22077, i16 20129, i16 19719, i16 19382, i16 19103, i16 18643, i16 17605, i16 17132, i16 13092, i16 12294, i16 9249, i16 7560, i16 0, i16 0], [15 x i16] [i16 29624, i16 27681, i16 25386, i16 25264, i16 25175, i16 25078, i16 24967, i16 24704, i16 24536, i16 23520, i16 22893, i16 22247, i16 3720, i16 0, i16 0]]], align 16
@default_switchable_interp_cdf = internal constant [16 x [4 x i16]] [[4 x i16] [i16 833, i16 48, i16 0, i16 0], [4 x i16] [i16 27200, i16 49, i16 0, i16 0], [4 x i16] [i16 32346, i16 29830, i16 0, i16 0], [4 x i16] [i16 4524, i16 160, i16 0, i16 0], [4 x i16] [i16 1562, i16 815, i16 0, i16 0], [4 x i16] [i16 27906, i16 647, i16 0, i16 0], [4 x i16] [i16 31998, i16 31616, i16 0, i16 0], [4 x i16] [i16 11879, i16 7131, i16 0, i16 0], [4 x i16] [i16 858, i16 44, i16 0, i16 0], [4 x i16] [i16 28648, i16 56, i16 0, i16 0], [4 x i16] [i16 32463, i16 30521, i16 0, i16 0], [4 x i16] [i16 5365, i16 132, i16 0, i16 0], [4 x i16] [i16 1746, i16 759, i16 0, i16 0], [4 x i16] [i16 29805, i16 675, i16 0, i16 0], [4 x i16] [i16 32167, i16 31825, i16 0, i16 0], [4 x i16] [i16 17799, i16 11370, i16 0, i16 0]], align 16
@default_skip_mode_cdfs = internal constant [3 x [3 x i16]] [[3 x i16] [i16 147, i16 0, i16 0], [3 x i16] [i16 12060, i16 0, i16 0], [3 x i16] [i16 24641, i16 0, i16 0]], align 16
@default_skip_cdfs = internal constant [3 x [3 x i16]] [[3 x i16] [i16 1097, i16 0, i16 0], [3 x i16] [i16 16253, i16 0, i16 0], [3 x i16] [i16 28192, i16 0, i16 0]], align 16
@default_intra_inter_cdf = internal constant [4 x [3 x i16]] [[3 x i16] [i16 31962, i16 0, i16 0], [3 x i16] [i16 16106, i16 0, i16 0], [3 x i16] [i16 12582, i16 0, i16 0], [3 x i16] [i16 6230, i16 0, i16 0]], align 16
@default_spatial_pred_seg_tree_cdf = internal constant [3 x [9 x i16]] [[9 x i16] [i16 27146, i16 24875, i16 16675, i16 14535, i16 4959, i16 4395, i16 235, i16 0, i16 0], [9 x i16] [i16 18494, i16 14538, i16 10211, i16 7833, i16 2788, i16 1917, i16 424, i16 0, i16 0], [9 x i16] [i16 5241, i16 4281, i16 4045, i16 3878, i16 371, i16 121, i16 89, i16 0, i16 0]], align 16
@default_tx_size_cdf = internal constant [4 x [3 x [4 x i16]]] [[3 x [4 x i16]] [[4 x i16] [i16 12800, i16 0, i16 0, i16 0], [4 x i16] [i16 12800, i16 0, i16 0, i16 0], [4 x i16] [i16 8448, i16 0, i16 0, i16 0]], [3 x [4 x i16]] [[4 x i16] [i16 20496, i16 2596, i16 0, i16 0], [4 x i16] [i16 20496, i16 2596, i16 0, i16 0], [4 x i16] [i16 14091, i16 1920, i16 0, i16 0]], [3 x [4 x i16]] [[4 x i16] [i16 19782, i16 17588, i16 0, i16 0], [4 x i16] [i16 19782, i16 17588, i16 0, i16 0], [4 x i16] [i16 8466, i16 7166, i16 0, i16 0]], [3 x [4 x i16]] [[4 x i16] [i16 26986, i16 21293, i16 0, i16 0], [4 x i16] [i16 26986, i16 21293, i16 0, i16 0], [4 x i16] [i16 15965, i16 10009, i16 0, i16 0]]], align 16
@default_delta_q_cdf = internal constant [5 x i16] [i16 4608, i16 648, i16 91, i16 0, i16 0], align 2
@default_delta_lf_cdf = internal constant [5 x i16] [i16 4608, i16 648, i16 91, i16 0, i16 0], align 2
@default_delta_lf_multi_cdf = internal constant [4 x [5 x i16]] [[5 x i16] [i16 4608, i16 648, i16 91, i16 0, i16 0], [5 x i16] [i16 4608, i16 648, i16 91, i16 0, i16 0], [5 x i16] [i16 4608, i16 648, i16 91, i16 0, i16 0], [5 x i16] [i16 4608, i16 648, i16 91, i16 0, i16 0]], align 16
@default_cfl_sign_cdf = internal constant [9 x i16] [i16 31350, i16 30645, i16 19428, i16 14363, i16 5796, i16 4425, i16 474, i16 0, i16 0], align 16
@default_cfl_alpha_cdf = internal constant [6 x [17 x i16]] [[17 x i16] [i16 25131, i16 12049, i16 1367, i16 287, i16 111, i16 80, i16 76, i16 72, i16 68, i16 64, i16 60, i16 56, i16 52, i16 48, i16 44, i16 0, i16 0], [17 x i16] [i16 18403, i16 9165, i16 4633, i16 1600, i16 601, i16 373, i16 281, i16 195, i16 148, i16 121, i16 100, i16 96, i16 92, i16 88, i16 84, i16 0, i16 0], [17 x i16] [i16 21236, i16 10388, i16 4323, i16 1408, i16 419, i16 245, i16 184, i16 119, i16 95, i16 91, i16 87, i16 83, i16 79, i16 75, i16 71, i16 0, i16 0], [17 x i16] [i16 5778, i16 1366, i16 486, i16 197, i16 76, i16 72, i16 68, i16 64, i16 60, i16 56, i16 52, i16 48, i16 44, i16 40, i16 36, i16 0, i16 0], [17 x i16] [i16 15520, i16 6710, i16 3864, i16 2160, i16 1463, i16 891, i16 642, i16 447, i16 374, i16 304, i16 252, i16 208, i16 192, i16 175, i16 146, i16 0, i16 0], [17 x i16] [i16 18030, i16 11090, i16 6989, i16 4867, i16 3744, i16 2466, i16 1788, i16 925, i16 624, i16 355, i16 248, i16 174, i16 146, i16 112, i16 108, i16 0, i16 0]], align 16
@default_intrabc_cdf = internal constant [3 x i16] [i16 2237, i16 0, i16 0], align 2
@default_palette_y_color_index_cdf = internal constant <{ [5 x <{ i16, [8 x i16] }>], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]] }> <{ [5 x <{ i16, [8 x i16] }>] [<{ i16, [8 x i16] }> <{ i16 4058, [8 x i16] zeroinitializer }>, <{ i16, [8 x i16] }> <{ i16 16384, [8 x i16] zeroinitializer }>, <{ i16, [8 x i16] }> <{ i16 22215, [8 x i16] zeroinitializer }>, <{ i16, [8 x i16] }> <{ i16 5732, [8 x i16] zeroinitializer }>, <{ i16, [8 x i16] }> <{ i16 1165, [8 x i16] zeroinitializer }>], [5 x [9 x i16]] [[9 x i16] [i16 4891, i16 2278, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 21236, i16 7071, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 26224, i16 2534, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 9750, i16 4696, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 853, i16 383, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 7196, i16 4722, i16 2723, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 23290, i16 11178, i16 5512, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 25520, i16 5931, i16 2944, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 13601, i16 8282, i16 4419, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 1368, i16 943, i16 518, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 7989, i16 5813, i16 4192, i16 2486, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 24099, i16 12404, i16 8695, i16 4675, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 28513, i16 5203, i16 3391, i16 1701, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 12904, i16 9094, i16 6052, i16 3238, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 1122, i16 875, i16 621, i16 342, i16 0, i16 0, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 9636, i16 7361, i16 5798, i16 4333, i16 2695, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 25325, i16 15526, i16 12051, i16 8006, i16 4786, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 26468, i16 7906, i16 5824, i16 3984, i16 2097, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 13852, i16 9873, i16 7501, i16 5333, i16 3116, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 1498, i16 1218, i16 960, i16 709, i16 415, i16 0, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 9663, i16 7569, i16 6304, i16 5084, i16 3837, i16 2450, i16 0, i16 0, i16 0], [9 x i16] [i16 25818, i16 17321, i16 13816, i16 10087, i16 7201, i16 4205, i16 0, i16 0, i16 0], [9 x i16] [i16 25208, i16 9294, i16 7278, i16 5565, i16 3847, i16 2060, i16 0, i16 0, i16 0], [9 x i16] [i16 14224, i16 10395, i16 8311, i16 6573, i16 4649, i16 2723, i16 0, i16 0, i16 0], [9 x i16] [i16 1570, i16 1317, i16 1098, i16 886, i16 645, i16 377, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 11079, i16 8885, i16 7605, i16 6416, i16 5262, i16 3941, i16 2573, i16 0, i16 0], [9 x i16] [i16 25876, i16 17383, i16 14928, i16 11162, i16 8481, i16 6015, i16 3564, i16 0, i16 0], [9 x i16] [i16 27117, i16 9586, i16 7726, i16 6250, i16 4786, i16 3376, i16 1868, i16 0, i16 0], [9 x i16] [i16 13419, i16 10190, i16 8350, i16 6774, i16 5244, i16 3737, i16 2320, i16 0, i16 0], [9 x i16] [i16 1740, i16 1498, i16 1264, i16 1063, i16 841, i16 615, i16 376, i16 0, i16 0]] }>, align 16
@default_palette_uv_color_index_cdf = internal constant <{ [5 x <{ i16, [8 x i16] }>], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]] }> <{ [5 x <{ i16, [8 x i16] }>] [<{ i16, [8 x i16] }> <{ i16 3679, [8 x i16] zeroinitializer }>, <{ i16, [8 x i16] }> <{ i16 16384, [8 x i16] zeroinitializer }>, <{ i16, [8 x i16] }> <{ i16 24055, [8 x i16] zeroinitializer }>, <{ i16, [8 x i16] }> <{ i16 3511, [8 x i16] zeroinitializer }>, <{ i16, [8 x i16] }> <{ i16 1158, [8 x i16] zeroinitializer }>], [5 x [9 x i16]] [[9 x i16] [i16 7511, i16 3623, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 20481, i16 5475, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 25735, i16 4808, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 12623, i16 7363, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 2160, i16 1129, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 8558, i16 5593, i16 2865, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 22880, i16 10382, i16 5554, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 26867, i16 6715, i16 3475, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 14450, i16 10616, i16 4435, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 2309, i16 1632, i16 842, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 9788, i16 7289, i16 4987, i16 2782, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 24355, i16 11360, i16 7909, i16 3894, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 30511, i16 3319, i16 2174, i16 1170, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 13579, i16 11566, i16 6853, i16 4148, i16 0, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 924, i16 724, i16 487, i16 250, i16 0, i16 0, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 10551, i16 8201, i16 6131, i16 4085, i16 2220, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 25461, i16 16362, i16 13132, i16 8136, i16 4344, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 28327, i16 7704, i16 5889, i16 3826, i16 1849, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 15558, i16 12240, i16 9449, i16 6018, i16 3186, i16 0, i16 0, i16 0, i16 0], [9 x i16] [i16 2094, i16 1815, i16 1372, i16 1033, i16 561, i16 0, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 11529, i16 9600, i16 7724, i16 5806, i16 4063, i16 2262, i16 0, i16 0, i16 0], [9 x i16] [i16 26223, i16 17756, i16 14764, i16 10951, i16 7265, i16 4067, i16 0, i16 0, i16 0], [9 x i16] [i16 29320, i16 6473, i16 5331, i16 4064, i16 2642, i16 1326, i16 0, i16 0, i16 0], [9 x i16] [i16 16879, i16 14445, i16 11064, i16 8070, i16 5792, i16 3078, i16 0, i16 0, i16 0], [9 x i16] [i16 1780, i16 1564, i16 1289, i16 1034, i16 785, i16 443, i16 0, i16 0, i16 0]], [5 x [9 x i16]] [[9 x i16] [i16 11326, i16 9480, i16 8010, i16 6522, i16 5119, i16 3788, i16 2205, i16 0, i16 0], [9 x i16] [i16 26905, i16 17835, i16 15216, i16 12100, i16 9085, i16 6357, i16 3495, i16 0, i16 0], [9 x i16] [i16 29353, i16 6958, i16 5891, i16 4778, i16 3545, i16 2374, i16 1150, i16 0, i16 0], [9 x i16] [i16 14803, i16 12684, i16 10536, i16 8794, i16 6494, i16 4366, i16 2378, i16 0, i16 0], [9 x i16] [i16 1578, i16 1439, i16 1252, i16 1089, i16 943, i16 742, i16 446, i16 0, i16 0]] }>, align 16
@default_partition_cdf = internal constant <{ <{ i16, i16, i16, [8 x i16] }>, <{ i16, i16, i16, [8 x i16] }>, <{ i16, i16, i16, [8 x i16] }>, <{ i16, i16, i16, [8 x i16] }>, [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16] }> <{ <{ i16, i16, i16, [8 x i16] }> <{ i16 13636, i16 7258, i16 2376, [8 x i16] zeroinitializer }>, <{ i16, i16, i16, [8 x i16] }> <{ i16 18840, i16 12913, i16 4228, [8 x i16] zeroinitializer }>, <{ i16, i16, i16, [8 x i16] }> <{ i16 20246, i16 9089, i16 4139, [8 x i16] zeroinitializer }>, <{ i16, i16, i16, [8 x i16] }> <{ i16 22872, i16 13985, i16 6915, [8 x i16] zeroinitializer }>, [11 x i16] [i16 17171, i16 11839, i16 8197, i16 6062, i16 5104, i16 3947, i16 3167, i16 2197, i16 866, i16 0, i16 0], [11 x i16] [i16 24843, i16 21725, i16 15983, i16 10298, i16 8797, i16 7725, i16 6117, i16 4067, i16 2934, i16 0, i16 0], [11 x i16] [i16 27354, i16 19499, i16 17657, i16 12280, i16 10408, i16 8268, i16 7231, i16 6432, i16 651, i16 0, i16 0], [11 x i16] [i16 30106, i16 26406, i16 24154, i16 11908, i16 9715, i16 7990, i16 6332, i16 4939, i16 1597, i16 0, i16 0], [11 x i16] [i16 14306, i16 11848, i16 9644, i16 5121, i16 4541, i16 3719, i16 3249, i16 2590, i16 1224, i16 0, i16 0], [11 x i16] [i16 25079, i16 23708, i16 20712, i16 7776, i16 7108, i16 6586, i16 5817, i16 4727, i16 3716, i16 0, i16 0], [11 x i16] [i16 26753, i16 23759, i16 22706, i16 8224, i16 7359, i16 6223, i16 5697, i16 5242, i16 721, i16 0, i16 0], [11 x i16] [i16 31374, i16 30560, i16 29972, i16 4154, i16 3707, i16 3302, i16 2928, i16 2583, i16 869, i16 0, i16 0], [11 x i16] [i16 12631, i16 11221, i16 9690, i16 3202, i16 2931, i16 2507, i16 2244, i16 1876, i16 1044, i16 0, i16 0], [11 x i16] [i16 26036, i16 25278, i16 23271, i16 4824, i16 4518, i16 4253, i16 3799, i16 3138, i16 2664, i16 0, i16 0], [11 x i16] [i16 26823, i16 25105, i16 24420, i16 4085, i16 3651, i16 3019, i16 2704, i16 2470, i16 530, i16 0, i16 0], [11 x i16] [i16 31898, i16 31556, i16 31281, i16 1570, i16 1374, i16 1194, i16 1025, i16 887, i16 436, i16 0, i16 0], [11 x i16] [i16 4869, i16 4549, i16 4239, i16 284, i16 229, i16 149, i16 129, i16 0, i16 0, i16 0, i16 0], [11 x i16] [i16 26161, i16 25778, i16 24500, i16 708, i16 549, i16 430, i16 397, i16 0, i16 0, i16 0, i16 0], [11 x i16] [i16 27339, i16 26092, i16 25646, i16 741, i16 541, i16 237, i16 186, i16 0, i16 0, i16 0, i16 0], [11 x i16] [i16 32057, i16 31802, i16 31596, i16 320, i16 230, i16 151, i16 104, i16 0, i16 0, i16 0, i16 0] }>, align 16
@default_intra_ext_tx_cdf = internal constant <{ [4 x [13 x [17 x i16]]], [4 x [13 x <{ i16, i16, i16, i16, i16, i16, [11 x i16] }>]], [4 x [13 x <{ i16, i16, i16, i16, [13 x i16] }>]] }> <{ [4 x [13 x [17 x i16]]] zeroinitializer, [4 x [13 x <{ i16, i16, i16, i16, i16, i16, [11 x i16] }>]] [[13 x <{ i16, i16, i16, i16, i16, i16, [11 x i16] }>] [<{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 31233, i16 24733, i16 23307, i16 20017, i16 9301, i16 4943, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32204, i16 29433, i16 23059, i16 21898, i16 14625, i16 4674, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32096, i16 29521, i16 29092, i16 20786, i16 13353, i16 9641, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 27489, i16 18883, i16 17281, i16 14724, i16 9241, i16 2516, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28345, i16 26694, i16 24783, i16 22352, i16 7075, i16 3470, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 31282, i16 28527, i16 23308, i16 22106, i16 16312, i16 5074, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32329, i16 29930, i16 29246, i16 26031, i16 14710, i16 9014, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 31578, i16 28535, i16 27913, i16 21098, i16 12487, i16 8391, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 31723, i16 28456, i16 24121, i16 22609, i16 14124, i16 3433, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32566, i16 29034, i16 28021, i16 25470, i16 15641, i16 8752, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32321, i16 28456, i16 25949, i16 23884, i16 16758, i16 8910, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32491, i16 28399, i16 27513, i16 23863, i16 16303, i16 10497, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 29359, i16 27332, i16 22169, i16 17169, i16 13081, i16 8728, [11 x i16] zeroinitializer }>], [13 x <{ i16, i16, i16, i16, i16, i16, [11 x i16] }>] [<{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 30898, i16 19026, i16 18238, i16 16270, i16 8998, i16 5070, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32442, i16 23972, i16 18136, i16 17689, i16 13496, i16 5282, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32284, i16 25192, i16 25056, i16 18325, i16 13609, i16 10177, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 31642, i16 17428, i16 16873, i16 15745, i16 11872, i16 2489, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32113, i16 27914, i16 27519, i16 26855, i16 10669, i16 5630, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 31469, i16 26310, i16 23883, i16 23478, i16 17917, i16 7271, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32457, i16 27473, i16 27216, i16 25883, i16 16661, i16 10096, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 31885, i16 24709, i16 24498, i16 21510, i16 15479, i16 11219, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32027, i16 25188, i16 23450, i16 22423, i16 16080, i16 3722, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32658, i16 25362, i16 24853, i16 23573, i16 16727, i16 9439, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32405, i16 24794, i16 23411, i16 22095, i16 17139, i16 8294, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 32615, i16 25121, i16 24656, i16 22832, i16 17461, i16 12772, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 29257, i16 26436, i16 21603, i16 17433, i16 13445, i16 9174, [11 x i16] zeroinitializer }>], [13 x <{ i16, i16, i16, i16, i16, i16, [11 x i16] }>] [<{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>], [13 x <{ i16, i16, i16, i16, i16, i16, [11 x i16] }>] [<{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, i16, i16, [11 x i16] }> <{ i16 28087, i16 23406, i16 18725, i16 14043, i16 9362, i16 4681, [11 x i16] zeroinitializer }>]], [4 x [13 x <{ i16, i16, i16, i16, [13 x i16] }>]] [[13 x <{ i16, i16, i16, i16, [13 x i16] }>] [<{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>], [13 x <{ i16, i16, i16, i16, [13 x i16] }>] [<{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>], [13 x <{ i16, i16, i16, i16, [13 x i16] }>] [<{ i16, i16, i16, i16, [13 x i16] }> <{ i16 31641, i16 19954, i16 9996, i16 5285, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32623, i16 26007, i16 20788, i16 6101, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32406, i16 26881, i16 21090, i16 16043, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32383, i16 17555, i16 14181, i16 2075, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32743, i16 29854, i16 9634, i16 4865, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32708, i16 28298, i16 21019, i16 8777, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32731, i16 29436, i16 18257, i16 11320, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32611, i16 26448, i16 19732, i16 15329, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32649, i16 26049, i16 19862, i16 3372, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32721, i16 27231, i16 20192, i16 11269, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32499, i16 26692, i16 21510, i16 9653, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 32685, i16 27153, i16 20767, i16 15540, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 30800, i16 27212, i16 20745, i16 14221, [13 x i16] zeroinitializer }>], [13 x <{ i16, i16, i16, i16, [13 x i16] }>] [<{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>, <{ i16, i16, i16, i16, [13 x i16] }> <{ i16 26214, i16 19661, i16 13107, i16 6554, [13 x i16] zeroinitializer }>]] }>, align 16
@default_inter_ext_tx_cdf = internal constant <{ [4 x [17 x i16]], [4 x [17 x i16]], [4 x [17 x i16]], [4 x <{ i16, [16 x i16] }>] }> <{ [4 x [17 x i16]] zeroinitializer, [4 x [17 x i16]] [[17 x i16] [i16 28310, i16 27208, i16 25073, i16 23059, i16 19438, i16 17979, i16 15231, i16 12502, i16 11264, i16 9920, i16 8834, i16 7294, i16 5041, i16 3853, i16 2137, i16 0, i16 0], [17 x i16] [i16 31123, i16 30195, i16 27990, i16 27057, i16 24961, i16 24146, i16 22246, i16 17411, i16 15094, i16 12360, i16 10251, i16 7758, i16 5652, i16 3912, i16 2019, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0], [17 x i16] [i16 30720, i16 28672, i16 26624, i16 24576, i16 22528, i16 20480, i16 18432, i16 16384, i16 14336, i16 12288, i16 10240, i16 8192, i16 6144, i16 4096, i16 2048, i16 0, i16 0]], [4 x [17 x i16]] [[17 x i16] [i16 30037, i16 27307, i16 24576, i16 21845, i16 19115, i16 16384, i16 13653, i16 10923, i16 8192, i16 5461, i16 2731, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [17 x i16] [i16 30037, i16 27307, i16 24576, i16 21845, i16 19115, i16 16384, i16 13653, i16 10923, i16 8192, i16 5461, i16 2731, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [17 x i16] [i16 31998, i16 30347, i16 27543, i16 19861, i16 16949, i16 13841, i16 11207, i16 8679, i16 6173, i16 4242, i16 2239, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0], [17 x i16] [i16 30037, i16 27307, i16 24576, i16 21845, i16 19115, i16 16384, i16 13653, i16 10923, i16 8192, i16 5461, i16 2731, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0]], [4 x <{ i16, [16 x i16] }>] [<{ i16, [16 x i16] }> <{ i16 16384, [16 x i16] zeroinitializer }>, <{ i16, [16 x i16] }> <{ i16 28601, [16 x i16] zeroinitializer }>, <{ i16, [16 x i16] }> <{ i16 30770, [16 x i16] zeroinitializer }>, <{ i16, [16 x i16] }> <{ i16 32020, [16 x i16] zeroinitializer }>] }>, align 16

; Function Attrs: nounwind
define hidden i32 @av1_get_palette_color_index_context(i8* %color_map, i32 %stride, i32 %r, i32 %c, i32 %palette_size, i8* %color_order, i32* %color_idx) #0 {
entry:
  %color_map.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %r.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  %palette_size.addr = alloca i32, align 4
  %color_order.addr = alloca i8*, align 4
  %color_idx.addr = alloca i32*, align 4
  %color_neighbors = alloca [3 x i32], align 4
  %scores = alloca [18 x i32], align 16
  %i = alloca i32, align 4
  %inverse_color_order = alloca [8 x i32], align 16
  %max = alloca i32, align 4
  %max_idx = alloca i32, align 4
  %j = alloca i32, align 4
  %max_score = alloca i32, align 4
  %max_color_order = alloca i8, align 1
  %k = alloca i32, align 4
  %color_index_ctx_hash = alloca i32, align 4
  %color_index_ctx = alloca i32, align 4
  store i8* %color_map, i8** %color_map.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %r, i32* %r.addr, align 4, !tbaa !6
  store i32 %c, i32* %c.addr, align 4, !tbaa !6
  store i32 %palette_size, i32* %palette_size.addr, align 4, !tbaa !6
  store i8* %color_order, i8** %color_order.addr, align 4, !tbaa !2
  store i32* %color_idx, i32** %color_idx.addr, align 4, !tbaa !2
  %0 = bitcast [3 x i32]* %color_neighbors to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #5
  %1 = load i32, i32* %c.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %1, 1
  %cmp = icmp sge i32 %sub, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i8*, i8** %color_map.addr, align 4, !tbaa !2
  %3 = load i32, i32* %r.addr, align 4, !tbaa !6
  %4 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %3, %4
  %5 = load i32, i32* %c.addr, align 4, !tbaa !6
  %add = add nsw i32 %mul, %5
  %sub1 = sub nsw i32 %add, 1
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %sub1
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %6 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ -1, %cond.false ]
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %color_neighbors, i32 0, i32 0
  store i32 %cond, i32* %arrayidx2, align 4, !tbaa !6
  %7 = load i32, i32* %c.addr, align 4, !tbaa !6
  %sub3 = sub nsw i32 %7, 1
  %cmp4 = icmp sge i32 %sub3, 0
  br i1 %cmp4, label %land.lhs.true, label %cond.false16

land.lhs.true:                                    ; preds = %cond.end
  %8 = load i32, i32* %r.addr, align 4, !tbaa !6
  %sub6 = sub nsw i32 %8, 1
  %cmp7 = icmp sge i32 %sub6, 0
  br i1 %cmp7, label %cond.true9, label %cond.false16

cond.true9:                                       ; preds = %land.lhs.true
  %9 = load i8*, i8** %color_map.addr, align 4, !tbaa !2
  %10 = load i32, i32* %r.addr, align 4, !tbaa !6
  %sub10 = sub nsw i32 %10, 1
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul11 = mul nsw i32 %sub10, %11
  %12 = load i32, i32* %c.addr, align 4, !tbaa !6
  %add12 = add nsw i32 %mul11, %12
  %sub13 = sub nsw i32 %add12, 1
  %arrayidx14 = getelementptr inbounds i8, i8* %9, i32 %sub13
  %13 = load i8, i8* %arrayidx14, align 1, !tbaa !8
  %conv15 = zext i8 %13 to i32
  br label %cond.end17

cond.false16:                                     ; preds = %land.lhs.true, %cond.end
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false16, %cond.true9
  %cond18 = phi i32 [ %conv15, %cond.true9 ], [ -1, %cond.false16 ]
  %arrayidx19 = getelementptr inbounds [3 x i32], [3 x i32]* %color_neighbors, i32 0, i32 1
  store i32 %cond18, i32* %arrayidx19, align 4, !tbaa !6
  %14 = load i32, i32* %r.addr, align 4, !tbaa !6
  %sub20 = sub nsw i32 %14, 1
  %cmp21 = icmp sge i32 %sub20, 0
  br i1 %cmp21, label %cond.true23, label %cond.false29

cond.true23:                                      ; preds = %cond.end17
  %15 = load i8*, i8** %color_map.addr, align 4, !tbaa !2
  %16 = load i32, i32* %r.addr, align 4, !tbaa !6
  %sub24 = sub nsw i32 %16, 1
  %17 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul25 = mul nsw i32 %sub24, %17
  %18 = load i32, i32* %c.addr, align 4, !tbaa !6
  %add26 = add nsw i32 %mul25, %18
  %arrayidx27 = getelementptr inbounds i8, i8* %15, i32 %add26
  %19 = load i8, i8* %arrayidx27, align 1, !tbaa !8
  %conv28 = zext i8 %19 to i32
  br label %cond.end30

cond.false29:                                     ; preds = %cond.end17
  br label %cond.end30

cond.end30:                                       ; preds = %cond.false29, %cond.true23
  %cond31 = phi i32 [ %conv28, %cond.true23 ], [ -1, %cond.false29 ]
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* %color_neighbors, i32 0, i32 2
  store i32 %cond31, i32* %arrayidx32, align 4, !tbaa !6
  %20 = bitcast [18 x i32]* %scores to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %20) #5
  %21 = bitcast [18 x i32]* %scores to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %21, i8 0, i32 72, i1 false)
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end30
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %cmp33 = icmp slt i32 %23, 3
  br i1 %cmp33, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds [3 x i32], [3 x i32]* %color_neighbors, i32 0, i32 %24
  %25 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %cmp36 = icmp sge i32 %25, 0
  br i1 %cmp36, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds [3 x i32], [3 x i32]* @av1_get_palette_color_index_context.weights, i32 0, i32 %26
  %27 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds [3 x i32], [3 x i32]* %color_neighbors, i32 0, i32 %28
  %29 = load i32, i32* %arrayidx39, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds [18 x i32], [18 x i32]* %scores, i32 0, i32 %29
  %30 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %add41 = add nsw i32 %30, %27
  store i32 %add41, i32* %arrayidx40, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %32 = bitcast [8 x i32]* %inverse_color_order to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %32) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc49, %for.end
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %cmp43 = icmp slt i32 %33, 8
  br i1 %cmp43, label %for.body45, label %for.end51

for.body45:                                       ; preds = %for.cond42
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %conv46 = trunc i32 %34 to i8
  %35 = load i8*, i8** %color_order.addr, align 4, !tbaa !2
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx47 = getelementptr inbounds i8, i8* %35, i32 %36
  store i8 %conv46, i8* %arrayidx47, align 1, !tbaa !8
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds [8 x i32], [8 x i32]* %inverse_color_order, i32 0, i32 %38
  store i32 %37, i32* %arrayidx48, align 4, !tbaa !6
  br label %for.inc49

for.inc49:                                        ; preds = %for.body45
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %inc50 = add nsw i32 %39, 1
  store i32 %inc50, i32* %i, align 4, !tbaa !6
  br label %for.cond42

for.end51:                                        ; preds = %for.cond42
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc97, %for.end51
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %cmp53 = icmp slt i32 %40, 3
  br i1 %cmp53, label %for.body55, label %for.end99

for.body55:                                       ; preds = %for.cond52
  %41 = bitcast i32* %max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #5
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds [18 x i32], [18 x i32]* %scores, i32 0, i32 %42
  %43 = load i32, i32* %arrayidx56, align 4, !tbaa !6
  store i32 %43, i32* %max, align 4, !tbaa !6
  %44 = bitcast i32* %max_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  %45 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %45, i32* %max_idx, align 4, !tbaa !6
  %46 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #5
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %add57 = add nsw i32 %47, 1
  store i32 %add57, i32* %j, align 4, !tbaa !6
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc68, %for.body55
  %48 = load i32, i32* %j, align 4, !tbaa !6
  %49 = load i32, i32* %palette_size.addr, align 4, !tbaa !6
  %cmp59 = icmp slt i32 %48, %49
  br i1 %cmp59, label %for.body61, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond58
  %50 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  br label %for.end70

for.body61:                                       ; preds = %for.cond58
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds [18 x i32], [18 x i32]* %scores, i32 0, i32 %51
  %52 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  %53 = load i32, i32* %max, align 4, !tbaa !6
  %cmp63 = icmp sgt i32 %52, %53
  br i1 %cmp63, label %if.then65, label %if.end67

if.then65:                                        ; preds = %for.body61
  %54 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx66 = getelementptr inbounds [18 x i32], [18 x i32]* %scores, i32 0, i32 %54
  %55 = load i32, i32* %arrayidx66, align 4, !tbaa !6
  store i32 %55, i32* %max, align 4, !tbaa !6
  %56 = load i32, i32* %j, align 4, !tbaa !6
  store i32 %56, i32* %max_idx, align 4, !tbaa !6
  br label %if.end67

if.end67:                                         ; preds = %if.then65, %for.body61
  br label %for.inc68

for.inc68:                                        ; preds = %if.end67
  %57 = load i32, i32* %j, align 4, !tbaa !6
  %inc69 = add nsw i32 %57, 1
  store i32 %inc69, i32* %j, align 4, !tbaa !6
  br label %for.cond58

for.end70:                                        ; preds = %for.cond.cleanup
  %58 = load i32, i32* %max_idx, align 4, !tbaa !6
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %cmp71 = icmp ne i32 %58, %59
  br i1 %cmp71, label %if.then73, label %if.end96

if.then73:                                        ; preds = %for.end70
  %60 = bitcast i32* %max_score to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #5
  %61 = load i32, i32* %max_idx, align 4, !tbaa !6
  %arrayidx74 = getelementptr inbounds [18 x i32], [18 x i32]* %scores, i32 0, i32 %61
  %62 = load i32, i32* %arrayidx74, align 4, !tbaa !6
  store i32 %62, i32* %max_score, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %max_color_order) #5
  %63 = load i8*, i8** %color_order.addr, align 4, !tbaa !2
  %64 = load i32, i32* %max_idx, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds i8, i8* %63, i32 %64
  %65 = load i8, i8* %arrayidx75, align 1, !tbaa !8
  store i8 %65, i8* %max_color_order, align 1, !tbaa !8
  %66 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #5
  %67 = load i32, i32* %max_idx, align 4, !tbaa !6
  store i32 %67, i32* %k, align 4, !tbaa !6
  br label %for.cond76

for.cond76:                                       ; preds = %for.inc89, %if.then73
  %68 = load i32, i32* %k, align 4, !tbaa !6
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %cmp77 = icmp sgt i32 %68, %69
  br i1 %cmp77, label %for.body80, label %for.cond.cleanup79

for.cond.cleanup79:                               ; preds = %for.cond76
  %70 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #5
  br label %for.end90

for.body80:                                       ; preds = %for.cond76
  %71 = load i32, i32* %k, align 4, !tbaa !6
  %sub81 = sub nsw i32 %71, 1
  %arrayidx82 = getelementptr inbounds [18 x i32], [18 x i32]* %scores, i32 0, i32 %sub81
  %72 = load i32, i32* %arrayidx82, align 4, !tbaa !6
  %73 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx83 = getelementptr inbounds [18 x i32], [18 x i32]* %scores, i32 0, i32 %73
  store i32 %72, i32* %arrayidx83, align 4, !tbaa !6
  %74 = load i8*, i8** %color_order.addr, align 4, !tbaa !2
  %75 = load i32, i32* %k, align 4, !tbaa !6
  %sub84 = sub nsw i32 %75, 1
  %arrayidx85 = getelementptr inbounds i8, i8* %74, i32 %sub84
  %76 = load i8, i8* %arrayidx85, align 1, !tbaa !8
  %77 = load i8*, i8** %color_order.addr, align 4, !tbaa !2
  %78 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds i8, i8* %77, i32 %78
  store i8 %76, i8* %arrayidx86, align 1, !tbaa !8
  %79 = load i32, i32* %k, align 4, !tbaa !6
  %80 = load i8*, i8** %color_order.addr, align 4, !tbaa !2
  %81 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds i8, i8* %80, i32 %81
  %82 = load i8, i8* %arrayidx87, align 1, !tbaa !8
  %idxprom = zext i8 %82 to i32
  %arrayidx88 = getelementptr inbounds [8 x i32], [8 x i32]* %inverse_color_order, i32 0, i32 %idxprom
  store i32 %79, i32* %arrayidx88, align 4, !tbaa !6
  br label %for.inc89

for.inc89:                                        ; preds = %for.body80
  %83 = load i32, i32* %k, align 4, !tbaa !6
  %dec = add nsw i32 %83, -1
  store i32 %dec, i32* %k, align 4, !tbaa !6
  br label %for.cond76

for.end90:                                        ; preds = %for.cond.cleanup79
  %84 = load i32, i32* %max_score, align 4, !tbaa !6
  %85 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx91 = getelementptr inbounds [18 x i32], [18 x i32]* %scores, i32 0, i32 %85
  store i32 %84, i32* %arrayidx91, align 4, !tbaa !6
  %86 = load i8, i8* %max_color_order, align 1, !tbaa !8
  %87 = load i8*, i8** %color_order.addr, align 4, !tbaa !2
  %88 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx92 = getelementptr inbounds i8, i8* %87, i32 %88
  store i8 %86, i8* %arrayidx92, align 1, !tbaa !8
  %89 = load i32, i32* %i, align 4, !tbaa !6
  %90 = load i8*, i8** %color_order.addr, align 4, !tbaa !2
  %91 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx93 = getelementptr inbounds i8, i8* %90, i32 %91
  %92 = load i8, i8* %arrayidx93, align 1, !tbaa !8
  %idxprom94 = zext i8 %92 to i32
  %arrayidx95 = getelementptr inbounds [8 x i32], [8 x i32]* %inverse_color_order, i32 0, i32 %idxprom94
  store i32 %89, i32* %arrayidx95, align 4, !tbaa !6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %max_color_order) #5
  %93 = bitcast i32* %max_score to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #5
  br label %if.end96

if.end96:                                         ; preds = %for.end90, %for.end70
  %94 = bitcast i32* %max_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  %95 = bitcast i32* %max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #5
  br label %for.inc97

for.inc97:                                        ; preds = %if.end96
  %96 = load i32, i32* %i, align 4, !tbaa !6
  %inc98 = add nsw i32 %96, 1
  store i32 %inc98, i32* %i, align 4, !tbaa !6
  br label %for.cond52

for.end99:                                        ; preds = %for.cond52
  %97 = load i32*, i32** %color_idx.addr, align 4, !tbaa !2
  %cmp100 = icmp ne i32* %97, null
  br i1 %cmp100, label %if.then102, label %if.end108

if.then102:                                       ; preds = %for.end99
  %98 = load i8*, i8** %color_map.addr, align 4, !tbaa !2
  %99 = load i32, i32* %r.addr, align 4, !tbaa !6
  %100 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul103 = mul nsw i32 %99, %100
  %101 = load i32, i32* %c.addr, align 4, !tbaa !6
  %add104 = add nsw i32 %mul103, %101
  %arrayidx105 = getelementptr inbounds i8, i8* %98, i32 %add104
  %102 = load i8, i8* %arrayidx105, align 1, !tbaa !8
  %idxprom106 = zext i8 %102 to i32
  %arrayidx107 = getelementptr inbounds [8 x i32], [8 x i32]* %inverse_color_order, i32 0, i32 %idxprom106
  %103 = load i32, i32* %arrayidx107, align 4, !tbaa !6
  %104 = load i32*, i32** %color_idx.addr, align 4, !tbaa !2
  store i32 %103, i32* %104, align 4, !tbaa !6
  br label %if.end108

if.end108:                                        ; preds = %if.then102, %for.end99
  %105 = bitcast i32* %color_index_ctx_hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #5
  store i32 0, i32* %color_index_ctx_hash, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond109

for.cond109:                                      ; preds = %for.inc117, %if.end108
  %106 = load i32, i32* %i, align 4, !tbaa !6
  %cmp110 = icmp slt i32 %106, 3
  br i1 %cmp110, label %for.body112, label %for.end119

for.body112:                                      ; preds = %for.cond109
  %107 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx113 = getelementptr inbounds [18 x i32], [18 x i32]* %scores, i32 0, i32 %107
  %108 = load i32, i32* %arrayidx113, align 4, !tbaa !6
  %109 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx114 = getelementptr inbounds [3 x i32], [3 x i32]* @av1_get_palette_color_index_context.hash_multipliers, i32 0, i32 %109
  %110 = load i32, i32* %arrayidx114, align 4, !tbaa !6
  %mul115 = mul nsw i32 %108, %110
  %111 = load i32, i32* %color_index_ctx_hash, align 4, !tbaa !6
  %add116 = add nsw i32 %111, %mul115
  store i32 %add116, i32* %color_index_ctx_hash, align 4, !tbaa !6
  br label %for.inc117

for.inc117:                                       ; preds = %for.body112
  %112 = load i32, i32* %i, align 4, !tbaa !6
  %inc118 = add nsw i32 %112, 1
  store i32 %inc118, i32* %i, align 4, !tbaa !6
  br label %for.cond109

for.end119:                                       ; preds = %for.cond109
  %113 = bitcast i32* %color_index_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #5
  %114 = load i32, i32* %color_index_ctx_hash, align 4, !tbaa !6
  %arrayidx120 = getelementptr inbounds [9 x i32], [9 x i32]* @palette_color_index_context_lookup, i32 0, i32 %114
  %115 = load i32, i32* %arrayidx120, align 4, !tbaa !6
  store i32 %115, i32* %color_index_ctx, align 4, !tbaa !6
  %116 = load i32, i32* %color_index_ctx, align 4, !tbaa !6
  %117 = bitcast i32* %color_index_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast i32* %color_index_ctx_hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast [8 x i32]* %inverse_color_order to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %119) #5
  %120 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  %121 = bitcast [18 x i32]* %scores to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %121) #5
  %122 = bitcast [3 x i32]* %color_neighbors to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %122) #5
  ret i32 %116
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_set_default_ref_deltas(i8* %ref_deltas) #0 {
entry:
  %ref_deltas.addr = alloca i8*, align 4
  store i8* %ref_deltas, i8** %ref_deltas.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 0
  store i8 1, i8* %arrayidx, align 1, !tbaa !8
  %1 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %1, i32 1
  store i8 0, i8* %arrayidx1, align 1, !tbaa !8
  %2 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %2, i32 1
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %4 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %4, i32 2
  store i8 %3, i8* %arrayidx3, align 1, !tbaa !8
  %5 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %5, i32 1
  %6 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %7 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %7, i32 3
  store i8 %6, i8* %arrayidx5, align 1, !tbaa !8
  %8 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %8, i32 1
  %9 = load i8, i8* %arrayidx6, align 1, !tbaa !8
  %10 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %10, i32 5
  store i8 %9, i8* %arrayidx7, align 1, !tbaa !8
  %11 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %11, i32 4
  store i8 -1, i8* %arrayidx8, align 1, !tbaa !8
  %12 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %12, i32 6
  store i8 -1, i8* %arrayidx9, align 1, !tbaa !8
  %13 = load i8*, i8** %ref_deltas.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %13, i32 7
  store i8 -1, i8* %arrayidx10, align 1, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_set_default_mode_deltas(i8* %mode_deltas) #0 {
entry:
  %mode_deltas.addr = alloca i8*, align 4
  store i8* %mode_deltas, i8** %mode_deltas.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %mode_deltas.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 0
  store i8 0, i8* %arrayidx, align 1, !tbaa !8
  %1 = load i8*, i8** %mode_deltas.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %1, i32 1
  store i8 0, i8* %arrayidx1, align 1, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_setup_frame_contexts(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %i = alloca i32, align 4
  %buf = alloca %struct.RefCntBuffer*, align 4
  %i5 = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %default_frame_context = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 39
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %default_frame_context, align 4, !tbaa !9
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %fc = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 38
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %fc, align 16, !tbaa !33
  %4 = bitcast %struct.frame_contexts* %1 to i8*
  %5 = bitcast %struct.frame_contexts* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 21264, i1 false), !tbaa.struct !34
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 40
  %large_scale = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles, i32 0, i32 17
  %7 = load i32, i32* %large_scale, align 4, !tbaa !35
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp sle i32 %9, 7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %11 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %conv = trunc i32 %13 to i8
  %call = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %12, i8 signext %conv)
  store %struct.RefCntBuffer* %call, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %14 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %cmp1 = icmp ne %struct.RefCntBuffer* %14, null
  br i1 %cmp1, label %if.then3, label %if.end

if.then3:                                         ; preds = %for.body
  %15 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf, align 4, !tbaa !2
  %frame_context = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %15, i32 0, i32 22
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %fc4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %16, i32 0, i32 38
  %17 = load %struct.frame_contexts*, %struct.frame_contexts** %fc4, align 16, !tbaa !33
  %18 = bitcast %struct.frame_contexts* %frame_context to i8*
  %19 = bitcast %struct.frame_contexts* %17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 21264, i1 false), !tbaa.struct !34
  br label %if.end

if.end:                                           ; preds = %if.then3, %for.body
  %20 = bitcast %struct.RefCntBuffer** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %22 = bitcast i32* %i5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  store i32 0, i32* %i5, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc13, %for.end
  %23 = load i32, i32* %i5, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %23, 16
  br i1 %cmp7, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond6
  %24 = bitcast i32* %i5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  br label %for.end15

for.body10:                                       ; preds = %for.cond6
  %25 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %25, i32 0, i32 41
  %26 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool, align 4, !tbaa !36
  %frame_bufs = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %26, i32 0, i32 3
  %27 = load i32, i32* %i5, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs, i32 0, i32 %27
  %frame_context11 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx, i32 0, i32 22
  %28 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %fc12 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %28, i32 0, i32 38
  %29 = load %struct.frame_contexts*, %struct.frame_contexts** %fc12, align 16, !tbaa !33
  %30 = bitcast %struct.frame_contexts* %frame_context11 to i8*
  %31 = bitcast %struct.frame_contexts* %29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 21264, i1 false), !tbaa.struct !34
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %32 = load i32, i32* %i5, align 4, !tbaa !6
  %inc14 = add nsw i32 %32, 1
  store i32 %inc14, i32* %i5, align 4, !tbaa !6
  br label %for.cond6

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %entry
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint nounwind
define internal %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %cm, i8 signext %ref_frame) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  %map_idx = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !8
  %0 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %2 = load i8, i8* %ref_frame.addr, align 1, !tbaa !8
  %call = call i32 @get_ref_frame_map_idx(%struct.AV1Common* %1, i8 signext %2)
  store i32 %call, i32* %map_idx, align 4, !tbaa !6
  %3 = load i32, i32* %map_idx, align 4, !tbaa !6
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 17
  %5 = load i32, i32* %map_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %5
  %6 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.RefCntBuffer* [ %6, %cond.true ], [ null, %cond.false ]
  %7 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  ret %struct.RefCntBuffer* %cond
}

; Function Attrs: nounwind
define hidden void @av1_setup_past_independence(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 24
  call void @av1_clearall_segfeatures(%struct.segmentation* %seg)
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 13
  %2 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !37
  %seg_map = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %2, i32 0, i32 6
  %3 = load i8*, i8** %seg_map, align 4, !tbaa !38
  %tobool = icmp ne i8* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 13
  %5 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame1, align 8, !tbaa !37
  %seg_map2 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %5, i32 0, i32 6
  %6 = load i8*, i8** %seg_map2, align 4, !tbaa !38
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %8 = load i32, i32* %mi_rows, align 4, !tbaa !44
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params3 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params3, i32 0, i32 4
  %10 = load i32, i32* %mi_cols, align 4, !tbaa !45
  %mul = mul nsw i32 %8, %10
  call void @llvm.memset.p0i8.i32(i8* align 1 %6, i8 0, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 13
  %12 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame4, align 8, !tbaa !37
  %ref_deltas = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %12, i32 0, i32 20
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %ref_deltas, i32 0, i32 0
  call void @av1_set_default_ref_deltas(i8* %arraydecay)
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame5 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 13
  %14 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame5, align 8, !tbaa !37
  %mode_deltas = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %14, i32 0, i32 21
  %arraydecay6 = getelementptr inbounds [2 x i8], [2 x i8]* %mode_deltas, i32 0, i32 0
  call void @av1_set_default_mode_deltas(i8* %arraydecay6)
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %lf = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 28
  call void @set_default_lf_deltas(%struct.loopfilter* %lf)
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  call void @av1_default_coef_probs(%struct.AV1Common* %16)
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %fc = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 38
  %18 = load %struct.frame_contexts*, %struct.frame_contexts** %fc, align 16, !tbaa !33
  call void @init_mode_probs(%struct.frame_contexts* %18)
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  call void @av1_init_mv_probs(%struct.AV1Common* %19)
  %20 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %fc7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %20, i32 0, i32 38
  %21 = load %struct.frame_contexts*, %struct.frame_contexts** %fc7, align 16, !tbaa !33
  %initialized = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %21, i32 0, i32 66
  store i32 1, i32* %initialized, align 4, !tbaa !46
  %22 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  call void @av1_setup_frame_contexts(%struct.AV1Common* %22)
  ret void
}

declare void @av1_clearall_segfeatures(%struct.segmentation*) #4

; Function Attrs: nounwind
define internal void @set_default_lf_deltas(%struct.loopfilter* %lf) #0 {
entry:
  %lf.addr = alloca %struct.loopfilter*, align 4
  store %struct.loopfilter* %lf, %struct.loopfilter** %lf.addr, align 4, !tbaa !2
  %0 = load %struct.loopfilter*, %struct.loopfilter** %lf.addr, align 4, !tbaa !2
  %mode_ref_delta_enabled = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %0, i32 0, i32 4
  store i8 1, i8* %mode_ref_delta_enabled, align 4, !tbaa !47
  %1 = load %struct.loopfilter*, %struct.loopfilter** %lf.addr, align 4, !tbaa !2
  %mode_ref_delta_update = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %1, i32 0, i32 5
  store i8 1, i8* %mode_ref_delta_update, align 1, !tbaa !48
  %2 = load %struct.loopfilter*, %struct.loopfilter** %lf.addr, align 4, !tbaa !2
  %ref_deltas = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %2, i32 0, i32 6
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %ref_deltas, i32 0, i32 0
  call void @av1_set_default_ref_deltas(i8* %arraydecay)
  %3 = load %struct.loopfilter*, %struct.loopfilter** %lf.addr, align 4, !tbaa !2
  %mode_deltas = getelementptr inbounds %struct.loopfilter, %struct.loopfilter* %3, i32 0, i32 7
  %arraydecay1 = getelementptr inbounds [2 x i8], [2 x i8]* %mode_deltas, i32 0, i32 0
  call void @av1_set_default_mode_deltas(i8* %arraydecay1)
  ret void
}

declare void @av1_default_coef_probs(%struct.AV1Common*) #4

; Function Attrs: nounwind
define internal void @init_mode_probs(%struct.frame_contexts* %fc) #0 {
entry:
  %fc.addr = alloca %struct.frame_contexts*, align 4
  %i = alloca i32, align 4
  store %struct.frame_contexts* %fc, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %0 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %palette_y_size_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %0, i32 0, i32 25
  %arraydecay = getelementptr inbounds [7 x [8 x i16]], [7 x [8 x i16]]* %palette_y_size_cdf, i32 0, i32 0
  %1 = bitcast [8 x i16]* %arraydecay to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 16 bitcast ([7 x [8 x i16]]* @default_palette_y_size_cdf to i8*), i32 112, i1 false)
  %2 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %palette_uv_size_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %2, i32 0, i32 26
  %arraydecay1 = getelementptr inbounds [7 x [8 x i16]], [7 x [8 x i16]]* %palette_uv_size_cdf, i32 0, i32 0
  %3 = bitcast [8 x i16]* %arraydecay1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 16 bitcast ([7 x [8 x i16]]* @default_palette_uv_size_cdf to i8*), i32 112, i1 false)
  %4 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %palette_y_color_index_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %4, i32 0, i32 27
  %arraydecay2 = getelementptr inbounds [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]]* %palette_y_color_index_cdf, i32 0, i32 0
  %5 = bitcast [5 x [9 x i16]]* %arraydecay2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 16 bitcast (<{ [5 x <{ i16, [8 x i16] }>], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]] }>* @default_palette_y_color_index_cdf to i8*), i32 630, i1 false)
  %6 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %palette_uv_color_index_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %6, i32 0, i32 28
  %arraydecay3 = getelementptr inbounds [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]]* %palette_uv_color_index_cdf, i32 0, i32 0
  %7 = bitcast [5 x [9 x i16]]* %arraydecay3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %7, i8* align 16 bitcast (<{ [5 x <{ i16, [8 x i16] }>], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]], [5 x [9 x i16]] }>* @default_palette_uv_color_index_cdf to i8*), i32 630, i1 false)
  %8 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %kf_y_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %8, i32 0, i32 56
  %arraydecay4 = getelementptr inbounds [5 x [5 x [14 x i16]]], [5 x [5 x [14 x i16]]]* %kf_y_cdf, i32 0, i32 0
  %9 = bitcast [5 x [14 x i16]]* %arraydecay4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 16 bitcast ([5 x [5 x [14 x i16]]]* @default_kf_y_mode_cdf to i8*), i32 700, i1 false)
  %10 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %angle_delta_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %10, i32 0, i32 57
  %arraydecay5 = getelementptr inbounds [8 x [8 x i16]], [8 x [8 x i16]]* %angle_delta_cdf, i32 0, i32 0
  %11 = bitcast [8 x i16]* %arraydecay5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 16 bitcast ([8 x [8 x i16]]* @default_angle_delta_cdf to i8*), i32 128, i1 false)
  %12 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %comp_inter_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %12, i32 0, i32 31
  %arraydecay6 = getelementptr inbounds [5 x [3 x i16]], [5 x [3 x i16]]* %comp_inter_cdf, i32 0, i32 0
  %13 = bitcast [3 x i16]* %arraydecay6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %13, i8* align 16 bitcast ([5 x [3 x i16]]* @default_comp_inter_cdf to i8*), i32 30, i1 false)
  %14 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %comp_ref_type_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %14, i32 0, i32 33
  %arraydecay7 = getelementptr inbounds [5 x [3 x i16]], [5 x [3 x i16]]* %comp_ref_type_cdf, i32 0, i32 0
  %15 = bitcast [3 x i16]* %arraydecay7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 16 bitcast ([5 x [3 x i16]]* @default_comp_ref_type_cdf to i8*), i32 30, i1 false)
  %16 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %uni_comp_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %16, i32 0, i32 34
  %arraydecay8 = getelementptr inbounds [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]]* %uni_comp_ref_cdf, i32 0, i32 0
  %17 = bitcast [3 x [3 x i16]]* %arraydecay8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %17, i8* align 16 bitcast ([3 x [3 x [3 x i16]]]* @default_uni_comp_ref_cdf to i8*), i32 54, i1 false)
  %18 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %palette_y_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %18, i32 0, i32 29
  %arraydecay9 = getelementptr inbounds [7 x [3 x [3 x i16]]], [7 x [3 x [3 x i16]]]* %palette_y_mode_cdf, i32 0, i32 0
  %19 = bitcast [3 x [3 x i16]]* %arraydecay9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 16 bitcast ([7 x [3 x [3 x i16]]]* @default_palette_y_mode_cdf to i8*), i32 126, i1 false)
  %20 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %palette_uv_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %20, i32 0, i32 30
  %arraydecay10 = getelementptr inbounds [2 x [3 x i16]], [2 x [3 x i16]]* %palette_uv_mode_cdf, i32 0, i32 0
  %21 = bitcast [3 x i16]* %arraydecay10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %21, i8* align 2 bitcast ([2 x [3 x i16]]* @default_palette_uv_mode_cdf to i8*), i32 12, i1 false)
  %22 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %comp_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %22, i32 0, i32 35
  %arraydecay11 = getelementptr inbounds [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]]* %comp_ref_cdf, i32 0, i32 0
  %23 = bitcast [3 x [3 x i16]]* %arraydecay11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 16 bitcast ([3 x [3 x [3 x i16]]]* @default_comp_ref_cdf to i8*), i32 54, i1 false)
  %24 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %comp_bwdref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %24, i32 0, i32 36
  %arraydecay12 = getelementptr inbounds [3 x [2 x [3 x i16]]], [3 x [2 x [3 x i16]]]* %comp_bwdref_cdf, i32 0, i32 0
  %25 = bitcast [2 x [3 x i16]]* %arraydecay12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %25, i8* align 16 bitcast ([3 x [2 x [3 x i16]]]* @default_comp_bwdref_cdf to i8*), i32 36, i1 false)
  %26 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %single_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %26, i32 0, i32 32
  %arraydecay13 = getelementptr inbounds [3 x [6 x [3 x i16]]], [3 x [6 x [3 x i16]]]* %single_ref_cdf, i32 0, i32 0
  %27 = bitcast [6 x [3 x i16]]* %arraydecay13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 16 bitcast ([3 x [6 x [3 x i16]]]* @default_single_ref_cdf to i8*), i32 108, i1 false)
  %28 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %txfm_partition_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %28, i32 0, i32 37
  %arraydecay14 = getelementptr inbounds [21 x [3 x i16]], [21 x [3 x i16]]* %txfm_partition_cdf, i32 0, i32 0
  %29 = bitcast [3 x i16]* %arraydecay14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %29, i8* align 16 bitcast ([21 x [3 x i16]]* @default_txfm_partition_cdf to i8*), i32 126, i1 false)
  %30 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %compound_index_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %30, i32 0, i32 38
  %arraydecay15 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %compound_index_cdf, i32 0, i32 0
  %31 = bitcast [3 x i16]* %arraydecay15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 16 bitcast ([6 x [3 x i16]]* @default_compound_idx_cdfs to i8*), i32 36, i1 false)
  %32 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %comp_group_idx_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %32, i32 0, i32 39
  %arraydecay16 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %comp_group_idx_cdf, i32 0, i32 0
  %33 = bitcast [3 x i16]* %arraydecay16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 16 bitcast ([6 x [3 x i16]]* @default_comp_group_idx_cdfs to i8*), i32 36, i1 false)
  %34 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %newmv_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %34, i32 0, i32 13
  %arraydecay17 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %newmv_cdf, i32 0, i32 0
  %35 = bitcast [3 x i16]* %arraydecay17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %35, i8* align 16 bitcast ([6 x [3 x i16]]* @default_newmv_cdf to i8*), i32 36, i1 false)
  %36 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %zeromv_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %36, i32 0, i32 14
  %arraydecay18 = getelementptr inbounds [2 x [3 x i16]], [2 x [3 x i16]]* %zeromv_cdf, i32 0, i32 0
  %37 = bitcast [3 x i16]* %arraydecay18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %37, i8* align 2 bitcast ([2 x [3 x i16]]* @default_zeromv_cdf to i8*), i32 12, i1 false)
  %38 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %refmv_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %38, i32 0, i32 15
  %arraydecay19 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %refmv_cdf, i32 0, i32 0
  %39 = bitcast [3 x i16]* %arraydecay19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %39, i8* align 16 bitcast ([6 x [3 x i16]]* @default_refmv_cdf to i8*), i32 36, i1 false)
  %40 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %drl_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %40, i32 0, i32 16
  %arraydecay20 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %drl_cdf, i32 0, i32 0
  %41 = bitcast [3 x i16]* %arraydecay20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %41, i8* align 16 bitcast ([3 x [3 x i16]]* @default_drl_cdf to i8*), i32 18, i1 false)
  %42 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %motion_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %42, i32 0, i32 23
  %arraydecay21 = getelementptr inbounds [22 x [4 x i16]], [22 x [4 x i16]]* %motion_mode_cdf, i32 0, i32 0
  %43 = bitcast [4 x i16]* %arraydecay21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 16 bitcast ([22 x [4 x i16]]* @default_motion_mode_cdf to i8*), i32 176, i1 false)
  %44 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %obmc_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %44, i32 0, i32 24
  %arraydecay22 = getelementptr inbounds [22 x [3 x i16]], [22 x [3 x i16]]* %obmc_cdf, i32 0, i32 0
  %45 = bitcast [3 x i16]* %arraydecay22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 16 bitcast ([22 x [3 x i16]]* @default_obmc_cdf to i8*), i32 132, i1 false)
  %46 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %inter_compound_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %46, i32 0, i32 17
  %arraydecay23 = getelementptr inbounds [8 x [9 x i16]], [8 x [9 x i16]]* %inter_compound_mode_cdf, i32 0, i32 0
  %47 = bitcast [9 x i16]* %arraydecay23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 16 bitcast ([8 x [9 x i16]]* @default_inter_compound_mode_cdf to i8*), i32 144, i1 false)
  %48 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %compound_type_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %48, i32 0, i32 18
  %arraydecay24 = getelementptr inbounds [22 x [3 x i16]], [22 x [3 x i16]]* %compound_type_cdf, i32 0, i32 0
  %49 = bitcast [3 x i16]* %arraydecay24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %49, i8* align 16 bitcast ([22 x [3 x i16]]* @default_compound_type_cdf to i8*), i32 132, i1 false)
  %50 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %wedge_idx_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %50, i32 0, i32 19
  %arraydecay25 = getelementptr inbounds [22 x [17 x i16]], [22 x [17 x i16]]* %wedge_idx_cdf, i32 0, i32 0
  %51 = bitcast [17 x i16]* %arraydecay25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %51, i8* align 16 bitcast ([22 x [17 x i16]]* @default_wedge_idx_cdf to i8*), i32 748, i1 false)
  %52 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %interintra_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %52, i32 0, i32 20
  %arraydecay26 = getelementptr inbounds [4 x [3 x i16]], [4 x [3 x i16]]* %interintra_cdf, i32 0, i32 0
  %53 = bitcast [3 x i16]* %arraydecay26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %53, i8* align 16 bitcast ([4 x [3 x i16]]* @default_interintra_cdf to i8*), i32 24, i1 false)
  %54 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %wedge_interintra_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %54, i32 0, i32 21
  %arraydecay27 = getelementptr inbounds [22 x [3 x i16]], [22 x [3 x i16]]* %wedge_interintra_cdf, i32 0, i32 0
  %55 = bitcast [3 x i16]* %arraydecay27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 16 bitcast ([22 x [3 x i16]]* @default_wedge_interintra_cdf to i8*), i32 132, i1 false)
  %56 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %interintra_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %56, i32 0, i32 22
  %arraydecay28 = getelementptr inbounds [4 x [5 x i16]], [4 x [5 x i16]]* %interintra_mode_cdf, i32 0, i32 0
  %57 = bitcast [5 x i16]* %arraydecay28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 16 bitcast ([4 x [5 x i16]]* @default_interintra_mode_cdf to i8*), i32 40, i1 false)
  %58 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %seg = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %58, i32 0, i32 46
  %pred_cdf = getelementptr inbounds %struct.segmentation_probs, %struct.segmentation_probs* %seg, i32 0, i32 1
  %arraydecay29 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %pred_cdf, i32 0, i32 0
  %59 = bitcast [3 x i16]* %arraydecay29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %59, i8* align 16 bitcast ([3 x [3 x i16]]* @default_segment_pred_cdf to i8*), i32 18, i1 false)
  %60 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %seg30 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %60, i32 0, i32 46
  %tree_cdf = getelementptr inbounds %struct.segmentation_probs, %struct.segmentation_probs* %seg30, i32 0, i32 0
  %arraydecay31 = getelementptr inbounds [9 x i16], [9 x i16]* %tree_cdf, i32 0, i32 0
  %61 = bitcast i16* %arraydecay31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %61, i8* align 16 bitcast ([9 x i16]* @default_seg_tree_cdf to i8*), i32 18, i1 false)
  %62 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %filter_intra_cdfs = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %62, i32 0, i32 47
  %arraydecay32 = getelementptr inbounds [22 x [3 x i16]], [22 x [3 x i16]]* %filter_intra_cdfs, i32 0, i32 0
  %63 = bitcast [3 x i16]* %arraydecay32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %63, i8* align 16 bitcast ([22 x [3 x i16]]* @default_filter_intra_cdfs to i8*), i32 132, i1 false)
  %64 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %filter_intra_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %64, i32 0, i32 48
  %arraydecay33 = getelementptr inbounds [6 x i16], [6 x i16]* %filter_intra_mode_cdf, i32 0, i32 0
  %65 = bitcast i16* %arraydecay33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %65, i8* align 2 bitcast ([6 x i16]* @default_filter_intra_mode_cdf to i8*), i32 12, i1 false)
  %66 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %switchable_restore_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %66, i32 0, i32 49
  %arraydecay34 = getelementptr inbounds [4 x i16], [4 x i16]* %switchable_restore_cdf, i32 0, i32 0
  %67 = bitcast i16* %arraydecay34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %67, i8* align 2 bitcast ([4 x i16]* @default_switchable_restore_cdf to i8*), i32 8, i1 false)
  %68 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %wiener_restore_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %68, i32 0, i32 50
  %arraydecay35 = getelementptr inbounds [3 x i16], [3 x i16]* %wiener_restore_cdf, i32 0, i32 0
  %69 = bitcast i16* %arraydecay35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 2 bitcast ([3 x i16]* @default_wiener_restore_cdf to i8*), i32 6, i1 false)
  %70 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %sgrproj_restore_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %70, i32 0, i32 51
  %arraydecay36 = getelementptr inbounds [3 x i16], [3 x i16]* %sgrproj_restore_cdf, i32 0, i32 0
  %71 = bitcast i16* %arraydecay36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %71, i8* align 2 bitcast ([3 x i16]* @default_sgrproj_restore_cdf to i8*), i32 6, i1 false)
  %72 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %y_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %72, i32 0, i32 52
  %arraydecay37 = getelementptr inbounds [4 x [14 x i16]], [4 x [14 x i16]]* %y_mode_cdf, i32 0, i32 0
  %73 = bitcast [14 x i16]* %arraydecay37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 16 bitcast ([4 x [14 x i16]]* @default_if_y_mode_cdf to i8*), i32 112, i1 false)
  %74 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %uv_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %74, i32 0, i32 53
  %arraydecay38 = getelementptr inbounds [2 x [13 x [15 x i16]]], [2 x [13 x [15 x i16]]]* %uv_mode_cdf, i32 0, i32 0
  %75 = bitcast [13 x [15 x i16]]* %arraydecay38 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %75, i8* align 16 bitcast ([2 x [13 x [15 x i16]]]* @default_uv_mode_cdf to i8*), i32 780, i1 false)
  %76 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %switchable_interp_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %76, i32 0, i32 55
  %arraydecay39 = getelementptr inbounds [16 x [4 x i16]], [16 x [4 x i16]]* %switchable_interp_cdf, i32 0, i32 0
  %77 = bitcast [4 x i16]* %arraydecay39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %77, i8* align 16 bitcast ([16 x [4 x i16]]* @default_switchable_interp_cdf to i8*), i32 128, i1 false)
  %78 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %partition_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %78, i32 0, i32 54
  %arraydecay40 = getelementptr inbounds [20 x [11 x i16]], [20 x [11 x i16]]* %partition_cdf, i32 0, i32 0
  %79 = bitcast [11 x i16]* %arraydecay40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %79, i8* align 16 bitcast (<{ <{ i16, i16, i16, [8 x i16] }>, <{ i16, i16, i16, [8 x i16] }>, <{ i16, i16, i16, [8 x i16] }>, <{ i16, i16, i16, [8 x i16] }>, [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16], [11 x i16] }>* @default_partition_cdf to i8*), i32 440, i1 false)
  %80 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %intra_ext_tx_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %80, i32 0, i32 62
  %arraydecay41 = getelementptr inbounds [3 x [4 x [13 x [17 x i16]]]], [3 x [4 x [13 x [17 x i16]]]]* %intra_ext_tx_cdf, i32 0, i32 0
  %81 = bitcast [4 x [13 x [17 x i16]]]* %arraydecay41 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 16 bitcast (<{ [4 x [13 x [17 x i16]]], [4 x [13 x <{ i16, i16, i16, i16, i16, i16, [11 x i16] }>]], [4 x [13 x <{ i16, i16, i16, i16, [13 x i16] }>]] }>* @default_intra_ext_tx_cdf to i8*), i32 5304, i1 false)
  %82 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %inter_ext_tx_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %82, i32 0, i32 63
  %arraydecay42 = getelementptr inbounds [4 x [4 x [17 x i16]]], [4 x [4 x [17 x i16]]]* %inter_ext_tx_cdf, i32 0, i32 0
  %83 = bitcast [4 x [17 x i16]]* %arraydecay42 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %83, i8* align 16 bitcast (<{ [4 x [17 x i16]], [4 x [17 x i16]], [4 x [17 x i16]], [4 x <{ i16, [16 x i16] }>] }>* @default_inter_ext_tx_cdf to i8*), i32 544, i1 false)
  %84 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %skip_mode_cdfs = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %84, i32 0, i32 40
  %arraydecay43 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %skip_mode_cdfs, i32 0, i32 0
  %85 = bitcast [3 x i16]* %arraydecay43 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %85, i8* align 16 bitcast ([3 x [3 x i16]]* @default_skip_mode_cdfs to i8*), i32 18, i1 false)
  %86 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %skip_cdfs = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %86, i32 0, i32 41
  %arraydecay44 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %skip_cdfs, i32 0, i32 0
  %87 = bitcast [3 x i16]* %arraydecay44 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %87, i8* align 16 bitcast ([3 x [3 x i16]]* @default_skip_cdfs to i8*), i32 18, i1 false)
  %88 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %intra_inter_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %88, i32 0, i32 42
  %arraydecay45 = getelementptr inbounds [4 x [3 x i16]], [4 x [3 x i16]]* %intra_inter_cdf, i32 0, i32 0
  %89 = bitcast [3 x i16]* %arraydecay45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %89, i8* align 16 bitcast ([4 x [3 x i16]]* @default_intra_inter_cdf to i8*), i32 24, i1 false)
  %90 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %91 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %91, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %92 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %93 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %seg46 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %93, i32 0, i32 46
  %spatial_pred_seg_cdf = getelementptr inbounds %struct.segmentation_probs, %struct.segmentation_probs* %seg46, i32 0, i32 2
  %94 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x [9 x i16]], [3 x [9 x i16]]* %spatial_pred_seg_cdf, i32 0, i32 %94
  %arraydecay47 = getelementptr inbounds [9 x i16], [9 x i16]* %arrayidx, i32 0, i32 0
  %95 = bitcast i16* %arraydecay47 to i8*
  %96 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds [3 x [9 x i16]], [3 x [9 x i16]]* @default_spatial_pred_seg_tree_cdf, i32 0, i32 %96
  %arraydecay49 = getelementptr inbounds [9 x i16], [9 x i16]* %arrayidx48, i32 0, i32 0
  %97 = bitcast i16* %arraydecay49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %95, i8* align 2 %97, i32 18, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %98 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %98, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %99 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %tx_size_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %99, i32 0, i32 58
  %arraydecay50 = getelementptr inbounds [4 x [3 x [4 x i16]]], [4 x [3 x [4 x i16]]]* %tx_size_cdf, i32 0, i32 0
  %100 = bitcast [3 x [4 x i16]]* %arraydecay50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %100, i8* align 16 bitcast ([4 x [3 x [4 x i16]]]* @default_tx_size_cdf to i8*), i32 96, i1 false)
  %101 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %delta_q_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %101, i32 0, i32 59
  %arraydecay51 = getelementptr inbounds [5 x i16], [5 x i16]* %delta_q_cdf, i32 0, i32 0
  %102 = bitcast i16* %arraydecay51 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %102, i8* align 2 bitcast ([5 x i16]* @default_delta_q_cdf to i8*), i32 10, i1 false)
  %103 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %delta_lf_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %103, i32 0, i32 61
  %arraydecay52 = getelementptr inbounds [5 x i16], [5 x i16]* %delta_lf_cdf, i32 0, i32 0
  %104 = bitcast i16* %arraydecay52 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %104, i8* align 2 bitcast ([5 x i16]* @default_delta_lf_cdf to i8*), i32 10, i1 false)
  %105 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %delta_lf_multi_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %105, i32 0, i32 60
  %arraydecay53 = getelementptr inbounds [4 x [5 x i16]], [4 x [5 x i16]]* %delta_lf_multi_cdf, i32 0, i32 0
  %106 = bitcast [5 x i16]* %arraydecay53 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %106, i8* align 16 bitcast ([4 x [5 x i16]]* @default_delta_lf_multi_cdf to i8*), i32 40, i1 false)
  %107 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %cfl_sign_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %107, i32 0, i32 64
  %arraydecay54 = getelementptr inbounds [9 x i16], [9 x i16]* %cfl_sign_cdf, i32 0, i32 0
  %108 = bitcast i16* %arraydecay54 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %108, i8* align 16 bitcast ([9 x i16]* @default_cfl_sign_cdf to i8*), i32 18, i1 false)
  %109 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %cfl_alpha_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %109, i32 0, i32 65
  %arraydecay55 = getelementptr inbounds [6 x [17 x i16]], [6 x [17 x i16]]* %cfl_alpha_cdf, i32 0, i32 0
  %110 = bitcast [17 x i16]* %arraydecay55 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %110, i8* align 16 bitcast ([6 x [17 x i16]]* @default_cfl_alpha_cdf to i8*), i32 204, i1 false)
  %111 = load %struct.frame_contexts*, %struct.frame_contexts** %fc.addr, align 4, !tbaa !2
  %intrabc_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %111, i32 0, i32 45
  %arraydecay56 = getelementptr inbounds [3 x i16], [3 x i16]* %intrabc_cdf, i32 0, i32 0
  %112 = bitcast i16* %arraydecay56 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %112, i8* align 2 bitcast ([3 x i16]* @default_intrabc_cdf to i8*), i32 6, i1 false)
  ret void
}

declare void @av1_init_mv_probs(%struct.AV1Common*) #4

; Function Attrs: inlinehint nounwind
define internal i32 @get_ref_frame_map_idx(%struct.AV1Common* %cm, i8 signext %ref_frame) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !8
  %0 = load i8, i8* %ref_frame.addr, align 1, !tbaa !8
  %conv = sext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 1
  br i1 %cmp, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %ref_frame.addr, align 1, !tbaa !8
  %conv2 = sext i8 %1 to i32
  %cmp3 = icmp sle i32 %conv2, 8
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %remapped_ref_idx = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 14
  %3 = load i8, i8* %ref_frame.addr, align 1, !tbaa !8
  %conv5 = sext i8 %3 to i32
  %sub = sub nsw i32 %conv5, 1
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %remapped_ref_idx, i32 0, i32 %sub
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ -1, %cond.false ]
  ret i32 %cond
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !3, i64 18132}
!10 = !{!"AV1Common", !11, i64 0, !13, i64 40, !7, i64 288, !7, i64 292, !7, i64 296, !7, i64 300, !7, i64 304, !7, i64 308, !4, i64 312, !14, i64 313, !4, i64 316, !7, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !15, i64 492, !4, i64 580, !4, i64 1284, !7, i64 1316, !7, i64 1320, !7, i64 1324, !16, i64 1328, !17, i64 1356, !18, i64 1420, !19, i64 10676, !3, i64 10848, !20, i64 10864, !21, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !22, i64 14880, !24, i64 15028, !25, i64 15168, !27, i64 15816, !4, i64 15836, !28, i64 16192, !3, i64 18128, !3, i64 18132, !31, i64 18136, !3, i64 18724, !32, i64 18728, !7, i64 18760, !4, i64 18764, !3, i64 18796, !7, i64 18800, !4, i64 18804, !4, i64 18836, !7, i64 18844, !7, i64 18848, !7, i64 18852, !7, i64 18856}
!11 = !{!"", !4, i64 0, !4, i64 1, !7, i64 4, !7, i64 8, !7, i64 12, !12, i64 16, !7, i64 32, !7, i64 36}
!12 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!13 = !{!"aom_internal_error_info", !4, i64 0, !7, i64 4, !4, i64 8, !7, i64 88, !4, i64 92}
!14 = !{!"_Bool", !4, i64 0}
!15 = !{!"scale_factors", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!16 = !{!"", !14, i64 0, !14, i64 1, !14, i64 2, !14, i64 3, !14, i64 4, !14, i64 5, !14, i64 6, !14, i64 7, !14, i64 8, !14, i64 9, !14, i64 10, !14, i64 11, !4, i64 12, !4, i64 13, !7, i64 16, !7, i64 20, !4, i64 24}
!17 = !{!"CommonModeInfoParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !7, i64 28, !4, i64 32, !3, i64 36, !7, i64 40, !7, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!18 = !{!"CommonQuantParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !14, i64 9240, !7, i64 9244, !7, i64 9248, !7, i64 9252}
!19 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !7, i64 164, !4, i64 168}
!20 = !{!"", !4, i64 0, !4, i64 3072}
!21 = !{!"loopfilter", !4, i64 0, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !7, i64 32}
!22 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !23, i64 80, !7, i64 84, !23, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!23 = !{!"long", !4, i64 0}
!24 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !4, i64 72, !7, i64 136}
!25 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !26, i64 644}
!26 = !{!"short", !4, i64 0}
!27 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!28 = !{!"SequenceHeader", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !4, i64 16, !7, i64 20, !7, i64 24, !4, i64 28, !7, i64 32, !7, i64 36, !12, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !7, i64 112, !4, i64 116, !7, i64 244, !29, i64 248, !4, i64 264, !30, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!29 = !{!"aom_timing", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!30 = !{!"aom_dec_model_info", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!31 = !{!"CommonTileParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !4, i64 60, !4, i64 320, !7, i64 580, !7, i64 584}
!32 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !7, i64 20, !7, i64 24, !7, i64 28}
!33 = !{!10, !3, i64 18128}
!34 = !{i64 0, i64 390, !8, i64 390, i64 540, !8, i64 930, i64 36, !8, i64 966, i64 48, !8, i64 1014, i64 56, !8, i64 1070, i64 64, !8, i64 1134, i64 72, !8, i64 1206, i64 80, !8, i64 1286, i64 88, !8, i64 1374, i64 96, !8, i64 1470, i64 320, !8, i64 1790, i64 4200, !8, i64 5990, i64 2100, !8, i64 8090, i64 36, !8, i64 8126, i64 12, !8, i64 8138, i64 36, !8, i64 8174, i64 18, !8, i64 8192, i64 144, !8, i64 8336, i64 132, !8, i64 8468, i64 748, !8, i64 9216, i64 24, !8, i64 9240, i64 132, !8, i64 9372, i64 40, !8, i64 9412, i64 176, !8, i64 9588, i64 132, !8, i64 9720, i64 112, !8, i64 9832, i64 112, !8, i64 9944, i64 630, !8, i64 10574, i64 630, !8, i64 11204, i64 126, !8, i64 11330, i64 12, !8, i64 11342, i64 30, !8, i64 11372, i64 108, !8, i64 11480, i64 30, !8, i64 11510, i64 54, !8, i64 11564, i64 54, !8, i64 11618, i64 36, !8, i64 11654, i64 126, !8, i64 11780, i64 36, !8, i64 11816, i64 36, !8, i64 11852, i64 18, !8, i64 11870, i64 18, !8, i64 11888, i64 24, !8, i64 11912, i64 10, !8, i64 11922, i64 276, !8, i64 12198, i64 10, !8, i64 12208, i64 276, !8, i64 12484, i64 6, !8, i64 12490, i64 18, !8, i64 12508, i64 18, !8, i64 12526, i64 54, !8, i64 12580, i64 132, !8, i64 12712, i64 12, !8, i64 12724, i64 8, !8, i64 12732, i64 6, !8, i64 12738, i64 6, !8, i64 12744, i64 112, !8, i64 12856, i64 780, !8, i64 13636, i64 440, !8, i64 14076, i64 128, !8, i64 14204, i64 700, !8, i64 14904, i64 128, !8, i64 15032, i64 96, !8, i64 15128, i64 10, !8, i64 15138, i64 40, !8, i64 15178, i64 10, !8, i64 15188, i64 5304, !8, i64 20492, i64 544, !8, i64 21036, i64 18, !8, i64 21054, i64 204, !8, i64 21260, i64 4, !6}
!35 = !{!10, !7, i64 18716}
!36 = !{!10, !3, i64 18724}
!37 = !{!10, !3, i64 456}
!38 = !{!39, !3, i64 72}
!39 = !{!"RefCntBuffer", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 36, !4, i64 40, !3, i64 68, !3, i64 72, !19, i64 76, !7, i64 248, !7, i64 252, !7, i64 256, !7, i64 260, !4, i64 264, !7, i64 616, !4, i64 620, !25, i64 624, !40, i64 1272, !22, i64 1284, !4, i64 1432, !4, i64 1436, !4, i64 1452, !4, i64 1460, !41, i64 1464}
!40 = !{!"aom_codec_frame_buffer", !3, i64 0, !23, i64 4, !3, i64 8}
!41 = !{!"frame_contexts", !4, i64 0, !4, i64 390, !4, i64 930, !4, i64 966, !4, i64 1014, !4, i64 1070, !4, i64 1134, !4, i64 1206, !4, i64 1286, !4, i64 1374, !4, i64 1470, !4, i64 1790, !4, i64 5990, !4, i64 8090, !4, i64 8126, !4, i64 8138, !4, i64 8174, !4, i64 8192, !4, i64 8336, !4, i64 8468, !4, i64 9216, !4, i64 9240, !4, i64 9372, !4, i64 9412, !4, i64 9588, !4, i64 9720, !4, i64 9832, !4, i64 9944, !4, i64 10574, !4, i64 11204, !4, i64 11330, !4, i64 11342, !4, i64 11372, !4, i64 11480, !4, i64 11510, !4, i64 11564, !4, i64 11618, !4, i64 11654, !4, i64 11780, !4, i64 11816, !4, i64 11852, !4, i64 11870, !4, i64 11888, !42, i64 11912, !42, i64 12198, !4, i64 12484, !43, i64 12490, !4, i64 12580, !4, i64 12712, !4, i64 12724, !4, i64 12732, !4, i64 12738, !4, i64 12744, !4, i64 12856, !4, i64 13636, !4, i64 14076, !4, i64 14204, !4, i64 14904, !4, i64 15032, !4, i64 15128, !4, i64 15138, !4, i64 15178, !4, i64 15188, !4, i64 20492, !4, i64 21036, !4, i64 21054, !7, i64 21260}
!42 = !{!"", !4, i64 0, !4, i64 10}
!43 = !{!"segmentation_probs", !4, i64 0, !4, i64 18, !4, i64 36}
!44 = !{!10, !7, i64 1368}
!45 = !{!10, !7, i64 1372}
!46 = !{!41, !7, i64 21260}
!47 = !{!21, !4, i64 20}
!48 = !{!21, !4, i64 21}
