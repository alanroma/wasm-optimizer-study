; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/idct.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/idct.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.txfm_param = type { i8, i8, i32, i32, i32, i8, i32 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_mv = type { i32 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.aom_internal_error_info = type opaque
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }

@tx_size_2d = internal constant [20 x i32] [i32 16, i32 64, i32 256, i32 1024, i32 4096, i32 32, i32 32, i32 128, i32 128, i32 512, i32 512, i32 2048, i32 2048, i32 64, i32 64, i32 256, i32 256, i32 1024, i32 1024, i32 0], align 16
@tx_size_wide = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 4, i32 8, i32 8, i32 16, i32 16, i32 32, i32 32, i32 64, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64], align 16
@tx_size_high = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 8, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64, i32 32, i32 16, i32 4, i32 32, i32 8, i32 64, i32 16], align 16
@txsize_sqr_up_map = internal constant [19 x i8] c"\00\01\02\03\04\01\01\02\02\03\03\04\04\02\02\03\03\04\04", align 16
@txsize_sqr_map = internal constant [19 x i8] c"\00\01\02\03\04\00\00\01\01\02\02\03\03\00\00\01\01\02\02", align 16
@av1_ext_tx_set_lookup = internal constant [2 x [2 x i8]] [[2 x i8] c"\03\02", [2 x i8] c"\05\04"], align 1

; Function Attrs: nounwind
define hidden i32 @av1_get_tx_scale(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  %pels = alloca i32, align 4
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !2
  %0 = bitcast i32* %pels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !2
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [20 x i32], [20 x i32]* @tx_size_2d, i32 0, i32 %idxprom
  %2 = load i32, i32* %arrayidx, align 4, !tbaa !5
  store i32 %2, i32* %pels, align 4, !tbaa !5
  %3 = load i32, i32* %pels, align 4, !tbaa !5
  %cmp = icmp sgt i32 %3, 256
  %conv = zext i1 %cmp to i32
  %4 = load i32, i32* %pels, align 4, !tbaa !5
  %cmp1 = icmp sgt i32 %4, 1024
  %conv2 = zext i1 %cmp1 to i32
  %add = add nsw i32 %conv, %conv2
  %5 = bitcast i32* %pels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  ret i32 %add
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_highbd_iwht4x4_add(i32* %input, i8* %dest, i32 %stride, i32 %eob, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %eob.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store i32 %eob, i32* %eob.addr, align 4, !tbaa !5
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !5
  %0 = load i32, i32* %eob.addr, align 4, !tbaa !5
  %cmp = icmp sgt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %2 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !5
  call void @av1_highbd_iwht4x4_16_add_c(i32* %1, i8* %2, i32 %3, i32 %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %6 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %8 = load i32, i32* %bd.addr, align 4, !tbaa !5
  call void @av1_highbd_iwht4x4_1_add_c(i32* %5, i8* %6, i32 %7, i32 %8)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

declare void @av1_highbd_iwht4x4_16_add_c(i32*, i8*, i32, i32) #2

declare void @av1_highbd_iwht4x4_1_add_c(i32*, i8*, i32, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_4x4_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %eob = alloca i32, align 4
  %bd = alloca i32, align 4
  %lossless = alloca i32, align 4
  %src = alloca i32*, align 4
  %tx_type = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32* %eob to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %eob1 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %1, i32 0, i32 6
  %2 = load i32, i32* %eob1, align 4, !tbaa !9
  store i32 %2, i32* %eob, align 4, !tbaa !5
  %3 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd2 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %4, i32 0, i32 3
  %5 = load i32, i32* %bd2, align 4, !tbaa !11
  store i32 %5, i32* %bd, align 4, !tbaa !5
  %6 = bitcast i32* %lossless to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %lossless3 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 2
  %8 = load i32, i32* %lossless3, align 4, !tbaa !12
  store i32 %8, i32* %lossless, align 4, !tbaa !5
  %9 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %10)
  store i32* %call, i32** %src, align 4, !tbaa !7
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type) #4
  %11 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type4 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %11, i32 0, i32 0
  %12 = load i8, i8* %tx_type4, align 4, !tbaa !13
  store i8 %12, i8* %tx_type, align 1, !tbaa !2
  %13 = load i32, i32* %lossless, align 4, !tbaa !5
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %14 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %15 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %16 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %17 = load i32, i32* %eob, align 4, !tbaa !5
  %18 = load i32, i32* %bd, align 4, !tbaa !5
  call void @av1_highbd_iwht4x4_add(i32* %14, i8* %15, i32 %16, i32 %17, i32 %18)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %19 = load i32*, i32** %src, align 4, !tbaa !7
  %20 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %21 = ptrtoint i8* %20 to i32
  %shl = shl i32 %21, 1
  %22 = inttoptr i32 %shl to i16*
  %23 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %24 = load i8, i8* %tx_type, align 1, !tbaa !2
  %25 = load i32, i32* %bd, align 4, !tbaa !5
  call void @av1_inv_txfm2d_add_4x4_c(i32* %19, i16* %22, i32 %23, i8 zeroext %24, i32 %25)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type) #4
  %26 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = bitcast i32* %lossless to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  %28 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  %29 = bitcast i32* %eob to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32* @cast_to_int32(i32* %input) #3 {
entry:
  %input.addr = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  %0 = load i32*, i32** %input.addr, align 4, !tbaa !7
  ret i32* %0
}

declare void @av1_inv_txfm2d_add_4x4_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_4x8_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_4x8_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_4x8_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_8x4_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_8x4_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_8x4_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_16x32_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_16x32_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_16x32_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_32x16_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_32x16_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_32x16_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_16x4_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_16x4_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_16x4_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_4x16_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_4x16_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_4x16_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_32x8_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_32x8_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_32x8_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_8x32_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_8x32_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_8x32_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_32x64_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_32x64_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_32x64_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_64x32_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_64x32_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_64x32_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_16x64_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_16x64_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_16x64_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_64x16_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_64x16_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_64x16_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_8x8_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %bd = alloca i32, align 4
  %tx_type = alloca i8, align 1
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd1 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %1, i32 0, i32 3
  %2 = load i32, i32* %bd1, align 4, !tbaa !11
  store i32 %2, i32* %bd, align 4, !tbaa !5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type) #4
  %3 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type2 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %3, i32 0, i32 0
  %4 = load i8, i8* %tx_type2, align 4, !tbaa !13
  store i8 %4, i8* %tx_type, align 1, !tbaa !2
  %5 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %6)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %7 = load i32*, i32** %src, align 4, !tbaa !7
  %8 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %9 = ptrtoint i8* %8 to i32
  %shl = shl i32 %9, 1
  %10 = inttoptr i32 %shl to i16*
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %12 = load i8, i8* %tx_type, align 1, !tbaa !2
  %13 = load i32, i32* %bd, align 4, !tbaa !5
  call void @av1_inv_txfm2d_add_8x8_c(i32* %7, i16* %10, i32 %11, i8 zeroext %12, i32 %13)
  %14 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type) #4
  %15 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret void
}

declare void @av1_inv_txfm2d_add_8x8_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_16x16_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %bd = alloca i32, align 4
  %tx_type = alloca i8, align 1
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd1 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %1, i32 0, i32 3
  %2 = load i32, i32* %bd1, align 4, !tbaa !11
  store i32 %2, i32* %bd, align 4, !tbaa !5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type) #4
  %3 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type2 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %3, i32 0, i32 0
  %4 = load i8, i8* %tx_type2, align 4, !tbaa !13
  store i8 %4, i8* %tx_type, align 1, !tbaa !2
  %5 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %6)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %7 = load i32*, i32** %src, align 4, !tbaa !7
  %8 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %9 = ptrtoint i8* %8 to i32
  %shl = shl i32 %9, 1
  %10 = inttoptr i32 %shl to i16*
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %12 = load i8, i8* %tx_type, align 1, !tbaa !2
  %13 = load i32, i32* %bd, align 4, !tbaa !5
  call void @av1_inv_txfm2d_add_16x16_c(i32* %7, i16* %10, i32 %11, i8 zeroext %12, i32 %13)
  %14 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type) #4
  %15 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret void
}

declare void @av1_inv_txfm2d_add_16x16_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_8x16_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_8x16_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_8x16_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_16x8_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %1)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %2 = load i32*, i32** %src, align 4, !tbaa !7
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %7 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %7, i32 0, i32 0
  %8 = load i8, i8* %tx_type, align 4, !tbaa !13
  %9 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %9, i32 0, i32 3
  %10 = load i32, i32* %bd, align 4, !tbaa !11
  call void @av1_inv_txfm2d_add_16x8_c(i32* %2, i16* %5, i32 %6, i8 zeroext %8, i32 %10)
  %11 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

declare void @av1_inv_txfm2d_add_16x8_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_32x32_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %bd = alloca i32, align 4
  %tx_type = alloca i8, align 1
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd1 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %1, i32 0, i32 3
  %2 = load i32, i32* %bd1, align 4, !tbaa !11
  store i32 %2, i32* %bd, align 4, !tbaa !5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type) #4
  %3 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type2 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %3, i32 0, i32 0
  %4 = load i8, i8* %tx_type2, align 4, !tbaa !13
  store i8 %4, i8* %tx_type, align 1, !tbaa !2
  %5 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %6)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %7 = load i32*, i32** %src, align 4, !tbaa !7
  %8 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %9 = ptrtoint i8* %8 to i32
  %shl = shl i32 %9, 1
  %10 = inttoptr i32 %shl to i16*
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %12 = load i8, i8* %tx_type, align 1, !tbaa !2
  %13 = load i32, i32* %bd, align 4, !tbaa !5
  call void @av1_inv_txfm2d_add_32x32_c(i32* %7, i16* %10, i32 %11, i8 zeroext %12, i32 %13)
  %14 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type) #4
  %15 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret void
}

declare void @av1_inv_txfm2d_add_32x32_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_64x64_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %bd = alloca i32, align 4
  %tx_type = alloca i8, align 1
  %src = alloca i32*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd1 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %1, i32 0, i32 3
  %2 = load i32, i32* %bd1, align 4, !tbaa !11
  store i32 %2, i32* %bd, align 4, !tbaa !5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type) #4
  %3 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type2 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %3, i32 0, i32 0
  %4 = load i8, i8* %tx_type2, align 4, !tbaa !13
  store i8 %4, i8* %tx_type, align 1, !tbaa !2
  %5 = bitcast i32** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %call = call i32* @cast_to_int32(i32* %6)
  store i32* %call, i32** %src, align 4, !tbaa !7
  %7 = load i32*, i32** %src, align 4, !tbaa !7
  %8 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %9 = ptrtoint i8* %8 to i32
  %shl = shl i32 %9, 1
  %10 = inttoptr i32 %shl to i16*
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %12 = load i8, i8* %tx_type, align 1, !tbaa !2
  %13 = load i32, i32* %bd, align 4, !tbaa !5
  call void @av1_inv_txfm2d_add_64x64_c(i32* %7, i16* %10, i32 %11, i8 zeroext %12, i32 %13)
  %14 = bitcast i32** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type) #4
  %15 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret void
}

declare void @av1_inv_txfm2d_add_64x64_c(i32*, i16*, i32, i8 zeroext, i32) #2

; Function Attrs: nounwind
define hidden void @av1_highbd_inv_txfm_add_c(i32* %input, i8* %dest, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %tx_size = alloca i8, align 1
  store i32* %input, i32** %input.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size) #4
  %0 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_size1 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %0, i32 0, i32 1
  %1 = load i8, i8* %tx_size1, align 1, !tbaa !14
  store i8 %1, i8* %tx_size, align 1, !tbaa !2
  %2 = load i8, i8* %tx_size, align 1, !tbaa !2
  %conv = zext i8 %2 to i32
  switch i32 %conv, label %sw.default [
    i32 3, label %sw.bb
    i32 2, label %sw.bb2
    i32 1, label %sw.bb3
    i32 5, label %sw.bb4
    i32 6, label %sw.bb5
    i32 7, label %sw.bb6
    i32 8, label %sw.bb7
    i32 9, label %sw.bb8
    i32 10, label %sw.bb9
    i32 4, label %sw.bb10
    i32 11, label %sw.bb11
    i32 12, label %sw.bb12
    i32 17, label %sw.bb13
    i32 18, label %sw.bb14
    i32 0, label %sw.bb15
    i32 14, label %sw.bb16
    i32 13, label %sw.bb17
    i32 15, label %sw.bb18
    i32 16, label %sw.bb19
  ]

sw.bb:                                            ; preds = %entry
  %3 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %4 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %5 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %6 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_32x32_c(i32* %3, i8* %4, i32 %5, %struct.txfm_param* %6)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %7 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %8 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %9 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %10 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_16x16_c(i32* %7, i8* %8, i32 %9, %struct.txfm_param* %10)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry
  %11 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %12 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %13 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %14 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_8x8_c(i32* %11, i8* %12, i32 %13, %struct.txfm_param* %14)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry
  %15 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %16 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %17 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %18 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_4x8_c(i32* %15, i8* %16, i32 %17, %struct.txfm_param* %18)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry
  %19 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %20 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %21 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %22 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_8x4_c(i32* %19, i8* %20, i32 %21, %struct.txfm_param* %22)
  br label %sw.epilog

sw.bb6:                                           ; preds = %entry
  %23 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %24 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %25 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %26 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_8x16_c(i32* %23, i8* %24, i32 %25, %struct.txfm_param* %26)
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry
  %27 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %28 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %29 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %30 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_16x8_c(i32* %27, i8* %28, i32 %29, %struct.txfm_param* %30)
  br label %sw.epilog

sw.bb8:                                           ; preds = %entry
  %31 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %32 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %33 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %34 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_16x32_c(i32* %31, i8* %32, i32 %33, %struct.txfm_param* %34)
  br label %sw.epilog

sw.bb9:                                           ; preds = %entry
  %35 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %36 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %37 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %38 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_32x16_c(i32* %35, i8* %36, i32 %37, %struct.txfm_param* %38)
  br label %sw.epilog

sw.bb10:                                          ; preds = %entry
  %39 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %40 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %41 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %42 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_64x64_c(i32* %39, i8* %40, i32 %41, %struct.txfm_param* %42)
  br label %sw.epilog

sw.bb11:                                          ; preds = %entry
  %43 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %44 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %45 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %46 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_32x64_c(i32* %43, i8* %44, i32 %45, %struct.txfm_param* %46)
  br label %sw.epilog

sw.bb12:                                          ; preds = %entry
  %47 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %48 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %49 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %50 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_64x32_c(i32* %47, i8* %48, i32 %49, %struct.txfm_param* %50)
  br label %sw.epilog

sw.bb13:                                          ; preds = %entry
  %51 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %52 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %53 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %54 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_16x64_c(i32* %51, i8* %52, i32 %53, %struct.txfm_param* %54)
  br label %sw.epilog

sw.bb14:                                          ; preds = %entry
  %55 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %56 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %57 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %58 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_64x16_c(i32* %55, i8* %56, i32 %57, %struct.txfm_param* %58)
  br label %sw.epilog

sw.bb15:                                          ; preds = %entry
  %59 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %60 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %61 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %62 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_4x4_c(i32* %59, i8* %60, i32 %61, %struct.txfm_param* %62)
  br label %sw.epilog

sw.bb16:                                          ; preds = %entry
  %63 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %64 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %65 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %66 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_16x4_c(i32* %63, i8* %64, i32 %65, %struct.txfm_param* %66)
  br label %sw.epilog

sw.bb17:                                          ; preds = %entry
  %67 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %68 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %69 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %70 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_4x16_c(i32* %67, i8* %68, i32 %69, %struct.txfm_param* %70)
  br label %sw.epilog

sw.bb18:                                          ; preds = %entry
  %71 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %72 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %73 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %74 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_8x32_c(i32* %71, i8* %72, i32 %73, %struct.txfm_param* %74)
  br label %sw.epilog

sw.bb19:                                          ; preds = %entry
  %75 = load i32*, i32** %input.addr, align 4, !tbaa !7
  %76 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %77 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %78 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_32x8_c(i32* %75, i8* %76, i32 %77, %struct.txfm_param* %78)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb19, %sw.bb18, %sw.bb17, %sw.bb16, %sw.bb15, %sw.bb14, %sw.bb13, %sw.bb12, %sw.bb11, %sw.bb10, %sw.bb9, %sw.bb8, %sw.bb7, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm_add_c(i32* %dqcoeff, i8* %dst, i32 %stride, %struct.txfm_param* %txfm_param) #0 {
entry:
  %dqcoeff.addr = alloca i32*, align 4
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  %tx_size = alloca i8, align 1
  %tmp = alloca [4096 x i16], align 32
  %tmp_stride = alloca i32, align 4
  %w = alloca i32, align 4
  %h = alloca i32, align 4
  %r = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %c = alloca i32, align 4
  %r15 = alloca i32, align 4
  %c21 = alloca i32, align 4
  store i32* %dqcoeff, i32** %dqcoeff.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size) #4
  %0 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_size1 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %0, i32 0, i32 1
  %1 = load i8, i8* %tx_size1, align 1, !tbaa !14
  store i8 %1, i8* %tx_size, align 1, !tbaa !2
  %2 = bitcast [4096 x i16]* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8192, i8* %2) #4
  %3 = bitcast i32* %tmp_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 64, i32* %tmp_stride, align 4, !tbaa !5
  %4 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i8, i8* %tx_size, align 1, !tbaa !2
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !5
  store i32 %6, i32* %w, align 4, !tbaa !5
  %7 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i8, i8* %tx_size, align 1, !tbaa !2
  %idxprom2 = zext i8 %8 to i32
  %arrayidx3 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom2
  %9 = load i32, i32* %arrayidx3, align 4, !tbaa !5
  store i32 %9, i32* %h, align 4, !tbaa !5
  %10 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  store i32 0, i32* %r, align 4, !tbaa !5
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %entry
  %11 = load i32, i32* %r, align 4, !tbaa !5
  %12 = load i32, i32* %h, align 4, !tbaa !5
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  br label %for.end14

for.body:                                         ; preds = %for.cond
  %14 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  store i32 0, i32* %c, align 4, !tbaa !5
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %15 = load i32, i32* %c, align 4, !tbaa !5
  %16 = load i32, i32* %w, align 4, !tbaa !5
  %cmp5 = icmp slt i32 %15, %16
  br i1 %cmp5, label %for.body7, label %for.cond.cleanup6

for.cond.cleanup6:                                ; preds = %for.cond4
  store i32 5, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  br label %for.end

for.body7:                                        ; preds = %for.cond4
  %18 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %19 = load i32, i32* %r, align 4, !tbaa !5
  %20 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %mul = mul nsw i32 %19, %20
  %21 = load i32, i32* %c, align 4, !tbaa !5
  %add = add nsw i32 %mul, %21
  %arrayidx8 = getelementptr inbounds i8, i8* %18, i32 %add
  %22 = load i8, i8* %arrayidx8, align 1, !tbaa !2
  %conv = zext i8 %22 to i16
  %23 = load i32, i32* %r, align 4, !tbaa !5
  %24 = load i32, i32* %tmp_stride, align 4, !tbaa !5
  %mul9 = mul nsw i32 %23, %24
  %25 = load i32, i32* %c, align 4, !tbaa !5
  %add10 = add nsw i32 %mul9, %25
  %arrayidx11 = getelementptr inbounds [4096 x i16], [4096 x i16]* %tmp, i32 0, i32 %add10
  store i16 %conv, i16* %arrayidx11, align 2, !tbaa !15
  br label %for.inc

for.inc:                                          ; preds = %for.body7
  %26 = load i32, i32* %c, align 4, !tbaa !5
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %c, align 4, !tbaa !5
  br label %for.cond4

for.end:                                          ; preds = %for.cond.cleanup6
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %27 = load i32, i32* %r, align 4, !tbaa !5
  %inc13 = add nsw i32 %27, 1
  store i32 %inc13, i32* %r, align 4, !tbaa !5
  br label %for.cond

for.end14:                                        ; preds = %for.cond.cleanup
  %28 = load i32*, i32** %dqcoeff.addr, align 4, !tbaa !7
  %arraydecay = getelementptr inbounds [4096 x i16], [4096 x i16]* %tmp, i32 0, i32 0
  %29 = ptrtoint i16* %arraydecay to i32
  %shr = lshr i32 %29, 1
  %30 = inttoptr i32 %shr to i8*
  %31 = load i32, i32* %tmp_stride, align 4, !tbaa !5
  %32 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  call void @av1_highbd_inv_txfm_add_c(i32* %28, i8* %30, i32 %31, %struct.txfm_param* %32)
  %33 = bitcast i32* %r15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  store i32 0, i32* %r15, align 4, !tbaa !5
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc37, %for.end14
  %34 = load i32, i32* %r15, align 4, !tbaa !5
  %35 = load i32, i32* %h, align 4, !tbaa !5
  %cmp17 = icmp slt i32 %34, %35
  br i1 %cmp17, label %for.body20, label %for.cond.cleanup19

for.cond.cleanup19:                               ; preds = %for.cond16
  store i32 8, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %r15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  br label %for.end39

for.body20:                                       ; preds = %for.cond16
  %37 = bitcast i32* %c21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  store i32 0, i32* %c21, align 4, !tbaa !5
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc34, %for.body20
  %38 = load i32, i32* %c21, align 4, !tbaa !5
  %39 = load i32, i32* %w, align 4, !tbaa !5
  %cmp23 = icmp slt i32 %38, %39
  br i1 %cmp23, label %for.body26, label %for.cond.cleanup25

for.cond.cleanup25:                               ; preds = %for.cond22
  store i32 11, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %c21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %for.end36

for.body26:                                       ; preds = %for.cond22
  %41 = load i32, i32* %r15, align 4, !tbaa !5
  %42 = load i32, i32* %tmp_stride, align 4, !tbaa !5
  %mul27 = mul nsw i32 %41, %42
  %43 = load i32, i32* %c21, align 4, !tbaa !5
  %add28 = add nsw i32 %mul27, %43
  %arrayidx29 = getelementptr inbounds [4096 x i16], [4096 x i16]* %tmp, i32 0, i32 %add28
  %44 = load i16, i16* %arrayidx29, align 2, !tbaa !15
  %conv30 = trunc i16 %44 to i8
  %45 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %46 = load i32, i32* %r15, align 4, !tbaa !5
  %47 = load i32, i32* %stride.addr, align 4, !tbaa !5
  %mul31 = mul nsw i32 %46, %47
  %48 = load i32, i32* %c21, align 4, !tbaa !5
  %add32 = add nsw i32 %mul31, %48
  %arrayidx33 = getelementptr inbounds i8, i8* %45, i32 %add32
  store i8 %conv30, i8* %arrayidx33, align 1, !tbaa !2
  br label %for.inc34

for.inc34:                                        ; preds = %for.body26
  %49 = load i32, i32* %c21, align 4, !tbaa !5
  %inc35 = add nsw i32 %49, 1
  store i32 %inc35, i32* %c21, align 4, !tbaa !5
  br label %for.cond22

for.end36:                                        ; preds = %for.cond.cleanup25
  br label %for.inc37

for.inc37:                                        ; preds = %for.end36
  %50 = load i32, i32* %r15, align 4, !tbaa !5
  %inc38 = add nsw i32 %50, 1
  store i32 %inc38, i32* %r15, align 4, !tbaa !5
  br label %for.cond16

for.end39:                                        ; preds = %for.cond.cleanup19
  %51 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast i32* %tmp_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast [4096 x i16]* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8192, i8* %54) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inverse_transform_block(%struct.macroblockd* %xd, i32* %dqcoeff, i32 %plane, i8 zeroext %tx_type, i8 zeroext %tx_size, i8* %dst, i32 %stride, i32 %eob, i32 %reduced_tx_set) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %dqcoeff.addr = alloca i32*, align 4
  %plane.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %eob.addr = alloca i32, align 4
  %reduced_tx_set.addr = alloca i32, align 4
  %txfm_param = alloca %struct.txfm_param, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  store i32* %dqcoeff, i32** %dqcoeff.addr, align 4, !tbaa !7
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !5
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !2
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !5
  store i32 %eob, i32* %eob.addr, align 4, !tbaa !5
  store i32 %reduced_tx_set, i32* %reduced_tx_set.addr, align 4, !tbaa !5
  %0 = load i32, i32* %eob.addr, align 4, !tbaa !5
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %struct.txfm_param* %txfm_param to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %1) #4
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  %3 = load i32, i32* %plane.addr, align 4, !tbaa !5
  %4 = load i8, i8* %tx_size.addr, align 1, !tbaa !2
  %5 = load i8, i8* %tx_type.addr, align 1, !tbaa !2
  %6 = load i32, i32* %eob.addr, align 4, !tbaa !5
  %7 = load i32, i32* %reduced_tx_set.addr, align 4, !tbaa !5
  call void @init_txfm_param(%struct.macroblockd* %2, i32 %3, i8 zeroext %4, i8 zeroext %5, i32 %6, i32 %7, %struct.txfm_param* %txfm_param)
  %is_hbd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %txfm_param, i32 0, i32 4
  %8 = load i32, i32* %is_hbd, align 4, !tbaa !17
  %tobool1 = icmp ne i32 %8, 0
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %9 = load i32*, i32** %dqcoeff.addr, align 4, !tbaa !7
  %10 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !5
  call void @av1_highbd_inv_txfm_add_c(i32* %9, i8* %10, i32 %11, %struct.txfm_param* %txfm_param)
  br label %if.end3

if.else:                                          ; preds = %if.end
  %12 = load i32*, i32** %dqcoeff.addr, align 4, !tbaa !7
  %13 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %14 = load i32, i32* %stride.addr, align 4, !tbaa !5
  call void @av1_inv_txfm_add_c(i32* %12, i8* %13, i32 %14, %struct.txfm_param* %txfm_param)
  br label %if.end3

if.end3:                                          ; preds = %if.else, %if.then2
  %15 = bitcast %struct.txfm_param* %txfm_param to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %15) #4
  br label %return

return:                                           ; preds = %if.end3, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @init_txfm_param(%struct.macroblockd* %xd, i32 %plane, i8 zeroext %tx_size, i8 zeroext %tx_type, i32 %eob, i32 %reduced_tx_set, %struct.txfm_param* %txfm_param) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %tx_type.addr = alloca i8, align 1
  %eob.addr = alloca i32, align 4
  %reduced_tx_set.addr = alloca i32, align 4
  %txfm_param.addr = alloca %struct.txfm_param*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !5
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !2
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !2
  store i32 %eob, i32* %eob.addr, align 4, !tbaa !5
  store i32 %reduced_tx_set, i32* %reduced_tx_set.addr, align 4, !tbaa !5
  store %struct.txfm_param* %txfm_param, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %0 = load i32, i32* %plane.addr, align 4, !tbaa !5
  %1 = load i8, i8* %tx_type.addr, align 1, !tbaa !2
  %2 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_type1 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %2, i32 0, i32 0
  store i8 %1, i8* %tx_type1, align 4, !tbaa !13
  %3 = load i8, i8* %tx_size.addr, align 1, !tbaa !2
  %4 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_size2 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %4, i32 0, i32 1
  store i8 %3, i8* %tx_size2, align 1, !tbaa !14
  %5 = load i32, i32* %eob.addr, align 4, !tbaa !5
  %6 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %eob3 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %6, i32 0, i32 6
  store i32 %5, i32* %eob3, align 4, !tbaa !9
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  %lossless = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 43
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 6
  %9 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %9, i32 0
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !7
  %segment_id = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %10, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id, align 4
  %bf.lshr = lshr i8 %bf.load, 1
  %bf.clear = and i8 %bf.lshr, 7
  %idxprom = zext i8 %bf.clear to i32
  %arrayidx4 = getelementptr inbounds [8 x i32], [8 x i32]* %lossless, i32 0, i32 %idxprom
  %11 = load i32, i32* %arrayidx4, align 4, !tbaa !5
  %12 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %lossless5 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %12, i32 0, i32 2
  store i32 %11, i32* %lossless5, align 4, !tbaa !12
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  %bd = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %13, i32 0, i32 41
  %14 = load i32, i32* %bd, align 4, !tbaa !24
  %15 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %bd6 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %15, i32 0, i32 3
  store i32 %14, i32* %bd6, align 4, !tbaa !11
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  %call = call i32 @is_cur_buf_hbd(%struct.macroblockd* %16)
  %17 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %is_hbd = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %17, i32 0, i32 4
  store i32 %call, i32* %is_hbd, align 4, !tbaa !17
  %18 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_size7 = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %18, i32 0, i32 1
  %19 = load i8, i8* %tx_size7, align 1, !tbaa !14
  %20 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  %mi8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %20, i32 0, i32 6
  %21 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi8, align 4, !tbaa !18
  %arrayidx9 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %21, i32 0
  %22 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx9, align 4, !tbaa !7
  %call10 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %22)
  %23 = load i32, i32* %reduced_tx_set.addr, align 4, !tbaa !5
  %call11 = call zeroext i8 @av1_get_ext_tx_set_type(i8 zeroext %19, i32 %call10, i32 %23)
  %24 = load %struct.txfm_param*, %struct.txfm_param** %txfm_param.addr, align 4, !tbaa !7
  %tx_set_type = getelementptr inbounds %struct.txfm_param, %struct.txfm_param* %24, i32 0, i32 5
  store i8 %call11, i8* %tx_set_type, align 4, !tbaa !25
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_cur_buf_hbd(%struct.macroblockd* %xd) #3 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !7
  %cur_buf = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 22
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %cur_buf, align 4, !tbaa !26
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 26
  %2 = load i32, i32* %flags, align 4, !tbaa !27
  %and = and i32 %2, 8
  %tobool = icmp ne i32 %and, 0
  %3 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_get_ext_tx_set_type(i8 zeroext %tx_size, i32 %is_inter, i32 %use_reduced_set) #3 {
entry:
  %retval = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %is_inter.addr = alloca i32, align 4
  %use_reduced_set.addr = alloca i32, align 4
  %tx_size_sqr_up = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %tx_size_sqr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !2
  store i32 %is_inter, i32* %is_inter.addr, align 4, !tbaa !5
  store i32 %use_reduced_set, i32* %use_reduced_set.addr, align 4, !tbaa !5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size_sqr_up) #4
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !2
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_up_map, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !2
  store i8 %1, i8* %tx_size_sqr_up, align 1, !tbaa !2
  %2 = load i8, i8* %tx_size_sqr_up, align 1, !tbaa !2
  %conv = zext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8 0, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load i8, i8* %tx_size_sqr_up, align 1, !tbaa !2
  %conv2 = zext i8 %3 to i32
  %cmp3 = icmp eq i32 %conv2, 3
  br i1 %cmp3, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %4 = load i32, i32* %is_inter.addr, align 4, !tbaa !5
  %tobool = icmp ne i32 %4, 0
  %5 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  %conv6 = trunc i32 %cond to i8
  store i8 %conv6, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  %6 = load i32, i32* %use_reduced_set.addr, align 4, !tbaa !5
  %tobool8 = icmp ne i32 %6, 0
  br i1 %tobool8, label %if.then9, label %if.end13

if.then9:                                         ; preds = %if.end7
  %7 = load i32, i32* %is_inter.addr, align 4, !tbaa !5
  %tobool10 = icmp ne i32 %7, 0
  %8 = zext i1 %tobool10 to i64
  %cond11 = select i1 %tobool10, i32 1, i32 2
  %conv12 = trunc i32 %cond11 to i8
  store i8 %conv12, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.end7
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size_sqr) #4
  %9 = load i8, i8* %tx_size.addr, align 1, !tbaa !2
  %idxprom14 = zext i8 %9 to i32
  %arrayidx15 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_map, i32 0, i32 %idxprom14
  %10 = load i8, i8* %arrayidx15, align 1, !tbaa !2
  store i8 %10, i8* %tx_size_sqr, align 1, !tbaa !2
  %11 = load i32, i32* %is_inter.addr, align 4, !tbaa !5
  %arrayidx16 = getelementptr inbounds [2 x [2 x i8]], [2 x [2 x i8]]* @av1_ext_tx_set_lookup, i32 0, i32 %11
  %12 = load i8, i8* %tx_size_sqr, align 1, !tbaa !2
  %conv17 = zext i8 %12 to i32
  %cmp18 = icmp eq i32 %conv17, 2
  %conv19 = zext i1 %cmp18 to i32
  %arrayidx20 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx16, i32 0, i32 %conv19
  %13 = load i8, i8* %arrayidx20, align 1, !tbaa !2
  store i8 %13, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size_sqr) #4
  br label %cleanup

cleanup:                                          ; preds = %if.end13, %if.then9, %if.then5, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size_sqr_up) #4
  %14 = load i8, i8* %retval, align 1
  ret i8 %14
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_block(%struct.MB_MODE_INFO* %mbmi) #3 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !7
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !7
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !7
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !2
  %conv = sext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_intrabc_block(%struct.MB_MODE_INFO* %mbmi) #3 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !7
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !7
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  ret i32 %conv
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"int", !3, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"any pointer", !3, i64 0}
!9 = !{!10, !6, i64 20}
!10 = !{!"txfm_param", !3, i64 0, !3, i64 1, !6, i64 4, !6, i64 8, !6, i64 12, !3, i64 16, !6, i64 20}
!11 = !{!10, !6, i64 8}
!12 = !{!10, !6, i64 4}
!13 = !{!10, !3, i64 0}
!14 = !{!10, !3, i64 1}
!15 = !{!16, !16, i64 0}
!16 = !{!"short", !3, i64 0}
!17 = !{!10, !6, i64 12}
!18 = !{!19, !8, i64 4084}
!19 = !{!"macroblockd", !6, i64 0, !6, i64 4, !6, i64 8, !20, i64 12, !3, i64 16, !21, i64 4060, !8, i64 4084, !20, i64 4088, !20, i64 4089, !20, i64 4090, !20, i64 4091, !8, i64 4092, !8, i64 4096, !8, i64 4100, !8, i64 4104, !8, i64 4108, !6, i64 4112, !6, i64 4116, !6, i64 4120, !6, i64 4124, !6, i64 4128, !3, i64 4132, !8, i64 4140, !3, i64 4144, !3, i64 4156, !8, i64 4252, !3, i64 4256, !8, i64 4288, !8, i64 4292, !3, i64 4296, !3, i64 4336, !3, i64 4432, !3, i64 4468, !3, i64 4469, !3, i64 4470, !3, i64 4500, !3, i64 6356, !3, i64 6820, !3, i64 6821, !8, i64 6832, !6, i64 6836, !3, i64 6840, !3, i64 6872, !6, i64 6904, !6, i64 6908, !8, i64 6912, !8, i64 6916, !6, i64 6920, !6, i64 6924, !3, i64 6928, !3, i64 6929, !3, i64 6933, !3, i64 6944, !3, i64 39712, !22, i64 39720, !23, i64 43980, !3, i64 43992, !3, i64 43998, !3, i64 44004, !8, i64 44008, !3, i64 44012}
!20 = !{!"_Bool", !3, i64 0}
!21 = !{!"TileInfo", !6, i64 0, !6, i64 4, !6, i64 8, !6, i64 12, !6, i64 16, !6, i64 20}
!22 = !{!"cfl_ctx", !3, i64 0, !3, i64 2048, !3, i64 4096, !6, i64 4104, !3, i64 4108, !6, i64 4236, !6, i64 4240, !6, i64 4244, !6, i64 4248, !6, i64 4252, !6, i64 4256}
!23 = !{!"dist_wtd_comp_params", !6, i64 0, !6, i64 4, !6, i64 8}
!24 = !{!19, !6, i64 6836}
!25 = !{!10, !3, i64 16}
!26 = !{!19, !8, i64 4140}
!27 = !{!28, !6, i64 140}
!28 = !{!"yv12_buffer_config", !3, i64 0, !3, i64 8, !3, i64 16, !3, i64 24, !3, i64 32, !3, i64 40, !6, i64 52, !3, i64 56, !8, i64 68, !6, i64 72, !8, i64 76, !29, i64 80, !6, i64 84, !29, i64 88, !6, i64 92, !6, i64 96, !6, i64 100, !3, i64 104, !3, i64 108, !3, i64 112, !3, i64 116, !3, i64 120, !3, i64 124, !6, i64 128, !6, i64 132, !6, i64 136, !6, i64 140, !8, i64 144}
!29 = !{!"long", !3, i64 0}
