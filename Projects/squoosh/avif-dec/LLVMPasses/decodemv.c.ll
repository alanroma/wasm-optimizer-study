; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/decodemv.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/decodemv.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.mv = type { i16, i16 }
%struct.wedge_params_type = type { i32, %struct.wedge_code_type*, i8*, [16 x i8*]* }
%struct.wedge_code_type = type { i8, i32, i32 }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.aom_reader = type { i8*, i8*, %struct.od_ec_dec, %struct.Accounting*, i8 }
%struct.od_ec_dec = type { i8*, i32, i8*, i8*, i32, i16, i16 }
%struct.Accounting = type { %struct.AccountingSymbols, i32, [1021 x i16], %struct.AccountingSymbolContext, i32 }
%struct.AccountingSymbols = type { %struct.AccountingSymbol*, i32, i32, i32, %struct.AccountingDictionary }
%struct.AccountingSymbol = type { %struct.AccountingSymbolContext, i32, i32, i32 }
%struct.AccountingDictionary = type { [256 x i8*], i32 }
%struct.AccountingSymbolContext = type { i16, i16 }
%struct.AV1Decoder = type { %struct.macroblockd, %struct.AV1Common, %struct.AVxWorker, %struct.AV1LfSyncData, %struct.AV1LrSyncData, %struct.AV1LrStruct, %struct.AVxWorker*, i32, %struct.DecWorkerData*, %struct.ThreadData, %struct.TileDataDec*, i32, [64 x [64 x %struct.TileBufferDec]], %struct.AV1DecTileMTData, i32, [4 x %struct.RefCntBuffer*], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.Accounting, i32, i32, i32, i32, i32, i32, i32, i32, %struct.DataBuffer, i32, i32, i32, i32, i32, i32, %struct.EXTERNAL_REFERENCES, %struct.yv12_buffer_config, %struct.CB_BUFFER*, i32, i32, %struct.AV1DecRowMTInfo, %struct.aom_metadata_array*, i32, i32, i32, i32, [8 x i32] }
%struct.AVxWorker = type { %struct.AVxWorkerImpl*, i32, i8*, i32 (i8*, i8*)*, i8*, i8*, i32 }
%struct.AVxWorkerImpl = type opaque
%struct.AV1LfSyncData = type { [3 x i32*], i32, i32, %struct.LoopFilterWorkerData*, i32, %struct.AV1LfMTInfo*, i32, i32 }
%struct.LoopFilterWorkerData = type { %struct.yv12_buffer_config*, %struct.AV1Common*, [3 x %struct.macroblockd_plane], %struct.macroblockd* }
%struct.AV1LfMTInfo = type { i32, i32, i32 }
%struct.AV1LrSyncData = type { [3 x i32*], i32, i32, i32, i32, %struct.LoopRestorationWorkerData*, %struct.AV1LrMTInfo*, i32, i32 }
%struct.LoopRestorationWorkerData = type { i32*, i8*, i8* }
%struct.AV1LrMTInfo = type { i32, i32, i32, i32, i32, i32, i32 }
%struct.AV1LrStruct = type { void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, [3 x %struct.FilterFrameCtxt], %struct.yv12_buffer_config*, %struct.yv12_buffer_config* }
%struct.RestorationTileLimits = type { i32, i32, i32, i32 }
%struct.AV1PixelRect = type { i32, i32, i32, i32 }
%struct.FilterFrameCtxt = type { %struct.RestorationInfo*, i32, i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.AV1PixelRect }
%struct.DecWorkerData = type { %struct.ThreadData*, i8*, %struct.aom_internal_error_info }
%struct.ThreadData = type { %struct.macroblockd, %struct.CB_BUFFER, %struct.aom_reader*, [2 x i8*], i32, i32, i16*, [2 x i8*], void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, i8)*, void (%struct.AV1Common*, %struct.macroblockd*)*, [8 x i8] }
%struct.CB_BUFFER = type { [3 x [16384 x i32]], [3 x [1024 x %struct.eob_info]], [2 x [16384 x i8]] }
%struct.TileDataDec = type { %struct.TileInfo, %struct.aom_reader, %struct.frame_contexts, %struct.AV1DecRowMTSyncData }
%struct.AV1DecRowMTSyncData = type { i32, i32*, i32, i32, i32, i32, i32, i32 }
%struct.TileBufferDec = type { i8*, i32 }
%struct.AV1DecTileMTData = type { %struct.TileJobsDec*, i32, i32, i32, i32 }
%struct.TileJobsDec = type { %struct.TileBufferDec*, %struct.TileDataDec* }
%struct.DataBuffer = type { i8*, i32 }
%struct.EXTERNAL_REFERENCES = type { [128 x %struct.yv12_buffer_config], i32 }
%struct.AV1DecRowMTInfo = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.InterpFilters = type { i16, i16 }
%struct.fullpel_mv = type { i16, i16 }

@txsize_sqr_map = internal constant [19 x i8] c"\00\01\02\03\04\00\00\01\01\02\02\03\03\00\00\01\01\02\02", align 16
@av1_num_ext_tx_set = internal constant [6 x i32] [i32 1, i32 2, i32 5, i32 7, i32 12, i32 16], align 16
@__func__.av1_read_tx_type = private unnamed_addr constant [17 x i8] c"av1_read_tx_type\00", align 1
@fimode_to_intradir = internal constant [5 x i8] c"\00\01\02\06\00", align 1
@txsize_sqr_up_map = internal constant [19 x i8] c"\00\01\02\03\04\01\01\02\02\03\03\04\04\02\02\03\03\04\04", align 16
@av1_ext_tx_set_lookup = internal constant [2 x [2 x i8]] [[2 x i8] c"\03\02", [2 x i8] c"\05\04"], align 1
@ext_tx_set_index = internal constant [2 x [6 x i32]] [[6 x i32] [i32 0, i32 -1, i32 2, i32 1, i32 -1, i32 -1], [6 x i32] [i32 0, i32 3, i32 -1, i32 -1, i32 2, i32 1]], align 16
@av1_ext_tx_inv = internal constant <{ [16 x i32], <{ i32, [15 x i32] }>, <{ i32, i32, i32, i32, i32, [11 x i32] }>, <{ i32, i32, i32, i32, i32, i32, i32, [9 x i32] }>, [16 x i32], [16 x i32] }> <{ [16 x i32] zeroinitializer, <{ i32, [15 x i32] }> <{ i32 9, [15 x i32] zeroinitializer }>, <{ i32, i32, i32, i32, i32, [11 x i32] }> <{ i32 9, i32 0, i32 3, i32 1, i32 2, [11 x i32] zeroinitializer }>, <{ i32, i32, i32, i32, i32, i32, i32, [9 x i32] }> <{ i32 9, i32 0, i32 10, i32 11, i32 3, i32 1, i32 2, [9 x i32] zeroinitializer }>, [16 x i32] [i32 9, i32 10, i32 11, i32 0, i32 1, i32 2, i32 4, i32 5, i32 3, i32 6, i32 7, i32 8, i32 0, i32 0, i32 0, i32 0], [16 x i32] [i32 9, i32 10, i32 11, i32 12, i32 13, i32 14, i32 15, i32 0, i32 1, i32 2, i32 4, i32 5, i32 3, i32 6, i32 7, i32 8] }>, align 16
@update_cdf.nsymbs2speed = internal constant [17 x i32] [i32 0, i32 0, i32 1, i32 1, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2], align 16
@mi_size_wide = internal constant [22 x i8] c"\01\01\02\02\02\04\04\04\08\08\08\10\10\10  \01\04\02\08\04\10", align 16
@mi_size_high = internal constant [22 x i8] c"\01\02\01\02\04\02\04\08\04\08\10\08\10 \10 \04\01\08\02\10\04", align 16
@__func__.read_segment_id = private unnamed_addr constant [16 x i8] c"read_segment_id\00", align 1
@.str = private unnamed_addr constant [22 x i8] c"Corrupted segment_ids\00", align 1
@__func__.read_skip = private unnamed_addr constant [10 x i8] c"read_skip\00", align 1
@__func__.read_cdef = private unnamed_addr constant [10 x i8] c"read_cdef\00", align 1
@__func__.read_delta_qindex = private unnamed_addr constant [18 x i8] c"read_delta_qindex\00", align 1
@__func__.read_delta_lflevel = private unnamed_addr constant [19 x i8] c"read_delta_lflevel\00", align 1
@__func__.read_intrabc_info = private unnamed_addr constant [18 x i8] c"read_intrabc_info\00", align 1
@.str.2 = private unnamed_addr constant [19 x i8] c"Invalid intrabc dv\00", align 1
@kZeroMv = internal constant %struct.mv zeroinitializer, align 2
@__func__.read_mv = private unnamed_addr constant [8 x i8] c"read_mv\00", align 1
@__func__.read_mv_component = private unnamed_addr constant [18 x i8] c"read_mv_component\00", align 1
@block_size_wide = internal constant [22 x i8] c"\04\04\08\08\08\10\10\10   @@@\80\80\04\10\08 \10@", align 16
@block_size_high = internal constant [22 x i8] c"\04\08\04\08\10\08\10 \10 @ @\80@\80\10\04 \08@\10", align 16
@__func__.read_intra_mode = private unnamed_addr constant [16 x i8] c"read_intra_mode\00", align 1
@intra_mode_context = internal constant [13 x i32] [i32 0, i32 1, i32 2, i32 3, i32 4, i32 4, i32 4, i32 4, i32 3, i32 0, i32 1, i32 2, i32 0], align 16
@__func__.read_angle_delta = private unnamed_addr constant [17 x i8] c"read_angle_delta\00", align 1
@__func__.read_intra_mode_uv = private unnamed_addr constant [19 x i8] c"read_intra_mode_uv\00", align 1
@ss_size_lookup = internal constant [22 x [2 x [2 x i8]]] [[2 x [2 x i8]] zeroinitializer, [2 x [2 x i8]] [[2 x i8] c"\01\00", [2 x i8] c"\FF\00"], [2 x [2 x i8]] [[2 x i8] c"\02\FF", [2 x i8] zeroinitializer], [2 x [2 x i8]] [[2 x i8] c"\03\02", [2 x i8] c"\01\00"], [2 x [2 x i8]] [[2 x i8] c"\04\03", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\05\FF", [2 x i8] c"\03\02"], [2 x [2 x i8]] [[2 x i8] c"\06\05", [2 x i8] c"\04\03"], [2 x [2 x i8]] [[2 x i8] c"\07\06", [2 x i8] c"\FF\04"], [2 x [2 x i8]] [[2 x i8] c"\08\FF", [2 x i8] c"\06\05"], [2 x [2 x i8]] [[2 x i8] c"\09\08", [2 x i8] c"\07\06"], [2 x [2 x i8]] [[2 x i8] c"\0A\09", [2 x i8] c"\FF\07"], [2 x [2 x i8]] [[2 x i8] c"\0B\FF", [2 x i8] c"\09\08"], [2 x [2 x i8]] [[2 x i8] c"\0C\0B", [2 x i8] c"\0A\09"], [2 x [2 x i8]] [[2 x i8] c"\0D\0C", [2 x i8] c"\FF\0A"], [2 x [2 x i8]] [[2 x i8] c"\0E\FF", [2 x i8] c"\0C\0B"], [2 x [2 x i8]] [[2 x i8] c"\0F\0E", [2 x i8] c"\0D\0C"], [2 x [2 x i8]] [[2 x i8] c"\10\01", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\11\FF", [2 x i8] c"\02\02"], [2 x [2 x i8]] [[2 x i8] c"\12\04", [2 x i8] c"\FF\10"], [2 x [2 x i8]] [[2 x i8] c"\13\FF", [2 x i8] c"\05\11"], [2 x [2 x i8]] [[2 x i8] c"\14\07", [2 x i8] c"\FF\12"], [2 x [2 x i8]] [[2 x i8] c"\15\FF", [2 x i8] c"\08\13"]], align 16
@.str.3 = private unnamed_addr constant [10 x i8] c"cfl:signs\00", align 1
@.str.4 = private unnamed_addr constant [12 x i8] c"cfl:alpha_u\00", align 1
@.str.5 = private unnamed_addr constant [12 x i8] c"cfl:alpha_v\00", align 1
@get_uv_mode.uv2y = internal constant [16 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B\0C\00\19\19", align 16
@__func__.read_palette_mode_info = private unnamed_addr constant [23 x i8] c"read_palette_mode_info\00", align 1
@num_pels_log2_lookup = internal constant [22 x i8] c"\04\05\05\06\07\07\08\09\09\0A\0B\0B\0C\0D\0D\0E\06\06\08\08\0A\0A", align 16
@__func__.read_palette_colors_y = private unnamed_addr constant [22 x i8] c"read_palette_colors_y\00", align 1
@__func__.read_palette_colors_uv = private unnamed_addr constant [23 x i8] c"read_palette_colors_uv\00", align 1
@__func__.read_filter_intra_mode_info = private unnamed_addr constant [28 x i8] c"read_filter_intra_mode_info\00", align 1
@__func__.read_inter_segment_id = private unnamed_addr constant [22 x i8] c"read_inter_segment_id\00", align 1
@__func__.read_skip_mode = private unnamed_addr constant [15 x i8] c"read_skip_mode\00", align 1
@__func__.read_is_inter_block = private unnamed_addr constant [20 x i8] c"read_is_inter_block\00", align 1
@.str.6 = private unnamed_addr constant [48 x i8] c"Prediction mode %d invalid with ref frame %d %d\00", align 1
@size_group_lookup = internal constant [22 x i8] c"\00\00\00\01\01\01\02\02\02\03\03\03\03\03\03\03\00\00\01\01\02\02", align 16
@__func__.read_inter_block_mode_info = private unnamed_addr constant [27 x i8] c"read_inter_block_mode_info\00", align 1
@__func__.read_ref_frames = private unnamed_addr constant [16 x i8] c"read_ref_frames\00", align 1
@__func__.read_block_reference_mode = private unnamed_addr constant [26 x i8] c"read_block_reference_mode\00", align 1
@__func__.read_comp_reference_type = private unnamed_addr constant [25 x i8] c"read_comp_reference_type\00", align 1
@comp_ref0.lut = internal constant [9 x i8] c"\01\01\01\05\02\02\03\05\06", align 1
@comp_ref1.lut = internal constant [9 x i8] c"\02\03\04\07\03\04\04\06\07", align 1
@compound_mode_ctx_map = internal global [3 x [5 x i16]] [[5 x i16] [i16 0, i16 1, i16 1, i16 1, i16 1], [5 x i16] [i16 1, i16 2, i16 3, i16 4, i16 4], [5 x i16] [i16 4, i16 4, i16 5, i16 6, i16 7]], align 16
@__func__.read_inter_compound_mode = private unnamed_addr constant [25 x i8] c"read_inter_compound_mode\00", align 1
@__func__.read_inter_mode = private unnamed_addr constant [16 x i8] c"read_inter_mode\00", align 1
@__func__.read_drl_idx = private unnamed_addr constant [13 x i8] c"read_drl_idx\00", align 1
@compound_ref0_mode.lut = internal constant [25 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B\0C\0D\0E\0F\10\0D\0E\0D\10\0E\10\0F\10", align 16
@compound_ref1_mode.lut = internal constant [25 x i8] c"\19\19\19\19\19\19\19\19\19\19\19\19\19\19\19\19\19\0D\0E\10\0D\10\0E\0F\10", align 16
@__func__.read_interintra_mode = private unnamed_addr constant [21 x i8] c"read_interintra_mode\00", align 1
@av1_wedge_params_lookup = external constant [22 x %struct.wedge_params_type], align 16
@__func__.read_motion_mode = private unnamed_addr constant [17 x i8] c"read_motion_mode\00", align 1
@__func__.read_mb_interp_filter = private unnamed_addr constant [22 x i8] c"read_mb_interp_filter\00", align 1

; Function Attrs: nounwind
define hidden i32 @av1_neg_deinterleave(i32 %diff, i32 %ref, i32 %max) #0 {
entry:
  %retval = alloca i32, align 4
  %diff.addr = alloca i32, align 4
  %ref.addr = alloca i32, align 4
  %max.addr = alloca i32, align 4
  store i32 %diff, i32* %diff.addr, align 4, !tbaa !2
  store i32 %ref, i32* %ref.addr, align 4, !tbaa !2
  store i32 %max, i32* %max.addr, align 4, !tbaa !2
  %0 = load i32, i32* %ref.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %diff.addr, align 4, !tbaa !2
  store i32 %1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %ref.addr, align 4, !tbaa !2
  %3 = load i32, i32* %max.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %3, 1
  %cmp = icmp sge i32 %2, %sub
  br i1 %cmp, label %if.then1, label %if.end4

if.then1:                                         ; preds = %if.end
  %4 = load i32, i32* %max.addr, align 4, !tbaa !2
  %5 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %sub2 = sub nsw i32 %4, %5
  %sub3 = sub nsw i32 %sub2, 1
  store i32 %sub3, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %6 = load i32, i32* %ref.addr, align 4, !tbaa !2
  %mul = mul nsw i32 2, %6
  %7 = load i32, i32* %max.addr, align 4, !tbaa !2
  %cmp5 = icmp slt i32 %mul, %7
  br i1 %cmp5, label %if.then6, label %if.else16

if.then6:                                         ; preds = %if.end4
  %8 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %9 = load i32, i32* %ref.addr, align 4, !tbaa !2
  %mul7 = mul nsw i32 2, %9
  %cmp8 = icmp sle i32 %8, %mul7
  br i1 %cmp8, label %if.then9, label %if.end15

if.then9:                                         ; preds = %if.then6
  %10 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %and = and i32 %10, 1
  %tobool10 = icmp ne i32 %and, 0
  br i1 %tobool10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.then9
  %11 = load i32, i32* %ref.addr, align 4, !tbaa !2
  %12 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %add = add nsw i32 %12, 1
  %shr = ashr i32 %add, 1
  %add12 = add nsw i32 %11, %shr
  store i32 %add12, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %if.then9
  %13 = load i32, i32* %ref.addr, align 4, !tbaa !2
  %14 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %shr13 = ashr i32 %14, 1
  %sub14 = sub nsw i32 %13, %shr13
  store i32 %sub14, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then6
  %15 = load i32, i32* %diff.addr, align 4, !tbaa !2
  store i32 %15, i32* %retval, align 4
  br label %return

if.else16:                                        ; preds = %if.end4
  %16 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %17 = load i32, i32* %max.addr, align 4, !tbaa !2
  %18 = load i32, i32* %ref.addr, align 4, !tbaa !2
  %sub17 = sub nsw i32 %17, %18
  %sub18 = sub nsw i32 %sub17, 1
  %mul19 = mul nsw i32 2, %sub18
  %cmp20 = icmp sle i32 %16, %mul19
  br i1 %cmp20, label %if.then21, label %if.end31

if.then21:                                        ; preds = %if.else16
  %19 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %and22 = and i32 %19, 1
  %tobool23 = icmp ne i32 %and22, 0
  br i1 %tobool23, label %if.then24, label %if.else28

if.then24:                                        ; preds = %if.then21
  %20 = load i32, i32* %ref.addr, align 4, !tbaa !2
  %21 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %add25 = add nsw i32 %21, 1
  %shr26 = ashr i32 %add25, 1
  %add27 = add nsw i32 %20, %shr26
  store i32 %add27, i32* %retval, align 4
  br label %return

if.else28:                                        ; preds = %if.then21
  %22 = load i32, i32* %ref.addr, align 4, !tbaa !2
  %23 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %shr29 = ashr i32 %23, 1
  %sub30 = sub nsw i32 %22, %shr29
  store i32 %sub30, i32* %retval, align 4
  br label %return

if.end31:                                         ; preds = %if.else16
  %24 = load i32, i32* %max.addr, align 4, !tbaa !2
  %25 = load i32, i32* %diff.addr, align 4, !tbaa !2
  %add32 = add nsw i32 %25, 1
  %sub33 = sub nsw i32 %24, %add32
  store i32 %sub33, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end31, %if.else28, %if.then24, %if.end15, %if.else, %if.then11, %if.then1, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: nounwind
define hidden void @av1_read_tx_type(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %blk_row, i32 %blk_col, i8 zeroext %tx_size, %struct.aom_reader* %r) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %blk_row.addr = alloca i32, align 4
  %blk_col.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %r.addr = alloca %struct.aom_reader*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %tx_type = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %qindex = alloca i32, align 4
  %inter_block = alloca i32, align 4
  %tx_set_type = alloca i8, align 1
  %eset = alloca i32, align 4
  %square_tx_size = alloca i8, align 1
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %intra_mode = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32 %blk_row, i32* %blk_row.addr, align 4, !tbaa !2
  store i32 %blk_col, i32* %blk_col.addr, align 4, !tbaa !2
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %4 = bitcast i8** %tx_type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tx_type_map = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 15
  %6 = load i8*, i8** %tx_type_map, align 4, !tbaa !15
  %7 = load i32, i32* %blk_row.addr, align 4, !tbaa !2
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tx_type_map_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 16
  %9 = load i32, i32* %tx_type_map_stride, align 16, !tbaa !16
  %mul = mul nsw i32 %7, %9
  %10 = load i32, i32* %blk_col.addr, align 4, !tbaa !2
  %add = add nsw i32 %mul, %10
  %arrayidx1 = getelementptr inbounds i8, i8* %6, i32 %add
  store i8* %arrayidx1, i8** %tx_type, align 4, !tbaa !6
  %11 = load i8*, i8** %tx_type, align 4, !tbaa !6
  store i8 0, i8* %11, align 1, !tbaa !8
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %12, i32 0, i32 14
  %13 = load i8, i8* %skip, align 4, !tbaa !17
  %conv = sext i8 %13 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 24
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %15, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id, align 4
  %bf.lshr = lshr i8 %bf.load, 1
  %bf.clear = and i8 %bf.lshr, 7
  %conv2 = zext i8 %bf.clear to i32
  %call = call i32 @segfeature_active(%struct.segmentation* %seg, i32 %conv2, i8 zeroext 6)
  %tobool3 = icmp ne i32 %call, 0
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup67

if.end:                                           ; preds = %lor.lhs.false
  %16 = bitcast i32* %qindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %qindex4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %17, i32 0, i32 42
  %18 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id5 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %18, i32 0, i32 26
  %bf.load6 = load i8, i8* %segment_id5, align 4
  %bf.lshr7 = lshr i8 %bf.load6, 1
  %bf.clear8 = and i8 %bf.lshr7, 7
  %idxprom = zext i8 %bf.clear8 to i32
  %arrayidx9 = getelementptr inbounds [8 x i32], [8 x i32]* %qindex4, i32 0, i32 %idxprom
  %19 = load i32, i32* %arrayidx9, align 4, !tbaa !2
  store i32 %19, i32* %qindex, align 4, !tbaa !2
  %20 = load i32, i32* %qindex, align 4, !tbaa !2
  %cmp = icmp eq i32 %20, 0
  br i1 %cmp, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.end
  %21 = bitcast i32* %inter_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %call13 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %22)
  store i32 %call13, i32* %inter_block, align 4, !tbaa !2
  %23 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %24 = load i32, i32* %inter_block, align 4, !tbaa !2
  %25 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %25, i32 0, i32 21
  %reduced_tx_set_used = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 9
  %26 = load i8, i8* %reduced_tx_set_used, align 1, !tbaa !24, !range !46
  %tobool14 = trunc i8 %26 to i1
  %conv15 = zext i1 %tobool14 to i32
  %call16 = call i32 @get_ext_tx_types(i8 zeroext %23, i32 %24, i32 %conv15)
  %cmp17 = icmp sgt i32 %call16, 1
  br i1 %cmp17, label %if.then19, label %if.end66

if.then19:                                        ; preds = %if.end12
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_set_type) #6
  %27 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %28 = load i32, i32* %inter_block, align 4, !tbaa !2
  %29 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features20 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %29, i32 0, i32 21
  %reduced_tx_set_used21 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features20, i32 0, i32 9
  %30 = load i8, i8* %reduced_tx_set_used21, align 1, !tbaa !24, !range !46
  %tobool22 = trunc i8 %30 to i1
  %conv23 = zext i1 %tobool22 to i32
  %call24 = call zeroext i8 @av1_get_ext_tx_set_type(i8 zeroext %27, i32 %28, i32 %conv23)
  store i8 %call24, i8* %tx_set_type, align 1, !tbaa !8
  %31 = bitcast i32* %eset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %33 = load i32, i32* %inter_block, align 4, !tbaa !2
  %34 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features25 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %34, i32 0, i32 21
  %reduced_tx_set_used26 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features25, i32 0, i32 9
  %35 = load i8, i8* %reduced_tx_set_used26, align 1, !tbaa !24, !range !46
  %tobool27 = trunc i8 %35 to i1
  %conv28 = zext i1 %tobool27 to i32
  %call29 = call i32 @get_ext_tx_set(i8 zeroext %32, i32 %33, i32 %conv28)
  store i32 %call29, i32* %eset, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %square_tx_size) #6
  %36 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom30 = zext i8 %36 to i32
  %arrayidx31 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_map, i32 0, i32 %idxprom30
  %37 = load i8, i8* %arrayidx31, align 1, !tbaa !8
  store i8 %37, i8* %square_tx_size, align 1, !tbaa !8
  %38 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  %39 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %39, i32 0, i32 40
  %40 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %40, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %41 = load i32, i32* %inter_block, align 4, !tbaa !2
  %tobool32 = icmp ne i32 %41, 0
  br i1 %tobool32, label %if.then33, label %if.else

if.then33:                                        ; preds = %if.then19
  %42 = load i8, i8* %tx_set_type, align 1, !tbaa !8
  %idxprom34 = zext i8 %42 to i32
  %arrayidx35 = getelementptr inbounds [6 x [16 x i32]], [6 x [16 x i32]]* bitcast (<{ [16 x i32], <{ i32, [15 x i32] }>, <{ i32, i32, i32, i32, i32, [11 x i32] }>, <{ i32, i32, i32, i32, i32, i32, i32, [9 x i32] }>, [16 x i32], [16 x i32] }>* @av1_ext_tx_inv to [6 x [16 x i32]]*), i32 0, i32 %idxprom34
  %43 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %44 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %inter_ext_tx_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %44, i32 0, i32 63
  %45 = load i32, i32* %eset, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds [4 x [4 x [17 x i16]]], [4 x [4 x [17 x i16]]]* %inter_ext_tx_cdf, i32 0, i32 %45
  %46 = load i8, i8* %square_tx_size, align 1, !tbaa !8
  %idxprom37 = zext i8 %46 to i32
  %arrayidx38 = getelementptr inbounds [4 x [17 x i16]], [4 x [17 x i16]]* %arrayidx36, i32 0, i32 %idxprom37
  %arraydecay = getelementptr inbounds [17 x i16], [17 x i16]* %arrayidx38, i32 0, i32 0
  %47 = load i8, i8* %tx_set_type, align 1, !tbaa !8
  %idxprom39 = zext i8 %47 to i32
  %arrayidx40 = getelementptr inbounds [6 x i32], [6 x i32]* @av1_num_ext_tx_set, i32 0, i32 %idxprom39
  %48 = load i32, i32* %arrayidx40, align 4, !tbaa !2
  %call41 = call i32 @aom_read_symbol_(%struct.aom_reader* %43, i16* %arraydecay, i32 %48, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @__func__.av1_read_tx_type, i32 0, i32 0))
  %arrayidx42 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx35, i32 0, i32 %call41
  %49 = load i32, i32* %arrayidx42, align 4, !tbaa !2
  %conv43 = trunc i32 %49 to i8
  %50 = load i8*, i8** %tx_type, align 4, !tbaa !6
  store i8 %conv43, i8* %50, align 1, !tbaa !8
  br label %if.end65

if.else:                                          ; preds = %if.then19
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %intra_mode) #6
  %51 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %filter_intra_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %51, i32 0, i32 13
  %use_filter_intra = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %filter_intra_mode_info, i32 0, i32 1
  %52 = load i8, i8* %use_filter_intra, align 1, !tbaa !48
  %conv44 = zext i8 %52 to i32
  %tobool45 = icmp ne i32 %conv44, 0
  br i1 %tobool45, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %53 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %filter_intra_mode_info46 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %53, i32 0, i32 13
  %filter_intra_mode = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %filter_intra_mode_info46, i32 0, i32 0
  %54 = load i8, i8* %filter_intra_mode, align 2, !tbaa !49
  %idxprom47 = zext i8 %54 to i32
  %arrayidx48 = getelementptr inbounds [5 x i8], [5 x i8]* @fimode_to_intradir, i32 0, i32 %idxprom47
  %55 = load i8, i8* %arrayidx48, align 1, !tbaa !8
  %conv49 = zext i8 %55 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %56 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %56, i32 0, i32 7
  %57 = load i8, i8* %mode, align 1, !tbaa !50
  %conv50 = zext i8 %57 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv49, %cond.true ], [ %conv50, %cond.false ]
  %conv51 = trunc i32 %cond to i8
  store i8 %conv51, i8* %intra_mode, align 1, !tbaa !8
  %58 = load i8, i8* %tx_set_type, align 1, !tbaa !8
  %idxprom52 = zext i8 %58 to i32
  %arrayidx53 = getelementptr inbounds [6 x [16 x i32]], [6 x [16 x i32]]* bitcast (<{ [16 x i32], <{ i32, [15 x i32] }>, <{ i32, i32, i32, i32, i32, [11 x i32] }>, <{ i32, i32, i32, i32, i32, i32, i32, [9 x i32] }>, [16 x i32], [16 x i32] }>* @av1_ext_tx_inv to [6 x [16 x i32]]*), i32 0, i32 %idxprom52
  %59 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %60 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %intra_ext_tx_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %60, i32 0, i32 62
  %61 = load i32, i32* %eset, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds [3 x [4 x [13 x [17 x i16]]]], [3 x [4 x [13 x [17 x i16]]]]* %intra_ext_tx_cdf, i32 0, i32 %61
  %62 = load i8, i8* %square_tx_size, align 1, !tbaa !8
  %idxprom55 = zext i8 %62 to i32
  %arrayidx56 = getelementptr inbounds [4 x [13 x [17 x i16]]], [4 x [13 x [17 x i16]]]* %arrayidx54, i32 0, i32 %idxprom55
  %63 = load i8, i8* %intra_mode, align 1, !tbaa !8
  %idxprom57 = zext i8 %63 to i32
  %arrayidx58 = getelementptr inbounds [13 x [17 x i16]], [13 x [17 x i16]]* %arrayidx56, i32 0, i32 %idxprom57
  %arraydecay59 = getelementptr inbounds [17 x i16], [17 x i16]* %arrayidx58, i32 0, i32 0
  %64 = load i8, i8* %tx_set_type, align 1, !tbaa !8
  %idxprom60 = zext i8 %64 to i32
  %arrayidx61 = getelementptr inbounds [6 x i32], [6 x i32]* @av1_num_ext_tx_set, i32 0, i32 %idxprom60
  %65 = load i32, i32* %arrayidx61, align 4, !tbaa !2
  %call62 = call i32 @aom_read_symbol_(%struct.aom_reader* %59, i16* %arraydecay59, i32 %65, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @__func__.av1_read_tx_type, i32 0, i32 0))
  %arrayidx63 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx53, i32 0, i32 %call62
  %66 = load i32, i32* %arrayidx63, align 4, !tbaa !2
  %conv64 = trunc i32 %66 to i8
  %67 = load i8*, i8** %tx_type, align 4, !tbaa !6
  store i8 %conv64, i8* %67, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %intra_mode) #6
  br label %if.end65

if.end65:                                         ; preds = %cond.end, %if.then33
  %68 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %square_tx_size) #6
  %69 = bitcast i32* %eset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_set_type) #6
  br label %if.end66

if.end66:                                         ; preds = %if.end65, %if.end12
  %70 = bitcast i32* %inter_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end66, %if.then11
  %71 = bitcast i32* %qindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  br label %cleanup67

cleanup67:                                        ; preds = %cleanup, %if.then
  %72 = bitcast i8** %tx_type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #6
  %73 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup67, %cleanup67
  ret void

unreachable:                                      ; preds = %cleanup67
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @segfeature_active(%struct.segmentation* %seg, i32 %segment_id, i8 zeroext %feature_id) #2 {
entry:
  %seg.addr = alloca %struct.segmentation*, align 4
  %segment_id.addr = alloca i32, align 4
  %feature_id.addr = alloca i8, align 1
  store %struct.segmentation* %seg, %struct.segmentation** %seg.addr, align 4, !tbaa !6
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !2
  store i8 %feature_id, i8* %feature_id.addr, align 1, !tbaa !8
  %0 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !6
  %enabled = getelementptr inbounds %struct.segmentation, %struct.segmentation* %0, i32 0, i32 0
  %1 = load i8, i8* %enabled, align 4, !tbaa !51
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !6
  %feature_mask = getelementptr inbounds %struct.segmentation, %struct.segmentation* %2, i32 0, i32 5
  %3 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %feature_mask, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !2
  %5 = load i8, i8* %feature_id.addr, align 1, !tbaa !8
  %conv1 = zext i8 %5 to i32
  %shl = shl i32 1, %conv1
  %and = and i32 %4, %shl
  %tobool2 = icmp ne i32 %and, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %tobool2, %land.rhs ]
  %land.ext = zext i1 %6 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_block(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !8
  %conv = sext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_ext_tx_types(i8 zeroext %tx_size, i32 %is_inter, i32 %use_reduced_set) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  %is_inter.addr = alloca i32, align 4
  %use_reduced_set.addr = alloca i32, align 4
  %set_type = alloca i32, align 4
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i32 %is_inter, i32* %is_inter.addr, align 4, !tbaa !2
  store i32 %use_reduced_set, i32* %use_reduced_set.addr, align 4, !tbaa !2
  %0 = bitcast i32* %set_type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %2 = load i32, i32* %is_inter.addr, align 4, !tbaa !2
  %3 = load i32, i32* %use_reduced_set.addr, align 4, !tbaa !2
  %call = call zeroext i8 @av1_get_ext_tx_set_type(i8 zeroext %1, i32 %2, i32 %3)
  %conv = zext i8 %call to i32
  store i32 %conv, i32* %set_type, align 4, !tbaa !2
  %4 = load i32, i32* %set_type, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [6 x i32], [6 x i32]* @av1_num_ext_tx_set, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx, align 4, !tbaa !2
  %6 = bitcast i32* %set_type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret i32 %5
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_get_ext_tx_set_type(i8 zeroext %tx_size, i32 %is_inter, i32 %use_reduced_set) #2 {
entry:
  %retval = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %is_inter.addr = alloca i32, align 4
  %use_reduced_set.addr = alloca i32, align 4
  %tx_size_sqr_up = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %tx_size_sqr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i32 %is_inter, i32* %is_inter.addr, align 4, !tbaa !2
  store i32 %use_reduced_set, i32* %use_reduced_set.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size_sqr_up) #6
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_up_map, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %1, i8* %tx_size_sqr_up, align 1, !tbaa !8
  %2 = load i8, i8* %tx_size_sqr_up, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8 0, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load i8, i8* %tx_size_sqr_up, align 1, !tbaa !8
  %conv2 = zext i8 %3 to i32
  %cmp3 = icmp eq i32 %conv2, 3
  br i1 %cmp3, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %4 = load i32, i32* %is_inter.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %4, 0
  %5 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  %conv6 = trunc i32 %cond to i8
  store i8 %conv6, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  %6 = load i32, i32* %use_reduced_set.addr, align 4, !tbaa !2
  %tobool8 = icmp ne i32 %6, 0
  br i1 %tobool8, label %if.then9, label %if.end13

if.then9:                                         ; preds = %if.end7
  %7 = load i32, i32* %is_inter.addr, align 4, !tbaa !2
  %tobool10 = icmp ne i32 %7, 0
  %8 = zext i1 %tobool10 to i64
  %cond11 = select i1 %tobool10, i32 1, i32 2
  %conv12 = trunc i32 %cond11 to i8
  store i8 %conv12, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.end7
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_size_sqr) #6
  %9 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %idxprom14 = zext i8 %9 to i32
  %arrayidx15 = getelementptr inbounds [19 x i8], [19 x i8]* @txsize_sqr_map, i32 0, i32 %idxprom14
  %10 = load i8, i8* %arrayidx15, align 1, !tbaa !8
  store i8 %10, i8* %tx_size_sqr, align 1, !tbaa !8
  %11 = load i32, i32* %is_inter.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds [2 x [2 x i8]], [2 x [2 x i8]]* @av1_ext_tx_set_lookup, i32 0, i32 %11
  %12 = load i8, i8* %tx_size_sqr, align 1, !tbaa !8
  %conv17 = zext i8 %12 to i32
  %cmp18 = icmp eq i32 %conv17, 2
  %conv19 = zext i1 %cmp18 to i32
  %arrayidx20 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx16, i32 0, i32 %conv19
  %13 = load i8, i8* %arrayidx20, align 1, !tbaa !8
  store i8 %13, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size_sqr) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end13, %if.then9, %if.then5, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_size_sqr_up) #6
  %14 = load i8, i8* %retval, align 1
  ret i8 %14
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_ext_tx_set(i8 zeroext %tx_size, i32 %is_inter, i32 %use_reduced_set) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  %is_inter.addr = alloca i32, align 4
  %use_reduced_set.addr = alloca i32, align 4
  %set_type = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !8
  store i32 %is_inter, i32* %is_inter.addr, align 4, !tbaa !2
  store i32 %use_reduced_set, i32* %use_reduced_set.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %set_type) #6
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !8
  %1 = load i32, i32* %is_inter.addr, align 4, !tbaa !2
  %2 = load i32, i32* %use_reduced_set.addr, align 4, !tbaa !2
  %call = call zeroext i8 @av1_get_ext_tx_set_type(i8 zeroext %0, i32 %1, i32 %2)
  store i8 %call, i8* %set_type, align 1, !tbaa !8
  %3 = load i32, i32* %is_inter.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [2 x [6 x i32]], [2 x [6 x i32]]* @ext_tx_set_index, i32 0, i32 %3
  %4 = load i8, i8* %set_type, align 1, !tbaa !8
  %idxprom = zext i8 %4 to i32
  %arrayidx1 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx, i32 0, i32 %idxprom
  %5 = load i32, i32* %arrayidx1, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %set_type) #6
  ret i32 %5
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_symbol_(%struct.aom_reader* %r, i16* %cdf, i32 %nsymbs, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %cdf.addr = alloca i16*, align 4
  %nsymbs.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %ret = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !6
  store i32 %nsymbs, i32* %nsymbs.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %3 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  %call = call i32 @aom_read_cdf_(%struct.aom_reader* %1, i16* %2, i32 %3, i8* %4)
  store i32 %call, i32* %ret, align 4, !tbaa !2
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %allow_update_cdf = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %5, i32 0, i32 4
  %6 = load i8, i8* %allow_update_cdf, align 4, !tbaa !52
  %tobool = icmp ne i8 %6, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %8 = load i32, i32* %ret, align 4, !tbaa !2
  %conv = trunc i32 %8 to i8
  %9 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  call void @update_cdf(i16* %7, i8 signext %conv, i32 %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i32, i32* %ret, align 4, !tbaa !2
  %11 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret i32 %10
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_read_mode_info(%struct.AV1Decoder* %pbi, %struct.macroblockd* %xd, %struct.aom_reader* %r, i32 %x_mis, i32 %y_mis) #0 {
entry:
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %x_mis.addr = alloca i32, align 4
  %y_mis.addr = alloca i32, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %mi = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i32 %x_mis, i32* %x_mis.addr, align 4, !tbaa !2
  store i32 %y_mis, i32* %y_mis.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %1, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !6
  %2 = bitcast %struct.MB_MODE_INFO** %mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 6
  %4 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi1, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %4, i32 0
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %mi, align 4, !tbaa !6
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi, align 4, !tbaa !6
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %6, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.clear = and i8 %bf.load, -65
  store i8 %bf.clear, i8* %use_intrabc, align 4
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %call = call i32 @frame_is_intra_only(%struct.AV1Common* %7)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %10 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_intra_frame_mode_info(%struct.AV1Common* %8, %struct.macroblockd* %9, %struct.aom_reader* %10)
  %11 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  %common2 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %11, i32 0, i32 1
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common2, i32 0, i32 37
  %order_hint_info = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 10
  %enable_ref_frame_mvs = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %order_hint_info, i32 0, i32 3
  %12 = load i32, i32* %enable_ref_frame_mvs, align 4, !tbaa !55
  %tobool3 = icmp ne i32 %12, 0
  br i1 %tobool3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %14 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %14, i32 0, i32 0
  %15 = load i32, i32* %mi_row, align 16, !tbaa !71
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %16, i32 0, i32 1
  %17 = load i32, i32* %mi_col, align 4, !tbaa !72
  %18 = load i32, i32* %x_mis.addr, align 4, !tbaa !2
  %19 = load i32, i32* %y_mis.addr, align 4, !tbaa !2
  call void @intra_copy_frame_mvs(%struct.AV1Common* %13, i32 %15, i32 %17, i32 %18, i32 %19)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then
  br label %if.end14

if.else:                                          ; preds = %entry
  %20 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %22 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_inter_frame_mode_info(%struct.AV1Decoder* %20, %struct.macroblockd* %21, %struct.aom_reader* %22)
  %23 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  %common5 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %23, i32 0, i32 1
  %seq_params6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common5, i32 0, i32 37
  %order_hint_info7 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params6, i32 0, i32 10
  %enable_ref_frame_mvs8 = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %order_hint_info7, i32 0, i32 3
  %24 = load i32, i32* %enable_ref_frame_mvs8, align 4, !tbaa !55
  %tobool9 = icmp ne i32 %24, 0
  br i1 %tobool9, label %if.then10, label %if.end13

if.then10:                                        ; preds = %if.else
  %25 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %26 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi, align 4, !tbaa !6
  %27 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row11 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %27, i32 0, i32 0
  %28 = load i32, i32* %mi_row11, align 16, !tbaa !71
  %29 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col12 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %29, i32 0, i32 1
  %30 = load i32, i32* %mi_col12, align 4, !tbaa !72
  %31 = load i32, i32* %x_mis.addr, align 4, !tbaa !2
  %32 = load i32, i32* %y_mis.addr, align 4, !tbaa !2
  call void @av1_copy_frame_mvs(%struct.AV1Common* %25, %struct.MB_MODE_INFO* %26, i32 %28, i32 %30, i32 %31, i32 %32)
  br label %if.end13

if.end13:                                         ; preds = %if.then10, %if.else
  br label %if.end14

if.end14:                                         ; preds = %if.end13, %if.end
  %33 = bitcast %struct.MB_MODE_INFO** %mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @frame_is_intra_only(%struct.AV1Common* %cm) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 0
  %frame_type = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 0
  %1 = load i8, i8* %frame_type, align 16, !tbaa !73
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %current_frame2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 0
  %frame_type3 = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame2, i32 0, i32 0
  %3 = load i8, i8* %frame_type3, align 16, !tbaa !73
  %conv4 = zext i8 %3 to i32
  %cmp5 = icmp eq i32 %conv4, 2
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %4 = phi i1 [ true, %entry ], [ %cmp5, %lor.rhs ]
  %lor.ext = zext i1 %4 to i32
  ret i32 %lor.ext
}

; Function Attrs: nounwind
define internal void @read_intra_frame_mode_info(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %above_mi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mi = alloca %struct.MB_MODE_INFO*, align 4
  %bsize = alloca i8, align 1
  %seg = alloca %struct.segmentation*, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %use_angle_delta = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %4 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 12
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 16, !tbaa !74
  store %struct.MB_MODE_INFO* %6, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %7 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 11
  %9 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !75
  store %struct.MB_MODE_INFO* %9, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %10, i32 0, i32 6
  %11 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %11, i8* %bsize, align 1, !tbaa !8
  %12 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 24
  store %struct.segmentation* %seg1, %struct.segmentation** %seg, align 4, !tbaa !6
  %14 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %15, i32 0, i32 40
  %16 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %16, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %17 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %segid_preskip = getelementptr inbounds %struct.segmentation, %struct.segmentation* %17, i32 0, i32 7
  %18 = load i8, i8* %segid_preskip, align 4, !tbaa !77
  %tobool = icmp ne i8 %18, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %20 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %21 = load i8, i8* %bsize, align 1, !tbaa !8
  %conv = zext i8 %21 to i32
  %22 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call = call i32 @read_intra_segment_id(%struct.AV1Common* %19, %struct.macroblockd* %20, i32 %conv, %struct.aom_reader* %22, i32 0)
  %conv2 = trunc i32 %call to i8
  %23 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %23, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id, align 4
  %bf.value = and i8 %conv2, 7
  %bf.shl = shl i8 %bf.value, 1
  %bf.clear = and i8 %bf.load, -15
  %bf.set = or i8 %bf.clear, %bf.shl
  store i8 %bf.set, i8* %segment_id, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %24 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %25 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %26 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id3 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %26, i32 0, i32 26
  %bf.load4 = load i8, i8* %segment_id3, align 4
  %bf.lshr = lshr i8 %bf.load4, 1
  %bf.clear5 = and i8 %bf.lshr, 7
  %conv6 = zext i8 %bf.clear5 to i32
  %27 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call7 = call i32 @read_skip(%struct.AV1Common* %24, %struct.macroblockd* %25, i32 %conv6, %struct.aom_reader* %27)
  %conv8 = trunc i32 %call7 to i8
  %28 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %28, i32 0, i32 14
  store i8 %conv8, i8* %skip, align 4, !tbaa !17
  %29 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %segid_preskip9 = getelementptr inbounds %struct.segmentation, %struct.segmentation* %29, i32 0, i32 7
  %30 = load i8, i8* %segid_preskip9, align 4, !tbaa !77
  %tobool10 = icmp ne i8 %30, 0
  br i1 %tobool10, label %if.end23, label %if.then11

if.then11:                                        ; preds = %if.end
  %31 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %32 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %33 = load i8, i8* %bsize, align 1, !tbaa !8
  %conv12 = zext i8 %33 to i32
  %34 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %35 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip13 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %35, i32 0, i32 14
  %36 = load i8, i8* %skip13, align 4, !tbaa !17
  %conv14 = sext i8 %36 to i32
  %call15 = call i32 @read_intra_segment_id(%struct.AV1Common* %31, %struct.macroblockd* %32, i32 %conv12, %struct.aom_reader* %34, i32 %conv14)
  %conv16 = trunc i32 %call15 to i8
  %37 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id17 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %37, i32 0, i32 26
  %bf.load18 = load i8, i8* %segment_id17, align 4
  %bf.value19 = and i8 %conv16, 7
  %bf.shl20 = shl i8 %bf.value19, 1
  %bf.clear21 = and i8 %bf.load18, -15
  %bf.set22 = or i8 %bf.clear21, %bf.shl20
  store i8 %bf.set22, i8* %segment_id17, align 4
  br label %if.end23

if.end23:                                         ; preds = %if.then11, %if.end
  %38 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %39 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %40 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  call void @read_cdef(%struct.AV1Common* %38, %struct.aom_reader* %39, %struct.macroblockd* %40)
  %41 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %42 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %43 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_delta_q_params(%struct.AV1Common* %41, %struct.macroblockd* %42, %struct.aom_reader* %43)
  %44 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %current_qindex = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %44, i32 0, i32 49
  %45 = load i32, i32* %current_qindex, align 4, !tbaa !78
  %46 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %current_qindex24 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %46, i32 0, i32 3
  store i32 %45, i32* %current_qindex24, align 4, !tbaa !79
  %47 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %47, i32 0, i32 12
  %arrayidx25 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  store i8 0, i8* %arrayidx25, align 4, !tbaa !8
  %48 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %ref_frame26 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %48, i32 0, i32 12
  %arrayidx27 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame26, i32 0, i32 1
  store i8 -1, i8* %arrayidx27, align 1, !tbaa !8
  %49 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %49, i32 0, i32 5
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 1
  %arrayidx28 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 0
  store i8 0, i8* %arrayidx28, align 4, !tbaa !8
  %50 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %palette_mode_info29 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %50, i32 0, i32 5
  %palette_size30 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info29, i32 0, i32 1
  %arrayidx31 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size30, i32 0, i32 1
  store i8 0, i8* %arrayidx31, align 1, !tbaa !8
  %51 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %filter_intra_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %51, i32 0, i32 13
  %use_filter_intra = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %filter_intra_mode_info, i32 0, i32 1
  store i8 0, i8* %use_filter_intra, align 1, !tbaa !48
  %52 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row32 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %53, i32 0, i32 0
  %54 = load i32, i32* %mi_row32, align 16, !tbaa !71
  store i32 %54, i32* %mi_row, align 4, !tbaa !2
  %55 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #6
  %56 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col33 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %56, i32 0, i32 1
  %57 = load i32, i32* %mi_col33, align 4, !tbaa !72
  store i32 %57, i32* %mi_col, align 4, !tbaa !2
  %58 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %above_contexts = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %58, i32 0, i32 42
  %txfm = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %above_contexts, i32 0, i32 2
  %59 = load i8**, i8*** %txfm, align 8, !tbaa !80
  %60 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %60, i32 0, i32 5
  %tile_row = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile, i32 0, i32 4
  %61 = load i32, i32* %tile_row, align 4, !tbaa !81
  %arrayidx34 = getelementptr inbounds i8*, i8** %59, i32 %61
  %62 = load i8*, i8** %arrayidx34, align 4, !tbaa !6
  %63 = load i32, i32* %mi_col, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %62, i32 %63
  %64 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_txfm_context = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %64, i32 0, i32 27
  store i8* %add.ptr, i8** %above_txfm_context, align 16, !tbaa !82
  %65 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_txfm_context_buffer = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %65, i32 0, i32 29
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %left_txfm_context_buffer, i32 0, i32 0
  %66 = load i32, i32* %mi_row, align 4, !tbaa !2
  %and = and i32 %66, 31
  %add.ptr35 = getelementptr inbounds i8, i8* %arraydecay, i32 %and
  %67 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_txfm_context = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %67, i32 0, i32 28
  store i8* %add.ptr35, i8** %left_txfm_context, align 4, !tbaa !83
  %68 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call36 = call i32 @av1_allow_intrabc(%struct.AV1Common* %68)
  %tobool37 = icmp ne i32 %call36, 0
  br i1 %tobool37, label %if.then38, label %if.end43

if.then38:                                        ; preds = %if.end23
  %69 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %70 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %71 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_intrabc_info(%struct.AV1Common* %69, %struct.macroblockd* %70, %struct.aom_reader* %71)
  %72 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %call39 = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %72)
  %tobool40 = icmp ne i32 %call39, 0
  br i1 %tobool40, label %if.then41, label %if.end42

if.then41:                                        ; preds = %if.then38
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end42:                                         ; preds = %if.then38
  br label %if.end43

if.end43:                                         ; preds = %if.end42, %if.end23
  %73 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %74 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %75 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %76 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %call44 = call i16* @get_y_mode_cdf(%struct.frame_contexts* %74, %struct.MB_MODE_INFO* %75, %struct.MB_MODE_INFO* %76)
  %call45 = call zeroext i8 @read_intra_mode(%struct.aom_reader* %73, i16* %call44)
  %77 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %77, i32 0, i32 7
  store i8 %call45, i8* %mode, align 1, !tbaa !50
  %78 = bitcast i32* %use_angle_delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #6
  %79 = load i8, i8* %bsize, align 1, !tbaa !8
  %call46 = call i32 @av1_use_angle_delta(i8 zeroext %79)
  store i32 %call46, i32* %use_angle_delta, align 4, !tbaa !2
  %80 = load i32, i32* %use_angle_delta, align 4, !tbaa !2
  %tobool47 = icmp ne i32 %80, 0
  br i1 %tobool47, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %if.end43
  %81 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mode48 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %81, i32 0, i32 7
  %82 = load i8, i8* %mode48, align 1, !tbaa !50
  %call49 = call i32 @av1_is_directional_mode(i8 zeroext %82)
  %tobool50 = icmp ne i32 %call49, 0
  br i1 %tobool50, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %83 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %84 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %angle_delta_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %84, i32 0, i32 57
  %85 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mode51 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %85, i32 0, i32 7
  %86 = load i8, i8* %mode51, align 1, !tbaa !50
  %conv52 = zext i8 %86 to i32
  %sub = sub nsw i32 %conv52, 1
  %arrayidx53 = getelementptr inbounds [8 x [8 x i16]], [8 x [8 x i16]]* %angle_delta_cdf, i32 0, i32 %sub
  %arraydecay54 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx53, i32 0, i32 0
  %call55 = call i32 @read_angle_delta(%struct.aom_reader* %83, i16* %arraydecay54)
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %if.end43
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call55, %cond.true ], [ 0, %cond.false ]
  %conv56 = trunc i32 %cond to i8
  %87 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %angle_delta = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %87, i32 0, i32 20
  %arrayidx57 = getelementptr inbounds [2 x i8], [2 x i8]* %angle_delta, i32 0, i32 0
  store i8 %conv56, i8* %arrayidx57, align 4, !tbaa !8
  %88 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %88, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %89 = load i8, i8* %monochrome, align 1, !tbaa !84
  %tobool58 = icmp ne i8 %89, 0
  br i1 %tobool58, label %if.else, label %land.lhs.true59

land.lhs.true59:                                  ; preds = %cond.end
  %90 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %is_chroma_ref = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %90, i32 0, i32 3
  %91 = load i8, i8* %is_chroma_ref, align 4, !tbaa !85, !range !46
  %tobool60 = trunc i8 %91 to i1
  br i1 %tobool60, label %if.then62, label %if.else

if.then62:                                        ; preds = %land.lhs.true59
  %92 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %93 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %94 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call63 = call zeroext i8 @is_cfl_allowed(%struct.macroblockd* %94)
  %95 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mode64 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %95, i32 0, i32 7
  %96 = load i8, i8* %mode64, align 1, !tbaa !50
  %call65 = call zeroext i8 @read_intra_mode_uv(%struct.frame_contexts* %92, %struct.aom_reader* %93, i8 zeroext %call63, i8 zeroext %96)
  %97 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %uv_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %97, i32 0, i32 8
  store i8 %call65, i8* %uv_mode, align 4, !tbaa !86
  %98 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %uv_mode66 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %98, i32 0, i32 8
  %99 = load i8, i8* %uv_mode66, align 4, !tbaa !86
  %conv67 = zext i8 %99 to i32
  %cmp = icmp eq i32 %conv67, 13
  br i1 %cmp, label %if.then69, label %if.end71

if.then69:                                        ; preds = %if.then62
  %100 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %101 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %102 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %cfl_alpha_signs = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %102, i32 0, i32 21
  %call70 = call zeroext i8 @read_cfl_alphas(%struct.frame_contexts* %100, %struct.aom_reader* %101, i8* %cfl_alpha_signs)
  %103 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %cfl_alpha_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %103, i32 0, i32 22
  store i8 %call70, i8* %cfl_alpha_idx, align 1, !tbaa !87
  br label %if.end71

if.end71:                                         ; preds = %if.then69, %if.then62
  %104 = load i32, i32* %use_angle_delta, align 4, !tbaa !2
  %tobool72 = icmp ne i32 %104, 0
  br i1 %tobool72, label %land.lhs.true73, label %cond.false86

land.lhs.true73:                                  ; preds = %if.end71
  %105 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %uv_mode74 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %105, i32 0, i32 8
  %106 = load i8, i8* %uv_mode74, align 4, !tbaa !86
  %call75 = call zeroext i8 @get_uv_mode(i8 zeroext %106)
  %call76 = call i32 @av1_is_directional_mode(i8 zeroext %call75)
  %tobool77 = icmp ne i32 %call76, 0
  br i1 %tobool77, label %cond.true78, label %cond.false86

cond.true78:                                      ; preds = %land.lhs.true73
  %107 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %108 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %angle_delta_cdf79 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %108, i32 0, i32 57
  %109 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %uv_mode80 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %109, i32 0, i32 8
  %110 = load i8, i8* %uv_mode80, align 4, !tbaa !86
  %conv81 = zext i8 %110 to i32
  %sub82 = sub nsw i32 %conv81, 1
  %arrayidx83 = getelementptr inbounds [8 x [8 x i16]], [8 x [8 x i16]]* %angle_delta_cdf79, i32 0, i32 %sub82
  %arraydecay84 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx83, i32 0, i32 0
  %call85 = call i32 @read_angle_delta(%struct.aom_reader* %107, i16* %arraydecay84)
  br label %cond.end87

cond.false86:                                     ; preds = %land.lhs.true73, %if.end71
  br label %cond.end87

cond.end87:                                       ; preds = %cond.false86, %cond.true78
  %cond88 = phi i32 [ %call85, %cond.true78 ], [ 0, %cond.false86 ]
  %conv89 = trunc i32 %cond88 to i8
  %111 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %angle_delta90 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %111, i32 0, i32 20
  %arrayidx91 = getelementptr inbounds [2 x i8], [2 x i8]* %angle_delta90, i32 0, i32 1
  store i8 %conv89, i8* %arrayidx91, align 1, !tbaa !8
  br label %if.end93

if.else:                                          ; preds = %land.lhs.true59, %cond.end
  %112 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %uv_mode92 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %112, i32 0, i32 8
  store i8 0, i8* %uv_mode92, align 4, !tbaa !86
  br label %if.end93

if.end93:                                         ; preds = %if.else, %cond.end87
  %113 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %114 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call94 = call zeroext i8 @store_cfl_required(%struct.AV1Common* %113, %struct.macroblockd* %114)
  %conv95 = zext i8 %call94 to i32
  %115 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cfl = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %115, i32 0, i32 56
  %store_y = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %cfl, i32 0, i32 10
  store i32 %conv95, i32* %store_y, align 8, !tbaa !88
  %116 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %116, i32 0, i32 21
  %allow_screen_content_tools = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 3
  %117 = load i8, i8* %allow_screen_content_tools, align 1, !tbaa !89, !range !46
  %tobool96 = trunc i8 %117 to i1
  %conv97 = zext i1 %tobool96 to i32
  %118 = load i8, i8* %bsize, align 1, !tbaa !8
  %call98 = call i32 @av1_allow_palette(i32 %conv97, i8 zeroext %118)
  %tobool99 = icmp ne i32 %call98, 0
  br i1 %tobool99, label %if.then100, label %if.end101

if.then100:                                       ; preds = %if.end93
  %119 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %120 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %121 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_palette_mode_info(%struct.AV1Common* %119, %struct.macroblockd* %120, %struct.aom_reader* %121)
  br label %if.end101

if.end101:                                        ; preds = %if.then100, %if.end93
  %122 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %123 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %124 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_filter_intra_mode_info(%struct.AV1Common* %122, %struct.macroblockd* %123, %struct.aom_reader* %124)
  %125 = bitcast i32* %use_angle_delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end101, %if.then41
  %126 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #6
  %127 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #6
  %128 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #6
  %129 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  %130 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #6
  %131 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #6
  %132 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @intra_copy_frame_mvs(%struct.AV1Common* %cm, i32 %mi_row, i32 %mi_col, i32 %x_mis, i32 %y_mis) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %x_mis.addr = alloca i32, align 4
  %y_mis.addr = alloca i32, align 4
  %frame_mvs_stride = alloca i32, align 4
  %frame_mvs = alloca %struct.MV_REF*, align 4
  %h = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %mv = alloca %struct.MV_REF*, align 4
  %w = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !2
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !2
  store i32 %x_mis, i32* %x_mis.addr, align 4, !tbaa !2
  store i32 %y_mis, i32* %y_mis.addr, align 4, !tbaa !2
  %0 = bitcast i32* %frame_mvs_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %2 = load i32, i32* %mi_cols, align 4, !tbaa !90
  %add = add nsw i32 %2, 1
  %shr = ashr i32 %add, 1
  store i32 %shr, i32* %frame_mvs_stride, align 4, !tbaa !2
  %3 = bitcast %struct.MV_REF** %frame_mvs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 13
  %5 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !91
  %mvs = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %5, i32 0, i32 5
  %6 = load %struct.MV_REF*, %struct.MV_REF** %mvs, align 4, !tbaa !92
  %7 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %shr1 = ashr i32 %7, 1
  %8 = load i32, i32* %frame_mvs_stride, align 4, !tbaa !2
  %mul = mul nsw i32 %shr1, %8
  %add.ptr = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %6, i32 %mul
  %9 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %shr2 = ashr i32 %9, 1
  %add.ptr3 = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %add.ptr, i32 %shr2
  store %struct.MV_REF* %add.ptr3, %struct.MV_REF** %frame_mvs, align 4, !tbaa !6
  %10 = load i32, i32* %x_mis.addr, align 4, !tbaa !2
  %add4 = add nsw i32 %10, 1
  %shr5 = ashr i32 %add4, 1
  store i32 %shr5, i32* %x_mis.addr, align 4, !tbaa !2
  %11 = load i32, i32* %y_mis.addr, align 4, !tbaa !2
  %add6 = add nsw i32 %11, 1
  %shr7 = ashr i32 %add6, 1
  store i32 %shr7, i32* %y_mis.addr, align 4, !tbaa !2
  %12 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  store i32 0, i32* %h, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %13 = load i32, i32* %h, align 4, !tbaa !2
  %14 = load i32, i32* %y_mis.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %13, %14
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %15 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  br label %for.end15

for.body:                                         ; preds = %for.cond
  %16 = bitcast %struct.MV_REF** %mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.MV_REF*, %struct.MV_REF** %frame_mvs, align 4, !tbaa !6
  store %struct.MV_REF* %17, %struct.MV_REF** %mv, align 4, !tbaa !6
  %18 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  store i32 0, i32* %w, align 4, !tbaa !2
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %19 = load i32, i32* %w, align 4, !tbaa !2
  %20 = load i32, i32* %x_mis.addr, align 4, !tbaa !2
  %cmp9 = icmp slt i32 %19, %20
  br i1 %cmp9, label %for.body11, label %for.cond.cleanup10

for.cond.cleanup10:                               ; preds = %for.cond8
  store i32 5, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  br label %for.end

for.body11:                                       ; preds = %for.cond8
  %22 = load %struct.MV_REF*, %struct.MV_REF** %mv, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %22, i32 0, i32 1
  store i8 -1, i8* %ref_frame, align 4, !tbaa !98
  %23 = load %struct.MV_REF*, %struct.MV_REF** %mv, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %23, i32 1
  store %struct.MV_REF* %incdec.ptr, %struct.MV_REF** %mv, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body11
  %24 = load i32, i32* %w, align 4, !tbaa !2
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %w, align 4, !tbaa !2
  br label %for.cond8

for.end:                                          ; preds = %for.cond.cleanup10
  %25 = load i32, i32* %frame_mvs_stride, align 4, !tbaa !2
  %26 = load %struct.MV_REF*, %struct.MV_REF** %frame_mvs, align 4, !tbaa !6
  %add.ptr12 = getelementptr inbounds %struct.MV_REF, %struct.MV_REF* %26, i32 %25
  store %struct.MV_REF* %add.ptr12, %struct.MV_REF** %frame_mvs, align 4, !tbaa !6
  %27 = bitcast %struct.MV_REF** %mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #6
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %28 = load i32, i32* %h, align 4, !tbaa !2
  %inc14 = add nsw i32 %28, 1
  store i32 %inc14, i32* %h, align 4, !tbaa !2
  br label %for.cond

for.end15:                                        ; preds = %for.cond.cleanup
  %29 = bitcast %struct.MV_REF** %frame_mvs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  %30 = bitcast i32* %frame_mvs_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  ret void
}

; Function Attrs: nounwind
define internal void @read_inter_frame_mode_info(%struct.AV1Decoder* %pbi, %struct.macroblockd* %xd, %struct.aom_reader* %r) #0 {
entry:
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %inter_block = alloca i32, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %1, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !6
  %2 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 6
  %4 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %4, i32 0
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %6 = bitcast i32* %inter_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 1, i32* %inter_block, align 4, !tbaa !2
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mv = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 2
  %arrayidx1 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv, i32 0, i32 0
  %as_int = bitcast %union.int_mv* %arrayidx1 to i32*
  store i32 0, i32* %as_int, align 4, !tbaa !8
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mv2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 2
  %arrayidx3 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv2, i32 0, i32 1
  %as_int4 = bitcast %union.int_mv* %arrayidx3 to i32*
  store i32 0, i32* %as_int4, align 4, !tbaa !8
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %10 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %11 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call = call i32 @read_inter_segment_id(%struct.AV1Common* %9, %struct.macroblockd* %10, i32 1, %struct.aom_reader* %11)
  %conv = trunc i32 %call to i8
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %12, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id, align 4
  %bf.value = and i8 %conv, 7
  %bf.shl = shl i8 %bf.value, 1
  %bf.clear = and i8 %bf.load, -15
  %bf.set = or i8 %bf.clear, %bf.shl
  store i8 %bf.set, i8* %segment_id, align 4
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %14 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id5 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %15, i32 0, i32 26
  %bf.load6 = load i8, i8* %segment_id5, align 4
  %bf.lshr = lshr i8 %bf.load6, 1
  %bf.clear7 = and i8 %bf.lshr, 7
  %conv8 = zext i8 %bf.clear7 to i32
  %16 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call9 = call i32 @read_skip_mode(%struct.AV1Common* %13, %struct.macroblockd* %14, i32 %conv8, %struct.aom_reader* %16)
  %conv10 = trunc i32 %call9 to i8
  %17 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %17, i32 0, i32 26
  %bf.load11 = load i8, i8* %skip_mode, align 4
  %bf.value12 = and i8 %conv10, 1
  %bf.shl13 = shl i8 %bf.value12, 5
  %bf.clear14 = and i8 %bf.load11, -33
  %bf.set15 = or i8 %bf.clear14, %bf.shl13
  store i8 %bf.set15, i8* %skip_mode, align 4
  %18 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip_mode16 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %18, i32 0, i32 26
  %bf.load17 = load i8, i8* %skip_mode16, align 4
  %bf.lshr18 = lshr i8 %bf.load17, 5
  %bf.clear19 = and i8 %bf.lshr18, 1
  %tobool = icmp ne i8 %bf.clear19, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %19 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %19, i32 0, i32 14
  store i8 1, i8* %skip, align 4, !tbaa !17
  br label %if.end

if.else:                                          ; preds = %entry
  %20 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %22 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id20 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %22, i32 0, i32 26
  %bf.load21 = load i8, i8* %segment_id20, align 4
  %bf.lshr22 = lshr i8 %bf.load21, 1
  %bf.clear23 = and i8 %bf.lshr22, 7
  %conv24 = zext i8 %bf.clear23 to i32
  %23 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call25 = call i32 @read_skip(%struct.AV1Common* %20, %struct.macroblockd* %21, i32 %conv24, %struct.aom_reader* %23)
  %conv26 = trunc i32 %call25 to i8
  %24 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip27 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %24, i32 0, i32 14
  store i8 %conv26, i8* %skip27, align 4, !tbaa !17
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %25 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %25, i32 0, i32 24
  %segid_preskip = getelementptr inbounds %struct.segmentation, %struct.segmentation* %seg, i32 0, i32 7
  %26 = load i8, i8* %segid_preskip, align 4, !tbaa !100
  %tobool28 = icmp ne i8 %26, 0
  br i1 %tobool28, label %if.end38, label %if.then29

if.then29:                                        ; preds = %if.end
  %27 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %28 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %29 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call30 = call i32 @read_inter_segment_id(%struct.AV1Common* %27, %struct.macroblockd* %28, i32 0, %struct.aom_reader* %29)
  %conv31 = trunc i32 %call30 to i8
  %30 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id32 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %30, i32 0, i32 26
  %bf.load33 = load i8, i8* %segment_id32, align 4
  %bf.value34 = and i8 %conv31, 7
  %bf.shl35 = shl i8 %bf.value34, 1
  %bf.clear36 = and i8 %bf.load33, -15
  %bf.set37 = or i8 %bf.clear36, %bf.shl35
  store i8 %bf.set37, i8* %segment_id32, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.then29, %if.end
  %31 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %32 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %33 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  call void @read_cdef(%struct.AV1Common* %31, %struct.aom_reader* %32, %struct.macroblockd* %33)
  %34 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %35 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %36 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_delta_q_params(%struct.AV1Common* %34, %struct.macroblockd* %35, %struct.aom_reader* %36)
  %37 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip_mode39 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %37, i32 0, i32 26
  %bf.load40 = load i8, i8* %skip_mode39, align 4
  %bf.lshr41 = lshr i8 %bf.load40, 5
  %bf.clear42 = and i8 %bf.lshr41, 1
  %tobool43 = icmp ne i8 %bf.clear42, 0
  br i1 %tobool43, label %if.end51, label %if.then44

if.then44:                                        ; preds = %if.end38
  %38 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %39 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %40 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id45 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %40, i32 0, i32 26
  %bf.load46 = load i8, i8* %segment_id45, align 4
  %bf.lshr47 = lshr i8 %bf.load46, 1
  %bf.clear48 = and i8 %bf.lshr47, 7
  %conv49 = zext i8 %bf.clear48 to i32
  %41 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call50 = call i32 @read_is_inter_block(%struct.AV1Common* %38, %struct.macroblockd* %39, i32 %conv49, %struct.aom_reader* %41)
  store i32 %call50, i32* %inter_block, align 4, !tbaa !2
  br label %if.end51

if.end51:                                         ; preds = %if.then44, %if.end38
  %42 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %current_qindex = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %42, i32 0, i32 49
  %43 = load i32, i32* %current_qindex, align 4, !tbaa !78
  %44 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %current_qindex52 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %44, i32 0, i32 3
  store i32 %43, i32* %current_qindex52, align 4, !tbaa !79
  %45 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %above_contexts = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %45, i32 0, i32 42
  %txfm = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %above_contexts, i32 0, i32 2
  %46 = load i8**, i8*** %txfm, align 8, !tbaa !80
  %47 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %47, i32 0, i32 5
  %tile_row = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile, i32 0, i32 4
  %48 = load i32, i32* %tile_row, align 4, !tbaa !81
  %arrayidx53 = getelementptr inbounds i8*, i8** %46, i32 %48
  %49 = load i8*, i8** %arrayidx53, align 4, !tbaa !6
  %50 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %50, i32 0, i32 1
  %51 = load i32, i32* %mi_col, align 4, !tbaa !72
  %add.ptr = getelementptr inbounds i8, i8* %49, i32 %51
  %52 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_txfm_context = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %52, i32 0, i32 27
  store i8* %add.ptr, i8** %above_txfm_context, align 16, !tbaa !82
  %53 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_txfm_context_buffer = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %53, i32 0, i32 29
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %left_txfm_context_buffer, i32 0, i32 0
  %54 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %54, i32 0, i32 0
  %55 = load i32, i32* %mi_row, align 16, !tbaa !71
  %and = and i32 %55, 31
  %add.ptr54 = getelementptr inbounds i8, i8* %arraydecay, i32 %and
  %56 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_txfm_context = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %56, i32 0, i32 28
  store i8* %add.ptr54, i8** %left_txfm_context, align 4, !tbaa !83
  %57 = load i32, i32* %inter_block, align 4, !tbaa !2
  %tobool55 = icmp ne i32 %57, 0
  br i1 %tobool55, label %if.then56, label %if.else57

if.then56:                                        ; preds = %if.end51
  %58 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  %59 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %60 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %61 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_inter_block_mode_info(%struct.AV1Decoder* %58, %struct.macroblockd* %59, %struct.MB_MODE_INFO* %60, %struct.aom_reader* %61)
  br label %if.end58

if.else57:                                        ; preds = %if.end51
  %62 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %63 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %64 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %65 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_intra_block_mode_info(%struct.AV1Common* %62, %struct.macroblockd* %63, %struct.MB_MODE_INFO* %64, %struct.aom_reader* %65)
  br label %if.end58

if.end58:                                         ; preds = %if.else57, %if.then56
  %66 = bitcast i32* %inter_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  ret void
}

declare void @av1_copy_frame_mvs(%struct.AV1Common*, %struct.MB_MODE_INFO*, i32, i32, i32, i32) #3

; Function Attrs: inlinehint nounwind
define internal i32 @is_intrabc_block(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  ret i32 %conv
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_cdf_(%struct.aom_reader* %r, i16* %cdf, i32 %nsymbs, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %cdf.addr = alloca i16*, align 4
  %nsymbs.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %symb = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !6
  store i32 %nsymbs, i32* %nsymbs.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !6
  %0 = bitcast i32* %symb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %1, i32 0, i32 2
  %2 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %3 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  %call = call i32 @od_ec_decode_cdf_q15(%struct.od_ec_dec* %ec, i16* %2, i32 %3)
  store i32 %call, i32* %symb, align 4, !tbaa !2
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  %tobool = icmp ne i8* %4, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  call void @aom_process_accounting(%struct.aom_reader* %5, i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %8 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  %cmp = icmp eq i32 %8, 2
  %conv = zext i1 %cmp to i32
  call void @aom_update_symb_counts(%struct.aom_reader* %7, i32 %conv)
  %9 = load i32, i32* %symb, align 4, !tbaa !2
  %10 = bitcast i32* %symb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  ret i32 %9
}

; Function Attrs: inlinehint nounwind
define internal void @update_cdf(i16* %cdf, i8 signext %val, i32 %nsymbs) #2 {
entry:
  %cdf.addr = alloca i16*, align 4
  %val.addr = alloca i8, align 1
  %nsymbs.addr = alloca i32, align 4
  %rate = alloca i32, align 4
  %i = alloca i32, align 4
  %tmp = alloca i32, align 4
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !6
  store i8 %val, i8* %val.addr, align 1, !tbaa !8
  store i32 %nsymbs, i32* %nsymbs.addr, align 4, !tbaa !2
  %0 = bitcast i32* %rate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %4 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 %4
  %5 = load i16, i16* %arrayidx, align 2, !tbaa !101
  %conv = zext i16 %5 to i32
  %cmp = icmp sgt i32 %conv, 15
  %conv1 = zext i1 %cmp to i32
  %add = add nsw i32 3, %conv1
  %6 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %7 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx2, align 2, !tbaa !101
  %conv3 = zext i16 %8 to i32
  %cmp4 = icmp sgt i32 %conv3, 31
  %conv5 = zext i1 %cmp4 to i32
  %add6 = add nsw i32 %add, %conv5
  %9 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds [17 x i32], [17 x i32]* @update_cdf.nsymbs2speed, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx7, align 4, !tbaa !2
  %add8 = add nsw i32 %add6, %10
  store i32 %add8, i32* %rate, align 4, !tbaa !2
  store i32 32768, i32* %tmp, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !2
  %12 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %12, 1
  %cmp9 = icmp slt i32 %11, %sub
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load i32, i32* %i, align 4, !tbaa !2
  %14 = load i8, i8* %val.addr, align 1, !tbaa !8
  %conv11 = sext i8 %14 to i32
  %cmp12 = icmp eq i32 %13, %conv11
  br i1 %cmp12, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %15 = load i32, i32* %tmp, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %15, %cond.false ]
  store i32 %cond, i32* %tmp, align 4, !tbaa !2
  %16 = load i32, i32* %tmp, align 4, !tbaa !2
  %17 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %18 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i16, i16* %17, i32 %18
  %19 = load i16, i16* %arrayidx14, align 2, !tbaa !101
  %conv15 = zext i16 %19 to i32
  %cmp16 = icmp slt i32 %16, %conv15
  br i1 %cmp16, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %20 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %21 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i16, i16* %20, i32 %21
  %22 = load i16, i16* %arrayidx18, align 2, !tbaa !101
  %conv19 = zext i16 %22 to i32
  %23 = load i32, i32* %tmp, align 4, !tbaa !2
  %sub20 = sub nsw i32 %conv19, %23
  %24 = load i32, i32* %rate, align 4, !tbaa !2
  %shr = ashr i32 %sub20, %24
  %25 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %26 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i16, i16* %25, i32 %26
  %27 = load i16, i16* %arrayidx21, align 2, !tbaa !101
  %conv22 = zext i16 %27 to i32
  %sub23 = sub nsw i32 %conv22, %shr
  %conv24 = trunc i32 %sub23 to i16
  store i16 %conv24, i16* %arrayidx21, align 2, !tbaa !101
  br label %if.end

if.else:                                          ; preds = %cond.end
  %28 = load i32, i32* %tmp, align 4, !tbaa !2
  %29 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %30 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i16, i16* %29, i32 %30
  %31 = load i16, i16* %arrayidx25, align 2, !tbaa !101
  %conv26 = zext i16 %31 to i32
  %sub27 = sub nsw i32 %28, %conv26
  %32 = load i32, i32* %rate, align 4, !tbaa !2
  %shr28 = ashr i32 %sub27, %32
  %33 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %34 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i16, i16* %33, i32 %34
  %35 = load i16, i16* %arrayidx29, align 2, !tbaa !101
  %conv30 = zext i16 %35 to i32
  %add31 = add nsw i32 %conv30, %shr28
  %conv32 = trunc i32 %add31 to i16
  store i16 %conv32, i16* %arrayidx29, align 2, !tbaa !101
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %36 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %38 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i16, i16* %37, i32 %38
  %39 = load i16, i16* %arrayidx33, align 2, !tbaa !101
  %conv34 = zext i16 %39 to i32
  %cmp35 = icmp slt i32 %conv34, 32
  %conv36 = zext i1 %cmp35 to i32
  %40 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %41 = load i32, i32* %nsymbs.addr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i16, i16* %40, i32 %41
  %42 = load i16, i16* %arrayidx37, align 2, !tbaa !101
  %conv38 = zext i16 %42 to i32
  %add39 = add nsw i32 %conv38, %conv36
  %conv40 = trunc i32 %add39 to i16
  store i16 %conv40, i16* %arrayidx37, align 2, !tbaa !101
  %43 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  %44 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  %45 = bitcast i32* %rate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  ret void
}

declare i32 @od_ec_decode_cdf_q15(%struct.od_ec_dec*, i16*, i32) #3

; Function Attrs: inlinehint nounwind
define internal void @aom_process_accounting(%struct.aom_reader* %r, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %acct_str.addr = alloca i8*, align 4
  %tell_frac = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !6
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %accounting = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting, align 4, !tbaa !102
  %cmp = icmp ne %struct.Accounting* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %tell_frac to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call = call i32 @aom_reader_tell_frac(%struct.aom_reader* %3)
  store i32 %call, i32* %tell_frac, align 4, !tbaa !2
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %accounting1 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %4, i32 0, i32 3
  %5 = load %struct.Accounting*, %struct.Accounting** %accounting1, align 4, !tbaa !102
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  %7 = load i32, i32* %tell_frac, align 4, !tbaa !2
  %8 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %accounting2 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %8, i32 0, i32 3
  %9 = load %struct.Accounting*, %struct.Accounting** %accounting2, align 4, !tbaa !102
  %last_tell_frac = getelementptr inbounds %struct.Accounting, %struct.Accounting* %9, i32 0, i32 4
  %10 = load i32, i32* %last_tell_frac, align 4, !tbaa !103
  %sub = sub i32 %7, %10
  call void @aom_accounting_record(%struct.Accounting* %5, i8* %6, i32 %sub)
  %11 = load i32, i32* %tell_frac, align 4, !tbaa !2
  %12 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %accounting3 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %12, i32 0, i32 3
  %13 = load %struct.Accounting*, %struct.Accounting** %accounting3, align 4, !tbaa !102
  %last_tell_frac4 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %13, i32 0, i32 4
  store i32 %11, i32* %last_tell_frac4, align 4, !tbaa !103
  %14 = bitcast i32* %tell_frac to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @aom_update_symb_counts(%struct.aom_reader* %r, i32 %is_binary) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %is_binary.addr = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i32 %is_binary, i32* %is_binary.addr, align 4, !tbaa !2
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %accounting = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting, align 4, !tbaa !102
  %cmp = icmp ne %struct.Accounting* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %is_binary.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %2, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %accounting1 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %3, i32 0, i32 3
  %4 = load %struct.Accounting*, %struct.Accounting** %accounting1, align 4, !tbaa !102
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %4, i32 0, i32 0
  %num_multi_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 2
  %5 = load i32, i32* %num_multi_syms, align 4, !tbaa !104
  %add = add nsw i32 %5, %lnot.ext
  store i32 %add, i32* %num_multi_syms, align 4, !tbaa !104
  %6 = load i32, i32* %is_binary.addr, align 4, !tbaa !2
  %tobool2 = icmp ne i32 %6, 0
  %lnot3 = xor i1 %tobool2, true
  %lnot5 = xor i1 %lnot3, true
  %lnot.ext6 = zext i1 %lnot5 to i32
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %accounting7 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %7, i32 0, i32 3
  %8 = load %struct.Accounting*, %struct.Accounting** %accounting7, align 4, !tbaa !102
  %syms8 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %8, i32 0, i32 0
  %num_binary_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms8, i32 0, i32 3
  %9 = load i32, i32* %num_binary_syms, align 4, !tbaa !105
  %add9 = add nsw i32 %9, %lnot.ext6
  store i32 %add9, i32* %num_binary_syms, align 4, !tbaa !105
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare i32 @aom_reader_tell_frac(%struct.aom_reader*) #3

declare void @aom_accounting_record(%struct.Accounting*, i8*, i32) #3

; Function Attrs: nounwind
define internal i32 @read_intra_segment_id(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %bsize, %struct.aom_reader* %r, i32 %skip) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bsize.addr = alloca i32, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %skip.addr = alloca i32, align 4
  %seg = alloca %struct.segmentation*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %mi_params = alloca %struct.CommonModeInfoParams*, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %mi_offset = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %x_mis = alloca i32, align 4
  %y_mis = alloca i32, align 4
  %segment_id = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32 %bsize, i32* %bsize.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i32 %skip, i32* %skip.addr, align 4, !tbaa !2
  %0 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 24
  store %struct.segmentation* %seg1, %struct.segmentation** %seg, align 4, !tbaa !6
  %2 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %enabled = getelementptr inbounds %struct.segmentation, %struct.segmentation* %2, i32 0, i32 0
  %3 = load i8, i8* %enabled, align 4, !tbaa !51
  %tobool = icmp ne i8 %3, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 22
  store %struct.CommonModeInfoParams* %mi_params2, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %6 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row3 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 0
  %8 = load i32, i32* %mi_row3, align 16, !tbaa !71
  store i32 %8, i32* %mi_row, align 4, !tbaa !2
  %9 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %10, i32 0, i32 1
  %11 = load i32, i32* %mi_col4, align 4, !tbaa !72
  store i32 %11, i32* %mi_col, align 4, !tbaa !2
  %12 = bitcast i32* %mi_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load i32, i32* %mi_row, align 4, !tbaa !2
  %14 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %14, i32 0, i32 4
  %15 = load i32, i32* %mi_cols, align 4, !tbaa !106
  %mul = mul nsw i32 %13, %15
  %16 = load i32, i32* %mi_col, align 4, !tbaa !2
  %add = add nsw i32 %mul, %16
  store i32 %add, i32* %mi_offset, align 4, !tbaa !2
  %17 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load i32, i32* %bsize.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %18
  %19 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %19 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !2
  %20 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load i32, i32* %bsize.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %21
  %22 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv6 = zext i8 %22 to i32
  store i32 %conv6, i32* %bh, align 4, !tbaa !2
  %23 = bitcast i32* %x_mis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_cols7 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %24, i32 0, i32 4
  %25 = load i32, i32* %mi_cols7, align 4, !tbaa !106
  %26 = load i32, i32* %mi_col, align 4, !tbaa !2
  %sub = sub nsw i32 %25, %26
  %27 = load i32, i32* %bw, align 4, !tbaa !2
  %cmp = icmp slt i32 %sub, %27
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %28 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_cols9 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %28, i32 0, i32 4
  %29 = load i32, i32* %mi_cols9, align 4, !tbaa !106
  %30 = load i32, i32* %mi_col, align 4, !tbaa !2
  %sub10 = sub nsw i32 %29, %30
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %31 = load i32, i32* %bw, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub10, %cond.true ], [ %31, %cond.false ]
  store i32 %cond, i32* %x_mis, align 4, !tbaa !2
  %32 = bitcast i32* %y_mis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  %33 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %33, i32 0, i32 3
  %34 = load i32, i32* %mi_rows, align 4, !tbaa !107
  %35 = load i32, i32* %mi_row, align 4, !tbaa !2
  %sub11 = sub nsw i32 %34, %35
  %36 = load i32, i32* %bh, align 4, !tbaa !2
  %cmp12 = icmp slt i32 %sub11, %36
  br i1 %cmp12, label %cond.true14, label %cond.false17

cond.true14:                                      ; preds = %cond.end
  %37 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_rows15 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %37, i32 0, i32 3
  %38 = load i32, i32* %mi_rows15, align 4, !tbaa !107
  %39 = load i32, i32* %mi_row, align 4, !tbaa !2
  %sub16 = sub nsw i32 %38, %39
  br label %cond.end18

cond.false17:                                     ; preds = %cond.end
  %40 = load i32, i32* %bh, align 4, !tbaa !2
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false17, %cond.true14
  %cond19 = phi i32 [ %sub16, %cond.true14 ], [ %40, %cond.false17 ]
  store i32 %cond19, i32* %y_mis, align 4, !tbaa !2
  %41 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  %42 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %43 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %44 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %45 = load i32, i32* %skip.addr, align 4, !tbaa !2
  %call = call i32 @read_segment_id(%struct.AV1Common* %42, %struct.macroblockd* %43, %struct.aom_reader* %44, i32 %45)
  store i32 %call, i32* %segment_id, align 4, !tbaa !2
  %46 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %47 = load i32, i32* %mi_offset, align 4, !tbaa !2
  %48 = load i32, i32* %x_mis, align 4, !tbaa !2
  %49 = load i32, i32* %y_mis, align 4, !tbaa !2
  %50 = load i32, i32* %segment_id, align 4, !tbaa !2
  call void @set_segment_id(%struct.AV1Common* %46, i32 %47, i32 %48, i32 %49, i32 %50)
  %51 = load i32, i32* %segment_id, align 4, !tbaa !2
  store i32 %51, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %52 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  %53 = bitcast i32* %y_mis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  %54 = bitcast i32* %x_mis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #6
  %56 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  %57 = bitcast i32* %mi_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  %58 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  br label %cleanup

cleanup:                                          ; preds = %cond.end18, %if.then
  %61 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = load i32, i32* %retval, align 4
  ret i32 %62
}

; Function Attrs: nounwind
define internal i32 @read_skip(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %segment_id, %struct.aom_reader* %r) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %segment_id.addr = alloca i32, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %ctx = alloca i32, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %skip = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 24
  %1 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call = call i32 @segfeature_active(%struct.segmentation* %seg, i32 %1, i8 zeroext 6)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %2 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call1 = call i32 @av1_get_skip_context(%struct.macroblockd* %3)
  store i32 %call1, i32* %ctx, align 4, !tbaa !2
  %4 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 40
  %6 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %6, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %7 = bitcast i32* %skip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %9 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %skip_cdfs = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %9, i32 0, i32 41
  %10 = load i32, i32* %ctx, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %skip_cdfs, i32 0, i32 %10
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx, i32 0, i32 0
  %call2 = call i32 @aom_read_symbol_(%struct.aom_reader* %8, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @__func__.read_skip, i32 0, i32 0))
  store i32 %call2, i32* %skip, align 4, !tbaa !2
  %11 = load i32, i32* %skip, align 4, !tbaa !2
  store i32 %11, i32* %retval, align 4
  %12 = bitcast i32* %skip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %return

return:                                           ; preds = %if.else, %if.then
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

; Function Attrs: nounwind
define internal void @read_cdef(%struct.AV1Common* %cm, %struct.aom_reader* %r, %struct.macroblockd* %xd) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %skip = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %sb_mask = alloca i32, align 4
  %mi_row_in_sb = alloca i32, align 4
  %mi_col_in_sb = alloca i32, align 4
  %cdef_size = alloca i32, align 4
  %index_mask = alloca i32, align 4
  %cdef_unit_row_in_sb = alloca i32, align 4
  %cdef_unit_col_in_sb = alloca i32, align 4
  %index = alloca i32, align 4
  %first_block_mask = alloca i32, align 4
  %mi_params = alloca %struct.CommonModeInfoParams*, align 4
  %grid_idx = alloca i32, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %skip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  %skip1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %3, i32 0, i32 14
  %4 = load i8, i8* %skip1, align 4, !tbaa !17
  %conv = sext i8 %4 to i32
  store i32 %conv, i32* %skip, align 4, !tbaa !2
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 21
  %coded_lossless = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 7
  %6 = load i8, i8* %coded_lossless, align 1, !tbaa !108, !range !46
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 21
  %allow_intrabc = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features2, i32 0, i32 4
  %8 = load i8, i8* %allow_intrabc, align 4, !tbaa !109, !range !46
  %tobool3 = trunc i8 %8 to i1
  br i1 %tobool3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %if.end
  %9 = bitcast i32* %sb_mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 37
  %mib_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 8
  %11 = load i32, i32* %mib_size, align 16, !tbaa !110
  %sub = sub nsw i32 %11, 1
  store i32 %sub, i32* %sb_mask, align 4, !tbaa !2
  %12 = bitcast i32* %mi_row_in_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %13, i32 0, i32 0
  %14 = load i32, i32* %mi_row, align 16, !tbaa !71
  %15 = load i32, i32* %sb_mask, align 4, !tbaa !2
  %and = and i32 %14, %15
  store i32 %and, i32* %mi_row_in_sb, align 4, !tbaa !2
  %16 = bitcast i32* %mi_col_in_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %17, i32 0, i32 1
  %18 = load i32, i32* %mi_col, align 4, !tbaa !72
  %19 = load i32, i32* %sb_mask, align 4, !tbaa !2
  %and6 = and i32 %18, %19
  store i32 %and6, i32* %mi_col_in_sb, align 4, !tbaa !2
  %20 = load i32, i32* %mi_row_in_sb, align 4, !tbaa !2
  %cmp = icmp eq i32 %20, 0
  br i1 %cmp, label %land.lhs.true, label %if.end18

land.lhs.true:                                    ; preds = %if.end5
  %21 = load i32, i32* %mi_col_in_sb, align 4, !tbaa !2
  %cmp8 = icmp eq i32 %21, 0
  br i1 %cmp8, label %if.then10, label %if.end18

if.then10:                                        ; preds = %land.lhs.true
  %22 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cdef_transmitted = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %22, i32 0, i32 52
  %arrayidx11 = getelementptr inbounds [4 x i8], [4 x i8]* %cdef_transmitted, i32 0, i32 3
  store i8 0, i8* %arrayidx11, align 1, !tbaa !111
  %23 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cdef_transmitted12 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %23, i32 0, i32 52
  %arrayidx13 = getelementptr inbounds [4 x i8], [4 x i8]* %cdef_transmitted12, i32 0, i32 2
  store i8 0, i8* %arrayidx13, align 1, !tbaa !111
  %24 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cdef_transmitted14 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %24, i32 0, i32 52
  %arrayidx15 = getelementptr inbounds [4 x i8], [4 x i8]* %cdef_transmitted14, i32 0, i32 1
  store i8 0, i8* %arrayidx15, align 1, !tbaa !111
  %25 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cdef_transmitted16 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %25, i32 0, i32 52
  %arrayidx17 = getelementptr inbounds [4 x i8], [4 x i8]* %cdef_transmitted16, i32 0, i32 0
  store i8 0, i8* %arrayidx17, align 1, !tbaa !111
  br label %if.end18

if.end18:                                         ; preds = %if.then10, %land.lhs.true, %if.end5
  %26 = bitcast i32* %cdef_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  store i32 16, i32* %cdef_size, align 4, !tbaa !2
  %27 = bitcast i32* %index_mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  store i32 16, i32* %index_mask, align 4, !tbaa !2
  %28 = bitcast i32* %cdef_unit_row_in_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %29 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row19 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %29, i32 0, i32 0
  %30 = load i32, i32* %mi_row19, align 16, !tbaa !71
  %and20 = and i32 %30, 16
  %cmp21 = icmp ne i32 %and20, 0
  %conv22 = zext i1 %cmp21 to i32
  store i32 %conv22, i32* %cdef_unit_row_in_sb, align 4, !tbaa !2
  %31 = bitcast i32* %cdef_unit_col_in_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col23 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %32, i32 0, i32 1
  %33 = load i32, i32* %mi_col23, align 4, !tbaa !72
  %and24 = and i32 %33, 16
  %cmp25 = icmp ne i32 %and24, 0
  %conv26 = zext i1 %cmp25 to i32
  store i32 %conv26, i32* %cdef_unit_col_in_sb, align 4, !tbaa !2
  %34 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %35 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params27 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %35, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params27, i32 0, i32 7
  %36 = load i8, i8* %sb_size, align 4, !tbaa !112
  %conv28 = zext i8 %36 to i32
  %cmp29 = icmp eq i32 %conv28, 15
  br i1 %cmp29, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end18
  %37 = load i32, i32* %cdef_unit_col_in_sb, align 4, !tbaa !2
  %38 = load i32, i32* %cdef_unit_row_in_sb, align 4, !tbaa !2
  %mul = mul nsw i32 2, %38
  %add = add nsw i32 %37, %mul
  br label %cond.end

cond.false:                                       ; preds = %if.end18
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %index, align 4, !tbaa !2
  %39 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cdef_transmitted31 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %39, i32 0, i32 52
  %40 = load i32, i32* %index, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds [4 x i8], [4 x i8]* %cdef_transmitted31, i32 0, i32 %40
  %41 = load i8, i8* %arrayidx32, align 1, !tbaa !111, !range !46
  %tobool33 = trunc i8 %41 to i1
  br i1 %tobool33, label %if.end47, label %land.lhs.true34

land.lhs.true34:                                  ; preds = %cond.end
  %42 = load i32, i32* %skip, align 4, !tbaa !2
  %tobool35 = icmp ne i32 %42, 0
  br i1 %tobool35, label %if.end47, label %if.then36

if.then36:                                        ; preds = %land.lhs.true34
  %43 = bitcast i32* %first_block_mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  store i32 -16, i32* %first_block_mask, align 4, !tbaa !2
  %44 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #6
  %45 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params37 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %45, i32 0, i32 22
  store %struct.CommonModeInfoParams* %mi_params37, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %46 = bitcast i32* %grid_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #6
  %47 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %48 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row38 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %48, i32 0, i32 0
  %49 = load i32, i32* %mi_row38, align 16, !tbaa !71
  %and39 = and i32 %49, -16
  %50 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col40 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %50, i32 0, i32 1
  %51 = load i32, i32* %mi_col40, align 4, !tbaa !72
  %and41 = and i32 %51, -16
  %call = call i32 @get_mi_grid_idx(%struct.CommonModeInfoParams* %47, i32 %and39, i32 %and41)
  store i32 %call, i32* %grid_idx, align 4, !tbaa !2
  %52 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_grid_base = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %53, i32 0, i32 9
  %54 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base, align 4, !tbaa !113
  %55 = load i32, i32* %grid_idx, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %54, i32 %55
  %56 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx42, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %56, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %57 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %58 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %cdef_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %58, i32 0, i32 33
  %cdef_bits = getelementptr inbounds %struct.CdefInfo, %struct.CdefInfo* %cdef_info, i32 0, i32 4
  %59 = load i32, i32* %cdef_bits, align 4, !tbaa !114
  %call43 = call i32 @aom_read_literal_(%struct.aom_reader* %57, i32 %59, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @__func__.read_cdef, i32 0, i32 0))
  %conv44 = trunc i32 %call43 to i8
  %60 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %cdef_strength = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %60, i32 0, i32 27
  %bf.load = load i8, i8* %cdef_strength, align 1
  %bf.value = and i8 %conv44, 15
  %bf.shl = shl i8 %bf.value, 3
  %bf.clear = and i8 %bf.load, -121
  %bf.set = or i8 %bf.clear, %bf.shl
  store i8 %bf.set, i8* %cdef_strength, align 1
  %bf.result.shl = shl i8 %bf.value, 4
  %bf.result.ashr = ashr i8 %bf.result.shl, 4
  %61 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cdef_transmitted45 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %61, i32 0, i32 52
  %62 = load i32, i32* %index, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds [4 x i8], [4 x i8]* %cdef_transmitted45, i32 0, i32 %62
  store i8 1, i8* %arrayidx46, align 1, !tbaa !111
  %63 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast i32* %grid_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  %66 = bitcast i32* %first_block_mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  br label %if.end47

if.end47:                                         ; preds = %if.then36, %land.lhs.true34, %cond.end
  %67 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast i32* %cdef_unit_col_in_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast i32* %cdef_unit_row_in_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast i32* %index_mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  %71 = bitcast i32* %cdef_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  %72 = bitcast i32* %mi_col_in_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #6
  %73 = bitcast i32* %mi_row_in_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #6
  %74 = bitcast i32* %sb_mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end47, %if.then4, %if.then
  %75 = bitcast i32* %skip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @read_delta_q_params(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %delta_q_info = alloca %struct.DeltaQInfo*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %frame_lf_count = alloca i32, align 4
  %lf_id = alloca i32, align 4
  %tmp_lvl = alloca i32, align 4
  %tmp_lvl24 = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.DeltaQInfo** %delta_q_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %delta_q_info1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 35
  store %struct.DeltaQInfo* %delta_q_info1, %struct.DeltaQInfo** %delta_q_info, align 4, !tbaa !6
  %2 = load %struct.DeltaQInfo*, %struct.DeltaQInfo** %delta_q_info, align 4, !tbaa !6
  %delta_q_present_flag = getelementptr inbounds %struct.DeltaQInfo, %struct.DeltaQInfo* %2, i32 0, i32 0
  %3 = load i32, i32* %delta_q_present_flag, align 4, !tbaa !115
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.end36

if.then:                                          ; preds = %entry
  %4 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 6
  %6 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %6, i32 0
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %7, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %10 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %call = call i32 @read_delta_qindex(%struct.AV1Common* %8, %struct.macroblockd* %9, %struct.aom_reader* %10, %struct.MB_MODE_INFO* %11)
  %12 = load %struct.DeltaQInfo*, %struct.DeltaQInfo** %delta_q_info, align 4, !tbaa !6
  %delta_q_res = getelementptr inbounds %struct.DeltaQInfo, %struct.DeltaQInfo* %12, i32 0, i32 1
  %13 = load i32, i32* %delta_q_res, align 4, !tbaa !116
  %mul = mul nsw i32 %call, %13
  %14 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %current_qindex = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %14, i32 0, i32 49
  %15 = load i32, i32* %current_qindex, align 4, !tbaa !78
  %add = add nsw i32 %15, %mul
  store i32 %add, i32* %current_qindex, align 4, !tbaa !78
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %current_qindex2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %16, i32 0, i32 49
  %17 = load i32, i32* %current_qindex2, align 4, !tbaa !78
  %call3 = call i32 @clamp(i32 %17, i32 1, i32 255)
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %current_qindex4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %18, i32 0, i32 49
  store i32 %call3, i32* %current_qindex4, align 4, !tbaa !78
  %19 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %20, i32 0, i32 40
  %21 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %21, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %22 = load %struct.DeltaQInfo*, %struct.DeltaQInfo** %delta_q_info, align 4, !tbaa !6
  %delta_lf_present_flag = getelementptr inbounds %struct.DeltaQInfo, %struct.DeltaQInfo* %22, i32 0, i32 2
  %23 = load i32, i32* %delta_lf_present_flag, align 4, !tbaa !117
  %tobool5 = icmp ne i32 %23, 0
  br i1 %tobool5, label %if.then6, label %if.end35

if.then6:                                         ; preds = %if.then
  %24 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row7 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %25, i32 0, i32 0
  %26 = load i32, i32* %mi_row7, align 16, !tbaa !71
  store i32 %26, i32* %mi_row, align 4, !tbaa !2
  %27 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %28, i32 0, i32 1
  %29 = load i32, i32* %mi_col8, align 4, !tbaa !72
  store i32 %29, i32* %mi_col, align 4, !tbaa !2
  %30 = load %struct.DeltaQInfo*, %struct.DeltaQInfo** %delta_q_info, align 4, !tbaa !6
  %delta_lf_multi = getelementptr inbounds %struct.DeltaQInfo, %struct.DeltaQInfo* %30, i32 0, i32 4
  %31 = load i32, i32* %delta_lf_multi, align 4, !tbaa !118
  %tobool9 = icmp ne i32 %31, 0
  br i1 %tobool9, label %if.then10, label %if.else

if.then10:                                        ; preds = %if.then6
  %32 = bitcast i32* %frame_lf_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  %33 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call11 = call i32 @av1_num_planes(%struct.AV1Common* %33)
  %cmp = icmp sgt i32 %call11, 1
  %34 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 4, i32 2
  store i32 %cond, i32* %frame_lf_count, align 4, !tbaa !2
  %35 = bitcast i32* %lf_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  store i32 0, i32* %lf_id, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then10
  %36 = load i32, i32* %lf_id, align 4, !tbaa !2
  %37 = load i32, i32* %frame_lf_count, align 4, !tbaa !2
  %cmp12 = icmp slt i32 %36, %37
  br i1 %cmp12, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %38 = bitcast i32* %lf_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %39 = bitcast i32* %tmp_lvl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  %40 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %delta_lf = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %40, i32 0, i32 51
  %41 = load i32, i32* %lf_id, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds [4 x i8], [4 x i8]* %delta_lf, i32 0, i32 %41
  %42 = load i8, i8* %arrayidx13, align 1, !tbaa !8
  %conv = sext i8 %42 to i32
  %43 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %44 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %45 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %delta_lf_multi_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %45, i32 0, i32 60
  %46 = load i32, i32* %lf_id, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds [4 x [5 x i16]], [4 x [5 x i16]]* %delta_lf_multi_cdf, i32 0, i32 %46
  %arraydecay = getelementptr inbounds [5 x i16], [5 x i16]* %arrayidx14, i32 0, i32 0
  %47 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %48 = load i32, i32* %mi_col, align 4, !tbaa !2
  %49 = load i32, i32* %mi_row, align 4, !tbaa !2
  %call15 = call i32 @read_delta_lflevel(%struct.AV1Common* %43, %struct.aom_reader* %44, i16* %arraydecay, %struct.MB_MODE_INFO* %47, i32 %48, i32 %49)
  %50 = load %struct.DeltaQInfo*, %struct.DeltaQInfo** %delta_q_info, align 4, !tbaa !6
  %delta_lf_res = getelementptr inbounds %struct.DeltaQInfo, %struct.DeltaQInfo* %50, i32 0, i32 3
  %51 = load i32, i32* %delta_lf_res, align 4, !tbaa !119
  %mul16 = mul nsw i32 %call15, %51
  %add17 = add nsw i32 %conv, %mul16
  store i32 %add17, i32* %tmp_lvl, align 4, !tbaa !2
  %52 = load i32, i32* %tmp_lvl, align 4, !tbaa !2
  %call18 = call i32 @clamp(i32 %52, i32 -63, i32 63)
  %conv19 = trunc i32 %call18 to i8
  %53 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %delta_lf20 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %53, i32 0, i32 51
  %54 = load i32, i32* %lf_id, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds [4 x i8], [4 x i8]* %delta_lf20, i32 0, i32 %54
  store i8 %conv19, i8* %arrayidx21, align 1, !tbaa !8
  %55 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %delta_lf22 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %55, i32 0, i32 18
  %56 = load i32, i32* %lf_id, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds [4 x i8], [4 x i8]* %delta_lf22, i32 0, i32 %56
  store i8 %conv19, i8* %arrayidx23, align 1, !tbaa !8
  %57 = bitcast i32* %tmp_lvl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %58 = load i32, i32* %lf_id, align 4, !tbaa !2
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %lf_id, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %59 = bitcast i32* %frame_lf_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  br label %if.end

if.else:                                          ; preds = %if.then6
  %60 = bitcast i32* %tmp_lvl24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #6
  %61 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %delta_lf_from_base = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %61, i32 0, i32 50
  %62 = load i8, i8* %delta_lf_from_base, align 16, !tbaa !120
  %conv25 = sext i8 %62 to i32
  %63 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %64 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %65 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %delta_lf_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %65, i32 0, i32 61
  %arraydecay26 = getelementptr inbounds [5 x i16], [5 x i16]* %delta_lf_cdf, i32 0, i32 0
  %66 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %67 = load i32, i32* %mi_col, align 4, !tbaa !2
  %68 = load i32, i32* %mi_row, align 4, !tbaa !2
  %call27 = call i32 @read_delta_lflevel(%struct.AV1Common* %63, %struct.aom_reader* %64, i16* %arraydecay26, %struct.MB_MODE_INFO* %66, i32 %67, i32 %68)
  %69 = load %struct.DeltaQInfo*, %struct.DeltaQInfo** %delta_q_info, align 4, !tbaa !6
  %delta_lf_res28 = getelementptr inbounds %struct.DeltaQInfo, %struct.DeltaQInfo* %69, i32 0, i32 3
  %70 = load i32, i32* %delta_lf_res28, align 4, !tbaa !119
  %mul29 = mul nsw i32 %call27, %70
  %add30 = add nsw i32 %conv25, %mul29
  store i32 %add30, i32* %tmp_lvl24, align 4, !tbaa !2
  %71 = load i32, i32* %tmp_lvl24, align 4, !tbaa !2
  %call31 = call i32 @clamp(i32 %71, i32 -63, i32 63)
  %conv32 = trunc i32 %call31 to i8
  %72 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %delta_lf_from_base33 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %72, i32 0, i32 50
  store i8 %conv32, i8* %delta_lf_from_base33, align 16, !tbaa !120
  %73 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %delta_lf_from_base34 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %73, i32 0, i32 17
  store i8 %conv32, i8* %delta_lf_from_base34, align 2, !tbaa !121
  %74 = bitcast i32* %tmp_lvl24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #6
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end
  %75 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #6
  %76 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  br label %if.end35

if.end35:                                         ; preds = %if.end, %if.then
  %77 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #6
  %78 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #6
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %entry
  %79 = bitcast %struct.DeltaQInfo** %delta_q_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_allow_intrabc(%struct.AV1Common* %cm) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call = call i32 @frame_is_intra_only(%struct.AV1Common* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 21
  %allow_screen_content_tools = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 3
  %2 = load i8, i8* %allow_screen_content_tools, align 1, !tbaa !89, !range !46
  %tobool1 = trunc i8 %2 to i1
  br i1 %tobool1, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 21
  %allow_intrabc = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features2, i32 0, i32 4
  %4 = load i8, i8* %allow_intrabc, align 4, !tbaa !109, !range !46
  %tobool3 = trunc i8 %4 to i1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %entry
  %5 = phi i1 [ false, %land.lhs.true ], [ false, %entry ], [ %tobool3, %land.rhs ]
  %land.ext = zext i1 %5 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define internal void @read_intrabc_info(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %bsize = alloca i8, align 1
  %tmp = alloca %union.int_interpfilters, align 4
  %inter_mode_ctx = alloca [29 x i16], align 16
  %ref_mvs = alloca [1 x [2 x %union.int_mv]], align 4
  %nearestmv = alloca %union.int_mv, align 4
  %nearmv = alloca %union.int_mv, align 4
  %dv_ref = alloca %union.int_mv, align 4
  %valid_dv = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %4 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 40
  %6 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %6, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %8 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %intrabc_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %8, i32 0, i32 45
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %intrabc_cdf, i32 0, i32 0
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %7, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_intrabc_info, i32 0, i32 0))
  %conv = trunc i32 %call to i8
  %9 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %9, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.value = and i8 %conv, 1
  %bf.shl = shl i8 %bf.value, 6
  %bf.clear = and i8 %bf.load, -65
  %bf.set = or i8 %bf.clear, %bf.shl
  store i8 %bf.set, i8* %use_intrabc, align 4
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %use_intrabc1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %10, i32 0, i32 26
  %bf.load2 = load i8, i8* %use_intrabc1, align 4
  %bf.lshr = lshr i8 %bf.load2, 6
  %bf.clear3 = and i8 %bf.lshr, 1
  %tobool = icmp ne i8 %bf.clear3, 0
  br i1 %tobool, label %if.then, label %if.end49

if.then:                                          ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %11, i32 0, i32 6
  %12 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %12, i8* %bsize, align 1, !tbaa !8
  %13 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %13, i32 0, i32 7
  store i8 0, i8* %mode, align 1, !tbaa !50
  %14 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %uv_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %14, i32 0, i32 8
  store i8 0, i8* %uv_mode, align 4, !tbaa !86
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %interp_filters = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %15, i32 0, i32 4
  %16 = bitcast %union.int_interpfilters* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  call void @av1_broadcast_interp_filter(%union.int_interpfilters* sret align 4 %tmp, i8 zeroext 3)
  %17 = bitcast %union.int_interpfilters* %interp_filters to i8*
  %18 = bitcast %union.int_interpfilters* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 4, i1 false), !tbaa.struct !122
  %19 = bitcast %union.int_interpfilters* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %motion_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %20, i32 0, i32 10
  store i8 0, i8* %motion_mode, align 2, !tbaa !123
  %21 = bitcast [29 x i16]* %inter_mode_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 58, i8* %21) #6
  %22 = bitcast [1 x [2 x %union.int_mv]]* %ref_mvs to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %22) #6
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %24 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %25 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %26 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_count = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %26, i32 0, i32 35
  %arraydecay4 = getelementptr inbounds [29 x i8], [29 x i8]* %ref_mv_count, i32 0, i32 0
  %27 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %27, i32 0, i32 36
  %arraydecay5 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack, i32 0, i32 0
  %28 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %weight = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %28, i32 0, i32 37
  %arraydecay6 = getelementptr inbounds [29 x [8 x i16]], [29 x [8 x i16]]* %weight, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [1 x [2 x %union.int_mv]], [1 x [2 x %union.int_mv]]* %ref_mvs, i32 0, i32 0
  %arraydecay8 = getelementptr inbounds [29 x i16], [29 x i16]* %inter_mode_ctx, i32 0, i32 0
  call void @av1_find_mv_refs(%struct.AV1Common* %23, %struct.macroblockd* %24, %struct.MB_MODE_INFO* %25, i8 signext 0, i8* %arraydecay4, [8 x %struct.candidate_mv]* %arraydecay5, [8 x i16]* %arraydecay6, [2 x %union.int_mv]* %arraydecay7, %union.int_mv* null, i16* %arraydecay8)
  %29 = bitcast %union.int_mv* %nearestmv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = bitcast %union.int_mv* %nearmv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %arrayidx9 = getelementptr inbounds [1 x [2 x %union.int_mv]], [1 x [2 x %union.int_mv]]* %ref_mvs, i32 0, i32 0
  %arraydecay10 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx9, i32 0, i32 0
  call void @av1_find_best_ref_mvs(i32 0, %union.int_mv* %arraydecay10, %union.int_mv* %nearestmv, %union.int_mv* %nearmv, i32 0)
  %31 = bitcast %union.int_mv* %dv_ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %as_int = bitcast %union.int_mv* %nearestmv to i32*
  %32 = load i32, i32* %as_int, align 4, !tbaa !8
  %cmp = icmp eq i32 %32, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %33 = bitcast %union.int_mv* %dv_ref to i8*
  %34 = bitcast %union.int_mv* %nearmv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 4, i1 false), !tbaa.struct !124
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %35 = bitcast %union.int_mv* %dv_ref to i8*
  %36 = bitcast %union.int_mv* %nearestmv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 4, i1 false), !tbaa.struct !124
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %as_int12 = bitcast %union.int_mv* %dv_ref to i32*
  %37 = load i32, i32* %as_int12, align 4, !tbaa !8
  %cmp13 = icmp eq i32 %37, 0
  br i1 %cmp13, label %if.then15, label %if.end

if.then15:                                        ; preds = %cond.end
  %38 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %38, i32 0, i32 5
  %39 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %39, i32 0, i32 37
  %mib_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 8
  %40 = load i32, i32* %mib_size, align 16, !tbaa !110
  %41 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %41, i32 0, i32 0
  %42 = load i32, i32* %mi_row, align 16, !tbaa !71
  call void @av1_find_ref_dv(%union.int_mv* %dv_ref, %struct.TileInfo* %tile, i32 %40, i32 %42)
  br label %if.end

if.end:                                           ; preds = %if.then15, %cond.end
  %43 = bitcast i32* %valid_dv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  %as_mv = bitcast %union.int_mv* %dv_ref to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv, i32 0, i32 1
  %44 = load i16, i16* %col, align 2, !tbaa !8
  %conv16 = sext i16 %44 to i32
  %and = and i32 %conv16, 7
  %cmp17 = icmp eq i32 %and, 0
  br i1 %cmp17, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end
  %as_mv19 = bitcast %union.int_mv* %dv_ref to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv19, i32 0, i32 0
  %45 = load i16, i16* %row, align 4, !tbaa !8
  %conv20 = sext i16 %45 to i32
  %and21 = and i32 %conv20, 7
  %cmp22 = icmp eq i32 %and21, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end
  %46 = phi i1 [ false, %if.end ], [ %cmp22, %land.rhs ]
  %land.ext = zext i1 %46 to i32
  store i32 %land.ext, i32* %valid_dv, align 4, !tbaa !2
  %as_mv24 = bitcast %union.int_mv* %dv_ref to %struct.mv*
  %col25 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv24, i32 0, i32 1
  %47 = load i16, i16* %col25, align 2, !tbaa !8
  %conv26 = sext i16 %47 to i32
  %shr = ashr i32 %conv26, 3
  %mul = mul nsw i32 %shr, 8
  %conv27 = trunc i32 %mul to i16
  %as_mv28 = bitcast %union.int_mv* %dv_ref to %struct.mv*
  %col29 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv28, i32 0, i32 1
  store i16 %conv27, i16* %col29, align 2, !tbaa !8
  %as_mv30 = bitcast %union.int_mv* %dv_ref to %struct.mv*
  %row31 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv30, i32 0, i32 0
  %48 = load i16, i16* %row31, align 4, !tbaa !8
  %conv32 = sext i16 %48 to i32
  %shr33 = ashr i32 %conv32, 3
  %mul34 = mul nsw i32 %shr33, 8
  %conv35 = trunc i32 %mul34 to i16
  %as_mv36 = bitcast %union.int_mv* %dv_ref to %struct.mv*
  %row37 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv36, i32 0, i32 0
  store i16 %conv35, i16* %row37, align 4, !tbaa !8
  %49 = load i32, i32* %valid_dv, align 4, !tbaa !2
  %tobool38 = icmp ne i32 %49, 0
  br i1 %tobool38, label %land.rhs39, label %land.end44

land.rhs39:                                       ; preds = %land.end
  %50 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %51 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %52 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mv = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %52, i32 0, i32 2
  %arrayidx40 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv, i32 0, i32 0
  %53 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row41 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %53, i32 0, i32 0
  %54 = load i32, i32* %mi_row41, align 16, !tbaa !71
  %55 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %55, i32 0, i32 1
  %56 = load i32, i32* %mi_col, align 4, !tbaa !72
  %57 = load i8, i8* %bsize, align 1, !tbaa !8
  %58 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call42 = call i32 @assign_dv(%struct.AV1Common* %50, %struct.macroblockd* %51, %union.int_mv* %arrayidx40, %union.int_mv* %dv_ref, i32 %54, i32 %56, i8 zeroext %57, %struct.aom_reader* %58)
  %tobool43 = icmp ne i32 %call42, 0
  br label %land.end44

land.end44:                                       ; preds = %land.rhs39, %land.end
  %59 = phi i1 [ false, %land.end ], [ %tobool43, %land.rhs39 ]
  %land.ext45 = zext i1 %59 to i32
  store i32 %land.ext45, i32* %valid_dv, align 4, !tbaa !2
  %60 = load i32, i32* %valid_dv, align 4, !tbaa !2
  %tobool46 = icmp ne i32 %60, 0
  br i1 %tobool46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %land.end44
  %61 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %error_info = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %61, i32 0, i32 46
  %62 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info, align 16, !tbaa !125
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %62, i32 7, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end48

if.end48:                                         ; preds = %if.then47, %land.end44
  %63 = bitcast i32* %valid_dv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast %union.int_mv* %dv_ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = bitcast %union.int_mv* %nearmv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  %66 = bitcast %union.int_mv* %nearestmv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = bitcast [1 x [2 x %union.int_mv]]* %ref_mvs to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %67) #6
  %68 = bitcast [29 x i16]* %inter_mode_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 58, i8* %68) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %entry
  %69 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  ret void
}

; Function Attrs: nounwind
define internal zeroext i8 @read_intra_mode(%struct.aom_reader* %r, i16* %cdf) #0 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %cdf.addr = alloca i16*, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !6
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %1 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %0, i16* %1, i32 13, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_intra_mode, i32 0, i32 0))
  %conv = trunc i32 %call to i8
  ret i8 %conv
}

; Function Attrs: inlinehint nounwind
define internal i16* @get_y_mode_cdf(%struct.frame_contexts* %tile_ctx, %struct.MB_MODE_INFO* %above_mi, %struct.MB_MODE_INFO* %left_mi) #2 {
entry:
  %tile_ctx.addr = alloca %struct.frame_contexts*, align 4
  %above_mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %left_mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %above = alloca i8, align 1
  %left = alloca i8, align 1
  %above_ctx = alloca i32, align 4
  %left_ctx = alloca i32, align 4
  store %struct.frame_contexts* %tile_ctx, %struct.frame_contexts** %tile_ctx.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %above_mi, %struct.MB_MODE_INFO** %above_mi.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %left_mi, %struct.MB_MODE_INFO** %left_mi.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %above) #6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi.addr, align 4, !tbaa !6
  %call = call zeroext i8 @av1_above_block_mode(%struct.MB_MODE_INFO* %0)
  store i8 %call, i8* %above, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %left) #6
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi.addr, align 4, !tbaa !6
  %call1 = call zeroext i8 @av1_left_block_mode(%struct.MB_MODE_INFO* %1)
  store i8 %call1, i8* %left, align 1, !tbaa !8
  %2 = bitcast i32* %above_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i8, i8* %above, align 1, !tbaa !8
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [13 x i32], [13 x i32]* @intra_mode_context, i32 0, i32 %idxprom
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !2
  store i32 %4, i32* %above_ctx, align 4, !tbaa !2
  %5 = bitcast i32* %left_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i8, i8* %left, align 1, !tbaa !8
  %idxprom2 = zext i8 %6 to i32
  %arrayidx3 = getelementptr inbounds [13 x i32], [13 x i32]* @intra_mode_context, i32 0, i32 %idxprom2
  %7 = load i32, i32* %arrayidx3, align 4, !tbaa !2
  store i32 %7, i32* %left_ctx, align 4, !tbaa !2
  %8 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx.addr, align 4, !tbaa !6
  %kf_y_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %8, i32 0, i32 56
  %9 = load i32, i32* %above_ctx, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds [5 x [5 x [14 x i16]]], [5 x [5 x [14 x i16]]]* %kf_y_cdf, i32 0, i32 %9
  %10 = load i32, i32* %left_ctx, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds [5 x [14 x i16]], [5 x [14 x i16]]* %arrayidx4, i32 0, i32 %10
  %arraydecay = getelementptr inbounds [14 x i16], [14 x i16]* %arrayidx5, i32 0, i32 0
  %11 = bitcast i32* %left_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast i32* %above_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %left) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %above) #6
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_use_angle_delta(i8 zeroext %bsize) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 3
  %conv1 = zext i1 %cmp to i32
  ret i32 %conv1
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_directional_mode(i8 zeroext %mode) #2 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp sle i32 %conv2, 8
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  %land.ext = zext i1 %2 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define internal i32 @read_angle_delta(%struct.aom_reader* %r, i16* %cdf) #0 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %cdf.addr = alloca i16*, align 4
  %sym = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !6
  %0 = bitcast i32* %sym to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %1, i16* %2, i32 7, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @__func__.read_angle_delta, i32 0, i32 0))
  store i32 %call, i32* %sym, align 4, !tbaa !2
  %3 = load i32, i32* %sym, align 4, !tbaa !2
  %sub = sub nsw i32 %3, 3
  %4 = bitcast i32* %sym to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  ret i32 %sub
}

; Function Attrs: nounwind
define internal zeroext i8 @read_intra_mode_uv(%struct.frame_contexts* %ec_ctx, %struct.aom_reader* %r, i8 zeroext %cfl_allowed, i8 zeroext %y_mode) #0 {
entry:
  %ec_ctx.addr = alloca %struct.frame_contexts*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %cfl_allowed.addr = alloca i8, align 1
  %y_mode.addr = alloca i8, align 1
  %uv_mode = alloca i8, align 1
  store %struct.frame_contexts* %ec_ctx, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i8 %cfl_allowed, i8* %cfl_allowed.addr, align 1, !tbaa !8
  store i8 %y_mode, i8* %y_mode.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %uv_mode) #6
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  %uv_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %1, i32 0, i32 53
  %2 = load i8, i8* %cfl_allowed.addr, align 1, !tbaa !8
  %idxprom = zext i8 %2 to i32
  %arrayidx = getelementptr inbounds [2 x [13 x [15 x i16]]], [2 x [13 x [15 x i16]]]* %uv_mode_cdf, i32 0, i32 %idxprom
  %3 = load i8, i8* %y_mode.addr, align 1, !tbaa !8
  %idxprom1 = zext i8 %3 to i32
  %arrayidx2 = getelementptr inbounds [13 x [15 x i16]], [13 x [15 x i16]]* %arrayidx, i32 0, i32 %idxprom1
  %arraydecay = getelementptr inbounds [15 x i16], [15 x i16]* %arrayidx2, i32 0, i32 0
  %4 = load i8, i8* %cfl_allowed.addr, align 1, !tbaa !8
  %tobool = icmp ne i8 %4, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %sub = sub nsw i32 14, %lnot.ext
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %0, i16* %arraydecay, i32 %sub, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @__func__.read_intra_mode_uv, i32 0, i32 0))
  %conv = trunc i32 %call to i8
  store i8 %conv, i8* %uv_mode, align 1, !tbaa !8
  %5 = load i8, i8* %uv_mode, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %uv_mode) #6
  ret i8 %5
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @is_cfl_allowed(%struct.macroblockd* %xd) #2 {
entry:
  %retval = alloca i8, align 1
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %bsize = alloca i8, align 1
  %ssx = alloca i32, align 4
  %ssy = alloca i32, align 4
  %plane_bsize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %4, i32 0, i32 6
  %5 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %5, i8* %bsize, align 1, !tbaa !8
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %lossless = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 43
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %segment_id = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id, align 4
  %bf.lshr = lshr i8 %bf.load, 1
  %bf.clear = and i8 %bf.lshr, 7
  %idxprom = zext i8 %bf.clear to i32
  %arrayidx1 = getelementptr inbounds [8 x i32], [8 x i32]* %lossless, i32 0, i32 %idxprom
  %8 = load i32, i32* %arrayidx1, align 4, !tbaa !2
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %9 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %10, i32 0, i32 4
  %arrayidx2 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 1
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx2, i32 0, i32 4
  %11 = load i32, i32* %subsampling_x, align 4, !tbaa !126
  store i32 %11, i32* %ssx, align 4, !tbaa !2
  %12 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %plane3 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %13, i32 0, i32 4
  %arrayidx4 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane3, i32 0, i32 1
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx4, i32 0, i32 5
  %14 = load i32, i32* %subsampling_y, align 4, !tbaa !129
  store i32 %14, i32* %ssy, align 4, !tbaa !2
  %15 = bitcast i32* %plane_bsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i8, i8* %bsize, align 1, !tbaa !8
  %17 = load i32, i32* %ssx, align 4, !tbaa !2
  %18 = load i32, i32* %ssy, align 4, !tbaa !2
  %call = call zeroext i8 @get_plane_block_size(i8 zeroext %16, i32 %17, i32 %18)
  %conv = zext i8 %call to i32
  store i32 %conv, i32* %plane_bsize, align 4, !tbaa !2
  %19 = load i32, i32* %plane_bsize, align 4, !tbaa !2
  %cmp = icmp eq i32 %19, 0
  %conv5 = zext i1 %cmp to i32
  %conv6 = trunc i32 %conv5 to i8
  store i8 %conv6, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %20 = bitcast i32* %plane_bsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  br label %cleanup

if.end:                                           ; preds = %entry
  %23 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom7 = zext i8 %23 to i32
  %arrayidx8 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom7
  %24 = load i8, i8* %arrayidx8, align 1, !tbaa !8
  %conv9 = zext i8 %24 to i32
  %cmp10 = icmp sle i32 %conv9, 32
  br i1 %cmp10, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end
  %25 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom12 = zext i8 %25 to i32
  %arrayidx13 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom12
  %26 = load i8, i8* %arrayidx13, align 1, !tbaa !8
  %conv14 = zext i8 %26 to i32
  %cmp15 = icmp sle i32 %conv14, 32
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end
  %27 = phi i1 [ false, %if.end ], [ %cmp15, %land.rhs ]
  %land.ext = zext i1 %27 to i32
  %conv17 = trunc i32 %land.ext to i8
  store i8 %conv17, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %land.end, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  %28 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  %29 = load i8, i8* %retval, align 1
  ret i8 %29
}

; Function Attrs: nounwind
define internal zeroext i8 @read_cfl_alphas(%struct.frame_contexts* %ec_ctx, %struct.aom_reader* %r, i8* %signs_out) #0 {
entry:
  %ec_ctx.addr = alloca %struct.frame_contexts*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %signs_out.addr = alloca i8*, align 4
  %joint_sign = alloca i8, align 1
  %idx = alloca i8, align 1
  %cdf_u = alloca i16*, align 4
  %cdf_v = alloca i16*, align 4
  store %struct.frame_contexts* %ec_ctx, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i8* %signs_out, i8** %signs_out.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %joint_sign) #6
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  %cfl_sign_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %1, i32 0, i32 64
  %arraydecay = getelementptr inbounds [9 x i16], [9 x i16]* %cfl_sign_cdf, i32 0, i32 0
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %0, i16* %arraydecay, i32 8, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.3, i32 0, i32 0))
  %conv = trunc i32 %call to i8
  store i8 %conv, i8* %joint_sign, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %idx) #6
  store i8 0, i8* %idx, align 1, !tbaa !8
  %2 = load i8, i8* %joint_sign, align 1, !tbaa !8
  %conv1 = sext i8 %2 to i32
  %add = add nsw i32 %conv1, 1
  %mul = mul nsw i32 %add, 11
  %shr = ashr i32 %mul, 5
  %cmp = icmp ne i32 %shr, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast i16** %cdf_u to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  %cfl_alpha_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %4, i32 0, i32 65
  %5 = load i8, i8* %joint_sign, align 1, !tbaa !8
  %conv3 = sext i8 %5 to i32
  %add4 = add nsw i32 %conv3, 1
  %sub = sub nsw i32 %add4, 3
  %arrayidx = getelementptr inbounds [6 x [17 x i16]], [6 x [17 x i16]]* %cfl_alpha_cdf, i32 0, i32 %sub
  %arraydecay5 = getelementptr inbounds [17 x i16], [17 x i16]* %arrayidx, i32 0, i32 0
  store i16* %arraydecay5, i16** %cdf_u, align 4, !tbaa !6
  %6 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %7 = load i16*, i16** %cdf_u, align 4, !tbaa !6
  %call6 = call i32 @aom_read_symbol_(%struct.aom_reader* %6, i16* %7, i32 16, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.4, i32 0, i32 0))
  %conv7 = trunc i32 %call6 to i8
  %conv8 = zext i8 %conv7 to i32
  %shl = shl i32 %conv8, 4
  %conv9 = trunc i32 %shl to i8
  store i8 %conv9, i8* %idx, align 1, !tbaa !8
  %8 = bitcast i16** %cdf_u to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load i8, i8* %joint_sign, align 1, !tbaa !8
  %conv10 = sext i8 %9 to i32
  %add11 = add nsw i32 %conv10, 1
  %10 = load i8, i8* %joint_sign, align 1, !tbaa !8
  %conv12 = sext i8 %10 to i32
  %add13 = add nsw i32 %conv12, 1
  %mul14 = mul nsw i32 %add13, 11
  %shr15 = ashr i32 %mul14, 5
  %mul16 = mul nsw i32 3, %shr15
  %sub17 = sub nsw i32 %add11, %mul16
  %cmp18 = icmp ne i32 %sub17, 0
  br i1 %cmp18, label %if.then20, label %if.end45

if.then20:                                        ; preds = %if.end
  %11 = bitcast i16** %cdf_v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  %cfl_alpha_cdf21 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %12, i32 0, i32 65
  %13 = load i8, i8* %joint_sign, align 1, !tbaa !8
  %conv22 = sext i8 %13 to i32
  %add23 = add nsw i32 %conv22, 1
  %14 = load i8, i8* %joint_sign, align 1, !tbaa !8
  %conv24 = sext i8 %14 to i32
  %add25 = add nsw i32 %conv24, 1
  %mul26 = mul nsw i32 %add25, 11
  %shr27 = ashr i32 %mul26, 5
  %mul28 = mul nsw i32 3, %shr27
  %sub29 = sub nsw i32 %add23, %mul28
  %mul30 = mul nsw i32 %sub29, 3
  %15 = load i8, i8* %joint_sign, align 1, !tbaa !8
  %conv31 = sext i8 %15 to i32
  %add32 = add nsw i32 %conv31, 1
  %mul33 = mul nsw i32 %add32, 11
  %shr34 = ashr i32 %mul33, 5
  %add35 = add nsw i32 %mul30, %shr34
  %sub36 = sub nsw i32 %add35, 3
  %arrayidx37 = getelementptr inbounds [6 x [17 x i16]], [6 x [17 x i16]]* %cfl_alpha_cdf21, i32 0, i32 %sub36
  %arraydecay38 = getelementptr inbounds [17 x i16], [17 x i16]* %arrayidx37, i32 0, i32 0
  store i16* %arraydecay38, i16** %cdf_v, align 4, !tbaa !6
  %16 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %17 = load i16*, i16** %cdf_v, align 4, !tbaa !6
  %call39 = call i32 @aom_read_symbol_(%struct.aom_reader* %16, i16* %17, i32 16, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.5, i32 0, i32 0))
  %conv40 = trunc i32 %call39 to i8
  %conv41 = zext i8 %conv40 to i32
  %18 = load i8, i8* %idx, align 1, !tbaa !8
  %conv42 = zext i8 %18 to i32
  %add43 = add nsw i32 %conv42, %conv41
  %conv44 = trunc i32 %add43 to i8
  store i8 %conv44, i8* %idx, align 1, !tbaa !8
  %19 = bitcast i16** %cdf_v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  br label %if.end45

if.end45:                                         ; preds = %if.then20, %if.end
  %20 = load i8, i8* %joint_sign, align 1, !tbaa !8
  %21 = load i8*, i8** %signs_out.addr, align 4, !tbaa !6
  store i8 %20, i8* %21, align 1, !tbaa !8
  %22 = load i8, i8* %idx, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %idx) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %joint_sign) #6
  ret i8 %22
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_uv_mode(i8 zeroext %mode) #2 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [16 x i8], [16 x i8]* @get_uv_mode.uv2y, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  ret i8 %1
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @store_cfl_required(%struct.AV1Common* %cm, %struct.macroblockd* %xd) #2 {
entry:
  %retval = alloca i8, align 1
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %5 = load i8, i8* %monochrome, align 1, !tbaa !84
  %tobool = icmp ne i8 %5, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8 0, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %is_chroma_ref = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 3
  %7 = load i8, i8* %is_chroma_ref, align 4, !tbaa !85, !range !46
  %tobool1 = trunc i8 %7 to i1
  br i1 %tobool1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i8 1, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %8)
  %tobool4 = icmp ne i32 %call, 0
  br i1 %tobool4, label %land.end, label %land.rhs

land.rhs:                                         ; preds = %if.end3
  %9 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %uv_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %9, i32 0, i32 8
  %10 = load i8, i8* %uv_mode, align 4, !tbaa !86
  %conv = zext i8 %10 to i32
  %cmp = icmp eq i32 %conv, 13
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end3
  %11 = phi i1 [ false, %if.end3 ], [ %cmp, %land.rhs ]
  %land.ext = zext i1 %11 to i32
  %conv6 = trunc i32 %land.ext to i8
  store i8 %conv6, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %land.end, %if.then2, %if.then
  %12 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = load i8, i8* %retval, align 1
  ret i8 %13
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_allow_palette(i32 %allow_screen_content_tools, i8 zeroext %sb_type) #2 {
entry:
  %allow_screen_content_tools.addr = alloca i32, align 4
  %sb_type.addr = alloca i8, align 1
  store i32 %allow_screen_content_tools, i32* %allow_screen_content_tools.addr, align 4, !tbaa !2
  store i8 %sb_type, i8* %sb_type.addr, align 1, !tbaa !8
  %0 = load i32, i32* %allow_screen_content_tools.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %sb_type.addr, align 1, !tbaa !8
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  %cmp = icmp sle i32 %conv, 64
  br i1 %cmp, label %land.lhs.true2, label %land.end

land.lhs.true2:                                   ; preds = %land.lhs.true
  %3 = load i8, i8* %sb_type.addr, align 1, !tbaa !8
  %idxprom3 = zext i8 %3 to i32
  %arrayidx4 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom3
  %4 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = zext i8 %4 to i32
  %cmp6 = icmp sle i32 %conv5, 64
  br i1 %cmp6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true2
  %5 = load i8, i8* %sb_type.addr, align 1, !tbaa !8
  %conv8 = zext i8 %5 to i32
  %cmp9 = icmp sge i32 %conv8, 3
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true2, %land.lhs.true, %entry
  %6 = phi i1 [ false, %land.lhs.true2 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp9, %land.rhs ]
  %land.ext = zext i1 %6 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define internal void @read_palette_mode_info(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %num_planes = alloca i32, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %bsize = alloca i8, align 1
  %pmi = alloca %struct.PALETTE_MODE_INFO*, align 4
  %bsize_ctx = alloca i32, align 4
  %palette_mode_ctx = alloca i32, align 4
  %modev = alloca i32, align 4
  %palette_uv_mode_ctx = alloca i32, align 4
  %modev29 = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !2
  %2 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 6
  %4 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %4, i32 0
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %6, i32 0, i32 6
  %7 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %7, i8* %bsize, align 1, !tbaa !8
  %8 = bitcast %struct.PALETTE_MODE_INFO** %pmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %9, i32 0, i32 5
  store %struct.PALETTE_MODE_INFO* %palette_mode_info, %struct.PALETTE_MODE_INFO** %pmi, align 4, !tbaa !6
  %10 = bitcast i32* %bsize_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load i8, i8* %bsize, align 1, !tbaa !8
  %call1 = call i32 @av1_get_palette_bsize_ctx(i8 zeroext %11)
  store i32 %call1, i32* %bsize_ctx, align 4, !tbaa !2
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %12, i32 0, i32 7
  %13 = load i8, i8* %mode, align 1, !tbaa !50
  %conv = zext i8 %13 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %14 = bitcast i32* %palette_mode_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call3 = call i32 @av1_get_palette_mode_ctx(%struct.macroblockd* %15)
  store i32 %call3, i32* %palette_mode_ctx, align 4, !tbaa !2
  %16 = bitcast i32* %modev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %18, i32 0, i32 40
  %19 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %palette_y_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %19, i32 0, i32 29
  %20 = load i32, i32* %bsize_ctx, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds [7 x [3 x [3 x i16]]], [7 x [3 x [3 x i16]]]* %palette_y_mode_cdf, i32 0, i32 %20
  %21 = load i32, i32* %palette_mode_ctx, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %arrayidx4, i32 0, i32 %21
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx5, i32 0, i32 0
  %call6 = call i32 @aom_read_symbol_(%struct.aom_reader* %17, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_mode_info, i32 0, i32 0))
  store i32 %call6, i32* %modev, align 4, !tbaa !2
  %22 = load i32, i32* %modev, align 4, !tbaa !2
  %tobool = icmp ne i32 %22, 0
  br i1 %tobool, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  %23 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %24 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %24, i32 0, i32 40
  %25 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx8, align 16, !tbaa !47
  %palette_y_size_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %25, i32 0, i32 25
  %26 = load i32, i32* %bsize_ctx, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds [7 x [8 x i16]], [7 x [8 x i16]]* %palette_y_size_cdf, i32 0, i32 %26
  %arraydecay10 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx9, i32 0, i32 0
  %call11 = call i32 @aom_read_symbol_(%struct.aom_reader* %23, i16* %arraydecay10, i32 7, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_mode_info, i32 0, i32 0))
  %add = add nsw i32 %call11, 2
  %conv12 = trunc i32 %add to i8
  %27 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi, align 4, !tbaa !6
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %27, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 0
  store i8 %conv12, i8* %arrayidx13, align 2, !tbaa !8
  %28 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %29 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %29, i32 0, i32 37
  %bit_depth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 25
  %30 = load i32, i32* %bit_depth, align 8, !tbaa !130
  %31 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi, align 4, !tbaa !6
  %32 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_palette_colors_y(%struct.macroblockd* %28, i32 %30, %struct.PALETTE_MODE_INFO* %31, %struct.aom_reader* %32)
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then
  %33 = bitcast i32* %modev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast i32* %palette_mode_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  br label %if.end14

if.end14:                                         ; preds = %if.end, %entry
  %35 = load i32, i32* %num_planes, align 4, !tbaa !2
  %cmp15 = icmp sgt i32 %35, 1
  br i1 %cmp15, label %land.lhs.true, label %if.end47

land.lhs.true:                                    ; preds = %if.end14
  %36 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %uv_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %36, i32 0, i32 8
  %37 = load i8, i8* %uv_mode, align 4, !tbaa !86
  %conv17 = zext i8 %37 to i32
  %cmp18 = icmp eq i32 %conv17, 0
  br i1 %cmp18, label %land.lhs.true20, label %if.end47

land.lhs.true20:                                  ; preds = %land.lhs.true
  %38 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %is_chroma_ref = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %38, i32 0, i32 3
  %39 = load i8, i8* %is_chroma_ref, align 4, !tbaa !85, !range !46
  %tobool21 = trunc i8 %39 to i1
  br i1 %tobool21, label %if.then23, label %if.end47

if.then23:                                        ; preds = %land.lhs.true20
  %40 = bitcast i32* %palette_uv_mode_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  %41 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi, align 4, !tbaa !6
  %palette_size24 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %41, i32 0, i32 1
  %arrayidx25 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size24, i32 0, i32 0
  %42 = load i8, i8* %arrayidx25, align 2, !tbaa !8
  %conv26 = zext i8 %42 to i32
  %cmp27 = icmp sgt i32 %conv26, 0
  %conv28 = zext i1 %cmp27 to i32
  store i32 %conv28, i32* %palette_uv_mode_ctx, align 4, !tbaa !2
  %43 = bitcast i32* %modev29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  %44 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %45 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx30 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %45, i32 0, i32 40
  %46 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx30, align 16, !tbaa !47
  %palette_uv_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %46, i32 0, i32 30
  %47 = load i32, i32* %palette_uv_mode_ctx, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds [2 x [3 x i16]], [2 x [3 x i16]]* %palette_uv_mode_cdf, i32 0, i32 %47
  %arraydecay32 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx31, i32 0, i32 0
  %call33 = call i32 @aom_read_symbol_(%struct.aom_reader* %44, i16* %arraydecay32, i32 2, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_mode_info, i32 0, i32 0))
  store i32 %call33, i32* %modev29, align 4, !tbaa !2
  %48 = load i32, i32* %modev29, align 4, !tbaa !2
  %tobool34 = icmp ne i32 %48, 0
  br i1 %tobool34, label %if.then35, label %if.end46

if.then35:                                        ; preds = %if.then23
  %49 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %50 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx36 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %50, i32 0, i32 40
  %51 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx36, align 16, !tbaa !47
  %palette_uv_size_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %51, i32 0, i32 26
  %52 = load i32, i32* %bsize_ctx, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds [7 x [8 x i16]], [7 x [8 x i16]]* %palette_uv_size_cdf, i32 0, i32 %52
  %arraydecay38 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx37, i32 0, i32 0
  %call39 = call i32 @aom_read_symbol_(%struct.aom_reader* %49, i16* %arraydecay38, i32 7, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_mode_info, i32 0, i32 0))
  %add40 = add nsw i32 %call39, 2
  %conv41 = trunc i32 %add40 to i8
  %53 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi, align 4, !tbaa !6
  %palette_size42 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %53, i32 0, i32 1
  %arrayidx43 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size42, i32 0, i32 1
  store i8 %conv41, i8* %arrayidx43, align 1, !tbaa !8
  %54 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %55 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params44 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %55, i32 0, i32 37
  %bit_depth45 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params44, i32 0, i32 25
  %56 = load i32, i32* %bit_depth45, align 8, !tbaa !130
  %57 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi, align 4, !tbaa !6
  %58 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_palette_colors_uv(%struct.macroblockd* %54, i32 %56, %struct.PALETTE_MODE_INFO* %57, %struct.aom_reader* %58)
  br label %if.end46

if.end46:                                         ; preds = %if.then35, %if.then23
  %59 = bitcast i32* %modev29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %palette_uv_mode_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %land.lhs.true20, %land.lhs.true, %if.end14
  %61 = bitcast i32* %bsize_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast %struct.PALETTE_MODE_INFO** %pmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  %63 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  ret void
}

; Function Attrs: nounwind
define internal void @read_filter_intra_mode_info(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %filter_intra_mode_info = alloca %struct.FILTER_INTRA_MODE_INFO*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %4 = bitcast %struct.FILTER_INTRA_MODE_INFO** %filter_intra_mode_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %filter_intra_mode_info1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 13
  store %struct.FILTER_INTRA_MODE_INFO* %filter_intra_mode_info1, %struct.FILTER_INTRA_MODE_INFO** %filter_intra_mode_info, align 4, !tbaa !6
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %call = call i32 @av1_filter_intra_allowed(%struct.AV1Common* %6, %struct.MB_MODE_INFO* %7)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %8 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %9, i32 0, i32 40
  %10 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %filter_intra_cdfs = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %10, i32 0, i32 47
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %11, i32 0, i32 6
  %12 = load i8, i8* %sb_type, align 2, !tbaa !76
  %idxprom = zext i8 %12 to i32
  %arrayidx2 = getelementptr inbounds [22 x [3 x i16]], [22 x [3 x i16]]* %filter_intra_cdfs, i32 0, i32 %idxprom
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx2, i32 0, i32 0
  %call3 = call i32 @aom_read_symbol_(%struct.aom_reader* %8, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @__func__.read_filter_intra_mode_info, i32 0, i32 0))
  %conv = trunc i32 %call3 to i8
  %13 = load %struct.FILTER_INTRA_MODE_INFO*, %struct.FILTER_INTRA_MODE_INFO** %filter_intra_mode_info, align 4, !tbaa !6
  %use_filter_intra = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %13, i32 0, i32 1
  store i8 %conv, i8* %use_filter_intra, align 1, !tbaa !131
  %14 = load %struct.FILTER_INTRA_MODE_INFO*, %struct.FILTER_INTRA_MODE_INFO** %filter_intra_mode_info, align 4, !tbaa !6
  %use_filter_intra4 = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %14, i32 0, i32 1
  %15 = load i8, i8* %use_filter_intra4, align 1, !tbaa !131
  %tobool5 = icmp ne i8 %15, 0
  br i1 %tobool5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  %16 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx7 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %17, i32 0, i32 40
  %18 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx7, align 16, !tbaa !47
  %filter_intra_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %18, i32 0, i32 48
  %arraydecay8 = getelementptr inbounds [6 x i16], [6 x i16]* %filter_intra_mode_cdf, i32 0, i32 0
  %call9 = call i32 @aom_read_symbol_(%struct.aom_reader* %16, i16* %arraydecay8, i32 5, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @__func__.read_filter_intra_mode_info, i32 0, i32 0))
  %conv10 = trunc i32 %call9 to i8
  %19 = load %struct.FILTER_INTRA_MODE_INFO*, %struct.FILTER_INTRA_MODE_INFO** %filter_intra_mode_info, align 4, !tbaa !6
  %filter_intra_mode = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %19, i32 0, i32 0
  store i8 %conv10, i8* %filter_intra_mode, align 1, !tbaa !132
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then
  br label %if.end12

if.else:                                          ; preds = %entry
  %20 = load %struct.FILTER_INTRA_MODE_INFO*, %struct.FILTER_INTRA_MODE_INFO** %filter_intra_mode_info, align 4, !tbaa !6
  %use_filter_intra11 = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %20, i32 0, i32 1
  store i8 0, i8* %use_filter_intra11, align 1, !tbaa !131
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.end
  %21 = bitcast %struct.FILTER_INTRA_MODE_INFO** %filter_intra_mode_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  ret void
}

; Function Attrs: nounwind
define internal i32 @read_segment_id(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r, i32 %skip) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %skip.addr = alloca i32, align 4
  %cdf_num = alloca i32, align 4
  %pred = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %seg = alloca %struct.segmentation*, align 4
  %segp = alloca %struct.segmentation_probs*, align 4
  %pred_cdf = alloca i16*, align 4
  %coded_id = alloca i32, align 4
  %segment_id = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i32 %skip, i32* %skip.addr, align 4, !tbaa !2
  %0 = bitcast i32* %cdf_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_spatial_seg_pred(%struct.AV1Common* %2, %struct.macroblockd* %3, i32* %cdf_num)
  store i32 %call, i32* %pred, align 4, !tbaa !2
  %4 = load i32, i32* %skip.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %pred, align 4, !tbaa !2
  store i32 %5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %6 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 40
  %8 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %8, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %9 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 24
  store %struct.segmentation* %seg1, %struct.segmentation** %seg, align 4, !tbaa !6
  %11 = bitcast %struct.segmentation_probs** %segp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %seg2 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %12, i32 0, i32 46
  store %struct.segmentation_probs* %seg2, %struct.segmentation_probs** %segp, align 4, !tbaa !6
  %13 = bitcast i16** %pred_cdf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load %struct.segmentation_probs*, %struct.segmentation_probs** %segp, align 4, !tbaa !6
  %spatial_pred_seg_cdf = getelementptr inbounds %struct.segmentation_probs, %struct.segmentation_probs* %14, i32 0, i32 2
  %15 = load i32, i32* %cdf_num, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [9 x i16]], [3 x [9 x i16]]* %spatial_pred_seg_cdf, i32 0, i32 %15
  %arraydecay = getelementptr inbounds [9 x i16], [9 x i16]* %arrayidx, i32 0, i32 0
  store i16* %arraydecay, i16** %pred_cdf, align 4, !tbaa !6
  %16 = bitcast i32* %coded_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %18 = load i16*, i16** %pred_cdf, align 4, !tbaa !6
  %call3 = call i32 @aom_read_symbol_(%struct.aom_reader* %17, i16* %18, i32 8, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_segment_id, i32 0, i32 0))
  store i32 %call3, i32* %coded_id, align 4, !tbaa !2
  %19 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i32, i32* %coded_id, align 4, !tbaa !2
  %21 = load i32, i32* %pred, align 4, !tbaa !2
  %22 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %last_active_segid = getelementptr inbounds %struct.segmentation, %struct.segmentation* %22, i32 0, i32 6
  %23 = load i32, i32* %last_active_segid, align 4, !tbaa !133
  %add = add nsw i32 %23, 1
  %call4 = call i32 @av1_neg_deinterleave(i32 %20, i32 %21, i32 %add)
  store i32 %call4, i32* %segment_id, align 4, !tbaa !2
  %24 = load i32, i32* %segment_id, align 4, !tbaa !2
  %cmp = icmp slt i32 %24, 0
  br i1 %cmp, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %25 = load i32, i32* %segment_id, align 4, !tbaa !2
  %26 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %last_active_segid5 = getelementptr inbounds %struct.segmentation, %struct.segmentation* %26, i32 0, i32 6
  %27 = load i32, i32* %last_active_segid5, align 4, !tbaa !133
  %cmp6 = icmp sgt i32 %25, %27
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %lor.lhs.false, %if.end
  %28 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %error_info = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %28, i32 0, i32 46
  %29 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info, align 16, !tbaa !125
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %29, i32 7, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str, i32 0, i32 0))
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %lor.lhs.false
  %30 = load i32, i32* %segment_id, align 4, !tbaa !2
  store i32 %30, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %31 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  %32 = bitcast i32* %coded_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  %33 = bitcast i16** %pred_cdf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast %struct.segmentation_probs** %segp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then
  %37 = bitcast i32* %pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  %38 = bitcast i32* %cdf_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  %39 = load i32, i32* %retval, align 4
  ret i32 %39
}

; Function Attrs: nounwind
define internal void @set_segment_id(%struct.AV1Common* %cm, i32 %mi_offset, i32 %x_mis, i32 %y_mis, i32 %segment_id) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %mi_offset.addr = alloca i32, align 4
  %x_mis.addr = alloca i32, align 4
  %y_mis.addr = alloca i32, align 4
  %segment_id.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i32 %mi_offset, i32* %mi_offset.addr, align 4, !tbaa !2
  store i32 %x_mis, i32* %x_mis.addr, align 4, !tbaa !2
  store i32 %y_mis, i32* %y_mis.addr, align 4, !tbaa !2
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !2
  %0 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc6, %entry
  %1 = load i32, i32* %y, align 4, !tbaa !2
  %2 = load i32, i32* %y_mis.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end8

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store i32 0, i32* %x, align 4, !tbaa !2
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %x, align 4, !tbaa !2
  %6 = load i32, i32* %x_mis.addr, align 4, !tbaa !2
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %conv = trunc i32 %8 to i8
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 13
  %10 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !91
  %seg_map = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %10, i32 0, i32 6
  %11 = load i8*, i8** %seg_map, align 4, !tbaa !134
  %12 = load i32, i32* %mi_offset.addr, align 4, !tbaa !2
  %13 = load i32, i32* %y, align 4, !tbaa !2
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %15 = load i32, i32* %mi_cols, align 4, !tbaa !90
  %mul = mul nsw i32 %13, %15
  %add = add nsw i32 %12, %mul
  %16 = load i32, i32* %x, align 4, !tbaa !2
  %add5 = add nsw i32 %add, %16
  %arrayidx = getelementptr inbounds i8, i8* %11, i32 %add5
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %17 = load i32, i32* %x, align 4, !tbaa !2
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %x, align 4, !tbaa !2
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc6

for.inc6:                                         ; preds = %for.end
  %18 = load i32, i32* %y, align 4, !tbaa !2
  %inc7 = add nsw i32 %18, 1
  store i32 %inc7, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.end8:                                         ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_get_spatial_seg_pred(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32* %cdf_index) #2 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %cdf_index.addr = alloca i32*, align 4
  %prev_ul = alloca i32, align 4
  %prev_l = alloca i32, align 4
  %prev_u = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %mi_params = alloca %struct.CommonModeInfoParams*, align 4
  %seg_map = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32* %cdf_index, i32** %cdf_index.addr, align 4, !tbaa !6
  %0 = bitcast i32* %prev_ul to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 -1, i32* %prev_ul, align 4, !tbaa !2
  %1 = bitcast i32* %prev_l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 -1, i32* %prev_l, align 4, !tbaa !2
  %2 = bitcast i32* %prev_u to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 -1, i32* %prev_u, align 4, !tbaa !2
  %3 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 0
  %5 = load i32, i32* %mi_row1, align 16, !tbaa !71
  store i32 %5, i32* %mi_row, align 4, !tbaa !2
  %6 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 1
  %8 = load i32, i32* %mi_col2, align 4, !tbaa !72
  store i32 %8, i32* %mi_col, align 4, !tbaa !2
  %9 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params3 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 22
  store %struct.CommonModeInfoParams* %mi_params3, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %11 = bitcast i8** %seg_map to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %12, i32 0, i32 13
  %13 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !91
  %seg_map4 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %13, i32 0, i32 6
  %14 = load i8*, i8** %seg_map4, align 4, !tbaa !134
  store i8* %14, i8** %seg_map, align 4, !tbaa !6
  %15 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %15, i32 0, i32 7
  %16 = load i8, i8* %up_available, align 8, !tbaa !135, !range !46
  %tobool = trunc i8 %16 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %17, i32 0, i32 8
  %18 = load i8, i8* %left_available, align 1, !tbaa !136, !range !46
  %tobool5 = trunc i8 %18 to i1
  br i1 %tobool5, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %19 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %20 = load i8*, i8** %seg_map, align 4, !tbaa !6
  %21 = load i32, i32* %mi_row, align 4, !tbaa !2
  %sub = sub nsw i32 %21, 1
  %22 = load i32, i32* %mi_col, align 4, !tbaa !2
  %sub6 = sub nsw i32 %22, 1
  %call = call i32 @get_segment_id(%struct.CommonModeInfoParams* %19, i8* %20, i8 zeroext 0, i32 %sub, i32 %sub6)
  store i32 %call, i32* %prev_ul, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %23 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %up_available7 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %23, i32 0, i32 7
  %24 = load i8, i8* %up_available7, align 8, !tbaa !135, !range !46
  %tobool8 = trunc i8 %24 to i1
  br i1 %tobool8, label %if.then9, label %if.end13

if.then9:                                         ; preds = %if.end
  %25 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %26 = load i8*, i8** %seg_map, align 4, !tbaa !6
  %27 = load i32, i32* %mi_row, align 4, !tbaa !2
  %sub10 = sub nsw i32 %27, 1
  %28 = load i32, i32* %mi_col, align 4, !tbaa !2
  %sub11 = sub nsw i32 %28, 0
  %call12 = call i32 @get_segment_id(%struct.CommonModeInfoParams* %25, i8* %26, i8 zeroext 0, i32 %sub10, i32 %sub11)
  store i32 %call12, i32* %prev_u, align 4, !tbaa !2
  br label %if.end13

if.end13:                                         ; preds = %if.then9, %if.end
  %29 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_available14 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %29, i32 0, i32 8
  %30 = load i8, i8* %left_available14, align 1, !tbaa !136, !range !46
  %tobool15 = trunc i8 %30 to i1
  br i1 %tobool15, label %if.then16, label %if.end20

if.then16:                                        ; preds = %if.end13
  %31 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %32 = load i8*, i8** %seg_map, align 4, !tbaa !6
  %33 = load i32, i32* %mi_row, align 4, !tbaa !2
  %sub17 = sub nsw i32 %33, 0
  %34 = load i32, i32* %mi_col, align 4, !tbaa !2
  %sub18 = sub nsw i32 %34, 1
  %call19 = call i32 @get_segment_id(%struct.CommonModeInfoParams* %31, i8* %32, i8 zeroext 0, i32 %sub17, i32 %sub18)
  store i32 %call19, i32* %prev_l, align 4, !tbaa !2
  br label %if.end20

if.end20:                                         ; preds = %if.then16, %if.end13
  %35 = load i32, i32* %prev_ul, align 4, !tbaa !2
  %cmp = icmp slt i32 %35, 0
  br i1 %cmp, label %if.then21, label %if.else

if.then21:                                        ; preds = %if.end20
  %36 = load i32*, i32** %cdf_index.addr, align 4, !tbaa !6
  store i32 0, i32* %36, align 4, !tbaa !2
  br label %if.end35

if.else:                                          ; preds = %if.end20
  %37 = load i32, i32* %prev_ul, align 4, !tbaa !2
  %38 = load i32, i32* %prev_u, align 4, !tbaa !2
  %cmp22 = icmp eq i32 %37, %38
  br i1 %cmp22, label %land.lhs.true23, label %if.else26

land.lhs.true23:                                  ; preds = %if.else
  %39 = load i32, i32* %prev_ul, align 4, !tbaa !2
  %40 = load i32, i32* %prev_l, align 4, !tbaa !2
  %cmp24 = icmp eq i32 %39, %40
  br i1 %cmp24, label %if.then25, label %if.else26

if.then25:                                        ; preds = %land.lhs.true23
  %41 = load i32*, i32** %cdf_index.addr, align 4, !tbaa !6
  store i32 2, i32* %41, align 4, !tbaa !2
  br label %if.end34

if.else26:                                        ; preds = %land.lhs.true23, %if.else
  %42 = load i32, i32* %prev_ul, align 4, !tbaa !2
  %43 = load i32, i32* %prev_u, align 4, !tbaa !2
  %cmp27 = icmp eq i32 %42, %43
  br i1 %cmp27, label %if.then31, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else26
  %44 = load i32, i32* %prev_ul, align 4, !tbaa !2
  %45 = load i32, i32* %prev_l, align 4, !tbaa !2
  %cmp28 = icmp eq i32 %44, %45
  br i1 %cmp28, label %if.then31, label %lor.lhs.false29

lor.lhs.false29:                                  ; preds = %lor.lhs.false
  %46 = load i32, i32* %prev_u, align 4, !tbaa !2
  %47 = load i32, i32* %prev_l, align 4, !tbaa !2
  %cmp30 = icmp eq i32 %46, %47
  br i1 %cmp30, label %if.then31, label %if.else32

if.then31:                                        ; preds = %lor.lhs.false29, %lor.lhs.false, %if.else26
  %48 = load i32*, i32** %cdf_index.addr, align 4, !tbaa !6
  store i32 1, i32* %48, align 4, !tbaa !2
  br label %if.end33

if.else32:                                        ; preds = %lor.lhs.false29
  %49 = load i32*, i32** %cdf_index.addr, align 4, !tbaa !6
  store i32 0, i32* %49, align 4, !tbaa !2
  br label %if.end33

if.end33:                                         ; preds = %if.else32, %if.then31
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.then25
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %if.then21
  %50 = load i32, i32* %prev_u, align 4, !tbaa !2
  %cmp36 = icmp eq i32 %50, -1
  br i1 %cmp36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.end35
  %51 = load i32, i32* %prev_l, align 4, !tbaa !2
  %cmp38 = icmp eq i32 %51, -1
  br i1 %cmp38, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then37
  br label %cond.end

cond.false:                                       ; preds = %if.then37
  %52 = load i32, i32* %prev_l, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %52, %cond.false ]
  store i32 %cond, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end39:                                         ; preds = %if.end35
  %53 = load i32, i32* %prev_l, align 4, !tbaa !2
  %cmp40 = icmp eq i32 %53, -1
  br i1 %cmp40, label %if.then41, label %if.end42

if.then41:                                        ; preds = %if.end39
  %54 = load i32, i32* %prev_u, align 4, !tbaa !2
  store i32 %54, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end42:                                         ; preds = %if.end39
  %55 = load i32, i32* %prev_ul, align 4, !tbaa !2
  %56 = load i32, i32* %prev_u, align 4, !tbaa !2
  %cmp43 = icmp eq i32 %55, %56
  br i1 %cmp43, label %cond.true44, label %cond.false45

cond.true44:                                      ; preds = %if.end42
  %57 = load i32, i32* %prev_u, align 4, !tbaa !2
  br label %cond.end46

cond.false45:                                     ; preds = %if.end42
  %58 = load i32, i32* %prev_l, align 4, !tbaa !2
  br label %cond.end46

cond.end46:                                       ; preds = %cond.false45, %cond.true44
  %cond47 = phi i32 [ %57, %cond.true44 ], [ %58, %cond.false45 ]
  store i32 %cond47, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %cond.end46, %if.then41, %cond.end
  %59 = bitcast i8** %seg_map to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast i32* %prev_u to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast i32* %prev_l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = bitcast i32* %prev_ul to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  %66 = load i32, i32* %retval, align 4
  ret i32 %66
}

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #3

; Function Attrs: inlinehint nounwind
define internal i32 @get_segment_id(%struct.CommonModeInfoParams* %mi_params, i8* %segment_ids, i8 zeroext %bsize, i32 %mi_row, i32 %mi_col) #2 {
entry:
  %mi_params.addr = alloca %struct.CommonModeInfoParams*, align 4
  %segment_ids.addr = alloca i8*, align 4
  %bsize.addr = alloca i8, align 1
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %mi_offset = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %xmis = alloca i32, align 4
  %ymis = alloca i32, align 4
  %segment_id = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  store %struct.CommonModeInfoParams* %mi_params, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  store i8* %segment_ids, i8** %segment_ids.addr, align 4, !tbaa !6
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !2
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !2
  %0 = bitcast i32* %mi_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %2 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %2, i32 0, i32 4
  %3 = load i32, i32* %mi_cols, align 4, !tbaa !106
  %mul = mul nsw i32 %1, %3
  %4 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %add = add nsw i32 %mul, %4
  store i32 %add, i32* %mi_offset, align 4, !tbaa !2
  %5 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom = zext i8 %6 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %7 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !2
  %8 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom1 = zext i8 %9 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom1
  %10 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = zext i8 %10 to i32
  store i32 %conv3, i32* %bh, align 4, !tbaa !2
  %11 = bitcast i32* %xmis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_cols4 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %12, i32 0, i32 4
  %13 = load i32, i32* %mi_cols4, align 4, !tbaa !106
  %14 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %13, %14
  %15 = load i32, i32* %bw, align 4, !tbaa !2
  %cmp = icmp slt i32 %sub, %15
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %16 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_cols6 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %16, i32 0, i32 4
  %17 = load i32, i32* %mi_cols6, align 4, !tbaa !106
  %18 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %sub7 = sub nsw i32 %17, %18
  br label %cond.end

cond.false:                                       ; preds = %entry
  %19 = load i32, i32* %bw, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub7, %cond.true ], [ %19, %cond.false ]
  store i32 %cond, i32* %xmis, align 4, !tbaa !2
  %20 = bitcast i32* %ymis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %21, i32 0, i32 3
  %22 = load i32, i32* %mi_rows, align 4, !tbaa !107
  %23 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %sub8 = sub nsw i32 %22, %23
  %24 = load i32, i32* %bh, align 4, !tbaa !2
  %cmp9 = icmp slt i32 %sub8, %24
  br i1 %cmp9, label %cond.true11, label %cond.false14

cond.true11:                                      ; preds = %cond.end
  %25 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_rows12 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %25, i32 0, i32 3
  %26 = load i32, i32* %mi_rows12, align 4, !tbaa !107
  %27 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %sub13 = sub nsw i32 %26, %27
  br label %cond.end15

cond.false14:                                     ; preds = %cond.end
  %28 = load i32, i32* %bh, align 4, !tbaa !2
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false14, %cond.true11
  %cond16 = phi i32 [ %sub13, %cond.true11 ], [ %28, %cond.false14 ]
  store i32 %cond16, i32* %ymis, align 4, !tbaa !2
  %29 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  store i32 8, i32* %segment_id, align 4, !tbaa !2
  %30 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  store i32 0, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc42, %cond.end15
  %31 = load i32, i32* %y, align 4, !tbaa !2
  %32 = load i32, i32* %ymis, align 4, !tbaa !2
  %cmp17 = icmp slt i32 %31, %32
  br i1 %cmp17, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  br label %for.end44

for.body:                                         ; preds = %for.cond
  %34 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  store i32 0, i32* %x, align 4, !tbaa !2
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc, %for.body
  %35 = load i32, i32* %x, align 4, !tbaa !2
  %36 = load i32, i32* %xmis, align 4, !tbaa !2
  %cmp20 = icmp slt i32 %35, %36
  br i1 %cmp20, label %for.body23, label %for.cond.cleanup22

for.cond.cleanup22:                               ; preds = %for.cond19
  store i32 5, i32* %cleanup.dest.slot, align 4
  %37 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #6
  br label %for.end

for.body23:                                       ; preds = %for.cond19
  %38 = load i32, i32* %segment_id, align 4, !tbaa !2
  %39 = load i8*, i8** %segment_ids.addr, align 4, !tbaa !6
  %40 = load i32, i32* %mi_offset, align 4, !tbaa !2
  %41 = load i32, i32* %y, align 4, !tbaa !2
  %42 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_cols24 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %42, i32 0, i32 4
  %43 = load i32, i32* %mi_cols24, align 4, !tbaa !106
  %mul25 = mul nsw i32 %41, %43
  %add26 = add nsw i32 %40, %mul25
  %44 = load i32, i32* %x, align 4, !tbaa !2
  %add27 = add nsw i32 %add26, %44
  %arrayidx28 = getelementptr inbounds i8, i8* %39, i32 %add27
  %45 = load i8, i8* %arrayidx28, align 1, !tbaa !8
  %conv29 = zext i8 %45 to i32
  %cmp30 = icmp slt i32 %38, %conv29
  br i1 %cmp30, label %cond.true32, label %cond.false33

cond.true32:                                      ; preds = %for.body23
  %46 = load i32, i32* %segment_id, align 4, !tbaa !2
  br label %cond.end40

cond.false33:                                     ; preds = %for.body23
  %47 = load i8*, i8** %segment_ids.addr, align 4, !tbaa !6
  %48 = load i32, i32* %mi_offset, align 4, !tbaa !2
  %49 = load i32, i32* %y, align 4, !tbaa !2
  %50 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_cols34 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %50, i32 0, i32 4
  %51 = load i32, i32* %mi_cols34, align 4, !tbaa !106
  %mul35 = mul nsw i32 %49, %51
  %add36 = add nsw i32 %48, %mul35
  %52 = load i32, i32* %x, align 4, !tbaa !2
  %add37 = add nsw i32 %add36, %52
  %arrayidx38 = getelementptr inbounds i8, i8* %47, i32 %add37
  %53 = load i8, i8* %arrayidx38, align 1, !tbaa !8
  %conv39 = zext i8 %53 to i32
  br label %cond.end40

cond.end40:                                       ; preds = %cond.false33, %cond.true32
  %cond41 = phi i32 [ %46, %cond.true32 ], [ %conv39, %cond.false33 ]
  store i32 %cond41, i32* %segment_id, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %cond.end40
  %54 = load i32, i32* %x, align 4, !tbaa !2
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %x, align 4, !tbaa !2
  br label %for.cond19

for.end:                                          ; preds = %for.cond.cleanup22
  br label %for.inc42

for.inc42:                                        ; preds = %for.end
  %55 = load i32, i32* %y, align 4, !tbaa !2
  %inc43 = add nsw i32 %55, 1
  store i32 %inc43, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.end44:                                        ; preds = %for.cond.cleanup
  %56 = load i32, i32* %segment_id, align 4, !tbaa !2
  store i32 1, i32* %cleanup.dest.slot, align 4
  %57 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #6
  %58 = bitcast i32* %ymis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast i32* %xmis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i32* %mi_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  ret i32 %56
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_get_skip_context(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %above_mi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mi = alloca %struct.MB_MODE_INFO*, align 4
  %above_skip = alloca i32, align 4
  %left_skip = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 12
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 16, !tbaa !74
  store %struct.MB_MODE_INFO* %2, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %3 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 11
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !75
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %6 = bitcast i32* %above_skip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %tobool = icmp ne %struct.MB_MODE_INFO* %7, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 14
  %9 = load i8, i8* %skip, align 4, !tbaa !17
  %conv = sext i8 %9 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %above_skip, align 4, !tbaa !2
  %10 = bitcast i32* %left_skip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %tobool1 = icmp ne %struct.MB_MODE_INFO* %11, null
  br i1 %tobool1, label %cond.true2, label %cond.false5

cond.true2:                                       ; preds = %cond.end
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %skip3 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %12, i32 0, i32 14
  %13 = load i8, i8* %skip3, align 4, !tbaa !17
  %conv4 = sext i8 %13 to i32
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true2
  %cond7 = phi i32 [ %conv4, %cond.true2 ], [ 0, %cond.false5 ]
  store i32 %cond7, i32* %left_skip, align 4, !tbaa !2
  %14 = load i32, i32* %above_skip, align 4, !tbaa !2
  %15 = load i32, i32* %left_skip, align 4, !tbaa !2
  %add = add nsw i32 %14, %15
  %16 = bitcast i32* %left_skip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast i32* %above_skip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  ret i32 %add
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_mi_grid_idx(%struct.CommonModeInfoParams* %mi_params, i32 %mi_row, i32 %mi_col) #2 {
entry:
  %mi_params.addr = alloca %struct.CommonModeInfoParams*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  store %struct.CommonModeInfoParams* %mi_params, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !2
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !2
  %0 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %1 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %1, i32 0, i32 11
  %2 = load i32, i32* %mi_stride, align 4, !tbaa !137
  %mul = mul nsw i32 %0, %2
  %3 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %add = add nsw i32 %mul, %3
  ret i32 %add
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_literal_(%struct.aom_reader* %r, i32 %bits, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %bits.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %literal = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !6
  %0 = bitcast i32* %literal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %literal, align 4, !tbaa !2
  %1 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %bits.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %2, 1
  store i32 %sub, i32* %bit, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %bit, align 4, !tbaa !2
  %cmp = icmp sge i32 %3, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call = call i32 @aom_read_bit_(%struct.aom_reader* %4, i8* null)
  %5 = load i32, i32* %bit, align 4, !tbaa !2
  %shl = shl i32 %call, %5
  %6 = load i32, i32* %literal, align 4, !tbaa !2
  %or = or i32 %6, %shl
  store i32 %or, i32* %literal, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %bit, align 4, !tbaa !2
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* %bit, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  %tobool = icmp ne i8* %8, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %9 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %10 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  call void @aom_process_accounting(%struct.aom_reader* %9, i8* %10)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %11 = load i32, i32* %literal, align 4, !tbaa !2
  %12 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast i32* %literal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  ret i32 %11
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_bit_(%struct.aom_reader* %r, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %acct_str.addr = alloca i8*, align 4
  %ret = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call = call i32 @aom_read_(%struct.aom_reader* %1, i32 128, i8* null)
  store i32 %call, i32* %ret, align 4, !tbaa !2
  %2 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  call void @aom_process_accounting(%struct.aom_reader* %3, i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i32, i32* %ret, align 4, !tbaa !2
  %6 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret i32 %5
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_(%struct.aom_reader* %r, i32 %prob, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %prob.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %p = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i32 %prob, i32* %prob.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !6
  %0 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %prob.addr, align 4, !tbaa !2
  %shl = shl i32 %1, 15
  %sub = sub nsw i32 8388607, %shl
  %2 = load i32, i32* %prob.addr, align 4, !tbaa !2
  %add = add nsw i32 %sub, %2
  %shr = ashr i32 %add, 8
  store i32 %shr, i32* %p, align 4, !tbaa !2
  %3 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %4, i32 0, i32 2
  %5 = load i32, i32* %p, align 4, !tbaa !2
  %call = call i32 @od_ec_decode_bool_q15(%struct.od_ec_dec* %ec, i32 %5)
  store i32 %call, i32* %bit, align 4, !tbaa !2
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %acct_str.addr, align 4, !tbaa !6
  call void @aom_process_accounting(%struct.aom_reader* %7, i8* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @aom_update_symb_counts(%struct.aom_reader* %9, i32 1)
  %10 = load i32, i32* %bit, align 4, !tbaa !2
  %11 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  ret i32 %10
}

declare i32 @od_ec_decode_bool_q15(%struct.od_ec_dec*, i32) #3

; Function Attrs: nounwind
define internal i32 @read_delta_qindex(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r, %struct.MB_MODE_INFO* %mbmi) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %sign = alloca i32, align 4
  %abs = alloca i32, align 4
  %reduced_delta_qindex = alloca i32, align 4
  %bsize = alloca i8, align 1
  %b_col = alloca i32, align 4
  %b_row = alloca i32, align 4
  %read_delta_q_flag = alloca i32, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %smallval = alloca i32, align 4
  %rem_bits = alloca i32, align 4
  %thr = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %0 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %abs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %reduced_delta_qindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 0, i32* %reduced_delta_qindex, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %3, i32 0, i32 6
  %4 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %4, i8* %bsize, align 1, !tbaa !8
  %5 = bitcast i32* %b_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 1
  %7 = load i32, i32* %mi_col, align 4, !tbaa !72
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 37
  %mib_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 8
  %9 = load i32, i32* %mib_size, align 16, !tbaa !110
  %sub = sub nsw i32 %9, 1
  %and = and i32 %7, %sub
  store i32 %and, i32* %b_col, align 4, !tbaa !2
  %10 = bitcast i32* %b_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %11, i32 0, i32 0
  %12 = load i32, i32* %mi_row, align 16, !tbaa !71
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 37
  %mib_size2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params1, i32 0, i32 8
  %14 = load i32, i32* %mib_size2, align 16, !tbaa !110
  %sub3 = sub nsw i32 %14, 1
  %and4 = and i32 %12, %sub3
  store i32 %and4, i32* %b_row, align 4, !tbaa !2
  %15 = bitcast i32* %read_delta_q_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i32, i32* %b_col, align 4, !tbaa !2
  %cmp = icmp eq i32 %16, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %17 = load i32, i32* %b_row, align 4, !tbaa !2
  %cmp5 = icmp eq i32 %17, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %18 = phi i1 [ false, %entry ], [ %cmp5, %land.rhs ]
  %land.ext = zext i1 %18 to i32
  store i32 %land.ext, i32* %read_delta_q_flag, align 4, !tbaa !2
  %19 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %20, i32 0, i32 40
  %21 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %21, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %22 = load i8, i8* %bsize, align 1, !tbaa !8
  %conv = zext i8 %22 to i32
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %23, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params6, i32 0, i32 7
  %24 = load i8, i8* %sb_size, align 4, !tbaa !112
  %conv7 = zext i8 %24 to i32
  %cmp8 = icmp ne i32 %conv, %conv7
  br i1 %cmp8, label %land.lhs.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.end
  %25 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %25, i32 0, i32 14
  %26 = load i8, i8* %skip, align 4, !tbaa !17
  %conv10 = sext i8 %26 to i32
  %cmp11 = icmp eq i32 %conv10, 0
  br i1 %cmp11, label %land.lhs.true, label %if.end27

land.lhs.true:                                    ; preds = %lor.lhs.false, %land.end
  %27 = load i32, i32* %read_delta_q_flag, align 4, !tbaa !2
  %tobool = icmp ne i32 %27, 0
  br i1 %tobool, label %if.then, label %if.end27

if.then:                                          ; preds = %land.lhs.true
  %28 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %29 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %delta_q_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %29, i32 0, i32 59
  %arraydecay = getelementptr inbounds [5 x i16], [5 x i16]* %delta_q_cdf, i32 0, i32 0
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %28, i16* %arraydecay, i32 4, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_delta_qindex, i32 0, i32 0))
  store i32 %call, i32* %abs, align 4, !tbaa !2
  %30 = bitcast i32* %smallval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load i32, i32* %abs, align 4, !tbaa !2
  %cmp13 = icmp slt i32 %31, 3
  %conv14 = zext i1 %cmp13 to i32
  store i32 %conv14, i32* %smallval, align 4, !tbaa !2
  %32 = load i32, i32* %smallval, align 4, !tbaa !2
  %tobool15 = icmp ne i32 %32, 0
  br i1 %tobool15, label %if.end, label %if.then16

if.then16:                                        ; preds = %if.then
  %33 = bitcast i32* %rem_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  %34 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call17 = call i32 @aom_read_literal_(%struct.aom_reader* %34, i32 3, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_delta_qindex, i32 0, i32 0))
  %add = add nsw i32 %call17, 1
  store i32 %add, i32* %rem_bits, align 4, !tbaa !2
  %35 = bitcast i32* %thr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %36 = load i32, i32* %rem_bits, align 4, !tbaa !2
  %shl = shl i32 1, %36
  %add18 = add nsw i32 %shl, 1
  store i32 %add18, i32* %thr, align 4, !tbaa !2
  %37 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %38 = load i32, i32* %rem_bits, align 4, !tbaa !2
  %call19 = call i32 @aom_read_literal_(%struct.aom_reader* %37, i32 %38, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_delta_qindex, i32 0, i32 0))
  %39 = load i32, i32* %thr, align 4, !tbaa !2
  %add20 = add nsw i32 %call19, %39
  store i32 %add20, i32* %abs, align 4, !tbaa !2
  %40 = bitcast i32* %thr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #6
  %41 = bitcast i32* %rem_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #6
  br label %if.end

if.end:                                           ; preds = %if.then16, %if.then
  %42 = load i32, i32* %abs, align 4, !tbaa !2
  %tobool21 = icmp ne i32 %42, 0
  br i1 %tobool21, label %if.then22, label %if.else

if.then22:                                        ; preds = %if.end
  %43 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call23 = call i32 @aom_read_bit_(%struct.aom_reader* %43, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_delta_qindex, i32 0, i32 0))
  store i32 %call23, i32* %sign, align 4, !tbaa !2
  br label %if.end24

if.else:                                          ; preds = %if.end
  store i32 1, i32* %sign, align 4, !tbaa !2
  br label %if.end24

if.end24:                                         ; preds = %if.else, %if.then22
  %44 = load i32, i32* %sign, align 4, !tbaa !2
  %tobool25 = icmp ne i32 %44, 0
  br i1 %tobool25, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end24
  %45 = load i32, i32* %abs, align 4, !tbaa !2
  %sub26 = sub nsw i32 0, %45
  br label %cond.end

cond.false:                                       ; preds = %if.end24
  %46 = load i32, i32* %abs, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub26, %cond.true ], [ %46, %cond.false ]
  store i32 %cond, i32* %reduced_delta_qindex, align 4, !tbaa !2
  %47 = bitcast i32* %smallval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  br label %if.end27

if.end27:                                         ; preds = %cond.end, %land.lhs.true, %lor.lhs.false
  %48 = load i32, i32* %reduced_delta_qindex, align 4, !tbaa !2
  %49 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  %50 = bitcast i32* %read_delta_q_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  %51 = bitcast i32* %b_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  %52 = bitcast i32* %b_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  %53 = bitcast i32* %reduced_delta_qindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  %54 = bitcast i32* %abs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #6
  ret i32 %48
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !2
  store i32 %low, i32* %low.addr, align 4, !tbaa !2
  store i32 %high, i32* %high.addr, align 4, !tbaa !2
  %0 = load i32, i32* %value.addr, align 4, !tbaa !2
  %1 = load i32, i32* %low.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !2
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !2
  %4 = load i32, i32* %high.addr, align 4, !tbaa !2
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !2
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_num_planes(%struct.AV1Common* %cm) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %1 = load i8, i8* %monochrome, align 1, !tbaa !84
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 3
  ret i32 %cond
}

; Function Attrs: nounwind
define internal i32 @read_delta_lflevel(%struct.AV1Common* %cm, %struct.aom_reader* %r, i16* %cdf, %struct.MB_MODE_INFO* %mbmi, i32 %mi_col, i32 %mi_row) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %cdf.addr = alloca i16*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %mi_col.addr = alloca i32, align 4
  %mi_row.addr = alloca i32, align 4
  %reduced_delta_lflevel = alloca i32, align 4
  %bsize = alloca i8, align 1
  %b_col = alloca i32, align 4
  %b_row = alloca i32, align 4
  %read_delta_lf_flag = alloca i32, align 4
  %abs = alloca i32, align 4
  %smallval = alloca i32, align 4
  %rem_bits = alloca i32, align 4
  %thr = alloca i32, align 4
  %sign = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !2
  %0 = bitcast i32* %reduced_delta_lflevel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %reduced_delta_lflevel, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 6
  %2 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %2, i8* %bsize, align 1, !tbaa !8
  %3 = bitcast i32* %b_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 37
  %mib_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 8
  %6 = load i32, i32* %mib_size, align 16, !tbaa !110
  %sub = sub nsw i32 %6, 1
  %and = and i32 %4, %sub
  store i32 %and, i32* %b_col, align 4, !tbaa !2
  %7 = bitcast i32* %b_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 37
  %mib_size2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params1, i32 0, i32 8
  %10 = load i32, i32* %mib_size2, align 16, !tbaa !110
  %sub3 = sub nsw i32 %10, 1
  %and4 = and i32 %8, %sub3
  store i32 %and4, i32* %b_row, align 4, !tbaa !2
  %11 = bitcast i32* %read_delta_lf_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load i32, i32* %b_col, align 4, !tbaa !2
  %cmp = icmp eq i32 %12, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %13 = load i32, i32* %b_row, align 4, !tbaa !2
  %cmp5 = icmp eq i32 %13, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %14 = phi i1 [ false, %entry ], [ %cmp5, %land.rhs ]
  %land.ext = zext i1 %14 to i32
  store i32 %land.ext, i32* %read_delta_lf_flag, align 4, !tbaa !2
  %15 = load i8, i8* %bsize, align 1, !tbaa !8
  %conv = zext i8 %15 to i32
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %16, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params6, i32 0, i32 7
  %17 = load i8, i8* %sb_size, align 4, !tbaa !112
  %conv7 = zext i8 %17 to i32
  %cmp8 = icmp ne i32 %conv, %conv7
  br i1 %cmp8, label %land.lhs.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.end
  %18 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %18, i32 0, i32 14
  %19 = load i8, i8* %skip, align 4, !tbaa !17
  %conv10 = sext i8 %19 to i32
  %cmp11 = icmp eq i32 %conv10, 0
  br i1 %cmp11, label %land.lhs.true, label %if.end29

land.lhs.true:                                    ; preds = %lor.lhs.false, %land.end
  %20 = load i32, i32* %read_delta_lf_flag, align 4, !tbaa !2
  %tobool = icmp ne i32 %20, 0
  br i1 %tobool, label %if.then, label %if.end29

if.then:                                          ; preds = %land.lhs.true
  %21 = bitcast i32* %abs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %23 = load i16*, i16** %cdf.addr, align 4, !tbaa !6
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %22, i16* %23, i32 4, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @__func__.read_delta_lflevel, i32 0, i32 0))
  store i32 %call, i32* %abs, align 4, !tbaa !2
  %24 = bitcast i32* %smallval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load i32, i32* %abs, align 4, !tbaa !2
  %cmp13 = icmp slt i32 %25, 3
  %conv14 = zext i1 %cmp13 to i32
  store i32 %conv14, i32* %smallval, align 4, !tbaa !2
  %26 = load i32, i32* %smallval, align 4, !tbaa !2
  %tobool15 = icmp ne i32 %26, 0
  br i1 %tobool15, label %if.end, label %if.then16

if.then16:                                        ; preds = %if.then
  %27 = bitcast i32* %rem_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call17 = call i32 @aom_read_literal_(%struct.aom_reader* %28, i32 3, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @__func__.read_delta_lflevel, i32 0, i32 0))
  %add = add nsw i32 %call17, 1
  store i32 %add, i32* %rem_bits, align 4, !tbaa !2
  %29 = bitcast i32* %thr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load i32, i32* %rem_bits, align 4, !tbaa !2
  %shl = shl i32 1, %30
  %add18 = add nsw i32 %shl, 1
  store i32 %add18, i32* %thr, align 4, !tbaa !2
  %31 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %32 = load i32, i32* %rem_bits, align 4, !tbaa !2
  %call19 = call i32 @aom_read_literal_(%struct.aom_reader* %31, i32 %32, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @__func__.read_delta_lflevel, i32 0, i32 0))
  %33 = load i32, i32* %thr, align 4, !tbaa !2
  %add20 = add nsw i32 %call19, %33
  store i32 %add20, i32* %abs, align 4, !tbaa !2
  %34 = bitcast i32* %thr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast i32* %rem_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  br label %if.end

if.end:                                           ; preds = %if.then16, %if.then
  %36 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  %37 = load i32, i32* %abs, align 4, !tbaa !2
  %tobool21 = icmp ne i32 %37, 0
  br i1 %tobool21, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %38 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call22 = call i32 @aom_read_bit_(%struct.aom_reader* %38, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @__func__.read_delta_lflevel, i32 0, i32 0))
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call22, %cond.true ], [ 1, %cond.false ]
  store i32 %cond, i32* %sign, align 4, !tbaa !2
  %39 = load i32, i32* %sign, align 4, !tbaa !2
  %tobool23 = icmp ne i32 %39, 0
  br i1 %tobool23, label %cond.true24, label %cond.false26

cond.true24:                                      ; preds = %cond.end
  %40 = load i32, i32* %abs, align 4, !tbaa !2
  %sub25 = sub nsw i32 0, %40
  br label %cond.end27

cond.false26:                                     ; preds = %cond.end
  %41 = load i32, i32* %abs, align 4, !tbaa !2
  br label %cond.end27

cond.end27:                                       ; preds = %cond.false26, %cond.true24
  %cond28 = phi i32 [ %sub25, %cond.true24 ], [ %41, %cond.false26 ]
  store i32 %cond28, i32* %reduced_delta_lflevel, align 4, !tbaa !2
  %42 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  %43 = bitcast i32* %smallval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  %44 = bitcast i32* %abs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  br label %if.end29

if.end29:                                         ; preds = %cond.end27, %land.lhs.true, %lor.lhs.false
  %45 = load i32, i32* %reduced_delta_lflevel, align 4, !tbaa !2
  %46 = bitcast i32* %read_delta_lf_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  %47 = bitcast i32* %b_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  %48 = bitcast i32* %b_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  %49 = bitcast i32* %reduced_delta_lflevel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  ret i32 %45
}

; Function Attrs: inlinehint nounwind
define internal void @av1_broadcast_interp_filter(%union.int_interpfilters* noalias sret align 4 %agg.result, i8 zeroext %filter) #2 {
entry:
  %filter.addr = alloca i8, align 1
  store i8 %filter, i8* %filter.addr, align 1, !tbaa !8
  %0 = load i8, i8* %filter.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i16
  %as_filters = bitcast %union.int_interpfilters* %agg.result to %struct.InterpFilters*
  %x_filter = getelementptr inbounds %struct.InterpFilters, %struct.InterpFilters* %as_filters, i32 0, i32 1
  store i16 %conv, i16* %x_filter, align 2, !tbaa !8
  %1 = load i8, i8* %filter.addr, align 1, !tbaa !8
  %conv1 = zext i8 %1 to i16
  %as_filters2 = bitcast %union.int_interpfilters* %agg.result to %struct.InterpFilters*
  %y_filter = getelementptr inbounds %struct.InterpFilters, %struct.InterpFilters* %as_filters2, i32 0, i32 0
  store i16 %conv1, i16* %y_filter, align 4, !tbaa !8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare void @av1_find_mv_refs(%struct.AV1Common*, %struct.macroblockd*, %struct.MB_MODE_INFO*, i8 signext, i8*, [8 x %struct.candidate_mv]*, [8 x i16]*, [2 x %union.int_mv]*, %union.int_mv*, i16*) #3

declare void @av1_find_best_ref_mvs(i32, %union.int_mv*, %union.int_mv*, %union.int_mv*, i32) #3

; Function Attrs: inlinehint nounwind
define internal void @av1_find_ref_dv(%union.int_mv* %ref_dv, %struct.TileInfo* %tile, i32 %mib_size, i32 %mi_row) #2 {
entry:
  %ref_dv.addr = alloca %union.int_mv*, align 4
  %tile.addr = alloca %struct.TileInfo*, align 4
  %mib_size.addr = alloca i32, align 4
  %mi_row.addr = alloca i32, align 4
  store %union.int_mv* %ref_dv, %union.int_mv** %ref_dv.addr, align 4, !tbaa !6
  store %struct.TileInfo* %tile, %struct.TileInfo** %tile.addr, align 4, !tbaa !6
  store i32 %mib_size, i32* %mib_size.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !2
  %0 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %1 = load i32, i32* %mib_size.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %0, %1
  %2 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !6
  %mi_row_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %2, i32 0, i32 0
  %3 = load i32, i32* %mi_row_start, align 4, !tbaa !138
  %cmp = icmp slt i32 %sub, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %union.int_mv*, %union.int_mv** %ref_dv.addr, align 4, !tbaa !6
  %as_fullmv = bitcast %union.int_mv* %4 to %struct.fullpel_mv*
  %row = getelementptr inbounds %struct.fullpel_mv, %struct.fullpel_mv* %as_fullmv, i32 0, i32 0
  store i16 0, i16* %row, align 4, !tbaa !8
  %5 = load i32, i32* %mib_size.addr, align 4, !tbaa !2
  %mul = mul nsw i32 -4, %5
  %sub1 = sub nsw i32 %mul, 256
  %conv = trunc i32 %sub1 to i16
  %6 = load %union.int_mv*, %union.int_mv** %ref_dv.addr, align 4, !tbaa !6
  %as_fullmv2 = bitcast %union.int_mv* %6 to %struct.fullpel_mv*
  %col = getelementptr inbounds %struct.fullpel_mv, %struct.fullpel_mv* %as_fullmv2, i32 0, i32 1
  store i16 %conv, i16* %col, align 2, !tbaa !8
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = load i32, i32* %mib_size.addr, align 4, !tbaa !2
  %mul3 = mul nsw i32 -4, %7
  %conv4 = trunc i32 %mul3 to i16
  %8 = load %union.int_mv*, %union.int_mv** %ref_dv.addr, align 4, !tbaa !6
  %as_fullmv5 = bitcast %union.int_mv* %8 to %struct.fullpel_mv*
  %row6 = getelementptr inbounds %struct.fullpel_mv, %struct.fullpel_mv* %as_fullmv5, i32 0, i32 0
  store i16 %conv4, i16* %row6, align 4, !tbaa !8
  %9 = load %union.int_mv*, %union.int_mv** %ref_dv.addr, align 4, !tbaa !6
  %as_fullmv7 = bitcast %union.int_mv* %9 to %struct.fullpel_mv*
  %col8 = getelementptr inbounds %struct.fullpel_mv, %struct.fullpel_mv* %as_fullmv7, i32 0, i32 1
  store i16 0, i16* %col8, align 2, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %10 = load %union.int_mv*, %union.int_mv** %ref_dv.addr, align 4, !tbaa !6
  call void @convert_fullmv_to_mv(%union.int_mv* %10)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @assign_dv(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %union.int_mv* %mv, %union.int_mv* %ref_mv, i32 %mi_row, i32 %mi_col, i8 zeroext %bsize, %struct.aom_reader* %r) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mv.addr = alloca %union.int_mv*, align 4
  %ref_mv.addr = alloca %union.int_mv*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %bsize.addr = alloca i8, align 1
  %r.addr = alloca %struct.aom_reader*, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %valid = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %union.int_mv* %mv, %union.int_mv** %mv.addr, align 4, !tbaa !6
  store %union.int_mv* %ref_mv, %union.int_mv** %ref_mv.addr, align 4, !tbaa !6
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !2
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 40
  %2 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %2, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %4 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %as_mv = bitcast %union.int_mv* %4 to %struct.mv*
  %5 = load %union.int_mv*, %union.int_mv** %ref_mv.addr, align 4, !tbaa !6
  %as_mv1 = bitcast %union.int_mv* %5 to %struct.mv*
  %6 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %ndvc = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %6, i32 0, i32 44
  call void @read_mv(%struct.aom_reader* %3, %struct.mv* %as_mv, %struct.mv* %as_mv1, %struct.nmv_context* %ndvc, i8 signext -1)
  %7 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %as_mv2 = bitcast %union.int_mv* %7 to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv2, i32 0, i32 1
  %8 = load i16, i16* %col, align 2, !tbaa !8
  %conv = sext i16 %8 to i32
  %shr = ashr i32 %conv, 3
  %mul = mul nsw i32 %shr, 8
  %conv3 = trunc i32 %mul to i16
  %9 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %as_mv4 = bitcast %union.int_mv* %9 to %struct.mv*
  %col5 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv4, i32 0, i32 1
  store i16 %conv3, i16* %col5, align 2, !tbaa !8
  %10 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %as_mv6 = bitcast %union.int_mv* %10 to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv6, i32 0, i32 0
  %11 = load i16, i16* %row, align 4, !tbaa !8
  %conv7 = sext i16 %11 to i32
  %shr8 = ashr i32 %conv7, 3
  %mul9 = mul nsw i32 %shr8, 8
  %conv10 = trunc i32 %mul9 to i16
  %12 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %as_mv11 = bitcast %union.int_mv* %12 to %struct.mv*
  %row12 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv11, i32 0, i32 0
  store i16 %conv10, i16* %row12, align 4, !tbaa !8
  %13 = bitcast i32* %valid to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %as_mv13 = bitcast %union.int_mv* %14 to %struct.mv*
  %call = call i32 @is_mv_valid(%struct.mv* %as_mv13)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %15 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %as_mv14 = bitcast %union.int_mv* %15 to %struct.mv*
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %18 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %19 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %20 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 37
  %mib_size_log2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 9
  %22 = load i32, i32* %mib_size_log2, align 4, !tbaa !139
  %call15 = call i32 @av1_is_dv_valid(%struct.mv* byval(%struct.mv) align 2 %as_mv14, %struct.AV1Common* %16, %struct.macroblockd* %17, i32 %18, i32 %19, i8 zeroext %20, i32 %22)
  %tobool16 = icmp ne i32 %call15, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %23 = phi i1 [ false, %entry ], [ %tobool16, %land.rhs ]
  %land.ext = zext i1 %23 to i32
  store i32 %land.ext, i32* %valid, align 4, !tbaa !2
  %24 = load i32, i32* %valid, align 4, !tbaa !2
  %25 = bitcast i32* %valid to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #6
  %26 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  ret i32 %24
}

; Function Attrs: inlinehint nounwind
define internal void @convert_fullmv_to_mv(%union.int_mv* %mv) #2 {
entry:
  %mv.addr = alloca %union.int_mv*, align 4
  %tmp = alloca %struct.mv, align 2
  store %union.int_mv* %mv, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %0 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %as_mv = bitcast %union.int_mv* %0 to %struct.mv*
  %1 = bitcast %struct.mv* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %as_fullmv = bitcast %union.int_mv* %2 to %struct.fullpel_mv*
  call void @get_mv_from_fullmv(%struct.mv* sret align 2 %tmp, %struct.fullpel_mv* %as_fullmv)
  %3 = bitcast %struct.mv* %as_mv to i8*
  %4 = bitcast %struct.mv* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 2 %4, i32 4, i1 false), !tbaa.struct !140
  %5 = bitcast %struct.mv* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @get_mv_from_fullmv(%struct.mv* noalias sret align 2 %agg.result, %struct.fullpel_mv* %full_mv) #2 {
entry:
  %full_mv.addr = alloca %struct.fullpel_mv*, align 4
  store %struct.fullpel_mv* %full_mv, %struct.fullpel_mv** %full_mv.addr, align 4, !tbaa !6
  %row = getelementptr inbounds %struct.mv, %struct.mv* %agg.result, i32 0, i32 0
  %0 = load %struct.fullpel_mv*, %struct.fullpel_mv** %full_mv.addr, align 4, !tbaa !6
  %row1 = getelementptr inbounds %struct.fullpel_mv, %struct.fullpel_mv* %0, i32 0, i32 0
  %1 = load i16, i16* %row1, align 2, !tbaa !141
  %conv = sext i16 %1 to i32
  %mul = mul nsw i32 %conv, 8
  %conv2 = trunc i32 %mul to i16
  store i16 %conv2, i16* %row, align 2, !tbaa !143
  %col = getelementptr inbounds %struct.mv, %struct.mv* %agg.result, i32 0, i32 1
  %2 = load %struct.fullpel_mv*, %struct.fullpel_mv** %full_mv.addr, align 4, !tbaa !6
  %col3 = getelementptr inbounds %struct.fullpel_mv, %struct.fullpel_mv* %2, i32 0, i32 1
  %3 = load i16, i16* %col3, align 2, !tbaa !145
  %conv4 = sext i16 %3 to i32
  %mul5 = mul nsw i32 %conv4, 8
  %conv6 = trunc i32 %mul5 to i16
  store i16 %conv6, i16* %col, align 2, !tbaa !146
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @read_mv(%struct.aom_reader* %r, %struct.mv* %mv, %struct.mv* %ref, %struct.nmv_context* %ctx, i8 signext %precision) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %mv.addr = alloca %struct.mv*, align 4
  %ref.addr = alloca %struct.mv*, align 4
  %ctx.addr = alloca %struct.nmv_context*, align 4
  %precision.addr = alloca i8, align 1
  %diff = alloca %struct.mv, align 2
  %joint_type = alloca i8, align 1
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store %struct.mv* %mv, %struct.mv** %mv.addr, align 4, !tbaa !6
  store %struct.mv* %ref, %struct.mv** %ref.addr, align 4, !tbaa !6
  store %struct.nmv_context* %ctx, %struct.nmv_context** %ctx.addr, align 4, !tbaa !6
  store i8 %precision, i8* %precision.addr, align 1, !tbaa !8
  %0 = bitcast %struct.mv* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast %struct.mv* %diff to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %1, i8* align 2 bitcast (%struct.mv* @kZeroMv to i8*), i32 4, i1 false), !tbaa.struct !140
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %joint_type) #6
  %2 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %3 = load %struct.nmv_context*, %struct.nmv_context** %ctx.addr, align 4, !tbaa !6
  %joints_cdf = getelementptr inbounds %struct.nmv_context, %struct.nmv_context* %3, i32 0, i32 0
  %arraydecay = getelementptr inbounds [5 x i16], [5 x i16]* %joints_cdf, i32 0, i32 0
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %2, i16* %arraydecay, i32 4, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @__func__.read_mv, i32 0, i32 0))
  %conv = trunc i32 %call to i8
  store i8 %conv, i8* %joint_type, align 1, !tbaa !8
  %4 = load i8, i8* %joint_type, align 1, !tbaa !8
  %call1 = call i32 @mv_joint_vertical(i8 zeroext %4)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %6 = load %struct.nmv_context*, %struct.nmv_context** %ctx.addr, align 4, !tbaa !6
  %comps = getelementptr inbounds %struct.nmv_context, %struct.nmv_context* %6, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.nmv_component], [2 x %struct.nmv_component]* %comps, i32 0, i32 0
  %7 = load i8, i8* %precision.addr, align 1, !tbaa !8
  %conv2 = sext i8 %7 to i32
  %cmp = icmp sgt i32 %conv2, -1
  %conv3 = zext i1 %cmp to i32
  %8 = load i8, i8* %precision.addr, align 1, !tbaa !8
  %conv4 = sext i8 %8 to i32
  %cmp5 = icmp sgt i32 %conv4, 0
  %conv6 = zext i1 %cmp5 to i32
  %call7 = call i32 @read_mv_component(%struct.aom_reader* %5, %struct.nmv_component* %arrayidx, i32 %conv3, i32 %conv6)
  %conv8 = trunc i32 %call7 to i16
  %row = getelementptr inbounds %struct.mv, %struct.mv* %diff, i32 0, i32 0
  store i16 %conv8, i16* %row, align 2, !tbaa !143
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load i8, i8* %joint_type, align 1, !tbaa !8
  %call9 = call i32 @mv_joint_horizontal(i8 zeroext %9)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.then11, label %if.end22

if.then11:                                        ; preds = %if.end
  %10 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %11 = load %struct.nmv_context*, %struct.nmv_context** %ctx.addr, align 4, !tbaa !6
  %comps12 = getelementptr inbounds %struct.nmv_context, %struct.nmv_context* %11, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [2 x %struct.nmv_component], [2 x %struct.nmv_component]* %comps12, i32 0, i32 1
  %12 = load i8, i8* %precision.addr, align 1, !tbaa !8
  %conv14 = sext i8 %12 to i32
  %cmp15 = icmp sgt i32 %conv14, -1
  %conv16 = zext i1 %cmp15 to i32
  %13 = load i8, i8* %precision.addr, align 1, !tbaa !8
  %conv17 = sext i8 %13 to i32
  %cmp18 = icmp sgt i32 %conv17, 0
  %conv19 = zext i1 %cmp18 to i32
  %call20 = call i32 @read_mv_component(%struct.aom_reader* %10, %struct.nmv_component* %arrayidx13, i32 %conv16, i32 %conv19)
  %conv21 = trunc i32 %call20 to i16
  %col = getelementptr inbounds %struct.mv, %struct.mv* %diff, i32 0, i32 1
  store i16 %conv21, i16* %col, align 2, !tbaa !146
  br label %if.end22

if.end22:                                         ; preds = %if.then11, %if.end
  %14 = load %struct.mv*, %struct.mv** %ref.addr, align 4, !tbaa !6
  %row23 = getelementptr inbounds %struct.mv, %struct.mv* %14, i32 0, i32 0
  %15 = load i16, i16* %row23, align 2, !tbaa !143
  %conv24 = sext i16 %15 to i32
  %row25 = getelementptr inbounds %struct.mv, %struct.mv* %diff, i32 0, i32 0
  %16 = load i16, i16* %row25, align 2, !tbaa !143
  %conv26 = sext i16 %16 to i32
  %add = add nsw i32 %conv24, %conv26
  %conv27 = trunc i32 %add to i16
  %17 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row28 = getelementptr inbounds %struct.mv, %struct.mv* %17, i32 0, i32 0
  store i16 %conv27, i16* %row28, align 2, !tbaa !143
  %18 = load %struct.mv*, %struct.mv** %ref.addr, align 4, !tbaa !6
  %col29 = getelementptr inbounds %struct.mv, %struct.mv* %18, i32 0, i32 1
  %19 = load i16, i16* %col29, align 2, !tbaa !146
  %conv30 = sext i16 %19 to i32
  %col31 = getelementptr inbounds %struct.mv, %struct.mv* %diff, i32 0, i32 1
  %20 = load i16, i16* %col31, align 2, !tbaa !146
  %conv32 = sext i16 %20 to i32
  %add33 = add nsw i32 %conv30, %conv32
  %conv34 = trunc i32 %add33 to i16
  %21 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col35 = getelementptr inbounds %struct.mv, %struct.mv* %21, i32 0, i32 1
  store i16 %conv34, i16* %col35, align 2, !tbaa !146
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %joint_type) #6
  %22 = bitcast %struct.mv* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_mv_valid(%struct.mv* %mv) #2 {
entry:
  %mv.addr = alloca %struct.mv*, align 4
  store %struct.mv* %mv, %struct.mv** %mv.addr, align 4, !tbaa !6
  %0 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row = getelementptr inbounds %struct.mv, %struct.mv* %0, i32 0, i32 0
  %1 = load i16, i16* %row, align 2, !tbaa !143
  %conv = sext i16 %1 to i32
  %cmp = icmp sgt i32 %conv, -16384
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row2 = getelementptr inbounds %struct.mv, %struct.mv* %2, i32 0, i32 0
  %3 = load i16, i16* %row2, align 2, !tbaa !143
  %conv3 = sext i16 %3 to i32
  %cmp4 = icmp slt i32 %conv3, 16384
  br i1 %cmp4, label %land.lhs.true6, label %land.end

land.lhs.true6:                                   ; preds = %land.lhs.true
  %4 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col = getelementptr inbounds %struct.mv, %struct.mv* %4, i32 0, i32 1
  %5 = load i16, i16* %col, align 2, !tbaa !146
  %conv7 = sext i16 %5 to i32
  %cmp8 = icmp sgt i32 %conv7, -16384
  br i1 %cmp8, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true6
  %6 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col10 = getelementptr inbounds %struct.mv, %struct.mv* %6, i32 0, i32 1
  %7 = load i16, i16* %col10, align 2, !tbaa !146
  %conv11 = sext i16 %7 to i32
  %cmp12 = icmp slt i32 %conv11, 16384
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true6, %land.lhs.true, %entry
  %8 = phi i1 [ false, %land.lhs.true6 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp12, %land.rhs ]
  %land.ext = zext i1 %8 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_dv_valid(%struct.mv* byval(%struct.mv) align 2 %dv, %struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %mi_row, i32 %mi_col, i8 zeroext %bsize, i32 %mib_size_log2) #2 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %bsize.addr = alloca i8, align 1
  %mib_size_log2.addr = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %SCALE_PX_TO_MV = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tile = alloca %struct.TileInfo*, align 4
  %src_top_edge = alloca i32, align 4
  %tile_top_edge = alloca i32, align 4
  %src_left_edge = alloca i32, align 4
  %tile_left_edge = alloca i32, align 4
  %src_bottom_edge = alloca i32, align 4
  %tile_bottom_edge = alloca i32, align 4
  %src_right_edge = alloca i32, align 4
  %tile_right_edge = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %max_mib_size = alloca i32, align 4
  %active_sb_row = alloca i32, align 4
  %active_sb64_col = alloca i32, align 4
  %sb_size = alloca i32, align 4
  %src_sb_row = alloca i32, align 4
  %src_sb64_col = alloca i32, align 4
  %total_sb64_per_row = alloca i32, align 4
  %active_sb64 = alloca i32, align 4
  %src_sb64 = alloca i32, align 4
  %gradient = alloca i32, align 4
  %wf_offset = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !2
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  store i32 %mib_size_log2, i32* %mib_size_log2.addr, align 4, !tbaa !2
  %0 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !2
  %3 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom1
  %5 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = zext i8 %5 to i32
  store i32 %conv3, i32* %bh, align 4, !tbaa !2
  %6 = bitcast i32* %SCALE_PX_TO_MV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 8, i32* %SCALE_PX_TO_MV, align 4, !tbaa !2
  %row = getelementptr inbounds %struct.mv, %struct.mv* %dv, i32 0, i32 0
  %7 = load i16, i16* %row, align 2, !tbaa !143
  %conv4 = sext i16 %7 to i32
  %and = and i32 %conv4, 7
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %col = getelementptr inbounds %struct.mv, %struct.mv* %dv, i32 0, i32 1
  %8 = load i16, i16* %col, align 2, !tbaa !146
  %conv5 = sext i16 %8 to i32
  %and6 = and i32 %conv5, 7
  %tobool7 = icmp ne i32 %and6, 0
  br i1 %tobool7, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup137

if.end:                                           ; preds = %lor.lhs.false
  %9 = bitcast %struct.TileInfo** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %10, i32 0, i32 5
  store %struct.TileInfo* %tile8, %struct.TileInfo** %tile, align 4, !tbaa !6
  %11 = bitcast i32* %src_top_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %mul = mul nsw i32 %12, 4
  %mul9 = mul nsw i32 %mul, 8
  %row10 = getelementptr inbounds %struct.mv, %struct.mv* %dv, i32 0, i32 0
  %13 = load i16, i16* %row10, align 2, !tbaa !143
  %conv11 = sext i16 %13 to i32
  %add = add nsw i32 %mul9, %conv11
  store i32 %add, i32* %src_top_edge, align 4, !tbaa !2
  %14 = bitcast i32* %tile_top_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !6
  %mi_row_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %15, i32 0, i32 0
  %16 = load i32, i32* %mi_row_start, align 4, !tbaa !138
  %mul12 = mul nsw i32 %16, 4
  %mul13 = mul nsw i32 %mul12, 8
  store i32 %mul13, i32* %tile_top_edge, align 4, !tbaa !2
  %17 = load i32, i32* %src_top_edge, align 4, !tbaa !2
  %18 = load i32, i32* %tile_top_edge, align 4, !tbaa !2
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup134

if.end16:                                         ; preds = %if.end
  %19 = bitcast i32* %src_left_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %mul17 = mul nsw i32 %20, 4
  %mul18 = mul nsw i32 %mul17, 8
  %col19 = getelementptr inbounds %struct.mv, %struct.mv* %dv, i32 0, i32 1
  %21 = load i16, i16* %col19, align 2, !tbaa !146
  %conv20 = sext i16 %21 to i32
  %add21 = add nsw i32 %mul18, %conv20
  store i32 %add21, i32* %src_left_edge, align 4, !tbaa !2
  %22 = bitcast i32* %tile_left_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !6
  %mi_col_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %23, i32 0, i32 2
  %24 = load i32, i32* %mi_col_start, align 4, !tbaa !147
  %mul22 = mul nsw i32 %24, 4
  %mul23 = mul nsw i32 %mul22, 8
  store i32 %mul23, i32* %tile_left_edge, align 4, !tbaa !2
  %25 = load i32, i32* %src_left_edge, align 4, !tbaa !2
  %26 = load i32, i32* %tile_left_edge, align 4, !tbaa !2
  %cmp24 = icmp slt i32 %25, %26
  br i1 %cmp24, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.end16
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup132

if.end27:                                         ; preds = %if.end16
  %27 = bitcast i32* %src_bottom_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %mul28 = mul nsw i32 %28, 4
  %29 = load i32, i32* %bh, align 4, !tbaa !2
  %add29 = add nsw i32 %mul28, %29
  %mul30 = mul nsw i32 %add29, 8
  %row31 = getelementptr inbounds %struct.mv, %struct.mv* %dv, i32 0, i32 0
  %30 = load i16, i16* %row31, align 2, !tbaa !143
  %conv32 = sext i16 %30 to i32
  %add33 = add nsw i32 %mul30, %conv32
  store i32 %add33, i32* %src_bottom_edge, align 4, !tbaa !2
  %31 = bitcast i32* %tile_bottom_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !6
  %mi_row_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %32, i32 0, i32 1
  %33 = load i32, i32* %mi_row_end, align 4, !tbaa !148
  %mul34 = mul nsw i32 %33, 4
  %mul35 = mul nsw i32 %mul34, 8
  store i32 %mul35, i32* %tile_bottom_edge, align 4, !tbaa !2
  %34 = load i32, i32* %src_bottom_edge, align 4, !tbaa !2
  %35 = load i32, i32* %tile_bottom_edge, align 4, !tbaa !2
  %cmp36 = icmp sgt i32 %34, %35
  br i1 %cmp36, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.end27
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup130

if.end39:                                         ; preds = %if.end27
  %36 = bitcast i32* %src_right_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  %37 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %mul40 = mul nsw i32 %37, 4
  %38 = load i32, i32* %bw, align 4, !tbaa !2
  %add41 = add nsw i32 %mul40, %38
  %mul42 = mul nsw i32 %add41, 8
  %col43 = getelementptr inbounds %struct.mv, %struct.mv* %dv, i32 0, i32 1
  %39 = load i16, i16* %col43, align 2, !tbaa !146
  %conv44 = sext i16 %39 to i32
  %add45 = add nsw i32 %mul42, %conv44
  store i32 %add45, i32* %src_right_edge, align 4, !tbaa !2
  %40 = bitcast i32* %tile_right_edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  %41 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !6
  %mi_col_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %41, i32 0, i32 3
  %42 = load i32, i32* %mi_col_end, align 4, !tbaa !149
  %mul46 = mul nsw i32 %42, 4
  %mul47 = mul nsw i32 %mul46, 8
  store i32 %mul47, i32* %tile_right_edge, align 4, !tbaa !2
  %43 = load i32, i32* %src_right_edge, align 4, !tbaa !2
  %44 = load i32, i32* %tile_right_edge, align 4, !tbaa !2
  %cmp48 = icmp sgt i32 %43, %44
  br i1 %cmp48, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.end39
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup128

if.end51:                                         ; preds = %if.end39
  %45 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %is_chroma_ref = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %45, i32 0, i32 3
  %46 = load i8, i8* %is_chroma_ref, align 4, !tbaa !85, !range !46
  %tobool52 = trunc i8 %46 to i1
  br i1 %tobool52, label %land.lhs.true, label %if.end80

land.lhs.true:                                    ; preds = %if.end51
  %47 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call = call i32 @av1_num_planes(%struct.AV1Common* %47)
  %cmp54 = icmp sgt i32 %call, 1
  br i1 %cmp54, label %if.then56, label %if.end80

if.then56:                                        ; preds = %land.lhs.true
  %48 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #6
  %49 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %49, i32 0, i32 4
  %arrayidx57 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 1
  store %struct.macroblockd_plane* %arrayidx57, %struct.macroblockd_plane** %pd, align 4, !tbaa !6
  %50 = load i32, i32* %bw, align 4, !tbaa !2
  %cmp58 = icmp slt i32 %50, 8
  br i1 %cmp58, label %land.lhs.true60, label %if.end68

land.lhs.true60:                                  ; preds = %if.then56
  %51 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !6
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %51, i32 0, i32 4
  %52 = load i32, i32* %subsampling_x, align 4, !tbaa !126
  %tobool61 = icmp ne i32 %52, 0
  br i1 %tobool61, label %if.then62, label %if.end68

if.then62:                                        ; preds = %land.lhs.true60
  %53 = load i32, i32* %src_left_edge, align 4, !tbaa !2
  %54 = load i32, i32* %tile_left_edge, align 4, !tbaa !2
  %add63 = add nsw i32 %54, 32
  %cmp64 = icmp slt i32 %53, %add63
  br i1 %cmp64, label %if.then66, label %if.end67

if.then66:                                        ; preds = %if.then62
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end67:                                         ; preds = %if.then62
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %land.lhs.true60, %if.then56
  %55 = load i32, i32* %bh, align 4, !tbaa !2
  %cmp69 = icmp slt i32 %55, 8
  br i1 %cmp69, label %land.lhs.true71, label %if.end79

land.lhs.true71:                                  ; preds = %if.end68
  %56 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !6
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %56, i32 0, i32 5
  %57 = load i32, i32* %subsampling_y, align 4, !tbaa !129
  %tobool72 = icmp ne i32 %57, 0
  br i1 %tobool72, label %if.then73, label %if.end79

if.then73:                                        ; preds = %land.lhs.true71
  %58 = load i32, i32* %src_top_edge, align 4, !tbaa !2
  %59 = load i32, i32* %tile_top_edge, align 4, !tbaa !2
  %add74 = add nsw i32 %59, 32
  %cmp75 = icmp slt i32 %58, %add74
  br i1 %cmp75, label %if.then77, label %if.end78

if.then77:                                        ; preds = %if.then73
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end78:                                         ; preds = %if.then73
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %land.lhs.true71, %if.end68
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end79, %if.then77, %if.then66
  %60 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup128 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end80

if.end80:                                         ; preds = %cleanup.cont, %land.lhs.true, %if.end51
  %61 = bitcast i32* %max_mib_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  %62 = load i32, i32* %mib_size_log2.addr, align 4, !tbaa !2
  %shl = shl i32 1, %62
  store i32 %shl, i32* %max_mib_size, align 4, !tbaa !2
  %63 = bitcast i32* %active_sb_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #6
  %64 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %65 = load i32, i32* %mib_size_log2.addr, align 4, !tbaa !2
  %shr = ashr i32 %64, %65
  store i32 %shr, i32* %active_sb_row, align 4, !tbaa !2
  %66 = bitcast i32* %active_sb64_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  %67 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %mul81 = mul nsw i32 %67, 4
  %shr82 = ashr i32 %mul81, 6
  store i32 %shr82, i32* %active_sb64_col, align 4, !tbaa !2
  %68 = bitcast i32* %sb_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #6
  %69 = load i32, i32* %max_mib_size, align 4, !tbaa !2
  %mul83 = mul nsw i32 %69, 4
  store i32 %mul83, i32* %sb_size, align 4, !tbaa !2
  %70 = bitcast i32* %src_sb_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #6
  %71 = load i32, i32* %src_bottom_edge, align 4, !tbaa !2
  %shr84 = ashr i32 %71, 3
  %sub = sub nsw i32 %shr84, 1
  %72 = load i32, i32* %sb_size, align 4, !tbaa !2
  %div = sdiv i32 %sub, %72
  store i32 %div, i32* %src_sb_row, align 4, !tbaa !2
  %73 = bitcast i32* %src_sb64_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  %74 = load i32, i32* %src_right_edge, align 4, !tbaa !2
  %shr85 = ashr i32 %74, 3
  %sub86 = sub nsw i32 %shr85, 1
  %shr87 = ashr i32 %sub86, 6
  store i32 %shr87, i32* %src_sb64_col, align 4, !tbaa !2
  %75 = bitcast i32* %total_sb64_per_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #6
  %76 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !6
  %mi_col_end88 = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %76, i32 0, i32 3
  %77 = load i32, i32* %mi_col_end88, align 4, !tbaa !149
  %78 = load %struct.TileInfo*, %struct.TileInfo** %tile, align 4, !tbaa !6
  %mi_col_start89 = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %78, i32 0, i32 2
  %79 = load i32, i32* %mi_col_start89, align 4, !tbaa !147
  %sub90 = sub nsw i32 %77, %79
  %sub91 = sub nsw i32 %sub90, 1
  %shr92 = ashr i32 %sub91, 4
  %add93 = add nsw i32 %shr92, 1
  store i32 %add93, i32* %total_sb64_per_row, align 4, !tbaa !2
  %80 = bitcast i32* %active_sb64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #6
  %81 = load i32, i32* %active_sb_row, align 4, !tbaa !2
  %82 = load i32, i32* %total_sb64_per_row, align 4, !tbaa !2
  %mul94 = mul nsw i32 %81, %82
  %83 = load i32, i32* %active_sb64_col, align 4, !tbaa !2
  %add95 = add nsw i32 %mul94, %83
  store i32 %add95, i32* %active_sb64, align 4, !tbaa !2
  %84 = bitcast i32* %src_sb64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #6
  %85 = load i32, i32* %src_sb_row, align 4, !tbaa !2
  %86 = load i32, i32* %total_sb64_per_row, align 4, !tbaa !2
  %mul96 = mul nsw i32 %85, %86
  %87 = load i32, i32* %src_sb64_col, align 4, !tbaa !2
  %add97 = add nsw i32 %mul96, %87
  store i32 %add97, i32* %src_sb64, align 4, !tbaa !2
  %88 = load i32, i32* %src_sb64, align 4, !tbaa !2
  %89 = load i32, i32* %active_sb64, align 4, !tbaa !2
  %sub98 = sub nsw i32 %89, 4
  %cmp99 = icmp sge i32 %88, %sub98
  br i1 %cmp99, label %if.then101, label %if.end102

if.then101:                                       ; preds = %if.end80
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup119

if.end102:                                        ; preds = %if.end80
  %90 = bitcast i32* %gradient to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  %91 = load i32, i32* %sb_size, align 4, !tbaa !2
  %cmp103 = icmp sgt i32 %91, 64
  %conv104 = zext i1 %cmp103 to i32
  %add105 = add nsw i32 5, %conv104
  store i32 %add105, i32* %gradient, align 4, !tbaa !2
  %92 = bitcast i32* %wf_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #6
  %93 = load i32, i32* %gradient, align 4, !tbaa !2
  %94 = load i32, i32* %active_sb_row, align 4, !tbaa !2
  %95 = load i32, i32* %src_sb_row, align 4, !tbaa !2
  %sub106 = sub nsw i32 %94, %95
  %mul107 = mul nsw i32 %93, %sub106
  store i32 %mul107, i32* %wf_offset, align 4, !tbaa !2
  %96 = load i32, i32* %src_sb_row, align 4, !tbaa !2
  %97 = load i32, i32* %active_sb_row, align 4, !tbaa !2
  %cmp108 = icmp sgt i32 %96, %97
  br i1 %cmp108, label %if.then115, label %lor.lhs.false110

lor.lhs.false110:                                 ; preds = %if.end102
  %98 = load i32, i32* %src_sb64_col, align 4, !tbaa !2
  %99 = load i32, i32* %active_sb64_col, align 4, !tbaa !2
  %sub111 = sub nsw i32 %99, 4
  %100 = load i32, i32* %wf_offset, align 4, !tbaa !2
  %add112 = add nsw i32 %sub111, %100
  %cmp113 = icmp sge i32 %98, %add112
  br i1 %cmp113, label %if.then115, label %if.end116

if.then115:                                       ; preds = %lor.lhs.false110, %if.end102
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup117

if.end116:                                        ; preds = %lor.lhs.false110
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup117

cleanup117:                                       ; preds = %if.end116, %if.then115
  %101 = bitcast i32* %wf_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #6
  %102 = bitcast i32* %gradient to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  br label %cleanup119

cleanup119:                                       ; preds = %cleanup117, %if.then101
  %103 = bitcast i32* %src_sb64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast i32* %active_sb64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  %105 = bitcast i32* %total_sb64_per_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #6
  %106 = bitcast i32* %src_sb64_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #6
  %107 = bitcast i32* %src_sb_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #6
  %108 = bitcast i32* %sb_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #6
  %109 = bitcast i32* %active_sb64_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #6
  %110 = bitcast i32* %active_sb_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast i32* %max_mib_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  br label %cleanup128

cleanup128:                                       ; preds = %cleanup119, %cleanup, %if.then50
  %112 = bitcast i32* %tile_right_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #6
  %113 = bitcast i32* %src_right_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #6
  br label %cleanup130

cleanup130:                                       ; preds = %cleanup128, %if.then38
  %114 = bitcast i32* %tile_bottom_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #6
  %115 = bitcast i32* %src_bottom_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #6
  br label %cleanup132

cleanup132:                                       ; preds = %cleanup130, %if.then26
  %116 = bitcast i32* %tile_left_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #6
  %117 = bitcast i32* %src_left_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #6
  br label %cleanup134

cleanup134:                                       ; preds = %cleanup132, %if.then15
  %118 = bitcast i32* %tile_top_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #6
  %119 = bitcast i32* %src_top_edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #6
  %120 = bitcast %struct.TileInfo** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #6
  br label %cleanup137

cleanup137:                                       ; preds = %cleanup134, %if.then
  %121 = bitcast i32* %SCALE_PX_TO_MV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #6
  %122 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #6
  %123 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #6
  %124 = load i32, i32* %retval, align 4
  ret i32 %124
}

; Function Attrs: inlinehint nounwind
define internal i32 @mv_joint_vertical(i8 zeroext %type) #2 {
entry:
  %type.addr = alloca i8, align 1
  store i8 %type, i8* %type.addr, align 1, !tbaa !8
  %0 = load i8, i8* %type.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 2
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i8, i8* %type.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp eq i32 %conv2, 3
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp3, %lor.rhs ]
  %lor.ext = zext i1 %2 to i32
  ret i32 %lor.ext
}

; Function Attrs: nounwind
define internal i32 @read_mv_component(%struct.aom_reader* %r, %struct.nmv_component* %mvcomp, i32 %use_subpel, i32 %usehp) #0 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %mvcomp.addr = alloca %struct.nmv_component*, align 4
  %use_subpel.addr = alloca i32, align 4
  %usehp.addr = alloca i32, align 4
  %mag = alloca i32, align 4
  %d = alloca i32, align 4
  %fr = alloca i32, align 4
  %hp = alloca i32, align 4
  %sign = alloca i32, align 4
  %mv_class = alloca i32, align 4
  %class0 = alloca i32, align 4
  %n = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store %struct.nmv_component* %mvcomp, %struct.nmv_component** %mvcomp.addr, align 4, !tbaa !6
  store i32 %use_subpel, i32* %use_subpel.addr, align 4, !tbaa !2
  store i32 %usehp, i32* %usehp.addr, align 4, !tbaa !2
  %0 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %fr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %hp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %6 = load %struct.nmv_component*, %struct.nmv_component** %mvcomp.addr, align 4, !tbaa !6
  %sign_cdf = getelementptr inbounds %struct.nmv_component, %struct.nmv_component* %6, i32 0, i32 3
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %sign_cdf, i32 0, i32 0
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %5, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_mv_component, i32 0, i32 0))
  store i32 %call, i32* %sign, align 4, !tbaa !2
  %7 = bitcast i32* %mv_class to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %9 = load %struct.nmv_component*, %struct.nmv_component** %mvcomp.addr, align 4, !tbaa !6
  %classes_cdf = getelementptr inbounds %struct.nmv_component, %struct.nmv_component* %9, i32 0, i32 0
  %arraydecay1 = getelementptr inbounds [12 x i16], [12 x i16]* %classes_cdf, i32 0, i32 0
  %call2 = call i32 @aom_read_symbol_(%struct.aom_reader* %8, i16* %arraydecay1, i32 11, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_mv_component, i32 0, i32 0))
  store i32 %call2, i32* %mv_class, align 4, !tbaa !2
  %10 = bitcast i32* %class0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load i32, i32* %mv_class, align 4, !tbaa !2
  %cmp = icmp eq i32 %11, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %class0, align 4, !tbaa !2
  %12 = load i32, i32* %class0, align 4, !tbaa !2
  %tobool = icmp ne i32 %12, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %13 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %14 = load %struct.nmv_component*, %struct.nmv_component** %mvcomp.addr, align 4, !tbaa !6
  %class0_cdf = getelementptr inbounds %struct.nmv_component, %struct.nmv_component* %14, i32 0, i32 6
  %arraydecay3 = getelementptr inbounds [3 x i16], [3 x i16]* %class0_cdf, i32 0, i32 0
  %call4 = call i32 @aom_read_symbol_(%struct.aom_reader* %13, i16* %arraydecay3, i32 2, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_mv_component, i32 0, i32 0))
  store i32 %call4, i32* %d, align 4, !tbaa !2
  store i32 0, i32* %mag, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %entry
  %15 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i32, i32* %mv_class, align 4, !tbaa !2
  %add = add nsw i32 %16, 1
  %sub = sub nsw i32 %add, 1
  store i32 %sub, i32* %n, align 4, !tbaa !2
  store i32 0, i32* %d, align 4, !tbaa !2
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %18 = load i32, i32* %i, align 4, !tbaa !2
  %19 = load i32, i32* %n, align 4, !tbaa !2
  %cmp5 = icmp slt i32 %18, %19
  br i1 %cmp5, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %21 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %22 = load %struct.nmv_component*, %struct.nmv_component** %mvcomp.addr, align 4, !tbaa !6
  %bits_cdf = getelementptr inbounds %struct.nmv_component, %struct.nmv_component* %22, i32 0, i32 7
  %23 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [10 x [3 x i16]], [10 x [3 x i16]]* %bits_cdf, i32 0, i32 %23
  %arraydecay7 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx, i32 0, i32 0
  %call8 = call i32 @aom_read_symbol_(%struct.aom_reader* %21, i16* %arraydecay7, i32 2, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_mv_component, i32 0, i32 0))
  %24 = load i32, i32* %i, align 4, !tbaa !2
  %shl = shl i32 %call8, %24
  %25 = load i32, i32* %d, align 4, !tbaa !2
  %or = or i32 %25, %shl
  store i32 %or, i32* %d, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %27 = load i32, i32* %mv_class, align 4, !tbaa !2
  %add9 = add nsw i32 %27, 2
  %shl10 = shl i32 2, %add9
  store i32 %shl10, i32* %mag, align 4, !tbaa !2
  %28 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  %29 = load i32, i32* %use_subpel.addr, align 4, !tbaa !2
  %tobool11 = icmp ne i32 %29, 0
  br i1 %tobool11, label %if.then12, label %if.else31

if.then12:                                        ; preds = %if.end
  %30 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %31 = load i32, i32* %class0, align 4, !tbaa !2
  %tobool13 = icmp ne i32 %31, 0
  br i1 %tobool13, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then12
  %32 = load %struct.nmv_component*, %struct.nmv_component** %mvcomp.addr, align 4, !tbaa !6
  %class0_fp_cdf = getelementptr inbounds %struct.nmv_component, %struct.nmv_component* %32, i32 0, i32 1
  %33 = load i32, i32* %d, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds [2 x [5 x i16]], [2 x [5 x i16]]* %class0_fp_cdf, i32 0, i32 %33
  %arraydecay15 = getelementptr inbounds [5 x i16], [5 x i16]* %arrayidx14, i32 0, i32 0
  br label %cond.end

cond.false:                                       ; preds = %if.then12
  %34 = load %struct.nmv_component*, %struct.nmv_component** %mvcomp.addr, align 4, !tbaa !6
  %fp_cdf = getelementptr inbounds %struct.nmv_component, %struct.nmv_component* %34, i32 0, i32 2
  %arraydecay16 = getelementptr inbounds [5 x i16], [5 x i16]* %fp_cdf, i32 0, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i16* [ %arraydecay15, %cond.true ], [ %arraydecay16, %cond.false ]
  %call17 = call i32 @aom_read_symbol_(%struct.aom_reader* %30, i16* %cond, i32 4, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_mv_component, i32 0, i32 0))
  store i32 %call17, i32* %fr, align 4, !tbaa !2
  %35 = load i32, i32* %usehp.addr, align 4, !tbaa !2
  %tobool18 = icmp ne i32 %35, 0
  br i1 %tobool18, label %cond.true19, label %cond.false28

cond.true19:                                      ; preds = %cond.end
  %36 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %37 = load i32, i32* %class0, align 4, !tbaa !2
  %tobool20 = icmp ne i32 %37, 0
  br i1 %tobool20, label %cond.true21, label %cond.false23

cond.true21:                                      ; preds = %cond.true19
  %38 = load %struct.nmv_component*, %struct.nmv_component** %mvcomp.addr, align 4, !tbaa !6
  %class0_hp_cdf = getelementptr inbounds %struct.nmv_component, %struct.nmv_component* %38, i32 0, i32 4
  %arraydecay22 = getelementptr inbounds [3 x i16], [3 x i16]* %class0_hp_cdf, i32 0, i32 0
  br label %cond.end25

cond.false23:                                     ; preds = %cond.true19
  %39 = load %struct.nmv_component*, %struct.nmv_component** %mvcomp.addr, align 4, !tbaa !6
  %hp_cdf = getelementptr inbounds %struct.nmv_component, %struct.nmv_component* %39, i32 0, i32 5
  %arraydecay24 = getelementptr inbounds [3 x i16], [3 x i16]* %hp_cdf, i32 0, i32 0
  br label %cond.end25

cond.end25:                                       ; preds = %cond.false23, %cond.true21
  %cond26 = phi i16* [ %arraydecay22, %cond.true21 ], [ %arraydecay24, %cond.false23 ]
  %call27 = call i32 @aom_read_symbol_(%struct.aom_reader* %36, i16* %cond26, i32 2, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.read_mv_component, i32 0, i32 0))
  br label %cond.end29

cond.false28:                                     ; preds = %cond.end
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false28, %cond.end25
  %cond30 = phi i32 [ %call27, %cond.end25 ], [ 1, %cond.false28 ]
  store i32 %cond30, i32* %hp, align 4, !tbaa !2
  br label %if.end32

if.else31:                                        ; preds = %if.end
  store i32 3, i32* %fr, align 4, !tbaa !2
  store i32 1, i32* %hp, align 4, !tbaa !2
  br label %if.end32

if.end32:                                         ; preds = %if.else31, %cond.end29
  %40 = load i32, i32* %d, align 4, !tbaa !2
  %shl33 = shl i32 %40, 3
  %41 = load i32, i32* %fr, align 4, !tbaa !2
  %shl34 = shl i32 %41, 1
  %or35 = or i32 %shl33, %shl34
  %42 = load i32, i32* %hp, align 4, !tbaa !2
  %or36 = or i32 %or35, %42
  %add37 = add nsw i32 %or36, 1
  %43 = load i32, i32* %mag, align 4, !tbaa !2
  %add38 = add nsw i32 %43, %add37
  store i32 %add38, i32* %mag, align 4, !tbaa !2
  %44 = load i32, i32* %sign, align 4, !tbaa !2
  %tobool39 = icmp ne i32 %44, 0
  br i1 %tobool39, label %cond.true40, label %cond.false42

cond.true40:                                      ; preds = %if.end32
  %45 = load i32, i32* %mag, align 4, !tbaa !2
  %sub41 = sub nsw i32 0, %45
  br label %cond.end43

cond.false42:                                     ; preds = %if.end32
  %46 = load i32, i32* %mag, align 4, !tbaa !2
  br label %cond.end43

cond.end43:                                       ; preds = %cond.false42, %cond.true40
  %cond44 = phi i32 [ %sub41, %cond.true40 ], [ %46, %cond.false42 ]
  %47 = bitcast i32* %class0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  %48 = bitcast i32* %mv_class to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #6
  %49 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  %50 = bitcast i32* %hp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  %51 = bitcast i32* %fr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  %52 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  %53 = bitcast i32* %mag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #6
  ret i32 %cond44
}

; Function Attrs: inlinehint nounwind
define internal i32 @mv_joint_horizontal(i8 zeroext %type) #2 {
entry:
  %type.addr = alloca i8, align 1
  store i8 %type, i8* %type.addr, align 1, !tbaa !8
  %0 = load i8, i8* %type.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 1
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i8, i8* %type.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp eq i32 %conv2, 3
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp3, %lor.rhs ]
  %lor.ext = zext i1 %2 to i32
  ret i32 %lor.ext
}

declare zeroext i8 @av1_above_block_mode(%struct.MB_MODE_INFO*) #3

declare zeroext i8 @av1_left_block_mode(%struct.MB_MODE_INFO*) #3

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_plane_block_size(i8 zeroext %bsize, i32 %subsampling_x, i32 %subsampling_y) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !2
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !2
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x [2 x [2 x i8]]], [22 x [2 x [2 x i8]]]* @ss_size_lookup, i32 0, i32 %idxprom
  %1 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds [2 x [2 x i8]], [2 x [2 x i8]]* %arrayidx, i32 0, i32 %1
  %2 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx1, i32 0, i32 %2
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  ret i8 %3
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_get_palette_bsize_ctx(i8 zeroext %bsize) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @num_pels_log2_lookup, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %1 to i32
  %2 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @num_pels_log2_lookup, i32 0, i32 3), align 1, !tbaa !8
  %conv1 = zext i8 %2 to i32
  %sub = sub nsw i32 %conv, %conv1
  ret i32 %sub
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_get_palette_mode_ctx(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %above_mi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mi = alloca %struct.MB_MODE_INFO*, align 4
  %ctx = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 12
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 16, !tbaa !74
  store %struct.MB_MODE_INFO* %2, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %3 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 11
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !75
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %6 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 0, i32* %ctx, align 4, !tbaa !2
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %tobool = icmp ne %struct.MB_MODE_INFO* %7, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 5
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 0
  %9 = load i8, i8* %arrayidx, align 4, !tbaa !8
  %conv = zext i8 %9 to i32
  %cmp = icmp sgt i32 %conv, 0
  %conv1 = zext i1 %cmp to i32
  %10 = load i32, i32* %ctx, align 4, !tbaa !2
  %add = add nsw i32 %10, %conv1
  store i32 %add, i32* %ctx, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %tobool2 = icmp ne %struct.MB_MODE_INFO* %11, null
  br i1 %tobool2, label %if.then3, label %if.end11

if.then3:                                         ; preds = %if.end
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %palette_mode_info4 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %12, i32 0, i32 5
  %palette_size5 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info4, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size5, i32 0, i32 0
  %13 = load i8, i8* %arrayidx6, align 4, !tbaa !8
  %conv7 = zext i8 %13 to i32
  %cmp8 = icmp sgt i32 %conv7, 0
  %conv9 = zext i1 %cmp8 to i32
  %14 = load i32, i32* %ctx, align 4, !tbaa !2
  %add10 = add nsw i32 %14, %conv9
  store i32 %add10, i32* %ctx, align 4, !tbaa !2
  br label %if.end11

if.end11:                                         ; preds = %if.then3, %if.end
  %15 = load i32, i32* %ctx, align 4, !tbaa !2
  %16 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  ret i32 %15
}

; Function Attrs: nounwind
define internal void @read_palette_colors_y(%struct.macroblockd* %xd, i32 %bit_depth, %struct.PALETTE_MODE_INFO* %pmi, %struct.aom_reader* %r) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bit_depth.addr = alloca i32, align 4
  %pmi.addr = alloca %struct.PALETTE_MODE_INFO*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %color_cache = alloca [16 x i16], align 16
  %cached_colors = alloca [8 x i16], align 16
  %n_cache = alloca i32, align 4
  %n = alloca i32, align 4
  %idx = alloca i32, align 4
  %i = alloca i32, align 4
  %n_cached_colors = alloca i32, align 4
  %min_bits = alloca i32, align 4
  %bits = alloca i32, align 4
  %range = alloca i32, align 4
  %delta = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !2
  store %struct.PALETTE_MODE_INFO* %pmi, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast [16 x i16]* %color_cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %0) #6
  %1 = bitcast [8 x i16]* %cached_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %2 = bitcast i32* %n_cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [16 x i16], [16 x i16]* %color_cache, i32 0, i32 0
  %call = call i32 @av1_get_palette_cache(%struct.macroblockd* %3, i32 0, i16* %arraydecay)
  store i32 %call, i32* %n_cache, align 4, !tbaa !2
  %4 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %5, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 0
  %6 = load i8, i8* %arrayidx, align 2, !tbaa !8
  %conv = zext i8 %6 to i32
  store i32 %conv, i32* %n, align 4, !tbaa !2
  %7 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store i32 0, i32* %idx, align 4, !tbaa !2
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %i, align 4, !tbaa !2
  %10 = load i32, i32* %n_cache, align 4, !tbaa !2
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %11 = load i32, i32* %idx, align 4, !tbaa !2
  %12 = load i32, i32* %n, align 4, !tbaa !2
  %cmp2 = icmp slt i32 %11, %12
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %13 = phi i1 [ false, %for.cond ], [ %cmp2, %land.rhs ]
  br i1 %13, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %land.end
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %for.end

for.body:                                         ; preds = %land.end
  %15 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call4 = call i32 @aom_read_bit_(%struct.aom_reader* %15, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @__func__.read_palette_colors_y, i32 0, i32 0))
  %tobool = icmp ne i32 %call4, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds [16 x i16], [16 x i16]* %color_cache, i32 0, i32 %16
  %17 = load i16, i16* %arrayidx5, align 2, !tbaa !101
  %18 = load i32, i32* %idx, align 4, !tbaa !2
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds [8 x i16], [8 x i16]* %cached_colors, i32 0, i32 %18
  store i16 %17, i16* %arrayidx6, align 2, !tbaa !101
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4, !tbaa !2
  %inc7 = add nsw i32 %19, 1
  store i32 %inc7, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %20 = load i32, i32* %idx, align 4, !tbaa !2
  %21 = load i32, i32* %n, align 4, !tbaa !2
  %cmp8 = icmp slt i32 %20, %21
  br i1 %cmp8, label %if.then10, label %if.else

if.then10:                                        ; preds = %for.end
  %22 = bitcast i32* %n_cached_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load i32, i32* %idx, align 4, !tbaa !2
  store i32 %23, i32* %n_cached_colors, align 4, !tbaa !2
  %24 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %25 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %call11 = call i32 @aom_read_literal_(%struct.aom_reader* %24, i32 %25, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @__func__.read_palette_colors_y, i32 0, i32 0))
  %conv12 = trunc i32 %call11 to i16
  %26 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %26, i32 0, i32 0
  %27 = load i32, i32* %idx, align 4, !tbaa !2
  %inc13 = add nsw i32 %27, 1
  store i32 %inc13, i32* %idx, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors, i32 0, i32 %27
  store i16 %conv12, i16* %arrayidx14, align 2, !tbaa !101
  %28 = load i32, i32* %idx, align 4, !tbaa !2
  %29 = load i32, i32* %n, align 4, !tbaa !2
  %cmp15 = icmp slt i32 %28, %29
  br i1 %cmp15, label %if.then17, label %if.end58

if.then17:                                        ; preds = %if.then10
  %30 = bitcast i32* %min_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %31, 3
  store i32 %sub, i32* %min_bits, align 4, !tbaa !2
  %32 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  %33 = load i32, i32* %min_bits, align 4, !tbaa !2
  %34 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call18 = call i32 @aom_read_literal_(%struct.aom_reader* %34, i32 2, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @__func__.read_palette_colors_y, i32 0, i32 0))
  %add = add nsw i32 %33, %call18
  store i32 %add, i32* %bits, align 4, !tbaa !2
  %35 = bitcast i32* %range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %36 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %shl = shl i32 1, %36
  %37 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors19 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %37, i32 0, i32 0
  %38 = load i32, i32* %idx, align 4, !tbaa !2
  %sub20 = sub nsw i32 %38, 1
  %arrayidx21 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors19, i32 0, i32 %sub20
  %39 = load i16, i16* %arrayidx21, align 2, !tbaa !101
  %conv22 = zext i16 %39 to i32
  %sub23 = sub nsw i32 %shl, %conv22
  %sub24 = sub nsw i32 %sub23, 1
  store i32 %sub24, i32* %range, align 4, !tbaa !2
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc55, %if.then17
  %40 = load i32, i32* %idx, align 4, !tbaa !2
  %41 = load i32, i32* %n, align 4, !tbaa !2
  %cmp26 = icmp slt i32 %40, %41
  br i1 %cmp26, label %for.body28, label %for.end57

for.body28:                                       ; preds = %for.cond25
  %42 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #6
  %43 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %44 = load i32, i32* %bits, align 4, !tbaa !2
  %call29 = call i32 @aom_read_literal_(%struct.aom_reader* %43, i32 %44, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @__func__.read_palette_colors_y, i32 0, i32 0))
  %add30 = add nsw i32 %call29, 1
  store i32 %add30, i32* %delta, align 4, !tbaa !2
  %45 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors31 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %45, i32 0, i32 0
  %46 = load i32, i32* %idx, align 4, !tbaa !2
  %sub32 = sub nsw i32 %46, 1
  %arrayidx33 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors31, i32 0, i32 %sub32
  %47 = load i16, i16* %arrayidx33, align 2, !tbaa !101
  %conv34 = zext i16 %47 to i32
  %48 = load i32, i32* %delta, align 4, !tbaa !2
  %add35 = add nsw i32 %conv34, %48
  %49 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %shl36 = shl i32 1, %49
  %sub37 = sub nsw i32 %shl36, 1
  %call38 = call i32 @clamp(i32 %add35, i32 0, i32 %sub37)
  %conv39 = trunc i32 %call38 to i16
  %50 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors40 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %50, i32 0, i32 0
  %51 = load i32, i32* %idx, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors40, i32 0, i32 %51
  store i16 %conv39, i16* %arrayidx41, align 2, !tbaa !101
  %52 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors42 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %52, i32 0, i32 0
  %53 = load i32, i32* %idx, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors42, i32 0, i32 %53
  %54 = load i16, i16* %arrayidx43, align 2, !tbaa !101
  %conv44 = zext i16 %54 to i32
  %55 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors45 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %55, i32 0, i32 0
  %56 = load i32, i32* %idx, align 4, !tbaa !2
  %sub46 = sub nsw i32 %56, 1
  %arrayidx47 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors45, i32 0, i32 %sub46
  %57 = load i16, i16* %arrayidx47, align 2, !tbaa !101
  %conv48 = zext i16 %57 to i32
  %sub49 = sub nsw i32 %conv44, %conv48
  %58 = load i32, i32* %range, align 4, !tbaa !2
  %sub50 = sub nsw i32 %58, %sub49
  store i32 %sub50, i32* %range, align 4, !tbaa !2
  %59 = load i32, i32* %bits, align 4, !tbaa !2
  %60 = load i32, i32* %range, align 4, !tbaa !2
  %call51 = call i32 @av1_ceil_log2(i32 %60)
  %cmp52 = icmp slt i32 %59, %call51
  br i1 %cmp52, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body28
  %61 = load i32, i32* %bits, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %for.body28
  %62 = load i32, i32* %range, align 4, !tbaa !2
  %call54 = call i32 @av1_ceil_log2(i32 %62)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %61, %cond.true ], [ %call54, %cond.false ]
  store i32 %cond, i32* %bits, align 4, !tbaa !2
  %63 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  br label %for.inc55

for.inc55:                                        ; preds = %cond.end
  %64 = load i32, i32* %idx, align 4, !tbaa !2
  %inc56 = add nsw i32 %64, 1
  store i32 %inc56, i32* %idx, align 4, !tbaa !2
  br label %for.cond25

for.end57:                                        ; preds = %for.cond25
  %65 = bitcast i32* %range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  %66 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = bitcast i32* %min_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  br label %if.end58

if.end58:                                         ; preds = %for.end57, %if.then10
  %68 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors59 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %68, i32 0, i32 0
  %arraydecay60 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors59, i32 0, i32 0
  %arraydecay61 = getelementptr inbounds [8 x i16], [8 x i16]* %cached_colors, i32 0, i32 0
  %69 = load i32, i32* %n, align 4, !tbaa !2
  %70 = load i32, i32* %n_cached_colors, align 4, !tbaa !2
  call void @merge_colors(i16* %arraydecay60, i16* %arraydecay61, i32 %69, i32 %70)
  %71 = bitcast i32* %n_cached_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  br label %if.end65

if.else:                                          ; preds = %for.end
  %72 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors62 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %72, i32 0, i32 0
  %arraydecay63 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors62, i32 0, i32 0
  %73 = bitcast i16* %arraydecay63 to i8*
  %arraydecay64 = getelementptr inbounds [8 x i16], [8 x i16]* %cached_colors, i32 0, i32 0
  %74 = bitcast i16* %arraydecay64 to i8*
  %75 = load i32, i32* %n, align 4, !tbaa !2
  %mul = mul i32 %75, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %73, i8* align 16 %74, i32 %mul, i1 false)
  br label %if.end65

if.end65:                                         ; preds = %if.else, %if.end58
  %76 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  %77 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #6
  %78 = bitcast i32* %n_cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #6
  %79 = bitcast [8 x i16]* %cached_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %79) #6
  %80 = bitcast [16 x i16]* %color_cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %80) #6
  ret void
}

; Function Attrs: nounwind
define internal void @read_palette_colors_uv(%struct.macroblockd* %xd, i32 %bit_depth, %struct.PALETTE_MODE_INFO* %pmi, %struct.aom_reader* %r) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bit_depth.addr = alloca i32, align 4
  %pmi.addr = alloca %struct.PALETTE_MODE_INFO*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %n = alloca i32, align 4
  %color_cache = alloca [16 x i16], align 16
  %cached_colors = alloca [8 x i16], align 16
  %n_cache = alloca i32, align 4
  %idx = alloca i32, align 4
  %i = alloca i32, align 4
  %n_cached_colors = alloca i32, align 4
  %min_bits = alloca i32, align 4
  %bits = alloca i32, align 4
  %range = alloca i32, align 4
  %delta = alloca i32, align 4
  %min_bits_v = alloca i32, align 4
  %max_val = alloca i32, align 4
  %bits73 = alloca i32, align 4
  %i80 = alloca i32, align 4
  %delta86 = alloca i32, align 4
  %val = alloca i32, align 4
  %i118 = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !2
  store %struct.PALETTE_MODE_INFO* %pmi, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 1
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %n, align 4, !tbaa !2
  %3 = bitcast [16 x i16]* %color_cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %3) #6
  %4 = bitcast [8 x i16]* %cached_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #6
  %5 = bitcast i32* %n_cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [16 x i16], [16 x i16]* %color_cache, i32 0, i32 0
  %call = call i32 @av1_get_palette_cache(%struct.macroblockd* %6, i32 1, i16* %arraydecay)
  store i32 %call, i32* %n_cache, align 4, !tbaa !2
  %7 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store i32 0, i32* %idx, align 4, !tbaa !2
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %i, align 4, !tbaa !2
  %10 = load i32, i32* %n_cache, align 4, !tbaa !2
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %11 = load i32, i32* %idx, align 4, !tbaa !2
  %12 = load i32, i32* %n, align 4, !tbaa !2
  %cmp2 = icmp slt i32 %11, %12
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %13 = phi i1 [ false, %for.cond ], [ %cmp2, %land.rhs ]
  br i1 %13, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %land.end
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %for.end

for.body:                                         ; preds = %land.end
  %15 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call4 = call i32 @aom_read_bit_(%struct.aom_reader* %15, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  %tobool = icmp ne i32 %call4, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds [16 x i16], [16 x i16]* %color_cache, i32 0, i32 %16
  %17 = load i16, i16* %arrayidx5, align 2, !tbaa !101
  %18 = load i32, i32* %idx, align 4, !tbaa !2
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds [8 x i16], [8 x i16]* %cached_colors, i32 0, i32 %18
  store i16 %17, i16* %arrayidx6, align 2, !tbaa !101
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4, !tbaa !2
  %inc7 = add nsw i32 %19, 1
  store i32 %inc7, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %20 = load i32, i32* %idx, align 4, !tbaa !2
  %21 = load i32, i32* %n, align 4, !tbaa !2
  %cmp8 = icmp slt i32 %20, %21
  br i1 %cmp8, label %if.then10, label %if.else

if.then10:                                        ; preds = %for.end
  %22 = bitcast i32* %n_cached_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load i32, i32* %idx, align 4, !tbaa !2
  store i32 %23, i32* %n_cached_colors, align 4, !tbaa !2
  %24 = load i32, i32* %idx, align 4, !tbaa !2
  %add = add nsw i32 %24, 8
  store i32 %add, i32* %idx, align 4, !tbaa !2
  %25 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %26 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %call11 = call i32 @aom_read_literal_(%struct.aom_reader* %25, i32 %26, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  %conv12 = trunc i32 %call11 to i16
  %27 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %27, i32 0, i32 0
  %28 = load i32, i32* %idx, align 4, !tbaa !2
  %inc13 = add nsw i32 %28, 1
  store i32 %inc13, i32* %idx, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors, i32 0, i32 %28
  store i16 %conv12, i16* %arrayidx14, align 2, !tbaa !101
  %29 = load i32, i32* %idx, align 4, !tbaa !2
  %30 = load i32, i32* %n, align 4, !tbaa !2
  %add15 = add nsw i32 8, %30
  %cmp16 = icmp slt i32 %29, %add15
  br i1 %cmp16, label %if.then18, label %if.end59

if.then18:                                        ; preds = %if.then10
  %31 = bitcast i32* %min_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %32, 3
  store i32 %sub, i32* %min_bits, align 4, !tbaa !2
  %33 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  %34 = load i32, i32* %min_bits, align 4, !tbaa !2
  %35 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call19 = call i32 @aom_read_literal_(%struct.aom_reader* %35, i32 2, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  %add20 = add nsw i32 %34, %call19
  store i32 %add20, i32* %bits, align 4, !tbaa !2
  %36 = bitcast i32* %range to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  %37 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %shl = shl i32 1, %37
  %38 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors21 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %38, i32 0, i32 0
  %39 = load i32, i32* %idx, align 4, !tbaa !2
  %sub22 = sub nsw i32 %39, 1
  %arrayidx23 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors21, i32 0, i32 %sub22
  %40 = load i16, i16* %arrayidx23, align 2, !tbaa !101
  %conv24 = zext i16 %40 to i32
  %sub25 = sub nsw i32 %shl, %conv24
  store i32 %sub25, i32* %range, align 4, !tbaa !2
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc56, %if.then18
  %41 = load i32, i32* %idx, align 4, !tbaa !2
  %42 = load i32, i32* %n, align 4, !tbaa !2
  %add27 = add nsw i32 8, %42
  %cmp28 = icmp slt i32 %41, %add27
  br i1 %cmp28, label %for.body30, label %for.end58

for.body30:                                       ; preds = %for.cond26
  %43 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  %44 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %45 = load i32, i32* %bits, align 4, !tbaa !2
  %call31 = call i32 @aom_read_literal_(%struct.aom_reader* %44, i32 %45, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  store i32 %call31, i32* %delta, align 4, !tbaa !2
  %46 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors32 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %46, i32 0, i32 0
  %47 = load i32, i32* %idx, align 4, !tbaa !2
  %sub33 = sub nsw i32 %47, 1
  %arrayidx34 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors32, i32 0, i32 %sub33
  %48 = load i16, i16* %arrayidx34, align 2, !tbaa !101
  %conv35 = zext i16 %48 to i32
  %49 = load i32, i32* %delta, align 4, !tbaa !2
  %add36 = add nsw i32 %conv35, %49
  %50 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %shl37 = shl i32 1, %50
  %sub38 = sub nsw i32 %shl37, 1
  %call39 = call i32 @clamp(i32 %add36, i32 0, i32 %sub38)
  %conv40 = trunc i32 %call39 to i16
  %51 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors41 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %51, i32 0, i32 0
  %52 = load i32, i32* %idx, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors41, i32 0, i32 %52
  store i16 %conv40, i16* %arrayidx42, align 2, !tbaa !101
  %53 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors43 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %53, i32 0, i32 0
  %54 = load i32, i32* %idx, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors43, i32 0, i32 %54
  %55 = load i16, i16* %arrayidx44, align 2, !tbaa !101
  %conv45 = zext i16 %55 to i32
  %56 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors46 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %56, i32 0, i32 0
  %57 = load i32, i32* %idx, align 4, !tbaa !2
  %sub47 = sub nsw i32 %57, 1
  %arrayidx48 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors46, i32 0, i32 %sub47
  %58 = load i16, i16* %arrayidx48, align 2, !tbaa !101
  %conv49 = zext i16 %58 to i32
  %sub50 = sub nsw i32 %conv45, %conv49
  %59 = load i32, i32* %range, align 4, !tbaa !2
  %sub51 = sub nsw i32 %59, %sub50
  store i32 %sub51, i32* %range, align 4, !tbaa !2
  %60 = load i32, i32* %bits, align 4, !tbaa !2
  %61 = load i32, i32* %range, align 4, !tbaa !2
  %call52 = call i32 @av1_ceil_log2(i32 %61)
  %cmp53 = icmp slt i32 %60, %call52
  br i1 %cmp53, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body30
  %62 = load i32, i32* %bits, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %for.body30
  %63 = load i32, i32* %range, align 4, !tbaa !2
  %call55 = call i32 @av1_ceil_log2(i32 %63)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %62, %cond.true ], [ %call55, %cond.false ]
  store i32 %cond, i32* %bits, align 4, !tbaa !2
  %64 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  br label %for.inc56

for.inc56:                                        ; preds = %cond.end
  %65 = load i32, i32* %idx, align 4, !tbaa !2
  %inc57 = add nsw i32 %65, 1
  store i32 %inc57, i32* %idx, align 4, !tbaa !2
  br label %for.cond26

for.end58:                                        ; preds = %for.cond26
  %66 = bitcast i32* %range to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast i32* %min_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  br label %if.end59

if.end59:                                         ; preds = %for.end58, %if.then10
  %69 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors60 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %69, i32 0, i32 0
  %arraydecay61 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors60, i32 0, i32 0
  %add.ptr = getelementptr inbounds i16, i16* %arraydecay61, i32 8
  %arraydecay62 = getelementptr inbounds [8 x i16], [8 x i16]* %cached_colors, i32 0, i32 0
  %70 = load i32, i32* %n, align 4, !tbaa !2
  %71 = load i32, i32* %n_cached_colors, align 4, !tbaa !2
  call void @merge_colors(i16* %add.ptr, i16* %arraydecay62, i32 %70, i32 %71)
  %72 = bitcast i32* %n_cached_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #6
  br label %if.end67

if.else:                                          ; preds = %for.end
  %73 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors63 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %73, i32 0, i32 0
  %arraydecay64 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors63, i32 0, i32 0
  %add.ptr65 = getelementptr inbounds i16, i16* %arraydecay64, i32 8
  %74 = bitcast i16* %add.ptr65 to i8*
  %arraydecay66 = getelementptr inbounds [8 x i16], [8 x i16]* %cached_colors, i32 0, i32 0
  %75 = bitcast i16* %arraydecay66 to i8*
  %76 = load i32, i32* %n, align 4, !tbaa !2
  %mul = mul i32 %76, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %74, i8* align 16 %75, i32 %mul, i1 false)
  br label %if.end67

if.end67:                                         ; preds = %if.else, %if.end59
  %77 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call68 = call i32 @aom_read_bit_(%struct.aom_reader* %77, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  %tobool69 = icmp ne i32 %call68, 0
  br i1 %tobool69, label %if.then70, label %if.else117

if.then70:                                        ; preds = %if.end67
  %78 = bitcast i32* %min_bits_v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #6
  %79 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %sub71 = sub nsw i32 %79, 4
  store i32 %sub71, i32* %min_bits_v, align 4, !tbaa !2
  %80 = bitcast i32* %max_val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #6
  %81 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %shl72 = shl i32 1, %81
  store i32 %shl72, i32* %max_val, align 4, !tbaa !2
  %82 = bitcast i32* %bits73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #6
  %83 = load i32, i32* %min_bits_v, align 4, !tbaa !2
  %84 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call74 = call i32 @aom_read_literal_(%struct.aom_reader* %84, i32 2, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  %add75 = add nsw i32 %83, %call74
  store i32 %add75, i32* %bits73, align 4, !tbaa !2
  %85 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %86 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %call76 = call i32 @aom_read_literal_(%struct.aom_reader* %85, i32 %86, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  %conv77 = trunc i32 %call76 to i16
  %87 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors78 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %87, i32 0, i32 0
  %arrayidx79 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors78, i32 0, i32 16
  store i16 %conv77, i16* %arrayidx79, align 2, !tbaa !101
  %88 = bitcast i32* %i80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #6
  store i32 1, i32* %i80, align 4, !tbaa !2
  br label %for.cond81

for.cond81:                                       ; preds = %for.inc114, %if.then70
  %89 = load i32, i32* %i80, align 4, !tbaa !2
  %90 = load i32, i32* %n, align 4, !tbaa !2
  %cmp82 = icmp slt i32 %89, %90
  br i1 %cmp82, label %for.body85, label %for.cond.cleanup84

for.cond.cleanup84:                               ; preds = %for.cond81
  %91 = bitcast i32* %i80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #6
  br label %for.end116

for.body85:                                       ; preds = %for.cond81
  %92 = bitcast i32* %delta86 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #6
  %93 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %94 = load i32, i32* %bits73, align 4, !tbaa !2
  %call87 = call i32 @aom_read_literal_(%struct.aom_reader* %93, i32 %94, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  store i32 %call87, i32* %delta86, align 4, !tbaa !2
  %95 = load i32, i32* %delta86, align 4, !tbaa !2
  %tobool88 = icmp ne i32 %95, 0
  br i1 %tobool88, label %land.lhs.true, label %if.end93

land.lhs.true:                                    ; preds = %for.body85
  %96 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call89 = call i32 @aom_read_bit_(%struct.aom_reader* %96, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  %tobool90 = icmp ne i32 %call89, 0
  br i1 %tobool90, label %if.then91, label %if.end93

if.then91:                                        ; preds = %land.lhs.true
  %97 = load i32, i32* %delta86, align 4, !tbaa !2
  %sub92 = sub nsw i32 0, %97
  store i32 %sub92, i32* %delta86, align 4, !tbaa !2
  br label %if.end93

if.end93:                                         ; preds = %if.then91, %land.lhs.true, %for.body85
  %98 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #6
  %99 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors94 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %99, i32 0, i32 0
  %100 = load i32, i32* %i80, align 4, !tbaa !2
  %add95 = add nsw i32 16, %100
  %sub96 = sub nsw i32 %add95, 1
  %arrayidx97 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors94, i32 0, i32 %sub96
  %101 = load i16, i16* %arrayidx97, align 2, !tbaa !101
  %conv98 = zext i16 %101 to i32
  %102 = load i32, i32* %delta86, align 4, !tbaa !2
  %add99 = add nsw i32 %conv98, %102
  store i32 %add99, i32* %val, align 4, !tbaa !2
  %103 = load i32, i32* %val, align 4, !tbaa !2
  %cmp100 = icmp slt i32 %103, 0
  br i1 %cmp100, label %if.then102, label %if.end104

if.then102:                                       ; preds = %if.end93
  %104 = load i32, i32* %max_val, align 4, !tbaa !2
  %105 = load i32, i32* %val, align 4, !tbaa !2
  %add103 = add nsw i32 %105, %104
  store i32 %add103, i32* %val, align 4, !tbaa !2
  br label %if.end104

if.end104:                                        ; preds = %if.then102, %if.end93
  %106 = load i32, i32* %val, align 4, !tbaa !2
  %107 = load i32, i32* %max_val, align 4, !tbaa !2
  %cmp105 = icmp sge i32 %106, %107
  br i1 %cmp105, label %if.then107, label %if.end109

if.then107:                                       ; preds = %if.end104
  %108 = load i32, i32* %max_val, align 4, !tbaa !2
  %109 = load i32, i32* %val, align 4, !tbaa !2
  %sub108 = sub nsw i32 %109, %108
  store i32 %sub108, i32* %val, align 4, !tbaa !2
  br label %if.end109

if.end109:                                        ; preds = %if.then107, %if.end104
  %110 = load i32, i32* %val, align 4, !tbaa !2
  %conv110 = trunc i32 %110 to i16
  %111 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors111 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %111, i32 0, i32 0
  %112 = load i32, i32* %i80, align 4, !tbaa !2
  %add112 = add nsw i32 16, %112
  %arrayidx113 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors111, i32 0, i32 %add112
  store i16 %conv110, i16* %arrayidx113, align 2, !tbaa !101
  %113 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #6
  %114 = bitcast i32* %delta86 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #6
  br label %for.inc114

for.inc114:                                       ; preds = %if.end109
  %115 = load i32, i32* %i80, align 4, !tbaa !2
  %inc115 = add nsw i32 %115, 1
  store i32 %inc115, i32* %i80, align 4, !tbaa !2
  br label %for.cond81

for.end116:                                       ; preds = %for.cond.cleanup84
  %116 = bitcast i32* %bits73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #6
  %117 = bitcast i32* %max_val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #6
  %118 = bitcast i32* %min_bits_v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #6
  br label %if.end132

if.else117:                                       ; preds = %if.end67
  %119 = bitcast i32* %i118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #6
  store i32 0, i32* %i118, align 4, !tbaa !2
  br label %for.cond119

for.cond119:                                      ; preds = %for.inc129, %if.else117
  %120 = load i32, i32* %i118, align 4, !tbaa !2
  %121 = load i32, i32* %n, align 4, !tbaa !2
  %cmp120 = icmp slt i32 %120, %121
  br i1 %cmp120, label %for.body123, label %for.cond.cleanup122

for.cond.cleanup122:                              ; preds = %for.cond119
  %122 = bitcast i32* %i118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #6
  br label %for.end131

for.body123:                                      ; preds = %for.cond119
  %123 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %124 = load i32, i32* %bit_depth.addr, align 4, !tbaa !2
  %call124 = call i32 @aom_read_literal_(%struct.aom_reader* %123, i32 %124, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.read_palette_colors_uv, i32 0, i32 0))
  %conv125 = trunc i32 %call124 to i16
  %125 = load %struct.PALETTE_MODE_INFO*, %struct.PALETTE_MODE_INFO** %pmi.addr, align 4, !tbaa !6
  %palette_colors126 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %125, i32 0, i32 0
  %126 = load i32, i32* %i118, align 4, !tbaa !2
  %add127 = add nsw i32 16, %126
  %arrayidx128 = getelementptr inbounds [24 x i16], [24 x i16]* %palette_colors126, i32 0, i32 %add127
  store i16 %conv125, i16* %arrayidx128, align 2, !tbaa !101
  br label %for.inc129

for.inc129:                                       ; preds = %for.body123
  %127 = load i32, i32* %i118, align 4, !tbaa !2
  %inc130 = add nsw i32 %127, 1
  store i32 %inc130, i32* %i118, align 4, !tbaa !2
  br label %for.cond119

for.end131:                                       ; preds = %for.cond.cleanup122
  br label %if.end132

if.end132:                                        ; preds = %for.end131, %for.end116
  %128 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #6
  %129 = bitcast i32* %n_cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #6
  %130 = bitcast [8 x i16]* %cached_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %130) #6
  %131 = bitcast [16 x i16]* %color_cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %131) #6
  %132 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #6
  ret void
}

declare i32 @av1_get_palette_cache(%struct.macroblockd*, i32, i16*) #3

; Function Attrs: inlinehint nounwind
define internal i32 @av1_ceil_log2(i32 %n) #2 {
entry:
  %retval = alloca i32, align 4
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %p = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !2
  %0 = load i32, i32* %n.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 1, i32* %i, align 4, !tbaa !2
  %2 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store i32 2, i32* %p, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %3 = load i32, i32* %p, align 4, !tbaa !2
  %4 = load i32, i32* %n.addr, align 4, !tbaa !2
  %cmp1 = icmp slt i32 %3, %4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  %6 = load i32, i32* %p, align 4, !tbaa !2
  %shl = shl i32 %6, 1
  store i32 %shl, i32* %p, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = load i32, i32* %i, align 4, !tbaa !2
  store i32 %7, i32* %retval, align 4
  %8 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  br label %return

return:                                           ; preds = %while.end, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define internal void @merge_colors(i16* %colors, i16* %cached_colors, i32 %n_colors, i32 %n_cached_colors) #0 {
entry:
  %colors.addr = alloca i16*, align 4
  %cached_colors.addr = alloca i16*, align 4
  %n_colors.addr = alloca i32, align 4
  %n_cached_colors.addr = alloca i32, align 4
  %cache_idx = alloca i32, align 4
  %trans_idx = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %colors, i16** %colors.addr, align 4, !tbaa !6
  store i16* %cached_colors, i16** %cached_colors.addr, align 4, !tbaa !6
  store i32 %n_colors, i32* %n_colors.addr, align 4, !tbaa !2
  store i32 %n_cached_colors, i32* %n_cached_colors.addr, align 4, !tbaa !2
  %0 = load i32, i32* %n_cached_colors.addr, align 4, !tbaa !2
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %cache_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %cache_idx, align 4, !tbaa !2
  %2 = bitcast i32* %trans_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %n_cached_colors.addr, align 4, !tbaa !2
  store i32 %3, i32* %trans_idx, align 4, !tbaa !2
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %5 = load i32, i32* %i, align 4, !tbaa !2
  %6 = load i32, i32* %n_colors.addr, align 4, !tbaa !2
  %cmp1 = icmp slt i32 %5, %6
  br i1 %cmp1, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load i32, i32* %cache_idx, align 4, !tbaa !2
  %9 = load i32, i32* %n_cached_colors.addr, align 4, !tbaa !2
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.body
  %10 = load i32, i32* %trans_idx, align 4, !tbaa !2
  %11 = load i32, i32* %n_colors.addr, align 4, !tbaa !2
  %cmp3 = icmp sge i32 %10, %11
  br i1 %cmp3, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %12 = load i16*, i16** %cached_colors.addr, align 4, !tbaa !6
  %13 = load i32, i32* %cache_idx, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx, align 2, !tbaa !101
  %conv = zext i16 %14 to i32
  %15 = load i16*, i16** %colors.addr, align 4, !tbaa !6
  %16 = load i32, i32* %trans_idx, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i16, i16* %15, i32 %16
  %17 = load i16, i16* %arrayidx4, align 2, !tbaa !101
  %conv5 = zext i16 %17 to i32
  %cmp6 = icmp sle i32 %conv, %conv5
  br i1 %cmp6, label %if.then8, label %if.else

if.then8:                                         ; preds = %lor.lhs.false, %land.lhs.true
  %18 = load i16*, i16** %cached_colors.addr, align 4, !tbaa !6
  %19 = load i32, i32* %cache_idx, align 4, !tbaa !2
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %cache_idx, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i16, i16* %18, i32 %19
  %20 = load i16, i16* %arrayidx9, align 2, !tbaa !101
  %21 = load i16*, i16** %colors.addr, align 4, !tbaa !6
  %22 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %21, i32 %22
  store i16 %20, i16* %arrayidx10, align 2, !tbaa !101
  br label %if.end14

if.else:                                          ; preds = %lor.lhs.false, %for.body
  %23 = load i16*, i16** %colors.addr, align 4, !tbaa !6
  %24 = load i32, i32* %trans_idx, align 4, !tbaa !2
  %inc11 = add nsw i32 %24, 1
  store i32 %inc11, i32* %trans_idx, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i16, i16* %23, i32 %24
  %25 = load i16, i16* %arrayidx12, align 2, !tbaa !101
  %26 = load i16*, i16** %colors.addr, align 4, !tbaa !6
  %27 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i16, i16* %26, i32 %27
  store i16 %25, i16* %arrayidx13, align 2, !tbaa !101
  br label %if.end14

if.end14:                                         ; preds = %if.else, %if.then8
  br label %for.inc

for.inc:                                          ; preds = %if.end14
  %28 = load i32, i32* %i, align 4, !tbaa !2
  %inc15 = add nsw i32 %28, 1
  store i32 %inc15, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %29 = bitcast i32* %trans_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  %30 = bitcast i32* %cache_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  br label %return

return:                                           ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_filter_intra_allowed(%struct.AV1Common* %cm, %struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 7
  %1 = load i8, i8* %mode, align 1, !tbaa !50
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 5
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 0
  %3 = load i8, i8* %arrayidx, align 4, !tbaa !8
  %conv2 = zext i8 %3 to i32
  %cmp3 = icmp eq i32 %conv2, 0
  br i1 %cmp3, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 6
  %6 = load i8, i8* %sb_type, align 2, !tbaa !76
  %call = call i32 @av1_filter_intra_allowed_bsize(%struct.AV1Common* %4, i8 zeroext %6)
  %tobool = icmp ne i32 %call, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %entry
  %7 = phi i1 [ false, %land.lhs.true ], [ false, %entry ], [ %tobool, %land.rhs ]
  %land.ext = zext i1 %7 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_filter_intra_allowed_bsize(%struct.AV1Common* %cm, i8 zeroext %bs) #2 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %bs.addr = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i8 %bs, i8* %bs.addr, align 1, !tbaa !8
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %enable_filter_intra = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 15
  %1 = load i8, i8* %enable_filter_intra, align 4, !tbaa !150
  %tobool = icmp ne i8 %1, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load i8, i8* %bs.addr, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  %cmp = icmp eq i32 %conv, 255
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i8, i8* %bs.addr, align 1, !tbaa !8
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv2 = zext i8 %4 to i32
  %cmp3 = icmp sle i32 %conv2, 32
  br i1 %cmp3, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end
  %5 = load i8, i8* %bs.addr, align 1, !tbaa !8
  %idxprom5 = zext i8 %5 to i32
  %arrayidx6 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom5
  %6 = load i8, i8* %arrayidx6, align 1, !tbaa !8
  %conv7 = zext i8 %6 to i32
  %cmp8 = icmp sle i32 %conv7, 32
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end
  %7 = phi i1 [ false, %if.end ], [ %cmp8, %land.rhs ]
  %land.ext = zext i1 %7 to i32
  store i32 %land.ext, i32* %retval, align 4
  br label %return

return:                                           ; preds = %land.end, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: nounwind
define internal i32 @read_inter_segment_id(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %preskip, %struct.aom_reader* %r) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %preskip.addr = alloca i32, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %seg = alloca %struct.segmentation*, align 4
  %mi_params = alloca %struct.CommonModeInfoParams*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %mi_offset = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %x_mis = alloca i32, align 4
  %y_mis = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %segment_id = alloca i32, align 4
  %ctx = alloca i32, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %segp = alloca %struct.segmentation_probs*, align 4
  %pred_cdf = alloca i16*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32 %preskip, i32* %preskip.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 24
  store %struct.segmentation* %seg1, %struct.segmentation** %seg, align 4, !tbaa !6
  %2 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 22
  store %struct.CommonModeInfoParams* %mi_params2, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %4 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 6
  %6 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %6, i32 0
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %7, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %8 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row3 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %9, i32 0, i32 0
  %10 = load i32, i32* %mi_row3, align 16, !tbaa !71
  store i32 %10, i32* %mi_row, align 4, !tbaa !2
  %11 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %12, i32 0, i32 1
  %13 = load i32, i32* %mi_col4, align 4, !tbaa !72
  store i32 %13, i32* %mi_col, align 4, !tbaa !2
  %14 = bitcast i32* %mi_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load i32, i32* %mi_row, align 4, !tbaa !2
  %16 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %16, i32 0, i32 4
  %17 = load i32, i32* %mi_cols, align 4, !tbaa !106
  %mul = mul nsw i32 %15, %17
  %18 = load i32, i32* %mi_col, align 4, !tbaa !2
  %add = add nsw i32 %mul, %18
  store i32 %add, i32* %mi_offset, align 4, !tbaa !2
  %19 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %20, i32 0, i32 6
  %21 = load i8, i8* %sb_type, align 2, !tbaa !76
  %idxprom = zext i8 %21 to i32
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom
  %22 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv = zext i8 %22 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !2
  %23 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %sb_type6 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %24, i32 0, i32 6
  %25 = load i8, i8* %sb_type6, align 2, !tbaa !76
  %idxprom7 = zext i8 %25 to i32
  %arrayidx8 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom7
  %26 = load i8, i8* %arrayidx8, align 1, !tbaa !8
  %conv9 = zext i8 %26 to i32
  store i32 %conv9, i32* %bh, align 4, !tbaa !2
  %27 = bitcast i32* %x_mis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_cols10 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %28, i32 0, i32 4
  %29 = load i32, i32* %mi_cols10, align 4, !tbaa !106
  %30 = load i32, i32* %mi_col, align 4, !tbaa !2
  %sub = sub nsw i32 %29, %30
  %31 = load i32, i32* %bw, align 4, !tbaa !2
  %cmp = icmp slt i32 %sub, %31
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %32 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_cols12 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %32, i32 0, i32 4
  %33 = load i32, i32* %mi_cols12, align 4, !tbaa !106
  %34 = load i32, i32* %mi_col, align 4, !tbaa !2
  %sub13 = sub nsw i32 %33, %34
  br label %cond.end

cond.false:                                       ; preds = %entry
  %35 = load i32, i32* %bw, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub13, %cond.true ], [ %35, %cond.false ]
  store i32 %cond, i32* %x_mis, align 4, !tbaa !2
  %36 = bitcast i32* %y_mis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  %37 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %37, i32 0, i32 3
  %38 = load i32, i32* %mi_rows, align 4, !tbaa !107
  %39 = load i32, i32* %mi_row, align 4, !tbaa !2
  %sub14 = sub nsw i32 %38, %39
  %40 = load i32, i32* %bh, align 4, !tbaa !2
  %cmp15 = icmp slt i32 %sub14, %40
  br i1 %cmp15, label %cond.true17, label %cond.false20

cond.true17:                                      ; preds = %cond.end
  %41 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %mi_rows18 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %41, i32 0, i32 3
  %42 = load i32, i32* %mi_rows18, align 4, !tbaa !107
  %43 = load i32, i32* %mi_row, align 4, !tbaa !2
  %sub19 = sub nsw i32 %42, %43
  br label %cond.end21

cond.false20:                                     ; preds = %cond.end
  %44 = load i32, i32* %bh, align 4, !tbaa !2
  br label %cond.end21

cond.end21:                                       ; preds = %cond.false20, %cond.true17
  %cond22 = phi i32 [ %sub19, %cond.true17 ], [ %44, %cond.false20 ]
  store i32 %cond22, i32* %y_mis, align 4, !tbaa !2
  %45 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %enabled = getelementptr inbounds %struct.segmentation, %struct.segmentation* %45, i32 0, i32 0
  %46 = load i8, i8* %enabled, align 4, !tbaa !51
  %tobool = icmp ne i8 %46, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %cond.end21
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup63

if.end:                                           ; preds = %cond.end21
  %47 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %update_map = getelementptr inbounds %struct.segmentation, %struct.segmentation* %47, i32 0, i32 1
  %48 = load i8, i8* %update_map, align 1, !tbaa !151
  %tobool23 = icmp ne i8 %48, 0
  br i1 %tobool23, label %if.end25, label %if.then24

if.then24:                                        ; preds = %if.end
  %49 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %50 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %last_frame_seg_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %50, i32 0, i32 25
  %51 = load i8*, i8** %last_frame_seg_map, align 16, !tbaa !152
  %52 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %52, i32 0, i32 13
  %53 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !91
  %seg_map = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %53, i32 0, i32 6
  %54 = load i8*, i8** %seg_map, align 4, !tbaa !134
  %55 = load i32, i32* %mi_offset, align 4, !tbaa !2
  %56 = load i32, i32* %x_mis, align 4, !tbaa !2
  %57 = load i32, i32* %y_mis, align 4, !tbaa !2
  call void @copy_segment_id(%struct.CommonModeInfoParams* %49, i8* %51, i8* %54, i32 %55, i32 %56, i32 %57)
  %58 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %59 = load i32, i32* %mi_offset, align 4, !tbaa !2
  %60 = load i32, i32* %x_mis, align 4, !tbaa !2
  %61 = load i32, i32* %y_mis, align 4, !tbaa !2
  %call = call i32 @get_predicted_segment_id(%struct.AV1Common* %58, i32 %59, i32 %60, i32 %61)
  store i32 %call, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup63

if.end25:                                         ; preds = %if.end
  %62 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #6
  %63 = load i32, i32* %preskip.addr, align 4, !tbaa !2
  %tobool26 = icmp ne i32 %63, 0
  br i1 %tobool26, label %if.then27, label %if.else

if.then27:                                        ; preds = %if.end25
  %64 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %segid_preskip = getelementptr inbounds %struct.segmentation, %struct.segmentation* %64, i32 0, i32 7
  %65 = load i8, i8* %segid_preskip, align 4, !tbaa !77
  %tobool28 = icmp ne i8 %65, 0
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %if.then27
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end30:                                         ; preds = %if.then27
  br label %if.end38

if.else:                                          ; preds = %if.end25
  %66 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %66, i32 0, i32 14
  %67 = load i8, i8* %skip, align 4, !tbaa !17
  %tobool31 = icmp ne i8 %67, 0
  br i1 %tobool31, label %if.then32, label %if.end37

if.then32:                                        ; preds = %if.else
  %68 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %temporal_update = getelementptr inbounds %struct.segmentation, %struct.segmentation* %68, i32 0, i32 3
  %69 = load i8, i8* %temporal_update, align 1, !tbaa !153
  %tobool33 = icmp ne i8 %69, 0
  br i1 %tobool33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.then32
  %70 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %seg_id_predicted = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %70, i32 0, i32 26
  %bf.load = load i8, i8* %seg_id_predicted, align 4
  %bf.clear = and i8 %bf.load, -17
  store i8 %bf.clear, i8* %seg_id_predicted, align 4
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.then32
  %71 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %72 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %73 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call36 = call i32 @read_segment_id(%struct.AV1Common* %71, %struct.macroblockd* %72, %struct.aom_reader* %73, i32 1)
  store i32 %call36, i32* %segment_id, align 4, !tbaa !2
  %74 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %75 = load i32, i32* %mi_offset, align 4, !tbaa !2
  %76 = load i32, i32* %x_mis, align 4, !tbaa !2
  %77 = load i32, i32* %y_mis, align 4, !tbaa !2
  %78 = load i32, i32* %segment_id, align 4, !tbaa !2
  call void @set_segment_id(%struct.AV1Common* %74, i32 %75, i32 %76, i32 %77, i32 %78)
  %79 = load i32, i32* %segment_id, align 4, !tbaa !2
  store i32 %79, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end37:                                         ; preds = %if.else
  br label %if.end38

if.end38:                                         ; preds = %if.end37, %if.end30
  %80 = load %struct.segmentation*, %struct.segmentation** %seg, align 4, !tbaa !6
  %temporal_update39 = getelementptr inbounds %struct.segmentation, %struct.segmentation* %80, i32 0, i32 3
  %81 = load i8, i8* %temporal_update39, align 1, !tbaa !153
  %tobool40 = icmp ne i8 %81, 0
  br i1 %tobool40, label %if.then41, label %if.else60

if.then41:                                        ; preds = %if.end38
  %82 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #6
  %83 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call42 = call i32 @av1_get_pred_context_seg_id(%struct.macroblockd* %83)
  store i32 %call42, i32* %ctx, align 4, !tbaa !2
  %84 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #6
  %85 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %85, i32 0, i32 40
  %86 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %86, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %87 = bitcast %struct.segmentation_probs** %segp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #6
  %88 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %seg43 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %88, i32 0, i32 46
  store %struct.segmentation_probs* %seg43, %struct.segmentation_probs** %segp, align 4, !tbaa !6
  %89 = bitcast i16** %pred_cdf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #6
  %90 = load %struct.segmentation_probs*, %struct.segmentation_probs** %segp, align 4, !tbaa !6
  %pred_cdf44 = getelementptr inbounds %struct.segmentation_probs, %struct.segmentation_probs* %90, i32 0, i32 1
  %91 = load i32, i32* %ctx, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %pred_cdf44, i32 0, i32 %91
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx45, i32 0, i32 0
  store i16* %arraydecay, i16** %pred_cdf, align 4, !tbaa !6
  %92 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %93 = load i16*, i16** %pred_cdf, align 4, !tbaa !6
  %call46 = call i32 @aom_read_symbol_(%struct.aom_reader* %92, i16* %93, i32 2, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @__func__.read_inter_segment_id, i32 0, i32 0))
  %conv47 = trunc i32 %call46 to i8
  %94 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %seg_id_predicted48 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %94, i32 0, i32 26
  %bf.load49 = load i8, i8* %seg_id_predicted48, align 4
  %bf.value = and i8 %conv47, 1
  %bf.shl = shl i8 %bf.value, 4
  %bf.clear50 = and i8 %bf.load49, -17
  %bf.set = or i8 %bf.clear50, %bf.shl
  store i8 %bf.set, i8* %seg_id_predicted48, align 4
  %95 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %seg_id_predicted51 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %95, i32 0, i32 26
  %bf.load52 = load i8, i8* %seg_id_predicted51, align 4
  %bf.lshr = lshr i8 %bf.load52, 4
  %bf.clear53 = and i8 %bf.lshr, 1
  %tobool54 = icmp ne i8 %bf.clear53, 0
  br i1 %tobool54, label %if.then55, label %if.else57

if.then55:                                        ; preds = %if.then41
  %96 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %97 = load i32, i32* %mi_offset, align 4, !tbaa !2
  %98 = load i32, i32* %x_mis, align 4, !tbaa !2
  %99 = load i32, i32* %y_mis, align 4, !tbaa !2
  %call56 = call i32 @get_predicted_segment_id(%struct.AV1Common* %96, i32 %97, i32 %98, i32 %99)
  store i32 %call56, i32* %segment_id, align 4, !tbaa !2
  br label %if.end59

if.else57:                                        ; preds = %if.then41
  %100 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %101 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %102 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call58 = call i32 @read_segment_id(%struct.AV1Common* %100, %struct.macroblockd* %101, %struct.aom_reader* %102, i32 0)
  store i32 %call58, i32* %segment_id, align 4, !tbaa !2
  br label %if.end59

if.end59:                                         ; preds = %if.else57, %if.then55
  %103 = bitcast i16** %pred_cdf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast %struct.segmentation_probs** %segp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  %105 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #6
  %106 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #6
  br label %if.end62

if.else60:                                        ; preds = %if.end38
  %107 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %108 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %109 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call61 = call i32 @read_segment_id(%struct.AV1Common* %107, %struct.macroblockd* %108, %struct.aom_reader* %109, i32 0)
  store i32 %call61, i32* %segment_id, align 4, !tbaa !2
  br label %if.end62

if.end62:                                         ; preds = %if.else60, %if.end59
  %110 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %111 = load i32, i32* %mi_offset, align 4, !tbaa !2
  %112 = load i32, i32* %x_mis, align 4, !tbaa !2
  %113 = load i32, i32* %y_mis, align 4, !tbaa !2
  %114 = load i32, i32* %segment_id, align 4, !tbaa !2
  call void @set_segment_id(%struct.AV1Common* %110, i32 %111, i32 %112, i32 %113, i32 %114)
  %115 = load i32, i32* %segment_id, align 4, !tbaa !2
  store i32 %115, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end62, %if.end35, %if.then29
  %116 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #6
  br label %cleanup63

cleanup63:                                        ; preds = %cleanup, %if.then24, %if.then
  %117 = bitcast i32* %y_mis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #6
  %118 = bitcast i32* %x_mis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #6
  %119 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #6
  %120 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #6
  %121 = bitcast i32* %mi_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #6
  %122 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #6
  %123 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #6
  %124 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #6
  %125 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #6
  %126 = bitcast %struct.segmentation** %seg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #6
  %127 = load i32, i32* %retval, align 4
  ret i32 %127
}

; Function Attrs: nounwind
define internal i32 @read_skip_mode(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %segment_id, %struct.aom_reader* %r) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %segment_id.addr = alloca i32, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %ctx = alloca i32, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %skip_mode = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 0
  %skip_mode_info = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 5
  %skip_mode_flag = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %skip_mode_info, i32 0, i32 1
  %1 = load i32, i32* %skip_mode_flag, align 4, !tbaa !154
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 24
  %3 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call = call i32 @segfeature_active(%struct.segmentation* %seg, i32 %3, i8 zeroext 6)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 6
  %5 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %5, i32 0
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %6, i32 0, i32 6
  %7 = load i8, i8* %sb_type, align 2, !tbaa !76
  %call4 = call i32 @is_comp_ref_allowed(i8 zeroext %7)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %if.end3
  store i32 0, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end3
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 24
  %9 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call9 = call i32 @segfeature_active(%struct.segmentation* %seg8, i32 %9, i8 zeroext 5)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.then14, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end7
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg11 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 24
  %11 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call12 = call i32 @segfeature_active(%struct.segmentation* %seg11, i32 %11, i8 zeroext 7)
  %tobool13 = icmp ne i32 %call12, 0
  br i1 %tobool13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %lor.lhs.false, %if.end7
  store i32 0, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %lor.lhs.false
  %12 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call16 = call i32 @av1_get_skip_mode_context(%struct.macroblockd* %13)
  store i32 %call16, i32* %ctx, align 4, !tbaa !2
  %14 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %15, i32 0, i32 40
  %16 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %16, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %17 = bitcast i32* %skip_mode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %19 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %skip_mode_cdfs = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %19, i32 0, i32 40
  %20 = load i32, i32* %ctx, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %skip_mode_cdfs, i32 0, i32 %20
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx17, i32 0, i32 0
  %call18 = call i32 @aom_read_symbol_(%struct.aom_reader* %18, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @__func__.read_skip_mode, i32 0, i32 0))
  store i32 %call18, i32* %skip_mode, align 4, !tbaa !2
  %21 = load i32, i32* %skip_mode, align 4, !tbaa !2
  store i32 %21, i32* %retval, align 4
  %22 = bitcast i32* %skip_mode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  %24 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  br label %return

return:                                           ; preds = %if.end15, %if.then14, %if.then6, %if.then2, %if.then
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

; Function Attrs: nounwind
define internal i32 @read_is_inter_block(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %segment_id, %struct.aom_reader* %r) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %segment_id.addr = alloca i32, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %frame = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ctx = alloca i32, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %is_inter = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 24
  %1 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call = call i32 @segfeature_active(%struct.segmentation* %seg, i32 %1, i8 zeroext 5)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 24
  %4 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call2 = call i32 @get_segdata(%struct.segmentation* %seg1, i32 %4, i8 zeroext 5)
  store i32 %call2, i32* %frame, align 4, !tbaa !2
  %5 = load i32, i32* %frame, align 4, !tbaa !2
  %cmp = icmp slt i32 %5, 1
  br i1 %cmp, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %6 = load i32, i32* %frame, align 4, !tbaa !2
  %cmp4 = icmp ne i32 %6, 0
  %conv = zext i1 %cmp4 to i32
  store i32 %conv, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then3
  %7 = bitcast i32* %frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %return

if.end5:                                          ; preds = %entry
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 24
  %9 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call7 = call i32 @segfeature_active(%struct.segmentation* %seg6, i32 %9, i8 zeroext 7)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end5
  store i32 1, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %if.end5
  %10 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call11 = call i32 @av1_get_intra_inter_context(%struct.macroblockd* %11)
  store i32 %call11, i32* %ctx, align 4, !tbaa !2
  %12 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %13, i32 0, i32 40
  %14 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %14, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %15 = bitcast i32* %is_inter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %17 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %intra_inter_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %17, i32 0, i32 42
  %18 = load i32, i32* %ctx, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [4 x [3 x i16]], [4 x [3 x i16]]* %intra_inter_cdf, i32 0, i32 %18
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx, i32 0, i32 0
  %call12 = call i32 @aom_read_symbol_(%struct.aom_reader* %16, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.read_is_inter_block, i32 0, i32 0))
  store i32 %call12, i32* %is_inter, align 4, !tbaa !2
  %19 = load i32, i32* %is_inter, align 4, !tbaa !2
  store i32 %19, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %20 = bitcast i32* %is_inter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  br label %return

return:                                           ; preds = %if.end10, %if.then9, %cleanup
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: nounwind
define internal void @read_inter_block_mode_info(%struct.AV1Decoder* %pbi, %struct.macroblockd* %xd, %struct.MB_MODE_INFO* %mbmi, %struct.aom_reader* %r) #0 {
entry:
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %features = alloca %struct.FeatureFlags*, align 4
  %bsize = alloca i8, align 1
  %allow_hp = alloca i32, align 4
  %nearestmv = alloca [2 x %union.int_mv], align 4
  %nearmv = alloca [2 x %union.int_mv], align 4
  %ref_mvs = alloca [29 x [2 x %union.int_mv]], align 16
  %inter_mode_ctx = alloca [29 x i16], align 16
  %pts = alloca [16 x i32], align 16
  %pts_inref = alloca [16 x i32], align 16
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %is_compound = alloca i32, align 4
  %ref_frame6 = alloca i8, align 1
  %mode_ctx = alloca i32, align 4
  %ref_mv_idx105 = alloca i32, align 4
  %ref_mv = alloca [2 x %union.int_mv], align 4
  %ref_mv_idx183 = alloca i32, align 4
  %mv_corrupted_flag = alloca i32, align 4
  %bsize_group = alloca i32, align 4
  %interintra = alloca i32, align 4
  %interintra_mode = alloca i8, align 1
  %ref = alloca i32, align 4
  %frame = alloca i8, align 1
  %masked_compound_used = alloca i32, align 4
  %ctx_comp_group_idx = alloca i32, align 4
  %comp_index_ctx = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !6
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %1, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !6
  %2 = bitcast %struct.FeatureFlags** %features to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %features1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 21
  store %struct.FeatureFlags* %features1, %struct.FeatureFlags** %features, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %4, i32 0, i32 6
  %5 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %5, i8* %bsize, align 1, !tbaa !8
  %6 = bitcast i32* %allow_hp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %allow_high_precision_mv = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %7, i32 0, i32 1
  %8 = load i8, i8* %allow_high_precision_mv, align 1, !tbaa !155, !range !46
  %tobool = trunc i8 %8 to i1
  %conv = zext i1 %tobool to i32
  store i32 %conv, i32* %allow_hp, align 4, !tbaa !2
  %9 = bitcast [2 x %union.int_mv]* %nearestmv to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %9) #6
  %10 = bitcast [2 x %union.int_mv]* %nearmv to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %10) #6
  %11 = bitcast [29 x [2 x %union.int_mv]]* %ref_mvs to i8*
  call void @llvm.lifetime.start.p0i8(i64 232, i8* %11) #6
  %12 = bitcast [29 x [2 x %union.int_mv]]* %ref_mvs to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %12, i8 0, i32 232, i1 false)
  %13 = bitcast [29 x i16]* %inter_mode_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 58, i8* %13) #6
  %14 = bitcast [16 x i32]* %pts to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %14) #6
  %15 = bitcast [16 x i32]* %pts_inref to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %15) #6
  %16 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %17, i32 0, i32 40
  %18 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %18, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %19 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %uv_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %19, i32 0, i32 8
  store i8 0, i8* %uv_mode, align 4, !tbaa !86
  %20 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %20, i32 0, i32 5
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 0
  store i8 0, i8* %arrayidx, align 4, !tbaa !8
  %21 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %palette_mode_info2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %21, i32 0, i32 5
  %palette_size3 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info2, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size3, i32 0, i32 1
  store i8 0, i8* %arrayidx4, align 1, !tbaa !8
  %22 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  call void @av1_collect_neighbors_ref_counts(%struct.macroblockd* %22)
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %24 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %25 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %26 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %segment_id = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %26, i32 0, i32 26
  %bf.load = load i8, i8* %segment_id, align 4
  %bf.lshr = lshr i8 %bf.load, 1
  %bf.clear = and i8 %bf.lshr, 7
  %conv5 = zext i8 %bf.clear to i32
  %27 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %27, i32 0, i32 12
  %arraydecay = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  call void @read_ref_frames(%struct.AV1Common* %23, %struct.macroblockd* %24, %struct.aom_reader* %25, i32 %conv5, i8* %arraydecay)
  %28 = bitcast i32* %is_compound to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %29 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call = call i32 @has_second_ref(%struct.MB_MODE_INFO* %29)
  store i32 %call, i32* %is_compound, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_frame6) #6
  %30 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame7 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %30, i32 0, i32 12
  %arraydecay8 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame7, i32 0, i32 0
  %call9 = call signext i8 @av1_ref_frame_type(i8* %arraydecay8)
  store i8 %call9, i8* %ref_frame6, align 1, !tbaa !8
  %31 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %32 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %33 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %34 = load i8, i8* %ref_frame6, align 1, !tbaa !8
  %35 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_count = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %35, i32 0, i32 35
  %arraydecay10 = getelementptr inbounds [29 x i8], [29 x i8]* %ref_mv_count, i32 0, i32 0
  %36 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %36, i32 0, i32 36
  %arraydecay11 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack, i32 0, i32 0
  %37 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %weight = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %37, i32 0, i32 37
  %arraydecay12 = getelementptr inbounds [29 x [8 x i16]], [29 x [8 x i16]]* %weight, i32 0, i32 0
  %arraydecay13 = getelementptr inbounds [29 x [2 x %union.int_mv]], [29 x [2 x %union.int_mv]]* %ref_mvs, i32 0, i32 0
  %arraydecay14 = getelementptr inbounds [29 x i16], [29 x i16]* %inter_mode_ctx, i32 0, i32 0
  call void @av1_find_mv_refs(%struct.AV1Common* %31, %struct.macroblockd* %32, %struct.MB_MODE_INFO* %33, i8 signext %34, i8* %arraydecay10, [8 x %struct.candidate_mv]* %arraydecay11, [8 x i16]* %arraydecay12, [2 x %union.int_mv]* %arraydecay13, %union.int_mv* null, i16* %arraydecay14)
  %38 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %38, i32 0, i32 27
  %bf.load15 = load i8, i8* %ref_mv_idx, align 1
  %bf.clear16 = and i8 %bf.load15, -4
  store i8 %bf.clear16, i8* %ref_mv_idx, align 1
  %39 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %skip_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %39, i32 0, i32 26
  %bf.load17 = load i8, i8* %skip_mode, align 4
  %bf.lshr18 = lshr i8 %bf.load17, 5
  %bf.clear19 = and i8 %bf.lshr18, 1
  %tobool20 = icmp ne i8 %bf.clear19, 0
  br i1 %tobool20, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %40 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %40, i32 0, i32 7
  store i8 17, i8* %mode, align 1, !tbaa !50
  br label %if.end68

if.else:                                          ; preds = %entry
  %41 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %41, i32 0, i32 24
  %42 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %segment_id21 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %42, i32 0, i32 26
  %bf.load22 = load i8, i8* %segment_id21, align 4
  %bf.lshr23 = lshr i8 %bf.load22, 1
  %bf.clear24 = and i8 %bf.lshr23, 7
  %conv25 = zext i8 %bf.clear24 to i32
  %call26 = call i32 @segfeature_active(%struct.segmentation* %seg, i32 %conv25, i8 zeroext 6)
  %tobool27 = icmp ne i32 %call26, 0
  br i1 %tobool27, label %if.then36, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %43 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %seg28 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %43, i32 0, i32 24
  %44 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %segment_id29 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %44, i32 0, i32 26
  %bf.load30 = load i8, i8* %segment_id29, align 4
  %bf.lshr31 = lshr i8 %bf.load30, 1
  %bf.clear32 = and i8 %bf.lshr31, 7
  %conv33 = zext i8 %bf.clear32 to i32
  %call34 = call i32 @segfeature_active(%struct.segmentation* %seg28, i32 %conv33, i8 zeroext 7)
  %tobool35 = icmp ne i32 %call34, 0
  br i1 %tobool35, label %if.then36, label %if.else38

if.then36:                                        ; preds = %lor.lhs.false, %if.else
  %45 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode37 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %45, i32 0, i32 7
  store i8 15, i8* %mode37, align 1, !tbaa !50
  br label %if.end67

if.else38:                                        ; preds = %lor.lhs.false
  %46 = bitcast i32* %mode_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #6
  %arraydecay39 = getelementptr inbounds [29 x i16], [29 x i16]* %inter_mode_ctx, i32 0, i32 0
  %47 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame40 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %47, i32 0, i32 12
  %arraydecay41 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame40, i32 0, i32 0
  %call42 = call signext i16 @av1_mode_context_analyzer(i16* %arraydecay39, i8* %arraydecay41)
  %conv43 = sext i16 %call42 to i32
  store i32 %conv43, i32* %mode_ctx, align 4, !tbaa !2
  %48 = load i32, i32* %is_compound, align 4, !tbaa !2
  %tobool44 = icmp ne i32 %48, 0
  br i1 %tobool44, label %if.then45, label %if.else49

if.then45:                                        ; preds = %if.else38
  %49 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %50 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %51 = load i32, i32* %mode_ctx, align 4, !tbaa !2
  %conv46 = trunc i32 %51 to i16
  %call47 = call zeroext i8 @read_inter_compound_mode(%struct.macroblockd* %49, %struct.aom_reader* %50, i16 signext %conv46)
  %52 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode48 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %52, i32 0, i32 7
  store i8 %call47, i8* %mode48, align 1, !tbaa !50
  br label %if.end

if.else49:                                        ; preds = %if.else38
  %53 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %54 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %55 = load i32, i32* %mode_ctx, align 4, !tbaa !2
  %conv50 = trunc i32 %55 to i16
  %call51 = call zeroext i8 @read_inter_mode(%struct.frame_contexts* %53, %struct.aom_reader* %54, i16 signext %conv50)
  %56 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode52 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %56, i32 0, i32 7
  store i8 %call51, i8* %mode52, align 1, !tbaa !50
  br label %if.end

if.end:                                           ; preds = %if.else49, %if.then45
  %57 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode53 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %57, i32 0, i32 7
  %58 = load i8, i8* %mode53, align 1, !tbaa !50
  %conv54 = zext i8 %58 to i32
  %cmp = icmp eq i32 %conv54, 16
  br i1 %cmp, label %if.then65, label %lor.lhs.false56

lor.lhs.false56:                                  ; preds = %if.end
  %59 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode57 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %59, i32 0, i32 7
  %60 = load i8, i8* %mode57, align 1, !tbaa !50
  %conv58 = zext i8 %60 to i32
  %cmp59 = icmp eq i32 %conv58, 24
  br i1 %cmp59, label %if.then65, label %lor.lhs.false61

lor.lhs.false61:                                  ; preds = %lor.lhs.false56
  %61 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode62 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %61, i32 0, i32 7
  %62 = load i8, i8* %mode62, align 1, !tbaa !50
  %call63 = call i32 @have_nearmv_in_inter_mode(i8 zeroext %62)
  %tobool64 = icmp ne i32 %call63, 0
  br i1 %tobool64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %lor.lhs.false61, %lor.lhs.false56, %if.end
  %63 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %64 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %65 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %66 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_drl_idx(%struct.frame_contexts* %63, %struct.macroblockd* %64, %struct.MB_MODE_INFO* %65, %struct.aom_reader* %66)
  br label %if.end66

if.end66:                                         ; preds = %if.then65, %lor.lhs.false61
  %67 = bitcast i32* %mode_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  br label %if.end67

if.end67:                                         ; preds = %if.end66, %if.then36
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %if.then
  %68 = load i32, i32* %is_compound, align 4, !tbaa !2
  %69 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode69 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %69, i32 0, i32 7
  %70 = load i8, i8* %mode69, align 1, !tbaa !50
  %call70 = call i32 @is_inter_compound_mode(i8 zeroext %70)
  %cmp71 = icmp ne i32 %68, %call70
  br i1 %cmp71, label %if.then73, label %if.end82

if.then73:                                        ; preds = %if.end68
  %71 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %error_info = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %71, i32 0, i32 46
  %72 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info, align 16, !tbaa !125
  %73 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode74 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %73, i32 0, i32 7
  %74 = load i8, i8* %mode74, align 1, !tbaa !50
  %conv75 = zext i8 %74 to i32
  %75 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame76 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %75, i32 0, i32 12
  %arrayidx77 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame76, i32 0, i32 0
  %76 = load i8, i8* %arrayidx77, align 4, !tbaa !8
  %conv78 = sext i8 %76 to i32
  %77 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame79 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %77, i32 0, i32 12
  %arrayidx80 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame79, i32 0, i32 1
  %78 = load i8, i8* %arrayidx80, align 1, !tbaa !8
  %conv81 = sext i8 %78 to i32
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %72, i32 7, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.6, i32 0, i32 0), i32 %conv75, i32 %conv78, i32 %conv81)
  br label %if.end82

if.end82:                                         ; preds = %if.then73, %if.end68
  %79 = load i32, i32* %is_compound, align 4, !tbaa !2
  %tobool83 = icmp ne i32 %79, 0
  br i1 %tobool83, label %if.end97, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end82
  %80 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode84 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %80, i32 0, i32 7
  %81 = load i8, i8* %mode84, align 1, !tbaa !50
  %conv85 = zext i8 %81 to i32
  %cmp86 = icmp ne i32 %conv85, 15
  br i1 %cmp86, label %if.then88, label %if.end97

if.then88:                                        ; preds = %land.lhs.true
  %82 = load i32, i32* %allow_hp, align 4, !tbaa !2
  %83 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame89 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %83, i32 0, i32 12
  %arrayidx90 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame89, i32 0, i32 0
  %84 = load i8, i8* %arrayidx90, align 4, !tbaa !8
  %idxprom = sext i8 %84 to i32
  %arrayidx91 = getelementptr inbounds [29 x [2 x %union.int_mv]], [29 x [2 x %union.int_mv]]* %ref_mvs, i32 0, i32 %idxprom
  %arraydecay92 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %arrayidx91, i32 0, i32 0
  %arrayidx93 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearestmv, i32 0, i32 0
  %arrayidx94 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearmv, i32 0, i32 0
  %85 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %cur_frame_force_integer_mv = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %85, i32 0, i32 2
  %86 = load i8, i8* %cur_frame_force_integer_mv, align 2, !tbaa !156, !range !46
  %tobool95 = trunc i8 %86 to i1
  %conv96 = zext i1 %tobool95 to i32
  call void @av1_find_best_ref_mvs(i32 %82, %union.int_mv* %arraydecay92, %union.int_mv* %arrayidx93, %union.int_mv* %arrayidx94, i32 %conv96)
  br label %if.end97

if.end97:                                         ; preds = %if.then88, %land.lhs.true, %if.end82
  %87 = load i32, i32* %is_compound, align 4, !tbaa !2
  %tobool98 = icmp ne i32 %87, 0
  br i1 %tobool98, label %land.lhs.true99, label %if.else151

land.lhs.true99:                                  ; preds = %if.end97
  %88 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode100 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %88, i32 0, i32 7
  %89 = load i8, i8* %mode100, align 1, !tbaa !50
  %conv101 = zext i8 %89 to i32
  %cmp102 = icmp ne i32 %conv101, 23
  br i1 %cmp102, label %if.then104, label %if.else151

if.then104:                                       ; preds = %land.lhs.true99
  %90 = bitcast i32* %ref_mv_idx105 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  %91 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx106 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %91, i32 0, i32 27
  %bf.load107 = load i8, i8* %ref_mv_idx106, align 1
  %bf.clear108 = and i8 %bf.load107, 3
  %conv109 = zext i8 %bf.clear108 to i32
  %add = add nsw i32 %conv109, 1
  store i32 %add, i32* %ref_mv_idx105, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearestmv, i32 0, i32 0
  %92 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack111 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %92, i32 0, i32 36
  %93 = load i8, i8* %ref_frame6, align 1, !tbaa !8
  %idxprom112 = sext i8 %93 to i32
  %arrayidx113 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack111, i32 0, i32 %idxprom112
  %arrayidx114 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %arrayidx113, i32 0, i32 0
  %this_mv = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx114, i32 0, i32 0
  %94 = bitcast %union.int_mv* %arrayidx110 to i8*
  %95 = bitcast %union.int_mv* %this_mv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %94, i8* align 4 %95, i32 4, i1 false), !tbaa.struct !124
  %arrayidx115 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearestmv, i32 0, i32 1
  %96 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack116 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %96, i32 0, i32 36
  %97 = load i8, i8* %ref_frame6, align 1, !tbaa !8
  %idxprom117 = sext i8 %97 to i32
  %arrayidx118 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack116, i32 0, i32 %idxprom117
  %arrayidx119 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %arrayidx118, i32 0, i32 0
  %comp_mv = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx119, i32 0, i32 1
  %98 = bitcast %union.int_mv* %arrayidx115 to i8*
  %99 = bitcast %union.int_mv* %comp_mv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %98, i8* align 4 %99, i32 4, i1 false), !tbaa.struct !124
  %arrayidx120 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearmv, i32 0, i32 0
  %100 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack121 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %100, i32 0, i32 36
  %101 = load i8, i8* %ref_frame6, align 1, !tbaa !8
  %idxprom122 = sext i8 %101 to i32
  %arrayidx123 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack121, i32 0, i32 %idxprom122
  %102 = load i32, i32* %ref_mv_idx105, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %arrayidx123, i32 0, i32 %102
  %this_mv125 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx124, i32 0, i32 0
  %103 = bitcast %union.int_mv* %arrayidx120 to i8*
  %104 = bitcast %union.int_mv* %this_mv125 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %103, i8* align 4 %104, i32 4, i1 false), !tbaa.struct !124
  %arrayidx126 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearmv, i32 0, i32 1
  %105 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack127 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %105, i32 0, i32 36
  %106 = load i8, i8* %ref_frame6, align 1, !tbaa !8
  %idxprom128 = sext i8 %106 to i32
  %arrayidx129 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack127, i32 0, i32 %idxprom128
  %107 = load i32, i32* %ref_mv_idx105, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %arrayidx129, i32 0, i32 %107
  %comp_mv131 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx130, i32 0, i32 1
  %108 = bitcast %union.int_mv* %arrayidx126 to i8*
  %109 = bitcast %union.int_mv* %comp_mv131 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %108, i8* align 4 %109, i32 4, i1 false), !tbaa.struct !124
  %arrayidx132 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearestmv, i32 0, i32 0
  %as_mv = bitcast %union.int_mv* %arrayidx132 to %struct.mv*
  %110 = load i32, i32* %allow_hp, align 4, !tbaa !2
  %111 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %cur_frame_force_integer_mv133 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %111, i32 0, i32 2
  %112 = load i8, i8* %cur_frame_force_integer_mv133, align 2, !tbaa !156, !range !46
  %tobool134 = trunc i8 %112 to i1
  %conv135 = zext i1 %tobool134 to i32
  call void @lower_mv_precision(%struct.mv* %as_mv, i32 %110, i32 %conv135)
  %arrayidx136 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearestmv, i32 0, i32 1
  %as_mv137 = bitcast %union.int_mv* %arrayidx136 to %struct.mv*
  %113 = load i32, i32* %allow_hp, align 4, !tbaa !2
  %114 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %cur_frame_force_integer_mv138 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %114, i32 0, i32 2
  %115 = load i8, i8* %cur_frame_force_integer_mv138, align 2, !tbaa !156, !range !46
  %tobool139 = trunc i8 %115 to i1
  %conv140 = zext i1 %tobool139 to i32
  call void @lower_mv_precision(%struct.mv* %as_mv137, i32 %113, i32 %conv140)
  %arrayidx141 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearmv, i32 0, i32 0
  %as_mv142 = bitcast %union.int_mv* %arrayidx141 to %struct.mv*
  %116 = load i32, i32* %allow_hp, align 4, !tbaa !2
  %117 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %cur_frame_force_integer_mv143 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %117, i32 0, i32 2
  %118 = load i8, i8* %cur_frame_force_integer_mv143, align 2, !tbaa !156, !range !46
  %tobool144 = trunc i8 %118 to i1
  %conv145 = zext i1 %tobool144 to i32
  call void @lower_mv_precision(%struct.mv* %as_mv142, i32 %116, i32 %conv145)
  %arrayidx146 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearmv, i32 0, i32 1
  %as_mv147 = bitcast %union.int_mv* %arrayidx146 to %struct.mv*
  %119 = load i32, i32* %allow_hp, align 4, !tbaa !2
  %120 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %cur_frame_force_integer_mv148 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %120, i32 0, i32 2
  %121 = load i8, i8* %cur_frame_force_integer_mv148, align 2, !tbaa !156, !range !46
  %tobool149 = trunc i8 %121 to i1
  %conv150 = zext i1 %tobool149 to i32
  call void @lower_mv_precision(%struct.mv* %as_mv147, i32 %119, i32 %conv150)
  %122 = bitcast i32* %ref_mv_idx105 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #6
  br label %if.end178

if.else151:                                       ; preds = %land.lhs.true99, %if.end97
  %123 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx152 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %123, i32 0, i32 27
  %bf.load153 = load i8, i8* %ref_mv_idx152, align 1
  %bf.clear154 = and i8 %bf.load153, 3
  %conv155 = zext i8 %bf.clear154 to i32
  %cmp156 = icmp sgt i32 %conv155, 0
  br i1 %cmp156, label %land.lhs.true158, label %if.end177

land.lhs.true158:                                 ; preds = %if.else151
  %124 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode159 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %124, i32 0, i32 7
  %125 = load i8, i8* %mode159, align 1, !tbaa !50
  %conv160 = zext i8 %125 to i32
  %cmp161 = icmp eq i32 %conv160, 14
  br i1 %cmp161, label %if.then163, label %if.end177

if.then163:                                       ; preds = %land.lhs.true158
  %arrayidx164 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearmv, i32 0, i32 0
  %126 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack165 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %126, i32 0, i32 36
  %127 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame166 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %127, i32 0, i32 12
  %arrayidx167 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame166, i32 0, i32 0
  %128 = load i8, i8* %arrayidx167, align 4, !tbaa !8
  %idxprom168 = sext i8 %128 to i32
  %arrayidx169 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack165, i32 0, i32 %idxprom168
  %129 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx170 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %129, i32 0, i32 27
  %bf.load171 = load i8, i8* %ref_mv_idx170, align 1
  %bf.clear172 = and i8 %bf.load171, 3
  %conv173 = zext i8 %bf.clear172 to i32
  %add174 = add nsw i32 1, %conv173
  %arrayidx175 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %arrayidx169, i32 0, i32 %add174
  %this_mv176 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx175, i32 0, i32 0
  %130 = bitcast %union.int_mv* %arrayidx164 to i8*
  %131 = bitcast %union.int_mv* %this_mv176 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %130, i8* align 4 %131, i32 4, i1 false), !tbaa.struct !124
  br label %if.end177

if.end177:                                        ; preds = %if.then163, %land.lhs.true158, %if.else151
  br label %if.end178

if.end178:                                        ; preds = %if.end177, %if.then104
  %132 = bitcast [2 x %union.int_mv]* %ref_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %132) #6
  %arrayinit.begin = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %ref_mv, i32 0, i32 0
  %arrayidx179 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearestmv, i32 0, i32 0
  %133 = bitcast %union.int_mv* %arrayinit.begin to i8*
  %134 = bitcast %union.int_mv* %arrayidx179 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %133, i8* align 4 %134, i32 4, i1 false), !tbaa.struct !124
  %arrayinit.element = getelementptr inbounds %union.int_mv, %union.int_mv* %arrayinit.begin, i32 1
  %arrayidx180 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearestmv, i32 0, i32 1
  %135 = bitcast %union.int_mv* %arrayinit.element to i8*
  %136 = bitcast %union.int_mv* %arrayidx180 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %135, i8* align 4 %136, i32 4, i1 false), !tbaa.struct !124
  %137 = load i32, i32* %is_compound, align 4, !tbaa !2
  %tobool181 = icmp ne i32 %137, 0
  br i1 %tobool181, label %if.then182, label %if.else230

if.then182:                                       ; preds = %if.end178
  %138 = bitcast i32* %ref_mv_idx183 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #6
  %139 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx184 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %139, i32 0, i32 27
  %bf.load185 = load i8, i8* %ref_mv_idx184, align 1
  %bf.clear186 = and i8 %bf.load185, 3
  %conv187 = zext i8 %bf.clear186 to i32
  store i32 %conv187, i32* %ref_mv_idx183, align 4, !tbaa !2
  %140 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode188 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %140, i32 0, i32 7
  %141 = load i8, i8* %mode188, align 1, !tbaa !50
  %conv189 = zext i8 %141 to i32
  %cmp190 = icmp eq i32 %conv189, 21
  br i1 %cmp190, label %if.then197, label %lor.lhs.false192

lor.lhs.false192:                                 ; preds = %if.then182
  %142 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode193 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %142, i32 0, i32 7
  %143 = load i8, i8* %mode193, align 1, !tbaa !50
  %conv194 = zext i8 %143 to i32
  %cmp195 = icmp eq i32 %conv194, 22
  br i1 %cmp195, label %if.then197, label %if.end203

if.then197:                                       ; preds = %lor.lhs.false192, %if.then182
  %144 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx198 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %144, i32 0, i32 27
  %bf.load199 = load i8, i8* %ref_mv_idx198, align 1
  %bf.clear200 = and i8 %bf.load199, 3
  %conv201 = zext i8 %bf.clear200 to i32
  %add202 = add nsw i32 1, %conv201
  store i32 %add202, i32* %ref_mv_idx183, align 4, !tbaa !2
  br label %if.end203

if.end203:                                        ; preds = %if.then197, %lor.lhs.false192
  %145 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode204 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %145, i32 0, i32 7
  %146 = load i8, i8* %mode204, align 1, !tbaa !50
  %call205 = call zeroext i8 @compound_ref0_mode(i8 zeroext %146)
  %conv206 = zext i8 %call205 to i32
  %cmp207 = icmp eq i32 %conv206, 16
  br i1 %cmp207, label %if.then209, label %if.end216

if.then209:                                       ; preds = %if.end203
  %arrayidx210 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %ref_mv, i32 0, i32 0
  %147 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack211 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %147, i32 0, i32 36
  %148 = load i8, i8* %ref_frame6, align 1, !tbaa !8
  %idxprom212 = sext i8 %148 to i32
  %arrayidx213 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack211, i32 0, i32 %idxprom212
  %149 = load i32, i32* %ref_mv_idx183, align 4, !tbaa !2
  %arrayidx214 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %arrayidx213, i32 0, i32 %149
  %this_mv215 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx214, i32 0, i32 0
  %150 = bitcast %union.int_mv* %arrayidx210 to i8*
  %151 = bitcast %union.int_mv* %this_mv215 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %150, i8* align 4 %151, i32 4, i1 false), !tbaa.struct !124
  br label %if.end216

if.end216:                                        ; preds = %if.then209, %if.end203
  %152 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode217 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %152, i32 0, i32 7
  %153 = load i8, i8* %mode217, align 1, !tbaa !50
  %call218 = call zeroext i8 @compound_ref1_mode(i8 zeroext %153)
  %conv219 = zext i8 %call218 to i32
  %cmp220 = icmp eq i32 %conv219, 16
  br i1 %cmp220, label %if.then222, label %if.end229

if.then222:                                       ; preds = %if.end216
  %arrayidx223 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %ref_mv, i32 0, i32 1
  %154 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack224 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %154, i32 0, i32 36
  %155 = load i8, i8* %ref_frame6, align 1, !tbaa !8
  %idxprom225 = sext i8 %155 to i32
  %arrayidx226 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack224, i32 0, i32 %idxprom225
  %156 = load i32, i32* %ref_mv_idx183, align 4, !tbaa !2
  %arrayidx227 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %arrayidx226, i32 0, i32 %156
  %comp_mv228 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx227, i32 0, i32 1
  %157 = bitcast %union.int_mv* %arrayidx223 to i8*
  %158 = bitcast %union.int_mv* %comp_mv228 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %157, i8* align 4 %158, i32 4, i1 false), !tbaa.struct !124
  br label %if.end229

if.end229:                                        ; preds = %if.then222, %if.end216
  %159 = bitcast i32* %ref_mv_idx183 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #6
  br label %if.end255

if.else230:                                       ; preds = %if.end178
  %160 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode231 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %160, i32 0, i32 7
  %161 = load i8, i8* %mode231, align 1, !tbaa !50
  %conv232 = zext i8 %161 to i32
  %cmp233 = icmp eq i32 %conv232, 16
  br i1 %cmp233, label %if.then235, label %if.end254

if.then235:                                       ; preds = %if.else230
  %162 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_count236 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %162, i32 0, i32 35
  %163 = load i8, i8* %ref_frame6, align 1, !tbaa !8
  %idxprom237 = sext i8 %163 to i32
  %arrayidx238 = getelementptr inbounds [29 x i8], [29 x i8]* %ref_mv_count236, i32 0, i32 %idxprom237
  %164 = load i8, i8* %arrayidx238, align 1, !tbaa !8
  %conv239 = zext i8 %164 to i32
  %cmp240 = icmp sgt i32 %conv239, 1
  br i1 %cmp240, label %if.then242, label %if.end253

if.then242:                                       ; preds = %if.then235
  %arrayidx243 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %ref_mv, i32 0, i32 0
  %165 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_stack244 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %165, i32 0, i32 36
  %166 = load i8, i8* %ref_frame6, align 1, !tbaa !8
  %idxprom245 = sext i8 %166 to i32
  %arrayidx246 = getelementptr inbounds [29 x [8 x %struct.candidate_mv]], [29 x [8 x %struct.candidate_mv]]* %ref_mv_stack244, i32 0, i32 %idxprom245
  %167 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx247 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %167, i32 0, i32 27
  %bf.load248 = load i8, i8* %ref_mv_idx247, align 1
  %bf.clear249 = and i8 %bf.load248, 3
  %idxprom250 = zext i8 %bf.clear249 to i32
  %arrayidx251 = getelementptr inbounds [8 x %struct.candidate_mv], [8 x %struct.candidate_mv]* %arrayidx246, i32 0, i32 %idxprom250
  %this_mv252 = getelementptr inbounds %struct.candidate_mv, %struct.candidate_mv* %arrayidx251, i32 0, i32 0
  %168 = bitcast %union.int_mv* %arrayidx243 to i8*
  %169 = bitcast %union.int_mv* %this_mv252 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %168, i8* align 4 %169, i32 4, i1 false), !tbaa.struct !124
  br label %if.end253

if.end253:                                        ; preds = %if.then242, %if.then235
  br label %if.end254

if.end254:                                        ; preds = %if.end253, %if.else230
  br label %if.end255

if.end255:                                        ; preds = %if.end254, %if.end229
  %170 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %skip_mode256 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %170, i32 0, i32 26
  %bf.load257 = load i8, i8* %skip_mode256, align 4
  %bf.lshr258 = lshr i8 %bf.load257, 5
  %bf.clear259 = and i8 %bf.lshr258, 1
  %tobool260 = icmp ne i8 %bf.clear259, 0
  br i1 %tobool260, label %if.then261, label %if.end262

if.then261:                                       ; preds = %if.end255
  br label %if.end262

if.end262:                                        ; preds = %if.then261, %if.end255
  %171 = bitcast i32* %mv_corrupted_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %171) #6
  %172 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %173 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %174 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode263 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %174, i32 0, i32 7
  %175 = load i8, i8* %mode263, align 1, !tbaa !50
  %176 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame264 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %176, i32 0, i32 12
  %arraydecay265 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame264, i32 0, i32 0
  %177 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mv = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %177, i32 0, i32 2
  %arraydecay266 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv, i32 0, i32 0
  %arraydecay267 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %ref_mv, i32 0, i32 0
  %arraydecay268 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearestmv, i32 0, i32 0
  %arraydecay269 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %nearmv, i32 0, i32 0
  %178 = load i32, i32* %is_compound, align 4, !tbaa !2
  %179 = load i32, i32* %allow_hp, align 4, !tbaa !2
  %180 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call270 = call i32 @assign_mv(%struct.AV1Common* %172, %struct.macroblockd* %173, i8 zeroext %175, i8* %arraydecay265, %union.int_mv* %arraydecay266, %union.int_mv* %arraydecay267, %union.int_mv* %arraydecay268, %union.int_mv* %arraydecay269, i32 %178, i32 %179, %struct.aom_reader* %180)
  %tobool271 = icmp ne i32 %call270, 0
  %lnot = xor i1 %tobool271, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %mv_corrupted_flag, align 4, !tbaa !2
  %181 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %corrupted = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %181, i32 0, i32 44
  %182 = load i32, i32* %mv_corrupted_flag, align 4, !tbaa !2
  call void @aom_merge_corrupted_flag(i32* %corrupted, i32 %182)
  %183 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %use_wedge_interintra = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %183, i32 0, i32 26
  %bf.load272 = load i8, i8* %use_wedge_interintra, align 4
  %bf.clear273 = and i8 %bf.load272, -2
  store i8 %bf.clear273, i8* %use_wedge_interintra, align 4
  %184 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %184, i32 0, i32 37
  %enable_interintra_compound = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 17
  %185 = load i8, i8* %enable_interintra_compound, align 2, !tbaa !157
  %conv274 = zext i8 %185 to i32
  %tobool275 = icmp ne i32 %conv274, 0
  br i1 %tobool275, label %land.lhs.true276, label %if.end325

land.lhs.true276:                                 ; preds = %if.end262
  %186 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %skip_mode277 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %186, i32 0, i32 26
  %bf.load278 = load i8, i8* %skip_mode277, align 4
  %bf.lshr279 = lshr i8 %bf.load278, 5
  %bf.clear280 = and i8 %bf.lshr279, 1
  %tobool281 = icmp ne i8 %bf.clear280, 0
  br i1 %tobool281, label %if.end325, label %land.lhs.true282

land.lhs.true282:                                 ; preds = %land.lhs.true276
  %187 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call283 = call i32 @is_interintra_allowed(%struct.MB_MODE_INFO* %187)
  %tobool284 = icmp ne i32 %call283, 0
  br i1 %tobool284, label %if.then285, label %if.end325

if.then285:                                       ; preds = %land.lhs.true282
  %188 = bitcast i32* %bsize_group to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %188) #6
  %189 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom286 = zext i8 %189 to i32
  %arrayidx287 = getelementptr inbounds [22 x i8], [22 x i8]* @size_group_lookup, i32 0, i32 %idxprom286
  %190 = load i8, i8* %arrayidx287, align 1, !tbaa !8
  %conv288 = zext i8 %190 to i32
  store i32 %conv288, i32* %bsize_group, align 4, !tbaa !2
  %191 = bitcast i32* %interintra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %191) #6
  %192 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %193 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %interintra_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %193, i32 0, i32 20
  %194 = load i32, i32* %bsize_group, align 4, !tbaa !2
  %arrayidx289 = getelementptr inbounds [4 x [3 x i16]], [4 x [3 x i16]]* %interintra_cdf, i32 0, i32 %194
  %arraydecay290 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx289, i32 0, i32 0
  %call291 = call i32 @aom_read_symbol_(%struct.aom_reader* %192, i16* %arraydecay290, i32 2, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.read_inter_block_mode_info, i32 0, i32 0))
  store i32 %call291, i32* %interintra, align 4, !tbaa !2
  %195 = load i32, i32* %interintra, align 4, !tbaa !2
  %tobool292 = icmp ne i32 %195, 0
  br i1 %tobool292, label %if.then293, label %if.end324

if.then293:                                       ; preds = %if.then285
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %interintra_mode) #6
  %196 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %197 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %198 = load i32, i32* %bsize_group, align 4, !tbaa !2
  %call294 = call zeroext i8 @read_interintra_mode(%struct.macroblockd* %196, %struct.aom_reader* %197, i32 %198)
  store i8 %call294, i8* %interintra_mode, align 1, !tbaa !8
  %199 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame295 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %199, i32 0, i32 12
  %arrayidx296 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame295, i32 0, i32 1
  store i8 0, i8* %arrayidx296, align 1, !tbaa !8
  %200 = load i8, i8* %interintra_mode, align 1, !tbaa !8
  %201 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interintra_mode297 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %201, i32 0, i32 9
  store i8 %200, i8* %interintra_mode297, align 1, !tbaa !158
  %202 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %angle_delta = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %202, i32 0, i32 20
  %arrayidx298 = getelementptr inbounds [2 x i8], [2 x i8]* %angle_delta, i32 0, i32 0
  store i8 0, i8* %arrayidx298, align 4, !tbaa !8
  %203 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %angle_delta299 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %203, i32 0, i32 20
  %arrayidx300 = getelementptr inbounds [2 x i8], [2 x i8]* %angle_delta299, i32 0, i32 1
  store i8 0, i8* %arrayidx300, align 1, !tbaa !8
  %204 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %filter_intra_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %204, i32 0, i32 13
  %use_filter_intra = getelementptr inbounds %struct.FILTER_INTRA_MODE_INFO, %struct.FILTER_INTRA_MODE_INFO* %filter_intra_mode_info, i32 0, i32 1
  store i8 0, i8* %use_filter_intra, align 1, !tbaa !48
  %205 = load i8, i8* %bsize, align 1, !tbaa !8
  %call301 = call i32 @av1_is_wedge_used(i8 zeroext %205)
  %tobool302 = icmp ne i32 %call301, 0
  br i1 %tobool302, label %if.then303, label %if.end323

if.then303:                                       ; preds = %if.then293
  %206 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %207 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %wedge_interintra_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %207, i32 0, i32 21
  %208 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom304 = zext i8 %208 to i32
  %arrayidx305 = getelementptr inbounds [22 x [3 x i16]], [22 x [3 x i16]]* %wedge_interintra_cdf, i32 0, i32 %idxprom304
  %arraydecay306 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx305, i32 0, i32 0
  %call307 = call i32 @aom_read_symbol_(%struct.aom_reader* %206, i16* %arraydecay306, i32 2, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.read_inter_block_mode_info, i32 0, i32 0))
  %conv308 = trunc i32 %call307 to i8
  %209 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %use_wedge_interintra309 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %209, i32 0, i32 26
  %bf.load310 = load i8, i8* %use_wedge_interintra309, align 4
  %bf.value = and i8 %conv308, 1
  %bf.clear311 = and i8 %bf.load310, -2
  %bf.set = or i8 %bf.clear311, %bf.value
  store i8 %bf.set, i8* %use_wedge_interintra309, align 4
  %210 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %use_wedge_interintra312 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %210, i32 0, i32 26
  %bf.load313 = load i8, i8* %use_wedge_interintra312, align 4
  %bf.clear314 = and i8 %bf.load313, 1
  %tobool315 = icmp ne i8 %bf.clear314, 0
  br i1 %tobool315, label %if.then316, label %if.end322

if.then316:                                       ; preds = %if.then303
  %211 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %212 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %wedge_idx_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %212, i32 0, i32 19
  %213 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom317 = zext i8 %213 to i32
  %arrayidx318 = getelementptr inbounds [22 x [17 x i16]], [22 x [17 x i16]]* %wedge_idx_cdf, i32 0, i32 %idxprom317
  %arraydecay319 = getelementptr inbounds [17 x i16], [17 x i16]* %arrayidx318, i32 0, i32 0
  %call320 = call i32 @aom_read_symbol_(%struct.aom_reader* %211, i16* %arraydecay319, i32 16, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.read_inter_block_mode_info, i32 0, i32 0))
  %conv321 = trunc i32 %call320 to i8
  %214 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interintra_wedge_index = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %214, i32 0, i32 19
  store i8 %conv321, i8* %interintra_wedge_index, align 1, !tbaa !159
  br label %if.end322

if.end322:                                        ; preds = %if.then316, %if.then303
  br label %if.end323

if.end323:                                        ; preds = %if.end322, %if.then293
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %interintra_mode) #6
  br label %if.end324

if.end324:                                        ; preds = %if.end323, %if.then285
  %215 = bitcast i32* %interintra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #6
  %216 = bitcast i32* %bsize_group to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #6
  br label %if.end325

if.end325:                                        ; preds = %if.end324, %land.lhs.true282, %land.lhs.true276, %if.end262
  %217 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %217) #6
  store i32 0, i32* %ref, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end325
  %218 = load i32, i32* %ref, align 4, !tbaa !2
  %219 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call326 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %219)
  %add327 = add nsw i32 1, %call326
  %cmp328 = icmp slt i32 %218, %add327
  br i1 %cmp328, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %220 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %frame) #6
  %221 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame330 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %221, i32 0, i32 12
  %222 = load i32, i32* %ref, align 4, !tbaa !2
  %arrayidx331 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame330, i32 0, i32 %222
  %223 = load i8, i8* %arrayidx331, align 1, !tbaa !8
  store i8 %223, i8* %frame, align 1, !tbaa !8
  %224 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %225 = load i8, i8* %frame, align 1, !tbaa !8
  %call332 = call %struct.scale_factors* @get_ref_scale_factors_const(%struct.AV1Common* %224, i8 signext %225)
  %226 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %block_ref_scale_factors = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %226, i32 0, i32 21
  %227 = load i32, i32* %ref, align 4, !tbaa !2
  %arrayidx333 = getelementptr inbounds [2 x %struct.scale_factors*], [2 x %struct.scale_factors*]* %block_ref_scale_factors, i32 0, i32 %227
  store %struct.scale_factors* %call332, %struct.scale_factors** %arrayidx333, align 4, !tbaa !6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %frame) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %228 = load i32, i32* %ref, align 4, !tbaa !2
  %inc = add nsw i32 %228, 1
  store i32 %inc, i32* %ref, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %229 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %motion_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %229, i32 0, i32 10
  store i8 0, i8* %motion_mode, align 2, !tbaa !123
  %230 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type334 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %230, i32 0, i32 6
  %231 = load i8, i8* %sb_type334, align 2, !tbaa !76
  %call335 = call i32 @is_motion_variation_allowed_bsize(i8 zeroext %231)
  %tobool336 = icmp ne i32 %call335, 0
  br i1 %tobool336, label %land.lhs.true337, label %if.end350

land.lhs.true337:                                 ; preds = %for.end
  %232 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %skip_mode338 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %232, i32 0, i32 26
  %bf.load339 = load i8, i8* %skip_mode338, align 4
  %bf.lshr340 = lshr i8 %bf.load339, 5
  %bf.clear341 = and i8 %bf.lshr340, 1
  %tobool342 = icmp ne i8 %bf.clear341, 0
  br i1 %tobool342, label %if.end350, label %land.lhs.true343

land.lhs.true343:                                 ; preds = %land.lhs.true337
  %233 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call344 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %233)
  %tobool345 = icmp ne i32 %call344, 0
  br i1 %tobool345, label %if.end350, label %if.then346

if.then346:                                       ; preds = %land.lhs.true343
  %234 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %235 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %arraydecay347 = getelementptr inbounds [16 x i32], [16 x i32]* %pts, i32 0, i32 0
  %arraydecay348 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_inref, i32 0, i32 0
  %call349 = call zeroext i8 @av1_findSamples(%struct.AV1Common* %234, %struct.macroblockd* %235, i32* %arraydecay347, i32* %arraydecay348)
  %236 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %num_proj_ref = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %236, i32 0, i32 23
  store i8 %call349, i8* %num_proj_ref, align 4, !tbaa !160
  br label %if.end350

if.end350:                                        ; preds = %if.then346, %land.lhs.true343, %land.lhs.true337, %for.end
  %237 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %238 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  call void @av1_count_overlappable_neighbors(%struct.AV1Common* %237, %struct.macroblockd* %238)
  %239 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame351 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %239, i32 0, i32 12
  %arrayidx352 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame351, i32 0, i32 1
  %240 = load i8, i8* %arrayidx352, align 1, !tbaa !8
  %conv353 = sext i8 %240 to i32
  %cmp354 = icmp ne i32 %conv353, 0
  br i1 %cmp354, label %if.then356, label %if.end359

if.then356:                                       ; preds = %if.end350
  %241 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %242 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %243 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %244 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call357 = call zeroext i8 @read_motion_mode(%struct.AV1Common* %241, %struct.macroblockd* %242, %struct.MB_MODE_INFO* %243, %struct.aom_reader* %244)
  %245 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %motion_mode358 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %245, i32 0, i32 10
  store i8 %call357, i8* %motion_mode358, align 2, !tbaa !123
  br label %if.end359

if.end359:                                        ; preds = %if.then356, %if.end350
  %246 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %comp_group_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %246, i32 0, i32 27
  %bf.load360 = load i8, i8* %comp_group_idx, align 1
  %bf.clear361 = and i8 %bf.load360, -5
  store i8 %bf.clear361, i8* %comp_group_idx, align 1
  %247 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %compound_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %247, i32 0, i32 25
  store i8 1, i8* %compound_idx, align 1, !tbaa !161
  %248 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interinter_comp = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %248, i32 0, i32 0
  %type = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp, i32 0, i32 4
  store i8 0, i8* %type, align 1, !tbaa !162
  %249 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call362 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %249)
  %tobool363 = icmp ne i32 %call362, 0
  br i1 %tobool363, label %land.lhs.true364, label %if.end455

land.lhs.true364:                                 ; preds = %if.end359
  %250 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %skip_mode365 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %250, i32 0, i32 26
  %bf.load366 = load i8, i8* %skip_mode365, align 4
  %bf.lshr367 = lshr i8 %bf.load366, 5
  %bf.clear368 = and i8 %bf.lshr367, 1
  %tobool369 = icmp ne i8 %bf.clear368, 0
  br i1 %tobool369, label %if.end455, label %if.then370

if.then370:                                       ; preds = %land.lhs.true364
  %251 = bitcast i32* %masked_compound_used to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %251) #6
  %252 = load i8, i8* %bsize, align 1, !tbaa !8
  %call371 = call i32 @is_any_masked_compound_used(i8 zeroext %252)
  %tobool372 = icmp ne i32 %call371, 0
  br i1 %tobool372, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.then370
  %253 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %seq_params373 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %253, i32 0, i32 37
  %enable_masked_compound = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params373, i32 0, i32 18
  %254 = load i8, i8* %enable_masked_compound, align 1, !tbaa !163
  %conv374 = zext i8 %254 to i32
  %tobool375 = icmp ne i32 %conv374, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.then370
  %255 = phi i1 [ false, %if.then370 ], [ %tobool375, %land.rhs ]
  %land.ext = zext i1 %255 to i32
  store i32 %land.ext, i32* %masked_compound_used, align 4, !tbaa !2
  %256 = load i32, i32* %masked_compound_used, align 4, !tbaa !2
  %tobool376 = icmp ne i32 %256, 0
  br i1 %tobool376, label %if.then377, label %if.end388

if.then377:                                       ; preds = %land.end
  %257 = bitcast i32* %ctx_comp_group_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %257) #6
  %258 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call378 = call i32 @get_comp_group_idx_context(%struct.macroblockd* %258)
  store i32 %call378, i32* %ctx_comp_group_idx, align 4, !tbaa !2
  %259 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %260 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %comp_group_idx_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %260, i32 0, i32 39
  %261 = load i32, i32* %ctx_comp_group_idx, align 4, !tbaa !2
  %arrayidx379 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %comp_group_idx_cdf, i32 0, i32 %261
  %arraydecay380 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx379, i32 0, i32 0
  %call381 = call i32 @aom_read_symbol_(%struct.aom_reader* %259, i16* %arraydecay380, i32 2, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.read_inter_block_mode_info, i32 0, i32 0))
  %conv382 = trunc i32 %call381 to i8
  %262 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %comp_group_idx383 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %262, i32 0, i32 27
  %bf.load384 = load i8, i8* %comp_group_idx383, align 1
  %bf.value385 = and i8 %conv382, 1
  %bf.shl = shl i8 %bf.value385, 2
  %bf.clear386 = and i8 %bf.load384, -5
  %bf.set387 = or i8 %bf.clear386, %bf.shl
  store i8 %bf.set387, i8* %comp_group_idx383, align 1
  %263 = bitcast i32* %ctx_comp_group_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #6
  br label %if.end388

if.end388:                                        ; preds = %if.then377, %land.end
  %264 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %comp_group_idx389 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %264, i32 0, i32 27
  %bf.load390 = load i8, i8* %comp_group_idx389, align 1
  %bf.lshr391 = lshr i8 %bf.load390, 2
  %bf.clear392 = and i8 %bf.lshr391, 1
  %conv393 = zext i8 %bf.clear392 to i32
  %cmp394 = icmp eq i32 %conv393, 0
  br i1 %cmp394, label %if.then396, label %if.else417

if.then396:                                       ; preds = %if.end388
  %265 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %seq_params397 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %265, i32 0, i32 37
  %order_hint_info = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params397, i32 0, i32 10
  %enable_dist_wtd_comp = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %order_hint_info, i32 0, i32 2
  %266 = load i32, i32* %enable_dist_wtd_comp, align 8, !tbaa !164
  %tobool398 = icmp ne i32 %266, 0
  br i1 %tobool398, label %if.then399, label %if.else412

if.then399:                                       ; preds = %if.then396
  %267 = bitcast i32* %comp_index_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %267) #6
  %268 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %269 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call400 = call i32 @get_comp_index_context(%struct.AV1Common* %268, %struct.macroblockd* %269)
  store i32 %call400, i32* %comp_index_ctx, align 4, !tbaa !2
  %270 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %271 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %compound_index_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %271, i32 0, i32 38
  %272 = load i32, i32* %comp_index_ctx, align 4, !tbaa !2
  %arrayidx401 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %compound_index_cdf, i32 0, i32 %272
  %arraydecay402 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx401, i32 0, i32 0
  %call403 = call i32 @aom_read_symbol_(%struct.aom_reader* %270, i16* %arraydecay402, i32 2, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.read_inter_block_mode_info, i32 0, i32 0))
  %conv404 = trunc i32 %call403 to i8
  %273 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %compound_idx405 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %273, i32 0, i32 25
  store i8 %conv404, i8* %compound_idx405, align 1, !tbaa !161
  %274 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %compound_idx406 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %274, i32 0, i32 25
  %275 = load i8, i8* %compound_idx406, align 1, !tbaa !161
  %conv407 = zext i8 %275 to i32
  %tobool408 = icmp ne i32 %conv407, 0
  %276 = zext i1 %tobool408 to i64
  %cond = select i1 %tobool408, i32 0, i32 1
  %conv409 = trunc i32 %cond to i8
  %277 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interinter_comp410 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %277, i32 0, i32 0
  %type411 = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp410, i32 0, i32 4
  store i8 %conv409, i8* %type411, align 1, !tbaa !162
  %278 = bitcast i32* %comp_index_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #6
  br label %if.end416

if.else412:                                       ; preds = %if.then396
  %279 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %compound_idx413 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %279, i32 0, i32 25
  store i8 1, i8* %compound_idx413, align 1, !tbaa !161
  %280 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interinter_comp414 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %280, i32 0, i32 0
  %type415 = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp414, i32 0, i32 4
  store i8 0, i8* %type415, align 1, !tbaa !162
  br label %if.end416

if.end416:                                        ; preds = %if.else412, %if.then399
  br label %if.end454

if.else417:                                       ; preds = %if.end388
  %281 = load i8, i8* %bsize, align 1, !tbaa !8
  %call418 = call i32 @is_interinter_compound_used(i8 zeroext 2, i8 zeroext %281)
  %tobool419 = icmp ne i32 %call418, 0
  br i1 %tobool419, label %if.then420, label %if.else429

if.then420:                                       ; preds = %if.else417
  %282 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %283 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %compound_type_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %283, i32 0, i32 18
  %284 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom421 = zext i8 %284 to i32
  %arrayidx422 = getelementptr inbounds [22 x [3 x i16]], [22 x [3 x i16]]* %compound_type_cdf, i32 0, i32 %idxprom421
  %arraydecay423 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx422, i32 0, i32 0
  %call424 = call i32 @aom_read_symbol_(%struct.aom_reader* %282, i16* %arraydecay423, i32 2, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.read_inter_block_mode_info, i32 0, i32 0))
  %add425 = add nsw i32 2, %call424
  %conv426 = trunc i32 %add425 to i8
  %285 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interinter_comp427 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %285, i32 0, i32 0
  %type428 = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp427, i32 0, i32 4
  store i8 %conv426, i8* %type428, align 1, !tbaa !162
  br label %if.end432

if.else429:                                       ; preds = %if.else417
  %286 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interinter_comp430 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %286, i32 0, i32 0
  %type431 = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp430, i32 0, i32 4
  store i8 3, i8* %type431, align 1, !tbaa !162
  br label %if.end432

if.end432:                                        ; preds = %if.else429, %if.then420
  %287 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interinter_comp433 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %287, i32 0, i32 0
  %type434 = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp433, i32 0, i32 4
  %288 = load i8, i8* %type434, align 1, !tbaa !162
  %conv435 = zext i8 %288 to i32
  %cmp436 = icmp eq i32 %conv435, 2
  br i1 %cmp436, label %if.then438, label %if.else449

if.then438:                                       ; preds = %if.end432
  %289 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %290 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %wedge_idx_cdf439 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %290, i32 0, i32 19
  %291 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom440 = zext i8 %291 to i32
  %arrayidx441 = getelementptr inbounds [22 x [17 x i16]], [22 x [17 x i16]]* %wedge_idx_cdf439, i32 0, i32 %idxprom440
  %arraydecay442 = getelementptr inbounds [17 x i16], [17 x i16]* %arrayidx441, i32 0, i32 0
  %call443 = call i32 @aom_read_symbol_(%struct.aom_reader* %289, i16* %arraydecay442, i32 16, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.read_inter_block_mode_info, i32 0, i32 0))
  %conv444 = trunc i32 %call443 to i8
  %292 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interinter_comp445 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %292, i32 0, i32 0
  %wedge_index = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp445, i32 0, i32 1
  store i8 %conv444, i8* %wedge_index, align 4, !tbaa !165
  %293 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call446 = call i32 @aom_read_bit_(%struct.aom_reader* %293, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.read_inter_block_mode_info, i32 0, i32 0))
  %conv447 = trunc i32 %call446 to i8
  %294 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interinter_comp448 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %294, i32 0, i32 0
  %wedge_sign = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp448, i32 0, i32 2
  store i8 %conv447, i8* %wedge_sign, align 1, !tbaa !166
  br label %if.end453

if.else449:                                       ; preds = %if.end432
  %295 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call450 = call i32 @aom_read_literal_(%struct.aom_reader* %295, i32 1, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.read_inter_block_mode_info, i32 0, i32 0))
  %conv451 = trunc i32 %call450 to i8
  %296 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interinter_comp452 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %296, i32 0, i32 0
  %mask_type = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp452, i32 0, i32 3
  store i8 %conv451, i8* %mask_type, align 2, !tbaa !167
  br label %if.end453

if.end453:                                        ; preds = %if.else449, %if.then438
  br label %if.end454

if.end454:                                        ; preds = %if.end453, %if.end416
  %297 = bitcast i32* %masked_compound_used to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %297) #6
  br label %if.end455

if.end455:                                        ; preds = %if.end454, %land.lhs.true364, %if.end359
  %298 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %299 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %interp_filter = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %299, i32 0, i32 13
  %300 = load i8, i8* %interp_filter, align 1, !tbaa !168
  %301 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %seq_params456 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %301, i32 0, i32 37
  %enable_dual_filter = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params456, i32 0, i32 19
  %302 = load i8, i8* %enable_dual_filter, align 16, !tbaa !169
  %tobool457 = icmp ne i8 %302, 0
  %303 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %304 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_mb_interp_filter(%struct.macroblockd* %298, i8 zeroext %300, i1 zeroext %tobool457, %struct.MB_MODE_INFO* %303, %struct.aom_reader* %304)
  %305 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %305) #6
  %306 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row458 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %306, i32 0, i32 0
  %307 = load i32, i32* %mi_row458, align 16, !tbaa !71
  store i32 %307, i32* %mi_row, align 4, !tbaa !2
  %308 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %308) #6
  %309 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col459 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %309, i32 0, i32 1
  %310 = load i32, i32* %mi_col459, align 4, !tbaa !72
  store i32 %310, i32* %mi_col, align 4, !tbaa !2
  %311 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %motion_mode460 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %311, i32 0, i32 10
  %312 = load i8, i8* %motion_mode460, align 2, !tbaa !123
  %conv461 = zext i8 %312 to i32
  %cmp462 = icmp eq i32 %conv461, 2
  br i1 %cmp462, label %if.then464, label %if.end500

if.then464:                                       ; preds = %if.end455
  %313 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %wm_params = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %313, i32 0, i32 1
  %wmtype = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %wm_params, i32 0, i32 5
  store i8 3, i8* %wmtype, align 4, !tbaa !170
  %314 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %wm_params465 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %314, i32 0, i32 1
  %invalid = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %wm_params465, i32 0, i32 6
  store i8 0, i8* %invalid, align 1, !tbaa !171
  %315 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %num_proj_ref466 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %315, i32 0, i32 23
  %316 = load i8, i8* %num_proj_ref466, align 4, !tbaa !160
  %conv467 = zext i8 %316 to i32
  %cmp468 = icmp sgt i32 %conv467, 1
  br i1 %cmp468, label %if.then470, label %if.end480

if.then470:                                       ; preds = %if.then464
  %317 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mv471 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %317, i32 0, i32 2
  %arrayidx472 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv471, i32 0, i32 0
  %as_mv473 = bitcast %union.int_mv* %arrayidx472 to %struct.mv*
  %arraydecay474 = getelementptr inbounds [16 x i32], [16 x i32]* %pts, i32 0, i32 0
  %arraydecay475 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_inref, i32 0, i32 0
  %318 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %num_proj_ref476 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %318, i32 0, i32 23
  %319 = load i8, i8* %num_proj_ref476, align 4, !tbaa !160
  %conv477 = zext i8 %319 to i32
  %320 = load i8, i8* %bsize, align 1, !tbaa !8
  %call478 = call zeroext i8 @av1_selectSamples(%struct.mv* %as_mv473, i32* %arraydecay474, i32* %arraydecay475, i32 %conv477, i8 zeroext %320)
  %321 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %num_proj_ref479 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %321, i32 0, i32 23
  store i8 %call478, i8* %num_proj_ref479, align 4, !tbaa !160
  br label %if.end480

if.end480:                                        ; preds = %if.then470, %if.then464
  %322 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %num_proj_ref481 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %322, i32 0, i32 23
  %323 = load i8, i8* %num_proj_ref481, align 4, !tbaa !160
  %conv482 = zext i8 %323 to i32
  %arraydecay483 = getelementptr inbounds [16 x i32], [16 x i32]* %pts, i32 0, i32 0
  %arraydecay484 = getelementptr inbounds [16 x i32], [16 x i32]* %pts_inref, i32 0, i32 0
  %324 = load i8, i8* %bsize, align 1, !tbaa !8
  %325 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mv485 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %325, i32 0, i32 2
  %arrayidx486 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv485, i32 0, i32 0
  %as_mv487 = bitcast %union.int_mv* %arrayidx486 to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv487, i32 0, i32 0
  %326 = load i16, i16* %row, align 4, !tbaa !8
  %conv488 = sext i16 %326 to i32
  %327 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mv489 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %327, i32 0, i32 2
  %arrayidx490 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv489, i32 0, i32 0
  %as_mv491 = bitcast %union.int_mv* %arrayidx490 to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv491, i32 0, i32 1
  %328 = load i16, i16* %col, align 2, !tbaa !8
  %conv492 = sext i16 %328 to i32
  %329 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %wm_params493 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %329, i32 0, i32 1
  %330 = load i32, i32* %mi_row, align 4, !tbaa !2
  %331 = load i32, i32* %mi_col, align 4, !tbaa !2
  %call494 = call i32 @av1_find_projection(i32 %conv482, i32* %arraydecay483, i32* %arraydecay484, i8 zeroext %324, i32 %conv488, i32 %conv492, %struct.WarpedMotionParams* %wm_params493, i32 %330, i32 %331)
  %tobool495 = icmp ne i32 %call494, 0
  br i1 %tobool495, label %if.then496, label %if.end499

if.then496:                                       ; preds = %if.end480
  %332 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %wm_params497 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %332, i32 0, i32 1
  %invalid498 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %wm_params497, i32 0, i32 6
  store i8 1, i8* %invalid498, align 1, !tbaa !171
  br label %if.end499

if.end499:                                        ; preds = %if.then496, %if.end480
  br label %if.end500

if.end500:                                        ; preds = %if.end499, %if.end455
  %333 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !6
  %334 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call501 = call zeroext i8 @store_cfl_required(%struct.AV1Common* %333, %struct.macroblockd* %334)
  %conv502 = zext i8 %call501 to i32
  %335 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cfl = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %335, i32 0, i32 56
  %store_y = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %cfl, i32 0, i32 10
  store i32 %conv502, i32* %store_y, align 8, !tbaa !88
  %336 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #6
  %337 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #6
  %338 = bitcast i32* %mv_corrupted_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #6
  %339 = bitcast [2 x %union.int_mv]* %ref_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %339) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_frame6) #6
  %340 = bitcast i32* %is_compound to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #6
  %341 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %341) #6
  %342 = bitcast [16 x i32]* %pts_inref to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %342) #6
  %343 = bitcast [16 x i32]* %pts to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %343) #6
  %344 = bitcast [29 x i16]* %inter_mode_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 58, i8* %344) #6
  %345 = bitcast [29 x [2 x %union.int_mv]]* %ref_mvs to i8*
  call void @llvm.lifetime.end.p0i8(i64 232, i8* %345) #6
  %346 = bitcast [2 x %union.int_mv]* %nearmv to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %346) #6
  %347 = bitcast [2 x %union.int_mv]* %nearestmv to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %347) #6
  %348 = bitcast i32* %allow_hp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %348) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  %349 = bitcast %struct.FeatureFlags** %features to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %349) #6
  %350 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %350) #6
  ret void
}

; Function Attrs: nounwind
define internal void @read_intra_block_mode_info(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.MB_MODE_INFO* %mbmi, %struct.aom_reader* %r) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %bsize = alloca i8, align 1
  %use_angle_delta = alloca i32, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 6
  %1 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %1, i8* %bsize, align 1, !tbaa !8
  %2 = bitcast i32* %use_angle_delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i8, i8* %bsize, align 1, !tbaa !8
  %call = call i32 @av1_use_angle_delta(i8 zeroext %3)
  store i32 %call, i32* %use_angle_delta, align 4, !tbaa !2
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %4, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  store i8 0, i8* %arrayidx, align 4, !tbaa !8
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 12
  %arrayidx2 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame1, i32 0, i32 1
  store i8 -1, i8* %arrayidx2, align 1, !tbaa !8
  %6 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 40
  %8 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %8, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %9 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %10 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %y_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %10, i32 0, i32 52
  %11 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom = zext i8 %11 to i32
  %arrayidx3 = getelementptr inbounds [22 x i8], [22 x i8]* @size_group_lookup, i32 0, i32 %idxprom
  %12 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  %idxprom4 = zext i8 %12 to i32
  %arrayidx5 = getelementptr inbounds [4 x [14 x i16]], [4 x [14 x i16]]* %y_mode_cdf, i32 0, i32 %idxprom4
  %arraydecay = getelementptr inbounds [14 x i16], [14 x i16]* %arrayidx5, i32 0, i32 0
  %call6 = call zeroext i8 @read_intra_mode(%struct.aom_reader* %9, i16* %arraydecay)
  %13 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %13, i32 0, i32 7
  store i8 %call6, i8* %mode, align 1, !tbaa !50
  %14 = load i32, i32* %use_angle_delta, align 4, !tbaa !2
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %entry
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode7 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %15, i32 0, i32 7
  %16 = load i8, i8* %mode7, align 1, !tbaa !50
  %call8 = call i32 @av1_is_directional_mode(i8 zeroext %16)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %17 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %18 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %angle_delta_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %18, i32 0, i32 57
  %19 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode10 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %19, i32 0, i32 7
  %20 = load i8, i8* %mode10, align 1, !tbaa !50
  %conv = zext i8 %20 to i32
  %sub = sub nsw i32 %conv, 1
  %arrayidx11 = getelementptr inbounds [8 x [8 x i16]], [8 x [8 x i16]]* %angle_delta_cdf, i32 0, i32 %sub
  %arraydecay12 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx11, i32 0, i32 0
  %call13 = call i32 @read_angle_delta(%struct.aom_reader* %17, i16* %arraydecay12)
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call13, %cond.true ], [ 0, %cond.false ]
  %conv14 = trunc i32 %cond to i8
  %21 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %angle_delta = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %21, i32 0, i32 20
  %arrayidx15 = getelementptr inbounds [2 x i8], [2 x i8]* %angle_delta, i32 0, i32 0
  store i8 %conv14, i8* %arrayidx15, align 4, !tbaa !8
  %22 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %22, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %23 = load i8, i8* %monochrome, align 1, !tbaa !84
  %tobool16 = icmp ne i8 %23, 0
  br i1 %tobool16, label %if.else, label %land.lhs.true17

land.lhs.true17:                                  ; preds = %cond.end
  %24 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %is_chroma_ref = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %24, i32 0, i32 3
  %25 = load i8, i8* %is_chroma_ref, align 4, !tbaa !85, !range !46
  %tobool18 = trunc i8 %25 to i1
  br i1 %tobool18, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true17
  %26 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %27 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %28 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call20 = call zeroext i8 @is_cfl_allowed(%struct.macroblockd* %28)
  %29 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode21 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %29, i32 0, i32 7
  %30 = load i8, i8* %mode21, align 1, !tbaa !50
  %call22 = call zeroext i8 @read_intra_mode_uv(%struct.frame_contexts* %26, %struct.aom_reader* %27, i8 zeroext %call20, i8 zeroext %30)
  %31 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %uv_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %31, i32 0, i32 8
  store i8 %call22, i8* %uv_mode, align 4, !tbaa !86
  %32 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %uv_mode23 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %32, i32 0, i32 8
  %33 = load i8, i8* %uv_mode23, align 4, !tbaa !86
  %conv24 = zext i8 %33 to i32
  %cmp = icmp eq i32 %conv24, 13
  br i1 %cmp, label %if.then26, label %if.end

if.then26:                                        ; preds = %if.then
  %34 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx27 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %34, i32 0, i32 40
  %35 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx27, align 16, !tbaa !47
  %36 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %37 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %cfl_alpha_signs = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %37, i32 0, i32 21
  %call28 = call zeroext i8 @read_cfl_alphas(%struct.frame_contexts* %35, %struct.aom_reader* %36, i8* %cfl_alpha_signs)
  %38 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %cfl_alpha_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %38, i32 0, i32 22
  store i8 %call28, i8* %cfl_alpha_idx, align 1, !tbaa !87
  br label %if.end

if.end:                                           ; preds = %if.then26, %if.then
  %39 = load i32, i32* %use_angle_delta, align 4, !tbaa !2
  %tobool29 = icmp ne i32 %39, 0
  br i1 %tobool29, label %land.lhs.true30, label %cond.false43

land.lhs.true30:                                  ; preds = %if.end
  %40 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %uv_mode31 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %40, i32 0, i32 8
  %41 = load i8, i8* %uv_mode31, align 4, !tbaa !86
  %call32 = call zeroext i8 @get_uv_mode(i8 zeroext %41)
  %call33 = call i32 @av1_is_directional_mode(i8 zeroext %call32)
  %tobool34 = icmp ne i32 %call33, 0
  br i1 %tobool34, label %cond.true35, label %cond.false43

cond.true35:                                      ; preds = %land.lhs.true30
  %42 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %43 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %angle_delta_cdf36 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %43, i32 0, i32 57
  %44 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %uv_mode37 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %44, i32 0, i32 8
  %45 = load i8, i8* %uv_mode37, align 4, !tbaa !86
  %conv38 = zext i8 %45 to i32
  %sub39 = sub nsw i32 %conv38, 1
  %arrayidx40 = getelementptr inbounds [8 x [8 x i16]], [8 x [8 x i16]]* %angle_delta_cdf36, i32 0, i32 %sub39
  %arraydecay41 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx40, i32 0, i32 0
  %call42 = call i32 @read_angle_delta(%struct.aom_reader* %42, i16* %arraydecay41)
  br label %cond.end44

cond.false43:                                     ; preds = %land.lhs.true30, %if.end
  br label %cond.end44

cond.end44:                                       ; preds = %cond.false43, %cond.true35
  %cond45 = phi i32 [ %call42, %cond.true35 ], [ 0, %cond.false43 ]
  %conv46 = trunc i32 %cond45 to i8
  %46 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %angle_delta47 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %46, i32 0, i32 20
  %arrayidx48 = getelementptr inbounds [2 x i8], [2 x i8]* %angle_delta47, i32 0, i32 1
  store i8 %conv46, i8* %arrayidx48, align 1, !tbaa !8
  br label %if.end50

if.else:                                          ; preds = %land.lhs.true17, %cond.end
  %47 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %uv_mode49 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %47, i32 0, i32 8
  store i8 0, i8* %uv_mode49, align 4, !tbaa !86
  br label %if.end50

if.end50:                                         ; preds = %if.else, %cond.end44
  %48 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %49 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call51 = call zeroext i8 @store_cfl_required(%struct.AV1Common* %48, %struct.macroblockd* %49)
  %conv52 = zext i8 %call51 to i32
  %50 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cfl = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %50, i32 0, i32 56
  %store_y = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %cfl, i32 0, i32 10
  store i32 %conv52, i32* %store_y, align 8, !tbaa !88
  %51 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %51, i32 0, i32 5
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 1
  %arrayidx53 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 0
  store i8 0, i8* %arrayidx53, align 4, !tbaa !8
  %52 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %palette_mode_info54 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %52, i32 0, i32 5
  %palette_size55 = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info54, i32 0, i32 1
  %arrayidx56 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size55, i32 0, i32 1
  store i8 0, i8* %arrayidx56, align 1, !tbaa !8
  %53 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %53, i32 0, i32 21
  %allow_screen_content_tools = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 3
  %54 = load i8, i8* %allow_screen_content_tools, align 1, !tbaa !89, !range !46
  %tobool57 = trunc i8 %54 to i1
  %conv58 = zext i1 %tobool57 to i32
  %55 = load i8, i8* %bsize, align 1, !tbaa !8
  %call59 = call i32 @av1_allow_palette(i32 %conv58, i8 zeroext %55)
  %tobool60 = icmp ne i32 %call59, 0
  br i1 %tobool60, label %if.then61, label %if.end62

if.then61:                                        ; preds = %if.end50
  %56 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %57 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %58 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_palette_mode_info(%struct.AV1Common* %56, %struct.macroblockd* %57, %struct.aom_reader* %58)
  br label %if.end62

if.end62:                                         ; preds = %if.then61, %if.end50
  %59 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %60 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %61 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @read_filter_intra_mode_info(%struct.AV1Common* %59, %struct.macroblockd* %60, %struct.aom_reader* %61)
  %62 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast i32* %use_angle_delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  ret void
}

; Function Attrs: nounwind
define internal void @copy_segment_id(%struct.CommonModeInfoParams* %mi_params, i8* %last_segment_ids, i8* %current_segment_ids, i32 %mi_offset, i32 %x_mis, i32 %y_mis) #0 {
entry:
  %mi_params.addr = alloca %struct.CommonModeInfoParams*, align 4
  %last_segment_ids.addr = alloca i8*, align 4
  %current_segment_ids.addr = alloca i8*, align 4
  %mi_offset.addr = alloca i32, align 4
  %x_mis.addr = alloca i32, align 4
  %y_mis.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  store %struct.CommonModeInfoParams* %mi_params, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  store i8* %last_segment_ids, i8** %last_segment_ids.addr, align 4, !tbaa !6
  store i8* %current_segment_ids, i8** %current_segment_ids.addr, align 4, !tbaa !6
  store i32 %mi_offset, i32* %mi_offset.addr, align 4, !tbaa !2
  store i32 %x_mis, i32* %x_mis.addr, align 4, !tbaa !2
  store i32 %y_mis, i32* %y_mis.addr, align 4, !tbaa !2
  %0 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %entry
  %1 = load i32, i32* %y, align 4, !tbaa !2
  %2 = load i32, i32* %y_mis.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end14

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store i32 0, i32* %x, align 4, !tbaa !2
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %x, align 4, !tbaa !2
  %6 = load i32, i32* %x_mis.addr, align 4, !tbaa !2
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i8*, i8** %last_segment_ids.addr, align 4, !tbaa !6
  %tobool = icmp ne i8* %8, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body4
  %9 = load i8*, i8** %last_segment_ids.addr, align 4, !tbaa !6
  %10 = load i32, i32* %mi_offset.addr, align 4, !tbaa !2
  %11 = load i32, i32* %y, align 4, !tbaa !2
  %12 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %12, i32 0, i32 4
  %13 = load i32, i32* %mi_cols, align 4, !tbaa !106
  %mul = mul nsw i32 %11, %13
  %add = add nsw i32 %10, %mul
  %14 = load i32, i32* %x, align 4, !tbaa !2
  %add5 = add nsw i32 %add, %14
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %add5
  %15 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %15 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ 0, %cond.false ]
  %conv6 = trunc i32 %cond to i8
  %16 = load i8*, i8** %current_segment_ids.addr, align 4, !tbaa !6
  %17 = load i32, i32* %mi_offset.addr, align 4, !tbaa !2
  %18 = load i32, i32* %y, align 4, !tbaa !2
  %19 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_cols7 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %19, i32 0, i32 4
  %20 = load i32, i32* %mi_cols7, align 4, !tbaa !106
  %mul8 = mul nsw i32 %18, %20
  %add9 = add nsw i32 %17, %mul8
  %21 = load i32, i32* %x, align 4, !tbaa !2
  %add10 = add nsw i32 %add9, %21
  %arrayidx11 = getelementptr inbounds i8, i8* %16, i32 %add10
  store i8 %conv6, i8* %arrayidx11, align 1, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %22 = load i32, i32* %x, align 4, !tbaa !2
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %x, align 4, !tbaa !2
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %23 = load i32, i32* %y, align 4, !tbaa !2
  %inc13 = add nsw i32 %23, 1
  store i32 %inc13, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.end14:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal i32 @get_predicted_segment_id(%struct.AV1Common* %cm, i32 %mi_offset, i32 %x_mis, i32 %y_mis) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %mi_offset.addr = alloca i32, align 4
  %x_mis.addr = alloca i32, align 4
  %y_mis.addr = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i32 %mi_offset, i32* %mi_offset.addr, align 4, !tbaa !2
  store i32 %x_mis, i32* %x_mis.addr, align 4, !tbaa !2
  store i32 %y_mis, i32* %y_mis.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %last_frame_seg_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 25
  %1 = load i8*, i8** %last_frame_seg_map, align 16, !tbaa !152
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %last_frame_seg_map1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 25
  %4 = load i8*, i8** %last_frame_seg_map1, align 16, !tbaa !152
  %5 = load i32, i32* %mi_offset.addr, align 4, !tbaa !2
  %6 = load i32, i32* %x_mis.addr, align 4, !tbaa !2
  %7 = load i32, i32* %y_mis.addr, align 4, !tbaa !2
  %call = call i32 @dec_get_segment_id(%struct.AV1Common* %2, i8* %4, i32 %5, i32 %6, i32 %7)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call, %cond.true ], [ 0, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_get_pred_context_seg_id(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %above_mi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mi = alloca %struct.MB_MODE_INFO*, align 4
  %above_sip = alloca i32, align 4
  %left_sip = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 12
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 16, !tbaa !74
  store %struct.MB_MODE_INFO* %2, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %3 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 11
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !75
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %6 = bitcast i32* %above_sip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %cmp = icmp ne %struct.MB_MODE_INFO* %7, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %seg_id_predicted = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 26
  %bf.load = load i8, i8* %seg_id_predicted, align 4
  %bf.lshr = lshr i8 %bf.load, 4
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %above_sip, align 4, !tbaa !2
  %9 = bitcast i32* %left_sip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %cmp1 = icmp ne %struct.MB_MODE_INFO* %10, null
  br i1 %cmp1, label %cond.true3, label %cond.false9

cond.true3:                                       ; preds = %cond.end
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %seg_id_predicted4 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %11, i32 0, i32 26
  %bf.load5 = load i8, i8* %seg_id_predicted4, align 4
  %bf.lshr6 = lshr i8 %bf.load5, 4
  %bf.clear7 = and i8 %bf.lshr6, 1
  %conv8 = zext i8 %bf.clear7 to i32
  br label %cond.end10

cond.false9:                                      ; preds = %cond.end
  br label %cond.end10

cond.end10:                                       ; preds = %cond.false9, %cond.true3
  %cond11 = phi i32 [ %conv8, %cond.true3 ], [ 0, %cond.false9 ]
  store i32 %cond11, i32* %left_sip, align 4, !tbaa !2
  %12 = load i32, i32* %above_sip, align 4, !tbaa !2
  %13 = load i32, i32* %left_sip, align 4, !tbaa !2
  %add = add nsw i32 %12, %13
  %14 = bitcast i32* %left_sip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast i32* %above_sip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret i32 %add
}

; Function Attrs: nounwind
define internal i32 @dec_get_segment_id(%struct.AV1Common* %cm, i8* %segment_ids, i32 %mi_offset, i32 %x_mis, i32 %y_mis) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %segment_ids.addr = alloca i8*, align 4
  %mi_offset.addr = alloca i32, align 4
  %x_mis.addr = alloca i32, align 4
  %y_mis.addr = alloca i32, align 4
  %segment_id = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i8* %segment_ids, i8** %segment_ids.addr, align 4, !tbaa !6
  store i32 %mi_offset, i32* %mi_offset.addr, align 4, !tbaa !2
  store i32 %x_mis, i32* %x_mis.addr, align 4, !tbaa !2
  store i32 %y_mis, i32* %y_mis.addr, align 4, !tbaa !2
  %0 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 2147483647, i32* %segment_id, align 4, !tbaa !2
  %1 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %2 = load i32, i32* %y, align 4, !tbaa !2
  %3 = load i32, i32* %y_mis.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  br label %for.end17

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store i32 0, i32* %x, align 4, !tbaa !2
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %x, align 4, !tbaa !2
  %7 = load i32, i32* %x_mis.addr, align 4, !tbaa !2
  %cmp2 = icmp slt i32 %6, %7
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %8 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %9 = load i32, i32* %segment_id, align 4, !tbaa !2
  %10 = load i8*, i8** %segment_ids.addr, align 4, !tbaa !6
  %11 = load i32, i32* %mi_offset.addr, align 4, !tbaa !2
  %12 = load i32, i32* %y, align 4, !tbaa !2
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %14 = load i32, i32* %mi_cols, align 4, !tbaa !90
  %mul = mul nsw i32 %12, %14
  %add = add nsw i32 %11, %mul
  %15 = load i32, i32* %x, align 4, !tbaa !2
  %add5 = add nsw i32 %add, %15
  %arrayidx = getelementptr inbounds i8, i8* %10, i32 %add5
  %16 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %16 to i32
  %cmp6 = icmp slt i32 %9, %conv
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body4
  %17 = load i32, i32* %segment_id, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %for.body4
  %18 = load i8*, i8** %segment_ids.addr, align 4, !tbaa !6
  %19 = load i32, i32* %mi_offset.addr, align 4, !tbaa !2
  %20 = load i32, i32* %y, align 4, !tbaa !2
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 22
  %mi_cols9 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params8, i32 0, i32 4
  %22 = load i32, i32* %mi_cols9, align 4, !tbaa !90
  %mul10 = mul nsw i32 %20, %22
  %add11 = add nsw i32 %19, %mul10
  %23 = load i32, i32* %x, align 4, !tbaa !2
  %add12 = add nsw i32 %add11, %23
  %arrayidx13 = getelementptr inbounds i8, i8* %18, i32 %add12
  %24 = load i8, i8* %arrayidx13, align 1, !tbaa !8
  %conv14 = zext i8 %24 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %17, %cond.true ], [ %conv14, %cond.false ]
  store i32 %cond, i32* %segment_id, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %25 = load i32, i32* %x, align 4, !tbaa !2
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %x, align 4, !tbaa !2
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %26 = load i32, i32* %y, align 4, !tbaa !2
  %inc16 = add nsw i32 %26, 1
  store i32 %inc16, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.end17:                                        ; preds = %for.cond.cleanup
  %27 = load i32, i32* %segment_id, align 4, !tbaa !2
  store i32 1, i32* %cleanup.dest.slot, align 4
  %28 = bitcast i32* %segment_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  ret i32 %27
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_comp_ref_allowed(i8 zeroext %bsize) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %1 to i32
  %2 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom1 = zext i8 %2 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom1
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = zext i8 %3 to i32
  %cmp = icmp slt i32 %conv, %conv3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom5 = zext i8 %4 to i32
  %arrayidx6 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom5
  %5 = load i8, i8* %arrayidx6, align 1, !tbaa !8
  %conv7 = zext i8 %5 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom8 = zext i8 %6 to i32
  %arrayidx9 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom8
  %7 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  %conv10 = zext i8 %7 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv7, %cond.true ], [ %conv10, %cond.false ]
  %cmp11 = icmp sge i32 %cond, 8
  %conv12 = zext i1 %cmp11 to i32
  ret i32 %conv12
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_get_skip_mode_context(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %above_mi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mi = alloca %struct.MB_MODE_INFO*, align 4
  %above_skip_mode = alloca i32, align 4
  %left_skip_mode = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 12
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 16, !tbaa !74
  store %struct.MB_MODE_INFO* %2, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %3 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 11
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !75
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %6 = bitcast i32* %above_skip_mode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %tobool = icmp ne %struct.MB_MODE_INFO* %7, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %skip_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 26
  %bf.load = load i8, i8* %skip_mode, align 4
  %bf.lshr = lshr i8 %bf.load, 5
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %above_skip_mode, align 4, !tbaa !2
  %9 = bitcast i32* %left_skip_mode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %tobool1 = icmp ne %struct.MB_MODE_INFO* %10, null
  br i1 %tobool1, label %cond.true2, label %cond.false8

cond.true2:                                       ; preds = %cond.end
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %skip_mode3 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %11, i32 0, i32 26
  %bf.load4 = load i8, i8* %skip_mode3, align 4
  %bf.lshr5 = lshr i8 %bf.load4, 5
  %bf.clear6 = and i8 %bf.lshr5, 1
  %conv7 = zext i8 %bf.clear6 to i32
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true2
  %cond10 = phi i32 [ %conv7, %cond.true2 ], [ 0, %cond.false8 ]
  store i32 %cond10, i32* %left_skip_mode, align 4, !tbaa !2
  %12 = load i32, i32* %above_skip_mode, align 4, !tbaa !2
  %13 = load i32, i32* %left_skip_mode, align 4, !tbaa !2
  %add = add nsw i32 %12, %13
  %14 = bitcast i32* %left_skip_mode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast i32* %above_skip_mode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret i32 %add
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_segdata(%struct.segmentation* %seg, i32 %segment_id, i8 zeroext %feature_id) #2 {
entry:
  %seg.addr = alloca %struct.segmentation*, align 4
  %segment_id.addr = alloca i32, align 4
  %feature_id.addr = alloca i8, align 1
  store %struct.segmentation* %seg, %struct.segmentation** %seg.addr, align 4, !tbaa !6
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !2
  store i8 %feature_id, i8* %feature_id.addr, align 1, !tbaa !8
  %0 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !6
  %feature_data = getelementptr inbounds %struct.segmentation, %struct.segmentation* %0, i32 0, i32 4
  %1 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [8 x [8 x i16]], [8 x [8 x i16]]* %feature_data, i32 0, i32 %1
  %2 = load i8, i8* %feature_id.addr, align 1, !tbaa !8
  %idxprom = zext i8 %2 to i32
  %arrayidx1 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx, i32 0, i32 %idxprom
  %3 = load i16, i16* %arrayidx1, align 2, !tbaa !101
  %conv = sext i16 %3 to i32
  ret i32 %conv
}

declare i32 @av1_get_intra_inter_context(%struct.macroblockd*) #3

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: inlinehint nounwind
define internal void @av1_collect_neighbors_ref_counts(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ref_counts = alloca i8*, align 4
  %above_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %above_in_image = alloca i32, align 4
  %left_in_image = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %neighbors_ref_counts = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 39
  %1 = bitcast [8 x i8]* %neighbors_ref_counts to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %1, i8 0, i32 8, i1 false)
  %2 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %neighbors_ref_counts1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 39
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %neighbors_ref_counts1, i32 0, i32 0
  store i8* %arraydecay, i8** %ref_counts, align 4, !tbaa !6
  %4 = bitcast %struct.MB_MODE_INFO** %above_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_mbmi2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 12
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi2, align 16, !tbaa !74
  store %struct.MB_MODE_INFO* %6, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !6
  %7 = bitcast %struct.MB_MODE_INFO** %left_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_mbmi3 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 11
  %9 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi3, align 4, !tbaa !75
  store %struct.MB_MODE_INFO* %9, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !6
  %10 = bitcast i32* %above_in_image to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %11, i32 0, i32 7
  %12 = load i8, i8* %up_available, align 8, !tbaa !135, !range !46
  %tobool = trunc i8 %12 to i1
  %conv = zext i1 %tobool to i32
  store i32 %conv, i32* %above_in_image, align 4, !tbaa !2
  %13 = bitcast i32* %left_in_image to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %14, i32 0, i32 8
  %15 = load i8, i8* %left_available, align 1, !tbaa !136, !range !46
  %tobool4 = trunc i8 %15 to i1
  %conv5 = zext i1 %tobool4 to i32
  store i32 %conv5, i32* %left_in_image, align 4, !tbaa !2
  %16 = load i32, i32* %above_in_image, align 4, !tbaa !2
  %tobool6 = icmp ne i32 %16, 0
  br i1 %tobool6, label %land.lhs.true, label %if.end17

land.lhs.true:                                    ; preds = %entry
  %17 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !6
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %17)
  %tobool7 = icmp ne i32 %call, 0
  br i1 %tobool7, label %if.then, label %if.end17

if.then:                                          ; preds = %land.lhs.true
  %18 = load i8*, i8** %ref_counts, align 4, !tbaa !6
  %19 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %19, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %20 = load i8, i8* %arrayidx, align 4, !tbaa !8
  %idxprom = sext i8 %20 to i32
  %arrayidx8 = getelementptr inbounds i8, i8* %18, i32 %idxprom
  %21 = load i8, i8* %arrayidx8, align 1, !tbaa !8
  %inc = add i8 %21, 1
  store i8 %inc, i8* %arrayidx8, align 1, !tbaa !8
  %22 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !6
  %call9 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %22)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then
  %23 = load i8*, i8** %ref_counts, align 4, !tbaa !6
  %24 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 4, !tbaa !6
  %ref_frame12 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %24, i32 0, i32 12
  %arrayidx13 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame12, i32 0, i32 1
  %25 = load i8, i8* %arrayidx13, align 1, !tbaa !8
  %idxprom14 = sext i8 %25 to i32
  %arrayidx15 = getelementptr inbounds i8, i8* %23, i32 %idxprom14
  %26 = load i8, i8* %arrayidx15, align 1, !tbaa !8
  %inc16 = add i8 %26, 1
  store i8 %inc16, i8* %arrayidx15, align 1, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then
  br label %if.end17

if.end17:                                         ; preds = %if.end, %land.lhs.true, %entry
  %27 = load i32, i32* %left_in_image, align 4, !tbaa !2
  %tobool18 = icmp ne i32 %27, 0
  br i1 %tobool18, label %land.lhs.true19, label %if.end37

land.lhs.true19:                                  ; preds = %if.end17
  %28 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !6
  %call20 = call i32 @is_inter_block(%struct.MB_MODE_INFO* %28)
  %tobool21 = icmp ne i32 %call20, 0
  br i1 %tobool21, label %if.then22, label %if.end37

if.then22:                                        ; preds = %land.lhs.true19
  %29 = load i8*, i8** %ref_counts, align 4, !tbaa !6
  %30 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !6
  %ref_frame23 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %30, i32 0, i32 12
  %arrayidx24 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame23, i32 0, i32 0
  %31 = load i8, i8* %arrayidx24, align 4, !tbaa !8
  %idxprom25 = sext i8 %31 to i32
  %arrayidx26 = getelementptr inbounds i8, i8* %29, i32 %idxprom25
  %32 = load i8, i8* %arrayidx26, align 1, !tbaa !8
  %inc27 = add i8 %32, 1
  store i8 %inc27, i8* %arrayidx26, align 1, !tbaa !8
  %33 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !6
  %call28 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %33)
  %tobool29 = icmp ne i32 %call28, 0
  br i1 %tobool29, label %if.then30, label %if.end36

if.then30:                                        ; preds = %if.then22
  %34 = load i8*, i8** %ref_counts, align 4, !tbaa !6
  %35 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !6
  %ref_frame31 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %35, i32 0, i32 12
  %arrayidx32 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame31, i32 0, i32 1
  %36 = load i8, i8* %arrayidx32, align 1, !tbaa !8
  %idxprom33 = sext i8 %36 to i32
  %arrayidx34 = getelementptr inbounds i8, i8* %34, i32 %idxprom33
  %37 = load i8, i8* %arrayidx34, align 1, !tbaa !8
  %inc35 = add i8 %37, 1
  store i8 %inc35, i8* %arrayidx34, align 1, !tbaa !8
  br label %if.end36

if.end36:                                         ; preds = %if.then30, %if.then22
  br label %if.end37

if.end37:                                         ; preds = %if.end36, %land.lhs.true19, %if.end17
  %38 = bitcast i32* %left_in_image to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #6
  %39 = bitcast i32* %above_in_image to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  %40 = bitcast %struct.MB_MODE_INFO** %left_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #6
  %41 = bitcast %struct.MB_MODE_INFO** %above_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #6
  %42 = bitcast i8** %ref_counts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  ret void
}

; Function Attrs: nounwind
define internal void @read_ref_frames(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r, i32 %segment_id, i8* %ref_frame) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %segment_id.addr = alloca i32, align 4
  %ref_frame.addr = alloca i8*, align 4
  %mode = alloca i8, align 1
  %comp_ref_type = alloca i8, align 1
  %bit = alloca i32, align 4
  %bit1 = alloca i32, align 4
  %bit2 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %idx = alloca i32, align 4
  %bit53 = alloca i32, align 4
  %bit158 = alloca i32, align 4
  %bit265 = alloca i32, align 4
  %bit_bwd = alloca i32, align 4
  %bit1_bwd = alloca i32, align 4
  %bit0 = alloca i32, align 4
  %bit195 = alloca i32, align 4
  %bit5 = alloca i32, align 4
  %bit2110 = alloca i32, align 4
  %bit4 = alloca i32, align 4
  %bit3 = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !2
  store i8* %ref_frame, i8** %ref_frame.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 6
  %1 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %1, i32 0
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  %skip_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 26
  %bf.load = load i8, i8* %skip_mode, align 4
  %bf.lshr = lshr i8 %bf.load, 5
  %bf.clear = and i8 %bf.lshr, 1
  %tobool = icmp ne i8 %bf.clear, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %4 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  call void @set_ref_frames_for_skip_mode(%struct.AV1Common* %3, i8* %4)
  br label %if.end138

if.end:                                           ; preds = %entry
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 24
  %6 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call = call i32 @segfeature_active(%struct.segmentation* %seg, i32 %6, i8 zeroext 5)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg3 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 24
  %8 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call4 = call i32 @get_segdata(%struct.segmentation* %seg3, i32 %8, i8 zeroext 5)
  %conv = trunc i32 %call4 to i8
  %9 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i8, i8* %9, i32 0
  store i8 %conv, i8* %arrayidx5, align 1, !tbaa !8
  %10 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds i8, i8* %10, i32 1
  store i8 -1, i8* %arrayidx6, align 1, !tbaa !8
  br label %if.end138

if.else:                                          ; preds = %if.end
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 24
  %12 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call8 = call i32 @segfeature_active(%struct.segmentation* %seg7, i32 %12, i8 zeroext 6)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %if.then13, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seg10 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 24
  %14 = load i32, i32* %segment_id.addr, align 4, !tbaa !2
  %call11 = call i32 @segfeature_active(%struct.segmentation* %seg10, i32 %14, i8 zeroext 7)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.then13, label %if.else16

if.then13:                                        ; preds = %lor.lhs.false, %if.else
  %15 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds i8, i8* %15, i32 0
  store i8 1, i8* %arrayidx14, align 1, !tbaa !8
  %16 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds i8, i8* %16, i32 1
  store i8 -1, i8* %arrayidx15, align 1, !tbaa !8
  br label %if.end137

if.else16:                                        ; preds = %lor.lhs.false
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mode) #6
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %19 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call17 = call zeroext i8 @read_block_reference_mode(%struct.AV1Common* %17, %struct.macroblockd* %18, %struct.aom_reader* %19)
  store i8 %call17, i8* %mode, align 1, !tbaa !8
  %20 = load i8, i8* %mode, align 1, !tbaa !8
  %conv18 = zext i8 %20 to i32
  %cmp = icmp eq i32 %conv18, 1
  br i1 %cmp, label %if.then20, label %if.else86

if.then20:                                        ; preds = %if.else16
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %comp_ref_type) #6
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %22 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %call21 = call zeroext i8 @read_comp_reference_type(%struct.macroblockd* %21, %struct.aom_reader* %22)
  store i8 %call21, i8* %comp_ref_type, align 1, !tbaa !8
  %23 = load i8, i8* %comp_ref_type, align 1, !tbaa !8
  %conv22 = zext i8 %23 to i32
  %cmp23 = icmp eq i32 %conv22, 0
  br i1 %cmp23, label %if.then25, label %if.end52

if.then25:                                        ; preds = %if.then20
  %24 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %26 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call26 = call i16* @av1_get_pred_cdf_uni_comp_ref_p(%struct.macroblockd* %26)
  %call27 = call i32 @aom_read_symbol_(%struct.aom_reader* %25, i16* %call26, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call27, i32* %bit, align 4, !tbaa !2
  %27 = load i32, i32* %bit, align 4, !tbaa !2
  %tobool28 = icmp ne i32 %27, 0
  br i1 %tobool28, label %if.then29, label %if.else32

if.then29:                                        ; preds = %if.then25
  %28 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds i8, i8* %28, i32 0
  store i8 5, i8* %arrayidx30, align 1, !tbaa !8
  %29 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds i8, i8* %29, i32 1
  store i8 7, i8* %arrayidx31, align 1, !tbaa !8
  br label %if.end51

if.else32:                                        ; preds = %if.then25
  %30 = bitcast i32* %bit1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %32 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call33 = call i16* @av1_get_pred_cdf_uni_comp_ref_p1(%struct.macroblockd* %32)
  %call34 = call i32 @aom_read_symbol_(%struct.aom_reader* %31, i16* %call33, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call34, i32* %bit1, align 4, !tbaa !2
  %33 = load i32, i32* %bit1, align 4, !tbaa !2
  %tobool35 = icmp ne i32 %33, 0
  br i1 %tobool35, label %if.then36, label %if.else47

if.then36:                                        ; preds = %if.else32
  %34 = bitcast i32* %bit2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %35 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %36 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call37 = call i16* @av1_get_pred_cdf_uni_comp_ref_p2(%struct.macroblockd* %36)
  %call38 = call i32 @aom_read_symbol_(%struct.aom_reader* %35, i16* %call37, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call38, i32* %bit2, align 4, !tbaa !2
  %37 = load i32, i32* %bit2, align 4, !tbaa !2
  %tobool39 = icmp ne i32 %37, 0
  br i1 %tobool39, label %if.then40, label %if.else43

if.then40:                                        ; preds = %if.then36
  %38 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds i8, i8* %38, i32 0
  store i8 1, i8* %arrayidx41, align 1, !tbaa !8
  %39 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds i8, i8* %39, i32 1
  store i8 4, i8* %arrayidx42, align 1, !tbaa !8
  br label %if.end46

if.else43:                                        ; preds = %if.then36
  %40 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds i8, i8* %40, i32 0
  store i8 1, i8* %arrayidx44, align 1, !tbaa !8
  %41 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds i8, i8* %41, i32 1
  store i8 3, i8* %arrayidx45, align 1, !tbaa !8
  br label %if.end46

if.end46:                                         ; preds = %if.else43, %if.then40
  %42 = bitcast i32* %bit2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  br label %if.end50

if.else47:                                        ; preds = %if.else32
  %43 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds i8, i8* %43, i32 0
  store i8 1, i8* %arrayidx48, align 1, !tbaa !8
  %44 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx49 = getelementptr inbounds i8, i8* %44, i32 1
  store i8 2, i8* %arrayidx49, align 1, !tbaa !8
  br label %if.end50

if.end50:                                         ; preds = %if.else47, %if.end46
  %45 = bitcast i32* %bit1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.then29
  store i32 1, i32* %cleanup.dest.slot, align 4
  %46 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  br label %cleanup

if.end52:                                         ; preds = %if.then20
  %47 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  store i32 1, i32* %idx, align 4, !tbaa !2
  %48 = bitcast i32* %bit53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #6
  %49 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %50 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call54 = call i16* @av1_get_pred_cdf_comp_ref_p(%struct.macroblockd* %50)
  %call55 = call i32 @aom_read_symbol_(%struct.aom_reader* %49, i16* %call54, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call55, i32* %bit53, align 4, !tbaa !2
  %51 = load i32, i32* %bit53, align 4, !tbaa !2
  %tobool56 = icmp ne i32 %51, 0
  br i1 %tobool56, label %if.else64, label %if.then57

if.then57:                                        ; preds = %if.end52
  %52 = bitcast i32* %bit158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %54 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call59 = call i16* @av1_get_pred_cdf_comp_ref_p1(%struct.macroblockd* %54)
  %call60 = call i32 @aom_read_symbol_(%struct.aom_reader* %53, i16* %call59, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call60, i32* %bit158, align 4, !tbaa !2
  %55 = load i32, i32* %bit158, align 4, !tbaa !2
  %tobool61 = icmp ne i32 %55, 0
  %56 = zext i1 %tobool61 to i64
  %cond = select i1 %tobool61, i32 2, i32 1
  %conv62 = trunc i32 %cond to i8
  %57 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds i8, i8* %57, i32 0
  store i8 %conv62, i8* %arrayidx63, align 1, !tbaa !8
  %58 = bitcast i32* %bit158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  br label %if.end72

if.else64:                                        ; preds = %if.end52
  %59 = bitcast i32* %bit265 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #6
  %60 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %61 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call66 = call i16* @av1_get_pred_cdf_comp_ref_p2(%struct.macroblockd* %61)
  %call67 = call i32 @aom_read_symbol_(%struct.aom_reader* %60, i16* %call66, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call67, i32* %bit265, align 4, !tbaa !2
  %62 = load i32, i32* %bit265, align 4, !tbaa !2
  %tobool68 = icmp ne i32 %62, 0
  %63 = zext i1 %tobool68 to i64
  %cond69 = select i1 %tobool68, i32 4, i32 3
  %conv70 = trunc i32 %cond69 to i8
  %64 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx71 = getelementptr inbounds i8, i8* %64, i32 0
  store i8 %conv70, i8* %arrayidx71, align 1, !tbaa !8
  %65 = bitcast i32* %bit265 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  br label %if.end72

if.end72:                                         ; preds = %if.else64, %if.then57
  %66 = bitcast i32* %bit_bwd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  %67 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %68 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call73 = call i16* @av1_get_pred_cdf_comp_bwdref_p(%struct.macroblockd* %68)
  %call74 = call i32 @aom_read_symbol_(%struct.aom_reader* %67, i16* %call73, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call74, i32* %bit_bwd, align 4, !tbaa !2
  %69 = load i32, i32* %bit_bwd, align 4, !tbaa !2
  %tobool75 = icmp ne i32 %69, 0
  br i1 %tobool75, label %if.else83, label %if.then76

if.then76:                                        ; preds = %if.end72
  %70 = bitcast i32* %bit1_bwd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #6
  %71 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %72 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call77 = call i16* @av1_get_pred_cdf_comp_bwdref_p1(%struct.macroblockd* %72)
  %call78 = call i32 @aom_read_symbol_(%struct.aom_reader* %71, i16* %call77, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call78, i32* %bit1_bwd, align 4, !tbaa !2
  %73 = load i32, i32* %bit1_bwd, align 4, !tbaa !2
  %tobool79 = icmp ne i32 %73, 0
  %74 = zext i1 %tobool79 to i64
  %cond80 = select i1 %tobool79, i32 6, i32 5
  %conv81 = trunc i32 %cond80 to i8
  %75 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx82 = getelementptr inbounds i8, i8* %75, i32 1
  store i8 %conv81, i8* %arrayidx82, align 1, !tbaa !8
  %76 = bitcast i32* %bit1_bwd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  br label %if.end85

if.else83:                                        ; preds = %if.end72
  %77 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx84 = getelementptr inbounds i8, i8* %77, i32 1
  store i8 7, i8* %arrayidx84, align 1, !tbaa !8
  br label %if.end85

if.end85:                                         ; preds = %if.else83, %if.then76
  %78 = bitcast i32* %bit_bwd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #6
  %79 = bitcast i32* %bit53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #6
  %80 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end85, %if.end51
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %comp_ref_type) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup134 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end133

if.else86:                                        ; preds = %if.else16
  %81 = load i8, i8* %mode, align 1, !tbaa !8
  %conv87 = zext i8 %81 to i32
  %cmp88 = icmp eq i32 %conv87, 0
  br i1 %cmp88, label %if.then90, label %if.else131

if.then90:                                        ; preds = %if.else86
  %82 = bitcast i32* %bit0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #6
  %83 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %84 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call91 = call i16* @av1_get_pred_cdf_single_ref_p1(%struct.macroblockd* %84)
  %call92 = call i32 @aom_read_symbol_(%struct.aom_reader* %83, i16* %call91, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call92, i32* %bit0, align 4, !tbaa !2
  %85 = load i32, i32* %bit0, align 4, !tbaa !2
  %tobool93 = icmp ne i32 %85, 0
  br i1 %tobool93, label %if.then94, label %if.else109

if.then94:                                        ; preds = %if.then90
  %86 = bitcast i32* %bit195 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #6
  %87 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %88 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call96 = call i16* @av1_get_pred_cdf_single_ref_p2(%struct.macroblockd* %88)
  %call97 = call i32 @aom_read_symbol_(%struct.aom_reader* %87, i16* %call96, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call97, i32* %bit195, align 4, !tbaa !2
  %89 = load i32, i32* %bit195, align 4, !tbaa !2
  %tobool98 = icmp ne i32 %89, 0
  br i1 %tobool98, label %if.else106, label %if.then99

if.then99:                                        ; preds = %if.then94
  %90 = bitcast i32* %bit5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  %91 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %92 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call100 = call i16* @av1_get_pred_cdf_single_ref_p6(%struct.macroblockd* %92)
  %call101 = call i32 @aom_read_symbol_(%struct.aom_reader* %91, i16* %call100, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call101, i32* %bit5, align 4, !tbaa !2
  %93 = load i32, i32* %bit5, align 4, !tbaa !2
  %tobool102 = icmp ne i32 %93, 0
  %94 = zext i1 %tobool102 to i64
  %cond103 = select i1 %tobool102, i32 6, i32 5
  %conv104 = trunc i32 %cond103 to i8
  %95 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx105 = getelementptr inbounds i8, i8* %95, i32 0
  store i8 %conv104, i8* %arrayidx105, align 1, !tbaa !8
  %96 = bitcast i32* %bit5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #6
  br label %if.end108

if.else106:                                       ; preds = %if.then94
  %97 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx107 = getelementptr inbounds i8, i8* %97, i32 0
  store i8 7, i8* %arrayidx107, align 1, !tbaa !8
  br label %if.end108

if.end108:                                        ; preds = %if.else106, %if.then99
  %98 = bitcast i32* %bit195 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #6
  br label %if.end129

if.else109:                                       ; preds = %if.then90
  %99 = bitcast i32* %bit2110 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #6
  %100 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %101 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call111 = call i16* @av1_get_pred_cdf_single_ref_p3(%struct.macroblockd* %101)
  %call112 = call i32 @aom_read_symbol_(%struct.aom_reader* %100, i16* %call111, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call112, i32* %bit2110, align 4, !tbaa !2
  %102 = load i32, i32* %bit2110, align 4, !tbaa !2
  %tobool113 = icmp ne i32 %102, 0
  br i1 %tobool113, label %if.then114, label %if.else121

if.then114:                                       ; preds = %if.else109
  %103 = bitcast i32* %bit4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #6
  %104 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %105 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call115 = call i16* @av1_get_pred_cdf_single_ref_p5(%struct.macroblockd* %105)
  %call116 = call i32 @aom_read_symbol_(%struct.aom_reader* %104, i16* %call115, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call116, i32* %bit4, align 4, !tbaa !2
  %106 = load i32, i32* %bit4, align 4, !tbaa !2
  %tobool117 = icmp ne i32 %106, 0
  %107 = zext i1 %tobool117 to i64
  %cond118 = select i1 %tobool117, i32 4, i32 3
  %conv119 = trunc i32 %cond118 to i8
  %108 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx120 = getelementptr inbounds i8, i8* %108, i32 0
  store i8 %conv119, i8* %arrayidx120, align 1, !tbaa !8
  %109 = bitcast i32* %bit4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #6
  br label %if.end128

if.else121:                                       ; preds = %if.else109
  %110 = bitcast i32* %bit3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #6
  %111 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %112 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call122 = call i16* @av1_get_pred_cdf_single_ref_p4(%struct.macroblockd* %112)
  %call123 = call i32 @aom_read_symbol_(%struct.aom_reader* %111, i16* %call122, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_ref_frames, i32 0, i32 0))
  store i32 %call123, i32* %bit3, align 4, !tbaa !2
  %113 = load i32, i32* %bit3, align 4, !tbaa !2
  %tobool124 = icmp ne i32 %113, 0
  %114 = zext i1 %tobool124 to i64
  %cond125 = select i1 %tobool124, i32 2, i32 1
  %conv126 = trunc i32 %cond125 to i8
  %115 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx127 = getelementptr inbounds i8, i8* %115, i32 0
  store i8 %conv126, i8* %arrayidx127, align 1, !tbaa !8
  %116 = bitcast i32* %bit3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #6
  br label %if.end128

if.end128:                                        ; preds = %if.else121, %if.then114
  %117 = bitcast i32* %bit2110 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #6
  br label %if.end129

if.end129:                                        ; preds = %if.end128, %if.end108
  %118 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx130 = getelementptr inbounds i8, i8* %118, i32 1
  store i8 -1, i8* %arrayidx130, align 1, !tbaa !8
  %119 = bitcast i32* %bit0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #6
  br label %if.end132

if.else131:                                       ; preds = %if.else86
  br label %if.end132

if.end132:                                        ; preds = %if.else131, %if.end129
  br label %if.end133

if.end133:                                        ; preds = %if.end132, %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup134

cleanup134:                                       ; preds = %if.end133, %cleanup
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mode) #6
  %cleanup.dest135 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest135, label %unreachable [
    i32 0, label %cleanup.cont136
    i32 1, label %if.end138
  ]

cleanup.cont136:                                  ; preds = %cleanup134
  br label %if.end137

if.end137:                                        ; preds = %cleanup.cont136, %if.then13
  br label %if.end138

if.end138:                                        ; preds = %if.then, %cleanup134, %if.end137, %if.then2
  ret void

unreachable:                                      ; preds = %cleanup134
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32 @has_second_ref(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 1
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = sext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 0
  %conv1 = zext i1 %cmp to i32
  ret i32 %conv1
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @av1_ref_frame_type(i8* %rf) #2 {
entry:
  %retval = alloca i8, align 1
  %rf.addr = alloca i8*, align 4
  %uni_comp_ref_idx = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 1
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = sext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %uni_comp_ref_idx) #6
  %2 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %call = call signext i8 @get_uni_comp_ref_idx(i8* %2)
  store i8 %call, i8* %uni_comp_ref_idx, align 1, !tbaa !8
  %3 = load i8, i8* %uni_comp_ref_idx, align 1, !tbaa !8
  %conv2 = sext i8 %3 to i32
  %cmp3 = icmp sge i32 %conv2, 0
  br i1 %cmp3, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.then
  %4 = load i8, i8* %uni_comp_ref_idx, align 1, !tbaa !8
  %conv6 = sext i8 %4 to i32
  %add = add nsw i32 20, %conv6
  %conv7 = trunc i32 %add to i8
  store i8 %conv7, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.then
  %5 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds i8, i8* %5, i32 0
  %6 = load i8, i8* %arrayidx8, align 1, !tbaa !8
  %conv9 = sext i8 %6 to i32
  %sub = sub nsw i32 %conv9, 1
  %add10 = add nsw i32 8, %sub
  %7 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds i8, i8* %7, i32 1
  %8 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  %conv12 = sext i8 %8 to i32
  %sub13 = sub nsw i32 %conv12, 5
  %mul = mul nsw i32 %sub13, 4
  %add14 = add nsw i32 %add10, %mul
  %conv15 = trunc i32 %add14 to i8
  store i8 %conv15, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %uni_comp_ref_idx) #6
  br label %return

if.end:                                           ; preds = %entry
  %9 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds i8, i8* %9, i32 0
  %10 = load i8, i8* %arrayidx16, align 1, !tbaa !8
  store i8 %10, i8* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %cleanup
  %11 = load i8, i8* %retval, align 1
  ret i8 %11
}

; Function Attrs: inlinehint nounwind
define internal signext i16 @av1_mode_context_analyzer(i16* %mode_context, i8* %rf) #2 {
entry:
  %retval = alloca i16, align 2
  %mode_context.addr = alloca i16*, align 4
  %rf.addr = alloca i8*, align 4
  %ref_frame = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %newmv_ctx = alloca i16, align 2
  %refmv_ctx = alloca i16, align 2
  %comp_ctx = alloca i16, align 2
  store i16* %mode_context, i16** %mode_context.addr, align 4, !tbaa !6
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_frame) #6
  %0 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %call = call signext i8 @av1_ref_frame_type(i8* %0)
  store i8 %call, i8* %ref_frame, align 1, !tbaa !8
  %1 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 1
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = sext i8 %2 to i32
  %cmp = icmp sle i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i16*, i16** %mode_context.addr, align 4, !tbaa !6
  %4 = load i8, i8* %ref_frame, align 1, !tbaa !8
  %idxprom = sext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds i16, i16* %3, i32 %idxprom
  %5 = load i16, i16* %arrayidx2, align 2, !tbaa !101
  store i16 %5, i16* %retval, align 2
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %6 = bitcast i16* %newmv_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %6) #6
  %7 = load i16*, i16** %mode_context.addr, align 4, !tbaa !6
  %8 = load i8, i8* %ref_frame, align 1, !tbaa !8
  %idxprom3 = sext i8 %8 to i32
  %arrayidx4 = getelementptr inbounds i16, i16* %7, i32 %idxprom3
  %9 = load i16, i16* %arrayidx4, align 2, !tbaa !101
  %conv5 = sext i16 %9 to i32
  %and = and i32 %conv5, 7
  %conv6 = trunc i32 %and to i16
  store i16 %conv6, i16* %newmv_ctx, align 2, !tbaa !101
  %10 = bitcast i16* %refmv_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %10) #6
  %11 = load i16*, i16** %mode_context.addr, align 4, !tbaa !6
  %12 = load i8, i8* %ref_frame, align 1, !tbaa !8
  %idxprom7 = sext i8 %12 to i32
  %arrayidx8 = getelementptr inbounds i16, i16* %11, i32 %idxprom7
  %13 = load i16, i16* %arrayidx8, align 2, !tbaa !101
  %conv9 = sext i16 %13 to i32
  %shr = ashr i32 %conv9, 4
  %and10 = and i32 %shr, 15
  %conv11 = trunc i32 %and10 to i16
  store i16 %conv11, i16* %refmv_ctx, align 2, !tbaa !101
  %14 = bitcast i16* %comp_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %14) #6
  %15 = load i16, i16* %refmv_ctx, align 2, !tbaa !101
  %conv12 = sext i16 %15 to i32
  %shr13 = ashr i32 %conv12, 1
  %arrayidx14 = getelementptr inbounds [3 x [5 x i16]], [3 x [5 x i16]]* @compound_mode_ctx_map, i32 0, i32 %shr13
  %16 = load i16, i16* %newmv_ctx, align 2, !tbaa !101
  %conv15 = sext i16 %16 to i32
  %cmp16 = icmp slt i32 %conv15, 4
  br i1 %cmp16, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %17 = load i16, i16* %newmv_ctx, align 2, !tbaa !101
  %conv18 = sext i16 %17 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv18, %cond.true ], [ 4, %cond.false ]
  %arrayidx19 = getelementptr inbounds [5 x i16], [5 x i16]* %arrayidx14, i32 0, i32 %cond
  %18 = load i16, i16* %arrayidx19, align 2, !tbaa !101
  store i16 %18, i16* %comp_ctx, align 2, !tbaa !101
  %19 = load i16, i16* %comp_ctx, align 2, !tbaa !101
  store i16 %19, i16* %retval, align 2
  store i32 1, i32* %cleanup.dest.slot, align 4
  %20 = bitcast i16* %comp_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %20) #6
  %21 = bitcast i16* %refmv_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %21) #6
  %22 = bitcast i16* %newmv_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %22) #6
  br label %cleanup

cleanup:                                          ; preds = %cond.end, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_frame) #6
  %23 = load i16, i16* %retval, align 2
  ret i16 %23
}

; Function Attrs: nounwind
define internal zeroext i8 @read_inter_compound_mode(%struct.macroblockd* %xd, %struct.aom_reader* %r, i16 signext %ctx) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %ctx.addr = alloca i16, align 2
  %mode = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i16 %ctx, i16* %ctx.addr, align 2, !tbaa !101
  %0 = bitcast i32* %mode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 40
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %inter_compound_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %3, i32 0, i32 17
  %4 = load i16, i16* %ctx.addr, align 2, !tbaa !101
  %idxprom = sext i16 %4 to i32
  %arrayidx = getelementptr inbounds [8 x [9 x i16]], [8 x [9 x i16]]* %inter_compound_mode_cdf, i32 0, i32 %idxprom
  %arraydecay = getelementptr inbounds [9 x i16], [9 x i16]* %arrayidx, i32 0, i32 0
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %1, i16* %arraydecay, i32 8, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @__func__.read_inter_compound_mode, i32 0, i32 0))
  store i32 %call, i32* %mode, align 4, !tbaa !2
  %5 = load i32, i32* %mode, align 4, !tbaa !2
  %add = add nsw i32 17, %5
  %conv = trunc i32 %add to i8
  %6 = bitcast i32* %mode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret i8 %conv
}

; Function Attrs: nounwind
define internal zeroext i8 @read_inter_mode(%struct.frame_contexts* %ec_ctx, %struct.aom_reader* %r, i16 signext %ctx) #0 {
entry:
  %retval = alloca i8, align 1
  %ec_ctx.addr = alloca %struct.frame_contexts*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %ctx.addr = alloca i16, align 2
  %mode_ctx = alloca i16, align 2
  %is_newmv = alloca i32, align 4
  %is_zeromv = alloca i32, align 4
  %is_refmv = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.frame_contexts* %ec_ctx, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i16 %ctx, i16* %ctx.addr, align 2, !tbaa !101
  %0 = bitcast i16* %mode_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #6
  %1 = load i16, i16* %ctx.addr, align 2, !tbaa !101
  %conv = sext i16 %1 to i32
  %and = and i32 %conv, 7
  %conv1 = trunc i32 %and to i16
  store i16 %conv1, i16* %mode_ctx, align 2, !tbaa !101
  %2 = bitcast i32* %is_newmv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %is_zeromv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = bitcast i32* %is_refmv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %6 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  %newmv_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %6, i32 0, i32 13
  %7 = load i16, i16* %mode_ctx, align 2, !tbaa !101
  %idxprom = sext i16 %7 to i32
  %arrayidx = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %newmv_cdf, i32 0, i32 %idxprom
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx, i32 0, i32 0
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %5, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_inter_mode, i32 0, i32 0))
  %cmp = icmp eq i32 %call, 0
  %conv2 = zext i1 %cmp to i32
  store i32 %conv2, i32* %is_newmv, align 4, !tbaa !2
  %8 = load i32, i32* %is_newmv, align 4, !tbaa !2
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8 16, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %9 = load i16, i16* %ctx.addr, align 2, !tbaa !101
  %conv3 = sext i16 %9 to i32
  %shr = ashr i32 %conv3, 3
  %and4 = and i32 %shr, 1
  %conv5 = trunc i32 %and4 to i16
  store i16 %conv5, i16* %mode_ctx, align 2, !tbaa !101
  %10 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %11 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  %zeromv_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %11, i32 0, i32 14
  %12 = load i16, i16* %mode_ctx, align 2, !tbaa !101
  %idxprom6 = sext i16 %12 to i32
  %arrayidx7 = getelementptr inbounds [2 x [3 x i16]], [2 x [3 x i16]]* %zeromv_cdf, i32 0, i32 %idxprom6
  %arraydecay8 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx7, i32 0, i32 0
  %call9 = call i32 @aom_read_symbol_(%struct.aom_reader* %10, i16* %arraydecay8, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_inter_mode, i32 0, i32 0))
  %cmp10 = icmp eq i32 %call9, 0
  %conv11 = zext i1 %cmp10 to i32
  store i32 %conv11, i32* %is_zeromv, align 4, !tbaa !2
  %13 = load i32, i32* %is_zeromv, align 4, !tbaa !2
  %tobool12 = icmp ne i32 %13, 0
  br i1 %tobool12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.end
  store i8 15, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.end
  %14 = load i16, i16* %ctx.addr, align 2, !tbaa !101
  %conv15 = sext i16 %14 to i32
  %shr16 = ashr i32 %conv15, 4
  %and17 = and i32 %shr16, 15
  %conv18 = trunc i32 %and17 to i16
  store i16 %conv18, i16* %mode_ctx, align 2, !tbaa !101
  %15 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %16 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  %refmv_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %16, i32 0, i32 15
  %17 = load i16, i16* %mode_ctx, align 2, !tbaa !101
  %idxprom19 = sext i16 %17 to i32
  %arrayidx20 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %refmv_cdf, i32 0, i32 %idxprom19
  %arraydecay21 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx20, i32 0, i32 0
  %call22 = call i32 @aom_read_symbol_(%struct.aom_reader* %15, i16* %arraydecay21, i32 2, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @__func__.read_inter_mode, i32 0, i32 0))
  %cmp23 = icmp eq i32 %call22, 0
  %conv24 = zext i1 %cmp23 to i32
  store i32 %conv24, i32* %is_refmv, align 4, !tbaa !2
  %18 = load i32, i32* %is_refmv, align 4, !tbaa !2
  %tobool25 = icmp ne i32 %18, 0
  br i1 %tobool25, label %if.then26, label %if.else

if.then26:                                        ; preds = %if.end14
  store i8 13, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.end14
  store i8 14, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then26, %if.then13, %if.then
  %19 = bitcast i32* %is_refmv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = bitcast i32* %is_zeromv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = bitcast i32* %is_newmv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast i16* %mode_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %22) #6
  %23 = load i8, i8* %retval, align 1
  ret i8 %23
}

; Function Attrs: inlinehint nounwind
define internal i32 @have_nearmv_in_inter_mode(i8 zeroext %mode) #2 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 14
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp eq i32 %conv2, 18
  br i1 %cmp3, label %lor.end, label %lor.lhs.false5

lor.lhs.false5:                                   ; preds = %lor.lhs.false
  %2 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv6 = zext i8 %2 to i32
  %cmp7 = icmp eq i32 %conv6, 21
  br i1 %cmp7, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %lor.lhs.false5
  %3 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv9 = zext i8 %3 to i32
  %cmp10 = icmp eq i32 %conv9, 22
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %lor.lhs.false5, %lor.lhs.false, %entry
  %4 = phi i1 [ true, %lor.lhs.false5 ], [ true, %lor.lhs.false ], [ true, %entry ], [ %cmp10, %lor.rhs ]
  %lor.ext = zext i1 %4 to i32
  ret i32 %lor.ext
}

; Function Attrs: nounwind
define internal void @read_drl_idx(%struct.frame_contexts* %ec_ctx, %struct.macroblockd* %xd, %struct.MB_MODE_INFO* %mbmi, %struct.aom_reader* %r) #0 {
entry:
  %ec_ctx.addr = alloca %struct.frame_contexts*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %ref_frame_type = alloca i8, align 1
  %idx = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %drl_ctx = alloca i8, align 1
  %drl_idx = alloca i32, align 4
  %idx35 = alloca i32, align 4
  %drl_ctx49 = alloca i8, align 1
  %drl_idx55 = alloca i32, align 4
  store %struct.frame_contexts* %ec_ctx, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_frame_type) #6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 12
  %arraydecay = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %call = call signext i8 @av1_ref_frame_type(i8* %arraydecay)
  store i8 %call, i8* %ref_frame_type, align 1, !tbaa !8
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 27
  %bf.load = load i8, i8* %ref_mv_idx, align 1
  %bf.clear = and i8 %bf.load, -4
  store i8 %bf.clear, i8* %ref_mv_idx, align 1
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 7
  %3 = load i8, i8* %mode, align 1, !tbaa !50
  %conv = zext i8 %3 to i32
  %cmp = icmp eq i32 %conv, 16
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %4, i32 0, i32 7
  %5 = load i8, i8* %mode2, align 1, !tbaa !50
  %conv3 = zext i8 %5 to i32
  %cmp4 = icmp eq i32 %conv3, 24
  br i1 %cmp4, label %if.then, label %if.end30

if.then:                                          ; preds = %lor.lhs.false, %entry
  %6 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 0, i32* %idx, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %7 = load i32, i32* %idx, align 4, !tbaa !2
  %cmp6 = icmp slt i32 %7, 2
  br i1 %cmp6, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

for.body:                                         ; preds = %for.cond
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_count = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 35
  %9 = load i8, i8* %ref_frame_type, align 1, !tbaa !8
  %idxprom = zext i8 %9 to i32
  %arrayidx = getelementptr inbounds [29 x i8], [29 x i8]* %ref_mv_count, i32 0, i32 %idxprom
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv8 = zext i8 %10 to i32
  %11 = load i32, i32* %idx, align 4, !tbaa !2
  %add = add nsw i32 %11, 1
  %cmp9 = icmp sgt i32 %conv8, %add
  br i1 %cmp9, label %if.then11, label %if.end27

if.then11:                                        ; preds = %for.body
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %drl_ctx) #6
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %weight = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %12, i32 0, i32 37
  %13 = load i8, i8* %ref_frame_type, align 1, !tbaa !8
  %idxprom12 = zext i8 %13 to i32
  %arrayidx13 = getelementptr inbounds [29 x [8 x i16]], [29 x [8 x i16]]* %weight, i32 0, i32 %idxprom12
  %arraydecay14 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx13, i32 0, i32 0
  %14 = load i32, i32* %idx, align 4, !tbaa !2
  %call15 = call zeroext i8 @av1_drl_ctx(i16* %arraydecay14, i32 %14)
  store i8 %call15, i8* %drl_ctx, align 1, !tbaa !8
  %15 = bitcast i32* %drl_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %17 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  %drl_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %17, i32 0, i32 16
  %18 = load i8, i8* %drl_ctx, align 1, !tbaa !8
  %idxprom16 = zext i8 %18 to i32
  %arrayidx17 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %drl_cdf, i32 0, i32 %idxprom16
  %arraydecay18 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx17, i32 0, i32 0
  %call19 = call i32 @aom_read_symbol_(%struct.aom_reader* %16, i16* %arraydecay18, i32 2, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @__func__.read_drl_idx, i32 0, i32 0))
  store i32 %call19, i32* %drl_idx, align 4, !tbaa !2
  %19 = load i32, i32* %idx, align 4, !tbaa !2
  %20 = load i32, i32* %drl_idx, align 4, !tbaa !2
  %add20 = add nsw i32 %19, %20
  %conv21 = trunc i32 %add20 to i8
  %21 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx22 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %21, i32 0, i32 27
  %bf.load23 = load i8, i8* %ref_mv_idx22, align 1
  %bf.value = and i8 %conv21, 3
  %bf.clear24 = and i8 %bf.load23, -4
  %bf.set = or i8 %bf.clear24, %bf.value
  store i8 %bf.set, i8* %ref_mv_idx22, align 1
  %22 = load i32, i32* %drl_idx, align 4, !tbaa !2
  %tobool = icmp ne i32 %22, 0
  br i1 %tobool, label %if.end, label %if.then25

if.then25:                                        ; preds = %if.then11
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then11
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then25
  %23 = bitcast i32* %drl_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %drl_ctx) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup28 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end27

if.end27:                                         ; preds = %cleanup.cont, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end27
  %24 = load i32, i32* %idx, align 4, !tbaa !2
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !2
  br label %for.cond

cleanup28:                                        ; preds = %cleanup, %for.cond.cleanup
  %25 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #6
  %cleanup.dest29 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest29, label %cleanup82 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup28
  br label %if.end30

if.end30:                                         ; preds = %for.end, %lor.lhs.false
  %26 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode31 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %26, i32 0, i32 7
  %27 = load i8, i8* %mode31, align 1, !tbaa !50
  %call32 = call i32 @have_nearmv_in_inter_mode(i8 zeroext %27)
  %tobool33 = icmp ne i32 %call32, 0
  br i1 %tobool33, label %if.then34, label %if.end81

if.then34:                                        ; preds = %if.end30
  %28 = bitcast i32* %idx35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  store i32 1, i32* %idx35, align 4, !tbaa !2
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc76, %if.then34
  %29 = load i32, i32* %idx35, align 4, !tbaa !2
  %cmp37 = icmp slt i32 %29, 3
  br i1 %cmp37, label %for.body40, label %for.cond.cleanup39

for.cond.cleanup39:                               ; preds = %for.cond36
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup78

for.body40:                                       ; preds = %for.cond36
  %30 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %ref_mv_count41 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %30, i32 0, i32 35
  %31 = load i8, i8* %ref_frame_type, align 1, !tbaa !8
  %idxprom42 = zext i8 %31 to i32
  %arrayidx43 = getelementptr inbounds [29 x i8], [29 x i8]* %ref_mv_count41, i32 0, i32 %idxprom42
  %32 = load i8, i8* %arrayidx43, align 1, !tbaa !8
  %conv44 = zext i8 %32 to i32
  %33 = load i32, i32* %idx35, align 4, !tbaa !2
  %add45 = add nsw i32 %33, 1
  %cmp46 = icmp sgt i32 %conv44, %add45
  br i1 %cmp46, label %if.then48, label %if.end75

if.then48:                                        ; preds = %for.body40
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %drl_ctx49) #6
  %34 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %weight50 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %34, i32 0, i32 37
  %35 = load i8, i8* %ref_frame_type, align 1, !tbaa !8
  %idxprom51 = zext i8 %35 to i32
  %arrayidx52 = getelementptr inbounds [29 x [8 x i16]], [29 x [8 x i16]]* %weight50, i32 0, i32 %idxprom51
  %arraydecay53 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx52, i32 0, i32 0
  %36 = load i32, i32* %idx35, align 4, !tbaa !2
  %call54 = call zeroext i8 @av1_drl_ctx(i16* %arraydecay53, i32 %36)
  store i8 %call54, i8* %drl_ctx49, align 1, !tbaa !8
  %37 = bitcast i32* %drl_idx55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #6
  %38 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %39 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx.addr, align 4, !tbaa !6
  %drl_cdf56 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %39, i32 0, i32 16
  %40 = load i8, i8* %drl_ctx49, align 1, !tbaa !8
  %idxprom57 = zext i8 %40 to i32
  %arrayidx58 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %drl_cdf56, i32 0, i32 %idxprom57
  %arraydecay59 = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx58, i32 0, i32 0
  %call60 = call i32 @aom_read_symbol_(%struct.aom_reader* %38, i16* %arraydecay59, i32 2, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @__func__.read_drl_idx, i32 0, i32 0))
  store i32 %call60, i32* %drl_idx55, align 4, !tbaa !2
  %41 = load i32, i32* %idx35, align 4, !tbaa !2
  %42 = load i32, i32* %drl_idx55, align 4, !tbaa !2
  %add61 = add nsw i32 %41, %42
  %sub = sub nsw i32 %add61, 1
  %conv62 = trunc i32 %sub to i8
  %43 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_mv_idx63 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %43, i32 0, i32 27
  %bf.load64 = load i8, i8* %ref_mv_idx63, align 1
  %bf.value65 = and i8 %conv62, 3
  %bf.clear66 = and i8 %bf.load64, -4
  %bf.set67 = or i8 %bf.clear66, %bf.value65
  store i8 %bf.set67, i8* %ref_mv_idx63, align 1
  %44 = load i32, i32* %drl_idx55, align 4, !tbaa !2
  %tobool68 = icmp ne i32 %44, 0
  br i1 %tobool68, label %if.end70, label %if.then69

if.then69:                                        ; preds = %if.then48
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup71

if.end70:                                         ; preds = %if.then48
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup71

cleanup71:                                        ; preds = %if.end70, %if.then69
  %45 = bitcast i32* %drl_idx55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %drl_ctx49) #6
  %cleanup.dest73 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest73, label %cleanup78 [
    i32 0, label %cleanup.cont74
  ]

cleanup.cont74:                                   ; preds = %cleanup71
  br label %if.end75

if.end75:                                         ; preds = %cleanup.cont74, %for.body40
  br label %for.inc76

for.inc76:                                        ; preds = %if.end75
  %46 = load i32, i32* %idx35, align 4, !tbaa !2
  %inc77 = add nsw i32 %46, 1
  store i32 %inc77, i32* %idx35, align 4, !tbaa !2
  br label %for.cond36

cleanup78:                                        ; preds = %cleanup71, %for.cond.cleanup39
  %47 = bitcast i32* %idx35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  %cleanup.dest79 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest79, label %cleanup82 [
    i32 5, label %for.end80
  ]

for.end80:                                        ; preds = %cleanup78
  br label %if.end81

if.end81:                                         ; preds = %for.end80, %if.end30
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup82

cleanup82:                                        ; preds = %if.end81, %cleanup78, %cleanup28
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_frame_type) #6
  %cleanup.dest83 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest83, label %unreachable [
    i32 0, label %cleanup.cont84
    i32 1, label %cleanup.cont84
  ]

cleanup.cont84:                                   ; preds = %cleanup82, %cleanup82
  ret void

unreachable:                                      ; preds = %cleanup82
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_compound_mode(i8 zeroext %mode) #2 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 17
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp slt i32 %conv2, 25
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  %land.ext = zext i1 %2 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal void @lower_mv_precision(%struct.mv* %mv, i32 %allow_hp, i32 %is_integer) #2 {
entry:
  %mv.addr = alloca %struct.mv*, align 4
  %allow_hp.addr = alloca i32, align 4
  %is_integer.addr = alloca i32, align 4
  store %struct.mv* %mv, %struct.mv** %mv.addr, align 4, !tbaa !6
  store i32 %allow_hp, i32* %allow_hp.addr, align 4, !tbaa !2
  store i32 %is_integer, i32* %is_integer.addr, align 4, !tbaa !2
  %0 = load i32, i32* %is_integer.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  call void @integer_mv_precision(%struct.mv* %1)
  br label %if.end26

if.else:                                          ; preds = %entry
  %2 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %tobool1 = icmp ne i32 %2, 0
  br i1 %tobool1, label %if.end25, label %if.then2

if.then2:                                         ; preds = %if.else
  %3 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row = getelementptr inbounds %struct.mv, %struct.mv* %3, i32 0, i32 0
  %4 = load i16, i16* %row, align 2, !tbaa !143
  %conv = sext i16 %4 to i32
  %and = and i32 %conv, 1
  %tobool3 = icmp ne i32 %and, 0
  br i1 %tobool3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then2
  %5 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row5 = getelementptr inbounds %struct.mv, %struct.mv* %5, i32 0, i32 0
  %6 = load i16, i16* %row5, align 2, !tbaa !143
  %conv6 = sext i16 %6 to i32
  %cmp = icmp sgt i32 %conv6, 0
  %7 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 -1, i32 1
  %8 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row8 = getelementptr inbounds %struct.mv, %struct.mv* %8, i32 0, i32 0
  %9 = load i16, i16* %row8, align 2, !tbaa !143
  %conv9 = sext i16 %9 to i32
  %add = add nsw i32 %conv9, %cond
  %conv10 = trunc i32 %add to i16
  store i16 %conv10, i16* %row8, align 2, !tbaa !143
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then2
  %10 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col = getelementptr inbounds %struct.mv, %struct.mv* %10, i32 0, i32 1
  %11 = load i16, i16* %col, align 2, !tbaa !146
  %conv11 = sext i16 %11 to i32
  %and12 = and i32 %conv11, 1
  %tobool13 = icmp ne i32 %and12, 0
  br i1 %tobool13, label %if.then14, label %if.end24

if.then14:                                        ; preds = %if.end
  %12 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col15 = getelementptr inbounds %struct.mv, %struct.mv* %12, i32 0, i32 1
  %13 = load i16, i16* %col15, align 2, !tbaa !146
  %conv16 = sext i16 %13 to i32
  %cmp17 = icmp sgt i32 %conv16, 0
  %14 = zext i1 %cmp17 to i64
  %cond19 = select i1 %cmp17, i32 -1, i32 1
  %15 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col20 = getelementptr inbounds %struct.mv, %struct.mv* %15, i32 0, i32 1
  %16 = load i16, i16* %col20, align 2, !tbaa !146
  %conv21 = sext i16 %16 to i32
  %add22 = add nsw i32 %conv21, %cond19
  %conv23 = trunc i32 %add22 to i16
  store i16 %conv23, i16* %col20, align 2, !tbaa !146
  br label %if.end24

if.end24:                                         ; preds = %if.then14, %if.end
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.else
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @compound_ref0_mode(i8 zeroext %mode) #2 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [25 x i8], [25 x i8]* @compound_ref0_mode.lut, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  ret i8 %1
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @compound_ref1_mode(i8 zeroext %mode) #2 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [25 x i8], [25 x i8]* @compound_ref1_mode.lut, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  ret i8 %1
}

; Function Attrs: inlinehint nounwind
define internal i32 @assign_mv(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i8 zeroext %mode, i8* %ref_frame, %union.int_mv* %mv, %union.int_mv* %ref_mv, %union.int_mv* %nearest_mv, %union.int_mv* %near_mv, i32 %is_compound, i32 %allow_hp, %struct.aom_reader* %r) #2 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mode.addr = alloca i8, align 1
  %ref_frame.addr = alloca i8*, align 4
  %mv.addr = alloca %union.int_mv*, align 4
  %ref_mv.addr = alloca %union.int_mv*, align 4
  %nearest_mv.addr = alloca %union.int_mv*, align 4
  %near_mv.addr = alloca %union.int_mv*, align 4
  %is_compound.addr = alloca i32, align 4
  %allow_hp.addr = alloca i32, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %bsize = alloca i8, align 1
  %features = alloca %struct.FeatureFlags*, align 4
  %nmvc = alloca %struct.nmv_context*, align 4
  %tmp = alloca %union.int_mv, align 4
  %i = alloca i32, align 4
  %nmvc29 = alloca %struct.nmv_context*, align 4
  %nmvc55 = alloca %struct.nmv_context*, align 4
  %nmvc67 = alloca %struct.nmv_context*, align 4
  %nmvc79 = alloca %struct.nmv_context*, align 4
  %nmvc91 = alloca %struct.nmv_context*, align 4
  %tmp115 = alloca %union.int_mv, align 4
  %tmp131 = alloca %union.int_mv, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ret = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  store i8* %ref_frame, i8** %ref_frame.addr, align 4, !tbaa !6
  store %union.int_mv* %mv, %union.int_mv** %mv.addr, align 4, !tbaa !6
  store %union.int_mv* %ref_mv, %union.int_mv** %ref_mv.addr, align 4, !tbaa !6
  store %union.int_mv* %nearest_mv, %union.int_mv** %nearest_mv.addr, align 4, !tbaa !6
  store %union.int_mv* %near_mv, %union.int_mv** %near_mv.addr, align 4, !tbaa !6
  store i32 %is_compound, i32* %is_compound.addr, align 4, !tbaa !2
  store i32 %allow_hp, i32* %allow_hp.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 40
  %2 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %2, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %3 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 6
  %5 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %5, i32 0
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %6, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 6
  %8 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %8, i8* %bsize, align 1, !tbaa !8
  %9 = bitcast %struct.FeatureFlags** %features to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 21
  store %struct.FeatureFlags* %features1, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %11 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %cur_frame_force_integer_mv = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %11, i32 0, i32 2
  %12 = load i8, i8* %cur_frame_force_integer_mv, align 2, !tbaa !156, !range !46
  %tobool = trunc i8 %12 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %allow_hp.addr, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv = zext i8 %13 to i32
  switch i32 %conv, label %sw.default [
    i32 16, label %sw.bb
    i32 13, label %sw.bb7
    i32 14, label %sw.bb11
    i32 15, label %sw.bb16
    i32 24, label %sw.bb27
    i32 17, label %sw.bb36
    i32 18, label %sw.bb45
    i32 20, label %sw.bb54
    i32 19, label %sw.bb66
    i32 21, label %sw.bb78
    i32 22, label %sw.bb90
    i32 23, label %sw.bb102
  ]

sw.bb:                                            ; preds = %if.end
  %14 = bitcast %struct.nmv_context** %nmvc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %nmvc2 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %15, i32 0, i32 43
  store %struct.nmv_context* %nmvc2, %struct.nmv_context** %nmvc, align 4, !tbaa !6
  %16 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %17 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %union.int_mv, %union.int_mv* %17, i32 0
  %as_mv = bitcast %union.int_mv* %arrayidx3 to %struct.mv*
  %18 = load %union.int_mv*, %union.int_mv** %ref_mv.addr, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds %union.int_mv, %union.int_mv* %18, i32 0
  %as_mv5 = bitcast %union.int_mv* %arrayidx4 to %struct.mv*
  %19 = load %struct.nmv_context*, %struct.nmv_context** %nmvc, align 4, !tbaa !6
  %20 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %conv6 = trunc i32 %20 to i8
  call void @read_mv(%struct.aom_reader* %16, %struct.mv* %as_mv, %struct.mv* %as_mv5, %struct.nmv_context* %19, i8 signext %conv6)
  %21 = bitcast %struct.nmv_context** %nmvc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  br label %sw.epilog

sw.bb7:                                           ; preds = %if.end
  %22 = load %union.int_mv*, %union.int_mv** %nearest_mv.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds %union.int_mv, %union.int_mv* %22, i32 0
  %as_int = bitcast %union.int_mv* %arrayidx8 to i32*
  %23 = load i32, i32* %as_int, align 4, !tbaa !8
  %24 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds %union.int_mv, %union.int_mv* %24, i32 0
  %as_int10 = bitcast %union.int_mv* %arrayidx9 to i32*
  store i32 %23, i32* %as_int10, align 4, !tbaa !8
  br label %sw.epilog

sw.bb11:                                          ; preds = %if.end
  %25 = load %union.int_mv*, %union.int_mv** %near_mv.addr, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %union.int_mv, %union.int_mv* %25, i32 0
  %as_int13 = bitcast %union.int_mv* %arrayidx12 to i32*
  %26 = load i32, i32* %as_int13, align 4, !tbaa !8
  %27 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds %union.int_mv, %union.int_mv* %27, i32 0
  %as_int15 = bitcast %union.int_mv* %arrayidx14 to i32*
  store i32 %26, i32* %as_int15, align 4, !tbaa !8
  br label %sw.epilog

sw.bb16:                                          ; preds = %if.end
  %28 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %global_motion = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %28, i32 0, i32 36
  %29 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds i8, i8* %29, i32 0
  %30 = load i8, i8* %arrayidx17, align 1, !tbaa !8
  %idxprom = sext i8 %30 to i32
  %arrayidx18 = getelementptr inbounds [8 x %struct.WarpedMotionParams], [8 x %struct.WarpedMotionParams]* %global_motion, i32 0, i32 %idxprom
  %31 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %allow_high_precision_mv = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %31, i32 0, i32 1
  %32 = load i8, i8* %allow_high_precision_mv, align 1, !tbaa !155, !range !46
  %tobool19 = trunc i8 %32 to i1
  %conv20 = zext i1 %tobool19 to i32
  %33 = load i8, i8* %bsize, align 1, !tbaa !8
  %34 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %34, i32 0, i32 1
  %35 = load i32, i32* %mi_col, align 4, !tbaa !72
  %36 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %36, i32 0, i32 0
  %37 = load i32, i32* %mi_row, align 16, !tbaa !71
  %38 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %cur_frame_force_integer_mv21 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %38, i32 0, i32 2
  %39 = load i8, i8* %cur_frame_force_integer_mv21, align 2, !tbaa !156, !range !46
  %tobool22 = trunc i8 %39 to i1
  %conv23 = zext i1 %tobool22 to i32
  call void @gm_get_motion_vector(%union.int_mv* sret align 4 %tmp, %struct.WarpedMotionParams* %arrayidx18, i32 %conv20, i8 zeroext %33, i32 %35, i32 %37, i32 %conv23)
  %as_int24 = bitcast %union.int_mv* %tmp to i32*
  %40 = load i32, i32* %as_int24, align 4, !tbaa !8
  %41 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds %union.int_mv, %union.int_mv* %41, i32 0
  %as_int26 = bitcast %union.int_mv* %arrayidx25 to i32*
  store i32 %40, i32* %as_int26, align 4, !tbaa !8
  br label %sw.epilog

sw.bb27:                                          ; preds = %if.end
  %42 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.bb27
  %43 = load i32, i32* %i, align 4, !tbaa !2
  %cmp = icmp slt i32 %43, 2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %44 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %45 = bitcast %struct.nmv_context** %nmvc29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #6
  %46 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %nmvc30 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %46, i32 0, i32 43
  store %struct.nmv_context* %nmvc30, %struct.nmv_context** %nmvc29, align 4, !tbaa !6
  %47 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %48 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %49 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds %union.int_mv, %union.int_mv* %48, i32 %49
  %as_mv32 = bitcast %union.int_mv* %arrayidx31 to %struct.mv*
  %50 = load %union.int_mv*, %union.int_mv** %ref_mv.addr, align 4, !tbaa !6
  %51 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds %union.int_mv, %union.int_mv* %50, i32 %51
  %as_mv34 = bitcast %union.int_mv* %arrayidx33 to %struct.mv*
  %52 = load %struct.nmv_context*, %struct.nmv_context** %nmvc29, align 4, !tbaa !6
  %53 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %conv35 = trunc i32 %53 to i8
  call void @read_mv(%struct.aom_reader* %47, %struct.mv* %as_mv32, %struct.mv* %as_mv34, %struct.nmv_context* %52, i8 signext %conv35)
  %54 = bitcast %struct.nmv_context** %nmvc29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %55 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %55, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %sw.epilog

sw.bb36:                                          ; preds = %if.end
  %56 = load %union.int_mv*, %union.int_mv** %nearest_mv.addr, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds %union.int_mv, %union.int_mv* %56, i32 0
  %as_int38 = bitcast %union.int_mv* %arrayidx37 to i32*
  %57 = load i32, i32* %as_int38, align 4, !tbaa !8
  %58 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds %union.int_mv, %union.int_mv* %58, i32 0
  %as_int40 = bitcast %union.int_mv* %arrayidx39 to i32*
  store i32 %57, i32* %as_int40, align 4, !tbaa !8
  %59 = load %union.int_mv*, %union.int_mv** %nearest_mv.addr, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds %union.int_mv, %union.int_mv* %59, i32 1
  %as_int42 = bitcast %union.int_mv* %arrayidx41 to i32*
  %60 = load i32, i32* %as_int42, align 4, !tbaa !8
  %61 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds %union.int_mv, %union.int_mv* %61, i32 1
  %as_int44 = bitcast %union.int_mv* %arrayidx43 to i32*
  store i32 %60, i32* %as_int44, align 4, !tbaa !8
  br label %sw.epilog

sw.bb45:                                          ; preds = %if.end
  %62 = load %union.int_mv*, %union.int_mv** %near_mv.addr, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds %union.int_mv, %union.int_mv* %62, i32 0
  %as_int47 = bitcast %union.int_mv* %arrayidx46 to i32*
  %63 = load i32, i32* %as_int47, align 4, !tbaa !8
  %64 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds %union.int_mv, %union.int_mv* %64, i32 0
  %as_int49 = bitcast %union.int_mv* %arrayidx48 to i32*
  store i32 %63, i32* %as_int49, align 4, !tbaa !8
  %65 = load %union.int_mv*, %union.int_mv** %near_mv.addr, align 4, !tbaa !6
  %arrayidx50 = getelementptr inbounds %union.int_mv, %union.int_mv* %65, i32 1
  %as_int51 = bitcast %union.int_mv* %arrayidx50 to i32*
  %66 = load i32, i32* %as_int51, align 4, !tbaa !8
  %67 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx52 = getelementptr inbounds %union.int_mv, %union.int_mv* %67, i32 1
  %as_int53 = bitcast %union.int_mv* %arrayidx52 to i32*
  store i32 %66, i32* %as_int53, align 4, !tbaa !8
  br label %sw.epilog

sw.bb54:                                          ; preds = %if.end
  %68 = bitcast %struct.nmv_context** %nmvc55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #6
  %69 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %nmvc56 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %69, i32 0, i32 43
  store %struct.nmv_context* %nmvc56, %struct.nmv_context** %nmvc55, align 4, !tbaa !6
  %70 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %71 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds %union.int_mv, %union.int_mv* %71, i32 0
  %as_mv58 = bitcast %union.int_mv* %arrayidx57 to %struct.mv*
  %72 = load %union.int_mv*, %union.int_mv** %ref_mv.addr, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds %union.int_mv, %union.int_mv* %72, i32 0
  %as_mv60 = bitcast %union.int_mv* %arrayidx59 to %struct.mv*
  %73 = load %struct.nmv_context*, %struct.nmv_context** %nmvc55, align 4, !tbaa !6
  %74 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %conv61 = trunc i32 %74 to i8
  call void @read_mv(%struct.aom_reader* %70, %struct.mv* %as_mv58, %struct.mv* %as_mv60, %struct.nmv_context* %73, i8 signext %conv61)
  %75 = load %union.int_mv*, %union.int_mv** %nearest_mv.addr, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds %union.int_mv, %union.int_mv* %75, i32 1
  %as_int63 = bitcast %union.int_mv* %arrayidx62 to i32*
  %76 = load i32, i32* %as_int63, align 4, !tbaa !8
  %77 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds %union.int_mv, %union.int_mv* %77, i32 1
  %as_int65 = bitcast %union.int_mv* %arrayidx64 to i32*
  store i32 %76, i32* %as_int65, align 4, !tbaa !8
  %78 = bitcast %struct.nmv_context** %nmvc55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #6
  br label %sw.epilog

sw.bb66:                                          ; preds = %if.end
  %79 = bitcast %struct.nmv_context** %nmvc67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #6
  %80 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %nmvc68 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %80, i32 0, i32 43
  store %struct.nmv_context* %nmvc68, %struct.nmv_context** %nmvc67, align 4, !tbaa !6
  %81 = load %union.int_mv*, %union.int_mv** %nearest_mv.addr, align 4, !tbaa !6
  %arrayidx69 = getelementptr inbounds %union.int_mv, %union.int_mv* %81, i32 0
  %as_int70 = bitcast %union.int_mv* %arrayidx69 to i32*
  %82 = load i32, i32* %as_int70, align 4, !tbaa !8
  %83 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx71 = getelementptr inbounds %union.int_mv, %union.int_mv* %83, i32 0
  %as_int72 = bitcast %union.int_mv* %arrayidx71 to i32*
  store i32 %82, i32* %as_int72, align 4, !tbaa !8
  %84 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %85 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx73 = getelementptr inbounds %union.int_mv, %union.int_mv* %85, i32 1
  %as_mv74 = bitcast %union.int_mv* %arrayidx73 to %struct.mv*
  %86 = load %union.int_mv*, %union.int_mv** %ref_mv.addr, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds %union.int_mv, %union.int_mv* %86, i32 1
  %as_mv76 = bitcast %union.int_mv* %arrayidx75 to %struct.mv*
  %87 = load %struct.nmv_context*, %struct.nmv_context** %nmvc67, align 4, !tbaa !6
  %88 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %conv77 = trunc i32 %88 to i8
  call void @read_mv(%struct.aom_reader* %84, %struct.mv* %as_mv74, %struct.mv* %as_mv76, %struct.nmv_context* %87, i8 signext %conv77)
  %89 = bitcast %struct.nmv_context** %nmvc67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #6
  br label %sw.epilog

sw.bb78:                                          ; preds = %if.end
  %90 = bitcast %struct.nmv_context** %nmvc79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  %91 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %nmvc80 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %91, i32 0, i32 43
  store %struct.nmv_context* %nmvc80, %struct.nmv_context** %nmvc79, align 4, !tbaa !6
  %92 = load %union.int_mv*, %union.int_mv** %near_mv.addr, align 4, !tbaa !6
  %arrayidx81 = getelementptr inbounds %union.int_mv, %union.int_mv* %92, i32 0
  %as_int82 = bitcast %union.int_mv* %arrayidx81 to i32*
  %93 = load i32, i32* %as_int82, align 4, !tbaa !8
  %94 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx83 = getelementptr inbounds %union.int_mv, %union.int_mv* %94, i32 0
  %as_int84 = bitcast %union.int_mv* %arrayidx83 to i32*
  store i32 %93, i32* %as_int84, align 4, !tbaa !8
  %95 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %96 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx85 = getelementptr inbounds %union.int_mv, %union.int_mv* %96, i32 1
  %as_mv86 = bitcast %union.int_mv* %arrayidx85 to %struct.mv*
  %97 = load %union.int_mv*, %union.int_mv** %ref_mv.addr, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds %union.int_mv, %union.int_mv* %97, i32 1
  %as_mv88 = bitcast %union.int_mv* %arrayidx87 to %struct.mv*
  %98 = load %struct.nmv_context*, %struct.nmv_context** %nmvc79, align 4, !tbaa !6
  %99 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %conv89 = trunc i32 %99 to i8
  call void @read_mv(%struct.aom_reader* %95, %struct.mv* %as_mv86, %struct.mv* %as_mv88, %struct.nmv_context* %98, i8 signext %conv89)
  %100 = bitcast %struct.nmv_context** %nmvc79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #6
  br label %sw.epilog

sw.bb90:                                          ; preds = %if.end
  %101 = bitcast %struct.nmv_context** %nmvc91 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #6
  %102 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %nmvc92 = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %102, i32 0, i32 43
  store %struct.nmv_context* %nmvc92, %struct.nmv_context** %nmvc91, align 4, !tbaa !6
  %103 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %104 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx93 = getelementptr inbounds %union.int_mv, %union.int_mv* %104, i32 0
  %as_mv94 = bitcast %union.int_mv* %arrayidx93 to %struct.mv*
  %105 = load %union.int_mv*, %union.int_mv** %ref_mv.addr, align 4, !tbaa !6
  %arrayidx95 = getelementptr inbounds %union.int_mv, %union.int_mv* %105, i32 0
  %as_mv96 = bitcast %union.int_mv* %arrayidx95 to %struct.mv*
  %106 = load %struct.nmv_context*, %struct.nmv_context** %nmvc91, align 4, !tbaa !6
  %107 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %conv97 = trunc i32 %107 to i8
  call void @read_mv(%struct.aom_reader* %103, %struct.mv* %as_mv94, %struct.mv* %as_mv96, %struct.nmv_context* %106, i8 signext %conv97)
  %108 = load %union.int_mv*, %union.int_mv** %near_mv.addr, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds %union.int_mv, %union.int_mv* %108, i32 1
  %as_int99 = bitcast %union.int_mv* %arrayidx98 to i32*
  %109 = load i32, i32* %as_int99, align 4, !tbaa !8
  %110 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx100 = getelementptr inbounds %union.int_mv, %union.int_mv* %110, i32 1
  %as_int101 = bitcast %union.int_mv* %arrayidx100 to i32*
  store i32 %109, i32* %as_int101, align 4, !tbaa !8
  %111 = bitcast %struct.nmv_context** %nmvc91 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  br label %sw.epilog

sw.bb102:                                         ; preds = %if.end
  %112 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %global_motion103 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %112, i32 0, i32 36
  %113 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx104 = getelementptr inbounds i8, i8* %113, i32 0
  %114 = load i8, i8* %arrayidx104, align 1, !tbaa !8
  %idxprom105 = sext i8 %114 to i32
  %arrayidx106 = getelementptr inbounds [8 x %struct.WarpedMotionParams], [8 x %struct.WarpedMotionParams]* %global_motion103, i32 0, i32 %idxprom105
  %115 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %allow_high_precision_mv107 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %115, i32 0, i32 1
  %116 = load i8, i8* %allow_high_precision_mv107, align 1, !tbaa !155, !range !46
  %tobool108 = trunc i8 %116 to i1
  %conv109 = zext i1 %tobool108 to i32
  %117 = load i8, i8* %bsize, align 1, !tbaa !8
  %118 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col110 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %118, i32 0, i32 1
  %119 = load i32, i32* %mi_col110, align 4, !tbaa !72
  %120 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row111 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %120, i32 0, i32 0
  %121 = load i32, i32* %mi_row111, align 16, !tbaa !71
  %122 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %cur_frame_force_integer_mv112 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %122, i32 0, i32 2
  %123 = load i8, i8* %cur_frame_force_integer_mv112, align 2, !tbaa !156, !range !46
  %tobool113 = trunc i8 %123 to i1
  %conv114 = zext i1 %tobool113 to i32
  call void @gm_get_motion_vector(%union.int_mv* sret align 4 %tmp115, %struct.WarpedMotionParams* %arrayidx106, i32 %conv109, i8 zeroext %117, i32 %119, i32 %121, i32 %conv114)
  %as_int116 = bitcast %union.int_mv* %tmp115 to i32*
  %124 = load i32, i32* %as_int116, align 4, !tbaa !8
  %125 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx117 = getelementptr inbounds %union.int_mv, %union.int_mv* %125, i32 0
  %as_int118 = bitcast %union.int_mv* %arrayidx117 to i32*
  store i32 %124, i32* %as_int118, align 4, !tbaa !8
  %126 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %global_motion119 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %126, i32 0, i32 36
  %127 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx120 = getelementptr inbounds i8, i8* %127, i32 1
  %128 = load i8, i8* %arrayidx120, align 1, !tbaa !8
  %idxprom121 = sext i8 %128 to i32
  %arrayidx122 = getelementptr inbounds [8 x %struct.WarpedMotionParams], [8 x %struct.WarpedMotionParams]* %global_motion119, i32 0, i32 %idxprom121
  %129 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %allow_high_precision_mv123 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %129, i32 0, i32 1
  %130 = load i8, i8* %allow_high_precision_mv123, align 1, !tbaa !155, !range !46
  %tobool124 = trunc i8 %130 to i1
  %conv125 = zext i1 %tobool124 to i32
  %131 = load i8, i8* %bsize, align 1, !tbaa !8
  %132 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_col126 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %132, i32 0, i32 1
  %133 = load i32, i32* %mi_col126, align 4, !tbaa !72
  %134 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi_row127 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %134, i32 0, i32 0
  %135 = load i32, i32* %mi_row127, align 16, !tbaa !71
  %136 = load %struct.FeatureFlags*, %struct.FeatureFlags** %features, align 4, !tbaa !6
  %cur_frame_force_integer_mv128 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %136, i32 0, i32 2
  %137 = load i8, i8* %cur_frame_force_integer_mv128, align 2, !tbaa !156, !range !46
  %tobool129 = trunc i8 %137 to i1
  %conv130 = zext i1 %tobool129 to i32
  call void @gm_get_motion_vector(%union.int_mv* sret align 4 %tmp131, %struct.WarpedMotionParams* %arrayidx122, i32 %conv125, i8 zeroext %131, i32 %133, i32 %135, i32 %conv130)
  %as_int132 = bitcast %union.int_mv* %tmp131 to i32*
  %138 = load i32, i32* %as_int132, align 4, !tbaa !8
  %139 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx133 = getelementptr inbounds %union.int_mv, %union.int_mv* %139, i32 1
  %as_int134 = bitcast %union.int_mv* %arrayidx133 to i32*
  store i32 %138, i32* %as_int134, align 4, !tbaa !8
  br label %sw.epilog

sw.default:                                       ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.epilog:                                        ; preds = %sw.bb102, %sw.bb90, %sw.bb78, %sw.bb66, %sw.bb54, %sw.bb45, %sw.bb36, %for.end, %sw.bb16, %sw.bb11, %sw.bb7, %sw.bb
  %140 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #6
  %141 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx135 = getelementptr inbounds %union.int_mv, %union.int_mv* %141, i32 0
  %as_mv136 = bitcast %union.int_mv* %arrayidx135 to %struct.mv*
  %call = call i32 @is_mv_valid(%struct.mv* %as_mv136)
  store i32 %call, i32* %ret, align 4, !tbaa !2
  %142 = load i32, i32* %is_compound.addr, align 4, !tbaa !2
  %tobool137 = icmp ne i32 %142, 0
  br i1 %tobool137, label %if.then138, label %if.end144

if.then138:                                       ; preds = %sw.epilog
  %143 = load i32, i32* %ret, align 4, !tbaa !2
  %tobool139 = icmp ne i32 %143, 0
  br i1 %tobool139, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.then138
  %144 = load %union.int_mv*, %union.int_mv** %mv.addr, align 4, !tbaa !6
  %arrayidx140 = getelementptr inbounds %union.int_mv, %union.int_mv* %144, i32 1
  %as_mv141 = bitcast %union.int_mv* %arrayidx140 to %struct.mv*
  %call142 = call i32 @is_mv_valid(%struct.mv* %as_mv141)
  %tobool143 = icmp ne i32 %call142, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.then138
  %145 = phi i1 [ false, %if.then138 ], [ %tobool143, %land.rhs ]
  %land.ext = zext i1 %145 to i32
  store i32 %land.ext, i32* %ret, align 4, !tbaa !2
  br label %if.end144

if.end144:                                        ; preds = %land.end, %sw.epilog
  %146 = load i32, i32* %ret, align 4, !tbaa !2
  store i32 %146, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %147 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end144, %sw.default
  %148 = bitcast %struct.FeatureFlags** %features to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  %149 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #6
  %150 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #6
  %151 = load i32, i32* %retval, align 4
  ret i32 %151
}

declare void @aom_merge_corrupted_flag(i32*, i32) #3

; Function Attrs: inlinehint nounwind
define internal i32 @is_interintra_allowed(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 6
  %1 = load i8, i8* %sb_type, align 2, !tbaa !76
  %call = call i32 @is_interintra_allowed_bsize(i8 zeroext %1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 7
  %3 = load i8, i8* %mode, align 1, !tbaa !50
  %call1 = call i32 @is_interintra_allowed_mode(i8 zeroext %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %4, i32 0, i32 12
  %arraydecay = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %call3 = call i32 @is_interintra_allowed_ref(i8* %arraydecay)
  %tobool4 = icmp ne i32 %call3, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %entry
  %5 = phi i1 [ false, %land.lhs.true ], [ false, %entry ], [ %tobool4, %land.rhs ]
  %land.ext = zext i1 %5 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define internal zeroext i8 @read_interintra_mode(%struct.macroblockd* %xd, %struct.aom_reader* %r, i32 %size_group) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %size_group.addr = alloca i32, align 4
  %ii_mode = alloca i8, align 1
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  store i32 %size_group, i32* %size_group.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ii_mode) #6
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 40
  %2 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %interintra_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %2, i32 0, i32 22
  %3 = load i32, i32* %size_group.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [4 x [5 x i16]], [4 x [5 x i16]]* %interintra_mode_cdf, i32 0, i32 %3
  %arraydecay = getelementptr inbounds [5 x i16], [5 x i16]* %arrayidx, i32 0, i32 0
  %call = call i32 @aom_read_symbol_(%struct.aom_reader* %0, i16* %arraydecay, i32 4, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @__func__.read_interintra_mode, i32 0, i32 0))
  %conv = trunc i32 %call to i8
  store i8 %conv, i8* %ii_mode, align 1, !tbaa !8
  %4 = load i8, i8* %ii_mode, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ii_mode) #6
  ret i8 %4
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_wedge_used(i8 zeroext %sb_type) #2 {
entry:
  %sb_type.addr = alloca i8, align 1
  store i8 %sb_type, i8* %sb_type.addr, align 1, !tbaa !8
  %0 = load i8, i8* %sb_type.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x %struct.wedge_params_type], [22 x %struct.wedge_params_type]* @av1_wedge_params_lookup, i32 0, i32 %idxprom
  %wedge_types = getelementptr inbounds %struct.wedge_params_type, %struct.wedge_params_type* %arrayidx, i32 0, i32 0
  %1 = load i32, i32* %wedge_types, align 16, !tbaa !172
  %cmp = icmp sgt i32 %1, 0
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: inlinehint nounwind
define internal %struct.scale_factors* @get_ref_scale_factors_const(%struct.AV1Common* %cm, i8 signext %ref_frame) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  %map_idx = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !8
  %0 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %2 = load i8, i8* %ref_frame.addr, align 1, !tbaa !8
  %call = call i32 @get_ref_frame_map_idx(%struct.AV1Common* %1, i8 signext %2)
  store i32 %call, i32* %map_idx, align 4, !tbaa !2
  %3 = load i32, i32* %map_idx, align 4, !tbaa !2
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %ref_scale_factors = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 16
  %5 = load i32, i32* %map_idx, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [8 x %struct.scale_factors], [8 x %struct.scale_factors]* %ref_scale_factors, i32 0, i32 %5
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.scale_factors* [ %arrayidx, %cond.true ], [ null, %cond.false ]
  %6 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret %struct.scale_factors* %cond
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_motion_variation_allowed_bsize(i8 zeroext %bsize) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %1 to i32
  %2 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom1 = zext i8 %2 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom1
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = zext i8 %3 to i32
  %cmp = icmp slt i32 %conv, %conv3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom5 = zext i8 %4 to i32
  %arrayidx6 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom5
  %5 = load i8, i8* %arrayidx6, align 1, !tbaa !8
  %conv7 = zext i8 %5 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %idxprom8 = zext i8 %6 to i32
  %arrayidx9 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom8
  %7 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  %conv10 = zext i8 %7 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv7, %cond.true ], [ %conv10, %cond.false ]
  %cmp11 = icmp sge i32 %cond, 8
  %conv12 = zext i1 %cmp11 to i32
  ret i32 %conv12
}

declare zeroext i8 @av1_findSamples(%struct.AV1Common*, %struct.macroblockd*, i32*, i32*) #3

declare void @av1_count_overlappable_neighbors(%struct.AV1Common*, %struct.macroblockd*) #3

; Function Attrs: nounwind
define internal zeroext i8 @read_motion_mode(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.MB_MODE_INFO* %mbmi, %struct.aom_reader* %r) #0 {
entry:
  %retval = alloca i8, align 1
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %last_motion_mode_allowed = alloca i8, align 1
  %motion_mode = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 21
  %switchable_motion_mode = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 11
  %1 = load i8, i8* %switchable_motion_mode, align 1, !tbaa !174, !range !46
  %tobool = trunc i8 %1 to i1
  %conv = zext i1 %tobool to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8 0, i8* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %skip_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 26
  %bf.load = load i8, i8* %skip_mode, align 4
  %bf.lshr = lshr i8 %bf.load, 5
  %bf.clear = and i8 %bf.lshr, 1
  %tobool2 = icmp ne i8 %bf.clear, 0
  br i1 %tobool2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i8 0, i8* %retval, align 1
  br label %return

if.end4:                                          ; preds = %if.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %last_motion_mode_allowed) #6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %global_motion = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 47
  %4 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %global_motion, align 4, !tbaa !175
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features5 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 21
  %allow_warped_motion = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features5, i32 0, i32 5
  %8 = load i8, i8* %allow_warped_motion, align 1, !tbaa !176, !range !46
  %tobool6 = trunc i8 %8 to i1
  %conv7 = zext i1 %tobool6 to i32
  %call = call zeroext i8 @motion_mode_allowed(%struct.WarpedMotionParams* %4, %struct.macroblockd* %5, %struct.MB_MODE_INFO* %6, i32 %conv7)
  store i8 %call, i8* %last_motion_mode_allowed, align 1, !tbaa !8
  %9 = bitcast i32* %motion_mode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i8, i8* %last_motion_mode_allowed, align 1, !tbaa !8
  %conv8 = zext i8 %10 to i32
  %cmp9 = icmp eq i32 %conv8, 0
  br i1 %cmp9, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end4
  store i8 0, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.end4
  %11 = load i8, i8* %last_motion_mode_allowed, align 1, !tbaa !8
  %conv13 = zext i8 %11 to i32
  %cmp14 = icmp eq i32 %conv13, 1
  br i1 %cmp14, label %if.then16, label %if.else

if.then16:                                        ; preds = %if.end12
  %12 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %13, i32 0, i32 40
  %14 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %obmc_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %14, i32 0, i32 24
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %15, i32 0, i32 6
  %16 = load i8, i8* %sb_type, align 2, !tbaa !76
  %idxprom = zext i8 %16 to i32
  %arrayidx = getelementptr inbounds [22 x [3 x i16]], [22 x [3 x i16]]* %obmc_cdf, i32 0, i32 %idxprom
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx, i32 0, i32 0
  %call17 = call i32 @aom_read_symbol_(%struct.aom_reader* %12, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @__func__.read_motion_mode, i32 0, i32 0))
  store i32 %call17, i32* %motion_mode, align 4, !tbaa !2
  %17 = load i32, i32* %motion_mode, align 4, !tbaa !2
  %add = add nsw i32 0, %17
  %conv18 = trunc i32 %add to i8
  store i8 %conv18, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.end12
  %18 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %19 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx19 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %19, i32 0, i32 40
  %20 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx19, align 16, !tbaa !47
  %motion_mode_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %20, i32 0, i32 23
  %21 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type20 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %21, i32 0, i32 6
  %22 = load i8, i8* %sb_type20, align 2, !tbaa !76
  %idxprom21 = zext i8 %22 to i32
  %arrayidx22 = getelementptr inbounds [22 x [4 x i16]], [22 x [4 x i16]]* %motion_mode_cdf, i32 0, i32 %idxprom21
  %arraydecay23 = getelementptr inbounds [4 x i16], [4 x i16]* %arrayidx22, i32 0, i32 0
  %call24 = call i32 @aom_read_symbol_(%struct.aom_reader* %18, i16* %arraydecay23, i32 3, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @__func__.read_motion_mode, i32 0, i32 0))
  store i32 %call24, i32* %motion_mode, align 4, !tbaa !2
  %23 = load i32, i32* %motion_mode, align 4, !tbaa !2
  %add25 = add nsw i32 0, %23
  %conv26 = trunc i32 %add25 to i8
  store i8 %conv26, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then16, %if.then11
  %24 = bitcast i32* %motion_mode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %last_motion_mode_allowed) #6
  br label %return

return:                                           ; preds = %cleanup, %if.then3, %if.then
  %25 = load i8, i8* %retval, align 1
  ret i8 %25
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_any_masked_compound_used(i8 zeroext %sb_type) #2 {
entry:
  %retval = alloca i32, align 4
  %sb_type.addr = alloca i8, align 1
  %comp_type = alloca i8, align 1
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8 %sb_type, i8* %sb_type.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %comp_type) #6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %sb_type.addr, align 1, !tbaa !8
  %call = call i32 @is_comp_ref_allowed(i8 zeroext %1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %2 = load i32, i32* %i, align 4, !tbaa !2
  %cmp = icmp slt i32 %2, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4, !tbaa !2
  %conv = trunc i32 %3 to i8
  store i8 %conv, i8* %comp_type, align 1, !tbaa !8
  %4 = load i8, i8* %comp_type, align 1, !tbaa !8
  %call1 = call i32 @is_masked_compound_type(i8 zeroext %4)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %land.lhs.true, label %if.end6

land.lhs.true:                                    ; preds = %for.body
  %5 = load i8, i8* %comp_type, align 1, !tbaa !8
  %6 = load i8, i8* %sb_type.addr, align 1, !tbaa !8
  %call3 = call i32 @is_interinter_compound_used(i8 zeroext %5, i8 zeroext %6)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end6
  %7 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then5, %if.then
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %comp_type) #6
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_comp_group_idx_context(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %above_mi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mi = alloca %struct.MB_MODE_INFO*, align 4
  %above_ctx = alloca i32, align 4
  %left_ctx = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 12
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 16, !tbaa !74
  store %struct.MB_MODE_INFO* %2, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %3 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 11
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !75
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %6 = bitcast i32* %above_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store i32 0, i32* %above_ctx, align 4, !tbaa !2
  %7 = bitcast i32* %left_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store i32 0, i32* %left_ctx, align 4, !tbaa !2
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %tobool = icmp ne %struct.MB_MODE_INFO* %8, null
  br i1 %tobool, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %9 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %call = call i32 @has_second_ref(%struct.MB_MODE_INFO* %9)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %comp_group_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %10, i32 0, i32 27
  %bf.load = load i8, i8* %comp_group_idx, align 1
  %bf.lshr = lshr i8 %bf.load, 2
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  store i32 %conv, i32* %above_ctx, align 4, !tbaa !2
  br label %if.end6

if.else:                                          ; preds = %if.then
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %11, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %12 = load i8, i8* %arrayidx, align 4, !tbaa !8
  %conv3 = sext i8 %12 to i32
  %cmp = icmp eq i32 %conv3, 7
  br i1 %cmp, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  store i32 3, i32* %above_ctx, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  br label %if.end6

if.end6:                                          ; preds = %if.end, %if.then2
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %entry
  %13 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %tobool8 = icmp ne %struct.MB_MODE_INFO* %13, null
  br i1 %tobool8, label %if.then9, label %if.end27

if.then9:                                         ; preds = %if.end7
  %14 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %call10 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %14)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.then12, label %if.else18

if.then12:                                        ; preds = %if.then9
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %comp_group_idx13 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %15, i32 0, i32 27
  %bf.load14 = load i8, i8* %comp_group_idx13, align 1
  %bf.lshr15 = lshr i8 %bf.load14, 2
  %bf.clear16 = and i8 %bf.lshr15, 1
  %conv17 = zext i8 %bf.clear16 to i32
  store i32 %conv17, i32* %left_ctx, align 4, !tbaa !2
  br label %if.end26

if.else18:                                        ; preds = %if.then9
  %16 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %ref_frame19 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %16, i32 0, i32 12
  %arrayidx20 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame19, i32 0, i32 0
  %17 = load i8, i8* %arrayidx20, align 4, !tbaa !8
  %conv21 = sext i8 %17 to i32
  %cmp22 = icmp eq i32 %conv21, 7
  br i1 %cmp22, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.else18
  store i32 3, i32* %left_ctx, align 4, !tbaa !2
  br label %if.end25

if.end25:                                         ; preds = %if.then24, %if.else18
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.then12
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.end7
  %18 = load i32, i32* %above_ctx, align 4, !tbaa !2
  %19 = load i32, i32* %left_ctx, align 4, !tbaa !2
  %add = add nsw i32 %18, %19
  %cmp28 = icmp slt i32 5, %add
  br i1 %cmp28, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end27
  br label %cond.end

cond.false:                                       ; preds = %if.end27
  %20 = load i32, i32* %above_ctx, align 4, !tbaa !2
  %21 = load i32, i32* %left_ctx, align 4, !tbaa !2
  %add30 = add nsw i32 %20, %21
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 5, %cond.true ], [ %add30, %cond.false ]
  %22 = bitcast i32* %left_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast i32* %above_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  %24 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  %25 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #6
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_comp_index_context(%struct.AV1Common* %cm, %struct.macroblockd* %xd) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %bck_buf = alloca %struct.RefCntBuffer*, align 4
  %fwd_buf = alloca %struct.RefCntBuffer*, align 4
  %bck_frame_index = alloca i32, align 4
  %fwd_frame_index = alloca i32, align 4
  %cur_frame_index = alloca i32, align 4
  %fwd = alloca i32, align 4
  %bck = alloca i32, align 4
  %above_mi = alloca %struct.MB_MODE_INFO*, align 4
  %left_mi = alloca %struct.MB_MODE_INFO*, align 4
  %above_ctx = alloca i32, align 4
  %left_ctx = alloca i32, align 4
  %offset = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %4 = bitcast %struct.RefCntBuffer** %bck_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %6, i32 0, i32 12
  %arrayidx1 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %7 = load i8, i8* %arrayidx1, align 4, !tbaa !8
  %call = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %5, i8 signext %7)
  store %struct.RefCntBuffer* %call, %struct.RefCntBuffer** %bck_buf, align 4, !tbaa !6
  %8 = bitcast %struct.RefCntBuffer** %fwd_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %ref_frame2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %10, i32 0, i32 12
  %arrayidx3 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame2, i32 0, i32 1
  %11 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  %call4 = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %9, i8 signext %11)
  store %struct.RefCntBuffer* %call4, %struct.RefCntBuffer** %fwd_buf, align 4, !tbaa !6
  %12 = bitcast i32* %bck_frame_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  store i32 0, i32* %bck_frame_index, align 4, !tbaa !2
  %13 = bitcast i32* %fwd_frame_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store i32 0, i32* %fwd_frame_index, align 4, !tbaa !2
  %14 = bitcast i32* %cur_frame_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 13
  %16 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !91
  %order_hint = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %16, i32 0, i32 1
  %17 = load i32, i32* %order_hint, align 4, !tbaa !177
  store i32 %17, i32* %cur_frame_index, align 4, !tbaa !2
  %18 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %bck_buf, align 4, !tbaa !6
  %cmp = icmp ne %struct.RefCntBuffer* %18, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %19 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %bck_buf, align 4, !tbaa !6
  %order_hint5 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %19, i32 0, i32 1
  %20 = load i32, i32* %order_hint5, align 4, !tbaa !177
  store i32 %20, i32* %bck_frame_index, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %21 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %fwd_buf, align 4, !tbaa !6
  %cmp6 = icmp ne %struct.RefCntBuffer* %21, null
  br i1 %cmp6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end
  %22 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %fwd_buf, align 4, !tbaa !6
  %order_hint8 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %22, i32 0, i32 1
  %23 = load i32, i32* %order_hint8, align 4, !tbaa !177
  store i32 %23, i32* %fwd_frame_index, align 4, !tbaa !2
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %if.end
  %24 = bitcast i32* %fwd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  %25 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %25, i32 0, i32 37
  %order_hint_info = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 10
  %26 = load i32, i32* %fwd_frame_index, align 4, !tbaa !2
  %27 = load i32, i32* %cur_frame_index, align 4, !tbaa !2
  %call10 = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info, i32 %26, i32 %27)
  %call11 = call i32 @abs(i32 %call10) #7
  store i32 %call11, i32* %fwd, align 4, !tbaa !2
  %28 = bitcast i32* %bck to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %29 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params12 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %29, i32 0, i32 37
  %order_hint_info13 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params12, i32 0, i32 10
  %30 = load i32, i32* %cur_frame_index, align 4, !tbaa !2
  %31 = load i32, i32* %bck_frame_index, align 4, !tbaa !2
  %call14 = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info13, i32 %30, i32 %31)
  %call15 = call i32 @abs(i32 %call14) #7
  store i32 %call15, i32* %bck, align 4, !tbaa !2
  %32 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  %33 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %above_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %33, i32 0, i32 12
  %34 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi, align 16, !tbaa !74
  store %struct.MB_MODE_INFO* %34, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %35 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %36 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %left_mbmi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %36, i32 0, i32 11
  %37 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi, align 4, !tbaa !75
  store %struct.MB_MODE_INFO* %37, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %38 = bitcast i32* %above_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  store i32 0, i32* %above_ctx, align 4, !tbaa !2
  %39 = bitcast i32* %left_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  store i32 0, i32* %left_ctx, align 4, !tbaa !2
  %40 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  %41 = load i32, i32* %fwd, align 4, !tbaa !2
  %42 = load i32, i32* %bck, align 4, !tbaa !2
  %cmp16 = icmp eq i32 %41, %42
  %conv = zext i1 %cmp16 to i32
  store i32 %conv, i32* %offset, align 4, !tbaa !2
  %43 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %cmp17 = icmp ne %struct.MB_MODE_INFO* %43, null
  br i1 %cmp17, label %if.then19, label %if.end31

if.then19:                                        ; preds = %if.end9
  %44 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %call20 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %44)
  %tobool = icmp ne i32 %call20, 0
  br i1 %tobool, label %if.then21, label %if.else

if.then21:                                        ; preds = %if.then19
  %45 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %compound_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %45, i32 0, i32 25
  %46 = load i8, i8* %compound_idx, align 1, !tbaa !161
  %conv22 = zext i8 %46 to i32
  store i32 %conv22, i32* %above_ctx, align 4, !tbaa !2
  br label %if.end30

if.else:                                          ; preds = %if.then19
  %47 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi, align 4, !tbaa !6
  %ref_frame23 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %47, i32 0, i32 12
  %arrayidx24 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame23, i32 0, i32 0
  %48 = load i8, i8* %arrayidx24, align 4, !tbaa !8
  %conv25 = sext i8 %48 to i32
  %cmp26 = icmp eq i32 %conv25, 7
  br i1 %cmp26, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.else
  store i32 1, i32* %above_ctx, align 4, !tbaa !2
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %if.else
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %if.then21
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %if.end9
  %49 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %cmp32 = icmp ne %struct.MB_MODE_INFO* %49, null
  br i1 %cmp32, label %if.then34, label %if.end49

if.then34:                                        ; preds = %if.end31
  %50 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %call35 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %50)
  %tobool36 = icmp ne i32 %call35, 0
  br i1 %tobool36, label %if.then37, label %if.else40

if.then37:                                        ; preds = %if.then34
  %51 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %compound_idx38 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %51, i32 0, i32 25
  %52 = load i8, i8* %compound_idx38, align 1, !tbaa !161
  %conv39 = zext i8 %52 to i32
  store i32 %conv39, i32* %left_ctx, align 4, !tbaa !2
  br label %if.end48

if.else40:                                        ; preds = %if.then34
  %53 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi, align 4, !tbaa !6
  %ref_frame41 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %53, i32 0, i32 12
  %arrayidx42 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame41, i32 0, i32 0
  %54 = load i8, i8* %arrayidx42, align 4, !tbaa !8
  %conv43 = sext i8 %54 to i32
  %cmp44 = icmp eq i32 %conv43, 7
  br i1 %cmp44, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.else40
  store i32 1, i32* %left_ctx, align 4, !tbaa !2
  br label %if.end47

if.end47:                                         ; preds = %if.then46, %if.else40
  br label %if.end48

if.end48:                                         ; preds = %if.end47, %if.then37
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.end31
  %55 = load i32, i32* %above_ctx, align 4, !tbaa !2
  %56 = load i32, i32* %left_ctx, align 4, !tbaa !2
  %add = add nsw i32 %55, %56
  %57 = load i32, i32* %offset, align 4, !tbaa !2
  %mul = mul nsw i32 3, %57
  %add50 = add nsw i32 %add, %mul
  %58 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %59 = bitcast i32* %left_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %above_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast %struct.MB_MODE_INFO** %left_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast %struct.MB_MODE_INFO** %above_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast i32* %bck to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast i32* %fwd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = bitcast i32* %cur_frame_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  %66 = bitcast i32* %fwd_frame_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = bitcast i32* %bck_frame_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast %struct.RefCntBuffer** %fwd_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast %struct.RefCntBuffer** %bck_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  ret i32 %add50
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_interinter_compound_used(i8 zeroext %type, i8 zeroext %sb_type) #2 {
entry:
  %retval = alloca i32, align 4
  %type.addr = alloca i8, align 1
  %sb_type.addr = alloca i8, align 1
  %comp_allowed = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8 %type, i8* %type.addr, align 1, !tbaa !8
  store i8 %sb_type, i8* %sb_type.addr, align 1, !tbaa !8
  %0 = bitcast i32* %comp_allowed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %sb_type.addr, align 1, !tbaa !8
  %call = call i32 @is_comp_ref_allowed(i8 zeroext %1)
  store i32 %call, i32* %comp_allowed, align 4, !tbaa !2
  %2 = load i8, i8* %type.addr, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb
    i32 3, label %sw.bb
    i32 2, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry
  %3 = load i32, i32* %comp_allowed, align 4, !tbaa !2
  store i32 %3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb1:                                           ; preds = %entry
  %4 = load i32, i32* %comp_allowed, align 4, !tbaa !2
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %sw.bb1
  %5 = load i8, i8* %sb_type.addr, align 1, !tbaa !8
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [22 x %struct.wedge_params_type], [22 x %struct.wedge_params_type]* @av1_wedge_params_lookup, i32 0, i32 %idxprom
  %wedge_types = getelementptr inbounds %struct.wedge_params_type, %struct.wedge_params_type* %arrayidx, i32 0, i32 0
  %6 = load i32, i32* %wedge_types, align 16, !tbaa !172
  %cmp = icmp sgt i32 %6, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %sw.bb1
  %7 = phi i1 [ false, %sw.bb1 ], [ %cmp, %land.rhs ]
  %land.ext = zext i1 %7 to i32
  store i32 %land.ext, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.default:                                       ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %sw.default, %land.end, %sw.bb
  %8 = bitcast i32* %comp_allowed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: inlinehint nounwind
define internal void @read_mb_interp_filter(%struct.macroblockd* %xd, i8 zeroext %interp_filter, i1 zeroext %enable_dual_filter, %struct.MB_MODE_INFO* %mbmi, %struct.aom_reader* %r) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %interp_filter.addr = alloca i8, align 1
  %enable_dual_filter.addr = alloca i8, align 1
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %ec_ctx = alloca %struct.frame_contexts*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tmp = alloca %union.int_interpfilters, align 4
  %ref0_filter = alloca [2 x i8], align 1
  %dir = alloca i32, align 4
  %ctx = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store i8 %interp_filter, i8* %interp_filter.addr, align 1, !tbaa !8
  %frombool = zext i1 %enable_dual_filter to i8
  store i8 %frombool, i8* %enable_dual_filter.addr, align 1, !tbaa !111
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 40
  %2 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  store %struct.frame_contexts* %2, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_is_interp_needed(%struct.macroblockd* %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %5 = load i8, i8* %interp_filter.addr, align 1, !tbaa !8
  call void @set_default_interp_filters(%struct.MB_MODE_INFO* %4, i8 zeroext %5)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

if.end:                                           ; preds = %entry
  %6 = load i8, i8* %interp_filter.addr, align 1, !tbaa !8
  %conv = zext i8 %6 to i32
  %cmp = icmp ne i32 %conv, 4
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interp_filters = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 4
  %8 = bitcast %union.int_interpfilters* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i8, i8* %interp_filter.addr, align 1, !tbaa !8
  call void @av1_broadcast_interp_filter(%union.int_interpfilters* sret align 4 %tmp, i8 zeroext %9)
  %10 = bitcast %union.int_interpfilters* %interp_filters to i8*
  %11 = bitcast %union.int_interpfilters* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 4, i1 false), !tbaa.struct !122
  %12 = bitcast %union.int_interpfilters* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  br label %if.end22

if.else:                                          ; preds = %if.end
  %13 = bitcast [2 x i8]* %ref0_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #6
  %14 = bitcast [2 x i8]* %ref0_filter to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %14, i8 0, i32 2, i1 false)
  %15 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  store i32 0, i32* %dir, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %16 = load i32, i32* %dir, align 4, !tbaa !2
  %cmp3 = icmp slt i32 %16, 2
  br i1 %cmp3, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

for.body:                                         ; preds = %for.cond
  %17 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %19 = load i32, i32* %dir, align 4, !tbaa !2
  %call5 = call i32 @av1_get_pred_context_switchable_interp(%struct.macroblockd* %18, i32 %19)
  store i32 %call5, i32* %ctx, align 4, !tbaa !2
  %20 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %21 = load %struct.frame_contexts*, %struct.frame_contexts** %ec_ctx, align 4, !tbaa !6
  %switchable_interp_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %21, i32 0, i32 55
  %22 = load i32, i32* %ctx, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [16 x [4 x i16]], [16 x [4 x i16]]* %switchable_interp_cdf, i32 0, i32 %22
  %arraydecay = getelementptr inbounds [4 x i16], [4 x i16]* %arrayidx, i32 0, i32 0
  %call6 = call i32 @aom_read_symbol_(%struct.aom_reader* %20, i16* %arraydecay, i32 3, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @__func__.read_mb_interp_filter, i32 0, i32 0))
  %conv7 = trunc i32 %call6 to i8
  %23 = load i32, i32* %dir, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds [2 x i8], [2 x i8]* %ref0_filter, i32 0, i32 %23
  store i8 %conv7, i8* %arrayidx8, align 1, !tbaa !8
  %24 = load i8, i8* %enable_dual_filter.addr, align 1, !tbaa !111, !range !46
  %tobool9 = trunc i8 %24 to i1
  br i1 %tobool9, label %if.end13, label %if.then10

if.then10:                                        ; preds = %for.body
  %arrayidx11 = getelementptr inbounds [2 x i8], [2 x i8]* %ref0_filter, i32 0, i32 0
  %25 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  %arrayidx12 = getelementptr inbounds [2 x i8], [2 x i8]* %ref0_filter, i32 0, i32 1
  store i8 %25, i8* %arrayidx12, align 1, !tbaa !8
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end13, %if.then10
  %26 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup14 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %27 = load i32, i32* %dir, align 4, !tbaa !2
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %dir, align 4, !tbaa !2
  br label %for.cond

cleanup14:                                        ; preds = %cleanup, %for.cond.cleanup
  %28 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  br label %for.end

for.end:                                          ; preds = %cleanup14
  %arrayidx15 = getelementptr inbounds [2 x i8], [2 x i8]* %ref0_filter, i32 0, i32 1
  %29 = load i8, i8* %arrayidx15, align 1, !tbaa !8
  %conv16 = zext i8 %29 to i16
  %30 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interp_filters17 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %30, i32 0, i32 4
  %as_filters = bitcast %union.int_interpfilters* %interp_filters17 to %struct.InterpFilters*
  %x_filter = getelementptr inbounds %struct.InterpFilters, %struct.InterpFilters* %as_filters, i32 0, i32 1
  store i16 %conv16, i16* %x_filter, align 2, !tbaa !8
  %arrayidx18 = getelementptr inbounds [2 x i8], [2 x i8]* %ref0_filter, i32 0, i32 0
  %31 = load i8, i8* %arrayidx18, align 1, !tbaa !8
  %conv19 = zext i8 %31 to i16
  %32 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interp_filters20 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %32, i32 0, i32 4
  %as_filters21 = bitcast %union.int_interpfilters* %interp_filters20 to %struct.InterpFilters*
  %y_filter = getelementptr inbounds %struct.InterpFilters, %struct.InterpFilters* %as_filters21, i32 0, i32 0
  store i16 %conv19, i16* %y_filter, align 4, !tbaa !8
  %33 = bitcast [2 x i8]* %ref0_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %33) #6
  br label %if.end22

if.end22:                                         ; preds = %for.end, %if.then2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.end22, %if.then
  %34 = bitcast %struct.frame_contexts** %ec_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %cleanup.dest24 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest24, label %unreachable [
    i32 0, label %cleanup.cont25
    i32 1, label %cleanup.cont25
  ]

cleanup.cont25:                                   ; preds = %cleanup23, %cleanup23
  ret void

unreachable:                                      ; preds = %cleanup23
  unreachable
}

declare zeroext i8 @av1_selectSamples(%struct.mv*, i32*, i32*, i32, i8 zeroext) #3

declare i32 @av1_find_projection(i32, i32*, i32*, i8 zeroext, i32, i32, %struct.WarpedMotionParams*, i32, i32) #3

; Function Attrs: nounwind
define internal void @set_ref_frames_for_skip_mode(%struct.AV1Common* %cm, i8* %ref_frame) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i8* %ref_frame, i8** %ref_frame.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 0
  %skip_mode_info = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 5
  %ref_frame_idx_0 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %skip_mode_info, i32 0, i32 2
  %1 = load i32, i32* %ref_frame_idx_0, align 8, !tbaa !178
  %add = add nsw i32 1, %1
  %conv = trunc i32 %add to i8
  %2 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 0
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !8
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %current_frame1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 0
  %skip_mode_info2 = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame1, i32 0, i32 5
  %ref_frame_idx_1 = getelementptr inbounds %struct.SkipModeInfo, %struct.SkipModeInfo* %skip_mode_info2, i32 0, i32 3
  %4 = load i32, i32* %ref_frame_idx_1, align 4, !tbaa !179
  %add3 = add nsw i32 1, %4
  %conv4 = trunc i32 %add3 to i8
  %5 = load i8*, i8** %ref_frame.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i8, i8* %5, i32 1
  store i8 %conv4, i8* %arrayidx5, align 1, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define internal zeroext i8 @read_block_reference_mode(%struct.AV1Common* %cm, %struct.macroblockd* %xd, %struct.aom_reader* %r) #0 {
entry:
  %retval = alloca i8, align 1
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %ctx = alloca i32, align 4
  %mode = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 6
  %1 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %1, i32 0
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 6
  %3 = load i8, i8* %sb_type, align 2, !tbaa !76
  %call = call i32 @is_comp_ref_allowed(i8 zeroext %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i8 0, i8* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 0
  %reference_mode = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 1
  %5 = load i8, i8* %reference_mode, align 1, !tbaa !180
  %conv = zext i8 %5 to i32
  %cmp = icmp eq i32 %conv, 2
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %6 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call3 = call i32 @av1_get_reference_mode_context(%struct.macroblockd* %7)
  store i32 %call3, i32* %ctx, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mode) #6
  %8 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %9, i32 0, i32 40
  %10 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %comp_inter_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %10, i32 0, i32 31
  %11 = load i32, i32* %ctx, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds [5 x [3 x i16]], [5 x [3 x i16]]* %comp_inter_cdf, i32 0, i32 %11
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx4, i32 0, i32 0
  %call5 = call i32 @aom_read_symbol_(%struct.aom_reader* %8, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @__func__.read_block_reference_mode, i32 0, i32 0))
  %conv6 = trunc i32 %call5 to i8
  store i8 %conv6, i8* %mode, align 1, !tbaa !8
  %12 = load i8, i8* %mode, align 1, !tbaa !8
  store i8 %12, i8* %retval, align 1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mode) #6
  %13 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  br label %return

if.else:                                          ; preds = %if.end
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %current_frame7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 0
  %reference_mode8 = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame7, i32 0, i32 1
  %15 = load i8, i8* %reference_mode8, align 1, !tbaa !180
  store i8 %15, i8* %retval, align 1
  br label %return

return:                                           ; preds = %if.else, %if.then2, %if.then
  %16 = load i8, i8* %retval, align 1
  ret i8 %16
}

; Function Attrs: nounwind
define internal zeroext i8 @read_comp_reference_type(%struct.macroblockd* %xd, %struct.aom_reader* %r) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %ctx = alloca i32, align 4
  %comp_ref_type = alloca i8, align 1
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_comp_reference_type_context(%struct.macroblockd* %1)
  store i32 %call, i32* %ctx, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %comp_ref_type) #6
  %2 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !6
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 40
  %4 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %comp_ref_type_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %4, i32 0, i32 33
  %5 = load i32, i32* %ctx, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [5 x [3 x i16]], [5 x [3 x i16]]* %comp_ref_type_cdf, i32 0, i32 %5
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx, i32 0, i32 0
  %call1 = call i32 @aom_read_symbol_(%struct.aom_reader* %2, i16* %arraydecay, i32 2, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @__func__.read_comp_reference_type, i32 0, i32 0))
  %conv = trunc i32 %call1 to i8
  store i8 %conv, i8* %comp_ref_type, align 1, !tbaa !8
  %6 = load i8, i8* %comp_ref_type, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %comp_ref_type) #6
  %7 = bitcast i32* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret i8 %6
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_uni_comp_ref_p(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_uni_comp_ref_p(%struct.macroblockd* %1)
  store i32 %call, i32* %pred_context, align 4, !tbaa !2
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 40
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %uni_comp_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %3, i32 0, i32 34
  %4 = load i32, i32* %pred_context, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]]* %uni_comp_ref_cdf, i32 0, i32 %4
  %arrayidx1 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %arrayidx, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  %5 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_uni_comp_ref_p1(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_uni_comp_ref_p1(%struct.macroblockd* %1)
  store i32 %call, i32* %pred_context, align 4, !tbaa !2
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 40
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %uni_comp_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %3, i32 0, i32 34
  %4 = load i32, i32* %pred_context, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]]* %uni_comp_ref_cdf, i32 0, i32 %4
  %arrayidx1 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %arrayidx, i32 0, i32 1
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  %5 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_uni_comp_ref_p2(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_uni_comp_ref_p2(%struct.macroblockd* %1)
  store i32 %call, i32* %pred_context, align 4, !tbaa !2
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 40
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %uni_comp_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %3, i32 0, i32 34
  %4 = load i32, i32* %pred_context, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]]* %uni_comp_ref_cdf, i32 0, i32 %4
  %arrayidx1 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %arrayidx, i32 0, i32 2
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  %5 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_comp_ref_p(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_comp_ref_p(%struct.macroblockd* %1)
  store i32 %call, i32* %pred_context, align 4, !tbaa !2
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 40
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %comp_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %3, i32 0, i32 35
  %4 = load i32, i32* %pred_context, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]]* %comp_ref_cdf, i32 0, i32 %4
  %arrayidx1 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %arrayidx, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  %5 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_comp_ref_p1(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_comp_ref_p1(%struct.macroblockd* %1)
  store i32 %call, i32* %pred_context, align 4, !tbaa !2
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 40
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %comp_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %3, i32 0, i32 35
  %4 = load i32, i32* %pred_context, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]]* %comp_ref_cdf, i32 0, i32 %4
  %arrayidx1 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %arrayidx, i32 0, i32 1
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  %5 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_comp_ref_p2(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_comp_ref_p2(%struct.macroblockd* %1)
  store i32 %call, i32* %pred_context, align 4, !tbaa !2
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 40
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %comp_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %3, i32 0, i32 35
  %4 = load i32, i32* %pred_context, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]]* %comp_ref_cdf, i32 0, i32 %4
  %arrayidx1 = getelementptr inbounds [3 x [3 x i16]], [3 x [3 x i16]]* %arrayidx, i32 0, i32 2
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  %5 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_comp_bwdref_p(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_comp_bwdref_p(%struct.macroblockd* %1)
  store i32 %call, i32* %pred_context, align 4, !tbaa !2
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 40
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %comp_bwdref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %3, i32 0, i32 36
  %4 = load i32, i32* %pred_context, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [2 x [3 x i16]]], [3 x [2 x [3 x i16]]]* %comp_bwdref_cdf, i32 0, i32 %4
  %arrayidx1 = getelementptr inbounds [2 x [3 x i16]], [2 x [3 x i16]]* %arrayidx, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  %5 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_comp_bwdref_p1(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred_context = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_comp_bwdref_p1(%struct.macroblockd* %1)
  store i32 %call, i32* %pred_context, align 4, !tbaa !2
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 40
  %3 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %comp_bwdref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %3, i32 0, i32 36
  %4 = load i32, i32* %pred_context, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x [2 x [3 x i16]]], [3 x [2 x [3 x i16]]]* %comp_bwdref_cdf, i32 0, i32 %4
  %arrayidx1 = getelementptr inbounds [2 x [3 x i16]], [2 x [3 x i16]]* %arrayidx, i32 0, i32 1
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  %5 = bitcast i32* %pred_context to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_single_ref_p1(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 40
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %single_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %1, i32 0, i32 32
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_single_ref_p1(%struct.macroblockd* %2)
  %arrayidx = getelementptr inbounds [3 x [6 x [3 x i16]]], [3 x [6 x [3 x i16]]]* %single_ref_cdf, i32 0, i32 %call
  %arrayidx1 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %arrayidx, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_single_ref_p2(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 40
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %single_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %1, i32 0, i32 32
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_single_ref_p2(%struct.macroblockd* %2)
  %arrayidx = getelementptr inbounds [3 x [6 x [3 x i16]]], [3 x [6 x [3 x i16]]]* %single_ref_cdf, i32 0, i32 %call
  %arrayidx1 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %arrayidx, i32 0, i32 1
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_single_ref_p6(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 40
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %single_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %1, i32 0, i32 32
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_single_ref_p6(%struct.macroblockd* %2)
  %arrayidx = getelementptr inbounds [3 x [6 x [3 x i16]]], [3 x [6 x [3 x i16]]]* %single_ref_cdf, i32 0, i32 %call
  %arrayidx1 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %arrayidx, i32 0, i32 5
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_single_ref_p3(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 40
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %single_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %1, i32 0, i32 32
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_single_ref_p3(%struct.macroblockd* %2)
  %arrayidx = getelementptr inbounds [3 x [6 x [3 x i16]]], [3 x [6 x [3 x i16]]]* %single_ref_cdf, i32 0, i32 %call
  %arrayidx1 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %arrayidx, i32 0, i32 2
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_single_ref_p5(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 40
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %single_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %1, i32 0, i32 32
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_single_ref_p5(%struct.macroblockd* %2)
  %arrayidx = getelementptr inbounds [3 x [6 x [3 x i16]]], [3 x [6 x [3 x i16]]]* %single_ref_cdf, i32 0, i32 %call
  %arrayidx1 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %arrayidx, i32 0, i32 4
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  ret i16* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_pred_cdf_single_ref_p4(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 40
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !47
  %single_ref_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %1, i32 0, i32 32
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %call = call i32 @av1_get_pred_context_single_ref_p4(%struct.macroblockd* %2)
  %arrayidx = getelementptr inbounds [3 x [6 x [3 x i16]]], [3 x [6 x [3 x i16]]]* %single_ref_cdf, i32 0, i32 %call
  %arrayidx1 = getelementptr inbounds [6 x [3 x i16]], [6 x [3 x i16]]* %arrayidx, i32 0, i32 3
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %arrayidx1, i32 0, i32 0
  ret i16* %arraydecay
}

declare i32 @av1_get_reference_mode_context(%struct.macroblockd*) #3

declare i32 @av1_get_comp_reference_type_context(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_uni_comp_ref_p(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_uni_comp_ref_p1(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_uni_comp_ref_p2(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_comp_ref_p(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_comp_ref_p1(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_comp_ref_p2(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_comp_bwdref_p(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_comp_bwdref_p1(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_single_ref_p1(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_single_ref_p2(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_single_ref_p6(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_single_ref_p3(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_single_ref_p5(%struct.macroblockd*) #3

declare i32 @av1_get_pred_context_single_ref_p4(%struct.macroblockd*) #3

; Function Attrs: inlinehint nounwind
define internal signext i8 @get_uni_comp_ref_idx(i8* %rf) #2 {
entry:
  %retval = alloca i8, align 1
  %rf.addr = alloca i8*, align 4
  %ref_idx = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 1
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = sext i8 %1 to i32
  %cmp = icmp sle i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8 -1, i8* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8, i8* %2, i32 0
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = sext i8 %3 to i32
  %cmp4 = icmp slt i32 %conv3, 5
  br i1 %cmp4, label %land.lhs.true, label %if.end11

land.lhs.true:                                    ; preds = %if.end
  %4 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds i8, i8* %4, i32 1
  %5 = load i8, i8* %arrayidx6, align 1, !tbaa !8
  %conv7 = sext i8 %5 to i32
  %cmp8 = icmp sge i32 %conv7, 5
  br i1 %cmp8, label %if.then10, label %if.end11

if.then10:                                        ; preds = %land.lhs.true
  store i8 -1, i8* %retval, align 1
  br label %return

if.end11:                                         ; preds = %land.lhs.true, %if.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_idx) #6
  store i8 0, i8* %ref_idx, align 1, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end11
  %6 = load i8, i8* %ref_idx, align 1, !tbaa !8
  %conv12 = sext i8 %6 to i32
  %cmp13 = icmp slt i32 %conv12, 9
  br i1 %cmp13, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %7 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds i8, i8* %7, i32 0
  %8 = load i8, i8* %arrayidx15, align 1, !tbaa !8
  %conv16 = sext i8 %8 to i32
  %9 = load i8, i8* %ref_idx, align 1, !tbaa !8
  %conv17 = sext i8 %9 to i32
  %call = call signext i8 @comp_ref0(i32 %conv17)
  %conv18 = sext i8 %call to i32
  %cmp19 = icmp eq i32 %conv16, %conv18
  br i1 %cmp19, label %land.lhs.true21, label %if.end30

land.lhs.true21:                                  ; preds = %for.body
  %10 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i8, i8* %10, i32 1
  %11 = load i8, i8* %arrayidx22, align 1, !tbaa !8
  %conv23 = sext i8 %11 to i32
  %12 = load i8, i8* %ref_idx, align 1, !tbaa !8
  %conv24 = sext i8 %12 to i32
  %call25 = call signext i8 @comp_ref1(i32 %conv24)
  %conv26 = sext i8 %call25 to i32
  %cmp27 = icmp eq i32 %conv23, %conv26
  br i1 %cmp27, label %if.then29, label %if.end30

if.then29:                                        ; preds = %land.lhs.true21
  %13 = load i8, i8* %ref_idx, align 1, !tbaa !8
  store i8 %13, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end30:                                         ; preds = %land.lhs.true21, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end30
  %14 = load i8, i8* %ref_idx, align 1, !tbaa !8
  %inc = add i8 %14, 1
  store i8 %inc, i8* %ref_idx, align 1, !tbaa !8
  br label %for.cond

cleanup:                                          ; preds = %if.then29, %for.cond.cleanup
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_idx) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  store i8 -1, i8* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %cleanup, %if.then10, %if.then
  %15 = load i8, i8* %retval, align 1
  ret i8 %15

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @comp_ref0(i32 %ref_idx) #2 {
entry:
  %ref_idx.addr = alloca i32, align 4
  store i32 %ref_idx, i32* %ref_idx.addr, align 4, !tbaa !2
  %0 = load i32, i32* %ref_idx.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [9 x i8], [9 x i8]* @comp_ref0.lut, i32 0, i32 %0
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  ret i8 %1
}

; Function Attrs: inlinehint nounwind
define internal signext i8 @comp_ref1(i32 %ref_idx) #2 {
entry:
  %ref_idx.addr = alloca i32, align 4
  store i32 %ref_idx, i32* %ref_idx.addr, align 4, !tbaa !2
  %0 = load i32, i32* %ref_idx.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [9 x i8], [9 x i8]* @comp_ref1.lut, i32 0, i32 %0
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  ret i8 %1
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_drl_ctx(i16* %ref_mv_weight, i32 %ref_idx) #2 {
entry:
  %retval = alloca i8, align 1
  %ref_mv_weight.addr = alloca i16*, align 4
  %ref_idx.addr = alloca i32, align 4
  store i16* %ref_mv_weight, i16** %ref_mv_weight.addr, align 4, !tbaa !6
  store i32 %ref_idx, i32* %ref_idx.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !6
  %1 = load i32, i32* %ref_idx.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %0, i32 %1
  %2 = load i16, i16* %arrayidx, align 2, !tbaa !101
  %conv = zext i16 %2 to i32
  %cmp = icmp sge i32 %conv, 640
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %3 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !6
  %4 = load i32, i32* %ref_idx.addr, align 4, !tbaa !2
  %add = add nsw i32 %4, 1
  %arrayidx2 = getelementptr inbounds i16, i16* %3, i32 %add
  %5 = load i16, i16* %arrayidx2, align 2, !tbaa !101
  %conv3 = zext i16 %5 to i32
  %cmp4 = icmp sge i32 %conv3, 640
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i8 0, i8* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %6 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !6
  %7 = load i32, i32* %ref_idx.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx6, align 2, !tbaa !101
  %conv7 = zext i16 %8 to i32
  %cmp8 = icmp sge i32 %conv7, 640
  br i1 %cmp8, label %land.lhs.true10, label %if.end17

land.lhs.true10:                                  ; preds = %if.end
  %9 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !6
  %10 = load i32, i32* %ref_idx.addr, align 4, !tbaa !2
  %add11 = add nsw i32 %10, 1
  %arrayidx12 = getelementptr inbounds i16, i16* %9, i32 %add11
  %11 = load i16, i16* %arrayidx12, align 2, !tbaa !101
  %conv13 = zext i16 %11 to i32
  %cmp14 = icmp slt i32 %conv13, 640
  br i1 %cmp14, label %if.then16, label %if.end17

if.then16:                                        ; preds = %land.lhs.true10
  store i8 1, i8* %retval, align 1
  br label %return

if.end17:                                         ; preds = %land.lhs.true10, %if.end
  %12 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !6
  %13 = load i32, i32* %ref_idx.addr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx18, align 2, !tbaa !101
  %conv19 = zext i16 %14 to i32
  %cmp20 = icmp slt i32 %conv19, 640
  br i1 %cmp20, label %land.lhs.true22, label %if.end29

land.lhs.true22:                                  ; preds = %if.end17
  %15 = load i16*, i16** %ref_mv_weight.addr, align 4, !tbaa !6
  %16 = load i32, i32* %ref_idx.addr, align 4, !tbaa !2
  %add23 = add nsw i32 %16, 1
  %arrayidx24 = getelementptr inbounds i16, i16* %15, i32 %add23
  %17 = load i16, i16* %arrayidx24, align 2, !tbaa !101
  %conv25 = zext i16 %17 to i32
  %cmp26 = icmp slt i32 %conv25, 640
  br i1 %cmp26, label %if.then28, label %if.end29

if.then28:                                        ; preds = %land.lhs.true22
  store i8 2, i8* %retval, align 1
  br label %return

if.end29:                                         ; preds = %land.lhs.true22, %if.end17
  store i8 0, i8* %retval, align 1
  br label %return

return:                                           ; preds = %if.end29, %if.then28, %if.then16, %if.then
  %18 = load i8, i8* %retval, align 1
  ret i8 %18
}

; Function Attrs: inlinehint nounwind
define internal void @integer_mv_precision(%struct.mv* %mv) #2 {
entry:
  %mv.addr = alloca %struct.mv*, align 4
  %mod = alloca i32, align 4
  store %struct.mv* %mv, %struct.mv** %mv.addr, align 4, !tbaa !6
  %0 = bitcast i32* %mod to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row = getelementptr inbounds %struct.mv, %struct.mv* %1, i32 0, i32 0
  %2 = load i16, i16* %row, align 2, !tbaa !143
  %conv = sext i16 %2 to i32
  %rem = srem i32 %conv, 8
  store i32 %rem, i32* %mod, align 4, !tbaa !2
  %3 = load i32, i32* %mod, align 4, !tbaa !2
  %cmp = icmp ne i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end19

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %mod, align 4, !tbaa !2
  %5 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row2 = getelementptr inbounds %struct.mv, %struct.mv* %5, i32 0, i32 0
  %6 = load i16, i16* %row2, align 2, !tbaa !143
  %conv3 = sext i16 %6 to i32
  %sub = sub nsw i32 %conv3, %4
  %conv4 = trunc i32 %sub to i16
  store i16 %conv4, i16* %row2, align 2, !tbaa !143
  %7 = load i32, i32* %mod, align 4, !tbaa !2
  %call = call i32 @abs(i32 %7) #7
  %cmp5 = icmp sgt i32 %call, 4
  br i1 %cmp5, label %if.then7, label %if.end18

if.then7:                                         ; preds = %if.then
  %8 = load i32, i32* %mod, align 4, !tbaa !2
  %cmp8 = icmp sgt i32 %8, 0
  br i1 %cmp8, label %if.then10, label %if.else

if.then10:                                        ; preds = %if.then7
  %9 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row11 = getelementptr inbounds %struct.mv, %struct.mv* %9, i32 0, i32 0
  %10 = load i16, i16* %row11, align 2, !tbaa !143
  %conv12 = sext i16 %10 to i32
  %add = add nsw i32 %conv12, 8
  %conv13 = trunc i32 %add to i16
  store i16 %conv13, i16* %row11, align 2, !tbaa !143
  br label %if.end

if.else:                                          ; preds = %if.then7
  %11 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %row14 = getelementptr inbounds %struct.mv, %struct.mv* %11, i32 0, i32 0
  %12 = load i16, i16* %row14, align 2, !tbaa !143
  %conv15 = sext i16 %12 to i32
  %sub16 = sub nsw i32 %conv15, 8
  %conv17 = trunc i32 %sub16 to i16
  store i16 %conv17, i16* %row14, align 2, !tbaa !143
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then10
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %entry
  %13 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col = getelementptr inbounds %struct.mv, %struct.mv* %13, i32 0, i32 1
  %14 = load i16, i16* %col, align 2, !tbaa !146
  %conv20 = sext i16 %14 to i32
  %rem21 = srem i32 %conv20, 8
  store i32 %rem21, i32* %mod, align 4, !tbaa !2
  %15 = load i32, i32* %mod, align 4, !tbaa !2
  %cmp22 = icmp ne i32 %15, 0
  br i1 %cmp22, label %if.then24, label %if.end47

if.then24:                                        ; preds = %if.end19
  %16 = load i32, i32* %mod, align 4, !tbaa !2
  %17 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col25 = getelementptr inbounds %struct.mv, %struct.mv* %17, i32 0, i32 1
  %18 = load i16, i16* %col25, align 2, !tbaa !146
  %conv26 = sext i16 %18 to i32
  %sub27 = sub nsw i32 %conv26, %16
  %conv28 = trunc i32 %sub27 to i16
  store i16 %conv28, i16* %col25, align 2, !tbaa !146
  %19 = load i32, i32* %mod, align 4, !tbaa !2
  %call29 = call i32 @abs(i32 %19) #7
  %cmp30 = icmp sgt i32 %call29, 4
  br i1 %cmp30, label %if.then32, label %if.end46

if.then32:                                        ; preds = %if.then24
  %20 = load i32, i32* %mod, align 4, !tbaa !2
  %cmp33 = icmp sgt i32 %20, 0
  br i1 %cmp33, label %if.then35, label %if.else40

if.then35:                                        ; preds = %if.then32
  %21 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col36 = getelementptr inbounds %struct.mv, %struct.mv* %21, i32 0, i32 1
  %22 = load i16, i16* %col36, align 2, !tbaa !146
  %conv37 = sext i16 %22 to i32
  %add38 = add nsw i32 %conv37, 8
  %conv39 = trunc i32 %add38 to i16
  store i16 %conv39, i16* %col36, align 2, !tbaa !146
  br label %if.end45

if.else40:                                        ; preds = %if.then32
  %23 = load %struct.mv*, %struct.mv** %mv.addr, align 4, !tbaa !6
  %col41 = getelementptr inbounds %struct.mv, %struct.mv* %23, i32 0, i32 1
  %24 = load i16, i16* %col41, align 2, !tbaa !146
  %conv42 = sext i16 %24 to i32
  %sub43 = sub nsw i32 %conv42, 8
  %conv44 = trunc i32 %sub43 to i16
  store i16 %conv44, i16* %col41, align 2, !tbaa !146
  br label %if.end45

if.end45:                                         ; preds = %if.else40, %if.then35
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.then24
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.end19
  %25 = bitcast i32* %mod to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #6
  ret void
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #5

; Function Attrs: inlinehint nounwind
define internal void @gm_get_motion_vector(%union.int_mv* noalias sret align 4 %agg.result, %struct.WarpedMotionParams* %gm, i32 %allow_hp, i8 zeroext %bsize, i32 %mi_col, i32 %mi_row, i32 %is_integer) #2 {
entry:
  %gm.addr = alloca %struct.WarpedMotionParams*, align 4
  %allow_hp.addr = alloca i32, align 4
  %bsize.addr = alloca i8, align 1
  %mi_col.addr = alloca i32, align 4
  %mi_row.addr = alloca i32, align 4
  %is_integer.addr = alloca i32, align 4
  %mat = alloca i32*, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %tx = alloca i32, align 4
  %ty = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %xc = alloca i32, align 4
  %yc = alloca i32, align 4
  store %struct.WarpedMotionParams* %gm, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !6
  store i32 %allow_hp, i32* %allow_hp.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !2
  store i32 %is_integer, i32* %is_integer.addr, align 4, !tbaa !2
  %0 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !6
  %wmtype = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %0, i32 0, i32 5
  %1 = load i8, i8* %wmtype, align 4, !tbaa !181
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %as_int = bitcast %union.int_mv* %agg.result to i32*
  store i32 0, i32* %as_int, align 4, !tbaa !8
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !6
  %wmmat = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %3, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat, i32 0, i32 0
  store i32* %arraydecay, i32** %mat, align 4, !tbaa !6
  %4 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = bitcast i32* %tx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = bitcast i32* %ty to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !6
  %wmtype2 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %8, i32 0, i32 5
  %9 = load i8, i8* %wmtype2, align 4, !tbaa !181
  %conv3 = zext i8 %9 to i32
  %cmp4 = icmp eq i32 %conv3, 1
  br i1 %cmp4, label %if.then6, label %if.end17

if.then6:                                         ; preds = %if.end
  %10 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !6
  %wmmat7 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %10, i32 0, i32 0
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat7, i32 0, i32 0
  %11 = load i32, i32* %arrayidx, align 4, !tbaa !2
  %shr = ashr i32 %11, 13
  %conv8 = trunc i32 %shr to i16
  %as_mv = bitcast %union.int_mv* %agg.result to %struct.mv*
  %row = getelementptr inbounds %struct.mv, %struct.mv* %as_mv, i32 0, i32 0
  store i16 %conv8, i16* %row, align 4, !tbaa !8
  %12 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !6
  %wmmat9 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %12, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [8 x i32], [8 x i32]* %wmmat9, i32 0, i32 1
  %13 = load i32, i32* %arrayidx10, align 4, !tbaa !2
  %shr11 = ashr i32 %13, 13
  %conv12 = trunc i32 %shr11 to i16
  %as_mv13 = bitcast %union.int_mv* %agg.result to %struct.mv*
  %col = getelementptr inbounds %struct.mv, %struct.mv* %as_mv13, i32 0, i32 1
  store i16 %conv12, i16* %col, align 2, !tbaa !8
  %14 = load i32, i32* %is_integer.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.then6
  %as_mv15 = bitcast %union.int_mv* %agg.result to %struct.mv*
  call void @integer_mv_precision(%struct.mv* %as_mv15)
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.then6
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %if.end
  %15 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %16 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %call = call i32 @block_center_x(i32 %15, i8 zeroext %16)
  store i32 %call, i32* %x, align 4, !tbaa !2
  %17 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %18 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %call18 = call i32 @block_center_y(i32 %17, i8 zeroext %18)
  store i32 %call18, i32* %y, align 4, !tbaa !2
  %19 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm.addr, align 4, !tbaa !6
  %wmtype19 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %19, i32 0, i32 5
  %20 = load i8, i8* %wmtype19, align 4, !tbaa !181
  %conv20 = zext i8 %20 to i32
  %cmp21 = icmp eq i32 %conv20, 2
  br i1 %cmp21, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end17
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %if.end17
  %21 = bitcast i32* %xc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load i32*, i32** %mat, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds i32, i32* %22, i32 2
  %23 = load i32, i32* %arrayidx25, align 4, !tbaa !2
  %sub = sub nsw i32 %23, 65536
  %24 = load i32, i32* %x, align 4, !tbaa !2
  %mul = mul nsw i32 %sub, %24
  %25 = load i32*, i32** %mat, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds i32, i32* %25, i32 3
  %26 = load i32, i32* %arrayidx26, align 4, !tbaa !2
  %27 = load i32, i32* %y, align 4, !tbaa !2
  %mul27 = mul nsw i32 %26, %27
  %add = add nsw i32 %mul, %mul27
  %28 = load i32*, i32** %mat, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds i32, i32* %28, i32 0
  %29 = load i32, i32* %arrayidx28, align 4, !tbaa !2
  %add29 = add nsw i32 %add, %29
  store i32 %add29, i32* %xc, align 4, !tbaa !2
  %30 = bitcast i32* %yc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load i32*, i32** %mat, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds i32, i32* %31, i32 4
  %32 = load i32, i32* %arrayidx30, align 4, !tbaa !2
  %33 = load i32, i32* %x, align 4, !tbaa !2
  %mul31 = mul nsw i32 %32, %33
  %34 = load i32*, i32** %mat, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds i32, i32* %34, i32 5
  %35 = load i32, i32* %arrayidx32, align 4, !tbaa !2
  %sub33 = sub nsw i32 %35, 65536
  %36 = load i32, i32* %y, align 4, !tbaa !2
  %mul34 = mul nsw i32 %sub33, %36
  %add35 = add nsw i32 %mul31, %mul34
  %37 = load i32*, i32** %mat, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds i32, i32* %37, i32 1
  %38 = load i32, i32* %arrayidx36, align 4, !tbaa !2
  %add37 = add nsw i32 %add35, %38
  store i32 %add37, i32* %yc, align 4, !tbaa !2
  %39 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %40 = load i32, i32* %xc, align 4, !tbaa !2
  %call38 = call i32 @convert_to_trans_prec(i32 %39, i32 %40)
  store i32 %call38, i32* %tx, align 4, !tbaa !2
  %41 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %42 = load i32, i32* %yc, align 4, !tbaa !2
  %call39 = call i32 @convert_to_trans_prec(i32 %41, i32 %42)
  store i32 %call39, i32* %ty, align 4, !tbaa !2
  %43 = load i32, i32* %ty, align 4, !tbaa !2
  %conv40 = trunc i32 %43 to i16
  %as_mv41 = bitcast %union.int_mv* %agg.result to %struct.mv*
  %row42 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv41, i32 0, i32 0
  store i16 %conv40, i16* %row42, align 4, !tbaa !8
  %44 = load i32, i32* %tx, align 4, !tbaa !2
  %conv43 = trunc i32 %44 to i16
  %as_mv44 = bitcast %union.int_mv* %agg.result to %struct.mv*
  %col45 = getelementptr inbounds %struct.mv, %struct.mv* %as_mv44, i32 0, i32 1
  store i16 %conv43, i16* %col45, align 2, !tbaa !8
  %45 = load i32, i32* %is_integer.addr, align 4, !tbaa !2
  %tobool46 = icmp ne i32 %45, 0
  br i1 %tobool46, label %if.then47, label %if.end49

if.then47:                                        ; preds = %if.end24
  %as_mv48 = bitcast %union.int_mv* %agg.result to %struct.mv*
  call void @integer_mv_precision(%struct.mv* %as_mv48)
  br label %if.end49

if.end49:                                         ; preds = %if.then47, %if.end24
  store i32 1, i32* %cleanup.dest.slot, align 4
  %46 = bitcast i32* %yc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #6
  %47 = bitcast i32* %xc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end49, %if.end16
  %48 = bitcast i32* %ty to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #6
  %49 = bitcast i32* %tx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  %50 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  %51 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  %52 = bitcast i32** %mat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #6
  br label %return

return:                                           ; preds = %cleanup, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @block_center_x(i32 %mi_col, i8 zeroext %bs) #2 {
entry:
  %mi_col.addr = alloca i32, align 4
  %bs.addr = alloca i8, align 1
  %bw = alloca i32, align 4
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !2
  store i8 %bs, i8* %bs.addr, align 1, !tbaa !8
  %0 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %bs.addr, align 1, !tbaa !8
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !2
  %3 = load i32, i32* %mi_col.addr, align 4, !tbaa !2
  %mul = mul nsw i32 %3, 4
  %4 = load i32, i32* %bw, align 4, !tbaa !2
  %div = sdiv i32 %4, 2
  %add = add nsw i32 %mul, %div
  %sub = sub nsw i32 %add, 1
  %5 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i32 %sub
}

; Function Attrs: inlinehint nounwind
define internal i32 @block_center_y(i32 %mi_row, i8 zeroext %bs) #2 {
entry:
  %mi_row.addr = alloca i32, align 4
  %bs.addr = alloca i8, align 1
  %bh = alloca i32, align 4
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !2
  store i8 %bs, i8* %bs.addr, align 1, !tbaa !8
  %0 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %bs.addr, align 1, !tbaa !8
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %bh, align 4, !tbaa !2
  %3 = load i32, i32* %mi_row.addr, align 4, !tbaa !2
  %mul = mul nsw i32 %3, 4
  %4 = load i32, i32* %bh, align 4, !tbaa !2
  %div = sdiv i32 %4, 2
  %add = add nsw i32 %mul, %div
  %sub = sub nsw i32 %add, 1
  %5 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret i32 %sub
}

; Function Attrs: inlinehint nounwind
define internal i32 @convert_to_trans_prec(i32 %allow_hp, i32 %coor) #2 {
entry:
  %retval = alloca i32, align 4
  %allow_hp.addr = alloca i32, align 4
  %coor.addr = alloca i32, align 4
  store i32 %allow_hp, i32* %allow_hp.addr, align 4, !tbaa !2
  store i32 %coor, i32* %coor.addr, align 4, !tbaa !2
  %0 = load i32, i32* %allow_hp.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %coor.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %2 = load i32, i32* %coor.addr, align 4, !tbaa !2
  %sub = sub nsw i32 0, %2
  %add = add nsw i32 %sub, 4096
  %shr = ashr i32 %add, 13
  %sub1 = sub nsw i32 0, %shr
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %3 = load i32, i32* %coor.addr, align 4, !tbaa !2
  %add2 = add nsw i32 %3, 4096
  %shr3 = ashr i32 %add2, 13
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub1, %cond.true ], [ %shr3, %cond.false ]
  store i32 %cond, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %coor.addr, align 4, !tbaa !2
  %cmp4 = icmp slt i32 %4, 0
  br i1 %cmp4, label %cond.true5, label %cond.false10

cond.true5:                                       ; preds = %if.else
  %5 = load i32, i32* %coor.addr, align 4, !tbaa !2
  %sub6 = sub nsw i32 0, %5
  %add7 = add nsw i32 %sub6, 8192
  %shr8 = ashr i32 %add7, 14
  %sub9 = sub nsw i32 0, %shr8
  br label %cond.end13

cond.false10:                                     ; preds = %if.else
  %6 = load i32, i32* %coor.addr, align 4, !tbaa !2
  %add11 = add nsw i32 %6, 8192
  %shr12 = ashr i32 %add11, 14
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false10, %cond.true5
  %cond14 = phi i32 [ %sub9, %cond.true5 ], [ %shr12, %cond.false10 ]
  %mul = mul nsw i32 %cond14, 2
  store i32 %mul, i32* %retval, align 4
  br label %return

return:                                           ; preds = %cond.end13, %cond.end
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_interintra_allowed_bsize(i8 zeroext %bsize) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !8
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i8, i8* %bsize.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp sle i32 %conv2, 9
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  %land.ext = zext i1 %2 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_interintra_allowed_mode(i8 zeroext %mode) #2 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 13
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp slt i32 %conv2, 17
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  %land.ext = zext i1 %2 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_interintra_allowed_ref(i8* %rf) #2 {
entry:
  %rf.addr = alloca i8*, align 4
  store i8* %rf, i8** %rf.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 0
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = sext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load i8*, i8** %rf.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8, i8* %2, i32 1
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !8
  %conv3 = sext i8 %3 to i32
  %cmp4 = icmp sle i32 %conv3, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %4 = phi i1 [ false, %entry ], [ %cmp4, %land.rhs ]
  %land.ext = zext i1 %4 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_ref_frame_map_idx(%struct.AV1Common* %cm, i8 signext %ref_frame) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !8
  %0 = load i8, i8* %ref_frame.addr, align 1, !tbaa !8
  %conv = sext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 1
  br i1 %cmp, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %ref_frame.addr, align 1, !tbaa !8
  %conv2 = sext i8 %1 to i32
  %cmp3 = icmp sle i32 %conv2, 8
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %remapped_ref_idx = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 14
  %3 = load i8, i8* %ref_frame.addr, align 1, !tbaa !8
  %conv5 = sext i8 %3 to i32
  %sub = sub nsw i32 %conv5, 1
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %remapped_ref_idx, i32 0, i32 %sub
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ -1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @motion_mode_allowed(%struct.WarpedMotionParams* %gm_params, %struct.macroblockd* %xd, %struct.MB_MODE_INFO* %mbmi, i32 %allow_warped_motion) #2 {
entry:
  %retval = alloca i8, align 1
  %gm_params.addr = alloca %struct.WarpedMotionParams*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %allow_warped_motion.addr = alloca i32, align 4
  %gm_type = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.WarpedMotionParams* %gm_params, %struct.WarpedMotionParams** %gm_params.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  store i32 %allow_warped_motion, i32* %allow_warped_motion.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cur_frame_force_integer_mv = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 45
  %1 = load i32, i32* %cur_frame_force_integer_mv, align 4, !tbaa !182
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %gm_type) #6
  %2 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm_params.addr, align 4, !tbaa !6
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %3, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %4 = load i8, i8* %arrayidx, align 4, !tbaa !8
  %idxprom = sext i8 %4 to i32
  %arrayidx1 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %2, i32 %idxprom
  %wmtype = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %arrayidx1, i32 0, i32 5
  %5 = load i8, i8* %wmtype, align 4, !tbaa !181
  store i8 %5, i8* %gm_type, align 1, !tbaa !8
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %7 = load i8, i8* %gm_type, align 1, !tbaa !8
  %call = call i32 @is_global_mv_block(%struct.MB_MODE_INFO* %6, i8 zeroext %7)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  store i8 0, i8* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %gm_type) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end3

if.end3:                                          ; preds = %cleanup.cont, %entry
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 6
  %9 = load i8, i8* %sb_type, align 2, !tbaa !76
  %call4 = call i32 @is_motion_variation_allowed_bsize(i8 zeroext %9)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.end3
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %10, i32 0, i32 7
  %11 = load i8, i8* %mode, align 1, !tbaa !50
  %call6 = call i32 @is_inter_mode(i8 zeroext %11)
  %tobool7 = icmp ne i32 %call6, 0
  br i1 %tobool7, label %land.lhs.true8, label %if.else

land.lhs.true8:                                   ; preds = %land.lhs.true
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame9 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %12, i32 0, i32 12
  %arrayidx10 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame9, i32 0, i32 1
  %13 = load i8, i8* %arrayidx10, align 1, !tbaa !8
  %conv = sext i8 %13 to i32
  %cmp11 = icmp ne i32 %conv, 0
  br i1 %cmp11, label %land.lhs.true13, label %if.else

land.lhs.true13:                                  ; preds = %land.lhs.true8
  %14 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call14 = call i32 @is_motion_variation_allowed_compound(%struct.MB_MODE_INFO* %14)
  %tobool15 = icmp ne i32 %call14, 0
  br i1 %tobool15, label %if.then16, label %if.else

if.then16:                                        ; preds = %land.lhs.true13
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call17 = call i32 @check_num_overlappable_neighbors(%struct.MB_MODE_INFO* %15)
  %tobool18 = icmp ne i32 %call17, 0
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %if.then16
  store i8 0, i8* %retval, align 1
  br label %return

if.end20:                                         ; preds = %if.then16
  %16 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %num_proj_ref = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %16, i32 0, i32 23
  %17 = load i8, i8* %num_proj_ref, align 4, !tbaa !160
  %conv21 = zext i8 %17 to i32
  %cmp22 = icmp sge i32 %conv21, 1
  br i1 %cmp22, label %land.lhs.true24, label %if.end35

land.lhs.true24:                                  ; preds = %if.end20
  %18 = load i32, i32* %allow_warped_motion.addr, align 4, !tbaa !2
  %tobool25 = icmp ne i32 %18, 0
  br i1 %tobool25, label %land.lhs.true26, label %if.end35

land.lhs.true26:                                  ; preds = %land.lhs.true24
  %19 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %block_ref_scale_factors = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %19, i32 0, i32 21
  %arrayidx27 = getelementptr inbounds [2 x %struct.scale_factors*], [2 x %struct.scale_factors*]* %block_ref_scale_factors, i32 0, i32 0
  %20 = load %struct.scale_factors*, %struct.scale_factors** %arrayidx27, align 4, !tbaa !6
  %call28 = call i32 @av1_is_scaled(%struct.scale_factors* %20)
  %tobool29 = icmp ne i32 %call28, 0
  br i1 %tobool29, label %if.end35, label %if.then30

if.then30:                                        ; preds = %land.lhs.true26
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %cur_frame_force_integer_mv31 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %21, i32 0, i32 45
  %22 = load i32, i32* %cur_frame_force_integer_mv31, align 4, !tbaa !182
  %tobool32 = icmp ne i32 %22, 0
  br i1 %tobool32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %if.then30
  store i8 1, i8* %retval, align 1
  br label %return

if.end34:                                         ; preds = %if.then30
  store i8 2, i8* %retval, align 1
  br label %return

if.end35:                                         ; preds = %land.lhs.true26, %land.lhs.true24, %if.end20
  store i8 1, i8* %retval, align 1
  br label %return

if.else:                                          ; preds = %land.lhs.true13, %land.lhs.true8, %land.lhs.true, %if.end3
  store i8 0, i8* %retval, align 1
  br label %return

return:                                           ; preds = %if.else, %if.end35, %if.end34, %if.then33, %if.then19, %cleanup
  %23 = load i8, i8* %retval, align 1
  ret i8 %23

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_global_mv_block(%struct.MB_MODE_INFO* %mbmi, i8 zeroext %type) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %type.addr = alloca i8, align 1
  %mode = alloca i8, align 1
  %bsize = alloca i8, align 1
  %block_size_allowed = alloca i32, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  store i8 %type, i8* %type.addr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mode) #6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 7
  %1 = load i8, i8* %mode1, align 1, !tbaa !50
  store i8 %1, i8* %mode, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #6
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 6
  %3 = load i8, i8* %sb_type, align 2, !tbaa !76
  store i8 %3, i8* %bsize, align 1, !tbaa !8
  %4 = bitcast i32* %block_size_allowed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %6 to i32
  %7 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom2 = zext i8 %7 to i32
  %arrayidx3 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom2
  %8 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  %conv4 = zext i8 %8 to i32
  %cmp = icmp slt i32 %conv, %conv4
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %9 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom6 = zext i8 %9 to i32
  %arrayidx7 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom6
  %10 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  %conv8 = zext i8 %10 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i8, i8* %bsize, align 1, !tbaa !8
  %idxprom9 = zext i8 %11 to i32
  %arrayidx10 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom9
  %12 = load i8, i8* %arrayidx10, align 1, !tbaa !8
  %conv11 = zext i8 %12 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv8, %cond.true ], [ %conv11, %cond.false ]
  %cmp12 = icmp sge i32 %cond, 8
  %conv13 = zext i1 %cmp12 to i32
  store i32 %conv13, i32* %block_size_allowed, align 4, !tbaa !2
  %13 = load i8, i8* %mode, align 1, !tbaa !8
  %conv14 = zext i8 %13 to i32
  %cmp15 = icmp eq i32 %conv14, 15
  br i1 %cmp15, label %land.lhs.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end
  %14 = load i8, i8* %mode, align 1, !tbaa !8
  %conv17 = zext i8 %14 to i32
  %cmp18 = icmp eq i32 %conv17, 23
  br i1 %cmp18, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %lor.lhs.false, %cond.end
  %15 = load i8, i8* %type.addr, align 1, !tbaa !8
  %conv20 = zext i8 %15 to i32
  %cmp21 = icmp sgt i32 %conv20, 1
  br i1 %cmp21, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %16 = load i32, i32* %block_size_allowed, align 4, !tbaa !2
  %tobool = icmp ne i32 %16, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %lor.lhs.false
  %17 = phi i1 [ false, %land.lhs.true ], [ false, %lor.lhs.false ], [ %tobool, %land.rhs ]
  %land.ext = zext i1 %17 to i32
  %18 = bitcast i32* %block_size_allowed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mode) #6
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_mode(i8 zeroext %mode) #2 {
entry:
  %mode.addr = alloca i8, align 1
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !8
  %0 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 13
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i8, i8* %mode.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp slt i32 %conv2, 25
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  %land.ext = zext i1 %2 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_motion_variation_allowed_compound(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call = call i32 @has_second_ref(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  ret i32 %lnot.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @check_num_overlappable_neighbors(%struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %overlappable_neighbors = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 24
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %overlappable_neighbors, i32 0, i32 0
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %overlappable_neighbors2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 24
  %arrayidx3 = getelementptr inbounds [2 x i8], [2 x i8]* %overlappable_neighbors2, i32 0, i32 1
  %3 = load i8, i8* %arrayidx3, align 1, !tbaa !8
  %conv4 = zext i8 %3 to i32
  %cmp5 = icmp eq i32 %conv4, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %4 = phi i1 [ false, %entry ], [ %cmp5, %land.rhs ]
  %lnot = xor i1 %4, true
  %lnot.ext = zext i1 %lnot to i32
  ret i32 %lnot.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_scaled(%struct.scale_factors* %sf) #2 {
entry:
  %sf.addr = alloca %struct.scale_factors*, align 4
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !6
  %0 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !6
  %call = call i32 @av1_is_valid_scale(%struct.scale_factors* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !6
  %x_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %1, i32 0, i32 0
  %2 = load i32, i32* %x_scale_fp, align 4, !tbaa !183
  %cmp = icmp ne i32 %2, 16384
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.rhs
  %3 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !6
  %y_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %3, i32 0, i32 1
  %4 = load i32, i32* %y_scale_fp, align 4, !tbaa !184
  %cmp1 = icmp ne i32 %4, 16384
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.rhs
  %5 = phi i1 [ true, %land.rhs ], [ %cmp1, %lor.rhs ]
  br label %land.end

land.end:                                         ; preds = %lor.end, %entry
  %6 = phi i1 [ false, %entry ], [ %5, %lor.end ]
  %land.ext = zext i1 %6 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_valid_scale(%struct.scale_factors* %sf) #2 {
entry:
  %sf.addr = alloca %struct.scale_factors*, align 4
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !6
  %0 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !6
  %x_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %0, i32 0, i32 0
  %1 = load i32, i32* %x_scale_fp, align 4, !tbaa !183
  %cmp = icmp ne i32 %1, -1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !6
  %y_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %2, i32 0, i32 1
  %3 = load i32, i32* %y_scale_fp, align 4, !tbaa !184
  %cmp1 = icmp ne i32 %3, -1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %4 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  %land.ext = zext i1 %4 to i32
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_masked_compound_type(i8 zeroext %type) #2 {
entry:
  %type.addr = alloca i8, align 1
  store i8 %type, i8* %type.addr, align 1, !tbaa !8
  %0 = load i8, i8* %type.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 2
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i8, i8* %type.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp eq i32 %conv2, 3
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp3, %lor.rhs ]
  %lor.ext = zext i1 %2 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %cm, i8 signext %ref_frame) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  %map_idx = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !8
  %0 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %2 = load i8, i8* %ref_frame.addr, align 1, !tbaa !8
  %call = call i32 @get_ref_frame_map_idx(%struct.AV1Common* %1, i8 signext %2)
  store i32 %call, i32* %map_idx, align 4, !tbaa !2
  %3 = load i32, i32* %map_idx, align 4, !tbaa !2
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 17
  %5 = load i32, i32* %map_idx, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %5
  %6 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.RefCntBuffer* [ %6, %cond.true ], [ null, %cond.false ]
  %7 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret %struct.RefCntBuffer* %cond
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_relative_dist(%struct.OrderHintInfo* %oh, i32 %a, i32 %b) #2 {
entry:
  %retval = alloca i32, align 4
  %oh.addr = alloca %struct.OrderHintInfo*, align 4
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  %bits = alloca i32, align 4
  %diff = alloca i32, align 4
  %m = alloca i32, align 4
  store %struct.OrderHintInfo* %oh, %struct.OrderHintInfo** %oh.addr, align 4, !tbaa !6
  store i32 %a, i32* %a.addr, align 4, !tbaa !2
  store i32 %b, i32* %b.addr, align 4, !tbaa !2
  %0 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %oh.addr, align 4, !tbaa !6
  %enable_order_hint = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %0, i32 0, i32 0
  %1 = load i32, i32* %enable_order_hint, align 4, !tbaa !185
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %oh.addr, align 4, !tbaa !6
  %order_hint_bits_minus_1 = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %3, i32 0, i32 1
  %4 = load i32, i32* %order_hint_bits_minus_1, align 4, !tbaa !186
  %add = add nsw i32 %4, 1
  store i32 %add, i32* %bits, align 4, !tbaa !2
  %5 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i32, i32* %a.addr, align 4, !tbaa !2
  %7 = load i32, i32* %b.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %6, %7
  store i32 %sub, i32* %diff, align 4, !tbaa !2
  %8 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %bits, align 4, !tbaa !2
  %sub1 = sub nsw i32 %9, 1
  %shl = shl i32 1, %sub1
  store i32 %shl, i32* %m, align 4, !tbaa !2
  %10 = load i32, i32* %diff, align 4, !tbaa !2
  %11 = load i32, i32* %m, align 4, !tbaa !2
  %sub2 = sub nsw i32 %11, 1
  %and = and i32 %10, %sub2
  %12 = load i32, i32* %diff, align 4, !tbaa !2
  %13 = load i32, i32* %m, align 4, !tbaa !2
  %and3 = and i32 %12, %13
  %sub4 = sub nsw i32 %and, %and3
  store i32 %sub4, i32* %diff, align 4, !tbaa !2
  %14 = load i32, i32* %diff, align 4, !tbaa !2
  store i32 %14, i32* %retval, align 4
  %15 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %return

return:                                           ; preds = %if.end, %if.then
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_interp_needed(%struct.macroblockd* %xd) #2 {
entry:
  %retval = alloca i32, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %skip_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %4, i32 0, i32 26
  %bf.load = load i8, i8* %skip_mode, align 4
  %bf.lshr = lshr i8 %bf.load, 5
  %bf.clear = and i8 %bf.lshr, 1
  %tobool = icmp ne i8 %bf.clear, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !6
  %motion_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 10
  %6 = load i8, i8* %motion_mode, align 2, !tbaa !123
  %conv = zext i8 %6 to i32
  %cmp = icmp eq i32 %conv, 2
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %mi4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 6
  %9 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi4, align 4, !tbaa !9
  %arrayidx5 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %9, i32 0
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx5, align 4, !tbaa !6
  %call = call i32 @is_nontrans_global_motion(%struct.macroblockd* %7, %struct.MB_MODE_INFO* %10)
  %tobool6 = icmp ne i32 %call, 0
  br i1 %tobool6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end3
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then7, %if.then2, %if.then
  %11 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: inlinehint nounwind
define internal void @set_default_interp_filters(%struct.MB_MODE_INFO* %mbmi, i8 zeroext %frame_interp_filter) #2 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %frame_interp_filter.addr = alloca i8, align 1
  %tmp = alloca %union.int_interpfilters, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  store i8 %frame_interp_filter, i8* %frame_interp_filter.addr, align 1, !tbaa !8
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %interp_filters = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 4
  %1 = bitcast %union.int_interpfilters* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i8, i8* %frame_interp_filter.addr, align 1, !tbaa !8
  %call = call zeroext i8 @av1_unswitchable_filter(i8 zeroext %2)
  call void @av1_broadcast_interp_filter(%union.int_interpfilters* sret align 4 %tmp, i8 zeroext %call)
  %3 = bitcast %union.int_interpfilters* %interp_filters to i8*
  %4 = bitcast %union.int_interpfilters* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false), !tbaa.struct !122
  %5 = bitcast %union.int_interpfilters* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

declare i32 @av1_get_pred_context_switchable_interp(%struct.macroblockd*, i32) #3

; Function Attrs: inlinehint nounwind
define internal i32 @is_nontrans_global_motion(%struct.macroblockd* %xd, %struct.MB_MODE_INFO* %mbmi) #2 {
entry:
  %retval = alloca i32, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %ref = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 7
  %2 = load i8, i8* %mode, align 1, !tbaa !50
  %conv = zext i8 %2 to i32
  %cmp = icmp ne i32 %conv, 15
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %mode2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %3, i32 0, i32 7
  %4 = load i8, i8* %mode2, align 1, !tbaa !50
  %conv3 = zext i8 %4 to i32
  %cmp4 = icmp ne i32 %conv3, 23
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %entry
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 6
  %6 = load i8, i8* %sb_type, align 2, !tbaa !76
  %idxprom = zext i8 %6 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv6 = zext i8 %7 to i32
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type7 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 6
  %9 = load i8, i8* %sb_type7, align 2, !tbaa !76
  %idxprom8 = zext i8 %9 to i32
  %arrayidx9 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom8
  %10 = load i8, i8* %arrayidx9, align 1, !tbaa !8
  %conv10 = zext i8 %10 to i32
  %cmp11 = icmp slt i32 %conv6, %conv10
  br i1 %cmp11, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type13 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %11, i32 0, i32 6
  %12 = load i8, i8* %sb_type13, align 2, !tbaa !76
  %idxprom14 = zext i8 %12 to i32
  %arrayidx15 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom14
  %13 = load i8, i8* %arrayidx15, align 1, !tbaa !8
  %conv16 = zext i8 %13 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %14 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %sb_type17 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %14, i32 0, i32 6
  %15 = load i8, i8* %sb_type17, align 2, !tbaa !76
  %idxprom18 = zext i8 %15 to i32
  %arrayidx19 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom18
  %16 = load i8, i8* %arrayidx19, align 1, !tbaa !8
  %conv20 = zext i8 %16 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv16, %cond.true ], [ %conv20, %cond.false ]
  %cmp21 = icmp slt i32 %cond, 2
  br i1 %cmp21, label %if.then23, label %if.end24

if.then23:                                        ; preds = %cond.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end24:                                         ; preds = %cond.end
  store i32 0, i32* %ref, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end24
  %17 = load i32, i32* %ref, align 4, !tbaa !2
  %18 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %call = call i32 @has_second_ref(%struct.MB_MODE_INFO* %18)
  %add = add nsw i32 1, %call
  %cmp25 = icmp slt i32 %17, %add
  br i1 %cmp25, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !6
  %global_motion = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %19, i32 0, i32 47
  %20 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %global_motion, align 4, !tbaa !175
  %21 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !6
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %21, i32 0, i32 12
  %22 = load i32, i32* %ref, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 %22
  %23 = load i8, i8* %arrayidx27, align 1, !tbaa !8
  %idxprom28 = sext i8 %23 to i32
  %arrayidx29 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %20, i32 %idxprom28
  %wmtype = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %arrayidx29, i32 0, i32 5
  %24 = load i8, i8* %wmtype, align 4, !tbaa !181
  %conv30 = zext i8 %24 to i32
  %cmp31 = icmp eq i32 %conv30, 1
  br i1 %cmp31, label %if.then33, label %if.end34

if.then33:                                        ; preds = %for.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end34:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end34
  %25 = load i32, i32* %ref, align 4, !tbaa !2
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %ref, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then33, %if.then23, %if.then
  %26 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  %27 = load i32, i32* %retval, align 4
  ret i32 %27
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @av1_unswitchable_filter(i8 zeroext %filter) #2 {
entry:
  %filter.addr = alloca i8, align 1
  store i8 %filter, i8* %filter.addr, align 1, !tbaa !8
  %0 = load i8, i8* %filter.addr, align 1, !tbaa !8
  %conv = zext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 4
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %1 = load i8, i8* %filter.addr, align 1, !tbaa !8
  %conv2 = zext i8 %1 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %conv2, %cond.false ]
  %conv3 = trunc i32 %cond to i8
  ret i8 %conv3
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"any pointer", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !7, i64 4084}
!10 = !{!"macroblockd", !3, i64 0, !3, i64 4, !3, i64 8, !11, i64 12, !4, i64 16, !12, i64 4060, !7, i64 4084, !11, i64 4088, !11, i64 4089, !11, i64 4090, !11, i64 4091, !7, i64 4092, !7, i64 4096, !7, i64 4100, !7, i64 4104, !7, i64 4108, !3, i64 4112, !3, i64 4116, !3, i64 4120, !3, i64 4124, !3, i64 4128, !4, i64 4132, !7, i64 4140, !4, i64 4144, !4, i64 4156, !7, i64 4252, !4, i64 4256, !7, i64 4288, !7, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !7, i64 6832, !3, i64 6836, !4, i64 6840, !4, i64 6872, !3, i64 6904, !3, i64 6908, !7, i64 6912, !7, i64 6916, !3, i64 6920, !3, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !13, i64 39720, !14, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !7, i64 44008, !4, i64 44012}
!11 = !{!"_Bool", !4, i64 0}
!12 = !{!"TileInfo", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20}
!13 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !3, i64 4104, !4, i64 4108, !3, i64 4236, !3, i64 4240, !3, i64 4244, !3, i64 4248, !3, i64 4252, !3, i64 4256}
!14 = !{!"dist_wtd_comp_params", !3, i64 0, !3, i64 4, !3, i64 8}
!15 = !{!10, !7, i64 4108}
!16 = !{!10, !3, i64 4112}
!17 = !{!18, !4, i64 128}
!18 = !{!"MB_MODE_INFO", !19, i64 0, !20, i64 8, !4, i64 52, !3, i64 60, !4, i64 64, !22, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !23, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!19 = !{!"", !7, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!20 = !{!"", !4, i64 0, !21, i64 32, !21, i64 34, !21, i64 36, !21, i64 38, !4, i64 40, !4, i64 41}
!21 = !{!"short", !4, i64 0}
!22 = !{!"", !4, i64 0, !4, i64 48}
!23 = !{!"", !4, i64 0, !4, i64 1}
!24 = !{!25, !11, i64 1337}
!25 = !{!"AV1Common", !26, i64 0, !28, i64 40, !3, i64 288, !3, i64 292, !3, i64 296, !3, i64 300, !3, i64 304, !3, i64 308, !4, i64 312, !11, i64 313, !4, i64 316, !3, i64 448, !7, i64 452, !7, i64 456, !4, i64 460, !29, i64 492, !4, i64 580, !4, i64 1284, !3, i64 1316, !3, i64 1320, !3, i64 1324, !30, i64 1328, !31, i64 1356, !32, i64 1420, !33, i64 10676, !7, i64 10848, !34, i64 10864, !35, i64 14704, !4, i64 14740, !7, i64 14872, !7, i64 14876, !36, i64 14880, !38, i64 15028, !39, i64 15168, !40, i64 15816, !4, i64 15836, !41, i64 16192, !7, i64 18128, !7, i64 18132, !44, i64 18136, !7, i64 18724, !45, i64 18728, !3, i64 18760, !4, i64 18764, !7, i64 18796, !3, i64 18800, !4, i64 18804, !4, i64 18836, !3, i64 18844, !3, i64 18848, !3, i64 18852, !3, i64 18856}
!26 = !{!"", !4, i64 0, !4, i64 1, !3, i64 4, !3, i64 8, !3, i64 12, !27, i64 16, !3, i64 32, !3, i64 36}
!27 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!28 = !{!"aom_internal_error_info", !4, i64 0, !3, i64 4, !4, i64 8, !3, i64 88, !4, i64 92}
!29 = !{!"scale_factors", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56}
!30 = !{!"", !11, i64 0, !11, i64 1, !11, i64 2, !11, i64 3, !11, i64 4, !11, i64 5, !11, i64 6, !11, i64 7, !11, i64 8, !11, i64 9, !11, i64 10, !11, i64 11, !4, i64 12, !4, i64 13, !3, i64 16, !3, i64 20, !4, i64 24}
!31 = !{!"CommonModeInfoParams", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !3, i64 24, !3, i64 28, !4, i64 32, !7, i64 36, !3, i64 40, !3, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60}
!32 = !{!"CommonQuantParams", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !11, i64 9240, !3, i64 9244, !3, i64 9248, !3, i64 9252}
!33 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !3, i64 164, !4, i64 168}
!34 = !{!"", !4, i64 0, !4, i64 3072}
!35 = !{!"loopfilter", !4, i64 0, !3, i64 8, !3, i64 12, !3, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !3, i64 32}
!36 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !3, i64 52, !4, i64 56, !7, i64 68, !3, i64 72, !7, i64 76, !37, i64 80, !3, i64 84, !37, i64 88, !3, i64 92, !3, i64 96, !3, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !3, i64 128, !3, i64 132, !3, i64 136, !3, i64 140, !7, i64 144}
!37 = !{!"long", !4, i64 0}
!38 = !{!"", !3, i64 0, !3, i64 4, !4, i64 8, !4, i64 72, !3, i64 136}
!39 = !{!"", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 120, !4, i64 124, !3, i64 204, !4, i64 208, !3, i64 288, !3, i64 292, !3, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !3, i64 596, !3, i64 600, !3, i64 604, !3, i64 608, !3, i64 612, !3, i64 616, !3, i64 620, !3, i64 624, !3, i64 628, !3, i64 632, !3, i64 636, !3, i64 640, !21, i64 644}
!40 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!41 = !{!"SequenceHeader", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !4, i64 16, !3, i64 20, !3, i64 24, !4, i64 28, !3, i64 32, !3, i64 36, !27, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !3, i64 92, !3, i64 96, !3, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !3, i64 112, !4, i64 116, !3, i64 244, !42, i64 248, !4, i64 264, !43, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!42 = !{!"aom_timing", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!43 = !{!"aom_dec_model_info", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!44 = !{!"CommonTileParams", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !4, i64 60, !4, i64 320, !3, i64 580, !3, i64 584}
!45 = !{!"CommonContexts", !7, i64 0, !4, i64 4, !7, i64 16, !3, i64 20, !3, i64 24, !3, i64 28}
!46 = !{i8 0, i8 2}
!47 = !{!10, !7, i64 6832}
!48 = !{!18, !4, i64 127}
!49 = !{!18, !4, i64 126}
!50 = !{!18, !4, i64 119}
!51 = !{!33, !4, i64 0}
!52 = !{!53, !4, i64 36}
!53 = !{!"aom_reader", !7, i64 0, !7, i64 4, !54, i64 8, !7, i64 32, !4, i64 36}
!54 = !{!"od_ec_dec", !7, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !21, i64 20, !21, i64 22}
!55 = !{!56, !3, i64 60276}
!56 = !{!"AV1Decoder", !10, i64 0, !25, i64 44032, !57, i64 62896, !58, i64 62924, !59, i64 62964, !60, i64 63008, !7, i64 63188, !3, i64 63192, !7, i64 63196, !61, i64 63200, !7, i64 348960, !3, i64 348964, !4, i64 348968, !63, i64 381736, !3, i64 381756, !4, i64 381760, !37, i64 381776, !3, i64 381780, !3, i64 381784, !3, i64 381788, !3, i64 381792, !3, i64 381796, !3, i64 381800, !3, i64 381804, !3, i64 381808, !3, i64 381812, !3, i64 381816, !3, i64 381820, !64, i64 381824, !3, i64 384924, !3, i64 384928, !3, i64 384932, !3, i64 384936, !3, i64 384940, !3, i64 384944, !3, i64 384948, !37, i64 384952, !68, i64 384956, !3, i64 384964, !3, i64 384968, !3, i64 384972, !3, i64 384976, !3, i64 384980, !3, i64 384984, !69, i64 384988, !36, i64 403936, !7, i64 404084, !3, i64 404088, !3, i64 404092, !70, i64 404096, !7, i64 404136, !3, i64 404140, !3, i64 404144, !3, i64 404148, !3, i64 404152, !4, i64 404156}
!57 = !{!"", !7, i64 0, !4, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !3, i64 24}
!58 = !{!"AV1LfSyncData", !4, i64 0, !3, i64 12, !3, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !3, i64 32, !3, i64 36}
!59 = !{!"AV1LrSyncData", !4, i64 0, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !3, i64 36, !3, i64 40}
!60 = !{!"AV1LrStruct", !7, i64 0, !4, i64 4, !7, i64 172, !7, i64 176}
!61 = !{!"ThreadData", !10, i64 0, !62, i64 44032, !7, i64 285696, !4, i64 285700, !3, i64 285708, !3, i64 285712, !7, i64 285716, !4, i64 285720, !7, i64 285728, !7, i64 285732, !7, i64 285736, !7, i64 285740, !7, i64 285744, !7, i64 285748}
!62 = !{!"", !4, i64 0, !4, i64 196608, !4, i64 208896}
!63 = !{!"AV1DecTileMTData", !7, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!64 = !{!"Accounting", !65, i64 0, !3, i64 1044, !4, i64 1048, !67, i64 3090, !3, i64 3096}
!65 = !{!"", !7, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !66, i64 16}
!66 = !{!"", !4, i64 0, !3, i64 1024}
!67 = !{!"", !21, i64 0, !21, i64 2}
!68 = !{!"DataBuffer", !7, i64 0, !37, i64 4}
!69 = !{!"EXTERNAL_REFERENCES", !4, i64 0, !3, i64 18944}
!70 = !{!"AV1DecRowMTInfo", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36}
!71 = !{!10, !3, i64 0}
!72 = !{!10, !3, i64 4}
!73 = !{!25, !4, i64 0}
!74 = !{!10, !7, i64 4096}
!75 = !{!10, !7, i64 4092}
!76 = !{!18, !4, i64 118}
!77 = !{!33, !4, i64 168}
!78 = !{!10, !3, i64 6924}
!79 = !{!18, !3, i64 60}
!80 = !{!25, !7, i64 18744}
!81 = !{!10, !3, i64 4076}
!82 = !{!10, !7, i64 4288}
!83 = !{!10, !7, i64 4292}
!84 = !{!25, !4, i64 16269}
!85 = !{!10, !11, i64 12}
!86 = !{!18, !4, i64 120}
!87 = !{!18, !4, i64 155}
!88 = !{!10, !3, i64 43976}
!89 = !{!25, !11, i64 1331}
!90 = !{!25, !3, i64 1372}
!91 = !{!25, !7, i64 456}
!92 = !{!93, !7, i64 68}
!93 = !{!"RefCntBuffer", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 36, !4, i64 40, !7, i64 68, !7, i64 72, !33, i64 76, !3, i64 248, !3, i64 252, !3, i64 256, !3, i64 260, !4, i64 264, !3, i64 616, !4, i64 620, !39, i64 624, !94, i64 1272, !36, i64 1284, !4, i64 1432, !4, i64 1436, !4, i64 1452, !4, i64 1460, !95, i64 1464}
!94 = !{!"aom_codec_frame_buffer", !7, i64 0, !37, i64 4, !7, i64 8}
!95 = !{!"frame_contexts", !4, i64 0, !4, i64 390, !4, i64 930, !4, i64 966, !4, i64 1014, !4, i64 1070, !4, i64 1134, !4, i64 1206, !4, i64 1286, !4, i64 1374, !4, i64 1470, !4, i64 1790, !4, i64 5990, !4, i64 8090, !4, i64 8126, !4, i64 8138, !4, i64 8174, !4, i64 8192, !4, i64 8336, !4, i64 8468, !4, i64 9216, !4, i64 9240, !4, i64 9372, !4, i64 9412, !4, i64 9588, !4, i64 9720, !4, i64 9832, !4, i64 9944, !4, i64 10574, !4, i64 11204, !4, i64 11330, !4, i64 11342, !4, i64 11372, !4, i64 11480, !4, i64 11510, !4, i64 11564, !4, i64 11618, !4, i64 11654, !4, i64 11780, !4, i64 11816, !4, i64 11852, !4, i64 11870, !4, i64 11888, !96, i64 11912, !96, i64 12198, !4, i64 12484, !97, i64 12490, !4, i64 12580, !4, i64 12712, !4, i64 12724, !4, i64 12732, !4, i64 12738, !4, i64 12744, !4, i64 12856, !4, i64 13636, !4, i64 14076, !4, i64 14204, !4, i64 14904, !4, i64 15032, !4, i64 15128, !4, i64 15138, !4, i64 15178, !4, i64 15188, !4, i64 20492, !4, i64 21036, !4, i64 21054, !3, i64 21260}
!96 = !{!"", !4, i64 0, !4, i64 10}
!97 = !{!"segmentation_probs", !4, i64 0, !4, i64 18, !4, i64 36}
!98 = !{!99, !4, i64 4}
!99 = !{!"", !4, i64 0, !4, i64 4}
!100 = !{!25, !4, i64 10844}
!101 = !{!21, !21, i64 0}
!102 = !{!53, !7, i64 32}
!103 = !{!64, !3, i64 3096}
!104 = !{!64, !3, i64 8}
!105 = !{!64, !3, i64 12}
!106 = !{!31, !3, i64 16}
!107 = !{!31, !3, i64 12}
!108 = !{!25, !11, i64 1335}
!109 = !{!25, !11, i64 1332}
!110 = !{!25, !3, i64 16224}
!111 = !{!11, !11, i64 0}
!112 = !{!25, !4, i64 16220}
!113 = !{!31, !7, i64 36}
!114 = !{!25, !3, i64 15164}
!115 = !{!40, !3, i64 0}
!116 = !{!40, !3, i64 4}
!117 = !{!40, !3, i64 8}
!118 = !{!40, !3, i64 16}
!119 = !{!40, !3, i64 12}
!120 = !{!10, !4, i64 6928}
!121 = !{!18, !4, i64 146}
!122 = !{i64 0, i64 4, !2, i64 0, i64 2, !101, i64 2, i64 2, !101}
!123 = !{!18, !4, i64 122}
!124 = !{i64 0, i64 4, !2, i64 0, i64 2, !101, i64 2, i64 2, !101, i64 0, i64 2, !101, i64 2, i64 2, !101}
!125 = !{!10, !7, i64 6912}
!126 = !{!127, !3, i64 16}
!127 = !{!"macroblockd_plane", !7, i64 0, !7, i64 4, !7, i64 8, !4, i64 12, !3, i64 16, !3, i64 20, !128, i64 24, !4, i64 44, !7, i64 84, !7, i64 88, !4, i64 92, !7, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!128 = !{!"buf_2d", !7, i64 0, !7, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!129 = !{!127, !3, i64 20}
!130 = !{!25, !4, i64 16264}
!131 = !{!23, !4, i64 1}
!132 = !{!23, !4, i64 0}
!133 = !{!33, !3, i64 164}
!134 = !{!93, !7, i64 72}
!135 = !{!10, !11, i64 4088}
!136 = !{!10, !11, i64 4089}
!137 = !{!31, !3, i64 44}
!138 = !{!12, !3, i64 0}
!139 = !{!25, !3, i64 16228}
!140 = !{i64 0, i64 2, !101, i64 2, i64 2, !101}
!141 = !{!142, !21, i64 0}
!142 = !{!"fullpel_mv", !21, i64 0, !21, i64 2}
!143 = !{!144, !21, i64 0}
!144 = !{!"mv", !21, i64 0, !21, i64 2}
!145 = !{!142, !21, i64 2}
!146 = !{!144, !21, i64 2}
!147 = !{!12, !3, i64 8}
!148 = !{!12, !3, i64 4}
!149 = !{!12, !3, i64 12}
!150 = !{!25, !4, i64 16252}
!151 = !{!33, !4, i64 1}
!152 = !{!25, !7, i64 10848}
!153 = !{!33, !4, i64 3}
!154 = !{!25, !3, i64 20}
!155 = !{!30, !11, i64 1}
!156 = !{!30, !11, i64 2}
!157 = !{!25, !4, i64 16254}
!158 = !{!18, !4, i64 121}
!159 = !{!18, !4, i64 151}
!160 = !{!18, !4, i64 156}
!161 = !{!18, !4, i64 159}
!162 = !{!18, !4, i64 7}
!163 = !{!25, !4, i64 16255}
!164 = !{!25, !3, i64 16240}
!165 = !{!18, !4, i64 4}
!166 = !{!18, !4, i64 5}
!167 = !{!18, !4, i64 6}
!168 = !{!30, !4, i64 13}
!169 = !{!25, !4, i64 16256}
!170 = !{!18, !4, i64 48}
!171 = !{!18, !4, i64 49}
!172 = !{!173, !3, i64 0}
!173 = !{!"", !3, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!174 = !{!25, !11, i64 1339}
!175 = !{!10, !7, i64 6916}
!176 = !{!25, !11, i64 1333}
!177 = !{!93, !3, i64 4}
!178 = !{!25, !3, i64 24}
!179 = !{!25, !3, i64 28}
!180 = !{!25, !4, i64 1}
!181 = !{!20, !4, i64 40}
!182 = !{!10, !3, i64 6908}
!183 = !{!29, !3, i64 0}
!184 = !{!29, !3, i64 4}
!185 = !{!27, !3, i64 0}
!186 = !{!27, !3, i64 4}
