; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/detokenize.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/detokenize.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_mv = type { i32 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.aom_reader = type { i8*, i8*, %struct.od_ec_dec, %struct.Accounting*, i8 }
%struct.od_ec_dec = type { i8*, i32, i8*, i8*, i32, i16, i16 }
%struct.Accounting = type { %struct.AccountingSymbols, i32, [1021 x i16], %struct.AccountingSymbolContext, i32 }
%struct.AccountingSymbols = type { %struct.AccountingSymbol*, i32, i32, i32, %struct.AccountingDictionary }
%struct.AccountingSymbol = type { %struct.AccountingSymbolContext, i32, i32, i32 }
%struct.AccountingDictionary = type { [256 x i8*], i32 }
%struct.AccountingSymbolContext = type { i16, i16 }
%struct.Av1ColorMapParam = type { i32, i32, i32, i32, i32, i8*, [5 x [9 x i16]]*, [7 x [5 x [8 x i32]]]* }

@block_size_high = internal constant [22 x i8] c"\04\08\04\08\10\08\10 \10 @ @\80@\80\10\04 \08@\10", align 16
@block_size_wide = internal constant [22 x i8] c"\04\04\08\08\08\10\10\10   @@@\80\80\04\10\08 \10@", align 16
@__func__.decode_color_map_tokens = private unnamed_addr constant [24 x i8] c"decode_color_map_tokens\00", align 1
@__func__.av1_read_uniform = private unnamed_addr constant [17 x i8] c"av1_read_uniform\00", align 1
@update_cdf.nsymbs2speed = internal constant [17 x i32] [i32 0, i32 0, i32 1, i32 1, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2], align 16

; Function Attrs: nounwind
define hidden void @av1_decode_palette_tokens(%struct.macroblockd* %xd, i32 %plane, %struct.aom_reader* %r) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %params = alloca %struct.Av1ColorMapParam, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %0 = bitcast %struct.Av1ColorMapParam* %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %0) #6
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 4
  %2 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane1, i32 0, i32 %2
  %color_index_map = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx, i32 0, i32 11
  %3 = load i8*, i8** %color_index_map, align 4, !tbaa !8
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %color_index_map_offset = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 60
  %5 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [2 x i16], [2 x i16]* %color_index_map_offset, i32 0, i32 %5
  %6 = load i16, i16* %arrayidx2, align 2, !tbaa !11
  %conv = zext i16 %6 to i32
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %conv
  %color_map = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %params, i32 0, i32 5
  store i8* %add.ptr, i8** %color_map, align 4, !tbaa !13
  %7 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tile_ctx = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 40
  %9 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx, align 16, !tbaa !15
  %palette_uv_color_index_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %9, i32 0, i32 28
  %arraydecay = getelementptr inbounds [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]]* %palette_uv_color_index_cdf, i32 0, i32 0
  br label %cond.end

cond.false:                                       ; preds = %entry
  %10 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tile_ctx3 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %10, i32 0, i32 40
  %11 = load %struct.frame_contexts*, %struct.frame_contexts** %tile_ctx3, align 16, !tbaa !15
  %palette_y_color_index_cdf = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %11, i32 0, i32 27
  %arraydecay4 = getelementptr inbounds [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]]* %palette_y_color_index_cdf, i32 0, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi [5 x [9 x i16]]* [ %arraydecay, %cond.true ], [ %arraydecay4, %cond.false ]
  %map_cdf = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %params, i32 0, i32 6
  store [5 x [9 x i16]]* %cond, [5 x [9 x i16]]** %map_cdf, align 4, !tbaa !21
  %12 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %13, i32 0, i32 6
  %14 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !22
  %arrayidx5 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %14, i32 0
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx5, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %15, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %16 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %palette_mode_info = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %16, i32 0, i32 5
  %palette_size = getelementptr inbounds %struct.PALETTE_MODE_INFO, %struct.PALETTE_MODE_INFO* %palette_mode_info, i32 0, i32 1
  %17 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [2 x i8], [2 x i8]* %palette_size, i32 0, i32 %17
  %18 = load i8, i8* %arrayidx6, align 1, !tbaa !23
  %conv7 = zext i8 %18 to i32
  %n_colors = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %params, i32 0, i32 2
  store i32 %conv7, i32* %n_colors, align 4, !tbaa !24
  %19 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %19, i32 0, i32 6
  %20 = load i8, i8* %sb_type, align 2, !tbaa !25
  %21 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %22 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane_width = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %params, i32 0, i32 3
  %plane_height = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %params, i32 0, i32 4
  %rows = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %params, i32 0, i32 0
  %cols = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %params, i32 0, i32 1
  call void @av1_get_block_dimensions(i8 zeroext %20, i32 %21, %struct.macroblockd* %22, i32* %plane_width, i32* %plane_height, i32* %rows, i32* %cols)
  %23 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  call void @decode_color_map_tokens(%struct.Av1ColorMapParam* %params, %struct.aom_reader* %23)
  %24 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  %25 = bitcast %struct.Av1ColorMapParam* %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %25) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal void @av1_get_block_dimensions(i8 zeroext %bsize, i32 %plane, %struct.macroblockd* %xd, i32* %width, i32* %height, i32* %rows_within_bounds, i32* %cols_within_bounds) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %width.addr = alloca i32*, align 4
  %height.addr = alloca i32*, align 4
  %rows_within_bounds.addr = alloca i32*, align 4
  %cols_within_bounds.addr = alloca i32*, align 4
  %block_height = alloca i32, align 4
  %block_width = alloca i32, align 4
  %block_rows = alloca i32, align 4
  %block_cols = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %plane_block_width = alloca i32, align 4
  %plane_block_height = alloca i32, align 4
  %is_chroma_sub8_x = alloca i32, align 4
  %is_chroma_sub8_y = alloca i32, align 4
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !23
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32* %width, i32** %width.addr, align 4, !tbaa !2
  store i32* %height, i32** %height.addr, align 4, !tbaa !2
  store i32* %rows_within_bounds, i32** %rows_within_bounds.addr, align 4, !tbaa !2
  store i32* %cols_within_bounds, i32** %cols_within_bounds.addr, align 4, !tbaa !2
  %0 = bitcast i32* %block_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %bsize.addr, align 1, !tbaa !23
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !23
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %block_height, align 4, !tbaa !6
  %3 = bitcast i32* %block_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8, i8* %bsize.addr, align 1, !tbaa !23
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom1
  %5 = load i8, i8* %arrayidx2, align 1, !tbaa !23
  %conv3 = zext i8 %5 to i32
  store i32 %conv3, i32* %block_width, align 4, !tbaa !6
  %6 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 20
  %8 = load i32, i32* %mb_to_bottom_edge, align 16, !tbaa !31
  %cmp = icmp sge i32 %8, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %9 = load i32, i32* %block_height, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %10 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge5 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %10, i32 0, i32 20
  %11 = load i32, i32* %mb_to_bottom_edge5, align 16, !tbaa !31
  %shr = ashr i32 %11, 3
  %12 = load i32, i32* %block_height, align 4, !tbaa !6
  %add = add nsw i32 %shr, %12
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %9, %cond.true ], [ %add, %cond.false ]
  store i32 %cond, i32* %block_rows, align 4, !tbaa !6
  %13 = bitcast i32* %block_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %14, i32 0, i32 18
  %15 = load i32, i32* %mb_to_right_edge, align 8, !tbaa !32
  %cmp6 = icmp sge i32 %15, 0
  br i1 %cmp6, label %cond.true8, label %cond.false9

cond.true8:                                       ; preds = %cond.end
  %16 = load i32, i32* %block_width, align 4, !tbaa !6
  br label %cond.end13

cond.false9:                                      ; preds = %cond.end
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge10 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %17, i32 0, i32 18
  %18 = load i32, i32* %mb_to_right_edge10, align 8, !tbaa !32
  %shr11 = ashr i32 %18, 3
  %19 = load i32, i32* %block_width, align 4, !tbaa !6
  %add12 = add nsw i32 %shr11, %19
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false9, %cond.true8
  %cond14 = phi i32 [ %16, %cond.true8 ], [ %add12, %cond.false9 ]
  store i32 %cond14, i32* %block_cols, align 4, !tbaa !6
  %20 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane15 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %21, i32 0, i32 4
  %22 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane15, i32 0, i32 %22
  store %struct.macroblockd_plane* %arrayidx16, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %23 = bitcast i32* %plane_block_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = load i32, i32* %block_width, align 4, !tbaa !6
  %25 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %25, i32 0, i32 4
  %26 = load i32, i32* %subsampling_x, align 4, !tbaa !33
  %shr17 = ashr i32 %24, %26
  store i32 %shr17, i32* %plane_block_width, align 4, !tbaa !6
  %27 = bitcast i32* %plane_block_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = load i32, i32* %block_height, align 4, !tbaa !6
  %29 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %29, i32 0, i32 5
  %30 = load i32, i32* %subsampling_y, align 4, !tbaa !34
  %shr18 = ashr i32 %28, %30
  store i32 %shr18, i32* %plane_block_height, align 4, !tbaa !6
  %31 = bitcast i32* %is_chroma_sub8_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  %32 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp19 = icmp sgt i32 %32, 0
  br i1 %cmp19, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %cond.end13
  %33 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %cmp21 = icmp slt i32 %33, 4
  br label %land.end

land.end:                                         ; preds = %land.rhs, %cond.end13
  %34 = phi i1 [ false, %cond.end13 ], [ %cmp21, %land.rhs ]
  %land.ext = zext i1 %34 to i32
  store i32 %land.ext, i32* %is_chroma_sub8_x, align 4, !tbaa !6
  %35 = bitcast i32* %is_chroma_sub8_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %36 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp23 = icmp sgt i32 %36, 0
  br i1 %cmp23, label %land.rhs25, label %land.end28

land.rhs25:                                       ; preds = %land.end
  %37 = load i32, i32* %plane_block_height, align 4, !tbaa !6
  %cmp26 = icmp slt i32 %37, 4
  br label %land.end28

land.end28:                                       ; preds = %land.rhs25, %land.end
  %38 = phi i1 [ false, %land.end ], [ %cmp26, %land.rhs25 ]
  %land.ext29 = zext i1 %38 to i32
  store i32 %land.ext29, i32* %is_chroma_sub8_y, align 4, !tbaa !6
  %39 = load i32*, i32** %width.addr, align 4, !tbaa !2
  %tobool = icmp ne i32* %39, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %land.end28
  %40 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %41 = load i32, i32* %is_chroma_sub8_x, align 4, !tbaa !6
  %mul = mul nsw i32 2, %41
  %add30 = add nsw i32 %40, %mul
  %42 = load i32*, i32** %width.addr, align 4, !tbaa !2
  store i32 %add30, i32* %42, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %land.end28
  %43 = load i32*, i32** %height.addr, align 4, !tbaa !2
  %tobool31 = icmp ne i32* %43, null
  br i1 %tobool31, label %if.then32, label %if.end35

if.then32:                                        ; preds = %if.end
  %44 = load i32, i32* %plane_block_height, align 4, !tbaa !6
  %45 = load i32, i32* %is_chroma_sub8_y, align 4, !tbaa !6
  %mul33 = mul nsw i32 2, %45
  %add34 = add nsw i32 %44, %mul33
  %46 = load i32*, i32** %height.addr, align 4, !tbaa !2
  store i32 %add34, i32* %46, align 4, !tbaa !6
  br label %if.end35

if.end35:                                         ; preds = %if.then32, %if.end
  %47 = load i32*, i32** %rows_within_bounds.addr, align 4, !tbaa !2
  %tobool36 = icmp ne i32* %47, null
  br i1 %tobool36, label %if.then37, label %if.end42

if.then37:                                        ; preds = %if.end35
  %48 = load i32, i32* %block_rows, align 4, !tbaa !6
  %49 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y38 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %49, i32 0, i32 5
  %50 = load i32, i32* %subsampling_y38, align 4, !tbaa !34
  %shr39 = ashr i32 %48, %50
  %51 = load i32, i32* %is_chroma_sub8_y, align 4, !tbaa !6
  %mul40 = mul nsw i32 2, %51
  %add41 = add nsw i32 %shr39, %mul40
  %52 = load i32*, i32** %rows_within_bounds.addr, align 4, !tbaa !2
  store i32 %add41, i32* %52, align 4, !tbaa !6
  br label %if.end42

if.end42:                                         ; preds = %if.then37, %if.end35
  %53 = load i32*, i32** %cols_within_bounds.addr, align 4, !tbaa !2
  %tobool43 = icmp ne i32* %53, null
  br i1 %tobool43, label %if.then44, label %if.end49

if.then44:                                        ; preds = %if.end42
  %54 = load i32, i32* %block_cols, align 4, !tbaa !6
  %55 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x45 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %55, i32 0, i32 4
  %56 = load i32, i32* %subsampling_x45, align 4, !tbaa !33
  %shr46 = ashr i32 %54, %56
  %57 = load i32, i32* %is_chroma_sub8_x, align 4, !tbaa !6
  %mul47 = mul nsw i32 2, %57
  %add48 = add nsw i32 %shr46, %mul47
  %58 = load i32*, i32** %cols_within_bounds.addr, align 4, !tbaa !2
  store i32 %add48, i32* %58, align 4, !tbaa !6
  br label %if.end49

if.end49:                                         ; preds = %if.then44, %if.end42
  %59 = bitcast i32* %is_chroma_sub8_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  %60 = bitcast i32* %is_chroma_sub8_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %plane_block_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i32* %plane_block_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %64 = bitcast i32* %block_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  %66 = bitcast i32* %block_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = bitcast i32* %block_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  ret void
}

; Function Attrs: nounwind
define internal void @decode_color_map_tokens(%struct.Av1ColorMapParam* %param, %struct.aom_reader* %r) #0 {
entry:
  %param.addr = alloca %struct.Av1ColorMapParam*, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %color_order = alloca [8 x i8], align 1
  %n = alloca i32, align 4
  %color_map = alloca i8*, align 4
  %color_map_cdf = alloca [5 x [9 x i16]]*, align 4
  %plane_block_width = alloca i32, align 4
  %plane_block_height = alloca i32, align 4
  %rows = alloca i32, align 4
  %cols = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %color_ctx = alloca i32, align 4
  %color_idx = alloca i32, align 4
  %i39 = alloca i32, align 4
  %i56 = alloca i32, align 4
  store %struct.Av1ColorMapParam* %param, %struct.Av1ColorMapParam** %param.addr, align 4, !tbaa !2
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %0 = bitcast [8 x i8]* %color_order to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load %struct.Av1ColorMapParam*, %struct.Av1ColorMapParam** %param.addr, align 4, !tbaa !2
  %n_colors = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %2, i32 0, i32 2
  %3 = load i32, i32* %n_colors, align 4, !tbaa !24
  store i32 %3, i32* %n, align 4, !tbaa !6
  %4 = bitcast i8** %color_map to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.Av1ColorMapParam*, %struct.Av1ColorMapParam** %param.addr, align 4, !tbaa !2
  %color_map1 = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %5, i32 0, i32 5
  %6 = load i8*, i8** %color_map1, align 4, !tbaa !13
  store i8* %6, i8** %color_map, align 4, !tbaa !2
  %7 = bitcast [5 x [9 x i16]]** %color_map_cdf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load %struct.Av1ColorMapParam*, %struct.Av1ColorMapParam** %param.addr, align 4, !tbaa !2
  %map_cdf = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %8, i32 0, i32 6
  %9 = load [5 x [9 x i16]]*, [5 x [9 x i16]]** %map_cdf, align 4, !tbaa !21
  store [5 x [9 x i16]]* %9, [5 x [9 x i16]]** %color_map_cdf, align 4, !tbaa !2
  %10 = bitcast i32* %plane_block_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %struct.Av1ColorMapParam*, %struct.Av1ColorMapParam** %param.addr, align 4, !tbaa !2
  %plane_width = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %11, i32 0, i32 3
  %12 = load i32, i32* %plane_width, align 4, !tbaa !35
  store i32 %12, i32* %plane_block_width, align 4, !tbaa !6
  %13 = bitcast i32* %plane_block_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load %struct.Av1ColorMapParam*, %struct.Av1ColorMapParam** %param.addr, align 4, !tbaa !2
  %plane_height = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %14, i32 0, i32 4
  %15 = load i32, i32* %plane_height, align 4, !tbaa !36
  store i32 %15, i32* %plane_block_height, align 4, !tbaa !6
  %16 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %struct.Av1ColorMapParam*, %struct.Av1ColorMapParam** %param.addr, align 4, !tbaa !2
  %rows2 = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %17, i32 0, i32 0
  %18 = load i32, i32* %rows2, align 4, !tbaa !37
  store i32 %18, i32* %rows, align 4, !tbaa !6
  %19 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  %20 = load %struct.Av1ColorMapParam*, %struct.Av1ColorMapParam** %param.addr, align 4, !tbaa !2
  %cols3 = getelementptr inbounds %struct.Av1ColorMapParam, %struct.Av1ColorMapParam* %20, i32 0, i32 1
  %21 = load i32, i32* %cols3, align 4, !tbaa !38
  store i32 %21, i32* %cols, align 4, !tbaa !6
  %22 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %23 = load i32, i32* %n, align 4, !tbaa !6
  %call = call i32 @av1_read_uniform(%struct.aom_reader* %22, i32 %23)
  %conv = trunc i32 %call to i8
  %24 = load i8*, i8** %color_map, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %24, i32 0
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !23
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc35, %entry
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %27 = load i32, i32* %rows, align 4, !tbaa !6
  %28 = load i32, i32* %cols, align 4, !tbaa !6
  %add = add nsw i32 %27, %28
  %sub = sub nsw i32 %add, 1
  %cmp = icmp slt i32 %26, %sub
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  br label %for.end36

for.body:                                         ; preds = %for.cond
  %30 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %32 = load i32, i32* %cols, align 4, !tbaa !6
  %sub5 = sub nsw i32 %32, 1
  %cmp6 = icmp slt i32 %31, %sub5
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %33 = load i32, i32* %i, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %34 = load i32, i32* %cols, align 4, !tbaa !6
  %sub8 = sub nsw i32 %34, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %33, %cond.true ], [ %sub8, %cond.false ]
  store i32 %cond, i32* %j, align 4, !tbaa !6
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %cond.end
  %35 = load i32, i32* %j, align 4, !tbaa !6
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %rows, align 4, !tbaa !6
  %sub10 = sub nsw i32 %36, %37
  %add11 = add nsw i32 %sub10, 1
  %cmp12 = icmp sgt i32 0, %add11
  br i1 %cmp12, label %cond.true14, label %cond.false15

cond.true14:                                      ; preds = %for.cond9
  br label %cond.end18

cond.false15:                                     ; preds = %for.cond9
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %39 = load i32, i32* %rows, align 4, !tbaa !6
  %sub16 = sub nsw i32 %38, %39
  %add17 = add nsw i32 %sub16, 1
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false15, %cond.true14
  %cond19 = phi i32 [ 0, %cond.true14 ], [ %add17, %cond.false15 ]
  %cmp20 = icmp sge i32 %35, %cond19
  br i1 %cmp20, label %for.body23, label %for.cond.cleanup22

for.cond.cleanup22:                               ; preds = %cond.end18
  store i32 5, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #6
  br label %for.end

for.body23:                                       ; preds = %cond.end18
  %41 = bitcast i32* %color_ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  %42 = load i8*, i8** %color_map, align 4, !tbaa !2
  %43 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %45 = load i32, i32* %j, align 4, !tbaa !6
  %sub24 = sub nsw i32 %44, %45
  %46 = load i32, i32* %j, align 4, !tbaa !6
  %47 = load i32, i32* %n, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %color_order, i32 0, i32 0
  %call25 = call i32 @av1_get_palette_color_index_context(i8* %42, i32 %43, i32 %sub24, i32 %46, i32 %47, i8* %arraydecay, i32* null)
  store i32 %call25, i32* %color_ctx, align 4, !tbaa !6
  %48 = bitcast i32* %color_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #6
  %49 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %50 = load [5 x [9 x i16]]*, [5 x [9 x i16]]** %color_map_cdf, align 4, !tbaa !2
  %51 = load i32, i32* %n, align 4, !tbaa !6
  %sub26 = sub nsw i32 %51, 2
  %arrayidx27 = getelementptr inbounds [5 x [9 x i16]], [5 x [9 x i16]]* %50, i32 %sub26
  %52 = load i32, i32* %color_ctx, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds [5 x [9 x i16]], [5 x [9 x i16]]* %arrayidx27, i32 0, i32 %52
  %arraydecay29 = getelementptr inbounds [9 x i16], [9 x i16]* %arrayidx28, i32 0, i32 0
  %53 = load i32, i32* %n, align 4, !tbaa !6
  %call30 = call i32 @aom_read_symbol_(%struct.aom_reader* %49, i16* %arraydecay29, i32 %53, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @__func__.decode_color_map_tokens, i32 0, i32 0))
  store i32 %call30, i32* %color_idx, align 4, !tbaa !6
  %54 = load i32, i32* %color_idx, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds [8 x i8], [8 x i8]* %color_order, i32 0, i32 %54
  %55 = load i8, i8* %arrayidx31, align 1, !tbaa !23
  %56 = load i8*, i8** %color_map, align 4, !tbaa !2
  %57 = load i32, i32* %i, align 4, !tbaa !6
  %58 = load i32, i32* %j, align 4, !tbaa !6
  %sub32 = sub nsw i32 %57, %58
  %59 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %mul = mul nsw i32 %sub32, %59
  %60 = load i32, i32* %j, align 4, !tbaa !6
  %add33 = add nsw i32 %mul, %60
  %arrayidx34 = getelementptr inbounds i8, i8* %56, i32 %add33
  store i8 %55, i8* %arrayidx34, align 1, !tbaa !23
  %61 = bitcast i32* %color_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i32* %color_ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body23
  %63 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %63, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %for.cond9

for.end:                                          ; preds = %for.cond.cleanup22
  br label %for.inc35

for.inc35:                                        ; preds = %for.end
  %64 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end36:                                        ; preds = %for.cond.cleanup
  %65 = load i32, i32* %cols, align 4, !tbaa !6
  %66 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %cmp37 = icmp slt i32 %65, %66
  br i1 %cmp37, label %if.then, label %if.end

if.then:                                          ; preds = %for.end36
  %67 = bitcast i32* %i39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #6
  store i32 0, i32* %i39, align 4, !tbaa !6
  br label %for.cond40

for.cond40:                                       ; preds = %for.inc53, %if.then
  %68 = load i32, i32* %i39, align 4, !tbaa !6
  %69 = load i32, i32* %rows, align 4, !tbaa !6
  %cmp41 = icmp slt i32 %68, %69
  br i1 %cmp41, label %for.body44, label %for.cond.cleanup43

for.cond.cleanup43:                               ; preds = %for.cond40
  store i32 8, i32* %cleanup.dest.slot, align 4
  %70 = bitcast i32* %i39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  br label %for.end55

for.body44:                                       ; preds = %for.cond40
  %71 = load i8*, i8** %color_map, align 4, !tbaa !2
  %72 = load i32, i32* %i39, align 4, !tbaa !6
  %73 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %mul45 = mul nsw i32 %72, %73
  %add.ptr = getelementptr inbounds i8, i8* %71, i32 %mul45
  %74 = load i32, i32* %cols, align 4, !tbaa !6
  %add.ptr46 = getelementptr inbounds i8, i8* %add.ptr, i32 %74
  %75 = load i8*, i8** %color_map, align 4, !tbaa !2
  %76 = load i32, i32* %i39, align 4, !tbaa !6
  %77 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %mul47 = mul nsw i32 %76, %77
  %78 = load i32, i32* %cols, align 4, !tbaa !6
  %add48 = add nsw i32 %mul47, %78
  %sub49 = sub nsw i32 %add48, 1
  %arrayidx50 = getelementptr inbounds i8, i8* %75, i32 %sub49
  %79 = load i8, i8* %arrayidx50, align 1, !tbaa !23
  %conv51 = zext i8 %79 to i32
  %80 = trunc i32 %conv51 to i8
  %81 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %82 = load i32, i32* %cols, align 4, !tbaa !6
  %sub52 = sub nsw i32 %81, %82
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr46, i8 %80, i32 %sub52, i1 false)
  br label %for.inc53

for.inc53:                                        ; preds = %for.body44
  %83 = load i32, i32* %i39, align 4, !tbaa !6
  %inc54 = add nsw i32 %83, 1
  store i32 %inc54, i32* %i39, align 4, !tbaa !6
  br label %for.cond40

for.end55:                                        ; preds = %for.cond.cleanup43
  br label %if.end

if.end:                                           ; preds = %for.end55, %for.end36
  %84 = bitcast i32* %i56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #6
  %85 = load i32, i32* %rows, align 4, !tbaa !6
  store i32 %85, i32* %i56, align 4, !tbaa !6
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc67, %if.end
  %86 = load i32, i32* %i56, align 4, !tbaa !6
  %87 = load i32, i32* %plane_block_height, align 4, !tbaa !6
  %cmp58 = icmp slt i32 %86, %87
  br i1 %cmp58, label %for.body61, label %for.cond.cleanup60

for.cond.cleanup60:                               ; preds = %for.cond57
  store i32 11, i32* %cleanup.dest.slot, align 4
  %88 = bitcast i32* %i56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #6
  br label %for.end69

for.body61:                                       ; preds = %for.cond57
  %89 = load i8*, i8** %color_map, align 4, !tbaa !2
  %90 = load i32, i32* %i56, align 4, !tbaa !6
  %91 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %mul62 = mul nsw i32 %90, %91
  %add.ptr63 = getelementptr inbounds i8, i8* %89, i32 %mul62
  %92 = load i8*, i8** %color_map, align 4, !tbaa !2
  %93 = load i32, i32* %rows, align 4, !tbaa !6
  %sub64 = sub nsw i32 %93, 1
  %94 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  %mul65 = mul nsw i32 %sub64, %94
  %add.ptr66 = getelementptr inbounds i8, i8* %92, i32 %mul65
  %95 = load i32, i32* %plane_block_width, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr63, i8* align 1 %add.ptr66, i32 %95, i1 false)
  br label %for.inc67

for.inc67:                                        ; preds = %for.body61
  %96 = load i32, i32* %i56, align 4, !tbaa !6
  %inc68 = add nsw i32 %96, 1
  store i32 %inc68, i32* %i56, align 4, !tbaa !6
  br label %for.cond57

for.end69:                                        ; preds = %for.cond.cleanup60
  %97 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #6
  %98 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #6
  %99 = bitcast i32* %plane_block_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #6
  %100 = bitcast i32* %plane_block_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #6
  %101 = bitcast [5 x [9 x i16]]** %color_map_cdf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #6
  %102 = bitcast i8** %color_map to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  %103 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast [8 x i8]* %color_order to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %104) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @av1_read_uniform(%struct.aom_reader* %r, i32 %n) #2 {
entry:
  %retval = alloca i32, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %n.addr = alloca i32, align 4
  %l = alloca i32, align 4
  %m = alloca i32, align 4
  %v = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %call = call i32 @get_unsigned_bits(i32 %1)
  store i32 %call, i32* %l, align 4, !tbaa !6
  %2 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i32, i32* %l, align 4, !tbaa !6
  %shl = shl i32 1, %3
  %4 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %shl, %4
  store i32 %sub, i32* %m, align 4, !tbaa !6
  %5 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %7 = load i32, i32* %l, align 4, !tbaa !6
  %sub1 = sub nsw i32 %7, 1
  %call2 = call i32 @aom_read_literal_(%struct.aom_reader* %6, i32 %sub1, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @__func__.av1_read_uniform, i32 0, i32 0))
  store i32 %call2, i32* %v, align 4, !tbaa !6
  %8 = load i32, i32* %v, align 4, !tbaa !6
  %9 = load i32, i32* %m, align 4, !tbaa !6
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %10 = load i32, i32* %v, align 4, !tbaa !6
  store i32 %10, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %v, align 4, !tbaa !6
  %shl3 = shl i32 %11, 1
  %12 = load i32, i32* %m, align 4, !tbaa !6
  %sub4 = sub nsw i32 %shl3, %12
  %13 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call5 = call i32 @aom_read_literal_(%struct.aom_reader* %13, i32 1, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @__func__.av1_read_uniform, i32 0, i32 0))
  %add = add nsw i32 %sub4, %call5
  store i32 %add, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %14 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

declare i32 @av1_get_palette_color_index_context(i8*, i32, i32, i32, i32, i8*, i32*) #3

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_symbol_(%struct.aom_reader* %r, i16* %cdf, i32 %nsymbs, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %cdf.addr = alloca i16*, align 4
  %nsymbs.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %ret = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !2
  store i32 %nsymbs, i32* %nsymbs.addr, align 4, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %3 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %call = call i32 @aom_read_cdf_(%struct.aom_reader* %1, i16* %2, i32 %3, i8* %4)
  store i32 %call, i32* %ret, align 4, !tbaa !6
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %allow_update_cdf = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %5, i32 0, i32 4
  %6 = load i8, i8* %allow_update_cdf, align 4, !tbaa !39
  %tobool = icmp ne i8 %6, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %8 = load i32, i32* %ret, align 4, !tbaa !6
  %conv = trunc i32 %8 to i8
  %9 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  call void @update_cdf(i16* %7, i8 signext %conv, i32 %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i32, i32* %ret, align 4, !tbaa !6
  %11 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret i32 %10
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint nounwind
define internal i32 @get_unsigned_bits(i32 %num_values) #2 {
entry:
  %num_values.addr = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4, !tbaa !6
  %0 = load i32, i32* %num_values.addr, align 4, !tbaa !6
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4, !tbaa !6
  %call = call i32 @get_msb(i32 %1)
  %add = add nsw i32 %call, 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ 0, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_literal_(%struct.aom_reader* %r, i32 %bits, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %bits.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %literal = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %literal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %literal, align 4, !tbaa !6
  %1 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %bits.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 1
  store i32 %sub, i32* %bit, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %bit, align 4, !tbaa !6
  %cmp = icmp sge i32 %3, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_read_bit_(%struct.aom_reader* %4, i8* null)
  %5 = load i32, i32* %bit, align 4, !tbaa !6
  %shl = shl i32 %call, %5
  %6 = load i32, i32* %literal, align 4, !tbaa !6
  %or = or i32 %6, %shl
  store i32 %or, i32* %literal, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %bit, align 4, !tbaa !6
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* %bit, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %8, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %9 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %9, i8* %10)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %11 = load i32, i32* %literal, align 4, !tbaa !6
  %12 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast i32* %literal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  ret i32 %11
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #2 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #5

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_bit_(%struct.aom_reader* %r, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %acct_str.addr = alloca i8*, align 4
  %ret = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_read_(%struct.aom_reader* %1, i32 128, i8* null)
  store i32 %call, i32* %ret, align 4, !tbaa !6
  %2 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %3, i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i32, i32* %ret, align 4, !tbaa !6
  %6 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret i32 %5
}

; Function Attrs: inlinehint nounwind
define internal void @aom_process_accounting(%struct.aom_reader* %r, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %acct_str.addr = alloca i8*, align 4
  %tell_frac = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting, align 4, !tbaa !42
  %cmp = icmp ne %struct.Accounting* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %tell_frac to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_reader_tell_frac(%struct.aom_reader* %3)
  store i32 %call, i32* %tell_frac, align 4, !tbaa !6
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting1 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %4, i32 0, i32 3
  %5 = load %struct.Accounting*, %struct.Accounting** %accounting1, align 4, !tbaa !42
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %7 = load i32, i32* %tell_frac, align 4, !tbaa !6
  %8 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting2 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %8, i32 0, i32 3
  %9 = load %struct.Accounting*, %struct.Accounting** %accounting2, align 4, !tbaa !42
  %last_tell_frac = getelementptr inbounds %struct.Accounting, %struct.Accounting* %9, i32 0, i32 4
  %10 = load i32, i32* %last_tell_frac, align 4, !tbaa !43
  %sub = sub i32 %7, %10
  call void @aom_accounting_record(%struct.Accounting* %5, i8* %6, i32 %sub)
  %11 = load i32, i32* %tell_frac, align 4, !tbaa !6
  %12 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting3 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %12, i32 0, i32 3
  %13 = load %struct.Accounting*, %struct.Accounting** %accounting3, align 4, !tbaa !42
  %last_tell_frac4 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %13, i32 0, i32 4
  store i32 %11, i32* %last_tell_frac4, align 4, !tbaa !43
  %14 = bitcast i32* %tell_frac to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_(%struct.aom_reader* %r, i32 %prob, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %prob.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %p = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %prob, i32* %prob.addr, align 4, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %prob.addr, align 4, !tbaa !6
  %shl = shl i32 %1, 15
  %sub = sub nsw i32 8388607, %shl
  %2 = load i32, i32* %prob.addr, align 4, !tbaa !6
  %add = add nsw i32 %sub, %2
  %shr = ashr i32 %add, 8
  store i32 %shr, i32* %p, align 4, !tbaa !6
  %3 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %4, i32 0, i32 2
  %5 = load i32, i32* %p, align 4, !tbaa !6
  %call = call i32 @od_ec_decode_bool_q15(%struct.od_ec_dec* %ec, i32 %5)
  store i32 %call, i32* %bit, align 4, !tbaa !6
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %8 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %7, i8* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  call void @aom_update_symb_counts(%struct.aom_reader* %9, i32 1)
  %10 = load i32, i32* %bit, align 4, !tbaa !6
  %11 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  ret i32 %10
}

declare i32 @od_ec_decode_bool_q15(%struct.od_ec_dec*, i32) #3

; Function Attrs: inlinehint nounwind
define internal void @aom_update_symb_counts(%struct.aom_reader* %r, i32 %is_binary) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %is_binary.addr = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %is_binary, i32* %is_binary.addr, align 4, !tbaa !6
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting, align 4, !tbaa !42
  %cmp = icmp ne %struct.Accounting* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %is_binary.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %2, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting1 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %3, i32 0, i32 3
  %4 = load %struct.Accounting*, %struct.Accounting** %accounting1, align 4, !tbaa !42
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %4, i32 0, i32 0
  %num_multi_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 2
  %5 = load i32, i32* %num_multi_syms, align 4, !tbaa !48
  %add = add nsw i32 %5, %lnot.ext
  store i32 %add, i32* %num_multi_syms, align 4, !tbaa !48
  %6 = load i32, i32* %is_binary.addr, align 4, !tbaa !6
  %tobool2 = icmp ne i32 %6, 0
  %lnot3 = xor i1 %tobool2, true
  %lnot5 = xor i1 %lnot3, true
  %lnot.ext6 = zext i1 %lnot5 to i32
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting7 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %7, i32 0, i32 3
  %8 = load %struct.Accounting*, %struct.Accounting** %accounting7, align 4, !tbaa !42
  %syms8 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %8, i32 0, i32 0
  %num_binary_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms8, i32 0, i32 3
  %9 = load i32, i32* %num_binary_syms, align 4, !tbaa !49
  %add9 = add nsw i32 %9, %lnot.ext6
  store i32 %add9, i32* %num_binary_syms, align 4, !tbaa !49
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare i32 @aom_reader_tell_frac(%struct.aom_reader*) #3

declare void @aom_accounting_record(%struct.Accounting*, i8*, i32) #3

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_cdf_(%struct.aom_reader* %r, i16* %cdf, i32 %nsymbs, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %cdf.addr = alloca i16*, align 4
  %nsymbs.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %symb = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !2
  store i32 %nsymbs, i32* %nsymbs.addr, align 4, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %symb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %1, i32 0, i32 2
  %2 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %3 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %call = call i32 @od_ec_decode_cdf_q15(%struct.od_ec_dec* %ec, i16* %2, i32 %3)
  store i32 %call, i32* %symb, align 4, !tbaa !6
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %4, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %5, i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %8 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %8, 2
  %conv = zext i1 %cmp to i32
  call void @aom_update_symb_counts(%struct.aom_reader* %7, i32 %conv)
  %9 = load i32, i32* %symb, align 4, !tbaa !6
  %10 = bitcast i32* %symb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  ret i32 %9
}

; Function Attrs: inlinehint nounwind
define internal void @update_cdf(i16* %cdf, i8 signext %val, i32 %nsymbs) #2 {
entry:
  %cdf.addr = alloca i16*, align 4
  %val.addr = alloca i8, align 1
  %nsymbs.addr = alloca i32, align 4
  %rate = alloca i32, align 4
  %i = alloca i32, align 4
  %tmp = alloca i32, align 4
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !2
  store i8 %val, i8* %val.addr, align 1, !tbaa !23
  store i32 %nsymbs, i32* %nsymbs.addr, align 4, !tbaa !6
  %0 = bitcast i32* %rate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 %4
  %5 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %5 to i32
  %cmp = icmp sgt i32 %conv, 15
  %conv1 = zext i1 %cmp to i32
  %add = add nsw i32 3, %conv1
  %6 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %7 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx2, align 2, !tbaa !11
  %conv3 = zext i16 %8 to i32
  %cmp4 = icmp sgt i32 %conv3, 31
  %conv5 = zext i1 %cmp4 to i32
  %add6 = add nsw i32 %add, %conv5
  %9 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [17 x i32], [17 x i32]* @update_cdf.nsymbs2speed, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx7, align 4, !tbaa !6
  %add8 = add nsw i32 %add6, %10
  store i32 %add8, i32* %rate, align 4, !tbaa !6
  store i32 32768, i32* %tmp, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %12, 1
  %cmp9 = icmp slt i32 %11, %sub
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %14 = load i8, i8* %val.addr, align 1, !tbaa !23
  %conv11 = sext i8 %14 to i32
  %cmp12 = icmp eq i32 %13, %conv11
  br i1 %cmp12, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %15 = load i32, i32* %tmp, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %15, %cond.false ]
  store i32 %cond, i32* %tmp, align 4, !tbaa !6
  %16 = load i32, i32* %tmp, align 4, !tbaa !6
  %17 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds i16, i16* %17, i32 %18
  %19 = load i16, i16* %arrayidx14, align 2, !tbaa !11
  %conv15 = zext i16 %19 to i32
  %cmp16 = icmp slt i32 %16, %conv15
  br i1 %cmp16, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %20 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds i16, i16* %20, i32 %21
  %22 = load i16, i16* %arrayidx18, align 2, !tbaa !11
  %conv19 = zext i16 %22 to i32
  %23 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub20 = sub nsw i32 %conv19, %23
  %24 = load i32, i32* %rate, align 4, !tbaa !6
  %shr = ashr i32 %sub20, %24
  %25 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds i16, i16* %25, i32 %26
  %27 = load i16, i16* %arrayidx21, align 2, !tbaa !11
  %conv22 = zext i16 %27 to i32
  %sub23 = sub nsw i32 %conv22, %shr
  %conv24 = trunc i32 %sub23 to i16
  store i16 %conv24, i16* %arrayidx21, align 2, !tbaa !11
  br label %if.end

if.else:                                          ; preds = %cond.end
  %28 = load i32, i32* %tmp, align 4, !tbaa !6
  %29 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds i16, i16* %29, i32 %30
  %31 = load i16, i16* %arrayidx25, align 2, !tbaa !11
  %conv26 = zext i16 %31 to i32
  %sub27 = sub nsw i32 %28, %conv26
  %32 = load i32, i32* %rate, align 4, !tbaa !6
  %shr28 = ashr i32 %sub27, %32
  %33 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds i16, i16* %33, i32 %34
  %35 = load i16, i16* %arrayidx29, align 2, !tbaa !11
  %conv30 = zext i16 %35 to i32
  %add31 = add nsw i32 %conv30, %shr28
  %conv32 = trunc i32 %add31 to i16
  store i16 %conv32, i16* %arrayidx29, align 2, !tbaa !11
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %38 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds i16, i16* %37, i32 %38
  %39 = load i16, i16* %arrayidx33, align 2, !tbaa !11
  %conv34 = zext i16 %39 to i32
  %cmp35 = icmp slt i32 %conv34, 32
  %conv36 = zext i1 %cmp35 to i32
  %40 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %41 = load i32, i32* %nsymbs.addr, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds i16, i16* %40, i32 %41
  %42 = load i16, i16* %arrayidx37, align 2, !tbaa !11
  %conv38 = zext i16 %42 to i32
  %add39 = add nsw i32 %conv38, %conv36
  %conv40 = trunc i32 %add39 to i16
  store i16 %conv40, i16* %arrayidx37, align 2, !tbaa !11
  %43 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  %44 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  %45 = bitcast i32* %rate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  ret void
}

declare i32 @od_ec_decode_cdf_q15(%struct.od_ec_dec*, i16*, i32) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 124}
!9 = !{!"macroblockd_plane", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !7, i64 16, !7, i64 20, !10, i64 24, !4, i64 44, !3, i64 84, !3, i64 88, !4, i64 92, !3, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!10 = !{!"buf_2d", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!11 = !{!12, !12, i64 0}
!12 = !{!"short", !4, i64 0}
!13 = !{!14, !3, i64 20}
!14 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !3, i64 24, !3, i64 28}
!15 = !{!16, !3, i64 6832}
!16 = !{!"macroblockd", !7, i64 0, !7, i64 4, !7, i64 8, !17, i64 12, !4, i64 16, !18, i64 4060, !3, i64 4084, !17, i64 4088, !17, i64 4089, !17, i64 4090, !17, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !7, i64 4112, !7, i64 4116, !7, i64 4120, !7, i64 4124, !7, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !7, i64 6836, !4, i64 6840, !4, i64 6872, !7, i64 6904, !7, i64 6908, !3, i64 6912, !3, i64 6916, !7, i64 6920, !7, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !19, i64 39720, !20, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!17 = !{!"_Bool", !4, i64 0}
!18 = !{!"TileInfo", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20}
!19 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !7, i64 4104, !4, i64 4108, !7, i64 4236, !7, i64 4240, !7, i64 4244, !7, i64 4248, !7, i64 4252, !7, i64 4256}
!20 = !{!"dist_wtd_comp_params", !7, i64 0, !7, i64 4, !7, i64 8}
!21 = !{!14, !3, i64 24}
!22 = !{!16, !3, i64 4084}
!23 = !{!4, !4, i64 0}
!24 = !{!14, !7, i64 8}
!25 = !{!26, !4, i64 118}
!26 = !{!"MB_MODE_INFO", !27, i64 0, !28, i64 8, !4, i64 52, !7, i64 60, !4, i64 64, !29, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !30, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!27 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!28 = !{!"", !4, i64 0, !12, i64 32, !12, i64 34, !12, i64 36, !12, i64 38, !4, i64 40, !4, i64 41}
!29 = !{!"", !4, i64 0, !4, i64 48}
!30 = !{!"", !4, i64 0, !4, i64 1}
!31 = !{!16, !7, i64 4128}
!32 = !{!16, !7, i64 4120}
!33 = !{!9, !7, i64 16}
!34 = !{!9, !7, i64 20}
!35 = !{!14, !7, i64 12}
!36 = !{!14, !7, i64 16}
!37 = !{!14, !7, i64 0}
!38 = !{!14, !7, i64 4}
!39 = !{!40, !4, i64 36}
!40 = !{!"aom_reader", !3, i64 0, !3, i64 4, !41, i64 8, !3, i64 32, !4, i64 36}
!41 = !{!"od_ec_dec", !3, i64 0, !7, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !12, i64 20, !12, i64 22}
!42 = !{!40, !3, i64 32}
!43 = !{!44, !7, i64 3096}
!44 = !{!"Accounting", !45, i64 0, !7, i64 1044, !4, i64 1048, !47, i64 3090, !7, i64 3096}
!45 = !{!"", !3, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !46, i64 16}
!46 = !{!"", !4, i64 0, !7, i64 1024}
!47 = !{!"", !12, i64 0, !12, i64 2}
!48 = !{!44, !7, i64 8}
!49 = !{!44, !7, i64 12}
