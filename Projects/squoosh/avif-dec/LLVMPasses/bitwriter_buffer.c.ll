; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/bitwriter_buffer.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/bitwriter_buffer.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_write_bit_buffer = type { i8*, i32 }

; Function Attrs: nounwind
define hidden i32 @aom_wb_is_byte_aligned(%struct.aom_write_bit_buffer* %wb) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %0 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_offset = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %0, i32 0, i32 1
  %1 = load i32, i32* %bit_offset, align 4, !tbaa !6
  %rem = urem i32 %1, 8
  %cmp = icmp eq i32 %rem, 0
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define hidden i32 @aom_wb_bytes_written(%struct.aom_write_bit_buffer* %wb) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %0 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_offset = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %0, i32 0, i32 1
  %1 = load i32, i32* %bit_offset, align 4, !tbaa !6
  %div = udiv i32 %1, 8
  %2 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_offset1 = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %2, i32 0, i32 1
  %3 = load i32, i32* %bit_offset1, align 4, !tbaa !6
  %rem = urem i32 %3, 8
  %cmp = icmp ugt i32 %rem, 0
  %conv = zext i1 %cmp to i32
  %add = add i32 %div, %conv
  ret i32 %add
}

; Function Attrs: nounwind
define hidden void @aom_wb_write_bit(%struct.aom_write_bit_buffer* %wb, i32 %bit) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %bit.addr = alloca i32, align 4
  %off = alloca i32, align 4
  %p = alloca i32, align 4
  %q = alloca i32, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i32 %bit, i32* %bit.addr, align 4, !tbaa !9
  %0 = bitcast i32* %off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_offset = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %1, i32 0, i32 1
  %2 = load i32, i32* %bit_offset, align 4, !tbaa !6
  store i32 %2, i32* %off, align 4, !tbaa !9
  %3 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %off, align 4, !tbaa !9
  %div = sdiv i32 %4, 8
  store i32 %div, i32* %p, align 4, !tbaa !9
  %5 = bitcast i32* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %off, align 4, !tbaa !9
  %rem = srem i32 %6, 8
  %sub = sub nsw i32 7, %rem
  store i32 %sub, i32* %q, align 4, !tbaa !9
  %7 = load i32, i32* %q, align 4, !tbaa !9
  %cmp = icmp eq i32 %7, 7
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %8 = load i32, i32* %bit.addr, align 4, !tbaa !9
  %9 = load i32, i32* %q, align 4, !tbaa !9
  %shl = shl i32 %8, %9
  %conv = trunc i32 %shl to i8
  %10 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_buffer = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %10, i32 0, i32 0
  %11 = load i8*, i8** %bit_buffer, align 4, !tbaa !10
  %12 = load i32, i32* %p, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds i8, i8* %11, i32 %12
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !11
  br label %if.end

if.else:                                          ; preds = %entry
  %13 = load i32, i32* %q, align 4, !tbaa !9
  %shl1 = shl i32 1, %13
  %neg = xor i32 %shl1, -1
  %14 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_buffer2 = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %14, i32 0, i32 0
  %15 = load i8*, i8** %bit_buffer2, align 4, !tbaa !10
  %16 = load i32, i32* %p, align 4, !tbaa !9
  %arrayidx3 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %arrayidx3, align 1, !tbaa !11
  %conv4 = zext i8 %17 to i32
  %and = and i32 %conv4, %neg
  %conv5 = trunc i32 %and to i8
  store i8 %conv5, i8* %arrayidx3, align 1, !tbaa !11
  %18 = load i32, i32* %bit.addr, align 4, !tbaa !9
  %19 = load i32, i32* %q, align 4, !tbaa !9
  %shl6 = shl i32 %18, %19
  %20 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_buffer7 = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %20, i32 0, i32 0
  %21 = load i8*, i8** %bit_buffer7, align 4, !tbaa !10
  %22 = load i32, i32* %p, align 4, !tbaa !9
  %arrayidx8 = getelementptr inbounds i8, i8* %21, i32 %22
  %23 = load i8, i8* %arrayidx8, align 1, !tbaa !11
  %conv9 = zext i8 %23 to i32
  %or = or i32 %conv9, %shl6
  %conv10 = trunc i32 %or to i8
  store i8 %conv10, i8* %arrayidx8, align 1, !tbaa !11
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %24 = load i32, i32* %off, align 4, !tbaa !9
  %add = add nsw i32 %24, 1
  %25 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_offset11 = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %25, i32 0, i32 1
  store i32 %add, i32* %bit_offset11, align 4, !tbaa !6
  %26 = bitcast i32* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  %28 = bitcast i32* %off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_wb_overwrite_bit(%struct.aom_write_bit_buffer* %wb, i32 %bit) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %bit.addr = alloca i32, align 4
  %off = alloca i32, align 4
  %p = alloca i32, align 4
  %q = alloca i32, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i32 %bit, i32* %bit.addr, align 4, !tbaa !9
  %0 = bitcast i32* %off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_offset = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %1, i32 0, i32 1
  %2 = load i32, i32* %bit_offset, align 4, !tbaa !6
  store i32 %2, i32* %off, align 4, !tbaa !9
  %3 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %off, align 4, !tbaa !9
  %div = sdiv i32 %4, 8
  store i32 %div, i32* %p, align 4, !tbaa !9
  %5 = bitcast i32* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %off, align 4, !tbaa !9
  %rem = srem i32 %6, 8
  %sub = sub nsw i32 7, %rem
  store i32 %sub, i32* %q, align 4, !tbaa !9
  %7 = load i32, i32* %q, align 4, !tbaa !9
  %shl = shl i32 1, %7
  %neg = xor i32 %shl, -1
  %8 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_buffer = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %8, i32 0, i32 0
  %9 = load i8*, i8** %bit_buffer, align 4, !tbaa !10
  %10 = load i32, i32* %p, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !11
  %conv = zext i8 %11 to i32
  %and = and i32 %conv, %neg
  %conv1 = trunc i32 %and to i8
  store i8 %conv1, i8* %arrayidx, align 1, !tbaa !11
  %12 = load i32, i32* %bit.addr, align 4, !tbaa !9
  %13 = load i32, i32* %q, align 4, !tbaa !9
  %shl2 = shl i32 %12, %13
  %14 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_buffer3 = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %14, i32 0, i32 0
  %15 = load i8*, i8** %bit_buffer3, align 4, !tbaa !10
  %16 = load i32, i32* %p, align 4, !tbaa !9
  %arrayidx4 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %arrayidx4, align 1, !tbaa !11
  %conv5 = zext i8 %17 to i32
  %or = or i32 %conv5, %shl2
  %conv6 = trunc i32 %or to i8
  store i8 %conv6, i8* %arrayidx4, align 1, !tbaa !11
  %18 = load i32, i32* %off, align 4, !tbaa !9
  %add = add nsw i32 %18, 1
  %19 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %bit_offset7 = getelementptr inbounds %struct.aom_write_bit_buffer, %struct.aom_write_bit_buffer* %19, i32 0, i32 1
  store i32 %add, i32* %bit_offset7, align 4, !tbaa !6
  %20 = bitcast i32* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  %22 = bitcast i32* %off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_wb_write_literal(%struct.aom_write_bit_buffer* %wb, i32 %data, i32 %bits) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %data.addr = alloca i32, align 4
  %bits.addr = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i32 %data, i32* %data.addr, align 4, !tbaa !9
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !9
  %0 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %bits.addr, align 4, !tbaa !9
  %sub = sub nsw i32 %1, 1
  store i32 %sub, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %bit, align 4, !tbaa !9
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %4 = load i32, i32* %data.addr, align 4, !tbaa !9
  %5 = load i32, i32* %bit, align 4, !tbaa !9
  %shr = ashr i32 %4, %5
  %and = and i32 %shr, 1
  call void @aom_wb_write_bit(%struct.aom_write_bit_buffer* %3, i32 %and)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %bit, align 4, !tbaa !9
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_wb_write_unsigned_literal(%struct.aom_write_bit_buffer* %wb, i32 %data, i32 %bits) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %data.addr = alloca i32, align 4
  %bits.addr = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i32 %data, i32* %data.addr, align 4, !tbaa !9
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !9
  %0 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %bits.addr, align 4, !tbaa !9
  %sub = sub nsw i32 %1, 1
  store i32 %sub, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %bit, align 4, !tbaa !9
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %4 = load i32, i32* %data.addr, align 4, !tbaa !9
  %5 = load i32, i32* %bit, align 4, !tbaa !9
  %shr = lshr i32 %4, %5
  %and = and i32 %shr, 1
  call void @aom_wb_write_bit(%struct.aom_write_bit_buffer* %3, i32 %and)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %bit, align 4, !tbaa !9
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_wb_overwrite_literal(%struct.aom_write_bit_buffer* %wb, i32 %data, i32 %bits) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %data.addr = alloca i32, align 4
  %bits.addr = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i32 %data, i32* %data.addr, align 4, !tbaa !9
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !9
  %0 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %bits.addr, align 4, !tbaa !9
  %sub = sub nsw i32 %1, 1
  store i32 %sub, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %bit, align 4, !tbaa !9
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %4 = load i32, i32* %data.addr, align 4, !tbaa !9
  %5 = load i32, i32* %bit, align 4, !tbaa !9
  %shr = ashr i32 %4, %5
  %and = and i32 %shr, 1
  call void @aom_wb_overwrite_bit(%struct.aom_write_bit_buffer* %3, i32 %and)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %bit, align 4, !tbaa !9
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_wb_write_inv_signed_literal(%struct.aom_write_bit_buffer* %wb, i32 %data, i32 %bits) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %data.addr = alloca i32, align 4
  %bits.addr = alloca i32, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i32 %data, i32* %data.addr, align 4, !tbaa !9
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !9
  %0 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %1 = load i32, i32* %data.addr, align 4, !tbaa !9
  %2 = load i32, i32* %bits.addr, align 4, !tbaa !9
  %add = add nsw i32 %2, 1
  call void @aom_wb_write_literal(%struct.aom_write_bit_buffer* %0, i32 %1, i32 %add)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_wb_write_uvlc(%struct.aom_write_bit_buffer* %wb, i32 %v) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %v.addr = alloca i32, align 4
  %shift_val = alloca i64, align 8
  %leading_zeroes = alloca i32, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i32 %v, i32* %v.addr, align 4, !tbaa !9
  %0 = bitcast i64* %shift_val to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = load i32, i32* %v.addr, align 4, !tbaa !9
  %inc = add i32 %1, 1
  store i32 %inc, i32* %v.addr, align 4, !tbaa !9
  %conv = zext i32 %inc to i64
  store i64 %conv, i64* %shift_val, align 8, !tbaa !12
  %2 = bitcast i32* %leading_zeroes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 1, i32* %leading_zeroes, align 4, !tbaa !9
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i64, i64* %shift_val, align 8, !tbaa !12
  %shr = ashr i64 %3, 1
  store i64 %shr, i64* %shift_val, align 8, !tbaa !12
  %tobool = icmp ne i64 %shr, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i32, i32* %leading_zeroes, align 4, !tbaa !9
  %add = add nsw i32 %4, 2
  store i32 %add, i32* %leading_zeroes, align 4, !tbaa !9
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %5 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %6 = load i32, i32* %leading_zeroes, align 4, !tbaa !9
  %shr1 = ashr i32 %6, 1
  call void @aom_wb_write_literal(%struct.aom_write_bit_buffer* %5, i32 0, i32 %shr1)
  %7 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %8 = load i32, i32* %v.addr, align 4, !tbaa !9
  %9 = load i32, i32* %leading_zeroes, align 4, !tbaa !9
  %add2 = add nsw i32 %9, 1
  %shr3 = ashr i32 %add2, 1
  call void @aom_wb_write_unsigned_literal(%struct.aom_write_bit_buffer* %7, i32 %8, i32 %shr3)
  %10 = bitcast i32* %leading_zeroes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i64* %shift_val to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_wb_write_signed_primitive_refsubexpfin(%struct.aom_write_bit_buffer* %wb, i16 zeroext %n, i16 zeroext %k, i16 signext %ref, i16 signext %v) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %ref.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  %scaled_n = alloca i16, align 2
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !14
  store i16 %k, i16* %k.addr, align 2, !tbaa !14
  store i16 %ref, i16* %ref.addr, align 2, !tbaa !14
  store i16 %v, i16* %v.addr, align 2, !tbaa !14
  %0 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv = zext i16 %0 to i32
  %sub = sub nsw i32 %conv, 1
  %1 = load i16, i16* %ref.addr, align 2, !tbaa !14
  %conv1 = sext i16 %1 to i32
  %add = add nsw i32 %conv1, %sub
  %conv2 = trunc i32 %add to i16
  store i16 %conv2, i16* %ref.addr, align 2, !tbaa !14
  %2 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv3 = zext i16 %2 to i32
  %sub4 = sub nsw i32 %conv3, 1
  %3 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv5 = sext i16 %3 to i32
  %add6 = add nsw i32 %conv5, %sub4
  %conv7 = trunc i32 %add6 to i16
  store i16 %conv7, i16* %v.addr, align 2, !tbaa !14
  %4 = bitcast i16* %scaled_n to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  %5 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv8 = zext i16 %5 to i32
  %shl = shl i32 %conv8, 1
  %sub9 = sub nsw i32 %shl, 1
  %conv10 = trunc i32 %sub9 to i16
  store i16 %conv10, i16* %scaled_n, align 2, !tbaa !14
  %6 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %7 = load i16, i16* %scaled_n, align 2, !tbaa !14
  %8 = load i16, i16* %k.addr, align 2, !tbaa !14
  %9 = load i16, i16* %ref.addr, align 2, !tbaa !14
  %10 = load i16, i16* %v.addr, align 2, !tbaa !14
  call void @wb_write_primitive_refsubexpfin(%struct.aom_write_bit_buffer* %6, i16 zeroext %7, i16 zeroext %8, i16 zeroext %9, i16 zeroext %10)
  %11 = bitcast i16* %scaled_n to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %11) #4
  ret void
}

; Function Attrs: nounwind
define internal void @wb_write_primitive_refsubexpfin(%struct.aom_write_bit_buffer* %wb, i16 zeroext %n, i16 zeroext %k, i16 zeroext %ref, i16 zeroext %v) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %ref.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !14
  store i16 %k, i16* %k.addr, align 2, !tbaa !14
  store i16 %ref, i16* %ref.addr, align 2, !tbaa !14
  store i16 %v, i16* %v.addr, align 2, !tbaa !14
  %0 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %1 = load i16, i16* %n.addr, align 2, !tbaa !14
  %2 = load i16, i16* %k.addr, align 2, !tbaa !14
  %3 = load i16, i16* %n.addr, align 2, !tbaa !14
  %4 = load i16, i16* %ref.addr, align 2, !tbaa !14
  %5 = load i16, i16* %v.addr, align 2, !tbaa !14
  %call = call zeroext i16 @recenter_finite_nonneg(i16 zeroext %3, i16 zeroext %4, i16 zeroext %5)
  call void @wb_write_primitive_subexpfin(%struct.aom_write_bit_buffer* %0, i16 zeroext %1, i16 zeroext %2, i16 zeroext %call)
  ret void
}

; Function Attrs: nounwind
define internal void @wb_write_primitive_subexpfin(%struct.aom_write_bit_buffer* %wb, i16 zeroext %n, i16 zeroext %k, i16 zeroext %v) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  %i = alloca i32, align 4
  %mk = alloca i32, align 4
  %b = alloca i32, align 4
  %a = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %t = alloca i32, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !14
  store i16 %k, i16* %k.addr, align 2, !tbaa !14
  store i16 %v, i16* %v.addr, align 2, !tbaa !14
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !9
  %1 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %mk, align 4, !tbaa !9
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont26, %entry
  br label %while.body

while.body:                                       ; preds = %while.cond
  %2 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i32, i32* %i, align 4, !tbaa !9
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %4 = load i16, i16* %k.addr, align 2, !tbaa !14
  %conv = zext i16 %4 to i32
  %5 = load i32, i32* %i, align 4, !tbaa !9
  %add = add nsw i32 %conv, %5
  %sub = sub nsw i32 %add, 1
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %6 = load i16, i16* %k.addr, align 2, !tbaa !14
  %conv1 = zext i16 %6 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ %conv1, %cond.false ]
  store i32 %cond, i32* %b, align 4, !tbaa !9
  %7 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %b, align 4, !tbaa !9
  %shl = shl i32 1, %8
  store i32 %shl, i32* %a, align 4, !tbaa !9
  %9 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv2 = zext i16 %9 to i32
  %10 = load i32, i32* %mk, align 4, !tbaa !9
  %11 = load i32, i32* %a, align 4, !tbaa !9
  %mul = mul nsw i32 3, %11
  %add3 = add nsw i32 %10, %mul
  %cmp = icmp sle i32 %conv2, %add3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %12 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %13 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv5 = zext i16 %13 to i32
  %14 = load i32, i32* %mk, align 4, !tbaa !9
  %sub6 = sub nsw i32 %conv5, %14
  %conv7 = trunc i32 %sub6 to i16
  %15 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv8 = zext i16 %15 to i32
  %16 = load i32, i32* %mk, align 4, !tbaa !9
  %sub9 = sub nsw i32 %conv8, %16
  %conv10 = trunc i32 %sub9 to i16
  call void @wb_write_primitive_quniform(%struct.aom_write_bit_buffer* %12, i16 zeroext %conv7, i16 zeroext %conv10)
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

if.else:                                          ; preds = %cond.end
  %17 = bitcast i32* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv11 = zext i16 %18 to i32
  %19 = load i32, i32* %mk, align 4, !tbaa !9
  %20 = load i32, i32* %a, align 4, !tbaa !9
  %add12 = add nsw i32 %19, %20
  %cmp13 = icmp sge i32 %conv11, %add12
  %conv14 = zext i1 %cmp13 to i32
  store i32 %conv14, i32* %t, align 4, !tbaa !9
  %21 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %22 = load i32, i32* %t, align 4, !tbaa !9
  call void @aom_wb_write_bit(%struct.aom_write_bit_buffer* %21, i32 %22)
  %23 = load i32, i32* %t, align 4, !tbaa !9
  %tobool15 = icmp ne i32 %23, 0
  br i1 %tobool15, label %if.then16, label %if.else19

if.then16:                                        ; preds = %if.else
  %24 = load i32, i32* %i, align 4, !tbaa !9
  %add17 = add nsw i32 %24, 1
  store i32 %add17, i32* %i, align 4, !tbaa !9
  %25 = load i32, i32* %a, align 4, !tbaa !9
  %26 = load i32, i32* %mk, align 4, !tbaa !9
  %add18 = add nsw i32 %26, %25
  store i32 %add18, i32* %mk, align 4, !tbaa !9
  br label %if.end

if.else19:                                        ; preds = %if.else
  %27 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %28 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv20 = zext i16 %28 to i32
  %29 = load i32, i32* %mk, align 4, !tbaa !9
  %sub21 = sub nsw i32 %conv20, %29
  %30 = load i32, i32* %b, align 4, !tbaa !9
  call void @aom_wb_write_literal(%struct.aom_write_bit_buffer* %27, i32 %sub21, i32 %30)
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then16
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else19
  %31 = bitcast i32* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup23 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end22

if.end22:                                         ; preds = %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.end22, %cleanup, %if.then
  %32 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #4
  %33 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  %cleanup.dest25 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest25, label %unreachable [
    i32 0, label %cleanup.cont26
    i32 3, label %while.end
  ]

cleanup.cont26:                                   ; preds = %cleanup23
  br label %while.cond

while.end:                                        ; preds = %cleanup23
  %34 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  %35 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  ret void

unreachable:                                      ; preds = %cleanup23
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @recenter_finite_nonneg(i16 zeroext %n, i16 zeroext %r, i16 zeroext %v) #2 {
entry:
  %retval = alloca i16, align 2
  %n.addr = alloca i16, align 2
  %r.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store i16 %n, i16* %n.addr, align 2, !tbaa !14
  store i16 %r, i16* %r.addr, align 2, !tbaa !14
  store i16 %v, i16* %v.addr, align 2, !tbaa !14
  %0 = load i16, i16* %r.addr, align 2, !tbaa !14
  %conv = zext i16 %0 to i32
  %shl = shl i32 %conv, 1
  %1 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv1 = zext i16 %1 to i32
  %cmp = icmp sle i32 %shl, %conv1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i16, i16* %r.addr, align 2, !tbaa !14
  %3 = load i16, i16* %v.addr, align 2, !tbaa !14
  %call = call zeroext i16 @recenter_nonneg(i16 zeroext %2, i16 zeroext %3)
  store i16 %call, i16* %retval, align 2
  br label %return

if.else:                                          ; preds = %entry
  %4 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv3 = zext i16 %4 to i32
  %sub = sub nsw i32 %conv3, 1
  %5 = load i16, i16* %r.addr, align 2, !tbaa !14
  %conv4 = zext i16 %5 to i32
  %sub5 = sub nsw i32 %sub, %conv4
  %conv6 = trunc i32 %sub5 to i16
  %6 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv7 = zext i16 %6 to i32
  %sub8 = sub nsw i32 %conv7, 1
  %7 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv9 = zext i16 %7 to i32
  %sub10 = sub nsw i32 %sub8, %conv9
  %conv11 = trunc i32 %sub10 to i16
  %call12 = call zeroext i16 @recenter_nonneg(i16 zeroext %conv6, i16 zeroext %conv11)
  store i16 %call12, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.else, %if.then
  %8 = load i16, i16* %retval, align 2
  ret i16 %8
}

; Function Attrs: nounwind
define internal void @wb_write_primitive_quniform(%struct.aom_write_bit_buffer* %wb, i16 zeroext %n, i16 zeroext %v) #0 {
entry:
  %wb.addr = alloca %struct.aom_write_bit_buffer*, align 4
  %n.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  %l = alloca i32, align 4
  %m = alloca i32, align 4
  store %struct.aom_write_bit_buffer* %wb, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !14
  store i16 %v, i16* %v.addr, align 2, !tbaa !14
  %0 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv = zext i16 %0 to i32
  %cmp = icmp sle i32 %conv, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv2 = zext i16 %2 to i32
  %call = call i32 @get_msb(i32 %conv2)
  %add = add nsw i32 %call, 1
  store i32 %add, i32* %l, align 4, !tbaa !9
  %3 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %l, align 4, !tbaa !9
  %shl = shl i32 1, %4
  %5 = load i16, i16* %n.addr, align 2, !tbaa !14
  %conv3 = zext i16 %5 to i32
  %sub = sub nsw i32 %shl, %conv3
  store i32 %sub, i32* %m, align 4, !tbaa !9
  %6 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv4 = zext i16 %6 to i32
  %7 = load i32, i32* %m, align 4, !tbaa !9
  %cmp5 = icmp slt i32 %conv4, %7
  br i1 %cmp5, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end
  %8 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %9 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv8 = zext i16 %9 to i32
  %10 = load i32, i32* %l, align 4, !tbaa !9
  %sub9 = sub nsw i32 %10, 1
  call void @aom_wb_write_literal(%struct.aom_write_bit_buffer* %8, i32 %conv8, i32 %sub9)
  br label %if.end16

if.else:                                          ; preds = %if.end
  %11 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %12 = load i32, i32* %m, align 4, !tbaa !9
  %13 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv10 = zext i16 %13 to i32
  %14 = load i32, i32* %m, align 4, !tbaa !9
  %sub11 = sub nsw i32 %conv10, %14
  %shr = ashr i32 %sub11, 1
  %add12 = add nsw i32 %12, %shr
  %15 = load i32, i32* %l, align 4, !tbaa !9
  %sub13 = sub nsw i32 %15, 1
  call void @aom_wb_write_literal(%struct.aom_write_bit_buffer* %11, i32 %add12, i32 %sub13)
  %16 = load %struct.aom_write_bit_buffer*, %struct.aom_write_bit_buffer** %wb.addr, align 4, !tbaa !2
  %17 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv14 = zext i16 %17 to i32
  %18 = load i32, i32* %m, align 4, !tbaa !9
  %sub15 = sub nsw i32 %conv14, %18
  %and = and i32 %sub15, 1
  call void @aom_wb_write_bit(%struct.aom_write_bit_buffer* %16, i32 %and)
  br label %if.end16

if.end16:                                         ; preds = %if.else, %if.then7
  %19 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  %20 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  br label %return

return:                                           ; preds = %if.end16, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #2 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !9
  %0 = load i32, i32* %n.addr, align 4, !tbaa !9
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #3

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @recenter_nonneg(i16 zeroext %r, i16 zeroext %v) #2 {
entry:
  %retval = alloca i16, align 2
  %r.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store i16 %r, i16* %r.addr, align 2, !tbaa !14
  store i16 %v, i16* %v.addr, align 2, !tbaa !14
  %0 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv = zext i16 %0 to i32
  %1 = load i16, i16* %r.addr, align 2, !tbaa !14
  %conv1 = zext i16 %1 to i32
  %shl = shl i32 %conv1, 1
  %cmp = icmp sgt i32 %conv, %shl
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i16, i16* %v.addr, align 2, !tbaa !14
  store i16 %2, i16* %retval, align 2
  br label %return

if.else:                                          ; preds = %entry
  %3 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv3 = zext i16 %3 to i32
  %4 = load i16, i16* %r.addr, align 2, !tbaa !14
  %conv4 = zext i16 %4 to i32
  %cmp5 = icmp sge i32 %conv3, %conv4
  br i1 %cmp5, label %if.then7, label %if.else12

if.then7:                                         ; preds = %if.else
  %5 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv8 = zext i16 %5 to i32
  %6 = load i16, i16* %r.addr, align 2, !tbaa !14
  %conv9 = zext i16 %6 to i32
  %sub = sub nsw i32 %conv8, %conv9
  %shl10 = shl i32 %sub, 1
  %conv11 = trunc i32 %shl10 to i16
  store i16 %conv11, i16* %retval, align 2
  br label %return

if.else12:                                        ; preds = %if.else
  %7 = load i16, i16* %r.addr, align 2, !tbaa !14
  %conv13 = zext i16 %7 to i32
  %8 = load i16, i16* %v.addr, align 2, !tbaa !14
  %conv14 = zext i16 %8 to i32
  %sub15 = sub nsw i32 %conv13, %conv14
  %shl16 = shl i32 %sub15, 1
  %sub17 = sub nsw i32 %shl16, 1
  %conv18 = trunc i32 %sub17 to i16
  store i16 %conv18, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.else12, %if.then7, %if.then
  %9 = load i16, i16* %retval, align 2
  ret i16 %9
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone speculatable willreturn }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 4}
!7 = !{!"aom_write_bit_buffer", !3, i64 0, !8, i64 4}
!8 = !{!"int", !4, i64 0}
!9 = !{!8, !8, i64 0}
!10 = !{!7, !3, i64 0}
!11 = !{!4, !4, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"long long", !4, i64 0}
!14 = !{!15, !15, i64 0}
!15 = !{!"short", !4, i64 0}
