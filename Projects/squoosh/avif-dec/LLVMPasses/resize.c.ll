; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/resize.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/resize.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }

@av1_resize_filter_normative = hidden constant [64 x [8 x i16]] [[8 x i16] [i16 0, i16 0, i16 0, i16 128, i16 0, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -1, i16 128, i16 2, i16 -1, i16 0, i16 0], [8 x i16] [i16 0, i16 1, i16 -3, i16 127, i16 4, i16 -2, i16 1, i16 0], [8 x i16] [i16 0, i16 1, i16 -4, i16 127, i16 6, i16 -3, i16 1, i16 0], [8 x i16] [i16 0, i16 2, i16 -6, i16 126, i16 8, i16 -3, i16 1, i16 0], [8 x i16] [i16 0, i16 2, i16 -7, i16 125, i16 11, i16 -4, i16 1, i16 0], [8 x i16] [i16 -1, i16 2, i16 -8, i16 125, i16 13, i16 -5, i16 2, i16 0], [8 x i16] [i16 -1, i16 3, i16 -9, i16 124, i16 15, i16 -6, i16 2, i16 0], [8 x i16] [i16 -1, i16 3, i16 -10, i16 123, i16 18, i16 -6, i16 2, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -11, i16 122, i16 20, i16 -7, i16 3, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -12, i16 121, i16 22, i16 -8, i16 3, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -13, i16 120, i16 25, i16 -9, i16 3, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -14, i16 118, i16 28, i16 -9, i16 3, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -15, i16 117, i16 30, i16 -10, i16 4, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -16, i16 116, i16 32, i16 -11, i16 4, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -16, i16 114, i16 35, i16 -12, i16 4, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -17, i16 112, i16 38, i16 -12, i16 4, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -18, i16 111, i16 40, i16 -13, i16 5, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -18, i16 109, i16 43, i16 -14, i16 5, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -19, i16 107, i16 45, i16 -14, i16 5, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -19, i16 105, i16 48, i16 -15, i16 5, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -19, i16 103, i16 51, i16 -16, i16 5, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -20, i16 101, i16 53, i16 -16, i16 6, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -20, i16 99, i16 56, i16 -17, i16 6, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -20, i16 97, i16 58, i16 -17, i16 6, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -20, i16 95, i16 61, i16 -18, i16 6, i16 -1], [8 x i16] [i16 -2, i16 7, i16 -20, i16 93, i16 64, i16 -18, i16 6, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -20, i16 91, i16 66, i16 -19, i16 6, i16 -1], [8 x i16] [i16 -2, i16 7, i16 -20, i16 88, i16 69, i16 -19, i16 6, i16 -1], [8 x i16] [i16 -2, i16 7, i16 -20, i16 86, i16 71, i16 -19, i16 6, i16 -1], [8 x i16] [i16 -2, i16 7, i16 -20, i16 84, i16 74, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -20, i16 81, i16 76, i16 -20, i16 7, i16 -1], [8 x i16] [i16 -2, i16 7, i16 -20, i16 79, i16 79, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -1, i16 7, i16 -20, i16 76, i16 81, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -2, i16 7, i16 -20, i16 74, i16 84, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -1, i16 6, i16 -19, i16 71, i16 86, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -1, i16 6, i16 -19, i16 69, i16 88, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -1, i16 6, i16 -19, i16 66, i16 91, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -2, i16 6, i16 -18, i16 64, i16 93, i16 -20, i16 7, i16 -2], [8 x i16] [i16 -1, i16 6, i16 -18, i16 61, i16 95, i16 -20, i16 6, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -17, i16 58, i16 97, i16 -20, i16 6, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -17, i16 56, i16 99, i16 -20, i16 6, i16 -1], [8 x i16] [i16 -1, i16 6, i16 -16, i16 53, i16 101, i16 -20, i16 6, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -16, i16 51, i16 103, i16 -19, i16 6, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -15, i16 48, i16 105, i16 -19, i16 6, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -14, i16 45, i16 107, i16 -19, i16 6, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -14, i16 43, i16 109, i16 -18, i16 5, i16 -1], [8 x i16] [i16 -1, i16 5, i16 -13, i16 40, i16 111, i16 -18, i16 5, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -12, i16 38, i16 112, i16 -17, i16 5, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -12, i16 35, i16 114, i16 -16, i16 5, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -11, i16 32, i16 116, i16 -16, i16 5, i16 -1], [8 x i16] [i16 -1, i16 4, i16 -10, i16 30, i16 117, i16 -15, i16 4, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -9, i16 28, i16 118, i16 -14, i16 4, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -9, i16 25, i16 120, i16 -13, i16 4, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -8, i16 22, i16 121, i16 -12, i16 4, i16 -1], [8 x i16] [i16 -1, i16 3, i16 -7, i16 20, i16 122, i16 -11, i16 3, i16 -1], [8 x i16] [i16 -1, i16 2, i16 -6, i16 18, i16 123, i16 -10, i16 3, i16 -1], [8 x i16] [i16 0, i16 2, i16 -6, i16 15, i16 124, i16 -9, i16 3, i16 -1], [8 x i16] [i16 0, i16 2, i16 -5, i16 13, i16 125, i16 -8, i16 2, i16 -1], [8 x i16] [i16 0, i16 1, i16 -4, i16 11, i16 125, i16 -7, i16 2, i16 0], [8 x i16] [i16 0, i16 1, i16 -3, i16 8, i16 126, i16 -6, i16 2, i16 0], [8 x i16] [i16 0, i16 1, i16 -3, i16 6, i16 127, i16 -4, i16 1, i16 0], [8 x i16] [i16 0, i16 1, i16 -2, i16 4, i16 127, i16 -3, i16 1, i16 0], [8 x i16] [i16 0, i16 0, i16 -1, i16 2, i16 128, i16 -1, i16 0, i16 0]], align 16
@.str = private unnamed_addr constant [54 x i8] c"Failed to allocate copy buffer for superres upscaling\00", align 1
@.str.1 = private unnamed_addr constant [62 x i8] c"Failed to free current frame buffer before superres upscaling\00", align 1
@.str.2 = private unnamed_addr constant [63 x i8] c"Failed to allocate current frame buffer for superres upscaling\00", align 1
@.str.3 = private unnamed_addr constant [65 x i8] c"Failed to reallocate current frame buffer for superres upscaling\00", align 1
@av1_down2_symodd_half_filter = internal constant [4 x i16] [i16 64, i16 35, i16 0, i16 -3], align 2
@av1_down2_symeven_half_filter = internal constant [4 x i16] [i16 56, i16 12, i16 -3, i16 -1], align 2
@filteredinterp_filters875 = internal constant [64 x [8 x i16]] [[8 x i16] [i16 3, i16 -8, i16 13, i16 112, i16 13, i16 -8, i16 3, i16 0], [8 x i16] [i16 2, i16 -7, i16 12, i16 112, i16 15, i16 -8, i16 3, i16 -1], [8 x i16] [i16 3, i16 -7, i16 10, i16 112, i16 17, i16 -9, i16 3, i16 -1], [8 x i16] [i16 2, i16 -6, i16 8, i16 112, i16 19, i16 -9, i16 3, i16 -1], [8 x i16] [i16 2, i16 -6, i16 7, i16 112, i16 21, i16 -10, i16 3, i16 -1], [8 x i16] [i16 2, i16 -5, i16 6, i16 111, i16 22, i16 -10, i16 3, i16 -1], [8 x i16] [i16 2, i16 -5, i16 4, i16 111, i16 24, i16 -10, i16 3, i16 -1], [8 x i16] [i16 2, i16 -4, i16 3, i16 110, i16 26, i16 -11, i16 3, i16 -1], [8 x i16] [i16 2, i16 -4, i16 1, i16 110, i16 28, i16 -11, i16 3, i16 -1], [8 x i16] [i16 2, i16 -4, i16 0, i16 109, i16 30, i16 -12, i16 4, i16 -1], [8 x i16] [i16 1, i16 -3, i16 -1, i16 108, i16 32, i16 -12, i16 4, i16 -1], [8 x i16] [i16 1, i16 -3, i16 -2, i16 108, i16 34, i16 -13, i16 4, i16 -1], [8 x i16] [i16 1, i16 -2, i16 -4, i16 107, i16 36, i16 -13, i16 4, i16 -1], [8 x i16] [i16 1, i16 -2, i16 -5, i16 106, i16 38, i16 -13, i16 4, i16 -1], [8 x i16] [i16 1, i16 -1, i16 -6, i16 105, i16 40, i16 -14, i16 4, i16 -1], [8 x i16] [i16 1, i16 -1, i16 -7, i16 104, i16 42, i16 -14, i16 4, i16 -1], [8 x i16] [i16 1, i16 -1, i16 -7, i16 103, i16 44, i16 -15, i16 4, i16 -1], [8 x i16] [i16 1, i16 0, i16 -8, i16 101, i16 46, i16 -15, i16 4, i16 -1], [8 x i16] [i16 1, i16 0, i16 -9, i16 100, i16 48, i16 -15, i16 4, i16 -1], [8 x i16] [i16 1, i16 0, i16 -10, i16 99, i16 50, i16 -15, i16 4, i16 -1], [8 x i16] [i16 1, i16 1, i16 -11, i16 97, i16 53, i16 -16, i16 4, i16 -1], [8 x i16] [i16 0, i16 1, i16 -11, i16 96, i16 55, i16 -16, i16 4, i16 -1], [8 x i16] [i16 0, i16 1, i16 -12, i16 95, i16 57, i16 -16, i16 4, i16 -1], [8 x i16] [i16 0, i16 2, i16 -13, i16 93, i16 59, i16 -16, i16 4, i16 -1], [8 x i16] [i16 0, i16 2, i16 -13, i16 91, i16 61, i16 -16, i16 4, i16 -1], [8 x i16] [i16 0, i16 2, i16 -14, i16 90, i16 63, i16 -16, i16 4, i16 -1], [8 x i16] [i16 0, i16 2, i16 -14, i16 88, i16 65, i16 -16, i16 4, i16 -1], [8 x i16] [i16 0, i16 2, i16 -15, i16 86, i16 67, i16 -16, i16 4, i16 0], [8 x i16] [i16 0, i16 3, i16 -15, i16 84, i16 69, i16 -17, i16 4, i16 0], [8 x i16] [i16 0, i16 3, i16 -16, i16 83, i16 71, i16 -17, i16 4, i16 0], [8 x i16] [i16 0, i16 3, i16 -16, i16 81, i16 73, i16 -16, i16 3, i16 0], [8 x i16] [i16 0, i16 3, i16 -16, i16 79, i16 75, i16 -16, i16 3, i16 0], [8 x i16] [i16 0, i16 3, i16 -16, i16 77, i16 77, i16 -16, i16 3, i16 0], [8 x i16] [i16 0, i16 3, i16 -16, i16 75, i16 79, i16 -16, i16 3, i16 0], [8 x i16] [i16 0, i16 3, i16 -16, i16 73, i16 81, i16 -16, i16 3, i16 0], [8 x i16] [i16 0, i16 4, i16 -17, i16 71, i16 83, i16 -16, i16 3, i16 0], [8 x i16] [i16 0, i16 4, i16 -17, i16 69, i16 84, i16 -15, i16 3, i16 0], [8 x i16] [i16 0, i16 4, i16 -16, i16 67, i16 86, i16 -15, i16 2, i16 0], [8 x i16] [i16 -1, i16 4, i16 -16, i16 65, i16 88, i16 -14, i16 2, i16 0], [8 x i16] [i16 -1, i16 4, i16 -16, i16 63, i16 90, i16 -14, i16 2, i16 0], [8 x i16] [i16 -1, i16 4, i16 -16, i16 61, i16 91, i16 -13, i16 2, i16 0], [8 x i16] [i16 -1, i16 4, i16 -16, i16 59, i16 93, i16 -13, i16 2, i16 0], [8 x i16] [i16 -1, i16 4, i16 -16, i16 57, i16 95, i16 -12, i16 1, i16 0], [8 x i16] [i16 -1, i16 4, i16 -16, i16 55, i16 96, i16 -11, i16 1, i16 0], [8 x i16] [i16 -1, i16 4, i16 -16, i16 53, i16 97, i16 -11, i16 1, i16 1], [8 x i16] [i16 -1, i16 4, i16 -15, i16 50, i16 99, i16 -10, i16 0, i16 1], [8 x i16] [i16 -1, i16 4, i16 -15, i16 48, i16 100, i16 -9, i16 0, i16 1], [8 x i16] [i16 -1, i16 4, i16 -15, i16 46, i16 101, i16 -8, i16 0, i16 1], [8 x i16] [i16 -1, i16 4, i16 -15, i16 44, i16 103, i16 -7, i16 -1, i16 1], [8 x i16] [i16 -1, i16 4, i16 -14, i16 42, i16 104, i16 -7, i16 -1, i16 1], [8 x i16] [i16 -1, i16 4, i16 -14, i16 40, i16 105, i16 -6, i16 -1, i16 1], [8 x i16] [i16 -1, i16 4, i16 -13, i16 38, i16 106, i16 -5, i16 -2, i16 1], [8 x i16] [i16 -1, i16 4, i16 -13, i16 36, i16 107, i16 -4, i16 -2, i16 1], [8 x i16] [i16 -1, i16 4, i16 -13, i16 34, i16 108, i16 -2, i16 -3, i16 1], [8 x i16] [i16 -1, i16 4, i16 -12, i16 32, i16 108, i16 -1, i16 -3, i16 1], [8 x i16] [i16 -1, i16 4, i16 -12, i16 30, i16 109, i16 0, i16 -4, i16 2], [8 x i16] [i16 -1, i16 3, i16 -11, i16 28, i16 110, i16 1, i16 -4, i16 2], [8 x i16] [i16 -1, i16 3, i16 -11, i16 26, i16 110, i16 3, i16 -4, i16 2], [8 x i16] [i16 -1, i16 3, i16 -10, i16 24, i16 111, i16 4, i16 -5, i16 2], [8 x i16] [i16 -1, i16 3, i16 -10, i16 22, i16 111, i16 6, i16 -5, i16 2], [8 x i16] [i16 -1, i16 3, i16 -10, i16 21, i16 112, i16 7, i16 -6, i16 2], [8 x i16] [i16 -1, i16 3, i16 -9, i16 19, i16 112, i16 8, i16 -6, i16 2], [8 x i16] [i16 -1, i16 3, i16 -9, i16 17, i16 112, i16 10, i16 -7, i16 3], [8 x i16] [i16 -1, i16 3, i16 -8, i16 15, i16 112, i16 12, i16 -7, i16 2]], align 16
@filteredinterp_filters750 = internal constant [64 x [8 x i16]] [[8 x i16] [i16 2, i16 -11, i16 25, i16 96, i16 25, i16 -11, i16 2, i16 0], [8 x i16] [i16 2, i16 -11, i16 24, i16 96, i16 26, i16 -11, i16 2, i16 0], [8 x i16] [i16 2, i16 -11, i16 22, i16 96, i16 28, i16 -11, i16 2, i16 0], [8 x i16] [i16 2, i16 -10, i16 21, i16 96, i16 29, i16 -12, i16 2, i16 0], [8 x i16] [i16 2, i16 -10, i16 19, i16 96, i16 31, i16 -12, i16 2, i16 0], [8 x i16] [i16 2, i16 -10, i16 18, i16 95, i16 32, i16 -11, i16 2, i16 0], [8 x i16] [i16 2, i16 -10, i16 17, i16 95, i16 34, i16 -12, i16 2, i16 0], [8 x i16] [i16 2, i16 -9, i16 15, i16 95, i16 35, i16 -12, i16 2, i16 0], [8 x i16] [i16 2, i16 -9, i16 14, i16 94, i16 37, i16 -12, i16 2, i16 0], [8 x i16] [i16 2, i16 -9, i16 13, i16 94, i16 38, i16 -12, i16 2, i16 0], [8 x i16] [i16 2, i16 -8, i16 12, i16 93, i16 40, i16 -12, i16 1, i16 0], [8 x i16] [i16 2, i16 -8, i16 11, i16 93, i16 41, i16 -12, i16 1, i16 0], [8 x i16] [i16 2, i16 -8, i16 9, i16 92, i16 43, i16 -12, i16 1, i16 1], [8 x i16] [i16 2, i16 -8, i16 8, i16 92, i16 44, i16 -12, i16 1, i16 1], [8 x i16] [i16 2, i16 -7, i16 7, i16 91, i16 46, i16 -12, i16 1, i16 0], [8 x i16] [i16 2, i16 -7, i16 6, i16 90, i16 47, i16 -12, i16 1, i16 1], [8 x i16] [i16 2, i16 -7, i16 5, i16 90, i16 49, i16 -12, i16 1, i16 0], [8 x i16] [i16 2, i16 -6, i16 4, i16 89, i16 50, i16 -12, i16 1, i16 0], [8 x i16] [i16 2, i16 -6, i16 3, i16 88, i16 52, i16 -12, i16 0, i16 1], [8 x i16] [i16 2, i16 -6, i16 2, i16 87, i16 54, i16 -12, i16 0, i16 1], [8 x i16] [i16 2, i16 -5, i16 1, i16 86, i16 55, i16 -12, i16 0, i16 1], [8 x i16] [i16 2, i16 -5, i16 0, i16 85, i16 57, i16 -12, i16 0, i16 1], [8 x i16] [i16 2, i16 -5, i16 -1, i16 84, i16 58, i16 -11, i16 0, i16 1], [8 x i16] [i16 2, i16 -5, i16 -2, i16 83, i16 60, i16 -11, i16 0, i16 1], [8 x i16] [i16 2, i16 -4, i16 -2, i16 82, i16 61, i16 -11, i16 -1, i16 1], [8 x i16] [i16 1, i16 -4, i16 -3, i16 81, i16 63, i16 -10, i16 -1, i16 1], [8 x i16] [i16 2, i16 -4, i16 -4, i16 80, i16 64, i16 -10, i16 -1, i16 1], [8 x i16] [i16 1, i16 -4, i16 -4, i16 79, i16 66, i16 -10, i16 -1, i16 1], [8 x i16] [i16 1, i16 -3, i16 -5, i16 77, i16 67, i16 -9, i16 -1, i16 1], [8 x i16] [i16 1, i16 -3, i16 -6, i16 76, i16 69, i16 -9, i16 -1, i16 1], [8 x i16] [i16 1, i16 -3, i16 -6, i16 75, i16 70, i16 -8, i16 -2, i16 1], [8 x i16] [i16 1, i16 -2, i16 -7, i16 74, i16 71, i16 -8, i16 -2, i16 1], [8 x i16] [i16 1, i16 -2, i16 -7, i16 72, i16 72, i16 -7, i16 -2, i16 1], [8 x i16] [i16 1, i16 -2, i16 -8, i16 71, i16 74, i16 -7, i16 -2, i16 1], [8 x i16] [i16 1, i16 -2, i16 -8, i16 70, i16 75, i16 -6, i16 -3, i16 1], [8 x i16] [i16 1, i16 -1, i16 -9, i16 69, i16 76, i16 -6, i16 -3, i16 1], [8 x i16] [i16 1, i16 -1, i16 -9, i16 67, i16 77, i16 -5, i16 -3, i16 1], [8 x i16] [i16 1, i16 -1, i16 -10, i16 66, i16 79, i16 -4, i16 -4, i16 1], [8 x i16] [i16 1, i16 -1, i16 -10, i16 64, i16 80, i16 -4, i16 -4, i16 2], [8 x i16] [i16 1, i16 -1, i16 -10, i16 63, i16 81, i16 -3, i16 -4, i16 1], [8 x i16] [i16 1, i16 -1, i16 -11, i16 61, i16 82, i16 -2, i16 -4, i16 2], [8 x i16] [i16 1, i16 0, i16 -11, i16 60, i16 83, i16 -2, i16 -5, i16 2], [8 x i16] [i16 1, i16 0, i16 -11, i16 58, i16 84, i16 -1, i16 -5, i16 2], [8 x i16] [i16 1, i16 0, i16 -12, i16 57, i16 85, i16 0, i16 -5, i16 2], [8 x i16] [i16 1, i16 0, i16 -12, i16 55, i16 86, i16 1, i16 -5, i16 2], [8 x i16] [i16 1, i16 0, i16 -12, i16 54, i16 87, i16 2, i16 -6, i16 2], [8 x i16] [i16 1, i16 0, i16 -12, i16 52, i16 88, i16 3, i16 -6, i16 2], [8 x i16] [i16 0, i16 1, i16 -12, i16 50, i16 89, i16 4, i16 -6, i16 2], [8 x i16] [i16 0, i16 1, i16 -12, i16 49, i16 90, i16 5, i16 -7, i16 2], [8 x i16] [i16 1, i16 1, i16 -12, i16 47, i16 90, i16 6, i16 -7, i16 2], [8 x i16] [i16 0, i16 1, i16 -12, i16 46, i16 91, i16 7, i16 -7, i16 2], [8 x i16] [i16 1, i16 1, i16 -12, i16 44, i16 92, i16 8, i16 -8, i16 2], [8 x i16] [i16 1, i16 1, i16 -12, i16 43, i16 92, i16 9, i16 -8, i16 2], [8 x i16] [i16 0, i16 1, i16 -12, i16 41, i16 93, i16 11, i16 -8, i16 2], [8 x i16] [i16 0, i16 1, i16 -12, i16 40, i16 93, i16 12, i16 -8, i16 2], [8 x i16] [i16 0, i16 2, i16 -12, i16 38, i16 94, i16 13, i16 -9, i16 2], [8 x i16] [i16 0, i16 2, i16 -12, i16 37, i16 94, i16 14, i16 -9, i16 2], [8 x i16] [i16 0, i16 2, i16 -12, i16 35, i16 95, i16 15, i16 -9, i16 2], [8 x i16] [i16 0, i16 2, i16 -12, i16 34, i16 95, i16 17, i16 -10, i16 2], [8 x i16] [i16 0, i16 2, i16 -11, i16 32, i16 95, i16 18, i16 -10, i16 2], [8 x i16] [i16 0, i16 2, i16 -12, i16 31, i16 96, i16 19, i16 -10, i16 2], [8 x i16] [i16 0, i16 2, i16 -12, i16 29, i16 96, i16 21, i16 -10, i16 2], [8 x i16] [i16 0, i16 2, i16 -11, i16 28, i16 96, i16 22, i16 -11, i16 2], [8 x i16] [i16 0, i16 2, i16 -11, i16 26, i16 96, i16 24, i16 -11, i16 2]], align 16
@filteredinterp_filters625 = internal constant [64 x [8 x i16]] [[8 x i16] [i16 -1, i16 -8, i16 33, i16 80, i16 33, i16 -8, i16 -1, i16 0], [8 x i16] [i16 -1, i16 -8, i16 31, i16 80, i16 34, i16 -8, i16 -1, i16 1], [8 x i16] [i16 -1, i16 -8, i16 30, i16 80, i16 35, i16 -8, i16 -1, i16 1], [8 x i16] [i16 -1, i16 -8, i16 29, i16 80, i16 36, i16 -7, i16 -2, i16 1], [8 x i16] [i16 -1, i16 -8, i16 28, i16 80, i16 37, i16 -7, i16 -2, i16 1], [8 x i16] [i16 -1, i16 -8, i16 27, i16 80, i16 38, i16 -7, i16 -2, i16 1], [8 x i16] [i16 0, i16 -8, i16 26, i16 79, i16 39, i16 -7, i16 -2, i16 1], [8 x i16] [i16 0, i16 -8, i16 25, i16 79, i16 40, i16 -7, i16 -2, i16 1], [8 x i16] [i16 0, i16 -8, i16 24, i16 79, i16 41, i16 -7, i16 -2, i16 1], [8 x i16] [i16 0, i16 -8, i16 23, i16 78, i16 42, i16 -6, i16 -2, i16 1], [8 x i16] [i16 0, i16 -8, i16 22, i16 78, i16 43, i16 -6, i16 -2, i16 1], [8 x i16] [i16 0, i16 -8, i16 21, i16 78, i16 44, i16 -6, i16 -2, i16 1], [8 x i16] [i16 0, i16 -8, i16 20, i16 78, i16 45, i16 -5, i16 -3, i16 1], [8 x i16] [i16 0, i16 -8, i16 19, i16 77, i16 47, i16 -5, i16 -3, i16 1], [8 x i16] [i16 0, i16 -8, i16 18, i16 77, i16 48, i16 -5, i16 -3, i16 1], [8 x i16] [i16 0, i16 -8, i16 17, i16 77, i16 49, i16 -5, i16 -3, i16 1], [8 x i16] [i16 0, i16 -8, i16 16, i16 76, i16 50, i16 -4, i16 -3, i16 1], [8 x i16] [i16 0, i16 -8, i16 15, i16 76, i16 51, i16 -4, i16 -3, i16 1], [8 x i16] [i16 0, i16 -8, i16 15, i16 75, i16 52, i16 -3, i16 -4, i16 1], [8 x i16] [i16 0, i16 -7, i16 14, i16 74, i16 53, i16 -3, i16 -4, i16 1], [8 x i16] [i16 0, i16 -7, i16 13, i16 74, i16 54, i16 -3, i16 -4, i16 1], [8 x i16] [i16 0, i16 -7, i16 12, i16 73, i16 55, i16 -2, i16 -4, i16 1], [8 x i16] [i16 0, i16 -7, i16 11, i16 73, i16 56, i16 -2, i16 -4, i16 1], [8 x i16] [i16 0, i16 -7, i16 10, i16 72, i16 57, i16 -1, i16 -4, i16 1], [8 x i16] [i16 1, i16 -7, i16 10, i16 71, i16 58, i16 -1, i16 -5, i16 1], [8 x i16] [i16 0, i16 -7, i16 9, i16 71, i16 59, i16 0, i16 -5, i16 1], [8 x i16] [i16 1, i16 -7, i16 8, i16 70, i16 60, i16 0, i16 -5, i16 1], [8 x i16] [i16 1, i16 -7, i16 7, i16 69, i16 61, i16 1, i16 -5, i16 1], [8 x i16] [i16 1, i16 -6, i16 6, i16 68, i16 62, i16 1, i16 -5, i16 1], [8 x i16] [i16 0, i16 -6, i16 6, i16 68, i16 62, i16 2, i16 -5, i16 1], [8 x i16] [i16 1, i16 -6, i16 5, i16 67, i16 63, i16 2, i16 -5, i16 1], [8 x i16] [i16 1, i16 -6, i16 5, i16 66, i16 64, i16 3, i16 -6, i16 1], [8 x i16] [i16 1, i16 -6, i16 4, i16 65, i16 65, i16 4, i16 -6, i16 1], [8 x i16] [i16 1, i16 -6, i16 3, i16 64, i16 66, i16 5, i16 -6, i16 1], [8 x i16] [i16 1, i16 -5, i16 2, i16 63, i16 67, i16 5, i16 -6, i16 1], [8 x i16] [i16 1, i16 -5, i16 2, i16 62, i16 68, i16 6, i16 -6, i16 0], [8 x i16] [i16 1, i16 -5, i16 1, i16 62, i16 68, i16 6, i16 -6, i16 1], [8 x i16] [i16 1, i16 -5, i16 1, i16 61, i16 69, i16 7, i16 -7, i16 1], [8 x i16] [i16 1, i16 -5, i16 0, i16 60, i16 70, i16 8, i16 -7, i16 1], [8 x i16] [i16 1, i16 -5, i16 0, i16 59, i16 71, i16 9, i16 -7, i16 0], [8 x i16] [i16 1, i16 -5, i16 -1, i16 58, i16 71, i16 10, i16 -7, i16 1], [8 x i16] [i16 1, i16 -4, i16 -1, i16 57, i16 72, i16 10, i16 -7, i16 0], [8 x i16] [i16 1, i16 -4, i16 -2, i16 56, i16 73, i16 11, i16 -7, i16 0], [8 x i16] [i16 1, i16 -4, i16 -2, i16 55, i16 73, i16 12, i16 -7, i16 0], [8 x i16] [i16 1, i16 -4, i16 -3, i16 54, i16 74, i16 13, i16 -7, i16 0], [8 x i16] [i16 1, i16 -4, i16 -3, i16 53, i16 74, i16 14, i16 -7, i16 0], [8 x i16] [i16 1, i16 -4, i16 -3, i16 52, i16 75, i16 15, i16 -8, i16 0], [8 x i16] [i16 1, i16 -3, i16 -4, i16 51, i16 76, i16 15, i16 -8, i16 0], [8 x i16] [i16 1, i16 -3, i16 -4, i16 50, i16 76, i16 16, i16 -8, i16 0], [8 x i16] [i16 1, i16 -3, i16 -5, i16 49, i16 77, i16 17, i16 -8, i16 0], [8 x i16] [i16 1, i16 -3, i16 -5, i16 48, i16 77, i16 18, i16 -8, i16 0], [8 x i16] [i16 1, i16 -3, i16 -5, i16 47, i16 77, i16 19, i16 -8, i16 0], [8 x i16] [i16 1, i16 -3, i16 -5, i16 45, i16 78, i16 20, i16 -8, i16 0], [8 x i16] [i16 1, i16 -2, i16 -6, i16 44, i16 78, i16 21, i16 -8, i16 0], [8 x i16] [i16 1, i16 -2, i16 -6, i16 43, i16 78, i16 22, i16 -8, i16 0], [8 x i16] [i16 1, i16 -2, i16 -6, i16 42, i16 78, i16 23, i16 -8, i16 0], [8 x i16] [i16 1, i16 -2, i16 -7, i16 41, i16 79, i16 24, i16 -8, i16 0], [8 x i16] [i16 1, i16 -2, i16 -7, i16 40, i16 79, i16 25, i16 -8, i16 0], [8 x i16] [i16 1, i16 -2, i16 -7, i16 39, i16 79, i16 26, i16 -8, i16 0], [8 x i16] [i16 1, i16 -2, i16 -7, i16 38, i16 80, i16 27, i16 -8, i16 -1], [8 x i16] [i16 1, i16 -2, i16 -7, i16 37, i16 80, i16 28, i16 -8, i16 -1], [8 x i16] [i16 1, i16 -2, i16 -7, i16 36, i16 80, i16 29, i16 -8, i16 -1], [8 x i16] [i16 1, i16 -1, i16 -8, i16 35, i16 80, i16 30, i16 -8, i16 -1], [8 x i16] [i16 1, i16 -1, i16 -8, i16 34, i16 80, i16 31, i16 -8, i16 -1]], align 16
@filteredinterp_filters500 = internal constant [64 x [8 x i16]] [[8 x i16] [i16 -3, i16 0, i16 35, i16 64, i16 35, i16 0, i16 -3, i16 0], [8 x i16] [i16 -3, i16 0, i16 34, i16 64, i16 36, i16 0, i16 -3, i16 0], [8 x i16] [i16 -3, i16 -1, i16 34, i16 64, i16 36, i16 1, i16 -3, i16 0], [8 x i16] [i16 -3, i16 -1, i16 33, i16 64, i16 37, i16 1, i16 -3, i16 0], [8 x i16] [i16 -3, i16 -1, i16 32, i16 64, i16 38, i16 1, i16 -3, i16 0], [8 x i16] [i16 -3, i16 -1, i16 31, i16 64, i16 39, i16 1, i16 -3, i16 0], [8 x i16] [i16 -3, i16 -1, i16 31, i16 63, i16 39, i16 2, i16 -3, i16 0], [8 x i16] [i16 -2, i16 -2, i16 30, i16 63, i16 40, i16 2, i16 -3, i16 0], [8 x i16] [i16 -2, i16 -2, i16 29, i16 63, i16 41, i16 2, i16 -3, i16 0], [8 x i16] [i16 -2, i16 -2, i16 29, i16 63, i16 41, i16 3, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -2, i16 28, i16 63, i16 42, i16 3, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -2, i16 27, i16 63, i16 43, i16 3, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -3, i16 27, i16 63, i16 43, i16 4, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -3, i16 26, i16 62, i16 44, i16 5, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -3, i16 25, i16 62, i16 45, i16 5, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -3, i16 25, i16 62, i16 45, i16 5, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -3, i16 24, i16 62, i16 46, i16 5, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -3, i16 23, i16 61, i16 47, i16 6, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -3, i16 23, i16 61, i16 47, i16 6, i16 -4, i16 0], [8 x i16] [i16 -2, i16 -3, i16 22, i16 61, i16 48, i16 7, i16 -4, i16 -1], [8 x i16] [i16 -2, i16 -3, i16 21, i16 60, i16 49, i16 7, i16 -4, i16 0], [8 x i16] [i16 -1, i16 -4, i16 20, i16 60, i16 49, i16 8, i16 -4, i16 0], [8 x i16] [i16 -1, i16 -4, i16 20, i16 60, i16 50, i16 8, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 19, i16 59, i16 51, i16 9, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 19, i16 59, i16 51, i16 9, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 18, i16 58, i16 52, i16 10, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 17, i16 58, i16 52, i16 11, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 16, i16 58, i16 53, i16 11, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 16, i16 57, i16 53, i16 12, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 15, i16 57, i16 54, i16 12, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 15, i16 56, i16 54, i16 13, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 14, i16 56, i16 55, i16 13, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 14, i16 55, i16 55, i16 14, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 13, i16 55, i16 56, i16 14, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 13, i16 54, i16 56, i16 15, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 12, i16 54, i16 57, i16 15, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 12, i16 53, i16 57, i16 16, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 11, i16 53, i16 58, i16 16, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 11, i16 52, i16 58, i16 17, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 10, i16 52, i16 58, i16 18, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 9, i16 51, i16 59, i16 19, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 9, i16 51, i16 59, i16 19, i16 -4, i16 -1], [8 x i16] [i16 -1, i16 -4, i16 8, i16 50, i16 60, i16 20, i16 -4, i16 -1], [8 x i16] [i16 0, i16 -4, i16 8, i16 49, i16 60, i16 20, i16 -4, i16 -1], [8 x i16] [i16 0, i16 -4, i16 7, i16 49, i16 60, i16 21, i16 -3, i16 -2], [8 x i16] [i16 -1, i16 -4, i16 7, i16 48, i16 61, i16 22, i16 -3, i16 -2], [8 x i16] [i16 0, i16 -4, i16 6, i16 47, i16 61, i16 23, i16 -3, i16 -2], [8 x i16] [i16 0, i16 -4, i16 6, i16 47, i16 61, i16 23, i16 -3, i16 -2], [8 x i16] [i16 0, i16 -4, i16 5, i16 46, i16 62, i16 24, i16 -3, i16 -2], [8 x i16] [i16 0, i16 -4, i16 5, i16 45, i16 62, i16 25, i16 -3, i16 -2], [8 x i16] [i16 0, i16 -4, i16 5, i16 45, i16 62, i16 25, i16 -3, i16 -2], [8 x i16] [i16 0, i16 -4, i16 5, i16 44, i16 62, i16 26, i16 -3, i16 -2], [8 x i16] [i16 0, i16 -4, i16 4, i16 43, i16 63, i16 27, i16 -3, i16 -2], [8 x i16] [i16 0, i16 -4, i16 3, i16 43, i16 63, i16 27, i16 -2, i16 -2], [8 x i16] [i16 0, i16 -4, i16 3, i16 42, i16 63, i16 28, i16 -2, i16 -2], [8 x i16] [i16 0, i16 -4, i16 3, i16 41, i16 63, i16 29, i16 -2, i16 -2], [8 x i16] [i16 0, i16 -3, i16 2, i16 41, i16 63, i16 29, i16 -2, i16 -2], [8 x i16] [i16 0, i16 -3, i16 2, i16 40, i16 63, i16 30, i16 -2, i16 -2], [8 x i16] [i16 0, i16 -3, i16 2, i16 39, i16 63, i16 31, i16 -1, i16 -3], [8 x i16] [i16 0, i16 -3, i16 1, i16 39, i16 64, i16 31, i16 -1, i16 -3], [8 x i16] [i16 0, i16 -3, i16 1, i16 38, i16 64, i16 32, i16 -1, i16 -3], [8 x i16] [i16 0, i16 -3, i16 1, i16 37, i16 64, i16 33, i16 -1, i16 -3], [8 x i16] [i16 0, i16 -3, i16 1, i16 36, i16 64, i16 34, i16 -1, i16 -3], [8 x i16] [i16 0, i16 -3, i16 0, i16 36, i16 64, i16 34, i16 0, i16 -3]], align 16
@highbd_down2_symodd.filter = internal global i16* getelementptr inbounds ([4 x i16], [4 x i16]* @av1_down2_symodd_half_filter, i32 0, i32 0), align 4
@highbd_down2_symeven.filter = internal global i16* getelementptr inbounds ([4 x i16], [4 x i16]* @av1_down2_symeven_half_filter, i32 0, i32 0), align 4

; Function Attrs: nounwind
define hidden i32 @av1_get_upscale_convolve_step(i32 %in_length, i32 %out_length) #0 {
entry:
  %in_length.addr = alloca i32, align 4
  %out_length.addr = alloca i32, align 4
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  %0 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %shl = shl i32 %0, 14
  %1 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div = sdiv i32 %1, 2
  %add = add nsw i32 %shl, %div
  %2 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div1 = sdiv i32 %add, %2
  ret i32 %div1
}

; Function Attrs: nounwind
define hidden void @av1_resize_plane(i8* %input, i32 %height, i32 %width, i32 %in_stride, i8* %output, i32 %height2, i32 %width2, i32 %out_stride) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %in_stride.addr = alloca i32, align 4
  %output.addr = alloca i8*, align 4
  %height2.addr = alloca i32, align 4
  %width2.addr = alloca i32, align 4
  %out_stride.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %intbuf = alloca i8*, align 4
  %tmpbuf = alloca i8*, align 4
  %arrbuf = alloca i8*, align 4
  %arrbuf2 = alloca i8*, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i32 %in_stride, i32* %in_stride.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !6
  store i32 %height2, i32* %height2.addr, align 4, !tbaa !2
  store i32 %width2, i32* %width2.addr, align 4, !tbaa !2
  store i32 %out_stride, i32* %out_stride.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i8** %intbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %mul = mul i32 1, %2
  %3 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul1 = mul i32 %mul, %3
  %call = call i8* @aom_malloc(i32 %mul1)
  store i8* %call, i8** %intbuf, align 4, !tbaa !6
  %4 = bitcast i8** %tmpbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %width.addr, align 4, !tbaa !2
  %6 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp = icmp sgt i32 %5, %6
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %7 = load i32, i32* %width.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load i32, i32* %height.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %7, %cond.true ], [ %8, %cond.false ]
  %mul2 = mul i32 1, %cond
  %call3 = call i8* @aom_malloc(i32 %mul2)
  store i8* %call3, i8** %tmpbuf, align 4, !tbaa !6
  %9 = bitcast i8** %arrbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul4 = mul i32 1, %10
  %call5 = call i8* @aom_malloc(i32 %mul4)
  store i8* %call5, i8** %arrbuf, align 4, !tbaa !6
  %11 = bitcast i8** %arrbuf2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %mul6 = mul i32 1, %12
  %call7 = call i8* @aom_malloc(i32 %mul6)
  store i8* %call7, i8** %arrbuf2, align 4, !tbaa !6
  %13 = load i8*, i8** %intbuf, align 4, !tbaa !6
  %cmp8 = icmp eq i8* %13, null
  br i1 %cmp8, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end
  %14 = load i8*, i8** %tmpbuf, align 4, !tbaa !6
  %cmp9 = icmp eq i8* %14, null
  br i1 %cmp9, label %if.then, label %lor.lhs.false10

lor.lhs.false10:                                  ; preds = %lor.lhs.false
  %15 = load i8*, i8** %arrbuf, align 4, !tbaa !6
  %cmp11 = icmp eq i8* %15, null
  br i1 %cmp11, label %if.then, label %lor.lhs.false12

lor.lhs.false12:                                  ; preds = %lor.lhs.false10
  %16 = load i8*, i8** %arrbuf2, align 4, !tbaa !6
  %cmp13 = icmp eq i8* %16, null
  br i1 %cmp13, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false12, %lor.lhs.false10, %lor.lhs.false, %cond.end
  br label %Error

if.end:                                           ; preds = %lor.lhs.false12
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %17 = load i32, i32* %i, align 4, !tbaa !2
  %18 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp14 = icmp slt i32 %17, %18
  br i1 %cmp14, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %20 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !2
  %mul15 = mul nsw i32 %20, %21
  %add.ptr = getelementptr inbounds i8, i8* %19, i32 %mul15
  %22 = load i32, i32* %width.addr, align 4, !tbaa !2
  %23 = load i8*, i8** %intbuf, align 4, !tbaa !6
  %24 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i, align 4, !tbaa !2
  %mul16 = mul nsw i32 %24, %25
  %add.ptr17 = getelementptr inbounds i8, i8* %23, i32 %mul16
  %26 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %27 = load i8*, i8** %tmpbuf, align 4, !tbaa !6
  call void @resize_multistep(i8* %add.ptr, i32 %22, i8* %add.ptr17, i32 %26, i8* %27)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %28 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc23, %for.end
  %29 = load i32, i32* %i, align 4, !tbaa !2
  %30 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %cmp19 = icmp slt i32 %29, %30
  br i1 %cmp19, label %for.body20, label %for.end25

for.body20:                                       ; preds = %for.cond18
  %31 = load i8*, i8** %intbuf, align 4, !tbaa !6
  %32 = load i32, i32* %i, align 4, !tbaa !2
  %add.ptr21 = getelementptr inbounds i8, i8* %31, i32 %32
  %33 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %34 = load i32, i32* %height.addr, align 4, !tbaa !2
  %35 = load i8*, i8** %arrbuf, align 4, !tbaa !6
  call void @fill_col_to_arr(i8* %add.ptr21, i32 %33, i32 %34, i8* %35)
  %36 = load i8*, i8** %arrbuf, align 4, !tbaa !6
  %37 = load i32, i32* %height.addr, align 4, !tbaa !2
  %38 = load i8*, i8** %arrbuf2, align 4, !tbaa !6
  %39 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %40 = load i8*, i8** %tmpbuf, align 4, !tbaa !6
  call void @resize_multistep(i8* %36, i32 %37, i8* %38, i32 %39, i8* %40)
  %41 = load i8*, i8** %output.addr, align 4, !tbaa !6
  %42 = load i32, i32* %i, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i8, i8* %41, i32 %42
  %43 = load i32, i32* %out_stride.addr, align 4, !tbaa !2
  %44 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %45 = load i8*, i8** %arrbuf2, align 4, !tbaa !6
  call void @fill_arr_to_col(i8* %add.ptr22, i32 %43, i32 %44, i8* %45)
  br label %for.inc23

for.inc23:                                        ; preds = %for.body20
  %46 = load i32, i32* %i, align 4, !tbaa !2
  %inc24 = add nsw i32 %46, 1
  store i32 %inc24, i32* %i, align 4, !tbaa !2
  br label %for.cond18

for.end25:                                        ; preds = %for.cond18
  br label %Error

Error:                                            ; preds = %for.end25, %if.then
  %47 = load i8*, i8** %intbuf, align 4, !tbaa !6
  call void @aom_free(i8* %47)
  %48 = load i8*, i8** %tmpbuf, align 4, !tbaa !6
  call void @aom_free(i8* %48)
  %49 = load i8*, i8** %arrbuf, align 4, !tbaa !6
  call void @aom_free(i8* %49)
  %50 = load i8*, i8** %arrbuf2, align 4, !tbaa !6
  call void @aom_free(i8* %50)
  %51 = bitcast i8** %arrbuf2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #5
  %52 = bitcast i8** %arrbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #5
  %53 = bitcast i8** %tmpbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #5
  %54 = bitcast i8** %intbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #5
  %55 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @aom_malloc(i32) #2

; Function Attrs: nounwind
define internal void @resize_multistep(i8* %input, i32 %length, i8* %output, i32 %olength, i8* %otmp) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %length.addr = alloca i32, align 4
  %output.addr = alloca i8*, align 4
  %olength.addr = alloca i32, align 4
  %otmp.addr = alloca i8*, align 4
  %steps = alloca i32, align 4
  %out = alloca i8*, align 4
  %filteredlength = alloca i32, align 4
  %otmp2 = alloca i8*, align 4
  %s = alloca i32, align 4
  %proj_filteredlength = alloca i32, align 4
  %in = alloca i8*, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !6
  store i32 %length, i32* %length.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !6
  store i32 %olength, i32* %olength.addr, align 4, !tbaa !2
  store i8* %otmp, i8** %otmp.addr, align 4, !tbaa !6
  %0 = load i32, i32* %length.addr, align 4, !tbaa !2
  %1 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %cmp = icmp eq i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i8*, i8** %output.addr, align 4, !tbaa !6
  %3 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %4 = load i32, i32* %length.addr, align 4, !tbaa !2
  %mul = mul i32 1, %4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %3, i32 %mul, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %5 = bitcast i32* %steps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %length.addr, align 4, !tbaa !2
  %7 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %call = call i32 @get_down2_steps(i32 %6, i32 %7)
  store i32 %call, i32* %steps, align 4, !tbaa !2
  %8 = load i32, i32* %steps, align 4, !tbaa !2
  %cmp1 = icmp sgt i32 %8, 0
  br i1 %cmp1, label %if.then2, label %if.else23

if.then2:                                         ; preds = %if.end
  %9 = bitcast i8** %out to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  store i8* null, i8** %out, align 4, !tbaa !6
  %10 = bitcast i32* %filteredlength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load i32, i32* %length.addr, align 4, !tbaa !2
  store i32 %11, i32* %filteredlength, align 4, !tbaa !2
  %12 = bitcast i8** %otmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load i8*, i8** %otmp.addr, align 4, !tbaa !6
  %14 = load i32, i32* %length.addr, align 4, !tbaa !2
  %call3 = call i32 @get_down2_length(i32 %14, i32 1)
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %call3
  store i8* %add.ptr, i8** %otmp2, align 4, !tbaa !6
  %15 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  store i32 0, i32* %s, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %16 = load i32, i32* %s, align 4, !tbaa !2
  %17 = load i32, i32* %steps, align 4, !tbaa !2
  %cmp4 = icmp slt i32 %16, %17
  br i1 %cmp4, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %18 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %19 = bitcast i32* %proj_filteredlength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %call5 = call i32 @get_down2_length(i32 %20, i32 1)
  store i32 %call5, i32* %proj_filteredlength, align 4, !tbaa !2
  %21 = bitcast i8** %in to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load i32, i32* %s, align 4, !tbaa !2
  %cmp6 = icmp eq i32 %22, 0
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %23 = load i8*, i8** %input.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %24 = load i8*, i8** %out, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %23, %cond.true ], [ %24, %cond.false ]
  store i8* %cond, i8** %in, align 4, !tbaa !6
  %25 = load i32, i32* %s, align 4, !tbaa !2
  %26 = load i32, i32* %steps, align 4, !tbaa !2
  %sub = sub nsw i32 %26, 1
  %cmp7 = icmp eq i32 %25, %sub
  br i1 %cmp7, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %cond.end
  %27 = load i32, i32* %proj_filteredlength, align 4, !tbaa !2
  %28 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %cmp8 = icmp eq i32 %27, %28
  br i1 %cmp8, label %if.then9, label %if.else

if.then9:                                         ; preds = %land.lhs.true
  %29 = load i8*, i8** %output.addr, align 4, !tbaa !6
  store i8* %29, i8** %out, align 4, !tbaa !6
  br label %if.end14

if.else:                                          ; preds = %land.lhs.true, %cond.end
  %30 = load i32, i32* %s, align 4, !tbaa !2
  %and = and i32 %30, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %if.else
  %31 = load i8*, i8** %otmp2, align 4, !tbaa !6
  br label %cond.end12

cond.false11:                                     ; preds = %if.else
  %32 = load i8*, i8** %otmp.addr, align 4, !tbaa !6
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false11, %cond.true10
  %cond13 = phi i8* [ %31, %cond.true10 ], [ %32, %cond.false11 ]
  store i8* %cond13, i8** %out, align 4, !tbaa !6
  br label %if.end14

if.end14:                                         ; preds = %cond.end12, %if.then9
  %33 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %and15 = and i32 %33, 1
  %tobool16 = icmp ne i32 %and15, 0
  br i1 %tobool16, label %if.then17, label %if.else18

if.then17:                                        ; preds = %if.end14
  %34 = load i8*, i8** %in, align 4, !tbaa !6
  %35 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %36 = load i8*, i8** %out, align 4, !tbaa !6
  call void @down2_symodd(i8* %34, i32 %35, i8* %36)
  br label %if.end19

if.else18:                                        ; preds = %if.end14
  %37 = load i8*, i8** %in, align 4, !tbaa !6
  %38 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %39 = load i8*, i8** %out, align 4, !tbaa !6
  call void @down2_symeven(i8* %37, i32 %38, i8* %39)
  br label %if.end19

if.end19:                                         ; preds = %if.else18, %if.then17
  %40 = load i32, i32* %proj_filteredlength, align 4, !tbaa !2
  store i32 %40, i32* %filteredlength, align 4, !tbaa !2
  %41 = bitcast i8** %in to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #5
  %42 = bitcast i32* %proj_filteredlength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end19
  %43 = load i32, i32* %s, align 4, !tbaa !2
  %inc = add nsw i32 %43, 1
  store i32 %inc, i32* %s, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %44 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %45 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %cmp20 = icmp ne i32 %44, %45
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %for.end
  %46 = load i8*, i8** %out, align 4, !tbaa !6
  %47 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %48 = load i8*, i8** %output.addr, align 4, !tbaa !6
  %49 = load i32, i32* %olength.addr, align 4, !tbaa !2
  call void @interpolate(i8* %46, i32 %47, i8* %48, i32 %49)
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %for.end
  %50 = bitcast i8** %otmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  %51 = bitcast i32* %filteredlength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #5
  %52 = bitcast i8** %out to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #5
  br label %if.end24

if.else23:                                        ; preds = %if.end
  %53 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %54 = load i32, i32* %length.addr, align 4, !tbaa !2
  %55 = load i8*, i8** %output.addr, align 4, !tbaa !6
  %56 = load i32, i32* %olength.addr, align 4, !tbaa !2
  call void @interpolate(i8* %53, i32 %54, i8* %55, i32 %56)
  br label %if.end24

if.end24:                                         ; preds = %if.else23, %if.end22
  %57 = bitcast i32* %steps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #5
  br label %return

return:                                           ; preds = %if.end24, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @fill_col_to_arr(i8* %img, i32 %stride, i32 %len, i8* %arr) #0 {
entry:
  %img.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %len.addr = alloca i32, align 4
  %arr.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %iptr = alloca i8*, align 4
  %aptr = alloca i8*, align 4
  store i8* %img, i8** %img.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !2
  store i8* %arr, i8** %arr.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i8** %iptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i8*, i8** %img.addr, align 4, !tbaa !6
  store i8* %2, i8** %iptr, align 4, !tbaa !6
  %3 = bitcast i8** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i8*, i8** %arr.addr, align 4, !tbaa !6
  store i8* %4, i8** %aptr, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !2
  %6 = load i32, i32* %len.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i8*, i8** %iptr, align 4, !tbaa !6
  %8 = load i8, i8* %7, align 1, !tbaa !8
  %9 = load i8*, i8** %aptr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i8, i8* %9, i32 1
  store i8* %incdec.ptr, i8** %aptr, align 4, !tbaa !6
  store i8 %8, i8* %9, align 1, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %iptr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %12, i32 %11
  store i8* %add.ptr, i8** %iptr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i8** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = bitcast i8** %iptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  ret void
}

; Function Attrs: nounwind
define internal void @fill_arr_to_col(i8* %img, i32 %stride, i32 %len, i8* %arr) #0 {
entry:
  %img.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %len.addr = alloca i32, align 4
  %arr.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %iptr = alloca i8*, align 4
  %aptr = alloca i8*, align 4
  store i8* %img, i8** %img.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !2
  store i8* %arr, i8** %arr.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i8** %iptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i8*, i8** %img.addr, align 4, !tbaa !6
  store i8* %2, i8** %iptr, align 4, !tbaa !6
  %3 = bitcast i8** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i8*, i8** %arr.addr, align 4, !tbaa !6
  store i8* %4, i8** %aptr, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !2
  %6 = load i32, i32* %len.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i8*, i8** %aptr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr, i8** %aptr, align 4, !tbaa !6
  %8 = load i8, i8* %7, align 1, !tbaa !8
  %9 = load i8*, i8** %iptr, align 4, !tbaa !6
  store i8 %8, i8* %9, align 1, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %iptr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %12, i32 %11
  store i8* %add.ptr, i8** %iptr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i8** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = bitcast i8** %iptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  ret void
}

declare void @aom_free(i8*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_upscale_plane_double_prec(double* %input, i32 %height, i32 %width, i32 %in_stride, double* %output, i32 %height2, i32 %width2, i32 %out_stride) #0 {
entry:
  %input.addr = alloca double*, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %in_stride.addr = alloca i32, align 4
  %output.addr = alloca double*, align 4
  %height2.addr = alloca i32, align 4
  %width2.addr = alloca i32, align 4
  %out_stride.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %intbuf = alloca double*, align 4
  %arrbuf = alloca double*, align 4
  %arrbuf2 = alloca double*, align 4
  store double* %input, double** %input.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i32 %in_stride, i32* %in_stride.addr, align 4, !tbaa !2
  store double* %output, double** %output.addr, align 4, !tbaa !6
  store i32 %height2, i32* %height2.addr, align 4, !tbaa !2
  store i32 %width2, i32* %width2.addr, align 4, !tbaa !2
  store i32 %out_stride, i32* %out_stride.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast double** %intbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %mul = mul i32 8, %2
  %3 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul1 = mul i32 %mul, %3
  %call = call i8* @aom_malloc(i32 %mul1)
  %4 = bitcast i8* %call to double*
  store double* %4, double** %intbuf, align 4, !tbaa !6
  %5 = bitcast double** %arrbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul2 = mul i32 8, %6
  %call3 = call i8* @aom_malloc(i32 %mul2)
  %7 = bitcast i8* %call3 to double*
  store double* %7, double** %arrbuf, align 4, !tbaa !6
  %8 = bitcast double** %arrbuf2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %mul4 = mul i32 8, %9
  %call5 = call i8* @aom_malloc(i32 %mul4)
  %10 = bitcast i8* %call5 to double*
  store double* %10, double** %arrbuf2, align 4, !tbaa !6
  %11 = load double*, double** %intbuf, align 4, !tbaa !6
  %cmp = icmp eq double* %11, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %12 = load double*, double** %arrbuf, align 4, !tbaa !6
  %cmp6 = icmp eq double* %12, null
  br i1 %cmp6, label %if.then, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %lor.lhs.false
  %13 = load double*, double** %arrbuf2, align 4, !tbaa !6
  %cmp8 = icmp eq double* %13, null
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false7, %lor.lhs.false, %entry
  br label %Error

if.end:                                           ; preds = %lor.lhs.false7
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %14 = load i32, i32* %i, align 4, !tbaa !2
  %15 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp9 = icmp slt i32 %14, %15
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load double*, double** %input.addr, align 4, !tbaa !6
  %17 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !2
  %mul10 = mul nsw i32 %17, %18
  %add.ptr = getelementptr inbounds double, double* %16, i32 %mul10
  %19 = load i32, i32* %width.addr, align 4, !tbaa !2
  %20 = load double*, double** %intbuf, align 4, !tbaa !6
  %21 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %22 = load i32, i32* %i, align 4, !tbaa !2
  %mul11 = mul nsw i32 %21, %22
  %add.ptr12 = getelementptr inbounds double, double* %20, i32 %mul11
  %23 = load i32, i32* %width2.addr, align 4, !tbaa !2
  call void @upscale_multistep_double_prec(double* %add.ptr, i32 %19, double* %add.ptr12, i32 %23)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %24 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc18, %for.end
  %25 = load i32, i32* %i, align 4, !tbaa !2
  %26 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %cmp14 = icmp slt i32 %25, %26
  br i1 %cmp14, label %for.body15, label %for.end20

for.body15:                                       ; preds = %for.cond13
  %27 = load double*, double** %intbuf, align 4, !tbaa !6
  %28 = load i32, i32* %i, align 4, !tbaa !2
  %add.ptr16 = getelementptr inbounds double, double* %27, i32 %28
  %29 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %30 = load i32, i32* %height.addr, align 4, !tbaa !2
  %31 = load double*, double** %arrbuf, align 4, !tbaa !6
  call void @fill_col_to_arr_double_prec(double* %add.ptr16, i32 %29, i32 %30, double* %31)
  %32 = load double*, double** %arrbuf, align 4, !tbaa !6
  %33 = load i32, i32* %height.addr, align 4, !tbaa !2
  %34 = load double*, double** %arrbuf2, align 4, !tbaa !6
  %35 = load i32, i32* %height2.addr, align 4, !tbaa !2
  call void @upscale_multistep_double_prec(double* %32, i32 %33, double* %34, i32 %35)
  %36 = load double*, double** %output.addr, align 4, !tbaa !6
  %37 = load i32, i32* %i, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds double, double* %36, i32 %37
  %38 = load i32, i32* %out_stride.addr, align 4, !tbaa !2
  %39 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %40 = load double*, double** %arrbuf2, align 4, !tbaa !6
  call void @fill_arr_to_col_double_prec(double* %add.ptr17, i32 %38, i32 %39, double* %40)
  br label %for.inc18

for.inc18:                                        ; preds = %for.body15
  %41 = load i32, i32* %i, align 4, !tbaa !2
  %inc19 = add nsw i32 %41, 1
  store i32 %inc19, i32* %i, align 4, !tbaa !2
  br label %for.cond13

for.end20:                                        ; preds = %for.cond13
  br label %Error

Error:                                            ; preds = %for.end20, %if.then
  %42 = load double*, double** %intbuf, align 4, !tbaa !6
  %43 = bitcast double* %42 to i8*
  call void @aom_free(i8* %43)
  %44 = load double*, double** %arrbuf, align 4, !tbaa !6
  %45 = bitcast double* %44 to i8*
  call void @aom_free(i8* %45)
  %46 = load double*, double** %arrbuf2, align 4, !tbaa !6
  %47 = bitcast double* %46 to i8*
  call void @aom_free(i8* %47)
  %48 = bitcast double** %arrbuf2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  %49 = bitcast double** %arrbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #5
  %50 = bitcast double** %intbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  %51 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #5
  ret void
}

; Function Attrs: nounwind
define internal void @upscale_multistep_double_prec(double* %input, i32 %length, double* %output, i32 %olength) #0 {
entry:
  %input.addr = alloca double*, align 4
  %length.addr = alloca i32, align 4
  %output.addr = alloca double*, align 4
  %olength.addr = alloca i32, align 4
  store double* %input, double** %input.addr, align 4, !tbaa !6
  store i32 %length, i32* %length.addr, align 4, !tbaa !2
  store double* %output, double** %output.addr, align 4, !tbaa !6
  store i32 %olength, i32* %olength.addr, align 4, !tbaa !2
  %0 = load double*, double** %input.addr, align 4, !tbaa !6
  %1 = load i32, i32* %length.addr, align 4, !tbaa !2
  %2 = load double*, double** %output.addr, align 4, !tbaa !6
  %3 = load i32, i32* %olength.addr, align 4, !tbaa !2
  call void @interpolate_double_prec(double* %0, i32 %1, double* %2, i32 %3)
  ret void
}

; Function Attrs: nounwind
define internal void @fill_col_to_arr_double_prec(double* %img, i32 %stride, i32 %len, double* %arr) #0 {
entry:
  %img.addr = alloca double*, align 4
  %stride.addr = alloca i32, align 4
  %len.addr = alloca i32, align 4
  %arr.addr = alloca double*, align 4
  %i = alloca i32, align 4
  %iptr = alloca double*, align 4
  %aptr = alloca double*, align 4
  store double* %img, double** %img.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !2
  store double* %arr, double** %arr.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast double** %iptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load double*, double** %img.addr, align 4, !tbaa !6
  store double* %2, double** %iptr, align 4, !tbaa !6
  %3 = bitcast double** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load double*, double** %arr.addr, align 4, !tbaa !6
  store double* %4, double** %aptr, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !2
  %6 = load i32, i32* %len.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load double*, double** %iptr, align 4, !tbaa !6
  %8 = load double, double* %7, align 8, !tbaa !9
  %9 = load double*, double** %aptr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds double, double* %9, i32 1
  store double* %incdec.ptr, double** %aptr, align 4, !tbaa !6
  store double %8, double* %9, align 8, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !2
  %12 = load double*, double** %iptr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds double, double* %12, i32 %11
  store double* %add.ptr, double** %iptr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast double** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = bitcast double** %iptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  ret void
}

; Function Attrs: nounwind
define internal void @fill_arr_to_col_double_prec(double* %img, i32 %stride, i32 %len, double* %arr) #0 {
entry:
  %img.addr = alloca double*, align 4
  %stride.addr = alloca i32, align 4
  %len.addr = alloca i32, align 4
  %arr.addr = alloca double*, align 4
  %i = alloca i32, align 4
  %iptr = alloca double*, align 4
  %aptr = alloca double*, align 4
  store double* %img, double** %img.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !2
  store double* %arr, double** %arr.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast double** %iptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load double*, double** %img.addr, align 4, !tbaa !6
  store double* %2, double** %iptr, align 4, !tbaa !6
  %3 = bitcast double** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load double*, double** %arr.addr, align 4, !tbaa !6
  store double* %4, double** %aptr, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !2
  %6 = load i32, i32* %len.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load double*, double** %aptr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds double, double* %7, i32 1
  store double* %incdec.ptr, double** %aptr, align 4, !tbaa !6
  %8 = load double, double* %7, align 8, !tbaa !9
  %9 = load double*, double** %iptr, align 4, !tbaa !6
  store double %8, double* %9, align 8, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !2
  %12 = load double*, double** %iptr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds double, double* %12, i32 %11
  store double* %add.ptr, double** %iptr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast double** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = bitcast double** %iptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_resize_plane(i8* %input, i32 %height, i32 %width, i32 %in_stride, i8* %output, i32 %height2, i32 %width2, i32 %out_stride, i32 %bd) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %in_stride.addr = alloca i32, align 4
  %output.addr = alloca i8*, align 4
  %height2.addr = alloca i32, align 4
  %width2.addr = alloca i32, align 4
  %out_stride.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %intbuf = alloca i16*, align 4
  %tmpbuf = alloca i16*, align 4
  %arrbuf = alloca i16*, align 4
  %arrbuf2 = alloca i16*, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i32 %in_stride, i32* %in_stride.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !6
  store i32 %height2, i32* %height2.addr, align 4, !tbaa !2
  store i32 %width2, i32* %width2.addr, align 4, !tbaa !2
  store i32 %out_stride, i32* %out_stride.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i16** %intbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %mul = mul i32 2, %2
  %3 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul1 = mul i32 %mul, %3
  %call = call i8* @aom_malloc(i32 %mul1)
  %4 = bitcast i8* %call to i16*
  store i16* %4, i16** %intbuf, align 4, !tbaa !6
  %5 = bitcast i16** %tmpbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %width.addr, align 4, !tbaa !2
  %7 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp = icmp sgt i32 %6, %7
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load i32, i32* %width.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %9 = load i32, i32* %height.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %8, %cond.true ], [ %9, %cond.false ]
  %mul2 = mul i32 2, %cond
  %call3 = call i8* @aom_malloc(i32 %mul2)
  %10 = bitcast i8* %call3 to i16*
  store i16* %10, i16** %tmpbuf, align 4, !tbaa !6
  %11 = bitcast i16** %arrbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul4 = mul i32 2, %12
  %call5 = call i8* @aom_malloc(i32 %mul4)
  %13 = bitcast i8* %call5 to i16*
  store i16* %13, i16** %arrbuf, align 4, !tbaa !6
  %14 = bitcast i16** %arrbuf2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %mul6 = mul i32 2, %15
  %call7 = call i8* @aom_malloc(i32 %mul6)
  %16 = bitcast i8* %call7 to i16*
  store i16* %16, i16** %arrbuf2, align 4, !tbaa !6
  %17 = load i16*, i16** %intbuf, align 4, !tbaa !6
  %cmp8 = icmp eq i16* %17, null
  br i1 %cmp8, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end
  %18 = load i16*, i16** %tmpbuf, align 4, !tbaa !6
  %cmp9 = icmp eq i16* %18, null
  br i1 %cmp9, label %if.then, label %lor.lhs.false10

lor.lhs.false10:                                  ; preds = %lor.lhs.false
  %19 = load i16*, i16** %arrbuf, align 4, !tbaa !6
  %cmp11 = icmp eq i16* %19, null
  br i1 %cmp11, label %if.then, label %lor.lhs.false12

lor.lhs.false12:                                  ; preds = %lor.lhs.false10
  %20 = load i16*, i16** %arrbuf2, align 4, !tbaa !6
  %cmp13 = icmp eq i16* %20, null
  br i1 %cmp13, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false12, %lor.lhs.false10, %lor.lhs.false, %cond.end
  br label %Error

if.end:                                           ; preds = %lor.lhs.false12
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %21 = load i32, i32* %i, align 4, !tbaa !2
  %22 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp14 = icmp slt i32 %21, %22
  br i1 %cmp14, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %24 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i, align 4, !tbaa !2
  %mul15 = mul nsw i32 %24, %25
  %add.ptr = getelementptr inbounds i8, i8* %23, i32 %mul15
  %26 = ptrtoint i8* %add.ptr to i32
  %shl = shl i32 %26, 1
  %27 = inttoptr i32 %shl to i16*
  %28 = load i32, i32* %width.addr, align 4, !tbaa !2
  %29 = load i16*, i16** %intbuf, align 4, !tbaa !6
  %30 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !2
  %mul16 = mul nsw i32 %30, %31
  %add.ptr17 = getelementptr inbounds i16, i16* %29, i32 %mul16
  %32 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %33 = load i16*, i16** %tmpbuf, align 4, !tbaa !6
  %34 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @highbd_resize_multistep(i16* %27, i32 %28, i16* %add.ptr17, i32 %32, i16* %33, i32 %34)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %35 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc24, %for.end
  %36 = load i32, i32* %i, align 4, !tbaa !2
  %37 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %cmp19 = icmp slt i32 %36, %37
  br i1 %cmp19, label %for.body20, label %for.end26

for.body20:                                       ; preds = %for.cond18
  %38 = load i16*, i16** %intbuf, align 4, !tbaa !6
  %39 = load i32, i32* %i, align 4, !tbaa !2
  %add.ptr21 = getelementptr inbounds i16, i16* %38, i32 %39
  %40 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %41 = load i32, i32* %height.addr, align 4, !tbaa !2
  %42 = load i16*, i16** %arrbuf, align 4, !tbaa !6
  call void @highbd_fill_col_to_arr(i16* %add.ptr21, i32 %40, i32 %41, i16* %42)
  %43 = load i16*, i16** %arrbuf, align 4, !tbaa !6
  %44 = load i32, i32* %height.addr, align 4, !tbaa !2
  %45 = load i16*, i16** %arrbuf2, align 4, !tbaa !6
  %46 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %47 = load i16*, i16** %tmpbuf, align 4, !tbaa !6
  %48 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @highbd_resize_multistep(i16* %43, i32 %44, i16* %45, i32 %46, i16* %47, i32 %48)
  %49 = load i8*, i8** %output.addr, align 4, !tbaa !6
  %50 = load i32, i32* %i, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i8, i8* %49, i32 %50
  %51 = ptrtoint i8* %add.ptr22 to i32
  %shl23 = shl i32 %51, 1
  %52 = inttoptr i32 %shl23 to i16*
  %53 = load i32, i32* %out_stride.addr, align 4, !tbaa !2
  %54 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %55 = load i16*, i16** %arrbuf2, align 4, !tbaa !6
  call void @highbd_fill_arr_to_col(i16* %52, i32 %53, i32 %54, i16* %55)
  br label %for.inc24

for.inc24:                                        ; preds = %for.body20
  %56 = load i32, i32* %i, align 4, !tbaa !2
  %inc25 = add nsw i32 %56, 1
  store i32 %inc25, i32* %i, align 4, !tbaa !2
  br label %for.cond18

for.end26:                                        ; preds = %for.cond18
  br label %Error

Error:                                            ; preds = %for.end26, %if.then
  %57 = load i16*, i16** %intbuf, align 4, !tbaa !6
  %58 = bitcast i16* %57 to i8*
  call void @aom_free(i8* %58)
  %59 = load i16*, i16** %tmpbuf, align 4, !tbaa !6
  %60 = bitcast i16* %59 to i8*
  call void @aom_free(i8* %60)
  %61 = load i16*, i16** %arrbuf, align 4, !tbaa !6
  %62 = bitcast i16* %61 to i8*
  call void @aom_free(i8* %62)
  %63 = load i16*, i16** %arrbuf2, align 4, !tbaa !6
  %64 = bitcast i16* %63 to i8*
  call void @aom_free(i8* %64)
  %65 = bitcast i16** %arrbuf2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %66 = bitcast i16** %arrbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = bitcast i16** %tmpbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast i16** %intbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  %69 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #5
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_resize_multistep(i16* %input, i32 %length, i16* %output, i32 %olength, i16* %otmp, i32 %bd) #0 {
entry:
  %input.addr = alloca i16*, align 4
  %length.addr = alloca i32, align 4
  %output.addr = alloca i16*, align 4
  %olength.addr = alloca i32, align 4
  %otmp.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %steps = alloca i32, align 4
  %out = alloca i16*, align 4
  %filteredlength = alloca i32, align 4
  %otmp2 = alloca i16*, align 4
  %s = alloca i32, align 4
  %proj_filteredlength = alloca i32, align 4
  %in = alloca i16*, align 4
  store i16* %input, i16** %input.addr, align 4, !tbaa !6
  store i32 %length, i32* %length.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !6
  store i32 %olength, i32* %olength.addr, align 4, !tbaa !2
  store i16* %otmp, i16** %otmp.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = load i32, i32* %length.addr, align 4, !tbaa !2
  %1 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %cmp = icmp eq i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !6
  %3 = bitcast i16* %2 to i8*
  %4 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %5 = bitcast i16* %4 to i8*
  %6 = load i32, i32* %length.addr, align 4, !tbaa !2
  %mul = mul i32 2, %6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %3, i8* align 2 %5, i32 %mul, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %7 = bitcast i32* %steps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %length.addr, align 4, !tbaa !2
  %9 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %call = call i32 @get_down2_steps(i32 %8, i32 %9)
  store i32 %call, i32* %steps, align 4, !tbaa !2
  %10 = load i32, i32* %steps, align 4, !tbaa !2
  %cmp1 = icmp sgt i32 %10, 0
  br i1 %cmp1, label %if.then2, label %if.else23

if.then2:                                         ; preds = %if.end
  %11 = bitcast i16** %out to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  store i16* null, i16** %out, align 4, !tbaa !6
  %12 = bitcast i32* %filteredlength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load i32, i32* %length.addr, align 4, !tbaa !2
  store i32 %13, i32* %filteredlength, align 4, !tbaa !2
  %14 = bitcast i16** %otmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load i16*, i16** %otmp.addr, align 4, !tbaa !6
  %16 = load i32, i32* %length.addr, align 4, !tbaa !2
  %call3 = call i32 @get_down2_length(i32 %16, i32 1)
  %add.ptr = getelementptr inbounds i16, i16* %15, i32 %call3
  store i16* %add.ptr, i16** %otmp2, align 4, !tbaa !6
  %17 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  store i32 0, i32* %s, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %18 = load i32, i32* %s, align 4, !tbaa !2
  %19 = load i32, i32* %steps, align 4, !tbaa !2
  %cmp4 = icmp slt i32 %18, %19
  br i1 %cmp4, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %20 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %21 = bitcast i32* %proj_filteredlength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %call5 = call i32 @get_down2_length(i32 %22, i32 1)
  store i32 %call5, i32* %proj_filteredlength, align 4, !tbaa !2
  %23 = bitcast i16** %in to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  %24 = load i32, i32* %s, align 4, !tbaa !2
  %cmp6 = icmp eq i32 %24, 0
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %25 = load i16*, i16** %input.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %26 = load i16*, i16** %out, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i16* [ %25, %cond.true ], [ %26, %cond.false ]
  store i16* %cond, i16** %in, align 4, !tbaa !6
  %27 = load i32, i32* %s, align 4, !tbaa !2
  %28 = load i32, i32* %steps, align 4, !tbaa !2
  %sub = sub nsw i32 %28, 1
  %cmp7 = icmp eq i32 %27, %sub
  br i1 %cmp7, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %cond.end
  %29 = load i32, i32* %proj_filteredlength, align 4, !tbaa !2
  %30 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %cmp8 = icmp eq i32 %29, %30
  br i1 %cmp8, label %if.then9, label %if.else

if.then9:                                         ; preds = %land.lhs.true
  %31 = load i16*, i16** %output.addr, align 4, !tbaa !6
  store i16* %31, i16** %out, align 4, !tbaa !6
  br label %if.end14

if.else:                                          ; preds = %land.lhs.true, %cond.end
  %32 = load i32, i32* %s, align 4, !tbaa !2
  %and = and i32 %32, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %if.else
  %33 = load i16*, i16** %otmp2, align 4, !tbaa !6
  br label %cond.end12

cond.false11:                                     ; preds = %if.else
  %34 = load i16*, i16** %otmp.addr, align 4, !tbaa !6
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false11, %cond.true10
  %cond13 = phi i16* [ %33, %cond.true10 ], [ %34, %cond.false11 ]
  store i16* %cond13, i16** %out, align 4, !tbaa !6
  br label %if.end14

if.end14:                                         ; preds = %cond.end12, %if.then9
  %35 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %and15 = and i32 %35, 1
  %tobool16 = icmp ne i32 %and15, 0
  br i1 %tobool16, label %if.then17, label %if.else18

if.then17:                                        ; preds = %if.end14
  %36 = load i16*, i16** %in, align 4, !tbaa !6
  %37 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %38 = load i16*, i16** %out, align 4, !tbaa !6
  %39 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @highbd_down2_symodd(i16* %36, i32 %37, i16* %38, i32 %39)
  br label %if.end19

if.else18:                                        ; preds = %if.end14
  %40 = load i16*, i16** %in, align 4, !tbaa !6
  %41 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %42 = load i16*, i16** %out, align 4, !tbaa !6
  %43 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @highbd_down2_symeven(i16* %40, i32 %41, i16* %42, i32 %43)
  br label %if.end19

if.end19:                                         ; preds = %if.else18, %if.then17
  %44 = load i32, i32* %proj_filteredlength, align 4, !tbaa !2
  store i32 %44, i32* %filteredlength, align 4, !tbaa !2
  %45 = bitcast i16** %in to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #5
  %46 = bitcast i32* %proj_filteredlength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end19
  %47 = load i32, i32* %s, align 4, !tbaa !2
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %s, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %48 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %49 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %cmp20 = icmp ne i32 %48, %49
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %for.end
  %50 = load i16*, i16** %out, align 4, !tbaa !6
  %51 = load i32, i32* %filteredlength, align 4, !tbaa !2
  %52 = load i16*, i16** %output.addr, align 4, !tbaa !6
  %53 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %54 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @highbd_interpolate(i16* %50, i32 %51, i16* %52, i32 %53, i32 %54)
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %for.end
  %55 = bitcast i16** %otmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  %56 = bitcast i32* %filteredlength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #5
  %57 = bitcast i16** %out to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #5
  br label %if.end24

if.else23:                                        ; preds = %if.end
  %58 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %59 = load i32, i32* %length.addr, align 4, !tbaa !2
  %60 = load i16*, i16** %output.addr, align 4, !tbaa !6
  %61 = load i32, i32* %olength.addr, align 4, !tbaa !2
  %62 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @highbd_interpolate(i16* %58, i32 %59, i16* %60, i32 %61, i32 %62)
  br label %if.end24

if.end24:                                         ; preds = %if.else23, %if.end22
  %63 = bitcast i32* %steps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  br label %return

return:                                           ; preds = %if.end24, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_fill_col_to_arr(i16* %img, i32 %stride, i32 %len, i16* %arr) #0 {
entry:
  %img.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %len.addr = alloca i32, align 4
  %arr.addr = alloca i16*, align 4
  %i = alloca i32, align 4
  %iptr = alloca i16*, align 4
  %aptr = alloca i16*, align 4
  store i16* %img, i16** %img.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !2
  store i16* %arr, i16** %arr.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i16** %iptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i16*, i16** %img.addr, align 4, !tbaa !6
  store i16* %2, i16** %iptr, align 4, !tbaa !6
  %3 = bitcast i16** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i16*, i16** %arr.addr, align 4, !tbaa !6
  store i16* %4, i16** %aptr, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !2
  %6 = load i32, i32* %len.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i16*, i16** %iptr, align 4, !tbaa !6
  %8 = load i16, i16* %7, align 2, !tbaa !11
  %9 = load i16*, i16** %aptr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i16, i16* %9, i32 1
  store i16* %incdec.ptr, i16** %aptr, align 4, !tbaa !6
  store i16 %8, i16* %9, align 2, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !2
  %12 = load i16*, i16** %iptr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i16, i16* %12, i32 %11
  store i16* %add.ptr, i16** %iptr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i16** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = bitcast i16** %iptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_fill_arr_to_col(i16* %img, i32 %stride, i32 %len, i16* %arr) #0 {
entry:
  %img.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %len.addr = alloca i32, align 4
  %arr.addr = alloca i16*, align 4
  %i = alloca i32, align 4
  %iptr = alloca i16*, align 4
  %aptr = alloca i16*, align 4
  store i16* %img, i16** %img.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !2
  store i16* %arr, i16** %arr.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i16** %iptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i16*, i16** %img.addr, align 4, !tbaa !6
  store i16* %2, i16** %iptr, align 4, !tbaa !6
  %3 = bitcast i16** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i16*, i16** %arr.addr, align 4, !tbaa !6
  store i16* %4, i16** %aptr, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !2
  %6 = load i32, i32* %len.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i16*, i16** %aptr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i16, i16* %7, i32 1
  store i16* %incdec.ptr, i16** %aptr, align 4, !tbaa !6
  %8 = load i16, i16* %7, align 2, !tbaa !11
  %9 = load i16*, i16** %iptr, align 4, !tbaa !6
  store i16 %8, i16* %9, align 2, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !2
  %12 = load i16*, i16** %iptr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i16, i16* %12, i32 %11
  store i16* %add.ptr, i16** %iptr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i16** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = bitcast i16** %iptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_resize_frame420(i8* %y, i32 %y_stride, i8* %u, i8* %v, i32 %uv_stride, i32 %height, i32 %width, i8* %oy, i32 %oy_stride, i8* %ou, i8* %ov, i32 %ouv_stride, i32 %oheight, i32 %owidth) #0 {
entry:
  %y.addr = alloca i8*, align 4
  %y_stride.addr = alloca i32, align 4
  %u.addr = alloca i8*, align 4
  %v.addr = alloca i8*, align 4
  %uv_stride.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %oy.addr = alloca i8*, align 4
  %oy_stride.addr = alloca i32, align 4
  %ou.addr = alloca i8*, align 4
  %ov.addr = alloca i8*, align 4
  %ouv_stride.addr = alloca i32, align 4
  %oheight.addr = alloca i32, align 4
  %owidth.addr = alloca i32, align 4
  store i8* %y, i8** %y.addr, align 4, !tbaa !6
  store i32 %y_stride, i32* %y_stride.addr, align 4, !tbaa !2
  store i8* %u, i8** %u.addr, align 4, !tbaa !6
  store i8* %v, i8** %v.addr, align 4, !tbaa !6
  store i32 %uv_stride, i32* %uv_stride.addr, align 4, !tbaa !2
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i8* %oy, i8** %oy.addr, align 4, !tbaa !6
  store i32 %oy_stride, i32* %oy_stride.addr, align 4, !tbaa !2
  store i8* %ou, i8** %ou.addr, align 4, !tbaa !6
  store i8* %ov, i8** %ov.addr, align 4, !tbaa !6
  store i32 %ouv_stride, i32* %ouv_stride.addr, align 4, !tbaa !2
  store i32 %oheight, i32* %oheight.addr, align 4, !tbaa !2
  store i32 %owidth, i32* %owidth.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %y.addr, align 4, !tbaa !6
  %1 = load i32, i32* %height.addr, align 4, !tbaa !2
  %2 = load i32, i32* %width.addr, align 4, !tbaa !2
  %3 = load i32, i32* %y_stride.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %oy.addr, align 4, !tbaa !6
  %5 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %6 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %7 = load i32, i32* %oy_stride.addr, align 4, !tbaa !2
  call void @av1_resize_plane(i8* %0, i32 %1, i32 %2, i32 %3, i8* %4, i32 %5, i32 %6, i32 %7)
  %8 = load i8*, i8** %u.addr, align 4, !tbaa !6
  %9 = load i32, i32* %height.addr, align 4, !tbaa !2
  %div = sdiv i32 %9, 2
  %10 = load i32, i32* %width.addr, align 4, !tbaa !2
  %div1 = sdiv i32 %10, 2
  %11 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %ou.addr, align 4, !tbaa !6
  %13 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %div2 = sdiv i32 %13, 2
  %14 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %div3 = sdiv i32 %14, 2
  %15 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  call void @av1_resize_plane(i8* %8, i32 %div, i32 %div1, i32 %11, i8* %12, i32 %div2, i32 %div3, i32 %15)
  %16 = load i8*, i8** %v.addr, align 4, !tbaa !6
  %17 = load i32, i32* %height.addr, align 4, !tbaa !2
  %div4 = sdiv i32 %17, 2
  %18 = load i32, i32* %width.addr, align 4, !tbaa !2
  %div5 = sdiv i32 %18, 2
  %19 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %ov.addr, align 4, !tbaa !6
  %21 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %div6 = sdiv i32 %21, 2
  %22 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %div7 = sdiv i32 %22, 2
  %23 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  call void @av1_resize_plane(i8* %16, i32 %div4, i32 %div5, i32 %19, i8* %20, i32 %div6, i32 %div7, i32 %23)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_resize_frame422(i8* %y, i32 %y_stride, i8* %u, i8* %v, i32 %uv_stride, i32 %height, i32 %width, i8* %oy, i32 %oy_stride, i8* %ou, i8* %ov, i32 %ouv_stride, i32 %oheight, i32 %owidth) #0 {
entry:
  %y.addr = alloca i8*, align 4
  %y_stride.addr = alloca i32, align 4
  %u.addr = alloca i8*, align 4
  %v.addr = alloca i8*, align 4
  %uv_stride.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %oy.addr = alloca i8*, align 4
  %oy_stride.addr = alloca i32, align 4
  %ou.addr = alloca i8*, align 4
  %ov.addr = alloca i8*, align 4
  %ouv_stride.addr = alloca i32, align 4
  %oheight.addr = alloca i32, align 4
  %owidth.addr = alloca i32, align 4
  store i8* %y, i8** %y.addr, align 4, !tbaa !6
  store i32 %y_stride, i32* %y_stride.addr, align 4, !tbaa !2
  store i8* %u, i8** %u.addr, align 4, !tbaa !6
  store i8* %v, i8** %v.addr, align 4, !tbaa !6
  store i32 %uv_stride, i32* %uv_stride.addr, align 4, !tbaa !2
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i8* %oy, i8** %oy.addr, align 4, !tbaa !6
  store i32 %oy_stride, i32* %oy_stride.addr, align 4, !tbaa !2
  store i8* %ou, i8** %ou.addr, align 4, !tbaa !6
  store i8* %ov, i8** %ov.addr, align 4, !tbaa !6
  store i32 %ouv_stride, i32* %ouv_stride.addr, align 4, !tbaa !2
  store i32 %oheight, i32* %oheight.addr, align 4, !tbaa !2
  store i32 %owidth, i32* %owidth.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %y.addr, align 4, !tbaa !6
  %1 = load i32, i32* %height.addr, align 4, !tbaa !2
  %2 = load i32, i32* %width.addr, align 4, !tbaa !2
  %3 = load i32, i32* %y_stride.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %oy.addr, align 4, !tbaa !6
  %5 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %6 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %7 = load i32, i32* %oy_stride.addr, align 4, !tbaa !2
  call void @av1_resize_plane(i8* %0, i32 %1, i32 %2, i32 %3, i8* %4, i32 %5, i32 %6, i32 %7)
  %8 = load i8*, i8** %u.addr, align 4, !tbaa !6
  %9 = load i32, i32* %height.addr, align 4, !tbaa !2
  %10 = load i32, i32* %width.addr, align 4, !tbaa !2
  %div = sdiv i32 %10, 2
  %11 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %ou.addr, align 4, !tbaa !6
  %13 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %14 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %div1 = sdiv i32 %14, 2
  %15 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  call void @av1_resize_plane(i8* %8, i32 %9, i32 %div, i32 %11, i8* %12, i32 %13, i32 %div1, i32 %15)
  %16 = load i8*, i8** %v.addr, align 4, !tbaa !6
  %17 = load i32, i32* %height.addr, align 4, !tbaa !2
  %18 = load i32, i32* %width.addr, align 4, !tbaa !2
  %div2 = sdiv i32 %18, 2
  %19 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %ov.addr, align 4, !tbaa !6
  %21 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %22 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %div3 = sdiv i32 %22, 2
  %23 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  call void @av1_resize_plane(i8* %16, i32 %17, i32 %div2, i32 %19, i8* %20, i32 %21, i32 %div3, i32 %23)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_resize_frame444(i8* %y, i32 %y_stride, i8* %u, i8* %v, i32 %uv_stride, i32 %height, i32 %width, i8* %oy, i32 %oy_stride, i8* %ou, i8* %ov, i32 %ouv_stride, i32 %oheight, i32 %owidth) #0 {
entry:
  %y.addr = alloca i8*, align 4
  %y_stride.addr = alloca i32, align 4
  %u.addr = alloca i8*, align 4
  %v.addr = alloca i8*, align 4
  %uv_stride.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %oy.addr = alloca i8*, align 4
  %oy_stride.addr = alloca i32, align 4
  %ou.addr = alloca i8*, align 4
  %ov.addr = alloca i8*, align 4
  %ouv_stride.addr = alloca i32, align 4
  %oheight.addr = alloca i32, align 4
  %owidth.addr = alloca i32, align 4
  store i8* %y, i8** %y.addr, align 4, !tbaa !6
  store i32 %y_stride, i32* %y_stride.addr, align 4, !tbaa !2
  store i8* %u, i8** %u.addr, align 4, !tbaa !6
  store i8* %v, i8** %v.addr, align 4, !tbaa !6
  store i32 %uv_stride, i32* %uv_stride.addr, align 4, !tbaa !2
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i8* %oy, i8** %oy.addr, align 4, !tbaa !6
  store i32 %oy_stride, i32* %oy_stride.addr, align 4, !tbaa !2
  store i8* %ou, i8** %ou.addr, align 4, !tbaa !6
  store i8* %ov, i8** %ov.addr, align 4, !tbaa !6
  store i32 %ouv_stride, i32* %ouv_stride.addr, align 4, !tbaa !2
  store i32 %oheight, i32* %oheight.addr, align 4, !tbaa !2
  store i32 %owidth, i32* %owidth.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %y.addr, align 4, !tbaa !6
  %1 = load i32, i32* %height.addr, align 4, !tbaa !2
  %2 = load i32, i32* %width.addr, align 4, !tbaa !2
  %3 = load i32, i32* %y_stride.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %oy.addr, align 4, !tbaa !6
  %5 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %6 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %7 = load i32, i32* %oy_stride.addr, align 4, !tbaa !2
  call void @av1_resize_plane(i8* %0, i32 %1, i32 %2, i32 %3, i8* %4, i32 %5, i32 %6, i32 %7)
  %8 = load i8*, i8** %u.addr, align 4, !tbaa !6
  %9 = load i32, i32* %height.addr, align 4, !tbaa !2
  %10 = load i32, i32* %width.addr, align 4, !tbaa !2
  %11 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %ou.addr, align 4, !tbaa !6
  %13 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %14 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %15 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  call void @av1_resize_plane(i8* %8, i32 %9, i32 %10, i32 %11, i8* %12, i32 %13, i32 %14, i32 %15)
  %16 = load i8*, i8** %v.addr, align 4, !tbaa !6
  %17 = load i32, i32* %height.addr, align 4, !tbaa !2
  %18 = load i32, i32* %width.addr, align 4, !tbaa !2
  %19 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %ov.addr, align 4, !tbaa !6
  %21 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %22 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %23 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  call void @av1_resize_plane(i8* %16, i32 %17, i32 %18, i32 %19, i8* %20, i32 %21, i32 %22, i32 %23)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_resize_frame420(i8* %y, i32 %y_stride, i8* %u, i8* %v, i32 %uv_stride, i32 %height, i32 %width, i8* %oy, i32 %oy_stride, i8* %ou, i8* %ov, i32 %ouv_stride, i32 %oheight, i32 %owidth, i32 %bd) #0 {
entry:
  %y.addr = alloca i8*, align 4
  %y_stride.addr = alloca i32, align 4
  %u.addr = alloca i8*, align 4
  %v.addr = alloca i8*, align 4
  %uv_stride.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %oy.addr = alloca i8*, align 4
  %oy_stride.addr = alloca i32, align 4
  %ou.addr = alloca i8*, align 4
  %ov.addr = alloca i8*, align 4
  %ouv_stride.addr = alloca i32, align 4
  %oheight.addr = alloca i32, align 4
  %owidth.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i8* %y, i8** %y.addr, align 4, !tbaa !6
  store i32 %y_stride, i32* %y_stride.addr, align 4, !tbaa !2
  store i8* %u, i8** %u.addr, align 4, !tbaa !6
  store i8* %v, i8** %v.addr, align 4, !tbaa !6
  store i32 %uv_stride, i32* %uv_stride.addr, align 4, !tbaa !2
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i8* %oy, i8** %oy.addr, align 4, !tbaa !6
  store i32 %oy_stride, i32* %oy_stride.addr, align 4, !tbaa !2
  store i8* %ou, i8** %ou.addr, align 4, !tbaa !6
  store i8* %ov, i8** %ov.addr, align 4, !tbaa !6
  store i32 %ouv_stride, i32* %ouv_stride.addr, align 4, !tbaa !2
  store i32 %oheight, i32* %oheight.addr, align 4, !tbaa !2
  store i32 %owidth, i32* %owidth.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %y.addr, align 4, !tbaa !6
  %1 = load i32, i32* %height.addr, align 4, !tbaa !2
  %2 = load i32, i32* %width.addr, align 4, !tbaa !2
  %3 = load i32, i32* %y_stride.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %oy.addr, align 4, !tbaa !6
  %5 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %6 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %7 = load i32, i32* %oy_stride.addr, align 4, !tbaa !2
  %8 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %0, i32 %1, i32 %2, i32 %3, i8* %4, i32 %5, i32 %6, i32 %7, i32 %8)
  %9 = load i8*, i8** %u.addr, align 4, !tbaa !6
  %10 = load i32, i32* %height.addr, align 4, !tbaa !2
  %div = sdiv i32 %10, 2
  %11 = load i32, i32* %width.addr, align 4, !tbaa !2
  %div1 = sdiv i32 %11, 2
  %12 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %13 = load i8*, i8** %ou.addr, align 4, !tbaa !6
  %14 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %div2 = sdiv i32 %14, 2
  %15 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %div3 = sdiv i32 %15, 2
  %16 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  %17 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %9, i32 %div, i32 %div1, i32 %12, i8* %13, i32 %div2, i32 %div3, i32 %16, i32 %17)
  %18 = load i8*, i8** %v.addr, align 4, !tbaa !6
  %19 = load i32, i32* %height.addr, align 4, !tbaa !2
  %div4 = sdiv i32 %19, 2
  %20 = load i32, i32* %width.addr, align 4, !tbaa !2
  %div5 = sdiv i32 %20, 2
  %21 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %ov.addr, align 4, !tbaa !6
  %23 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %div6 = sdiv i32 %23, 2
  %24 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %div7 = sdiv i32 %24, 2
  %25 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  %26 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %18, i32 %div4, i32 %div5, i32 %21, i8* %22, i32 %div6, i32 %div7, i32 %25, i32 %26)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_resize_frame422(i8* %y, i32 %y_stride, i8* %u, i8* %v, i32 %uv_stride, i32 %height, i32 %width, i8* %oy, i32 %oy_stride, i8* %ou, i8* %ov, i32 %ouv_stride, i32 %oheight, i32 %owidth, i32 %bd) #0 {
entry:
  %y.addr = alloca i8*, align 4
  %y_stride.addr = alloca i32, align 4
  %u.addr = alloca i8*, align 4
  %v.addr = alloca i8*, align 4
  %uv_stride.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %oy.addr = alloca i8*, align 4
  %oy_stride.addr = alloca i32, align 4
  %ou.addr = alloca i8*, align 4
  %ov.addr = alloca i8*, align 4
  %ouv_stride.addr = alloca i32, align 4
  %oheight.addr = alloca i32, align 4
  %owidth.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i8* %y, i8** %y.addr, align 4, !tbaa !6
  store i32 %y_stride, i32* %y_stride.addr, align 4, !tbaa !2
  store i8* %u, i8** %u.addr, align 4, !tbaa !6
  store i8* %v, i8** %v.addr, align 4, !tbaa !6
  store i32 %uv_stride, i32* %uv_stride.addr, align 4, !tbaa !2
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i8* %oy, i8** %oy.addr, align 4, !tbaa !6
  store i32 %oy_stride, i32* %oy_stride.addr, align 4, !tbaa !2
  store i8* %ou, i8** %ou.addr, align 4, !tbaa !6
  store i8* %ov, i8** %ov.addr, align 4, !tbaa !6
  store i32 %ouv_stride, i32* %ouv_stride.addr, align 4, !tbaa !2
  store i32 %oheight, i32* %oheight.addr, align 4, !tbaa !2
  store i32 %owidth, i32* %owidth.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %y.addr, align 4, !tbaa !6
  %1 = load i32, i32* %height.addr, align 4, !tbaa !2
  %2 = load i32, i32* %width.addr, align 4, !tbaa !2
  %3 = load i32, i32* %y_stride.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %oy.addr, align 4, !tbaa !6
  %5 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %6 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %7 = load i32, i32* %oy_stride.addr, align 4, !tbaa !2
  %8 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %0, i32 %1, i32 %2, i32 %3, i8* %4, i32 %5, i32 %6, i32 %7, i32 %8)
  %9 = load i8*, i8** %u.addr, align 4, !tbaa !6
  %10 = load i32, i32* %height.addr, align 4, !tbaa !2
  %11 = load i32, i32* %width.addr, align 4, !tbaa !2
  %div = sdiv i32 %11, 2
  %12 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %13 = load i8*, i8** %ou.addr, align 4, !tbaa !6
  %14 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %15 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %div1 = sdiv i32 %15, 2
  %16 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  %17 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %9, i32 %10, i32 %div, i32 %12, i8* %13, i32 %14, i32 %div1, i32 %16, i32 %17)
  %18 = load i8*, i8** %v.addr, align 4, !tbaa !6
  %19 = load i32, i32* %height.addr, align 4, !tbaa !2
  %20 = load i32, i32* %width.addr, align 4, !tbaa !2
  %div2 = sdiv i32 %20, 2
  %21 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %ov.addr, align 4, !tbaa !6
  %23 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %24 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %div3 = sdiv i32 %24, 2
  %25 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  %26 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %18, i32 %19, i32 %div2, i32 %21, i8* %22, i32 %23, i32 %div3, i32 %25, i32 %26)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_resize_frame444(i8* %y, i32 %y_stride, i8* %u, i8* %v, i32 %uv_stride, i32 %height, i32 %width, i8* %oy, i32 %oy_stride, i8* %ou, i8* %ov, i32 %ouv_stride, i32 %oheight, i32 %owidth, i32 %bd) #0 {
entry:
  %y.addr = alloca i8*, align 4
  %y_stride.addr = alloca i32, align 4
  %u.addr = alloca i8*, align 4
  %v.addr = alloca i8*, align 4
  %uv_stride.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %oy.addr = alloca i8*, align 4
  %oy_stride.addr = alloca i32, align 4
  %ou.addr = alloca i8*, align 4
  %ov.addr = alloca i8*, align 4
  %ouv_stride.addr = alloca i32, align 4
  %oheight.addr = alloca i32, align 4
  %owidth.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i8* %y, i8** %y.addr, align 4, !tbaa !6
  store i32 %y_stride, i32* %y_stride.addr, align 4, !tbaa !2
  store i8* %u, i8** %u.addr, align 4, !tbaa !6
  store i8* %v, i8** %v.addr, align 4, !tbaa !6
  store i32 %uv_stride, i32* %uv_stride.addr, align 4, !tbaa !2
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i8* %oy, i8** %oy.addr, align 4, !tbaa !6
  store i32 %oy_stride, i32* %oy_stride.addr, align 4, !tbaa !2
  store i8* %ou, i8** %ou.addr, align 4, !tbaa !6
  store i8* %ov, i8** %ov.addr, align 4, !tbaa !6
  store i32 %ouv_stride, i32* %ouv_stride.addr, align 4, !tbaa !2
  store i32 %oheight, i32* %oheight.addr, align 4, !tbaa !2
  store i32 %owidth, i32* %owidth.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %y.addr, align 4, !tbaa !6
  %1 = load i32, i32* %height.addr, align 4, !tbaa !2
  %2 = load i32, i32* %width.addr, align 4, !tbaa !2
  %3 = load i32, i32* %y_stride.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %oy.addr, align 4, !tbaa !6
  %5 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %6 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %7 = load i32, i32* %oy_stride.addr, align 4, !tbaa !2
  %8 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %0, i32 %1, i32 %2, i32 %3, i8* %4, i32 %5, i32 %6, i32 %7, i32 %8)
  %9 = load i8*, i8** %u.addr, align 4, !tbaa !6
  %10 = load i32, i32* %height.addr, align 4, !tbaa !2
  %11 = load i32, i32* %width.addr, align 4, !tbaa !2
  %12 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %13 = load i8*, i8** %ou.addr, align 4, !tbaa !6
  %14 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %15 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %16 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  %17 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %9, i32 %10, i32 %11, i32 %12, i8* %13, i32 %14, i32 %15, i32 %16, i32 %17)
  %18 = load i8*, i8** %v.addr, align 4, !tbaa !6
  %19 = load i32, i32* %height.addr, align 4, !tbaa !2
  %20 = load i32, i32* %width.addr, align 4, !tbaa !2
  %21 = load i32, i32* %uv_stride.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %ov.addr, align 4, !tbaa !6
  %23 = load i32, i32* %oheight.addr, align 4, !tbaa !2
  %24 = load i32, i32* %owidth.addr, align 4, !tbaa !2
  %25 = load i32, i32* %ouv_stride.addr, align 4, !tbaa !2
  %26 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %18, i32 %19, i32 %20, i32 %21, i8* %22, i32 %23, i32 %24, i32 %25, i32 %26)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_resize_and_extend_frame(%struct.yv12_buffer_config* %src, %struct.yv12_buffer_config* %dst, i32 %bd, i32 %num_planes) #0 {
entry:
  %src.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst.addr = alloca %struct.yv12_buffer_config*, align 4
  %bd.addr = alloca i32, align 4
  %num_planes.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %is_uv = alloca i32, align 4
  store %struct.yv12_buffer_config* %src, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %dst, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !2
  %2 = load i32, i32* %num_planes.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %2, 3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.cond
  %3 = load i32, i32* %num_planes.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %for.cond
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %3, %cond.true ], [ 3, %cond.false ]
  %cmp1 = icmp slt i32 %1, %cond
  br i1 %cmp1, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %cond.end
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #5
  br label %for.end

for.body:                                         ; preds = %cond.end
  %5 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %i, align 4, !tbaa !2
  %cmp2 = icmp sgt i32 %6, 0
  %conv = zext i1 %cmp2 to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !2
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 26
  %8 = load i32, i32* %flags, align 4, !tbaa !13
  %and = and i32 %8, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %9 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %10 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %9, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %10 to [3 x i8*]*
  %11 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %11
  %12 = load i8*, i8** %arrayidx, align 4, !tbaa !8
  %13 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %14 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %13, i32 0, i32 3
  %crop_heights = bitcast %union.anon.4* %14 to [2 x i32]*
  %15 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights, i32 0, i32 %15
  %16 = load i32, i32* %arrayidx3, align 4, !tbaa !8
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %18 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 2
  %crop_widths = bitcast %union.anon.2* %18 to [2 x i32]*
  %19 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths, i32 0, i32 %19
  %20 = load i32, i32* %arrayidx4, align 4, !tbaa !8
  %21 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %22 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %21, i32 0, i32 4
  %strides = bitcast %union.anon.6* %22 to [2 x i32]*
  %23 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx5, align 4, !tbaa !8
  %25 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %26 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %25, i32 0, i32 5
  %buffers6 = bitcast %union.anon.8* %26 to [3 x i8*]*
  %27 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers6, i32 0, i32 %27
  %28 = load i8*, i8** %arrayidx7, align 4, !tbaa !8
  %29 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %30 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %29, i32 0, i32 3
  %crop_heights8 = bitcast %union.anon.4* %30 to [2 x i32]*
  %31 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights8, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx9, align 4, !tbaa !8
  %33 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %34 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %33, i32 0, i32 2
  %crop_widths10 = bitcast %union.anon.2* %34 to [2 x i32]*
  %35 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths10, i32 0, i32 %35
  %36 = load i32, i32* %arrayidx11, align 4, !tbaa !8
  %37 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %38 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %37, i32 0, i32 4
  %strides12 = bitcast %union.anon.6* %38 to [2 x i32]*
  %39 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds [2 x i32], [2 x i32]* %strides12, i32 0, i32 %39
  %40 = load i32, i32* %arrayidx13, align 4, !tbaa !8
  %41 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_resize_plane(i8* %12, i32 %16, i32 %20, i32 %24, i8* %28, i32 %32, i32 %36, i32 %40, i32 %41)
  br label %if.end

if.else:                                          ; preds = %for.body
  %42 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %43 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %42, i32 0, i32 5
  %buffers14 = bitcast %union.anon.8* %43 to [3 x i8*]*
  %44 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers14, i32 0, i32 %44
  %45 = load i8*, i8** %arrayidx15, align 4, !tbaa !8
  %46 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %47 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %46, i32 0, i32 3
  %crop_heights16 = bitcast %union.anon.4* %47 to [2 x i32]*
  %48 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights16, i32 0, i32 %48
  %49 = load i32, i32* %arrayidx17, align 4, !tbaa !8
  %50 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %51 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %50, i32 0, i32 2
  %crop_widths18 = bitcast %union.anon.2* %51 to [2 x i32]*
  %52 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths18, i32 0, i32 %52
  %53 = load i32, i32* %arrayidx19, align 4, !tbaa !8
  %54 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %55 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %54, i32 0, i32 4
  %strides20 = bitcast %union.anon.6* %55 to [2 x i32]*
  %56 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds [2 x i32], [2 x i32]* %strides20, i32 0, i32 %56
  %57 = load i32, i32* %arrayidx21, align 4, !tbaa !8
  %58 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %59 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %58, i32 0, i32 5
  %buffers22 = bitcast %union.anon.8* %59 to [3 x i8*]*
  %60 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers22, i32 0, i32 %60
  %61 = load i8*, i8** %arrayidx23, align 4, !tbaa !8
  %62 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %63 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %62, i32 0, i32 3
  %crop_heights24 = bitcast %union.anon.4* %63 to [2 x i32]*
  %64 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights24, i32 0, i32 %64
  %65 = load i32, i32* %arrayidx25, align 4, !tbaa !8
  %66 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %67 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %66, i32 0, i32 2
  %crop_widths26 = bitcast %union.anon.2* %67 to [2 x i32]*
  %68 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths26, i32 0, i32 %68
  %69 = load i32, i32* %arrayidx27, align 4, !tbaa !8
  %70 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %71 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %70, i32 0, i32 4
  %strides28 = bitcast %union.anon.6* %71 to [2 x i32]*
  %72 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds [2 x i32], [2 x i32]* %strides28, i32 0, i32 %72
  %73 = load i32, i32* %arrayidx29, align 4, !tbaa !8
  call void @av1_resize_plane(i8* %45, i32 %49, i32 %53, i32 %57, i8* %61, i32 %65, i32 %69, i32 %73)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %74 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %75 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %75, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %76 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %77 = load i32, i32* %num_planes.addr, align 4, !tbaa !2
  call void @aom_extend_frame_borders_c(%struct.yv12_buffer_config* %76, i32 %77)
  ret void
}

declare void @aom_extend_frame_borders_c(%struct.yv12_buffer_config*, i32) #2

; Function Attrs: nounwind
define hidden void @av1_upscale_normative_rows(%struct.AV1Common* %cm, i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %plane, i32 %rows) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %rows.addr = alloca i32, align 4
  %is_uv = alloca i32, align 4
  %ss_x = alloca i32, align 4
  %downscaled_plane_width = alloca i32, align 4
  %upscaled_plane_width = alloca i32, align 4
  %superres_denom = alloca i32, align 4
  %tile_col = alloca %struct.TileInfo, align 4
  %x_step_qn = alloca i32, align 4
  %x0_qn = alloca i32, align 4
  %j = alloca i32, align 4
  %downscaled_x0 = alloca i32, align 4
  %downscaled_x1 = alloca i32, align 4
  %src_width = alloca i32, align 4
  %upscaled_x0 = alloca i32, align 4
  %upscaled_x1 = alloca i32, align 4
  %src_ptr = alloca i8*, align 4
  %dst_ptr = alloca i8*, align 4
  %dst_width = alloca i32, align 4
  %pad_left = alloca i32, align 4
  %pad_right = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !6
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !6
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !2
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !2
  %0 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %plane.addr, align 4, !tbaa !2
  %cmp = icmp sgt i32 %1, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !2
  %2 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %is_uv, align 4, !tbaa !2
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 37
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 32
  %5 = load i32, i32* %subsampling_x, align 16, !tbaa !16
  %tobool1 = icmp ne i32 %5, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %tobool1, %land.rhs ]
  %land.ext = zext i1 %6 to i32
  store i32 %land.ext, i32* %ss_x, align 4, !tbaa !2
  %7 = bitcast i32* %downscaled_plane_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 2
  %9 = load i32, i32* %width, align 16, !tbaa !37
  %10 = load i32, i32* %ss_x, align 4, !tbaa !2
  %shl = shl i32 1, %10
  %shr = ashr i32 %shl, 1
  %add = add nsw i32 %9, %shr
  %11 = load i32, i32* %ss_x, align 4, !tbaa !2
  %shr2 = ashr i32 %add, %11
  store i32 %shr2, i32* %downscaled_plane_width, align 4, !tbaa !2
  %12 = bitcast i32* %upscaled_plane_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 6
  %14 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !38
  %15 = load i32, i32* %ss_x, align 4, !tbaa !2
  %shl3 = shl i32 1, %15
  %shr4 = ashr i32 %shl3, 1
  %add5 = add nsw i32 %14, %shr4
  %16 = load i32, i32* %ss_x, align 4, !tbaa !2
  %shr6 = ashr i32 %add5, %16
  store i32 %shr6, i32* %upscaled_plane_width, align 4, !tbaa !2
  %17 = bitcast i32* %superres_denom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %superres_scale_denominator = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 8
  %19 = load i8, i8* %superres_scale_denominator, align 8, !tbaa !39
  %conv7 = zext i8 %19 to i32
  store i32 %conv7, i32* %superres_denom, align 4, !tbaa !2
  %20 = bitcast %struct.TileInfo* %tile_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %20) #5
  %21 = bitcast i32* %x_step_qn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load i32, i32* %downscaled_plane_width, align 4, !tbaa !2
  %23 = load i32, i32* %upscaled_plane_width, align 4, !tbaa !2
  %call = call i32 @av1_get_upscale_convolve_step(i32 %22, i32 %23)
  store i32 %call, i32* %x_step_qn, align 4, !tbaa !2
  %24 = bitcast i32* %x0_qn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  %25 = load i32, i32* %downscaled_plane_width, align 4, !tbaa !2
  %26 = load i32, i32* %upscaled_plane_width, align 4, !tbaa !2
  %27 = load i32, i32* %x_step_qn, align 4, !tbaa !2
  %call8 = call i32 @get_upscale_convolve_x0(i32 %25, i32 %26, i32 %27)
  store i32 %call8, i32* %x0_qn, align 4, !tbaa !2
  %28 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  store i32 0, i32* %j, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %land.end
  %29 = load i32, i32* %j, align 4, !tbaa !2
  %30 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %tiles = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %30, i32 0, i32 40
  %cols = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles, i32 0, i32 0
  %31 = load i32, i32* %cols, align 8, !tbaa !40
  %cmp9 = icmp slt i32 %29, %31
  br i1 %cmp9, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %32 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %33 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %34 = load i32, i32* %j, align 4, !tbaa !2
  call void @av1_tile_set_col(%struct.TileInfo* %tile_col, %struct.AV1Common* %33, i32 %34)
  %35 = bitcast i32* %downscaled_x0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #5
  %mi_col_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile_col, i32 0, i32 2
  %36 = load i32, i32* %mi_col_start, align 4, !tbaa !41
  %37 = load i32, i32* %ss_x, align 4, !tbaa !2
  %sub = sub nsw i32 2, %37
  %shl11 = shl i32 %36, %sub
  store i32 %shl11, i32* %downscaled_x0, align 4, !tbaa !2
  %38 = bitcast i32* %downscaled_x1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #5
  %mi_col_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile_col, i32 0, i32 3
  %39 = load i32, i32* %mi_col_end, align 4, !tbaa !43
  %40 = load i32, i32* %ss_x, align 4, !tbaa !2
  %sub12 = sub nsw i32 2, %40
  %shl13 = shl i32 %39, %sub12
  store i32 %shl13, i32* %downscaled_x1, align 4, !tbaa !2
  %41 = bitcast i32* %src_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #5
  %42 = load i32, i32* %downscaled_x1, align 4, !tbaa !2
  %43 = load i32, i32* %downscaled_x0, align 4, !tbaa !2
  %sub14 = sub nsw i32 %42, %43
  store i32 %sub14, i32* %src_width, align 4, !tbaa !2
  %44 = bitcast i32* %upscaled_x0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  %45 = load i32, i32* %downscaled_x0, align 4, !tbaa !2
  %46 = load i32, i32* %superres_denom, align 4, !tbaa !2
  %mul = mul nsw i32 %45, %46
  %div = sdiv i32 %mul, 8
  store i32 %div, i32* %upscaled_x0, align 4, !tbaa !2
  %47 = bitcast i32* %upscaled_x1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  %48 = load i32, i32* %j, align 4, !tbaa !2
  %49 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %tiles15 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %49, i32 0, i32 40
  %cols16 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles15, i32 0, i32 0
  %50 = load i32, i32* %cols16, align 8, !tbaa !40
  %sub17 = sub nsw i32 %50, 1
  %cmp18 = icmp eq i32 %48, %sub17
  br i1 %cmp18, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %51 = load i32, i32* %upscaled_plane_width, align 4, !tbaa !2
  store i32 %51, i32* %upscaled_x1, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %for.body
  %52 = load i32, i32* %downscaled_x1, align 4, !tbaa !2
  %53 = load i32, i32* %superres_denom, align 4, !tbaa !2
  %mul20 = mul nsw i32 %52, %53
  %div21 = sdiv i32 %mul20, 8
  store i32 %div21, i32* %upscaled_x1, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %54 = bitcast i8** %src_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #5
  %55 = load i8*, i8** %src.addr, align 4, !tbaa !6
  %56 = load i32, i32* %downscaled_x0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %55, i32 %56
  store i8* %add.ptr, i8** %src_ptr, align 4, !tbaa !6
  %57 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #5
  %58 = load i8*, i8** %dst.addr, align 4, !tbaa !6
  %59 = load i32, i32* %upscaled_x0, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i8, i8* %58, i32 %59
  store i8* %add.ptr22, i8** %dst_ptr, align 4, !tbaa !6
  %60 = bitcast i32* %dst_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #5
  %61 = load i32, i32* %upscaled_x1, align 4, !tbaa !2
  %62 = load i32, i32* %upscaled_x0, align 4, !tbaa !2
  %sub23 = sub nsw i32 %61, %62
  store i32 %sub23, i32* %dst_width, align 4, !tbaa !2
  %63 = bitcast i32* %pad_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #5
  %64 = load i32, i32* %j, align 4, !tbaa !2
  %cmp24 = icmp eq i32 %64, 0
  %conv25 = zext i1 %cmp24 to i32
  store i32 %conv25, i32* %pad_left, align 4, !tbaa !2
  %65 = bitcast i32* %pad_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #5
  %66 = load i32, i32* %j, align 4, !tbaa !2
  %67 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %tiles26 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %67, i32 0, i32 40
  %cols27 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles26, i32 0, i32 0
  %68 = load i32, i32* %cols27, align 8, !tbaa !40
  %sub28 = sub nsw i32 %68, 1
  %cmp29 = icmp eq i32 %66, %sub28
  %conv30 = zext i1 %cmp29 to i32
  store i32 %conv30, i32* %pad_right, align 4, !tbaa !2
  %69 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params31 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %69, i32 0, i32 37
  %use_highbitdepth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params31, i32 0, i32 26
  %70 = load i8, i8* %use_highbitdepth, align 4, !tbaa !44
  %tobool32 = icmp ne i8 %70, 0
  br i1 %tobool32, label %if.then33, label %if.else35

if.then33:                                        ; preds = %if.end
  %71 = load i8*, i8** %src_ptr, align 4, !tbaa !6
  %72 = load i32, i32* %rows.addr, align 4, !tbaa !2
  %73 = load i32, i32* %src_width, align 4, !tbaa !2
  %74 = load i32, i32* %src_stride.addr, align 4, !tbaa !2
  %75 = load i8*, i8** %dst_ptr, align 4, !tbaa !6
  %76 = load i32, i32* %rows.addr, align 4, !tbaa !2
  %77 = load i32, i32* %dst_width, align 4, !tbaa !2
  %78 = load i32, i32* %dst_stride.addr, align 4, !tbaa !2
  %79 = load i32, i32* %x_step_qn, align 4, !tbaa !2
  %80 = load i32, i32* %x0_qn, align 4, !tbaa !2
  %81 = load i32, i32* %pad_left, align 4, !tbaa !2
  %82 = load i32, i32* %pad_right, align 4, !tbaa !2
  %83 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params34 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %83, i32 0, i32 37
  %bit_depth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params34, i32 0, i32 25
  %84 = load i32, i32* %bit_depth, align 8, !tbaa !45
  call void @highbd_upscale_normative_rect(i8* %71, i32 %72, i32 %73, i32 %74, i8* %75, i32 %76, i32 %77, i32 %78, i32 %79, i32 %80, i32 %81, i32 %82, i32 %84)
  br label %if.end36

if.else35:                                        ; preds = %if.end
  %85 = load i8*, i8** %src_ptr, align 4, !tbaa !6
  %86 = load i32, i32* %rows.addr, align 4, !tbaa !2
  %87 = load i32, i32* %src_width, align 4, !tbaa !2
  %88 = load i32, i32* %src_stride.addr, align 4, !tbaa !2
  %89 = load i8*, i8** %dst_ptr, align 4, !tbaa !6
  %90 = load i32, i32* %rows.addr, align 4, !tbaa !2
  %91 = load i32, i32* %dst_width, align 4, !tbaa !2
  %92 = load i32, i32* %dst_stride.addr, align 4, !tbaa !2
  %93 = load i32, i32* %x_step_qn, align 4, !tbaa !2
  %94 = load i32, i32* %x0_qn, align 4, !tbaa !2
  %95 = load i32, i32* %pad_left, align 4, !tbaa !2
  %96 = load i32, i32* %pad_right, align 4, !tbaa !2
  call void @upscale_normative_rect(i8* %85, i32 %86, i32 %87, i32 %88, i8* %89, i32 %90, i32 %91, i32 %92, i32 %93, i32 %94, i32 %95, i32 %96)
  br label %if.end36

if.end36:                                         ; preds = %if.else35, %if.then33
  %97 = load i32, i32* %dst_width, align 4, !tbaa !2
  %98 = load i32, i32* %x_step_qn, align 4, !tbaa !2
  %mul37 = mul nsw i32 %97, %98
  %99 = load i32, i32* %src_width, align 4, !tbaa !2
  %shl38 = shl i32 %99, 14
  %sub39 = sub nsw i32 %mul37, %shl38
  %100 = load i32, i32* %x0_qn, align 4, !tbaa !2
  %add40 = add nsw i32 %100, %sub39
  store i32 %add40, i32* %x0_qn, align 4, !tbaa !2
  %101 = bitcast i32* %pad_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #5
  %102 = bitcast i32* %pad_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #5
  %103 = bitcast i32* %dst_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #5
  %104 = bitcast i8** %dst_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #5
  %105 = bitcast i8** %src_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #5
  %106 = bitcast i32* %upscaled_x1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #5
  %107 = bitcast i32* %upscaled_x0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #5
  %108 = bitcast i32* %src_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  %109 = bitcast i32* %downscaled_x1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #5
  %110 = bitcast i32* %downscaled_x0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end36
  %111 = load i32, i32* %j, align 4, !tbaa !2
  %inc = add nsw i32 %111, 1
  store i32 %inc, i32* %j, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %112 = bitcast i32* %x0_qn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #5
  %113 = bitcast i32* %x_step_qn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #5
  %114 = bitcast %struct.TileInfo* %tile_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %114) #5
  %115 = bitcast i32* %superres_denom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #5
  %116 = bitcast i32* %upscaled_plane_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #5
  %117 = bitcast i32* %downscaled_plane_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  ret void
}

; Function Attrs: nounwind
define internal i32 @get_upscale_convolve_x0(i32 %in_length, i32 %out_length, i32 %x_step_qn) #0 {
entry:
  %in_length.addr = alloca i32, align 4
  %out_length.addr = alloca i32, align 4
  %x_step_qn.addr = alloca i32, align 4
  %err = alloca i32, align 4
  %x0 = alloca i32, align 4
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  store i32 %x_step_qn, i32* %x_step_qn.addr, align 4, !tbaa !2
  %0 = bitcast i32* %err to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %2 = load i32, i32* %x_step_qn.addr, align 4, !tbaa !2
  %mul = mul nsw i32 %1, %2
  %3 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %shl = shl i32 %3, 14
  %sub = sub nsw i32 %mul, %shl
  store i32 %sub, i32* %err, align 4, !tbaa !2
  %4 = bitcast i32* %x0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %6 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub1 = sub nsw i32 %5, %6
  %shl2 = shl i32 %sub1, 13
  %sub3 = sub nsw i32 0, %shl2
  %7 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div = sdiv i32 %7, 2
  %add = add nsw i32 %sub3, %div
  %8 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div4 = sdiv i32 %add, %8
  %add5 = add nsw i32 %div4, 128
  %9 = load i32, i32* %err, align 4, !tbaa !2
  %div6 = sdiv i32 %9, 2
  %sub7 = sub nsw i32 %add5, %div6
  store i32 %sub7, i32* %x0, align 4, !tbaa !2
  %10 = load i32, i32* %x0, align 4, !tbaa !2
  %and = and i32 %10, 16383
  %11 = bitcast i32* %x0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #5
  %12 = bitcast i32* %err to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  ret i32 %and
}

declare void @av1_tile_set_col(%struct.TileInfo*, %struct.AV1Common*, i32) #2

; Function Attrs: nounwind
define internal void @highbd_upscale_normative_rect(i8* %input, i32 %height, i32 %width, i32 %in_stride, i8* %output, i32 %height2, i32 %width2, i32 %out_stride, i32 %x_step_qn, i32 %x0_qn, i32 %pad_left, i32 %pad_right, i32 %bd) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %in_stride.addr = alloca i32, align 4
  %output.addr = alloca i8*, align 4
  %height2.addr = alloca i32, align 4
  %width2.addr = alloca i32, align 4
  %out_stride.addr = alloca i32, align 4
  %x_step_qn.addr = alloca i32, align 4
  %x0_qn.addr = alloca i32, align 4
  %pad_left.addr = alloca i32, align 4
  %pad_right.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %border_cols = alloca i32, align 4
  %border_size = alloca i32, align 4
  %tmp_left = alloca i16*, align 4
  %tmp_right = alloca i16*, align 4
  %input16 = alloca i16*, align 4
  %in_tl = alloca i16*, align 4
  %in_tr = alloca i16*, align 4
  %i = alloca i32, align 4
  %i14 = alloca i32, align 4
  %i39 = alloca i32, align 4
  %i55 = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i32 %in_stride, i32* %in_stride.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !6
  store i32 %height2, i32* %height2.addr, align 4, !tbaa !2
  store i32 %width2, i32* %width2.addr, align 4, !tbaa !2
  store i32 %out_stride, i32* %out_stride.addr, align 4, !tbaa !2
  store i32 %x_step_qn, i32* %x_step_qn.addr, align 4, !tbaa !2
  store i32 %x0_qn, i32* %x0_qn.addr, align 4, !tbaa !2
  store i32 %pad_left, i32* %pad_left.addr, align 4, !tbaa !2
  store i32 %pad_right, i32* %pad_right.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = bitcast i32* %border_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 5, i32* %border_cols, align 4, !tbaa !2
  %1 = bitcast i32* %border_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 10, i32* %border_size, align 4, !tbaa !2
  %2 = bitcast i16** %tmp_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i16* null, i16** %tmp_left, align 4, !tbaa !6
  %3 = bitcast i16** %tmp_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i16* null, i16** %tmp_right, align 4, !tbaa !6
  %4 = bitcast i16** %input16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %6 = ptrtoint i8* %5 to i32
  %shl = shl i32 %6, 1
  %7 = inttoptr i32 %shl to i16*
  store i16* %7, i16** %input16, align 4, !tbaa !6
  %8 = bitcast i16** %in_tl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i16*, i16** %input16, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i16, i16* %9, i32 -5
  store i16* %add.ptr, i16** %in_tl, align 4, !tbaa !6
  %10 = bitcast i16** %in_tr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load i16*, i16** %input16, align 4, !tbaa !6
  %12 = load i32, i32* %width.addr, align 4, !tbaa !2
  %add.ptr1 = getelementptr inbounds i16, i16* %11, i32 %12
  store i16* %add.ptr1, i16** %in_tr, align 4, !tbaa !6
  %13 = load i32, i32* %pad_left.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %14 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul = mul i32 10, %14
  %call = call i8* @aom_malloc(i32 %mul)
  %15 = bitcast i8* %call to i16*
  store i16* %15, i16** %tmp_left, align 4, !tbaa !6
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %17 = load i32, i32* %i, align 4, !tbaa !2
  %18 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %20 = load i16*, i16** %tmp_left, align 4, !tbaa !6
  %21 = load i32, i32* %i, align 4, !tbaa !2
  %mul2 = mul nsw i32 %21, 5
  %add.ptr3 = getelementptr inbounds i16, i16* %20, i32 %mul2
  %22 = bitcast i16* %add.ptr3 to i8*
  %23 = load i16*, i16** %in_tl, align 4, !tbaa !6
  %24 = load i32, i32* %i, align 4, !tbaa !2
  %25 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul4 = mul nsw i32 %24, %25
  %add.ptr5 = getelementptr inbounds i16, i16* %23, i32 %mul4
  %26 = bitcast i16* %add.ptr5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %22, i8* align 2 %26, i32 10, i1 false)
  %27 = load i16*, i16** %in_tl, align 4, !tbaa !6
  %28 = load i32, i32* %i, align 4, !tbaa !2
  %29 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul6 = mul nsw i32 %28, %29
  %add.ptr7 = getelementptr inbounds i16, i16* %27, i32 %mul6
  %30 = bitcast i16* %add.ptr7 to i8*
  %31 = load i16*, i16** %input16, align 4, !tbaa !6
  %32 = load i32, i32* %i, align 4, !tbaa !2
  %33 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul8 = mul nsw i32 %32, %33
  %arrayidx = getelementptr inbounds i16, i16* %31, i32 %mul8
  %34 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %34 to i32
  %call9 = call i8* @aom_memset16(i8* %30, i32 %conv, i32 5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %35 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %36 = load i32, i32* %pad_right.addr, align 4, !tbaa !2
  %tobool10 = icmp ne i32 %36, 0
  br i1 %tobool10, label %if.then11, label %if.end33

if.then11:                                        ; preds = %if.end
  %37 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul12 = mul i32 10, %37
  %call13 = call i8* @aom_malloc(i32 %mul12)
  %38 = bitcast i8* %call13 to i16*
  store i16* %38, i16** %tmp_right, align 4, !tbaa !6
  %39 = bitcast i32* %i14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #5
  store i32 0, i32* %i14, align 4, !tbaa !2
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc30, %if.then11
  %40 = load i32, i32* %i14, align 4, !tbaa !2
  %41 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp16 = icmp slt i32 %40, %41
  br i1 %cmp16, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond15
  %42 = bitcast i32* %i14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #5
  br label %for.end32

for.body19:                                       ; preds = %for.cond15
  %43 = load i16*, i16** %tmp_right, align 4, !tbaa !6
  %44 = load i32, i32* %i14, align 4, !tbaa !2
  %mul20 = mul nsw i32 %44, 5
  %add.ptr21 = getelementptr inbounds i16, i16* %43, i32 %mul20
  %45 = bitcast i16* %add.ptr21 to i8*
  %46 = load i16*, i16** %in_tr, align 4, !tbaa !6
  %47 = load i32, i32* %i14, align 4, !tbaa !2
  %48 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul22 = mul nsw i32 %47, %48
  %add.ptr23 = getelementptr inbounds i16, i16* %46, i32 %mul22
  %49 = bitcast i16* %add.ptr23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %45, i8* align 2 %49, i32 10, i1 false)
  %50 = load i16*, i16** %in_tr, align 4, !tbaa !6
  %51 = load i32, i32* %i14, align 4, !tbaa !2
  %52 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul24 = mul nsw i32 %51, %52
  %add.ptr25 = getelementptr inbounds i16, i16* %50, i32 %mul24
  %53 = bitcast i16* %add.ptr25 to i8*
  %54 = load i16*, i16** %input16, align 4, !tbaa !6
  %55 = load i32, i32* %i14, align 4, !tbaa !2
  %56 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul26 = mul nsw i32 %55, %56
  %57 = load i32, i32* %width.addr, align 4, !tbaa !2
  %add = add nsw i32 %mul26, %57
  %sub = sub nsw i32 %add, 1
  %arrayidx27 = getelementptr inbounds i16, i16* %54, i32 %sub
  %58 = load i16, i16* %arrayidx27, align 2, !tbaa !11
  %conv28 = zext i16 %58 to i32
  %call29 = call i8* @aom_memset16(i8* %53, i32 %conv28, i32 5)
  br label %for.inc30

for.inc30:                                        ; preds = %for.body19
  %59 = load i32, i32* %i14, align 4, !tbaa !2
  %inc31 = add nsw i32 %59, 1
  store i32 %inc31, i32* %i14, align 4, !tbaa !2
  br label %for.cond15

for.end32:                                        ; preds = %for.cond.cleanup18
  br label %if.end33

if.end33:                                         ; preds = %for.end32, %if.end
  %60 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %add.ptr34 = getelementptr inbounds i8, i8* %60, i32 -1
  %61 = ptrtoint i8* %add.ptr34 to i32
  %shl35 = shl i32 %61, 1
  %62 = inttoptr i32 %shl35 to i16*
  %63 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %64 = load i8*, i8** %output.addr, align 4, !tbaa !6
  %65 = ptrtoint i8* %64 to i32
  %shl36 = shl i32 %65, 1
  %66 = inttoptr i32 %shl36 to i16*
  %67 = load i32, i32* %out_stride.addr, align 4, !tbaa !2
  %68 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %69 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %70 = load i32, i32* %x0_qn.addr, align 4, !tbaa !2
  %71 = load i32, i32* %x_step_qn.addr, align 4, !tbaa !2
  %72 = load i32, i32* %bd.addr, align 4, !tbaa !2
  call void @av1_highbd_convolve_horiz_rs_c(i16* %62, i32 %63, i16* %66, i32 %67, i32 %68, i32 %69, i16* getelementptr inbounds ([64 x [8 x i16]], [64 x [8 x i16]]* @av1_resize_filter_normative, i32 0, i32 0, i32 0), i32 %70, i32 %71, i32 %72)
  %73 = load i32, i32* %pad_left.addr, align 4, !tbaa !2
  %tobool37 = icmp ne i32 %73, 0
  br i1 %tobool37, label %if.then38, label %if.end52

if.then38:                                        ; preds = %if.end33
  %74 = bitcast i32* %i39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #5
  store i32 0, i32* %i39, align 4, !tbaa !2
  br label %for.cond40

for.cond40:                                       ; preds = %for.inc49, %if.then38
  %75 = load i32, i32* %i39, align 4, !tbaa !2
  %76 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp41 = icmp slt i32 %75, %76
  br i1 %cmp41, label %for.body44, label %for.cond.cleanup43

for.cond.cleanup43:                               ; preds = %for.cond40
  %77 = bitcast i32* %i39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #5
  br label %for.end51

for.body44:                                       ; preds = %for.cond40
  %78 = load i16*, i16** %in_tl, align 4, !tbaa !6
  %79 = load i32, i32* %i39, align 4, !tbaa !2
  %80 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul45 = mul nsw i32 %79, %80
  %add.ptr46 = getelementptr inbounds i16, i16* %78, i32 %mul45
  %81 = bitcast i16* %add.ptr46 to i8*
  %82 = load i16*, i16** %tmp_left, align 4, !tbaa !6
  %83 = load i32, i32* %i39, align 4, !tbaa !2
  %mul47 = mul nsw i32 %83, 5
  %add.ptr48 = getelementptr inbounds i16, i16* %82, i32 %mul47
  %84 = bitcast i16* %add.ptr48 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %81, i8* align 2 %84, i32 10, i1 false)
  br label %for.inc49

for.inc49:                                        ; preds = %for.body44
  %85 = load i32, i32* %i39, align 4, !tbaa !2
  %inc50 = add nsw i32 %85, 1
  store i32 %inc50, i32* %i39, align 4, !tbaa !2
  br label %for.cond40

for.end51:                                        ; preds = %for.cond.cleanup43
  %86 = load i16*, i16** %tmp_left, align 4, !tbaa !6
  %87 = bitcast i16* %86 to i8*
  call void @aom_free(i8* %87)
  br label %if.end52

if.end52:                                         ; preds = %for.end51, %if.end33
  %88 = load i32, i32* %pad_right.addr, align 4, !tbaa !2
  %tobool53 = icmp ne i32 %88, 0
  br i1 %tobool53, label %if.then54, label %if.end68

if.then54:                                        ; preds = %if.end52
  %89 = bitcast i32* %i55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #5
  store i32 0, i32* %i55, align 4, !tbaa !2
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc65, %if.then54
  %90 = load i32, i32* %i55, align 4, !tbaa !2
  %91 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp57 = icmp slt i32 %90, %91
  br i1 %cmp57, label %for.body60, label %for.cond.cleanup59

for.cond.cleanup59:                               ; preds = %for.cond56
  %92 = bitcast i32* %i55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #5
  br label %for.end67

for.body60:                                       ; preds = %for.cond56
  %93 = load i16*, i16** %in_tr, align 4, !tbaa !6
  %94 = load i32, i32* %i55, align 4, !tbaa !2
  %95 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul61 = mul nsw i32 %94, %95
  %add.ptr62 = getelementptr inbounds i16, i16* %93, i32 %mul61
  %96 = bitcast i16* %add.ptr62 to i8*
  %97 = load i16*, i16** %tmp_right, align 4, !tbaa !6
  %98 = load i32, i32* %i55, align 4, !tbaa !2
  %mul63 = mul nsw i32 %98, 5
  %add.ptr64 = getelementptr inbounds i16, i16* %97, i32 %mul63
  %99 = bitcast i16* %add.ptr64 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %96, i8* align 2 %99, i32 10, i1 false)
  br label %for.inc65

for.inc65:                                        ; preds = %for.body60
  %100 = load i32, i32* %i55, align 4, !tbaa !2
  %inc66 = add nsw i32 %100, 1
  store i32 %inc66, i32* %i55, align 4, !tbaa !2
  br label %for.cond56

for.end67:                                        ; preds = %for.cond.cleanup59
  %101 = load i16*, i16** %tmp_right, align 4, !tbaa !6
  %102 = bitcast i16* %101 to i8*
  call void @aom_free(i8* %102)
  br label %if.end68

if.end68:                                         ; preds = %for.end67, %if.end52
  %103 = bitcast i16** %in_tr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #5
  %104 = bitcast i16** %in_tl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #5
  %105 = bitcast i16** %input16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #5
  %106 = bitcast i16** %tmp_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #5
  %107 = bitcast i16** %tmp_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #5
  %108 = bitcast i32* %border_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  %109 = bitcast i32* %border_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #5
  ret void
}

; Function Attrs: nounwind
define internal void @upscale_normative_rect(i8* %input, i32 %height, i32 %width, i32 %in_stride, i8* %output, i32 %height2, i32 %width2, i32 %out_stride, i32 %x_step_qn, i32 %x0_qn, i32 %pad_left, i32 %pad_right) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %in_stride.addr = alloca i32, align 4
  %output.addr = alloca i8*, align 4
  %height2.addr = alloca i32, align 4
  %width2.addr = alloca i32, align 4
  %out_stride.addr = alloca i32, align 4
  %x_step_qn.addr = alloca i32, align 4
  %x0_qn.addr = alloca i32, align 4
  %pad_left.addr = alloca i32, align 4
  %pad_right.addr = alloca i32, align 4
  %border_cols = alloca i32, align 4
  %tmp_left = alloca i8*, align 4
  %tmp_right = alloca i8*, align 4
  %in_tl = alloca i8*, align 4
  %in_tr = alloca i8*, align 4
  %i = alloca i32, align 4
  %i13 = alloca i32, align 4
  %i35 = alloca i32, align 4
  %i51 = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i32 %in_stride, i32* %in_stride.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !6
  store i32 %height2, i32* %height2.addr, align 4, !tbaa !2
  store i32 %width2, i32* %width2.addr, align 4, !tbaa !2
  store i32 %out_stride, i32* %out_stride.addr, align 4, !tbaa !2
  store i32 %x_step_qn, i32* %x_step_qn.addr, align 4, !tbaa !2
  store i32 %x0_qn, i32* %x0_qn.addr, align 4, !tbaa !2
  store i32 %pad_left, i32* %pad_left.addr, align 4, !tbaa !2
  store i32 %pad_right, i32* %pad_right.addr, align 4, !tbaa !2
  %0 = bitcast i32* %border_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 5, i32* %border_cols, align 4, !tbaa !2
  %1 = bitcast i8** %tmp_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i8* null, i8** %tmp_left, align 4, !tbaa !6
  %2 = bitcast i8** %tmp_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i8* null, i8** %tmp_right, align 4, !tbaa !6
  %3 = bitcast i8** %in_tl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 -5
  store i8* %add.ptr, i8** %in_tl, align 4, !tbaa !6
  %5 = bitcast i8** %in_tr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %7 = load i32, i32* %width.addr, align 4, !tbaa !2
  %add.ptr1 = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr1, i8** %in_tr, align 4, !tbaa !6
  %8 = load i32, i32* %pad_left.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %9 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul = mul i32 5, %9
  %call = call i8* @aom_malloc(i32 %mul)
  store i8* %call, i8** %tmp_left, align 4, !tbaa !6
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4, !tbaa !2
  %12 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i8*, i8** %tmp_left, align 4, !tbaa !6
  %15 = load i32, i32* %i, align 4, !tbaa !2
  %mul2 = mul nsw i32 %15, 5
  %add.ptr3 = getelementptr inbounds i8, i8* %14, i32 %mul2
  %16 = load i8*, i8** %in_tl, align 4, !tbaa !6
  %17 = load i32, i32* %i, align 4, !tbaa !2
  %18 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul4 = mul nsw i32 %17, %18
  %add.ptr5 = getelementptr inbounds i8, i8* %16, i32 %mul4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr3, i8* align 1 %add.ptr5, i32 5, i1 false)
  %19 = load i8*, i8** %in_tl, align 4, !tbaa !6
  %20 = load i32, i32* %i, align 4, !tbaa !2
  %21 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul6 = mul nsw i32 %20, %21
  %add.ptr7 = getelementptr inbounds i8, i8* %19, i32 %mul6
  %22 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %23 = load i32, i32* %i, align 4, !tbaa !2
  %24 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul8 = mul nsw i32 %23, %24
  %arrayidx = getelementptr inbounds i8, i8* %22, i32 %mul8
  %25 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %25 to i32
  %26 = trunc i32 %conv to i8
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr7, i8 %26, i32 5, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %28 = load i32, i32* %pad_right.addr, align 4, !tbaa !2
  %tobool9 = icmp ne i32 %28, 0
  br i1 %tobool9, label %if.then10, label %if.end31

if.then10:                                        ; preds = %if.end
  %29 = load i32, i32* %height.addr, align 4, !tbaa !2
  %mul11 = mul i32 5, %29
  %call12 = call i8* @aom_malloc(i32 %mul11)
  store i8* %call12, i8** %tmp_right, align 4, !tbaa !6
  %30 = bitcast i32* %i13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #5
  store i32 0, i32* %i13, align 4, !tbaa !2
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc28, %if.then10
  %31 = load i32, i32* %i13, align 4, !tbaa !2
  %32 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp15 = icmp slt i32 %31, %32
  br i1 %cmp15, label %for.body18, label %for.cond.cleanup17

for.cond.cleanup17:                               ; preds = %for.cond14
  %33 = bitcast i32* %i13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  br label %for.end30

for.body18:                                       ; preds = %for.cond14
  %34 = load i8*, i8** %tmp_right, align 4, !tbaa !6
  %35 = load i32, i32* %i13, align 4, !tbaa !2
  %mul19 = mul nsw i32 %35, 5
  %add.ptr20 = getelementptr inbounds i8, i8* %34, i32 %mul19
  %36 = load i8*, i8** %in_tr, align 4, !tbaa !6
  %37 = load i32, i32* %i13, align 4, !tbaa !2
  %38 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul21 = mul nsw i32 %37, %38
  %add.ptr22 = getelementptr inbounds i8, i8* %36, i32 %mul21
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr20, i8* align 1 %add.ptr22, i32 5, i1 false)
  %39 = load i8*, i8** %in_tr, align 4, !tbaa !6
  %40 = load i32, i32* %i13, align 4, !tbaa !2
  %41 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul23 = mul nsw i32 %40, %41
  %add.ptr24 = getelementptr inbounds i8, i8* %39, i32 %mul23
  %42 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %43 = load i32, i32* %i13, align 4, !tbaa !2
  %44 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul25 = mul nsw i32 %43, %44
  %45 = load i32, i32* %width.addr, align 4, !tbaa !2
  %add = add nsw i32 %mul25, %45
  %sub = sub nsw i32 %add, 1
  %arrayidx26 = getelementptr inbounds i8, i8* %42, i32 %sub
  %46 = load i8, i8* %arrayidx26, align 1, !tbaa !8
  %conv27 = zext i8 %46 to i32
  %47 = trunc i32 %conv27 to i8
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr24, i8 %47, i32 5, i1 false)
  br label %for.inc28

for.inc28:                                        ; preds = %for.body18
  %48 = load i32, i32* %i13, align 4, !tbaa !2
  %inc29 = add nsw i32 %48, 1
  store i32 %inc29, i32* %i13, align 4, !tbaa !2
  br label %for.cond14

for.end30:                                        ; preds = %for.cond.cleanup17
  br label %if.end31

if.end31:                                         ; preds = %for.end30, %if.end
  %49 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %add.ptr32 = getelementptr inbounds i8, i8* %49, i32 -1
  %50 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %51 = load i8*, i8** %output.addr, align 4, !tbaa !6
  %52 = load i32, i32* %out_stride.addr, align 4, !tbaa !2
  %53 = load i32, i32* %width2.addr, align 4, !tbaa !2
  %54 = load i32, i32* %height2.addr, align 4, !tbaa !2
  %55 = load i32, i32* %x0_qn.addr, align 4, !tbaa !2
  %56 = load i32, i32* %x_step_qn.addr, align 4, !tbaa !2
  call void @av1_convolve_horiz_rs_c(i8* %add.ptr32, i32 %50, i8* %51, i32 %52, i32 %53, i32 %54, i16* getelementptr inbounds ([64 x [8 x i16]], [64 x [8 x i16]]* @av1_resize_filter_normative, i32 0, i32 0, i32 0), i32 %55, i32 %56)
  %57 = load i32, i32* %pad_left.addr, align 4, !tbaa !2
  %tobool33 = icmp ne i32 %57, 0
  br i1 %tobool33, label %if.then34, label %if.end48

if.then34:                                        ; preds = %if.end31
  %58 = bitcast i32* %i35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #5
  store i32 0, i32* %i35, align 4, !tbaa !2
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc45, %if.then34
  %59 = load i32, i32* %i35, align 4, !tbaa !2
  %60 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp37 = icmp slt i32 %59, %60
  br i1 %cmp37, label %for.body40, label %for.cond.cleanup39

for.cond.cleanup39:                               ; preds = %for.cond36
  %61 = bitcast i32* %i35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #5
  br label %for.end47

for.body40:                                       ; preds = %for.cond36
  %62 = load i8*, i8** %in_tl, align 4, !tbaa !6
  %63 = load i32, i32* %i35, align 4, !tbaa !2
  %64 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul41 = mul nsw i32 %63, %64
  %add.ptr42 = getelementptr inbounds i8, i8* %62, i32 %mul41
  %65 = load i8*, i8** %tmp_left, align 4, !tbaa !6
  %66 = load i32, i32* %i35, align 4, !tbaa !2
  %mul43 = mul nsw i32 %66, 5
  %add.ptr44 = getelementptr inbounds i8, i8* %65, i32 %mul43
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr42, i8* align 1 %add.ptr44, i32 5, i1 false)
  br label %for.inc45

for.inc45:                                        ; preds = %for.body40
  %67 = load i32, i32* %i35, align 4, !tbaa !2
  %inc46 = add nsw i32 %67, 1
  store i32 %inc46, i32* %i35, align 4, !tbaa !2
  br label %for.cond36

for.end47:                                        ; preds = %for.cond.cleanup39
  %68 = load i8*, i8** %tmp_left, align 4, !tbaa !6
  call void @aom_free(i8* %68)
  br label %if.end48

if.end48:                                         ; preds = %for.end47, %if.end31
  %69 = load i32, i32* %pad_right.addr, align 4, !tbaa !2
  %tobool49 = icmp ne i32 %69, 0
  br i1 %tobool49, label %if.then50, label %if.end64

if.then50:                                        ; preds = %if.end48
  %70 = bitcast i32* %i51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #5
  store i32 0, i32* %i51, align 4, !tbaa !2
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc61, %if.then50
  %71 = load i32, i32* %i51, align 4, !tbaa !2
  %72 = load i32, i32* %height.addr, align 4, !tbaa !2
  %cmp53 = icmp slt i32 %71, %72
  br i1 %cmp53, label %for.body56, label %for.cond.cleanup55

for.cond.cleanup55:                               ; preds = %for.cond52
  %73 = bitcast i32* %i51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #5
  br label %for.end63

for.body56:                                       ; preds = %for.cond52
  %74 = load i8*, i8** %in_tr, align 4, !tbaa !6
  %75 = load i32, i32* %i51, align 4, !tbaa !2
  %76 = load i32, i32* %in_stride.addr, align 4, !tbaa !2
  %mul57 = mul nsw i32 %75, %76
  %add.ptr58 = getelementptr inbounds i8, i8* %74, i32 %mul57
  %77 = load i8*, i8** %tmp_right, align 4, !tbaa !6
  %78 = load i32, i32* %i51, align 4, !tbaa !2
  %mul59 = mul nsw i32 %78, 5
  %add.ptr60 = getelementptr inbounds i8, i8* %77, i32 %mul59
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr58, i8* align 1 %add.ptr60, i32 5, i1 false)
  br label %for.inc61

for.inc61:                                        ; preds = %for.body56
  %79 = load i32, i32* %i51, align 4, !tbaa !2
  %inc62 = add nsw i32 %79, 1
  store i32 %inc62, i32* %i51, align 4, !tbaa !2
  br label %for.cond52

for.end63:                                        ; preds = %for.cond.cleanup55
  %80 = load i8*, i8** %tmp_right, align 4, !tbaa !6
  call void @aom_free(i8* %80)
  br label %if.end64

if.end64:                                         ; preds = %for.end63, %if.end48
  %81 = bitcast i8** %in_tr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #5
  %82 = bitcast i8** %in_tl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #5
  %83 = bitcast i8** %tmp_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #5
  %84 = bitcast i8** %tmp_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #5
  %85 = bitcast i32* %border_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_upscale_normative_and_extend_frame(%struct.AV1Common* %cm, %struct.yv12_buffer_config* %src, %struct.yv12_buffer_config* %dst) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %src.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst.addr = alloca %struct.yv12_buffer_config*, align 4
  %num_planes = alloca i32, align 4
  %i = alloca i32, align 4
  %is_uv = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %src, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %dst, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !2
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !2
  %4 = load i32, i32* %num_planes, align 4, !tbaa !2
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %i, align 4, !tbaa !2
  %cmp1 = icmp sgt i32 %7, 0
  %conv = zext i1 %cmp1 to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !2
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %9 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %10 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %9, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %10 to [3 x i8*]*
  %11 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %11
  %12 = load i8*, i8** %arrayidx, align 4, !tbaa !8
  %13 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %14 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %13, i32 0, i32 4
  %strides = bitcast %union.anon.6* %14 to [2 x i32]*
  %15 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %15
  %16 = load i32, i32* %arrayidx2, align 4, !tbaa !8
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %18 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 5
  %buffers3 = bitcast %union.anon.8* %18 to [3 x i8*]*
  %19 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers3, i32 0, i32 %19
  %20 = load i8*, i8** %arrayidx4, align 4, !tbaa !8
  %21 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %22 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %21, i32 0, i32 4
  %strides5 = bitcast %union.anon.6* %22 to [2 x i32]*
  %23 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds [2 x i32], [2 x i32]* %strides5, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx6, align 4, !tbaa !8
  %25 = load i32, i32* %i, align 4, !tbaa !2
  %26 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %27 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %26, i32 0, i32 3
  %crop_heights = bitcast %union.anon.4* %27 to [2 x i32]*
  %28 = load i32, i32* %is_uv, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights, i32 0, i32 %28
  %29 = load i32, i32* %arrayidx7, align 4, !tbaa !8
  call void @av1_upscale_normative_rows(%struct.AV1Common* %8, i8* %12, i32 %16, i8* %20, i32 %24, i32 %25, i32 %29)
  %30 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %31 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %32 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %33 = load i32, i32* %num_planes, align 4, !tbaa !2
  call void @aom_extend_frame_borders_c(%struct.yv12_buffer_config* %32, i32 %33)
  %34 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_num_planes(%struct.AV1Common* %cm) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %1 = load i8, i8* %monochrome, align 1, !tbaa !46
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 3
  ret i32 %cond
}

; Function Attrs: nounwind
define hidden %struct.yv12_buffer_config* @av1_scale_if_required(%struct.AV1Common* %cm, %struct.yv12_buffer_config* %unscaled, %struct.yv12_buffer_config* %scaled) #0 {
entry:
  %retval = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %unscaled.addr = alloca %struct.yv12_buffer_config*, align 4
  %scaled.addr = alloca %struct.yv12_buffer_config*, align 4
  %num_planes = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %unscaled, %struct.yv12_buffer_config** %unscaled.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %scaled, %struct.yv12_buffer_config** %scaled.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !2
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 2
  %3 = load i32, i32* %width, align 16, !tbaa !37
  %4 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %unscaled.addr, align 4, !tbaa !6
  %5 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %4, i32 0, i32 2
  %6 = bitcast %union.anon.2* %5 to %struct.anon.3*
  %y_crop_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %6, i32 0, i32 0
  %7 = load i32, i32* %y_crop_width, align 4, !tbaa !8
  %cmp = icmp ne i32 %3, %7
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %height = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 3
  %9 = load i32, i32* %height, align 4, !tbaa !47
  %10 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %unscaled.addr, align 4, !tbaa !6
  %11 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %10, i32 0, i32 3
  %12 = bitcast %union.anon.4* %11 to %struct.anon.5*
  %y_crop_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %12, i32 0, i32 0
  %13 = load i32, i32* %y_crop_height, align 4, !tbaa !8
  %cmp1 = icmp ne i32 %9, %13
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %14 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %unscaled.addr, align 4, !tbaa !6
  %15 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %scaled.addr, align 4, !tbaa !6
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %16, i32 0, i32 37
  %bit_depth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 25
  %17 = load i32, i32* %bit_depth, align 8, !tbaa !45
  %18 = load i32, i32* %num_planes, align 4, !tbaa !2
  call void @av1_resize_and_extend_frame(%struct.yv12_buffer_config* %14, %struct.yv12_buffer_config* %15, i32 %17, i32 %18)
  %19 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %scaled.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %19, %struct.yv12_buffer_config** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %lor.lhs.false
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %unscaled.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %20, %struct.yv12_buffer_config** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %21 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  %22 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %retval, align 4
  ret %struct.yv12_buffer_config* %22
}

; Function Attrs: nounwind
define hidden void @av1_calculate_scaled_size(i32* %width, i32* %height, i32 %resize_denom) #0 {
entry:
  %width.addr = alloca i32*, align 4
  %height.addr = alloca i32*, align 4
  %resize_denom.addr = alloca i32, align 4
  store i32* %width, i32** %width.addr, align 4, !tbaa !6
  store i32* %height, i32** %height.addr, align 4, !tbaa !6
  store i32 %resize_denom, i32* %resize_denom.addr, align 4, !tbaa !2
  %0 = load i32*, i32** %width.addr, align 4, !tbaa !6
  %1 = load i32, i32* %resize_denom.addr, align 4, !tbaa !2
  call void @calculate_scaled_size_helper(i32* %0, i32 %1)
  %2 = load i32*, i32** %height.addr, align 4, !tbaa !6
  %3 = load i32, i32* %resize_denom.addr, align 4, !tbaa !2
  call void @calculate_scaled_size_helper(i32* %2, i32 %3)
  ret void
}

; Function Attrs: nounwind
define internal void @calculate_scaled_size_helper(i32* %dim, i32 %denom) #0 {
entry:
  %dim.addr = alloca i32*, align 4
  %denom.addr = alloca i32, align 4
  %min_dim = alloca i32, align 4
  store i32* %dim, i32** %dim.addr, align 4, !tbaa !6
  store i32 %denom, i32* %denom.addr, align 4, !tbaa !2
  %0 = load i32, i32* %denom.addr, align 4, !tbaa !2
  %cmp = icmp ne i32 %0, 8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %min_dim to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i32*, i32** %dim.addr, align 4, !tbaa !6
  %3 = load i32, i32* %2, align 4, !tbaa !2
  %cmp1 = icmp slt i32 16, %3
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %4 = load i32*, i32** %dim.addr, align 4, !tbaa !6
  %5 = load i32, i32* %4, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 16, %cond.true ], [ %5, %cond.false ]
  store i32 %cond, i32* %min_dim, align 4, !tbaa !2
  %6 = load i32*, i32** %dim.addr, align 4, !tbaa !6
  %7 = load i32, i32* %6, align 4, !tbaa !2
  %mul = mul nsw i32 %7, 8
  %8 = load i32, i32* %denom.addr, align 4, !tbaa !2
  %div = sdiv i32 %8, 2
  %add = add nsw i32 %mul, %div
  %9 = load i32, i32* %denom.addr, align 4, !tbaa !2
  %div2 = sdiv i32 %add, %9
  %10 = load i32*, i32** %dim.addr, align 4, !tbaa !6
  store i32 %div2, i32* %10, align 4, !tbaa !2
  %11 = load i32*, i32** %dim.addr, align 4, !tbaa !6
  %12 = load i32, i32* %11, align 4, !tbaa !2
  %13 = load i32, i32* %min_dim, align 4, !tbaa !2
  %cmp3 = icmp sgt i32 %12, %13
  br i1 %cmp3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.end
  %14 = load i32*, i32** %dim.addr, align 4, !tbaa !6
  %15 = load i32, i32* %14, align 4, !tbaa !2
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  %16 = load i32, i32* %min_dim, align 4, !tbaa !2
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi i32 [ %15, %cond.true4 ], [ %16, %cond.false5 ]
  %17 = load i32*, i32** %dim.addr, align 4, !tbaa !6
  store i32 %cond7, i32* %17, align 4, !tbaa !2
  %18 = bitcast i32* %min_dim to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  br label %if.end

if.end:                                           ; preds = %cond.end6, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_calculate_scaled_superres_size(i32* %width, i32* %height, i32 %superres_denom) #0 {
entry:
  %width.addr = alloca i32*, align 4
  %height.addr = alloca i32*, align 4
  %superres_denom.addr = alloca i32, align 4
  store i32* %width, i32** %width.addr, align 4, !tbaa !6
  store i32* %height, i32** %height.addr, align 4, !tbaa !6
  store i32 %superres_denom, i32* %superres_denom.addr, align 4, !tbaa !2
  %0 = load i32*, i32** %height.addr, align 4, !tbaa !6
  %1 = load i32*, i32** %width.addr, align 4, !tbaa !6
  %2 = load i32, i32* %superres_denom.addr, align 4, !tbaa !2
  call void @calculate_scaled_size_helper(i32* %1, i32 %2)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_calculate_unscaled_superres_size(i32* %width, i32* %height, i32 %denom) #0 {
entry:
  %width.addr = alloca i32*, align 4
  %height.addr = alloca i32*, align 4
  %denom.addr = alloca i32, align 4
  store i32* %width, i32** %width.addr, align 4, !tbaa !6
  store i32* %height, i32** %height.addr, align 4, !tbaa !6
  store i32 %denom, i32* %denom.addr, align 4, !tbaa !2
  %0 = load i32, i32* %denom.addr, align 4, !tbaa !2
  %cmp = icmp ne i32 %0, 8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32*, i32** %width.addr, align 4, !tbaa !6
  %2 = load i32, i32* %1, align 4, !tbaa !2
  %3 = load i32, i32* %denom.addr, align 4, !tbaa !2
  %mul = mul nsw i32 %2, %3
  %div = sdiv i32 %mul, 8
  %4 = load i32*, i32** %width.addr, align 4, !tbaa !6
  store i32 %div, i32* %4, align 4, !tbaa !2
  %5 = load i32*, i32** %height.addr, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_superres_upscale(%struct.AV1Common* %cm, %struct.BufferPool* %pool) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %pool.addr = alloca %struct.BufferPool*, align 4
  %num_planes = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %seq_params = alloca %struct.SequenceHeader*, align 4
  %byte_alignment = alloca i32, align 4
  %copy_buffer = alloca %struct.yv12_buffer_config, align 4
  %frame_to_show = alloca %struct.yv12_buffer_config*, align 4
  %aligned_width = alloca i32, align 4
  %fb = alloca %struct.aom_codec_frame_buffer*, align 4
  %release_fb_cb = alloca i32 (i8*, %struct.aom_codec_frame_buffer*)*, align 4
  %cb = alloca i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, align 4
  %cb_priv = alloca i8*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !2
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call1 = call i32 @av1_superres_scaled(%struct.AV1Common* %2)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = bitcast %struct.SequenceHeader** %seq_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 37
  store %struct.SequenceHeader* %seq_params2, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %5 = bitcast i32* %byte_alignment to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %6, i32 0, i32 21
  %byte_alignment3 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 15
  %7 = load i32, i32* %byte_alignment3, align 4, !tbaa !48
  store i32 %7, i32* %byte_alignment, align 4, !tbaa !2
  %8 = bitcast %struct.yv12_buffer_config* %copy_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 148, i8* %8) #5
  %9 = bitcast %struct.yv12_buffer_config* %copy_buffer to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %9, i8 0, i32 148, i1 false)
  %10 = bitcast %struct.yv12_buffer_config** %frame_to_show to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 13
  %12 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !49
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %12, i32 0, i32 17
  store %struct.yv12_buffer_config* %buf, %struct.yv12_buffer_config** %frame_to_show, align 4, !tbaa !6
  %13 = bitcast i32* %aligned_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 2
  %15 = load i32, i32* %width, align 16, !tbaa !37
  %add = add nsw i32 %15, 7
  %and = and i32 %add, -8
  store i32 %and, i32* %aligned_width, align 4, !tbaa !2
  %16 = load i32, i32* %aligned_width, align 4, !tbaa !2
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %height = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 3
  %18 = load i32, i32* %height, align 4, !tbaa !47
  %19 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %19, i32 0, i32 32
  %20 = load i32, i32* %subsampling_x, align 8, !tbaa !50
  %21 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %21, i32 0, i32 33
  %22 = load i32, i32* %subsampling_y, align 4, !tbaa !51
  %23 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %use_highbitdepth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %23, i32 0, i32 26
  %24 = load i8, i8* %use_highbitdepth, align 4, !tbaa !52
  %conv = zext i8 %24 to i32
  %25 = load i32, i32* %byte_alignment, align 4, !tbaa !2
  %call4 = call i32 @aom_alloc_frame_buffer(%struct.yv12_buffer_config* %copy_buffer, i32 %16, i32 %18, i32 %20, i32 %22, i32 %conv, i32 288, i32 %25)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  %26 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %26, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 2, i8* getelementptr inbounds ([54 x i8], [54 x i8]* @.str, i32 0, i32 0))
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %if.end
  %27 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_to_show, align 4, !tbaa !6
  %28 = load i32, i32* %num_planes, align 4, !tbaa !2
  call void @aom_yv12_copy_frame_c(%struct.yv12_buffer_config* %27, %struct.yv12_buffer_config* %copy_buffer, i32 %28)
  %29 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %cmp = icmp ne %struct.BufferPool* %29, null
  br i1 %cmp, label %if.then9, label %if.else

if.then9:                                         ; preds = %if.end7
  %30 = bitcast %struct.aom_codec_frame_buffer** %fb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #5
  %31 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %cur_frame10 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %31, i32 0, i32 13
  %32 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame10, align 8, !tbaa !49
  %raw_frame_buffer = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %32, i32 0, i32 16
  store %struct.aom_codec_frame_buffer* %raw_frame_buffer, %struct.aom_codec_frame_buffer** %fb, align 4, !tbaa !6
  %33 = bitcast i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #5
  %34 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %release_fb_cb11 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %34, i32 0, i32 2
  %35 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb11, align 4, !tbaa !53
  store i32 (i8*, %struct.aom_codec_frame_buffer*)* %35, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb, align 4, !tbaa !6
  %36 = bitcast i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #5
  %37 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %get_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %37, i32 0, i32 1
  %38 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %get_fb_cb, align 4, !tbaa !56
  store i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %38, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb, align 4, !tbaa !6
  %39 = bitcast i8** %cb_priv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #5
  %40 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %cb_priv12 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %40, i32 0, i32 0
  %41 = load i8*, i8** %cb_priv12, align 4, !tbaa !57
  store i8* %41, i8** %cb_priv, align 4, !tbaa !6
  %42 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  call void @lock_buffer_pool(%struct.BufferPool* %42)
  %43 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb, align 4, !tbaa !6
  %44 = load i8*, i8** %cb_priv, align 4, !tbaa !6
  %45 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb, align 4, !tbaa !6
  %call13 = call i32 %43(i8* %44, %struct.aom_codec_frame_buffer* %45)
  %tobool14 = icmp ne i32 %call13, 0
  br i1 %tobool14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %if.then9
  %46 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  call void @unlock_buffer_pool(%struct.BufferPool* %46)
  %47 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %error16 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %47, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error16, i32 2, i8* getelementptr inbounds ([62 x i8], [62 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end17

if.end17:                                         ; preds = %if.then15, %if.then9
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_to_show, align 4, !tbaa !6
  %49 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %49, i32 0, i32 6
  %50 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !38
  %51 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %superres_upscaled_height = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %51, i32 0, i32 7
  %52 = load i32, i32* %superres_upscaled_height, align 4, !tbaa !58
  %53 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %subsampling_x18 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %53, i32 0, i32 32
  %54 = load i32, i32* %subsampling_x18, align 8, !tbaa !50
  %55 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %subsampling_y19 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %55, i32 0, i32 33
  %56 = load i32, i32* %subsampling_y19, align 4, !tbaa !51
  %57 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %use_highbitdepth20 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %57, i32 0, i32 26
  %58 = load i8, i8* %use_highbitdepth20, align 4, !tbaa !52
  %conv21 = zext i8 %58 to i32
  %59 = load i32, i32* %byte_alignment, align 4, !tbaa !2
  %60 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb, align 4, !tbaa !6
  %61 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb, align 4, !tbaa !6
  %62 = load i8*, i8** %cb_priv, align 4, !tbaa !6
  %call22 = call i32 @aom_realloc_frame_buffer(%struct.yv12_buffer_config* %48, i32 %50, i32 %52, i32 %54, i32 %56, i32 %conv21, i32 288, i32 %59, %struct.aom_codec_frame_buffer* %60, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %61, i8* %62)
  %tobool23 = icmp ne i32 %call22, 0
  br i1 %tobool23, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.end17
  %63 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  call void @unlock_buffer_pool(%struct.BufferPool* %63)
  %64 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %error25 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %64, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error25, i32 2, i8* getelementptr inbounds ([63 x i8], [63 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %if.end17
  %65 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  call void @unlock_buffer_pool(%struct.BufferPool* %65)
  %66 = bitcast i8** %cb_priv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = bitcast i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  %69 = bitcast %struct.aom_codec_frame_buffer** %fb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #5
  br label %if.end38

if.else:                                          ; preds = %if.end7
  %70 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_to_show, align 4, !tbaa !6
  call void @copy_buffer_config(%struct.yv12_buffer_config* %70, %struct.yv12_buffer_config* %copy_buffer)
  %71 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_to_show, align 4, !tbaa !6
  %72 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %superres_upscaled_width27 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %72, i32 0, i32 6
  %73 = load i32, i32* %superres_upscaled_width27, align 16, !tbaa !38
  %74 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %superres_upscaled_height28 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %74, i32 0, i32 7
  %75 = load i32, i32* %superres_upscaled_height28, align 4, !tbaa !58
  %76 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %subsampling_x29 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %76, i32 0, i32 32
  %77 = load i32, i32* %subsampling_x29, align 8, !tbaa !50
  %78 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %subsampling_y30 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %78, i32 0, i32 33
  %79 = load i32, i32* %subsampling_y30, align 4, !tbaa !51
  %80 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !6
  %use_highbitdepth31 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %80, i32 0, i32 26
  %81 = load i8, i8* %use_highbitdepth31, align 4, !tbaa !52
  %conv32 = zext i8 %81 to i32
  %82 = load i32, i32* %byte_alignment, align 4, !tbaa !2
  %call33 = call i32 @aom_alloc_frame_buffer(%struct.yv12_buffer_config* %71, i32 %73, i32 %75, i32 %77, i32 %79, i32 %conv32, i32 288, i32 %82)
  %tobool34 = icmp ne i32 %call33, 0
  br i1 %tobool34, label %if.then35, label %if.end37

if.then35:                                        ; preds = %if.else
  %83 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %error36 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %83, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error36, i32 2, i8* getelementptr inbounds ([65 x i8], [65 x i8]* @.str.3, i32 0, i32 0))
  br label %if.end37

if.end37:                                         ; preds = %if.then35, %if.else
  %84 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_to_show, align 4, !tbaa !6
  call void @copy_buffer_config(%struct.yv12_buffer_config* %copy_buffer, %struct.yv12_buffer_config* %84)
  br label %if.end38

if.end38:                                         ; preds = %if.end37, %if.end26
  %85 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %86 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame_to_show, align 4, !tbaa !6
  call void @av1_upscale_normative_and_extend_frame(%struct.AV1Common* %85, %struct.yv12_buffer_config* %copy_buffer, %struct.yv12_buffer_config* %86)
  %call39 = call i32 @aom_free_frame_buffer(%struct.yv12_buffer_config* %copy_buffer)
  %87 = bitcast i32* %aligned_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #5
  %88 = bitcast %struct.yv12_buffer_config** %frame_to_show to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #5
  %89 = bitcast %struct.yv12_buffer_config* %copy_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 148, i8* %89) #5
  %90 = bitcast i32* %byte_alignment to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #5
  %91 = bitcast %struct.SequenceHeader** %seq_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end38, %if.then
  %92 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_superres_scaled(%struct.AV1Common* %cm) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 2
  %1 = load i32, i32* %width, align 16, !tbaa !37
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 6
  %3 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !38
  %cmp = icmp eq i32 %1, %3
  %lnot = xor i1 %cmp, true
  %lnot.ext = zext i1 %lnot to i32
  ret i32 %lnot.ext
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

declare i32 @aom_alloc_frame_buffer(%struct.yv12_buffer_config*, i32, i32, i32, i32, i32, i32, i32) #2

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #2

declare void @aom_yv12_copy_frame_c(%struct.yv12_buffer_config*, %struct.yv12_buffer_config*, i32) #2

; Function Attrs: nounwind
define internal void @lock_buffer_pool(%struct.BufferPool* %pool) #0 {
entry:
  %pool.addr = alloca %struct.BufferPool*, align 4
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %0 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define internal void @unlock_buffer_pool(%struct.BufferPool* %pool) #0 {
entry:
  %pool.addr = alloca %struct.BufferPool*, align 4
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %0 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  ret void
}

declare i32 @aom_realloc_frame_buffer(%struct.yv12_buffer_config*, i32, i32, i32, i32, i32, i32, i32, %struct.aom_codec_frame_buffer*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i8*) #2

; Function Attrs: nounwind
define internal void @copy_buffer_config(%struct.yv12_buffer_config* %src, %struct.yv12_buffer_config* %dst) #0 {
entry:
  %src.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst.addr = alloca %struct.yv12_buffer_config*, align 4
  store %struct.yv12_buffer_config* %src, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %dst, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %bit_depth = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %0, i32 0, i32 16
  %1 = load i32, i32* %bit_depth, align 4, !tbaa !59
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %bit_depth1 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 16
  store i32 %1, i32* %bit_depth1, align 4, !tbaa !59
  %3 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %color_primaries = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %3, i32 0, i32 17
  %4 = load i32, i32* %color_primaries, align 4, !tbaa !60
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %color_primaries2 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %5, i32 0, i32 17
  store i32 %4, i32* %color_primaries2, align 4, !tbaa !60
  %6 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %transfer_characteristics = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %6, i32 0, i32 18
  %7 = load i32, i32* %transfer_characteristics, align 4, !tbaa !61
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %transfer_characteristics3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %8, i32 0, i32 18
  store i32 %7, i32* %transfer_characteristics3, align 4, !tbaa !61
  %9 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %matrix_coefficients = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %9, i32 0, i32 19
  %10 = load i32, i32* %matrix_coefficients, align 4, !tbaa !62
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %matrix_coefficients4 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 19
  store i32 %10, i32* %matrix_coefficients4, align 4, !tbaa !62
  %12 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %monochrome = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %12, i32 0, i32 20
  %13 = load i8, i8* %monochrome, align 4, !tbaa !63
  %14 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %monochrome5 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %14, i32 0, i32 20
  store i8 %13, i8* %monochrome5, align 4, !tbaa !63
  %15 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %chroma_sample_position = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %15, i32 0, i32 21
  %16 = load i32, i32* %chroma_sample_position, align 4, !tbaa !64
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %chroma_sample_position6 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 21
  store i32 %16, i32* %chroma_sample_position6, align 4, !tbaa !64
  %18 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !6
  %color_range = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %18, i32 0, i32 22
  %19 = load i32, i32* %color_range, align 4, !tbaa !65
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !6
  %color_range7 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %20, i32 0, i32 22
  store i32 %19, i32* %color_range7, align 4, !tbaa !65
  ret void
}

declare i32 @aom_free_frame_buffer(%struct.yv12_buffer_config*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal i32 @get_down2_steps(i32 %in_length, i32 %out_length) #0 {
entry:
  %in_length.addr = alloca i32, align 4
  %out_length.addr = alloca i32, align 4
  %steps = alloca i32, align 4
  %proj_in_length = alloca i32, align 4
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  %0 = bitcast i32* %steps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %steps, align 4, !tbaa !2
  %1 = bitcast i32* %proj_in_length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %2 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %call = call i32 @get_down2_length(i32 %2, i32 1)
  store i32 %call, i32* %proj_in_length, align 4, !tbaa !2
  %3 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp = icmp sge i32 %call, %3
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i32, i32* %steps, align 4, !tbaa !2
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %steps, align 4, !tbaa !2
  %5 = load i32, i32* %proj_in_length, align 4, !tbaa !2
  store i32 %5, i32* %in_length.addr, align 4, !tbaa !2
  %6 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %cmp1 = icmp eq i32 %6, 1
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  br label %while.end

if.end:                                           ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %if.then, %while.cond
  %7 = load i32, i32* %steps, align 4, !tbaa !2
  %8 = bitcast i32* %proj_in_length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #5
  %9 = bitcast i32* %steps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #5
  ret i32 %7
}

; Function Attrs: nounwind
define internal i32 @get_down2_length(i32 %length, i32 %steps) #0 {
entry:
  %length.addr = alloca i32, align 4
  %steps.addr = alloca i32, align 4
  %s = alloca i32, align 4
  store i32 %length, i32* %length.addr, align 4, !tbaa !2
  store i32 %steps, i32* %steps.addr, align 4, !tbaa !2
  %0 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %s, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %s, align 4, !tbaa !2
  %2 = load i32, i32* %steps.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %length.addr, align 4, !tbaa !2
  %add = add nsw i32 %4, 1
  %shr = ashr i32 %add, 1
  store i32 %shr, i32* %length.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %s, align 4, !tbaa !2
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %s, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %6 = load i32, i32* %length.addr, align 4, !tbaa !2
  ret i32 %6
}

; Function Attrs: nounwind
define internal void @down2_symodd(i8* %input, i32 %length, i8* %output) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %length.addr = alloca i32, align 4
  %output.addr = alloca i8*, align 4
  %filter = alloca i16*, align 4
  %filter_len_half = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %optr = alloca i8*, align 4
  %l1 = alloca i32, align 4
  %l2 = alloca i32, align 4
  %sum = alloca i32, align 4
  %sum41 = alloca i32, align 4
  %sum83 = alloca i32, align 4
  %sum118 = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !6
  store i32 %length, i32* %length.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !6
  %0 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i16* getelementptr inbounds ([4 x i16], [4 x i16]* @av1_down2_symodd_half_filter, i32 0, i32 0), i16** %filter, align 4, !tbaa !6
  %1 = bitcast i32* %filter_len_half to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 4, i32* %filter_len_half, align 4, !tbaa !2
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i8** %optr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i8*, i8** %output.addr, align 4, !tbaa !6
  store i8* %5, i8** %optr, align 4, !tbaa !6
  %6 = bitcast i32* %l1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  store i32 3, i32* %l1, align 4, !tbaa !2
  %7 = bitcast i32* %l2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %8, 4
  %add = add nsw i32 %sub, 1
  store i32 %add, i32* %l2, align 4, !tbaa !2
  %9 = load i32, i32* %l1, align 4, !tbaa !2
  %and = and i32 %9, 1
  %10 = load i32, i32* %l1, align 4, !tbaa !2
  %add1 = add nsw i32 %10, %and
  store i32 %add1, i32* %l1, align 4, !tbaa !2
  %11 = load i32, i32* %l2, align 4, !tbaa !2
  %and2 = and i32 %11, 1
  %12 = load i32, i32* %l2, align 4, !tbaa !2
  %add3 = add nsw i32 %12, %and2
  store i32 %add3, i32* %l2, align 4, !tbaa !2
  %13 = load i32, i32* %l1, align 4, !tbaa !2
  %14 = load i32, i32* %l2, align 4, !tbaa !2
  %cmp = icmp sgt i32 %13, %14
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc34, %if.then
  %15 = load i32, i32* %i, align 4, !tbaa !2
  %16 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp4 = icmp slt i32 %15, %16
  br i1 %cmp4, label %for.body, label %for.end36

for.body:                                         ; preds = %for.cond
  %17 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %19 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %18, i32 %19
  %20 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %20 to i32
  %21 = load i16*, i16** %filter, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i16, i16* %21, i32 0
  %22 = load i16, i16* %arrayidx5, align 2, !tbaa !11
  %conv6 = sext i16 %22 to i32
  %mul = mul nsw i32 %conv, %conv6
  %add7 = add nsw i32 64, %mul
  store i32 %add7, i32* %sum, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !2
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %23 = load i32, i32* %j, align 4, !tbaa !2
  %cmp9 = icmp slt i32 %23, 4
  br i1 %cmp9, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond8
  %24 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %25 = load i32, i32* %i, align 4, !tbaa !2
  %26 = load i32, i32* %j, align 4, !tbaa !2
  %sub12 = sub nsw i32 %25, %26
  %cmp13 = icmp slt i32 %sub12, 0
  br i1 %cmp13, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body11
  br label %cond.end

cond.false:                                       ; preds = %for.body11
  %27 = load i32, i32* %i, align 4, !tbaa !2
  %28 = load i32, i32* %j, align 4, !tbaa !2
  %sub15 = sub nsw i32 %27, %28
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %sub15, %cond.false ]
  %arrayidx16 = getelementptr inbounds i8, i8* %24, i32 %cond
  %29 = load i8, i8* %arrayidx16, align 1, !tbaa !8
  %conv17 = zext i8 %29 to i32
  %30 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %31 = load i32, i32* %i, align 4, !tbaa !2
  %32 = load i32, i32* %j, align 4, !tbaa !2
  %add18 = add nsw i32 %31, %32
  %33 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp19 = icmp sge i32 %add18, %33
  br i1 %cmp19, label %cond.true21, label %cond.false23

cond.true21:                                      ; preds = %cond.end
  %34 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub22 = sub nsw i32 %34, 1
  br label %cond.end25

cond.false23:                                     ; preds = %cond.end
  %35 = load i32, i32* %i, align 4, !tbaa !2
  %36 = load i32, i32* %j, align 4, !tbaa !2
  %add24 = add nsw i32 %35, %36
  br label %cond.end25

cond.end25:                                       ; preds = %cond.false23, %cond.true21
  %cond26 = phi i32 [ %sub22, %cond.true21 ], [ %add24, %cond.false23 ]
  %arrayidx27 = getelementptr inbounds i8, i8* %30, i32 %cond26
  %37 = load i8, i8* %arrayidx27, align 1, !tbaa !8
  %conv28 = zext i8 %37 to i32
  %add29 = add nsw i32 %conv17, %conv28
  %38 = load i16*, i16** %filter, align 4, !tbaa !6
  %39 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i16, i16* %38, i32 %39
  %40 = load i16, i16* %arrayidx30, align 2, !tbaa !11
  %conv31 = sext i16 %40 to i32
  %mul32 = mul nsw i32 %add29, %conv31
  %41 = load i32, i32* %sum, align 4, !tbaa !2
  %add33 = add nsw i32 %41, %mul32
  store i32 %add33, i32* %sum, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %cond.end25
  %42 = load i32, i32* %j, align 4, !tbaa !2
  %inc = add nsw i32 %42, 1
  store i32 %inc, i32* %j, align 4, !tbaa !2
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %43 = load i32, i32* %sum, align 4, !tbaa !2
  %shr = ashr i32 %43, 7
  store i32 %shr, i32* %sum, align 4, !tbaa !2
  %44 = load i32, i32* %sum, align 4, !tbaa !2
  %call = call zeroext i8 @clip_pixel(i32 %44)
  %45 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i8, i8* %45, i32 1
  store i8* %incdec.ptr, i8** %optr, align 4, !tbaa !6
  store i8 %call, i8* %45, align 1, !tbaa !8
  %46 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  br label %for.inc34

for.inc34:                                        ; preds = %for.end
  %47 = load i32, i32* %i, align 4, !tbaa !2
  %add35 = add nsw i32 %47, 2
  store i32 %add35, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end36:                                        ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc76, %if.else
  %48 = load i32, i32* %i, align 4, !tbaa !2
  %49 = load i32, i32* %l1, align 4, !tbaa !2
  %cmp38 = icmp slt i32 %48, %49
  br i1 %cmp38, label %for.body40, label %for.end78

for.body40:                                       ; preds = %for.cond37
  %50 = bitcast i32* %sum41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #5
  %51 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %52 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i8, i8* %arrayidx42, align 1, !tbaa !8
  %conv43 = zext i8 %53 to i32
  %54 = load i16*, i16** %filter, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds i16, i16* %54, i32 0
  %55 = load i16, i16* %arrayidx44, align 2, !tbaa !11
  %conv45 = sext i16 %55 to i32
  %mul46 = mul nsw i32 %conv43, %conv45
  %add47 = add nsw i32 64, %mul46
  store i32 %add47, i32* %sum41, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !2
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc70, %for.body40
  %56 = load i32, i32* %j, align 4, !tbaa !2
  %cmp49 = icmp slt i32 %56, 4
  br i1 %cmp49, label %for.body51, label %for.end72

for.body51:                                       ; preds = %for.cond48
  %57 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %58 = load i32, i32* %i, align 4, !tbaa !2
  %59 = load i32, i32* %j, align 4, !tbaa !2
  %sub52 = sub nsw i32 %58, %59
  %cmp53 = icmp slt i32 %sub52, 0
  br i1 %cmp53, label %cond.true55, label %cond.false56

cond.true55:                                      ; preds = %for.body51
  br label %cond.end58

cond.false56:                                     ; preds = %for.body51
  %60 = load i32, i32* %i, align 4, !tbaa !2
  %61 = load i32, i32* %j, align 4, !tbaa !2
  %sub57 = sub nsw i32 %60, %61
  br label %cond.end58

cond.end58:                                       ; preds = %cond.false56, %cond.true55
  %cond59 = phi i32 [ 0, %cond.true55 ], [ %sub57, %cond.false56 ]
  %arrayidx60 = getelementptr inbounds i8, i8* %57, i32 %cond59
  %62 = load i8, i8* %arrayidx60, align 1, !tbaa !8
  %conv61 = zext i8 %62 to i32
  %63 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %64 = load i32, i32* %i, align 4, !tbaa !2
  %65 = load i32, i32* %j, align 4, !tbaa !2
  %add62 = add nsw i32 %64, %65
  %arrayidx63 = getelementptr inbounds i8, i8* %63, i32 %add62
  %66 = load i8, i8* %arrayidx63, align 1, !tbaa !8
  %conv64 = zext i8 %66 to i32
  %add65 = add nsw i32 %conv61, %conv64
  %67 = load i16*, i16** %filter, align 4, !tbaa !6
  %68 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i16, i16* %67, i32 %68
  %69 = load i16, i16* %arrayidx66, align 2, !tbaa !11
  %conv67 = sext i16 %69 to i32
  %mul68 = mul nsw i32 %add65, %conv67
  %70 = load i32, i32* %sum41, align 4, !tbaa !2
  %add69 = add nsw i32 %70, %mul68
  store i32 %add69, i32* %sum41, align 4, !tbaa !2
  br label %for.inc70

for.inc70:                                        ; preds = %cond.end58
  %71 = load i32, i32* %j, align 4, !tbaa !2
  %inc71 = add nsw i32 %71, 1
  store i32 %inc71, i32* %j, align 4, !tbaa !2
  br label %for.cond48

for.end72:                                        ; preds = %for.cond48
  %72 = load i32, i32* %sum41, align 4, !tbaa !2
  %shr73 = ashr i32 %72, 7
  store i32 %shr73, i32* %sum41, align 4, !tbaa !2
  %73 = load i32, i32* %sum41, align 4, !tbaa !2
  %call74 = call zeroext i8 @clip_pixel(i32 %73)
  %74 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr75 = getelementptr inbounds i8, i8* %74, i32 1
  store i8* %incdec.ptr75, i8** %optr, align 4, !tbaa !6
  store i8 %call74, i8* %74, align 1, !tbaa !8
  %75 = bitcast i32* %sum41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #5
  br label %for.inc76

for.inc76:                                        ; preds = %for.end72
  %76 = load i32, i32* %i, align 4, !tbaa !2
  %add77 = add nsw i32 %76, 2
  store i32 %add77, i32* %i, align 4, !tbaa !2
  br label %for.cond37

for.end78:                                        ; preds = %for.cond37
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc111, %for.end78
  %77 = load i32, i32* %i, align 4, !tbaa !2
  %78 = load i32, i32* %l2, align 4, !tbaa !2
  %cmp80 = icmp slt i32 %77, %78
  br i1 %cmp80, label %for.body82, label %for.end113

for.body82:                                       ; preds = %for.cond79
  %79 = bitcast i32* %sum83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #5
  %80 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %81 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i8, i8* %80, i32 %81
  %82 = load i8, i8* %arrayidx84, align 1, !tbaa !8
  %conv85 = zext i8 %82 to i32
  %83 = load i16*, i16** %filter, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds i16, i16* %83, i32 0
  %84 = load i16, i16* %arrayidx86, align 2, !tbaa !11
  %conv87 = sext i16 %84 to i32
  %mul88 = mul nsw i32 %conv85, %conv87
  %add89 = add nsw i32 64, %mul88
  store i32 %add89, i32* %sum83, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !2
  br label %for.cond90

for.cond90:                                       ; preds = %for.inc105, %for.body82
  %85 = load i32, i32* %j, align 4, !tbaa !2
  %cmp91 = icmp slt i32 %85, 4
  br i1 %cmp91, label %for.body93, label %for.end107

for.body93:                                       ; preds = %for.cond90
  %86 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %87 = load i32, i32* %i, align 4, !tbaa !2
  %88 = load i32, i32* %j, align 4, !tbaa !2
  %sub94 = sub nsw i32 %87, %88
  %arrayidx95 = getelementptr inbounds i8, i8* %86, i32 %sub94
  %89 = load i8, i8* %arrayidx95, align 1, !tbaa !8
  %conv96 = zext i8 %89 to i32
  %90 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %91 = load i32, i32* %i, align 4, !tbaa !2
  %92 = load i32, i32* %j, align 4, !tbaa !2
  %add97 = add nsw i32 %91, %92
  %arrayidx98 = getelementptr inbounds i8, i8* %90, i32 %add97
  %93 = load i8, i8* %arrayidx98, align 1, !tbaa !8
  %conv99 = zext i8 %93 to i32
  %add100 = add nsw i32 %conv96, %conv99
  %94 = load i16*, i16** %filter, align 4, !tbaa !6
  %95 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i16, i16* %94, i32 %95
  %96 = load i16, i16* %arrayidx101, align 2, !tbaa !11
  %conv102 = sext i16 %96 to i32
  %mul103 = mul nsw i32 %add100, %conv102
  %97 = load i32, i32* %sum83, align 4, !tbaa !2
  %add104 = add nsw i32 %97, %mul103
  store i32 %add104, i32* %sum83, align 4, !tbaa !2
  br label %for.inc105

for.inc105:                                       ; preds = %for.body93
  %98 = load i32, i32* %j, align 4, !tbaa !2
  %inc106 = add nsw i32 %98, 1
  store i32 %inc106, i32* %j, align 4, !tbaa !2
  br label %for.cond90

for.end107:                                       ; preds = %for.cond90
  %99 = load i32, i32* %sum83, align 4, !tbaa !2
  %shr108 = ashr i32 %99, 7
  store i32 %shr108, i32* %sum83, align 4, !tbaa !2
  %100 = load i32, i32* %sum83, align 4, !tbaa !2
  %call109 = call zeroext i8 @clip_pixel(i32 %100)
  %101 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr110 = getelementptr inbounds i8, i8* %101, i32 1
  store i8* %incdec.ptr110, i8** %optr, align 4, !tbaa !6
  store i8 %call109, i8* %101, align 1, !tbaa !8
  %102 = bitcast i32* %sum83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #5
  br label %for.inc111

for.inc111:                                       ; preds = %for.end107
  %103 = load i32, i32* %i, align 4, !tbaa !2
  %add112 = add nsw i32 %103, 2
  store i32 %add112, i32* %i, align 4, !tbaa !2
  br label %for.cond79

for.end113:                                       ; preds = %for.cond79
  br label %for.cond114

for.cond114:                                      ; preds = %for.inc154, %for.end113
  %104 = load i32, i32* %i, align 4, !tbaa !2
  %105 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp115 = icmp slt i32 %104, %105
  br i1 %cmp115, label %for.body117, label %for.end156

for.body117:                                      ; preds = %for.cond114
  %106 = bitcast i32* %sum118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #5
  %107 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %108 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds i8, i8* %107, i32 %108
  %109 = load i8, i8* %arrayidx119, align 1, !tbaa !8
  %conv120 = zext i8 %109 to i32
  %110 = load i16*, i16** %filter, align 4, !tbaa !6
  %arrayidx121 = getelementptr inbounds i16, i16* %110, i32 0
  %111 = load i16, i16* %arrayidx121, align 2, !tbaa !11
  %conv122 = sext i16 %111 to i32
  %mul123 = mul nsw i32 %conv120, %conv122
  %add124 = add nsw i32 64, %mul123
  store i32 %add124, i32* %sum118, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !2
  br label %for.cond125

for.cond125:                                      ; preds = %for.inc148, %for.body117
  %112 = load i32, i32* %j, align 4, !tbaa !2
  %cmp126 = icmp slt i32 %112, 4
  br i1 %cmp126, label %for.body128, label %for.end150

for.body128:                                      ; preds = %for.cond125
  %113 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %114 = load i32, i32* %i, align 4, !tbaa !2
  %115 = load i32, i32* %j, align 4, !tbaa !2
  %sub129 = sub nsw i32 %114, %115
  %arrayidx130 = getelementptr inbounds i8, i8* %113, i32 %sub129
  %116 = load i8, i8* %arrayidx130, align 1, !tbaa !8
  %conv131 = zext i8 %116 to i32
  %117 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %118 = load i32, i32* %i, align 4, !tbaa !2
  %119 = load i32, i32* %j, align 4, !tbaa !2
  %add132 = add nsw i32 %118, %119
  %120 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp133 = icmp sge i32 %add132, %120
  br i1 %cmp133, label %cond.true135, label %cond.false137

cond.true135:                                     ; preds = %for.body128
  %121 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub136 = sub nsw i32 %121, 1
  br label %cond.end139

cond.false137:                                    ; preds = %for.body128
  %122 = load i32, i32* %i, align 4, !tbaa !2
  %123 = load i32, i32* %j, align 4, !tbaa !2
  %add138 = add nsw i32 %122, %123
  br label %cond.end139

cond.end139:                                      ; preds = %cond.false137, %cond.true135
  %cond140 = phi i32 [ %sub136, %cond.true135 ], [ %add138, %cond.false137 ]
  %arrayidx141 = getelementptr inbounds i8, i8* %117, i32 %cond140
  %124 = load i8, i8* %arrayidx141, align 1, !tbaa !8
  %conv142 = zext i8 %124 to i32
  %add143 = add nsw i32 %conv131, %conv142
  %125 = load i16*, i16** %filter, align 4, !tbaa !6
  %126 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds i16, i16* %125, i32 %126
  %127 = load i16, i16* %arrayidx144, align 2, !tbaa !11
  %conv145 = sext i16 %127 to i32
  %mul146 = mul nsw i32 %add143, %conv145
  %128 = load i32, i32* %sum118, align 4, !tbaa !2
  %add147 = add nsw i32 %128, %mul146
  store i32 %add147, i32* %sum118, align 4, !tbaa !2
  br label %for.inc148

for.inc148:                                       ; preds = %cond.end139
  %129 = load i32, i32* %j, align 4, !tbaa !2
  %inc149 = add nsw i32 %129, 1
  store i32 %inc149, i32* %j, align 4, !tbaa !2
  br label %for.cond125

for.end150:                                       ; preds = %for.cond125
  %130 = load i32, i32* %sum118, align 4, !tbaa !2
  %shr151 = ashr i32 %130, 7
  store i32 %shr151, i32* %sum118, align 4, !tbaa !2
  %131 = load i32, i32* %sum118, align 4, !tbaa !2
  %call152 = call zeroext i8 @clip_pixel(i32 %131)
  %132 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr153 = getelementptr inbounds i8, i8* %132, i32 1
  store i8* %incdec.ptr153, i8** %optr, align 4, !tbaa !6
  store i8 %call152, i8* %132, align 1, !tbaa !8
  %133 = bitcast i32* %sum118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #5
  br label %for.inc154

for.inc154:                                       ; preds = %for.end150
  %134 = load i32, i32* %i, align 4, !tbaa !2
  %add155 = add nsw i32 %134, 2
  store i32 %add155, i32* %i, align 4, !tbaa !2
  br label %for.cond114

for.end156:                                       ; preds = %for.cond114
  br label %if.end

if.end:                                           ; preds = %for.end156, %for.end36
  %135 = bitcast i32* %l2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #5
  %136 = bitcast i32* %l1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #5
  %137 = bitcast i8** %optr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #5
  %138 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #5
  %139 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #5
  %140 = bitcast i32* %filter_len_half to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #5
  %141 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #5
  ret void
}

; Function Attrs: nounwind
define internal void @down2_symeven(i8* %input, i32 %length, i8* %output) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %length.addr = alloca i32, align 4
  %output.addr = alloca i8*, align 4
  %filter = alloca i16*, align 4
  %filter_len_half = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %optr = alloca i8*, align 4
  %l1 = alloca i32, align 4
  %l2 = alloca i32, align 4
  %sum = alloca i32, align 4
  %sum35 = alloca i32, align 4
  %sum72 = alloca i32, align 4
  %sum102 = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !6
  store i32 %length, i32* %length.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !6
  %0 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i16* getelementptr inbounds ([4 x i16], [4 x i16]* @av1_down2_symeven_half_filter, i32 0, i32 0), i16** %filter, align 4, !tbaa !6
  %1 = bitcast i32* %filter_len_half to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 4, i32* %filter_len_half, align 4, !tbaa !2
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i8** %optr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i8*, i8** %output.addr, align 4, !tbaa !6
  store i8* %5, i8** %optr, align 4, !tbaa !6
  %6 = bitcast i32* %l1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  store i32 4, i32* %l1, align 4, !tbaa !2
  %7 = bitcast i32* %l2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %8, 4
  store i32 %sub, i32* %l2, align 4, !tbaa !2
  %9 = load i32, i32* %l1, align 4, !tbaa !2
  %and = and i32 %9, 1
  %10 = load i32, i32* %l1, align 4, !tbaa !2
  %add = add nsw i32 %10, %and
  store i32 %add, i32* %l1, align 4, !tbaa !2
  %11 = load i32, i32* %l2, align 4, !tbaa !2
  %and1 = and i32 %11, 1
  %12 = load i32, i32* %l2, align 4, !tbaa !2
  %add2 = add nsw i32 %12, %and1
  store i32 %add2, i32* %l2, align 4, !tbaa !2
  %13 = load i32, i32* %l1, align 4, !tbaa !2
  %14 = load i32, i32* %l2, align 4, !tbaa !2
  %cmp = icmp sgt i32 %13, %14
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %if.then
  %15 = load i32, i32* %i, align 4, !tbaa !2
  %16 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp3 = icmp slt i32 %15, %16
  br i1 %cmp3, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %17 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  store i32 64, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %j, align 4, !tbaa !2
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %18 = load i32, i32* %j, align 4, !tbaa !2
  %cmp5 = icmp slt i32 %18, 4
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %19 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %20 = load i32, i32* %i, align 4, !tbaa !2
  %21 = load i32, i32* %j, align 4, !tbaa !2
  %sub7 = sub nsw i32 %20, %21
  %cmp8 = icmp sgt i32 %sub7, 0
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body6
  %22 = load i32, i32* %i, align 4, !tbaa !2
  %23 = load i32, i32* %j, align 4, !tbaa !2
  %sub9 = sub nsw i32 %22, %23
  br label %cond.end

cond.false:                                       ; preds = %for.body6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub9, %cond.true ], [ 0, %cond.false ]
  %arrayidx = getelementptr inbounds i8, i8* %19, i32 %cond
  %24 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %24 to i32
  %25 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %26 = load i32, i32* %i, align 4, !tbaa !2
  %add10 = add nsw i32 %26, 1
  %27 = load i32, i32* %j, align 4, !tbaa !2
  %add11 = add nsw i32 %add10, %27
  %28 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub12 = sub nsw i32 %28, 1
  %cmp13 = icmp slt i32 %add11, %sub12
  br i1 %cmp13, label %cond.true15, label %cond.false18

cond.true15:                                      ; preds = %cond.end
  %29 = load i32, i32* %i, align 4, !tbaa !2
  %add16 = add nsw i32 %29, 1
  %30 = load i32, i32* %j, align 4, !tbaa !2
  %add17 = add nsw i32 %add16, %30
  br label %cond.end20

cond.false18:                                     ; preds = %cond.end
  %31 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub19 = sub nsw i32 %31, 1
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false18, %cond.true15
  %cond21 = phi i32 [ %add17, %cond.true15 ], [ %sub19, %cond.false18 ]
  %arrayidx22 = getelementptr inbounds i8, i8* %25, i32 %cond21
  %32 = load i8, i8* %arrayidx22, align 1, !tbaa !8
  %conv23 = zext i8 %32 to i32
  %add24 = add nsw i32 %conv, %conv23
  %33 = load i16*, i16** %filter, align 4, !tbaa !6
  %34 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i16, i16* %33, i32 %34
  %35 = load i16, i16* %arrayidx25, align 2, !tbaa !11
  %conv26 = sext i16 %35 to i32
  %mul = mul nsw i32 %add24, %conv26
  %36 = load i32, i32* %sum, align 4, !tbaa !2
  %add27 = add nsw i32 %36, %mul
  store i32 %add27, i32* %sum, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %cond.end20
  %37 = load i32, i32* %j, align 4, !tbaa !2
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %j, align 4, !tbaa !2
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %38 = load i32, i32* %sum, align 4, !tbaa !2
  %shr = ashr i32 %38, 7
  store i32 %shr, i32* %sum, align 4, !tbaa !2
  %39 = load i32, i32* %sum, align 4, !tbaa !2
  %call = call zeroext i8 @clip_pixel(i32 %39)
  %40 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i8, i8* %40, i32 1
  store i8* %incdec.ptr, i8** %optr, align 4, !tbaa !6
  store i8 %call, i8* %40, align 1, !tbaa !8
  %41 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #5
  br label %for.inc28

for.inc28:                                        ; preds = %for.end
  %42 = load i32, i32* %i, align 4, !tbaa !2
  %add29 = add nsw i32 %42, 2
  store i32 %add29, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end30:                                        ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc65, %if.else
  %43 = load i32, i32* %i, align 4, !tbaa !2
  %44 = load i32, i32* %l1, align 4, !tbaa !2
  %cmp32 = icmp slt i32 %43, %44
  br i1 %cmp32, label %for.body34, label %for.end67

for.body34:                                       ; preds = %for.cond31
  %45 = bitcast i32* %sum35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #5
  store i32 64, i32* %sum35, align 4, !tbaa !2
  store i32 0, i32* %j, align 4, !tbaa !2
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc59, %for.body34
  %46 = load i32, i32* %j, align 4, !tbaa !2
  %cmp37 = icmp slt i32 %46, 4
  br i1 %cmp37, label %for.body39, label %for.end61

for.body39:                                       ; preds = %for.cond36
  %47 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %48 = load i32, i32* %i, align 4, !tbaa !2
  %49 = load i32, i32* %j, align 4, !tbaa !2
  %sub40 = sub nsw i32 %48, %49
  %cmp41 = icmp sgt i32 %sub40, 0
  br i1 %cmp41, label %cond.true43, label %cond.false45

cond.true43:                                      ; preds = %for.body39
  %50 = load i32, i32* %i, align 4, !tbaa !2
  %51 = load i32, i32* %j, align 4, !tbaa !2
  %sub44 = sub nsw i32 %50, %51
  br label %cond.end46

cond.false45:                                     ; preds = %for.body39
  br label %cond.end46

cond.end46:                                       ; preds = %cond.false45, %cond.true43
  %cond47 = phi i32 [ %sub44, %cond.true43 ], [ 0, %cond.false45 ]
  %arrayidx48 = getelementptr inbounds i8, i8* %47, i32 %cond47
  %52 = load i8, i8* %arrayidx48, align 1, !tbaa !8
  %conv49 = zext i8 %52 to i32
  %53 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %54 = load i32, i32* %i, align 4, !tbaa !2
  %add50 = add nsw i32 %54, 1
  %55 = load i32, i32* %j, align 4, !tbaa !2
  %add51 = add nsw i32 %add50, %55
  %arrayidx52 = getelementptr inbounds i8, i8* %53, i32 %add51
  %56 = load i8, i8* %arrayidx52, align 1, !tbaa !8
  %conv53 = zext i8 %56 to i32
  %add54 = add nsw i32 %conv49, %conv53
  %57 = load i16*, i16** %filter, align 4, !tbaa !6
  %58 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i16, i16* %57, i32 %58
  %59 = load i16, i16* %arrayidx55, align 2, !tbaa !11
  %conv56 = sext i16 %59 to i32
  %mul57 = mul nsw i32 %add54, %conv56
  %60 = load i32, i32* %sum35, align 4, !tbaa !2
  %add58 = add nsw i32 %60, %mul57
  store i32 %add58, i32* %sum35, align 4, !tbaa !2
  br label %for.inc59

for.inc59:                                        ; preds = %cond.end46
  %61 = load i32, i32* %j, align 4, !tbaa !2
  %inc60 = add nsw i32 %61, 1
  store i32 %inc60, i32* %j, align 4, !tbaa !2
  br label %for.cond36

for.end61:                                        ; preds = %for.cond36
  %62 = load i32, i32* %sum35, align 4, !tbaa !2
  %shr62 = ashr i32 %62, 7
  store i32 %shr62, i32* %sum35, align 4, !tbaa !2
  %63 = load i32, i32* %sum35, align 4, !tbaa !2
  %call63 = call zeroext i8 @clip_pixel(i32 %63)
  %64 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr64 = getelementptr inbounds i8, i8* %64, i32 1
  store i8* %incdec.ptr64, i8** %optr, align 4, !tbaa !6
  store i8 %call63, i8* %64, align 1, !tbaa !8
  %65 = bitcast i32* %sum35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  br label %for.inc65

for.inc65:                                        ; preds = %for.end61
  %66 = load i32, i32* %i, align 4, !tbaa !2
  %add66 = add nsw i32 %66, 2
  store i32 %add66, i32* %i, align 4, !tbaa !2
  br label %for.cond31

for.end67:                                        ; preds = %for.cond31
  br label %for.cond68

for.cond68:                                       ; preds = %for.inc95, %for.end67
  %67 = load i32, i32* %i, align 4, !tbaa !2
  %68 = load i32, i32* %l2, align 4, !tbaa !2
  %cmp69 = icmp slt i32 %67, %68
  br i1 %cmp69, label %for.body71, label %for.end97

for.body71:                                       ; preds = %for.cond68
  %69 = bitcast i32* %sum72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #5
  store i32 64, i32* %sum72, align 4, !tbaa !2
  store i32 0, i32* %j, align 4, !tbaa !2
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc89, %for.body71
  %70 = load i32, i32* %j, align 4, !tbaa !2
  %cmp74 = icmp slt i32 %70, 4
  br i1 %cmp74, label %for.body76, label %for.end91

for.body76:                                       ; preds = %for.cond73
  %71 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %72 = load i32, i32* %i, align 4, !tbaa !2
  %73 = load i32, i32* %j, align 4, !tbaa !2
  %sub77 = sub nsw i32 %72, %73
  %arrayidx78 = getelementptr inbounds i8, i8* %71, i32 %sub77
  %74 = load i8, i8* %arrayidx78, align 1, !tbaa !8
  %conv79 = zext i8 %74 to i32
  %75 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %76 = load i32, i32* %i, align 4, !tbaa !2
  %add80 = add nsw i32 %76, 1
  %77 = load i32, i32* %j, align 4, !tbaa !2
  %add81 = add nsw i32 %add80, %77
  %arrayidx82 = getelementptr inbounds i8, i8* %75, i32 %add81
  %78 = load i8, i8* %arrayidx82, align 1, !tbaa !8
  %conv83 = zext i8 %78 to i32
  %add84 = add nsw i32 %conv79, %conv83
  %79 = load i16*, i16** %filter, align 4, !tbaa !6
  %80 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i16, i16* %79, i32 %80
  %81 = load i16, i16* %arrayidx85, align 2, !tbaa !11
  %conv86 = sext i16 %81 to i32
  %mul87 = mul nsw i32 %add84, %conv86
  %82 = load i32, i32* %sum72, align 4, !tbaa !2
  %add88 = add nsw i32 %82, %mul87
  store i32 %add88, i32* %sum72, align 4, !tbaa !2
  br label %for.inc89

for.inc89:                                        ; preds = %for.body76
  %83 = load i32, i32* %j, align 4, !tbaa !2
  %inc90 = add nsw i32 %83, 1
  store i32 %inc90, i32* %j, align 4, !tbaa !2
  br label %for.cond73

for.end91:                                        ; preds = %for.cond73
  %84 = load i32, i32* %sum72, align 4, !tbaa !2
  %shr92 = ashr i32 %84, 7
  store i32 %shr92, i32* %sum72, align 4, !tbaa !2
  %85 = load i32, i32* %sum72, align 4, !tbaa !2
  %call93 = call zeroext i8 @clip_pixel(i32 %85)
  %86 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr94 = getelementptr inbounds i8, i8* %86, i32 1
  store i8* %incdec.ptr94, i8** %optr, align 4, !tbaa !6
  store i8 %call93, i8* %86, align 1, !tbaa !8
  %87 = bitcast i32* %sum72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #5
  br label %for.inc95

for.inc95:                                        ; preds = %for.end91
  %88 = load i32, i32* %i, align 4, !tbaa !2
  %add96 = add nsw i32 %88, 2
  store i32 %add96, i32* %i, align 4, !tbaa !2
  br label %for.cond68

for.end97:                                        ; preds = %for.cond68
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc135, %for.end97
  %89 = load i32, i32* %i, align 4, !tbaa !2
  %90 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp99 = icmp slt i32 %89, %90
  br i1 %cmp99, label %for.body101, label %for.end137

for.body101:                                      ; preds = %for.cond98
  %91 = bitcast i32* %sum102 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #5
  store i32 64, i32* %sum102, align 4, !tbaa !2
  store i32 0, i32* %j, align 4, !tbaa !2
  br label %for.cond103

for.cond103:                                      ; preds = %for.inc129, %for.body101
  %92 = load i32, i32* %j, align 4, !tbaa !2
  %cmp104 = icmp slt i32 %92, 4
  br i1 %cmp104, label %for.body106, label %for.end131

for.body106:                                      ; preds = %for.cond103
  %93 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %94 = load i32, i32* %i, align 4, !tbaa !2
  %95 = load i32, i32* %j, align 4, !tbaa !2
  %sub107 = sub nsw i32 %94, %95
  %arrayidx108 = getelementptr inbounds i8, i8* %93, i32 %sub107
  %96 = load i8, i8* %arrayidx108, align 1, !tbaa !8
  %conv109 = zext i8 %96 to i32
  %97 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %98 = load i32, i32* %i, align 4, !tbaa !2
  %add110 = add nsw i32 %98, 1
  %99 = load i32, i32* %j, align 4, !tbaa !2
  %add111 = add nsw i32 %add110, %99
  %100 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub112 = sub nsw i32 %100, 1
  %cmp113 = icmp slt i32 %add111, %sub112
  br i1 %cmp113, label %cond.true115, label %cond.false118

cond.true115:                                     ; preds = %for.body106
  %101 = load i32, i32* %i, align 4, !tbaa !2
  %add116 = add nsw i32 %101, 1
  %102 = load i32, i32* %j, align 4, !tbaa !2
  %add117 = add nsw i32 %add116, %102
  br label %cond.end120

cond.false118:                                    ; preds = %for.body106
  %103 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub119 = sub nsw i32 %103, 1
  br label %cond.end120

cond.end120:                                      ; preds = %cond.false118, %cond.true115
  %cond121 = phi i32 [ %add117, %cond.true115 ], [ %sub119, %cond.false118 ]
  %arrayidx122 = getelementptr inbounds i8, i8* %97, i32 %cond121
  %104 = load i8, i8* %arrayidx122, align 1, !tbaa !8
  %conv123 = zext i8 %104 to i32
  %add124 = add nsw i32 %conv109, %conv123
  %105 = load i16*, i16** %filter, align 4, !tbaa !6
  %106 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i16, i16* %105, i32 %106
  %107 = load i16, i16* %arrayidx125, align 2, !tbaa !11
  %conv126 = sext i16 %107 to i32
  %mul127 = mul nsw i32 %add124, %conv126
  %108 = load i32, i32* %sum102, align 4, !tbaa !2
  %add128 = add nsw i32 %108, %mul127
  store i32 %add128, i32* %sum102, align 4, !tbaa !2
  br label %for.inc129

for.inc129:                                       ; preds = %cond.end120
  %109 = load i32, i32* %j, align 4, !tbaa !2
  %inc130 = add nsw i32 %109, 1
  store i32 %inc130, i32* %j, align 4, !tbaa !2
  br label %for.cond103

for.end131:                                       ; preds = %for.cond103
  %110 = load i32, i32* %sum102, align 4, !tbaa !2
  %shr132 = ashr i32 %110, 7
  store i32 %shr132, i32* %sum102, align 4, !tbaa !2
  %111 = load i32, i32* %sum102, align 4, !tbaa !2
  %call133 = call zeroext i8 @clip_pixel(i32 %111)
  %112 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr134 = getelementptr inbounds i8, i8* %112, i32 1
  store i8* %incdec.ptr134, i8** %optr, align 4, !tbaa !6
  store i8 %call133, i8* %112, align 1, !tbaa !8
  %113 = bitcast i32* %sum102 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #5
  br label %for.inc135

for.inc135:                                       ; preds = %for.end131
  %114 = load i32, i32* %i, align 4, !tbaa !2
  %add136 = add nsw i32 %114, 2
  store i32 %add136, i32* %i, align 4, !tbaa !2
  br label %for.cond98

for.end137:                                       ; preds = %for.cond98
  br label %if.end

if.end:                                           ; preds = %for.end137, %for.end30
  %115 = bitcast i32* %l2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #5
  %116 = bitcast i32* %l1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #5
  %117 = bitcast i8** %optr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i32* %filter_len_half to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  %121 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  ret void
}

; Function Attrs: nounwind
define internal void @interpolate(i8* %input, i32 %in_length, i8* %output, i32 %out_length) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %in_length.addr = alloca i32, align 4
  %output.addr = alloca i8*, align 4
  %out_length.addr = alloca i32, align 4
  %interp_filters = alloca [8 x i16]*, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !6
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !6
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  %0 = bitcast [8 x i16]** %interp_filters to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %2 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %call = call [8 x i16]* @choose_interp_filter(i32 %1, i32 %2)
  store [8 x i16]* %call, [8 x i16]** %interp_filters, align 4, !tbaa !6
  %3 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %4 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %output.addr, align 4, !tbaa !6
  %6 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %7 = load [8 x i16]*, [8 x i16]** %interp_filters, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x i16], [8 x i16]* %7, i32 0
  %arrayidx1 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx, i32 0, i32 0
  call void @interpolate_core(i8* %3, i32 %4, i8* %5, i32 %6, i16* %arrayidx1, i32 8)
  %8 = bitcast [8 x i16]** %interp_filters to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #5
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @clip_pixel(i32 %val) #3 {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !2
  %0 = load i32, i32* %val.addr, align 4, !tbaa !2
  %cmp = icmp sgt i32 %0, 255
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4, !tbaa !2
  %cmp1 = icmp slt i32 %1, 0
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %2 = load i32, i32* %val.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ 0, %cond.true2 ], [ %2, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ 255, %cond.true ], [ %cond, %cond.end ]
  %conv = trunc i32 %cond5 to i8
  ret i8 %conv
}

; Function Attrs: nounwind
define internal [8 x i16]* @choose_interp_filter(i32 %in_length, i32 %out_length) #0 {
entry:
  %retval = alloca [8 x i16]*, align 4
  %in_length.addr = alloca i32, align 4
  %out_length.addr = alloca i32, align 4
  %out_length16 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  %0 = bitcast i32* %out_length16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %mul = mul nsw i32 %1, 16
  store i32 %mul, i32* %out_length16, align 4, !tbaa !2
  %2 = load i32, i32* %out_length16, align 4, !tbaa !2
  %3 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %mul1 = mul nsw i32 %3, 16
  %cmp = icmp sge i32 %2, %mul1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store [8 x i16]* getelementptr inbounds ([64 x [8 x i16]], [64 x [8 x i16]]* @av1_resize_filter_normative, i32 0, i32 0), [8 x i16]** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %out_length16, align 4, !tbaa !2
  %5 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %mul2 = mul nsw i32 %5, 13
  %cmp3 = icmp sge i32 %4, %mul2
  br i1 %cmp3, label %if.then4, label %if.else5

if.then4:                                         ; preds = %if.else
  store [8 x i16]* getelementptr inbounds ([64 x [8 x i16]], [64 x [8 x i16]]* @filteredinterp_filters875, i32 0, i32 0), [8 x i16]** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else5:                                         ; preds = %if.else
  %6 = load i32, i32* %out_length16, align 4, !tbaa !2
  %7 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %mul6 = mul nsw i32 %7, 11
  %cmp7 = icmp sge i32 %6, %mul6
  br i1 %cmp7, label %if.then8, label %if.else9

if.then8:                                         ; preds = %if.else5
  store [8 x i16]* getelementptr inbounds ([64 x [8 x i16]], [64 x [8 x i16]]* @filteredinterp_filters750, i32 0, i32 0), [8 x i16]** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else9:                                         ; preds = %if.else5
  %8 = load i32, i32* %out_length16, align 4, !tbaa !2
  %9 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %mul10 = mul nsw i32 %9, 9
  %cmp11 = icmp sge i32 %8, %mul10
  br i1 %cmp11, label %if.then12, label %if.else13

if.then12:                                        ; preds = %if.else9
  store [8 x i16]* getelementptr inbounds ([64 x [8 x i16]], [64 x [8 x i16]]* @filteredinterp_filters625, i32 0, i32 0), [8 x i16]** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else13:                                        ; preds = %if.else9
  store [8 x i16]* getelementptr inbounds ([64 x [8 x i16]], [64 x [8 x i16]]* @filteredinterp_filters500, i32 0, i32 0), [8 x i16]** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else13, %if.then12, %if.then8, %if.then4, %if.then
  %10 = bitcast i32* %out_length16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  %11 = load [8 x i16]*, [8 x i16]** %retval, align 4
  ret [8 x i16]* %11
}

; Function Attrs: nounwind
define internal void @interpolate_core(i8* %input, i32 %in_length, i8* %output, i32 %out_length, i16* %interp_filters, i32 %interp_taps) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %in_length.addr = alloca i32, align 4
  %output.addr = alloca i8*, align 4
  %out_length.addr = alloca i32, align 4
  %interp_filters.addr = alloca i16*, align 4
  %interp_taps.addr = alloca i32, align 4
  %delta = alloca i32, align 4
  %offset = alloca i32, align 4
  %optr = alloca i8*, align 4
  %x = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x2 = alloca i32, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  %int_pel = alloca i32, align 4
  %sub_pel = alloca i32, align 4
  %y = alloca i32, align 4
  %filter = alloca i16*, align 4
  %pk = alloca i32, align 4
  %filter83 = alloca i16*, align 4
  %filter128 = alloca i16*, align 4
  %filter163 = alloca i16*, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !6
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !6
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  store i16* %interp_filters, i16** %interp_filters.addr, align 4, !tbaa !6
  store i32 %interp_taps, i32* %interp_taps.addr, align 4, !tbaa !2
  %0 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %shl = shl i32 %1, 14
  %2 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div = sdiv i32 %2, 2
  %add = add i32 %shl, %div
  %3 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div1 = udiv i32 %add, %3
  store i32 %div1, i32* %delta, align 4, !tbaa !2
  %4 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %6 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp = icmp sgt i32 %5, %6
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %7 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %8 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %7, %8
  %shl2 = shl i32 %sub, 13
  %9 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div3 = sdiv i32 %9, 2
  %add4 = add nsw i32 %shl2, %div3
  %10 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div5 = sdiv i32 %add4, %10
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %12 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub6 = sub nsw i32 %11, %12
  %shl7 = shl i32 %sub6, 13
  %13 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div8 = sdiv i32 %13, 2
  %add9 = add nsw i32 %shl7, %div8
  %sub10 = sub nsw i32 0, %add9
  %14 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div11 = sdiv i32 %sub10, %14
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %div5, %cond.true ], [ %div11, %cond.false ]
  store i32 %cond, i32* %offset, align 4, !tbaa !2
  %15 = bitcast i8** %optr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load i8*, i8** %output.addr, align 4, !tbaa !6
  store i8* %16, i8** %optr, align 4, !tbaa !6
  %17 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = bitcast i32* %x1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  %21 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = bitcast i32* %int_pel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = bitcast i32* %sub_pel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  %24 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  store i32 0, i32* %x, align 4, !tbaa !2
  %25 = load i32, i32* %offset, align 4, !tbaa !2
  %add12 = add nsw i32 %25, 128
  store i32 %add12, i32* %y, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %cond.end
  %26 = load i32, i32* %y, align 4, !tbaa !2
  %shr = ashr i32 %26, 14
  %27 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div13 = sdiv i32 %27, 2
  %sub14 = sub nsw i32 %div13, 1
  %cmp15 = icmp slt i32 %shr, %sub14
  br i1 %cmp15, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %28 = load i32, i32* %x, align 4, !tbaa !2
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %x, align 4, !tbaa !2
  %29 = load i32, i32* %delta, align 4, !tbaa !2
  %30 = load i32, i32* %y, align 4, !tbaa !2
  %add16 = add nsw i32 %30, %29
  store i32 %add16, i32* %y, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %31 = load i32, i32* %x, align 4, !tbaa !2
  store i32 %31, i32* %x1, align 4, !tbaa !2
  %32 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %sub17 = sub nsw i32 %32, 1
  store i32 %sub17, i32* %x, align 4, !tbaa !2
  %33 = load i32, i32* %delta, align 4, !tbaa !2
  %34 = load i32, i32* %x, align 4, !tbaa !2
  %mul = mul nsw i32 %33, %34
  %35 = load i32, i32* %offset, align 4, !tbaa !2
  %add18 = add nsw i32 %mul, %35
  %add19 = add nsw i32 %add18, 128
  store i32 %add19, i32* %y, align 4, !tbaa !2
  br label %while.cond20

while.cond20:                                     ; preds = %while.body25, %while.end
  %36 = load i32, i32* %y, align 4, !tbaa !2
  %shr21 = ashr i32 %36, 14
  %37 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div22 = sdiv i32 %37, 2
  %add23 = add nsw i32 %shr21, %div22
  %38 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %cmp24 = icmp sge i32 %add23, %38
  br i1 %cmp24, label %while.body25, label %while.end27

while.body25:                                     ; preds = %while.cond20
  %39 = load i32, i32* %x, align 4, !tbaa !2
  %dec = add nsw i32 %39, -1
  store i32 %dec, i32* %x, align 4, !tbaa !2
  %40 = load i32, i32* %delta, align 4, !tbaa !2
  %41 = load i32, i32* %y, align 4, !tbaa !2
  %sub26 = sub nsw i32 %41, %40
  store i32 %sub26, i32* %y, align 4, !tbaa !2
  br label %while.cond20

while.end27:                                      ; preds = %while.cond20
  %42 = load i32, i32* %x, align 4, !tbaa !2
  store i32 %42, i32* %x2, align 4, !tbaa !2
  %43 = load i32, i32* %x1, align 4, !tbaa !2
  %44 = load i32, i32* %x2, align 4, !tbaa !2
  %cmp28 = icmp sgt i32 %43, %44
  br i1 %cmp28, label %if.then, label %if.else

if.then:                                          ; preds = %while.end27
  store i32 0, i32* %x, align 4, !tbaa !2
  %45 = load i32, i32* %offset, align 4, !tbaa !2
  %add29 = add nsw i32 %45, 128
  store i32 %add29, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc71, %if.then
  %46 = load i32, i32* %x, align 4, !tbaa !2
  %47 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp30 = icmp slt i32 %46, %47
  br i1 %cmp30, label %for.body, label %for.end74

for.body:                                         ; preds = %for.cond
  %48 = load i32, i32* %y, align 4, !tbaa !2
  %shr31 = ashr i32 %48, 14
  store i32 %shr31, i32* %int_pel, align 4, !tbaa !2
  %49 = load i32, i32* %y, align 4, !tbaa !2
  %shr32 = ashr i32 %49, 8
  %and = and i32 %shr32, 63
  store i32 %and, i32* %sub_pel, align 4, !tbaa !2
  %50 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #5
  %51 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %52 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %53 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul33 = mul nsw i32 %52, %53
  %arrayidx = getelementptr inbounds i16, i16* %51, i32 %mul33
  store i16* %arrayidx, i16** %filter, align 4, !tbaa !6
  store i32 0, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc, %for.body
  %54 = load i32, i32* %k, align 4, !tbaa !2
  %55 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp35 = icmp slt i32 %54, %55
  br i1 %cmp35, label %for.body36, label %for.end

for.body36:                                       ; preds = %for.cond34
  %56 = bitcast i32* %pk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #5
  %57 = load i32, i32* %int_pel, align 4, !tbaa !2
  %58 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div37 = sdiv i32 %58, 2
  %sub38 = sub nsw i32 %57, %div37
  %add39 = add nsw i32 %sub38, 1
  %59 = load i32, i32* %k, align 4, !tbaa !2
  %add40 = add nsw i32 %add39, %59
  store i32 %add40, i32* %pk, align 4, !tbaa !2
  %60 = load i16*, i16** %filter, align 4, !tbaa !6
  %61 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i16, i16* %60, i32 %61
  %62 = load i16, i16* %arrayidx41, align 2, !tbaa !11
  %conv = sext i16 %62 to i32
  %63 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %64 = load i32, i32* %pk, align 4, !tbaa !2
  %65 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub42 = sub nsw i32 %65, 1
  %cmp43 = icmp slt i32 %64, %sub42
  br i1 %cmp43, label %cond.true45, label %cond.false46

cond.true45:                                      ; preds = %for.body36
  %66 = load i32, i32* %pk, align 4, !tbaa !2
  br label %cond.end48

cond.false46:                                     ; preds = %for.body36
  %67 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub47 = sub nsw i32 %67, 1
  br label %cond.end48

cond.end48:                                       ; preds = %cond.false46, %cond.true45
  %cond49 = phi i32 [ %66, %cond.true45 ], [ %sub47, %cond.false46 ]
  %cmp50 = icmp sgt i32 %cond49, 0
  br i1 %cmp50, label %cond.true52, label %cond.false61

cond.true52:                                      ; preds = %cond.end48
  %68 = load i32, i32* %pk, align 4, !tbaa !2
  %69 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub53 = sub nsw i32 %69, 1
  %cmp54 = icmp slt i32 %68, %sub53
  br i1 %cmp54, label %cond.true56, label %cond.false57

cond.true56:                                      ; preds = %cond.true52
  %70 = load i32, i32* %pk, align 4, !tbaa !2
  br label %cond.end59

cond.false57:                                     ; preds = %cond.true52
  %71 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub58 = sub nsw i32 %71, 1
  br label %cond.end59

cond.end59:                                       ; preds = %cond.false57, %cond.true56
  %cond60 = phi i32 [ %70, %cond.true56 ], [ %sub58, %cond.false57 ]
  br label %cond.end62

cond.false61:                                     ; preds = %cond.end48
  br label %cond.end62

cond.end62:                                       ; preds = %cond.false61, %cond.end59
  %cond63 = phi i32 [ %cond60, %cond.end59 ], [ 0, %cond.false61 ]
  %arrayidx64 = getelementptr inbounds i8, i8* %63, i32 %cond63
  %72 = load i8, i8* %arrayidx64, align 1, !tbaa !8
  %conv65 = zext i8 %72 to i32
  %mul66 = mul nsw i32 %conv, %conv65
  %73 = load i32, i32* %sum, align 4, !tbaa !2
  %add67 = add nsw i32 %73, %mul66
  store i32 %add67, i32* %sum, align 4, !tbaa !2
  %74 = bitcast i32* %pk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end62
  %75 = load i32, i32* %k, align 4, !tbaa !2
  %inc68 = add nsw i32 %75, 1
  store i32 %inc68, i32* %k, align 4, !tbaa !2
  br label %for.cond34

for.end:                                          ; preds = %for.cond34
  %76 = load i32, i32* %sum, align 4, !tbaa !2
  %add69 = add nsw i32 %76, 64
  %shr70 = ashr i32 %add69, 7
  %call = call zeroext i8 @clip_pixel(i32 %shr70)
  %77 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i8, i8* %77, i32 1
  store i8* %incdec.ptr, i8** %optr, align 4, !tbaa !6
  store i8 %call, i8* %77, align 1, !tbaa !8
  %78 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  br label %for.inc71

for.inc71:                                        ; preds = %for.end
  %79 = load i32, i32* %x, align 4, !tbaa !2
  %inc72 = add nsw i32 %79, 1
  store i32 %inc72, i32* %x, align 4, !tbaa !2
  %80 = load i32, i32* %delta, align 4, !tbaa !2
  %81 = load i32, i32* %y, align 4, !tbaa !2
  %add73 = add nsw i32 %81, %80
  store i32 %add73, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.end74:                                        ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %while.end27
  store i32 0, i32* %x, align 4, !tbaa !2
  %82 = load i32, i32* %offset, align 4, !tbaa !2
  %add75 = add nsw i32 %82, 128
  store i32 %add75, i32* %y, align 4, !tbaa !2
  br label %for.cond76

for.cond76:                                       ; preds = %for.inc117, %if.else
  %83 = load i32, i32* %x, align 4, !tbaa !2
  %84 = load i32, i32* %x1, align 4, !tbaa !2
  %cmp77 = icmp slt i32 %83, %84
  br i1 %cmp77, label %for.body79, label %for.end120

for.body79:                                       ; preds = %for.cond76
  %85 = load i32, i32* %y, align 4, !tbaa !2
  %shr80 = ashr i32 %85, 14
  store i32 %shr80, i32* %int_pel, align 4, !tbaa !2
  %86 = load i32, i32* %y, align 4, !tbaa !2
  %shr81 = ashr i32 %86, 8
  %and82 = and i32 %shr81, 63
  store i32 %and82, i32* %sub_pel, align 4, !tbaa !2
  %87 = bitcast i16** %filter83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #5
  %88 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %89 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %90 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul84 = mul nsw i32 %89, %90
  %arrayidx85 = getelementptr inbounds i16, i16* %88, i32 %mul84
  store i16* %arrayidx85, i16** %filter83, align 4, !tbaa !6
  store i32 0, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond86

for.cond86:                                       ; preds = %for.inc110, %for.body79
  %91 = load i32, i32* %k, align 4, !tbaa !2
  %92 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp87 = icmp slt i32 %91, %92
  br i1 %cmp87, label %for.body89, label %for.end112

for.body89:                                       ; preds = %for.cond86
  %93 = load i16*, i16** %filter83, align 4, !tbaa !6
  %94 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i16, i16* %93, i32 %94
  %95 = load i16, i16* %arrayidx90, align 2, !tbaa !11
  %conv91 = sext i16 %95 to i32
  %96 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %97 = load i32, i32* %int_pel, align 4, !tbaa !2
  %98 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div92 = sdiv i32 %98, 2
  %sub93 = sub nsw i32 %97, %div92
  %add94 = add nsw i32 %sub93, 1
  %99 = load i32, i32* %k, align 4, !tbaa !2
  %add95 = add nsw i32 %add94, %99
  %cmp96 = icmp sgt i32 %add95, 0
  br i1 %cmp96, label %cond.true98, label %cond.false103

cond.true98:                                      ; preds = %for.body89
  %100 = load i32, i32* %int_pel, align 4, !tbaa !2
  %101 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div99 = sdiv i32 %101, 2
  %sub100 = sub nsw i32 %100, %div99
  %add101 = add nsw i32 %sub100, 1
  %102 = load i32, i32* %k, align 4, !tbaa !2
  %add102 = add nsw i32 %add101, %102
  br label %cond.end104

cond.false103:                                    ; preds = %for.body89
  br label %cond.end104

cond.end104:                                      ; preds = %cond.false103, %cond.true98
  %cond105 = phi i32 [ %add102, %cond.true98 ], [ 0, %cond.false103 ]
  %arrayidx106 = getelementptr inbounds i8, i8* %96, i32 %cond105
  %103 = load i8, i8* %arrayidx106, align 1, !tbaa !8
  %conv107 = zext i8 %103 to i32
  %mul108 = mul nsw i32 %conv91, %conv107
  %104 = load i32, i32* %sum, align 4, !tbaa !2
  %add109 = add nsw i32 %104, %mul108
  store i32 %add109, i32* %sum, align 4, !tbaa !2
  br label %for.inc110

for.inc110:                                       ; preds = %cond.end104
  %105 = load i32, i32* %k, align 4, !tbaa !2
  %inc111 = add nsw i32 %105, 1
  store i32 %inc111, i32* %k, align 4, !tbaa !2
  br label %for.cond86

for.end112:                                       ; preds = %for.cond86
  %106 = load i32, i32* %sum, align 4, !tbaa !2
  %add113 = add nsw i32 %106, 64
  %shr114 = ashr i32 %add113, 7
  %call115 = call zeroext i8 @clip_pixel(i32 %shr114)
  %107 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr116 = getelementptr inbounds i8, i8* %107, i32 1
  store i8* %incdec.ptr116, i8** %optr, align 4, !tbaa !6
  store i8 %call115, i8* %107, align 1, !tbaa !8
  %108 = bitcast i16** %filter83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  br label %for.inc117

for.inc117:                                       ; preds = %for.end112
  %109 = load i32, i32* %x, align 4, !tbaa !2
  %inc118 = add nsw i32 %109, 1
  store i32 %inc118, i32* %x, align 4, !tbaa !2
  %110 = load i32, i32* %delta, align 4, !tbaa !2
  %111 = load i32, i32* %y, align 4, !tbaa !2
  %add119 = add nsw i32 %111, %110
  store i32 %add119, i32* %y, align 4, !tbaa !2
  br label %for.cond76

for.end120:                                       ; preds = %for.cond76
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc152, %for.end120
  %112 = load i32, i32* %x, align 4, !tbaa !2
  %113 = load i32, i32* %x2, align 4, !tbaa !2
  %cmp122 = icmp sle i32 %112, %113
  br i1 %cmp122, label %for.body124, label %for.end155

for.body124:                                      ; preds = %for.cond121
  %114 = load i32, i32* %y, align 4, !tbaa !2
  %shr125 = ashr i32 %114, 14
  store i32 %shr125, i32* %int_pel, align 4, !tbaa !2
  %115 = load i32, i32* %y, align 4, !tbaa !2
  %shr126 = ashr i32 %115, 8
  %and127 = and i32 %shr126, 63
  store i32 %and127, i32* %sub_pel, align 4, !tbaa !2
  %116 = bitcast i16** %filter128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #5
  %117 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %118 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %119 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul129 = mul nsw i32 %118, %119
  %arrayidx130 = getelementptr inbounds i16, i16* %117, i32 %mul129
  store i16* %arrayidx130, i16** %filter128, align 4, !tbaa !6
  store i32 0, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond131

for.cond131:                                      ; preds = %for.inc145, %for.body124
  %120 = load i32, i32* %k, align 4, !tbaa !2
  %121 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp132 = icmp slt i32 %120, %121
  br i1 %cmp132, label %for.body134, label %for.end147

for.body134:                                      ; preds = %for.cond131
  %122 = load i16*, i16** %filter128, align 4, !tbaa !6
  %123 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i16, i16* %122, i32 %123
  %124 = load i16, i16* %arrayidx135, align 2, !tbaa !11
  %conv136 = sext i16 %124 to i32
  %125 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %126 = load i32, i32* %int_pel, align 4, !tbaa !2
  %127 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div137 = sdiv i32 %127, 2
  %sub138 = sub nsw i32 %126, %div137
  %add139 = add nsw i32 %sub138, 1
  %128 = load i32, i32* %k, align 4, !tbaa !2
  %add140 = add nsw i32 %add139, %128
  %arrayidx141 = getelementptr inbounds i8, i8* %125, i32 %add140
  %129 = load i8, i8* %arrayidx141, align 1, !tbaa !8
  %conv142 = zext i8 %129 to i32
  %mul143 = mul nsw i32 %conv136, %conv142
  %130 = load i32, i32* %sum, align 4, !tbaa !2
  %add144 = add nsw i32 %130, %mul143
  store i32 %add144, i32* %sum, align 4, !tbaa !2
  br label %for.inc145

for.inc145:                                       ; preds = %for.body134
  %131 = load i32, i32* %k, align 4, !tbaa !2
  %inc146 = add nsw i32 %131, 1
  store i32 %inc146, i32* %k, align 4, !tbaa !2
  br label %for.cond131

for.end147:                                       ; preds = %for.cond131
  %132 = load i32, i32* %sum, align 4, !tbaa !2
  %add148 = add nsw i32 %132, 64
  %shr149 = ashr i32 %add148, 7
  %call150 = call zeroext i8 @clip_pixel(i32 %shr149)
  %133 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr151 = getelementptr inbounds i8, i8* %133, i32 1
  store i8* %incdec.ptr151, i8** %optr, align 4, !tbaa !6
  store i8 %call150, i8* %133, align 1, !tbaa !8
  %134 = bitcast i16** %filter128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #5
  br label %for.inc152

for.inc152:                                       ; preds = %for.end147
  %135 = load i32, i32* %x, align 4, !tbaa !2
  %inc153 = add nsw i32 %135, 1
  store i32 %inc153, i32* %x, align 4, !tbaa !2
  %136 = load i32, i32* %delta, align 4, !tbaa !2
  %137 = load i32, i32* %y, align 4, !tbaa !2
  %add154 = add nsw i32 %137, %136
  store i32 %add154, i32* %y, align 4, !tbaa !2
  br label %for.cond121

for.end155:                                       ; preds = %for.cond121
  br label %for.cond156

for.cond156:                                      ; preds = %for.inc199, %for.end155
  %138 = load i32, i32* %x, align 4, !tbaa !2
  %139 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp157 = icmp slt i32 %138, %139
  br i1 %cmp157, label %for.body159, label %for.end202

for.body159:                                      ; preds = %for.cond156
  %140 = load i32, i32* %y, align 4, !tbaa !2
  %shr160 = ashr i32 %140, 14
  store i32 %shr160, i32* %int_pel, align 4, !tbaa !2
  %141 = load i32, i32* %y, align 4, !tbaa !2
  %shr161 = ashr i32 %141, 8
  %and162 = and i32 %shr161, 63
  store i32 %and162, i32* %sub_pel, align 4, !tbaa !2
  %142 = bitcast i16** %filter163 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #5
  %143 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %144 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %145 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul164 = mul nsw i32 %144, %145
  %arrayidx165 = getelementptr inbounds i16, i16* %143, i32 %mul164
  store i16* %arrayidx165, i16** %filter163, align 4, !tbaa !6
  store i32 0, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond166

for.cond166:                                      ; preds = %for.inc192, %for.body159
  %146 = load i32, i32* %k, align 4, !tbaa !2
  %147 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp167 = icmp slt i32 %146, %147
  br i1 %cmp167, label %for.body169, label %for.end194

for.body169:                                      ; preds = %for.cond166
  %148 = load i16*, i16** %filter163, align 4, !tbaa !6
  %149 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx170 = getelementptr inbounds i16, i16* %148, i32 %149
  %150 = load i16, i16* %arrayidx170, align 2, !tbaa !11
  %conv171 = sext i16 %150 to i32
  %151 = load i8*, i8** %input.addr, align 4, !tbaa !6
  %152 = load i32, i32* %int_pel, align 4, !tbaa !2
  %153 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div172 = sdiv i32 %153, 2
  %sub173 = sub nsw i32 %152, %div172
  %add174 = add nsw i32 %sub173, 1
  %154 = load i32, i32* %k, align 4, !tbaa !2
  %add175 = add nsw i32 %add174, %154
  %155 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub176 = sub nsw i32 %155, 1
  %cmp177 = icmp slt i32 %add175, %sub176
  br i1 %cmp177, label %cond.true179, label %cond.false184

cond.true179:                                     ; preds = %for.body169
  %156 = load i32, i32* %int_pel, align 4, !tbaa !2
  %157 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div180 = sdiv i32 %157, 2
  %sub181 = sub nsw i32 %156, %div180
  %add182 = add nsw i32 %sub181, 1
  %158 = load i32, i32* %k, align 4, !tbaa !2
  %add183 = add nsw i32 %add182, %158
  br label %cond.end186

cond.false184:                                    ; preds = %for.body169
  %159 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub185 = sub nsw i32 %159, 1
  br label %cond.end186

cond.end186:                                      ; preds = %cond.false184, %cond.true179
  %cond187 = phi i32 [ %add183, %cond.true179 ], [ %sub185, %cond.false184 ]
  %arrayidx188 = getelementptr inbounds i8, i8* %151, i32 %cond187
  %160 = load i8, i8* %arrayidx188, align 1, !tbaa !8
  %conv189 = zext i8 %160 to i32
  %mul190 = mul nsw i32 %conv171, %conv189
  %161 = load i32, i32* %sum, align 4, !tbaa !2
  %add191 = add nsw i32 %161, %mul190
  store i32 %add191, i32* %sum, align 4, !tbaa !2
  br label %for.inc192

for.inc192:                                       ; preds = %cond.end186
  %162 = load i32, i32* %k, align 4, !tbaa !2
  %inc193 = add nsw i32 %162, 1
  store i32 %inc193, i32* %k, align 4, !tbaa !2
  br label %for.cond166

for.end194:                                       ; preds = %for.cond166
  %163 = load i32, i32* %sum, align 4, !tbaa !2
  %add195 = add nsw i32 %163, 64
  %shr196 = ashr i32 %add195, 7
  %call197 = call zeroext i8 @clip_pixel(i32 %shr196)
  %164 = load i8*, i8** %optr, align 4, !tbaa !6
  %incdec.ptr198 = getelementptr inbounds i8, i8* %164, i32 1
  store i8* %incdec.ptr198, i8** %optr, align 4, !tbaa !6
  store i8 %call197, i8* %164, align 1, !tbaa !8
  %165 = bitcast i16** %filter163 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #5
  br label %for.inc199

for.inc199:                                       ; preds = %for.end194
  %166 = load i32, i32* %x, align 4, !tbaa !2
  %inc200 = add nsw i32 %166, 1
  store i32 %inc200, i32* %x, align 4, !tbaa !2
  %167 = load i32, i32* %delta, align 4, !tbaa !2
  %168 = load i32, i32* %y, align 4, !tbaa !2
  %add201 = add nsw i32 %168, %167
  store i32 %add201, i32* %y, align 4, !tbaa !2
  br label %for.cond156

for.end202:                                       ; preds = %for.cond156
  br label %if.end

if.end:                                           ; preds = %for.end202, %for.end74
  %169 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #5
  %170 = bitcast i32* %sub_pel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #5
  %171 = bitcast i32* %int_pel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #5
  %172 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #5
  %173 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #5
  %174 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #5
  %175 = bitcast i32* %x1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #5
  %176 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #5
  %177 = bitcast i8** %optr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #5
  %178 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #5
  %179 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #5
  ret void
}

; Function Attrs: nounwind
define internal void @interpolate_double_prec(double* %input, i32 %in_length, double* %output, i32 %out_length) #0 {
entry:
  %input.addr = alloca double*, align 4
  %in_length.addr = alloca i32, align 4
  %output.addr = alloca double*, align 4
  %out_length.addr = alloca i32, align 4
  %interp_filters = alloca [8 x i16]*, align 4
  store double* %input, double** %input.addr, align 4, !tbaa !6
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store double* %output, double** %output.addr, align 4, !tbaa !6
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  %0 = bitcast [8 x i16]** %interp_filters to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %2 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %call = call [8 x i16]* @choose_interp_filter(i32 %1, i32 %2)
  store [8 x i16]* %call, [8 x i16]** %interp_filters, align 4, !tbaa !6
  %3 = load double*, double** %input.addr, align 4, !tbaa !6
  %4 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %5 = load double*, double** %output.addr, align 4, !tbaa !6
  %6 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %7 = load [8 x i16]*, [8 x i16]** %interp_filters, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x i16], [8 x i16]* %7, i32 0
  %arrayidx1 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx, i32 0, i32 0
  call void @interpolate_core_double_prec(double* %3, i32 %4, double* %5, i32 %6, i16* %arrayidx1, i32 8)
  %8 = bitcast [8 x i16]** %interp_filters to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #5
  ret void
}

; Function Attrs: nounwind
define internal void @interpolate_core_double_prec(double* %input, i32 %in_length, double* %output, i32 %out_length, i16* %interp_filters, i32 %interp_taps) #0 {
entry:
  %input.addr = alloca double*, align 4
  %in_length.addr = alloca i32, align 4
  %output.addr = alloca double*, align 4
  %out_length.addr = alloca i32, align 4
  %interp_filters.addr = alloca i16*, align 4
  %interp_taps.addr = alloca i32, align 4
  %delta = alloca i32, align 4
  %offset = alloca i32, align 4
  %optr = alloca double*, align 4
  %x = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x2 = alloca i32, align 4
  %k = alloca i32, align 4
  %int_pel = alloca i32, align 4
  %sub_pel = alloca i32, align 4
  %sum = alloca double, align 8
  %y = alloca i32, align 4
  %filter = alloca i16*, align 4
  %pk = alloca i32, align 4
  %filter82 = alloca i16*, align 4
  %filter125 = alloca i16*, align 4
  %filter158 = alloca i16*, align 4
  store double* %input, double** %input.addr, align 4, !tbaa !6
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store double* %output, double** %output.addr, align 4, !tbaa !6
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  store i16* %interp_filters, i16** %interp_filters.addr, align 4, !tbaa !6
  store i32 %interp_taps, i32* %interp_taps.addr, align 4, !tbaa !2
  %0 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %shl = shl i32 %1, 14
  %2 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div = sdiv i32 %2, 2
  %add = add i32 %shl, %div
  %3 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div1 = udiv i32 %add, %3
  store i32 %div1, i32* %delta, align 4, !tbaa !2
  %4 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %6 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp = icmp sgt i32 %5, %6
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %7 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %8 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %7, %8
  %shl2 = shl i32 %sub, 13
  %9 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div3 = sdiv i32 %9, 2
  %add4 = add nsw i32 %shl2, %div3
  %10 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div5 = sdiv i32 %add4, %10
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %12 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub6 = sub nsw i32 %11, %12
  %shl7 = shl i32 %sub6, 13
  %13 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div8 = sdiv i32 %13, 2
  %add9 = add nsw i32 %shl7, %div8
  %sub10 = sub nsw i32 0, %add9
  %14 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div11 = sdiv i32 %sub10, %14
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %div5, %cond.true ], [ %div11, %cond.false ]
  store i32 %cond, i32* %offset, align 4, !tbaa !2
  %15 = bitcast double** %optr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load double*, double** %output.addr, align 4, !tbaa !6
  store double* %16, double** %optr, align 4, !tbaa !6
  %17 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = bitcast i32* %x1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  %21 = bitcast i32* %int_pel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = bitcast i32* %sub_pel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = bitcast double* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %23) #5
  %24 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  store i32 0, i32* %x, align 4, !tbaa !2
  %25 = load i32, i32* %offset, align 4, !tbaa !2
  %add12 = add nsw i32 %25, 128
  store i32 %add12, i32* %y, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %cond.end
  %26 = load i32, i32* %y, align 4, !tbaa !2
  %shr = ashr i32 %26, 14
  %27 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div13 = sdiv i32 %27, 2
  %sub14 = sub nsw i32 %div13, 1
  %cmp15 = icmp slt i32 %shr, %sub14
  br i1 %cmp15, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %28 = load i32, i32* %x, align 4, !tbaa !2
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %x, align 4, !tbaa !2
  %29 = load i32, i32* %delta, align 4, !tbaa !2
  %30 = load i32, i32* %y, align 4, !tbaa !2
  %add16 = add nsw i32 %30, %29
  store i32 %add16, i32* %y, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %31 = load i32, i32* %x, align 4, !tbaa !2
  store i32 %31, i32* %x1, align 4, !tbaa !2
  %32 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %sub17 = sub nsw i32 %32, 1
  store i32 %sub17, i32* %x, align 4, !tbaa !2
  %33 = load i32, i32* %delta, align 4, !tbaa !2
  %34 = load i32, i32* %x, align 4, !tbaa !2
  %mul = mul nsw i32 %33, %34
  %35 = load i32, i32* %offset, align 4, !tbaa !2
  %add18 = add nsw i32 %mul, %35
  %add19 = add nsw i32 %add18, 128
  store i32 %add19, i32* %y, align 4, !tbaa !2
  br label %while.cond20

while.cond20:                                     ; preds = %while.body25, %while.end
  %36 = load i32, i32* %y, align 4, !tbaa !2
  %shr21 = ashr i32 %36, 14
  %37 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div22 = sdiv i32 %37, 2
  %add23 = add nsw i32 %shr21, %div22
  %38 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %cmp24 = icmp sge i32 %add23, %38
  br i1 %cmp24, label %while.body25, label %while.end27

while.body25:                                     ; preds = %while.cond20
  %39 = load i32, i32* %x, align 4, !tbaa !2
  %dec = add nsw i32 %39, -1
  store i32 %dec, i32* %x, align 4, !tbaa !2
  %40 = load i32, i32* %delta, align 4, !tbaa !2
  %41 = load i32, i32* %y, align 4, !tbaa !2
  %sub26 = sub nsw i32 %41, %40
  store i32 %sub26, i32* %y, align 4, !tbaa !2
  br label %while.cond20

while.end27:                                      ; preds = %while.cond20
  %42 = load i32, i32* %x, align 4, !tbaa !2
  store i32 %42, i32* %x2, align 4, !tbaa !2
  %43 = load i32, i32* %x1, align 4, !tbaa !2
  %44 = load i32, i32* %x2, align 4, !tbaa !2
  %cmp28 = icmp sgt i32 %43, %44
  br i1 %cmp28, label %if.then, label %if.else

if.then:                                          ; preds = %while.end27
  store i32 0, i32* %x, align 4, !tbaa !2
  %45 = load i32, i32* %offset, align 4, !tbaa !2
  %add29 = add nsw i32 %45, 128
  store i32 %add29, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc70, %if.then
  %46 = load i32, i32* %x, align 4, !tbaa !2
  %47 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp30 = icmp slt i32 %46, %47
  br i1 %cmp30, label %for.body, label %for.end73

for.body:                                         ; preds = %for.cond
  %48 = load i32, i32* %y, align 4, !tbaa !2
  %shr31 = ashr i32 %48, 14
  store i32 %shr31, i32* %int_pel, align 4, !tbaa !2
  %49 = load i32, i32* %y, align 4, !tbaa !2
  %shr32 = ashr i32 %49, 8
  %and = and i32 %shr32, 63
  store i32 %and, i32* %sub_pel, align 4, !tbaa !2
  %50 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #5
  %51 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %52 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %53 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul33 = mul nsw i32 %52, %53
  %arrayidx = getelementptr inbounds i16, i16* %51, i32 %mul33
  store i16* %arrayidx, i16** %filter, align 4, !tbaa !6
  store double 0.000000e+00, double* %sum, align 8, !tbaa !9
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc, %for.body
  %54 = load i32, i32* %k, align 4, !tbaa !2
  %55 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp35 = icmp slt i32 %54, %55
  br i1 %cmp35, label %for.body36, label %for.end

for.body36:                                       ; preds = %for.cond34
  %56 = bitcast i32* %pk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #5
  %57 = load i32, i32* %int_pel, align 4, !tbaa !2
  %58 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div37 = sdiv i32 %58, 2
  %sub38 = sub nsw i32 %57, %div37
  %add39 = add nsw i32 %sub38, 1
  %59 = load i32, i32* %k, align 4, !tbaa !2
  %add40 = add nsw i32 %add39, %59
  store i32 %add40, i32* %pk, align 4, !tbaa !2
  %60 = load i16*, i16** %filter, align 4, !tbaa !6
  %61 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i16, i16* %60, i32 %61
  %62 = load i16, i16* %arrayidx41, align 2, !tbaa !11
  %conv = sext i16 %62 to i32
  %conv42 = sitofp i32 %conv to double
  %63 = load double*, double** %input.addr, align 4, !tbaa !6
  %64 = load i32, i32* %pk, align 4, !tbaa !2
  %65 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub43 = sub nsw i32 %65, 1
  %cmp44 = icmp slt i32 %64, %sub43
  br i1 %cmp44, label %cond.true46, label %cond.false47

cond.true46:                                      ; preds = %for.body36
  %66 = load i32, i32* %pk, align 4, !tbaa !2
  br label %cond.end49

cond.false47:                                     ; preds = %for.body36
  %67 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub48 = sub nsw i32 %67, 1
  br label %cond.end49

cond.end49:                                       ; preds = %cond.false47, %cond.true46
  %cond50 = phi i32 [ %66, %cond.true46 ], [ %sub48, %cond.false47 ]
  %cmp51 = icmp sgt i32 %cond50, 0
  br i1 %cmp51, label %cond.true53, label %cond.false62

cond.true53:                                      ; preds = %cond.end49
  %68 = load i32, i32* %pk, align 4, !tbaa !2
  %69 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub54 = sub nsw i32 %69, 1
  %cmp55 = icmp slt i32 %68, %sub54
  br i1 %cmp55, label %cond.true57, label %cond.false58

cond.true57:                                      ; preds = %cond.true53
  %70 = load i32, i32* %pk, align 4, !tbaa !2
  br label %cond.end60

cond.false58:                                     ; preds = %cond.true53
  %71 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub59 = sub nsw i32 %71, 1
  br label %cond.end60

cond.end60:                                       ; preds = %cond.false58, %cond.true57
  %cond61 = phi i32 [ %70, %cond.true57 ], [ %sub59, %cond.false58 ]
  br label %cond.end63

cond.false62:                                     ; preds = %cond.end49
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false62, %cond.end60
  %cond64 = phi i32 [ %cond61, %cond.end60 ], [ 0, %cond.false62 ]
  %arrayidx65 = getelementptr inbounds double, double* %63, i32 %cond64
  %72 = load double, double* %arrayidx65, align 8, !tbaa !9
  %mul66 = fmul double %conv42, %72
  %73 = load double, double* %sum, align 8, !tbaa !9
  %add67 = fadd double %73, %mul66
  store double %add67, double* %sum, align 8, !tbaa !9
  %74 = bitcast i32* %pk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end63
  %75 = load i32, i32* %k, align 4, !tbaa !2
  %inc68 = add nsw i32 %75, 1
  store i32 %inc68, i32* %k, align 4, !tbaa !2
  br label %for.cond34

for.end:                                          ; preds = %for.cond34
  %76 = load double, double* %sum, align 8, !tbaa !9
  %div69 = fdiv double %76, 1.280000e+02
  %77 = load double*, double** %optr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds double, double* %77, i32 1
  store double* %incdec.ptr, double** %optr, align 4, !tbaa !6
  store double %div69, double* %77, align 8, !tbaa !9
  %78 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  br label %for.inc70

for.inc70:                                        ; preds = %for.end
  %79 = load i32, i32* %x, align 4, !tbaa !2
  %inc71 = add nsw i32 %79, 1
  store i32 %inc71, i32* %x, align 4, !tbaa !2
  %80 = load i32, i32* %delta, align 4, !tbaa !2
  %81 = load i32, i32* %y, align 4, !tbaa !2
  %add72 = add nsw i32 %81, %80
  store i32 %add72, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.end73:                                        ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %while.end27
  store i32 0, i32* %x, align 4, !tbaa !2
  %82 = load i32, i32* %offset, align 4, !tbaa !2
  %add74 = add nsw i32 %82, 128
  store i32 %add74, i32* %y, align 4, !tbaa !2
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc114, %if.else
  %83 = load i32, i32* %x, align 4, !tbaa !2
  %84 = load i32, i32* %x1, align 4, !tbaa !2
  %cmp76 = icmp slt i32 %83, %84
  br i1 %cmp76, label %for.body78, label %for.end117

for.body78:                                       ; preds = %for.cond75
  %85 = load i32, i32* %y, align 4, !tbaa !2
  %shr79 = ashr i32 %85, 14
  store i32 %shr79, i32* %int_pel, align 4, !tbaa !2
  %86 = load i32, i32* %y, align 4, !tbaa !2
  %shr80 = ashr i32 %86, 8
  %and81 = and i32 %shr80, 63
  store i32 %and81, i32* %sub_pel, align 4, !tbaa !2
  %87 = bitcast i16** %filter82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #5
  %88 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %89 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %90 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul83 = mul nsw i32 %89, %90
  %arrayidx84 = getelementptr inbounds i16, i16* %88, i32 %mul83
  store i16* %arrayidx84, i16** %filter82, align 4, !tbaa !6
  store double 0.000000e+00, double* %sum, align 8, !tbaa !9
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond85

for.cond85:                                       ; preds = %for.inc109, %for.body78
  %91 = load i32, i32* %k, align 4, !tbaa !2
  %92 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp86 = icmp slt i32 %91, %92
  br i1 %cmp86, label %for.body88, label %for.end111

for.body88:                                       ; preds = %for.cond85
  %93 = load i16*, i16** %filter82, align 4, !tbaa !6
  %94 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i16, i16* %93, i32 %94
  %95 = load i16, i16* %arrayidx89, align 2, !tbaa !11
  %conv90 = sext i16 %95 to i32
  %conv91 = sitofp i32 %conv90 to double
  %96 = load double*, double** %input.addr, align 4, !tbaa !6
  %97 = load i32, i32* %int_pel, align 4, !tbaa !2
  %98 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div92 = sdiv i32 %98, 2
  %sub93 = sub nsw i32 %97, %div92
  %add94 = add nsw i32 %sub93, 1
  %99 = load i32, i32* %k, align 4, !tbaa !2
  %add95 = add nsw i32 %add94, %99
  %cmp96 = icmp sgt i32 %add95, 0
  br i1 %cmp96, label %cond.true98, label %cond.false103

cond.true98:                                      ; preds = %for.body88
  %100 = load i32, i32* %int_pel, align 4, !tbaa !2
  %101 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div99 = sdiv i32 %101, 2
  %sub100 = sub nsw i32 %100, %div99
  %add101 = add nsw i32 %sub100, 1
  %102 = load i32, i32* %k, align 4, !tbaa !2
  %add102 = add nsw i32 %add101, %102
  br label %cond.end104

cond.false103:                                    ; preds = %for.body88
  br label %cond.end104

cond.end104:                                      ; preds = %cond.false103, %cond.true98
  %cond105 = phi i32 [ %add102, %cond.true98 ], [ 0, %cond.false103 ]
  %arrayidx106 = getelementptr inbounds double, double* %96, i32 %cond105
  %103 = load double, double* %arrayidx106, align 8, !tbaa !9
  %mul107 = fmul double %conv91, %103
  %104 = load double, double* %sum, align 8, !tbaa !9
  %add108 = fadd double %104, %mul107
  store double %add108, double* %sum, align 8, !tbaa !9
  br label %for.inc109

for.inc109:                                       ; preds = %cond.end104
  %105 = load i32, i32* %k, align 4, !tbaa !2
  %inc110 = add nsw i32 %105, 1
  store i32 %inc110, i32* %k, align 4, !tbaa !2
  br label %for.cond85

for.end111:                                       ; preds = %for.cond85
  %106 = load double, double* %sum, align 8, !tbaa !9
  %div112 = fdiv double %106, 1.280000e+02
  %107 = load double*, double** %optr, align 4, !tbaa !6
  %incdec.ptr113 = getelementptr inbounds double, double* %107, i32 1
  store double* %incdec.ptr113, double** %optr, align 4, !tbaa !6
  store double %div112, double* %107, align 8, !tbaa !9
  %108 = bitcast i16** %filter82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  br label %for.inc114

for.inc114:                                       ; preds = %for.end111
  %109 = load i32, i32* %x, align 4, !tbaa !2
  %inc115 = add nsw i32 %109, 1
  store i32 %inc115, i32* %x, align 4, !tbaa !2
  %110 = load i32, i32* %delta, align 4, !tbaa !2
  %111 = load i32, i32* %y, align 4, !tbaa !2
  %add116 = add nsw i32 %111, %110
  store i32 %add116, i32* %y, align 4, !tbaa !2
  br label %for.cond75

for.end117:                                       ; preds = %for.cond75
  br label %for.cond118

for.cond118:                                      ; preds = %for.inc147, %for.end117
  %112 = load i32, i32* %x, align 4, !tbaa !2
  %113 = load i32, i32* %x2, align 4, !tbaa !2
  %cmp119 = icmp sle i32 %112, %113
  br i1 %cmp119, label %for.body121, label %for.end150

for.body121:                                      ; preds = %for.cond118
  %114 = load i32, i32* %y, align 4, !tbaa !2
  %shr122 = ashr i32 %114, 14
  store i32 %shr122, i32* %int_pel, align 4, !tbaa !2
  %115 = load i32, i32* %y, align 4, !tbaa !2
  %shr123 = ashr i32 %115, 8
  %and124 = and i32 %shr123, 63
  store i32 %and124, i32* %sub_pel, align 4, !tbaa !2
  %116 = bitcast i16** %filter125 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #5
  %117 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %118 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %119 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul126 = mul nsw i32 %118, %119
  %arrayidx127 = getelementptr inbounds i16, i16* %117, i32 %mul126
  store i16* %arrayidx127, i16** %filter125, align 4, !tbaa !6
  store double 0.000000e+00, double* %sum, align 8, !tbaa !9
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond128

for.cond128:                                      ; preds = %for.inc142, %for.body121
  %120 = load i32, i32* %k, align 4, !tbaa !2
  %121 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp129 = icmp slt i32 %120, %121
  br i1 %cmp129, label %for.body131, label %for.end144

for.body131:                                      ; preds = %for.cond128
  %122 = load i16*, i16** %filter125, align 4, !tbaa !6
  %123 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i16, i16* %122, i32 %123
  %124 = load i16, i16* %arrayidx132, align 2, !tbaa !11
  %conv133 = sext i16 %124 to i32
  %conv134 = sitofp i32 %conv133 to double
  %125 = load double*, double** %input.addr, align 4, !tbaa !6
  %126 = load i32, i32* %int_pel, align 4, !tbaa !2
  %127 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div135 = sdiv i32 %127, 2
  %sub136 = sub nsw i32 %126, %div135
  %add137 = add nsw i32 %sub136, 1
  %128 = load i32, i32* %k, align 4, !tbaa !2
  %add138 = add nsw i32 %add137, %128
  %arrayidx139 = getelementptr inbounds double, double* %125, i32 %add138
  %129 = load double, double* %arrayidx139, align 8, !tbaa !9
  %mul140 = fmul double %conv134, %129
  %130 = load double, double* %sum, align 8, !tbaa !9
  %add141 = fadd double %130, %mul140
  store double %add141, double* %sum, align 8, !tbaa !9
  br label %for.inc142

for.inc142:                                       ; preds = %for.body131
  %131 = load i32, i32* %k, align 4, !tbaa !2
  %inc143 = add nsw i32 %131, 1
  store i32 %inc143, i32* %k, align 4, !tbaa !2
  br label %for.cond128

for.end144:                                       ; preds = %for.cond128
  %132 = load double, double* %sum, align 8, !tbaa !9
  %div145 = fdiv double %132, 1.280000e+02
  %133 = load double*, double** %optr, align 4, !tbaa !6
  %incdec.ptr146 = getelementptr inbounds double, double* %133, i32 1
  store double* %incdec.ptr146, double** %optr, align 4, !tbaa !6
  store double %div145, double* %133, align 8, !tbaa !9
  %134 = bitcast i16** %filter125 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #5
  br label %for.inc147

for.inc147:                                       ; preds = %for.end144
  %135 = load i32, i32* %x, align 4, !tbaa !2
  %inc148 = add nsw i32 %135, 1
  store i32 %inc148, i32* %x, align 4, !tbaa !2
  %136 = load i32, i32* %delta, align 4, !tbaa !2
  %137 = load i32, i32* %y, align 4, !tbaa !2
  %add149 = add nsw i32 %137, %136
  store i32 %add149, i32* %y, align 4, !tbaa !2
  br label %for.cond118

for.end150:                                       ; preds = %for.cond118
  br label %for.cond151

for.cond151:                                      ; preds = %for.inc192, %for.end150
  %138 = load i32, i32* %x, align 4, !tbaa !2
  %139 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp152 = icmp slt i32 %138, %139
  br i1 %cmp152, label %for.body154, label %for.end195

for.body154:                                      ; preds = %for.cond151
  %140 = load i32, i32* %y, align 4, !tbaa !2
  %shr155 = ashr i32 %140, 14
  store i32 %shr155, i32* %int_pel, align 4, !tbaa !2
  %141 = load i32, i32* %y, align 4, !tbaa !2
  %shr156 = ashr i32 %141, 8
  %and157 = and i32 %shr156, 63
  store i32 %and157, i32* %sub_pel, align 4, !tbaa !2
  %142 = bitcast i16** %filter158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #5
  %143 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %144 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %145 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul159 = mul nsw i32 %144, %145
  %arrayidx160 = getelementptr inbounds i16, i16* %143, i32 %mul159
  store i16* %arrayidx160, i16** %filter158, align 4, !tbaa !6
  store double 0.000000e+00, double* %sum, align 8, !tbaa !9
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond161

for.cond161:                                      ; preds = %for.inc187, %for.body154
  %146 = load i32, i32* %k, align 4, !tbaa !2
  %147 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp162 = icmp slt i32 %146, %147
  br i1 %cmp162, label %for.body164, label %for.end189

for.body164:                                      ; preds = %for.cond161
  %148 = load i16*, i16** %filter158, align 4, !tbaa !6
  %149 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx165 = getelementptr inbounds i16, i16* %148, i32 %149
  %150 = load i16, i16* %arrayidx165, align 2, !tbaa !11
  %conv166 = sext i16 %150 to i32
  %conv167 = sitofp i32 %conv166 to double
  %151 = load double*, double** %input.addr, align 4, !tbaa !6
  %152 = load i32, i32* %int_pel, align 4, !tbaa !2
  %153 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div168 = sdiv i32 %153, 2
  %sub169 = sub nsw i32 %152, %div168
  %add170 = add nsw i32 %sub169, 1
  %154 = load i32, i32* %k, align 4, !tbaa !2
  %add171 = add nsw i32 %add170, %154
  %155 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub172 = sub nsw i32 %155, 1
  %cmp173 = icmp slt i32 %add171, %sub172
  br i1 %cmp173, label %cond.true175, label %cond.false180

cond.true175:                                     ; preds = %for.body164
  %156 = load i32, i32* %int_pel, align 4, !tbaa !2
  %157 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div176 = sdiv i32 %157, 2
  %sub177 = sub nsw i32 %156, %div176
  %add178 = add nsw i32 %sub177, 1
  %158 = load i32, i32* %k, align 4, !tbaa !2
  %add179 = add nsw i32 %add178, %158
  br label %cond.end182

cond.false180:                                    ; preds = %for.body164
  %159 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub181 = sub nsw i32 %159, 1
  br label %cond.end182

cond.end182:                                      ; preds = %cond.false180, %cond.true175
  %cond183 = phi i32 [ %add179, %cond.true175 ], [ %sub181, %cond.false180 ]
  %arrayidx184 = getelementptr inbounds double, double* %151, i32 %cond183
  %160 = load double, double* %arrayidx184, align 8, !tbaa !9
  %mul185 = fmul double %conv167, %160
  %161 = load double, double* %sum, align 8, !tbaa !9
  %add186 = fadd double %161, %mul185
  store double %add186, double* %sum, align 8, !tbaa !9
  br label %for.inc187

for.inc187:                                       ; preds = %cond.end182
  %162 = load i32, i32* %k, align 4, !tbaa !2
  %inc188 = add nsw i32 %162, 1
  store i32 %inc188, i32* %k, align 4, !tbaa !2
  br label %for.cond161

for.end189:                                       ; preds = %for.cond161
  %163 = load double, double* %sum, align 8, !tbaa !9
  %div190 = fdiv double %163, 1.280000e+02
  %164 = load double*, double** %optr, align 4, !tbaa !6
  %incdec.ptr191 = getelementptr inbounds double, double* %164, i32 1
  store double* %incdec.ptr191, double** %optr, align 4, !tbaa !6
  store double %div190, double* %164, align 8, !tbaa !9
  %165 = bitcast i16** %filter158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #5
  br label %for.inc192

for.inc192:                                       ; preds = %for.end189
  %166 = load i32, i32* %x, align 4, !tbaa !2
  %inc193 = add nsw i32 %166, 1
  store i32 %inc193, i32* %x, align 4, !tbaa !2
  %167 = load i32, i32* %delta, align 4, !tbaa !2
  %168 = load i32, i32* %y, align 4, !tbaa !2
  %add194 = add nsw i32 %168, %167
  store i32 %add194, i32* %y, align 4, !tbaa !2
  br label %for.cond151

for.end195:                                       ; preds = %for.cond151
  br label %if.end

if.end:                                           ; preds = %for.end195, %for.end73
  %169 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #5
  %170 = bitcast double* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %170) #5
  %171 = bitcast i32* %sub_pel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #5
  %172 = bitcast i32* %int_pel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #5
  %173 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #5
  %174 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #5
  %175 = bitcast i32* %x1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #5
  %176 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #5
  %177 = bitcast double** %optr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #5
  %178 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #5
  %179 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #5
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_down2_symodd(i16* %input, i32 %length, i16* %output, i32 %bd) #0 {
entry:
  %input.addr = alloca i16*, align 4
  %length.addr = alloca i32, align 4
  %output.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %filter_len_half = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %optr = alloca i16*, align 4
  %l1 = alloca i32, align 4
  %l2 = alloca i32, align 4
  %sum = alloca i32, align 4
  %sum42 = alloca i32, align 4
  %sum84 = alloca i32, align 4
  %sum119 = alloca i32, align 4
  store i16* %input, i16** %input.addr, align 4, !tbaa !6
  store i32 %length, i32* %length.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = bitcast i32* %filter_len_half to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 4, i32* %filter_len_half, align 4, !tbaa !2
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i16** %optr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i16*, i16** %output.addr, align 4, !tbaa !6
  store i16* %4, i16** %optr, align 4, !tbaa !6
  %5 = bitcast i32* %l1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  store i32 3, i32* %l1, align 4, !tbaa !2
  %6 = bitcast i32* %l2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %7, 4
  %add = add nsw i32 %sub, 1
  store i32 %add, i32* %l2, align 4, !tbaa !2
  %8 = load i32, i32* %l1, align 4, !tbaa !2
  %and = and i32 %8, 1
  %9 = load i32, i32* %l1, align 4, !tbaa !2
  %add1 = add nsw i32 %9, %and
  store i32 %add1, i32* %l1, align 4, !tbaa !2
  %10 = load i32, i32* %l2, align 4, !tbaa !2
  %and2 = and i32 %10, 1
  %11 = load i32, i32* %l2, align 4, !tbaa !2
  %add3 = add nsw i32 %11, %and2
  store i32 %add3, i32* %l2, align 4, !tbaa !2
  %12 = load i32, i32* %l1, align 4, !tbaa !2
  %13 = load i32, i32* %l2, align 4, !tbaa !2
  %cmp = icmp sgt i32 %12, %13
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc35, %if.then
  %14 = load i32, i32* %i, align 4, !tbaa !2
  %15 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp4 = icmp slt i32 %14, %15
  br i1 %cmp4, label %for.body, label %for.end37

for.body:                                         ; preds = %for.cond
  %16 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %18 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %17, i32 %18
  %19 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %19 to i32
  %20 = load i16*, i16** @highbd_down2_symodd.filter, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i16, i16* %20, i32 0
  %21 = load i16, i16* %arrayidx5, align 2, !tbaa !11
  %conv6 = sext i16 %21 to i32
  %mul = mul nsw i32 %conv, %conv6
  %add7 = add nsw i32 64, %mul
  store i32 %add7, i32* %sum, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !2
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %22 = load i32, i32* %j, align 4, !tbaa !2
  %cmp9 = icmp slt i32 %22, 4
  br i1 %cmp9, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond8
  %23 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %24 = load i32, i32* %i, align 4, !tbaa !2
  %25 = load i32, i32* %j, align 4, !tbaa !2
  %sub12 = sub nsw i32 %24, %25
  %cmp13 = icmp sgt i32 %sub12, 0
  br i1 %cmp13, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body11
  %26 = load i32, i32* %i, align 4, !tbaa !2
  %27 = load i32, i32* %j, align 4, !tbaa !2
  %sub15 = sub nsw i32 %26, %27
  br label %cond.end

cond.false:                                       ; preds = %for.body11
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub15, %cond.true ], [ 0, %cond.false ]
  %arrayidx16 = getelementptr inbounds i16, i16* %23, i32 %cond
  %28 = load i16, i16* %arrayidx16, align 2, !tbaa !11
  %conv17 = zext i16 %28 to i32
  %29 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %30 = load i32, i32* %i, align 4, !tbaa !2
  %31 = load i32, i32* %j, align 4, !tbaa !2
  %add18 = add nsw i32 %30, %31
  %32 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub19 = sub nsw i32 %32, 1
  %cmp20 = icmp slt i32 %add18, %sub19
  br i1 %cmp20, label %cond.true22, label %cond.false24

cond.true22:                                      ; preds = %cond.end
  %33 = load i32, i32* %i, align 4, !tbaa !2
  %34 = load i32, i32* %j, align 4, !tbaa !2
  %add23 = add nsw i32 %33, %34
  br label %cond.end26

cond.false24:                                     ; preds = %cond.end
  %35 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub25 = sub nsw i32 %35, 1
  br label %cond.end26

cond.end26:                                       ; preds = %cond.false24, %cond.true22
  %cond27 = phi i32 [ %add23, %cond.true22 ], [ %sub25, %cond.false24 ]
  %arrayidx28 = getelementptr inbounds i16, i16* %29, i32 %cond27
  %36 = load i16, i16* %arrayidx28, align 2, !tbaa !11
  %conv29 = zext i16 %36 to i32
  %add30 = add nsw i32 %conv17, %conv29
  %37 = load i16*, i16** @highbd_down2_symodd.filter, align 4, !tbaa !6
  %38 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i16, i16* %37, i32 %38
  %39 = load i16, i16* %arrayidx31, align 2, !tbaa !11
  %conv32 = sext i16 %39 to i32
  %mul33 = mul nsw i32 %add30, %conv32
  %40 = load i32, i32* %sum, align 4, !tbaa !2
  %add34 = add nsw i32 %40, %mul33
  store i32 %add34, i32* %sum, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %cond.end26
  %41 = load i32, i32* %j, align 4, !tbaa !2
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %j, align 4, !tbaa !2
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %42 = load i32, i32* %sum, align 4, !tbaa !2
  %shr = ashr i32 %42, 7
  store i32 %shr, i32* %sum, align 4, !tbaa !2
  %43 = load i32, i32* %sum, align 4, !tbaa !2
  %44 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call = call zeroext i16 @clip_pixel_highbd(i32 %43, i32 %44)
  %45 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i16, i16* %45, i32 1
  store i16* %incdec.ptr, i16** %optr, align 4, !tbaa !6
  store i16 %call, i16* %45, align 2, !tbaa !11
  %46 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  br label %for.inc35

for.inc35:                                        ; preds = %for.end
  %47 = load i32, i32* %i, align 4, !tbaa !2
  %add36 = add nsw i32 %47, 2
  store i32 %add36, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end37:                                        ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc77, %if.else
  %48 = load i32, i32* %i, align 4, !tbaa !2
  %49 = load i32, i32* %l1, align 4, !tbaa !2
  %cmp39 = icmp slt i32 %48, %49
  br i1 %cmp39, label %for.body41, label %for.end79

for.body41:                                       ; preds = %for.cond38
  %50 = bitcast i32* %sum42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #5
  %51 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %52 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i16, i16* %51, i32 %52
  %53 = load i16, i16* %arrayidx43, align 2, !tbaa !11
  %conv44 = zext i16 %53 to i32
  %54 = load i16*, i16** @highbd_down2_symodd.filter, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds i16, i16* %54, i32 0
  %55 = load i16, i16* %arrayidx45, align 2, !tbaa !11
  %conv46 = sext i16 %55 to i32
  %mul47 = mul nsw i32 %conv44, %conv46
  %add48 = add nsw i32 64, %mul47
  store i32 %add48, i32* %sum42, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !2
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc71, %for.body41
  %56 = load i32, i32* %j, align 4, !tbaa !2
  %cmp50 = icmp slt i32 %56, 4
  br i1 %cmp50, label %for.body52, label %for.end73

for.body52:                                       ; preds = %for.cond49
  %57 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %58 = load i32, i32* %i, align 4, !tbaa !2
  %59 = load i32, i32* %j, align 4, !tbaa !2
  %sub53 = sub nsw i32 %58, %59
  %cmp54 = icmp sgt i32 %sub53, 0
  br i1 %cmp54, label %cond.true56, label %cond.false58

cond.true56:                                      ; preds = %for.body52
  %60 = load i32, i32* %i, align 4, !tbaa !2
  %61 = load i32, i32* %j, align 4, !tbaa !2
  %sub57 = sub nsw i32 %60, %61
  br label %cond.end59

cond.false58:                                     ; preds = %for.body52
  br label %cond.end59

cond.end59:                                       ; preds = %cond.false58, %cond.true56
  %cond60 = phi i32 [ %sub57, %cond.true56 ], [ 0, %cond.false58 ]
  %arrayidx61 = getelementptr inbounds i16, i16* %57, i32 %cond60
  %62 = load i16, i16* %arrayidx61, align 2, !tbaa !11
  %conv62 = zext i16 %62 to i32
  %63 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %64 = load i32, i32* %i, align 4, !tbaa !2
  %65 = load i32, i32* %j, align 4, !tbaa !2
  %add63 = add nsw i32 %64, %65
  %arrayidx64 = getelementptr inbounds i16, i16* %63, i32 %add63
  %66 = load i16, i16* %arrayidx64, align 2, !tbaa !11
  %conv65 = zext i16 %66 to i32
  %add66 = add nsw i32 %conv62, %conv65
  %67 = load i16*, i16** @highbd_down2_symodd.filter, align 4, !tbaa !6
  %68 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i16, i16* %67, i32 %68
  %69 = load i16, i16* %arrayidx67, align 2, !tbaa !11
  %conv68 = sext i16 %69 to i32
  %mul69 = mul nsw i32 %add66, %conv68
  %70 = load i32, i32* %sum42, align 4, !tbaa !2
  %add70 = add nsw i32 %70, %mul69
  store i32 %add70, i32* %sum42, align 4, !tbaa !2
  br label %for.inc71

for.inc71:                                        ; preds = %cond.end59
  %71 = load i32, i32* %j, align 4, !tbaa !2
  %inc72 = add nsw i32 %71, 1
  store i32 %inc72, i32* %j, align 4, !tbaa !2
  br label %for.cond49

for.end73:                                        ; preds = %for.cond49
  %72 = load i32, i32* %sum42, align 4, !tbaa !2
  %shr74 = ashr i32 %72, 7
  store i32 %shr74, i32* %sum42, align 4, !tbaa !2
  %73 = load i32, i32* %sum42, align 4, !tbaa !2
  %74 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call75 = call zeroext i16 @clip_pixel_highbd(i32 %73, i32 %74)
  %75 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr76 = getelementptr inbounds i16, i16* %75, i32 1
  store i16* %incdec.ptr76, i16** %optr, align 4, !tbaa !6
  store i16 %call75, i16* %75, align 2, !tbaa !11
  %76 = bitcast i32* %sum42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #5
  br label %for.inc77

for.inc77:                                        ; preds = %for.end73
  %77 = load i32, i32* %i, align 4, !tbaa !2
  %add78 = add nsw i32 %77, 2
  store i32 %add78, i32* %i, align 4, !tbaa !2
  br label %for.cond38

for.end79:                                        ; preds = %for.cond38
  br label %for.cond80

for.cond80:                                       ; preds = %for.inc112, %for.end79
  %78 = load i32, i32* %i, align 4, !tbaa !2
  %79 = load i32, i32* %l2, align 4, !tbaa !2
  %cmp81 = icmp slt i32 %78, %79
  br i1 %cmp81, label %for.body83, label %for.end114

for.body83:                                       ; preds = %for.cond80
  %80 = bitcast i32* %sum84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #5
  %81 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %82 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i16, i16* %81, i32 %82
  %83 = load i16, i16* %arrayidx85, align 2, !tbaa !11
  %conv86 = zext i16 %83 to i32
  %84 = load i16*, i16** @highbd_down2_symodd.filter, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds i16, i16* %84, i32 0
  %85 = load i16, i16* %arrayidx87, align 2, !tbaa !11
  %conv88 = sext i16 %85 to i32
  %mul89 = mul nsw i32 %conv86, %conv88
  %add90 = add nsw i32 64, %mul89
  store i32 %add90, i32* %sum84, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !2
  br label %for.cond91

for.cond91:                                       ; preds = %for.inc106, %for.body83
  %86 = load i32, i32* %j, align 4, !tbaa !2
  %cmp92 = icmp slt i32 %86, 4
  br i1 %cmp92, label %for.body94, label %for.end108

for.body94:                                       ; preds = %for.cond91
  %87 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %88 = load i32, i32* %i, align 4, !tbaa !2
  %89 = load i32, i32* %j, align 4, !tbaa !2
  %sub95 = sub nsw i32 %88, %89
  %arrayidx96 = getelementptr inbounds i16, i16* %87, i32 %sub95
  %90 = load i16, i16* %arrayidx96, align 2, !tbaa !11
  %conv97 = zext i16 %90 to i32
  %91 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %92 = load i32, i32* %i, align 4, !tbaa !2
  %93 = load i32, i32* %j, align 4, !tbaa !2
  %add98 = add nsw i32 %92, %93
  %arrayidx99 = getelementptr inbounds i16, i16* %91, i32 %add98
  %94 = load i16, i16* %arrayidx99, align 2, !tbaa !11
  %conv100 = zext i16 %94 to i32
  %add101 = add nsw i32 %conv97, %conv100
  %95 = load i16*, i16** @highbd_down2_symodd.filter, align 4, !tbaa !6
  %96 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i16, i16* %95, i32 %96
  %97 = load i16, i16* %arrayidx102, align 2, !tbaa !11
  %conv103 = sext i16 %97 to i32
  %mul104 = mul nsw i32 %add101, %conv103
  %98 = load i32, i32* %sum84, align 4, !tbaa !2
  %add105 = add nsw i32 %98, %mul104
  store i32 %add105, i32* %sum84, align 4, !tbaa !2
  br label %for.inc106

for.inc106:                                       ; preds = %for.body94
  %99 = load i32, i32* %j, align 4, !tbaa !2
  %inc107 = add nsw i32 %99, 1
  store i32 %inc107, i32* %j, align 4, !tbaa !2
  br label %for.cond91

for.end108:                                       ; preds = %for.cond91
  %100 = load i32, i32* %sum84, align 4, !tbaa !2
  %shr109 = ashr i32 %100, 7
  store i32 %shr109, i32* %sum84, align 4, !tbaa !2
  %101 = load i32, i32* %sum84, align 4, !tbaa !2
  %102 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call110 = call zeroext i16 @clip_pixel_highbd(i32 %101, i32 %102)
  %103 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr111 = getelementptr inbounds i16, i16* %103, i32 1
  store i16* %incdec.ptr111, i16** %optr, align 4, !tbaa !6
  store i16 %call110, i16* %103, align 2, !tbaa !11
  %104 = bitcast i32* %sum84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #5
  br label %for.inc112

for.inc112:                                       ; preds = %for.end108
  %105 = load i32, i32* %i, align 4, !tbaa !2
  %add113 = add nsw i32 %105, 2
  store i32 %add113, i32* %i, align 4, !tbaa !2
  br label %for.cond80

for.end114:                                       ; preds = %for.cond80
  br label %for.cond115

for.cond115:                                      ; preds = %for.inc156, %for.end114
  %106 = load i32, i32* %i, align 4, !tbaa !2
  %107 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp116 = icmp slt i32 %106, %107
  br i1 %cmp116, label %for.body118, label %for.end158

for.body118:                                      ; preds = %for.cond115
  %108 = bitcast i32* %sum119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #5
  %109 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %110 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds i16, i16* %109, i32 %110
  %111 = load i16, i16* %arrayidx120, align 2, !tbaa !11
  %conv121 = zext i16 %111 to i32
  %112 = load i16*, i16** @highbd_down2_symodd.filter, align 4, !tbaa !6
  %arrayidx122 = getelementptr inbounds i16, i16* %112, i32 0
  %113 = load i16, i16* %arrayidx122, align 2, !tbaa !11
  %conv123 = sext i16 %113 to i32
  %mul124 = mul nsw i32 %conv121, %conv123
  %add125 = add nsw i32 64, %mul124
  store i32 %add125, i32* %sum119, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !2
  br label %for.cond126

for.cond126:                                      ; preds = %for.inc150, %for.body118
  %114 = load i32, i32* %j, align 4, !tbaa !2
  %cmp127 = icmp slt i32 %114, 4
  br i1 %cmp127, label %for.body129, label %for.end152

for.body129:                                      ; preds = %for.cond126
  %115 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %116 = load i32, i32* %i, align 4, !tbaa !2
  %117 = load i32, i32* %j, align 4, !tbaa !2
  %sub130 = sub nsw i32 %116, %117
  %arrayidx131 = getelementptr inbounds i16, i16* %115, i32 %sub130
  %118 = load i16, i16* %arrayidx131, align 2, !tbaa !11
  %conv132 = zext i16 %118 to i32
  %119 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %120 = load i32, i32* %i, align 4, !tbaa !2
  %121 = load i32, i32* %j, align 4, !tbaa !2
  %add133 = add nsw i32 %120, %121
  %122 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub134 = sub nsw i32 %122, 1
  %cmp135 = icmp slt i32 %add133, %sub134
  br i1 %cmp135, label %cond.true137, label %cond.false139

cond.true137:                                     ; preds = %for.body129
  %123 = load i32, i32* %i, align 4, !tbaa !2
  %124 = load i32, i32* %j, align 4, !tbaa !2
  %add138 = add nsw i32 %123, %124
  br label %cond.end141

cond.false139:                                    ; preds = %for.body129
  %125 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub140 = sub nsw i32 %125, 1
  br label %cond.end141

cond.end141:                                      ; preds = %cond.false139, %cond.true137
  %cond142 = phi i32 [ %add138, %cond.true137 ], [ %sub140, %cond.false139 ]
  %arrayidx143 = getelementptr inbounds i16, i16* %119, i32 %cond142
  %126 = load i16, i16* %arrayidx143, align 2, !tbaa !11
  %conv144 = zext i16 %126 to i32
  %add145 = add nsw i32 %conv132, %conv144
  %127 = load i16*, i16** @highbd_down2_symodd.filter, align 4, !tbaa !6
  %128 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i16, i16* %127, i32 %128
  %129 = load i16, i16* %arrayidx146, align 2, !tbaa !11
  %conv147 = sext i16 %129 to i32
  %mul148 = mul nsw i32 %add145, %conv147
  %130 = load i32, i32* %sum119, align 4, !tbaa !2
  %add149 = add nsw i32 %130, %mul148
  store i32 %add149, i32* %sum119, align 4, !tbaa !2
  br label %for.inc150

for.inc150:                                       ; preds = %cond.end141
  %131 = load i32, i32* %j, align 4, !tbaa !2
  %inc151 = add nsw i32 %131, 1
  store i32 %inc151, i32* %j, align 4, !tbaa !2
  br label %for.cond126

for.end152:                                       ; preds = %for.cond126
  %132 = load i32, i32* %sum119, align 4, !tbaa !2
  %shr153 = ashr i32 %132, 7
  store i32 %shr153, i32* %sum119, align 4, !tbaa !2
  %133 = load i32, i32* %sum119, align 4, !tbaa !2
  %134 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call154 = call zeroext i16 @clip_pixel_highbd(i32 %133, i32 %134)
  %135 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr155 = getelementptr inbounds i16, i16* %135, i32 1
  store i16* %incdec.ptr155, i16** %optr, align 4, !tbaa !6
  store i16 %call154, i16* %135, align 2, !tbaa !11
  %136 = bitcast i32* %sum119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #5
  br label %for.inc156

for.inc156:                                       ; preds = %for.end152
  %137 = load i32, i32* %i, align 4, !tbaa !2
  %add157 = add nsw i32 %137, 2
  store i32 %add157, i32* %i, align 4, !tbaa !2
  br label %for.cond115

for.end158:                                       ; preds = %for.cond115
  br label %if.end

if.end:                                           ; preds = %for.end158, %for.end37
  %138 = bitcast i32* %l2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #5
  %139 = bitcast i32* %l1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #5
  %140 = bitcast i16** %optr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #5
  %141 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #5
  %142 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #5
  %143 = bitcast i32* %filter_len_half to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #5
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_down2_symeven(i16* %input, i32 %length, i16* %output, i32 %bd) #0 {
entry:
  %input.addr = alloca i16*, align 4
  %length.addr = alloca i32, align 4
  %output.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %filter_len_half = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %optr = alloca i16*, align 4
  %l1 = alloca i32, align 4
  %l2 = alloca i32, align 4
  %sum = alloca i32, align 4
  %sum35 = alloca i32, align 4
  %sum72 = alloca i32, align 4
  %sum102 = alloca i32, align 4
  store i16* %input, i16** %input.addr, align 4, !tbaa !6
  store i32 %length, i32* %length.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = bitcast i32* %filter_len_half to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 4, i32* %filter_len_half, align 4, !tbaa !2
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i16** %optr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i16*, i16** %output.addr, align 4, !tbaa !6
  store i16* %4, i16** %optr, align 4, !tbaa !6
  %5 = bitcast i32* %l1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  store i32 4, i32* %l1, align 4, !tbaa !2
  %6 = bitcast i32* %l2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %7, 4
  store i32 %sub, i32* %l2, align 4, !tbaa !2
  %8 = load i32, i32* %l1, align 4, !tbaa !2
  %and = and i32 %8, 1
  %9 = load i32, i32* %l1, align 4, !tbaa !2
  %add = add nsw i32 %9, %and
  store i32 %add, i32* %l1, align 4, !tbaa !2
  %10 = load i32, i32* %l2, align 4, !tbaa !2
  %and1 = and i32 %10, 1
  %11 = load i32, i32* %l2, align 4, !tbaa !2
  %add2 = add nsw i32 %11, %and1
  store i32 %add2, i32* %l2, align 4, !tbaa !2
  %12 = load i32, i32* %l1, align 4, !tbaa !2
  %13 = load i32, i32* %l2, align 4, !tbaa !2
  %cmp = icmp sgt i32 %12, %13
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %if.then
  %14 = load i32, i32* %i, align 4, !tbaa !2
  %15 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp3 = icmp slt i32 %14, %15
  br i1 %cmp3, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %16 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  store i32 64, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %j, align 4, !tbaa !2
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %17 = load i32, i32* %j, align 4, !tbaa !2
  %cmp5 = icmp slt i32 %17, 4
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %18 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %19 = load i32, i32* %i, align 4, !tbaa !2
  %20 = load i32, i32* %j, align 4, !tbaa !2
  %sub7 = sub nsw i32 %19, %20
  %cmp8 = icmp sgt i32 0, %sub7
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body6
  br label %cond.end

cond.false:                                       ; preds = %for.body6
  %21 = load i32, i32* %i, align 4, !tbaa !2
  %22 = load i32, i32* %j, align 4, !tbaa !2
  %sub9 = sub nsw i32 %21, %22
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %sub9, %cond.false ]
  %arrayidx = getelementptr inbounds i16, i16* %18, i32 %cond
  %23 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %23 to i32
  %24 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %25 = load i32, i32* %i, align 4, !tbaa !2
  %add10 = add nsw i32 %25, 1
  %26 = load i32, i32* %j, align 4, !tbaa !2
  %add11 = add nsw i32 %add10, %26
  %27 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub12 = sub nsw i32 %27, 1
  %cmp13 = icmp slt i32 %add11, %sub12
  br i1 %cmp13, label %cond.true15, label %cond.false18

cond.true15:                                      ; preds = %cond.end
  %28 = load i32, i32* %i, align 4, !tbaa !2
  %add16 = add nsw i32 %28, 1
  %29 = load i32, i32* %j, align 4, !tbaa !2
  %add17 = add nsw i32 %add16, %29
  br label %cond.end20

cond.false18:                                     ; preds = %cond.end
  %30 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub19 = sub nsw i32 %30, 1
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false18, %cond.true15
  %cond21 = phi i32 [ %add17, %cond.true15 ], [ %sub19, %cond.false18 ]
  %arrayidx22 = getelementptr inbounds i16, i16* %24, i32 %cond21
  %31 = load i16, i16* %arrayidx22, align 2, !tbaa !11
  %conv23 = zext i16 %31 to i32
  %add24 = add nsw i32 %conv, %conv23
  %32 = load i16*, i16** @highbd_down2_symeven.filter, align 4, !tbaa !6
  %33 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i16, i16* %32, i32 %33
  %34 = load i16, i16* %arrayidx25, align 2, !tbaa !11
  %conv26 = sext i16 %34 to i32
  %mul = mul nsw i32 %add24, %conv26
  %35 = load i32, i32* %sum, align 4, !tbaa !2
  %add27 = add nsw i32 %35, %mul
  store i32 %add27, i32* %sum, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %cond.end20
  %36 = load i32, i32* %j, align 4, !tbaa !2
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %j, align 4, !tbaa !2
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %37 = load i32, i32* %sum, align 4, !tbaa !2
  %shr = ashr i32 %37, 7
  store i32 %shr, i32* %sum, align 4, !tbaa !2
  %38 = load i32, i32* %sum, align 4, !tbaa !2
  %39 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call = call zeroext i16 @clip_pixel_highbd(i32 %38, i32 %39)
  %40 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i16, i16* %40, i32 1
  store i16* %incdec.ptr, i16** %optr, align 4, !tbaa !6
  store i16 %call, i16* %40, align 2, !tbaa !11
  %41 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #5
  br label %for.inc28

for.inc28:                                        ; preds = %for.end
  %42 = load i32, i32* %i, align 4, !tbaa !2
  %add29 = add nsw i32 %42, 2
  store i32 %add29, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end30:                                        ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc65, %if.else
  %43 = load i32, i32* %i, align 4, !tbaa !2
  %44 = load i32, i32* %l1, align 4, !tbaa !2
  %cmp32 = icmp slt i32 %43, %44
  br i1 %cmp32, label %for.body34, label %for.end67

for.body34:                                       ; preds = %for.cond31
  %45 = bitcast i32* %sum35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #5
  store i32 64, i32* %sum35, align 4, !tbaa !2
  store i32 0, i32* %j, align 4, !tbaa !2
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc59, %for.body34
  %46 = load i32, i32* %j, align 4, !tbaa !2
  %cmp37 = icmp slt i32 %46, 4
  br i1 %cmp37, label %for.body39, label %for.end61

for.body39:                                       ; preds = %for.cond36
  %47 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %48 = load i32, i32* %i, align 4, !tbaa !2
  %49 = load i32, i32* %j, align 4, !tbaa !2
  %sub40 = sub nsw i32 %48, %49
  %cmp41 = icmp sgt i32 0, %sub40
  br i1 %cmp41, label %cond.true43, label %cond.false44

cond.true43:                                      ; preds = %for.body39
  br label %cond.end46

cond.false44:                                     ; preds = %for.body39
  %50 = load i32, i32* %i, align 4, !tbaa !2
  %51 = load i32, i32* %j, align 4, !tbaa !2
  %sub45 = sub nsw i32 %50, %51
  br label %cond.end46

cond.end46:                                       ; preds = %cond.false44, %cond.true43
  %cond47 = phi i32 [ 0, %cond.true43 ], [ %sub45, %cond.false44 ]
  %arrayidx48 = getelementptr inbounds i16, i16* %47, i32 %cond47
  %52 = load i16, i16* %arrayidx48, align 2, !tbaa !11
  %conv49 = zext i16 %52 to i32
  %53 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %54 = load i32, i32* %i, align 4, !tbaa !2
  %add50 = add nsw i32 %54, 1
  %55 = load i32, i32* %j, align 4, !tbaa !2
  %add51 = add nsw i32 %add50, %55
  %arrayidx52 = getelementptr inbounds i16, i16* %53, i32 %add51
  %56 = load i16, i16* %arrayidx52, align 2, !tbaa !11
  %conv53 = zext i16 %56 to i32
  %add54 = add nsw i32 %conv49, %conv53
  %57 = load i16*, i16** @highbd_down2_symeven.filter, align 4, !tbaa !6
  %58 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i16, i16* %57, i32 %58
  %59 = load i16, i16* %arrayidx55, align 2, !tbaa !11
  %conv56 = sext i16 %59 to i32
  %mul57 = mul nsw i32 %add54, %conv56
  %60 = load i32, i32* %sum35, align 4, !tbaa !2
  %add58 = add nsw i32 %60, %mul57
  store i32 %add58, i32* %sum35, align 4, !tbaa !2
  br label %for.inc59

for.inc59:                                        ; preds = %cond.end46
  %61 = load i32, i32* %j, align 4, !tbaa !2
  %inc60 = add nsw i32 %61, 1
  store i32 %inc60, i32* %j, align 4, !tbaa !2
  br label %for.cond36

for.end61:                                        ; preds = %for.cond36
  %62 = load i32, i32* %sum35, align 4, !tbaa !2
  %shr62 = ashr i32 %62, 7
  store i32 %shr62, i32* %sum35, align 4, !tbaa !2
  %63 = load i32, i32* %sum35, align 4, !tbaa !2
  %64 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call63 = call zeroext i16 @clip_pixel_highbd(i32 %63, i32 %64)
  %65 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr64 = getelementptr inbounds i16, i16* %65, i32 1
  store i16* %incdec.ptr64, i16** %optr, align 4, !tbaa !6
  store i16 %call63, i16* %65, align 2, !tbaa !11
  %66 = bitcast i32* %sum35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  br label %for.inc65

for.inc65:                                        ; preds = %for.end61
  %67 = load i32, i32* %i, align 4, !tbaa !2
  %add66 = add nsw i32 %67, 2
  store i32 %add66, i32* %i, align 4, !tbaa !2
  br label %for.cond31

for.end67:                                        ; preds = %for.cond31
  br label %for.cond68

for.cond68:                                       ; preds = %for.inc95, %for.end67
  %68 = load i32, i32* %i, align 4, !tbaa !2
  %69 = load i32, i32* %l2, align 4, !tbaa !2
  %cmp69 = icmp slt i32 %68, %69
  br i1 %cmp69, label %for.body71, label %for.end97

for.body71:                                       ; preds = %for.cond68
  %70 = bitcast i32* %sum72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #5
  store i32 64, i32* %sum72, align 4, !tbaa !2
  store i32 0, i32* %j, align 4, !tbaa !2
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc89, %for.body71
  %71 = load i32, i32* %j, align 4, !tbaa !2
  %cmp74 = icmp slt i32 %71, 4
  br i1 %cmp74, label %for.body76, label %for.end91

for.body76:                                       ; preds = %for.cond73
  %72 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %73 = load i32, i32* %i, align 4, !tbaa !2
  %74 = load i32, i32* %j, align 4, !tbaa !2
  %sub77 = sub nsw i32 %73, %74
  %arrayidx78 = getelementptr inbounds i16, i16* %72, i32 %sub77
  %75 = load i16, i16* %arrayidx78, align 2, !tbaa !11
  %conv79 = zext i16 %75 to i32
  %76 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %77 = load i32, i32* %i, align 4, !tbaa !2
  %add80 = add nsw i32 %77, 1
  %78 = load i32, i32* %j, align 4, !tbaa !2
  %add81 = add nsw i32 %add80, %78
  %arrayidx82 = getelementptr inbounds i16, i16* %76, i32 %add81
  %79 = load i16, i16* %arrayidx82, align 2, !tbaa !11
  %conv83 = zext i16 %79 to i32
  %add84 = add nsw i32 %conv79, %conv83
  %80 = load i16*, i16** @highbd_down2_symeven.filter, align 4, !tbaa !6
  %81 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i16, i16* %80, i32 %81
  %82 = load i16, i16* %arrayidx85, align 2, !tbaa !11
  %conv86 = sext i16 %82 to i32
  %mul87 = mul nsw i32 %add84, %conv86
  %83 = load i32, i32* %sum72, align 4, !tbaa !2
  %add88 = add nsw i32 %83, %mul87
  store i32 %add88, i32* %sum72, align 4, !tbaa !2
  br label %for.inc89

for.inc89:                                        ; preds = %for.body76
  %84 = load i32, i32* %j, align 4, !tbaa !2
  %inc90 = add nsw i32 %84, 1
  store i32 %inc90, i32* %j, align 4, !tbaa !2
  br label %for.cond73

for.end91:                                        ; preds = %for.cond73
  %85 = load i32, i32* %sum72, align 4, !tbaa !2
  %shr92 = ashr i32 %85, 7
  store i32 %shr92, i32* %sum72, align 4, !tbaa !2
  %86 = load i32, i32* %sum72, align 4, !tbaa !2
  %87 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call93 = call zeroext i16 @clip_pixel_highbd(i32 %86, i32 %87)
  %88 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr94 = getelementptr inbounds i16, i16* %88, i32 1
  store i16* %incdec.ptr94, i16** %optr, align 4, !tbaa !6
  store i16 %call93, i16* %88, align 2, !tbaa !11
  %89 = bitcast i32* %sum72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #5
  br label %for.inc95

for.inc95:                                        ; preds = %for.end91
  %90 = load i32, i32* %i, align 4, !tbaa !2
  %add96 = add nsw i32 %90, 2
  store i32 %add96, i32* %i, align 4, !tbaa !2
  br label %for.cond68

for.end97:                                        ; preds = %for.cond68
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc135, %for.end97
  %91 = load i32, i32* %i, align 4, !tbaa !2
  %92 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp99 = icmp slt i32 %91, %92
  br i1 %cmp99, label %for.body101, label %for.end137

for.body101:                                      ; preds = %for.cond98
  %93 = bitcast i32* %sum102 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #5
  store i32 64, i32* %sum102, align 4, !tbaa !2
  store i32 0, i32* %j, align 4, !tbaa !2
  br label %for.cond103

for.cond103:                                      ; preds = %for.inc129, %for.body101
  %94 = load i32, i32* %j, align 4, !tbaa !2
  %cmp104 = icmp slt i32 %94, 4
  br i1 %cmp104, label %for.body106, label %for.end131

for.body106:                                      ; preds = %for.cond103
  %95 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %96 = load i32, i32* %i, align 4, !tbaa !2
  %97 = load i32, i32* %j, align 4, !tbaa !2
  %sub107 = sub nsw i32 %96, %97
  %arrayidx108 = getelementptr inbounds i16, i16* %95, i32 %sub107
  %98 = load i16, i16* %arrayidx108, align 2, !tbaa !11
  %conv109 = zext i16 %98 to i32
  %99 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %100 = load i32, i32* %i, align 4, !tbaa !2
  %add110 = add nsw i32 %100, 1
  %101 = load i32, i32* %j, align 4, !tbaa !2
  %add111 = add nsw i32 %add110, %101
  %102 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub112 = sub nsw i32 %102, 1
  %cmp113 = icmp slt i32 %add111, %sub112
  br i1 %cmp113, label %cond.true115, label %cond.false118

cond.true115:                                     ; preds = %for.body106
  %103 = load i32, i32* %i, align 4, !tbaa !2
  %add116 = add nsw i32 %103, 1
  %104 = load i32, i32* %j, align 4, !tbaa !2
  %add117 = add nsw i32 %add116, %104
  br label %cond.end120

cond.false118:                                    ; preds = %for.body106
  %105 = load i32, i32* %length.addr, align 4, !tbaa !2
  %sub119 = sub nsw i32 %105, 1
  br label %cond.end120

cond.end120:                                      ; preds = %cond.false118, %cond.true115
  %cond121 = phi i32 [ %add117, %cond.true115 ], [ %sub119, %cond.false118 ]
  %arrayidx122 = getelementptr inbounds i16, i16* %99, i32 %cond121
  %106 = load i16, i16* %arrayidx122, align 2, !tbaa !11
  %conv123 = zext i16 %106 to i32
  %add124 = add nsw i32 %conv109, %conv123
  %107 = load i16*, i16** @highbd_down2_symeven.filter, align 4, !tbaa !6
  %108 = load i32, i32* %j, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i16, i16* %107, i32 %108
  %109 = load i16, i16* %arrayidx125, align 2, !tbaa !11
  %conv126 = sext i16 %109 to i32
  %mul127 = mul nsw i32 %add124, %conv126
  %110 = load i32, i32* %sum102, align 4, !tbaa !2
  %add128 = add nsw i32 %110, %mul127
  store i32 %add128, i32* %sum102, align 4, !tbaa !2
  br label %for.inc129

for.inc129:                                       ; preds = %cond.end120
  %111 = load i32, i32* %j, align 4, !tbaa !2
  %inc130 = add nsw i32 %111, 1
  store i32 %inc130, i32* %j, align 4, !tbaa !2
  br label %for.cond103

for.end131:                                       ; preds = %for.cond103
  %112 = load i32, i32* %sum102, align 4, !tbaa !2
  %shr132 = ashr i32 %112, 7
  store i32 %shr132, i32* %sum102, align 4, !tbaa !2
  %113 = load i32, i32* %sum102, align 4, !tbaa !2
  %114 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call133 = call zeroext i16 @clip_pixel_highbd(i32 %113, i32 %114)
  %115 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr134 = getelementptr inbounds i16, i16* %115, i32 1
  store i16* %incdec.ptr134, i16** %optr, align 4, !tbaa !6
  store i16 %call133, i16* %115, align 2, !tbaa !11
  %116 = bitcast i32* %sum102 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #5
  br label %for.inc135

for.inc135:                                       ; preds = %for.end131
  %117 = load i32, i32* %i, align 4, !tbaa !2
  %add136 = add nsw i32 %117, 2
  store i32 %add136, i32* %i, align 4, !tbaa !2
  br label %for.cond98

for.end137:                                       ; preds = %for.cond98
  br label %if.end

if.end:                                           ; preds = %for.end137, %for.end30
  %118 = bitcast i32* %l2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %l1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i16** %optr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  %121 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  %122 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  %123 = bitcast i32* %filter_len_half to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_interpolate(i16* %input, i32 %in_length, i16* %output, i32 %out_length, i32 %bd) #0 {
entry:
  %input.addr = alloca i16*, align 4
  %in_length.addr = alloca i32, align 4
  %output.addr = alloca i16*, align 4
  %out_length.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %interp_filters = alloca [8 x i16]*, align 4
  store i16* %input, i16** %input.addr, align 4, !tbaa !6
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !6
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = bitcast [8 x i16]** %interp_filters to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %2 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %call = call [8 x i16]* @choose_interp_filter(i32 %1, i32 %2)
  store [8 x i16]* %call, [8 x i16]** %interp_filters, align 4, !tbaa !6
  %3 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %4 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %5 = load i16*, i16** %output.addr, align 4, !tbaa !6
  %6 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %7 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %8 = load [8 x i16]*, [8 x i16]** %interp_filters, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x i16], [8 x i16]* %8, i32 0
  %arrayidx1 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx, i32 0, i32 0
  call void @highbd_interpolate_core(i16* %3, i32 %4, i16* %5, i32 %6, i32 %7, i16* %arrayidx1, i32 8)
  %9 = bitcast [8 x i16]** %interp_filters to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #5
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @clip_pixel_highbd(i32 %val, i32 %bd) #3 {
entry:
  %retval = alloca i16, align 2
  %val.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !2
  switch i32 %0, label %sw.default [
    i32 8, label %sw.bb
    i32 10, label %sw.bb1
    i32 12, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb
  %1 = load i32, i32* %val.addr, align 4, !tbaa !2
  %call = call i32 @clamp(i32 %1, i32 0, i32 255)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %retval, align 2
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !2
  %call2 = call i32 @clamp(i32 %2, i32 0, i32 1023)
  %conv3 = trunc i32 %call2 to i16
  store i16 %conv3, i16* %retval, align 2
  br label %return

sw.bb4:                                           ; preds = %entry
  %3 = load i32, i32* %val.addr, align 4, !tbaa !2
  %call5 = call i32 @clamp(i32 %3, i32 0, i32 4095)
  %conv6 = trunc i32 %call5 to i16
  store i16 %conv6, i16* %retval, align 2
  br label %return

return:                                           ; preds = %sw.bb4, %sw.bb1, %sw.default
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #3 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !2
  store i32 %low, i32* %low.addr, align 4, !tbaa !2
  store i32 %high, i32* %high.addr, align 4, !tbaa !2
  %0 = load i32, i32* %value.addr, align 4, !tbaa !2
  %1 = load i32, i32* %low.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !2
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !2
  %4 = load i32, i32* %high.addr, align 4, !tbaa !2
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !2
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: nounwind
define internal void @highbd_interpolate_core(i16* %input, i32 %in_length, i16* %output, i32 %out_length, i32 %bd, i16* %interp_filters, i32 %interp_taps) #0 {
entry:
  %input.addr = alloca i16*, align 4
  %in_length.addr = alloca i32, align 4
  %output.addr = alloca i16*, align 4
  %out_length.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %interp_filters.addr = alloca i16*, align 4
  %interp_taps.addr = alloca i32, align 4
  %delta = alloca i32, align 4
  %offset = alloca i32, align 4
  %optr = alloca i16*, align 4
  %x = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x2 = alloca i32, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  %int_pel = alloca i32, align 4
  %sub_pel = alloca i32, align 4
  %y = alloca i32, align 4
  %filter = alloca i16*, align 4
  %pk = alloca i32, align 4
  %filter83 = alloca i16*, align 4
  %filter128 = alloca i16*, align 4
  %filter163 = alloca i16*, align 4
  store i16* %input, i16** %input.addr, align 4, !tbaa !6
  store i32 %in_length, i32* %in_length.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !6
  store i32 %out_length, i32* %out_length.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  store i16* %interp_filters, i16** %interp_filters.addr, align 4, !tbaa !6
  store i32 %interp_taps, i32* %interp_taps.addr, align 4, !tbaa !2
  %0 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %shl = shl i32 %1, 14
  %2 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div = sdiv i32 %2, 2
  %add = add i32 %shl, %div
  %3 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div1 = udiv i32 %add, %3
  store i32 %div1, i32* %delta, align 4, !tbaa !2
  %4 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %6 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp = icmp sgt i32 %5, %6
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %7 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %8 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %7, %8
  %shl2 = shl i32 %sub, 13
  %9 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div3 = sdiv i32 %9, 2
  %add4 = add nsw i32 %shl2, %div3
  %10 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div5 = sdiv i32 %add4, %10
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %12 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub6 = sub nsw i32 %11, %12
  %shl7 = shl i32 %sub6, 13
  %13 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div8 = sdiv i32 %13, 2
  %add9 = add nsw i32 %shl7, %div8
  %sub10 = sub nsw i32 0, %add9
  %14 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %div11 = sdiv i32 %sub10, %14
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %div5, %cond.true ], [ %div11, %cond.false ]
  store i32 %cond, i32* %offset, align 4, !tbaa !2
  %15 = bitcast i16** %optr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load i16*, i16** %output.addr, align 4, !tbaa !6
  store i16* %16, i16** %optr, align 4, !tbaa !6
  %17 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = bitcast i32* %x1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  %21 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = bitcast i32* %int_pel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = bitcast i32* %sub_pel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  %24 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  store i32 0, i32* %x, align 4, !tbaa !2
  %25 = load i32, i32* %offset, align 4, !tbaa !2
  %add12 = add nsw i32 %25, 128
  store i32 %add12, i32* %y, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %cond.end
  %26 = load i32, i32* %y, align 4, !tbaa !2
  %shr = ashr i32 %26, 14
  %27 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div13 = sdiv i32 %27, 2
  %sub14 = sub nsw i32 %div13, 1
  %cmp15 = icmp slt i32 %shr, %sub14
  br i1 %cmp15, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %28 = load i32, i32* %x, align 4, !tbaa !2
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %x, align 4, !tbaa !2
  %29 = load i32, i32* %delta, align 4, !tbaa !2
  %30 = load i32, i32* %y, align 4, !tbaa !2
  %add16 = add nsw i32 %30, %29
  store i32 %add16, i32* %y, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %31 = load i32, i32* %x, align 4, !tbaa !2
  store i32 %31, i32* %x1, align 4, !tbaa !2
  %32 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %sub17 = sub nsw i32 %32, 1
  store i32 %sub17, i32* %x, align 4, !tbaa !2
  %33 = load i32, i32* %delta, align 4, !tbaa !2
  %34 = load i32, i32* %x, align 4, !tbaa !2
  %mul = mul nsw i32 %33, %34
  %35 = load i32, i32* %offset, align 4, !tbaa !2
  %add18 = add nsw i32 %mul, %35
  %add19 = add nsw i32 %add18, 128
  store i32 %add19, i32* %y, align 4, !tbaa !2
  br label %while.cond20

while.cond20:                                     ; preds = %while.body25, %while.end
  %36 = load i32, i32* %y, align 4, !tbaa !2
  %shr21 = ashr i32 %36, 14
  %37 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div22 = sdiv i32 %37, 2
  %add23 = add nsw i32 %shr21, %div22
  %38 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %cmp24 = icmp sge i32 %add23, %38
  br i1 %cmp24, label %while.body25, label %while.end27

while.body25:                                     ; preds = %while.cond20
  %39 = load i32, i32* %x, align 4, !tbaa !2
  %dec = add nsw i32 %39, -1
  store i32 %dec, i32* %x, align 4, !tbaa !2
  %40 = load i32, i32* %delta, align 4, !tbaa !2
  %41 = load i32, i32* %y, align 4, !tbaa !2
  %sub26 = sub nsw i32 %41, %40
  store i32 %sub26, i32* %y, align 4, !tbaa !2
  br label %while.cond20

while.end27:                                      ; preds = %while.cond20
  %42 = load i32, i32* %x, align 4, !tbaa !2
  store i32 %42, i32* %x2, align 4, !tbaa !2
  %43 = load i32, i32* %x1, align 4, !tbaa !2
  %44 = load i32, i32* %x2, align 4, !tbaa !2
  %cmp28 = icmp sgt i32 %43, %44
  br i1 %cmp28, label %if.then, label %if.else

if.then:                                          ; preds = %while.end27
  store i32 0, i32* %x, align 4, !tbaa !2
  %45 = load i32, i32* %offset, align 4, !tbaa !2
  %add29 = add nsw i32 %45, 128
  store i32 %add29, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc71, %if.then
  %46 = load i32, i32* %x, align 4, !tbaa !2
  %47 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp30 = icmp slt i32 %46, %47
  br i1 %cmp30, label %for.body, label %for.end74

for.body:                                         ; preds = %for.cond
  %48 = load i32, i32* %y, align 4, !tbaa !2
  %shr31 = ashr i32 %48, 14
  store i32 %shr31, i32* %int_pel, align 4, !tbaa !2
  %49 = load i32, i32* %y, align 4, !tbaa !2
  %shr32 = ashr i32 %49, 8
  %and = and i32 %shr32, 63
  store i32 %and, i32* %sub_pel, align 4, !tbaa !2
  %50 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #5
  %51 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %52 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %53 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul33 = mul nsw i32 %52, %53
  %arrayidx = getelementptr inbounds i16, i16* %51, i32 %mul33
  store i16* %arrayidx, i16** %filter, align 4, !tbaa !6
  store i32 0, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc, %for.body
  %54 = load i32, i32* %k, align 4, !tbaa !2
  %55 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp35 = icmp slt i32 %54, %55
  br i1 %cmp35, label %for.body36, label %for.end

for.body36:                                       ; preds = %for.cond34
  %56 = bitcast i32* %pk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #5
  %57 = load i32, i32* %int_pel, align 4, !tbaa !2
  %58 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div37 = sdiv i32 %58, 2
  %sub38 = sub nsw i32 %57, %div37
  %add39 = add nsw i32 %sub38, 1
  %59 = load i32, i32* %k, align 4, !tbaa !2
  %add40 = add nsw i32 %add39, %59
  store i32 %add40, i32* %pk, align 4, !tbaa !2
  %60 = load i16*, i16** %filter, align 4, !tbaa !6
  %61 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i16, i16* %60, i32 %61
  %62 = load i16, i16* %arrayidx41, align 2, !tbaa !11
  %conv = sext i16 %62 to i32
  %63 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %64 = load i32, i32* %pk, align 4, !tbaa !2
  %65 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub42 = sub nsw i32 %65, 1
  %cmp43 = icmp slt i32 %64, %sub42
  br i1 %cmp43, label %cond.true45, label %cond.false46

cond.true45:                                      ; preds = %for.body36
  %66 = load i32, i32* %pk, align 4, !tbaa !2
  br label %cond.end48

cond.false46:                                     ; preds = %for.body36
  %67 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub47 = sub nsw i32 %67, 1
  br label %cond.end48

cond.end48:                                       ; preds = %cond.false46, %cond.true45
  %cond49 = phi i32 [ %66, %cond.true45 ], [ %sub47, %cond.false46 ]
  %cmp50 = icmp sgt i32 %cond49, 0
  br i1 %cmp50, label %cond.true52, label %cond.false61

cond.true52:                                      ; preds = %cond.end48
  %68 = load i32, i32* %pk, align 4, !tbaa !2
  %69 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub53 = sub nsw i32 %69, 1
  %cmp54 = icmp slt i32 %68, %sub53
  br i1 %cmp54, label %cond.true56, label %cond.false57

cond.true56:                                      ; preds = %cond.true52
  %70 = load i32, i32* %pk, align 4, !tbaa !2
  br label %cond.end59

cond.false57:                                     ; preds = %cond.true52
  %71 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub58 = sub nsw i32 %71, 1
  br label %cond.end59

cond.end59:                                       ; preds = %cond.false57, %cond.true56
  %cond60 = phi i32 [ %70, %cond.true56 ], [ %sub58, %cond.false57 ]
  br label %cond.end62

cond.false61:                                     ; preds = %cond.end48
  br label %cond.end62

cond.end62:                                       ; preds = %cond.false61, %cond.end59
  %cond63 = phi i32 [ %cond60, %cond.end59 ], [ 0, %cond.false61 ]
  %arrayidx64 = getelementptr inbounds i16, i16* %63, i32 %cond63
  %72 = load i16, i16* %arrayidx64, align 2, !tbaa !11
  %conv65 = zext i16 %72 to i32
  %mul66 = mul nsw i32 %conv, %conv65
  %73 = load i32, i32* %sum, align 4, !tbaa !2
  %add67 = add nsw i32 %73, %mul66
  store i32 %add67, i32* %sum, align 4, !tbaa !2
  %74 = bitcast i32* %pk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end62
  %75 = load i32, i32* %k, align 4, !tbaa !2
  %inc68 = add nsw i32 %75, 1
  store i32 %inc68, i32* %k, align 4, !tbaa !2
  br label %for.cond34

for.end:                                          ; preds = %for.cond34
  %76 = load i32, i32* %sum, align 4, !tbaa !2
  %add69 = add nsw i32 %76, 64
  %shr70 = ashr i32 %add69, 7
  %77 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call = call zeroext i16 @clip_pixel_highbd(i32 %shr70, i32 %77)
  %78 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i16, i16* %78, i32 1
  store i16* %incdec.ptr, i16** %optr, align 4, !tbaa !6
  store i16 %call, i16* %78, align 2, !tbaa !11
  %79 = bitcast i16** %filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #5
  br label %for.inc71

for.inc71:                                        ; preds = %for.end
  %80 = load i32, i32* %x, align 4, !tbaa !2
  %inc72 = add nsw i32 %80, 1
  store i32 %inc72, i32* %x, align 4, !tbaa !2
  %81 = load i32, i32* %delta, align 4, !tbaa !2
  %82 = load i32, i32* %y, align 4, !tbaa !2
  %add73 = add nsw i32 %82, %81
  store i32 %add73, i32* %y, align 4, !tbaa !2
  br label %for.cond

for.end74:                                        ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %while.end27
  store i32 0, i32* %x, align 4, !tbaa !2
  %83 = load i32, i32* %offset, align 4, !tbaa !2
  %add75 = add nsw i32 %83, 128
  store i32 %add75, i32* %y, align 4, !tbaa !2
  br label %for.cond76

for.cond76:                                       ; preds = %for.inc117, %if.else
  %84 = load i32, i32* %x, align 4, !tbaa !2
  %85 = load i32, i32* %x1, align 4, !tbaa !2
  %cmp77 = icmp slt i32 %84, %85
  br i1 %cmp77, label %for.body79, label %for.end120

for.body79:                                       ; preds = %for.cond76
  %86 = load i32, i32* %y, align 4, !tbaa !2
  %shr80 = ashr i32 %86, 14
  store i32 %shr80, i32* %int_pel, align 4, !tbaa !2
  %87 = load i32, i32* %y, align 4, !tbaa !2
  %shr81 = ashr i32 %87, 8
  %and82 = and i32 %shr81, 63
  store i32 %and82, i32* %sub_pel, align 4, !tbaa !2
  %88 = bitcast i16** %filter83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #5
  %89 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %90 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %91 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul84 = mul nsw i32 %90, %91
  %arrayidx85 = getelementptr inbounds i16, i16* %89, i32 %mul84
  store i16* %arrayidx85, i16** %filter83, align 4, !tbaa !6
  store i32 0, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond86

for.cond86:                                       ; preds = %for.inc110, %for.body79
  %92 = load i32, i32* %k, align 4, !tbaa !2
  %93 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp87 = icmp slt i32 %92, %93
  br i1 %cmp87, label %for.body89, label %for.end112

for.body89:                                       ; preds = %for.cond86
  %94 = load i16*, i16** %filter83, align 4, !tbaa !6
  %95 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i16, i16* %94, i32 %95
  %96 = load i16, i16* %arrayidx90, align 2, !tbaa !11
  %conv91 = sext i16 %96 to i32
  %97 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %98 = load i32, i32* %int_pel, align 4, !tbaa !2
  %99 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div92 = sdiv i32 %99, 2
  %sub93 = sub nsw i32 %98, %div92
  %add94 = add nsw i32 %sub93, 1
  %100 = load i32, i32* %k, align 4, !tbaa !2
  %add95 = add nsw i32 %add94, %100
  %cmp96 = icmp sgt i32 %add95, 0
  br i1 %cmp96, label %cond.true98, label %cond.false103

cond.true98:                                      ; preds = %for.body89
  %101 = load i32, i32* %int_pel, align 4, !tbaa !2
  %102 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div99 = sdiv i32 %102, 2
  %sub100 = sub nsw i32 %101, %div99
  %add101 = add nsw i32 %sub100, 1
  %103 = load i32, i32* %k, align 4, !tbaa !2
  %add102 = add nsw i32 %add101, %103
  br label %cond.end104

cond.false103:                                    ; preds = %for.body89
  br label %cond.end104

cond.end104:                                      ; preds = %cond.false103, %cond.true98
  %cond105 = phi i32 [ %add102, %cond.true98 ], [ 0, %cond.false103 ]
  %arrayidx106 = getelementptr inbounds i16, i16* %97, i32 %cond105
  %104 = load i16, i16* %arrayidx106, align 2, !tbaa !11
  %conv107 = zext i16 %104 to i32
  %mul108 = mul nsw i32 %conv91, %conv107
  %105 = load i32, i32* %sum, align 4, !tbaa !2
  %add109 = add nsw i32 %105, %mul108
  store i32 %add109, i32* %sum, align 4, !tbaa !2
  br label %for.inc110

for.inc110:                                       ; preds = %cond.end104
  %106 = load i32, i32* %k, align 4, !tbaa !2
  %inc111 = add nsw i32 %106, 1
  store i32 %inc111, i32* %k, align 4, !tbaa !2
  br label %for.cond86

for.end112:                                       ; preds = %for.cond86
  %107 = load i32, i32* %sum, align 4, !tbaa !2
  %add113 = add nsw i32 %107, 64
  %shr114 = ashr i32 %add113, 7
  %108 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call115 = call zeroext i16 @clip_pixel_highbd(i32 %shr114, i32 %108)
  %109 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr116 = getelementptr inbounds i16, i16* %109, i32 1
  store i16* %incdec.ptr116, i16** %optr, align 4, !tbaa !6
  store i16 %call115, i16* %109, align 2, !tbaa !11
  %110 = bitcast i16** %filter83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #5
  br label %for.inc117

for.inc117:                                       ; preds = %for.end112
  %111 = load i32, i32* %x, align 4, !tbaa !2
  %inc118 = add nsw i32 %111, 1
  store i32 %inc118, i32* %x, align 4, !tbaa !2
  %112 = load i32, i32* %delta, align 4, !tbaa !2
  %113 = load i32, i32* %y, align 4, !tbaa !2
  %add119 = add nsw i32 %113, %112
  store i32 %add119, i32* %y, align 4, !tbaa !2
  br label %for.cond76

for.end120:                                       ; preds = %for.cond76
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc152, %for.end120
  %114 = load i32, i32* %x, align 4, !tbaa !2
  %115 = load i32, i32* %x2, align 4, !tbaa !2
  %cmp122 = icmp sle i32 %114, %115
  br i1 %cmp122, label %for.body124, label %for.end155

for.body124:                                      ; preds = %for.cond121
  %116 = load i32, i32* %y, align 4, !tbaa !2
  %shr125 = ashr i32 %116, 14
  store i32 %shr125, i32* %int_pel, align 4, !tbaa !2
  %117 = load i32, i32* %y, align 4, !tbaa !2
  %shr126 = ashr i32 %117, 8
  %and127 = and i32 %shr126, 63
  store i32 %and127, i32* %sub_pel, align 4, !tbaa !2
  %118 = bitcast i16** %filter128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #5
  %119 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %120 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %121 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul129 = mul nsw i32 %120, %121
  %arrayidx130 = getelementptr inbounds i16, i16* %119, i32 %mul129
  store i16* %arrayidx130, i16** %filter128, align 4, !tbaa !6
  store i32 0, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond131

for.cond131:                                      ; preds = %for.inc145, %for.body124
  %122 = load i32, i32* %k, align 4, !tbaa !2
  %123 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp132 = icmp slt i32 %122, %123
  br i1 %cmp132, label %for.body134, label %for.end147

for.body134:                                      ; preds = %for.cond131
  %124 = load i16*, i16** %filter128, align 4, !tbaa !6
  %125 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i16, i16* %124, i32 %125
  %126 = load i16, i16* %arrayidx135, align 2, !tbaa !11
  %conv136 = sext i16 %126 to i32
  %127 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %128 = load i32, i32* %int_pel, align 4, !tbaa !2
  %129 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div137 = sdiv i32 %129, 2
  %sub138 = sub nsw i32 %128, %div137
  %add139 = add nsw i32 %sub138, 1
  %130 = load i32, i32* %k, align 4, !tbaa !2
  %add140 = add nsw i32 %add139, %130
  %arrayidx141 = getelementptr inbounds i16, i16* %127, i32 %add140
  %131 = load i16, i16* %arrayidx141, align 2, !tbaa !11
  %conv142 = zext i16 %131 to i32
  %mul143 = mul nsw i32 %conv136, %conv142
  %132 = load i32, i32* %sum, align 4, !tbaa !2
  %add144 = add nsw i32 %132, %mul143
  store i32 %add144, i32* %sum, align 4, !tbaa !2
  br label %for.inc145

for.inc145:                                       ; preds = %for.body134
  %133 = load i32, i32* %k, align 4, !tbaa !2
  %inc146 = add nsw i32 %133, 1
  store i32 %inc146, i32* %k, align 4, !tbaa !2
  br label %for.cond131

for.end147:                                       ; preds = %for.cond131
  %134 = load i32, i32* %sum, align 4, !tbaa !2
  %add148 = add nsw i32 %134, 64
  %shr149 = ashr i32 %add148, 7
  %135 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call150 = call zeroext i16 @clip_pixel_highbd(i32 %shr149, i32 %135)
  %136 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr151 = getelementptr inbounds i16, i16* %136, i32 1
  store i16* %incdec.ptr151, i16** %optr, align 4, !tbaa !6
  store i16 %call150, i16* %136, align 2, !tbaa !11
  %137 = bitcast i16** %filter128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #5
  br label %for.inc152

for.inc152:                                       ; preds = %for.end147
  %138 = load i32, i32* %x, align 4, !tbaa !2
  %inc153 = add nsw i32 %138, 1
  store i32 %inc153, i32* %x, align 4, !tbaa !2
  %139 = load i32, i32* %delta, align 4, !tbaa !2
  %140 = load i32, i32* %y, align 4, !tbaa !2
  %add154 = add nsw i32 %140, %139
  store i32 %add154, i32* %y, align 4, !tbaa !2
  br label %for.cond121

for.end155:                                       ; preds = %for.cond121
  br label %for.cond156

for.cond156:                                      ; preds = %for.inc199, %for.end155
  %141 = load i32, i32* %x, align 4, !tbaa !2
  %142 = load i32, i32* %out_length.addr, align 4, !tbaa !2
  %cmp157 = icmp slt i32 %141, %142
  br i1 %cmp157, label %for.body159, label %for.end202

for.body159:                                      ; preds = %for.cond156
  %143 = load i32, i32* %y, align 4, !tbaa !2
  %shr160 = ashr i32 %143, 14
  store i32 %shr160, i32* %int_pel, align 4, !tbaa !2
  %144 = load i32, i32* %y, align 4, !tbaa !2
  %shr161 = ashr i32 %144, 8
  %and162 = and i32 %shr161, 63
  store i32 %and162, i32* %sub_pel, align 4, !tbaa !2
  %145 = bitcast i16** %filter163 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #5
  %146 = load i16*, i16** %interp_filters.addr, align 4, !tbaa !6
  %147 = load i32, i32* %sub_pel, align 4, !tbaa !2
  %148 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %mul164 = mul nsw i32 %147, %148
  %arrayidx165 = getelementptr inbounds i16, i16* %146, i32 %mul164
  store i16* %arrayidx165, i16** %filter163, align 4, !tbaa !6
  store i32 0, i32* %sum, align 4, !tbaa !2
  store i32 0, i32* %k, align 4, !tbaa !2
  br label %for.cond166

for.cond166:                                      ; preds = %for.inc192, %for.body159
  %149 = load i32, i32* %k, align 4, !tbaa !2
  %150 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %cmp167 = icmp slt i32 %149, %150
  br i1 %cmp167, label %for.body169, label %for.end194

for.body169:                                      ; preds = %for.cond166
  %151 = load i16*, i16** %filter163, align 4, !tbaa !6
  %152 = load i32, i32* %k, align 4, !tbaa !2
  %arrayidx170 = getelementptr inbounds i16, i16* %151, i32 %152
  %153 = load i16, i16* %arrayidx170, align 2, !tbaa !11
  %conv171 = sext i16 %153 to i32
  %154 = load i16*, i16** %input.addr, align 4, !tbaa !6
  %155 = load i32, i32* %int_pel, align 4, !tbaa !2
  %156 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div172 = sdiv i32 %156, 2
  %sub173 = sub nsw i32 %155, %div172
  %add174 = add nsw i32 %sub173, 1
  %157 = load i32, i32* %k, align 4, !tbaa !2
  %add175 = add nsw i32 %add174, %157
  %158 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub176 = sub nsw i32 %158, 1
  %cmp177 = icmp slt i32 %add175, %sub176
  br i1 %cmp177, label %cond.true179, label %cond.false184

cond.true179:                                     ; preds = %for.body169
  %159 = load i32, i32* %int_pel, align 4, !tbaa !2
  %160 = load i32, i32* %interp_taps.addr, align 4, !tbaa !2
  %div180 = sdiv i32 %160, 2
  %sub181 = sub nsw i32 %159, %div180
  %add182 = add nsw i32 %sub181, 1
  %161 = load i32, i32* %k, align 4, !tbaa !2
  %add183 = add nsw i32 %add182, %161
  br label %cond.end186

cond.false184:                                    ; preds = %for.body169
  %162 = load i32, i32* %in_length.addr, align 4, !tbaa !2
  %sub185 = sub nsw i32 %162, 1
  br label %cond.end186

cond.end186:                                      ; preds = %cond.false184, %cond.true179
  %cond187 = phi i32 [ %add183, %cond.true179 ], [ %sub185, %cond.false184 ]
  %arrayidx188 = getelementptr inbounds i16, i16* %154, i32 %cond187
  %163 = load i16, i16* %arrayidx188, align 2, !tbaa !11
  %conv189 = zext i16 %163 to i32
  %mul190 = mul nsw i32 %conv171, %conv189
  %164 = load i32, i32* %sum, align 4, !tbaa !2
  %add191 = add nsw i32 %164, %mul190
  store i32 %add191, i32* %sum, align 4, !tbaa !2
  br label %for.inc192

for.inc192:                                       ; preds = %cond.end186
  %165 = load i32, i32* %k, align 4, !tbaa !2
  %inc193 = add nsw i32 %165, 1
  store i32 %inc193, i32* %k, align 4, !tbaa !2
  br label %for.cond166

for.end194:                                       ; preds = %for.cond166
  %166 = load i32, i32* %sum, align 4, !tbaa !2
  %add195 = add nsw i32 %166, 64
  %shr196 = ashr i32 %add195, 7
  %167 = load i32, i32* %bd.addr, align 4, !tbaa !2
  %call197 = call zeroext i16 @clip_pixel_highbd(i32 %shr196, i32 %167)
  %168 = load i16*, i16** %optr, align 4, !tbaa !6
  %incdec.ptr198 = getelementptr inbounds i16, i16* %168, i32 1
  store i16* %incdec.ptr198, i16** %optr, align 4, !tbaa !6
  store i16 %call197, i16* %168, align 2, !tbaa !11
  %169 = bitcast i16** %filter163 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #5
  br label %for.inc199

for.inc199:                                       ; preds = %for.end194
  %170 = load i32, i32* %x, align 4, !tbaa !2
  %inc200 = add nsw i32 %170, 1
  store i32 %inc200, i32* %x, align 4, !tbaa !2
  %171 = load i32, i32* %delta, align 4, !tbaa !2
  %172 = load i32, i32* %y, align 4, !tbaa !2
  %add201 = add nsw i32 %172, %171
  store i32 %add201, i32* %y, align 4, !tbaa !2
  br label %for.cond156

for.end202:                                       ; preds = %for.cond156
  br label %if.end

if.end:                                           ; preds = %for.end202, %for.end74
  %173 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #5
  %174 = bitcast i32* %sub_pel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #5
  %175 = bitcast i32* %int_pel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #5
  %176 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #5
  %177 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #5
  %178 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #5
  %179 = bitcast i32* %x1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #5
  %180 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #5
  %181 = bitcast i16** %optr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #5
  %182 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #5
  %183 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #5
  ret void
}

declare i8* @aom_memset16(i8*, i32, i32) #2

declare void @av1_highbd_convolve_horiz_rs_c(i16*, i32, i16*, i32, i32, i32, i16*, i32, i32, i32) #2

declare void @av1_convolve_horiz_rs_c(i8*, i32, i8*, i32, i32, i32, i16*, i32, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"any pointer", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"double", !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"short", !4, i64 0}
!13 = !{!14, !3, i64 140}
!14 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !3, i64 52, !4, i64 56, !7, i64 68, !3, i64 72, !7, i64 76, !15, i64 80, !3, i64 84, !15, i64 88, !3, i64 92, !3, i64 96, !3, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !3, i64 128, !3, i64 132, !3, i64 136, !3, i64 140, !7, i64 144}
!15 = !{!"long", !4, i64 0}
!16 = !{!17, !3, i64 16288}
!17 = !{!"AV1Common", !18, i64 0, !20, i64 40, !3, i64 288, !3, i64 292, !3, i64 296, !3, i64 300, !3, i64 304, !3, i64 308, !4, i64 312, !21, i64 313, !4, i64 316, !3, i64 448, !7, i64 452, !7, i64 456, !4, i64 460, !22, i64 492, !4, i64 580, !4, i64 1284, !3, i64 1316, !3, i64 1320, !3, i64 1324, !23, i64 1328, !24, i64 1356, !25, i64 1420, !26, i64 10676, !7, i64 10848, !27, i64 10864, !28, i64 14704, !4, i64 14740, !7, i64 14872, !7, i64 14876, !14, i64 14880, !29, i64 15028, !30, i64 15168, !31, i64 15816, !4, i64 15836, !32, i64 16192, !7, i64 18128, !7, i64 18132, !35, i64 18136, !7, i64 18724, !36, i64 18728, !3, i64 18760, !4, i64 18764, !7, i64 18796, !3, i64 18800, !4, i64 18804, !4, i64 18836, !3, i64 18844, !3, i64 18848, !3, i64 18852, !3, i64 18856}
!18 = !{!"", !4, i64 0, !4, i64 1, !3, i64 4, !3, i64 8, !3, i64 12, !19, i64 16, !3, i64 32, !3, i64 36}
!19 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!20 = !{!"aom_internal_error_info", !4, i64 0, !3, i64 4, !4, i64 8, !3, i64 88, !4, i64 92}
!21 = !{!"_Bool", !4, i64 0}
!22 = !{!"scale_factors", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56}
!23 = !{!"", !21, i64 0, !21, i64 1, !21, i64 2, !21, i64 3, !21, i64 4, !21, i64 5, !21, i64 6, !21, i64 7, !21, i64 8, !21, i64 9, !21, i64 10, !21, i64 11, !4, i64 12, !4, i64 13, !3, i64 16, !3, i64 20, !4, i64 24}
!24 = !{!"CommonModeInfoParams", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !3, i64 24, !3, i64 28, !4, i64 32, !7, i64 36, !3, i64 40, !3, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60}
!25 = !{!"CommonQuantParams", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !21, i64 9240, !3, i64 9244, !3, i64 9248, !3, i64 9252}
!26 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !3, i64 164, !4, i64 168}
!27 = !{!"", !4, i64 0, !4, i64 3072}
!28 = !{!"loopfilter", !4, i64 0, !3, i64 8, !3, i64 12, !3, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !3, i64 32}
!29 = !{!"", !3, i64 0, !3, i64 4, !4, i64 8, !4, i64 72, !3, i64 136}
!30 = !{!"", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 120, !4, i64 124, !3, i64 204, !4, i64 208, !3, i64 288, !3, i64 292, !3, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !3, i64 596, !3, i64 600, !3, i64 604, !3, i64 608, !3, i64 612, !3, i64 616, !3, i64 620, !3, i64 624, !3, i64 628, !3, i64 632, !3, i64 636, !3, i64 640, !12, i64 644}
!31 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!32 = !{!"SequenceHeader", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !4, i64 16, !3, i64 20, !3, i64 24, !4, i64 28, !3, i64 32, !3, i64 36, !19, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !3, i64 92, !3, i64 96, !3, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !3, i64 112, !4, i64 116, !3, i64 244, !33, i64 248, !4, i64 264, !34, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!33 = !{!"aom_timing", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!34 = !{!"aom_dec_model_info", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!35 = !{!"CommonTileParams", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !4, i64 60, !4, i64 320, !3, i64 580, !3, i64 584}
!36 = !{!"CommonContexts", !7, i64 0, !4, i64 4, !7, i64 16, !3, i64 20, !3, i64 24, !3, i64 28}
!37 = !{!17, !3, i64 288}
!38 = !{!17, !3, i64 304}
!39 = !{!17, !4, i64 312}
!40 = !{!17, !3, i64 18136}
!41 = !{!42, !3, i64 8}
!42 = !{!"TileInfo", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20}
!43 = !{!42, !3, i64 12}
!44 = !{!17, !4, i64 16268}
!45 = !{!17, !4, i64 16264}
!46 = !{!17, !4, i64 16269}
!47 = !{!17, !3, i64 292}
!48 = !{!17, !3, i64 1348}
!49 = !{!17, !7, i64 456}
!50 = !{!32, !3, i64 96}
!51 = !{!32, !3, i64 100}
!52 = !{!32, !4, i64 76}
!53 = !{!54, !7, i64 8}
!54 = !{!"BufferPool", !7, i64 0, !7, i64 4, !7, i64 8, !4, i64 12, !55, i64 363660}
!55 = !{!"InternalFrameBufferList", !3, i64 0, !7, i64 4}
!56 = !{!54, !7, i64 4}
!57 = !{!54, !7, i64 0}
!58 = !{!17, !3, i64 308}
!59 = !{!14, !3, i64 100}
!60 = !{!14, !4, i64 104}
!61 = !{!14, !4, i64 108}
!62 = !{!14, !4, i64 112}
!63 = !{!14, !4, i64 116}
!64 = !{!14, !4, i64 120}
!65 = !{!14, !4, i64 124}
