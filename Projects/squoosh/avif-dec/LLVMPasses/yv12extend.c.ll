; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_scale/generic/yv12extend.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_scale/generic/yv12extend.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }

; Function Attrs: nounwind
define hidden void @aom_yv12_extend_frame_borders_c(%struct.yv12_buffer_config* %ybf, i32 %num_planes) #0 {
entry:
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %num_planes.addr = alloca i32, align 4
  %plane = alloca i32, align 4
  %is_uv = alloca i32, align 4
  %plane_border = alloca i32, align 4
  %plane13 = alloca i32, align 4
  %is_uv19 = alloca i32, align 4
  %plane_border22 = alloca i32, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %0, i32 0, i32 26
  %1 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %1, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %plane, align 4, !tbaa !6
  %4 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i32, i32* %plane, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %7, 0
  %conv = zext i1 %cmp1 to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !6
  %8 = bitcast i32* %plane_border to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %border = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %9, i32 0, i32 12
  %10 = load i32, i32* %border, align 4, !tbaa !11
  %11 = load i32, i32* %is_uv, align 4, !tbaa !6
  %shr = ashr i32 %10, %11
  store i32 %shr, i32* %plane_border, align 4, !tbaa !6
  %12 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %13 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %12, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %13 to [3 x i8*]*
  %14 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %14
  %15 = load i8*, i8** %arrayidx, align 4, !tbaa !12
  %16 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %17 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %16, i32 0, i32 4
  %strides = bitcast %union.anon.6* %17 to [2 x i32]*
  %18 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx2, align 4, !tbaa !12
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %21 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %20, i32 0, i32 2
  %crop_widths = bitcast %union.anon.2* %21 to [2 x i32]*
  %22 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths, i32 0, i32 %22
  %23 = load i32, i32* %arrayidx3, align 4, !tbaa !12
  %24 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %25 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %24, i32 0, i32 3
  %crop_heights = bitcast %union.anon.4* %25 to [2 x i32]*
  %26 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights, i32 0, i32 %26
  %27 = load i32, i32* %arrayidx4, align 4, !tbaa !12
  %28 = load i32, i32* %plane_border, align 4, !tbaa !6
  %29 = load i32, i32* %plane_border, align 4, !tbaa !6
  %30 = load i32, i32* %plane_border, align 4, !tbaa !6
  %31 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %32 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %31, i32 0, i32 1
  %heights = bitcast %union.anon.0* %32 to [2 x i32]*
  %33 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [2 x i32], [2 x i32]* %heights, i32 0, i32 %33
  %34 = load i32, i32* %arrayidx5, align 4, !tbaa !12
  %add = add nsw i32 %30, %34
  %35 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %36 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %35, i32 0, i32 3
  %crop_heights6 = bitcast %union.anon.4* %36 to [2 x i32]*
  %37 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights6, i32 0, i32 %37
  %38 = load i32, i32* %arrayidx7, align 4, !tbaa !12
  %sub = sub nsw i32 %add, %38
  %39 = load i32, i32* %plane_border, align 4, !tbaa !6
  %40 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %41 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %40, i32 0, i32 0
  %widths = bitcast %union.anon* %41 to [2 x i32]*
  %42 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds [2 x i32], [2 x i32]* %widths, i32 0, i32 %42
  %43 = load i32, i32* %arrayidx8, align 4, !tbaa !12
  %add9 = add nsw i32 %39, %43
  %44 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %45 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %44, i32 0, i32 2
  %crop_widths10 = bitcast %union.anon.2* %45 to [2 x i32]*
  %46 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths10, i32 0, i32 %46
  %47 = load i32, i32* %arrayidx11, align 4, !tbaa !12
  %sub12 = sub nsw i32 %add9, %47
  call void @extend_plane_high(i8* %15, i32 %19, i32 %23, i32 %27, i32 %28, i32 %29, i32 %sub, i32 %sub12)
  %48 = bitcast i32* %plane_border to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %50 = load i32, i32* %plane, align 4, !tbaa !6
  %inc = add nsw i32 %50, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %for.end47

if.end:                                           ; preds = %entry
  %51 = bitcast i32* %plane13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #4
  store i32 0, i32* %plane13, align 4, !tbaa !6
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc45, %if.end
  %52 = load i32, i32* %plane13, align 4, !tbaa !6
  %53 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp15 = icmp slt i32 %52, %53
  br i1 %cmp15, label %for.body18, label %for.cond.cleanup17

for.cond.cleanup17:                               ; preds = %for.cond14
  %54 = bitcast i32* %plane13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  br label %for.end47

for.body18:                                       ; preds = %for.cond14
  %55 = bitcast i32* %is_uv19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #4
  %56 = load i32, i32* %plane13, align 4, !tbaa !6
  %cmp20 = icmp sgt i32 %56, 0
  %conv21 = zext i1 %cmp20 to i32
  store i32 %conv21, i32* %is_uv19, align 4, !tbaa !6
  %57 = bitcast i32* %plane_border22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #4
  %58 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %border23 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %58, i32 0, i32 12
  %59 = load i32, i32* %border23, align 4, !tbaa !11
  %60 = load i32, i32* %is_uv19, align 4, !tbaa !6
  %shr24 = ashr i32 %59, %60
  store i32 %shr24, i32* %plane_border22, align 4, !tbaa !6
  %61 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %62 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %61, i32 0, i32 5
  %buffers25 = bitcast %union.anon.8* %62 to [3 x i8*]*
  %63 = load i32, i32* %plane13, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers25, i32 0, i32 %63
  %64 = load i8*, i8** %arrayidx26, align 4, !tbaa !12
  %65 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %66 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %65, i32 0, i32 4
  %strides27 = bitcast %union.anon.6* %66 to [2 x i32]*
  %67 = load i32, i32* %is_uv19, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds [2 x i32], [2 x i32]* %strides27, i32 0, i32 %67
  %68 = load i32, i32* %arrayidx28, align 4, !tbaa !12
  %69 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %70 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %69, i32 0, i32 2
  %crop_widths29 = bitcast %union.anon.2* %70 to [2 x i32]*
  %71 = load i32, i32* %is_uv19, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths29, i32 0, i32 %71
  %72 = load i32, i32* %arrayidx30, align 4, !tbaa !12
  %73 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %74 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %73, i32 0, i32 3
  %crop_heights31 = bitcast %union.anon.4* %74 to [2 x i32]*
  %75 = load i32, i32* %is_uv19, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights31, i32 0, i32 %75
  %76 = load i32, i32* %arrayidx32, align 4, !tbaa !12
  %77 = load i32, i32* %plane_border22, align 4, !tbaa !6
  %78 = load i32, i32* %plane_border22, align 4, !tbaa !6
  %79 = load i32, i32* %plane_border22, align 4, !tbaa !6
  %80 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %81 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %80, i32 0, i32 1
  %heights33 = bitcast %union.anon.0* %81 to [2 x i32]*
  %82 = load i32, i32* %is_uv19, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds [2 x i32], [2 x i32]* %heights33, i32 0, i32 %82
  %83 = load i32, i32* %arrayidx34, align 4, !tbaa !12
  %add35 = add nsw i32 %79, %83
  %84 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %85 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %84, i32 0, i32 3
  %crop_heights36 = bitcast %union.anon.4* %85 to [2 x i32]*
  %86 = load i32, i32* %is_uv19, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights36, i32 0, i32 %86
  %87 = load i32, i32* %arrayidx37, align 4, !tbaa !12
  %sub38 = sub nsw i32 %add35, %87
  %88 = load i32, i32* %plane_border22, align 4, !tbaa !6
  %89 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %90 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %89, i32 0, i32 0
  %widths39 = bitcast %union.anon* %90 to [2 x i32]*
  %91 = load i32, i32* %is_uv19, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds [2 x i32], [2 x i32]* %widths39, i32 0, i32 %91
  %92 = load i32, i32* %arrayidx40, align 4, !tbaa !12
  %add41 = add nsw i32 %88, %92
  %93 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %94 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %93, i32 0, i32 2
  %crop_widths42 = bitcast %union.anon.2* %94 to [2 x i32]*
  %95 = load i32, i32* %is_uv19, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths42, i32 0, i32 %95
  %96 = load i32, i32* %arrayidx43, align 4, !tbaa !12
  %sub44 = sub nsw i32 %add41, %96
  call void @extend_plane(i8* %64, i32 %68, i32 %72, i32 %76, i32 %77, i32 %78, i32 %sub38, i32 %sub44)
  %97 = bitcast i32* %plane_border22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  %98 = bitcast i32* %is_uv19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #4
  br label %for.inc45

for.inc45:                                        ; preds = %for.body18
  %99 = load i32, i32* %plane13, align 4, !tbaa !6
  %inc46 = add nsw i32 %99, 1
  store i32 %inc46, i32* %plane13, align 4, !tbaa !6
  br label %for.cond14

for.end47:                                        ; preds = %for.end, %for.cond.cleanup17
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @extend_plane_high(i8* %src8, i32 %src_stride, i32 %width, i32 %height, i32 %extend_top, i32 %extend_left, i32 %extend_bottom, i32 %extend_right) #0 {
entry:
  %src8.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %extend_top.addr = alloca i32, align 4
  %extend_left.addr = alloca i32, align 4
  %extend_bottom.addr = alloca i32, align 4
  %extend_right.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %linesize = alloca i32, align 4
  %src = alloca i16*, align 4
  %src_ptr1 = alloca i16*, align 4
  %src_ptr2 = alloca i16*, align 4
  %dst_ptr1 = alloca i16*, align 4
  %dst_ptr2 = alloca i16*, align 4
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %extend_top, i32* %extend_top.addr, align 4, !tbaa !6
  store i32 %extend_left, i32* %extend_left.addr, align 4, !tbaa !6
  store i32 %extend_bottom, i32* %extend_bottom.addr, align 4, !tbaa !6
  store i32 %extend_right, i32* %extend_right.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %linesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %3 = load i32, i32* %extend_right.addr, align 4, !tbaa !6
  %add = add nsw i32 %2, %3
  %4 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add1 = add nsw i32 %add, %4
  store i32 %add1, i32* %linesize, align 4, !tbaa !6
  %5 = bitcast i16** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %7 = ptrtoint i8* %6 to i32
  %shl = shl i32 %7, 1
  %8 = inttoptr i32 %shl to i16*
  store i16* %8, i16** %src, align 4, !tbaa !2
  %9 = bitcast i16** %src_ptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load i16*, i16** %src, align 4, !tbaa !2
  store i16* %10, i16** %src_ptr1, align 4, !tbaa !2
  %11 = bitcast i16** %src_ptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i16*, i16** %src, align 4, !tbaa !2
  %13 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i16, i16* %12, i32 %13
  %add.ptr2 = getelementptr inbounds i16, i16* %add.ptr, i32 -1
  store i16* %add.ptr2, i16** %src_ptr2, align 4, !tbaa !2
  %14 = bitcast i16** %dst_ptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load i16*, i16** %src, align 4, !tbaa !2
  %16 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg = sub i32 0, %16
  %add.ptr3 = getelementptr inbounds i16, i16* %15, i32 %idx.neg
  store i16* %add.ptr3, i16** %dst_ptr1, align 4, !tbaa !2
  %17 = bitcast i16** %dst_ptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load i16*, i16** %src, align 4, !tbaa !2
  %19 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add.ptr4 = getelementptr inbounds i16, i16* %18, i32 %19
  store i16* %add.ptr4, i16** %dst_ptr2, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %20, %21
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %22 = load i16*, i16** %dst_ptr1, align 4, !tbaa !2
  %23 = bitcast i16* %22 to i8*
  %24 = load i16*, i16** %src_ptr1, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %24, i32 0
  %25 = load i16, i16* %arrayidx, align 2, !tbaa !13
  %conv = zext i16 %25 to i32
  %26 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %call = call i8* @aom_memset16(i8* %23, i32 %conv, i32 %26)
  %27 = load i16*, i16** %dst_ptr2, align 4, !tbaa !2
  %28 = bitcast i16* %27 to i8*
  %29 = load i16*, i16** %src_ptr2, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i16, i16* %29, i32 0
  %30 = load i16, i16* %arrayidx5, align 2, !tbaa !13
  %conv6 = zext i16 %30 to i32
  %31 = load i32, i32* %extend_right.addr, align 4, !tbaa !6
  %call7 = call i8* @aom_memset16(i8* %28, i32 %conv6, i32 %31)
  %32 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %33 = load i16*, i16** %src_ptr1, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds i16, i16* %33, i32 %32
  store i16* %add.ptr8, i16** %src_ptr1, align 4, !tbaa !2
  %34 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %35 = load i16*, i16** %src_ptr2, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i16, i16* %35, i32 %34
  store i16* %add.ptr9, i16** %src_ptr2, align 4, !tbaa !2
  %36 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %37 = load i16*, i16** %dst_ptr1, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i16, i16* %37, i32 %36
  store i16* %add.ptr10, i16** %dst_ptr1, align 4, !tbaa !2
  %38 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %39 = load i16*, i16** %dst_ptr2, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i16, i16* %39, i32 %38
  store i16* %add.ptr11, i16** %dst_ptr2, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %41 = load i16*, i16** %src, align 4, !tbaa !2
  %42 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg12 = sub i32 0, %42
  %add.ptr13 = getelementptr inbounds i16, i16* %41, i32 %idx.neg12
  store i16* %add.ptr13, i16** %src_ptr1, align 4, !tbaa !2
  %43 = load i16*, i16** %src, align 4, !tbaa !2
  %44 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %45 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %45, 1
  %mul = mul nsw i32 %44, %sub
  %add.ptr14 = getelementptr inbounds i16, i16* %43, i32 %mul
  %46 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg15 = sub i32 0, %46
  %add.ptr16 = getelementptr inbounds i16, i16* %add.ptr14, i32 %idx.neg15
  store i16* %add.ptr16, i16** %src_ptr2, align 4, !tbaa !2
  %47 = load i16*, i16** %src, align 4, !tbaa !2
  %48 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %49 = load i32, i32* %extend_top.addr, align 4, !tbaa !6
  %sub17 = sub nsw i32 0, %49
  %mul18 = mul nsw i32 %48, %sub17
  %add.ptr19 = getelementptr inbounds i16, i16* %47, i32 %mul18
  %50 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg20 = sub i32 0, %50
  %add.ptr21 = getelementptr inbounds i16, i16* %add.ptr19, i32 %idx.neg20
  store i16* %add.ptr21, i16** %dst_ptr1, align 4, !tbaa !2
  %51 = load i16*, i16** %src, align 4, !tbaa !2
  %52 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %53 = load i32, i32* %height.addr, align 4, !tbaa !6
  %mul22 = mul nsw i32 %52, %53
  %add.ptr23 = getelementptr inbounds i16, i16* %51, i32 %mul22
  %54 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg24 = sub i32 0, %54
  %add.ptr25 = getelementptr inbounds i16, i16* %add.ptr23, i32 %idx.neg24
  store i16* %add.ptr25, i16** %dst_ptr2, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc32, %for.end
  %55 = load i32, i32* %i, align 4, !tbaa !6
  %56 = load i32, i32* %extend_top.addr, align 4, !tbaa !6
  %cmp27 = icmp slt i32 %55, %56
  br i1 %cmp27, label %for.body29, label %for.end34

for.body29:                                       ; preds = %for.cond26
  %57 = load i16*, i16** %dst_ptr1, align 4, !tbaa !2
  %58 = bitcast i16* %57 to i8*
  %59 = load i16*, i16** %src_ptr1, align 4, !tbaa !2
  %60 = bitcast i16* %59 to i8*
  %61 = load i32, i32* %linesize, align 4, !tbaa !6
  %mul30 = mul i32 %61, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %58, i8* align 2 %60, i32 %mul30, i1 false)
  %62 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %63 = load i16*, i16** %dst_ptr1, align 4, !tbaa !2
  %add.ptr31 = getelementptr inbounds i16, i16* %63, i32 %62
  store i16* %add.ptr31, i16** %dst_ptr1, align 4, !tbaa !2
  br label %for.inc32

for.inc32:                                        ; preds = %for.body29
  %64 = load i32, i32* %i, align 4, !tbaa !6
  %inc33 = add nsw i32 %64, 1
  store i32 %inc33, i32* %i, align 4, !tbaa !6
  br label %for.cond26

for.end34:                                        ; preds = %for.cond26
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc41, %for.end34
  %65 = load i32, i32* %i, align 4, !tbaa !6
  %66 = load i32, i32* %extend_bottom.addr, align 4, !tbaa !6
  %cmp36 = icmp slt i32 %65, %66
  br i1 %cmp36, label %for.body38, label %for.end43

for.body38:                                       ; preds = %for.cond35
  %67 = load i16*, i16** %dst_ptr2, align 4, !tbaa !2
  %68 = bitcast i16* %67 to i8*
  %69 = load i16*, i16** %src_ptr2, align 4, !tbaa !2
  %70 = bitcast i16* %69 to i8*
  %71 = load i32, i32* %linesize, align 4, !tbaa !6
  %mul39 = mul i32 %71, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %68, i8* align 2 %70, i32 %mul39, i1 false)
  %72 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %73 = load i16*, i16** %dst_ptr2, align 4, !tbaa !2
  %add.ptr40 = getelementptr inbounds i16, i16* %73, i32 %72
  store i16* %add.ptr40, i16** %dst_ptr2, align 4, !tbaa !2
  br label %for.inc41

for.inc41:                                        ; preds = %for.body38
  %74 = load i32, i32* %i, align 4, !tbaa !6
  %inc42 = add nsw i32 %74, 1
  store i32 %inc42, i32* %i, align 4, !tbaa !6
  br label %for.cond35

for.end43:                                        ; preds = %for.cond35
  %75 = bitcast i16** %dst_ptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  %76 = bitcast i16** %dst_ptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  %77 = bitcast i16** %src_ptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i16** %src_ptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  %79 = bitcast i16** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast i32* %linesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @extend_plane(i8* %src, i32 %src_stride, i32 %width, i32 %height, i32 %extend_top, i32 %extend_left, i32 %extend_bottom, i32 %extend_right) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %extend_top.addr = alloca i32, align 4
  %extend_left.addr = alloca i32, align 4
  %extend_bottom.addr = alloca i32, align 4
  %extend_right.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %linesize = alloca i32, align 4
  %src_ptr1 = alloca i8*, align 4
  %src_ptr2 = alloca i8*, align 4
  %dst_ptr1 = alloca i8*, align 4
  %dst_ptr2 = alloca i8*, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %extend_top, i32* %extend_top.addr, align 4, !tbaa !6
  store i32 %extend_left, i32* %extend_left.addr, align 4, !tbaa !6
  store i32 %extend_bottom, i32* %extend_bottom.addr, align 4, !tbaa !6
  store i32 %extend_right, i32* %extend_right.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %linesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %3 = load i32, i32* %extend_right.addr, align 4, !tbaa !6
  %add = add nsw i32 %2, %3
  %4 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add1 = add nsw i32 %add, %4
  store i32 %add1, i32* %linesize, align 4, !tbaa !6
  %5 = bitcast i8** %src_ptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i8*, i8** %src.addr, align 4, !tbaa !2
  store i8* %6, i8** %src_ptr1, align 4, !tbaa !2
  %7 = bitcast i8** %src_ptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %9 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 %9
  %add.ptr2 = getelementptr inbounds i8, i8* %add.ptr, i32 -1
  store i8* %add.ptr2, i8** %src_ptr2, align 4, !tbaa !2
  %10 = bitcast i8** %dst_ptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %12 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg = sub i32 0, %12
  %add.ptr3 = getelementptr inbounds i8, i8* %11, i32 %idx.neg
  store i8* %add.ptr3, i8** %dst_ptr1, align 4, !tbaa !2
  %13 = bitcast i8** %dst_ptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %15 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add.ptr4 = getelementptr inbounds i8, i8* %14, i32 %15
  store i8* %add.ptr4, i8** %dst_ptr2, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %16, %17
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load i8*, i8** %dst_ptr1, align 4, !tbaa !2
  %19 = load i8*, i8** %src_ptr1, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %19, i32 0
  %20 = load i8, i8* %arrayidx, align 1, !tbaa !12
  %conv = zext i8 %20 to i32
  %21 = trunc i32 %conv to i8
  %22 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %18, i8 %21, i32 %22, i1 false)
  %23 = load i8*, i8** %dst_ptr2, align 4, !tbaa !2
  %24 = load i8*, i8** %src_ptr2, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %24, i32 0
  %25 = load i8, i8* %arrayidx5, align 1, !tbaa !12
  %conv6 = zext i8 %25 to i32
  %26 = trunc i32 %conv6 to i8
  %27 = load i32, i32* %extend_right.addr, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %23, i8 %26, i32 %27, i1 false)
  %28 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %29 = load i8*, i8** %src_ptr1, align 4, !tbaa !2
  %add.ptr7 = getelementptr inbounds i8, i8* %29, i32 %28
  store i8* %add.ptr7, i8** %src_ptr1, align 4, !tbaa !2
  %30 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %31 = load i8*, i8** %src_ptr2, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds i8, i8* %31, i32 %30
  store i8* %add.ptr8, i8** %src_ptr2, align 4, !tbaa !2
  %32 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %33 = load i8*, i8** %dst_ptr1, align 4, !tbaa !2
  %add.ptr9 = getelementptr inbounds i8, i8* %33, i32 %32
  store i8* %add.ptr9, i8** %dst_ptr1, align 4, !tbaa !2
  %34 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %35 = load i8*, i8** %dst_ptr2, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i8, i8* %35, i32 %34
  store i8* %add.ptr10, i8** %dst_ptr2, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %38 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg11 = sub i32 0, %38
  %add.ptr12 = getelementptr inbounds i8, i8* %37, i32 %idx.neg11
  store i8* %add.ptr12, i8** %src_ptr1, align 4, !tbaa !2
  %39 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %40 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %41 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %41, 1
  %mul = mul nsw i32 %40, %sub
  %add.ptr13 = getelementptr inbounds i8, i8* %39, i32 %mul
  %42 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg14 = sub i32 0, %42
  %add.ptr15 = getelementptr inbounds i8, i8* %add.ptr13, i32 %idx.neg14
  store i8* %add.ptr15, i8** %src_ptr2, align 4, !tbaa !2
  %43 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %44 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %45 = load i32, i32* %extend_top.addr, align 4, !tbaa !6
  %sub16 = sub nsw i32 0, %45
  %mul17 = mul nsw i32 %44, %sub16
  %add.ptr18 = getelementptr inbounds i8, i8* %43, i32 %mul17
  %46 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg19 = sub i32 0, %46
  %add.ptr20 = getelementptr inbounds i8, i8* %add.ptr18, i32 %idx.neg19
  store i8* %add.ptr20, i8** %dst_ptr1, align 4, !tbaa !2
  %47 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %48 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %49 = load i32, i32* %height.addr, align 4, !tbaa !6
  %mul21 = mul nsw i32 %48, %49
  %add.ptr22 = getelementptr inbounds i8, i8* %47, i32 %mul21
  %50 = load i32, i32* %extend_left.addr, align 4, !tbaa !6
  %idx.neg23 = sub i32 0, %50
  %add.ptr24 = getelementptr inbounds i8, i8* %add.ptr22, i32 %idx.neg23
  store i8* %add.ptr24, i8** %dst_ptr2, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc30, %for.end
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %52 = load i32, i32* %extend_top.addr, align 4, !tbaa !6
  %cmp26 = icmp slt i32 %51, %52
  br i1 %cmp26, label %for.body28, label %for.end32

for.body28:                                       ; preds = %for.cond25
  %53 = load i8*, i8** %dst_ptr1, align 4, !tbaa !2
  %54 = load i8*, i8** %src_ptr1, align 4, !tbaa !2
  %55 = load i32, i32* %linesize, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %53, i8* align 1 %54, i32 %55, i1 false)
  %56 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %57 = load i8*, i8** %dst_ptr1, align 4, !tbaa !2
  %add.ptr29 = getelementptr inbounds i8, i8* %57, i32 %56
  store i8* %add.ptr29, i8** %dst_ptr1, align 4, !tbaa !2
  br label %for.inc30

for.inc30:                                        ; preds = %for.body28
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %inc31 = add nsw i32 %58, 1
  store i32 %inc31, i32* %i, align 4, !tbaa !6
  br label %for.cond25

for.end32:                                        ; preds = %for.cond25
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc38, %for.end32
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %60 = load i32, i32* %extend_bottom.addr, align 4, !tbaa !6
  %cmp34 = icmp slt i32 %59, %60
  br i1 %cmp34, label %for.body36, label %for.end40

for.body36:                                       ; preds = %for.cond33
  %61 = load i8*, i8** %dst_ptr2, align 4, !tbaa !2
  %62 = load i8*, i8** %src_ptr2, align 4, !tbaa !2
  %63 = load i32, i32* %linesize, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %61, i8* align 1 %62, i32 %63, i1 false)
  %64 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %65 = load i8*, i8** %dst_ptr2, align 4, !tbaa !2
  %add.ptr37 = getelementptr inbounds i8, i8* %65, i32 %64
  store i8* %add.ptr37, i8** %dst_ptr2, align 4, !tbaa !2
  br label %for.inc38

for.inc38:                                        ; preds = %for.body36
  %66 = load i32, i32* %i, align 4, !tbaa !6
  %inc39 = add nsw i32 %66, 1
  store i32 %inc39, i32* %i, align 4, !tbaa !6
  br label %for.cond33

for.end40:                                        ; preds = %for.cond33
  %67 = bitcast i8** %dst_ptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #4
  %68 = bitcast i8** %dst_ptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #4
  %69 = bitcast i8** %src_ptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %70 = bitcast i8** %src_ptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast i32* %linesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_extend_frame_borders_c(%struct.yv12_buffer_config* %ybf, i32 %num_planes) #0 {
entry:
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %num_planes.addr = alloca i32, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %border = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 12
  %2 = load i32, i32* %border, align 4, !tbaa !11
  %3 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  call void @extend_frame(%struct.yv12_buffer_config* %0, i32 %2, i32 %3)
  ret void
}

; Function Attrs: nounwind
define internal void @extend_frame(%struct.yv12_buffer_config* %ybf, i32 %ext_size, i32 %num_planes) #0 {
entry:
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %ext_size.addr = alloca i32, align 4
  %num_planes.addr = alloca i32, align 4
  %ss_x = alloca i32, align 4
  %ss_y = alloca i32, align 4
  %plane = alloca i32, align 4
  %is_uv = alloca i32, align 4
  %top = alloca i32, align 4
  %left = alloca i32, align 4
  %bottom = alloca i32, align 4
  %right = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %plane25 = alloca i32, align 4
  %is_uv31 = alloca i32, align 4
  %top34 = alloca i32, align 4
  %left41 = alloca i32, align 4
  %bottom48 = alloca i32, align 4
  %right55 = alloca i32, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  store i32 %ext_size, i32* %ext_size.addr, align 4, !tbaa !6
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %2 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 0
  %3 = bitcast %union.anon* %2 to %struct.anon*
  %uv_width = getelementptr inbounds %struct.anon, %struct.anon* %3, i32 0, i32 1
  %4 = load i32, i32* %uv_width, align 4, !tbaa !12
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %6 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %5, i32 0, i32 0
  %7 = bitcast %union.anon* %6 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %7, i32 0, i32 0
  %8 = load i32, i32* %y_width, align 4, !tbaa !12
  %cmp = icmp slt i32 %4, %8
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %ss_x, align 4, !tbaa !6
  %9 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %11 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %10, i32 0, i32 1
  %12 = bitcast %union.anon.0* %11 to %struct.anon.1*
  %uv_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %12, i32 0, i32 1
  %13 = load i32, i32* %uv_height, align 4, !tbaa !12
  %14 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %15 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %14, i32 0, i32 1
  %16 = bitcast %union.anon.0* %15 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %16, i32 0, i32 0
  %17 = load i32, i32* %y_height, align 4, !tbaa !12
  %cmp1 = icmp slt i32 %13, %17
  %conv2 = zext i1 %cmp1 to i32
  store i32 %conv2, i32* %ss_y, align 4, !tbaa !6
  %18 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %18, i32 0, i32 26
  %19 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %19, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %20 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  store i32 0, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %21 = load i32, i32* %plane, align 4, !tbaa !6
  %22 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %21, %22
  br i1 %cmp3, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %23 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %24 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load i32, i32* %plane, align 4, !tbaa !6
  %cmp5 = icmp sgt i32 %25, 0
  %conv6 = zext i1 %cmp5 to i32
  store i32 %conv6, i32* %is_uv, align 4, !tbaa !6
  %26 = bitcast i32* %top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load i32, i32* %ext_size.addr, align 4, !tbaa !6
  %28 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool7 = icmp ne i32 %28, 0
  br i1 %tobool7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %29 = load i32, i32* %ss_y, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %29, %cond.true ], [ 0, %cond.false ]
  %shr = ashr i32 %27, %cond
  store i32 %shr, i32* %top, align 4, !tbaa !6
  %30 = bitcast i32* %left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load i32, i32* %ext_size.addr, align 4, !tbaa !6
  %32 = load i32, i32* %is_uv, align 4, !tbaa !6
  %tobool8 = icmp ne i32 %32, 0
  br i1 %tobool8, label %cond.true9, label %cond.false10

cond.true9:                                       ; preds = %cond.end
  %33 = load i32, i32* %ss_x, align 4, !tbaa !6
  br label %cond.end11

cond.false10:                                     ; preds = %cond.end
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false10, %cond.true9
  %cond12 = phi i32 [ %33, %cond.true9 ], [ 0, %cond.false10 ]
  %shr13 = ashr i32 %31, %cond12
  store i32 %shr13, i32* %left, align 4, !tbaa !6
  %34 = bitcast i32* %bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %35 = load i32, i32* %top, align 4, !tbaa !6
  %36 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %37 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %36, i32 0, i32 1
  %heights = bitcast %union.anon.0* %37 to [2 x i32]*
  %38 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %heights, i32 0, i32 %38
  %39 = load i32, i32* %arrayidx, align 4, !tbaa !12
  %add = add nsw i32 %35, %39
  %40 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %41 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %40, i32 0, i32 3
  %crop_heights = bitcast %union.anon.4* %41 to [2 x i32]*
  %42 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights, i32 0, i32 %42
  %43 = load i32, i32* %arrayidx14, align 4, !tbaa !12
  %sub = sub nsw i32 %add, %43
  store i32 %sub, i32* %bottom, align 4, !tbaa !6
  %44 = bitcast i32* %right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #4
  %45 = load i32, i32* %left, align 4, !tbaa !6
  %46 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %47 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %46, i32 0, i32 0
  %widths = bitcast %union.anon* %47 to [2 x i32]*
  %48 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [2 x i32], [2 x i32]* %widths, i32 0, i32 %48
  %49 = load i32, i32* %arrayidx15, align 4, !tbaa !12
  %add16 = add nsw i32 %45, %49
  %50 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %51 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %50, i32 0, i32 2
  %crop_widths = bitcast %union.anon.2* %51 to [2 x i32]*
  %52 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths, i32 0, i32 %52
  %53 = load i32, i32* %arrayidx17, align 4, !tbaa !12
  %sub18 = sub nsw i32 %add16, %53
  store i32 %sub18, i32* %right, align 4, !tbaa !6
  %54 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %55 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %54, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %55 to [3 x i8*]*
  %56 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %56
  %57 = load i8*, i8** %arrayidx19, align 4, !tbaa !12
  %58 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %59 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %58, i32 0, i32 4
  %strides = bitcast %union.anon.6* %59 to [2 x i32]*
  %60 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %60
  %61 = load i32, i32* %arrayidx20, align 4, !tbaa !12
  %62 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %63 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %62, i32 0, i32 2
  %crop_widths21 = bitcast %union.anon.2* %63 to [2 x i32]*
  %64 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths21, i32 0, i32 %64
  %65 = load i32, i32* %arrayidx22, align 4, !tbaa !12
  %66 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %67 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %66, i32 0, i32 3
  %crop_heights23 = bitcast %union.anon.4* %67 to [2 x i32]*
  %68 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights23, i32 0, i32 %68
  %69 = load i32, i32* %arrayidx24, align 4, !tbaa !12
  %70 = load i32, i32* %top, align 4, !tbaa !6
  %71 = load i32, i32* %left, align 4, !tbaa !6
  %72 = load i32, i32* %bottom, align 4, !tbaa !6
  %73 = load i32, i32* %right, align 4, !tbaa !6
  call void @extend_plane_high(i8* %57, i32 %61, i32 %65, i32 %69, i32 %70, i32 %71, i32 %72, i32 %73)
  %74 = bitcast i32* %right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  %75 = bitcast i32* %bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  %76 = bitcast i32* %left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  %77 = bitcast i32* %top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  br label %for.inc

for.inc:                                          ; preds = %cond.end11
  %79 = load i32, i32* %plane, align 4, !tbaa !6
  %inc = add nsw i32 %79, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %80 = bitcast i32* %plane25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #4
  store i32 0, i32* %plane25, align 4, !tbaa !6
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc70, %if.end
  %81 = load i32, i32* %plane25, align 4, !tbaa !6
  %82 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp27 = icmp slt i32 %81, %82
  br i1 %cmp27, label %for.body30, label %for.cond.cleanup29

for.cond.cleanup29:                               ; preds = %for.cond26
  store i32 5, i32* %cleanup.dest.slot, align 4
  %83 = bitcast i32* %plane25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  br label %for.end72

for.body30:                                       ; preds = %for.cond26
  %84 = bitcast i32* %is_uv31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #4
  %85 = load i32, i32* %plane25, align 4, !tbaa !6
  %cmp32 = icmp sgt i32 %85, 0
  %conv33 = zext i1 %cmp32 to i32
  store i32 %conv33, i32* %is_uv31, align 4, !tbaa !6
  %86 = bitcast i32* %top34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #4
  %87 = load i32, i32* %ext_size.addr, align 4, !tbaa !6
  %88 = load i32, i32* %is_uv31, align 4, !tbaa !6
  %tobool35 = icmp ne i32 %88, 0
  br i1 %tobool35, label %cond.true36, label %cond.false37

cond.true36:                                      ; preds = %for.body30
  %89 = load i32, i32* %ss_y, align 4, !tbaa !6
  br label %cond.end38

cond.false37:                                     ; preds = %for.body30
  br label %cond.end38

cond.end38:                                       ; preds = %cond.false37, %cond.true36
  %cond39 = phi i32 [ %89, %cond.true36 ], [ 0, %cond.false37 ]
  %shr40 = ashr i32 %87, %cond39
  store i32 %shr40, i32* %top34, align 4, !tbaa !6
  %90 = bitcast i32* %left41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #4
  %91 = load i32, i32* %ext_size.addr, align 4, !tbaa !6
  %92 = load i32, i32* %is_uv31, align 4, !tbaa !6
  %tobool42 = icmp ne i32 %92, 0
  br i1 %tobool42, label %cond.true43, label %cond.false44

cond.true43:                                      ; preds = %cond.end38
  %93 = load i32, i32* %ss_x, align 4, !tbaa !6
  br label %cond.end45

cond.false44:                                     ; preds = %cond.end38
  br label %cond.end45

cond.end45:                                       ; preds = %cond.false44, %cond.true43
  %cond46 = phi i32 [ %93, %cond.true43 ], [ 0, %cond.false44 ]
  %shr47 = ashr i32 %91, %cond46
  store i32 %shr47, i32* %left41, align 4, !tbaa !6
  %94 = bitcast i32* %bottom48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #4
  %95 = load i32, i32* %top34, align 4, !tbaa !6
  %96 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %97 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %96, i32 0, i32 1
  %heights49 = bitcast %union.anon.0* %97 to [2 x i32]*
  %98 = load i32, i32* %is_uv31, align 4, !tbaa !6
  %arrayidx50 = getelementptr inbounds [2 x i32], [2 x i32]* %heights49, i32 0, i32 %98
  %99 = load i32, i32* %arrayidx50, align 4, !tbaa !12
  %add51 = add nsw i32 %95, %99
  %100 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %101 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %100, i32 0, i32 3
  %crop_heights52 = bitcast %union.anon.4* %101 to [2 x i32]*
  %102 = load i32, i32* %is_uv31, align 4, !tbaa !6
  %arrayidx53 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights52, i32 0, i32 %102
  %103 = load i32, i32* %arrayidx53, align 4, !tbaa !12
  %sub54 = sub nsw i32 %add51, %103
  store i32 %sub54, i32* %bottom48, align 4, !tbaa !6
  %104 = bitcast i32* %right55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #4
  %105 = load i32, i32* %left41, align 4, !tbaa !6
  %106 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %107 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %106, i32 0, i32 0
  %widths56 = bitcast %union.anon* %107 to [2 x i32]*
  %108 = load i32, i32* %is_uv31, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds [2 x i32], [2 x i32]* %widths56, i32 0, i32 %108
  %109 = load i32, i32* %arrayidx57, align 4, !tbaa !12
  %add58 = add nsw i32 %105, %109
  %110 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %111 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %110, i32 0, i32 2
  %crop_widths59 = bitcast %union.anon.2* %111 to [2 x i32]*
  %112 = load i32, i32* %is_uv31, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths59, i32 0, i32 %112
  %113 = load i32, i32* %arrayidx60, align 4, !tbaa !12
  %sub61 = sub nsw i32 %add58, %113
  store i32 %sub61, i32* %right55, align 4, !tbaa !6
  %114 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %115 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %114, i32 0, i32 5
  %buffers62 = bitcast %union.anon.8* %115 to [3 x i8*]*
  %116 = load i32, i32* %plane25, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers62, i32 0, i32 %116
  %117 = load i8*, i8** %arrayidx63, align 4, !tbaa !12
  %118 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %119 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %118, i32 0, i32 4
  %strides64 = bitcast %union.anon.6* %119 to [2 x i32]*
  %120 = load i32, i32* %is_uv31, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds [2 x i32], [2 x i32]* %strides64, i32 0, i32 %120
  %121 = load i32, i32* %arrayidx65, align 4, !tbaa !12
  %122 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %123 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %122, i32 0, i32 2
  %crop_widths66 = bitcast %union.anon.2* %123 to [2 x i32]*
  %124 = load i32, i32* %is_uv31, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths66, i32 0, i32 %124
  %125 = load i32, i32* %arrayidx67, align 4, !tbaa !12
  %126 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %127 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %126, i32 0, i32 3
  %crop_heights68 = bitcast %union.anon.4* %127 to [2 x i32]*
  %128 = load i32, i32* %is_uv31, align 4, !tbaa !6
  %arrayidx69 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights68, i32 0, i32 %128
  %129 = load i32, i32* %arrayidx69, align 4, !tbaa !12
  %130 = load i32, i32* %top34, align 4, !tbaa !6
  %131 = load i32, i32* %left41, align 4, !tbaa !6
  %132 = load i32, i32* %bottom48, align 4, !tbaa !6
  %133 = load i32, i32* %right55, align 4, !tbaa !6
  call void @extend_plane(i8* %117, i32 %121, i32 %125, i32 %129, i32 %130, i32 %131, i32 %132, i32 %133)
  %134 = bitcast i32* %right55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #4
  %135 = bitcast i32* %bottom48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #4
  %136 = bitcast i32* %left41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #4
  %137 = bitcast i32* %top34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #4
  %138 = bitcast i32* %is_uv31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  br label %for.inc70

for.inc70:                                        ; preds = %cond.end45
  %139 = load i32, i32* %plane25, align 4, !tbaa !6
  %inc71 = add nsw i32 %139, 1
  store i32 %inc71, i32* %plane25, align 4, !tbaa !6
  br label %for.cond26

for.end72:                                        ; preds = %for.cond.cleanup29
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end72, %for.end
  %140 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @aom_extend_frame_inner_borders_c(%struct.yv12_buffer_config* %ybf, i32 %num_planes) #0 {
entry:
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %num_planes.addr = alloca i32, align 4
  %inner_bw = alloca i32, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = bitcast i32* %inner_bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %border = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 12
  %2 = load i32, i32* %border, align 4, !tbaa !11
  %cmp = icmp sgt i32 %2, 160
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %border1 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %3, i32 0, i32 12
  %4 = load i32, i32* %border1, align 4, !tbaa !11
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 160, %cond.true ], [ %4, %cond.false ]
  store i32 %cond, i32* %inner_bw, align 4, !tbaa !6
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %6 = load i32, i32* %inner_bw, align 4, !tbaa !6
  %7 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  call void @extend_frame(%struct.yv12_buffer_config* %5, i32 %6, i32 %7)
  %8 = bitcast i32* %inner_bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_extend_frame_borders_y_c(%struct.yv12_buffer_config* %ybf) #0 {
entry:
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %ext_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ext_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %border = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 12
  %2 = load i32, i32* %border, align 4, !tbaa !11
  store i32 %2, i32* %ext_size, align 4, !tbaa !6
  %3 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %3, i32 0, i32 26
  %4 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %4, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %6 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %5, i32 0, i32 5
  %7 = bitcast %union.anon.8* %6 to %struct.anon.9*
  %y_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %7, i32 0, i32 0
  %8 = load i8*, i8** %y_buffer, align 4, !tbaa !12
  %9 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %10 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %9, i32 0, i32 4
  %11 = bitcast %union.anon.6* %10 to %struct.anon.7*
  %y_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %11, i32 0, i32 0
  %12 = load i32, i32* %y_stride, align 4, !tbaa !12
  %13 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %14 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %13, i32 0, i32 2
  %15 = bitcast %union.anon.2* %14 to %struct.anon.3*
  %y_crop_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %15, i32 0, i32 0
  %16 = load i32, i32* %y_crop_width, align 4, !tbaa !12
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %18 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 3
  %19 = bitcast %union.anon.4* %18 to %struct.anon.5*
  %y_crop_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %19, i32 0, i32 0
  %20 = load i32, i32* %y_crop_height, align 4, !tbaa !12
  %21 = load i32, i32* %ext_size, align 4, !tbaa !6
  %22 = load i32, i32* %ext_size, align 4, !tbaa !6
  %23 = load i32, i32* %ext_size, align 4, !tbaa !6
  %24 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %25 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %24, i32 0, i32 1
  %26 = bitcast %union.anon.0* %25 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %26, i32 0, i32 0
  %27 = load i32, i32* %y_height, align 4, !tbaa !12
  %add = add nsw i32 %23, %27
  %28 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %29 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %28, i32 0, i32 3
  %30 = bitcast %union.anon.4* %29 to %struct.anon.5*
  %y_crop_height1 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %30, i32 0, i32 0
  %31 = load i32, i32* %y_crop_height1, align 4, !tbaa !12
  %sub = sub nsw i32 %add, %31
  %32 = load i32, i32* %ext_size, align 4, !tbaa !6
  %33 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %34 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %33, i32 0, i32 0
  %35 = bitcast %union.anon* %34 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %35, i32 0, i32 0
  %36 = load i32, i32* %y_width, align 4, !tbaa !12
  %add2 = add nsw i32 %32, %36
  %37 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %38 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %37, i32 0, i32 2
  %39 = bitcast %union.anon.2* %38 to %struct.anon.3*
  %y_crop_width3 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %39, i32 0, i32 0
  %40 = load i32, i32* %y_crop_width3, align 4, !tbaa !12
  %sub4 = sub nsw i32 %add2, %40
  call void @extend_plane_high(i8* %8, i32 %12, i32 %16, i32 %20, i32 %21, i32 %22, i32 %sub, i32 %sub4)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %41 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %42 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %41, i32 0, i32 5
  %43 = bitcast %union.anon.8* %42 to %struct.anon.9*
  %y_buffer5 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %43, i32 0, i32 0
  %44 = load i8*, i8** %y_buffer5, align 4, !tbaa !12
  %45 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %46 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %45, i32 0, i32 4
  %47 = bitcast %union.anon.6* %46 to %struct.anon.7*
  %y_stride6 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %47, i32 0, i32 0
  %48 = load i32, i32* %y_stride6, align 4, !tbaa !12
  %49 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %50 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %49, i32 0, i32 2
  %51 = bitcast %union.anon.2* %50 to %struct.anon.3*
  %y_crop_width7 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %51, i32 0, i32 0
  %52 = load i32, i32* %y_crop_width7, align 4, !tbaa !12
  %53 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %54 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %53, i32 0, i32 3
  %55 = bitcast %union.anon.4* %54 to %struct.anon.5*
  %y_crop_height8 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %55, i32 0, i32 0
  %56 = load i32, i32* %y_crop_height8, align 4, !tbaa !12
  %57 = load i32, i32* %ext_size, align 4, !tbaa !6
  %58 = load i32, i32* %ext_size, align 4, !tbaa !6
  %59 = load i32, i32* %ext_size, align 4, !tbaa !6
  %60 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %61 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %60, i32 0, i32 1
  %62 = bitcast %union.anon.0* %61 to %struct.anon.1*
  %y_height9 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %62, i32 0, i32 0
  %63 = load i32, i32* %y_height9, align 4, !tbaa !12
  %add10 = add nsw i32 %59, %63
  %64 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %65 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %64, i32 0, i32 3
  %66 = bitcast %union.anon.4* %65 to %struct.anon.5*
  %y_crop_height11 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %66, i32 0, i32 0
  %67 = load i32, i32* %y_crop_height11, align 4, !tbaa !12
  %sub12 = sub nsw i32 %add10, %67
  %68 = load i32, i32* %ext_size, align 4, !tbaa !6
  %69 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %70 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %69, i32 0, i32 0
  %71 = bitcast %union.anon* %70 to %struct.anon*
  %y_width13 = getelementptr inbounds %struct.anon, %struct.anon* %71, i32 0, i32 0
  %72 = load i32, i32* %y_width13, align 4, !tbaa !12
  %add14 = add nsw i32 %68, %72
  %73 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %74 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %73, i32 0, i32 2
  %75 = bitcast %union.anon.2* %74 to %struct.anon.3*
  %y_crop_width15 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %75, i32 0, i32 0
  %76 = load i32, i32* %y_crop_width15, align 4, !tbaa !12
  %sub16 = sub nsw i32 %add14, %76
  call void @extend_plane(i8* %44, i32 %48, i32 %52, i32 %56, i32 %57, i32 %58, i32 %sub12, i32 %sub16)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %77 = bitcast i32* %ext_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @aom_yv12_copy_frame_c(%struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config* %dst_bc, i32 %num_planes) #0 {
entry:
  %src_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %num_planes.addr = alloca i32, align 4
  %plane = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %plane_src = alloca i8*, align 4
  %plane_dst = alloca i8*, align 4
  %is_uv = alloca i32, align 4
  %row = alloca i32, align 4
  %plane18 = alloca i32, align 4
  %plane_src24 = alloca i8*, align 4
  %plane_dst27 = alloca i8*, align 4
  %is_uv30 = alloca i32, align 4
  %row33 = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %dst_bc, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %0, i32 0, i32 26
  %1 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %1, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %if.then
  %3 = load i32, i32* %plane, align 4, !tbaa !6
  %4 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %5 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  br label %for.end17

for.body:                                         ; preds = %for.cond
  %6 = bitcast i8** %plane_src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %8 to [3 x i8*]*
  %9 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %9
  %10 = load i8*, i8** %arrayidx, align 4, !tbaa !12
  store i8* %10, i8** %plane_src, align 4, !tbaa !2
  %11 = bitcast i8** %plane_dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %13 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %12, i32 0, i32 5
  %buffers1 = bitcast %union.anon.8* %13 to [3 x i8*]*
  %14 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers1, i32 0, i32 %14
  %15 = load i8*, i8** %arrayidx2, align 4, !tbaa !12
  store i8* %15, i8** %plane_dst, align 4, !tbaa !2
  %16 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %plane, align 4, !tbaa !6
  %cmp3 = icmp sgt i32 %17, 0
  %conv = zext i1 %cmp3 to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !6
  %18 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %19 = load i32, i32* %row, align 4, !tbaa !6
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %21 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %20, i32 0, i32 1
  %heights = bitcast %union.anon.0* %21 to [2 x i32]*
  %22 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [2 x i32], [2 x i32]* %heights, i32 0, i32 %22
  %23 = load i32, i32* %arrayidx5, align 4, !tbaa !12
  %cmp6 = icmp slt i32 %19, %23
  br i1 %cmp6, label %for.body9, label %for.cond.cleanup8

for.cond.cleanup8:                                ; preds = %for.cond4
  store i32 5, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  br label %for.end

for.body9:                                        ; preds = %for.cond4
  %25 = load i8*, i8** %plane_dst, align 4, !tbaa !2
  %26 = load i8*, i8** %plane_src, align 4, !tbaa !2
  %27 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %28 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %27, i32 0, i32 0
  %widths = bitcast %union.anon* %28 to [2 x i32]*
  %29 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [2 x i32], [2 x i32]* %widths, i32 0, i32 %29
  %30 = load i32, i32* %arrayidx10, align 4, !tbaa !12
  call void @memcpy_short_addr(i8* %25, i8* %26, i32 %30)
  %31 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %32 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %31, i32 0, i32 4
  %strides = bitcast %union.anon.6* %32 to [2 x i32]*
  %33 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %33
  %34 = load i32, i32* %arrayidx11, align 4, !tbaa !12
  %35 = load i8*, i8** %plane_src, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %35, i32 %34
  store i8* %add.ptr, i8** %plane_src, align 4, !tbaa !2
  %36 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %37 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %36, i32 0, i32 4
  %strides12 = bitcast %union.anon.6* %37 to [2 x i32]*
  %38 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds [2 x i32], [2 x i32]* %strides12, i32 0, i32 %38
  %39 = load i32, i32* %arrayidx13, align 4, !tbaa !12
  %40 = load i8*, i8** %plane_dst, align 4, !tbaa !2
  %add.ptr14 = getelementptr inbounds i8, i8* %40, i32 %39
  store i8* %add.ptr14, i8** %plane_dst, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body9
  %41 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond4

for.end:                                          ; preds = %for.cond.cleanup8
  %42 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %plane_dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %plane_src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %45 = load i32, i32* %plane, align 4, !tbaa !6
  %inc16 = add nsw i32 %45, 1
  store i32 %inc16, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end17:                                        ; preds = %for.cond.cleanup
  %46 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %47 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  call void @aom_yv12_extend_frame_borders_c(%struct.yv12_buffer_config* %46, i32 %47)
  br label %return

if.end:                                           ; preds = %entry
  %48 = bitcast i32* %plane18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #4
  store i32 0, i32* %plane18, align 4, !tbaa !6
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc52, %if.end
  %49 = load i32, i32* %plane18, align 4, !tbaa !6
  %50 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp20 = icmp slt i32 %49, %50
  br i1 %cmp20, label %for.body23, label %for.cond.cleanup22

for.cond.cleanup22:                               ; preds = %for.cond19
  store i32 8, i32* %cleanup.dest.slot, align 4
  %51 = bitcast i32* %plane18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  br label %for.end54

for.body23:                                       ; preds = %for.cond19
  %52 = bitcast i8** %plane_src24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #4
  %53 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %54 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %53, i32 0, i32 5
  %buffers25 = bitcast %union.anon.8* %54 to [3 x i8*]*
  %55 = load i32, i32* %plane18, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers25, i32 0, i32 %55
  %56 = load i8*, i8** %arrayidx26, align 4, !tbaa !12
  store i8* %56, i8** %plane_src24, align 4, !tbaa !2
  %57 = bitcast i8** %plane_dst27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #4
  %58 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %59 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %58, i32 0, i32 5
  %buffers28 = bitcast %union.anon.8* %59 to [3 x i8*]*
  %60 = load i32, i32* %plane18, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers28, i32 0, i32 %60
  %61 = load i8*, i8** %arrayidx29, align 4, !tbaa !12
  store i8* %61, i8** %plane_dst27, align 4, !tbaa !2
  %62 = bitcast i32* %is_uv30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #4
  %63 = load i32, i32* %plane18, align 4, !tbaa !6
  %cmp31 = icmp sgt i32 %63, 0
  %conv32 = zext i1 %cmp31 to i32
  store i32 %conv32, i32* %is_uv30, align 4, !tbaa !6
  %64 = bitcast i32* %row33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #4
  store i32 0, i32* %row33, align 4, !tbaa !6
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc49, %for.body23
  %65 = load i32, i32* %row33, align 4, !tbaa !6
  %66 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %67 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %66, i32 0, i32 1
  %heights35 = bitcast %union.anon.0* %67 to [2 x i32]*
  %68 = load i32, i32* %is_uv30, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds [2 x i32], [2 x i32]* %heights35, i32 0, i32 %68
  %69 = load i32, i32* %arrayidx36, align 4, !tbaa !12
  %cmp37 = icmp slt i32 %65, %69
  br i1 %cmp37, label %for.body40, label %for.cond.cleanup39

for.cond.cleanup39:                               ; preds = %for.cond34
  store i32 11, i32* %cleanup.dest.slot, align 4
  %70 = bitcast i32* %row33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  br label %for.end51

for.body40:                                       ; preds = %for.cond34
  %71 = load i8*, i8** %plane_dst27, align 4, !tbaa !2
  %72 = load i8*, i8** %plane_src24, align 4, !tbaa !2
  %73 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %74 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %73, i32 0, i32 0
  %widths41 = bitcast %union.anon* %74 to [2 x i32]*
  %75 = load i32, i32* %is_uv30, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds [2 x i32], [2 x i32]* %widths41, i32 0, i32 %75
  %76 = load i32, i32* %arrayidx42, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %71, i8* align 1 %72, i32 %76, i1 false)
  %77 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %78 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %77, i32 0, i32 4
  %strides43 = bitcast %union.anon.6* %78 to [2 x i32]*
  %79 = load i32, i32* %is_uv30, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds [2 x i32], [2 x i32]* %strides43, i32 0, i32 %79
  %80 = load i32, i32* %arrayidx44, align 4, !tbaa !12
  %81 = load i8*, i8** %plane_src24, align 4, !tbaa !2
  %add.ptr45 = getelementptr inbounds i8, i8* %81, i32 %80
  store i8* %add.ptr45, i8** %plane_src24, align 4, !tbaa !2
  %82 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %83 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %82, i32 0, i32 4
  %strides46 = bitcast %union.anon.6* %83 to [2 x i32]*
  %84 = load i32, i32* %is_uv30, align 4, !tbaa !6
  %arrayidx47 = getelementptr inbounds [2 x i32], [2 x i32]* %strides46, i32 0, i32 %84
  %85 = load i32, i32* %arrayidx47, align 4, !tbaa !12
  %86 = load i8*, i8** %plane_dst27, align 4, !tbaa !2
  %add.ptr48 = getelementptr inbounds i8, i8* %86, i32 %85
  store i8* %add.ptr48, i8** %plane_dst27, align 4, !tbaa !2
  br label %for.inc49

for.inc49:                                        ; preds = %for.body40
  %87 = load i32, i32* %row33, align 4, !tbaa !6
  %inc50 = add nsw i32 %87, 1
  store i32 %inc50, i32* %row33, align 4, !tbaa !6
  br label %for.cond34

for.end51:                                        ; preds = %for.cond.cleanup39
  %88 = bitcast i32* %is_uv30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i8** %plane_dst27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i8** %plane_src24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  br label %for.inc52

for.inc52:                                        ; preds = %for.end51
  %91 = load i32, i32* %plane18, align 4, !tbaa !6
  %inc53 = add nsw i32 %91, 1
  store i32 %inc53, i32* %plane18, align 4, !tbaa !6
  br label %for.cond19

for.end54:                                        ; preds = %for.cond.cleanup22
  %92 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %93 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  call void @aom_yv12_extend_frame_borders_c(%struct.yv12_buffer_config* %92, i32 %93)
  br label %return

return:                                           ; preds = %for.end54, %for.end17
  ret void
}

; Function Attrs: nounwind
define internal void @memcpy_short_addr(i8* %dst8, i8* %src8, i32 %num) #0 {
entry:
  %dst8.addr = alloca i8*, align 4
  %src8.addr = alloca i8*, align 4
  %num.addr = alloca i32, align 4
  %dst = alloca i16*, align 4
  %src = alloca i16*, align 4
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !6
  %0 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %2 = ptrtoint i8* %1 to i32
  %shl = shl i32 %2, 1
  %3 = inttoptr i32 %shl to i16*
  store i16* %3, i16** %dst, align 4, !tbaa !2
  %4 = bitcast i16** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %6 = ptrtoint i8* %5 to i32
  %shl1 = shl i32 %6, 1
  %7 = inttoptr i32 %shl1 to i16*
  store i16* %7, i16** %src, align 4, !tbaa !2
  %8 = load i16*, i16** %dst, align 4, !tbaa !2
  %9 = bitcast i16* %8 to i8*
  %10 = load i16*, i16** %src, align 4, !tbaa !2
  %11 = bitcast i16* %10 to i8*
  %12 = load i32, i32* %num.addr, align 4, !tbaa !6
  %mul = mul i32 %12, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %9, i8* align 2 %11, i32 %mul, i1 false)
  %13 = bitcast i16** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden void @aom_yv12_copy_y_c(%struct.yv12_buffer_config* %src_ybc, %struct.yv12_buffer_config* %dst_ybc) #0 {
entry:
  %src_ybc.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst_ybc.addr = alloca %struct.yv12_buffer_config*, align 4
  %row = alloca i32, align 4
  %src = alloca i8*, align 4
  %dst = alloca i8*, align 4
  %src16 = alloca i16*, align 4
  %dst16 = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_ybc, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %dst_ybc, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 5
  %4 = bitcast %union.anon.8* %3 to %struct.anon.9*
  %y_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %4, i32 0, i32 0
  %5 = load i8*, i8** %y_buffer, align 4, !tbaa !12
  store i8* %5, i8** %src, align 4, !tbaa !2
  %6 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 5
  %9 = bitcast %union.anon.8* %8 to %struct.anon.9*
  %y_buffer1 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %9, i32 0, i32 0
  %10 = load i8*, i8** %y_buffer1, align 4, !tbaa !12
  store i8* %10, i8** %dst, align 4, !tbaa !2
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 26
  %12 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %12, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i8*, i8** %src, align 4, !tbaa !2
  %15 = ptrtoint i8* %14 to i32
  %shl = shl i32 %15, 1
  %16 = inttoptr i32 %shl to i16*
  store i16* %16, i16** %src16, align 4, !tbaa !2
  %17 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load i8*, i8** %dst, align 4, !tbaa !2
  %19 = ptrtoint i8* %18 to i32
  %shl2 = shl i32 %19, 1
  %20 = inttoptr i32 %shl2 to i16*
  store i16* %20, i16** %dst16, align 4, !tbaa !2
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %21 = load i32, i32* %row, align 4, !tbaa !6
  %22 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %23 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %22, i32 0, i32 1
  %24 = bitcast %union.anon.0* %23 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %24, i32 0, i32 0
  %25 = load i32, i32* %y_height, align 4, !tbaa !12
  %cmp = icmp slt i32 %21, %25
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i16*, i16** %dst16, align 4, !tbaa !2
  %27 = bitcast i16* %26 to i8*
  %28 = load i16*, i16** %src16, align 4, !tbaa !2
  %29 = bitcast i16* %28 to i8*
  %30 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %31 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %30, i32 0, i32 0
  %32 = bitcast %union.anon* %31 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %32, i32 0, i32 0
  %33 = load i32, i32* %y_width, align 4, !tbaa !12
  %mul = mul i32 %33, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %27, i8* align 2 %29, i32 %mul, i1 false)
  %34 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %35 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %34, i32 0, i32 4
  %36 = bitcast %union.anon.6* %35 to %struct.anon.7*
  %y_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %36, i32 0, i32 0
  %37 = load i32, i32* %y_stride, align 4, !tbaa !12
  %38 = load i16*, i16** %src16, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %38, i32 %37
  store i16* %add.ptr, i16** %src16, align 4, !tbaa !2
  %39 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %40 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %39, i32 0, i32 4
  %41 = bitcast %union.anon.6* %40 to %struct.anon.7*
  %y_stride3 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %41, i32 0, i32 0
  %42 = load i32, i32* %y_stride3, align 4, !tbaa !12
  %43 = load i16*, i16** %dst16, align 4, !tbaa !2
  %add.ptr4 = getelementptr inbounds i16, i16* %43, i32 %42
  store i16* %add.ptr4, i16** %dst16, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc14, %if.end
  %47 = load i32, i32* %row, align 4, !tbaa !6
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %49 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 1
  %50 = bitcast %union.anon.0* %49 to %struct.anon.1*
  %y_height6 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %50, i32 0, i32 0
  %51 = load i32, i32* %y_height6, align 4, !tbaa !12
  %cmp7 = icmp slt i32 %47, %51
  br i1 %cmp7, label %for.body8, label %for.end16

for.body8:                                        ; preds = %for.cond5
  %52 = load i8*, i8** %dst, align 4, !tbaa !2
  %53 = load i8*, i8** %src, align 4, !tbaa !2
  %54 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %55 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %54, i32 0, i32 0
  %56 = bitcast %union.anon* %55 to %struct.anon*
  %y_width9 = getelementptr inbounds %struct.anon, %struct.anon* %56, i32 0, i32 0
  %57 = load i32, i32* %y_width9, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %52, i8* align 1 %53, i32 %57, i1 false)
  %58 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %59 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %58, i32 0, i32 4
  %60 = bitcast %union.anon.6* %59 to %struct.anon.7*
  %y_stride10 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %60, i32 0, i32 0
  %61 = load i32, i32* %y_stride10, align 4, !tbaa !12
  %62 = load i8*, i8** %src, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i8, i8* %62, i32 %61
  store i8* %add.ptr11, i8** %src, align 4, !tbaa !2
  %63 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %64 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %63, i32 0, i32 4
  %65 = bitcast %union.anon.6* %64 to %struct.anon.7*
  %y_stride12 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %65, i32 0, i32 0
  %66 = load i32, i32* %y_stride12, align 4, !tbaa !12
  %67 = load i8*, i8** %dst, align 4, !tbaa !2
  %add.ptr13 = getelementptr inbounds i8, i8* %67, i32 %66
  store i8* %add.ptr13, i8** %dst, align 4, !tbaa !2
  br label %for.inc14

for.inc14:                                        ; preds = %for.body8
  %68 = load i32, i32* %row, align 4, !tbaa !6
  %inc15 = add nsw i32 %68, 1
  store i32 %inc15, i32* %row, align 4, !tbaa !6
  br label %for.cond5

for.end16:                                        ; preds = %for.cond5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end16, %for.end
  %69 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %70 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @aom_yv12_copy_u_c(%struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config* %dst_bc) #0 {
entry:
  %src_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %row = alloca i32, align 4
  %src = alloca i8*, align 4
  %dst = alloca i8*, align 4
  %src16 = alloca i16*, align 4
  %dst16 = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %dst_bc, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 5
  %4 = bitcast %union.anon.8* %3 to %struct.anon.9*
  %u_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %4, i32 0, i32 1
  %5 = load i8*, i8** %u_buffer, align 4, !tbaa !12
  store i8* %5, i8** %src, align 4, !tbaa !2
  %6 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 5
  %9 = bitcast %union.anon.8* %8 to %struct.anon.9*
  %u_buffer1 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %9, i32 0, i32 1
  %10 = load i8*, i8** %u_buffer1, align 4, !tbaa !12
  store i8* %10, i8** %dst, align 4, !tbaa !2
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 26
  %12 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %12, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i8*, i8** %src, align 4, !tbaa !2
  %15 = ptrtoint i8* %14 to i32
  %shl = shl i32 %15, 1
  %16 = inttoptr i32 %shl to i16*
  store i16* %16, i16** %src16, align 4, !tbaa !2
  %17 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load i8*, i8** %dst, align 4, !tbaa !2
  %19 = ptrtoint i8* %18 to i32
  %shl2 = shl i32 %19, 1
  %20 = inttoptr i32 %shl2 to i16*
  store i16* %20, i16** %dst16, align 4, !tbaa !2
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %21 = load i32, i32* %row, align 4, !tbaa !6
  %22 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %23 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %22, i32 0, i32 1
  %24 = bitcast %union.anon.0* %23 to %struct.anon.1*
  %uv_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %24, i32 0, i32 1
  %25 = load i32, i32* %uv_height, align 4, !tbaa !12
  %cmp = icmp slt i32 %21, %25
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i16*, i16** %dst16, align 4, !tbaa !2
  %27 = bitcast i16* %26 to i8*
  %28 = load i16*, i16** %src16, align 4, !tbaa !2
  %29 = bitcast i16* %28 to i8*
  %30 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %31 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %30, i32 0, i32 0
  %32 = bitcast %union.anon* %31 to %struct.anon*
  %uv_width = getelementptr inbounds %struct.anon, %struct.anon* %32, i32 0, i32 1
  %33 = load i32, i32* %uv_width, align 4, !tbaa !12
  %mul = mul i32 %33, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %27, i8* align 2 %29, i32 %mul, i1 false)
  %34 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %35 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %34, i32 0, i32 4
  %36 = bitcast %union.anon.6* %35 to %struct.anon.7*
  %uv_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %36, i32 0, i32 1
  %37 = load i32, i32* %uv_stride, align 4, !tbaa !12
  %38 = load i16*, i16** %src16, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %38, i32 %37
  store i16* %add.ptr, i16** %src16, align 4, !tbaa !2
  %39 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %40 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %39, i32 0, i32 4
  %41 = bitcast %union.anon.6* %40 to %struct.anon.7*
  %uv_stride3 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %41, i32 0, i32 1
  %42 = load i32, i32* %uv_stride3, align 4, !tbaa !12
  %43 = load i16*, i16** %dst16, align 4, !tbaa !2
  %add.ptr4 = getelementptr inbounds i16, i16* %43, i32 %42
  store i16* %add.ptr4, i16** %dst16, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc14, %if.end
  %47 = load i32, i32* %row, align 4, !tbaa !6
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %49 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 1
  %50 = bitcast %union.anon.0* %49 to %struct.anon.1*
  %uv_height6 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %50, i32 0, i32 1
  %51 = load i32, i32* %uv_height6, align 4, !tbaa !12
  %cmp7 = icmp slt i32 %47, %51
  br i1 %cmp7, label %for.body8, label %for.end16

for.body8:                                        ; preds = %for.cond5
  %52 = load i8*, i8** %dst, align 4, !tbaa !2
  %53 = load i8*, i8** %src, align 4, !tbaa !2
  %54 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %55 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %54, i32 0, i32 0
  %56 = bitcast %union.anon* %55 to %struct.anon*
  %uv_width9 = getelementptr inbounds %struct.anon, %struct.anon* %56, i32 0, i32 1
  %57 = load i32, i32* %uv_width9, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %52, i8* align 1 %53, i32 %57, i1 false)
  %58 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %59 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %58, i32 0, i32 4
  %60 = bitcast %union.anon.6* %59 to %struct.anon.7*
  %uv_stride10 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %60, i32 0, i32 1
  %61 = load i32, i32* %uv_stride10, align 4, !tbaa !12
  %62 = load i8*, i8** %src, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i8, i8* %62, i32 %61
  store i8* %add.ptr11, i8** %src, align 4, !tbaa !2
  %63 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %64 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %63, i32 0, i32 4
  %65 = bitcast %union.anon.6* %64 to %struct.anon.7*
  %uv_stride12 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %65, i32 0, i32 1
  %66 = load i32, i32* %uv_stride12, align 4, !tbaa !12
  %67 = load i8*, i8** %dst, align 4, !tbaa !2
  %add.ptr13 = getelementptr inbounds i8, i8* %67, i32 %66
  store i8* %add.ptr13, i8** %dst, align 4, !tbaa !2
  br label %for.inc14

for.inc14:                                        ; preds = %for.body8
  %68 = load i32, i32* %row, align 4, !tbaa !6
  %inc15 = add nsw i32 %68, 1
  store i32 %inc15, i32* %row, align 4, !tbaa !6
  br label %for.cond5

for.end16:                                        ; preds = %for.cond5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end16, %for.end
  %69 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %70 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @aom_yv12_copy_v_c(%struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config* %dst_bc) #0 {
entry:
  %src_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %row = alloca i32, align 4
  %src = alloca i8*, align 4
  %dst = alloca i8*, align 4
  %src16 = alloca i16*, align 4
  %dst16 = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %dst_bc, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 5
  %4 = bitcast %union.anon.8* %3 to %struct.anon.9*
  %v_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %4, i32 0, i32 2
  %5 = load i8*, i8** %v_buffer, align 4, !tbaa !12
  store i8* %5, i8** %src, align 4, !tbaa !2
  %6 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 5
  %9 = bitcast %union.anon.8* %8 to %struct.anon.9*
  %v_buffer1 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %9, i32 0, i32 2
  %10 = load i8*, i8** %v_buffer1, align 4, !tbaa !12
  store i8* %10, i8** %dst, align 4, !tbaa !2
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 26
  %12 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %12, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i8*, i8** %src, align 4, !tbaa !2
  %15 = ptrtoint i8* %14 to i32
  %shl = shl i32 %15, 1
  %16 = inttoptr i32 %shl to i16*
  store i16* %16, i16** %src16, align 4, !tbaa !2
  %17 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load i8*, i8** %dst, align 4, !tbaa !2
  %19 = ptrtoint i8* %18 to i32
  %shl2 = shl i32 %19, 1
  %20 = inttoptr i32 %shl2 to i16*
  store i16* %20, i16** %dst16, align 4, !tbaa !2
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %21 = load i32, i32* %row, align 4, !tbaa !6
  %22 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %23 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %22, i32 0, i32 1
  %24 = bitcast %union.anon.0* %23 to %struct.anon.1*
  %uv_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %24, i32 0, i32 1
  %25 = load i32, i32* %uv_height, align 4, !tbaa !12
  %cmp = icmp slt i32 %21, %25
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i16*, i16** %dst16, align 4, !tbaa !2
  %27 = bitcast i16* %26 to i8*
  %28 = load i16*, i16** %src16, align 4, !tbaa !2
  %29 = bitcast i16* %28 to i8*
  %30 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %31 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %30, i32 0, i32 0
  %32 = bitcast %union.anon* %31 to %struct.anon*
  %uv_width = getelementptr inbounds %struct.anon, %struct.anon* %32, i32 0, i32 1
  %33 = load i32, i32* %uv_width, align 4, !tbaa !12
  %mul = mul i32 %33, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %27, i8* align 2 %29, i32 %mul, i1 false)
  %34 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %35 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %34, i32 0, i32 4
  %36 = bitcast %union.anon.6* %35 to %struct.anon.7*
  %uv_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %36, i32 0, i32 1
  %37 = load i32, i32* %uv_stride, align 4, !tbaa !12
  %38 = load i16*, i16** %src16, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %38, i32 %37
  store i16* %add.ptr, i16** %src16, align 4, !tbaa !2
  %39 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %40 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %39, i32 0, i32 4
  %41 = bitcast %union.anon.6* %40 to %struct.anon.7*
  %uv_stride3 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %41, i32 0, i32 1
  %42 = load i32, i32* %uv_stride3, align 4, !tbaa !12
  %43 = load i16*, i16** %dst16, align 4, !tbaa !2
  %add.ptr4 = getelementptr inbounds i16, i16* %43, i32 %42
  store i16* %add.ptr4, i16** %dst16, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc14, %if.end
  %47 = load i32, i32* %row, align 4, !tbaa !6
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %49 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 1
  %50 = bitcast %union.anon.0* %49 to %struct.anon.1*
  %uv_height6 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %50, i32 0, i32 1
  %51 = load i32, i32* %uv_height6, align 4, !tbaa !12
  %cmp7 = icmp slt i32 %47, %51
  br i1 %cmp7, label %for.body8, label %for.end16

for.body8:                                        ; preds = %for.cond5
  %52 = load i8*, i8** %dst, align 4, !tbaa !2
  %53 = load i8*, i8** %src, align 4, !tbaa !2
  %54 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %55 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %54, i32 0, i32 0
  %56 = bitcast %union.anon* %55 to %struct.anon*
  %uv_width9 = getelementptr inbounds %struct.anon, %struct.anon* %56, i32 0, i32 1
  %57 = load i32, i32* %uv_width9, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %52, i8* align 1 %53, i32 %57, i1 false)
  %58 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %59 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %58, i32 0, i32 4
  %60 = bitcast %union.anon.6* %59 to %struct.anon.7*
  %uv_stride10 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %60, i32 0, i32 1
  %61 = load i32, i32* %uv_stride10, align 4, !tbaa !12
  %62 = load i8*, i8** %src, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i8, i8* %62, i32 %61
  store i8* %add.ptr11, i8** %src, align 4, !tbaa !2
  %63 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %64 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %63, i32 0, i32 4
  %65 = bitcast %union.anon.6* %64 to %struct.anon.7*
  %uv_stride12 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %65, i32 0, i32 1
  %66 = load i32, i32* %uv_stride12, align 4, !tbaa !12
  %67 = load i8*, i8** %dst, align 4, !tbaa !2
  %add.ptr13 = getelementptr inbounds i8, i8* %67, i32 %66
  store i8* %add.ptr13, i8** %dst, align 4, !tbaa !2
  br label %for.inc14

for.inc14:                                        ; preds = %for.body8
  %68 = load i32, i32* %row, align 4, !tbaa !6
  %inc15 = add nsw i32 %68, 1
  store i32 %inc15, i32* %row, align 4, !tbaa !6
  br label %for.cond5

for.end16:                                        ; preds = %for.cond5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end16, %for.end
  %69 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %70 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @aom_yv12_partial_copy_y_c(%struct.yv12_buffer_config* %src_ybc, i32 %hstart1, i32 %hend1, i32 %vstart1, i32 %vend1, %struct.yv12_buffer_config* %dst_ybc, i32 %hstart2, i32 %vstart2) #0 {
entry:
  %src_ybc.addr = alloca %struct.yv12_buffer_config*, align 4
  %hstart1.addr = alloca i32, align 4
  %hend1.addr = alloca i32, align 4
  %vstart1.addr = alloca i32, align 4
  %vend1.addr = alloca i32, align 4
  %dst_ybc.addr = alloca %struct.yv12_buffer_config*, align 4
  %hstart2.addr = alloca i32, align 4
  %vstart2.addr = alloca i32, align 4
  %row = alloca i32, align 4
  %src = alloca i8*, align 4
  %dst = alloca i8*, align 4
  %src16 = alloca i16*, align 4
  %dst16 = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_ybc, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  store i32 %hstart1, i32* %hstart1.addr, align 4, !tbaa !6
  store i32 %hend1, i32* %hend1.addr, align 4, !tbaa !6
  store i32 %vstart1, i32* %vstart1.addr, align 4, !tbaa !6
  store i32 %vend1, i32* %vend1.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %dst_ybc, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  store i32 %hstart2, i32* %hstart2.addr, align 4, !tbaa !6
  store i32 %vstart2, i32* %vstart2.addr, align 4, !tbaa !6
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 5
  %4 = bitcast %union.anon.8* %3 to %struct.anon.9*
  %y_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %4, i32 0, i32 0
  %5 = load i8*, i8** %y_buffer, align 4, !tbaa !12
  store i8* %5, i8** %src, align 4, !tbaa !2
  %6 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 5
  %9 = bitcast %union.anon.8* %8 to %struct.anon.9*
  %y_buffer1 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %9, i32 0, i32 0
  %10 = load i8*, i8** %y_buffer1, align 4, !tbaa !12
  store i8* %10, i8** %dst, align 4, !tbaa !2
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 26
  %12 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %12, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i8*, i8** %src, align 4, !tbaa !2
  %15 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  %16 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %17 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %16, i32 0, i32 4
  %18 = bitcast %union.anon.6* %17 to %struct.anon.7*
  %y_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %18, i32 0, i32 0
  %19 = load i32, i32* %y_stride, align 4, !tbaa !12
  %mul = mul nsw i32 %15, %19
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %mul
  %20 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %add.ptr2 = getelementptr inbounds i8, i8* %add.ptr, i32 %20
  %21 = ptrtoint i8* %add.ptr2 to i32
  %shl = shl i32 %21, 1
  %22 = inttoptr i32 %shl to i16*
  store i16* %22, i16** %src16, align 4, !tbaa !2
  %23 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load i8*, i8** %dst, align 4, !tbaa !2
  %25 = load i32, i32* %vstart2.addr, align 4, !tbaa !6
  %26 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %27 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %26, i32 0, i32 4
  %28 = bitcast %union.anon.6* %27 to %struct.anon.7*
  %y_stride3 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %28, i32 0, i32 0
  %29 = load i32, i32* %y_stride3, align 4, !tbaa !12
  %mul4 = mul nsw i32 %25, %29
  %add.ptr5 = getelementptr inbounds i8, i8* %24, i32 %mul4
  %30 = load i32, i32* %hstart2.addr, align 4, !tbaa !6
  %add.ptr6 = getelementptr inbounds i8, i8* %add.ptr5, i32 %30
  %31 = ptrtoint i8* %add.ptr6 to i32
  %shl7 = shl i32 %31, 1
  %32 = inttoptr i32 %shl7 to i16*
  store i16* %32, i16** %dst16, align 4, !tbaa !2
  %33 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  store i32 %33, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %34 = load i32, i32* %row, align 4, !tbaa !6
  %35 = load i32, i32* %vend1.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %34, %35
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i16*, i16** %dst16, align 4, !tbaa !2
  %37 = bitcast i16* %36 to i8*
  %38 = load i16*, i16** %src16, align 4, !tbaa !2
  %39 = bitcast i16* %38 to i8*
  %40 = load i32, i32* %hend1.addr, align 4, !tbaa !6
  %41 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %40, %41
  %mul8 = mul i32 %sub, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %37, i8* align 2 %39, i32 %mul8, i1 false)
  %42 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %43 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %42, i32 0, i32 4
  %44 = bitcast %union.anon.6* %43 to %struct.anon.7*
  %y_stride9 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %44, i32 0, i32 0
  %45 = load i32, i32* %y_stride9, align 4, !tbaa !12
  %46 = load i16*, i16** %src16, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i16, i16* %46, i32 %45
  store i16* %add.ptr10, i16** %src16, align 4, !tbaa !2
  %47 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %48 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %47, i32 0, i32 4
  %49 = bitcast %union.anon.6* %48 to %struct.anon.7*
  %y_stride11 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %49, i32 0, i32 0
  %50 = load i32, i32* %y_stride11, align 4, !tbaa !12
  %51 = load i16*, i16** %dst16, align 4, !tbaa !2
  %add.ptr12 = getelementptr inbounds i16, i16* %51, i32 %50
  store i16* %add.ptr12, i16** %dst16, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %52 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  %53 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  br label %cleanup

if.end:                                           ; preds = %entry
  %55 = load i8*, i8** %src, align 4, !tbaa !2
  %56 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  %57 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %58 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %57, i32 0, i32 4
  %59 = bitcast %union.anon.6* %58 to %struct.anon.7*
  %y_stride13 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %59, i32 0, i32 0
  %60 = load i32, i32* %y_stride13, align 4, !tbaa !12
  %mul14 = mul nsw i32 %56, %60
  %add.ptr15 = getelementptr inbounds i8, i8* %55, i32 %mul14
  %61 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %add.ptr16 = getelementptr inbounds i8, i8* %add.ptr15, i32 %61
  store i8* %add.ptr16, i8** %src, align 4, !tbaa !2
  %62 = load i8*, i8** %dst, align 4, !tbaa !2
  %63 = load i32, i32* %vstart2.addr, align 4, !tbaa !6
  %64 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %65 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %64, i32 0, i32 4
  %66 = bitcast %union.anon.6* %65 to %struct.anon.7*
  %y_stride17 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %66, i32 0, i32 0
  %67 = load i32, i32* %y_stride17, align 4, !tbaa !12
  %mul18 = mul nsw i32 %63, %67
  %add.ptr19 = getelementptr inbounds i8, i8* %62, i32 %mul18
  %68 = load i32, i32* %hstart2.addr, align 4, !tbaa !6
  %add.ptr20 = getelementptr inbounds i8, i8* %add.ptr19, i32 %68
  store i8* %add.ptr20, i8** %dst, align 4, !tbaa !2
  %69 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  store i32 %69, i32* %row, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc29, %if.end
  %70 = load i32, i32* %row, align 4, !tbaa !6
  %71 = load i32, i32* %vend1.addr, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %70, %71
  br i1 %cmp22, label %for.body23, label %for.end31

for.body23:                                       ; preds = %for.cond21
  %72 = load i8*, i8** %dst, align 4, !tbaa !2
  %73 = load i8*, i8** %src, align 4, !tbaa !2
  %74 = load i32, i32* %hend1.addr, align 4, !tbaa !6
  %75 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %sub24 = sub nsw i32 %74, %75
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %72, i8* align 1 %73, i32 %sub24, i1 false)
  %76 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %77 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %76, i32 0, i32 4
  %78 = bitcast %union.anon.6* %77 to %struct.anon.7*
  %y_stride25 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %78, i32 0, i32 0
  %79 = load i32, i32* %y_stride25, align 4, !tbaa !12
  %80 = load i8*, i8** %src, align 4, !tbaa !2
  %add.ptr26 = getelementptr inbounds i8, i8* %80, i32 %79
  store i8* %add.ptr26, i8** %src, align 4, !tbaa !2
  %81 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %82 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %81, i32 0, i32 4
  %83 = bitcast %union.anon.6* %82 to %struct.anon.7*
  %y_stride27 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %83, i32 0, i32 0
  %84 = load i32, i32* %y_stride27, align 4, !tbaa !12
  %85 = load i8*, i8** %dst, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds i8, i8* %85, i32 %84
  store i8* %add.ptr28, i8** %dst, align 4, !tbaa !2
  br label %for.inc29

for.inc29:                                        ; preds = %for.body23
  %86 = load i32, i32* %row, align 4, !tbaa !6
  %inc30 = add nsw i32 %86, 1
  store i32 %inc30, i32* %row, align 4, !tbaa !6
  br label %for.cond21

for.end31:                                        ; preds = %for.cond21
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end31, %for.end
  %87 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @aom_yv12_partial_coloc_copy_y_c(%struct.yv12_buffer_config* %src_ybc, %struct.yv12_buffer_config* %dst_ybc, i32 %hstart, i32 %hend, i32 %vstart, i32 %vend) #0 {
entry:
  %src_ybc.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst_ybc.addr = alloca %struct.yv12_buffer_config*, align 4
  %hstart.addr = alloca i32, align 4
  %hend.addr = alloca i32, align 4
  %vstart.addr = alloca i32, align 4
  %vend.addr = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_ybc, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %dst_ybc, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  store i32 %hstart, i32* %hstart.addr, align 4, !tbaa !6
  store i32 %hend, i32* %hend.addr, align 4, !tbaa !6
  store i32 %vstart, i32* %vstart.addr, align 4, !tbaa !6
  store i32 %vend, i32* %vend.addr, align 4, !tbaa !6
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_ybc.addr, align 4, !tbaa !2
  %1 = load i32, i32* %hstart.addr, align 4, !tbaa !6
  %2 = load i32, i32* %hend.addr, align 4, !tbaa !6
  %3 = load i32, i32* %vstart.addr, align 4, !tbaa !6
  %4 = load i32, i32* %vend.addr, align 4, !tbaa !6
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_ybc.addr, align 4, !tbaa !2
  %6 = load i32, i32* %hstart.addr, align 4, !tbaa !6
  %7 = load i32, i32* %vstart.addr, align 4, !tbaa !6
  call void @aom_yv12_partial_copy_y_c(%struct.yv12_buffer_config* %0, i32 %1, i32 %2, i32 %3, i32 %4, %struct.yv12_buffer_config* %5, i32 %6, i32 %7)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_yv12_partial_copy_u_c(%struct.yv12_buffer_config* %src_bc, i32 %hstart1, i32 %hend1, i32 %vstart1, i32 %vend1, %struct.yv12_buffer_config* %dst_bc, i32 %hstart2, i32 %vstart2) #0 {
entry:
  %src_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %hstart1.addr = alloca i32, align 4
  %hend1.addr = alloca i32, align 4
  %vstart1.addr = alloca i32, align 4
  %vend1.addr = alloca i32, align 4
  %dst_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %hstart2.addr = alloca i32, align 4
  %vstart2.addr = alloca i32, align 4
  %row = alloca i32, align 4
  %src = alloca i8*, align 4
  %dst = alloca i8*, align 4
  %src16 = alloca i16*, align 4
  %dst16 = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  store i32 %hstart1, i32* %hstart1.addr, align 4, !tbaa !6
  store i32 %hend1, i32* %hend1.addr, align 4, !tbaa !6
  store i32 %vstart1, i32* %vstart1.addr, align 4, !tbaa !6
  store i32 %vend1, i32* %vend1.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %dst_bc, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  store i32 %hstart2, i32* %hstart2.addr, align 4, !tbaa !6
  store i32 %vstart2, i32* %vstart2.addr, align 4, !tbaa !6
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 5
  %4 = bitcast %union.anon.8* %3 to %struct.anon.9*
  %u_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %4, i32 0, i32 1
  %5 = load i8*, i8** %u_buffer, align 4, !tbaa !12
  store i8* %5, i8** %src, align 4, !tbaa !2
  %6 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 5
  %9 = bitcast %union.anon.8* %8 to %struct.anon.9*
  %u_buffer1 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %9, i32 0, i32 1
  %10 = load i8*, i8** %u_buffer1, align 4, !tbaa !12
  store i8* %10, i8** %dst, align 4, !tbaa !2
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 26
  %12 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %12, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i8*, i8** %src, align 4, !tbaa !2
  %15 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  %16 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %17 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %16, i32 0, i32 4
  %18 = bitcast %union.anon.6* %17 to %struct.anon.7*
  %uv_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %18, i32 0, i32 1
  %19 = load i32, i32* %uv_stride, align 4, !tbaa !12
  %mul = mul nsw i32 %15, %19
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %mul
  %20 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %add.ptr2 = getelementptr inbounds i8, i8* %add.ptr, i32 %20
  %21 = ptrtoint i8* %add.ptr2 to i32
  %shl = shl i32 %21, 1
  %22 = inttoptr i32 %shl to i16*
  store i16* %22, i16** %src16, align 4, !tbaa !2
  %23 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load i8*, i8** %dst, align 4, !tbaa !2
  %25 = load i32, i32* %vstart2.addr, align 4, !tbaa !6
  %26 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %27 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %26, i32 0, i32 4
  %28 = bitcast %union.anon.6* %27 to %struct.anon.7*
  %uv_stride3 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %28, i32 0, i32 1
  %29 = load i32, i32* %uv_stride3, align 4, !tbaa !12
  %mul4 = mul nsw i32 %25, %29
  %add.ptr5 = getelementptr inbounds i8, i8* %24, i32 %mul4
  %30 = load i32, i32* %hstart2.addr, align 4, !tbaa !6
  %add.ptr6 = getelementptr inbounds i8, i8* %add.ptr5, i32 %30
  %31 = ptrtoint i8* %add.ptr6 to i32
  %shl7 = shl i32 %31, 1
  %32 = inttoptr i32 %shl7 to i16*
  store i16* %32, i16** %dst16, align 4, !tbaa !2
  %33 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  store i32 %33, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %34 = load i32, i32* %row, align 4, !tbaa !6
  %35 = load i32, i32* %vend1.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %34, %35
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i16*, i16** %dst16, align 4, !tbaa !2
  %37 = bitcast i16* %36 to i8*
  %38 = load i16*, i16** %src16, align 4, !tbaa !2
  %39 = bitcast i16* %38 to i8*
  %40 = load i32, i32* %hend1.addr, align 4, !tbaa !6
  %41 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %40, %41
  %mul8 = mul i32 %sub, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %37, i8* align 2 %39, i32 %mul8, i1 false)
  %42 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %43 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %42, i32 0, i32 4
  %44 = bitcast %union.anon.6* %43 to %struct.anon.7*
  %uv_stride9 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %44, i32 0, i32 1
  %45 = load i32, i32* %uv_stride9, align 4, !tbaa !12
  %46 = load i16*, i16** %src16, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i16, i16* %46, i32 %45
  store i16* %add.ptr10, i16** %src16, align 4, !tbaa !2
  %47 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %48 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %47, i32 0, i32 4
  %49 = bitcast %union.anon.6* %48 to %struct.anon.7*
  %uv_stride11 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %49, i32 0, i32 1
  %50 = load i32, i32* %uv_stride11, align 4, !tbaa !12
  %51 = load i16*, i16** %dst16, align 4, !tbaa !2
  %add.ptr12 = getelementptr inbounds i16, i16* %51, i32 %50
  store i16* %add.ptr12, i16** %dst16, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %52 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  %53 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  br label %cleanup

if.end:                                           ; preds = %entry
  %55 = load i8*, i8** %src, align 4, !tbaa !2
  %56 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  %57 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %58 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %57, i32 0, i32 4
  %59 = bitcast %union.anon.6* %58 to %struct.anon.7*
  %uv_stride13 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %59, i32 0, i32 1
  %60 = load i32, i32* %uv_stride13, align 4, !tbaa !12
  %mul14 = mul nsw i32 %56, %60
  %add.ptr15 = getelementptr inbounds i8, i8* %55, i32 %mul14
  %61 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %add.ptr16 = getelementptr inbounds i8, i8* %add.ptr15, i32 %61
  store i8* %add.ptr16, i8** %src, align 4, !tbaa !2
  %62 = load i8*, i8** %dst, align 4, !tbaa !2
  %63 = load i32, i32* %vstart2.addr, align 4, !tbaa !6
  %64 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %65 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %64, i32 0, i32 4
  %66 = bitcast %union.anon.6* %65 to %struct.anon.7*
  %uv_stride17 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %66, i32 0, i32 1
  %67 = load i32, i32* %uv_stride17, align 4, !tbaa !12
  %mul18 = mul nsw i32 %63, %67
  %add.ptr19 = getelementptr inbounds i8, i8* %62, i32 %mul18
  %68 = load i32, i32* %hstart2.addr, align 4, !tbaa !6
  %add.ptr20 = getelementptr inbounds i8, i8* %add.ptr19, i32 %68
  store i8* %add.ptr20, i8** %dst, align 4, !tbaa !2
  %69 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  store i32 %69, i32* %row, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc29, %if.end
  %70 = load i32, i32* %row, align 4, !tbaa !6
  %71 = load i32, i32* %vend1.addr, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %70, %71
  br i1 %cmp22, label %for.body23, label %for.end31

for.body23:                                       ; preds = %for.cond21
  %72 = load i8*, i8** %dst, align 4, !tbaa !2
  %73 = load i8*, i8** %src, align 4, !tbaa !2
  %74 = load i32, i32* %hend1.addr, align 4, !tbaa !6
  %75 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %sub24 = sub nsw i32 %74, %75
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %72, i8* align 1 %73, i32 %sub24, i1 false)
  %76 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %77 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %76, i32 0, i32 4
  %78 = bitcast %union.anon.6* %77 to %struct.anon.7*
  %uv_stride25 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %78, i32 0, i32 1
  %79 = load i32, i32* %uv_stride25, align 4, !tbaa !12
  %80 = load i8*, i8** %src, align 4, !tbaa !2
  %add.ptr26 = getelementptr inbounds i8, i8* %80, i32 %79
  store i8* %add.ptr26, i8** %src, align 4, !tbaa !2
  %81 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %82 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %81, i32 0, i32 4
  %83 = bitcast %union.anon.6* %82 to %struct.anon.7*
  %uv_stride27 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %83, i32 0, i32 1
  %84 = load i32, i32* %uv_stride27, align 4, !tbaa !12
  %85 = load i8*, i8** %dst, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds i8, i8* %85, i32 %84
  store i8* %add.ptr28, i8** %dst, align 4, !tbaa !2
  br label %for.inc29

for.inc29:                                        ; preds = %for.body23
  %86 = load i32, i32* %row, align 4, !tbaa !6
  %inc30 = add nsw i32 %86, 1
  store i32 %inc30, i32* %row, align 4, !tbaa !6
  br label %for.cond21

for.end31:                                        ; preds = %for.cond21
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end31, %for.end
  %87 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @aom_yv12_partial_coloc_copy_u_c(%struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config* %dst_bc, i32 %hstart, i32 %hend, i32 %vstart, i32 %vend) #0 {
entry:
  %src_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %hstart.addr = alloca i32, align 4
  %hend.addr = alloca i32, align 4
  %vstart.addr = alloca i32, align 4
  %vend.addr = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %dst_bc, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  store i32 %hstart, i32* %hstart.addr, align 4, !tbaa !6
  store i32 %hend, i32* %hend.addr, align 4, !tbaa !6
  store i32 %vstart, i32* %vstart.addr, align 4, !tbaa !6
  store i32 %vend, i32* %vend.addr, align 4, !tbaa !6
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %1 = load i32, i32* %hstart.addr, align 4, !tbaa !6
  %2 = load i32, i32* %hend.addr, align 4, !tbaa !6
  %3 = load i32, i32* %vstart.addr, align 4, !tbaa !6
  %4 = load i32, i32* %vend.addr, align 4, !tbaa !6
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %6 = load i32, i32* %hstart.addr, align 4, !tbaa !6
  %7 = load i32, i32* %vstart.addr, align 4, !tbaa !6
  call void @aom_yv12_partial_copy_u_c(%struct.yv12_buffer_config* %0, i32 %1, i32 %2, i32 %3, i32 %4, %struct.yv12_buffer_config* %5, i32 %6, i32 %7)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_yv12_partial_copy_v_c(%struct.yv12_buffer_config* %src_bc, i32 %hstart1, i32 %hend1, i32 %vstart1, i32 %vend1, %struct.yv12_buffer_config* %dst_bc, i32 %hstart2, i32 %vstart2) #0 {
entry:
  %src_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %hstart1.addr = alloca i32, align 4
  %hend1.addr = alloca i32, align 4
  %vstart1.addr = alloca i32, align 4
  %vend1.addr = alloca i32, align 4
  %dst_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %hstart2.addr = alloca i32, align 4
  %vstart2.addr = alloca i32, align 4
  %row = alloca i32, align 4
  %src = alloca i8*, align 4
  %dst = alloca i8*, align 4
  %src16 = alloca i16*, align 4
  %dst16 = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  store i32 %hstart1, i32* %hstart1.addr, align 4, !tbaa !6
  store i32 %hend1, i32* %hend1.addr, align 4, !tbaa !6
  store i32 %vstart1, i32* %vstart1.addr, align 4, !tbaa !6
  store i32 %vend1, i32* %vend1.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %dst_bc, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  store i32 %hstart2, i32* %hstart2.addr, align 4, !tbaa !6
  store i32 %vstart2, i32* %vstart2.addr, align 4, !tbaa !6
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 5
  %4 = bitcast %union.anon.8* %3 to %struct.anon.9*
  %v_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %4, i32 0, i32 2
  %5 = load i8*, i8** %v_buffer, align 4, !tbaa !12
  store i8* %5, i8** %src, align 4, !tbaa !2
  %6 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 5
  %9 = bitcast %union.anon.8* %8 to %struct.anon.9*
  %v_buffer1 = getelementptr inbounds %struct.anon.9, %struct.anon.9* %9, i32 0, i32 2
  %10 = load i8*, i8** %v_buffer1, align 4, !tbaa !12
  store i8* %10, i8** %dst, align 4, !tbaa !2
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 26
  %12 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %12, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i8*, i8** %src, align 4, !tbaa !2
  %15 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  %16 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %17 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %16, i32 0, i32 4
  %18 = bitcast %union.anon.6* %17 to %struct.anon.7*
  %uv_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %18, i32 0, i32 1
  %19 = load i32, i32* %uv_stride, align 4, !tbaa !12
  %mul = mul nsw i32 %15, %19
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %mul
  %20 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %add.ptr2 = getelementptr inbounds i8, i8* %add.ptr, i32 %20
  %21 = ptrtoint i8* %add.ptr2 to i32
  %shl = shl i32 %21, 1
  %22 = inttoptr i32 %shl to i16*
  store i16* %22, i16** %src16, align 4, !tbaa !2
  %23 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load i8*, i8** %dst, align 4, !tbaa !2
  %25 = load i32, i32* %vstart2.addr, align 4, !tbaa !6
  %26 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %27 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %26, i32 0, i32 4
  %28 = bitcast %union.anon.6* %27 to %struct.anon.7*
  %uv_stride3 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %28, i32 0, i32 1
  %29 = load i32, i32* %uv_stride3, align 4, !tbaa !12
  %mul4 = mul nsw i32 %25, %29
  %add.ptr5 = getelementptr inbounds i8, i8* %24, i32 %mul4
  %30 = load i32, i32* %hstart2.addr, align 4, !tbaa !6
  %add.ptr6 = getelementptr inbounds i8, i8* %add.ptr5, i32 %30
  %31 = ptrtoint i8* %add.ptr6 to i32
  %shl7 = shl i32 %31, 1
  %32 = inttoptr i32 %shl7 to i16*
  store i16* %32, i16** %dst16, align 4, !tbaa !2
  %33 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  store i32 %33, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %34 = load i32, i32* %row, align 4, !tbaa !6
  %35 = load i32, i32* %vend1.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %34, %35
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i16*, i16** %dst16, align 4, !tbaa !2
  %37 = bitcast i16* %36 to i8*
  %38 = load i16*, i16** %src16, align 4, !tbaa !2
  %39 = bitcast i16* %38 to i8*
  %40 = load i32, i32* %hend1.addr, align 4, !tbaa !6
  %41 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %40, %41
  %mul8 = mul i32 %sub, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %37, i8* align 2 %39, i32 %mul8, i1 false)
  %42 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %43 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %42, i32 0, i32 4
  %44 = bitcast %union.anon.6* %43 to %struct.anon.7*
  %uv_stride9 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %44, i32 0, i32 1
  %45 = load i32, i32* %uv_stride9, align 4, !tbaa !12
  %46 = load i16*, i16** %src16, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i16, i16* %46, i32 %45
  store i16* %add.ptr10, i16** %src16, align 4, !tbaa !2
  %47 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %48 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %47, i32 0, i32 4
  %49 = bitcast %union.anon.6* %48 to %struct.anon.7*
  %uv_stride11 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %49, i32 0, i32 1
  %50 = load i32, i32* %uv_stride11, align 4, !tbaa !12
  %51 = load i16*, i16** %dst16, align 4, !tbaa !2
  %add.ptr12 = getelementptr inbounds i16, i16* %51, i32 %50
  store i16* %add.ptr12, i16** %dst16, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %52 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  %53 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i16** %src16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  br label %cleanup

if.end:                                           ; preds = %entry
  %55 = load i8*, i8** %src, align 4, !tbaa !2
  %56 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  %57 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %58 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %57, i32 0, i32 4
  %59 = bitcast %union.anon.6* %58 to %struct.anon.7*
  %uv_stride13 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %59, i32 0, i32 1
  %60 = load i32, i32* %uv_stride13, align 4, !tbaa !12
  %mul14 = mul nsw i32 %56, %60
  %add.ptr15 = getelementptr inbounds i8, i8* %55, i32 %mul14
  %61 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %add.ptr16 = getelementptr inbounds i8, i8* %add.ptr15, i32 %61
  store i8* %add.ptr16, i8** %src, align 4, !tbaa !2
  %62 = load i8*, i8** %dst, align 4, !tbaa !2
  %63 = load i32, i32* %vstart2.addr, align 4, !tbaa !6
  %64 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %65 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %64, i32 0, i32 4
  %66 = bitcast %union.anon.6* %65 to %struct.anon.7*
  %uv_stride17 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %66, i32 0, i32 1
  %67 = load i32, i32* %uv_stride17, align 4, !tbaa !12
  %mul18 = mul nsw i32 %63, %67
  %add.ptr19 = getelementptr inbounds i8, i8* %62, i32 %mul18
  %68 = load i32, i32* %hstart2.addr, align 4, !tbaa !6
  %add.ptr20 = getelementptr inbounds i8, i8* %add.ptr19, i32 %68
  store i8* %add.ptr20, i8** %dst, align 4, !tbaa !2
  %69 = load i32, i32* %vstart1.addr, align 4, !tbaa !6
  store i32 %69, i32* %row, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc29, %if.end
  %70 = load i32, i32* %row, align 4, !tbaa !6
  %71 = load i32, i32* %vend1.addr, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %70, %71
  br i1 %cmp22, label %for.body23, label %for.end31

for.body23:                                       ; preds = %for.cond21
  %72 = load i8*, i8** %dst, align 4, !tbaa !2
  %73 = load i8*, i8** %src, align 4, !tbaa !2
  %74 = load i32, i32* %hend1.addr, align 4, !tbaa !6
  %75 = load i32, i32* %hstart1.addr, align 4, !tbaa !6
  %sub24 = sub nsw i32 %74, %75
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %72, i8* align 1 %73, i32 %sub24, i1 false)
  %76 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %77 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %76, i32 0, i32 4
  %78 = bitcast %union.anon.6* %77 to %struct.anon.7*
  %uv_stride25 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %78, i32 0, i32 1
  %79 = load i32, i32* %uv_stride25, align 4, !tbaa !12
  %80 = load i8*, i8** %src, align 4, !tbaa !2
  %add.ptr26 = getelementptr inbounds i8, i8* %80, i32 %79
  store i8* %add.ptr26, i8** %src, align 4, !tbaa !2
  %81 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %82 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %81, i32 0, i32 4
  %83 = bitcast %union.anon.6* %82 to %struct.anon.7*
  %uv_stride27 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %83, i32 0, i32 1
  %84 = load i32, i32* %uv_stride27, align 4, !tbaa !12
  %85 = load i8*, i8** %dst, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds i8, i8* %85, i32 %84
  store i8* %add.ptr28, i8** %dst, align 4, !tbaa !2
  br label %for.inc29

for.inc29:                                        ; preds = %for.body23
  %86 = load i32, i32* %row, align 4, !tbaa !6
  %inc30 = add nsw i32 %86, 1
  store i32 %inc30, i32* %row, align 4, !tbaa !6
  br label %for.cond21

for.end31:                                        ; preds = %for.cond21
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end31, %for.end
  %87 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @aom_yv12_partial_coloc_copy_v_c(%struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config* %dst_bc, i32 %hstart, i32 %hend, i32 %vstart, i32 %vend) #0 {
entry:
  %src_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst_bc.addr = alloca %struct.yv12_buffer_config*, align 4
  %hstart.addr = alloca i32, align 4
  %hend.addr = alloca i32, align 4
  %vstart.addr = alloca i32, align 4
  %vend.addr = alloca i32, align 4
  store %struct.yv12_buffer_config* %src_bc, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %dst_bc, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  store i32 %hstart, i32* %hstart.addr, align 4, !tbaa !6
  store i32 %hend, i32* %hend.addr, align 4, !tbaa !6
  store i32 %vstart, i32* %vstart.addr, align 4, !tbaa !6
  store i32 %vend, i32* %vend.addr, align 4, !tbaa !6
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src_bc.addr, align 4, !tbaa !2
  %1 = load i32, i32* %hstart.addr, align 4, !tbaa !6
  %2 = load i32, i32* %hend.addr, align 4, !tbaa !6
  %3 = load i32, i32* %vstart.addr, align 4, !tbaa !6
  %4 = load i32, i32* %vend.addr, align 4, !tbaa !6
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst_bc.addr, align 4, !tbaa !2
  %6 = load i32, i32* %hstart.addr, align 4, !tbaa !6
  %7 = load i32, i32* %vstart.addr, align 4, !tbaa !6
  call void @aom_yv12_partial_copy_v_c(%struct.yv12_buffer_config* %0, i32 %1, i32 %2, i32 %3, i32 %4, %struct.yv12_buffer_config* %5, i32 %6, i32 %7)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_yv12_realloc_with_new_border_c(%struct.yv12_buffer_config* %ybf, i32 %new_border, i32 %byte_alignment, i32 %num_planes) #0 {
entry:
  %retval = alloca i32, align 4
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %new_border.addr = alloca i32, align 4
  %byte_alignment.addr = alloca i32, align 4
  %num_planes.addr = alloca i32, align 4
  %new_buf = alloca %struct.yv12_buffer_config, align 4
  %error = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  store i32 %new_border, i32* %new_border.addr, align 4, !tbaa !6
  store i32 %byte_alignment, i32* %byte_alignment.addr, align 4, !tbaa !6
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.yv12_buffer_config* %0, null
  br i1 %tobool, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %new_border.addr, align 4, !tbaa !6
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %border = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 12
  %3 = load i32, i32* %border, align 4, !tbaa !11
  %cmp = icmp eq i32 %1, %3
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  %4 = bitcast %struct.yv12_buffer_config* %new_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 148, i8* %4) #4
  %5 = bitcast %struct.yv12_buffer_config* %new_buf to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %5, i8 0, i32 148, i1 false)
  %6 = bitcast i32* %error to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 2
  %9 = bitcast %union.anon.2* %8 to %struct.anon.3*
  %y_crop_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %9, i32 0, i32 0
  %10 = load i32, i32* %y_crop_width, align 4, !tbaa !12
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %12 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 3
  %13 = bitcast %union.anon.4* %12 to %struct.anon.5*
  %y_crop_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %13, i32 0, i32 0
  %14 = load i32, i32* %y_crop_height, align 4, !tbaa !12
  %15 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %15, i32 0, i32 14
  %16 = load i32, i32* %subsampling_x, align 4, !tbaa !15
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 15
  %18 = load i32, i32* %subsampling_y, align 4, !tbaa !16
  %19 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %19, i32 0, i32 26
  %20 = load i32, i32* %flags, align 4, !tbaa !8
  %and = and i32 %20, 8
  %21 = load i32, i32* %new_border.addr, align 4, !tbaa !6
  %22 = load i32, i32* %byte_alignment.addr, align 4, !tbaa !6
  %call = call i32 @aom_alloc_frame_buffer(%struct.yv12_buffer_config* %new_buf, i32 %10, i32 %14, i32 %16, i32 %18, i32 %and, i32 %21, i32 %22)
  store i32 %call, i32* %error, align 4, !tbaa !6
  %23 = load i32, i32* %error, align 4, !tbaa !6
  %tobool2 = icmp ne i32 %23, 0
  br i1 %tobool2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %24 = load i32, i32* %error, align 4, !tbaa !6
  store i32 %24, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %if.end
  %25 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %26 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  call void @aom_yv12_copy_frame_c(%struct.yv12_buffer_config* %25, %struct.yv12_buffer_config* %new_buf, i32 %26)
  %27 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  call void @aom_extend_frame_borders_c(%struct.yv12_buffer_config* %new_buf, i32 %27)
  %28 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %call5 = call i32 @aom_free_frame_buffer(%struct.yv12_buffer_config* %28)
  %29 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %30 = bitcast %struct.yv12_buffer_config* %29 to i8*
  %31 = bitcast %struct.yv12_buffer_config* %new_buf to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 148, i1 false)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end4, %if.then3
  %32 = bitcast i32* %error to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #4
  %33 = bitcast %struct.yv12_buffer_config* %new_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 148, i8* %33) #4
  br label %return

if.end7:                                          ; preds = %entry
  store i32 -2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end7, %cleanup, %if.then1
  %34 = load i32, i32* %retval, align 4
  ret i32 %34
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare i32 @aom_alloc_frame_buffer(%struct.yv12_buffer_config*, i32, i32, i32, i32, i32, i32, i32) #3

declare i32 @aom_free_frame_buffer(%struct.yv12_buffer_config*) #3

declare i8* @aom_memset16(i8*, i32, i32) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 140}
!9 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !10, i64 80, !7, i64 84, !10, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!10 = !{!"long", !4, i64 0}
!11 = !{!9, !7, i64 84}
!12 = !{!4, !4, i64 0}
!13 = !{!14, !14, i64 0}
!14 = !{!"short", !4, i64 0}
!15 = !{!9, !7, i64 92}
!16 = !{!9, !7, i64 96}
