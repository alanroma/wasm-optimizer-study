; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/cfl.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/cfl.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_mv = type { i32 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }

@tx_size_wide = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 4, i32 8, i32 8, i32 16, i32 16, i32 32, i32 32, i32 64, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64], align 16
@tx_size_high = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 8, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64, i32 32, i32 16, i32 4, i32 32, i32 8, i32 64, i32 16], align 16
@cfl_get_subtract_average_fn_c.sub_avg = internal constant [19 x void (i16*, i16*)*] [void (i16*, i16*)* @cfl_subtract_average_4x4_c, void (i16*, i16*)* @cfl_subtract_average_8x8_c, void (i16*, i16*)* @cfl_subtract_average_16x16_c, void (i16*, i16*)* @cfl_subtract_average_32x32_c, void (i16*, i16*)* null, void (i16*, i16*)* @cfl_subtract_average_4x8_c, void (i16*, i16*)* @cfl_subtract_average_8x4_c, void (i16*, i16*)* @cfl_subtract_average_8x16_c, void (i16*, i16*)* @cfl_subtract_average_16x8_c, void (i16*, i16*)* @cfl_subtract_average_16x32_c, void (i16*, i16*)* @cfl_subtract_average_32x16_c, void (i16*, i16*)* null, void (i16*, i16*)* null, void (i16*, i16*)* @cfl_subtract_average_4x16_c, void (i16*, i16*)* @cfl_subtract_average_16x4_c, void (i16*, i16*)* @cfl_subtract_average_8x32_c, void (i16*, i16*)* @cfl_subtract_average_32x8_c, void (i16*, i16*)* null, void (i16*, i16*)* null], align 16
@cfl_get_predict_lbd_fn_c.pred = internal constant [19 x void (i16*, i8*, i32, i32)*] [void (i16*, i8*, i32, i32)* @cfl_predict_lbd_4x4_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_8x8_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_16x16_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_32x32_c, void (i16*, i8*, i32, i32)* null, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_4x8_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_8x4_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_8x16_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_16x8_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_16x32_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_32x16_c, void (i16*, i8*, i32, i32)* null, void (i16*, i8*, i32, i32)* null, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_4x16_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_16x4_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_8x32_c, void (i16*, i8*, i32, i32)* @cfl_predict_lbd_32x8_c, void (i16*, i8*, i32, i32)* null, void (i16*, i8*, i32, i32)* null], align 16
@cfl_get_predict_hbd_fn_c.pred = internal constant [19 x void (i16*, i16*, i32, i32, i32)*] [void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_4x4_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_8x8_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_16x16_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_32x32_c, void (i16*, i16*, i32, i32, i32)* null, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_4x8_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_8x4_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_8x16_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_16x8_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_16x32_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_32x16_c, void (i16*, i16*, i32, i32, i32)* null, void (i16*, i16*, i32, i32, i32)* null, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_4x16_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_16x4_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_8x32_c, void (i16*, i16*, i32, i32, i32)* @cfl_predict_hbd_32x8_c, void (i16*, i16*, i32, i32, i32)* null, void (i16*, i16*, i32, i32, i32)* null], align 16
@cfl_get_luma_subsampling_420_lbd_c.subfn_420 = internal constant [19 x void (i8*, i32, i16*)*] [void (i8*, i32, i16*)* @cfl_subsample_lbd_420_4x4_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_8x8_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_16x16_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_32x32_c, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_4x8_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_8x4_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_8x16_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_16x8_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_16x32_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_32x16_c, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_4x16_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_16x4_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_8x32_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_420_32x8_c, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* null], align 16
@cfl_get_luma_subsampling_422_lbd_c.subfn_422 = internal constant [19 x void (i8*, i32, i16*)*] [void (i8*, i32, i16*)* @cfl_subsample_lbd_422_4x4_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_8x8_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_16x16_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_32x32_c, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_4x8_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_8x4_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_8x16_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_16x8_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_16x32_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_32x16_c, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_4x16_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_16x4_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_8x32_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_422_32x8_c, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* null], align 16
@cfl_get_luma_subsampling_444_lbd_c.subfn_444 = internal constant [19 x void (i8*, i32, i16*)*] [void (i8*, i32, i16*)* @cfl_subsample_lbd_444_4x4_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_8x8_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_16x16_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_32x32_c, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_4x8_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_8x4_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_8x16_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_16x8_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_16x32_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_32x16_c, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_4x16_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_16x4_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_8x32_c, void (i8*, i32, i16*)* @cfl_subsample_lbd_444_32x8_c, void (i8*, i32, i16*)* null, void (i8*, i32, i16*)* null], align 16
@cfl_get_luma_subsampling_420_hbd_c.subfn_420 = internal constant [19 x void (i16*, i32, i16*)*] [void (i16*, i32, i16*)* @cfl_subsample_hbd_420_4x4_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_8x8_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_16x16_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_32x32_c, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_4x8_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_8x4_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_8x16_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_16x8_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_16x32_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_32x16_c, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_4x16_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_16x4_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_8x32_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_420_32x8_c, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* null], align 16
@cfl_get_luma_subsampling_422_hbd_c.subfn_422 = internal constant [19 x void (i16*, i32, i16*)*] [void (i16*, i32, i16*)* @cfl_subsample_hbd_422_4x4_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_8x8_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_16x16_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_32x32_c, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_4x8_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_8x4_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_8x16_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_16x8_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_16x32_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_32x16_c, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_4x16_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_16x4_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_8x32_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_422_32x8_c, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* null], align 16
@cfl_get_luma_subsampling_444_hbd_c.subfn_444 = internal constant [19 x void (i16*, i32, i16*)*] [void (i16*, i32, i16*)* @cfl_subsample_hbd_444_4x4_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_8x8_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_16x16_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_32x32_c, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_4x8_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_8x4_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_8x16_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_16x8_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_16x32_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_32x16_c, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_4x16_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_16x4_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_8x32_c, void (i16*, i32, i16*)* @cfl_subsample_hbd_444_32x8_c, void (i16*, i32, i16*)* null, void (i16*, i32, i16*)* null], align 16
@block_size_high = internal constant [22 x i8] c"\04\08\04\08\10\08\10 \10 @ @\80@\80\10\04 \08@\10", align 16
@block_size_wide = internal constant [22 x i8] c"\04\04\08\08\08\10\10\10   @@@\80\80\04\10\08 \10@", align 16
@tx_size_wide_log2 = internal constant [19 x i32] [i32 2, i32 3, i32 4, i32 5, i32 6, i32 2, i32 3, i32 3, i32 4, i32 4, i32 5, i32 5, i32 6, i32 2, i32 4, i32 3, i32 5, i32 4, i32 6], align 16
@tx_size_high_log2 = internal constant [19 x i32] [i32 2, i32 3, i32 4, i32 5, i32 6, i32 3, i32 2, i32 4, i32 3, i32 5, i32 4, i32 6, i32 5, i32 4, i32 2, i32 5, i32 3, i32 6, i32 4], align 16

; Function Attrs: nounwind
define hidden void @cfl_init(%struct.cfl_ctx* %cfl, %struct.SequenceHeader* %seq_params) #0 {
entry:
  %cfl.addr = alloca %struct.cfl_ctx*, align 4
  %seq_params.addr = alloca %struct.SequenceHeader*, align 4
  store %struct.cfl_ctx* %cfl, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  store %struct.SequenceHeader* %seq_params, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %0 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %recon_buf_q3 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %0, i32 0, i32 0
  %1 = bitcast [1024 x i16]* %recon_buf_q3 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 2048, i1 false)
  %2 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %ac_buf_q3 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %2, i32 0, i32 1
  %3 = bitcast [1024 x i16]* %ac_buf_q3 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 2048, i1 false)
  %4 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %4, i32 0, i32 32
  %5 = load i32, i32* %subsampling_x, align 8, !tbaa !6
  %6 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %subsampling_x1 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %6, i32 0, i32 8
  store i32 %5, i32* %subsampling_x1, align 4, !tbaa !12
  %7 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %7, i32 0, i32 33
  %8 = load i32, i32* %subsampling_y, align 4, !tbaa !14
  %9 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %subsampling_y2 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %9, i32 0, i32 9
  store i32 %8, i32* %subsampling_y2, align 4, !tbaa !15
  %10 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %are_parameters_computed = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %10, i32 0, i32 7
  store i32 0, i32* %are_parameters_computed, align 4, !tbaa !16
  %11 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %store_y = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %11, i32 0, i32 10
  store i32 0, i32* %store_y, align 4, !tbaa !17
  %12 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %use_dc_pred_cache = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %12, i32 0, i32 3
  store i32 0, i32* %use_dc_pred_cache, align 4, !tbaa !18
  %13 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %dc_pred_is_cached = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %13, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %dc_pred_is_cached, i32 0, i32 0
  store i32 0, i32* %arrayidx, align 4, !tbaa !19
  %14 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %dc_pred_is_cached3 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %14, i32 0, i32 2
  %arrayidx4 = getelementptr inbounds [2 x i32], [2 x i32]* %dc_pred_is_cached3, i32 0, i32 1
  store i32 0, i32* %arrayidx4, align 4, !tbaa !19
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden void @cfl_store_dc_pred(%struct.macroblockd* %xd, i8* %input, i8 zeroext %pred_plane, i32 %width) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %input.addr = alloca i8*, align 4
  %pred_plane.addr = alloca i8, align 1
  %width.addr = alloca i32, align 4
  %input_16 = alloca i16*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8* %input, i8** %input.addr, align 4, !tbaa !2
  store i8 %pred_plane, i8* %pred_plane.addr, align 1, !tbaa !20
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @is_cur_buf_hbd(%struct.macroblockd* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i16** %input_16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %3 = ptrtoint i8* %2 to i32
  %shl = shl i32 %3, 1
  %4 = inttoptr i32 %shl to i16*
  store i16* %4, i16** %input_16, align 4, !tbaa !2
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cfl = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 56
  %dc_pred_cache = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %cfl, i32 0, i32 4
  %6 = load i8, i8* %pred_plane.addr, align 1, !tbaa !20
  %idxprom = zext i8 %6 to i32
  %arrayidx = getelementptr inbounds [2 x [32 x i16]], [2 x [32 x i16]]* %dc_pred_cache, i32 0, i32 %idxprom
  %arraydecay = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx, i32 0, i32 0
  %7 = bitcast i16* %arraydecay to i8*
  %8 = load i16*, i16** %input_16, align 4, !tbaa !2
  %9 = bitcast i16* %8 to i8*
  %10 = load i32, i32* %width.addr, align 4, !tbaa !19
  %shl1 = shl i32 %10, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 2 %9, i32 %shl1, i1 false)
  %11 = bitcast i16** %input_16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  br label %return

if.end:                                           ; preds = %entry
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cfl2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %12, i32 0, i32 56
  %dc_pred_cache3 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %cfl2, i32 0, i32 4
  %13 = load i8, i8* %pred_plane.addr, align 1, !tbaa !20
  %idxprom4 = zext i8 %13 to i32
  %arrayidx5 = getelementptr inbounds [2 x [32 x i16]], [2 x [32 x i16]]* %dc_pred_cache3, i32 0, i32 %idxprom4
  %arraydecay6 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx5, i32 0, i32 0
  %14 = bitcast i16* %arraydecay6 to i8*
  %15 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %16 = load i32, i32* %width.addr, align 4, !tbaa !19
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 1 %15, i32 %16, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_cur_buf_hbd(%struct.macroblockd* %xd) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cur_buf = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 22
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %cur_buf, align 4, !tbaa !21
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 26
  %2 = load i32, i32* %flags, align 4, !tbaa !26
  %and = and i32 %2, 8
  %tobool = icmp ne i32 %and, 0
  %3 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  ret i32 %cond
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: nounwind
define hidden void @cfl_load_dc_pred(%struct.macroblockd* %xd, i8* %dst, i32 %dst_stride, i8 zeroext %tx_size, i8 zeroext %pred_plane) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %pred_plane.addr = alloca i8, align 1
  %width = alloca i32, align 4
  %height = alloca i32, align 4
  %dst_16 = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  store i8 %pred_plane, i8* %pred_plane.addr, align 1, !tbaa !20
  %0 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %2 = load i32, i32* %arrayidx, align 4, !tbaa !19
  store i32 %2, i32* %width, align 4, !tbaa !19
  %3 = bitcast i32* %height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom1
  %5 = load i32, i32* %arrayidx2, align 4, !tbaa !19
  store i32 %5, i32* %height, align 4, !tbaa !19
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @is_cur_buf_hbd(%struct.macroblockd* %6)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = bitcast i16** %dst_16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %9 = ptrtoint i8* %8 to i32
  %shl = shl i32 %9, 1
  %10 = inttoptr i32 %shl to i16*
  store i16* %10, i16** %dst_16, align 4, !tbaa !2
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cfl = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %11, i32 0, i32 56
  %dc_pred_cache = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %cfl, i32 0, i32 4
  %12 = load i8, i8* %pred_plane.addr, align 1, !tbaa !20
  %idxprom3 = zext i8 %12 to i32
  %arrayidx4 = getelementptr inbounds [2 x [32 x i16]], [2 x [32 x i16]]* %dc_pred_cache, i32 0, i32 %idxprom3
  %arraydecay = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx4, i32 0, i32 0
  %13 = load i16*, i16** %dst_16, align 4, !tbaa !2
  %14 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %15 = load i32, i32* %width, align 4, !tbaa !19
  %16 = load i32, i32* %height, align 4, !tbaa !19
  call void @cfl_load_dc_pred_hbd(i16* %arraydecay, i16* %13, i32 %14, i32 %15, i32 %16)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i16** %dst_16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  br label %cleanup

if.end:                                           ; preds = %entry
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cfl5 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %18, i32 0, i32 56
  %dc_pred_cache6 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %cfl5, i32 0, i32 4
  %19 = load i8, i8* %pred_plane.addr, align 1, !tbaa !20
  %idxprom7 = zext i8 %19 to i32
  %arrayidx8 = getelementptr inbounds [2 x [32 x i16]], [2 x [32 x i16]]* %dc_pred_cache6, i32 0, i32 %idxprom7
  %arraydecay9 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx8, i32 0, i32 0
  %20 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %21 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %22 = load i32, i32* %width, align 4, !tbaa !19
  %23 = load i32, i32* %height, align 4, !tbaa !19
  call void @cfl_load_dc_pred_lbd(i16* %arraydecay9, i8* %20, i32 %21, i32 %22, i32 %23)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %24 = bitcast i32* %height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @cfl_load_dc_pred_hbd(i16* %dc_pred_cache, i16* %dst, i32 %dst_stride, i32 %width, i32 %height) #0 {
entry:
  %dc_pred_cache.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %num_bytes = alloca i32, align 4
  %j = alloca i32, align 4
  store i16* %dc_pred_cache, i16** %dc_pred_cache.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %num_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %width.addr, align 4, !tbaa !19
  %shl = shl i32 %1, 1
  store i32 %shl, i32* %num_bytes, align 4, !tbaa !29
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %j, align 4, !tbaa !19
  %4 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %7 = bitcast i16* %6 to i8*
  %8 = load i16*, i16** %dc_pred_cache.addr, align 4, !tbaa !2
  %9 = bitcast i16* %8 to i8*
  %10 = load i32, i32* %num_bytes, align 4, !tbaa !29
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %7, i8* align 2 %9, i32 %10, i1 false)
  %11 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %12 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %12, i32 %11
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %j, align 4, !tbaa !19
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = bitcast i32* %num_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  ret void
}

; Function Attrs: nounwind
define internal void @cfl_load_dc_pred_lbd(i16* %dc_pred_cache, i8* %dst, i32 %dst_stride, i32 %width, i32 %height) #0 {
entry:
  %dc_pred_cache.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  store i16* %dc_pred_cache, i16** %dc_pred_cache.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !19
  %2 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %5 = load i16*, i16** %dc_pred_cache.addr, align 4, !tbaa !2
  %6 = bitcast i16* %5 to i8*
  %7 = load i32, i32* %width.addr, align 4, !tbaa !19
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %4, i8* align 2 %6, i32 %7, i1 false)
  %8 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %9 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %8
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %j, align 4, !tbaa !19
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_4x4_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 4, i32 4, i32 8, i32 4)
  ret void
}

; Function Attrs: nounwind
define internal void @subtract_average_c(i16* %src, i16* %dst, i32 %width, i32 %height, i32 %round_offset, i32 %num_pel_log2) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %round_offset.addr = alloca i32, align 4
  %num_pel_log2.addr = alloca i32, align 4
  %sum = alloca i32, align 4
  %recon = alloca i16*, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %avg = alloca i32, align 4
  %j8 = alloca i32, align 4
  %i14 = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  store i32 %round_offset, i32* %round_offset.addr, align 4, !tbaa !19
  store i32 %num_pel_log2, i32* %num_pel_log2.addr, align 4, !tbaa !19
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %round_offset.addr, align 4, !tbaa !19
  store i32 %1, i32* %sum, align 4, !tbaa !19
  %2 = bitcast i16** %recon to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i16*, i16** %src.addr, align 4, !tbaa !2
  store i16* %3, i16** %recon, align 4, !tbaa !2
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc5, %entry
  %5 = load i32, i32* %j, align 4, !tbaa !19
  %6 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end7

for.body:                                         ; preds = %for.cond
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %10 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %9, %10
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %12 = load i16*, i16** %recon, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx, align 2, !tbaa !30
  %conv = zext i16 %14 to i32
  %15 = load i32, i32* %sum, align 4, !tbaa !19
  %add = add nsw i32 %15, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !19
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %16 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %17 = load i16*, i16** %recon, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %17, i32 32
  store i16* %add.ptr, i16** %recon, align 4, !tbaa !2
  br label %for.inc5

for.inc5:                                         ; preds = %for.end
  %18 = load i32, i32* %j, align 4, !tbaa !19
  %inc6 = add nsw i32 %18, 1
  store i32 %inc6, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end7:                                         ; preds = %for.cond.cleanup
  %19 = bitcast i32* %avg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load i32, i32* %sum, align 4, !tbaa !19
  %21 = load i32, i32* %num_pel_log2.addr, align 4, !tbaa !19
  %shr = ashr i32 %20, %21
  store i32 %shr, i32* %avg, align 4, !tbaa !19
  %22 = bitcast i32* %j8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  store i32 0, i32* %j8, align 4, !tbaa !19
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc29, %for.end7
  %23 = load i32, i32* %j8, align 4, !tbaa !19
  %24 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp10 = icmp slt i32 %23, %24
  br i1 %cmp10, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond9
  store i32 8, i32* %cleanup.dest.slot, align 4
  %25 = bitcast i32* %j8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  br label %for.end31

for.body13:                                       ; preds = %for.cond9
  %26 = bitcast i32* %i14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  store i32 0, i32* %i14, align 4, !tbaa !19
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc24, %for.body13
  %27 = load i32, i32* %i14, align 4, !tbaa !19
  %28 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp16 = icmp slt i32 %27, %28
  br i1 %cmp16, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond15
  store i32 11, i32* %cleanup.dest.slot, align 4
  %29 = bitcast i32* %i14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  br label %for.end26

for.body19:                                       ; preds = %for.cond15
  %30 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %31 = load i32, i32* %i14, align 4, !tbaa !19
  %arrayidx20 = getelementptr inbounds i16, i16* %30, i32 %31
  %32 = load i16, i16* %arrayidx20, align 2, !tbaa !30
  %conv21 = zext i16 %32 to i32
  %33 = load i32, i32* %avg, align 4, !tbaa !19
  %sub = sub nsw i32 %conv21, %33
  %conv22 = trunc i32 %sub to i16
  %34 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %35 = load i32, i32* %i14, align 4, !tbaa !19
  %arrayidx23 = getelementptr inbounds i16, i16* %34, i32 %35
  store i16 %conv22, i16* %arrayidx23, align 2, !tbaa !30
  br label %for.inc24

for.inc24:                                        ; preds = %for.body19
  %36 = load i32, i32* %i14, align 4, !tbaa !19
  %inc25 = add nsw i32 %36, 1
  store i32 %inc25, i32* %i14, align 4, !tbaa !19
  br label %for.cond15

for.end26:                                        ; preds = %for.cond.cleanup18
  %37 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %add.ptr27 = getelementptr inbounds i16, i16* %37, i32 32
  store i16* %add.ptr27, i16** %src.addr, align 4, !tbaa !2
  %38 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds i16, i16* %38, i32 32
  store i16* %add.ptr28, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc29

for.inc29:                                        ; preds = %for.end26
  %39 = load i32, i32* %j8, align 4, !tbaa !19
  %inc30 = add nsw i32 %39, 1
  store i32 %inc30, i32* %j8, align 4, !tbaa !19
  br label %for.cond9

for.end31:                                        ; preds = %for.cond.cleanup12
  %40 = bitcast i32* %avg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i16** %recon to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_4x8_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 4, i32 8, i32 16, i32 5)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_4x16_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 4, i32 16, i32 32, i32 6)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_8x4_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 8, i32 4, i32 16, i32 5)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_8x8_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 8, i32 8, i32 32, i32 6)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_8x16_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 8, i32 16, i32 64, i32 7)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_8x32_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 8, i32 32, i32 128, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_16x4_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 16, i32 4, i32 32, i32 6)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_16x8_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 16, i32 8, i32 64, i32 7)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_16x16_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 16, i32 16, i32 128, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_16x32_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 16, i32 32, i32 256, i32 9)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_32x8_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 32, i32 8, i32 128, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_32x16_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 32, i32 16, i32 256, i32 9)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subtract_average_32x32_c(i16* %src, i16* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  call void @subtract_average_c(i16* %0, i16* %1, i32 32, i32 32, i32 512, i32 10)
  ret void
}

; Function Attrs: nounwind
define hidden void (i16*, i16*)* @cfl_get_subtract_average_fn_c(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %conv = zext i8 %0 to i32
  %rem = srem i32 %conv, 19
  %arrayidx = getelementptr inbounds [19 x void (i16*, i16*)*], [19 x void (i16*, i16*)*]* @cfl_get_subtract_average_fn_c.sub_avg, i32 0, i32 %rem
  %1 = load void (i16*, i16*)*, void (i16*, i16*)** %arrayidx, align 4, !tbaa !2
  ret void (i16*, i16*)* %1
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_4x4_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 4, i32 4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @cfl_predict_lbd_c(i16* %ac_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %width, i32 %height) #2 {
entry:
  %ac_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %ac_buf_q3, i16** %ac_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc9, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !19
  %2 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end11

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %9 = load i16*, i16** %ac_buf_q3.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds i16, i16* %9, i32 %10
  %11 = load i16, i16* %arrayidx, align 2, !tbaa !30
  %call = call i32 @get_scaled_luma_q0(i32 %8, i16 signext %11)
  %12 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx5 = getelementptr inbounds i8, i8* %12, i32 %13
  %14 = load i8, i8* %arrayidx5, align 1, !tbaa !20
  %conv = zext i8 %14 to i32
  %add = add nsw i32 %call, %conv
  %call6 = call zeroext i8 @clip_pixel(i32 %add)
  %15 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx7 = getelementptr inbounds i8, i8* %15, i32 %16
  store i8 %call6, i8* %arrayidx7, align 1, !tbaa !20
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %17 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %18 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %19 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %19, i32 %18
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  %20 = load i16*, i16** %ac_buf_q3.addr, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds i16, i16* %20, i32 32
  store i16* %add.ptr8, i16** %ac_buf_q3.addr, align 4, !tbaa !2
  br label %for.inc9

for.inc9:                                         ; preds = %for.end
  %21 = load i32, i32* %j, align 4, !tbaa !19
  %inc10 = add nsw i32 %21, 1
  store i32 %inc10, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end11:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_4x8_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 4, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_4x16_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 4, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_8x4_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 8, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_8x8_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 8, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_8x16_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 8, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_8x32_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 8, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_16x4_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 16, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_16x8_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 16, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_16x16_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 16, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_16x32_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 16, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_32x8_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 32, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_32x16_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 32, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_lbd_32x32_c(i16* %pred_buf_q3, i8* %dst, i32 %dst_stride, i32 %alpha_q3) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  call void @cfl_predict_lbd_c(i16* %0, i8* %1, i32 %2, i32 %3, i32 32, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void (i16*, i8*, i32, i32)* @cfl_get_predict_lbd_fn_c(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %conv = zext i8 %0 to i32
  %rem = srem i32 %conv, 19
  %arrayidx = getelementptr inbounds [19 x void (i16*, i8*, i32, i32)*], [19 x void (i16*, i8*, i32, i32)*]* @cfl_get_predict_lbd_fn_c.pred, i32 0, i32 %rem
  %1 = load void (i16*, i8*, i32, i32)*, void (i16*, i8*, i32, i32)** %arrayidx, align 4, !tbaa !2
  ret void (i16*, i8*, i32, i32)* %1
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_c(i16* %ac_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bit_depth, i32 %width, i32 %height) #0 {
entry:
  %ac_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %ac_buf_q3, i16** %ac_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !19
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc9, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !19
  %2 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end11

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %9 = load i16*, i16** %ac_buf_q3.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds i16, i16* %9, i32 %10
  %11 = load i16, i16* %arrayidx, align 2, !tbaa !30
  %call = call i32 @get_scaled_luma_q0(i32 %8, i16 signext %11)
  %12 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx5 = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx5, align 2, !tbaa !30
  %conv = zext i16 %14 to i32
  %add = add nsw i32 %call, %conv
  %15 = load i32, i32* %bit_depth.addr, align 4, !tbaa !19
  %call6 = call zeroext i16 @clip_pixel_highbd(i32 %add, i32 %15)
  %16 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %17 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx7 = getelementptr inbounds i16, i16* %16, i32 %17
  store i16 %call6, i16* %arrayidx7, align 2, !tbaa !30
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %18 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %19 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %20 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %20, i32 %19
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  %21 = load i16*, i16** %ac_buf_q3.addr, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds i16, i16* %21, i32 32
  store i16* %add.ptr8, i16** %ac_buf_q3.addr, align 4, !tbaa !2
  br label %for.inc9

for.inc9:                                         ; preds = %for.end
  %22 = load i32, i32* %j, align 4, !tbaa !19
  %inc10 = add nsw i32 %22, 1
  store i32 %inc10, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end11:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @clip_pixel_highbd(i32 %val, i32 %bd) #2 {
entry:
  %retval = alloca i16, align 2
  %val.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !19
  switch i32 %0, label %sw.default [
    i32 8, label %sw.bb
    i32 10, label %sw.bb1
    i32 12, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb
  %1 = load i32, i32* %val.addr, align 4, !tbaa !19
  %call = call i32 @clamp(i32 %1, i32 0, i32 255)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %retval, align 2
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !19
  %call2 = call i32 @clamp(i32 %2, i32 0, i32 1023)
  %conv3 = trunc i32 %call2 to i16
  store i16 %conv3, i16* %retval, align 2
  br label %return

sw.bb4:                                           ; preds = %entry
  %3 = load i32, i32* %val.addr, align 4, !tbaa !19
  %call5 = call i32 @clamp(i32 %3, i32 0, i32 4095)
  %conv6 = trunc i32 %call5 to i16
  store i16 %conv6, i16* %retval, align 2
  br label %return

return:                                           ; preds = %sw.bb4, %sw.bb1, %sw.default
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_scaled_luma_q0(i32 %alpha_q3, i16 signext %pred_buf_q3) #2 {
entry:
  %alpha_q3.addr = alloca i32, align 4
  %pred_buf_q3.addr = alloca i16, align 2
  %scaled_luma_q6 = alloca i32, align 4
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i16 %pred_buf_q3, i16* %pred_buf_q3.addr, align 2, !tbaa !30
  %0 = bitcast i32* %scaled_luma_q6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %2 = load i16, i16* %pred_buf_q3.addr, align 2, !tbaa !30
  %conv = sext i16 %2 to i32
  %mul = mul nsw i32 %1, %conv
  store i32 %mul, i32* %scaled_luma_q6, align 4, !tbaa !19
  %3 = load i32, i32* %scaled_luma_q6, align 4, !tbaa !19
  %cmp = icmp slt i32 %3, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32, i32* %scaled_luma_q6, align 4, !tbaa !19
  %sub = sub nsw i32 0, %4
  %add = add nsw i32 %sub, 32
  %shr = ashr i32 %add, 6
  %sub2 = sub nsw i32 0, %shr
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32, i32* %scaled_luma_q6, align 4, !tbaa !19
  %add3 = add nsw i32 %5, 32
  %shr4 = ashr i32 %add3, 6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub2, %cond.true ], [ %shr4, %cond.false ]
  %6 = bitcast i32* %scaled_luma_q6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  ret i32 %cond
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_4x4_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 4, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_4x8_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 4, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_4x16_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 4, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_8x4_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 8, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_8x8_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 8, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_8x16_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 8, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_8x32_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 8, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_16x4_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 16, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_16x8_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 16, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_16x16_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 16, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_16x32_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 16, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_32x8_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 32, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_32x16_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 32, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_predict_hbd_32x32_c(i16* %pred_buf_q3, i16* %dst, i32 %dst_stride, i32 %alpha_q3, i32 %bd) #0 {
entry:
  %pred_buf_q3.addr = alloca i16*, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %alpha_q3.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i16* %pred_buf_q3, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i32 %alpha_q3, i32* %alpha_q3.addr, align 4, !tbaa !19
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !19
  %0 = load i16*, i16** %pred_buf_q3.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %3 = load i32, i32* %alpha_q3.addr, align 4, !tbaa !19
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !19
  call void @cfl_predict_hbd_c(i16* %0, i16* %1, i32 %2, i32 %3, i32 %4, i32 32, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void (i16*, i16*, i32, i32, i32)* @cfl_get_predict_hbd_fn_c(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %conv = zext i8 %0 to i32
  %rem = srem i32 %conv, 19
  %arrayidx = getelementptr inbounds [19 x void (i16*, i16*, i32, i32, i32)*], [19 x void (i16*, i16*, i32, i32, i32)*]* @cfl_get_predict_hbd_fn_c.pred, i32 0, i32 %rem
  %1 = load void (i16*, i16*, i32, i32, i32)*, void (i16*, i16*, i32, i32, i32)** %arrayidx, align 4, !tbaa !2
  ret void (i16*, i16*, i32, i32, i32)* %1
}

; Function Attrs: nounwind
define hidden void @cfl_predict_block(%struct.macroblockd* %xd, i8* %dst, i32 %dst_stride, i8 zeroext %tx_size, i32 %plane) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %cfl = alloca %struct.cfl_ctx*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %alpha_q3 = alloca i32, align 4
  %dst_16 = alloca i16*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !19
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !19
  %0 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cfl1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 56
  store %struct.cfl_ctx* %cfl1, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %2 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 6
  %4 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %4, i32 0
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %5, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %6 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %are_parameters_computed = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %6, i32 0, i32 7
  %7 = load i32, i32* %are_parameters_computed, align 4, !tbaa !16
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %9 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  call void @cfl_compute_parameters(%struct.macroblockd* %8, i8 zeroext %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = bitcast i32* %alpha_q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %cfl_alpha_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %11, i32 0, i32 22
  %12 = load i8, i8* %cfl_alpha_idx, align 1, !tbaa !33
  %13 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %cfl_alpha_signs = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %13, i32 0, i32 21
  %14 = load i8, i8* %cfl_alpha_signs, align 2, !tbaa !39
  %15 = load i32, i32* %plane.addr, align 4, !tbaa !19
  %sub = sub nsw i32 %15, 1
  %conv = trunc i32 %sub to i8
  %call = call i32 @cfl_idx_to_alpha(i8 zeroext %12, i8 signext %14, i8 zeroext %conv)
  store i32 %call, i32* %alpha_q3, align 4, !tbaa !19
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call2 = call i32 @is_cur_buf_hbd(%struct.macroblockd* %16)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %17 = bitcast i16** %dst_16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %19 = ptrtoint i8* %18 to i32
  %shl = shl i32 %19, 1
  %20 = inttoptr i32 %shl to i16*
  store i16* %20, i16** %dst_16, align 4, !tbaa !2
  %21 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call5 = call void (i16*, i16*, i32, i32, i32)* @cfl_get_predict_hbd_fn_c(i8 zeroext %21)
  %22 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %ac_buf_q3 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %22, i32 0, i32 1
  %arraydecay = getelementptr inbounds [1024 x i16], [1024 x i16]* %ac_buf_q3, i32 0, i32 0
  %23 = load i16*, i16** %dst_16, align 4, !tbaa !2
  %24 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %25 = load i32, i32* %alpha_q3, align 4, !tbaa !19
  %26 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %26, i32 0, i32 41
  %27 = load i32, i32* %bd, align 4, !tbaa !40
  call void %call5(i16* %arraydecay, i16* %23, i32 %24, i32 %25, i32 %27)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %28 = bitcast i16** %dst_16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  br label %cleanup

if.end6:                                          ; preds = %if.end
  %29 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call7 = call void (i16*, i8*, i32, i32)* @cfl_get_predict_lbd_fn_c(i8 zeroext %29)
  %30 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %ac_buf_q38 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %30, i32 0, i32 1
  %arraydecay9 = getelementptr inbounds [1024 x i16], [1024 x i16]* %ac_buf_q38, i32 0, i32 0
  %31 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %32 = load i32, i32* %dst_stride.addr, align 4, !tbaa !19
  %33 = load i32, i32* %alpha_q3, align 4, !tbaa !19
  call void %call7(i16* %arraydecay9, i8* %31, i32 %32, i32 %33)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end6, %if.then4
  %34 = bitcast i32* %alpha_q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  %35 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  %36 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @cfl_compute_parameters(%struct.macroblockd* %xd, i8 zeroext %tx_size) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %tx_size.addr = alloca i8, align 1
  %cfl = alloca %struct.cfl_ctx*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cfl1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 56
  store %struct.cfl_ctx* %cfl1, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %2 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %3 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !19
  %5 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom2 = zext i8 %5 to i32
  %arrayidx3 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom2
  %6 = load i32, i32* %arrayidx3, align 4, !tbaa !19
  call void @cfl_pad(%struct.cfl_ctx* %2, i32 %4, i32 %6)
  %7 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call = call void (i16*, i16*)* @cfl_get_subtract_average_fn_c(i8 zeroext %7)
  %8 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %recon_buf_q3 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %8, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1024 x i16], [1024 x i16]* %recon_buf_q3, i32 0, i32 0
  %9 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %ac_buf_q3 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %9, i32 0, i32 1
  %arraydecay4 = getelementptr inbounds [1024 x i16], [1024 x i16]* %ac_buf_q3, i32 0, i32 0
  call void %call(i16* %arraydecay, i16* %arraydecay4)
  %10 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %are_parameters_computed = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %10, i32 0, i32 7
  store i32 1, i32* %are_parameters_computed, align 4, !tbaa !16
  %11 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @cfl_idx_to_alpha(i8 zeroext %alpha_idx, i8 signext %joint_sign, i8 zeroext %pred_type) #2 {
entry:
  %retval = alloca i32, align 4
  %alpha_idx.addr = alloca i8, align 1
  %joint_sign.addr = alloca i8, align 1
  %pred_type.addr = alloca i8, align 1
  %alpha_sign = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %abs_alpha_q3 = alloca i32, align 4
  store i8 %alpha_idx, i8* %alpha_idx.addr, align 1, !tbaa !20
  store i8 %joint_sign, i8* %joint_sign.addr, align 1, !tbaa !20
  store i8 %pred_type, i8* %pred_type.addr, align 1, !tbaa !20
  %0 = bitcast i32* %alpha_sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8, i8* %pred_type.addr, align 1, !tbaa !20
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i8, i8* %joint_sign.addr, align 1, !tbaa !20
  %conv2 = sext i8 %2 to i32
  %add = add nsw i32 %conv2, 1
  %mul = mul nsw i32 %add, 11
  %shr = ashr i32 %mul, 5
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i8, i8* %joint_sign.addr, align 1, !tbaa !20
  %conv3 = sext i8 %3 to i32
  %add4 = add nsw i32 %conv3, 1
  %4 = load i8, i8* %joint_sign.addr, align 1, !tbaa !20
  %conv5 = sext i8 %4 to i32
  %add6 = add nsw i32 %conv5, 1
  %mul7 = mul nsw i32 %add6, 11
  %shr8 = ashr i32 %mul7, 5
  %mul9 = mul nsw i32 3, %shr8
  %sub = sub nsw i32 %add4, %mul9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shr, %cond.true ], [ %sub, %cond.false ]
  store i32 %cond, i32* %alpha_sign, align 4, !tbaa !19
  %5 = load i32, i32* %alpha_sign, align 4, !tbaa !19
  %cmp10 = icmp eq i32 %5, 0
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %cond.end
  %6 = bitcast i32* %abs_alpha_q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i8, i8* %pred_type.addr, align 1, !tbaa !20
  %conv12 = zext i8 %7 to i32
  %cmp13 = icmp eq i32 %conv12, 0
  br i1 %cmp13, label %cond.true15, label %cond.false18

cond.true15:                                      ; preds = %if.end
  %8 = load i8, i8* %alpha_idx.addr, align 1, !tbaa !20
  %conv16 = zext i8 %8 to i32
  %shr17 = ashr i32 %conv16, 4
  br label %cond.end20

cond.false18:                                     ; preds = %if.end
  %9 = load i8, i8* %alpha_idx.addr, align 1, !tbaa !20
  %conv19 = zext i8 %9 to i32
  %and = and i32 %conv19, 15
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false18, %cond.true15
  %cond21 = phi i32 [ %shr17, %cond.true15 ], [ %and, %cond.false18 ]
  store i32 %cond21, i32* %abs_alpha_q3, align 4, !tbaa !19
  %10 = load i32, i32* %alpha_sign, align 4, !tbaa !19
  %cmp22 = icmp eq i32 %10, 2
  br i1 %cmp22, label %cond.true24, label %cond.false26

cond.true24:                                      ; preds = %cond.end20
  %11 = load i32, i32* %abs_alpha_q3, align 4, !tbaa !19
  %add25 = add nsw i32 %11, 1
  br label %cond.end29

cond.false26:                                     ; preds = %cond.end20
  %12 = load i32, i32* %abs_alpha_q3, align 4, !tbaa !19
  %sub27 = sub nsw i32 0, %12
  %sub28 = sub nsw i32 %sub27, 1
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false26, %cond.true24
  %cond30 = phi i32 [ %add25, %cond.true24 ], [ %sub28, %cond.false26 ]
  store i32 %cond30, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %abs_alpha_q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  br label %cleanup

cleanup:                                          ; preds = %cond.end29, %if.then
  %14 = bitcast i32* %alpha_sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_4x4_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 4, i32 4)
  ret void
}

; Function Attrs: nounwind
define internal void @cfl_luma_subsampling_420_lbd_c(i8* %input, i32 %input_stride, i16* %output_q3, i32 %width, i32 %height) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %bot = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !19
  %2 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end23

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = bitcast i32* %bot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %10 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %add = add nsw i32 %9, %10
  store i32 %add, i32* %bot, align 4, !tbaa !19
  %11 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load i8, i8* %arrayidx, align 1, !tbaa !20
  %conv = zext i8 %13 to i32
  %14 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %15 = load i32, i32* %i, align 4, !tbaa !19
  %add5 = add nsw i32 %15, 1
  %arrayidx6 = getelementptr inbounds i8, i8* %14, i32 %add5
  %16 = load i8, i8* %arrayidx6, align 1, !tbaa !20
  %conv7 = zext i8 %16 to i32
  %add8 = add nsw i32 %conv, %conv7
  %17 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %18 = load i32, i32* %bot, align 4, !tbaa !19
  %arrayidx9 = getelementptr inbounds i8, i8* %17, i32 %18
  %19 = load i8, i8* %arrayidx9, align 1, !tbaa !20
  %conv10 = zext i8 %19 to i32
  %add11 = add nsw i32 %add8, %conv10
  %20 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %21 = load i32, i32* %bot, align 4, !tbaa !19
  %add12 = add nsw i32 %21, 1
  %arrayidx13 = getelementptr inbounds i8, i8* %20, i32 %add12
  %22 = load i8, i8* %arrayidx13, align 1, !tbaa !20
  %conv14 = zext i8 %22 to i32
  %add15 = add nsw i32 %add11, %conv14
  %shl = shl i32 %add15, 1
  %conv16 = trunc i32 %shl to i16
  %23 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !19
  %shr = ashr i32 %24, 1
  %arrayidx17 = getelementptr inbounds i16, i16* %23, i32 %shr
  store i16 %conv16, i16* %arrayidx17, align 2, !tbaa !30
  %25 = bitcast i32* %bot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %26 = load i32, i32* %i, align 4, !tbaa !19
  %add18 = add nsw i32 %26, 2
  store i32 %add18, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %27 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %shl19 = shl i32 %27, 1
  %28 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %28, i32 %shl19
  store i8* %add.ptr, i8** %input.addr, align 4, !tbaa !2
  %29 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %add.ptr20 = getelementptr inbounds i16, i16* %29, i32 32
  store i16* %add.ptr20, i16** %output_q3.addr, align 4, !tbaa !2
  br label %for.inc21

for.inc21:                                        ; preds = %for.end
  %30 = load i32, i32* %j, align 4, !tbaa !19
  %add22 = add nsw i32 %30, 2
  store i32 %add22, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end23:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_8x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_16x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_32x32_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 32, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_4x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 4, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_8x4_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_8x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_16x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_16x32_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_32x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 32, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_4x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 4, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_16x4_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_8x32_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_420_32x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_lbd_c(i8* %0, i32 %1, i16* %2, i32 32, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void (i8*, i32, i16*)* @cfl_get_luma_subsampling_420_lbd_c(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x void (i8*, i32, i16*)*], [19 x void (i8*, i32, i16*)*]* @cfl_get_luma_subsampling_420_lbd_c.subfn_420, i32 0, i32 %idxprom
  %1 = load void (i8*, i32, i16*)*, void (i8*, i32, i16*)** %arrayidx, align 4, !tbaa !2
  ret void (i8*, i32, i16*)* %1
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_4x4_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 4, i32 4)
  ret void
}

; Function Attrs: nounwind
define internal void @cfl_luma_subsampling_422_lbd_c(i8* %input, i32 %input_stride, i16* %output_q3, i32 %width, i32 %height) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !19
  %2 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end13

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !20
  %conv = zext i8 %10 to i32
  %11 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !19
  %add = add nsw i32 %12, 1
  %arrayidx5 = getelementptr inbounds i8, i8* %11, i32 %add
  %13 = load i8, i8* %arrayidx5, align 1, !tbaa !20
  %conv6 = zext i8 %13 to i32
  %add7 = add nsw i32 %conv, %conv6
  %shl = shl i32 %add7, 2
  %conv8 = trunc i32 %shl to i16
  %14 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %15 = load i32, i32* %i, align 4, !tbaa !19
  %shr = ashr i32 %15, 1
  %arrayidx9 = getelementptr inbounds i16, i16* %14, i32 %shr
  store i16 %conv8, i16* %arrayidx9, align 2, !tbaa !30
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %16 = load i32, i32* %i, align 4, !tbaa !19
  %add10 = add nsw i32 %16, 2
  store i32 %add10, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %17 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %18 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %18, i32 %17
  store i8* %add.ptr, i8** %input.addr, align 4, !tbaa !2
  %19 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i16, i16* %19, i32 32
  store i16* %add.ptr11, i16** %output_q3.addr, align 4, !tbaa !2
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %20 = load i32, i32* %j, align 4, !tbaa !19
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end13:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_8x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_16x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_32x32_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 32, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_4x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 4, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_8x4_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_8x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_16x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_16x32_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_32x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 32, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_4x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 4, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_16x4_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_8x32_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_422_32x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_lbd_c(i8* %0, i32 %1, i16* %2, i32 32, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void (i8*, i32, i16*)* @cfl_get_luma_subsampling_422_lbd_c(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x void (i8*, i32, i16*)*], [19 x void (i8*, i32, i16*)*]* @cfl_get_luma_subsampling_422_lbd_c.subfn_422, i32 0, i32 %idxprom
  %1 = load void (i8*, i32, i16*)*, void (i8*, i32, i16*)** %arrayidx, align 4, !tbaa !2
  ret void (i8*, i32, i16*)* %1
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_4x4_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 4, i32 4)
  ret void
}

; Function Attrs: nounwind
define internal void @cfl_luma_subsampling_444_lbd_c(i8* %input, i32 %input_stride, i16* %output_q3, i32 %width, i32 %height) #0 {
entry:
  %input.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc8, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !19
  %2 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end10

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !20
  %conv = zext i8 %10 to i32
  %shl = shl i32 %conv, 3
  %conv5 = trunc i32 %shl to i16
  %11 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx6 = getelementptr inbounds i16, i16* %11, i32 %12
  store i16 %conv5, i16* %arrayidx6, align 2, !tbaa !30
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %13 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %14 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %15 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %15, i32 %14
  store i8* %add.ptr, i8** %input.addr, align 4, !tbaa !2
  %16 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %add.ptr7 = getelementptr inbounds i16, i16* %16, i32 32
  store i16* %add.ptr7, i16** %output_q3.addr, align 4, !tbaa !2
  br label %for.inc8

for.inc8:                                         ; preds = %for.end
  %17 = load i32, i32* %j, align 4, !tbaa !19
  %inc9 = add nsw i32 %17, 1
  store i32 %inc9, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end10:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_8x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_16x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_32x32_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 32, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_4x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 4, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_8x4_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_8x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_16x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_16x32_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_32x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 32, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_4x16_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 4, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_16x4_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 16, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_8x32_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 8, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_lbd_444_32x8_c(i8* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i8* %cfl_type, i8** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_lbd_c(i8* %0, i32 %1, i16* %2, i32 32, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void (i8*, i32, i16*)* @cfl_get_luma_subsampling_444_lbd_c(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x void (i8*, i32, i16*)*], [19 x void (i8*, i32, i16*)*]* @cfl_get_luma_subsampling_444_lbd_c.subfn_444, i32 0, i32 %idxprom
  %1 = load void (i8*, i32, i16*)*, void (i8*, i32, i16*)** %arrayidx, align 4, !tbaa !2
  ret void (i8*, i32, i16*)* %1
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_4x4_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 4, i32 4)
  ret void
}

; Function Attrs: nounwind
define internal void @cfl_luma_subsampling_420_hbd_c(i16* %input, i32 %input_stride, i16* %output_q3, i32 %width, i32 %height) #0 {
entry:
  %input.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %bot = alloca i32, align 4
  store i16* %input, i16** %input.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !19
  %2 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end23

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = bitcast i32* %bot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %10 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %add = add nsw i32 %9, %10
  store i32 %add, i32* %bot, align 4, !tbaa !19
  %11 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds i16, i16* %11, i32 %12
  %13 = load i16, i16* %arrayidx, align 2, !tbaa !30
  %conv = zext i16 %13 to i32
  %14 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %15 = load i32, i32* %i, align 4, !tbaa !19
  %add5 = add nsw i32 %15, 1
  %arrayidx6 = getelementptr inbounds i16, i16* %14, i32 %add5
  %16 = load i16, i16* %arrayidx6, align 2, !tbaa !30
  %conv7 = zext i16 %16 to i32
  %add8 = add nsw i32 %conv, %conv7
  %17 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %18 = load i32, i32* %bot, align 4, !tbaa !19
  %arrayidx9 = getelementptr inbounds i16, i16* %17, i32 %18
  %19 = load i16, i16* %arrayidx9, align 2, !tbaa !30
  %conv10 = zext i16 %19 to i32
  %add11 = add nsw i32 %add8, %conv10
  %20 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %21 = load i32, i32* %bot, align 4, !tbaa !19
  %add12 = add nsw i32 %21, 1
  %arrayidx13 = getelementptr inbounds i16, i16* %20, i32 %add12
  %22 = load i16, i16* %arrayidx13, align 2, !tbaa !30
  %conv14 = zext i16 %22 to i32
  %add15 = add nsw i32 %add11, %conv14
  %shl = shl i32 %add15, 1
  %conv16 = trunc i32 %shl to i16
  %23 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !19
  %shr = ashr i32 %24, 1
  %arrayidx17 = getelementptr inbounds i16, i16* %23, i32 %shr
  store i16 %conv16, i16* %arrayidx17, align 2, !tbaa !30
  %25 = bitcast i32* %bot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %26 = load i32, i32* %i, align 4, !tbaa !19
  %add18 = add nsw i32 %26, 2
  store i32 %add18, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %27 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %shl19 = shl i32 %27, 1
  %28 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %28, i32 %shl19
  store i16* %add.ptr, i16** %input.addr, align 4, !tbaa !2
  %29 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %add.ptr20 = getelementptr inbounds i16, i16* %29, i32 32
  store i16* %add.ptr20, i16** %output_q3.addr, align 4, !tbaa !2
  br label %for.inc21

for.inc21:                                        ; preds = %for.end
  %30 = load i32, i32* %j, align 4, !tbaa !19
  %add22 = add nsw i32 %30, 2
  store i32 %add22, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end23:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_8x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_16x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_32x32_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 32, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_4x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 4, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_8x4_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_8x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_16x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_16x32_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_32x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 32, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_4x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 4, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_16x4_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_8x32_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_420_32x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_420_hbd_c(i16* %0, i32 %1, i16* %2, i32 32, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void (i16*, i32, i16*)* @cfl_get_luma_subsampling_420_hbd_c(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x void (i16*, i32, i16*)*], [19 x void (i16*, i32, i16*)*]* @cfl_get_luma_subsampling_420_hbd_c.subfn_420, i32 0, i32 %idxprom
  %1 = load void (i16*, i32, i16*)*, void (i16*, i32, i16*)** %arrayidx, align 4, !tbaa !2
  ret void (i16*, i32, i16*)* %1
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_4x4_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 4, i32 4)
  ret void
}

; Function Attrs: nounwind
define internal void @cfl_luma_subsampling_422_hbd_c(i16* %input, i32 %input_stride, i16* %output_q3, i32 %width, i32 %height) #0 {
entry:
  %input.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %input, i16** %input.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !19
  %2 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end13

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds i16, i16* %8, i32 %9
  %10 = load i16, i16* %arrayidx, align 2, !tbaa !30
  %conv = zext i16 %10 to i32
  %11 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !19
  %add = add nsw i32 %12, 1
  %arrayidx5 = getelementptr inbounds i16, i16* %11, i32 %add
  %13 = load i16, i16* %arrayidx5, align 2, !tbaa !30
  %conv6 = zext i16 %13 to i32
  %add7 = add nsw i32 %conv, %conv6
  %shl = shl i32 %add7, 2
  %conv8 = trunc i32 %shl to i16
  %14 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %15 = load i32, i32* %i, align 4, !tbaa !19
  %shr = ashr i32 %15, 1
  %arrayidx9 = getelementptr inbounds i16, i16* %14, i32 %shr
  store i16 %conv8, i16* %arrayidx9, align 2, !tbaa !30
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %16 = load i32, i32* %i, align 4, !tbaa !19
  %add10 = add nsw i32 %16, 2
  store i32 %add10, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %17 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %18 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %18, i32 %17
  store i16* %add.ptr, i16** %input.addr, align 4, !tbaa !2
  %19 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i16, i16* %19, i32 32
  store i16* %add.ptr11, i16** %output_q3.addr, align 4, !tbaa !2
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %20 = load i32, i32* %j, align 4, !tbaa !19
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end13:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_8x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_16x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_32x32_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 32, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_4x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 4, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_8x4_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_8x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_16x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_16x32_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_32x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 32, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_4x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 4, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_16x4_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_8x32_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_422_32x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_422_hbd_c(i16* %0, i32 %1, i16* %2, i32 32, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void (i16*, i32, i16*)* @cfl_get_luma_subsampling_422_hbd_c(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x void (i16*, i32, i16*)*], [19 x void (i16*, i32, i16*)*]* @cfl_get_luma_subsampling_422_hbd_c.subfn_422, i32 0, i32 %idxprom
  %1 = load void (i16*, i32, i16*)*, void (i16*, i32, i16*)** %arrayidx, align 4, !tbaa !2
  ret void (i16*, i32, i16*)* %1
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_4x4_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 4, i32 4)
  ret void
}

; Function Attrs: nounwind
define internal void @cfl_luma_subsampling_444_hbd_c(i16* %input, i32 %input_stride, i16* %output_q3, i32 %width, i32 %height) #0 {
entry:
  %input.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %input, i16** %input.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc8, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !19
  %2 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end10

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds i16, i16* %8, i32 %9
  %10 = load i16, i16* %arrayidx, align 2, !tbaa !30
  %conv = zext i16 %10 to i32
  %shl = shl i32 %conv, 3
  %conv5 = trunc i32 %shl to i16
  %11 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx6 = getelementptr inbounds i16, i16* %11, i32 %12
  store i16 %conv5, i16* %arrayidx6, align 2, !tbaa !30
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %13 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %14 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %15 = load i16*, i16** %input.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %15, i32 %14
  store i16* %add.ptr, i16** %input.addr, align 4, !tbaa !2
  %16 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  %add.ptr7 = getelementptr inbounds i16, i16* %16, i32 32
  store i16* %add.ptr7, i16** %output_q3.addr, align 4, !tbaa !2
  br label %for.inc8

for.inc8:                                         ; preds = %for.end
  %17 = load i32, i32* %j, align 4, !tbaa !19
  %inc9 = add nsw i32 %17, 1
  store i32 %inc9, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end10:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_8x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_16x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_32x32_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 32, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_4x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 4, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_8x4_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_8x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_16x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_16x32_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_32x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 32, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_4x16_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 4, i32 16)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_16x4_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 16, i32 4)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_8x32_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 8, i32 32)
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_subsample_hbd_444_32x8_c(i16* %cfl_type, i32 %input_stride, i16* %output_q3) #0 {
entry:
  %cfl_type.addr = alloca i16*, align 4
  %input_stride.addr = alloca i32, align 4
  %output_q3.addr = alloca i16*, align 4
  store i16* %cfl_type, i16** %cfl_type.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i16* %output_q3, i16** %output_q3.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %cfl_type.addr, align 4, !tbaa !2
  %1 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %2 = load i16*, i16** %output_q3.addr, align 4, !tbaa !2
  call void @cfl_luma_subsampling_444_hbd_c(i16* %0, i32 %1, i16* %2, i32 32, i32 8)
  ret void
}

; Function Attrs: nounwind
define hidden void (i16*, i32, i16*)* @cfl_get_luma_subsampling_444_hbd_c(i8 zeroext %tx_size) #0 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x void (i16*, i32, i16*)*], [19 x void (i16*, i32, i16*)*]* @cfl_get_luma_subsampling_444_hbd_c.subfn_444, i32 0, i32 %idxprom
  %1 = load void (i16*, i32, i16*)*, void (i16*, i32, i16*)** %arrayidx, align 4, !tbaa !2
  ret void (i16*, i32, i16*)* %1
}

; Function Attrs: nounwind
define hidden void @cfl_store_tx(%struct.macroblockd* %xd, i32 %row, i32 %col, i8 zeroext %tx_size, i8 zeroext %bsize) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %bsize.addr = alloca i8, align 1
  %cfl = alloca %struct.cfl_ctx*, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %dst = alloca i8*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !19
  store i32 %col, i32* %col.addr, align 4, !tbaa !19
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !20
  %0 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cfl1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 56
  store %struct.cfl_ctx* %cfl1, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %2 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 4
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 0
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %4 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst2 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %5, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst2, i32 0, i32 0
  %6 = load i8*, i8** %buf, align 4, !tbaa !41
  %7 = load i32, i32* %row.addr, align 4, !tbaa !19
  %8 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst3 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %8, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst3, i32 0, i32 4
  %9 = load i32, i32* %stride, align 4, !tbaa !44
  %mul = mul nsw i32 %7, %9
  %10 = load i32, i32* %col.addr, align 4, !tbaa !19
  %add = add nsw i32 %mul, %10
  %shl = shl i32 %add, 2
  %arrayidx4 = getelementptr inbounds i8, i8* %6, i32 %shl
  store i8* %arrayidx4, i8** %dst, align 4, !tbaa !2
  %11 = load i8, i8* %bsize.addr, align 1, !tbaa !20
  %idxprom = zext i8 %11 to i32
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom
  %12 = load i8, i8* %arrayidx5, align 1, !tbaa !20
  %conv = zext i8 %12 to i32
  %cmp = icmp eq i32 %conv, 4
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %13 = load i8, i8* %bsize.addr, align 1, !tbaa !20
  %idxprom7 = zext i8 %13 to i32
  %arrayidx8 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom7
  %14 = load i8, i8* %arrayidx8, align 1, !tbaa !20
  %conv9 = zext i8 %14 to i32
  %cmp10 = icmp eq i32 %conv9, 4
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %15 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %16, i32 0, i32 0
  %17 = load i32, i32* %mi_row, align 16, !tbaa !45
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %18, i32 0, i32 1
  %19 = load i32, i32* %mi_col, align 4, !tbaa !46
  call void @sub8x8_adjust_offset(%struct.cfl_ctx* %15, i32 %17, i32 %19, i32* %row.addr, i32* %col.addr)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %20 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %21 = load i8*, i8** %dst, align 4, !tbaa !2
  %22 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst12 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %22, i32 0, i32 6
  %stride13 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst12, i32 0, i32 4
  %23 = load i32, i32* %stride13, align 4, !tbaa !44
  %24 = load i32, i32* %row.addr, align 4, !tbaa !19
  %25 = load i32, i32* %col.addr, align 4, !tbaa !19
  %26 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %27 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @is_cur_buf_hbd(%struct.macroblockd* %27)
  call void @cfl_store(%struct.cfl_ctx* %20, i8* %21, i32 %23, i32 %24, i32 %25, i8 zeroext %26, i32 %call)
  %28 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  %29 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  %30 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @sub8x8_adjust_offset(%struct.cfl_ctx* %cfl, i32 %mi_row, i32 %mi_col, i32* %row_out, i32* %col_out) #2 {
entry:
  %cfl.addr = alloca %struct.cfl_ctx*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %row_out.addr = alloca i32*, align 4
  %col_out.addr = alloca i32*, align 4
  store %struct.cfl_ctx* %cfl, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !19
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !19
  store i32* %row_out, i32** %row_out.addr, align 4, !tbaa !2
  store i32* %col_out, i32** %col_out.addr, align 4, !tbaa !2
  %0 = load i32, i32* %mi_row.addr, align 4, !tbaa !19
  %and = and i32 %0, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %1, i32 0, i32 9
  %2 = load i32, i32* %subsampling_y, align 4, !tbaa !15
  %tobool1 = icmp ne i32 %2, 0
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load i32*, i32** %row_out.addr, align 4, !tbaa !2
  %4 = load i32, i32* %3, align 4, !tbaa !19
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %3, align 4, !tbaa !19
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %5 = load i32, i32* %mi_col.addr, align 4, !tbaa !19
  %and2 = and i32 %5, 1
  %tobool3 = icmp ne i32 %and2, 0
  br i1 %tobool3, label %land.lhs.true4, label %if.end8

land.lhs.true4:                                   ; preds = %if.end
  %6 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %6, i32 0, i32 8
  %7 = load i32, i32* %subsampling_x, align 4, !tbaa !12
  %tobool5 = icmp ne i32 %7, 0
  br i1 %tobool5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %land.lhs.true4
  %8 = load i32*, i32** %col_out.addr, align 4, !tbaa !2
  %9 = load i32, i32* %8, align 4, !tbaa !19
  %inc7 = add nsw i32 %9, 1
  store i32 %inc7, i32* %8, align 4, !tbaa !19
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %land.lhs.true4, %if.end
  ret void
}

; Function Attrs: nounwind
define internal void @cfl_store(%struct.cfl_ctx* %cfl, i8* %input, i32 %input_stride, i32 %row, i32 %col, i8 zeroext %tx_size, i32 %use_hbd) #0 {
entry:
  %cfl.addr = alloca %struct.cfl_ctx*, align 4
  %input.addr = alloca i8*, align 4
  %input_stride.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %use_hbd.addr = alloca i32, align 4
  %width = alloca i32, align 4
  %height = alloca i32, align 4
  %tx_off_log2 = alloca i32, align 4
  %sub_x = alloca i32, align 4
  %sub_y = alloca i32, align 4
  %store_row = alloca i32, align 4
  %store_col = alloca i32, align 4
  %store_height = alloca i32, align 4
  %store_width = alloca i32, align 4
  %recon_buf_q3 = alloca i16*, align 4
  store %struct.cfl_ctx* %cfl, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  store i8* %input, i8** %input.addr, align 4, !tbaa !2
  store i32 %input_stride, i32* %input_stride.addr, align 4, !tbaa !19
  store i32 %row, i32* %row.addr, align 4, !tbaa !19
  store i32 %col, i32* %col.addr, align 4, !tbaa !19
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  store i32 %use_hbd, i32* %use_hbd.addr, align 4, !tbaa !19
  %0 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %2 = load i32, i32* %arrayidx, align 4, !tbaa !19
  store i32 %2, i32* %width, align 4, !tbaa !19
  %3 = bitcast i32* %height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom1
  %5 = load i32, i32* %arrayidx2, align 4, !tbaa !19
  store i32 %5, i32* %height, align 4, !tbaa !19
  %6 = bitcast i32* %tx_off_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  store i32 2, i32* %tx_off_log2, align 4, !tbaa !19
  %7 = bitcast i32* %sub_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %8, i32 0, i32 8
  %9 = load i32, i32* %subsampling_x, align 4, !tbaa !12
  store i32 %9, i32* %sub_x, align 4, !tbaa !19
  %10 = bitcast i32* %sub_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %11, i32 0, i32 9
  %12 = load i32, i32* %subsampling_y, align 4, !tbaa !15
  store i32 %12, i32* %sub_y, align 4, !tbaa !19
  %13 = bitcast i32* %store_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i32, i32* %row.addr, align 4, !tbaa !19
  %15 = load i32, i32* %sub_y, align 4, !tbaa !19
  %sub = sub nsw i32 2, %15
  %shl = shl i32 %14, %sub
  store i32 %shl, i32* %store_row, align 4, !tbaa !19
  %16 = bitcast i32* %store_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %col.addr, align 4, !tbaa !19
  %18 = load i32, i32* %sub_x, align 4, !tbaa !19
  %sub3 = sub nsw i32 2, %18
  %shl4 = shl i32 %17, %sub3
  store i32 %shl4, i32* %store_col, align 4, !tbaa !19
  %19 = bitcast i32* %store_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load i32, i32* %height, align 4, !tbaa !19
  %21 = load i32, i32* %sub_y, align 4, !tbaa !19
  %shr = ashr i32 %20, %21
  store i32 %shr, i32* %store_height, align 4, !tbaa !19
  %22 = bitcast i32* %store_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  %23 = load i32, i32* %width, align 4, !tbaa !19
  %24 = load i32, i32* %sub_x, align 4, !tbaa !19
  %shr5 = ashr i32 %23, %24
  store i32 %shr5, i32* %store_width, align 4, !tbaa !19
  %25 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %are_parameters_computed = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %25, i32 0, i32 7
  store i32 0, i32* %are_parameters_computed, align 4, !tbaa !16
  %26 = load i32, i32* %col.addr, align 4, !tbaa !19
  %cmp = icmp eq i32 %26, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %27 = load i32, i32* %row.addr, align 4, !tbaa !19
  %cmp6 = icmp eq i32 %27, 0
  br i1 %cmp6, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %28 = load i32, i32* %store_width, align 4, !tbaa !19
  %29 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_width = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %29, i32 0, i32 6
  store i32 %28, i32* %buf_width, align 4, !tbaa !47
  %30 = load i32, i32* %store_height, align 4, !tbaa !19
  %31 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_height = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %31, i32 0, i32 5
  store i32 %30, i32* %buf_height, align 4, !tbaa !48
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %entry
  %32 = load i32, i32* %store_col, align 4, !tbaa !19
  %33 = load i32, i32* %store_width, align 4, !tbaa !19
  %add = add nsw i32 %32, %33
  %34 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_width7 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %34, i32 0, i32 6
  %35 = load i32, i32* %buf_width7, align 4, !tbaa !47
  %cmp8 = icmp sgt i32 %add, %35
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %36 = load i32, i32* %store_col, align 4, !tbaa !19
  %37 = load i32, i32* %store_width, align 4, !tbaa !19
  %add9 = add nsw i32 %36, %37
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %38 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_width10 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %38, i32 0, i32 6
  %39 = load i32, i32* %buf_width10, align 4, !tbaa !47
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add9, %cond.true ], [ %39, %cond.false ]
  %40 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_width11 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %40, i32 0, i32 6
  store i32 %cond, i32* %buf_width11, align 4, !tbaa !47
  %41 = load i32, i32* %store_row, align 4, !tbaa !19
  %42 = load i32, i32* %store_height, align 4, !tbaa !19
  %add12 = add nsw i32 %41, %42
  %43 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_height13 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %43, i32 0, i32 5
  %44 = load i32, i32* %buf_height13, align 4, !tbaa !48
  %cmp14 = icmp sgt i32 %add12, %44
  br i1 %cmp14, label %cond.true15, label %cond.false17

cond.true15:                                      ; preds = %cond.end
  %45 = load i32, i32* %store_row, align 4, !tbaa !19
  %46 = load i32, i32* %store_height, align 4, !tbaa !19
  %add16 = add nsw i32 %45, %46
  br label %cond.end19

cond.false17:                                     ; preds = %cond.end
  %47 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_height18 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %47, i32 0, i32 5
  %48 = load i32, i32* %buf_height18, align 4, !tbaa !48
  br label %cond.end19

cond.end19:                                       ; preds = %cond.false17, %cond.true15
  %cond20 = phi i32 [ %add16, %cond.true15 ], [ %48, %cond.false17 ]
  %49 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_height21 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %49, i32 0, i32 5
  store i32 %cond20, i32* %buf_height21, align 4, !tbaa !48
  br label %if.end

if.end:                                           ; preds = %cond.end19, %if.then
  %50 = bitcast i16** %recon_buf_q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #4
  %51 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %recon_buf_q322 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %51, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1024 x i16], [1024 x i16]* %recon_buf_q322, i32 0, i32 0
  %52 = load i32, i32* %store_row, align 4, !tbaa !19
  %mul = mul nsw i32 %52, 32
  %53 = load i32, i32* %store_col, align 4, !tbaa !19
  %add23 = add nsw i32 %mul, %53
  %add.ptr = getelementptr inbounds i16, i16* %arraydecay, i32 %add23
  store i16* %add.ptr, i16** %recon_buf_q3, align 4, !tbaa !2
  %54 = load i32, i32* %use_hbd.addr, align 4, !tbaa !19
  %tobool = icmp ne i32 %54, 0
  br i1 %tobool, label %if.then24, label %if.else26

if.then24:                                        ; preds = %if.end
  %55 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %56 = load i32, i32* %sub_x, align 4, !tbaa !19
  %57 = load i32, i32* %sub_y, align 4, !tbaa !19
  %call = call void (i16*, i32, i16*)* @cfl_subsampling_hbd(i8 zeroext %55, i32 %56, i32 %57)
  %58 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %59 = ptrtoint i8* %58 to i32
  %shl25 = shl i32 %59, 1
  %60 = inttoptr i32 %shl25 to i16*
  %61 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %62 = load i16*, i16** %recon_buf_q3, align 4, !tbaa !2
  call void %call(i16* %60, i32 %61, i16* %62)
  br label %if.end28

if.else26:                                        ; preds = %if.end
  %63 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %64 = load i32, i32* %sub_x, align 4, !tbaa !19
  %65 = load i32, i32* %sub_y, align 4, !tbaa !19
  %call27 = call void (i8*, i32, i16*)* @cfl_subsampling_lbd(i8 zeroext %63, i32 %64, i32 %65)
  %66 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %67 = load i32, i32* %input_stride.addr, align 4, !tbaa !19
  %68 = load i16*, i16** %recon_buf_q3, align 4, !tbaa !2
  call void %call27(i8* %66, i32 %67, i16* %68)
  br label %if.end28

if.end28:                                         ; preds = %if.else26, %if.then24
  %69 = bitcast i16** %recon_buf_q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %70 = bitcast i32* %store_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast i32* %store_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  %72 = bitcast i32* %store_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #4
  %73 = bitcast i32* %store_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  %74 = bitcast i32* %sub_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  %75 = bitcast i32* %sub_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  %76 = bitcast i32* %tx_off_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  %77 = bitcast i32* %height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @cfl_store_block(%struct.macroblockd* %xd, i8 zeroext %bsize, i8 zeroext %tx_size) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bsize.addr = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %cfl = alloca %struct.cfl_ctx*, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %width = alloca i32, align 4
  %height = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !20
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cfl1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 56
  store %struct.cfl_ctx* %cfl1, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %2 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 4
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 0
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %4 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %row, align 4, !tbaa !19
  %5 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %col, align 4, !tbaa !19
  %6 = load i8, i8* %bsize.addr, align 1, !tbaa !20
  %idxprom = zext i8 %6 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom
  %7 = load i8, i8* %arrayidx2, align 1, !tbaa !20
  %conv = zext i8 %7 to i32
  %cmp = icmp eq i32 %conv, 4
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %8 = load i8, i8* %bsize.addr, align 1, !tbaa !20
  %idxprom4 = zext i8 %8 to i32
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom4
  %9 = load i8, i8* %arrayidx5, align 1, !tbaa !20
  %conv6 = zext i8 %9 to i32
  %cmp7 = icmp eq i32 %conv6, 4
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %10 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %11, i32 0, i32 0
  %12 = load i32, i32* %mi_row, align 16, !tbaa !45
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %13, i32 0, i32 1
  %14 = load i32, i32* %mi_col, align 4, !tbaa !46
  call void @sub8x8_adjust_offset(%struct.cfl_ctx* %10, i32 %12, i32 %14, i32* %row, i32* %col)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %15 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %17 = load i8, i8* %bsize.addr, align 1, !tbaa !20
  %18 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call = call i32 @max_intra_block_width(%struct.macroblockd* %16, i8 zeroext %17, i32 0, i8 zeroext %18)
  store i32 %call, i32* %width, align 4, !tbaa !19
  %19 = bitcast i32* %height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %21 = load i8, i8* %bsize.addr, align 1, !tbaa !20
  %22 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call9 = call i32 @max_intra_block_height(%struct.macroblockd* %20, i8 zeroext %21, i32 0, i8 zeroext %22)
  store i32 %call9, i32* %height, align 4, !tbaa !19
  %23 = load i32, i32* %width, align 4, !tbaa !19
  %24 = load i32, i32* %height, align 4, !tbaa !19
  %call10 = call zeroext i8 @get_tx_size(i32 %23, i32 %24)
  store i8 %call10, i8* %tx_size.addr, align 1, !tbaa !20
  %25 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl, align 4, !tbaa !2
  %26 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %26, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 0
  %27 = load i8*, i8** %buf, align 4, !tbaa !41
  %28 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst11 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %28, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst11, i32 0, i32 4
  %29 = load i32, i32* %stride, align 4, !tbaa !44
  %30 = load i32, i32* %row, align 4, !tbaa !19
  %31 = load i32, i32* %col, align 4, !tbaa !19
  %32 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %33 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call12 = call i32 @is_cur_buf_hbd(%struct.macroblockd* %33)
  call void @cfl_store(%struct.cfl_ctx* %25, i8* %27, i32 %29, i32 %30, i32 %31, i8 zeroext %32, i32 %call12)
  %34 = bitcast i32* %height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  %35 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  %36 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %38 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  %39 = bitcast %struct.cfl_ctx** %cfl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @max_intra_block_width(%struct.macroblockd* %xd, i8 zeroext %plane_bsize, i32 %plane, i8 zeroext %tx_size) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane_bsize.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %max_blocks_wide = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %plane_bsize, i8* %plane_bsize.addr, align 1, !tbaa !20
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !19
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = bitcast i32* %max_blocks_wide to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %2 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !20
  %3 = load i32, i32* %plane.addr, align 4, !tbaa !19
  %call = call i32 @max_block_wide(%struct.macroblockd* %1, i8 zeroext %2, i32 %3)
  %shl = shl i32 %call, 2
  store i32 %shl, i32* %max_blocks_wide, align 4, !tbaa !19
  %4 = load i32, i32* %max_blocks_wide, align 4, !tbaa !19
  %5 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_log2, i32 0, i32 %idxprom
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !19
  %shl1 = shl i32 1, %6
  %sub = sub nsw i32 %shl1, 1
  %add = add nsw i32 %4, %sub
  %7 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom2 = zext i8 %7 to i32
  %arrayidx3 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_log2, i32 0, i32 %idxprom2
  %8 = load i32, i32* %arrayidx3, align 4, !tbaa !19
  %shl4 = shl i32 1, %8
  %sub5 = sub nsw i32 %shl4, 1
  %neg = xor i32 %sub5, -1
  %and = and i32 %add, %neg
  %9 = bitcast i32* %max_blocks_wide to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  ret i32 %and
}

; Function Attrs: inlinehint nounwind
define internal i32 @max_intra_block_height(%struct.macroblockd* %xd, i8 zeroext %plane_bsize, i32 %plane, i8 zeroext %tx_size) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane_bsize.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %tx_size.addr = alloca i8, align 1
  %max_blocks_high = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %plane_bsize, i8* %plane_bsize.addr, align 1, !tbaa !20
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !19
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  %0 = bitcast i32* %max_blocks_high to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %2 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !20
  %3 = load i32, i32* %plane.addr, align 4, !tbaa !19
  %call = call i32 @max_block_high(%struct.macroblockd* %1, i8 zeroext %2, i32 %3)
  %shl = shl i32 %call, 2
  store i32 %shl, i32* %max_blocks_high, align 4, !tbaa !19
  %4 = load i32, i32* %max_blocks_high, align 4, !tbaa !19
  %5 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_log2, i32 0, i32 %idxprom
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !19
  %shl1 = shl i32 1, %6
  %sub = sub nsw i32 %shl1, 1
  %add = add nsw i32 %4, %sub
  %7 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %idxprom2 = zext i8 %7 to i32
  %arrayidx3 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_log2, i32 0, i32 %idxprom2
  %8 = load i32, i32* %arrayidx3, align 4, !tbaa !19
  %shl4 = shl i32 1, %8
  %sub5 = sub nsw i32 %shl4, 1
  %neg = xor i32 %sub5, -1
  %and = and i32 %add, %neg
  %9 = bitcast i32* %max_blocks_high to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  ret i32 %and
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_tx_size(i32 %width, i32 %height) #2 {
entry:
  %retval = alloca i8, align 1
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = load i32, i32* %width.addr, align 4, !tbaa !19
  %1 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp = icmp eq i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %width.addr, align 4, !tbaa !19
  %call = call zeroext i8 @get_sqr_tx_size(i32 %2)
  store i8 %call, i8* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %width.addr, align 4, !tbaa !19
  %4 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp1 = icmp slt i32 %3, %4
  br i1 %cmp1, label %if.then2, label %if.else13

if.then2:                                         ; preds = %if.end
  %5 = load i32, i32* %width.addr, align 4, !tbaa !19
  %6 = load i32, i32* %width.addr, align 4, !tbaa !19
  %add = add nsw i32 %5, %6
  %7 = load i32, i32* %height.addr, align 4, !tbaa !19
  %cmp3 = icmp eq i32 %add, %7
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then2
  %8 = load i32, i32* %width.addr, align 4, !tbaa !19
  switch i32 %8, label %sw.epilog [
    i32 4, label %sw.bb
    i32 8, label %sw.bb5
    i32 16, label %sw.bb6
    i32 32, label %sw.bb7
  ]

sw.bb:                                            ; preds = %if.then4
  store i8 5, i8* %retval, align 1
  br label %return

sw.bb5:                                           ; preds = %if.then4
  store i8 7, i8* %retval, align 1
  br label %return

sw.bb6:                                           ; preds = %if.then4
  store i8 9, i8* %retval, align 1
  br label %return

sw.bb7:                                           ; preds = %if.then4
  store i8 11, i8* %retval, align 1
  br label %return

sw.epilog:                                        ; preds = %if.then4
  br label %if.end12

if.else:                                          ; preds = %if.then2
  %9 = load i32, i32* %width.addr, align 4, !tbaa !19
  switch i32 %9, label %sw.epilog11 [
    i32 4, label %sw.bb8
    i32 8, label %sw.bb9
    i32 16, label %sw.bb10
  ]

sw.bb8:                                           ; preds = %if.else
  store i8 13, i8* %retval, align 1
  br label %return

sw.bb9:                                           ; preds = %if.else
  store i8 15, i8* %retval, align 1
  br label %return

sw.bb10:                                          ; preds = %if.else
  store i8 17, i8* %retval, align 1
  br label %return

sw.epilog11:                                      ; preds = %if.else
  br label %if.end12

if.end12:                                         ; preds = %sw.epilog11, %sw.epilog
  br label %if.end28

if.else13:                                        ; preds = %if.end
  %10 = load i32, i32* %height.addr, align 4, !tbaa !19
  %11 = load i32, i32* %height.addr, align 4, !tbaa !19
  %add14 = add nsw i32 %10, %11
  %12 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp15 = icmp eq i32 %add14, %12
  br i1 %cmp15, label %if.then16, label %if.else22

if.then16:                                        ; preds = %if.else13
  %13 = load i32, i32* %height.addr, align 4, !tbaa !19
  switch i32 %13, label %sw.epilog21 [
    i32 4, label %sw.bb17
    i32 8, label %sw.bb18
    i32 16, label %sw.bb19
    i32 32, label %sw.bb20
  ]

sw.bb17:                                          ; preds = %if.then16
  store i8 6, i8* %retval, align 1
  br label %return

sw.bb18:                                          ; preds = %if.then16
  store i8 8, i8* %retval, align 1
  br label %return

sw.bb19:                                          ; preds = %if.then16
  store i8 10, i8* %retval, align 1
  br label %return

sw.bb20:                                          ; preds = %if.then16
  store i8 12, i8* %retval, align 1
  br label %return

sw.epilog21:                                      ; preds = %if.then16
  br label %if.end27

if.else22:                                        ; preds = %if.else13
  %14 = load i32, i32* %height.addr, align 4, !tbaa !19
  switch i32 %14, label %sw.epilog26 [
    i32 4, label %sw.bb23
    i32 8, label %sw.bb24
    i32 16, label %sw.bb25
  ]

sw.bb23:                                          ; preds = %if.else22
  store i8 14, i8* %retval, align 1
  br label %return

sw.bb24:                                          ; preds = %if.else22
  store i8 16, i8* %retval, align 1
  br label %return

sw.bb25:                                          ; preds = %if.else22
  store i8 18, i8* %retval, align 1
  br label %return

sw.epilog26:                                      ; preds = %if.else22
  br label %if.end27

if.end27:                                         ; preds = %sw.epilog26, %sw.epilog21
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.end12
  store i8 0, i8* %retval, align 1
  br label %return

return:                                           ; preds = %if.end28, %sw.bb25, %sw.bb24, %sw.bb23, %sw.bb20, %sw.bb19, %sw.bb18, %sw.bb17, %sw.bb10, %sw.bb9, %sw.bb8, %sw.bb7, %sw.bb6, %sw.bb5, %sw.bb, %if.then
  %15 = load i8, i8* %retval, align 1
  ret i8 %15
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @clip_pixel(i32 %val) #2 {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !19
  %0 = load i32, i32* %val.addr, align 4, !tbaa !19
  %cmp = icmp sgt i32 %0, 255
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4, !tbaa !19
  %cmp1 = icmp slt i32 %1, 0
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %2 = load i32, i32* %val.addr, align 4, !tbaa !19
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ 0, %cond.true2 ], [ %2, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ 255, %cond.true ], [ %cond, %cond.end ]
  %conv = trunc i32 %cond5 to i8
  ret i8 %conv
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !19
  store i32 %low, i32* %low.addr, align 4, !tbaa !19
  store i32 %high, i32* %high.addr, align 4, !tbaa !19
  %0 = load i32, i32* %value.addr, align 4, !tbaa !19
  %1 = load i32, i32* %low.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !19
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !19
  %4 = load i32, i32* %high.addr, align 4, !tbaa !19
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !19
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !19
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal void @cfl_pad(%struct.cfl_ctx* %cfl, i32 %width, i32 %height) #2 {
entry:
  %cfl.addr = alloca %struct.cfl_ctx*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %diff_width = alloca i32, align 4
  %diff_height = alloca i32, align 4
  %min_height = alloca i32, align 4
  %recon_buf_q3 = alloca i16*, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %last_pixel = alloca i16, align 2
  %i = alloca i32, align 4
  %recon_buf_q318 = alloca i16*, align 4
  %j23 = alloca i32, align 4
  %last_row_q3 = alloca i16*, align 4
  %i29 = alloca i32, align 4
  store %struct.cfl_ctx* %cfl, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !19
  store i32 %height, i32* %height.addr, align 4, !tbaa !19
  %0 = bitcast i32* %diff_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %width.addr, align 4, !tbaa !19
  %2 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_width = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %2, i32 0, i32 6
  %3 = load i32, i32* %buf_width, align 4, !tbaa !47
  %sub = sub nsw i32 %1, %3
  store i32 %sub, i32* %diff_width, align 4, !tbaa !19
  %4 = bitcast i32* %diff_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i32, i32* %height.addr, align 4, !tbaa !19
  %6 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_height = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %6, i32 0, i32 5
  %7 = load i32, i32* %buf_height, align 4, !tbaa !48
  %sub1 = sub nsw i32 %5, %7
  store i32 %sub1, i32* %diff_height, align 4, !tbaa !19
  %8 = load i32, i32* %diff_width, align 4, !tbaa !19
  %cmp = icmp sgt i32 %8, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %9 = bitcast i32* %min_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load i32, i32* %height.addr, align 4, !tbaa !19
  %11 = load i32, i32* %diff_height, align 4, !tbaa !19
  %sub2 = sub nsw i32 %10, %11
  store i32 %sub2, i32* %min_height, align 4, !tbaa !19
  %12 = bitcast i16** %recon_buf_q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %recon_buf_q33 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %13, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1024 x i16], [1024 x i16]* %recon_buf_q33, i32 0, i32 0
  %14 = load i32, i32* %width.addr, align 4, !tbaa !19
  %15 = load i32, i32* %diff_width, align 4, !tbaa !19
  %sub4 = sub nsw i32 %14, %15
  %add.ptr = getelementptr inbounds i16, i16* %arraydecay, i32 %sub4
  store i16* %add.ptr, i16** %recon_buf_q3, align 4, !tbaa !2
  %16 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  store i32 0, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %if.then
  %17 = load i32, i32* %j, align 4, !tbaa !19
  %18 = load i32, i32* %min_height, align 4, !tbaa !19
  %cmp5 = icmp slt i32 %17, %18
  br i1 %cmp5, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  br label %for.end14

for.body:                                         ; preds = %for.cond
  %20 = bitcast i16* %last_pixel to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %20) #4
  %21 = load i16*, i16** %recon_buf_q3, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %21, i32 -1
  %22 = load i16, i16* %arrayidx, align 2, !tbaa !30
  store i16 %22, i16* %last_pixel, align 2, !tbaa !30
  %23 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %24 = load i32, i32* %i, align 4, !tbaa !19
  %25 = load i32, i32* %diff_width, align 4, !tbaa !19
  %cmp7 = icmp slt i32 %24, %25
  br i1 %cmp7, label %for.body9, label %for.cond.cleanup8

for.cond.cleanup8:                                ; preds = %for.cond6
  store i32 5, i32* %cleanup.dest.slot, align 4
  %26 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  br label %for.end

for.body9:                                        ; preds = %for.cond6
  %27 = load i16, i16* %last_pixel, align 2, !tbaa !30
  %28 = load i16*, i16** %recon_buf_q3, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx10 = getelementptr inbounds i16, i16* %28, i32 %29
  store i16 %27, i16* %arrayidx10, align 2, !tbaa !30
  br label %for.inc

for.inc:                                          ; preds = %for.body9
  %30 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond6

for.end:                                          ; preds = %for.cond.cleanup8
  %31 = load i16*, i16** %recon_buf_q3, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i16, i16* %31, i32 32
  store i16* %add.ptr11, i16** %recon_buf_q3, align 4, !tbaa !2
  %32 = bitcast i16* %last_pixel to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %32) #4
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %33 = load i32, i32* %j, align 4, !tbaa !19
  %inc13 = add nsw i32 %33, 1
  store i32 %inc13, i32* %j, align 4, !tbaa !19
  br label %for.cond

for.end14:                                        ; preds = %for.cond.cleanup
  %34 = load i32, i32* %width.addr, align 4, !tbaa !19
  %35 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_width15 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %35, i32 0, i32 6
  store i32 %34, i32* %buf_width15, align 4, !tbaa !47
  %36 = bitcast i16** %recon_buf_q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %min_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  br label %if.end

if.end:                                           ; preds = %for.end14, %entry
  %38 = load i32, i32* %diff_height, align 4, !tbaa !19
  %cmp16 = icmp sgt i32 %38, 0
  br i1 %cmp16, label %if.then17, label %if.end44

if.then17:                                        ; preds = %if.end
  %39 = bitcast i16** %recon_buf_q318 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #4
  %40 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %recon_buf_q319 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %40, i32 0, i32 0
  %arraydecay20 = getelementptr inbounds [1024 x i16], [1024 x i16]* %recon_buf_q319, i32 0, i32 0
  %41 = load i32, i32* %height.addr, align 4, !tbaa !19
  %42 = load i32, i32* %diff_height, align 4, !tbaa !19
  %sub21 = sub nsw i32 %41, %42
  %mul = mul nsw i32 %sub21, 32
  %add.ptr22 = getelementptr inbounds i16, i16* %arraydecay20, i32 %mul
  store i16* %add.ptr22, i16** %recon_buf_q318, align 4, !tbaa !2
  %43 = bitcast i32* %j23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #4
  store i32 0, i32* %j23, align 4, !tbaa !19
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc40, %if.then17
  %44 = load i32, i32* %j23, align 4, !tbaa !19
  %45 = load i32, i32* %diff_height, align 4, !tbaa !19
  %cmp25 = icmp slt i32 %44, %45
  br i1 %cmp25, label %for.body27, label %for.cond.cleanup26

for.cond.cleanup26:                               ; preds = %for.cond24
  store i32 8, i32* %cleanup.dest.slot, align 4
  %46 = bitcast i32* %j23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %for.end42

for.body27:                                       ; preds = %for.cond24
  %47 = bitcast i16** %last_row_q3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #4
  %48 = load i16*, i16** %recon_buf_q318, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds i16, i16* %48, i32 -32
  store i16* %add.ptr28, i16** %last_row_q3, align 4, !tbaa !2
  %49 = bitcast i32* %i29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #4
  store i32 0, i32* %i29, align 4, !tbaa !19
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc36, %for.body27
  %50 = load i32, i32* %i29, align 4, !tbaa !19
  %51 = load i32, i32* %width.addr, align 4, !tbaa !19
  %cmp31 = icmp slt i32 %50, %51
  br i1 %cmp31, label %for.body33, label %for.cond.cleanup32

for.cond.cleanup32:                               ; preds = %for.cond30
  store i32 11, i32* %cleanup.dest.slot, align 4
  %52 = bitcast i32* %i29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  br label %for.end38

for.body33:                                       ; preds = %for.cond30
  %53 = load i16*, i16** %last_row_q3, align 4, !tbaa !2
  %54 = load i32, i32* %i29, align 4, !tbaa !19
  %arrayidx34 = getelementptr inbounds i16, i16* %53, i32 %54
  %55 = load i16, i16* %arrayidx34, align 2, !tbaa !30
  %56 = load i16*, i16** %recon_buf_q318, align 4, !tbaa !2
  %57 = load i32, i32* %i29, align 4, !tbaa !19
  %arrayidx35 = getelementptr inbounds i16, i16* %56, i32 %57
  store i16 %55, i16* %arrayidx35, align 2, !tbaa !30
  br label %for.inc36

for.inc36:                                        ; preds = %for.body33
  %58 = load i32, i32* %i29, align 4, !tbaa !19
  %inc37 = add nsw i32 %58, 1
  store i32 %inc37, i32* %i29, align 4, !tbaa !19
  br label %for.cond30

for.end38:                                        ; preds = %for.cond.cleanup32
  %59 = load i16*, i16** %recon_buf_q318, align 4, !tbaa !2
  %add.ptr39 = getelementptr inbounds i16, i16* %59, i32 32
  store i16* %add.ptr39, i16** %recon_buf_q318, align 4, !tbaa !2
  %60 = bitcast i16** %last_row_q3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  br label %for.inc40

for.inc40:                                        ; preds = %for.end38
  %61 = load i32, i32* %j23, align 4, !tbaa !19
  %inc41 = add nsw i32 %61, 1
  store i32 %inc41, i32* %j23, align 4, !tbaa !19
  br label %for.cond24

for.end42:                                        ; preds = %for.cond.cleanup26
  %62 = load i32, i32* %height.addr, align 4, !tbaa !19
  %63 = load %struct.cfl_ctx*, %struct.cfl_ctx** %cfl.addr, align 4, !tbaa !2
  %buf_height43 = getelementptr inbounds %struct.cfl_ctx, %struct.cfl_ctx* %63, i32 0, i32 5
  store i32 %62, i32* %buf_height43, align 4, !tbaa !48
  %64 = bitcast i16** %recon_buf_q318 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #4
  br label %if.end44

if.end44:                                         ; preds = %for.end42, %if.end
  %65 = bitcast i32* %diff_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  %66 = bitcast i32* %diff_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void (i16*, i32, i16*)* @cfl_subsampling_hbd(i8 zeroext %tx_size, i32 %sub_x, i32 %sub_y) #2 {
entry:
  %retval = alloca void (i16*, i32, i16*)*, align 4
  %tx_size.addr = alloca i8, align 1
  %sub_x.addr = alloca i32, align 4
  %sub_y.addr = alloca i32, align 4
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  store i32 %sub_x, i32* %sub_x.addr, align 4, !tbaa !19
  store i32 %sub_y, i32* %sub_y.addr, align 4, !tbaa !19
  %0 = load i32, i32* %sub_x.addr, align 4, !tbaa !19
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %sub_y.addr, align 4, !tbaa !19
  %cmp1 = icmp eq i32 %1, 1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %2 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call = call void (i16*, i32, i16*)* @cfl_get_luma_subsampling_420_hbd_c(i8 zeroext %2)
  store void (i16*, i32, i16*)* %call, void (i16*, i32, i16*)** %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  %3 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call3 = call void (i16*, i32, i16*)* @cfl_get_luma_subsampling_422_hbd_c(i8 zeroext %3)
  store void (i16*, i32, i16*)* %call3, void (i16*, i32, i16*)** %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  %4 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call5 = call void (i16*, i32, i16*)* @cfl_get_luma_subsampling_444_hbd_c(i8 zeroext %4)
  store void (i16*, i32, i16*)* %call5, void (i16*, i32, i16*)** %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load void (i16*, i32, i16*)*, void (i16*, i32, i16*)** %retval, align 4
  ret void (i16*, i32, i16*)* %5
}

; Function Attrs: inlinehint nounwind
define internal void (i8*, i32, i16*)* @cfl_subsampling_lbd(i8 zeroext %tx_size, i32 %sub_x, i32 %sub_y) #2 {
entry:
  %retval = alloca void (i8*, i32, i16*)*, align 4
  %tx_size.addr = alloca i8, align 1
  %sub_x.addr = alloca i32, align 4
  %sub_y.addr = alloca i32, align 4
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !20
  store i32 %sub_x, i32* %sub_x.addr, align 4, !tbaa !19
  store i32 %sub_y, i32* %sub_y.addr, align 4, !tbaa !19
  %0 = load i32, i32* %sub_x.addr, align 4, !tbaa !19
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %sub_y.addr, align 4, !tbaa !19
  %cmp1 = icmp eq i32 %1, 1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %2 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call = call void (i8*, i32, i16*)* @cfl_get_luma_subsampling_420_lbd_c(i8 zeroext %2)
  store void (i8*, i32, i16*)* %call, void (i8*, i32, i16*)** %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  %3 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call3 = call void (i8*, i32, i16*)* @cfl_get_luma_subsampling_422_lbd_c(i8 zeroext %3)
  store void (i8*, i32, i16*)* %call3, void (i8*, i32, i16*)** %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  %4 = load i8, i8* %tx_size.addr, align 1, !tbaa !20
  %call5 = call void (i8*, i32, i16*)* @cfl_get_luma_subsampling_444_lbd_c(i8 zeroext %4)
  store void (i8*, i32, i16*)* %call5, void (i8*, i32, i16*)** %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load void (i8*, i32, i16*)*, void (i8*, i32, i16*)** %retval, align 4
  ret void (i8*, i32, i16*)* %5
}

; Function Attrs: inlinehint nounwind
define internal i32 @max_block_wide(%struct.macroblockd* %xd, i8 zeroext %bsize, i32 %plane) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bsize.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %max_blocks_wide = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !20
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !19
  %0 = bitcast i32* %max_blocks_wide to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8, i8* %bsize.addr, align 1, !tbaa !20
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !20
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %max_blocks_wide, align 4, !tbaa !19
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 18
  %4 = load i32, i32* %mb_to_right_edge, align 8, !tbaa !49
  %cmp = icmp slt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 4
  %7 = load i32, i32* %plane.addr, align 4, !tbaa !19
  %arrayidx3 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane2, i32 0, i32 %7
  store %struct.macroblockd_plane* %arrayidx3, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 18
  %9 = load i32, i32* %mb_to_right_edge4, align 8, !tbaa !49
  %10 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %10, i32 0, i32 4
  %11 = load i32, i32* %subsampling_x, align 4, !tbaa !50
  %add = add nsw i32 3, %11
  %shr = ashr i32 %9, %add
  %12 = load i32, i32* %max_blocks_wide, align 4, !tbaa !19
  %add5 = add nsw i32 %12, %shr
  store i32 %add5, i32* %max_blocks_wide, align 4, !tbaa !19
  %13 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load i32, i32* %max_blocks_wide, align 4, !tbaa !19
  %shr6 = ashr i32 %14, 2
  %15 = bitcast i32* %max_blocks_wide to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret i32 %shr6
}

; Function Attrs: inlinehint nounwind
define internal i32 @max_block_high(%struct.macroblockd* %xd, i8 zeroext %bsize, i32 %plane) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bsize.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %max_blocks_high = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !20
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !19
  %0 = bitcast i32* %max_blocks_high to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8, i8* %bsize.addr, align 1, !tbaa !20
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !20
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %max_blocks_high, align 4, !tbaa !19
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 20
  %4 = load i32, i32* %mb_to_bottom_edge, align 16, !tbaa !51
  %cmp = icmp slt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 4
  %7 = load i32, i32* %plane.addr, align 4, !tbaa !19
  %arrayidx3 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane2, i32 0, i32 %7
  store %struct.macroblockd_plane* %arrayidx3, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 20
  %9 = load i32, i32* %mb_to_bottom_edge4, align 16, !tbaa !51
  %10 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %10, i32 0, i32 5
  %11 = load i32, i32* %subsampling_y, align 4, !tbaa !52
  %add = add nsw i32 3, %11
  %shr = ashr i32 %9, %add
  %12 = load i32, i32* %max_blocks_high, align 4, !tbaa !19
  %add5 = add nsw i32 %12, %shr
  store i32 %add5, i32* %max_blocks_high, align 4, !tbaa !19
  %13 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load i32, i32* %max_blocks_high, align 4, !tbaa !19
  %shr6 = ashr i32 %14, 2
  %15 = bitcast i32* %max_blocks_high to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret i32 %shr6
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_sqr_tx_size(i32 %tx_dim) #2 {
entry:
  %retval = alloca i8, align 1
  %tx_dim.addr = alloca i32, align 4
  store i32 %tx_dim, i32* %tx_dim.addr, align 4, !tbaa !19
  %0 = load i32, i32* %tx_dim.addr, align 4, !tbaa !19
  switch i32 %0, label %sw.default [
    i32 128, label %sw.bb
    i32 64, label %sw.bb
    i32 32, label %sw.bb1
    i32 16, label %sw.bb2
    i32 8, label %sw.bb3
  ]

sw.bb:                                            ; preds = %entry, %entry
  store i8 4, i8* %retval, align 1
  br label %return

sw.bb1:                                           ; preds = %entry
  store i8 3, i8* %retval, align 1
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8 2, i8* %retval, align 1
  br label %return

sw.bb3:                                           ; preds = %entry
  store i8 1, i8* %retval, align 1
  br label %return

sw.default:                                       ; preds = %entry
  store i8 0, i8* %retval, align 1
  br label %return

return:                                           ; preds = %sw.default, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i8, i8* %retval, align 1
  ret i8 %1
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 96}
!7 = !{!"SequenceHeader", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !4, i64 16, !8, i64 20, !8, i64 24, !4, i64 28, !8, i64 32, !8, i64 36, !9, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !8, i64 112, !4, i64 116, !8, i64 244, !10, i64 248, !4, i64 264, !11, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!8 = !{!"int", !4, i64 0}
!9 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!10 = !{!"aom_timing", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!11 = !{!"aom_dec_model_info", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!12 = !{!13, !8, i64 4248}
!13 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !8, i64 4104, !4, i64 4108, !8, i64 4236, !8, i64 4240, !8, i64 4244, !8, i64 4248, !8, i64 4252, !8, i64 4256}
!14 = !{!7, !8, i64 100}
!15 = !{!13, !8, i64 4252}
!16 = !{!13, !8, i64 4244}
!17 = !{!13, !8, i64 4256}
!18 = !{!13, !8, i64 4104}
!19 = !{!8, !8, i64 0}
!20 = !{!4, !4, i64 0}
!21 = !{!22, !3, i64 4140}
!22 = !{!"macroblockd", !8, i64 0, !8, i64 4, !8, i64 8, !23, i64 12, !4, i64 16, !24, i64 4060, !3, i64 4084, !23, i64 4088, !23, i64 4089, !23, i64 4090, !23, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !8, i64 4112, !8, i64 4116, !8, i64 4120, !8, i64 4124, !8, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !8, i64 6836, !4, i64 6840, !4, i64 6872, !8, i64 6904, !8, i64 6908, !3, i64 6912, !3, i64 6916, !8, i64 6920, !8, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !13, i64 39720, !25, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!23 = !{!"_Bool", !4, i64 0}
!24 = !{!"TileInfo", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20}
!25 = !{!"dist_wtd_comp_params", !8, i64 0, !8, i64 4, !8, i64 8}
!26 = !{!27, !8, i64 140}
!27 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !8, i64 52, !4, i64 56, !3, i64 68, !8, i64 72, !3, i64 76, !28, i64 80, !8, i64 84, !28, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !8, i64 128, !8, i64 132, !8, i64 136, !8, i64 140, !3, i64 144}
!28 = !{!"long", !4, i64 0}
!29 = !{!28, !28, i64 0}
!30 = !{!31, !31, i64 0}
!31 = !{!"short", !4, i64 0}
!32 = !{!22, !3, i64 4084}
!33 = !{!34, !4, i64 155}
!34 = !{!"MB_MODE_INFO", !35, i64 0, !36, i64 8, !4, i64 52, !8, i64 60, !4, i64 64, !37, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !38, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!35 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!36 = !{!"", !4, i64 0, !31, i64 32, !31, i64 34, !31, i64 36, !31, i64 38, !4, i64 40, !4, i64 41}
!37 = !{!"", !4, i64 0, !4, i64 48}
!38 = !{!"", !4, i64 0, !4, i64 1}
!39 = !{!34, !4, i64 154}
!40 = !{!22, !8, i64 6836}
!41 = !{!42, !3, i64 24}
!42 = !{!"macroblockd_plane", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !8, i64 16, !8, i64 20, !43, i64 24, !4, i64 44, !3, i64 84, !3, i64 88, !4, i64 92, !3, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!43 = !{!"buf_2d", !3, i64 0, !3, i64 4, !8, i64 8, !8, i64 12, !8, i64 16}
!44 = !{!42, !8, i64 40}
!45 = !{!22, !8, i64 0}
!46 = !{!22, !8, i64 4}
!47 = !{!13, !8, i64 4240}
!48 = !{!13, !8, i64 4236}
!49 = !{!22, !8, i64 4120}
!50 = !{!42, !8, i64 16}
!51 = !{!22, !8, i64 4128}
!52 = !{!42, !8, i64 20}
