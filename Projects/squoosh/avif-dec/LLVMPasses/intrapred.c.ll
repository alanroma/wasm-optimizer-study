; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/intrapred.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/intrapred.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@sm_weight_arrays = internal constant [128 x i8] c"\00\00\FF\80\FF\95U@\FF\C5\92iI2% \FF\E1\C4\AA\91{fTD6+!\1A\14\11\10\FF\F0\E1\D2\C4\B6\A9\9D\91\85zoe\\SJB;4-'\22\1D\19\15\11\0E\0C\0A\09\08\08\FF\F8\F0\E9\E1\DA\D2\CB\C4\BD\B6\B0\A9\A3\9C\96\90\8A\85\7Fytoje`[VRMIEA=962/,)&# \1D\1B\19\16\14\12\10\0F\0D\0C\0A\09\08\07\06\06\05\05\04\04\04", align 16

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3, i32 2, i32 21846)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @dc_predictor_rect(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left, i32 %shift1, i32 %multiplier) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %shift1.addr = alloca i32, align 4
  %multiplier.addr = alloca i32, align 4
  %sum = alloca i32, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  %expected_dc = alloca i32, align 4
  %r = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  store i32 %shift1, i32* %shift1.addr, align 4, !tbaa !8
  store i32 %multiplier, i32* %multiplier.addr, align 4, !tbaa !8
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %sum, align 4, !tbaa !8
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %7 to i32
  %8 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %8, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %10 = bitcast i32* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  store i32 0, i32* %i1, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc10, %for.end
  %11 = load i32, i32* %i1, align 4, !tbaa !8
  %12 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %11, %12
  br i1 %cmp3, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond2
  %13 = bitcast i32* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  br label %for.end12

for.body6:                                        ; preds = %for.cond2
  %14 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %15 = load i32, i32* %i1, align 4, !tbaa !8
  %arrayidx7 = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load i8, i8* %arrayidx7, align 1, !tbaa !10
  %conv8 = zext i8 %16 to i32
  %17 = load i32, i32* %sum, align 4, !tbaa !8
  %add9 = add nsw i32 %17, %conv8
  store i32 %add9, i32* %sum, align 4, !tbaa !8
  br label %for.inc10

for.inc10:                                        ; preds = %for.body6
  %18 = load i32, i32* %i1, align 4, !tbaa !8
  %inc11 = add nsw i32 %18, 1
  store i32 %inc11, i32* %i1, align 4, !tbaa !8
  br label %for.cond2

for.end12:                                        ; preds = %for.cond.cleanup5
  %19 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = load i32, i32* %sum, align 4, !tbaa !8
  %21 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %22 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add13 = add nsw i32 %21, %22
  %shr = ashr i32 %add13, 1
  %add14 = add nsw i32 %20, %shr
  %23 = load i32, i32* %shift1.addr, align 4, !tbaa !8
  %24 = load i32, i32* %multiplier.addr, align 4, !tbaa !8
  %call = call i32 @divide_using_multiply_shift(i32 %add14, i32 %23, i32 %24, i32 16)
  store i32 %call, i32* %expected_dc, align 4, !tbaa !8
  %25 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc20, %for.end12
  %26 = load i32, i32* %r, align 4, !tbaa !8
  %27 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp16 = icmp slt i32 %26, %27
  br i1 %cmp16, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond15
  %28 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  br label %for.end22

for.body19:                                       ; preds = %for.cond15
  %29 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %30 = load i32, i32* %expected_dc, align 4, !tbaa !8
  %31 = trunc i32 %30 to i8
  %32 = load i32, i32* %bw.addr, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %29, i8 %31, i32 %32, i1 false)
  %33 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %34 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %34, i32 %33
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc20

for.inc20:                                        ; preds = %for.body19
  %35 = load i32, i32* %r, align 4, !tbaa !8
  %inc21 = add nsw i32 %35, 1
  store i32 %inc21, i32* %r, align 4, !tbaa !8
  br label %for.cond15

for.end22:                                        ; preds = %for.cond.cleanup18
  %36 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  %37 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3, i32 2, i32 21846)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3, i32 2, i32 13108)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3, i32 2, i32 13108)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3, i32 3, i32 21846)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3, i32 3, i32 21846)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3, i32 3, i32 13108)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3, i32 3, i32 13108)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3, i32 4, i32 21846)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3, i32 4, i32 21846)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3, i32 4, i32 13108)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3, i32 4, i32 13108)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3, i32 5, i32 21846)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor_rect(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3, i32 5, i32 21846)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4, i32 2, i32 43691)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_dc_predictor_rect(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd, i32 %shift1, i32 %multiplier) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %shift1.addr = alloca i32, align 4
  %multiplier.addr = alloca i32, align 4
  %sum = alloca i32, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  %expected_dc = alloca i32, align 4
  %r = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  store i32 %shift1, i32* %shift1.addr, align 4, !tbaa !8
  store i32 %multiplier, i32* %multiplier.addr, align 4, !tbaa !8
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %sum, align 4, !tbaa !8
  %1 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !8
  %4 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %8 to i32
  %9 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %9, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %11 = bitcast i32* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  store i32 0, i32* %i1, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc10, %for.end
  %12 = load i32, i32* %i1, align 4, !tbaa !8
  %13 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %12, %13
  br i1 %cmp3, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond2
  %14 = bitcast i32* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  br label %for.end12

for.body6:                                        ; preds = %for.cond2
  %15 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i1, align 4, !tbaa !8
  %arrayidx7 = getelementptr inbounds i16, i16* %15, i32 %16
  %17 = load i16, i16* %arrayidx7, align 2, !tbaa !11
  %conv8 = zext i16 %17 to i32
  %18 = load i32, i32* %sum, align 4, !tbaa !8
  %add9 = add nsw i32 %18, %conv8
  store i32 %add9, i32* %sum, align 4, !tbaa !8
  br label %for.inc10

for.inc10:                                        ; preds = %for.body6
  %19 = load i32, i32* %i1, align 4, !tbaa !8
  %inc11 = add nsw i32 %19, 1
  store i32 %inc11, i32* %i1, align 4, !tbaa !8
  br label %for.cond2

for.end12:                                        ; preds = %for.cond.cleanup5
  %20 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  %21 = load i32, i32* %sum, align 4, !tbaa !8
  %22 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %23 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add13 = add nsw i32 %22, %23
  %shr = ashr i32 %add13, 1
  %add14 = add nsw i32 %21, %shr
  %24 = load i32, i32* %shift1.addr, align 4, !tbaa !8
  %25 = load i32, i32* %multiplier.addr, align 4, !tbaa !8
  %call = call i32 @divide_using_multiply_shift(i32 %add14, i32 %24, i32 %25, i32 17)
  store i32 %call, i32* %expected_dc, align 4, !tbaa !8
  %26 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #5
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc21, %for.end12
  %27 = load i32, i32* %r, align 4, !tbaa !8
  %28 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp16 = icmp slt i32 %27, %28
  br i1 %cmp16, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond15
  %29 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  br label %for.end23

for.body19:                                       ; preds = %for.cond15
  %30 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %31 = bitcast i16* %30 to i8*
  %32 = load i32, i32* %expected_dc, align 4, !tbaa !8
  %33 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %call20 = call i8* @aom_memset16(i8* %31, i32 %32, i32 %33)
  %34 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %35 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %35, i32 %34
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc21

for.inc21:                                        ; preds = %for.body19
  %36 = load i32, i32* %r, align 4, !tbaa !8
  %inc22 = add nsw i32 %36, 1
  store i32 %inc22, i32* %r, align 4, !tbaa !8
  br label %for.cond15

for.end23:                                        ; preds = %for.cond.cleanup18
  %37 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  %38 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4, i32 2, i32 43691)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4, i32 2, i32 26215)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4, i32 2, i32 26215)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4, i32 3, i32 43691)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4, i32 3, i32 43691)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4, i32 3, i32 26215)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4, i32 3, i32 26215)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4, i32 4, i32 43691)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4, i32 4, i32 43691)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4, i32 4, i32 26215)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4, i32 4, i32 26215)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4, i32 5, i32 43691)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor_rect(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4, i32 5, i32 43691)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @v_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %r = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %left.addr, align 4, !tbaa !2
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %r, align 4, !tbaa !8
  %3 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %6 = load i32, i32* %bw.addr, align 4, !tbaa !8
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %4, i8* align 1 %5, i32 %6, i1 false)
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 %7
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_v_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %r = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %2 = load i32, i32* %bd.addr, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %r, align 4, !tbaa !8
  %4 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %6 = bitcast i16* %5 to i8*
  %7 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %8 = bitcast i16* %7 to i8*
  %9 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %mul = mul i32 %9, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %6, i8* align 2 %8, i32 %mul, i1 false)
  %10 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %11 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %11, i32 %10
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_v_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @v_predictor(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_v_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_v_predictor(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @h_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %r = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %above.addr, align 4, !tbaa !2
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %r, align 4, !tbaa !8
  %3 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %6 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %7 to i32
  %8 = trunc i32 %conv to i8
  %9 = load i32, i32* %bw.addr, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %4, i8 %8, i32 %9, i1 false)
  %10 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %11 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %11, i32 %10
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_h_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %r = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %2 = load i32, i32* %bd.addr, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %r, align 4, !tbaa !8
  %4 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %6 = bitcast i16* %5 to i8*
  %7 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %8 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %7, i32 %8
  %9 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %9 to i32
  %10 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %call = call i8* @aom_memset16(i8* %6, i32 %conv, i32 %10)
  %11 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %12 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %12, i32 %11
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_h_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @h_predictor(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_h_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_h_predictor(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @smooth_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %below_pred = alloca i8, align 1
  %right_pred = alloca i8, align 1
  %sm_weights_w = alloca i8*, align 4
  %sm_weights_h = alloca i8*, align 4
  %log2_scale = alloca i32, align 4
  %scale = alloca i16, align 2
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %pixels = alloca [4 x i8], align 1
  %weights = alloca [4 x i8], align 1
  %this_pred = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %below_pred) #5
  %0 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %1 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %1, 1
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 %sub
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !10
  store i8 %2, i8* %below_pred, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %right_pred) #5
  %3 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %sub1 = sub nsw i32 %4, 1
  %arrayidx2 = getelementptr inbounds i8, i8* %3, i32 %sub1
  %5 = load i8, i8* %arrayidx2, align 1, !tbaa !10
  store i8 %5, i8* %right_pred, align 1, !tbaa !10
  %6 = bitcast i8** %sm_weights_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* getelementptr inbounds ([128 x i8], [128 x i8]* @sm_weight_arrays, i32 0, i32 0), i32 %7
  store i8* %add.ptr, i8** %sm_weights_w, align 4, !tbaa !2
  %8 = bitcast i8** %sm_weights_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add.ptr3 = getelementptr inbounds i8, i8* getelementptr inbounds ([128 x i8], [128 x i8]* @sm_weight_arrays, i32 0, i32 0), i32 %9
  store i8* %add.ptr3, i8** %sm_weights_h, align 4, !tbaa !2
  %10 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  store i32 9, i32* %log2_scale, align 4, !tbaa !8
  %11 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %11) #5
  store i16 256, i16* %scale, align 2, !tbaa !11
  %12 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc39, %entry
  %13 = load i32, i32* %r, align 4, !tbaa !8
  %14 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %13, %14
  br i1 %cmp, label %for.body, label %for.end41

for.body:                                         ; preds = %for.cond
  %15 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc35, %for.body
  %16 = load i32, i32* %c, align 4, !tbaa !8
  %17 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp5 = icmp slt i32 %16, %17
  br i1 %cmp5, label %for.body6, label %for.end37

for.body6:                                        ; preds = %for.cond4
  %18 = bitcast [4 x i8]* %pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %arrayinit.begin = getelementptr inbounds [4 x i8], [4 x i8]* %pixels, i32 0, i32 0
  %19 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %20 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx7 = getelementptr inbounds i8, i8* %19, i32 %20
  %21 = load i8, i8* %arrayidx7, align 1, !tbaa !10
  store i8 %21, i8* %arrayinit.begin, align 1, !tbaa !10
  %arrayinit.element = getelementptr inbounds i8, i8* %arrayinit.begin, i32 1
  %22 = load i8, i8* %below_pred, align 1, !tbaa !10
  store i8 %22, i8* %arrayinit.element, align 1, !tbaa !10
  %arrayinit.element8 = getelementptr inbounds i8, i8* %arrayinit.element, i32 1
  %23 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %24 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx9 = getelementptr inbounds i8, i8* %23, i32 %24
  %25 = load i8, i8* %arrayidx9, align 1, !tbaa !10
  store i8 %25, i8* %arrayinit.element8, align 1, !tbaa !10
  %arrayinit.element10 = getelementptr inbounds i8, i8* %arrayinit.element8, i32 1
  %26 = load i8, i8* %right_pred, align 1, !tbaa !10
  store i8 %26, i8* %arrayinit.element10, align 1, !tbaa !10
  %27 = bitcast [4 x i8]* %weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  %arrayinit.begin11 = getelementptr inbounds [4 x i8], [4 x i8]* %weights, i32 0, i32 0
  %28 = load i8*, i8** %sm_weights_h, align 4, !tbaa !2
  %29 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds i8, i8* %28, i32 %29
  %30 = load i8, i8* %arrayidx12, align 1, !tbaa !10
  store i8 %30, i8* %arrayinit.begin11, align 1, !tbaa !10
  %arrayinit.element13 = getelementptr inbounds i8, i8* %arrayinit.begin11, i32 1
  %31 = load i8*, i8** %sm_weights_h, align 4, !tbaa !2
  %32 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx14 = getelementptr inbounds i8, i8* %31, i32 %32
  %33 = load i8, i8* %arrayidx14, align 1, !tbaa !10
  %conv = zext i8 %33 to i32
  %sub15 = sub nsw i32 256, %conv
  %conv16 = trunc i32 %sub15 to i8
  store i8 %conv16, i8* %arrayinit.element13, align 1, !tbaa !10
  %arrayinit.element17 = getelementptr inbounds i8, i8* %arrayinit.element13, i32 1
  %34 = load i8*, i8** %sm_weights_w, align 4, !tbaa !2
  %35 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx18 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx18, align 1, !tbaa !10
  store i8 %36, i8* %arrayinit.element17, align 1, !tbaa !10
  %arrayinit.element19 = getelementptr inbounds i8, i8* %arrayinit.element17, i32 1
  %37 = load i8*, i8** %sm_weights_w, align 4, !tbaa !2
  %38 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx20 = getelementptr inbounds i8, i8* %37, i32 %38
  %39 = load i8, i8* %arrayidx20, align 1, !tbaa !10
  %conv21 = zext i8 %39 to i32
  %sub22 = sub nsw i32 256, %conv21
  %conv23 = trunc i32 %sub22 to i8
  store i8 %conv23, i8* %arrayinit.element19, align 1, !tbaa !10
  %40 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  store i32 0, i32* %this_pred, align 4, !tbaa !8
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #5
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc, %for.body6
  %42 = load i32, i32* %i, align 4, !tbaa !8
  %cmp25 = icmp slt i32 %42, 4
  br i1 %cmp25, label %for.body27, label %for.end

for.body27:                                       ; preds = %for.cond24
  %43 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx28 = getelementptr inbounds [4 x i8], [4 x i8]* %weights, i32 0, i32 %43
  %44 = load i8, i8* %arrayidx28, align 1, !tbaa !10
  %conv29 = zext i8 %44 to i32
  %45 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx30 = getelementptr inbounds [4 x i8], [4 x i8]* %pixels, i32 0, i32 %45
  %46 = load i8, i8* %arrayidx30, align 1, !tbaa !10
  %conv31 = zext i8 %46 to i32
  %mul = mul nsw i32 %conv29, %conv31
  %47 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add = add i32 %47, %mul
  store i32 %add, i32* %this_pred, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body27
  %48 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond24

for.end:                                          ; preds = %for.cond24
  %49 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add32 = add i32 %49, 256
  %shr = lshr i32 %add32, 9
  %conv33 = trunc i32 %shr to i8
  %50 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %51 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx34 = getelementptr inbounds i8, i8* %50, i32 %51
  store i8 %conv33, i8* %arrayidx34, align 1, !tbaa !10
  %52 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #5
  %53 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #5
  %54 = bitcast [4 x i8]* %weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #5
  %55 = bitcast [4 x i8]* %pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  br label %for.inc35

for.inc35:                                        ; preds = %for.end
  %56 = load i32, i32* %c, align 4, !tbaa !8
  %inc36 = add nsw i32 %56, 1
  store i32 %inc36, i32* %c, align 4, !tbaa !8
  br label %for.cond4

for.end37:                                        ; preds = %for.cond4
  %57 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %58 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr38 = getelementptr inbounds i8, i8* %58, i32 %57
  store i8* %add.ptr38, i8** %dst.addr, align 4, !tbaa !2
  %59 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #5
  br label %for.inc39

for.inc39:                                        ; preds = %for.end37
  %60 = load i32, i32* %r, align 4, !tbaa !8
  %inc40 = add nsw i32 %60, 1
  store i32 %inc40, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end41:                                        ; preds = %for.cond
  %61 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #5
  %62 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %62) #5
  %63 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  %64 = bitcast i8** %sm_weights_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  %65 = bitcast i8** %sm_weights_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %right_pred) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %below_pred) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_smooth_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %below_pred = alloca i16, align 2
  %right_pred = alloca i16, align 2
  %sm_weights_w = alloca i8*, align 4
  %sm_weights_h = alloca i8*, align 4
  %log2_scale = alloca i32, align 4
  %scale = alloca i16, align 2
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %pixels = alloca [4 x i16], align 2
  %weights = alloca [4 x i8], align 1
  %this_pred = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %1 = bitcast i16* %below_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %1) #5
  %2 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %3 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i16, i16* %2, i32 %sub
  %4 = load i16, i16* %arrayidx, align 2, !tbaa !11
  store i16 %4, i16* %below_pred, align 2, !tbaa !11
  %5 = bitcast i16* %right_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %5) #5
  %6 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %7 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %sub1 = sub nsw i32 %7, 1
  %arrayidx2 = getelementptr inbounds i16, i16* %6, i32 %sub1
  %8 = load i16, i16* %arrayidx2, align 2, !tbaa !11
  store i16 %8, i16* %right_pred, align 2, !tbaa !11
  %9 = bitcast i8** %sm_weights_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* getelementptr inbounds ([128 x i8], [128 x i8]* @sm_weight_arrays, i32 0, i32 0), i32 %10
  store i8* %add.ptr, i8** %sm_weights_w, align 4, !tbaa !2
  %11 = bitcast i8** %sm_weights_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add.ptr3 = getelementptr inbounds i8, i8* getelementptr inbounds ([128 x i8], [128 x i8]* @sm_weight_arrays, i32 0, i32 0), i32 %12
  store i8* %add.ptr3, i8** %sm_weights_h, align 4, !tbaa !2
  %13 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  store i32 9, i32* %log2_scale, align 4, !tbaa !8
  %14 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %14) #5
  store i16 256, i16* %scale, align 2, !tbaa !11
  %15 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc39, %entry
  %16 = load i32, i32* %r, align 4, !tbaa !8
  %17 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %16, %17
  br i1 %cmp, label %for.body, label %for.end41

for.body:                                         ; preds = %for.cond
  %18 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc35, %for.body
  %19 = load i32, i32* %c, align 4, !tbaa !8
  %20 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp5 = icmp slt i32 %19, %20
  br i1 %cmp5, label %for.body6, label %for.end37

for.body6:                                        ; preds = %for.cond4
  %21 = bitcast [4 x i16]* %pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %21) #5
  %arrayinit.begin = getelementptr inbounds [4 x i16], [4 x i16]* %pixels, i32 0, i32 0
  %22 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %23 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx7 = getelementptr inbounds i16, i16* %22, i32 %23
  %24 = load i16, i16* %arrayidx7, align 2, !tbaa !11
  store i16 %24, i16* %arrayinit.begin, align 2, !tbaa !11
  %arrayinit.element = getelementptr inbounds i16, i16* %arrayinit.begin, i32 1
  %25 = load i16, i16* %below_pred, align 2, !tbaa !11
  store i16 %25, i16* %arrayinit.element, align 2, !tbaa !11
  %arrayinit.element8 = getelementptr inbounds i16, i16* %arrayinit.element, i32 1
  %26 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %27 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx9 = getelementptr inbounds i16, i16* %26, i32 %27
  %28 = load i16, i16* %arrayidx9, align 2, !tbaa !11
  store i16 %28, i16* %arrayinit.element8, align 2, !tbaa !11
  %arrayinit.element10 = getelementptr inbounds i16, i16* %arrayinit.element8, i32 1
  %29 = load i16, i16* %right_pred, align 2, !tbaa !11
  store i16 %29, i16* %arrayinit.element10, align 2, !tbaa !11
  %30 = bitcast [4 x i8]* %weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #5
  %arrayinit.begin11 = getelementptr inbounds [4 x i8], [4 x i8]* %weights, i32 0, i32 0
  %31 = load i8*, i8** %sm_weights_h, align 4, !tbaa !2
  %32 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds i8, i8* %31, i32 %32
  %33 = load i8, i8* %arrayidx12, align 1, !tbaa !10
  store i8 %33, i8* %arrayinit.begin11, align 1, !tbaa !10
  %arrayinit.element13 = getelementptr inbounds i8, i8* %arrayinit.begin11, i32 1
  %34 = load i8*, i8** %sm_weights_h, align 4, !tbaa !2
  %35 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx14, align 1, !tbaa !10
  %conv = zext i8 %36 to i32
  %sub15 = sub nsw i32 256, %conv
  %conv16 = trunc i32 %sub15 to i8
  store i8 %conv16, i8* %arrayinit.element13, align 1, !tbaa !10
  %arrayinit.element17 = getelementptr inbounds i8, i8* %arrayinit.element13, i32 1
  %37 = load i8*, i8** %sm_weights_w, align 4, !tbaa !2
  %38 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx18 = getelementptr inbounds i8, i8* %37, i32 %38
  %39 = load i8, i8* %arrayidx18, align 1, !tbaa !10
  store i8 %39, i8* %arrayinit.element17, align 1, !tbaa !10
  %arrayinit.element19 = getelementptr inbounds i8, i8* %arrayinit.element17, i32 1
  %40 = load i8*, i8** %sm_weights_w, align 4, !tbaa !2
  %41 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx20 = getelementptr inbounds i8, i8* %40, i32 %41
  %42 = load i8, i8* %arrayidx20, align 1, !tbaa !10
  %conv21 = zext i8 %42 to i32
  %sub22 = sub nsw i32 256, %conv21
  %conv23 = trunc i32 %sub22 to i8
  store i8 %conv23, i8* %arrayinit.element19, align 1, !tbaa !10
  %43 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #5
  store i32 0, i32* %this_pred, align 4, !tbaa !8
  %44 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc, %for.body6
  %45 = load i32, i32* %i, align 4, !tbaa !8
  %cmp25 = icmp slt i32 %45, 4
  br i1 %cmp25, label %for.body27, label %for.end

for.body27:                                       ; preds = %for.cond24
  %46 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx28 = getelementptr inbounds [4 x i8], [4 x i8]* %weights, i32 0, i32 %46
  %47 = load i8, i8* %arrayidx28, align 1, !tbaa !10
  %conv29 = zext i8 %47 to i32
  %48 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx30 = getelementptr inbounds [4 x i16], [4 x i16]* %pixels, i32 0, i32 %48
  %49 = load i16, i16* %arrayidx30, align 2, !tbaa !11
  %conv31 = zext i16 %49 to i32
  %mul = mul nsw i32 %conv29, %conv31
  %50 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add = add i32 %50, %mul
  store i32 %add, i32* %this_pred, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body27
  %51 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond24

for.end:                                          ; preds = %for.cond24
  %52 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add32 = add i32 %52, 256
  %shr = lshr i32 %add32, 9
  %conv33 = trunc i32 %shr to i16
  %53 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %54 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx34 = getelementptr inbounds i16, i16* %53, i32 %54
  store i16 %conv33, i16* %arrayidx34, align 2, !tbaa !11
  %55 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  %56 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #5
  %57 = bitcast [4 x i8]* %weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #5
  %58 = bitcast [4 x i16]* %pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %58) #5
  br label %for.inc35

for.inc35:                                        ; preds = %for.end
  %59 = load i32, i32* %c, align 4, !tbaa !8
  %inc36 = add nsw i32 %59, 1
  store i32 %inc36, i32* %c, align 4, !tbaa !8
  br label %for.cond4

for.end37:                                        ; preds = %for.cond4
  %60 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %61 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr38 = getelementptr inbounds i16, i16* %61, i32 %60
  store i16* %add.ptr38, i16** %dst.addr, align 4, !tbaa !2
  %62 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #5
  br label %for.inc39

for.inc39:                                        ; preds = %for.end37
  %63 = load i32, i32* %r, align 4, !tbaa !8
  %inc40 = add nsw i32 %63, 1
  store i32 %inc40, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end41:                                        ; preds = %for.cond
  %64 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  %65 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %65) #5
  %66 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = bitcast i8** %sm_weights_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast i8** %sm_weights_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  %69 = bitcast i16* %right_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %69) #5
  %70 = bitcast i16* %below_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %70) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_predictor(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_predictor(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @smooth_v_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %below_pred = alloca i8, align 1
  %sm_weights = alloca i8*, align 4
  %log2_scale = alloca i32, align 4
  %scale = alloca i16, align 2
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %pixels = alloca [2 x i8], align 1
  %weights = alloca [2 x i8], align 1
  %this_pred = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %below_pred) #5
  %0 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %1 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %1, 1
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 %sub
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !10
  store i8 %2, i8* %below_pred, align 1, !tbaa !10
  %3 = bitcast i8** %sm_weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* getelementptr inbounds ([128 x i8], [128 x i8]* @sm_weight_arrays, i32 0, i32 0), i32 %4
  store i8* %add.ptr, i8** %sm_weights, align 4, !tbaa !2
  %5 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  store i32 8, i32* %log2_scale, align 4, !tbaa !8
  %6 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %6) #5
  store i16 256, i16* %scale, align 2, !tbaa !11
  %7 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc26, %entry
  %8 = load i32, i32* %r, align 4, !tbaa !8
  %9 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end28

for.body:                                         ; preds = %for.cond
  %10 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc22, %for.body
  %11 = load i32, i32* %c, align 4, !tbaa !8
  %12 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %11, %12
  br i1 %cmp2, label %for.body3, label %for.end24

for.body3:                                        ; preds = %for.cond1
  %13 = bitcast [2 x i8]* %pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #5
  %arrayinit.begin = getelementptr inbounds [2 x i8], [2 x i8]* %pixels, i32 0, i32 0
  %14 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %15 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load i8, i8* %arrayidx4, align 1, !tbaa !10
  store i8 %16, i8* %arrayinit.begin, align 1, !tbaa !10
  %arrayinit.element = getelementptr inbounds i8, i8* %arrayinit.begin, i32 1
  %17 = load i8, i8* %below_pred, align 1, !tbaa !10
  store i8 %17, i8* %arrayinit.element, align 1, !tbaa !10
  %18 = bitcast [2 x i8]* %weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %18) #5
  %arrayinit.begin5 = getelementptr inbounds [2 x i8], [2 x i8]* %weights, i32 0, i32 0
  %19 = load i8*, i8** %sm_weights, align 4, !tbaa !2
  %20 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds i8, i8* %19, i32 %20
  %21 = load i8, i8* %arrayidx6, align 1, !tbaa !10
  store i8 %21, i8* %arrayinit.begin5, align 1, !tbaa !10
  %arrayinit.element7 = getelementptr inbounds i8, i8* %arrayinit.begin5, i32 1
  %22 = load i8*, i8** %sm_weights, align 4, !tbaa !2
  %23 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx8 = getelementptr inbounds i8, i8* %22, i32 %23
  %24 = load i8, i8* %arrayidx8, align 1, !tbaa !10
  %conv = zext i8 %24 to i32
  %sub9 = sub nsw i32 256, %conv
  %conv10 = trunc i32 %sub9 to i8
  store i8 %conv10, i8* %arrayinit.element7, align 1, !tbaa !10
  %25 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  store i32 0, i32* %this_pred, align 4, !tbaa !8
  %26 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #5
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc, %for.body3
  %27 = load i32, i32* %i, align 4, !tbaa !8
  %cmp12 = icmp slt i32 %27, 2
  br i1 %cmp12, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond11
  %28 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx15 = getelementptr inbounds [2 x i8], [2 x i8]* %weights, i32 0, i32 %28
  %29 = load i8, i8* %arrayidx15, align 1, !tbaa !10
  %conv16 = zext i8 %29 to i32
  %30 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx17 = getelementptr inbounds [2 x i8], [2 x i8]* %pixels, i32 0, i32 %30
  %31 = load i8, i8* %arrayidx17, align 1, !tbaa !10
  %conv18 = zext i8 %31 to i32
  %mul = mul nsw i32 %conv16, %conv18
  %32 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add = add i32 %32, %mul
  store i32 %add, i32* %this_pred, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %33 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond11

for.end:                                          ; preds = %for.cond11
  %34 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add19 = add i32 %34, 128
  %shr = lshr i32 %add19, 8
  %conv20 = trunc i32 %shr to i8
  %35 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %36 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx21 = getelementptr inbounds i8, i8* %35, i32 %36
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !10
  %37 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  %38 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #5
  %39 = bitcast [2 x i8]* %weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %39) #5
  %40 = bitcast [2 x i8]* %pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %40) #5
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %41 = load i32, i32* %c, align 4, !tbaa !8
  %inc23 = add nsw i32 %41, 1
  store i32 %inc23, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.end24:                                        ; preds = %for.cond1
  %42 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %43 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr25 = getelementptr inbounds i8, i8* %43, i32 %42
  store i8* %add.ptr25, i8** %dst.addr, align 4, !tbaa !2
  %44 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #5
  br label %for.inc26

for.inc26:                                        ; preds = %for.end24
  %45 = load i32, i32* %r, align 4, !tbaa !8
  %inc27 = add nsw i32 %45, 1
  store i32 %inc27, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end28:                                        ; preds = %for.cond
  %46 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  %47 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %47) #5
  %48 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  %49 = bitcast i8** %sm_weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %below_pred) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_smooth_v_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %below_pred = alloca i16, align 2
  %sm_weights = alloca i8*, align 4
  %log2_scale = alloca i32, align 4
  %scale = alloca i16, align 2
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %pixels = alloca [2 x i16], align 2
  %weights = alloca [2 x i8], align 1
  %this_pred = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %1 = bitcast i16* %below_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %1) #5
  %2 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %3 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i16, i16* %2, i32 %sub
  %4 = load i16, i16* %arrayidx, align 2, !tbaa !11
  store i16 %4, i16* %below_pred, align 2, !tbaa !11
  %5 = bitcast i8** %sm_weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* getelementptr inbounds ([128 x i8], [128 x i8]* @sm_weight_arrays, i32 0, i32 0), i32 %6
  store i8* %add.ptr, i8** %sm_weights, align 4, !tbaa !2
  %7 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  store i32 8, i32* %log2_scale, align 4, !tbaa !8
  %8 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %8) #5
  store i16 256, i16* %scale, align 2, !tbaa !11
  %9 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc26, %entry
  %10 = load i32, i32* %r, align 4, !tbaa !8
  %11 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.end28

for.body:                                         ; preds = %for.cond
  %12 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc22, %for.body
  %13 = load i32, i32* %c, align 4, !tbaa !8
  %14 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %13, %14
  br i1 %cmp2, label %for.body3, label %for.end24

for.body3:                                        ; preds = %for.cond1
  %15 = bitcast [2 x i16]* %pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %arrayinit.begin = getelementptr inbounds [2 x i16], [2 x i16]* %pixels, i32 0, i32 0
  %16 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %17 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds i16, i16* %16, i32 %17
  %18 = load i16, i16* %arrayidx4, align 2, !tbaa !11
  store i16 %18, i16* %arrayinit.begin, align 2, !tbaa !11
  %arrayinit.element = getelementptr inbounds i16, i16* %arrayinit.begin, i32 1
  %19 = load i16, i16* %below_pred, align 2, !tbaa !11
  store i16 %19, i16* %arrayinit.element, align 2, !tbaa !11
  %20 = bitcast [2 x i8]* %weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %20) #5
  %arrayinit.begin5 = getelementptr inbounds [2 x i8], [2 x i8]* %weights, i32 0, i32 0
  %21 = load i8*, i8** %sm_weights, align 4, !tbaa !2
  %22 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds i8, i8* %21, i32 %22
  %23 = load i8, i8* %arrayidx6, align 1, !tbaa !10
  store i8 %23, i8* %arrayinit.begin5, align 1, !tbaa !10
  %arrayinit.element7 = getelementptr inbounds i8, i8* %arrayinit.begin5, i32 1
  %24 = load i8*, i8** %sm_weights, align 4, !tbaa !2
  %25 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx8 = getelementptr inbounds i8, i8* %24, i32 %25
  %26 = load i8, i8* %arrayidx8, align 1, !tbaa !10
  %conv = zext i8 %26 to i32
  %sub9 = sub nsw i32 256, %conv
  %conv10 = trunc i32 %sub9 to i8
  store i8 %conv10, i8* %arrayinit.element7, align 1, !tbaa !10
  %27 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  store i32 0, i32* %this_pred, align 4, !tbaa !8
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc, %for.body3
  %29 = load i32, i32* %i, align 4, !tbaa !8
  %cmp12 = icmp slt i32 %29, 2
  br i1 %cmp12, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond11
  %30 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx15 = getelementptr inbounds [2 x i8], [2 x i8]* %weights, i32 0, i32 %30
  %31 = load i8, i8* %arrayidx15, align 1, !tbaa !10
  %conv16 = zext i8 %31 to i32
  %32 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx17 = getelementptr inbounds [2 x i16], [2 x i16]* %pixels, i32 0, i32 %32
  %33 = load i16, i16* %arrayidx17, align 2, !tbaa !11
  %conv18 = zext i16 %33 to i32
  %mul = mul nsw i32 %conv16, %conv18
  %34 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add = add i32 %34, %mul
  store i32 %add, i32* %this_pred, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %35 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond11

for.end:                                          ; preds = %for.cond11
  %36 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add19 = add i32 %36, 128
  %shr = lshr i32 %add19, 8
  %conv20 = trunc i32 %shr to i16
  %37 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %38 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx21 = getelementptr inbounds i16, i16* %37, i32 %38
  store i16 %conv20, i16* %arrayidx21, align 2, !tbaa !11
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #5
  %40 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #5
  %41 = bitcast [2 x i8]* %weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %41) #5
  %42 = bitcast [2 x i16]* %pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #5
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %43 = load i32, i32* %c, align 4, !tbaa !8
  %inc23 = add nsw i32 %43, 1
  store i32 %inc23, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.end24:                                        ; preds = %for.cond1
  %44 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %45 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr25 = getelementptr inbounds i16, i16* %45, i32 %44
  store i16* %add.ptr25, i16** %dst.addr, align 4, !tbaa !2
  %46 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  br label %for.inc26

for.inc26:                                        ; preds = %for.end24
  %47 = load i32, i32* %r, align 4, !tbaa !8
  %inc27 = add nsw i32 %47, 1
  store i32 %inc27, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end28:                                        ; preds = %for.cond
  %48 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  %49 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %49) #5
  %50 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  %51 = bitcast i8** %sm_weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #5
  %52 = bitcast i16* %below_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %52) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_v_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_v_predictor(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_v_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_v_predictor(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @smooth_h_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %right_pred = alloca i8, align 1
  %sm_weights = alloca i8*, align 4
  %log2_scale = alloca i32, align 4
  %scale = alloca i16, align 2
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %pixels = alloca [2 x i8], align 1
  %weights = alloca [2 x i8], align 1
  %this_pred = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %right_pred) #5
  %0 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %1 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %1, 1
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 %sub
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !10
  store i8 %2, i8* %right_pred, align 1, !tbaa !10
  %3 = bitcast i8** %sm_weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* getelementptr inbounds ([128 x i8], [128 x i8]* @sm_weight_arrays, i32 0, i32 0), i32 %4
  store i8* %add.ptr, i8** %sm_weights, align 4, !tbaa !2
  %5 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  store i32 8, i32* %log2_scale, align 4, !tbaa !8
  %6 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %6) #5
  store i16 256, i16* %scale, align 2, !tbaa !11
  %7 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc26, %entry
  %8 = load i32, i32* %r, align 4, !tbaa !8
  %9 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end28

for.body:                                         ; preds = %for.cond
  %10 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc22, %for.body
  %11 = load i32, i32* %c, align 4, !tbaa !8
  %12 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %11, %12
  br i1 %cmp2, label %for.body3, label %for.end24

for.body3:                                        ; preds = %for.cond1
  %13 = bitcast [2 x i8]* %pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #5
  %arrayinit.begin = getelementptr inbounds [2 x i8], [2 x i8]* %pixels, i32 0, i32 0
  %14 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %15 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load i8, i8* %arrayidx4, align 1, !tbaa !10
  store i8 %16, i8* %arrayinit.begin, align 1, !tbaa !10
  %arrayinit.element = getelementptr inbounds i8, i8* %arrayinit.begin, i32 1
  %17 = load i8, i8* %right_pred, align 1, !tbaa !10
  store i8 %17, i8* %arrayinit.element, align 1, !tbaa !10
  %18 = bitcast [2 x i8]* %weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %18) #5
  %arrayinit.begin5 = getelementptr inbounds [2 x i8], [2 x i8]* %weights, i32 0, i32 0
  %19 = load i8*, i8** %sm_weights, align 4, !tbaa !2
  %20 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds i8, i8* %19, i32 %20
  %21 = load i8, i8* %arrayidx6, align 1, !tbaa !10
  store i8 %21, i8* %arrayinit.begin5, align 1, !tbaa !10
  %arrayinit.element7 = getelementptr inbounds i8, i8* %arrayinit.begin5, i32 1
  %22 = load i8*, i8** %sm_weights, align 4, !tbaa !2
  %23 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx8 = getelementptr inbounds i8, i8* %22, i32 %23
  %24 = load i8, i8* %arrayidx8, align 1, !tbaa !10
  %conv = zext i8 %24 to i32
  %sub9 = sub nsw i32 256, %conv
  %conv10 = trunc i32 %sub9 to i8
  store i8 %conv10, i8* %arrayinit.element7, align 1, !tbaa !10
  %25 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  store i32 0, i32* %this_pred, align 4, !tbaa !8
  %26 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #5
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc, %for.body3
  %27 = load i32, i32* %i, align 4, !tbaa !8
  %cmp12 = icmp slt i32 %27, 2
  br i1 %cmp12, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond11
  %28 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx15 = getelementptr inbounds [2 x i8], [2 x i8]* %weights, i32 0, i32 %28
  %29 = load i8, i8* %arrayidx15, align 1, !tbaa !10
  %conv16 = zext i8 %29 to i32
  %30 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx17 = getelementptr inbounds [2 x i8], [2 x i8]* %pixels, i32 0, i32 %30
  %31 = load i8, i8* %arrayidx17, align 1, !tbaa !10
  %conv18 = zext i8 %31 to i32
  %mul = mul nsw i32 %conv16, %conv18
  %32 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add = add i32 %32, %mul
  store i32 %add, i32* %this_pred, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %33 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond11

for.end:                                          ; preds = %for.cond11
  %34 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add19 = add i32 %34, 128
  %shr = lshr i32 %add19, 8
  %conv20 = trunc i32 %shr to i8
  %35 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %36 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx21 = getelementptr inbounds i8, i8* %35, i32 %36
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !10
  %37 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  %38 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #5
  %39 = bitcast [2 x i8]* %weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %39) #5
  %40 = bitcast [2 x i8]* %pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %40) #5
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %41 = load i32, i32* %c, align 4, !tbaa !8
  %inc23 = add nsw i32 %41, 1
  store i32 %inc23, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.end24:                                        ; preds = %for.cond1
  %42 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %43 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr25 = getelementptr inbounds i8, i8* %43, i32 %42
  store i8* %add.ptr25, i8** %dst.addr, align 4, !tbaa !2
  %44 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #5
  br label %for.inc26

for.inc26:                                        ; preds = %for.end24
  %45 = load i32, i32* %r, align 4, !tbaa !8
  %inc27 = add nsw i32 %45, 1
  store i32 %inc27, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end28:                                        ; preds = %for.cond
  %46 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  %47 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %47) #5
  %48 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  %49 = bitcast i8** %sm_weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %right_pred) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_smooth_h_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %right_pred = alloca i16, align 2
  %sm_weights = alloca i8*, align 4
  %log2_scale = alloca i32, align 4
  %scale = alloca i16, align 2
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %pixels = alloca [2 x i16], align 2
  %weights = alloca [2 x i8], align 1
  %this_pred = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %1 = bitcast i16* %right_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %1) #5
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i16, i16* %2, i32 %sub
  %4 = load i16, i16* %arrayidx, align 2, !tbaa !11
  store i16 %4, i16* %right_pred, align 2, !tbaa !11
  %5 = bitcast i8** %sm_weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* getelementptr inbounds ([128 x i8], [128 x i8]* @sm_weight_arrays, i32 0, i32 0), i32 %6
  store i8* %add.ptr, i8** %sm_weights, align 4, !tbaa !2
  %7 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  store i32 8, i32* %log2_scale, align 4, !tbaa !8
  %8 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %8) #5
  store i16 256, i16* %scale, align 2, !tbaa !11
  %9 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc26, %entry
  %10 = load i32, i32* %r, align 4, !tbaa !8
  %11 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.end28

for.body:                                         ; preds = %for.cond
  %12 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc22, %for.body
  %13 = load i32, i32* %c, align 4, !tbaa !8
  %14 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %13, %14
  br i1 %cmp2, label %for.body3, label %for.end24

for.body3:                                        ; preds = %for.cond1
  %15 = bitcast [2 x i16]* %pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %arrayinit.begin = getelementptr inbounds [2 x i16], [2 x i16]* %pixels, i32 0, i32 0
  %16 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %17 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds i16, i16* %16, i32 %17
  %18 = load i16, i16* %arrayidx4, align 2, !tbaa !11
  store i16 %18, i16* %arrayinit.begin, align 2, !tbaa !11
  %arrayinit.element = getelementptr inbounds i16, i16* %arrayinit.begin, i32 1
  %19 = load i16, i16* %right_pred, align 2, !tbaa !11
  store i16 %19, i16* %arrayinit.element, align 2, !tbaa !11
  %20 = bitcast [2 x i8]* %weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %20) #5
  %arrayinit.begin5 = getelementptr inbounds [2 x i8], [2 x i8]* %weights, i32 0, i32 0
  %21 = load i8*, i8** %sm_weights, align 4, !tbaa !2
  %22 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds i8, i8* %21, i32 %22
  %23 = load i8, i8* %arrayidx6, align 1, !tbaa !10
  store i8 %23, i8* %arrayinit.begin5, align 1, !tbaa !10
  %arrayinit.element7 = getelementptr inbounds i8, i8* %arrayinit.begin5, i32 1
  %24 = load i8*, i8** %sm_weights, align 4, !tbaa !2
  %25 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx8 = getelementptr inbounds i8, i8* %24, i32 %25
  %26 = load i8, i8* %arrayidx8, align 1, !tbaa !10
  %conv = zext i8 %26 to i32
  %sub9 = sub nsw i32 256, %conv
  %conv10 = trunc i32 %sub9 to i8
  store i8 %conv10, i8* %arrayinit.element7, align 1, !tbaa !10
  %27 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  store i32 0, i32* %this_pred, align 4, !tbaa !8
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc, %for.body3
  %29 = load i32, i32* %i, align 4, !tbaa !8
  %cmp12 = icmp slt i32 %29, 2
  br i1 %cmp12, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond11
  %30 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx15 = getelementptr inbounds [2 x i8], [2 x i8]* %weights, i32 0, i32 %30
  %31 = load i8, i8* %arrayidx15, align 1, !tbaa !10
  %conv16 = zext i8 %31 to i32
  %32 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx17 = getelementptr inbounds [2 x i16], [2 x i16]* %pixels, i32 0, i32 %32
  %33 = load i16, i16* %arrayidx17, align 2, !tbaa !11
  %conv18 = zext i16 %33 to i32
  %mul = mul nsw i32 %conv16, %conv18
  %34 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add = add i32 %34, %mul
  store i32 %add, i32* %this_pred, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %35 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond11

for.end:                                          ; preds = %for.cond11
  %36 = load i32, i32* %this_pred, align 4, !tbaa !8
  %add19 = add i32 %36, 128
  %shr = lshr i32 %add19, 8
  %conv20 = trunc i32 %shr to i16
  %37 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %38 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx21 = getelementptr inbounds i16, i16* %37, i32 %38
  store i16 %conv20, i16* %arrayidx21, align 2, !tbaa !11
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #5
  %40 = bitcast i32* %this_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #5
  %41 = bitcast [2 x i8]* %weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %41) #5
  %42 = bitcast [2 x i16]* %pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #5
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %43 = load i32, i32* %c, align 4, !tbaa !8
  %inc23 = add nsw i32 %43, 1
  store i32 %inc23, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.end24:                                        ; preds = %for.cond1
  %44 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %45 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr25 = getelementptr inbounds i16, i16* %45, i32 %44
  store i16* %add.ptr25, i16** %dst.addr, align 4, !tbaa !2
  %46 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  br label %for.inc26

for.inc26:                                        ; preds = %for.end24
  %47 = load i32, i32* %r, align 4, !tbaa !8
  %inc27 = add nsw i32 %47, 1
  store i32 %inc27, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end28:                                        ; preds = %for.cond
  %48 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  %49 = bitcast i16* %scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %49) #5
  %50 = bitcast i32* %log2_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  %51 = bitcast i8** %sm_weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #5
  %52 = bitcast i16* %right_pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %52) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_smooth_h_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @smooth_h_predictor(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_smooth_h_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_smooth_h_predictor(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @paeth_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %ytop_left = alloca i8, align 1
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ytop_left) #5
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 -1
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !10
  store i8 %3, i8* %ytop_left, align 1, !tbaa !10
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc10, %entry
  %4 = load i32, i32* %r, align 4, !tbaa !8
  %5 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end12

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %c, align 4, !tbaa !8
  %7 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %6, %7
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %8 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %9 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx4, align 1, !tbaa !10
  %conv = zext i8 %10 to i16
  %11 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %12 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx5 = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load i8, i8* %arrayidx5, align 1, !tbaa !10
  %conv6 = zext i8 %13 to i16
  %14 = load i8, i8* %ytop_left, align 1, !tbaa !10
  %conv7 = zext i8 %14 to i16
  %call = call zeroext i16 @paeth_predictor_single(i16 zeroext %conv, i16 zeroext %conv6, i16 zeroext %conv7)
  %conv8 = trunc i16 %call to i8
  %15 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %16 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx9 = getelementptr inbounds i8, i8* %15, i32 %16
  store i8 %conv8, i8* %arrayidx9, align 1, !tbaa !10
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %17 = load i32, i32* %c, align 4, !tbaa !8
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %18 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %19 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %19, i32 %18
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc10

for.inc10:                                        ; preds = %for.end
  %20 = load i32, i32* %r, align 4, !tbaa !8
  %inc11 = add nsw i32 %20, 1
  store i32 %inc11, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end12:                                        ; preds = %for.cond
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ytop_left) #5
  %21 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  %22 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_paeth_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %ytop_left = alloca i16, align 2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i16* %ytop_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %2) #5
  %3 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 -1
  %4 = load i16, i16* %arrayidx, align 2, !tbaa !11
  store i16 %4, i16* %ytop_left, align 2, !tbaa !11
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc7, %entry
  %6 = load i32, i32* %r, align 4, !tbaa !8
  %7 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end9

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %8 = load i32, i32* %c, align 4, !tbaa !8
  %9 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %10 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %11 = load i32, i32* %r, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds i16, i16* %10, i32 %11
  %12 = load i16, i16* %arrayidx4, align 2, !tbaa !11
  %13 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %14 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx5 = getelementptr inbounds i16, i16* %13, i32 %14
  %15 = load i16, i16* %arrayidx5, align 2, !tbaa !11
  %16 = load i16, i16* %ytop_left, align 2, !tbaa !11
  %call = call zeroext i16 @paeth_predictor_single(i16 zeroext %12, i16 zeroext %15, i16 zeroext %16)
  %17 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %18 = load i32, i32* %c, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds i16, i16* %17, i32 %18
  store i16 %call, i16* %arrayidx6, align 2, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %19 = load i32, i32* %c, align 4, !tbaa !8
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %c, align 4, !tbaa !8
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %20 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %21 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %21, i32 %20
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc7

for.inc7:                                         ; preds = %for.end
  %22 = load i32, i32* %r, align 4, !tbaa !8
  %inc8 = add nsw i32 %22, 1
  store i32 %inc8, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end9:                                         ; preds = %for.cond
  %23 = bitcast i16* %ytop_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %23) #5
  %24 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  %25 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_paeth_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @paeth_predictor(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_paeth_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_paeth_predictor(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @dc_128_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %r = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %2 = load i8*, i8** %left.addr, align 4, !tbaa !2
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %r, align 4, !tbaa !8
  %4 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %6 = load i32, i32* %bw.addr, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %5, i8 -128, i32 %6, i1 false)
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 %7
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_dc_128_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %r = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %left.addr, align 4, !tbaa !2
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %r, align 4, !tbaa !8
  %4 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %6 = bitcast i16* %5 to i8*
  %7 = load i32, i32* %bd.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %7, 8
  %shl = shl i32 128, %sub
  %8 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %call = call i8* @aom_memset16(i8* %6, i32 %shl, i32 %8)
  %9 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %10 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %10, i32 %9
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %r, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %r, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_128_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_128_predictor(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_128_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_128_predictor(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @dc_left_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %r = alloca i32, align 4
  %expected_dc = alloca i32, align 4
  %sum = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %sum, align 4, !tbaa !8
  %4 = load i8*, i8** %above.addr, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i8, i8* %7, i32 %8
  %9 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %9 to i32
  %10 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %10, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load i32, i32* %sum, align 4, !tbaa !8
  %13 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %shr = ashr i32 %13, 1
  %add1 = add nsw i32 %12, %shr
  %14 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %div = sdiv i32 %add1, %14
  store i32 %div, i32* %expected_dc, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc6, %for.end
  %15 = load i32, i32* %r, align 4, !tbaa !8
  %16 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %15, %16
  br i1 %cmp3, label %for.body5, label %for.end8

for.body5:                                        ; preds = %for.cond2
  %17 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %18 = load i32, i32* %expected_dc, align 4, !tbaa !8
  %19 = trunc i32 %18 to i8
  %20 = load i32, i32* %bw.addr, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %17, i8 %19, i32 %20, i1 false)
  %21 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %22 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 %21
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc6

for.inc6:                                         ; preds = %for.body5
  %23 = load i32, i32* %r, align 4, !tbaa !8
  %inc7 = add nsw i32 %23, 1
  store i32 %inc7, i32* %r, align 4, !tbaa !8
  br label %for.cond2

for.end8:                                         ; preds = %for.cond2
  %24 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  %25 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_dc_left_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %r = alloca i32, align 4
  %expected_dc = alloca i32, align 4
  %sum = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %sum, align 4, !tbaa !8
  %4 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !8
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %7 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %8, i32 %9
  %10 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %10 to i32
  %11 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %11, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = load i32, i32* %sum, align 4, !tbaa !8
  %14 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %shr = ashr i32 %14, 1
  %add1 = add nsw i32 %13, %shr
  %15 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %div = sdiv i32 %add1, %15
  store i32 %div, i32* %expected_dc, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc6, %for.end
  %16 = load i32, i32* %r, align 4, !tbaa !8
  %17 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %16, %17
  br i1 %cmp3, label %for.body5, label %for.end8

for.body5:                                        ; preds = %for.cond2
  %18 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %19 = bitcast i16* %18 to i8*
  %20 = load i32, i32* %expected_dc, align 4, !tbaa !8
  %21 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %call = call i8* @aom_memset16(i8* %19, i32 %20, i32 %21)
  %22 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %23 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %23, i32 %22
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc6

for.inc6:                                         ; preds = %for.body5
  %24 = load i32, i32* %r, align 4, !tbaa !8
  %inc7 = add nsw i32 %24, 1
  store i32 %inc7, i32* %r, align 4, !tbaa !8
  br label %for.cond2

for.end8:                                         ; preds = %for.cond2
  %25 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_left_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_left_predictor(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_left_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_left_predictor(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @dc_top_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %r = alloca i32, align 4
  %expected_dc = alloca i32, align 4
  %sum = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %sum, align 4, !tbaa !8
  %4 = load i8*, i8** %left.addr, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i8, i8* %7, i32 %8
  %9 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %9 to i32
  %10 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %10, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load i32, i32* %sum, align 4, !tbaa !8
  %13 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %shr = ashr i32 %13, 1
  %add1 = add nsw i32 %12, %shr
  %14 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %div = sdiv i32 %add1, %14
  store i32 %div, i32* %expected_dc, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc6, %for.end
  %15 = load i32, i32* %r, align 4, !tbaa !8
  %16 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %15, %16
  br i1 %cmp3, label %for.body5, label %for.end8

for.body5:                                        ; preds = %for.cond2
  %17 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %18 = load i32, i32* %expected_dc, align 4, !tbaa !8
  %19 = trunc i32 %18 to i8
  %20 = load i32, i32* %bw.addr, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %17, i8 %19, i32 %20, i1 false)
  %21 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %22 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 %21
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc6

for.inc6:                                         ; preds = %for.body5
  %23 = load i32, i32* %r, align 4, !tbaa !8
  %inc7 = add nsw i32 %23, 1
  store i32 %inc7, i32* %r, align 4, !tbaa !8
  br label %for.cond2

for.end8:                                         ; preds = %for.cond2
  %24 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  %25 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_dc_top_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %r = alloca i32, align 4
  %expected_dc = alloca i32, align 4
  %sum = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %sum, align 4, !tbaa !8
  %4 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !8
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %7 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %8, i32 %9
  %10 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %10 to i32
  %11 = load i32, i32* %sum, align 4, !tbaa !8
  %add = add nsw i32 %11, %conv
  store i32 %add, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = load i32, i32* %sum, align 4, !tbaa !8
  %14 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %shr = ashr i32 %14, 1
  %add1 = add nsw i32 %13, %shr
  %15 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %div = sdiv i32 %add1, %15
  store i32 %div, i32* %expected_dc, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc6, %for.end
  %16 = load i32, i32* %r, align 4, !tbaa !8
  %17 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %16, %17
  br i1 %cmp3, label %for.body5, label %for.end8

for.body5:                                        ; preds = %for.cond2
  %18 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %19 = bitcast i16* %18 to i8*
  %20 = load i32, i32* %expected_dc, align 4, !tbaa !8
  %21 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %call = call i8* @aom_memset16(i8* %19, i32 %20, i32 %21)
  %22 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %23 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %23, i32 %22
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc6

for.inc6:                                         ; preds = %for.body5
  %24 = load i32, i32* %r, align 4, !tbaa !8
  %inc7 = add nsw i32 %24, 1
  store i32 %inc7, i32* %r, align 4, !tbaa !8
  br label %for.cond2

for.end8:                                         ; preds = %for.cond2
  %25 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_4x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 4, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_8x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 8, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_8x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 8, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_16x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 16, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_16x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 16, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_32x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 32, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_32x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 32, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_64x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 64, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_4x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 4, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_16x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 16, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_8x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 8, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_32x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 32, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_16x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 16, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_top_predictor_64x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_top_predictor(i8* %0, i32 %1, i32 64, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_4x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 4, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_8x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 8, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_8x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 8, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_16x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 16, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_16x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 16, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_32x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 32, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_32x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 32, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_64x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 64, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_4x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 4, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_16x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 16, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_8x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 8, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_32x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 32, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_16x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 16, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_top_predictor_64x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_top_predictor(i16* %0, i32 %1, i32 64, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_4x4_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor(i8* %0, i32 %1, i32 4, i32 4, i8* %2, i8* %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @dc_predictor(i8* %dst, i32 %stride, i32 %bw, i32 %bh, i8* %above, i8* %left) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %r = alloca i32, align 4
  %expected_dc = alloca i32, align 4
  %sum = alloca i32, align 4
  %count = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %sum, align 4, !tbaa !8
  %4 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %6 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add = add nsw i32 %5, %6
  store i32 %add, i32* %count, align 4, !tbaa !8
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !8
  %8 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %7, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %11 to i32
  %12 = load i32, i32* %sum, align 4, !tbaa !8
  %add1 = add nsw i32 %12, %conv
  store i32 %add1, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc9, %for.end
  %14 = load i32, i32* %i, align 4, !tbaa !8
  %15 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %14, %15
  br i1 %cmp3, label %for.body5, label %for.end11

for.body5:                                        ; preds = %for.cond2
  %16 = load i8*, i8** %left.addr, align 4, !tbaa !2
  %17 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds i8, i8* %16, i32 %17
  %18 = load i8, i8* %arrayidx6, align 1, !tbaa !10
  %conv7 = zext i8 %18 to i32
  %19 = load i32, i32* %sum, align 4, !tbaa !8
  %add8 = add nsw i32 %19, %conv7
  store i32 %add8, i32* %sum, align 4, !tbaa !8
  br label %for.inc9

for.inc9:                                         ; preds = %for.body5
  %20 = load i32, i32* %i, align 4, !tbaa !8
  %inc10 = add nsw i32 %20, 1
  store i32 %inc10, i32* %i, align 4, !tbaa !8
  br label %for.cond2

for.end11:                                        ; preds = %for.cond2
  %21 = load i32, i32* %sum, align 4, !tbaa !8
  %22 = load i32, i32* %count, align 4, !tbaa !8
  %shr = ashr i32 %22, 1
  %add12 = add nsw i32 %21, %shr
  %23 = load i32, i32* %count, align 4, !tbaa !8
  %div = sdiv i32 %add12, %23
  store i32 %div, i32* %expected_dc, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc17, %for.end11
  %24 = load i32, i32* %r, align 4, !tbaa !8
  %25 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp14 = icmp slt i32 %24, %25
  br i1 %cmp14, label %for.body16, label %for.end19

for.body16:                                       ; preds = %for.cond13
  %26 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %27 = load i32, i32* %expected_dc, align 4, !tbaa !8
  %28 = trunc i32 %27 to i8
  %29 = load i32, i32* %bw.addr, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %26, i8 %28, i32 %29, i1 false)
  %30 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %31 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %31, i32 %30
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  br label %for.inc17

for.inc17:                                        ; preds = %for.body16
  %32 = load i32, i32* %r, align 4, !tbaa !8
  %inc18 = add nsw i32 %32, 1
  store i32 %inc18, i32* %r, align 4, !tbaa !8
  br label %for.cond13

for.end19:                                        ; preds = %for.cond13
  %33 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  %34 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  %35 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  %36 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  %37 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_8x8_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor(i8* %0, i32 %1, i32 8, i32 8, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_16x16_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor(i8* %0, i32 %1, i32 16, i32 16, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_32x32_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor(i8* %0, i32 %1, i32 32, i32 32, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_dc_predictor_64x64_c(i8* %dst, i32 %stride, i8* %above, i8* %left) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i8*, align 4
  %left.addr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8* %above, i8** %above.addr, align 4, !tbaa !2
  store i8* %left, i8** %left.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %above.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %left.addr, align 4, !tbaa !2
  call void @dc_predictor(i8* %0, i32 %1, i32 64, i32 64, i8* %2, i8* %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_4x4_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor(i16* %0, i32 %1, i32 4, i32 4, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @highbd_dc_predictor(i16* %dst, i32 %stride, i32 %bw, i32 %bh, i16* %above, i16* %left, i32 %bd) #1 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %r = alloca i32, align 4
  %expected_dc = alloca i32, align 4
  %sum = alloca i32, align 4
  %count = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !8
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !8
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %sum, align 4, !tbaa !8
  %4 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %6 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %add = add nsw i32 %5, %6
  store i32 %add, i32* %count, align 4, !tbaa !8
  %7 = load i32, i32* %bd.addr, align 4, !tbaa !8
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4, !tbaa !8
  %9 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %10, i32 %11
  %12 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %12 to i32
  %13 = load i32, i32* %sum, align 4, !tbaa !8
  %add1 = add nsw i32 %13, %conv
  store i32 %add1, i32* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc9, %for.end
  %15 = load i32, i32* %i, align 4, !tbaa !8
  %16 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp3 = icmp slt i32 %15, %16
  br i1 %cmp3, label %for.body5, label %for.end11

for.body5:                                        ; preds = %for.cond2
  %17 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds i16, i16* %17, i32 %18
  %19 = load i16, i16* %arrayidx6, align 2, !tbaa !11
  %conv7 = zext i16 %19 to i32
  %20 = load i32, i32* %sum, align 4, !tbaa !8
  %add8 = add nsw i32 %20, %conv7
  store i32 %add8, i32* %sum, align 4, !tbaa !8
  br label %for.inc9

for.inc9:                                         ; preds = %for.body5
  %21 = load i32, i32* %i, align 4, !tbaa !8
  %inc10 = add nsw i32 %21, 1
  store i32 %inc10, i32* %i, align 4, !tbaa !8
  br label %for.cond2

for.end11:                                        ; preds = %for.cond2
  %22 = load i32, i32* %sum, align 4, !tbaa !8
  %23 = load i32, i32* %count, align 4, !tbaa !8
  %shr = ashr i32 %23, 1
  %add12 = add nsw i32 %22, %shr
  %24 = load i32, i32* %count, align 4, !tbaa !8
  %div = sdiv i32 %add12, %24
  store i32 %div, i32* %expected_dc, align 4, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !8
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc17, %for.end11
  %25 = load i32, i32* %r, align 4, !tbaa !8
  %26 = load i32, i32* %bh.addr, align 4, !tbaa !8
  %cmp14 = icmp slt i32 %25, %26
  br i1 %cmp14, label %for.body16, label %for.end19

for.body16:                                       ; preds = %for.cond13
  %27 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %28 = bitcast i16* %27 to i8*
  %29 = load i32, i32* %expected_dc, align 4, !tbaa !8
  %30 = load i32, i32* %bw.addr, align 4, !tbaa !8
  %call = call i8* @aom_memset16(i8* %28, i32 %29, i32 %30)
  %31 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %32 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %32, i32 %31
  store i16* %add.ptr, i16** %dst.addr, align 4, !tbaa !2
  br label %for.inc17

for.inc17:                                        ; preds = %for.body16
  %33 = load i32, i32* %r, align 4, !tbaa !8
  %inc18 = add nsw i32 %33, 1
  store i32 %inc18, i32* %r, align 4, !tbaa !8
  br label %for.cond13

for.end19:                                        ; preds = %for.cond13
  %34 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  %35 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  %36 = bitcast i32* %expected_dc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  %37 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  %38 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_8x8_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor(i16* %0, i32 %1, i32 8, i32 8, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_16x16_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor(i16* %0, i32 %1, i32 16, i32 16, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_32x32_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor(i16* %0, i32 %1, i32 32, i32 32, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_highbd_dc_predictor_64x64_c(i16* %dst, i32 %stride, i16* %above, i16* %left, i32 %bd) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %above.addr = alloca i16*, align 4
  %left.addr = alloca i16*, align 4
  %bd.addr = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i16* %above, i16** %above.addr, align 4, !tbaa !2
  store i16* %left, i16** %left.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !8
  %0 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %1 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %2 = load i16*, i16** %above.addr, align 4, !tbaa !2
  %3 = load i16*, i16** %left.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bd.addr, align 4, !tbaa !8
  call void @highbd_dc_predictor(i16* %0, i32 %1, i32 64, i32 64, i16* %2, i16* %3, i32 %4)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define internal i32 @divide_using_multiply_shift(i32 %num, i32 %shift1, i32 %multiplier, i32 %shift2) #1 {
entry:
  %num.addr = alloca i32, align 4
  %shift1.addr = alloca i32, align 4
  %multiplier.addr = alloca i32, align 4
  %shift2.addr = alloca i32, align 4
  %interm = alloca i32, align 4
  store i32 %num, i32* %num.addr, align 4, !tbaa !8
  store i32 %shift1, i32* %shift1.addr, align 4, !tbaa !8
  store i32 %multiplier, i32* %multiplier.addr, align 4, !tbaa !8
  store i32 %shift2, i32* %shift2.addr, align 4, !tbaa !8
  %0 = bitcast i32* %interm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %num.addr, align 4, !tbaa !8
  %2 = load i32, i32* %shift1.addr, align 4, !tbaa !8
  %shr = ashr i32 %1, %2
  store i32 %shr, i32* %interm, align 4, !tbaa !8
  %3 = load i32, i32* %interm, align 4, !tbaa !8
  %4 = load i32, i32* %multiplier.addr, align 4, !tbaa !8
  %mul = mul nsw i32 %3, %4
  %5 = load i32, i32* %shift2.addr, align 4, !tbaa !8
  %shr1 = ashr i32 %mul, %5
  %6 = bitcast i32* %interm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #5
  ret i32 %shr1
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

declare i8* @aom_memset16(i8*, i32, i32) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @paeth_predictor_single(i16 zeroext %left, i16 zeroext %top, i16 zeroext %top_left) #1 {
entry:
  %left.addr = alloca i16, align 2
  %top.addr = alloca i16, align 2
  %top_left.addr = alloca i16, align 2
  %base = alloca i32, align 4
  %p_left = alloca i32, align 4
  %p_top = alloca i32, align 4
  %p_top_left = alloca i32, align 4
  store i16 %left, i16* %left.addr, align 2, !tbaa !11
  store i16 %top, i16* %top.addr, align 2, !tbaa !11
  store i16 %top_left, i16* %top_left.addr, align 2, !tbaa !11
  %0 = bitcast i32* %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i16, i16* %top.addr, align 2, !tbaa !11
  %conv = zext i16 %1 to i32
  %2 = load i16, i16* %left.addr, align 2, !tbaa !11
  %conv1 = zext i16 %2 to i32
  %add = add nsw i32 %conv, %conv1
  %3 = load i16, i16* %top_left.addr, align 2, !tbaa !11
  %conv2 = zext i16 %3 to i32
  %sub = sub nsw i32 %add, %conv2
  store i32 %sub, i32* %base, align 4, !tbaa !8
  %4 = bitcast i32* %p_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %base, align 4, !tbaa !8
  %6 = load i16, i16* %left.addr, align 2, !tbaa !11
  %conv3 = zext i16 %6 to i32
  %call = call i32 @abs_diff(i32 %5, i32 %conv3)
  store i32 %call, i32* %p_left, align 4, !tbaa !8
  %7 = bitcast i32* %p_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %base, align 4, !tbaa !8
  %9 = load i16, i16* %top.addr, align 2, !tbaa !11
  %conv4 = zext i16 %9 to i32
  %call5 = call i32 @abs_diff(i32 %8, i32 %conv4)
  store i32 %call5, i32* %p_top, align 4, !tbaa !8
  %10 = bitcast i32* %p_top_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load i32, i32* %base, align 4, !tbaa !8
  %12 = load i16, i16* %top_left.addr, align 2, !tbaa !11
  %conv6 = zext i16 %12 to i32
  %call7 = call i32 @abs_diff(i32 %11, i32 %conv6)
  store i32 %call7, i32* %p_top_left, align 4, !tbaa !8
  %13 = load i32, i32* %p_left, align 4, !tbaa !8
  %14 = load i32, i32* %p_top, align 4, !tbaa !8
  %cmp = icmp sle i32 %13, %14
  br i1 %cmp, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %entry
  %15 = load i32, i32* %p_left, align 4, !tbaa !8
  %16 = load i32, i32* %p_top_left, align 4, !tbaa !8
  %cmp9 = icmp sle i32 %15, %16
  br i1 %cmp9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %17 = load i16, i16* %left.addr, align 2, !tbaa !11
  %conv11 = zext i16 %17 to i32
  br label %cond.end18

cond.false:                                       ; preds = %land.lhs.true, %entry
  %18 = load i32, i32* %p_top, align 4, !tbaa !8
  %19 = load i32, i32* %p_top_left, align 4, !tbaa !8
  %cmp12 = icmp sle i32 %18, %19
  br i1 %cmp12, label %cond.true14, label %cond.false16

cond.true14:                                      ; preds = %cond.false
  %20 = load i16, i16* %top.addr, align 2, !tbaa !11
  %conv15 = zext i16 %20 to i32
  br label %cond.end

cond.false16:                                     ; preds = %cond.false
  %21 = load i16, i16* %top_left.addr, align 2, !tbaa !11
  %conv17 = zext i16 %21 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false16, %cond.true14
  %cond = phi i32 [ %conv15, %cond.true14 ], [ %conv17, %cond.false16 ]
  br label %cond.end18

cond.end18:                                       ; preds = %cond.end, %cond.true
  %cond19 = phi i32 [ %conv11, %cond.true ], [ %cond, %cond.end ]
  %conv20 = trunc i32 %cond19 to i16
  %22 = bitcast i32* %p_top_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = bitcast i32* %p_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  %24 = bitcast i32* %p_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  %25 = bitcast i32* %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  ret i16 %conv20
}

; Function Attrs: inlinehint nounwind
define internal i32 @abs_diff(i32 %a, i32 %b) #1 {
entry:
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4, !tbaa !8
  store i32 %b, i32* %b.addr, align 4, !tbaa !8
  %0 = load i32, i32* %a.addr, align 4, !tbaa !8
  %1 = load i32, i32* %b.addr, align 4, !tbaa !8
  %cmp = icmp sgt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %a.addr, align 4, !tbaa !8
  %3 = load i32, i32* %b.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %2, %3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load i32, i32* %b.addr, align 4, !tbaa !8
  %5 = load i32, i32* %a.addr, align 4, !tbaa !8
  %sub1 = sub nsw i32 %4, %5
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ %sub1, %cond.false ]
  ret i32 %cond
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!4, !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"short", !4, i64 0}
