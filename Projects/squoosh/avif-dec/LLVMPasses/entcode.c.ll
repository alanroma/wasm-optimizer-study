; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/entcode.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/entcode.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden i32 @od_ec_tell_frac(i32 %nbits_total, i32 %rng) #0 {
entry:
  %nbits_total.addr = alloca i32, align 4
  %rng.addr = alloca i32, align 4
  %nbits = alloca i32, align 4
  %l = alloca i32, align 4
  %i = alloca i32, align 4
  %b = alloca i32, align 4
  store i32 %nbits_total, i32* %nbits_total.addr, align 4, !tbaa !2
  store i32 %rng, i32* %rng.addr, align 4, !tbaa !2
  %0 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = load i32, i32* %nbits_total.addr, align 4, !tbaa !2
  %shl = shl i32 %3, 3
  store i32 %shl, i32* %nbits, align 4, !tbaa !2
  store i32 0, i32* %l, align 4, !tbaa !2
  store i32 3, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.body, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !2
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %i, align 4, !tbaa !2
  %cmp = icmp sgt i32 %4, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = load i32, i32* %rng.addr, align 4, !tbaa !2
  %7 = load i32, i32* %rng.addr, align 4, !tbaa !2
  %mul = mul i32 %6, %7
  %shr = lshr i32 %mul, 15
  store i32 %shr, i32* %rng.addr, align 4, !tbaa !2
  %8 = load i32, i32* %rng.addr, align 4, !tbaa !2
  %shr1 = lshr i32 %8, 16
  store i32 %shr1, i32* %b, align 4, !tbaa !2
  %9 = load i32, i32* %l, align 4, !tbaa !2
  %shl2 = shl i32 %9, 1
  %10 = load i32, i32* %b, align 4, !tbaa !2
  %or = or i32 %shl2, %10
  store i32 %or, i32* %l, align 4, !tbaa !2
  %11 = load i32, i32* %b, align 4, !tbaa !2
  %12 = load i32, i32* %rng.addr, align 4, !tbaa !2
  %shr3 = lshr i32 %12, %11
  store i32 %shr3, i32* %rng.addr, align 4, !tbaa !2
  %13 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load i32, i32* %nbits, align 4, !tbaa !2
  %15 = load i32, i32* %l, align 4, !tbaa !2
  %sub = sub i32 %14, %15
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #2
  ret i32 %sub
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
