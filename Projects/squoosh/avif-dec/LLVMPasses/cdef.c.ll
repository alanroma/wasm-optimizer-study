; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/cdef.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/cdef.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_mv = type { i32 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.cdef_list = type { i8, i8 }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }

@mi_size_high = internal constant [22 x i8] c"\01\02\01\02\04\02\04\08\04\08\10\08\10 \10 \04\01\08\02\10\04", align 16
@mi_size_wide = internal constant [22 x i8] c"\01\01\02\02\02\04\04\04\08\08\08\10\10\10  \01\04\02\08\04\10", align 16

; Function Attrs: nounwind
define hidden i32 @av1_cdef_compute_sb_list(%struct.CommonModeInfoParams* %mi_params, i32 %mi_row, i32 %mi_col, %struct.cdef_list* %dlist, i8 zeroext %bs) #0 {
entry:
  %mi_params.addr = alloca %struct.CommonModeInfoParams*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %dlist.addr = alloca %struct.cdef_list*, align 4
  %bs.addr = alloca i8, align 1
  %grid = alloca %struct.MB_MODE_INFO**, align 4
  %maxc = alloca i32, align 4
  %maxr = alloca i32, align 4
  %r_step = alloca i32, align 4
  %c_step = alloca i32, align 4
  %r_shift = alloca i32, align 4
  %c_shift = alloca i32, align 4
  %count = alloca i32, align 4
  %r = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %c = alloca i32, align 4
  store %struct.CommonModeInfoParams* %mi_params, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store %struct.cdef_list* %dlist, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  store i8 %bs, i8* %bs.addr, align 1, !tbaa !8
  %0 = bitcast %struct.MB_MODE_INFO*** %grid to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_grid_base = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %1, i32 0, i32 9
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base, align 4, !tbaa !9
  store %struct.MB_MODE_INFO** %2, %struct.MB_MODE_INFO*** %grid, align 4, !tbaa !2
  %3 = bitcast i32* %maxc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %4, i32 0, i32 4
  %5 = load i32, i32* %mi_cols, align 4, !tbaa !11
  %6 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %5, %6
  store i32 %sub, i32* %maxc, align 4, !tbaa !6
  %7 = bitcast i32* %maxr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %8, i32 0, i32 3
  %9 = load i32, i32* %mi_rows, align 4, !tbaa !12
  %10 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 %9, %10
  store i32 %sub1, i32* %maxr, align 4, !tbaa !6
  %11 = load i8, i8* %bs.addr, align 1, !tbaa !8
  %conv = zext i8 %11 to i32
  %cmp = icmp eq i32 %conv, 15
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %12 = load i8, i8* %bs.addr, align 1, !tbaa !8
  %conv3 = zext i8 %12 to i32
  %cmp4 = icmp eq i32 %conv3, 14
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %13 = load i32, i32* %maxc, align 4, !tbaa !6
  %cmp6 = icmp slt i32 %13, 32
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %14 = load i32, i32* %maxc, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.then
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %14, %cond.true ], [ 32, %cond.false ]
  store i32 %cond, i32* %maxc, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  %15 = load i32, i32* %maxc, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, 16
  br i1 %cmp8, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %if.else
  %16 = load i32, i32* %maxc, align 4, !tbaa !6
  br label %cond.end12

cond.false11:                                     ; preds = %if.else
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false11, %cond.true10
  %cond13 = phi i32 [ %16, %cond.true10 ], [ 16, %cond.false11 ]
  store i32 %cond13, i32* %maxc, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %cond.end12, %cond.end
  %17 = load i8, i8* %bs.addr, align 1, !tbaa !8
  %conv14 = zext i8 %17 to i32
  %cmp15 = icmp eq i32 %conv14, 15
  br i1 %cmp15, label %if.then21, label %lor.lhs.false17

lor.lhs.false17:                                  ; preds = %if.end
  %18 = load i8, i8* %bs.addr, align 1, !tbaa !8
  %conv18 = zext i8 %18 to i32
  %cmp19 = icmp eq i32 %conv18, 13
  br i1 %cmp19, label %if.then21, label %if.else28

if.then21:                                        ; preds = %lor.lhs.false17, %if.end
  %19 = load i32, i32* %maxr, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %19, 32
  br i1 %cmp22, label %cond.true24, label %cond.false25

cond.true24:                                      ; preds = %if.then21
  %20 = load i32, i32* %maxr, align 4, !tbaa !6
  br label %cond.end26

cond.false25:                                     ; preds = %if.then21
  br label %cond.end26

cond.end26:                                       ; preds = %cond.false25, %cond.true24
  %cond27 = phi i32 [ %20, %cond.true24 ], [ 32, %cond.false25 ]
  store i32 %cond27, i32* %maxr, align 4, !tbaa !6
  br label %if.end35

if.else28:                                        ; preds = %lor.lhs.false17
  %21 = load i32, i32* %maxr, align 4, !tbaa !6
  %cmp29 = icmp slt i32 %21, 16
  br i1 %cmp29, label %cond.true31, label %cond.false32

cond.true31:                                      ; preds = %if.else28
  %22 = load i32, i32* %maxr, align 4, !tbaa !6
  br label %cond.end33

cond.false32:                                     ; preds = %if.else28
  br label %cond.end33

cond.end33:                                       ; preds = %cond.false32, %cond.true31
  %cond34 = phi i32 [ %22, %cond.true31 ], [ 16, %cond.false32 ]
  store i32 %cond34, i32* %maxr, align 4, !tbaa !6
  br label %if.end35

if.end35:                                         ; preds = %cond.end33, %cond.end26
  %23 = bitcast i32* %r_step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  store i32 2, i32* %r_step, align 4, !tbaa !6
  %24 = bitcast i32* %c_step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  store i32 2, i32* %c_step, align 4, !tbaa !6
  %25 = bitcast i32* %r_shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  store i32 1, i32* %r_shift, align 4, !tbaa !6
  %26 = bitcast i32* %c_shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #5
  store i32 1, i32* %c_shift, align 4, !tbaa !6
  %27 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  store i32 0, i32* %count, align 4, !tbaa !6
  %28 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  store i32 0, i32* %r, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc51, %if.end35
  %29 = load i32, i32* %r, align 4, !tbaa !6
  %30 = load i32, i32* %maxr, align 4, !tbaa !6
  %cmp36 = icmp slt i32 %29, %30
  br i1 %cmp36, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %31 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #5
  br label %for.end53

for.body:                                         ; preds = %for.cond
  %32 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc, %for.body
  %33 = load i32, i32* %c, align 4, !tbaa !6
  %34 = load i32, i32* %maxc, align 4, !tbaa !6
  %cmp39 = icmp slt i32 %33, %34
  br i1 %cmp39, label %for.body42, label %for.cond.cleanup41

for.cond.cleanup41:                               ; preds = %for.cond38
  store i32 5, i32* %cleanup.dest.slot, align 4
  %35 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  br label %for.end

for.body42:                                       ; preds = %for.cond38
  %36 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %grid, align 4, !tbaa !2
  %37 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %38 = load i32, i32* %r, align 4, !tbaa !6
  %add = add nsw i32 %37, %38
  %39 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %40 = load i32, i32* %c, align 4, !tbaa !6
  %add43 = add nsw i32 %39, %40
  %41 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %41, i32 0, i32 11
  %42 = load i32, i32* %mi_stride, align 4, !tbaa !13
  %call = call i32 @is_8x8_block_skip(%struct.MB_MODE_INFO** %36, i32 %add, i32 %add43, i32 %42)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end49, label %if.then44

if.then44:                                        ; preds = %for.body42
  %43 = load i32, i32* %r, align 4, !tbaa !6
  %shr = ashr i32 %43, 1
  %conv45 = trunc i32 %shr to i8
  %44 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %45 = load i32, i32* %count, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %44, i32 %45
  %by = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx, i32 0, i32 0
  store i8 %conv45, i8* %by, align 1, !tbaa !14
  %46 = load i32, i32* %c, align 4, !tbaa !6
  %shr46 = ashr i32 %46, 1
  %conv47 = trunc i32 %shr46 to i8
  %47 = load %struct.cdef_list*, %struct.cdef_list** %dlist.addr, align 4, !tbaa !2
  %48 = load i32, i32* %count, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %47, i32 %48
  %bx = getelementptr inbounds %struct.cdef_list, %struct.cdef_list* %arrayidx48, i32 0, i32 1
  store i8 %conv47, i8* %bx, align 1, !tbaa !16
  %49 = load i32, i32* %count, align 4, !tbaa !6
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %count, align 4, !tbaa !6
  br label %if.end49

if.end49:                                         ; preds = %if.then44, %for.body42
  br label %for.inc

for.inc:                                          ; preds = %if.end49
  %50 = load i32, i32* %c, align 4, !tbaa !6
  %add50 = add nsw i32 %50, 2
  store i32 %add50, i32* %c, align 4, !tbaa !6
  br label %for.cond38

for.end:                                          ; preds = %for.cond.cleanup41
  br label %for.inc51

for.inc51:                                        ; preds = %for.end
  %51 = load i32, i32* %r, align 4, !tbaa !6
  %add52 = add nsw i32 %51, 2
  store i32 %add52, i32* %r, align 4, !tbaa !6
  br label %for.cond

for.end53:                                        ; preds = %for.cond.cleanup
  %52 = load i32, i32* %count, align 4, !tbaa !6
  store i32 1, i32* %cleanup.dest.slot, align 4
  %53 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #5
  %54 = bitcast i32* %c_shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #5
  %55 = bitcast i32* %r_shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  %56 = bitcast i32* %c_step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #5
  %57 = bitcast i32* %r_step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #5
  %58 = bitcast i32* %maxr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #5
  %59 = bitcast i32* %maxc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #5
  %60 = bitcast %struct.MB_MODE_INFO*** %grid to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #5
  ret i32 %52
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @is_8x8_block_skip(%struct.MB_MODE_INFO** %grid, i32 %mi_row, i32 %mi_col, i32 %mi_stride) #0 {
entry:
  %retval = alloca i32, align 4
  %grid.addr = alloca %struct.MB_MODE_INFO**, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %mi_stride.addr = alloca i32, align 4
  %mbmi = alloca %struct.MB_MODE_INFO**, align 4
  %r = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %c = alloca i32, align 4
  store %struct.MB_MODE_INFO** %grid, %struct.MB_MODE_INFO*** %grid.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i32 %mi_stride, i32* %mi_stride.addr, align 4, !tbaa !6
  %0 = bitcast %struct.MB_MODE_INFO*** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %grid.addr, align 4, !tbaa !2
  %2 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %3 = load i32, i32* %mi_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %2, %3
  %add.ptr = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %1, i32 %mul
  %4 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %add.ptr1 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %add.ptr, i32 %4
  store %struct.MB_MODE_INFO** %add.ptr1, %struct.MB_MODE_INFO*** %mbmi, align 4, !tbaa !2
  %5 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  store i32 0, i32* %r, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc9, %entry
  %6 = load i32, i32* %r, align 4, !tbaa !6
  %7 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 3), align 1, !tbaa !8
  %conv = zext i8 %7 to i32
  %cmp = icmp slt i32 %6, %conv
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup12

for.body:                                         ; preds = %for.cond
  %8 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %c, align 4, !tbaa !6
  %10 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 3), align 1, !tbaa !8
  %conv4 = zext i8 %10 to i32
  %cmp5 = icmp slt i32 %9, %conv4
  br i1 %cmp5, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body8:                                        ; preds = %for.cond3
  %11 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mbmi, align 4, !tbaa !2
  %12 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %11, i32 %12
  %13 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  %skip = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %13, i32 0, i32 14
  %14 = load i8, i8* %skip, align 4, !tbaa !17
  %tobool = icmp ne i8 %14, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %15 = load i32, i32* %c, align 4, !tbaa !6
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %c, align 4, !tbaa !6
  br label %for.cond3

cleanup:                                          ; preds = %if.then, %for.cond.cleanup7
  %16 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup12 [
    i32 5, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  br label %for.inc9

for.inc9:                                         ; preds = %for.end
  %17 = load i32, i32* %r, align 4, !tbaa !6
  %inc10 = add nsw i32 %17, 1
  store i32 %inc10, i32* %r, align 4, !tbaa !6
  %18 = load i32, i32* %mi_stride.addr, align 4, !tbaa !6
  %19 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mbmi, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %19, i32 %18
  store %struct.MB_MODE_INFO** %add.ptr11, %struct.MB_MODE_INFO*** %mbmi, align 4, !tbaa !2
  br label %for.cond

cleanup12:                                        ; preds = %cleanup, %for.cond.cleanup
  %20 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %cleanup.dest13 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest13, label %cleanup15 [
    i32 2, label %for.end14
  ]

for.end14:                                        ; preds = %cleanup12
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup15

cleanup15:                                        ; preds = %for.end14, %cleanup12
  %21 = bitcast %struct.MB_MODE_INFO*** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @cdef_copy_rect8_8bit_to_16bit_c(i16* %dst, i32 %dstride, i8* %src, i32 %sstride, i32 %v, i32 %h) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %dstride.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %sstride.addr = alloca i32, align 4
  %v.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dstride, i32* %dstride.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %sstride, i32* %sstride.addr, align 4, !tbaa !6
  store i32 %v, i32* %v.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc8, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %v.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end10

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %j, align 4, !tbaa !6
  %6 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = load i32, i32* %sstride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %9, %10
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul, %11
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %add
  %12 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %12 to i16
  %13 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 %14, %15
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %add6 = add nsw i32 %mul5, %16
  %arrayidx7 = getelementptr inbounds i16, i16* %13, i32 %add6
  store i16 %conv, i16* %arrayidx7, align 2, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc8

for.inc8:                                         ; preds = %for.end
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %inc9 = add nsw i32 %18, 1
  store i32 %inc9, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end10:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @cdef_copy_rect8_16bit_to_16bit_c(i16* %dst, i32 %dstride, i16* %src, i32 %sstride, i32 %v, i32 %h) #0 {
entry:
  %dst.addr = alloca i16*, align 4
  %dstride.addr = alloca i32, align 4
  %src.addr = alloca i16*, align 4
  %sstride.addr = alloca i32, align 4
  %v.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dstride, i32* %dstride.addr, align 4, !tbaa !6
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %sstride, i32* %sstride.addr, align 4, !tbaa !6
  store i32 %v, i32* %v.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc8, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %v.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end10

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %j, align 4, !tbaa !6
  %6 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = load i32, i32* %sstride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %9, %10
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul, %11
  %arrayidx = getelementptr inbounds i16, i16* %8, i32 %add
  %12 = load i16, i16* %arrayidx, align 2, !tbaa !23
  %13 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 %14, %15
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %add6 = add nsw i32 %mul5, %16
  %arrayidx7 = getelementptr inbounds i16, i16* %13, i32 %add6
  store i16 %12, i16* %arrayidx7, align 2, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc8

for.inc8:                                         ; preds = %for.end
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %inc9 = add nsw i32 %18, 1
  store i32 %inc9, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end10:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_cdef_frame(%struct.yv12_buffer_config* %frame, %struct.AV1Common* %cm, %struct.macroblockd* %xd) #0 {
entry:
  %frame.addr = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %cdef_info = alloca %struct.CdefInfo*, align 4
  %mi_params = alloca %struct.CommonModeInfoParams*, align 4
  %num_planes = alloca i32, align 4
  %src = alloca [19296 x i16], align 16
  %linebuf = alloca [3 x i16*], align 4
  %colbuf = alloca [3 x i16*], align 4
  %dlist = alloca [256 x %struct.cdef_list], align 16
  %row_cdef = alloca i8*, align 4
  %prev_row_cdef = alloca i8*, align 4
  %curr_row_cdef = alloca i8*, align 4
  %cdef_count = alloca i32, align 4
  %dir = alloca [16 x [16 x i32]], align 16
  %var = alloca [16 x [16 x i32]], align 16
  %mi_wide_l2 = alloca [3 x i32], align 4
  %mi_high_l2 = alloca [3 x i32], align 4
  %xdec = alloca [3 x i32], align 4
  %ydec = alloca [3 x i32], align 4
  %coeff_shift = alloca i32, align 4
  %nvfb = alloca i32, align 4
  %nhfb = alloca i32, align 4
  %pli = alloca i32, align 4
  %stride = alloca i32, align 4
  %pli37 = alloca i32, align 4
  %fbr = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pli59 = alloca i32, align 4
  %block_height = alloca i32, align 4
  %cdef_left = alloca i32, align 4
  %fbc = alloca i32, align 4
  %level = alloca i32, align 4
  %sec_strength = alloca i32, align 4
  %uv_level = alloca i32, align 4
  %uv_sec_strength = alloca i32, align 4
  %nhb = alloca i32, align 4
  %nvb = alloca i32, align 4
  %cstart = alloca i32, align 4
  %frame_top = alloca i32, align 4
  %frame_left = alloca i32, align 4
  %frame_bottom = alloca i32, align 4
  %frame_right = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %mbmi_cdef_strength = alloca i32, align 4
  %pli193 = alloca i32, align 4
  %coffset = alloca i32, align 4
  %rend = alloca i32, align 4
  %cend = alloca i32, align 4
  %damping = alloca i32, align 4
  %hsize = alloca i32, align 4
  %vsize = alloca i32, align 4
  %tmp = alloca i8*, align 4
  %pli486 = alloca i32, align 4
  store %struct.yv12_buffer_config* %frame, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast %struct.CdefInfo** %cdef_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cdef_info1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 33
  store %struct.CdefInfo* %cdef_info1, %struct.CdefInfo** %cdef_info, align 4, !tbaa !2
  %2 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 22
  store %struct.CommonModeInfoParams* %mi_params2, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %4 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %5)
  store i32 %call, i32* %num_planes, align 4, !tbaa !6
  %6 = bitcast [19296 x i16]* %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 38592, i8* %6) #5
  %7 = bitcast [3 x i16*]* %linebuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %7) #5
  %8 = bitcast [3 x i16*]* %colbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %8) #5
  %9 = bitcast [256 x %struct.cdef_list]* %dlist to i8*
  call void @llvm.lifetime.start.p0i8(i64 512, i8* %9) #5
  %10 = bitcast i8** %row_cdef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = bitcast i8** %prev_row_cdef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = bitcast i8** %curr_row_cdef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = bitcast i32* %cdef_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = bitcast [16 x [16 x i32]]* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %14) #5
  %15 = bitcast [16 x [16 x i32]]* %dir to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %15, i8 0, i32 1024, i1 false)
  %16 = bitcast [16 x [16 x i32]]* %var to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %16) #5
  %17 = bitcast [16 x [16 x i32]]* %var to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %17, i8 0, i32 1024, i1 false)
  %18 = bitcast [3 x i32]* %mi_wide_l2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %18) #5
  %19 = bitcast [3 x i32]* %mi_high_l2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %19) #5
  %20 = bitcast [3 x i32]* %xdec to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %20) #5
  %21 = bitcast [3 x i32]* %ydec to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %21) #5
  %22 = bitcast i32* %coeff_shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %23, i32 0, i32 37
  %bit_depth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 25
  %24 = load i32, i32* %bit_depth, align 8, !tbaa !24
  %sub = sub i32 %24, 8
  %cmp = icmp ugt i32 %sub, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %25 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params3 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %25, i32 0, i32 37
  %bit_depth4 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params3, i32 0, i32 25
  %26 = load i32, i32* %bit_depth4, align 8, !tbaa !24
  %sub5 = sub i32 %26, 8
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub5, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %coeff_shift, align 4, !tbaa !6
  %27 = bitcast i32* %nvfb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  %28 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %28, i32 0, i32 3
  %29 = load i32, i32* %mi_rows, align 4, !tbaa !12
  %add = add nsw i32 %29, 16
  %sub6 = sub nsw i32 %add, 1
  %div = sdiv i32 %sub6, 16
  store i32 %div, i32* %nvfb, align 4, !tbaa !6
  %30 = bitcast i32* %nhfb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #5
  %31 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %31, i32 0, i32 4
  %32 = load i32, i32* %mi_cols, align 4, !tbaa !11
  %add7 = add nsw i32 %32, 16
  %sub8 = sub nsw i32 %add7, 1
  %div9 = sdiv i32 %sub8, 16
  store i32 %div9, i32* %nhfb, align 4, !tbaa !6
  %33 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %33, i32 0, i32 4
  %arraydecay = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 0
  %34 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params10 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %34, i32 0, i32 37
  %sb_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params10, i32 0, i32 7
  %35 = load i8, i8* %sb_size, align 4, !tbaa !46
  %36 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %frame.addr, align 4, !tbaa !2
  %37 = load i32, i32* %num_planes, align 4, !tbaa !6
  call void @av1_setup_dst_planes(%struct.macroblockd_plane* %arraydecay, i8 zeroext %35, %struct.yv12_buffer_config* %36, i32 0, i32 0, i32 0, i32 %37)
  %38 = load i32, i32* %nhfb, align 4, !tbaa !6
  %add11 = add nsw i32 %38, 2
  %mul = mul i32 1, %add11
  %mul12 = mul i32 %mul, 2
  %call13 = call i8* @aom_malloc(i32 %mul12)
  store i8* %call13, i8** %row_cdef, align 4, !tbaa !2
  %39 = load i8*, i8** %row_cdef, align 4, !tbaa !2
  %40 = load i32, i32* %nhfb, align 4, !tbaa !6
  %add14 = add nsw i32 %40, 2
  %mul15 = mul i32 1, %add14
  %mul16 = mul i32 %mul15, 2
  call void @llvm.memset.p0i8.i32(i8* align 1 %39, i8 1, i32 %mul16, i1 false)
  %41 = load i8*, i8** %row_cdef, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %41, i32 1
  store i8* %add.ptr, i8** %prev_row_cdef, align 4, !tbaa !2
  %42 = load i8*, i8** %prev_row_cdef, align 4, !tbaa !2
  %43 = load i32, i32* %nhfb, align 4, !tbaa !6
  %add.ptr17 = getelementptr inbounds i8, i8* %42, i32 %43
  %add.ptr18 = getelementptr inbounds i8, i8* %add.ptr17, i32 2
  store i8* %add.ptr18, i8** %curr_row_cdef, align 4, !tbaa !2
  %44 = bitcast i32* %pli to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  store i32 0, i32* %pli, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %45 = load i32, i32* %pli, align 4, !tbaa !6
  %46 = load i32, i32* %num_planes, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %45, %46
  br i1 %cmp19, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %47 = bitcast i32* %pli to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane20 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %48, i32 0, i32 4
  %49 = load i32, i32* %pli, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane20, i32 0, i32 %49
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx, i32 0, i32 4
  %50 = load i32, i32* %subsampling_x, align 4, !tbaa !47
  %51 = load i32, i32* %pli, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [3 x i32], [3 x i32]* %xdec, i32 0, i32 %51
  store i32 %50, i32* %arrayidx21, align 4, !tbaa !6
  %52 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane22 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %52, i32 0, i32 4
  %53 = load i32, i32* %pli, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane22, i32 0, i32 %53
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx23, i32 0, i32 5
  %54 = load i32, i32* %subsampling_y, align 4, !tbaa !50
  %55 = load i32, i32* %pli, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds [3 x i32], [3 x i32]* %ydec, i32 0, i32 %55
  store i32 %54, i32* %arrayidx24, align 4, !tbaa !6
  %56 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane25 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %56, i32 0, i32 4
  %57 = load i32, i32* %pli, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane25, i32 0, i32 %57
  %subsampling_x27 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx26, i32 0, i32 4
  %58 = load i32, i32* %subsampling_x27, align 4, !tbaa !47
  %sub28 = sub nsw i32 2, %58
  %59 = load i32, i32* %pli, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_wide_l2, i32 0, i32 %59
  store i32 %sub28, i32* %arrayidx29, align 4, !tbaa !6
  %60 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane30 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %60, i32 0, i32 4
  %61 = load i32, i32* %pli, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane30, i32 0, i32 %61
  %subsampling_y32 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx31, i32 0, i32 5
  %62 = load i32, i32* %subsampling_y32, align 4, !tbaa !50
  %sub33 = sub nsw i32 2, %62
  %63 = load i32, i32* %pli, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %63
  store i32 %sub33, i32* %arrayidx34, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %64 = load i32, i32* %pli, align 4, !tbaa !6
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %pli, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %65 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #5
  %66 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_cols35 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %66, i32 0, i32 4
  %67 = load i32, i32* %mi_cols35, align 4, !tbaa !11
  %shl = shl i32 %67, 2
  %add36 = add nsw i32 %shl, 16
  store i32 %add36, i32* %stride, align 4, !tbaa !6
  %68 = bitcast i32* %pli37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #5
  store i32 0, i32* %pli37, align 4, !tbaa !6
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc52, %for.end
  %69 = load i32, i32* %pli37, align 4, !tbaa !6
  %70 = load i32, i32* %num_planes, align 4, !tbaa !6
  %cmp39 = icmp slt i32 %69, %70
  br i1 %cmp39, label %for.body41, label %for.cond.cleanup40

for.cond.cleanup40:                               ; preds = %for.cond38
  %71 = bitcast i32* %pli37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #5
  br label %for.end54

for.body41:                                       ; preds = %for.cond38
  %72 = load i32, i32* %stride, align 4, !tbaa !6
  %mul42 = mul i32 12, %72
  %call43 = call i8* @aom_malloc(i32 %mul42)
  %73 = bitcast i8* %call43 to i16*
  %74 = load i32, i32* %pli37, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds [3 x i16*], [3 x i16*]* %linebuf, i32 0, i32 %74
  store i16* %73, i16** %arrayidx44, align 4, !tbaa !2
  %75 = load i32, i32* %pli37, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %75
  %76 = load i32, i32* %arrayidx45, align 4, !tbaa !6
  %shl46 = shl i32 64, %76
  %add47 = add nsw i32 %shl46, 6
  %mul48 = mul i32 4, %add47
  %mul49 = mul i32 %mul48, 8
  %call50 = call i8* @aom_malloc(i32 %mul49)
  %77 = bitcast i8* %call50 to i16*
  %78 = load i32, i32* %pli37, align 4, !tbaa !6
  %arrayidx51 = getelementptr inbounds [3 x i16*], [3 x i16*]* %colbuf, i32 0, i32 %78
  store i16* %77, i16** %arrayidx51, align 4, !tbaa !2
  br label %for.inc52

for.inc52:                                        ; preds = %for.body41
  %79 = load i32, i32* %pli37, align 4, !tbaa !6
  %inc53 = add nsw i32 %79, 1
  store i32 %inc53, i32* %pli37, align 4, !tbaa !6
  br label %for.cond38

for.end54:                                        ; preds = %for.cond.cleanup40
  %80 = bitcast i32* %fbr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #5
  store i32 0, i32* %fbr, align 4, !tbaa !6
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc482, %for.end54
  %81 = load i32, i32* %fbr, align 4, !tbaa !6
  %82 = load i32, i32* %nvfb, align 4, !tbaa !6
  %cmp56 = icmp slt i32 %81, %82
  br i1 %cmp56, label %for.body58, label %for.cond.cleanup57

for.cond.cleanup57:                               ; preds = %for.cond55
  store i32 8, i32* %cleanup.dest.slot, align 4
  %83 = bitcast i32* %fbr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #5
  br label %for.end485

for.body58:                                       ; preds = %for.cond55
  %84 = bitcast i32* %pli59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #5
  store i32 0, i32* %pli59, align 4, !tbaa !6
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc68, %for.body58
  %85 = load i32, i32* %pli59, align 4, !tbaa !6
  %86 = load i32, i32* %num_planes, align 4, !tbaa !6
  %cmp61 = icmp slt i32 %85, %86
  br i1 %cmp61, label %for.body63, label %for.cond.cleanup62

for.cond.cleanup62:                               ; preds = %for.cond60
  store i32 11, i32* %cleanup.dest.slot, align 4
  %87 = bitcast i32* %pli59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #5
  br label %for.end70

for.body63:                                       ; preds = %for.cond60
  %88 = bitcast i32* %block_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #5
  %89 = load i32, i32* %pli59, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %89
  %90 = load i32, i32* %arrayidx64, align 4, !tbaa !6
  %shl65 = shl i32 16, %90
  %add66 = add nsw i32 %shl65, 6
  store i32 %add66, i32* %block_height, align 4, !tbaa !6
  %91 = load i32, i32* %pli59, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds [3 x i16*], [3 x i16*]* %colbuf, i32 0, i32 %91
  %92 = load i16*, i16** %arrayidx67, align 4, !tbaa !2
  %93 = load i32, i32* %block_height, align 4, !tbaa !6
  call void @fill_rect(i16* %92, i32 8, i32 %93, i32 8, i16 zeroext 30000)
  %94 = bitcast i32* %block_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  br label %for.inc68

for.inc68:                                        ; preds = %for.body63
  %95 = load i32, i32* %pli59, align 4, !tbaa !6
  %inc69 = add nsw i32 %95, 1
  store i32 %inc69, i32* %pli59, align 4, !tbaa !6
  br label %for.cond60

for.end70:                                        ; preds = %for.cond.cleanup62
  %96 = bitcast i32* %cdef_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #5
  store i32 1, i32* %cdef_left, align 4, !tbaa !6
  %97 = bitcast i32* %fbc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #5
  store i32 0, i32* %fbc, align 4, !tbaa !6
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc478, %for.end70
  %98 = load i32, i32* %fbc, align 4, !tbaa !6
  %99 = load i32, i32* %nhfb, align 4, !tbaa !6
  %cmp72 = icmp slt i32 %98, %99
  br i1 %cmp72, label %for.body74, label %for.cond.cleanup73

for.cond.cleanup73:                               ; preds = %for.cond71
  store i32 14, i32* %cleanup.dest.slot, align 4
  %100 = bitcast i32* %fbc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #5
  br label %for.end481

for.body74:                                       ; preds = %for.cond71
  %101 = bitcast i32* %level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #5
  %102 = bitcast i32* %sec_strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #5
  %103 = bitcast i32* %uv_level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #5
  %104 = bitcast i32* %uv_sec_strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #5
  %105 = bitcast i32* %nhb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #5
  %106 = bitcast i32* %nvb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #5
  %107 = bitcast i32* %cstart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #5
  store i32 0, i32* %cstart, align 4, !tbaa !6
  %108 = load i8*, i8** %curr_row_cdef, align 4, !tbaa !2
  %109 = load i32, i32* %fbc, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds i8, i8* %108, i32 %109
  store i8 0, i8* %arrayidx75, align 1, !tbaa !8
  %110 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_grid_base = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %110, i32 0, i32 9
  %111 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base, align 4, !tbaa !9
  %112 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul76 = mul nsw i32 16, %112
  %113 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %113, i32 0, i32 11
  %114 = load i32, i32* %mi_stride, align 4, !tbaa !13
  %mul77 = mul nsw i32 %mul76, %114
  %115 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul78 = mul nsw i32 16, %115
  %add79 = add nsw i32 %mul77, %mul78
  %arrayidx80 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %111, i32 %add79
  %116 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx80, align 4, !tbaa !2
  %cmp81 = icmp eq %struct.MB_MODE_INFO* %116, null
  br i1 %cmp81, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body74
  %117 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_grid_base82 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %117, i32 0, i32 9
  %118 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base82, align 4, !tbaa !9
  %119 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul83 = mul nsw i32 16, %119
  %120 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_stride84 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %120, i32 0, i32 11
  %121 = load i32, i32* %mi_stride84, align 4, !tbaa !13
  %mul85 = mul nsw i32 %mul83, %121
  %122 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul86 = mul nsw i32 16, %122
  %add87 = add nsw i32 %mul85, %mul86
  %arrayidx88 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %118, i32 %add87
  %123 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx88, align 4, !tbaa !2
  %cdef_strength = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %123, i32 0, i32 27
  %bf.load = load i8, i8* %cdef_strength, align 1
  %bf.shl = shl i8 %bf.load, 1
  %bf.ashr = ashr i8 %bf.shl, 4
  %conv = sext i8 %bf.ashr to i32
  %cmp89 = icmp eq i32 %conv, -1
  br i1 %cmp89, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body74
  store i32 0, i32* %cdef_left, align 4, !tbaa !6
  store i32 16, i32* %cleanup.dest.slot, align 4
  br label %cleanup471

if.end:                                           ; preds = %lor.lhs.false
  %124 = load i32, i32* %cdef_left, align 4, !tbaa !6
  %tobool = icmp ne i32 %124, 0
  br i1 %tobool, label %if.end92, label %if.then91

if.then91:                                        ; preds = %if.end
  store i32 -8, i32* %cstart, align 4, !tbaa !6
  br label %if.end92

if.end92:                                         ; preds = %if.then91, %if.end
  %125 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_cols93 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %125, i32 0, i32 4
  %126 = load i32, i32* %mi_cols93, align 4, !tbaa !11
  %127 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul94 = mul nsw i32 16, %127
  %sub95 = sub nsw i32 %126, %mul94
  %cmp96 = icmp slt i32 16, %sub95
  br i1 %cmp96, label %cond.true98, label %cond.false99

cond.true98:                                      ; preds = %if.end92
  br label %cond.end103

cond.false99:                                     ; preds = %if.end92
  %128 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_cols100 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %128, i32 0, i32 4
  %129 = load i32, i32* %mi_cols100, align 4, !tbaa !11
  %130 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul101 = mul nsw i32 16, %130
  %sub102 = sub nsw i32 %129, %mul101
  br label %cond.end103

cond.end103:                                      ; preds = %cond.false99, %cond.true98
  %cond104 = phi i32 [ 16, %cond.true98 ], [ %sub102, %cond.false99 ]
  store i32 %cond104, i32* %nhb, align 4, !tbaa !6
  %131 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_rows105 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %131, i32 0, i32 3
  %132 = load i32, i32* %mi_rows105, align 4, !tbaa !12
  %133 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul106 = mul nsw i32 16, %133
  %sub107 = sub nsw i32 %132, %mul106
  %cmp108 = icmp slt i32 16, %sub107
  br i1 %cmp108, label %cond.true110, label %cond.false111

cond.true110:                                     ; preds = %cond.end103
  br label %cond.end115

cond.false111:                                    ; preds = %cond.end103
  %134 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_rows112 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %134, i32 0, i32 3
  %135 = load i32, i32* %mi_rows112, align 4, !tbaa !12
  %136 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul113 = mul nsw i32 16, %136
  %sub114 = sub nsw i32 %135, %mul113
  br label %cond.end115

cond.end115:                                      ; preds = %cond.false111, %cond.true110
  %cond116 = phi i32 [ 16, %cond.true110 ], [ %sub114, %cond.false111 ]
  store i32 %cond116, i32* %nvb, align 4, !tbaa !6
  %137 = bitcast i32* %frame_top to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #5
  %138 = bitcast i32* %frame_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #5
  %139 = bitcast i32* %frame_bottom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #5
  %140 = bitcast i32* %frame_right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #5
  %141 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %141) #5
  %142 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul117 = mul nsw i32 16, %142
  store i32 %mul117, i32* %mi_row, align 4, !tbaa !6
  %143 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %143) #5
  %144 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul118 = mul nsw i32 16, %144
  store i32 %mul118, i32* %mi_col, align 4, !tbaa !6
  %145 = load i32, i32* %mi_row, align 4, !tbaa !6
  %cmp119 = icmp eq i32 %145, 0
  %146 = zext i1 %cmp119 to i64
  %cond121 = select i1 %cmp119, i32 1, i32 0
  store i32 %cond121, i32* %frame_top, align 4, !tbaa !6
  %147 = load i32, i32* %mi_col, align 4, !tbaa !6
  %cmp122 = icmp eq i32 %147, 0
  %148 = zext i1 %cmp122 to i64
  %cond124 = select i1 %cmp122, i32 1, i32 0
  store i32 %cond124, i32* %frame_left, align 4, !tbaa !6
  %149 = load i32, i32* %fbr, align 4, !tbaa !6
  %150 = load i32, i32* %nvfb, align 4, !tbaa !6
  %sub125 = sub nsw i32 %150, 1
  %cmp126 = icmp ne i32 %149, %sub125
  br i1 %cmp126, label %if.then128, label %if.else

if.then128:                                       ; preds = %cond.end115
  %151 = load i32, i32* %mi_row, align 4, !tbaa !6
  %add129 = add nsw i32 %151, 16
  %152 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_rows130 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %152, i32 0, i32 3
  %153 = load i32, i32* %mi_rows130, align 4, !tbaa !12
  %cmp131 = icmp eq i32 %add129, %153
  %154 = zext i1 %cmp131 to i64
  %cond133 = select i1 %cmp131, i32 1, i32 0
  store i32 %cond133, i32* %frame_bottom, align 4, !tbaa !6
  br label %if.end134

if.else:                                          ; preds = %cond.end115
  store i32 1, i32* %frame_bottom, align 4, !tbaa !6
  br label %if.end134

if.end134:                                        ; preds = %if.else, %if.then128
  %155 = load i32, i32* %fbc, align 4, !tbaa !6
  %156 = load i32, i32* %nhfb, align 4, !tbaa !6
  %sub135 = sub nsw i32 %156, 1
  %cmp136 = icmp ne i32 %155, %sub135
  br i1 %cmp136, label %if.then138, label %if.else144

if.then138:                                       ; preds = %if.end134
  %157 = load i32, i32* %mi_col, align 4, !tbaa !6
  %add139 = add nsw i32 %157, 16
  %158 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_cols140 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %158, i32 0, i32 4
  %159 = load i32, i32* %mi_cols140, align 4, !tbaa !11
  %cmp141 = icmp eq i32 %add139, %159
  %160 = zext i1 %cmp141 to i64
  %cond143 = select i1 %cmp141, i32 1, i32 0
  store i32 %cond143, i32* %frame_right, align 4, !tbaa !6
  br label %if.end145

if.else144:                                       ; preds = %if.end134
  store i32 1, i32* %frame_right, align 4, !tbaa !6
  br label %if.end145

if.end145:                                        ; preds = %if.else144, %if.then138
  %161 = bitcast i32* %mbmi_cdef_strength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %161) #5
  %162 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_grid_base146 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %162, i32 0, i32 9
  %163 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base146, align 4, !tbaa !9
  %164 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul147 = mul nsw i32 16, %164
  %165 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %mi_stride148 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %165, i32 0, i32 11
  %166 = load i32, i32* %mi_stride148, align 4, !tbaa !13
  %mul149 = mul nsw i32 %mul147, %166
  %167 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul150 = mul nsw i32 16, %167
  %add151 = add nsw i32 %mul149, %mul150
  %arrayidx152 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %163, i32 %add151
  %168 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx152, align 4, !tbaa !2
  %cdef_strength153 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %168, i32 0, i32 27
  %bf.load154 = load i8, i8* %cdef_strength153, align 1
  %bf.shl155 = shl i8 %bf.load154, 1
  %bf.ashr156 = ashr i8 %bf.shl155, 4
  %conv157 = sext i8 %bf.ashr156 to i32
  store i32 %conv157, i32* %mbmi_cdef_strength, align 4, !tbaa !6
  %169 = load %struct.CdefInfo*, %struct.CdefInfo** %cdef_info, align 4, !tbaa !2
  %cdef_strengths = getelementptr inbounds %struct.CdefInfo, %struct.CdefInfo* %169, i32 0, i32 2
  %170 = load i32, i32* %mbmi_cdef_strength, align 4, !tbaa !6
  %arrayidx158 = getelementptr inbounds [16 x i32], [16 x i32]* %cdef_strengths, i32 0, i32 %170
  %171 = load i32, i32* %arrayidx158, align 4, !tbaa !6
  %div159 = sdiv i32 %171, 4
  store i32 %div159, i32* %level, align 4, !tbaa !6
  %172 = load %struct.CdefInfo*, %struct.CdefInfo** %cdef_info, align 4, !tbaa !2
  %cdef_strengths160 = getelementptr inbounds %struct.CdefInfo, %struct.CdefInfo* %172, i32 0, i32 2
  %173 = load i32, i32* %mbmi_cdef_strength, align 4, !tbaa !6
  %arrayidx161 = getelementptr inbounds [16 x i32], [16 x i32]* %cdef_strengths160, i32 0, i32 %173
  %174 = load i32, i32* %arrayidx161, align 4, !tbaa !6
  %rem = srem i32 %174, 4
  store i32 %rem, i32* %sec_strength, align 4, !tbaa !6
  %175 = load i32, i32* %sec_strength, align 4, !tbaa !6
  %cmp162 = icmp eq i32 %175, 3
  %conv163 = zext i1 %cmp162 to i32
  %176 = load i32, i32* %sec_strength, align 4, !tbaa !6
  %add164 = add nsw i32 %176, %conv163
  store i32 %add164, i32* %sec_strength, align 4, !tbaa !6
  %177 = load %struct.CdefInfo*, %struct.CdefInfo** %cdef_info, align 4, !tbaa !2
  %cdef_uv_strengths = getelementptr inbounds %struct.CdefInfo, %struct.CdefInfo* %177, i32 0, i32 3
  %178 = load i32, i32* %mbmi_cdef_strength, align 4, !tbaa !6
  %arrayidx165 = getelementptr inbounds [16 x i32], [16 x i32]* %cdef_uv_strengths, i32 0, i32 %178
  %179 = load i32, i32* %arrayidx165, align 4, !tbaa !6
  %div166 = sdiv i32 %179, 4
  store i32 %div166, i32* %uv_level, align 4, !tbaa !6
  %180 = load %struct.CdefInfo*, %struct.CdefInfo** %cdef_info, align 4, !tbaa !2
  %cdef_uv_strengths167 = getelementptr inbounds %struct.CdefInfo, %struct.CdefInfo* %180, i32 0, i32 3
  %181 = load i32, i32* %mbmi_cdef_strength, align 4, !tbaa !6
  %arrayidx168 = getelementptr inbounds [16 x i32], [16 x i32]* %cdef_uv_strengths167, i32 0, i32 %181
  %182 = load i32, i32* %arrayidx168, align 4, !tbaa !6
  %rem169 = srem i32 %182, 4
  store i32 %rem169, i32* %uv_sec_strength, align 4, !tbaa !6
  %183 = load i32, i32* %uv_sec_strength, align 4, !tbaa !6
  %cmp170 = icmp eq i32 %183, 3
  %conv171 = zext i1 %cmp170 to i32
  %184 = load i32, i32* %uv_sec_strength, align 4, !tbaa !6
  %add172 = add nsw i32 %184, %conv171
  store i32 %add172, i32* %uv_sec_strength, align 4, !tbaa !6
  %185 = load i32, i32* %level, align 4, !tbaa !6
  %cmp173 = icmp eq i32 %185, 0
  br i1 %cmp173, label %land.lhs.true, label %lor.lhs.false183

land.lhs.true:                                    ; preds = %if.end145
  %186 = load i32, i32* %sec_strength, align 4, !tbaa !6
  %cmp175 = icmp eq i32 %186, 0
  br i1 %cmp175, label %land.lhs.true177, label %lor.lhs.false183

land.lhs.true177:                                 ; preds = %land.lhs.true
  %187 = load i32, i32* %uv_level, align 4, !tbaa !6
  %cmp178 = icmp eq i32 %187, 0
  br i1 %cmp178, label %land.lhs.true180, label %lor.lhs.false183

land.lhs.true180:                                 ; preds = %land.lhs.true177
  %188 = load i32, i32* %uv_sec_strength, align 4, !tbaa !6
  %cmp181 = icmp eq i32 %188, 0
  br i1 %cmp181, label %if.then190, label %lor.lhs.false183

lor.lhs.false183:                                 ; preds = %land.lhs.true180, %land.lhs.true177, %land.lhs.true, %if.end145
  %189 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !2
  %190 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul184 = mul nsw i32 %190, 16
  %191 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul185 = mul nsw i32 %191, 16
  %arraydecay186 = getelementptr inbounds [256 x %struct.cdef_list], [256 x %struct.cdef_list]* %dlist, i32 0, i32 0
  %call187 = call i32 @av1_cdef_compute_sb_list(%struct.CommonModeInfoParams* %189, i32 %mul184, i32 %mul185, %struct.cdef_list* %arraydecay186, i8 zeroext 12)
  store i32 %call187, i32* %cdef_count, align 4, !tbaa !6
  %cmp188 = icmp eq i32 %call187, 0
  br i1 %cmp188, label %if.then190, label %if.end191

if.then190:                                       ; preds = %lor.lhs.false183, %land.lhs.true180
  store i32 0, i32* %cdef_left, align 4, !tbaa !6
  store i32 16, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end191:                                        ; preds = %lor.lhs.false183
  %192 = load i8*, i8** %curr_row_cdef, align 4, !tbaa !2
  %193 = load i32, i32* %fbc, align 4, !tbaa !6
  %arrayidx192 = getelementptr inbounds i8, i8* %192, i32 %193
  store i8 1, i8* %arrayidx192, align 1, !tbaa !8
  %194 = bitcast i32* %pli193 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %194) #5
  store i32 0, i32* %pli193, align 4, !tbaa !6
  br label %for.cond194

for.cond194:                                      ; preds = %for.inc462, %if.end191
  %195 = load i32, i32* %pli193, align 4, !tbaa !6
  %196 = load i32, i32* %num_planes, align 4, !tbaa !6
  %cmp195 = icmp slt i32 %195, %196
  br i1 %cmp195, label %for.body198, label %for.cond.cleanup197

for.cond.cleanup197:                              ; preds = %for.cond194
  store i32 17, i32* %cleanup.dest.slot, align 4
  %197 = bitcast i32* %pli193 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #5
  br label %for.end464

for.body198:                                      ; preds = %for.cond194
  %198 = bitcast i32* %coffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #5
  %199 = bitcast i32* %rend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %199) #5
  %200 = bitcast i32* %cend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #5
  %201 = bitcast i32* %damping to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %201) #5
  %202 = load %struct.CdefInfo*, %struct.CdefInfo** %cdef_info, align 4, !tbaa !2
  %cdef_damping = getelementptr inbounds %struct.CdefInfo, %struct.CdefInfo* %202, i32 0, i32 0
  %203 = load i32, i32* %cdef_damping, align 4, !tbaa !51
  store i32 %203, i32* %damping, align 4, !tbaa !6
  %204 = bitcast i32* %hsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %204) #5
  %205 = load i32, i32* %nhb, align 4, !tbaa !6
  %206 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx199 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_wide_l2, i32 0, i32 %206
  %207 = load i32, i32* %arrayidx199, align 4, !tbaa !6
  %shl200 = shl i32 %205, %207
  store i32 %shl200, i32* %hsize, align 4, !tbaa !6
  %208 = bitcast i32* %vsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %208) #5
  %209 = load i32, i32* %nvb, align 4, !tbaa !6
  %210 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx201 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %210
  %211 = load i32, i32* %arrayidx201, align 4, !tbaa !6
  %shl202 = shl i32 %209, %211
  store i32 %shl202, i32* %vsize, align 4, !tbaa !6
  %212 = load i32, i32* %pli193, align 4, !tbaa !6
  %tobool203 = icmp ne i32 %212, 0
  br i1 %tobool203, label %if.then204, label %if.end205

if.then204:                                       ; preds = %for.body198
  %213 = load i32, i32* %uv_level, align 4, !tbaa !6
  store i32 %213, i32* %level, align 4, !tbaa !6
  %214 = load i32, i32* %uv_sec_strength, align 4, !tbaa !6
  store i32 %214, i32* %sec_strength, align 4, !tbaa !6
  br label %if.end205

if.end205:                                        ; preds = %if.then204, %for.body198
  %215 = load i32, i32* %fbc, align 4, !tbaa !6
  %216 = load i32, i32* %nhfb, align 4, !tbaa !6
  %sub206 = sub nsw i32 %216, 1
  %cmp207 = icmp eq i32 %215, %sub206
  br i1 %cmp207, label %if.then209, label %if.else210

if.then209:                                       ; preds = %if.end205
  %217 = load i32, i32* %hsize, align 4, !tbaa !6
  store i32 %217, i32* %cend, align 4, !tbaa !6
  br label %if.end212

if.else210:                                       ; preds = %if.end205
  %218 = load i32, i32* %hsize, align 4, !tbaa !6
  %add211 = add nsw i32 %218, 8
  store i32 %add211, i32* %cend, align 4, !tbaa !6
  br label %if.end212

if.end212:                                        ; preds = %if.else210, %if.then209
  %219 = load i32, i32* %fbr, align 4, !tbaa !6
  %220 = load i32, i32* %nvfb, align 4, !tbaa !6
  %sub213 = sub nsw i32 %220, 1
  %cmp214 = icmp eq i32 %219, %sub213
  br i1 %cmp214, label %if.then216, label %if.else217

if.then216:                                       ; preds = %if.end212
  %221 = load i32, i32* %vsize, align 4, !tbaa !6
  store i32 %221, i32* %rend, align 4, !tbaa !6
  br label %if.end219

if.else217:                                       ; preds = %if.end212
  %222 = load i32, i32* %vsize, align 4, !tbaa !6
  %add218 = add nsw i32 %222, 3
  store i32 %add218, i32* %rend, align 4, !tbaa !6
  br label %if.end219

if.end219:                                        ; preds = %if.else217, %if.then216
  %223 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul220 = mul nsw i32 %223, 16
  %224 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx221 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_wide_l2, i32 0, i32 %224
  %225 = load i32, i32* %arrayidx221, align 4, !tbaa !6
  %shl222 = shl i32 %mul220, %225
  store i32 %shl222, i32* %coffset, align 4, !tbaa !6
  %226 = load i32, i32* %fbc, align 4, !tbaa !6
  %227 = load i32, i32* %nhfb, align 4, !tbaa !6
  %sub223 = sub nsw i32 %227, 1
  %cmp224 = icmp eq i32 %226, %sub223
  br i1 %cmp224, label %if.then226, label %if.end232

if.then226:                                       ; preds = %if.end219
  %228 = load i32, i32* %cend, align 4, !tbaa !6
  %add227 = add nsw i32 %228, 8
  %arrayidx228 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 %add227
  %229 = load i32, i32* %rend, align 4, !tbaa !6
  %add229 = add nsw i32 %229, 3
  %230 = load i32, i32* %hsize, align 4, !tbaa !6
  %add230 = add nsw i32 %230, 8
  %231 = load i32, i32* %cend, align 4, !tbaa !6
  %sub231 = sub nsw i32 %add230, %231
  call void @fill_rect(i16* %arrayidx228, i32 144, i32 %add229, i32 %sub231, i16 zeroext 30000)
  br label %if.end232

if.end232:                                        ; preds = %if.then226, %if.end219
  %232 = load i32, i32* %fbr, align 4, !tbaa !6
  %233 = load i32, i32* %nvfb, align 4, !tbaa !6
  %sub233 = sub nsw i32 %233, 1
  %cmp234 = icmp eq i32 %232, %sub233
  br i1 %cmp234, label %if.then236, label %if.end241

if.then236:                                       ; preds = %if.end232
  %234 = load i32, i32* %rend, align 4, !tbaa !6
  %add237 = add nsw i32 %234, 3
  %mul238 = mul nsw i32 %add237, 144
  %arrayidx239 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 %mul238
  %235 = load i32, i32* %hsize, align 4, !tbaa !6
  %add240 = add nsw i32 %235, 16
  call void @fill_rect(i16* %arrayidx239, i32 144, i32 3, i32 %add240, i16 zeroext 30000)
  br label %if.end241

if.end241:                                        ; preds = %if.then236, %if.end232
  %236 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %237 = load i32, i32* %cstart, align 4, !tbaa !6
  %add242 = add nsw i32 440, %237
  %arrayidx243 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 %add242
  %238 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane244 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %238, i32 0, i32 4
  %239 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx245 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane244, i32 0, i32 %239
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx245, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 0
  %240 = load i8*, i8** %buf, align 4, !tbaa !52
  %241 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx246 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %241
  %242 = load i32, i32* %arrayidx246, align 4, !tbaa !6
  %shl247 = shl i32 16, %242
  %243 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul248 = mul nsw i32 %shl247, %243
  %244 = load i32, i32* %coffset, align 4, !tbaa !6
  %245 = load i32, i32* %cstart, align 4, !tbaa !6
  %add249 = add nsw i32 %244, %245
  %246 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane250 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %246, i32 0, i32 4
  %247 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx251 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane250, i32 0, i32 %247
  %dst252 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx251, i32 0, i32 6
  %stride253 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst252, i32 0, i32 4
  %248 = load i32, i32* %stride253, align 4, !tbaa !53
  %249 = load i32, i32* %rend, align 4, !tbaa !6
  %250 = load i32, i32* %cend, align 4, !tbaa !6
  %251 = load i32, i32* %cstart, align 4, !tbaa !6
  %sub254 = sub nsw i32 %250, %251
  call void @copy_sb8_16(%struct.AV1Common* %236, i16* %arrayidx243, i32 144, i8* %240, i32 %mul248, i32 %add249, i32 %248, i32 %249, i32 %sub254)
  %252 = load i8*, i8** %prev_row_cdef, align 4, !tbaa !2
  %253 = load i32, i32* %fbc, align 4, !tbaa !6
  %arrayidx255 = getelementptr inbounds i8, i8* %252, i32 %253
  %254 = load i8, i8* %arrayidx255, align 1, !tbaa !8
  %tobool256 = icmp ne i8 %254, 0
  br i1 %tobool256, label %if.else271, label %if.then257

if.then257:                                       ; preds = %if.end241
  %255 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %arrayidx258 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 8
  %256 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane259 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %256, i32 0, i32 4
  %257 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx260 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane259, i32 0, i32 %257
  %dst261 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx260, i32 0, i32 6
  %buf262 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst261, i32 0, i32 0
  %258 = load i8*, i8** %buf262, align 4, !tbaa !52
  %259 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx263 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %259
  %260 = load i32, i32* %arrayidx263, align 4, !tbaa !6
  %shl264 = shl i32 16, %260
  %261 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul265 = mul nsw i32 %shl264, %261
  %sub266 = sub nsw i32 %mul265, 3
  %262 = load i32, i32* %coffset, align 4, !tbaa !6
  %263 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane267 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %263, i32 0, i32 4
  %264 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx268 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane267, i32 0, i32 %264
  %dst269 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx268, i32 0, i32 6
  %stride270 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst269, i32 0, i32 4
  %265 = load i32, i32* %stride270, align 4, !tbaa !53
  %266 = load i32, i32* %hsize, align 4, !tbaa !6
  call void @copy_sb8_16(%struct.AV1Common* %255, i16* %arrayidx258, i32 144, i8* %258, i32 %sub266, i32 %262, i32 %265, i32 3, i32 %266)
  br label %if.end281

if.else271:                                       ; preds = %if.end241
  %267 = load i32, i32* %fbr, align 4, !tbaa !6
  %cmp272 = icmp sgt i32 %267, 0
  br i1 %cmp272, label %if.then274, label %if.else278

if.then274:                                       ; preds = %if.else271
  %arrayidx275 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 8
  %268 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx276 = getelementptr inbounds [3 x i16*], [3 x i16*]* %linebuf, i32 0, i32 %268
  %269 = load i16*, i16** %arrayidx276, align 4, !tbaa !2
  %270 = load i32, i32* %coffset, align 4, !tbaa !6
  %arrayidx277 = getelementptr inbounds i16, i16* %269, i32 %270
  %271 = load i32, i32* %stride, align 4, !tbaa !6
  %272 = load i32, i32* %hsize, align 4, !tbaa !6
  call void @copy_rect(i16* %arrayidx275, i32 144, i16* %arrayidx277, i32 %271, i32 3, i32 %272)
  br label %if.end280

if.else278:                                       ; preds = %if.else271
  %arrayidx279 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 8
  %273 = load i32, i32* %hsize, align 4, !tbaa !6
  call void @fill_rect(i16* %arrayidx279, i32 144, i32 3, i32 %273, i16 zeroext 30000)
  br label %if.end280

if.end280:                                        ; preds = %if.else278, %if.then274
  br label %if.end281

if.end281:                                        ; preds = %if.end280, %if.then257
  %274 = load i8*, i8** %prev_row_cdef, align 4, !tbaa !2
  %275 = load i32, i32* %fbc, align 4, !tbaa !6
  %sub282 = sub nsw i32 %275, 1
  %arrayidx283 = getelementptr inbounds i8, i8* %274, i32 %sub282
  %276 = load i8, i8* %arrayidx283, align 1, !tbaa !8
  %tobool284 = icmp ne i8 %276, 0
  br i1 %tobool284, label %if.else300, label %if.then285

if.then285:                                       ; preds = %if.end281
  %277 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %arraydecay286 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 0
  %278 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane287 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %278, i32 0, i32 4
  %279 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx288 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane287, i32 0, i32 %279
  %dst289 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx288, i32 0, i32 6
  %buf290 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst289, i32 0, i32 0
  %280 = load i8*, i8** %buf290, align 4, !tbaa !52
  %281 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx291 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %281
  %282 = load i32, i32* %arrayidx291, align 4, !tbaa !6
  %shl292 = shl i32 16, %282
  %283 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul293 = mul nsw i32 %shl292, %283
  %sub294 = sub nsw i32 %mul293, 3
  %284 = load i32, i32* %coffset, align 4, !tbaa !6
  %sub295 = sub nsw i32 %284, 8
  %285 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane296 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %285, i32 0, i32 4
  %286 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx297 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane296, i32 0, i32 %286
  %dst298 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx297, i32 0, i32 6
  %stride299 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst298, i32 0, i32 4
  %287 = load i32, i32* %stride299, align 4, !tbaa !53
  call void @copy_sb8_16(%struct.AV1Common* %277, i16* %arraydecay286, i32 144, i8* %280, i32 %sub294, i32 %sub295, i32 %287, i32 3, i32 8)
  br label %if.end314

if.else300:                                       ; preds = %if.end281
  %288 = load i32, i32* %fbr, align 4, !tbaa !6
  %cmp301 = icmp sgt i32 %288, 0
  br i1 %cmp301, label %land.lhs.true303, label %if.else311

land.lhs.true303:                                 ; preds = %if.else300
  %289 = load i32, i32* %fbc, align 4, !tbaa !6
  %cmp304 = icmp sgt i32 %289, 0
  br i1 %cmp304, label %if.then306, label %if.else311

if.then306:                                       ; preds = %land.lhs.true303
  %arraydecay307 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 0
  %290 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx308 = getelementptr inbounds [3 x i16*], [3 x i16*]* %linebuf, i32 0, i32 %290
  %291 = load i16*, i16** %arrayidx308, align 4, !tbaa !2
  %292 = load i32, i32* %coffset, align 4, !tbaa !6
  %sub309 = sub nsw i32 %292, 8
  %arrayidx310 = getelementptr inbounds i16, i16* %291, i32 %sub309
  %293 = load i32, i32* %stride, align 4, !tbaa !6
  call void @copy_rect(i16* %arraydecay307, i32 144, i16* %arrayidx310, i32 %293, i32 3, i32 8)
  br label %if.end313

if.else311:                                       ; preds = %land.lhs.true303, %if.else300
  %arraydecay312 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 0
  call void @fill_rect(i16* %arraydecay312, i32 144, i32 3, i32 8, i16 zeroext 30000)
  br label %if.end313

if.end313:                                        ; preds = %if.else311, %if.then306
  br label %if.end314

if.end314:                                        ; preds = %if.end313, %if.then285
  %294 = load i8*, i8** %prev_row_cdef, align 4, !tbaa !2
  %295 = load i32, i32* %fbc, align 4, !tbaa !6
  %add315 = add nsw i32 %295, 1
  %arrayidx316 = getelementptr inbounds i8, i8* %294, i32 %add315
  %296 = load i8, i8* %arrayidx316, align 1, !tbaa !8
  %tobool317 = icmp ne i8 %296, 0
  br i1 %tobool317, label %if.else336, label %if.then318

if.then318:                                       ; preds = %if.end314
  %297 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %298 = load i32, i32* %nhb, align 4, !tbaa !6
  %299 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx319 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_wide_l2, i32 0, i32 %299
  %300 = load i32, i32* %arrayidx319, align 4, !tbaa !6
  %shl320 = shl i32 %298, %300
  %add321 = add nsw i32 8, %shl320
  %arrayidx322 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 %add321
  %301 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane323 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %301, i32 0, i32 4
  %302 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx324 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane323, i32 0, i32 %302
  %dst325 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx324, i32 0, i32 6
  %buf326 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst325, i32 0, i32 0
  %303 = load i8*, i8** %buf326, align 4, !tbaa !52
  %304 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx327 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %304
  %305 = load i32, i32* %arrayidx327, align 4, !tbaa !6
  %shl328 = shl i32 16, %305
  %306 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul329 = mul nsw i32 %shl328, %306
  %sub330 = sub nsw i32 %mul329, 3
  %307 = load i32, i32* %coffset, align 4, !tbaa !6
  %308 = load i32, i32* %hsize, align 4, !tbaa !6
  %add331 = add nsw i32 %307, %308
  %309 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane332 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %309, i32 0, i32 4
  %310 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx333 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane332, i32 0, i32 %310
  %dst334 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx333, i32 0, i32 6
  %stride335 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst334, i32 0, i32 4
  %311 = load i32, i32* %stride335, align 4, !tbaa !53
  call void @copy_sb8_16(%struct.AV1Common* %297, i16* %arrayidx322, i32 144, i8* %303, i32 %sub330, i32 %add331, i32 %311, i32 3, i32 8)
  br label %if.end353

if.else336:                                       ; preds = %if.end314
  %312 = load i32, i32* %fbr, align 4, !tbaa !6
  %cmp337 = icmp sgt i32 %312, 0
  br i1 %cmp337, label %land.lhs.true339, label %if.else349

land.lhs.true339:                                 ; preds = %if.else336
  %313 = load i32, i32* %fbc, align 4, !tbaa !6
  %314 = load i32, i32* %nhfb, align 4, !tbaa !6
  %sub340 = sub nsw i32 %314, 1
  %cmp341 = icmp slt i32 %313, %sub340
  br i1 %cmp341, label %if.then343, label %if.else349

if.then343:                                       ; preds = %land.lhs.true339
  %315 = load i32, i32* %hsize, align 4, !tbaa !6
  %add344 = add nsw i32 %315, 8
  %arrayidx345 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 %add344
  %316 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx346 = getelementptr inbounds [3 x i16*], [3 x i16*]* %linebuf, i32 0, i32 %316
  %317 = load i16*, i16** %arrayidx346, align 4, !tbaa !2
  %318 = load i32, i32* %coffset, align 4, !tbaa !6
  %319 = load i32, i32* %hsize, align 4, !tbaa !6
  %add347 = add nsw i32 %318, %319
  %arrayidx348 = getelementptr inbounds i16, i16* %317, i32 %add347
  %320 = load i32, i32* %stride, align 4, !tbaa !6
  call void @copy_rect(i16* %arrayidx345, i32 144, i16* %arrayidx348, i32 %320, i32 3, i32 8)
  br label %if.end352

if.else349:                                       ; preds = %land.lhs.true339, %if.else336
  %321 = load i32, i32* %hsize, align 4, !tbaa !6
  %add350 = add nsw i32 %321, 8
  %arrayidx351 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 %add350
  call void @fill_rect(i16* %arrayidx351, i32 144, i32 3, i32 8, i16 zeroext 30000)
  br label %if.end352

if.end352:                                        ; preds = %if.else349, %if.then343
  br label %if.end353

if.end353:                                        ; preds = %if.end352, %if.then318
  %322 = load i32, i32* %cdef_left, align 4, !tbaa !6
  %tobool354 = icmp ne i32 %322, 0
  br i1 %tobool354, label %if.then355, label %if.end359

if.then355:                                       ; preds = %if.end353
  %arraydecay356 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 0
  %323 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx357 = getelementptr inbounds [3 x i16*], [3 x i16*]* %colbuf, i32 0, i32 %323
  %324 = load i16*, i16** %arrayidx357, align 4, !tbaa !2
  %325 = load i32, i32* %rend, align 4, !tbaa !6
  %add358 = add nsw i32 %325, 3
  call void @copy_rect(i16* %arraydecay356, i32 144, i16* %324, i32 8, i32 %add358, i32 8)
  br label %if.end359

if.end359:                                        ; preds = %if.then355, %if.end353
  %326 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx360 = getelementptr inbounds [3 x i16*], [3 x i16*]* %colbuf, i32 0, i32 %326
  %327 = load i16*, i16** %arrayidx360, align 4, !tbaa !2
  %arraydecay361 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 0
  %328 = load i32, i32* %hsize, align 4, !tbaa !6
  %add.ptr362 = getelementptr inbounds i16, i16* %arraydecay361, i32 %328
  %329 = load i32, i32* %rend, align 4, !tbaa !6
  %add363 = add nsw i32 %329, 3
  call void @copy_rect(i16* %327, i32 8, i16* %add.ptr362, i32 144, i32 %add363, i32 8)
  %330 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %331 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx364 = getelementptr inbounds [3 x i16*], [3 x i16*]* %linebuf, i32 0, i32 %331
  %332 = load i16*, i16** %arrayidx364, align 4, !tbaa !2
  %333 = load i32, i32* %coffset, align 4, !tbaa !6
  %arrayidx365 = getelementptr inbounds i16, i16* %332, i32 %333
  %334 = load i32, i32* %stride, align 4, !tbaa !6
  %335 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane366 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %335, i32 0, i32 4
  %336 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx367 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane366, i32 0, i32 %336
  %dst368 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx367, i32 0, i32 6
  %buf369 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst368, i32 0, i32 0
  %337 = load i8*, i8** %buf369, align 4, !tbaa !52
  %338 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx370 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %338
  %339 = load i32, i32* %arrayidx370, align 4, !tbaa !6
  %shl371 = shl i32 16, %339
  %340 = load i32, i32* %fbr, align 4, !tbaa !6
  %add372 = add nsw i32 %340, 1
  %mul373 = mul nsw i32 %shl371, %add372
  %sub374 = sub nsw i32 %mul373, 3
  %341 = load i32, i32* %coffset, align 4, !tbaa !6
  %342 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane375 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %342, i32 0, i32 4
  %343 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx376 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane375, i32 0, i32 %343
  %dst377 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx376, i32 0, i32 6
  %stride378 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst377, i32 0, i32 4
  %344 = load i32, i32* %stride378, align 4, !tbaa !53
  %345 = load i32, i32* %hsize, align 4, !tbaa !6
  call void @copy_sb8_16(%struct.AV1Common* %330, i16* %arrayidx365, i32 %334, i8* %337, i32 %sub374, i32 %341, i32 %344, i32 3, i32 %345)
  %346 = load i32, i32* %frame_top, align 4, !tbaa !6
  %tobool379 = icmp ne i32 %346, 0
  br i1 %tobool379, label %if.then380, label %if.end383

if.then380:                                       ; preds = %if.end359
  %arraydecay381 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 0
  %347 = load i32, i32* %hsize, align 4, !tbaa !6
  %add382 = add nsw i32 %347, 16
  call void @fill_rect(i16* %arraydecay381, i32 144, i32 3, i32 %add382, i16 zeroext 30000)
  br label %if.end383

if.end383:                                        ; preds = %if.then380, %if.end359
  %348 = load i32, i32* %frame_left, align 4, !tbaa !6
  %tobool384 = icmp ne i32 %348, 0
  br i1 %tobool384, label %if.then385, label %if.end388

if.then385:                                       ; preds = %if.end383
  %arraydecay386 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 0
  %349 = load i32, i32* %vsize, align 4, !tbaa !6
  %add387 = add nsw i32 %349, 6
  call void @fill_rect(i16* %arraydecay386, i32 144, i32 %add387, i32 8, i16 zeroext 30000)
  br label %if.end388

if.end388:                                        ; preds = %if.then385, %if.end383
  %350 = load i32, i32* %frame_bottom, align 4, !tbaa !6
  %tobool389 = icmp ne i32 %350, 0
  br i1 %tobool389, label %if.then390, label %if.end395

if.then390:                                       ; preds = %if.end388
  %351 = load i32, i32* %vsize, align 4, !tbaa !6
  %add391 = add nsw i32 %351, 3
  %mul392 = mul nsw i32 %add391, 144
  %arrayidx393 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 %mul392
  %352 = load i32, i32* %hsize, align 4, !tbaa !6
  %add394 = add nsw i32 %352, 16
  call void @fill_rect(i16* %arrayidx393, i32 144, i32 3, i32 %add394, i16 zeroext 30000)
  br label %if.end395

if.end395:                                        ; preds = %if.then390, %if.end388
  %353 = load i32, i32* %frame_right, align 4, !tbaa !6
  %tobool396 = icmp ne i32 %353, 0
  br i1 %tobool396, label %if.then397, label %if.end401

if.then397:                                       ; preds = %if.end395
  %354 = load i32, i32* %hsize, align 4, !tbaa !6
  %add398 = add nsw i32 %354, 8
  %arrayidx399 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 %add398
  %355 = load i32, i32* %vsize, align 4, !tbaa !6
  %add400 = add nsw i32 %355, 6
  call void @fill_rect(i16* %arrayidx399, i32 144, i32 %add400, i32 8, i16 zeroext 30000)
  br label %if.end401

if.end401:                                        ; preds = %if.then397, %if.end395
  %356 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params402 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %356, i32 0, i32 37
  %use_highbitdepth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params402, i32 0, i32 26
  %357 = load i8, i8* %use_highbitdepth, align 4, !tbaa !54
  %tobool403 = icmp ne i8 %357, 0
  br i1 %tobool403, label %if.then404, label %if.else433

if.then404:                                       ; preds = %if.end401
  %358 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane405 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %358, i32 0, i32 4
  %359 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx406 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane405, i32 0, i32 %359
  %dst407 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx406, i32 0, i32 6
  %buf408 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst407, i32 0, i32 0
  %360 = load i8*, i8** %buf408, align 4, !tbaa !52
  %361 = ptrtoint i8* %360 to i32
  %shl409 = shl i32 %361, 1
  %362 = inttoptr i32 %shl409 to i16*
  %363 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane410 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %363, i32 0, i32 4
  %364 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx411 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane410, i32 0, i32 %364
  %dst412 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx411, i32 0, i32 6
  %stride413 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst412, i32 0, i32 4
  %365 = load i32, i32* %stride413, align 4, !tbaa !53
  %366 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul414 = mul nsw i32 16, %366
  %367 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx415 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %367
  %368 = load i32, i32* %arrayidx415, align 4, !tbaa !6
  %shl416 = shl i32 %mul414, %368
  %mul417 = mul nsw i32 %365, %shl416
  %369 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul418 = mul nsw i32 %369, 16
  %370 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx419 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_wide_l2, i32 0, i32 %370
  %371 = load i32, i32* %arrayidx419, align 4, !tbaa !6
  %shl420 = shl i32 %mul418, %371
  %add421 = add nsw i32 %mul417, %shl420
  %arrayidx422 = getelementptr inbounds i16, i16* %362, i32 %add421
  %372 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane423 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %372, i32 0, i32 4
  %373 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx424 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane423, i32 0, i32 %373
  %dst425 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx424, i32 0, i32 6
  %stride426 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst425, i32 0, i32 4
  %374 = load i32, i32* %stride426, align 4, !tbaa !53
  %arrayidx427 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 440
  %375 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx428 = getelementptr inbounds [3 x i32], [3 x i32]* %xdec, i32 0, i32 %375
  %376 = load i32, i32* %arrayidx428, align 4, !tbaa !6
  %377 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx429 = getelementptr inbounds [3 x i32], [3 x i32]* %ydec, i32 0, i32 %377
  %378 = load i32, i32* %arrayidx429, align 4, !tbaa !6
  %arraydecay430 = getelementptr inbounds [16 x [16 x i32]], [16 x [16 x i32]]* %dir, i32 0, i32 0
  %arraydecay431 = getelementptr inbounds [16 x [16 x i32]], [16 x [16 x i32]]* %var, i32 0, i32 0
  %379 = load i32, i32* %pli193, align 4, !tbaa !6
  %arraydecay432 = getelementptr inbounds [256 x %struct.cdef_list], [256 x %struct.cdef_list]* %dlist, i32 0, i32 0
  %380 = load i32, i32* %cdef_count, align 4, !tbaa !6
  %381 = load i32, i32* %level, align 4, !tbaa !6
  %382 = load i32, i32* %sec_strength, align 4, !tbaa !6
  %383 = load i32, i32* %damping, align 4, !tbaa !6
  %384 = load i32, i32* %coeff_shift, align 4, !tbaa !6
  call void @av1_cdef_filter_fb(i8* null, i16* %arrayidx422, i32 %374, i16* %arrayidx427, i32 %376, i32 %378, [16 x i32]* %arraydecay430, i32* null, [16 x i32]* %arraydecay431, i32 %379, %struct.cdef_list* %arraydecay432, i32 %380, i32 %381, i32 %382, i32 %383, i32 %384)
  br label %if.end461

if.else433:                                       ; preds = %if.end401
  %385 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane434 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %385, i32 0, i32 4
  %386 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx435 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane434, i32 0, i32 %386
  %dst436 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx435, i32 0, i32 6
  %buf437 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst436, i32 0, i32 0
  %387 = load i8*, i8** %buf437, align 4, !tbaa !52
  %388 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane438 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %388, i32 0, i32 4
  %389 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx439 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane438, i32 0, i32 %389
  %dst440 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx439, i32 0, i32 6
  %stride441 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst440, i32 0, i32 4
  %390 = load i32, i32* %stride441, align 4, !tbaa !53
  %391 = load i32, i32* %fbr, align 4, !tbaa !6
  %mul442 = mul nsw i32 16, %391
  %392 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx443 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_high_l2, i32 0, i32 %392
  %393 = load i32, i32* %arrayidx443, align 4, !tbaa !6
  %shl444 = shl i32 %mul442, %393
  %mul445 = mul nsw i32 %390, %shl444
  %394 = load i32, i32* %fbc, align 4, !tbaa !6
  %mul446 = mul nsw i32 %394, 16
  %395 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx447 = getelementptr inbounds [3 x i32], [3 x i32]* %mi_wide_l2, i32 0, i32 %395
  %396 = load i32, i32* %arrayidx447, align 4, !tbaa !6
  %shl448 = shl i32 %mul446, %396
  %add449 = add nsw i32 %mul445, %shl448
  %arrayidx450 = getelementptr inbounds i8, i8* %387, i32 %add449
  %397 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane451 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %397, i32 0, i32 4
  %398 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx452 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane451, i32 0, i32 %398
  %dst453 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx452, i32 0, i32 6
  %stride454 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst453, i32 0, i32 4
  %399 = load i32, i32* %stride454, align 4, !tbaa !53
  %arrayidx455 = getelementptr inbounds [19296 x i16], [19296 x i16]* %src, i32 0, i32 440
  %400 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx456 = getelementptr inbounds [3 x i32], [3 x i32]* %xdec, i32 0, i32 %400
  %401 = load i32, i32* %arrayidx456, align 4, !tbaa !6
  %402 = load i32, i32* %pli193, align 4, !tbaa !6
  %arrayidx457 = getelementptr inbounds [3 x i32], [3 x i32]* %ydec, i32 0, i32 %402
  %403 = load i32, i32* %arrayidx457, align 4, !tbaa !6
  %arraydecay458 = getelementptr inbounds [16 x [16 x i32]], [16 x [16 x i32]]* %dir, i32 0, i32 0
  %arraydecay459 = getelementptr inbounds [16 x [16 x i32]], [16 x [16 x i32]]* %var, i32 0, i32 0
  %404 = load i32, i32* %pli193, align 4, !tbaa !6
  %arraydecay460 = getelementptr inbounds [256 x %struct.cdef_list], [256 x %struct.cdef_list]* %dlist, i32 0, i32 0
  %405 = load i32, i32* %cdef_count, align 4, !tbaa !6
  %406 = load i32, i32* %level, align 4, !tbaa !6
  %407 = load i32, i32* %sec_strength, align 4, !tbaa !6
  %408 = load i32, i32* %damping, align 4, !tbaa !6
  %409 = load i32, i32* %coeff_shift, align 4, !tbaa !6
  call void @av1_cdef_filter_fb(i8* %arrayidx450, i16* null, i32 %399, i16* %arrayidx455, i32 %401, i32 %403, [16 x i32]* %arraydecay458, i32* null, [16 x i32]* %arraydecay459, i32 %404, %struct.cdef_list* %arraydecay460, i32 %405, i32 %406, i32 %407, i32 %408, i32 %409)
  br label %if.end461

if.end461:                                        ; preds = %if.else433, %if.then404
  %410 = bitcast i32* %vsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %410) #5
  %411 = bitcast i32* %hsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %411) #5
  %412 = bitcast i32* %damping to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %412) #5
  %413 = bitcast i32* %cend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %413) #5
  %414 = bitcast i32* %rend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %414) #5
  %415 = bitcast i32* %coffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %415) #5
  br label %for.inc462

for.inc462:                                       ; preds = %if.end461
  %416 = load i32, i32* %pli193, align 4, !tbaa !6
  %inc463 = add nsw i32 %416, 1
  store i32 %inc463, i32* %pli193, align 4, !tbaa !6
  br label %for.cond194

for.end464:                                       ; preds = %for.cond.cleanup197
  store i32 1, i32* %cdef_left, align 4, !tbaa !6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end464, %if.then190
  %417 = bitcast i32* %mbmi_cdef_strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %417) #5
  %418 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %418) #5
  %419 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %419) #5
  %420 = bitcast i32* %frame_right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %420) #5
  %421 = bitcast i32* %frame_bottom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %421) #5
  %422 = bitcast i32* %frame_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %422) #5
  %423 = bitcast i32* %frame_top to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %423) #5
  br label %cleanup471

cleanup471:                                       ; preds = %cleanup, %if.then
  %424 = bitcast i32* %cstart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %424) #5
  %425 = bitcast i32* %nvb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %425) #5
  %426 = bitcast i32* %nhb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %426) #5
  %427 = bitcast i32* %uv_sec_strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %427) #5
  %428 = bitcast i32* %uv_level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %428) #5
  %429 = bitcast i32* %sec_strength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %429) #5
  %430 = bitcast i32* %level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %430) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 16, label %for.inc478
  ]

cleanup.cont:                                     ; preds = %cleanup471
  br label %for.inc478

for.inc478:                                       ; preds = %cleanup.cont, %cleanup471
  %431 = load i32, i32* %fbc, align 4, !tbaa !6
  %inc479 = add nsw i32 %431, 1
  store i32 %inc479, i32* %fbc, align 4, !tbaa !6
  br label %for.cond71

for.end481:                                       ; preds = %for.cond.cleanup73
  %432 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %432) #5
  %433 = load i8*, i8** %prev_row_cdef, align 4, !tbaa !2
  store i8* %433, i8** %tmp, align 4, !tbaa !2
  %434 = load i8*, i8** %curr_row_cdef, align 4, !tbaa !2
  store i8* %434, i8** %prev_row_cdef, align 4, !tbaa !2
  %435 = load i8*, i8** %tmp, align 4, !tbaa !2
  store i8* %435, i8** %curr_row_cdef, align 4, !tbaa !2
  %436 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %436) #5
  %437 = bitcast i32* %cdef_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %437) #5
  br label %for.inc482

for.inc482:                                       ; preds = %for.end481
  %438 = load i32, i32* %fbr, align 4, !tbaa !6
  %inc483 = add nsw i32 %438, 1
  store i32 %inc483, i32* %fbr, align 4, !tbaa !6
  br label %for.cond55

for.end485:                                       ; preds = %for.cond.cleanup57
  %439 = load i8*, i8** %row_cdef, align 4, !tbaa !2
  call void @aom_free(i8* %439)
  %440 = bitcast i32* %pli486 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %440) #5
  store i32 0, i32* %pli486, align 4, !tbaa !6
  br label %for.cond487

for.cond487:                                      ; preds = %for.inc494, %for.end485
  %441 = load i32, i32* %pli486, align 4, !tbaa !6
  %442 = load i32, i32* %num_planes, align 4, !tbaa !6
  %cmp488 = icmp slt i32 %441, %442
  br i1 %cmp488, label %for.body491, label %for.cond.cleanup490

for.cond.cleanup490:                              ; preds = %for.cond487
  store i32 20, i32* %cleanup.dest.slot, align 4
  %443 = bitcast i32* %pli486 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %443) #5
  br label %for.end497

for.body491:                                      ; preds = %for.cond487
  %444 = load i32, i32* %pli486, align 4, !tbaa !6
  %arrayidx492 = getelementptr inbounds [3 x i16*], [3 x i16*]* %linebuf, i32 0, i32 %444
  %445 = load i16*, i16** %arrayidx492, align 4, !tbaa !2
  %446 = bitcast i16* %445 to i8*
  call void @aom_free(i8* %446)
  %447 = load i32, i32* %pli486, align 4, !tbaa !6
  %arrayidx493 = getelementptr inbounds [3 x i16*], [3 x i16*]* %colbuf, i32 0, i32 %447
  %448 = load i16*, i16** %arrayidx493, align 4, !tbaa !2
  %449 = bitcast i16* %448 to i8*
  call void @aom_free(i8* %449)
  br label %for.inc494

for.inc494:                                       ; preds = %for.body491
  %450 = load i32, i32* %pli486, align 4, !tbaa !6
  %inc495 = add nsw i32 %450, 1
  store i32 %inc495, i32* %pli486, align 4, !tbaa !6
  br label %for.cond487

for.end497:                                       ; preds = %for.cond.cleanup490
  %451 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %451) #5
  %452 = bitcast i32* %nhfb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %452) #5
  %453 = bitcast i32* %nvfb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %453) #5
  %454 = bitcast i32* %coeff_shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %454) #5
  %455 = bitcast [3 x i32]* %ydec to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %455) #5
  %456 = bitcast [3 x i32]* %xdec to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %456) #5
  %457 = bitcast [3 x i32]* %mi_high_l2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %457) #5
  %458 = bitcast [3 x i32]* %mi_wide_l2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %458) #5
  %459 = bitcast [16 x [16 x i32]]* %var to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %459) #5
  %460 = bitcast [16 x [16 x i32]]* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %460) #5
  %461 = bitcast i32* %cdef_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %461) #5
  %462 = bitcast i8** %curr_row_cdef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %462) #5
  %463 = bitcast i8** %prev_row_cdef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %463) #5
  %464 = bitcast i8** %row_cdef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %464) #5
  %465 = bitcast [256 x %struct.cdef_list]* %dlist to i8*
  call void @llvm.lifetime.end.p0i8(i64 512, i8* %465) #5
  %466 = bitcast [3 x i16*]* %colbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %466) #5
  %467 = bitcast [3 x i16*]* %linebuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %467) #5
  %468 = bitcast [19296 x i16]* %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 38592, i8* %468) #5
  %469 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %469) #5
  %470 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %470) #5
  %471 = bitcast %struct.CdefInfo** %cdef_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %471) #5
  ret void

unreachable:                                      ; preds = %cleanup471
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_num_planes(%struct.AV1Common* %cm) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %1 = load i8, i8* %monochrome, align 1, !tbaa !55
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 3
  ret i32 %cond
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

declare void @av1_setup_dst_planes(%struct.macroblockd_plane*, i8 zeroext, %struct.yv12_buffer_config*, i32, i32, i32, i32) #4

declare i8* @aom_malloc(i32) #4

; Function Attrs: inlinehint nounwind
define internal void @fill_rect(i16* %dst, i32 %dstride, i32 %v, i32 %h, i16 zeroext %x) #2 {
entry:
  %dst.addr = alloca i16*, align 4
  %dstride.addr = alloca i32, align 4
  %v.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %x.addr = alloca i16, align 2
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dstride, i32* %dstride.addr, align 4, !tbaa !6
  store i32 %v, i32* %v.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i16 %x, i16* %x.addr, align 2, !tbaa !23
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc5, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %v.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end7

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %j, align 4, !tbaa !6
  %6 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i16, i16* %x.addr, align 2, !tbaa !23
  %9 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %10, %11
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul, %12
  %arrayidx = getelementptr inbounds i16, i16* %9, i32 %add
  store i16 %8, i16* %arrayidx, align 2, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc5

for.inc5:                                         ; preds = %for.end
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %inc6 = add nsw i32 %14, 1
  store i32 %inc6, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end7:                                         ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal void @copy_sb8_16(%struct.AV1Common* %cm, i16* %dst, i32 %dstride, i8* %src, i32 %src_voffset, i32 %src_hoffset, i32 %sstride, i32 %vsize, i32 %hsize) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %dst.addr = alloca i16*, align 4
  %dstride.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %src_voffset.addr = alloca i32, align 4
  %src_hoffset.addr = alloca i32, align 4
  %sstride.addr = alloca i32, align 4
  %vsize.addr = alloca i32, align 4
  %hsize.addr = alloca i32, align 4
  %base = alloca i16*, align 4
  %base1 = alloca i8*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dstride, i32* %dstride.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_voffset, i32* %src_voffset.addr, align 4, !tbaa !6
  store i32 %src_hoffset, i32* %src_hoffset.addr, align 4, !tbaa !6
  store i32 %sstride, i32* %sstride.addr, align 4, !tbaa !6
  store i32 %vsize, i32* %vsize.addr, align 4, !tbaa !6
  store i32 %hsize, i32* %hsize.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %use_highbitdepth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 26
  %1 = load i8, i8* %use_highbitdepth, align 4, !tbaa !54
  %tobool = icmp ne i8 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast i16** %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  %6 = load i32, i32* %src_voffset.addr, align 4, !tbaa !6
  %7 = load i32, i32* %sstride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %6, %7
  %8 = load i32, i32* %src_hoffset.addr, align 4, !tbaa !6
  %add = add nsw i32 %mul, %8
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 %add
  store i16* %arrayidx, i16** %base, align 4, !tbaa !2
  %9 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %11 = load i16*, i16** %base, align 4, !tbaa !2
  %12 = load i32, i32* %sstride.addr, align 4, !tbaa !6
  %13 = load i32, i32* %vsize.addr, align 4, !tbaa !6
  %14 = load i32, i32* %hsize.addr, align 4, !tbaa !6
  call void @cdef_copy_rect8_16bit_to_16bit_c(i16* %9, i32 %10, i16* %11, i32 %12, i32 %13, i32 %14)
  %15 = bitcast i16** %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  br label %if.end

if.else:                                          ; preds = %entry
  %16 = bitcast i8** %base1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %18 = load i32, i32* %src_voffset.addr, align 4, !tbaa !6
  %19 = load i32, i32* %sstride.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 %18, %19
  %20 = load i32, i32* %src_hoffset.addr, align 4, !tbaa !6
  %add3 = add nsw i32 %mul2, %20
  %arrayidx4 = getelementptr inbounds i8, i8* %17, i32 %add3
  store i8* %arrayidx4, i8** %base1, align 4, !tbaa !2
  %21 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %22 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %23 = load i8*, i8** %base1, align 4, !tbaa !2
  %24 = load i32, i32* %sstride.addr, align 4, !tbaa !6
  %25 = load i32, i32* %vsize.addr, align 4, !tbaa !6
  %26 = load i32, i32* %hsize.addr, align 4, !tbaa !6
  call void @cdef_copy_rect8_8bit_to_16bit_c(i16* %21, i32 %22, i8* %23, i32 %24, i32 %25, i32 %26)
  %27 = bitcast i8** %base1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @copy_rect(i16* %dst, i32 %dstride, i16* %src, i32 %sstride, i32 %v, i32 %h) #2 {
entry:
  %dst.addr = alloca i16*, align 4
  %dstride.addr = alloca i32, align 4
  %src.addr = alloca i16*, align 4
  %sstride.addr = alloca i32, align 4
  %v.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dstride, i32* %dstride.addr, align 4, !tbaa !6
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %sstride, i32* %sstride.addr, align 4, !tbaa !6
  store i32 %v, i32* %v.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc8, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %v.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  br label %for.end10

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %j, align 4, !tbaa !6
  %6 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = load i32, i32* %sstride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %9, %10
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul, %11
  %arrayidx = getelementptr inbounds i16, i16* %8, i32 %add
  %12 = load i16, i16* %arrayidx, align 2, !tbaa !23
  %13 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %dstride.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 %14, %15
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %add6 = add nsw i32 %mul5, %16
  %arrayidx7 = getelementptr inbounds i16, i16* %13, i32 %add6
  store i16 %12, i16* %arrayidx7, align 2, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc8

for.inc8:                                         ; preds = %for.end
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %inc9 = add nsw i32 %18, 1
  store i32 %inc9, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end10:                                        ; preds = %for.cond.cleanup
  ret void
}

declare void @av1_cdef_filter_fb(i8*, i16*, i32, i16*, i32, i32, [16 x i32]*, i32*, [16 x i32]*, i32, %struct.cdef_list*, i32, i32, i32, i32, i32) #4

declare void @aom_free(i8*) #4

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !3, i64 36}
!10 = !{!"CommonModeInfoParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !7, i64 28, !4, i64 32, !3, i64 36, !7, i64 40, !7, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!11 = !{!10, !7, i64 16}
!12 = !{!10, !7, i64 12}
!13 = !{!10, !7, i64 44}
!14 = !{!15, !4, i64 0}
!15 = !{!"", !4, i64 0, !4, i64 1}
!16 = !{!15, !4, i64 1}
!17 = !{!18, !4, i64 128}
!18 = !{!"MB_MODE_INFO", !19, i64 0, !20, i64 8, !4, i64 52, !7, i64 60, !4, i64 64, !22, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !15, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!19 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!20 = !{!"", !4, i64 0, !21, i64 32, !21, i64 34, !21, i64 36, !21, i64 38, !4, i64 40, !4, i64 41}
!21 = !{!"short", !4, i64 0}
!22 = !{!"", !4, i64 0, !4, i64 48}
!23 = !{!21, !21, i64 0}
!24 = !{!25, !4, i64 16264}
!25 = !{!"AV1Common", !26, i64 0, !28, i64 40, !7, i64 288, !7, i64 292, !7, i64 296, !7, i64 300, !7, i64 304, !7, i64 308, !4, i64 312, !29, i64 313, !4, i64 316, !7, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !30, i64 492, !4, i64 580, !4, i64 1284, !7, i64 1316, !7, i64 1320, !7, i64 1324, !31, i64 1328, !10, i64 1356, !32, i64 1420, !33, i64 10676, !3, i64 10848, !34, i64 10864, !35, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !36, i64 14880, !38, i64 15028, !39, i64 15168, !40, i64 15816, !4, i64 15836, !41, i64 16192, !3, i64 18128, !3, i64 18132, !44, i64 18136, !3, i64 18724, !45, i64 18728, !7, i64 18760, !4, i64 18764, !3, i64 18796, !7, i64 18800, !4, i64 18804, !4, i64 18836, !7, i64 18844, !7, i64 18848, !7, i64 18852, !7, i64 18856}
!26 = !{!"", !4, i64 0, !4, i64 1, !7, i64 4, !7, i64 8, !7, i64 12, !27, i64 16, !7, i64 32, !7, i64 36}
!27 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!28 = !{!"aom_internal_error_info", !4, i64 0, !7, i64 4, !4, i64 8, !7, i64 88, !4, i64 92}
!29 = !{!"_Bool", !4, i64 0}
!30 = !{!"scale_factors", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!31 = !{!"", !29, i64 0, !29, i64 1, !29, i64 2, !29, i64 3, !29, i64 4, !29, i64 5, !29, i64 6, !29, i64 7, !29, i64 8, !29, i64 9, !29, i64 10, !29, i64 11, !4, i64 12, !4, i64 13, !7, i64 16, !7, i64 20, !4, i64 24}
!32 = !{!"CommonQuantParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !29, i64 9240, !7, i64 9244, !7, i64 9248, !7, i64 9252}
!33 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !7, i64 164, !4, i64 168}
!34 = !{!"", !4, i64 0, !4, i64 3072}
!35 = !{!"loopfilter", !4, i64 0, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !7, i64 32}
!36 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !37, i64 80, !7, i64 84, !37, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!37 = !{!"long", !4, i64 0}
!38 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !4, i64 72, !7, i64 136}
!39 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !21, i64 644}
!40 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!41 = !{!"SequenceHeader", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !4, i64 16, !7, i64 20, !7, i64 24, !4, i64 28, !7, i64 32, !7, i64 36, !27, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !7, i64 112, !4, i64 116, !7, i64 244, !42, i64 248, !4, i64 264, !43, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!42 = !{!"aom_timing", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!43 = !{!"aom_dec_model_info", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!44 = !{!"CommonTileParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !4, i64 60, !4, i64 320, !7, i64 580, !7, i64 584}
!45 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !7, i64 20, !7, i64 24, !7, i64 28}
!46 = !{!25, !4, i64 16220}
!47 = !{!48, !7, i64 16}
!48 = !{!"macroblockd_plane", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !7, i64 16, !7, i64 20, !49, i64 24, !4, i64 44, !3, i64 84, !3, i64 88, !4, i64 92, !3, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!49 = !{!"buf_2d", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!50 = !{!48, !7, i64 20}
!51 = !{!38, !7, i64 0}
!52 = !{!48, !3, i64 24}
!53 = !{!48, !7, i64 40}
!54 = !{!25, !4, i64 16268}
!55 = !{!25, !4, i64 16269}
