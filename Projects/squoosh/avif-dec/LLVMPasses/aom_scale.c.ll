; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_scale/generic/aom_scale.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_scale/generic/aom_scale.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }

; Function Attrs: nounwind
define hidden void @aom_scale_frame(%struct.yv12_buffer_config* %src, %struct.yv12_buffer_config* %dst, i8* %temp_area, i8 zeroext %temp_height, i32 %hscale, i32 %hratio, i32 %vscale, i32 %vratio, i32 %interlaced, i32 %num_planes) #0 {
entry:
  %src.addr = alloca %struct.yv12_buffer_config*, align 4
  %dst.addr = alloca %struct.yv12_buffer_config*, align 4
  %temp_area.addr = alloca i8*, align 4
  %temp_height.addr = alloca i8, align 1
  %hscale.addr = alloca i32, align 4
  %hratio.addr = alloca i32, align 4
  %vscale.addr = alloca i32, align 4
  %vratio.addr = alloca i32, align 4
  %interlaced.addr = alloca i32, align 4
  %num_planes.addr = alloca i32, align 4
  %dw = alloca i32, align 4
  %dh = alloca i32, align 4
  %plane = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %is_uv = alloca i32, align 4
  %plane_dw = alloca i32, align 4
  %plane_dh = alloca i32, align 4
  %i = alloca i32, align 4
  %i48 = alloca i32, align 4
  store %struct.yv12_buffer_config* %src, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %dst, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  store i8* %temp_area, i8** %temp_area.addr, align 4, !tbaa !2
  store i8 %temp_height, i8* %temp_height.addr, align 1, !tbaa !6
  store i32 %hscale, i32* %hscale.addr, align 4, !tbaa !7
  store i32 %hratio, i32* %hratio.addr, align 4, !tbaa !7
  store i32 %vscale, i32* %vscale.addr, align 4, !tbaa !7
  store i32 %vratio, i32* %vratio.addr, align 4, !tbaa !7
  store i32 %interlaced, i32* %interlaced.addr, align 4, !tbaa !7
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !7
  %0 = bitcast i32* %dw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %hscale.addr, align 4, !tbaa !7
  %sub = sub i32 %1, 1
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 0
  %4 = bitcast %union.anon* %3 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %4, i32 0, i32 0
  %5 = load i32, i32* %y_width, align 4, !tbaa !6
  %6 = load i32, i32* %hratio.addr, align 4, !tbaa !7
  %mul = mul i32 %5, %6
  %add = add i32 %sub, %mul
  %7 = load i32, i32* %hscale.addr, align 4, !tbaa !7
  %div = udiv i32 %add, %7
  store i32 %div, i32* %dw, align 4, !tbaa !7
  %8 = bitcast i32* %dh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i32, i32* %vscale.addr, align 4, !tbaa !7
  %sub1 = sub i32 %9, 1
  %10 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %11 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %10, i32 0, i32 1
  %12 = bitcast %union.anon.0* %11 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %12, i32 0, i32 0
  %13 = load i32, i32* %y_height, align 4, !tbaa !6
  %14 = load i32, i32* %vratio.addr, align 4, !tbaa !7
  %mul2 = mul i32 %13, %14
  %add3 = add i32 %sub1, %mul2
  %15 = load i32, i32* %vscale.addr, align 4, !tbaa !7
  %div4 = udiv i32 %add3, %15
  store i32 %div4, i32* %dh, align 4, !tbaa !7
  %16 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  store i32 0, i32* %plane, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc77, %entry
  %17 = load i32, i32* %plane, align 4, !tbaa !7
  %18 = load i32, i32* %num_planes.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  br label %for.end79

for.body:                                         ; preds = %for.cond
  %20 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load i32, i32* %plane, align 4, !tbaa !7
  %cmp5 = icmp sgt i32 %21, 0
  %conv = zext i1 %cmp5 to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !7
  %22 = bitcast i32* %plane_dw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  %23 = load i32, i32* %dw, align 4, !tbaa !7
  %24 = load i32, i32* %is_uv, align 4, !tbaa !7
  %shr = ashr i32 %23, %24
  store i32 %shr, i32* %plane_dw, align 4, !tbaa !7
  %25 = bitcast i32* %plane_dh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  %26 = load i32, i32* %dh, align 4, !tbaa !7
  %27 = load i32, i32* %is_uv, align 4, !tbaa !7
  %shr6 = ashr i32 %26, %27
  store i32 %shr6, i32* %plane_dh, align 4, !tbaa !7
  %28 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %29 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %28, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %29 to [3 x i8*]*
  %30 = load i32, i32* %plane, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %30
  %31 = load i8*, i8** %arrayidx, align 4, !tbaa !6
  %32 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %33 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %32, i32 0, i32 4
  %strides = bitcast %union.anon.6* %33 to [2 x i32]*
  %34 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx7 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %34
  %35 = load i32, i32* %arrayidx7, align 4, !tbaa !6
  %36 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %37 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %36, i32 0, i32 0
  %widths = bitcast %union.anon* %37 to [2 x i32]*
  %38 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx8 = getelementptr inbounds [2 x i32], [2 x i32]* %widths, i32 0, i32 %38
  %39 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %40 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %41 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %40, i32 0, i32 1
  %heights = bitcast %union.anon.0* %41 to [2 x i32]*
  %42 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx9 = getelementptr inbounds [2 x i32], [2 x i32]* %heights, i32 0, i32 %42
  %43 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %44 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %45 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %44, i32 0, i32 5
  %buffers10 = bitcast %union.anon.8* %45 to [3 x i8*]*
  %46 = load i32, i32* %plane, align 4, !tbaa !7
  %arrayidx11 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers10, i32 0, i32 %46
  %47 = load i8*, i8** %arrayidx11, align 4, !tbaa !6
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %49 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 4
  %strides12 = bitcast %union.anon.6* %49 to [2 x i32]*
  %50 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx13 = getelementptr inbounds [2 x i32], [2 x i32]* %strides12, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx13, align 4, !tbaa !6
  %52 = load i32, i32* %plane_dw, align 4, !tbaa !7
  %53 = load i32, i32* %plane_dh, align 4, !tbaa !7
  %54 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %55 = load i8, i8* %temp_height.addr, align 1, !tbaa !6
  %56 = load i32, i32* %hscale.addr, align 4, !tbaa !7
  %57 = load i32, i32* %hratio.addr, align 4, !tbaa !7
  %58 = load i32, i32* %vscale.addr, align 4, !tbaa !7
  %59 = load i32, i32* %vratio.addr, align 4, !tbaa !7
  %60 = load i32, i32* %interlaced.addr, align 4, !tbaa !7
  call void @Scale2D(i8* %31, i32 %35, i32 %39, i32 %43, i8* %47, i32 %51, i32 %52, i32 %53, i8* %54, i8 zeroext %55, i32 %56, i32 %57, i32 %58, i32 %59, i32 %60)
  %61 = load i32, i32* %plane_dw, align 4, !tbaa !7
  %62 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %63 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %62, i32 0, i32 0
  %widths14 = bitcast %union.anon* %63 to [2 x i32]*
  %64 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx15 = getelementptr inbounds [2 x i32], [2 x i32]* %widths14, i32 0, i32 %64
  %65 = load i32, i32* %arrayidx15, align 4, !tbaa !6
  %cmp16 = icmp slt i32 %61, %65
  br i1 %cmp16, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %66 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #4
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %if.then
  %67 = load i32, i32* %i, align 4, !tbaa !7
  %68 = load i32, i32* %plane_dh, align 4, !tbaa !7
  %cmp19 = icmp slt i32 %67, %68
  br i1 %cmp19, label %for.body22, label %for.cond.cleanup21

for.cond.cleanup21:                               ; preds = %for.cond18
  store i32 5, i32* %cleanup.dest.slot, align 4
  %69 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  br label %for.end

for.body22:                                       ; preds = %for.cond18
  %70 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %71 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %70, i32 0, i32 5
  %buffers23 = bitcast %union.anon.8* %71 to [3 x i8*]*
  %72 = load i32, i32* %plane, align 4, !tbaa !7
  %arrayidx24 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers23, i32 0, i32 %72
  %73 = load i8*, i8** %arrayidx24, align 4, !tbaa !6
  %74 = load i32, i32* %i, align 4, !tbaa !7
  %75 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %76 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %75, i32 0, i32 4
  %strides25 = bitcast %union.anon.6* %76 to [2 x i32]*
  %77 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx26 = getelementptr inbounds [2 x i32], [2 x i32]* %strides25, i32 0, i32 %77
  %78 = load i32, i32* %arrayidx26, align 4, !tbaa !6
  %mul27 = mul nsw i32 %74, %78
  %add.ptr = getelementptr inbounds i8, i8* %73, i32 %mul27
  %79 = load i32, i32* %plane_dw, align 4, !tbaa !7
  %add.ptr28 = getelementptr inbounds i8, i8* %add.ptr, i32 %79
  %add.ptr29 = getelementptr inbounds i8, i8* %add.ptr28, i32 -1
  %80 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %81 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %80, i32 0, i32 5
  %buffers30 = bitcast %union.anon.8* %81 to [3 x i8*]*
  %82 = load i32, i32* %plane, align 4, !tbaa !7
  %arrayidx31 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers30, i32 0, i32 %82
  %83 = load i8*, i8** %arrayidx31, align 4, !tbaa !6
  %84 = load i32, i32* %i, align 4, !tbaa !7
  %85 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %86 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %85, i32 0, i32 4
  %strides32 = bitcast %union.anon.6* %86 to [2 x i32]*
  %87 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx33 = getelementptr inbounds [2 x i32], [2 x i32]* %strides32, i32 0, i32 %87
  %88 = load i32, i32* %arrayidx33, align 4, !tbaa !6
  %mul34 = mul nsw i32 %84, %88
  %89 = load i32, i32* %plane_dw, align 4, !tbaa !7
  %add35 = add nsw i32 %mul34, %89
  %sub36 = sub nsw i32 %add35, 2
  %arrayidx37 = getelementptr inbounds i8, i8* %83, i32 %sub36
  %90 = load i8, i8* %arrayidx37, align 1, !tbaa !6
  %conv38 = zext i8 %90 to i32
  %91 = trunc i32 %conv38 to i8
  %92 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %93 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %92, i32 0, i32 0
  %widths39 = bitcast %union.anon* %93 to [2 x i32]*
  %94 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx40 = getelementptr inbounds [2 x i32], [2 x i32]* %widths39, i32 0, i32 %94
  %95 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %96 = load i32, i32* %plane_dw, align 4, !tbaa !7
  %sub41 = sub nsw i32 %95, %96
  %add42 = add nsw i32 %sub41, 1
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr29, i8 %91, i32 %add42, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body22
  %97 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %97, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond18

for.end:                                          ; preds = %for.cond.cleanup21
  br label %if.end

if.end:                                           ; preds = %for.end, %for.body
  %98 = load i32, i32* %plane_dh, align 4, !tbaa !7
  %99 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %100 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %99, i32 0, i32 1
  %heights43 = bitcast %union.anon.0* %100 to [2 x i32]*
  %101 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx44 = getelementptr inbounds [2 x i32], [2 x i32]* %heights43, i32 0, i32 %101
  %102 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  %cmp45 = icmp slt i32 %98, %102
  br i1 %cmp45, label %if.then47, label %if.end76

if.then47:                                        ; preds = %if.end
  %103 = bitcast i32* %i48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #4
  %104 = load i32, i32* %plane_dh, align 4, !tbaa !7
  %sub49 = sub nsw i32 %104, 1
  store i32 %sub49, i32* %i48, align 4, !tbaa !7
  br label %for.cond50

for.cond50:                                       ; preds = %for.inc73, %if.then47
  %105 = load i32, i32* %i48, align 4, !tbaa !7
  %106 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %107 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %106, i32 0, i32 1
  %heights51 = bitcast %union.anon.0* %107 to [2 x i32]*
  %108 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx52 = getelementptr inbounds [2 x i32], [2 x i32]* %heights51, i32 0, i32 %108
  %109 = load i32, i32* %arrayidx52, align 4, !tbaa !6
  %cmp53 = icmp slt i32 %105, %109
  br i1 %cmp53, label %for.body56, label %for.cond.cleanup55

for.cond.cleanup55:                               ; preds = %for.cond50
  store i32 8, i32* %cleanup.dest.slot, align 4
  %110 = bitcast i32* %i48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  br label %for.end75

for.body56:                                       ; preds = %for.cond50
  %111 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %112 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %111, i32 0, i32 5
  %buffers57 = bitcast %union.anon.8* %112 to [3 x i8*]*
  %113 = load i32, i32* %plane, align 4, !tbaa !7
  %arrayidx58 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers57, i32 0, i32 %113
  %114 = load i8*, i8** %arrayidx58, align 4, !tbaa !6
  %115 = load i32, i32* %i48, align 4, !tbaa !7
  %116 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %117 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %116, i32 0, i32 4
  %strides59 = bitcast %union.anon.6* %117 to [2 x i32]*
  %118 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx60 = getelementptr inbounds [2 x i32], [2 x i32]* %strides59, i32 0, i32 %118
  %119 = load i32, i32* %arrayidx60, align 4, !tbaa !6
  %mul61 = mul nsw i32 %115, %119
  %add.ptr62 = getelementptr inbounds i8, i8* %114, i32 %mul61
  %120 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %121 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %120, i32 0, i32 5
  %buffers63 = bitcast %union.anon.8* %121 to [3 x i8*]*
  %122 = load i32, i32* %plane, align 4, !tbaa !7
  %arrayidx64 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers63, i32 0, i32 %122
  %123 = load i8*, i8** %arrayidx64, align 4, !tbaa !6
  %124 = load i32, i32* %plane_dh, align 4, !tbaa !7
  %sub65 = sub nsw i32 %124, 2
  %125 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %126 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %125, i32 0, i32 4
  %strides66 = bitcast %union.anon.6* %126 to [2 x i32]*
  %127 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx67 = getelementptr inbounds [2 x i32], [2 x i32]* %strides66, i32 0, i32 %127
  %128 = load i32, i32* %arrayidx67, align 4, !tbaa !6
  %mul68 = mul nsw i32 %sub65, %128
  %add.ptr69 = getelementptr inbounds i8, i8* %123, i32 %mul68
  %129 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %dst.addr, align 4, !tbaa !2
  %130 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %129, i32 0, i32 0
  %widths70 = bitcast %union.anon* %130 to [2 x i32]*
  %131 = load i32, i32* %is_uv, align 4, !tbaa !7
  %arrayidx71 = getelementptr inbounds [2 x i32], [2 x i32]* %widths70, i32 0, i32 %131
  %132 = load i32, i32* %arrayidx71, align 4, !tbaa !6
  %add72 = add nsw i32 %132, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr62, i8* align 1 %add.ptr69, i32 %add72, i1 false)
  br label %for.inc73

for.inc73:                                        ; preds = %for.body56
  %133 = load i32, i32* %i48, align 4, !tbaa !7
  %inc74 = add nsw i32 %133, 1
  store i32 %inc74, i32* %i48, align 4, !tbaa !7
  br label %for.cond50

for.end75:                                        ; preds = %for.cond.cleanup55
  br label %if.end76

if.end76:                                         ; preds = %for.end75, %if.end
  %134 = bitcast i32* %plane_dh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #4
  %135 = bitcast i32* %plane_dw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #4
  %136 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #4
  br label %for.inc77

for.inc77:                                        ; preds = %if.end76
  %137 = load i32, i32* %plane, align 4, !tbaa !7
  %inc78 = add nsw i32 %137, 1
  store i32 %inc78, i32* %plane, align 4, !tbaa !7
  br label %for.cond

for.end79:                                        ; preds = %for.cond.cleanup
  %138 = bitcast i32* %dh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i32* %dw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @Scale2D(i8* %source, i32 %source_pitch, i32 %source_width, i32 %source_height, i8* %dest, i32 %dest_pitch, i32 %dest_width, i32 %dest_height, i8* %temp_area, i8 zeroext %temp_area_height, i32 %hscale, i32 %hratio, i32 %vscale, i32 %vratio, i32 %interlaced) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %source_pitch.addr = alloca i32, align 4
  %source_width.addr = alloca i32, align 4
  %source_height.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_pitch.addr = alloca i32, align 4
  %dest_width.addr = alloca i32, align 4
  %dest_height.addr = alloca i32, align 4
  %temp_area.addr = alloca i8*, align 4
  %temp_area_height.addr = alloca i8, align 1
  %hscale.addr = alloca i32, align 4
  %hratio.addr = alloca i32, align 4
  %vscale.addr = alloca i32, align 4
  %vratio.addr = alloca i32, align 4
  %interlaced.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %bands = alloca i32, align 4
  %dest_band_height = alloca i32, align 4
  %source_band_height = alloca i32, align 4
  %Scale1Dv = alloca void (i8*, i32, i32, i32, i8*, i32, i32, i32)*, align 4
  %Scale1Dh = alloca void (i8*, i32, i32, i32, i8*, i32, i32, i32)*, align 4
  %horiz_line_scale = alloca void (i8*, i32, i8*, i32)*, align 4
  %vert_band_scale = alloca void (i8*, i32, i8*, i32, i32)*, align 4
  %ratio_scalable = alloca i32, align 4
  %interpolation = alloca i32, align 4
  %source_base = alloca i8*, align 4
  %line_src = alloca i8*, align 4
  %offset = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %source_pitch, i32* %source_pitch.addr, align 4, !tbaa !7
  store i32 %source_width, i32* %source_width.addr, align 4, !tbaa !7
  store i32 %source_height, i32* %source_height.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_pitch, i32* %dest_pitch.addr, align 4, !tbaa !7
  store i32 %dest_width, i32* %dest_width.addr, align 4, !tbaa !7
  store i32 %dest_height, i32* %dest_height.addr, align 4, !tbaa !7
  store i8* %temp_area, i8** %temp_area.addr, align 4, !tbaa !2
  store i8 %temp_area_height, i8* %temp_area_height.addr, align 1, !tbaa !6
  store i32 %hscale, i32* %hscale.addr, align 4, !tbaa !7
  store i32 %hratio, i32* %hratio.addr, align 4, !tbaa !7
  store i32 %vscale, i32* %vscale.addr, align 4, !tbaa !7
  store i32 %vratio, i32* %vratio.addr, align 4, !tbaa !7
  store i32 %interlaced, i32* %interlaced.addr, align 4, !tbaa !7
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %bands to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %dest_band_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %source_band_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  store void (i8*, i32, i32, i32, i8*, i32, i32, i32)* @scale1d_c, void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dv, align 4, !tbaa !2
  %7 = bitcast void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  store void (i8*, i32, i32, i32, i8*, i32, i32, i32)* @scale1d_c, void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dh, align 4, !tbaa !2
  %8 = bitcast void (i8*, i32, i8*, i32)** %horiz_line_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store void (i8*, i32, i8*, i32)* null, void (i8*, i32, i8*, i32)** %horiz_line_scale, align 4, !tbaa !2
  %9 = bitcast void (i8*, i32, i8*, i32, i32)** %vert_band_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  store void (i8*, i32, i8*, i32, i32)* null, void (i8*, i32, i8*, i32, i32)** %vert_band_scale, align 4, !tbaa !2
  %10 = bitcast i32* %ratio_scalable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  store i32 1, i32* %ratio_scalable, align 4, !tbaa !7
  %11 = bitcast i32* %interpolation to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  store i32 0, i32* %interpolation, align 4, !tbaa !7
  %12 = bitcast i8** %source_base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %line_src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i8*, i8** %source.addr, align 4, !tbaa !2
  store i8* %14, i8** %source_base, align 4, !tbaa !2
  %15 = load i32, i32* %source_pitch.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %15, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %16 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %source_height.addr, align 4, !tbaa !7
  %sub = sub i32 %17, 1
  store i32 %sub, i32* %offset, align 4, !tbaa !7
  %18 = load i32, i32* %source_pitch.addr, align 4, !tbaa !7
  %19 = load i32, i32* %offset, align 4, !tbaa !7
  %mul = mul nsw i32 %19, %18
  store i32 %mul, i32* %offset, align 4, !tbaa !7
  %20 = load i32, i32* %offset, align 4, !tbaa !7
  %21 = load i8*, i8** %source_base, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 %20
  store i8* %add.ptr, i8** %source_base, align 4, !tbaa !2
  %22 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %23 = load i32, i32* %hratio.addr, align 4, !tbaa !7
  %mul1 = mul i32 %23, 10
  %24 = load i32, i32* %hscale.addr, align 4, !tbaa !7
  %div = udiv i32 %mul1, %24
  switch i32 %div, label %sw.default [
    i32 8, label %sw.bb
    i32 6, label %sw.bb2
    i32 5, label %sw.bb3
  ]

sw.bb:                                            ; preds = %if.end
  store void (i8*, i32, i8*, i32)* @aom_horizontal_line_5_4_scale_c, void (i8*, i32, i8*, i32)** %horiz_line_scale, align 4, !tbaa !2
  br label %sw.epilog

sw.bb2:                                           ; preds = %if.end
  store void (i8*, i32, i8*, i32)* @aom_horizontal_line_5_3_scale_c, void (i8*, i32, i8*, i32)** %horiz_line_scale, align 4, !tbaa !2
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.end
  store void (i8*, i32, i8*, i32)* @aom_horizontal_line_2_1_scale_c, void (i8*, i32, i8*, i32)** %horiz_line_scale, align 4, !tbaa !2
  br label %sw.epilog

sw.default:                                       ; preds = %if.end
  store i32 0, i32* %ratio_scalable, align 4, !tbaa !7
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb3, %sw.bb2, %sw.bb
  %25 = load i32, i32* %vratio.addr, align 4, !tbaa !7
  %mul4 = mul i32 %25, 10
  %26 = load i32, i32* %vscale.addr, align 4, !tbaa !7
  %div5 = udiv i32 %mul4, %26
  switch i32 %div5, label %sw.default11 [
    i32 8, label %sw.bb6
    i32 6, label %sw.bb7
    i32 5, label %sw.bb8
  ]

sw.bb6:                                           ; preds = %sw.epilog
  store void (i8*, i32, i8*, i32, i32)* @aom_vertical_band_5_4_scale_c, void (i8*, i32, i8*, i32, i32)** %vert_band_scale, align 4, !tbaa !2
  store i32 5, i32* %source_band_height, align 4, !tbaa !7
  store i32 4, i32* %dest_band_height, align 4, !tbaa !7
  br label %sw.epilog12

sw.bb7:                                           ; preds = %sw.epilog
  store void (i8*, i32, i8*, i32, i32)* @aom_vertical_band_5_3_scale_c, void (i8*, i32, i8*, i32, i32)** %vert_band_scale, align 4, !tbaa !2
  store i32 5, i32* %source_band_height, align 4, !tbaa !7
  store i32 3, i32* %dest_band_height, align 4, !tbaa !7
  br label %sw.epilog12

sw.bb8:                                           ; preds = %sw.epilog
  %27 = load i32, i32* %interlaced.addr, align 4, !tbaa !7
  %tobool = icmp ne i32 %27, 0
  br i1 %tobool, label %if.then9, label %if.else

if.then9:                                         ; preds = %sw.bb8
  store void (i8*, i32, i8*, i32, i32)* @aom_vertical_band_2_1_scale_c, void (i8*, i32, i8*, i32, i32)** %vert_band_scale, align 4, !tbaa !2
  br label %if.end10

if.else:                                          ; preds = %sw.bb8
  store i32 1, i32* %interpolation, align 4, !tbaa !7
  store void (i8*, i32, i8*, i32, i32)* @aom_vertical_band_2_1_scale_i_c, void (i8*, i32, i8*, i32, i32)** %vert_band_scale, align 4, !tbaa !2
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then9
  store i32 2, i32* %source_band_height, align 4, !tbaa !7
  store i32 1, i32* %dest_band_height, align 4, !tbaa !7
  br label %sw.epilog12

sw.default11:                                     ; preds = %sw.epilog
  store i32 0, i32* %ratio_scalable, align 4, !tbaa !7
  br label %sw.epilog12

sw.epilog12:                                      ; preds = %sw.default11, %if.end10, %sw.bb7, %sw.bb6
  %28 = load i32, i32* %ratio_scalable, align 4, !tbaa !7
  %tobool13 = icmp ne i32 %28, 0
  br i1 %tobool13, label %if.then14, label %if.end59

if.then14:                                        ; preds = %sw.epilog12
  %29 = load i32, i32* %source_height.addr, align 4, !tbaa !7
  %30 = load i32, i32* %dest_height.addr, align 4, !tbaa !7
  %cmp15 = icmp eq i32 %29, %30
  br i1 %cmp15, label %if.then16, label %if.end20

if.then16:                                        ; preds = %if.then14
  store i32 0, i32* %k, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then16
  %31 = load i32, i32* %k, align 4, !tbaa !7
  %32 = load i32, i32* %dest_height.addr, align 4, !tbaa !7
  %cmp17 = icmp ult i32 %31, %32
  br i1 %cmp17, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %33 = load void (i8*, i32, i8*, i32)*, void (i8*, i32, i8*, i32)** %horiz_line_scale, align 4, !tbaa !2
  %34 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %35 = load i32, i32* %source_width.addr, align 4, !tbaa !7
  %36 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %37 = load i32, i32* %dest_width.addr, align 4, !tbaa !7
  call void %33(i8* %34, i32 %35, i8* %36, i32 %37)
  %38 = load i32, i32* %source_pitch.addr, align 4, !tbaa !7
  %39 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr18 = getelementptr inbounds i8, i8* %39, i32 %38
  store i8* %add.ptr18, i8** %source.addr, align 4, !tbaa !2
  %40 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %41 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %add.ptr19 = getelementptr inbounds i8, i8* %41, i32 %40
  store i8* %add.ptr19, i8** %dest.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %42 = load i32, i32* %k, align 4, !tbaa !7
  %inc = add i32 %42, 1
  store i32 %inc, i32* %k, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %if.then14
  %43 = load i32, i32* %interpolation, align 4, !tbaa !7
  %tobool21 = icmp ne i32 %43, 0
  br i1 %tobool21, label %if.then22, label %if.end26

if.then22:                                        ; preds = %if.end20
  %44 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %45 = load i8*, i8** %source_base, align 4, !tbaa !2
  %cmp23 = icmp ult i8* %44, %45
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.then22
  %46 = load i8*, i8** %source_base, align 4, !tbaa !2
  store i8* %46, i8** %source.addr, align 4, !tbaa !2
  br label %if.end25

if.end25:                                         ; preds = %if.then24, %if.then22
  %47 = load void (i8*, i32, i8*, i32)*, void (i8*, i32, i8*, i32)** %horiz_line_scale, align 4, !tbaa !2
  %48 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %49 = load i32, i32* %source_width.addr, align 4, !tbaa !7
  %50 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %51 = load i32, i32* %dest_width.addr, align 4, !tbaa !7
  call void %47(i8* %48, i32 %49, i8* %50, i32 %51)
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.end20
  store i32 0, i32* %k, align 4, !tbaa !7
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc56, %if.end26
  %52 = load i32, i32* %k, align 4, !tbaa !7
  %53 = load i32, i32* %dest_height.addr, align 4, !tbaa !7
  %54 = load i32, i32* %dest_band_height, align 4, !tbaa !7
  %add = add i32 %53, %54
  %sub28 = sub i32 %add, 1
  %55 = load i32, i32* %dest_band_height, align 4, !tbaa !7
  %div29 = udiv i32 %sub28, %55
  %cmp30 = icmp ult i32 %52, %div29
  br i1 %cmp30, label %for.body31, label %for.end58

for.body31:                                       ; preds = %for.cond27
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc43, %for.body31
  %56 = load i32, i32* %i, align 4, !tbaa !7
  %57 = load i32, i32* %source_band_height, align 4, !tbaa !7
  %cmp33 = icmp ult i32 %56, %57
  br i1 %cmp33, label %for.body34, label %for.end45

for.body34:                                       ; preds = %for.cond32
  %58 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %59 = load i32, i32* %i, align 4, !tbaa !7
  %60 = load i32, i32* %source_pitch.addr, align 4, !tbaa !7
  %mul35 = mul i32 %59, %60
  %add.ptr36 = getelementptr inbounds i8, i8* %58, i32 %mul35
  store i8* %add.ptr36, i8** %line_src, align 4, !tbaa !2
  %61 = load i8*, i8** %line_src, align 4, !tbaa !2
  %62 = load i8*, i8** %source_base, align 4, !tbaa !2
  %cmp37 = icmp ult i8* %61, %62
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %for.body34
  %63 = load i8*, i8** %source_base, align 4, !tbaa !2
  store i8* %63, i8** %line_src, align 4, !tbaa !2
  br label %if.end39

if.end39:                                         ; preds = %if.then38, %for.body34
  %64 = load void (i8*, i32, i8*, i32)*, void (i8*, i32, i8*, i32)** %horiz_line_scale, align 4, !tbaa !2
  %65 = load i8*, i8** %line_src, align 4, !tbaa !2
  %66 = load i32, i32* %source_width.addr, align 4, !tbaa !7
  %67 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %68 = load i32, i32* %i, align 4, !tbaa !7
  %add40 = add i32 %68, 1
  %69 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %mul41 = mul i32 %add40, %69
  %add.ptr42 = getelementptr inbounds i8, i8* %67, i32 %mul41
  %70 = load i32, i32* %dest_width.addr, align 4, !tbaa !7
  call void %64(i8* %65, i32 %66, i8* %add.ptr42, i32 %70)
  br label %for.inc43

for.inc43:                                        ; preds = %if.end39
  %71 = load i32, i32* %i, align 4, !tbaa !7
  %inc44 = add i32 %71, 1
  store i32 %inc44, i32* %i, align 4, !tbaa !7
  br label %for.cond32

for.end45:                                        ; preds = %for.cond32
  %72 = load void (i8*, i32, i8*, i32, i32)*, void (i8*, i32, i8*, i32, i32)** %vert_band_scale, align 4, !tbaa !2
  %73 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %74 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %add.ptr46 = getelementptr inbounds i8, i8* %73, i32 %74
  %75 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %76 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %77 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %78 = load i32, i32* %dest_width.addr, align 4, !tbaa !7
  call void %72(i8* %add.ptr46, i32 %75, i8* %76, i32 %77, i32 %78)
  %79 = load i32, i32* %interpolation, align 4, !tbaa !7
  %tobool47 = icmp ne i32 %79, 0
  br i1 %tobool47, label %if.then48, label %if.end51

if.then48:                                        ; preds = %for.end45
  %80 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %81 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %82 = load i32, i32* %source_band_height, align 4, !tbaa !7
  %83 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %mul49 = mul i32 %82, %83
  %add.ptr50 = getelementptr inbounds i8, i8* %81, i32 %mul49
  %84 = load i32, i32* %dest_width.addr, align 4, !tbaa !7
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %80, i8* align 1 %add.ptr50, i32 %84, i1 false)
  br label %if.end51

if.end51:                                         ; preds = %if.then48, %for.end45
  %85 = load i32, i32* %source_band_height, align 4, !tbaa !7
  %86 = load i32, i32* %source_pitch.addr, align 4, !tbaa !7
  %mul52 = mul i32 %85, %86
  %87 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr53 = getelementptr inbounds i8, i8* %87, i32 %mul52
  store i8* %add.ptr53, i8** %source.addr, align 4, !tbaa !2
  %88 = load i32, i32* %dest_band_height, align 4, !tbaa !7
  %89 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %mul54 = mul i32 %88, %89
  %90 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %add.ptr55 = getelementptr inbounds i8, i8* %90, i32 %mul54
  store i8* %add.ptr55, i8** %dest.addr, align 4, !tbaa !2
  br label %for.inc56

for.inc56:                                        ; preds = %if.end51
  %91 = load i32, i32* %k, align 4, !tbaa !7
  %inc57 = add i32 %91, 1
  store i32 %inc57, i32* %k, align 4, !tbaa !7
  br label %for.cond27

for.end58:                                        ; preds = %for.cond27
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end59:                                         ; preds = %sw.epilog12
  %92 = load i32, i32* %hscale.addr, align 4, !tbaa !7
  %cmp60 = icmp eq i32 %92, 2
  br i1 %cmp60, label %land.lhs.true, label %if.end63

land.lhs.true:                                    ; preds = %if.end59
  %93 = load i32, i32* %hratio.addr, align 4, !tbaa !7
  %cmp61 = icmp eq i32 %93, 1
  br i1 %cmp61, label %if.then62, label %if.end63

if.then62:                                        ; preds = %land.lhs.true
  store void (i8*, i32, i32, i32, i8*, i32, i32, i32)* @scale1d_2t1_ps, void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dh, align 4, !tbaa !2
  br label %if.end63

if.end63:                                         ; preds = %if.then62, %land.lhs.true, %if.end59
  %94 = load i32, i32* %vscale.addr, align 4, !tbaa !7
  %cmp64 = icmp eq i32 %94, 2
  br i1 %cmp64, label %land.lhs.true65, label %if.end72

land.lhs.true65:                                  ; preds = %if.end63
  %95 = load i32, i32* %vratio.addr, align 4, !tbaa !7
  %cmp66 = icmp eq i32 %95, 1
  br i1 %cmp66, label %if.then67, label %if.end72

if.then67:                                        ; preds = %land.lhs.true65
  %96 = load i32, i32* %interlaced.addr, align 4, !tbaa !7
  %tobool68 = icmp ne i32 %96, 0
  br i1 %tobool68, label %if.then69, label %if.else70

if.then69:                                        ; preds = %if.then67
  store void (i8*, i32, i32, i32, i8*, i32, i32, i32)* @scale1d_2t1_ps, void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dv, align 4, !tbaa !2
  br label %if.end71

if.else70:                                        ; preds = %if.then67
  store void (i8*, i32, i32, i32, i8*, i32, i32, i32)* @scale1d_2t1_i, void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dv, align 4, !tbaa !2
  br label %if.end71

if.end71:                                         ; preds = %if.else70, %if.then69
  br label %if.end72

if.end72:                                         ; preds = %if.end71, %land.lhs.true65, %if.end63
  %97 = load i32, i32* %source_height.addr, align 4, !tbaa !7
  %98 = load i32, i32* %dest_height.addr, align 4, !tbaa !7
  %cmp73 = icmp eq i32 %97, %98
  br i1 %cmp73, label %if.then74, label %if.end84

if.then74:                                        ; preds = %if.end72
  store i32 0, i32* %k, align 4, !tbaa !7
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc81, %if.then74
  %99 = load i32, i32* %k, align 4, !tbaa !7
  %100 = load i32, i32* %dest_height.addr, align 4, !tbaa !7
  %cmp76 = icmp ult i32 %99, %100
  br i1 %cmp76, label %for.body77, label %for.end83

for.body77:                                       ; preds = %for.cond75
  %101 = load void (i8*, i32, i32, i32, i8*, i32, i32, i32)*, void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dh, align 4, !tbaa !2
  %102 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %103 = load i32, i32* %hscale.addr, align 4, !tbaa !7
  %104 = load i32, i32* %source_width.addr, align 4, !tbaa !7
  %add78 = add i32 %104, 1
  %105 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %106 = load i32, i32* %hratio.addr, align 4, !tbaa !7
  %107 = load i32, i32* %dest_width.addr, align 4, !tbaa !7
  call void %101(i8* %102, i32 1, i32 %103, i32 %add78, i8* %105, i32 1, i32 %106, i32 %107)
  %108 = load i32, i32* %source_pitch.addr, align 4, !tbaa !7
  %109 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr79 = getelementptr inbounds i8, i8* %109, i32 %108
  store i8* %add.ptr79, i8** %source.addr, align 4, !tbaa !2
  %110 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %111 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %add.ptr80 = getelementptr inbounds i8, i8* %111, i32 %110
  store i8* %add.ptr80, i8** %dest.addr, align 4, !tbaa !2
  br label %for.inc81

for.inc81:                                        ; preds = %for.body77
  %112 = load i32, i32* %k, align 4, !tbaa !7
  %inc82 = add i32 %112, 1
  store i32 %inc82, i32* %k, align 4, !tbaa !7
  br label %for.cond75

for.end83:                                        ; preds = %for.cond75
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end84:                                         ; preds = %if.end72
  %113 = load i32, i32* %dest_height.addr, align 4, !tbaa !7
  %114 = load i32, i32* %source_height.addr, align 4, !tbaa !7
  %cmp85 = icmp ugt i32 %113, %114
  br i1 %cmp85, label %if.then86, label %if.else90

if.then86:                                        ; preds = %if.end84
  %115 = load i8, i8* %temp_area_height.addr, align 1, !tbaa !6
  %conv = zext i8 %115 to i32
  %sub87 = sub nsw i32 %conv, 1
  store i32 %sub87, i32* %dest_band_height, align 4, !tbaa !7
  %116 = load i32, i32* %dest_band_height, align 4, !tbaa !7
  %117 = load i32, i32* %source_height.addr, align 4, !tbaa !7
  %mul88 = mul i32 %116, %117
  %118 = load i32, i32* %dest_height.addr, align 4, !tbaa !7
  %div89 = udiv i32 %mul88, %118
  store i32 %div89, i32* %source_band_height, align 4, !tbaa !7
  br label %if.end95

if.else90:                                        ; preds = %if.end84
  %119 = load i8, i8* %temp_area_height.addr, align 1, !tbaa !6
  %conv91 = zext i8 %119 to i32
  %sub92 = sub nsw i32 %conv91, 1
  store i32 %sub92, i32* %source_band_height, align 4, !tbaa !7
  %120 = load i32, i32* %source_band_height, align 4, !tbaa !7
  %121 = load i32, i32* %vratio.addr, align 4, !tbaa !7
  %mul93 = mul i32 %120, %121
  %122 = load i32, i32* %vscale.addr, align 4, !tbaa !7
  %div94 = udiv i32 %mul93, %122
  store i32 %div94, i32* %dest_band_height, align 4, !tbaa !7
  br label %if.end95

if.end95:                                         ; preds = %if.else90, %if.then86
  %123 = load void (i8*, i32, i32, i32, i8*, i32, i32, i32)*, void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dh, align 4, !tbaa !2
  %124 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %125 = load i32, i32* %hscale.addr, align 4, !tbaa !7
  %126 = load i32, i32* %source_width.addr, align 4, !tbaa !7
  %add96 = add i32 %126, 1
  %127 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %128 = load i32, i32* %hratio.addr, align 4, !tbaa !7
  %129 = load i32, i32* %dest_width.addr, align 4, !tbaa !7
  call void %123(i8* %124, i32 1, i32 %125, i32 %add96, i8* %127, i32 1, i32 %128, i32 %129)
  %130 = load i32, i32* %dest_height.addr, align 4, !tbaa !7
  %131 = load i32, i32* %dest_band_height, align 4, !tbaa !7
  %add97 = add i32 %130, %131
  %sub98 = sub i32 %add97, 1
  %132 = load i32, i32* %dest_band_height, align 4, !tbaa !7
  %div99 = udiv i32 %sub98, %132
  store i32 %div99, i32* %bands, align 4, !tbaa !7
  store i32 0, i32* %k, align 4, !tbaa !7
  br label %for.cond100

for.cond100:                                      ; preds = %for.inc144, %if.end95
  %133 = load i32, i32* %k, align 4, !tbaa !7
  %134 = load i32, i32* %bands, align 4, !tbaa !7
  %cmp101 = icmp ult i32 %133, %134
  br i1 %cmp101, label %for.body103, label %for.end146

for.body103:                                      ; preds = %for.cond100
  store i32 1, i32* %i, align 4, !tbaa !7
  br label %for.cond104

for.cond104:                                      ; preds = %for.inc126, %for.body103
  %135 = load i32, i32* %i, align 4, !tbaa !7
  %136 = load i32, i32* %source_band_height, align 4, !tbaa !7
  %add105 = add i32 %136, 1
  %cmp106 = icmp ult i32 %135, %add105
  br i1 %cmp106, label %for.body108, label %for.end128

for.body108:                                      ; preds = %for.cond104
  %137 = load i32, i32* %k, align 4, !tbaa !7
  %138 = load i32, i32* %source_band_height, align 4, !tbaa !7
  %mul109 = mul i32 %137, %138
  %139 = load i32, i32* %i, align 4, !tbaa !7
  %add110 = add i32 %mul109, %139
  %140 = load i32, i32* %source_height.addr, align 4, !tbaa !7
  %cmp111 = icmp ult i32 %add110, %140
  br i1 %cmp111, label %if.then113, label %if.else119

if.then113:                                       ; preds = %for.body108
  %141 = load void (i8*, i32, i32, i32, i8*, i32, i32, i32)*, void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dh, align 4, !tbaa !2
  %142 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %143 = load i32, i32* %i, align 4, !tbaa !7
  %144 = load i32, i32* %source_pitch.addr, align 4, !tbaa !7
  %mul114 = mul i32 %143, %144
  %add.ptr115 = getelementptr inbounds i8, i8* %142, i32 %mul114
  %145 = load i32, i32* %hscale.addr, align 4, !tbaa !7
  %146 = load i32, i32* %source_width.addr, align 4, !tbaa !7
  %add116 = add i32 %146, 1
  %147 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %148 = load i32, i32* %i, align 4, !tbaa !7
  %149 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %mul117 = mul i32 %148, %149
  %add.ptr118 = getelementptr inbounds i8, i8* %147, i32 %mul117
  %150 = load i32, i32* %hratio.addr, align 4, !tbaa !7
  %151 = load i32, i32* %dest_width.addr, align 4, !tbaa !7
  call void %141(i8* %add.ptr115, i32 1, i32 %145, i32 %add116, i8* %add.ptr118, i32 1, i32 %150, i32 %151)
  br label %if.end125

if.else119:                                       ; preds = %for.body108
  %152 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %153 = load i32, i32* %i, align 4, !tbaa !7
  %154 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %mul120 = mul i32 %153, %154
  %add.ptr121 = getelementptr inbounds i8, i8* %152, i32 %mul120
  %155 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %156 = load i32, i32* %i, align 4, !tbaa !7
  %sub122 = sub i32 %156, 1
  %157 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %mul123 = mul i32 %sub122, %157
  %add.ptr124 = getelementptr inbounds i8, i8* %155, i32 %mul123
  %158 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr121, i8* align 1 %add.ptr124, i32 %158, i1 false)
  br label %if.end125

if.end125:                                        ; preds = %if.else119, %if.then113
  br label %for.inc126

for.inc126:                                       ; preds = %if.end125
  %159 = load i32, i32* %i, align 4, !tbaa !7
  %inc127 = add i32 %159, 1
  store i32 %inc127, i32* %i, align 4, !tbaa !7
  br label %for.cond104

for.end128:                                       ; preds = %for.cond104
  store i32 0, i32* %j, align 4, !tbaa !7
  br label %for.cond129

for.cond129:                                      ; preds = %for.inc135, %for.end128
  %160 = load i32, i32* %j, align 4, !tbaa !7
  %161 = load i32, i32* %dest_width.addr, align 4, !tbaa !7
  %cmp130 = icmp ult i32 %160, %161
  br i1 %cmp130, label %for.body132, label %for.end137

for.body132:                                      ; preds = %for.cond129
  %162 = load void (i8*, i32, i32, i32, i8*, i32, i32, i32)*, void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dv, align 4, !tbaa !2
  %163 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %164 = load i32, i32* %j, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %163, i32 %164
  %165 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %166 = load i32, i32* %vscale.addr, align 4, !tbaa !7
  %167 = load i32, i32* %source_band_height, align 4, !tbaa !7
  %add133 = add i32 %167, 1
  %168 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %169 = load i32, i32* %j, align 4, !tbaa !7
  %arrayidx134 = getelementptr inbounds i8, i8* %168, i32 %169
  %170 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %171 = load i32, i32* %vratio.addr, align 4, !tbaa !7
  %172 = load i32, i32* %dest_band_height, align 4, !tbaa !7
  call void %162(i8* %arrayidx, i32 %165, i32 %166, i32 %add133, i8* %arrayidx134, i32 %170, i32 %171, i32 %172)
  br label %for.inc135

for.inc135:                                       ; preds = %for.body132
  %173 = load i32, i32* %j, align 4, !tbaa !7
  %inc136 = add i32 %173, 1
  store i32 %inc136, i32* %j, align 4, !tbaa !7
  br label %for.cond129

for.end137:                                       ; preds = %for.cond129
  %174 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %175 = load i8*, i8** %temp_area.addr, align 4, !tbaa !2
  %176 = load i32, i32* %source_band_height, align 4, !tbaa !7
  %177 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %mul138 = mul i32 %176, %177
  %add.ptr139 = getelementptr inbounds i8, i8* %175, i32 %mul138
  %178 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %174, i8* align 1 %add.ptr139, i32 %178, i1 false)
  %179 = load i32, i32* %source_band_height, align 4, !tbaa !7
  %180 = load i32, i32* %source_pitch.addr, align 4, !tbaa !7
  %mul140 = mul i32 %179, %180
  %181 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr141 = getelementptr inbounds i8, i8* %181, i32 %mul140
  store i8* %add.ptr141, i8** %source.addr, align 4, !tbaa !2
  %182 = load i32, i32* %dest_band_height, align 4, !tbaa !7
  %183 = load i32, i32* %dest_pitch.addr, align 4, !tbaa !7
  %mul142 = mul i32 %182, %183
  %184 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %add.ptr143 = getelementptr inbounds i8, i8* %184, i32 %mul142
  store i8* %add.ptr143, i8** %dest.addr, align 4, !tbaa !2
  br label %for.inc144

for.inc144:                                       ; preds = %for.end137
  %185 = load i32, i32* %k, align 4, !tbaa !7
  %inc145 = add i32 %185, 1
  store i32 %inc145, i32* %k, align 4, !tbaa !7
  br label %for.cond100

for.end146:                                       ; preds = %for.cond100
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end146, %for.end83, %for.end58, %for.end
  %186 = bitcast i8** %line_src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #4
  %187 = bitcast i8** %source_base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #4
  %188 = bitcast i32* %interpolation to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #4
  %189 = bitcast i32* %ratio_scalable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #4
  %190 = bitcast void (i8*, i32, i8*, i32, i32)** %vert_band_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #4
  %191 = bitcast void (i8*, i32, i8*, i32)** %horiz_line_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #4
  %192 = bitcast void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #4
  %193 = bitcast void (i8*, i32, i32, i32, i8*, i32, i32, i32)** %Scale1Dv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #4
  %194 = bitcast i32* %source_band_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #4
  %195 = bitcast i32* %dest_band_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #4
  %196 = bitcast i32* %bands to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  %197 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #4
  %198 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #4
  %199 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal void @scale1d_c(i8* %source, i32 %source_step, i32 %source_scale, i32 %source_length, i8* %dest, i32 %dest_step, i32 %dest_scale, i32 %dest_length) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %source_step.addr = alloca i32, align 4
  %source_scale.addr = alloca i32, align 4
  %source_length.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_step.addr = alloca i32, align 4
  %dest_scale.addr = alloca i32, align 4
  %dest_length.addr = alloca i32, align 4
  %dest_end = alloca i8*, align 4
  %round_value = alloca i32, align 4
  %left_modifier = alloca i32, align 4
  %right_modifier = alloca i32, align 4
  %left_pixel = alloca i8, align 1
  %right_pixel = alloca i8, align 1
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %source_step, i32* %source_step.addr, align 4, !tbaa !7
  store i32 %source_scale, i32* %source_scale.addr, align 4, !tbaa !7
  store i32 %source_length, i32* %source_length.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_step, i32* %dest_step.addr, align 4, !tbaa !7
  store i32 %dest_scale, i32* %dest_scale.addr, align 4, !tbaa !7
  store i32 %dest_length, i32* %dest_length.addr, align 4, !tbaa !7
  %0 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dest_length.addr, align 4, !tbaa !7
  %3 = load i32, i32* %dest_step.addr, align 4, !tbaa !7
  %mul = mul i32 %2, %3
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %mul
  store i8* %add.ptr, i8** %dest_end, align 4, !tbaa !2
  %4 = bitcast i32* %round_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i32, i32* %dest_scale.addr, align 4, !tbaa !7
  %div = udiv i32 %5, 2
  store i32 %div, i32* %round_value, align 4, !tbaa !7
  %6 = bitcast i32* %left_modifier to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i32, i32* %dest_scale.addr, align 4, !tbaa !7
  store i32 %7, i32* %left_modifier, align 4, !tbaa !7
  %8 = bitcast i32* %right_modifier to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store i32 0, i32* %right_modifier, align 4, !tbaa !7
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %left_pixel) #4
  %9 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 0
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !6
  store i8 %10, i8* %left_pixel, align 1, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %right_pixel) #4
  %11 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %12 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %arrayidx1 = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load i8, i8* %arrayidx1, align 1, !tbaa !6
  store i8 %13, i8* %right_pixel, align 1, !tbaa !6
  %14 = load i32, i32* %source_length.addr, align 4, !tbaa !7
  br label %while.cond

while.cond:                                       ; preds = %while.end, %entry
  %15 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %16 = load i8*, i8** %dest_end, align 4, !tbaa !2
  %cmp = icmp ult i8* %15, %16
  br i1 %cmp, label %while.body, label %while.end17

while.body:                                       ; preds = %while.cond
  %17 = load i32, i32* %left_modifier, align 4, !tbaa !7
  %18 = load i8, i8* %left_pixel, align 1, !tbaa !6
  %conv = zext i8 %18 to i32
  %mul2 = mul i32 %17, %conv
  %19 = load i32, i32* %right_modifier, align 4, !tbaa !7
  %20 = load i8, i8* %right_pixel, align 1, !tbaa !6
  %conv3 = zext i8 %20 to i32
  %mul4 = mul i32 %19, %conv3
  %add = add i32 %mul2, %mul4
  %21 = load i32, i32* %round_value, align 4, !tbaa !7
  %add5 = add i32 %add, %21
  %22 = load i32, i32* %dest_scale.addr, align 4, !tbaa !7
  %div6 = udiv i32 %add5, %22
  %conv7 = trunc i32 %div6 to i8
  %23 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  store i8 %conv7, i8* %23, align 1, !tbaa !6
  %24 = load i32, i32* %source_scale.addr, align 4, !tbaa !7
  %25 = load i32, i32* %right_modifier, align 4, !tbaa !7
  %add8 = add i32 %25, %24
  store i32 %add8, i32* %right_modifier, align 4, !tbaa !7
  br label %while.cond9

while.cond9:                                      ; preds = %while.body12, %while.body
  %26 = load i32, i32* %right_modifier, align 4, !tbaa !7
  %27 = load i32, i32* %dest_scale.addr, align 4, !tbaa !7
  %cmp10 = icmp ugt i32 %26, %27
  br i1 %cmp10, label %while.body12, label %while.end

while.body12:                                     ; preds = %while.cond9
  %28 = load i32, i32* %dest_scale.addr, align 4, !tbaa !7
  %29 = load i32, i32* %right_modifier, align 4, !tbaa !7
  %sub = sub i32 %29, %28
  store i32 %sub, i32* %right_modifier, align 4, !tbaa !7
  %30 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %31 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr13 = getelementptr inbounds i8, i8* %31, i32 %30
  store i8* %add.ptr13, i8** %source.addr, align 4, !tbaa !2
  %32 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %32, i32 0
  %33 = load i8, i8* %arrayidx14, align 1, !tbaa !6
  store i8 %33, i8* %left_pixel, align 1, !tbaa !6
  %34 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %35 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %arrayidx15 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx15, align 1, !tbaa !6
  store i8 %36, i8* %right_pixel, align 1, !tbaa !6
  br label %while.cond9

while.end:                                        ; preds = %while.cond9
  %37 = load i32, i32* %dest_scale.addr, align 4, !tbaa !7
  %38 = load i32, i32* %right_modifier, align 4, !tbaa !7
  %sub16 = sub i32 %37, %38
  store i32 %sub16, i32* %left_modifier, align 4, !tbaa !7
  br label %while.cond

while.end17:                                      ; preds = %while.cond
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %right_pixel) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %left_pixel) #4
  %39 = bitcast i32* %right_modifier to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %40 = bitcast i32* %left_modifier to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %round_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  ret void
}

declare void @aom_horizontal_line_5_4_scale_c(i8*, i32, i8*, i32) #3

declare void @aom_horizontal_line_5_3_scale_c(i8*, i32, i8*, i32) #3

declare void @aom_horizontal_line_2_1_scale_c(i8*, i32, i8*, i32) #3

declare void @aom_vertical_band_5_4_scale_c(i8*, i32, i8*, i32, i32) #3

declare void @aom_vertical_band_5_3_scale_c(i8*, i32, i8*, i32, i32) #3

declare void @aom_vertical_band_2_1_scale_c(i8*, i32, i8*, i32, i32) #3

declare void @aom_vertical_band_2_1_scale_i_c(i8*, i32, i8*, i32, i32) #3

; Function Attrs: nounwind
define internal void @scale1d_2t1_ps(i8* %source, i32 %source_step, i32 %source_scale, i32 %source_length, i8* %dest, i32 %dest_step, i32 %dest_scale, i32 %dest_length) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %source_step.addr = alloca i32, align 4
  %source_scale.addr = alloca i32, align 4
  %source_length.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_step.addr = alloca i32, align 4
  %dest_scale.addr = alloca i32, align 4
  %dest_length.addr = alloca i32, align 4
  %dest_end = alloca i8*, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %source_step, i32* %source_step.addr, align 4, !tbaa !7
  store i32 %source_scale, i32* %source_scale.addr, align 4, !tbaa !7
  store i32 %source_length, i32* %source_length.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_step, i32* %dest_step.addr, align 4, !tbaa !7
  store i32 %dest_scale, i32* %dest_scale.addr, align 4, !tbaa !7
  store i32 %dest_length, i32* %dest_length.addr, align 4, !tbaa !7
  %0 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dest_length.addr, align 4, !tbaa !7
  %3 = load i32, i32* %dest_step.addr, align 4, !tbaa !7
  %mul = mul i32 %2, %3
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %mul
  store i8* %add.ptr, i8** %dest_end, align 4, !tbaa !2
  %4 = load i32, i32* %source_length.addr, align 4, !tbaa !7
  %5 = load i32, i32* %source_scale.addr, align 4, !tbaa !7
  %6 = load i32, i32* %dest_scale.addr, align 4, !tbaa !7
  %7 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %mul1 = mul nsw i32 %7, 2
  store i32 %mul1, i32* %source_step.addr, align 4, !tbaa !7
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %8 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %dest_end, align 4, !tbaa !2
  %cmp = icmp ult i8* %8, %9
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %11 = load i8, i8* %10, align 1, !tbaa !6
  %12 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  store i8 %11, i8* %12, align 1, !tbaa !6
  %13 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %14 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds i8, i8* %14, i32 %13
  store i8* %add.ptr2, i8** %source.addr, align 4, !tbaa !2
  %15 = load i32, i32* %dest_step.addr, align 4, !tbaa !7
  %16 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %add.ptr3 = getelementptr inbounds i8, i8* %16, i32 %15
  store i8* %add.ptr3, i8** %dest.addr, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %17 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  ret void
}

; Function Attrs: nounwind
define internal void @scale1d_2t1_i(i8* %source, i32 %source_step, i32 %source_scale, i32 %source_length, i8* %dest, i32 %dest_step, i32 %dest_scale, i32 %dest_length) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %source_step.addr = alloca i32, align 4
  %source_scale.addr = alloca i32, align 4
  %source_length.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %dest_step.addr = alloca i32, align 4
  %dest_scale.addr = alloca i32, align 4
  %dest_length.addr = alloca i32, align 4
  %dest_end = alloca i8*, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !2
  store i32 %source_step, i32* %source_step.addr, align 4, !tbaa !7
  store i32 %source_scale, i32* %source_scale.addr, align 4, !tbaa !7
  store i32 %source_length, i32* %source_length.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  store i32 %dest_step, i32* %dest_step.addr, align 4, !tbaa !7
  store i32 %dest_scale, i32* %dest_scale.addr, align 4, !tbaa !7
  store i32 %dest_length, i32* %dest_length.addr, align 4, !tbaa !7
  %0 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %2 = load i32, i32* %dest_length.addr, align 4, !tbaa !7
  %3 = load i32, i32* %dest_step.addr, align 4, !tbaa !7
  %mul = mul i32 %2, %3
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %mul
  store i8* %add.ptr, i8** %dest_end, align 4, !tbaa !2
  %4 = load i32, i32* %source_length.addr, align 4, !tbaa !7
  %5 = load i32, i32* %source_scale.addr, align 4, !tbaa !7
  %6 = load i32, i32* %dest_scale.addr, align 4, !tbaa !7
  %7 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %mul1 = mul nsw i32 %7, 2
  store i32 %mul1, i32* %source_step.addr, align 4, !tbaa !7
  %8 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 0
  %9 = load i8, i8* %arrayidx, align 1, !tbaa !6
  %10 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %10, i32 0
  store i8 %9, i8* %arrayidx2, align 1, !tbaa !6
  %11 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %12 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr3 = getelementptr inbounds i8, i8* %12, i32 %11
  store i8* %add.ptr3, i8** %source.addr, align 4, !tbaa !2
  %13 = load i32, i32* %dest_step.addr, align 4, !tbaa !7
  %14 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %add.ptr4 = getelementptr inbounds i8, i8* %14, i32 %13
  store i8* %add.ptr4, i8** %dest.addr, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %15 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %16 = load i8*, i8** %dest_end, align 4, !tbaa !2
  %cmp = icmp ult i8* %15, %16
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %19 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %sub = sub nsw i32 0, %19
  %arrayidx5 = getelementptr inbounds i8, i8* %18, i32 %sub
  %20 = load i8, i8* %arrayidx5, align 1, !tbaa !6
  %conv = zext i8 %20 to i32
  %mul6 = mul nsw i32 3, %conv
  store i32 %mul6, i32* %a, align 4, !tbaa !7
  %21 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %22, i32 0
  %23 = load i8, i8* %arrayidx7, align 1, !tbaa !6
  %conv8 = zext i8 %23 to i32
  %mul9 = mul nsw i32 10, %conv8
  store i32 %mul9, i32* %b, align 4, !tbaa !7
  %24 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %26 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %arrayidx10 = getelementptr inbounds i8, i8* %25, i32 %26
  %27 = load i8, i8* %arrayidx10, align 1, !tbaa !6
  %conv11 = zext i8 %27 to i32
  %mul12 = mul nsw i32 3, %conv11
  store i32 %mul12, i32* %c, align 4, !tbaa !7
  %28 = load i32, i32* %a, align 4, !tbaa !7
  %add = add i32 8, %28
  %29 = load i32, i32* %b, align 4, !tbaa !7
  %add13 = add i32 %add, %29
  %30 = load i32, i32* %c, align 4, !tbaa !7
  %add14 = add i32 %add13, %30
  %shr = lshr i32 %add14, 4
  %conv15 = trunc i32 %shr to i8
  %31 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  store i8 %conv15, i8* %31, align 1, !tbaa !6
  %32 = load i32, i32* %source_step.addr, align 4, !tbaa !7
  %33 = load i8*, i8** %source.addr, align 4, !tbaa !2
  %add.ptr16 = getelementptr inbounds i8, i8* %33, i32 %32
  store i8* %add.ptr16, i8** %source.addr, align 4, !tbaa !2
  %34 = load i32, i32* %dest_step.addr, align 4, !tbaa !7
  %35 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds i8, i8* %35, i32 %34
  store i8* %add.ptr17, i8** %dest.addr, align 4, !tbaa !2
  %36 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %38 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %39 = bitcast i8** %dest_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!4, !4, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"int", !4, i64 0}
