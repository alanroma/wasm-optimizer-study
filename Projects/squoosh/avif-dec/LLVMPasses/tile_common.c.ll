; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/tile_common.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/tile_common.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.AV1PixelRect = type { i32, i32, i32, i32 }

; Function Attrs: nounwind
define hidden void @av1_tile_init(%struct.TileInfo* %tile, %struct.AV1Common* %cm, i32 %row, i32 %col) #0 {
entry:
  %tile.addr = alloca %struct.TileInfo*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  store %struct.TileInfo* %tile, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  store i32 %col, i32* %col.addr, align 4, !tbaa !6
  %0 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %2 = load i32, i32* %row.addr, align 4, !tbaa !6
  call void @av1_tile_set_row(%struct.TileInfo* %0, %struct.AV1Common* %1, i32 %2)
  %3 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %5 = load i32, i32* %col.addr, align 4, !tbaa !6
  call void @av1_tile_set_col(%struct.TileInfo* %3, %struct.AV1Common* %4, i32 %5)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_tile_set_row(%struct.TileInfo* %tile, %struct.AV1Common* %cm, i32 %row) #0 {
entry:
  %tile.addr = alloca %struct.TileInfo*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %row.addr = alloca i32, align 4
  %mi_row_start = alloca i32, align 4
  %mi_row_end = alloca i32, align 4
  store %struct.TileInfo* %tile, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  %0 = bitcast i32* %mi_row_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 40
  %row_start_sb = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles, i32 0, i32 16
  %2 = load i32, i32* %row.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [65 x i32], [65 x i32]* %row_start_sb, i32 0, i32 %2
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 37
  %mib_size_log2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 9
  %5 = load i32, i32* %mib_size_log2, align 4, !tbaa !8
  %shl = shl i32 %3, %5
  store i32 %shl, i32* %mi_row_start, align 4, !tbaa !6
  %6 = bitcast i32* %mi_row_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tiles1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 40
  %row_start_sb2 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles1, i32 0, i32 16
  %8 = load i32, i32* %row.addr, align 4, !tbaa !6
  %add = add nsw i32 %8, 1
  %arrayidx3 = getelementptr inbounds [65 x i32], [65 x i32]* %row_start_sb2, i32 0, i32 %add
  %9 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 37
  %mib_size_log25 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params4, i32 0, i32 9
  %11 = load i32, i32* %mib_size_log25, align 4, !tbaa !8
  %shl6 = shl i32 %9, %11
  store i32 %shl6, i32* %mi_row_end, align 4, !tbaa !6
  %12 = load i32, i32* %row.addr, align 4, !tbaa !6
  %13 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %tile_row = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %13, i32 0, i32 4
  store i32 %12, i32* %tile_row, align 4, !tbaa !32
  %14 = load i32, i32* %mi_row_start, align 4, !tbaa !6
  %15 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_row_start7 = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %15, i32 0, i32 0
  store i32 %14, i32* %mi_row_start7, align 4, !tbaa !34
  %16 = load i32, i32* %mi_row_end, align 4, !tbaa !6
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %18 = load i32, i32* %mi_rows, align 4, !tbaa !35
  %cmp = icmp slt i32 %16, %18
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %19 = load i32, i32* %mi_row_end, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %20 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %20, i32 0, i32 22
  %mi_rows9 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params8, i32 0, i32 3
  %21 = load i32, i32* %mi_rows9, align 4, !tbaa !35
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %19, %cond.true ], [ %21, %cond.false ]
  %22 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_row_end10 = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %22, i32 0, i32 1
  store i32 %cond, i32* %mi_row_end10, align 4, !tbaa !36
  %23 = bitcast i32* %mi_row_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %24 = bitcast i32* %mi_row_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_tile_set_col(%struct.TileInfo* %tile, %struct.AV1Common* %cm, i32 %col) #0 {
entry:
  %tile.addr = alloca %struct.TileInfo*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %col.addr = alloca i32, align 4
  %mi_col_start = alloca i32, align 4
  %mi_col_end = alloca i32, align 4
  store %struct.TileInfo* %tile, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %col, i32* %col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %mi_col_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 40
  %col_start_sb = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles, i32 0, i32 15
  %2 = load i32, i32* %col.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [65 x i32], [65 x i32]* %col_start_sb, i32 0, i32 %2
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 37
  %mib_size_log2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 9
  %5 = load i32, i32* %mib_size_log2, align 4, !tbaa !8
  %shl = shl i32 %3, %5
  store i32 %shl, i32* %mi_col_start, align 4, !tbaa !6
  %6 = bitcast i32* %mi_col_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tiles1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 40
  %col_start_sb2 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles1, i32 0, i32 15
  %8 = load i32, i32* %col.addr, align 4, !tbaa !6
  %add = add nsw i32 %8, 1
  %arrayidx3 = getelementptr inbounds [65 x i32], [65 x i32]* %col_start_sb2, i32 0, i32 %add
  %9 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %10, i32 0, i32 37
  %mib_size_log25 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params4, i32 0, i32 9
  %11 = load i32, i32* %mib_size_log25, align 4, !tbaa !8
  %shl6 = shl i32 %9, %11
  store i32 %shl6, i32* %mi_col_end, align 4, !tbaa !6
  %12 = load i32, i32* %col.addr, align 4, !tbaa !6
  %13 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %tile_col = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %13, i32 0, i32 5
  store i32 %12, i32* %tile_col, align 4, !tbaa !37
  %14 = load i32, i32* %mi_col_start, align 4, !tbaa !6
  %15 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_col_start7 = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %15, i32 0, i32 2
  store i32 %14, i32* %mi_col_start7, align 4, !tbaa !38
  %16 = load i32, i32* %mi_col_end, align 4, !tbaa !6
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %18 = load i32, i32* %mi_cols, align 4, !tbaa !39
  %cmp = icmp slt i32 %16, %18
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %19 = load i32, i32* %mi_col_end, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %20 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %20, i32 0, i32 22
  %mi_cols9 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params8, i32 0, i32 4
  %21 = load i32, i32* %mi_cols9, align 4, !tbaa !39
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %19, %cond.true ], [ %21, %cond.false ]
  %22 = load %struct.TileInfo*, %struct.TileInfo** %tile.addr, align 4, !tbaa !2
  %mi_col_end10 = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %22, i32 0, i32 3
  store i32 %cond, i32* %mi_col_end10, align 4, !tbaa !40
  %23 = bitcast i32* %mi_col_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %24 = bitcast i32* %mi_col_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_get_tile_limits(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %seq_params = alloca %struct.SequenceHeader*, align 4
  %tiles = alloca %struct.CommonTileParams*, align 4
  %mi_cols = alloca i32, align 4
  %mi_rows = alloca i32, align 4
  %sb_cols = alloca i32, align 4
  %sb_rows = alloca i32, align 4
  %sb_size_log2 = alloca i32, align 4
  %max_tile_area_sb = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = bitcast %struct.SequenceHeader** %seq_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 37
  store %struct.SequenceHeader* %seq_params1, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %2 = bitcast %struct.CommonTileParams** %tiles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tiles2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 40
  store %struct.CommonTileParams* %tiles2, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %4 = bitcast i32* %mi_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 22
  %mi_cols3 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %6 = load i32, i32* %mi_cols3, align 4, !tbaa !39
  %7 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %mib_size_log2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %7, i32 0, i32 9
  %8 = load i32, i32* %mib_size_log2, align 4, !tbaa !41
  %shl = shl i32 1, %8
  %sub = sub nsw i32 %shl, 1
  %add = add nsw i32 %6, %sub
  %9 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %mib_size_log24 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %9, i32 0, i32 9
  %10 = load i32, i32* %mib_size_log24, align 4, !tbaa !41
  %shl5 = shl i32 1, %10
  %sub6 = sub nsw i32 %shl5, 1
  %neg = xor i32 %sub6, -1
  %and = and i32 %add, %neg
  store i32 %and, i32* %mi_cols, align 4, !tbaa !6
  %11 = bitcast i32* %mi_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %12, i32 0, i32 22
  %mi_rows8 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params7, i32 0, i32 3
  %13 = load i32, i32* %mi_rows8, align 4, !tbaa !35
  %14 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %mib_size_log29 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %14, i32 0, i32 9
  %15 = load i32, i32* %mib_size_log29, align 4, !tbaa !41
  %shl10 = shl i32 1, %15
  %sub11 = sub nsw i32 %shl10, 1
  %add12 = add nsw i32 %13, %sub11
  %16 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %mib_size_log213 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %16, i32 0, i32 9
  %17 = load i32, i32* %mib_size_log213, align 4, !tbaa !41
  %shl14 = shl i32 1, %17
  %sub15 = sub nsw i32 %shl14, 1
  %neg16 = xor i32 %sub15, -1
  %and17 = and i32 %add12, %neg16
  store i32 %and17, i32* %mi_rows, align 4, !tbaa !6
  %18 = bitcast i32* %sb_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load i32, i32* %mi_cols, align 4, !tbaa !6
  %20 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %mib_size_log218 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %20, i32 0, i32 9
  %21 = load i32, i32* %mib_size_log218, align 4, !tbaa !41
  %shr = ashr i32 %19, %21
  store i32 %shr, i32* %sb_cols, align 4, !tbaa !6
  %22 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  %23 = load i32, i32* %mi_rows, align 4, !tbaa !6
  %24 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %mib_size_log219 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %24, i32 0, i32 9
  %25 = load i32, i32* %mib_size_log219, align 4, !tbaa !41
  %shr20 = ashr i32 %23, %25
  store i32 %shr20, i32* %sb_rows, align 4, !tbaa !6
  %26 = bitcast i32* %sb_size_log2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params, align 4, !tbaa !2
  %mib_size_log221 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %27, i32 0, i32 9
  %28 = load i32, i32* %mib_size_log221, align 4, !tbaa !41
  %add22 = add nsw i32 %28, 2
  store i32 %add22, i32* %sb_size_log2, align 4, !tbaa !6
  %29 = load i32, i32* %sb_size_log2, align 4, !tbaa !6
  %shr23 = ashr i32 4096, %29
  %30 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %max_width_sb = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %30, i32 0, i32 2
  store i32 %shr23, i32* %max_width_sb, align 4, !tbaa !42
  %31 = bitcast i32* %max_tile_area_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = load i32, i32* %sb_size_log2, align 4, !tbaa !6
  %mul = mul nsw i32 2, %32
  %shr24 = ashr i32 9437184, %mul
  store i32 %shr24, i32* %max_tile_area_sb, align 4, !tbaa !6
  %33 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %max_width_sb25 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %33, i32 0, i32 2
  %34 = load i32, i32* %max_width_sb25, align 4, !tbaa !42
  %35 = load i32, i32* %sb_cols, align 4, !tbaa !6
  %call = call i32 @tile_log2(i32 %34, i32 %35)
  %36 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %min_log2_cols = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %36, i32 0, i32 10
  store i32 %call, i32* %min_log2_cols, align 4, !tbaa !43
  %37 = load i32, i32* %sb_cols, align 4, !tbaa !6
  %cmp = icmp slt i32 %37, 64
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %38 = load i32, i32* %sb_cols, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %38, %cond.true ], [ 64, %cond.false ]
  %call26 = call i32 @tile_log2(i32 1, i32 %cond)
  %39 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %max_log2_cols = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %39, i32 0, i32 12
  store i32 %call26, i32* %max_log2_cols, align 4, !tbaa !44
  %40 = load i32, i32* %sb_rows, align 4, !tbaa !6
  %cmp27 = icmp slt i32 %40, 64
  br i1 %cmp27, label %cond.true28, label %cond.false29

cond.true28:                                      ; preds = %cond.end
  %41 = load i32, i32* %sb_rows, align 4, !tbaa !6
  br label %cond.end30

cond.false29:                                     ; preds = %cond.end
  br label %cond.end30

cond.end30:                                       ; preds = %cond.false29, %cond.true28
  %cond31 = phi i32 [ %41, %cond.true28 ], [ 64, %cond.false29 ]
  %call32 = call i32 @tile_log2(i32 1, i32 %cond31)
  %42 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %max_log2_rows = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %42, i32 0, i32 13
  store i32 %call32, i32* %max_log2_rows, align 4, !tbaa !45
  %43 = load i32, i32* %max_tile_area_sb, align 4, !tbaa !6
  %44 = load i32, i32* %sb_cols, align 4, !tbaa !6
  %45 = load i32, i32* %sb_rows, align 4, !tbaa !6
  %mul33 = mul nsw i32 %44, %45
  %call34 = call i32 @tile_log2(i32 %43, i32 %mul33)
  %46 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %min_log2 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %46, i32 0, i32 14
  store i32 %call34, i32* %min_log2, align 4, !tbaa !46
  %47 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %min_log235 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %47, i32 0, i32 14
  %48 = load i32, i32* %min_log235, align 4, !tbaa !46
  %49 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %min_log2_cols36 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %49, i32 0, i32 10
  %50 = load i32, i32* %min_log2_cols36, align 4, !tbaa !43
  %cmp37 = icmp sgt i32 %48, %50
  br i1 %cmp37, label %cond.true38, label %cond.false40

cond.true38:                                      ; preds = %cond.end30
  %51 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %min_log239 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %51, i32 0, i32 14
  %52 = load i32, i32* %min_log239, align 4, !tbaa !46
  br label %cond.end42

cond.false40:                                     ; preds = %cond.end30
  %53 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %min_log2_cols41 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %53, i32 0, i32 10
  %54 = load i32, i32* %min_log2_cols41, align 4, !tbaa !43
  br label %cond.end42

cond.end42:                                       ; preds = %cond.false40, %cond.true38
  %cond43 = phi i32 [ %52, %cond.true38 ], [ %54, %cond.false40 ]
  %55 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %min_log244 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %55, i32 0, i32 14
  store i32 %cond43, i32* %min_log244, align 4, !tbaa !46
  %56 = bitcast i32* %max_tile_area_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %57 = bitcast i32* %sb_size_log2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i32* %sb_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  %60 = bitcast i32* %mi_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  %61 = bitcast i32* %mi_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  %62 = bitcast %struct.CommonTileParams** %tiles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  %63 = bitcast %struct.SequenceHeader** %seq_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @tile_log2(i32 %blk_size, i32 %target) #0 {
entry:
  %blk_size.addr = alloca i32, align 4
  %target.addr = alloca i32, align 4
  %k = alloca i32, align 4
  store i32 %blk_size, i32* %blk_size.addr, align 4, !tbaa !6
  store i32 %target, i32* %target.addr, align 4, !tbaa !6
  %0 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %blk_size.addr, align 4, !tbaa !6
  %2 = load i32, i32* %k, align 4, !tbaa !6
  %shl = shl i32 %1, %2
  %3 = load i32, i32* %target.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %shl, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %5 = load i32, i32* %k, align 4, !tbaa !6
  %6 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  ret i32 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_calculate_tile_cols(%struct.SequenceHeader* %seq_params, i32 %cm_mi_rows, i32 %cm_mi_cols, %struct.CommonTileParams* %tiles) #0 {
entry:
  %seq_params.addr = alloca %struct.SequenceHeader*, align 4
  %cm_mi_rows.addr = alloca i32, align 4
  %cm_mi_cols.addr = alloca i32, align 4
  %tiles.addr = alloca %struct.CommonTileParams*, align 4
  %mi_cols = alloca i32, align 4
  %mi_rows = alloca i32, align 4
  %sb_cols = alloca i32, align 4
  %sb_rows = alloca i32, align 4
  %i = alloca i32, align 4
  %start_sb = alloca i32, align 4
  %size_sb = alloca i32, align 4
  %max_tile_area_sb = alloca i32, align 4
  %widest_tile_sb = alloca i32, align 4
  %narrowest_inner_tile_sb = alloca i32, align 4
  %size_sb58 = alloca i32, align 4
  store %struct.SequenceHeader* %seq_params, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  store i32 %cm_mi_rows, i32* %cm_mi_rows.addr, align 4, !tbaa !6
  store i32 %cm_mi_cols, i32* %cm_mi_cols.addr, align 4, !tbaa !6
  store %struct.CommonTileParams* %tiles, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %0 = bitcast i32* %mi_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %cm_mi_cols.addr, align 4, !tbaa !6
  %2 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %2, i32 0, i32 9
  %3 = load i32, i32* %mib_size_log2, align 4, !tbaa !41
  %shl = shl i32 1, %3
  %sub = sub nsw i32 %shl, 1
  %add = add nsw i32 %1, %sub
  %4 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log21 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %4, i32 0, i32 9
  %5 = load i32, i32* %mib_size_log21, align 4, !tbaa !41
  %shl2 = shl i32 1, %5
  %sub3 = sub nsw i32 %shl2, 1
  %neg = xor i32 %sub3, -1
  %and = and i32 %add, %neg
  store i32 %and, i32* %mi_cols, align 4, !tbaa !6
  %6 = bitcast i32* %mi_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i32, i32* %cm_mi_rows.addr, align 4, !tbaa !6
  %8 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log24 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %8, i32 0, i32 9
  %9 = load i32, i32* %mib_size_log24, align 4, !tbaa !41
  %shl5 = shl i32 1, %9
  %sub6 = sub nsw i32 %shl5, 1
  %add7 = add nsw i32 %7, %sub6
  %10 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log28 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %10, i32 0, i32 9
  %11 = load i32, i32* %mib_size_log28, align 4, !tbaa !41
  %shl9 = shl i32 1, %11
  %sub10 = sub nsw i32 %shl9, 1
  %neg11 = xor i32 %sub10, -1
  %and12 = and i32 %add7, %neg11
  store i32 %and12, i32* %mi_rows, align 4, !tbaa !6
  %12 = bitcast i32* %sb_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i32, i32* %mi_cols, align 4, !tbaa !6
  %14 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log213 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %14, i32 0, i32 9
  %15 = load i32, i32* %mib_size_log213, align 4, !tbaa !41
  %shr = ashr i32 %13, %15
  store i32 %shr, i32* %sb_cols, align 4, !tbaa !6
  %16 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %mi_rows, align 4, !tbaa !6
  %18 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log214 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %18, i32 0, i32 9
  %19 = load i32, i32* %mib_size_log214, align 4, !tbaa !41
  %shr15 = ashr i32 %17, %19
  store i32 %shr15, i32* %sb_rows, align 4, !tbaa !6
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %min_inner_width = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %21, i32 0, i32 4
  store i32 -1, i32* %min_inner_width, align 4, !tbaa !47
  %22 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %uniform_spacing = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %22, i32 0, i32 5
  %23 = load i32, i32* %uniform_spacing, align 4, !tbaa !48
  %tobool = icmp ne i32 %23, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %24 = bitcast i32* %start_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i32* %size_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  %26 = load i32, i32* %sb_cols, align 4, !tbaa !6
  %27 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_cols = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %27, i32 0, i32 6
  %28 = load i32, i32* %log2_cols, align 4, !tbaa !49
  %shl16 = shl i32 1, %28
  %sub17 = sub nsw i32 %shl16, 1
  %add18 = add nsw i32 %26, %sub17
  %29 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_cols19 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %29, i32 0, i32 6
  %30 = load i32, i32* %log2_cols19, align 4, !tbaa !49
  %shl20 = shl i32 1, %30
  %sub21 = sub nsw i32 %shl20, 1
  %neg22 = xor i32 %sub21, -1
  %and23 = and i32 %add18, %neg22
  store i32 %and23, i32* %size_sb, align 4, !tbaa !6
  %31 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_cols24 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %31, i32 0, i32 6
  %32 = load i32, i32* %log2_cols24, align 4, !tbaa !49
  %33 = load i32, i32* %size_sb, align 4, !tbaa !6
  %shr25 = ashr i32 %33, %32
  store i32 %shr25, i32* %size_sb, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  store i32 0, i32* %start_sb, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %34 = load i32, i32* %start_sb, align 4, !tbaa !6
  %35 = load i32, i32* %sb_cols, align 4, !tbaa !6
  %cmp = icmp slt i32 %34, %35
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i32, i32* %start_sb, align 4, !tbaa !6
  %37 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %col_start_sb = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %37, i32 0, i32 15
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [65 x i32], [65 x i32]* %col_start_sb, i32 0, i32 %38
  store i32 %36, i32* %arrayidx, align 4, !tbaa !6
  %39 = load i32, i32* %size_sb, align 4, !tbaa !6
  %40 = load i32, i32* %start_sb, align 4, !tbaa !6
  %add26 = add nsw i32 %40, %39
  store i32 %add26, i32* %start_sb, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %43 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %cols = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %43, i32 0, i32 0
  store i32 %42, i32* %cols, align 4, !tbaa !50
  %44 = load i32, i32* %sb_cols, align 4, !tbaa !6
  %45 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %col_start_sb27 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %45, i32 0, i32 15
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds [65 x i32], [65 x i32]* %col_start_sb27, i32 0, i32 %46
  store i32 %44, i32* %arrayidx28, align 4, !tbaa !6
  %47 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %min_log2 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %47, i32 0, i32 14
  %48 = load i32, i32* %min_log2, align 4, !tbaa !46
  %49 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_cols29 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %49, i32 0, i32 6
  %50 = load i32, i32* %log2_cols29, align 4, !tbaa !49
  %sub30 = sub nsw i32 %48, %50
  %cmp31 = icmp sgt i32 %sub30, 0
  br i1 %cmp31, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %51 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %min_log232 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %51, i32 0, i32 14
  %52 = load i32, i32* %min_log232, align 4, !tbaa !46
  %53 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_cols33 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %53, i32 0, i32 6
  %54 = load i32, i32* %log2_cols33, align 4, !tbaa !49
  %sub34 = sub nsw i32 %52, %54
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub34, %cond.true ], [ 0, %cond.false ]
  %55 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %min_log2_rows = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %55, i32 0, i32 11
  store i32 %cond, i32* %min_log2_rows, align 4, !tbaa !51
  %56 = load i32, i32* %sb_rows, align 4, !tbaa !6
  %57 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %min_log2_rows35 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %57, i32 0, i32 11
  %58 = load i32, i32* %min_log2_rows35, align 4, !tbaa !51
  %shr36 = ashr i32 %56, %58
  %59 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %max_height_sb = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %59, i32 0, i32 3
  store i32 %shr36, i32* %max_height_sb, align 4, !tbaa !52
  %60 = load i32, i32* %size_sb, align 4, !tbaa !6
  %61 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log237 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %61, i32 0, i32 9
  %62 = load i32, i32* %mib_size_log237, align 4, !tbaa !41
  %shl38 = shl i32 %60, %62
  %63 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %63, i32 0, i32 8
  store i32 %shl38, i32* %width, align 4, !tbaa !53
  %64 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %width39 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %64, i32 0, i32 8
  %65 = load i32, i32* %width39, align 4, !tbaa !53
  %66 = load i32, i32* %cm_mi_cols.addr, align 4, !tbaa !6
  %cmp40 = icmp slt i32 %65, %66
  br i1 %cmp40, label %cond.true41, label %cond.false43

cond.true41:                                      ; preds = %cond.end
  %67 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %width42 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %67, i32 0, i32 8
  %68 = load i32, i32* %width42, align 4, !tbaa !53
  br label %cond.end44

cond.false43:                                     ; preds = %cond.end
  %69 = load i32, i32* %cm_mi_cols.addr, align 4, !tbaa !6
  br label %cond.end44

cond.end44:                                       ; preds = %cond.false43, %cond.true41
  %cond45 = phi i32 [ %68, %cond.true41 ], [ %69, %cond.false43 ]
  %70 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %width46 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %70, i32 0, i32 8
  store i32 %cond45, i32* %width46, align 4, !tbaa !53
  %71 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %cols47 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %71, i32 0, i32 0
  %72 = load i32, i32* %cols47, align 4, !tbaa !50
  %cmp48 = icmp sgt i32 %72, 1
  br i1 %cmp48, label %if.then49, label %if.end

if.then49:                                        ; preds = %cond.end44
  %73 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %width50 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %73, i32 0, i32 8
  %74 = load i32, i32* %width50, align 4, !tbaa !53
  %75 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %min_inner_width51 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %75, i32 0, i32 4
  store i32 %74, i32* %min_inner_width51, align 4, !tbaa !47
  br label %if.end

if.end:                                           ; preds = %if.then49, %cond.end44
  %76 = bitcast i32* %size_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  %77 = bitcast i32* %start_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  br label %if.end104

if.else:                                          ; preds = %entry
  %78 = bitcast i32* %max_tile_area_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #4
  %79 = load i32, i32* %sb_rows, align 4, !tbaa !6
  %80 = load i32, i32* %sb_cols, align 4, !tbaa !6
  %mul = mul nsw i32 %79, %80
  store i32 %mul, i32* %max_tile_area_sb, align 4, !tbaa !6
  %81 = bitcast i32* %widest_tile_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #4
  store i32 1, i32* %widest_tile_sb, align 4, !tbaa !6
  %82 = bitcast i32* %narrowest_inner_tile_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #4
  store i32 65536, i32* %narrowest_inner_tile_sb, align 4, !tbaa !6
  %83 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %cols52 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %83, i32 0, i32 0
  %84 = load i32, i32* %cols52, align 4, !tbaa !50
  %call = call i32 @tile_log2(i32 1, i32 %84)
  %85 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_cols53 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %85, i32 0, i32 6
  store i32 %call, i32* %log2_cols53, align 4, !tbaa !49
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond54

for.cond54:                                       ; preds = %for.inc80, %if.else
  %86 = load i32, i32* %i, align 4, !tbaa !6
  %87 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %cols55 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %87, i32 0, i32 0
  %88 = load i32, i32* %cols55, align 4, !tbaa !50
  %cmp56 = icmp slt i32 %86, %88
  br i1 %cmp56, label %for.body57, label %for.end82

for.body57:                                       ; preds = %for.cond54
  %89 = bitcast i32* %size_sb58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #4
  %90 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %col_start_sb59 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %90, i32 0, i32 15
  %91 = load i32, i32* %i, align 4, !tbaa !6
  %add60 = add nsw i32 %91, 1
  %arrayidx61 = getelementptr inbounds [65 x i32], [65 x i32]* %col_start_sb59, i32 0, i32 %add60
  %92 = load i32, i32* %arrayidx61, align 4, !tbaa !6
  %93 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %col_start_sb62 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %93, i32 0, i32 15
  %94 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds [65 x i32], [65 x i32]* %col_start_sb62, i32 0, i32 %94
  %95 = load i32, i32* %arrayidx63, align 4, !tbaa !6
  %sub64 = sub nsw i32 %92, %95
  store i32 %sub64, i32* %size_sb58, align 4, !tbaa !6
  %96 = load i32, i32* %widest_tile_sb, align 4, !tbaa !6
  %97 = load i32, i32* %size_sb58, align 4, !tbaa !6
  %cmp65 = icmp sgt i32 %96, %97
  br i1 %cmp65, label %cond.true66, label %cond.false67

cond.true66:                                      ; preds = %for.body57
  %98 = load i32, i32* %widest_tile_sb, align 4, !tbaa !6
  br label %cond.end68

cond.false67:                                     ; preds = %for.body57
  %99 = load i32, i32* %size_sb58, align 4, !tbaa !6
  br label %cond.end68

cond.end68:                                       ; preds = %cond.false67, %cond.true66
  %cond69 = phi i32 [ %98, %cond.true66 ], [ %99, %cond.false67 ]
  store i32 %cond69, i32* %widest_tile_sb, align 4, !tbaa !6
  %100 = load i32, i32* %i, align 4, !tbaa !6
  %101 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %cols70 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %101, i32 0, i32 0
  %102 = load i32, i32* %cols70, align 4, !tbaa !50
  %sub71 = sub nsw i32 %102, 1
  %cmp72 = icmp slt i32 %100, %sub71
  br i1 %cmp72, label %if.then73, label %if.end79

if.then73:                                        ; preds = %cond.end68
  %103 = load i32, i32* %narrowest_inner_tile_sb, align 4, !tbaa !6
  %104 = load i32, i32* %size_sb58, align 4, !tbaa !6
  %cmp74 = icmp slt i32 %103, %104
  br i1 %cmp74, label %cond.true75, label %cond.false76

cond.true75:                                      ; preds = %if.then73
  %105 = load i32, i32* %narrowest_inner_tile_sb, align 4, !tbaa !6
  br label %cond.end77

cond.false76:                                     ; preds = %if.then73
  %106 = load i32, i32* %size_sb58, align 4, !tbaa !6
  br label %cond.end77

cond.end77:                                       ; preds = %cond.false76, %cond.true75
  %cond78 = phi i32 [ %105, %cond.true75 ], [ %106, %cond.false76 ]
  store i32 %cond78, i32* %narrowest_inner_tile_sb, align 4, !tbaa !6
  br label %if.end79

if.end79:                                         ; preds = %cond.end77, %cond.end68
  %107 = bitcast i32* %size_sb58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  br label %for.inc80

for.inc80:                                        ; preds = %if.end79
  %108 = load i32, i32* %i, align 4, !tbaa !6
  %inc81 = add nsw i32 %108, 1
  store i32 %inc81, i32* %i, align 4, !tbaa !6
  br label %for.cond54

for.end82:                                        ; preds = %for.cond54
  %109 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %min_log283 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %109, i32 0, i32 14
  %110 = load i32, i32* %min_log283, align 4, !tbaa !46
  %tobool84 = icmp ne i32 %110, 0
  br i1 %tobool84, label %if.then85, label %if.end89

if.then85:                                        ; preds = %for.end82
  %111 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %min_log286 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %111, i32 0, i32 14
  %112 = load i32, i32* %min_log286, align 4, !tbaa !46
  %add87 = add nsw i32 %112, 1
  %113 = load i32, i32* %max_tile_area_sb, align 4, !tbaa !6
  %shr88 = ashr i32 %113, %add87
  store i32 %shr88, i32* %max_tile_area_sb, align 4, !tbaa !6
  br label %if.end89

if.end89:                                         ; preds = %if.then85, %for.end82
  %114 = load i32, i32* %max_tile_area_sb, align 4, !tbaa !6
  %115 = load i32, i32* %widest_tile_sb, align 4, !tbaa !6
  %div = sdiv i32 %114, %115
  %cmp90 = icmp sgt i32 %div, 1
  br i1 %cmp90, label %cond.true91, label %cond.false93

cond.true91:                                      ; preds = %if.end89
  %116 = load i32, i32* %max_tile_area_sb, align 4, !tbaa !6
  %117 = load i32, i32* %widest_tile_sb, align 4, !tbaa !6
  %div92 = sdiv i32 %116, %117
  br label %cond.end94

cond.false93:                                     ; preds = %if.end89
  br label %cond.end94

cond.end94:                                       ; preds = %cond.false93, %cond.true91
  %cond95 = phi i32 [ %div92, %cond.true91 ], [ 1, %cond.false93 ]
  %118 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %max_height_sb96 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %118, i32 0, i32 3
  store i32 %cond95, i32* %max_height_sb96, align 4, !tbaa !52
  %119 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %cols97 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %119, i32 0, i32 0
  %120 = load i32, i32* %cols97, align 4, !tbaa !50
  %cmp98 = icmp sgt i32 %120, 1
  br i1 %cmp98, label %if.then99, label %if.end103

if.then99:                                        ; preds = %cond.end94
  %121 = load i32, i32* %narrowest_inner_tile_sb, align 4, !tbaa !6
  %122 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log2100 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %122, i32 0, i32 9
  %123 = load i32, i32* %mib_size_log2100, align 4, !tbaa !41
  %shl101 = shl i32 %121, %123
  %124 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %min_inner_width102 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %124, i32 0, i32 4
  store i32 %shl101, i32* %min_inner_width102, align 4, !tbaa !47
  br label %if.end103

if.end103:                                        ; preds = %if.then99, %cond.end94
  %125 = bitcast i32* %narrowest_inner_tile_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #4
  %126 = bitcast i32* %widest_tile_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #4
  %127 = bitcast i32* %max_tile_area_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #4
  br label %if.end104

if.end104:                                        ; preds = %if.end103, %if.end
  %128 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #4
  %129 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #4
  %130 = bitcast i32* %sb_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #4
  %131 = bitcast i32* %mi_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #4
  %132 = bitcast i32* %mi_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_calculate_tile_rows(%struct.SequenceHeader* %seq_params, i32 %cm_mi_rows, %struct.CommonTileParams* %tiles) #0 {
entry:
  %seq_params.addr = alloca %struct.SequenceHeader*, align 4
  %cm_mi_rows.addr = alloca i32, align 4
  %tiles.addr = alloca %struct.CommonTileParams*, align 4
  %mi_rows = alloca i32, align 4
  %sb_rows = alloca i32, align 4
  %start_sb = alloca i32, align 4
  %size_sb = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.SequenceHeader* %seq_params, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  store i32 %cm_mi_rows, i32* %cm_mi_rows.addr, align 4, !tbaa !6
  store %struct.CommonTileParams* %tiles, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %0 = bitcast i32* %mi_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %cm_mi_rows.addr, align 4, !tbaa !6
  %2 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %2, i32 0, i32 9
  %3 = load i32, i32* %mib_size_log2, align 4, !tbaa !41
  %shl = shl i32 1, %3
  %sub = sub nsw i32 %shl, 1
  %add = add nsw i32 %1, %sub
  %4 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log21 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %4, i32 0, i32 9
  %5 = load i32, i32* %mib_size_log21, align 4, !tbaa !41
  %shl2 = shl i32 1, %5
  %sub3 = sub nsw i32 %shl2, 1
  %neg = xor i32 %sub3, -1
  %and = and i32 %add, %neg
  store i32 %and, i32* %mi_rows, align 4, !tbaa !6
  %6 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i32, i32* %mi_rows, align 4, !tbaa !6
  %8 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log24 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %8, i32 0, i32 9
  %9 = load i32, i32* %mib_size_log24, align 4, !tbaa !41
  %shr = ashr i32 %7, %9
  store i32 %shr, i32* %sb_rows, align 4, !tbaa !6
  %10 = bitcast i32* %start_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %size_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %uniform_spacing = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %13, i32 0, i32 5
  %14 = load i32, i32* %uniform_spacing, align 4, !tbaa !48
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %15 = load i32, i32* %sb_rows, align 4, !tbaa !6
  %16 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_rows = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %16, i32 0, i32 7
  %17 = load i32, i32* %log2_rows, align 4, !tbaa !54
  %shl5 = shl i32 1, %17
  %sub6 = sub nsw i32 %shl5, 1
  %add7 = add nsw i32 %15, %sub6
  %18 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_rows8 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %18, i32 0, i32 7
  %19 = load i32, i32* %log2_rows8, align 4, !tbaa !54
  %shl9 = shl i32 1, %19
  %sub10 = sub nsw i32 %shl9, 1
  %neg11 = xor i32 %sub10, -1
  %and12 = and i32 %add7, %neg11
  store i32 %and12, i32* %size_sb, align 4, !tbaa !6
  %20 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_rows13 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %20, i32 0, i32 7
  %21 = load i32, i32* %log2_rows13, align 4, !tbaa !54
  %22 = load i32, i32* %size_sb, align 4, !tbaa !6
  %shr14 = ashr i32 %22, %21
  store i32 %shr14, i32* %size_sb, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  store i32 0, i32* %start_sb, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %23 = load i32, i32* %start_sb, align 4, !tbaa !6
  %24 = load i32, i32* %sb_rows, align 4, !tbaa !6
  %cmp = icmp slt i32 %23, %24
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %25 = load i32, i32* %start_sb, align 4, !tbaa !6
  %26 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %row_start_sb = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %26, i32 0, i32 16
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [65 x i32], [65 x i32]* %row_start_sb, i32 0, i32 %27
  store i32 %25, i32* %arrayidx, align 4, !tbaa !6
  %28 = load i32, i32* %size_sb, align 4, !tbaa !6
  %29 = load i32, i32* %start_sb, align 4, !tbaa !6
  %add15 = add nsw i32 %29, %28
  store i32 %add15, i32* %start_sb, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %32 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %32, i32 0, i32 1
  store i32 %31, i32* %rows, align 4, !tbaa !55
  %33 = load i32, i32* %sb_rows, align 4, !tbaa !6
  %34 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %row_start_sb16 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %34, i32 0, i32 16
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [65 x i32], [65 x i32]* %row_start_sb16, i32 0, i32 %35
  store i32 %33, i32* %arrayidx17, align 4, !tbaa !6
  %36 = load i32, i32* %size_sb, align 4, !tbaa !6
  %37 = load %struct.SequenceHeader*, %struct.SequenceHeader** %seq_params.addr, align 4, !tbaa !2
  %mib_size_log218 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %37, i32 0, i32 9
  %38 = load i32, i32* %mib_size_log218, align 4, !tbaa !41
  %shl19 = shl i32 %36, %38
  %39 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %39, i32 0, i32 9
  store i32 %shl19, i32* %height, align 4, !tbaa !56
  %40 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %height20 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %40, i32 0, i32 9
  %41 = load i32, i32* %height20, align 4, !tbaa !56
  %42 = load i32, i32* %cm_mi_rows.addr, align 4, !tbaa !6
  %cmp21 = icmp slt i32 %41, %42
  br i1 %cmp21, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %43 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %height22 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %43, i32 0, i32 9
  %44 = load i32, i32* %height22, align 4, !tbaa !56
  br label %cond.end

cond.false:                                       ; preds = %for.end
  %45 = load i32, i32* %cm_mi_rows.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %44, %cond.true ], [ %45, %cond.false ]
  %46 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %height23 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %46, i32 0, i32 9
  store i32 %cond, i32* %height23, align 4, !tbaa !56
  br label %if.end

if.else:                                          ; preds = %entry
  %47 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %rows24 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %47, i32 0, i32 1
  %48 = load i32, i32* %rows24, align 4, !tbaa !55
  %call = call i32 @tile_log2(i32 1, i32 %48)
  %49 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles.addr, align 4, !tbaa !2
  %log2_rows25 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %49, i32 0, i32 7
  store i32 %call, i32* %log2_rows25, align 4, !tbaa !54
  br label %if.end

if.end:                                           ; preds = %if.else, %cond.end
  %50 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %size_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %start_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i32* %mi_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_get_sb_rows_in_tile(%struct.AV1Common* %cm, %struct.TileInfo* byval(%struct.TileInfo) align 4 %tile) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %mi_rows_aligned_to_sb = alloca i32, align 4
  %sb_rows = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = bitcast i32* %mi_rows_aligned_to_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %mi_row_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile, i32 0, i32 1
  %1 = load i32, i32* %mi_row_end, align 4, !tbaa !36
  %mi_row_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile, i32 0, i32 0
  %2 = load i32, i32* %mi_row_start, align 4, !tbaa !34
  %sub = sub nsw i32 %1, %2
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 37
  %mib_size_log2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 9
  %4 = load i32, i32* %mib_size_log2, align 4, !tbaa !8
  %shl = shl i32 1, %4
  %sub1 = sub nsw i32 %shl, 1
  %add = add nsw i32 %sub, %sub1
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 37
  %mib_size_log23 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params2, i32 0, i32 9
  %6 = load i32, i32* %mib_size_log23, align 4, !tbaa !8
  %shl4 = shl i32 1, %6
  %sub5 = sub nsw i32 %shl4, 1
  %neg = xor i32 %sub5, -1
  %and = and i32 %add, %neg
  store i32 %and, i32* %mi_rows_aligned_to_sb, align 4, !tbaa !6
  %7 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %mi_rows_aligned_to_sb, align 4, !tbaa !6
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 37
  %mib_size_log27 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params6, i32 0, i32 9
  %10 = load i32, i32* %mib_size_log27, align 4, !tbaa !8
  %shr = ashr i32 %8, %10
  store i32 %shr, i32* %sb_rows, align 4, !tbaa !6
  %11 = load i32, i32* %sb_rows, align 4, !tbaa !6
  %12 = bitcast i32* %sb_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %mi_rows_aligned_to_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  ret i32 %11
}

; Function Attrs: nounwind
define hidden i32 @av1_get_sb_cols_in_tile(%struct.AV1Common* %cm, %struct.TileInfo* byval(%struct.TileInfo) align 4 %tile) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %mi_cols_aligned_to_sb = alloca i32, align 4
  %sb_cols = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = bitcast i32* %mi_cols_aligned_to_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %mi_col_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile, i32 0, i32 3
  %1 = load i32, i32* %mi_col_end, align 4, !tbaa !40
  %mi_col_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile, i32 0, i32 2
  %2 = load i32, i32* %mi_col_start, align 4, !tbaa !38
  %sub = sub nsw i32 %1, %2
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 37
  %mib_size_log2 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 9
  %4 = load i32, i32* %mib_size_log2, align 4, !tbaa !8
  %shl = shl i32 1, %4
  %sub1 = sub nsw i32 %shl, 1
  %add = add nsw i32 %sub, %sub1
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 37
  %mib_size_log23 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params2, i32 0, i32 9
  %6 = load i32, i32* %mib_size_log23, align 4, !tbaa !8
  %shl4 = shl i32 1, %6
  %sub5 = sub nsw i32 %shl4, 1
  %neg = xor i32 %sub5, -1
  %and = and i32 %add, %neg
  store i32 %and, i32* %mi_cols_aligned_to_sb, align 4, !tbaa !6
  %7 = bitcast i32* %sb_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %mi_cols_aligned_to_sb, align 4, !tbaa !6
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 37
  %mib_size_log27 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params6, i32 0, i32 9
  %10 = load i32, i32* %mib_size_log27, align 4, !tbaa !8
  %shr = ashr i32 %8, %10
  store i32 %shr, i32* %sb_cols, align 4, !tbaa !6
  %11 = load i32, i32* %sb_cols, align 4, !tbaa !6
  %12 = bitcast i32* %sb_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %mi_cols_aligned_to_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  ret i32 %11
}

; Function Attrs: nounwind
define hidden void @av1_get_tile_rect(%struct.AV1PixelRect* noalias sret align 4 %agg.result, %struct.TileInfo* %tile_info, %struct.AV1Common* %cm, i32 %is_uv) #0 {
entry:
  %tile_info.addr = alloca %struct.TileInfo*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %is_uv.addr = alloca i32, align 4
  %frame_w = alloca i32, align 4
  %frame_h = alloca i32, align 4
  %ss_x = alloca i32, align 4
  %ss_y = alloca i32, align 4
  store %struct.TileInfo* %tile_info, %struct.TileInfo** %tile_info.addr, align 4, !tbaa !2
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %is_uv, i32* %is_uv.addr, align 4, !tbaa !6
  %0 = load %struct.TileInfo*, %struct.TileInfo** %tile_info.addr, align 4, !tbaa !2
  %mi_col_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %0, i32 0, i32 2
  %1 = load i32, i32* %mi_col_start, align 4, !tbaa !38
  %mul = mul nsw i32 %1, 4
  %left = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 0
  store i32 %mul, i32* %left, align 4, !tbaa !57
  %2 = load %struct.TileInfo*, %struct.TileInfo** %tile_info.addr, align 4, !tbaa !2
  %mi_col_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %2, i32 0, i32 3
  %3 = load i32, i32* %mi_col_end, align 4, !tbaa !40
  %mul1 = mul nsw i32 %3, 4
  %right = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 2
  store i32 %mul1, i32* %right, align 4, !tbaa !58
  %4 = load %struct.TileInfo*, %struct.TileInfo** %tile_info.addr, align 4, !tbaa !2
  %mi_row_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %4, i32 0, i32 0
  %5 = load i32, i32* %mi_row_start, align 4, !tbaa !34
  %mul2 = mul nsw i32 %5, 4
  %top = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 1
  store i32 %mul2, i32* %top, align 4, !tbaa !59
  %6 = load %struct.TileInfo*, %struct.TileInfo** %tile_info.addr, align 4, !tbaa !2
  %mi_row_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %6, i32 0, i32 1
  %7 = load i32, i32* %mi_row_end, align 4, !tbaa !36
  %mul3 = mul nsw i32 %7, 4
  %bottom = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 3
  store i32 %mul3, i32* %bottom, align 4, !tbaa !60
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_superres_scaled(%struct.AV1Common* %8)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %left4 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 0
  %top5 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 1
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_scale_denominator = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 8
  %10 = load i8, i8* %superres_scale_denominator, align 8, !tbaa !61
  %conv = zext i8 %10 to i32
  call void @av1_calculate_unscaled_superres_size(i32* %left4, i32* %top5, i32 %conv)
  %right6 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 2
  %bottom7 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 3
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_scale_denominator8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 8
  %12 = load i8, i8* %superres_scale_denominator8, align 8, !tbaa !61
  %conv9 = zext i8 %12 to i32
  call void @av1_calculate_unscaled_superres_size(i32* %right6, i32* %bottom7, i32 %conv9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = bitcast i32* %frame_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 6
  %15 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !62
  store i32 %15, i32* %frame_w, align 4, !tbaa !6
  %16 = bitcast i32* %frame_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_upscaled_height = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 7
  %18 = load i32, i32* %superres_upscaled_height, align 4, !tbaa !63
  store i32 %18, i32* %frame_h, align 4, !tbaa !6
  %right10 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 2
  %19 = load i32, i32* %right10, align 4, !tbaa !58
  %20 = load i32, i32* %frame_w, align 4, !tbaa !6
  %cmp = icmp slt i32 %19, %20
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %right12 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 2
  %21 = load i32, i32* %right12, align 4, !tbaa !58
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %22 = load i32, i32* %frame_w, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %21, %cond.true ], [ %22, %cond.false ]
  %right13 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 2
  store i32 %cond, i32* %right13, align 4, !tbaa !58
  %bottom14 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 3
  %23 = load i32, i32* %bottom14, align 4, !tbaa !60
  %24 = load i32, i32* %frame_h, align 4, !tbaa !6
  %cmp15 = icmp slt i32 %23, %24
  br i1 %cmp15, label %cond.true17, label %cond.false19

cond.true17:                                      ; preds = %cond.end
  %bottom18 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 3
  %25 = load i32, i32* %bottom18, align 4, !tbaa !60
  br label %cond.end20

cond.false19:                                     ; preds = %cond.end
  %26 = load i32, i32* %frame_h, align 4, !tbaa !6
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false19, %cond.true17
  %cond21 = phi i32 [ %25, %cond.true17 ], [ %26, %cond.false19 ]
  %bottom22 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 3
  store i32 %cond21, i32* %bottom22, align 4, !tbaa !60
  %27 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load i32, i32* %is_uv.addr, align 4, !tbaa !6
  %tobool23 = icmp ne i32 %28, 0
  br i1 %tobool23, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %cond.end20
  %29 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %29, i32 0, i32 37
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 32
  %30 = load i32, i32* %subsampling_x, align 16, !tbaa !64
  %tobool24 = icmp ne i32 %30, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %cond.end20
  %31 = phi i1 [ false, %cond.end20 ], [ %tobool24, %land.rhs ]
  %land.ext = zext i1 %31 to i32
  store i32 %land.ext, i32* %ss_x, align 4, !tbaa !6
  %32 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  %33 = load i32, i32* %is_uv.addr, align 4, !tbaa !6
  %tobool25 = icmp ne i32 %33, 0
  br i1 %tobool25, label %land.rhs26, label %land.end29

land.rhs26:                                       ; preds = %land.end
  %34 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params27 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %34, i32 0, i32 37
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params27, i32 0, i32 33
  %35 = load i32, i32* %subsampling_y, align 4, !tbaa !65
  %tobool28 = icmp ne i32 %35, 0
  br label %land.end29

land.end29:                                       ; preds = %land.rhs26, %land.end
  %36 = phi i1 [ false, %land.end ], [ %tobool28, %land.rhs26 ]
  %land.ext30 = zext i1 %36 to i32
  store i32 %land.ext30, i32* %ss_y, align 4, !tbaa !6
  %left31 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 0
  %37 = load i32, i32* %left31, align 4, !tbaa !57
  %38 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shl = shl i32 1, %38
  %shr = ashr i32 %shl, 1
  %add = add nsw i32 %37, %shr
  %39 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shr32 = ashr i32 %add, %39
  %left33 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 0
  store i32 %shr32, i32* %left33, align 4, !tbaa !57
  %right34 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 2
  %40 = load i32, i32* %right34, align 4, !tbaa !58
  %41 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shl35 = shl i32 1, %41
  %shr36 = ashr i32 %shl35, 1
  %add37 = add nsw i32 %40, %shr36
  %42 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shr38 = ashr i32 %add37, %42
  %right39 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 2
  store i32 %shr38, i32* %right39, align 4, !tbaa !58
  %top40 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 1
  %43 = load i32, i32* %top40, align 4, !tbaa !59
  %44 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shl41 = shl i32 1, %44
  %shr42 = ashr i32 %shl41, 1
  %add43 = add nsw i32 %43, %shr42
  %45 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shr44 = ashr i32 %add43, %45
  %top45 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 1
  store i32 %shr44, i32* %top45, align 4, !tbaa !59
  %bottom46 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 3
  %46 = load i32, i32* %bottom46, align 4, !tbaa !60
  %47 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shl47 = shl i32 1, %47
  %shr48 = ashr i32 %shl47, 1
  %add49 = add nsw i32 %46, %shr48
  %48 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shr50 = ashr i32 %add49, %48
  %bottom51 = getelementptr inbounds %struct.AV1PixelRect, %struct.AV1PixelRect* %agg.result, i32 0, i32 3
  store i32 %shr50, i32* %bottom51, align 4, !tbaa !60
  %49 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %frame_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %frame_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_superres_scaled(%struct.AV1Common* %cm) #2 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 2
  %1 = load i32, i32* %width, align 16, !tbaa !66
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 6
  %3 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !62
  %cmp = icmp eq i32 %1, %3
  %lnot = xor i1 %cmp, true
  %lnot.ext = zext i1 %lnot to i32
  ret i32 %lnot.ext
}

declare void @av1_calculate_unscaled_superres_size(i32*, i32*, i32) #3

; Function Attrs: nounwind
define hidden void @av1_get_uniform_tile_size(%struct.AV1Common* %cm, i32* %w, i32* %h) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %w.addr = alloca i32*, align 4
  %h.addr = alloca i32*, align 4
  %tiles = alloca %struct.CommonTileParams*, align 4
  %i = alloca i32, align 4
  %tile_width_sb = alloca i32, align 4
  %tile_w = alloca i32, align 4
  %i4 = alloca i32, align 4
  %tile_height_sb = alloca i32, align 4
  %tile_h = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32* %w, i32** %w.addr, align 4, !tbaa !2
  store i32* %h, i32** %h.addr, align 4, !tbaa !2
  %0 = bitcast %struct.CommonTileParams** %tiles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tiles1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 40
  store %struct.CommonTileParams* %tiles1, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %2 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %uniform_spacing = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %2, i32 0, i32 5
  %3 = load i32, i32* %uniform_spacing, align 4, !tbaa !48
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %4, i32 0, i32 8
  %5 = load i32, i32* %width, align 4, !tbaa !53
  %6 = load i32*, i32** %w.addr, align 4, !tbaa !2
  store i32 %5, i32* %6, align 4, !tbaa !6
  %7 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %7, i32 0, i32 9
  %8 = load i32, i32* %height, align 4, !tbaa !56
  %9 = load i32*, i32** %h.addr, align 4, !tbaa !2
  store i32 %8, i32* %9, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %cols = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %12, i32 0, i32 0
  %13 = load i32, i32* %cols, align 4, !tbaa !50
  %cmp = icmp slt i32 %11, %13
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %15 = bitcast i32* %tile_width_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %col_start_sb = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %16, i32 0, i32 15
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %17, 1
  %arrayidx = getelementptr inbounds [65 x i32], [65 x i32]* %col_start_sb, i32 0, i32 %add
  %18 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %19 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %col_start_sb2 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %19, i32 0, i32 15
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [65 x i32], [65 x i32]* %col_start_sb2, i32 0, i32 %20
  %21 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %sub = sub nsw i32 %18, %21
  store i32 %sub, i32* %tile_width_sb, align 4, !tbaa !6
  %22 = bitcast i32* %tile_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  %23 = load i32, i32* %tile_width_sb, align 4, !tbaa !6
  %24 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %24, i32 0, i32 37
  %mib_size = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 8
  %25 = load i32, i32* %mib_size, align 16, !tbaa !67
  %mul = mul nsw i32 %23, %25
  store i32 %mul, i32* %tile_w, align 4, !tbaa !6
  %26 = load i32, i32* %tile_w, align 4, !tbaa !6
  %27 = load i32*, i32** %w.addr, align 4, !tbaa !2
  store i32 %26, i32* %27, align 4, !tbaa !6
  %28 = bitcast i32* %tile_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  %29 = bitcast i32* %tile_width_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %31 = bitcast i32* %i4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  store i32 0, i32* %i4, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc17, %for.end
  %32 = load i32, i32* %i4, align 4, !tbaa !6
  %33 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %33, i32 0, i32 1
  %34 = load i32, i32* %rows, align 4, !tbaa !55
  %cmp6 = icmp slt i32 %32, %34
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond5
  %35 = bitcast i32* %i4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  br label %for.end19

for.body8:                                        ; preds = %for.cond5
  %36 = bitcast i32* %tile_height_sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  %37 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %row_start_sb = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %37, i32 0, i32 16
  %38 = load i32, i32* %i4, align 4, !tbaa !6
  %add9 = add nsw i32 %38, 1
  %arrayidx10 = getelementptr inbounds [65 x i32], [65 x i32]* %row_start_sb, i32 0, i32 %add9
  %39 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %40 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %row_start_sb11 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %40, i32 0, i32 16
  %41 = load i32, i32* %i4, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [65 x i32], [65 x i32]* %row_start_sb11, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %sub13 = sub nsw i32 %39, %42
  store i32 %sub13, i32* %tile_height_sb, align 4, !tbaa !6
  %43 = bitcast i32* %tile_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #4
  %44 = load i32, i32* %tile_height_sb, align 4, !tbaa !6
  %45 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params14 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %45, i32 0, i32 37
  %mib_size15 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params14, i32 0, i32 8
  %46 = load i32, i32* %mib_size15, align 16, !tbaa !67
  %mul16 = mul nsw i32 %44, %46
  store i32 %mul16, i32* %tile_h, align 4, !tbaa !6
  %47 = load i32, i32* %tile_h, align 4, !tbaa !6
  %48 = load i32*, i32** %h.addr, align 4, !tbaa !2
  store i32 %47, i32* %48, align 4, !tbaa !6
  %49 = bitcast i32* %tile_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %tile_height_sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  br label %for.inc17

for.inc17:                                        ; preds = %for.body8
  %51 = load i32, i32* %i4, align 4, !tbaa !6
  %inc18 = add nsw i32 %51, 1
  store i32 %inc18, i32* %i4, align 4, !tbaa !6
  br label %for.cond5

for.end19:                                        ; preds = %for.cond.cleanup7
  br label %if.end

if.end:                                           ; preds = %for.end19, %if.then
  %52 = bitcast %struct.CommonTileParams** %tiles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_is_min_tile_width_satisfied(%struct.AV1Common* %cm) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 40
  %cols = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles, i32 0, i32 0
  %1 = load i32, i32* %cols, align 8, !tbaa !68
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %tiles1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 40
  %min_inner_width = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles1, i32 0, i32 4
  %3 = load i32, i32* %min_inner_width, align 8, !tbaa !69
  %shl = shl i32 %3, 2
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_superres_scaled(%struct.AV1Common* %4)
  %shl2 = shl i32 64, %call
  %cmp3 = icmp sge i32 %shl, %shl2
  %conv = zext i1 %cmp3 to i32
  store i32 %conv, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 16228}
!9 = !{!"AV1Common", !10, i64 0, !12, i64 40, !7, i64 288, !7, i64 292, !7, i64 296, !7, i64 300, !7, i64 304, !7, i64 308, !4, i64 312, !13, i64 313, !4, i64 316, !7, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !14, i64 492, !4, i64 580, !4, i64 1284, !7, i64 1316, !7, i64 1320, !7, i64 1324, !15, i64 1328, !16, i64 1356, !17, i64 1420, !18, i64 10676, !3, i64 10848, !19, i64 10864, !20, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !21, i64 14880, !23, i64 15028, !24, i64 15168, !26, i64 15816, !4, i64 15836, !27, i64 16192, !3, i64 18128, !3, i64 18132, !30, i64 18136, !3, i64 18724, !31, i64 18728, !7, i64 18760, !4, i64 18764, !3, i64 18796, !7, i64 18800, !4, i64 18804, !4, i64 18836, !7, i64 18844, !7, i64 18848, !7, i64 18852, !7, i64 18856}
!10 = !{!"", !4, i64 0, !4, i64 1, !7, i64 4, !7, i64 8, !7, i64 12, !11, i64 16, !7, i64 32, !7, i64 36}
!11 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!12 = !{!"aom_internal_error_info", !4, i64 0, !7, i64 4, !4, i64 8, !7, i64 88, !4, i64 92}
!13 = !{!"_Bool", !4, i64 0}
!14 = !{!"scale_factors", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!15 = !{!"", !13, i64 0, !13, i64 1, !13, i64 2, !13, i64 3, !13, i64 4, !13, i64 5, !13, i64 6, !13, i64 7, !13, i64 8, !13, i64 9, !13, i64 10, !13, i64 11, !4, i64 12, !4, i64 13, !7, i64 16, !7, i64 20, !4, i64 24}
!16 = !{!"CommonModeInfoParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !7, i64 28, !4, i64 32, !3, i64 36, !7, i64 40, !7, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!17 = !{!"CommonQuantParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !13, i64 9240, !7, i64 9244, !7, i64 9248, !7, i64 9252}
!18 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !7, i64 164, !4, i64 168}
!19 = !{!"", !4, i64 0, !4, i64 3072}
!20 = !{!"loopfilter", !4, i64 0, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !7, i64 32}
!21 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !22, i64 80, !7, i64 84, !22, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!22 = !{!"long", !4, i64 0}
!23 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !4, i64 72, !7, i64 136}
!24 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !25, i64 644}
!25 = !{!"short", !4, i64 0}
!26 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!27 = !{!"SequenceHeader", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !4, i64 16, !7, i64 20, !7, i64 24, !4, i64 28, !7, i64 32, !7, i64 36, !11, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !7, i64 112, !4, i64 116, !7, i64 244, !28, i64 248, !4, i64 264, !29, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!28 = !{!"aom_timing", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!29 = !{!"aom_dec_model_info", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!30 = !{!"CommonTileParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !4, i64 60, !4, i64 320, !7, i64 580, !7, i64 584}
!31 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !7, i64 20, !7, i64 24, !7, i64 28}
!32 = !{!33, !7, i64 16}
!33 = !{!"TileInfo", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20}
!34 = !{!33, !7, i64 0}
!35 = !{!9, !7, i64 1368}
!36 = !{!33, !7, i64 4}
!37 = !{!33, !7, i64 20}
!38 = !{!33, !7, i64 8}
!39 = !{!9, !7, i64 1372}
!40 = !{!33, !7, i64 12}
!41 = !{!27, !7, i64 36}
!42 = !{!30, !7, i64 8}
!43 = !{!30, !7, i64 40}
!44 = !{!30, !7, i64 48}
!45 = !{!30, !7, i64 52}
!46 = !{!30, !7, i64 56}
!47 = !{!30, !7, i64 16}
!48 = !{!30, !7, i64 20}
!49 = !{!30, !7, i64 24}
!50 = !{!30, !7, i64 0}
!51 = !{!30, !7, i64 44}
!52 = !{!30, !7, i64 12}
!53 = !{!30, !7, i64 32}
!54 = !{!30, !7, i64 28}
!55 = !{!30, !7, i64 4}
!56 = !{!30, !7, i64 36}
!57 = !{!11, !7, i64 0}
!58 = !{!11, !7, i64 8}
!59 = !{!11, !7, i64 4}
!60 = !{!11, !7, i64 12}
!61 = !{!9, !4, i64 312}
!62 = !{!9, !7, i64 304}
!63 = !{!9, !7, i64 308}
!64 = !{!9, !7, i64 16288}
!65 = !{!9, !7, i64 16292}
!66 = !{!9, !7, i64 288}
!67 = !{!9, !7, i64 16224}
!68 = !{!9, !7, i64 18136}
!69 = !{!9, !7, i64 18152}
