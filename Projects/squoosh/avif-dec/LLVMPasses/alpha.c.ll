; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-dec/src/alpha.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-dec/src/alpha.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifAlphaParams = type { i32, i32, i32, i32, i8*, i32, i32, i32, i32, i32, i8*, i32, i32, i32 }

; Function Attrs: nounwind
define hidden i32 @avifFillAlpha(%struct.avifAlphaParams* %params) #0 {
entry:
  %params.addr = alloca %struct.avifAlphaParams*, align 4
  %maxChannel = alloca i16, align 2
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %dstRow = alloca i8*, align 4
  %i = alloca i32, align 4
  %maxChannel12 = alloca i8, align 1
  %j17 = alloca i32, align 4
  %dstRow24 = alloca i8*, align 4
  %i31 = alloca i32, align 4
  store %struct.avifAlphaParams* %params, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %0 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %0, i32 0, i32 8
  %1 = load i32, i32* %dstDepth, align 4, !tbaa !6
  %cmp = icmp ugt i32 %1, 8
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast i16* %maxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %2) #3
  %3 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth1 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %3, i32 0, i32 8
  %4 = load i32, i32* %dstDepth1, align 4, !tbaa !6
  %5 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %5, i32 0, i32 9
  %6 = load i32, i32* %dstRange, align 4, !tbaa !9
  %call = call i32 @calcMaxChannel(i32 %4, i32 %6)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %maxChannel, align 2, !tbaa !10
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  store i32 0, i32* %j, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc9, %if.then
  %8 = load i32, i32* %j, align 4, !tbaa !12
  %9 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %9, i32 0, i32 1
  %10 = load i32, i32* %height, align 4, !tbaa !13
  %cmp2 = icmp ult i32 %8, %10
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  br label %for.end11

for.body:                                         ; preds = %for.cond
  %12 = bitcast i8** %dstRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %13, i32 0, i32 10
  %14 = load i8*, i8** %dstPlane, align 4, !tbaa !14
  %15 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %15, i32 0, i32 12
  %16 = load i32, i32* %dstOffsetBytes, align 4, !tbaa !15
  %17 = load i32, i32* %j, align 4, !tbaa !12
  %18 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %18, i32 0, i32 11
  %19 = load i32, i32* %dstRowBytes, align 4, !tbaa !16
  %mul = mul i32 %17, %19
  %add = add i32 %16, %mul
  %arrayidx = getelementptr inbounds i8, i8* %14, i32 %add
  store i8* %arrayidx, i8** %dstRow, align 4, !tbaa !2
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %21 = load i32, i32* %i, align 4, !tbaa !12
  %22 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %22, i32 0, i32 0
  %23 = load i32, i32* %width, align 4, !tbaa !17
  %cmp5 = icmp ult i32 %21, %23
  br i1 %cmp5, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond4
  store i32 5, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  br label %for.end

for.body8:                                        ; preds = %for.cond4
  %25 = load i16, i16* %maxChannel, align 2, !tbaa !10
  %26 = load i8*, i8** %dstRow, align 4, !tbaa !2
  %27 = bitcast i8* %26 to i16*
  store i16 %25, i16* %27, align 2, !tbaa !10
  %28 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %28, i32 0, i32 13
  %29 = load i32, i32* %dstPixelBytes, align 4, !tbaa !18
  %30 = load i8*, i8** %dstRow, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %30, i32 %29
  store i8* %add.ptr, i8** %dstRow, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %31 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %31, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond4

for.end:                                          ; preds = %for.cond.cleanup7
  %32 = bitcast i8** %dstRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  br label %for.inc9

for.inc9:                                         ; preds = %for.end
  %33 = load i32, i32* %j, align 4, !tbaa !12
  %inc10 = add i32 %33, 1
  store i32 %inc10, i32* %j, align 4, !tbaa !12
  br label %for.cond

for.end11:                                        ; preds = %for.cond.cleanup
  %34 = bitcast i16* %maxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %34) #3
  br label %if.end

if.else:                                          ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %maxChannel12) #3
  %35 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth13 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %35, i32 0, i32 8
  %36 = load i32, i32* %dstDepth13, align 4, !tbaa !6
  %37 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange14 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %37, i32 0, i32 9
  %38 = load i32, i32* %dstRange14, align 4, !tbaa !9
  %call15 = call i32 @calcMaxChannel(i32 %36, i32 %38)
  %conv16 = trunc i32 %call15 to i8
  store i8 %conv16, i8* %maxChannel12, align 1, !tbaa !19
  %39 = bitcast i32* %j17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #3
  store i32 0, i32* %j17, align 4, !tbaa !12
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc43, %if.else
  %40 = load i32, i32* %j17, align 4, !tbaa !12
  %41 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height19 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %41, i32 0, i32 1
  %42 = load i32, i32* %height19, align 4, !tbaa !13
  %cmp20 = icmp ult i32 %40, %42
  br i1 %cmp20, label %for.body23, label %for.cond.cleanup22

for.cond.cleanup22:                               ; preds = %for.cond18
  store i32 8, i32* %cleanup.dest.slot, align 4
  %43 = bitcast i32* %j17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  br label %for.end45

for.body23:                                       ; preds = %for.cond18
  %44 = bitcast i8** %dstRow24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane25 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %45, i32 0, i32 10
  %46 = load i8*, i8** %dstPlane25, align 4, !tbaa !14
  %47 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes26 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %47, i32 0, i32 12
  %48 = load i32, i32* %dstOffsetBytes26, align 4, !tbaa !15
  %49 = load i32, i32* %j17, align 4, !tbaa !12
  %50 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes27 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %50, i32 0, i32 11
  %51 = load i32, i32* %dstRowBytes27, align 4, !tbaa !16
  %mul28 = mul i32 %49, %51
  %add29 = add i32 %48, %mul28
  %arrayidx30 = getelementptr inbounds i8, i8* %46, i32 %add29
  store i8* %arrayidx30, i8** %dstRow24, align 4, !tbaa !2
  %52 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  store i32 0, i32* %i31, align 4, !tbaa !12
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc40, %for.body23
  %53 = load i32, i32* %i31, align 4, !tbaa !12
  %54 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width33 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %54, i32 0, i32 0
  %55 = load i32, i32* %width33, align 4, !tbaa !17
  %cmp34 = icmp ult i32 %53, %55
  br i1 %cmp34, label %for.body37, label %for.cond.cleanup36

for.cond.cleanup36:                               ; preds = %for.cond32
  store i32 11, i32* %cleanup.dest.slot, align 4
  %56 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  br label %for.end42

for.body37:                                       ; preds = %for.cond32
  %57 = load i8, i8* %maxChannel12, align 1, !tbaa !19
  %58 = load i8*, i8** %dstRow24, align 4, !tbaa !2
  store i8 %57, i8* %58, align 1, !tbaa !19
  %59 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes38 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %59, i32 0, i32 13
  %60 = load i32, i32* %dstPixelBytes38, align 4, !tbaa !18
  %61 = load i8*, i8** %dstRow24, align 4, !tbaa !2
  %add.ptr39 = getelementptr inbounds i8, i8* %61, i32 %60
  store i8* %add.ptr39, i8** %dstRow24, align 4, !tbaa !2
  br label %for.inc40

for.inc40:                                        ; preds = %for.body37
  %62 = load i32, i32* %i31, align 4, !tbaa !12
  %inc41 = add i32 %62, 1
  store i32 %inc41, i32* %i31, align 4, !tbaa !12
  br label %for.cond32

for.end42:                                        ; preds = %for.cond.cleanup36
  %63 = bitcast i8** %dstRow24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #3
  br label %for.inc43

for.inc43:                                        ; preds = %for.end42
  %64 = load i32, i32* %j17, align 4, !tbaa !12
  %inc44 = add i32 %64, 1
  store i32 %inc44, i32* %j17, align 4, !tbaa !12
  br label %for.cond18

for.end45:                                        ; preds = %for.cond.cleanup22
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %maxChannel12) #3
  br label %if.end

if.end:                                           ; preds = %for.end45, %for.end11
  ret i32 1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @calcMaxChannel(i32 %depth, i32 %range) #0 {
entry:
  %depth.addr = alloca i32, align 4
  %range.addr = alloca i32, align 4
  %maxChannel = alloca i32, align 4
  store i32 %depth, i32* %depth.addr, align 4, !tbaa !12
  store i32 %range, i32* %range.addr, align 4, !tbaa !19
  %0 = bitcast i32* %maxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i32, i32* %depth.addr, align 4, !tbaa !12
  %shl = shl i32 1, %1
  %sub = sub nsw i32 %shl, 1
  store i32 %sub, i32* %maxChannel, align 4, !tbaa !12
  %2 = load i32, i32* %range.addr, align 4, !tbaa !19
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %depth.addr, align 4, !tbaa !12
  %4 = load i32, i32* %maxChannel, align 4, !tbaa !12
  %call = call i32 @avifFullToLimitedY(i32 %3, i32 %4)
  store i32 %call, i32* %maxChannel, align 4, !tbaa !12
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i32, i32* %maxChannel, align 4, !tbaa !12
  %6 = bitcast i32* %maxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #3
  ret i32 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @avifReformatAlpha(%struct.avifAlphaParams* %params) #0 {
entry:
  %params.addr = alloca %struct.avifAlphaParams*, align 4
  %srcMaxChannel = alloca i32, align 4
  %dstMaxChannel = alloca i32, align 4
  %srcMaxChannelF = alloca float, align 4
  %dstMaxChannelF = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %srcRow = alloca i8*, align 4
  %dstRow = alloca i8*, align 4
  %i = alloca i32, align 4
  %j33 = alloca i32, align 4
  %srcRow40 = alloca i8*, align 4
  %dstRow47 = alloca i8*, align 4
  %i54 = alloca i32, align 4
  %j86 = alloca i32, align 4
  %srcRow93 = alloca i8*, align 4
  %dstRow100 = alloca i8*, align 4
  %i107 = alloca i32, align 4
  %srcAlpha = alloca i32, align 4
  %dstAlpha = alloca i32, align 4
  %j130 = alloca i32, align 4
  %srcRow137 = alloca i8*, align 4
  %dstRow144 = alloca i8*, align 4
  %i151 = alloca i32, align 4
  %srcAlpha158 = alloca i32, align 4
  %dstAlpha163 = alloca i32, align 4
  %j190 = alloca i32, align 4
  %srcRow197 = alloca i8*, align 4
  %dstRow204 = alloca i8*, align 4
  %i211 = alloca i32, align 4
  %srcAlpha218 = alloca i32, align 4
  %dstAlpha223 = alloca i32, align 4
  %j237 = alloca i32, align 4
  %srcRow244 = alloca i8*, align 4
  %dstRow251 = alloca i8*, align 4
  %i258 = alloca i32, align 4
  %srcAlpha265 = alloca i32, align 4
  %dstAlpha270 = alloca i32, align 4
  %j297 = alloca i32, align 4
  %srcRow304 = alloca i8*, align 4
  %dstRow311 = alloca i8*, align 4
  %i318 = alloca i32, align 4
  %j338 = alloca i32, align 4
  %srcRow345 = alloca i8*, align 4
  %dstRow352 = alloca i8*, align 4
  %i359 = alloca i32, align 4
  %j400 = alloca i32, align 4
  %srcRow407 = alloca i8*, align 4
  %dstRow414 = alloca i8*, align 4
  %i421 = alloca i32, align 4
  %srcAlpha428 = alloca i32, align 4
  %alphaF = alloca float, align 4
  %dstAlpha434 = alloca i32, align 4
  %j457 = alloca i32, align 4
  %srcRow464 = alloca i8*, align 4
  %dstRow471 = alloca i8*, align 4
  %i478 = alloca i32, align 4
  %srcAlpha485 = alloca i32, align 4
  %alphaF490 = alloca float, align 4
  %dstAlpha493 = alloca i32, align 4
  %j521 = alloca i32, align 4
  %srcRow528 = alloca i8*, align 4
  %dstRow535 = alloca i8*, align 4
  %i542 = alloca i32, align 4
  %srcAlpha549 = alloca i32, align 4
  %alphaF554 = alloca float, align 4
  %dstAlpha557 = alloca i32, align 4
  %j601 = alloca i32, align 4
  %srcRow608 = alloca i8*, align 4
  %dstRow615 = alloca i8*, align 4
  %i622 = alloca i32, align 4
  %srcAlpha629 = alloca i32, align 4
  %alphaF636 = alloca float, align 4
  %dstAlpha639 = alloca i32, align 4
  %j666 = alloca i32, align 4
  %srcRow673 = alloca i8*, align 4
  %dstRow680 = alloca i8*, align 4
  %i687 = alloca i32, align 4
  %srcAlpha694 = alloca i32, align 4
  %alphaF701 = alloca float, align 4
  %dstAlpha704 = alloca i32, align 4
  %j732 = alloca i32, align 4
  %srcRow739 = alloca i8*, align 4
  %dstRow746 = alloca i8*, align 4
  %i753 = alloca i32, align 4
  %srcAlpha760 = alloca i32, align 4
  %alphaF767 = alloca float, align 4
  %dstAlpha770 = alloca i32, align 4
  %j814 = alloca i32, align 4
  %srcRow821 = alloca i8*, align 4
  %dstRow828 = alloca i8*, align 4
  %i835 = alloca i32, align 4
  %srcAlpha842 = alloca i32, align 4
  %alphaF847 = alloca float, align 4
  %dstAlpha850 = alloca i32, align 4
  %j879 = alloca i32, align 4
  %srcRow886 = alloca i8*, align 4
  %dstRow893 = alloca i8*, align 4
  %i900 = alloca i32, align 4
  %srcAlpha907 = alloca i32, align 4
  %alphaF912 = alloca float, align 4
  %dstAlpha915 = alloca i32, align 4
  %j945 = alloca i32, align 4
  %srcRow952 = alloca i8*, align 4
  %dstRow959 = alloca i8*, align 4
  %i966 = alloca i32, align 4
  %srcAlpha973 = alloca i32, align 4
  %alphaF978 = alloca float, align 4
  %dstAlpha981 = alloca i32, align 4
  %j1027 = alloca i32, align 4
  %srcRow1034 = alloca i8*, align 4
  %dstRow1041 = alloca i8*, align 4
  %i1048 = alloca i32, align 4
  %srcAlpha1055 = alloca i32, align 4
  %alphaF1062 = alloca float, align 4
  %dstAlpha1065 = alloca i32, align 4
  %j1094 = alloca i32, align 4
  %srcRow1101 = alloca i8*, align 4
  %dstRow1108 = alloca i8*, align 4
  %i1115 = alloca i32, align 4
  %srcAlpha1122 = alloca i32, align 4
  %alphaF1129 = alloca float, align 4
  %dstAlpha1132 = alloca i32, align 4
  %j1162 = alloca i32, align 4
  %srcRow1169 = alloca i8*, align 4
  %dstRow1176 = alloca i8*, align 4
  %i1183 = alloca i32, align 4
  %srcAlpha1190 = alloca i32, align 4
  %alphaF1197 = alloca float, align 4
  %dstAlpha1200 = alloca i32, align 4
  store %struct.avifAlphaParams* %params, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %0 = bitcast i32* %srcMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1, i32 0, i32 2
  %2 = load i32, i32* %srcDepth, align 4, !tbaa !20
  %shl = shl i32 1, %2
  %sub = sub nsw i32 %shl, 1
  store i32 %sub, i32* %srcMaxChannel, align 4, !tbaa !12
  %3 = bitcast i32* %dstMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %4, i32 0, i32 8
  %5 = load i32, i32* %dstDepth, align 4, !tbaa !6
  %shl1 = shl i32 1, %5
  %sub2 = sub nsw i32 %shl1, 1
  store i32 %sub2, i32* %dstMaxChannel, align 4, !tbaa !12
  %6 = bitcast float* %srcMaxChannelF to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load i32, i32* %srcMaxChannel, align 4, !tbaa !12
  %conv = sitofp i32 %7 to float
  store float %conv, float* %srcMaxChannelF, align 4, !tbaa !21
  %8 = bitcast float* %dstMaxChannelF to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %conv3 = sitofp i32 %9 to float
  store float %conv3, float* %dstMaxChannelF, align 4, !tbaa !21
  %10 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth4 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %10, i32 0, i32 2
  %11 = load i32, i32* %srcDepth4, align 4, !tbaa !20
  %12 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth5 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %12, i32 0, i32 8
  %13 = load i32, i32* %dstDepth5, align 4, !tbaa !6
  %cmp = icmp eq i32 %11, %13
  br i1 %cmp, label %if.then, label %if.else383

if.then:                                          ; preds = %entry
  %14 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRange = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %14, i32 0, i32 3
  %15 = load i32, i32* %srcRange, align 4, !tbaa !23
  %cmp7 = icmp eq i32 %15, 1
  br i1 %cmp7, label %land.lhs.true, label %if.else73

land.lhs.true:                                    ; preds = %if.then
  %16 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %16, i32 0, i32 9
  %17 = load i32, i32* %dstRange, align 4, !tbaa !9
  %cmp9 = icmp eq i32 %17, 1
  br i1 %cmp9, label %if.then11, label %if.else73

if.then11:                                        ; preds = %land.lhs.true
  %18 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth12 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %18, i32 0, i32 2
  %19 = load i32, i32* %srcDepth12, align 4, !tbaa !20
  %cmp13 = icmp ugt i32 %19, 8
  br i1 %cmp13, label %if.then15, label %if.else

if.then15:                                        ; preds = %if.then11
  %20 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  store i32 0, i32* %j, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc30, %if.then15
  %21 = load i32, i32* %j, align 4, !tbaa !12
  %22 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %22, i32 0, i32 1
  %23 = load i32, i32* %height, align 4, !tbaa !13
  %cmp16 = icmp ult i32 %21, %23
  br i1 %cmp16, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  br label %for.end32

for.body:                                         ; preds = %for.cond
  %25 = bitcast i8** %srcRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  %26 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %26, i32 0, i32 4
  %27 = load i8*, i8** %srcPlane, align 4, !tbaa !24
  %28 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %28, i32 0, i32 6
  %29 = load i32, i32* %srcOffsetBytes, align 4, !tbaa !25
  %30 = load i32, i32* %j, align 4, !tbaa !12
  %31 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %31, i32 0, i32 5
  %32 = load i32, i32* %srcRowBytes, align 4, !tbaa !26
  %mul = mul i32 %30, %32
  %add = add i32 %29, %mul
  %arrayidx = getelementptr inbounds i8, i8* %27, i32 %add
  store i8* %arrayidx, i8** %srcRow, align 4, !tbaa !2
  %33 = bitcast i8** %dstRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #3
  %34 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %34, i32 0, i32 10
  %35 = load i8*, i8** %dstPlane, align 4, !tbaa !14
  %36 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %36, i32 0, i32 12
  %37 = load i32, i32* %dstOffsetBytes, align 4, !tbaa !15
  %38 = load i32, i32* %j, align 4, !tbaa !12
  %39 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %39, i32 0, i32 11
  %40 = load i32, i32* %dstRowBytes, align 4, !tbaa !16
  %mul18 = mul i32 %38, %40
  %add19 = add i32 %37, %mul18
  %arrayidx20 = getelementptr inbounds i8, i8* %35, i32 %add19
  store i8* %arrayidx20, i8** %dstRow, align 4, !tbaa !2
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #3
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc, %for.body
  %42 = load i32, i32* %i, align 4, !tbaa !12
  %43 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %43, i32 0, i32 0
  %44 = load i32, i32* %width, align 4, !tbaa !17
  %cmp22 = icmp ult i32 %42, %44
  br i1 %cmp22, label %for.body25, label %for.cond.cleanup24

for.cond.cleanup24:                               ; preds = %for.cond21
  store i32 5, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  br label %for.end

for.body25:                                       ; preds = %for.cond21
  %46 = load i8*, i8** %srcRow, align 4, !tbaa !2
  %47 = load i32, i32* %i, align 4, !tbaa !12
  %48 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %48, i32 0, i32 7
  %49 = load i32, i32* %srcPixelBytes, align 4, !tbaa !27
  %mul26 = mul i32 %47, %49
  %arrayidx27 = getelementptr inbounds i8, i8* %46, i32 %mul26
  %50 = bitcast i8* %arrayidx27 to i16*
  %51 = load i16, i16* %50, align 2, !tbaa !10
  %52 = load i8*, i8** %dstRow, align 4, !tbaa !2
  %53 = load i32, i32* %i, align 4, !tbaa !12
  %54 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %54, i32 0, i32 13
  %55 = load i32, i32* %dstPixelBytes, align 4, !tbaa !18
  %mul28 = mul i32 %53, %55
  %arrayidx29 = getelementptr inbounds i8, i8* %52, i32 %mul28
  %56 = bitcast i8* %arrayidx29 to i16*
  store i16 %51, i16* %56, align 2, !tbaa !10
  br label %for.inc

for.inc:                                          ; preds = %for.body25
  %57 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %57, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond21

for.end:                                          ; preds = %for.cond.cleanup24
  %58 = bitcast i8** %dstRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  %59 = bitcast i8** %srcRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  br label %for.inc30

for.inc30:                                        ; preds = %for.end
  %60 = load i32, i32* %j, align 4, !tbaa !12
  %inc31 = add i32 %60, 1
  store i32 %inc31, i32* %j, align 4, !tbaa !12
  br label %for.cond

for.end32:                                        ; preds = %for.cond.cleanup
  br label %if.end

if.else:                                          ; preds = %if.then11
  %61 = bitcast i32* %j33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #3
  store i32 0, i32* %j33, align 4, !tbaa !12
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc70, %if.else
  %62 = load i32, i32* %j33, align 4, !tbaa !12
  %63 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height35 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %63, i32 0, i32 1
  %64 = load i32, i32* %height35, align 4, !tbaa !13
  %cmp36 = icmp ult i32 %62, %64
  br i1 %cmp36, label %for.body39, label %for.cond.cleanup38

for.cond.cleanup38:                               ; preds = %for.cond34
  store i32 8, i32* %cleanup.dest.slot, align 4
  %65 = bitcast i32* %j33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #3
  br label %for.end72

for.body39:                                       ; preds = %for.cond34
  %66 = bitcast i8** %srcRow40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #3
  %67 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane41 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %67, i32 0, i32 4
  %68 = load i8*, i8** %srcPlane41, align 4, !tbaa !24
  %69 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes42 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %69, i32 0, i32 6
  %70 = load i32, i32* %srcOffsetBytes42, align 4, !tbaa !25
  %71 = load i32, i32* %j33, align 4, !tbaa !12
  %72 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes43 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %72, i32 0, i32 5
  %73 = load i32, i32* %srcRowBytes43, align 4, !tbaa !26
  %mul44 = mul i32 %71, %73
  %add45 = add i32 %70, %mul44
  %arrayidx46 = getelementptr inbounds i8, i8* %68, i32 %add45
  store i8* %arrayidx46, i8** %srcRow40, align 4, !tbaa !2
  %74 = bitcast i8** %dstRow47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #3
  %75 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane48 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %75, i32 0, i32 10
  %76 = load i8*, i8** %dstPlane48, align 4, !tbaa !14
  %77 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes49 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %77, i32 0, i32 12
  %78 = load i32, i32* %dstOffsetBytes49, align 4, !tbaa !15
  %79 = load i32, i32* %j33, align 4, !tbaa !12
  %80 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes50 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %80, i32 0, i32 11
  %81 = load i32, i32* %dstRowBytes50, align 4, !tbaa !16
  %mul51 = mul i32 %79, %81
  %add52 = add i32 %78, %mul51
  %arrayidx53 = getelementptr inbounds i8, i8* %76, i32 %add52
  store i8* %arrayidx53, i8** %dstRow47, align 4, !tbaa !2
  %82 = bitcast i32* %i54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #3
  store i32 0, i32* %i54, align 4, !tbaa !12
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc67, %for.body39
  %83 = load i32, i32* %i54, align 4, !tbaa !12
  %84 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width56 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %84, i32 0, i32 0
  %85 = load i32, i32* %width56, align 4, !tbaa !17
  %cmp57 = icmp ult i32 %83, %85
  br i1 %cmp57, label %for.body60, label %for.cond.cleanup59

for.cond.cleanup59:                               ; preds = %for.cond55
  store i32 11, i32* %cleanup.dest.slot, align 4
  %86 = bitcast i32* %i54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #3
  br label %for.end69

for.body60:                                       ; preds = %for.cond55
  %87 = load i8*, i8** %srcRow40, align 4, !tbaa !2
  %88 = load i32, i32* %i54, align 4, !tbaa !12
  %89 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes61 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %89, i32 0, i32 7
  %90 = load i32, i32* %srcPixelBytes61, align 4, !tbaa !27
  %mul62 = mul i32 %88, %90
  %arrayidx63 = getelementptr inbounds i8, i8* %87, i32 %mul62
  %91 = load i8, i8* %arrayidx63, align 1, !tbaa !19
  %92 = load i8*, i8** %dstRow47, align 4, !tbaa !2
  %93 = load i32, i32* %i54, align 4, !tbaa !12
  %94 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes64 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %94, i32 0, i32 13
  %95 = load i32, i32* %dstPixelBytes64, align 4, !tbaa !18
  %mul65 = mul i32 %93, %95
  %arrayidx66 = getelementptr inbounds i8, i8* %92, i32 %mul65
  store i8 %91, i8* %arrayidx66, align 1, !tbaa !19
  br label %for.inc67

for.inc67:                                        ; preds = %for.body60
  %96 = load i32, i32* %i54, align 4, !tbaa !12
  %inc68 = add i32 %96, 1
  store i32 %inc68, i32* %i54, align 4, !tbaa !12
  br label %for.cond55

for.end69:                                        ; preds = %for.cond.cleanup59
  %97 = bitcast i8** %dstRow47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #3
  %98 = bitcast i8** %srcRow40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #3
  br label %for.inc70

for.inc70:                                        ; preds = %for.end69
  %99 = load i32, i32* %j33, align 4, !tbaa !12
  %inc71 = add i32 %99, 1
  store i32 %inc71, i32* %j33, align 4, !tbaa !12
  br label %for.cond34

for.end72:                                        ; preds = %for.cond.cleanup38
  br label %if.end

if.end:                                           ; preds = %for.end72, %for.end32
  br label %if.end382

if.else73:                                        ; preds = %land.lhs.true, %if.then
  %100 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRange74 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %100, i32 0, i32 3
  %101 = load i32, i32* %srcRange74, align 4, !tbaa !23
  %cmp75 = icmp eq i32 %101, 0
  br i1 %cmp75, label %land.lhs.true77, label %if.else177

land.lhs.true77:                                  ; preds = %if.else73
  %102 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange78 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %102, i32 0, i32 9
  %103 = load i32, i32* %dstRange78, align 4, !tbaa !9
  %cmp79 = icmp eq i32 %103, 1
  br i1 %cmp79, label %if.then81, label %if.else177

if.then81:                                        ; preds = %land.lhs.true77
  %104 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth82 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %104, i32 0, i32 2
  %105 = load i32, i32* %srcDepth82, align 4, !tbaa !20
  %cmp83 = icmp ugt i32 %105, 8
  br i1 %cmp83, label %if.then85, label %if.else129

if.then85:                                        ; preds = %if.then81
  %106 = bitcast i32* %j86 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #3
  store i32 0, i32* %j86, align 4, !tbaa !12
  br label %for.cond87

for.cond87:                                       ; preds = %for.inc126, %if.then85
  %107 = load i32, i32* %j86, align 4, !tbaa !12
  %108 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height88 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %108, i32 0, i32 1
  %109 = load i32, i32* %height88, align 4, !tbaa !13
  %cmp89 = icmp ult i32 %107, %109
  br i1 %cmp89, label %for.body92, label %for.cond.cleanup91

for.cond.cleanup91:                               ; preds = %for.cond87
  store i32 14, i32* %cleanup.dest.slot, align 4
  %110 = bitcast i32* %j86 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  br label %for.end128

for.body92:                                       ; preds = %for.cond87
  %111 = bitcast i8** %srcRow93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #3
  %112 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane94 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %112, i32 0, i32 4
  %113 = load i8*, i8** %srcPlane94, align 4, !tbaa !24
  %114 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes95 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %114, i32 0, i32 6
  %115 = load i32, i32* %srcOffsetBytes95, align 4, !tbaa !25
  %116 = load i32, i32* %j86, align 4, !tbaa !12
  %117 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes96 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %117, i32 0, i32 5
  %118 = load i32, i32* %srcRowBytes96, align 4, !tbaa !26
  %mul97 = mul i32 %116, %118
  %add98 = add i32 %115, %mul97
  %arrayidx99 = getelementptr inbounds i8, i8* %113, i32 %add98
  store i8* %arrayidx99, i8** %srcRow93, align 4, !tbaa !2
  %119 = bitcast i8** %dstRow100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #3
  %120 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane101 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %120, i32 0, i32 10
  %121 = load i8*, i8** %dstPlane101, align 4, !tbaa !14
  %122 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes102 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %122, i32 0, i32 12
  %123 = load i32, i32* %dstOffsetBytes102, align 4, !tbaa !15
  %124 = load i32, i32* %j86, align 4, !tbaa !12
  %125 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes103 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %125, i32 0, i32 11
  %126 = load i32, i32* %dstRowBytes103, align 4, !tbaa !16
  %mul104 = mul i32 %124, %126
  %add105 = add i32 %123, %mul104
  %arrayidx106 = getelementptr inbounds i8, i8* %121, i32 %add105
  store i8* %arrayidx106, i8** %dstRow100, align 4, !tbaa !2
  %127 = bitcast i32* %i107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %127) #3
  store i32 0, i32* %i107, align 4, !tbaa !12
  br label %for.cond108

for.cond108:                                      ; preds = %for.inc123, %for.body92
  %128 = load i32, i32* %i107, align 4, !tbaa !12
  %129 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width109 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %129, i32 0, i32 0
  %130 = load i32, i32* %width109, align 4, !tbaa !17
  %cmp110 = icmp ult i32 %128, %130
  br i1 %cmp110, label %for.body113, label %for.cond.cleanup112

for.cond.cleanup112:                              ; preds = %for.cond108
  store i32 17, i32* %cleanup.dest.slot, align 4
  %131 = bitcast i32* %i107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #3
  br label %for.end125

for.body113:                                      ; preds = %for.cond108
  %132 = bitcast i32* %srcAlpha to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #3
  %133 = load i8*, i8** %srcRow93, align 4, !tbaa !2
  %134 = load i32, i32* %i107, align 4, !tbaa !12
  %135 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes114 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %135, i32 0, i32 7
  %136 = load i32, i32* %srcPixelBytes114, align 4, !tbaa !27
  %mul115 = mul i32 %134, %136
  %arrayidx116 = getelementptr inbounds i8, i8* %133, i32 %mul115
  %137 = bitcast i8* %arrayidx116 to i16*
  %138 = load i16, i16* %137, align 2, !tbaa !10
  %conv117 = zext i16 %138 to i32
  store i32 %conv117, i32* %srcAlpha, align 4, !tbaa !12
  %139 = bitcast i32* %dstAlpha to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #3
  %140 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth118 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %140, i32 0, i32 2
  %141 = load i32, i32* %srcDepth118, align 4, !tbaa !20
  %142 = load i32, i32* %srcAlpha, align 4, !tbaa !12
  %call = call i32 @avifLimitedToFullY(i32 %141, i32 %142)
  store i32 %call, i32* %dstAlpha, align 4, !tbaa !12
  %143 = load i32, i32* %dstAlpha, align 4, !tbaa !12
  %conv119 = trunc i32 %143 to i16
  %144 = load i8*, i8** %dstRow100, align 4, !tbaa !2
  %145 = load i32, i32* %i107, align 4, !tbaa !12
  %146 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes120 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %146, i32 0, i32 13
  %147 = load i32, i32* %dstPixelBytes120, align 4, !tbaa !18
  %mul121 = mul i32 %145, %147
  %arrayidx122 = getelementptr inbounds i8, i8* %144, i32 %mul121
  %148 = bitcast i8* %arrayidx122 to i16*
  store i16 %conv119, i16* %148, align 2, !tbaa !10
  %149 = bitcast i32* %dstAlpha to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #3
  %150 = bitcast i32* %srcAlpha to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #3
  br label %for.inc123

for.inc123:                                       ; preds = %for.body113
  %151 = load i32, i32* %i107, align 4, !tbaa !12
  %inc124 = add i32 %151, 1
  store i32 %inc124, i32* %i107, align 4, !tbaa !12
  br label %for.cond108

for.end125:                                       ; preds = %for.cond.cleanup112
  %152 = bitcast i8** %dstRow100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #3
  %153 = bitcast i8** %srcRow93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #3
  br label %for.inc126

for.inc126:                                       ; preds = %for.end125
  %154 = load i32, i32* %j86, align 4, !tbaa !12
  %inc127 = add i32 %154, 1
  store i32 %inc127, i32* %j86, align 4, !tbaa !12
  br label %for.cond87

for.end128:                                       ; preds = %for.cond.cleanup91
  br label %if.end176

if.else129:                                       ; preds = %if.then81
  %155 = bitcast i32* %j130 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %155) #3
  store i32 0, i32* %j130, align 4, !tbaa !12
  br label %for.cond131

for.cond131:                                      ; preds = %for.inc173, %if.else129
  %156 = load i32, i32* %j130, align 4, !tbaa !12
  %157 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height132 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %157, i32 0, i32 1
  %158 = load i32, i32* %height132, align 4, !tbaa !13
  %cmp133 = icmp ult i32 %156, %158
  br i1 %cmp133, label %for.body136, label %for.cond.cleanup135

for.cond.cleanup135:                              ; preds = %for.cond131
  store i32 20, i32* %cleanup.dest.slot, align 4
  %159 = bitcast i32* %j130 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #3
  br label %for.end175

for.body136:                                      ; preds = %for.cond131
  %160 = bitcast i8** %srcRow137 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #3
  %161 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane138 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %161, i32 0, i32 4
  %162 = load i8*, i8** %srcPlane138, align 4, !tbaa !24
  %163 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes139 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %163, i32 0, i32 6
  %164 = load i32, i32* %srcOffsetBytes139, align 4, !tbaa !25
  %165 = load i32, i32* %j130, align 4, !tbaa !12
  %166 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes140 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %166, i32 0, i32 5
  %167 = load i32, i32* %srcRowBytes140, align 4, !tbaa !26
  %mul141 = mul i32 %165, %167
  %add142 = add i32 %164, %mul141
  %arrayidx143 = getelementptr inbounds i8, i8* %162, i32 %add142
  store i8* %arrayidx143, i8** %srcRow137, align 4, !tbaa !2
  %168 = bitcast i8** %dstRow144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %168) #3
  %169 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane145 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %169, i32 0, i32 10
  %170 = load i8*, i8** %dstPlane145, align 4, !tbaa !14
  %171 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes146 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %171, i32 0, i32 12
  %172 = load i32, i32* %dstOffsetBytes146, align 4, !tbaa !15
  %173 = load i32, i32* %j130, align 4, !tbaa !12
  %174 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes147 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %174, i32 0, i32 11
  %175 = load i32, i32* %dstRowBytes147, align 4, !tbaa !16
  %mul148 = mul i32 %173, %175
  %add149 = add i32 %172, %mul148
  %arrayidx150 = getelementptr inbounds i8, i8* %170, i32 %add149
  store i8* %arrayidx150, i8** %dstRow144, align 4, !tbaa !2
  %176 = bitcast i32* %i151 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %176) #3
  store i32 0, i32* %i151, align 4, !tbaa !12
  br label %for.cond152

for.cond152:                                      ; preds = %for.inc170, %for.body136
  %177 = load i32, i32* %i151, align 4, !tbaa !12
  %178 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width153 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %178, i32 0, i32 0
  %179 = load i32, i32* %width153, align 4, !tbaa !17
  %cmp154 = icmp ult i32 %177, %179
  br i1 %cmp154, label %for.body157, label %for.cond.cleanup156

for.cond.cleanup156:                              ; preds = %for.cond152
  store i32 23, i32* %cleanup.dest.slot, align 4
  %180 = bitcast i32* %i151 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #3
  br label %for.end172

for.body157:                                      ; preds = %for.cond152
  %181 = bitcast i32* %srcAlpha158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %181) #3
  %182 = load i8*, i8** %srcRow137, align 4, !tbaa !2
  %183 = load i32, i32* %i151, align 4, !tbaa !12
  %184 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes159 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %184, i32 0, i32 7
  %185 = load i32, i32* %srcPixelBytes159, align 4, !tbaa !27
  %mul160 = mul i32 %183, %185
  %arrayidx161 = getelementptr inbounds i8, i8* %182, i32 %mul160
  %186 = load i8, i8* %arrayidx161, align 1, !tbaa !19
  %conv162 = zext i8 %186 to i32
  store i32 %conv162, i32* %srcAlpha158, align 4, !tbaa !12
  %187 = bitcast i32* %dstAlpha163 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #3
  %188 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth164 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %188, i32 0, i32 2
  %189 = load i32, i32* %srcDepth164, align 4, !tbaa !20
  %190 = load i32, i32* %srcAlpha158, align 4, !tbaa !12
  %call165 = call i32 @avifLimitedToFullY(i32 %189, i32 %190)
  store i32 %call165, i32* %dstAlpha163, align 4, !tbaa !12
  %191 = load i32, i32* %dstAlpha163, align 4, !tbaa !12
  %conv166 = trunc i32 %191 to i8
  %192 = load i8*, i8** %dstRow144, align 4, !tbaa !2
  %193 = load i32, i32* %i151, align 4, !tbaa !12
  %194 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes167 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %194, i32 0, i32 13
  %195 = load i32, i32* %dstPixelBytes167, align 4, !tbaa !18
  %mul168 = mul i32 %193, %195
  %arrayidx169 = getelementptr inbounds i8, i8* %192, i32 %mul168
  store i8 %conv166, i8* %arrayidx169, align 1, !tbaa !19
  %196 = bitcast i32* %dstAlpha163 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #3
  %197 = bitcast i32* %srcAlpha158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #3
  br label %for.inc170

for.inc170:                                       ; preds = %for.body157
  %198 = load i32, i32* %i151, align 4, !tbaa !12
  %inc171 = add i32 %198, 1
  store i32 %inc171, i32* %i151, align 4, !tbaa !12
  br label %for.cond152

for.end172:                                       ; preds = %for.cond.cleanup156
  %199 = bitcast i8** %dstRow144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #3
  %200 = bitcast i8** %srcRow137 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #3
  br label %for.inc173

for.inc173:                                       ; preds = %for.end172
  %201 = load i32, i32* %j130, align 4, !tbaa !12
  %inc174 = add i32 %201, 1
  store i32 %inc174, i32* %j130, align 4, !tbaa !12
  br label %for.cond131

for.end175:                                       ; preds = %for.cond.cleanup135
  br label %if.end176

if.end176:                                        ; preds = %for.end175, %for.end128
  br label %if.end381

if.else177:                                       ; preds = %land.lhs.true77, %if.else73
  %202 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRange178 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %202, i32 0, i32 3
  %203 = load i32, i32* %srcRange178, align 4, !tbaa !23
  %cmp179 = icmp eq i32 %203, 1
  br i1 %cmp179, label %land.lhs.true181, label %if.else284

land.lhs.true181:                                 ; preds = %if.else177
  %204 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange182 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %204, i32 0, i32 9
  %205 = load i32, i32* %dstRange182, align 4, !tbaa !9
  %cmp183 = icmp eq i32 %205, 0
  br i1 %cmp183, label %if.then185, label %if.else284

if.then185:                                       ; preds = %land.lhs.true181
  %206 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth186 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %206, i32 0, i32 2
  %207 = load i32, i32* %srcDepth186, align 4, !tbaa !20
  %cmp187 = icmp ugt i32 %207, 8
  br i1 %cmp187, label %if.then189, label %if.else236

if.then189:                                       ; preds = %if.then185
  %208 = bitcast i32* %j190 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %208) #3
  store i32 0, i32* %j190, align 4, !tbaa !12
  br label %for.cond191

for.cond191:                                      ; preds = %for.inc233, %if.then189
  %209 = load i32, i32* %j190, align 4, !tbaa !12
  %210 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height192 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %210, i32 0, i32 1
  %211 = load i32, i32* %height192, align 4, !tbaa !13
  %cmp193 = icmp ult i32 %209, %211
  br i1 %cmp193, label %for.body196, label %for.cond.cleanup195

for.cond.cleanup195:                              ; preds = %for.cond191
  store i32 26, i32* %cleanup.dest.slot, align 4
  %212 = bitcast i32* %j190 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #3
  br label %for.end235

for.body196:                                      ; preds = %for.cond191
  %213 = bitcast i8** %srcRow197 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %213) #3
  %214 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane198 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %214, i32 0, i32 4
  %215 = load i8*, i8** %srcPlane198, align 4, !tbaa !24
  %216 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes199 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %216, i32 0, i32 6
  %217 = load i32, i32* %srcOffsetBytes199, align 4, !tbaa !25
  %218 = load i32, i32* %j190, align 4, !tbaa !12
  %219 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes200 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %219, i32 0, i32 5
  %220 = load i32, i32* %srcRowBytes200, align 4, !tbaa !26
  %mul201 = mul i32 %218, %220
  %add202 = add i32 %217, %mul201
  %arrayidx203 = getelementptr inbounds i8, i8* %215, i32 %add202
  store i8* %arrayidx203, i8** %srcRow197, align 4, !tbaa !2
  %221 = bitcast i8** %dstRow204 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %221) #3
  %222 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane205 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %222, i32 0, i32 10
  %223 = load i8*, i8** %dstPlane205, align 4, !tbaa !14
  %224 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes206 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %224, i32 0, i32 12
  %225 = load i32, i32* %dstOffsetBytes206, align 4, !tbaa !15
  %226 = load i32, i32* %j190, align 4, !tbaa !12
  %227 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes207 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %227, i32 0, i32 11
  %228 = load i32, i32* %dstRowBytes207, align 4, !tbaa !16
  %mul208 = mul i32 %226, %228
  %add209 = add i32 %225, %mul208
  %arrayidx210 = getelementptr inbounds i8, i8* %223, i32 %add209
  store i8* %arrayidx210, i8** %dstRow204, align 4, !tbaa !2
  %229 = bitcast i32* %i211 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %229) #3
  store i32 0, i32* %i211, align 4, !tbaa !12
  br label %for.cond212

for.cond212:                                      ; preds = %for.inc230, %for.body196
  %230 = load i32, i32* %i211, align 4, !tbaa !12
  %231 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width213 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %231, i32 0, i32 0
  %232 = load i32, i32* %width213, align 4, !tbaa !17
  %cmp214 = icmp ult i32 %230, %232
  br i1 %cmp214, label %for.body217, label %for.cond.cleanup216

for.cond.cleanup216:                              ; preds = %for.cond212
  store i32 29, i32* %cleanup.dest.slot, align 4
  %233 = bitcast i32* %i211 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #3
  br label %for.end232

for.body217:                                      ; preds = %for.cond212
  %234 = bitcast i32* %srcAlpha218 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %234) #3
  %235 = load i8*, i8** %srcRow197, align 4, !tbaa !2
  %236 = load i32, i32* %i211, align 4, !tbaa !12
  %237 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes219 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %237, i32 0, i32 7
  %238 = load i32, i32* %srcPixelBytes219, align 4, !tbaa !27
  %mul220 = mul i32 %236, %238
  %arrayidx221 = getelementptr inbounds i8, i8* %235, i32 %mul220
  %239 = bitcast i8* %arrayidx221 to i16*
  %240 = load i16, i16* %239, align 2, !tbaa !10
  %conv222 = zext i16 %240 to i32
  store i32 %conv222, i32* %srcAlpha218, align 4, !tbaa !12
  %241 = bitcast i32* %dstAlpha223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %241) #3
  %242 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth224 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %242, i32 0, i32 8
  %243 = load i32, i32* %dstDepth224, align 4, !tbaa !6
  %244 = load i32, i32* %srcAlpha218, align 4, !tbaa !12
  %call225 = call i32 @avifFullToLimitedY(i32 %243, i32 %244)
  store i32 %call225, i32* %dstAlpha223, align 4, !tbaa !12
  %245 = load i32, i32* %dstAlpha223, align 4, !tbaa !12
  %conv226 = trunc i32 %245 to i16
  %246 = load i8*, i8** %dstRow204, align 4, !tbaa !2
  %247 = load i32, i32* %i211, align 4, !tbaa !12
  %248 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes227 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %248, i32 0, i32 13
  %249 = load i32, i32* %dstPixelBytes227, align 4, !tbaa !18
  %mul228 = mul i32 %247, %249
  %arrayidx229 = getelementptr inbounds i8, i8* %246, i32 %mul228
  %250 = bitcast i8* %arrayidx229 to i16*
  store i16 %conv226, i16* %250, align 2, !tbaa !10
  %251 = bitcast i32* %dstAlpha223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #3
  %252 = bitcast i32* %srcAlpha218 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #3
  br label %for.inc230

for.inc230:                                       ; preds = %for.body217
  %253 = load i32, i32* %i211, align 4, !tbaa !12
  %inc231 = add i32 %253, 1
  store i32 %inc231, i32* %i211, align 4, !tbaa !12
  br label %for.cond212

for.end232:                                       ; preds = %for.cond.cleanup216
  %254 = bitcast i8** %dstRow204 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #3
  %255 = bitcast i8** %srcRow197 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #3
  br label %for.inc233

for.inc233:                                       ; preds = %for.end232
  %256 = load i32, i32* %j190, align 4, !tbaa !12
  %inc234 = add i32 %256, 1
  store i32 %inc234, i32* %j190, align 4, !tbaa !12
  br label %for.cond191

for.end235:                                       ; preds = %for.cond.cleanup195
  br label %if.end283

if.else236:                                       ; preds = %if.then185
  %257 = bitcast i32* %j237 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %257) #3
  store i32 0, i32* %j237, align 4, !tbaa !12
  br label %for.cond238

for.cond238:                                      ; preds = %for.inc280, %if.else236
  %258 = load i32, i32* %j237, align 4, !tbaa !12
  %259 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height239 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %259, i32 0, i32 1
  %260 = load i32, i32* %height239, align 4, !tbaa !13
  %cmp240 = icmp ult i32 %258, %260
  br i1 %cmp240, label %for.body243, label %for.cond.cleanup242

for.cond.cleanup242:                              ; preds = %for.cond238
  store i32 32, i32* %cleanup.dest.slot, align 4
  %261 = bitcast i32* %j237 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #3
  br label %for.end282

for.body243:                                      ; preds = %for.cond238
  %262 = bitcast i8** %srcRow244 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %262) #3
  %263 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane245 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %263, i32 0, i32 4
  %264 = load i8*, i8** %srcPlane245, align 4, !tbaa !24
  %265 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes246 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %265, i32 0, i32 6
  %266 = load i32, i32* %srcOffsetBytes246, align 4, !tbaa !25
  %267 = load i32, i32* %j237, align 4, !tbaa !12
  %268 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes247 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %268, i32 0, i32 5
  %269 = load i32, i32* %srcRowBytes247, align 4, !tbaa !26
  %mul248 = mul i32 %267, %269
  %add249 = add i32 %266, %mul248
  %arrayidx250 = getelementptr inbounds i8, i8* %264, i32 %add249
  store i8* %arrayidx250, i8** %srcRow244, align 4, !tbaa !2
  %270 = bitcast i8** %dstRow251 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %270) #3
  %271 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane252 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %271, i32 0, i32 10
  %272 = load i8*, i8** %dstPlane252, align 4, !tbaa !14
  %273 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes253 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %273, i32 0, i32 12
  %274 = load i32, i32* %dstOffsetBytes253, align 4, !tbaa !15
  %275 = load i32, i32* %j237, align 4, !tbaa !12
  %276 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes254 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %276, i32 0, i32 11
  %277 = load i32, i32* %dstRowBytes254, align 4, !tbaa !16
  %mul255 = mul i32 %275, %277
  %add256 = add i32 %274, %mul255
  %arrayidx257 = getelementptr inbounds i8, i8* %272, i32 %add256
  store i8* %arrayidx257, i8** %dstRow251, align 4, !tbaa !2
  %278 = bitcast i32* %i258 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %278) #3
  store i32 0, i32* %i258, align 4, !tbaa !12
  br label %for.cond259

for.cond259:                                      ; preds = %for.inc277, %for.body243
  %279 = load i32, i32* %i258, align 4, !tbaa !12
  %280 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width260 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %280, i32 0, i32 0
  %281 = load i32, i32* %width260, align 4, !tbaa !17
  %cmp261 = icmp ult i32 %279, %281
  br i1 %cmp261, label %for.body264, label %for.cond.cleanup263

for.cond.cleanup263:                              ; preds = %for.cond259
  store i32 35, i32* %cleanup.dest.slot, align 4
  %282 = bitcast i32* %i258 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #3
  br label %for.end279

for.body264:                                      ; preds = %for.cond259
  %283 = bitcast i32* %srcAlpha265 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %283) #3
  %284 = load i8*, i8** %srcRow244, align 4, !tbaa !2
  %285 = load i32, i32* %i258, align 4, !tbaa !12
  %286 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes266 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %286, i32 0, i32 7
  %287 = load i32, i32* %srcPixelBytes266, align 4, !tbaa !27
  %mul267 = mul i32 %285, %287
  %arrayidx268 = getelementptr inbounds i8, i8* %284, i32 %mul267
  %288 = load i8, i8* %arrayidx268, align 1, !tbaa !19
  %conv269 = zext i8 %288 to i32
  store i32 %conv269, i32* %srcAlpha265, align 4, !tbaa !12
  %289 = bitcast i32* %dstAlpha270 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %289) #3
  %290 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth271 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %290, i32 0, i32 8
  %291 = load i32, i32* %dstDepth271, align 4, !tbaa !6
  %292 = load i32, i32* %srcAlpha265, align 4, !tbaa !12
  %call272 = call i32 @avifFullToLimitedY(i32 %291, i32 %292)
  store i32 %call272, i32* %dstAlpha270, align 4, !tbaa !12
  %293 = load i32, i32* %dstAlpha270, align 4, !tbaa !12
  %conv273 = trunc i32 %293 to i8
  %294 = load i8*, i8** %dstRow251, align 4, !tbaa !2
  %295 = load i32, i32* %i258, align 4, !tbaa !12
  %296 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes274 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %296, i32 0, i32 13
  %297 = load i32, i32* %dstPixelBytes274, align 4, !tbaa !18
  %mul275 = mul i32 %295, %297
  %arrayidx276 = getelementptr inbounds i8, i8* %294, i32 %mul275
  store i8 %conv273, i8* %arrayidx276, align 1, !tbaa !19
  %298 = bitcast i32* %dstAlpha270 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #3
  %299 = bitcast i32* %srcAlpha265 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %299) #3
  br label %for.inc277

for.inc277:                                       ; preds = %for.body264
  %300 = load i32, i32* %i258, align 4, !tbaa !12
  %inc278 = add i32 %300, 1
  store i32 %inc278, i32* %i258, align 4, !tbaa !12
  br label %for.cond259

for.end279:                                       ; preds = %for.cond.cleanup263
  %301 = bitcast i8** %dstRow251 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %301) #3
  %302 = bitcast i8** %srcRow244 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %302) #3
  br label %for.inc280

for.inc280:                                       ; preds = %for.end279
  %303 = load i32, i32* %j237, align 4, !tbaa !12
  %inc281 = add i32 %303, 1
  store i32 %inc281, i32* %j237, align 4, !tbaa !12
  br label %for.cond238

for.end282:                                       ; preds = %for.cond.cleanup242
  br label %if.end283

if.end283:                                        ; preds = %for.end282, %for.end235
  br label %if.end380

if.else284:                                       ; preds = %land.lhs.true181, %if.else177
  %304 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRange285 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %304, i32 0, i32 3
  %305 = load i32, i32* %srcRange285, align 4, !tbaa !23
  %cmp286 = icmp eq i32 %305, 0
  br i1 %cmp286, label %land.lhs.true288, label %if.end379

land.lhs.true288:                                 ; preds = %if.else284
  %306 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange289 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %306, i32 0, i32 9
  %307 = load i32, i32* %dstRange289, align 4, !tbaa !9
  %cmp290 = icmp eq i32 %307, 0
  br i1 %cmp290, label %if.then292, label %if.end379

if.then292:                                       ; preds = %land.lhs.true288
  %308 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth293 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %308, i32 0, i32 2
  %309 = load i32, i32* %srcDepth293, align 4, !tbaa !20
  %cmp294 = icmp ugt i32 %309, 8
  br i1 %cmp294, label %if.then296, label %if.else337

if.then296:                                       ; preds = %if.then292
  %310 = bitcast i32* %j297 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %310) #3
  store i32 0, i32* %j297, align 4, !tbaa !12
  br label %for.cond298

for.cond298:                                      ; preds = %for.inc334, %if.then296
  %311 = load i32, i32* %j297, align 4, !tbaa !12
  %312 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height299 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %312, i32 0, i32 1
  %313 = load i32, i32* %height299, align 4, !tbaa !13
  %cmp300 = icmp ult i32 %311, %313
  br i1 %cmp300, label %for.body303, label %for.cond.cleanup302

for.cond.cleanup302:                              ; preds = %for.cond298
  store i32 38, i32* %cleanup.dest.slot, align 4
  %314 = bitcast i32* %j297 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #3
  br label %for.end336

for.body303:                                      ; preds = %for.cond298
  %315 = bitcast i8** %srcRow304 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %315) #3
  %316 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane305 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %316, i32 0, i32 4
  %317 = load i8*, i8** %srcPlane305, align 4, !tbaa !24
  %318 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes306 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %318, i32 0, i32 6
  %319 = load i32, i32* %srcOffsetBytes306, align 4, !tbaa !25
  %320 = load i32, i32* %j297, align 4, !tbaa !12
  %321 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes307 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %321, i32 0, i32 5
  %322 = load i32, i32* %srcRowBytes307, align 4, !tbaa !26
  %mul308 = mul i32 %320, %322
  %add309 = add i32 %319, %mul308
  %arrayidx310 = getelementptr inbounds i8, i8* %317, i32 %add309
  store i8* %arrayidx310, i8** %srcRow304, align 4, !tbaa !2
  %323 = bitcast i8** %dstRow311 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %323) #3
  %324 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane312 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %324, i32 0, i32 10
  %325 = load i8*, i8** %dstPlane312, align 4, !tbaa !14
  %326 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes313 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %326, i32 0, i32 12
  %327 = load i32, i32* %dstOffsetBytes313, align 4, !tbaa !15
  %328 = load i32, i32* %j297, align 4, !tbaa !12
  %329 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes314 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %329, i32 0, i32 11
  %330 = load i32, i32* %dstRowBytes314, align 4, !tbaa !16
  %mul315 = mul i32 %328, %330
  %add316 = add i32 %327, %mul315
  %arrayidx317 = getelementptr inbounds i8, i8* %325, i32 %add316
  store i8* %arrayidx317, i8** %dstRow311, align 4, !tbaa !2
  %331 = bitcast i32* %i318 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %331) #3
  store i32 0, i32* %i318, align 4, !tbaa !12
  br label %for.cond319

for.cond319:                                      ; preds = %for.inc331, %for.body303
  %332 = load i32, i32* %i318, align 4, !tbaa !12
  %333 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width320 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %333, i32 0, i32 0
  %334 = load i32, i32* %width320, align 4, !tbaa !17
  %cmp321 = icmp ult i32 %332, %334
  br i1 %cmp321, label %for.body324, label %for.cond.cleanup323

for.cond.cleanup323:                              ; preds = %for.cond319
  store i32 41, i32* %cleanup.dest.slot, align 4
  %335 = bitcast i32* %i318 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %335) #3
  br label %for.end333

for.body324:                                      ; preds = %for.cond319
  %336 = load i8*, i8** %srcRow304, align 4, !tbaa !2
  %337 = load i32, i32* %i318, align 4, !tbaa !12
  %338 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes325 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %338, i32 0, i32 7
  %339 = load i32, i32* %srcPixelBytes325, align 4, !tbaa !27
  %mul326 = mul i32 %337, %339
  %arrayidx327 = getelementptr inbounds i8, i8* %336, i32 %mul326
  %340 = bitcast i8* %arrayidx327 to i16*
  %341 = load i16, i16* %340, align 2, !tbaa !10
  %342 = load i8*, i8** %dstRow311, align 4, !tbaa !2
  %343 = load i32, i32* %i318, align 4, !tbaa !12
  %344 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes328 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %344, i32 0, i32 13
  %345 = load i32, i32* %dstPixelBytes328, align 4, !tbaa !18
  %mul329 = mul i32 %343, %345
  %arrayidx330 = getelementptr inbounds i8, i8* %342, i32 %mul329
  %346 = bitcast i8* %arrayidx330 to i16*
  store i16 %341, i16* %346, align 2, !tbaa !10
  br label %for.inc331

for.inc331:                                       ; preds = %for.body324
  %347 = load i32, i32* %i318, align 4, !tbaa !12
  %inc332 = add i32 %347, 1
  store i32 %inc332, i32* %i318, align 4, !tbaa !12
  br label %for.cond319

for.end333:                                       ; preds = %for.cond.cleanup323
  %348 = bitcast i8** %dstRow311 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %348) #3
  %349 = bitcast i8** %srcRow304 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %349) #3
  br label %for.inc334

for.inc334:                                       ; preds = %for.end333
  %350 = load i32, i32* %j297, align 4, !tbaa !12
  %inc335 = add i32 %350, 1
  store i32 %inc335, i32* %j297, align 4, !tbaa !12
  br label %for.cond298

for.end336:                                       ; preds = %for.cond.cleanup302
  br label %if.end378

if.else337:                                       ; preds = %if.then292
  %351 = bitcast i32* %j338 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %351) #3
  store i32 0, i32* %j338, align 4, !tbaa !12
  br label %for.cond339

for.cond339:                                      ; preds = %for.inc375, %if.else337
  %352 = load i32, i32* %j338, align 4, !tbaa !12
  %353 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height340 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %353, i32 0, i32 1
  %354 = load i32, i32* %height340, align 4, !tbaa !13
  %cmp341 = icmp ult i32 %352, %354
  br i1 %cmp341, label %for.body344, label %for.cond.cleanup343

for.cond.cleanup343:                              ; preds = %for.cond339
  store i32 44, i32* %cleanup.dest.slot, align 4
  %355 = bitcast i32* %j338 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %355) #3
  br label %for.end377

for.body344:                                      ; preds = %for.cond339
  %356 = bitcast i8** %srcRow345 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %356) #3
  %357 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane346 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %357, i32 0, i32 4
  %358 = load i8*, i8** %srcPlane346, align 4, !tbaa !24
  %359 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes347 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %359, i32 0, i32 6
  %360 = load i32, i32* %srcOffsetBytes347, align 4, !tbaa !25
  %361 = load i32, i32* %j338, align 4, !tbaa !12
  %362 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes348 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %362, i32 0, i32 5
  %363 = load i32, i32* %srcRowBytes348, align 4, !tbaa !26
  %mul349 = mul i32 %361, %363
  %add350 = add i32 %360, %mul349
  %arrayidx351 = getelementptr inbounds i8, i8* %358, i32 %add350
  store i8* %arrayidx351, i8** %srcRow345, align 4, !tbaa !2
  %364 = bitcast i8** %dstRow352 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %364) #3
  %365 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane353 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %365, i32 0, i32 10
  %366 = load i8*, i8** %dstPlane353, align 4, !tbaa !14
  %367 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes354 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %367, i32 0, i32 12
  %368 = load i32, i32* %dstOffsetBytes354, align 4, !tbaa !15
  %369 = load i32, i32* %j338, align 4, !tbaa !12
  %370 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes355 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %370, i32 0, i32 11
  %371 = load i32, i32* %dstRowBytes355, align 4, !tbaa !16
  %mul356 = mul i32 %369, %371
  %add357 = add i32 %368, %mul356
  %arrayidx358 = getelementptr inbounds i8, i8* %366, i32 %add357
  store i8* %arrayidx358, i8** %dstRow352, align 4, !tbaa !2
  %372 = bitcast i32* %i359 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %372) #3
  store i32 0, i32* %i359, align 4, !tbaa !12
  br label %for.cond360

for.cond360:                                      ; preds = %for.inc372, %for.body344
  %373 = load i32, i32* %i359, align 4, !tbaa !12
  %374 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width361 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %374, i32 0, i32 0
  %375 = load i32, i32* %width361, align 4, !tbaa !17
  %cmp362 = icmp ult i32 %373, %375
  br i1 %cmp362, label %for.body365, label %for.cond.cleanup364

for.cond.cleanup364:                              ; preds = %for.cond360
  store i32 47, i32* %cleanup.dest.slot, align 4
  %376 = bitcast i32* %i359 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %376) #3
  br label %for.end374

for.body365:                                      ; preds = %for.cond360
  %377 = load i8*, i8** %srcRow345, align 4, !tbaa !2
  %378 = load i32, i32* %i359, align 4, !tbaa !12
  %379 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes366 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %379, i32 0, i32 7
  %380 = load i32, i32* %srcPixelBytes366, align 4, !tbaa !27
  %mul367 = mul i32 %378, %380
  %arrayidx368 = getelementptr inbounds i8, i8* %377, i32 %mul367
  %381 = load i8, i8* %arrayidx368, align 1, !tbaa !19
  %382 = load i8*, i8** %dstRow352, align 4, !tbaa !2
  %383 = load i32, i32* %i359, align 4, !tbaa !12
  %384 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes369 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %384, i32 0, i32 13
  %385 = load i32, i32* %dstPixelBytes369, align 4, !tbaa !18
  %mul370 = mul i32 %383, %385
  %arrayidx371 = getelementptr inbounds i8, i8* %382, i32 %mul370
  store i8 %381, i8* %arrayidx371, align 1, !tbaa !19
  br label %for.inc372

for.inc372:                                       ; preds = %for.body365
  %386 = load i32, i32* %i359, align 4, !tbaa !12
  %inc373 = add i32 %386, 1
  store i32 %inc373, i32* %i359, align 4, !tbaa !12
  br label %for.cond360

for.end374:                                       ; preds = %for.cond.cleanup364
  %387 = bitcast i8** %dstRow352 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %387) #3
  %388 = bitcast i8** %srcRow345 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %388) #3
  br label %for.inc375

for.inc375:                                       ; preds = %for.end374
  %389 = load i32, i32* %j338, align 4, !tbaa !12
  %inc376 = add i32 %389, 1
  store i32 %inc376, i32* %j338, align 4, !tbaa !12
  br label %for.cond339

for.end377:                                       ; preds = %for.cond.cleanup343
  br label %if.end378

if.end378:                                        ; preds = %for.end377, %for.end336
  br label %if.end379

if.end379:                                        ; preds = %if.end378, %land.lhs.true288, %if.else284
  br label %if.end380

if.end380:                                        ; preds = %if.end379, %if.end283
  br label %if.end381

if.end381:                                        ; preds = %if.end380, %if.end176
  br label %if.end382

if.end382:                                        ; preds = %if.end381, %if.end
  br label %if.end1233

if.else383:                                       ; preds = %entry
  %390 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRange384 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %390, i32 0, i32 3
  %391 = load i32, i32* %srcRange384, align 4, !tbaa !23
  %cmp385 = icmp eq i32 %391, 1
  br i1 %cmp385, label %land.lhs.true387, label %if.else584

land.lhs.true387:                                 ; preds = %if.else383
  %392 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange388 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %392, i32 0, i32 9
  %393 = load i32, i32* %dstRange388, align 4, !tbaa !9
  %cmp389 = icmp eq i32 %393, 1
  br i1 %cmp389, label %if.then391, label %if.else584

if.then391:                                       ; preds = %land.lhs.true387
  %394 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth392 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %394, i32 0, i32 2
  %395 = load i32, i32* %srcDepth392, align 4, !tbaa !20
  %cmp393 = icmp ugt i32 %395, 8
  br i1 %cmp393, label %if.then395, label %if.else520

if.then395:                                       ; preds = %if.then391
  %396 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth396 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %396, i32 0, i32 8
  %397 = load i32, i32* %dstDepth396, align 4, !tbaa !6
  %cmp397 = icmp ugt i32 %397, 8
  br i1 %cmp397, label %if.then399, label %if.else456

if.then399:                                       ; preds = %if.then395
  %398 = bitcast i32* %j400 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %398) #3
  store i32 0, i32* %j400, align 4, !tbaa !12
  br label %for.cond401

for.cond401:                                      ; preds = %for.inc453, %if.then399
  %399 = load i32, i32* %j400, align 4, !tbaa !12
  %400 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height402 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %400, i32 0, i32 1
  %401 = load i32, i32* %height402, align 4, !tbaa !13
  %cmp403 = icmp ult i32 %399, %401
  br i1 %cmp403, label %for.body406, label %for.cond.cleanup405

for.cond.cleanup405:                              ; preds = %for.cond401
  store i32 50, i32* %cleanup.dest.slot, align 4
  %402 = bitcast i32* %j400 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %402) #3
  br label %for.end455

for.body406:                                      ; preds = %for.cond401
  %403 = bitcast i8** %srcRow407 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %403) #3
  %404 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane408 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %404, i32 0, i32 4
  %405 = load i8*, i8** %srcPlane408, align 4, !tbaa !24
  %406 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes409 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %406, i32 0, i32 6
  %407 = load i32, i32* %srcOffsetBytes409, align 4, !tbaa !25
  %408 = load i32, i32* %j400, align 4, !tbaa !12
  %409 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes410 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %409, i32 0, i32 5
  %410 = load i32, i32* %srcRowBytes410, align 4, !tbaa !26
  %mul411 = mul i32 %408, %410
  %add412 = add i32 %407, %mul411
  %arrayidx413 = getelementptr inbounds i8, i8* %405, i32 %add412
  store i8* %arrayidx413, i8** %srcRow407, align 4, !tbaa !2
  %411 = bitcast i8** %dstRow414 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %411) #3
  %412 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane415 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %412, i32 0, i32 10
  %413 = load i8*, i8** %dstPlane415, align 4, !tbaa !14
  %414 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes416 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %414, i32 0, i32 12
  %415 = load i32, i32* %dstOffsetBytes416, align 4, !tbaa !15
  %416 = load i32, i32* %j400, align 4, !tbaa !12
  %417 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes417 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %417, i32 0, i32 11
  %418 = load i32, i32* %dstRowBytes417, align 4, !tbaa !16
  %mul418 = mul i32 %416, %418
  %add419 = add i32 %415, %mul418
  %arrayidx420 = getelementptr inbounds i8, i8* %413, i32 %add419
  store i8* %arrayidx420, i8** %dstRow414, align 4, !tbaa !2
  %419 = bitcast i32* %i421 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %419) #3
  store i32 0, i32* %i421, align 4, !tbaa !12
  br label %for.cond422

for.cond422:                                      ; preds = %for.inc450, %for.body406
  %420 = load i32, i32* %i421, align 4, !tbaa !12
  %421 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width423 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %421, i32 0, i32 0
  %422 = load i32, i32* %width423, align 4, !tbaa !17
  %cmp424 = icmp ult i32 %420, %422
  br i1 %cmp424, label %for.body427, label %for.cond.cleanup426

for.cond.cleanup426:                              ; preds = %for.cond422
  store i32 53, i32* %cleanup.dest.slot, align 4
  %423 = bitcast i32* %i421 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %423) #3
  br label %for.end452

for.body427:                                      ; preds = %for.cond422
  %424 = bitcast i32* %srcAlpha428 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %424) #3
  %425 = load i8*, i8** %srcRow407, align 4, !tbaa !2
  %426 = load i32, i32* %i421, align 4, !tbaa !12
  %427 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes429 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %427, i32 0, i32 7
  %428 = load i32, i32* %srcPixelBytes429, align 4, !tbaa !27
  %mul430 = mul i32 %426, %428
  %arrayidx431 = getelementptr inbounds i8, i8* %425, i32 %mul430
  %429 = bitcast i8* %arrayidx431 to i16*
  %430 = load i16, i16* %429, align 2, !tbaa !10
  %conv432 = zext i16 %430 to i32
  store i32 %conv432, i32* %srcAlpha428, align 4, !tbaa !12
  %431 = bitcast float* %alphaF to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %431) #3
  %432 = load i32, i32* %srcAlpha428, align 4, !tbaa !12
  %conv433 = sitofp i32 %432 to float
  %433 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div = fdiv float %conv433, %433
  store float %div, float* %alphaF, align 4, !tbaa !21
  %434 = bitcast i32* %dstAlpha434 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %434) #3
  %435 = load float, float* %alphaF, align 4, !tbaa !21
  %436 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul435 = fmul float %435, %436
  %add436 = fadd float 5.000000e-01, %mul435
  %conv437 = fptosi float %add436 to i32
  store i32 %conv437, i32* %dstAlpha434, align 4, !tbaa !12
  %437 = load i32, i32* %dstAlpha434, align 4, !tbaa !12
  %cmp438 = icmp slt i32 %437, 0
  br i1 %cmp438, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body427
  br label %cond.end444

cond.false:                                       ; preds = %for.body427
  %438 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %439 = load i32, i32* %dstAlpha434, align 4, !tbaa !12
  %cmp440 = icmp slt i32 %438, %439
  br i1 %cmp440, label %cond.true442, label %cond.false443

cond.true442:                                     ; preds = %cond.false
  %440 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end

cond.false443:                                    ; preds = %cond.false
  %441 = load i32, i32* %dstAlpha434, align 4, !tbaa !12
  br label %cond.end

cond.end:                                         ; preds = %cond.false443, %cond.true442
  %cond = phi i32 [ %440, %cond.true442 ], [ %441, %cond.false443 ]
  br label %cond.end444

cond.end444:                                      ; preds = %cond.end, %cond.true
  %cond445 = phi i32 [ 0, %cond.true ], [ %cond, %cond.end ]
  store i32 %cond445, i32* %dstAlpha434, align 4, !tbaa !12
  %442 = load i32, i32* %dstAlpha434, align 4, !tbaa !12
  %conv446 = trunc i32 %442 to i16
  %443 = load i8*, i8** %dstRow414, align 4, !tbaa !2
  %444 = load i32, i32* %i421, align 4, !tbaa !12
  %445 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes447 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %445, i32 0, i32 13
  %446 = load i32, i32* %dstPixelBytes447, align 4, !tbaa !18
  %mul448 = mul i32 %444, %446
  %arrayidx449 = getelementptr inbounds i8, i8* %443, i32 %mul448
  %447 = bitcast i8* %arrayidx449 to i16*
  store i16 %conv446, i16* %447, align 2, !tbaa !10
  %448 = bitcast i32* %dstAlpha434 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %448) #3
  %449 = bitcast float* %alphaF to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %449) #3
  %450 = bitcast i32* %srcAlpha428 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %450) #3
  br label %for.inc450

for.inc450:                                       ; preds = %cond.end444
  %451 = load i32, i32* %i421, align 4, !tbaa !12
  %inc451 = add i32 %451, 1
  store i32 %inc451, i32* %i421, align 4, !tbaa !12
  br label %for.cond422

for.end452:                                       ; preds = %for.cond.cleanup426
  %452 = bitcast i8** %dstRow414 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %452) #3
  %453 = bitcast i8** %srcRow407 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %453) #3
  br label %for.inc453

for.inc453:                                       ; preds = %for.end452
  %454 = load i32, i32* %j400, align 4, !tbaa !12
  %inc454 = add i32 %454, 1
  store i32 %inc454, i32* %j400, align 4, !tbaa !12
  br label %for.cond401

for.end455:                                       ; preds = %for.cond.cleanup405
  br label %if.end519

if.else456:                                       ; preds = %if.then395
  %455 = bitcast i32* %j457 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %455) #3
  store i32 0, i32* %j457, align 4, !tbaa !12
  br label %for.cond458

for.cond458:                                      ; preds = %for.inc516, %if.else456
  %456 = load i32, i32* %j457, align 4, !tbaa !12
  %457 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height459 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %457, i32 0, i32 1
  %458 = load i32, i32* %height459, align 4, !tbaa !13
  %cmp460 = icmp ult i32 %456, %458
  br i1 %cmp460, label %for.body463, label %for.cond.cleanup462

for.cond.cleanup462:                              ; preds = %for.cond458
  store i32 56, i32* %cleanup.dest.slot, align 4
  %459 = bitcast i32* %j457 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %459) #3
  br label %for.end518

for.body463:                                      ; preds = %for.cond458
  %460 = bitcast i8** %srcRow464 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %460) #3
  %461 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane465 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %461, i32 0, i32 4
  %462 = load i8*, i8** %srcPlane465, align 4, !tbaa !24
  %463 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes466 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %463, i32 0, i32 6
  %464 = load i32, i32* %srcOffsetBytes466, align 4, !tbaa !25
  %465 = load i32, i32* %j457, align 4, !tbaa !12
  %466 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes467 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %466, i32 0, i32 5
  %467 = load i32, i32* %srcRowBytes467, align 4, !tbaa !26
  %mul468 = mul i32 %465, %467
  %add469 = add i32 %464, %mul468
  %arrayidx470 = getelementptr inbounds i8, i8* %462, i32 %add469
  store i8* %arrayidx470, i8** %srcRow464, align 4, !tbaa !2
  %468 = bitcast i8** %dstRow471 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %468) #3
  %469 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane472 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %469, i32 0, i32 10
  %470 = load i8*, i8** %dstPlane472, align 4, !tbaa !14
  %471 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes473 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %471, i32 0, i32 12
  %472 = load i32, i32* %dstOffsetBytes473, align 4, !tbaa !15
  %473 = load i32, i32* %j457, align 4, !tbaa !12
  %474 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes474 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %474, i32 0, i32 11
  %475 = load i32, i32* %dstRowBytes474, align 4, !tbaa !16
  %mul475 = mul i32 %473, %475
  %add476 = add i32 %472, %mul475
  %arrayidx477 = getelementptr inbounds i8, i8* %470, i32 %add476
  store i8* %arrayidx477, i8** %dstRow471, align 4, !tbaa !2
  %476 = bitcast i32* %i478 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %476) #3
  store i32 0, i32* %i478, align 4, !tbaa !12
  br label %for.cond479

for.cond479:                                      ; preds = %for.inc513, %for.body463
  %477 = load i32, i32* %i478, align 4, !tbaa !12
  %478 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width480 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %478, i32 0, i32 0
  %479 = load i32, i32* %width480, align 4, !tbaa !17
  %cmp481 = icmp ult i32 %477, %479
  br i1 %cmp481, label %for.body484, label %for.cond.cleanup483

for.cond.cleanup483:                              ; preds = %for.cond479
  store i32 59, i32* %cleanup.dest.slot, align 4
  %480 = bitcast i32* %i478 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %480) #3
  br label %for.end515

for.body484:                                      ; preds = %for.cond479
  %481 = bitcast i32* %srcAlpha485 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %481) #3
  %482 = load i8*, i8** %srcRow464, align 4, !tbaa !2
  %483 = load i32, i32* %i478, align 4, !tbaa !12
  %484 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes486 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %484, i32 0, i32 7
  %485 = load i32, i32* %srcPixelBytes486, align 4, !tbaa !27
  %mul487 = mul i32 %483, %485
  %arrayidx488 = getelementptr inbounds i8, i8* %482, i32 %mul487
  %486 = bitcast i8* %arrayidx488 to i16*
  %487 = load i16, i16* %486, align 2, !tbaa !10
  %conv489 = zext i16 %487 to i32
  store i32 %conv489, i32* %srcAlpha485, align 4, !tbaa !12
  %488 = bitcast float* %alphaF490 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %488) #3
  %489 = load i32, i32* %srcAlpha485, align 4, !tbaa !12
  %conv491 = sitofp i32 %489 to float
  %490 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div492 = fdiv float %conv491, %490
  store float %div492, float* %alphaF490, align 4, !tbaa !21
  %491 = bitcast i32* %dstAlpha493 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %491) #3
  %492 = load float, float* %alphaF490, align 4, !tbaa !21
  %493 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul494 = fmul float %492, %493
  %add495 = fadd float 5.000000e-01, %mul494
  %conv496 = fptosi float %add495 to i32
  store i32 %conv496, i32* %dstAlpha493, align 4, !tbaa !12
  %494 = load i32, i32* %dstAlpha493, align 4, !tbaa !12
  %cmp497 = icmp slt i32 %494, 0
  br i1 %cmp497, label %cond.true499, label %cond.false500

cond.true499:                                     ; preds = %for.body484
  br label %cond.end507

cond.false500:                                    ; preds = %for.body484
  %495 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %496 = load i32, i32* %dstAlpha493, align 4, !tbaa !12
  %cmp501 = icmp slt i32 %495, %496
  br i1 %cmp501, label %cond.true503, label %cond.false504

cond.true503:                                     ; preds = %cond.false500
  %497 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end505

cond.false504:                                    ; preds = %cond.false500
  %498 = load i32, i32* %dstAlpha493, align 4, !tbaa !12
  br label %cond.end505

cond.end505:                                      ; preds = %cond.false504, %cond.true503
  %cond506 = phi i32 [ %497, %cond.true503 ], [ %498, %cond.false504 ]
  br label %cond.end507

cond.end507:                                      ; preds = %cond.end505, %cond.true499
  %cond508 = phi i32 [ 0, %cond.true499 ], [ %cond506, %cond.end505 ]
  store i32 %cond508, i32* %dstAlpha493, align 4, !tbaa !12
  %499 = load i32, i32* %dstAlpha493, align 4, !tbaa !12
  %conv509 = trunc i32 %499 to i8
  %500 = load i8*, i8** %dstRow471, align 4, !tbaa !2
  %501 = load i32, i32* %i478, align 4, !tbaa !12
  %502 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes510 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %502, i32 0, i32 13
  %503 = load i32, i32* %dstPixelBytes510, align 4, !tbaa !18
  %mul511 = mul i32 %501, %503
  %arrayidx512 = getelementptr inbounds i8, i8* %500, i32 %mul511
  store i8 %conv509, i8* %arrayidx512, align 1, !tbaa !19
  %504 = bitcast i32* %dstAlpha493 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %504) #3
  %505 = bitcast float* %alphaF490 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %505) #3
  %506 = bitcast i32* %srcAlpha485 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %506) #3
  br label %for.inc513

for.inc513:                                       ; preds = %cond.end507
  %507 = load i32, i32* %i478, align 4, !tbaa !12
  %inc514 = add i32 %507, 1
  store i32 %inc514, i32* %i478, align 4, !tbaa !12
  br label %for.cond479

for.end515:                                       ; preds = %for.cond.cleanup483
  %508 = bitcast i8** %dstRow471 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %508) #3
  %509 = bitcast i8** %srcRow464 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %509) #3
  br label %for.inc516

for.inc516:                                       ; preds = %for.end515
  %510 = load i32, i32* %j457, align 4, !tbaa !12
  %inc517 = add i32 %510, 1
  store i32 %inc517, i32* %j457, align 4, !tbaa !12
  br label %for.cond458

for.end518:                                       ; preds = %for.cond.cleanup462
  br label %if.end519

if.end519:                                        ; preds = %for.end518, %for.end455
  br label %if.end583

if.else520:                                       ; preds = %if.then391
  %511 = bitcast i32* %j521 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %511) #3
  store i32 0, i32* %j521, align 4, !tbaa !12
  br label %for.cond522

for.cond522:                                      ; preds = %for.inc580, %if.else520
  %512 = load i32, i32* %j521, align 4, !tbaa !12
  %513 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height523 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %513, i32 0, i32 1
  %514 = load i32, i32* %height523, align 4, !tbaa !13
  %cmp524 = icmp ult i32 %512, %514
  br i1 %cmp524, label %for.body527, label %for.cond.cleanup526

for.cond.cleanup526:                              ; preds = %for.cond522
  store i32 62, i32* %cleanup.dest.slot, align 4
  %515 = bitcast i32* %j521 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %515) #3
  br label %for.end582

for.body527:                                      ; preds = %for.cond522
  %516 = bitcast i8** %srcRow528 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %516) #3
  %517 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane529 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %517, i32 0, i32 4
  %518 = load i8*, i8** %srcPlane529, align 4, !tbaa !24
  %519 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes530 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %519, i32 0, i32 6
  %520 = load i32, i32* %srcOffsetBytes530, align 4, !tbaa !25
  %521 = load i32, i32* %j521, align 4, !tbaa !12
  %522 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes531 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %522, i32 0, i32 5
  %523 = load i32, i32* %srcRowBytes531, align 4, !tbaa !26
  %mul532 = mul i32 %521, %523
  %add533 = add i32 %520, %mul532
  %arrayidx534 = getelementptr inbounds i8, i8* %518, i32 %add533
  store i8* %arrayidx534, i8** %srcRow528, align 4, !tbaa !2
  %524 = bitcast i8** %dstRow535 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %524) #3
  %525 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane536 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %525, i32 0, i32 10
  %526 = load i8*, i8** %dstPlane536, align 4, !tbaa !14
  %527 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes537 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %527, i32 0, i32 12
  %528 = load i32, i32* %dstOffsetBytes537, align 4, !tbaa !15
  %529 = load i32, i32* %j521, align 4, !tbaa !12
  %530 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes538 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %530, i32 0, i32 11
  %531 = load i32, i32* %dstRowBytes538, align 4, !tbaa !16
  %mul539 = mul i32 %529, %531
  %add540 = add i32 %528, %mul539
  %arrayidx541 = getelementptr inbounds i8, i8* %526, i32 %add540
  store i8* %arrayidx541, i8** %dstRow535, align 4, !tbaa !2
  %532 = bitcast i32* %i542 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %532) #3
  store i32 0, i32* %i542, align 4, !tbaa !12
  br label %for.cond543

for.cond543:                                      ; preds = %for.inc577, %for.body527
  %533 = load i32, i32* %i542, align 4, !tbaa !12
  %534 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width544 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %534, i32 0, i32 0
  %535 = load i32, i32* %width544, align 4, !tbaa !17
  %cmp545 = icmp ult i32 %533, %535
  br i1 %cmp545, label %for.body548, label %for.cond.cleanup547

for.cond.cleanup547:                              ; preds = %for.cond543
  store i32 65, i32* %cleanup.dest.slot, align 4
  %536 = bitcast i32* %i542 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %536) #3
  br label %for.end579

for.body548:                                      ; preds = %for.cond543
  %537 = bitcast i32* %srcAlpha549 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %537) #3
  %538 = load i8*, i8** %srcRow528, align 4, !tbaa !2
  %539 = load i32, i32* %i542, align 4, !tbaa !12
  %540 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes550 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %540, i32 0, i32 7
  %541 = load i32, i32* %srcPixelBytes550, align 4, !tbaa !27
  %mul551 = mul i32 %539, %541
  %arrayidx552 = getelementptr inbounds i8, i8* %538, i32 %mul551
  %542 = load i8, i8* %arrayidx552, align 1, !tbaa !19
  %conv553 = zext i8 %542 to i32
  store i32 %conv553, i32* %srcAlpha549, align 4, !tbaa !12
  %543 = bitcast float* %alphaF554 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %543) #3
  %544 = load i32, i32* %srcAlpha549, align 4, !tbaa !12
  %conv555 = sitofp i32 %544 to float
  %545 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div556 = fdiv float %conv555, %545
  store float %div556, float* %alphaF554, align 4, !tbaa !21
  %546 = bitcast i32* %dstAlpha557 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %546) #3
  %547 = load float, float* %alphaF554, align 4, !tbaa !21
  %548 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul558 = fmul float %547, %548
  %add559 = fadd float 5.000000e-01, %mul558
  %conv560 = fptosi float %add559 to i32
  store i32 %conv560, i32* %dstAlpha557, align 4, !tbaa !12
  %549 = load i32, i32* %dstAlpha557, align 4, !tbaa !12
  %cmp561 = icmp slt i32 %549, 0
  br i1 %cmp561, label %cond.true563, label %cond.false564

cond.true563:                                     ; preds = %for.body548
  br label %cond.end571

cond.false564:                                    ; preds = %for.body548
  %550 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %551 = load i32, i32* %dstAlpha557, align 4, !tbaa !12
  %cmp565 = icmp slt i32 %550, %551
  br i1 %cmp565, label %cond.true567, label %cond.false568

cond.true567:                                     ; preds = %cond.false564
  %552 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end569

cond.false568:                                    ; preds = %cond.false564
  %553 = load i32, i32* %dstAlpha557, align 4, !tbaa !12
  br label %cond.end569

cond.end569:                                      ; preds = %cond.false568, %cond.true567
  %cond570 = phi i32 [ %552, %cond.true567 ], [ %553, %cond.false568 ]
  br label %cond.end571

cond.end571:                                      ; preds = %cond.end569, %cond.true563
  %cond572 = phi i32 [ 0, %cond.true563 ], [ %cond570, %cond.end569 ]
  store i32 %cond572, i32* %dstAlpha557, align 4, !tbaa !12
  %554 = load i32, i32* %dstAlpha557, align 4, !tbaa !12
  %conv573 = trunc i32 %554 to i16
  %555 = load i8*, i8** %dstRow535, align 4, !tbaa !2
  %556 = load i32, i32* %i542, align 4, !tbaa !12
  %557 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes574 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %557, i32 0, i32 13
  %558 = load i32, i32* %dstPixelBytes574, align 4, !tbaa !18
  %mul575 = mul i32 %556, %558
  %arrayidx576 = getelementptr inbounds i8, i8* %555, i32 %mul575
  %559 = bitcast i8* %arrayidx576 to i16*
  store i16 %conv573, i16* %559, align 2, !tbaa !10
  %560 = bitcast i32* %dstAlpha557 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %560) #3
  %561 = bitcast float* %alphaF554 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %561) #3
  %562 = bitcast i32* %srcAlpha549 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %562) #3
  br label %for.inc577

for.inc577:                                       ; preds = %cond.end571
  %563 = load i32, i32* %i542, align 4, !tbaa !12
  %inc578 = add i32 %563, 1
  store i32 %inc578, i32* %i542, align 4, !tbaa !12
  br label %for.cond543

for.end579:                                       ; preds = %for.cond.cleanup547
  %564 = bitcast i8** %dstRow535 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %564) #3
  %565 = bitcast i8** %srcRow528 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %565) #3
  br label %for.inc580

for.inc580:                                       ; preds = %for.end579
  %566 = load i32, i32* %j521, align 4, !tbaa !12
  %inc581 = add i32 %566, 1
  store i32 %inc581, i32* %j521, align 4, !tbaa !12
  br label %for.cond522

for.end582:                                       ; preds = %for.cond.cleanup526
  br label %if.end583

if.end583:                                        ; preds = %for.end582, %if.end519
  br label %if.end1232

if.else584:                                       ; preds = %land.lhs.true387, %if.else383
  %567 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRange585 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %567, i32 0, i32 3
  %568 = load i32, i32* %srcRange585, align 4, !tbaa !23
  %cmp586 = icmp eq i32 %568, 0
  br i1 %cmp586, label %land.lhs.true588, label %if.else797

land.lhs.true588:                                 ; preds = %if.else584
  %569 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange589 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %569, i32 0, i32 9
  %570 = load i32, i32* %dstRange589, align 4, !tbaa !9
  %cmp590 = icmp eq i32 %570, 1
  br i1 %cmp590, label %if.then592, label %if.else797

if.then592:                                       ; preds = %land.lhs.true588
  %571 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth593 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %571, i32 0, i32 2
  %572 = load i32, i32* %srcDepth593, align 4, !tbaa !20
  %cmp594 = icmp ugt i32 %572, 8
  br i1 %cmp594, label %if.then596, label %if.else731

if.then596:                                       ; preds = %if.then592
  %573 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth597 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %573, i32 0, i32 8
  %574 = load i32, i32* %dstDepth597, align 4, !tbaa !6
  %cmp598 = icmp ugt i32 %574, 8
  br i1 %cmp598, label %if.then600, label %if.else665

if.then600:                                       ; preds = %if.then596
  %575 = bitcast i32* %j601 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %575) #3
  store i32 0, i32* %j601, align 4, !tbaa !12
  br label %for.cond602

for.cond602:                                      ; preds = %for.inc662, %if.then600
  %576 = load i32, i32* %j601, align 4, !tbaa !12
  %577 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height603 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %577, i32 0, i32 1
  %578 = load i32, i32* %height603, align 4, !tbaa !13
  %cmp604 = icmp ult i32 %576, %578
  br i1 %cmp604, label %for.body607, label %for.cond.cleanup606

for.cond.cleanup606:                              ; preds = %for.cond602
  store i32 68, i32* %cleanup.dest.slot, align 4
  %579 = bitcast i32* %j601 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %579) #3
  br label %for.end664

for.body607:                                      ; preds = %for.cond602
  %580 = bitcast i8** %srcRow608 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %580) #3
  %581 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane609 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %581, i32 0, i32 4
  %582 = load i8*, i8** %srcPlane609, align 4, !tbaa !24
  %583 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes610 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %583, i32 0, i32 6
  %584 = load i32, i32* %srcOffsetBytes610, align 4, !tbaa !25
  %585 = load i32, i32* %j601, align 4, !tbaa !12
  %586 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes611 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %586, i32 0, i32 5
  %587 = load i32, i32* %srcRowBytes611, align 4, !tbaa !26
  %mul612 = mul i32 %585, %587
  %add613 = add i32 %584, %mul612
  %arrayidx614 = getelementptr inbounds i8, i8* %582, i32 %add613
  store i8* %arrayidx614, i8** %srcRow608, align 4, !tbaa !2
  %588 = bitcast i8** %dstRow615 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %588) #3
  %589 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane616 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %589, i32 0, i32 10
  %590 = load i8*, i8** %dstPlane616, align 4, !tbaa !14
  %591 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes617 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %591, i32 0, i32 12
  %592 = load i32, i32* %dstOffsetBytes617, align 4, !tbaa !15
  %593 = load i32, i32* %j601, align 4, !tbaa !12
  %594 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes618 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %594, i32 0, i32 11
  %595 = load i32, i32* %dstRowBytes618, align 4, !tbaa !16
  %mul619 = mul i32 %593, %595
  %add620 = add i32 %592, %mul619
  %arrayidx621 = getelementptr inbounds i8, i8* %590, i32 %add620
  store i8* %arrayidx621, i8** %dstRow615, align 4, !tbaa !2
  %596 = bitcast i32* %i622 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %596) #3
  store i32 0, i32* %i622, align 4, !tbaa !12
  br label %for.cond623

for.cond623:                                      ; preds = %for.inc659, %for.body607
  %597 = load i32, i32* %i622, align 4, !tbaa !12
  %598 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width624 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %598, i32 0, i32 0
  %599 = load i32, i32* %width624, align 4, !tbaa !17
  %cmp625 = icmp ult i32 %597, %599
  br i1 %cmp625, label %for.body628, label %for.cond.cleanup627

for.cond.cleanup627:                              ; preds = %for.cond623
  store i32 71, i32* %cleanup.dest.slot, align 4
  %600 = bitcast i32* %i622 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %600) #3
  br label %for.end661

for.body628:                                      ; preds = %for.cond623
  %601 = bitcast i32* %srcAlpha629 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %601) #3
  %602 = load i8*, i8** %srcRow608, align 4, !tbaa !2
  %603 = load i32, i32* %i622, align 4, !tbaa !12
  %604 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes630 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %604, i32 0, i32 7
  %605 = load i32, i32* %srcPixelBytes630, align 4, !tbaa !27
  %mul631 = mul i32 %603, %605
  %arrayidx632 = getelementptr inbounds i8, i8* %602, i32 %mul631
  %606 = bitcast i8* %arrayidx632 to i16*
  %607 = load i16, i16* %606, align 2, !tbaa !10
  %conv633 = zext i16 %607 to i32
  store i32 %conv633, i32* %srcAlpha629, align 4, !tbaa !12
  %608 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth634 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %608, i32 0, i32 2
  %609 = load i32, i32* %srcDepth634, align 4, !tbaa !20
  %610 = load i32, i32* %srcAlpha629, align 4, !tbaa !12
  %call635 = call i32 @avifLimitedToFullY(i32 %609, i32 %610)
  store i32 %call635, i32* %srcAlpha629, align 4, !tbaa !12
  %611 = bitcast float* %alphaF636 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %611) #3
  %612 = load i32, i32* %srcAlpha629, align 4, !tbaa !12
  %conv637 = sitofp i32 %612 to float
  %613 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div638 = fdiv float %conv637, %613
  store float %div638, float* %alphaF636, align 4, !tbaa !21
  %614 = bitcast i32* %dstAlpha639 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %614) #3
  %615 = load float, float* %alphaF636, align 4, !tbaa !21
  %616 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul640 = fmul float %615, %616
  %add641 = fadd float 5.000000e-01, %mul640
  %conv642 = fptosi float %add641 to i32
  store i32 %conv642, i32* %dstAlpha639, align 4, !tbaa !12
  %617 = load i32, i32* %dstAlpha639, align 4, !tbaa !12
  %cmp643 = icmp slt i32 %617, 0
  br i1 %cmp643, label %cond.true645, label %cond.false646

cond.true645:                                     ; preds = %for.body628
  br label %cond.end653

cond.false646:                                    ; preds = %for.body628
  %618 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %619 = load i32, i32* %dstAlpha639, align 4, !tbaa !12
  %cmp647 = icmp slt i32 %618, %619
  br i1 %cmp647, label %cond.true649, label %cond.false650

cond.true649:                                     ; preds = %cond.false646
  %620 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end651

cond.false650:                                    ; preds = %cond.false646
  %621 = load i32, i32* %dstAlpha639, align 4, !tbaa !12
  br label %cond.end651

cond.end651:                                      ; preds = %cond.false650, %cond.true649
  %cond652 = phi i32 [ %620, %cond.true649 ], [ %621, %cond.false650 ]
  br label %cond.end653

cond.end653:                                      ; preds = %cond.end651, %cond.true645
  %cond654 = phi i32 [ 0, %cond.true645 ], [ %cond652, %cond.end651 ]
  store i32 %cond654, i32* %dstAlpha639, align 4, !tbaa !12
  %622 = load i32, i32* %dstAlpha639, align 4, !tbaa !12
  %conv655 = trunc i32 %622 to i16
  %623 = load i8*, i8** %dstRow615, align 4, !tbaa !2
  %624 = load i32, i32* %i622, align 4, !tbaa !12
  %625 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes656 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %625, i32 0, i32 13
  %626 = load i32, i32* %dstPixelBytes656, align 4, !tbaa !18
  %mul657 = mul i32 %624, %626
  %arrayidx658 = getelementptr inbounds i8, i8* %623, i32 %mul657
  %627 = bitcast i8* %arrayidx658 to i16*
  store i16 %conv655, i16* %627, align 2, !tbaa !10
  %628 = bitcast i32* %dstAlpha639 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %628) #3
  %629 = bitcast float* %alphaF636 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %629) #3
  %630 = bitcast i32* %srcAlpha629 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %630) #3
  br label %for.inc659

for.inc659:                                       ; preds = %cond.end653
  %631 = load i32, i32* %i622, align 4, !tbaa !12
  %inc660 = add i32 %631, 1
  store i32 %inc660, i32* %i622, align 4, !tbaa !12
  br label %for.cond623

for.end661:                                       ; preds = %for.cond.cleanup627
  %632 = bitcast i8** %dstRow615 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %632) #3
  %633 = bitcast i8** %srcRow608 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %633) #3
  br label %for.inc662

for.inc662:                                       ; preds = %for.end661
  %634 = load i32, i32* %j601, align 4, !tbaa !12
  %inc663 = add i32 %634, 1
  store i32 %inc663, i32* %j601, align 4, !tbaa !12
  br label %for.cond602

for.end664:                                       ; preds = %for.cond.cleanup606
  br label %if.end730

if.else665:                                       ; preds = %if.then596
  %635 = bitcast i32* %j666 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %635) #3
  store i32 0, i32* %j666, align 4, !tbaa !12
  br label %for.cond667

for.cond667:                                      ; preds = %for.inc727, %if.else665
  %636 = load i32, i32* %j666, align 4, !tbaa !12
  %637 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height668 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %637, i32 0, i32 1
  %638 = load i32, i32* %height668, align 4, !tbaa !13
  %cmp669 = icmp ult i32 %636, %638
  br i1 %cmp669, label %for.body672, label %for.cond.cleanup671

for.cond.cleanup671:                              ; preds = %for.cond667
  store i32 74, i32* %cleanup.dest.slot, align 4
  %639 = bitcast i32* %j666 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %639) #3
  br label %for.end729

for.body672:                                      ; preds = %for.cond667
  %640 = bitcast i8** %srcRow673 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %640) #3
  %641 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane674 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %641, i32 0, i32 4
  %642 = load i8*, i8** %srcPlane674, align 4, !tbaa !24
  %643 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes675 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %643, i32 0, i32 6
  %644 = load i32, i32* %srcOffsetBytes675, align 4, !tbaa !25
  %645 = load i32, i32* %j666, align 4, !tbaa !12
  %646 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes676 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %646, i32 0, i32 5
  %647 = load i32, i32* %srcRowBytes676, align 4, !tbaa !26
  %mul677 = mul i32 %645, %647
  %add678 = add i32 %644, %mul677
  %arrayidx679 = getelementptr inbounds i8, i8* %642, i32 %add678
  store i8* %arrayidx679, i8** %srcRow673, align 4, !tbaa !2
  %648 = bitcast i8** %dstRow680 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %648) #3
  %649 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane681 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %649, i32 0, i32 10
  %650 = load i8*, i8** %dstPlane681, align 4, !tbaa !14
  %651 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes682 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %651, i32 0, i32 12
  %652 = load i32, i32* %dstOffsetBytes682, align 4, !tbaa !15
  %653 = load i32, i32* %j666, align 4, !tbaa !12
  %654 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes683 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %654, i32 0, i32 11
  %655 = load i32, i32* %dstRowBytes683, align 4, !tbaa !16
  %mul684 = mul i32 %653, %655
  %add685 = add i32 %652, %mul684
  %arrayidx686 = getelementptr inbounds i8, i8* %650, i32 %add685
  store i8* %arrayidx686, i8** %dstRow680, align 4, !tbaa !2
  %656 = bitcast i32* %i687 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %656) #3
  store i32 0, i32* %i687, align 4, !tbaa !12
  br label %for.cond688

for.cond688:                                      ; preds = %for.inc724, %for.body672
  %657 = load i32, i32* %i687, align 4, !tbaa !12
  %658 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width689 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %658, i32 0, i32 0
  %659 = load i32, i32* %width689, align 4, !tbaa !17
  %cmp690 = icmp ult i32 %657, %659
  br i1 %cmp690, label %for.body693, label %for.cond.cleanup692

for.cond.cleanup692:                              ; preds = %for.cond688
  store i32 77, i32* %cleanup.dest.slot, align 4
  %660 = bitcast i32* %i687 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %660) #3
  br label %for.end726

for.body693:                                      ; preds = %for.cond688
  %661 = bitcast i32* %srcAlpha694 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %661) #3
  %662 = load i8*, i8** %srcRow673, align 4, !tbaa !2
  %663 = load i32, i32* %i687, align 4, !tbaa !12
  %664 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes695 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %664, i32 0, i32 7
  %665 = load i32, i32* %srcPixelBytes695, align 4, !tbaa !27
  %mul696 = mul i32 %663, %665
  %arrayidx697 = getelementptr inbounds i8, i8* %662, i32 %mul696
  %666 = bitcast i8* %arrayidx697 to i16*
  %667 = load i16, i16* %666, align 2, !tbaa !10
  %conv698 = zext i16 %667 to i32
  store i32 %conv698, i32* %srcAlpha694, align 4, !tbaa !12
  %668 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth699 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %668, i32 0, i32 2
  %669 = load i32, i32* %srcDepth699, align 4, !tbaa !20
  %670 = load i32, i32* %srcAlpha694, align 4, !tbaa !12
  %call700 = call i32 @avifLimitedToFullY(i32 %669, i32 %670)
  store i32 %call700, i32* %srcAlpha694, align 4, !tbaa !12
  %671 = bitcast float* %alphaF701 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %671) #3
  %672 = load i32, i32* %srcAlpha694, align 4, !tbaa !12
  %conv702 = sitofp i32 %672 to float
  %673 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div703 = fdiv float %conv702, %673
  store float %div703, float* %alphaF701, align 4, !tbaa !21
  %674 = bitcast i32* %dstAlpha704 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %674) #3
  %675 = load float, float* %alphaF701, align 4, !tbaa !21
  %676 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul705 = fmul float %675, %676
  %add706 = fadd float 5.000000e-01, %mul705
  %conv707 = fptosi float %add706 to i32
  store i32 %conv707, i32* %dstAlpha704, align 4, !tbaa !12
  %677 = load i32, i32* %dstAlpha704, align 4, !tbaa !12
  %cmp708 = icmp slt i32 %677, 0
  br i1 %cmp708, label %cond.true710, label %cond.false711

cond.true710:                                     ; preds = %for.body693
  br label %cond.end718

cond.false711:                                    ; preds = %for.body693
  %678 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %679 = load i32, i32* %dstAlpha704, align 4, !tbaa !12
  %cmp712 = icmp slt i32 %678, %679
  br i1 %cmp712, label %cond.true714, label %cond.false715

cond.true714:                                     ; preds = %cond.false711
  %680 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end716

cond.false715:                                    ; preds = %cond.false711
  %681 = load i32, i32* %dstAlpha704, align 4, !tbaa !12
  br label %cond.end716

cond.end716:                                      ; preds = %cond.false715, %cond.true714
  %cond717 = phi i32 [ %680, %cond.true714 ], [ %681, %cond.false715 ]
  br label %cond.end718

cond.end718:                                      ; preds = %cond.end716, %cond.true710
  %cond719 = phi i32 [ 0, %cond.true710 ], [ %cond717, %cond.end716 ]
  store i32 %cond719, i32* %dstAlpha704, align 4, !tbaa !12
  %682 = load i32, i32* %dstAlpha704, align 4, !tbaa !12
  %conv720 = trunc i32 %682 to i8
  %683 = load i8*, i8** %dstRow680, align 4, !tbaa !2
  %684 = load i32, i32* %i687, align 4, !tbaa !12
  %685 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes721 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %685, i32 0, i32 13
  %686 = load i32, i32* %dstPixelBytes721, align 4, !tbaa !18
  %mul722 = mul i32 %684, %686
  %arrayidx723 = getelementptr inbounds i8, i8* %683, i32 %mul722
  store i8 %conv720, i8* %arrayidx723, align 1, !tbaa !19
  %687 = bitcast i32* %dstAlpha704 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %687) #3
  %688 = bitcast float* %alphaF701 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %688) #3
  %689 = bitcast i32* %srcAlpha694 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %689) #3
  br label %for.inc724

for.inc724:                                       ; preds = %cond.end718
  %690 = load i32, i32* %i687, align 4, !tbaa !12
  %inc725 = add i32 %690, 1
  store i32 %inc725, i32* %i687, align 4, !tbaa !12
  br label %for.cond688

for.end726:                                       ; preds = %for.cond.cleanup692
  %691 = bitcast i8** %dstRow680 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %691) #3
  %692 = bitcast i8** %srcRow673 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %692) #3
  br label %for.inc727

for.inc727:                                       ; preds = %for.end726
  %693 = load i32, i32* %j666, align 4, !tbaa !12
  %inc728 = add i32 %693, 1
  store i32 %inc728, i32* %j666, align 4, !tbaa !12
  br label %for.cond667

for.end729:                                       ; preds = %for.cond.cleanup671
  br label %if.end730

if.end730:                                        ; preds = %for.end729, %for.end664
  br label %if.end796

if.else731:                                       ; preds = %if.then592
  %694 = bitcast i32* %j732 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %694) #3
  store i32 0, i32* %j732, align 4, !tbaa !12
  br label %for.cond733

for.cond733:                                      ; preds = %for.inc793, %if.else731
  %695 = load i32, i32* %j732, align 4, !tbaa !12
  %696 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height734 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %696, i32 0, i32 1
  %697 = load i32, i32* %height734, align 4, !tbaa !13
  %cmp735 = icmp ult i32 %695, %697
  br i1 %cmp735, label %for.body738, label %for.cond.cleanup737

for.cond.cleanup737:                              ; preds = %for.cond733
  store i32 80, i32* %cleanup.dest.slot, align 4
  %698 = bitcast i32* %j732 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %698) #3
  br label %for.end795

for.body738:                                      ; preds = %for.cond733
  %699 = bitcast i8** %srcRow739 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %699) #3
  %700 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane740 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %700, i32 0, i32 4
  %701 = load i8*, i8** %srcPlane740, align 4, !tbaa !24
  %702 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes741 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %702, i32 0, i32 6
  %703 = load i32, i32* %srcOffsetBytes741, align 4, !tbaa !25
  %704 = load i32, i32* %j732, align 4, !tbaa !12
  %705 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes742 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %705, i32 0, i32 5
  %706 = load i32, i32* %srcRowBytes742, align 4, !tbaa !26
  %mul743 = mul i32 %704, %706
  %add744 = add i32 %703, %mul743
  %arrayidx745 = getelementptr inbounds i8, i8* %701, i32 %add744
  store i8* %arrayidx745, i8** %srcRow739, align 4, !tbaa !2
  %707 = bitcast i8** %dstRow746 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %707) #3
  %708 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane747 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %708, i32 0, i32 10
  %709 = load i8*, i8** %dstPlane747, align 4, !tbaa !14
  %710 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes748 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %710, i32 0, i32 12
  %711 = load i32, i32* %dstOffsetBytes748, align 4, !tbaa !15
  %712 = load i32, i32* %j732, align 4, !tbaa !12
  %713 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes749 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %713, i32 0, i32 11
  %714 = load i32, i32* %dstRowBytes749, align 4, !tbaa !16
  %mul750 = mul i32 %712, %714
  %add751 = add i32 %711, %mul750
  %arrayidx752 = getelementptr inbounds i8, i8* %709, i32 %add751
  store i8* %arrayidx752, i8** %dstRow746, align 4, !tbaa !2
  %715 = bitcast i32* %i753 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %715) #3
  store i32 0, i32* %i753, align 4, !tbaa !12
  br label %for.cond754

for.cond754:                                      ; preds = %for.inc790, %for.body738
  %716 = load i32, i32* %i753, align 4, !tbaa !12
  %717 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width755 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %717, i32 0, i32 0
  %718 = load i32, i32* %width755, align 4, !tbaa !17
  %cmp756 = icmp ult i32 %716, %718
  br i1 %cmp756, label %for.body759, label %for.cond.cleanup758

for.cond.cleanup758:                              ; preds = %for.cond754
  store i32 83, i32* %cleanup.dest.slot, align 4
  %719 = bitcast i32* %i753 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %719) #3
  br label %for.end792

for.body759:                                      ; preds = %for.cond754
  %720 = bitcast i32* %srcAlpha760 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %720) #3
  %721 = load i8*, i8** %srcRow739, align 4, !tbaa !2
  %722 = load i32, i32* %i753, align 4, !tbaa !12
  %723 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes761 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %723, i32 0, i32 7
  %724 = load i32, i32* %srcPixelBytes761, align 4, !tbaa !27
  %mul762 = mul i32 %722, %724
  %arrayidx763 = getelementptr inbounds i8, i8* %721, i32 %mul762
  %725 = load i8, i8* %arrayidx763, align 1, !tbaa !19
  %conv764 = zext i8 %725 to i32
  store i32 %conv764, i32* %srcAlpha760, align 4, !tbaa !12
  %726 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth765 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %726, i32 0, i32 2
  %727 = load i32, i32* %srcDepth765, align 4, !tbaa !20
  %728 = load i32, i32* %srcAlpha760, align 4, !tbaa !12
  %call766 = call i32 @avifLimitedToFullY(i32 %727, i32 %728)
  store i32 %call766, i32* %srcAlpha760, align 4, !tbaa !12
  %729 = bitcast float* %alphaF767 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %729) #3
  %730 = load i32, i32* %srcAlpha760, align 4, !tbaa !12
  %conv768 = sitofp i32 %730 to float
  %731 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div769 = fdiv float %conv768, %731
  store float %div769, float* %alphaF767, align 4, !tbaa !21
  %732 = bitcast i32* %dstAlpha770 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %732) #3
  %733 = load float, float* %alphaF767, align 4, !tbaa !21
  %734 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul771 = fmul float %733, %734
  %add772 = fadd float 5.000000e-01, %mul771
  %conv773 = fptosi float %add772 to i32
  store i32 %conv773, i32* %dstAlpha770, align 4, !tbaa !12
  %735 = load i32, i32* %dstAlpha770, align 4, !tbaa !12
  %cmp774 = icmp slt i32 %735, 0
  br i1 %cmp774, label %cond.true776, label %cond.false777

cond.true776:                                     ; preds = %for.body759
  br label %cond.end784

cond.false777:                                    ; preds = %for.body759
  %736 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %737 = load i32, i32* %dstAlpha770, align 4, !tbaa !12
  %cmp778 = icmp slt i32 %736, %737
  br i1 %cmp778, label %cond.true780, label %cond.false781

cond.true780:                                     ; preds = %cond.false777
  %738 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end782

cond.false781:                                    ; preds = %cond.false777
  %739 = load i32, i32* %dstAlpha770, align 4, !tbaa !12
  br label %cond.end782

cond.end782:                                      ; preds = %cond.false781, %cond.true780
  %cond783 = phi i32 [ %738, %cond.true780 ], [ %739, %cond.false781 ]
  br label %cond.end784

cond.end784:                                      ; preds = %cond.end782, %cond.true776
  %cond785 = phi i32 [ 0, %cond.true776 ], [ %cond783, %cond.end782 ]
  store i32 %cond785, i32* %dstAlpha770, align 4, !tbaa !12
  %740 = load i32, i32* %dstAlpha770, align 4, !tbaa !12
  %conv786 = trunc i32 %740 to i16
  %741 = load i8*, i8** %dstRow746, align 4, !tbaa !2
  %742 = load i32, i32* %i753, align 4, !tbaa !12
  %743 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes787 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %743, i32 0, i32 13
  %744 = load i32, i32* %dstPixelBytes787, align 4, !tbaa !18
  %mul788 = mul i32 %742, %744
  %arrayidx789 = getelementptr inbounds i8, i8* %741, i32 %mul788
  %745 = bitcast i8* %arrayidx789 to i16*
  store i16 %conv786, i16* %745, align 2, !tbaa !10
  %746 = bitcast i32* %dstAlpha770 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %746) #3
  %747 = bitcast float* %alphaF767 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %747) #3
  %748 = bitcast i32* %srcAlpha760 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %748) #3
  br label %for.inc790

for.inc790:                                       ; preds = %cond.end784
  %749 = load i32, i32* %i753, align 4, !tbaa !12
  %inc791 = add i32 %749, 1
  store i32 %inc791, i32* %i753, align 4, !tbaa !12
  br label %for.cond754

for.end792:                                       ; preds = %for.cond.cleanup758
  %750 = bitcast i8** %dstRow746 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %750) #3
  %751 = bitcast i8** %srcRow739 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %751) #3
  br label %for.inc793

for.inc793:                                       ; preds = %for.end792
  %752 = load i32, i32* %j732, align 4, !tbaa !12
  %inc794 = add i32 %752, 1
  store i32 %inc794, i32* %j732, align 4, !tbaa !12
  br label %for.cond733

for.end795:                                       ; preds = %for.cond.cleanup737
  br label %if.end796

if.end796:                                        ; preds = %for.end795, %if.end730
  br label %if.end1231

if.else797:                                       ; preds = %land.lhs.true588, %if.else584
  %753 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRange798 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %753, i32 0, i32 3
  %754 = load i32, i32* %srcRange798, align 4, !tbaa !23
  %cmp799 = icmp eq i32 %754, 1
  br i1 %cmp799, label %land.lhs.true801, label %if.else1010

land.lhs.true801:                                 ; preds = %if.else797
  %755 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange802 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %755, i32 0, i32 9
  %756 = load i32, i32* %dstRange802, align 4, !tbaa !9
  %cmp803 = icmp eq i32 %756, 0
  br i1 %cmp803, label %if.then805, label %if.else1010

if.then805:                                       ; preds = %land.lhs.true801
  %757 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth806 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %757, i32 0, i32 2
  %758 = load i32, i32* %srcDepth806, align 4, !tbaa !20
  %cmp807 = icmp ugt i32 %758, 8
  br i1 %cmp807, label %if.then809, label %if.else944

if.then809:                                       ; preds = %if.then805
  %759 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth810 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %759, i32 0, i32 8
  %760 = load i32, i32* %dstDepth810, align 4, !tbaa !6
  %cmp811 = icmp ugt i32 %760, 8
  br i1 %cmp811, label %if.then813, label %if.else878

if.then813:                                       ; preds = %if.then809
  %761 = bitcast i32* %j814 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %761) #3
  store i32 0, i32* %j814, align 4, !tbaa !12
  br label %for.cond815

for.cond815:                                      ; preds = %for.inc875, %if.then813
  %762 = load i32, i32* %j814, align 4, !tbaa !12
  %763 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height816 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %763, i32 0, i32 1
  %764 = load i32, i32* %height816, align 4, !tbaa !13
  %cmp817 = icmp ult i32 %762, %764
  br i1 %cmp817, label %for.body820, label %for.cond.cleanup819

for.cond.cleanup819:                              ; preds = %for.cond815
  store i32 86, i32* %cleanup.dest.slot, align 4
  %765 = bitcast i32* %j814 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %765) #3
  br label %for.end877

for.body820:                                      ; preds = %for.cond815
  %766 = bitcast i8** %srcRow821 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %766) #3
  %767 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane822 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %767, i32 0, i32 4
  %768 = load i8*, i8** %srcPlane822, align 4, !tbaa !24
  %769 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes823 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %769, i32 0, i32 6
  %770 = load i32, i32* %srcOffsetBytes823, align 4, !tbaa !25
  %771 = load i32, i32* %j814, align 4, !tbaa !12
  %772 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes824 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %772, i32 0, i32 5
  %773 = load i32, i32* %srcRowBytes824, align 4, !tbaa !26
  %mul825 = mul i32 %771, %773
  %add826 = add i32 %770, %mul825
  %arrayidx827 = getelementptr inbounds i8, i8* %768, i32 %add826
  store i8* %arrayidx827, i8** %srcRow821, align 4, !tbaa !2
  %774 = bitcast i8** %dstRow828 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %774) #3
  %775 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane829 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %775, i32 0, i32 10
  %776 = load i8*, i8** %dstPlane829, align 4, !tbaa !14
  %777 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes830 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %777, i32 0, i32 12
  %778 = load i32, i32* %dstOffsetBytes830, align 4, !tbaa !15
  %779 = load i32, i32* %j814, align 4, !tbaa !12
  %780 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes831 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %780, i32 0, i32 11
  %781 = load i32, i32* %dstRowBytes831, align 4, !tbaa !16
  %mul832 = mul i32 %779, %781
  %add833 = add i32 %778, %mul832
  %arrayidx834 = getelementptr inbounds i8, i8* %776, i32 %add833
  store i8* %arrayidx834, i8** %dstRow828, align 4, !tbaa !2
  %782 = bitcast i32* %i835 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %782) #3
  store i32 0, i32* %i835, align 4, !tbaa !12
  br label %for.cond836

for.cond836:                                      ; preds = %for.inc872, %for.body820
  %783 = load i32, i32* %i835, align 4, !tbaa !12
  %784 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width837 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %784, i32 0, i32 0
  %785 = load i32, i32* %width837, align 4, !tbaa !17
  %cmp838 = icmp ult i32 %783, %785
  br i1 %cmp838, label %for.body841, label %for.cond.cleanup840

for.cond.cleanup840:                              ; preds = %for.cond836
  store i32 89, i32* %cleanup.dest.slot, align 4
  %786 = bitcast i32* %i835 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %786) #3
  br label %for.end874

for.body841:                                      ; preds = %for.cond836
  %787 = bitcast i32* %srcAlpha842 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %787) #3
  %788 = load i8*, i8** %srcRow821, align 4, !tbaa !2
  %789 = load i32, i32* %i835, align 4, !tbaa !12
  %790 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes843 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %790, i32 0, i32 7
  %791 = load i32, i32* %srcPixelBytes843, align 4, !tbaa !27
  %mul844 = mul i32 %789, %791
  %arrayidx845 = getelementptr inbounds i8, i8* %788, i32 %mul844
  %792 = bitcast i8* %arrayidx845 to i16*
  %793 = load i16, i16* %792, align 2, !tbaa !10
  %conv846 = zext i16 %793 to i32
  store i32 %conv846, i32* %srcAlpha842, align 4, !tbaa !12
  %794 = bitcast float* %alphaF847 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %794) #3
  %795 = load i32, i32* %srcAlpha842, align 4, !tbaa !12
  %conv848 = sitofp i32 %795 to float
  %796 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div849 = fdiv float %conv848, %796
  store float %div849, float* %alphaF847, align 4, !tbaa !21
  %797 = bitcast i32* %dstAlpha850 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %797) #3
  %798 = load float, float* %alphaF847, align 4, !tbaa !21
  %799 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul851 = fmul float %798, %799
  %add852 = fadd float 5.000000e-01, %mul851
  %conv853 = fptosi float %add852 to i32
  store i32 %conv853, i32* %dstAlpha850, align 4, !tbaa !12
  %800 = load i32, i32* %dstAlpha850, align 4, !tbaa !12
  %cmp854 = icmp slt i32 %800, 0
  br i1 %cmp854, label %cond.true856, label %cond.false857

cond.true856:                                     ; preds = %for.body841
  br label %cond.end864

cond.false857:                                    ; preds = %for.body841
  %801 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %802 = load i32, i32* %dstAlpha850, align 4, !tbaa !12
  %cmp858 = icmp slt i32 %801, %802
  br i1 %cmp858, label %cond.true860, label %cond.false861

cond.true860:                                     ; preds = %cond.false857
  %803 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end862

cond.false861:                                    ; preds = %cond.false857
  %804 = load i32, i32* %dstAlpha850, align 4, !tbaa !12
  br label %cond.end862

cond.end862:                                      ; preds = %cond.false861, %cond.true860
  %cond863 = phi i32 [ %803, %cond.true860 ], [ %804, %cond.false861 ]
  br label %cond.end864

cond.end864:                                      ; preds = %cond.end862, %cond.true856
  %cond865 = phi i32 [ 0, %cond.true856 ], [ %cond863, %cond.end862 ]
  store i32 %cond865, i32* %dstAlpha850, align 4, !tbaa !12
  %805 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth866 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %805, i32 0, i32 8
  %806 = load i32, i32* %dstDepth866, align 4, !tbaa !6
  %807 = load i32, i32* %dstAlpha850, align 4, !tbaa !12
  %call867 = call i32 @avifFullToLimitedY(i32 %806, i32 %807)
  store i32 %call867, i32* %dstAlpha850, align 4, !tbaa !12
  %808 = load i32, i32* %dstAlpha850, align 4, !tbaa !12
  %conv868 = trunc i32 %808 to i16
  %809 = load i8*, i8** %dstRow828, align 4, !tbaa !2
  %810 = load i32, i32* %i835, align 4, !tbaa !12
  %811 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes869 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %811, i32 0, i32 13
  %812 = load i32, i32* %dstPixelBytes869, align 4, !tbaa !18
  %mul870 = mul i32 %810, %812
  %arrayidx871 = getelementptr inbounds i8, i8* %809, i32 %mul870
  %813 = bitcast i8* %arrayidx871 to i16*
  store i16 %conv868, i16* %813, align 2, !tbaa !10
  %814 = bitcast i32* %dstAlpha850 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %814) #3
  %815 = bitcast float* %alphaF847 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %815) #3
  %816 = bitcast i32* %srcAlpha842 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %816) #3
  br label %for.inc872

for.inc872:                                       ; preds = %cond.end864
  %817 = load i32, i32* %i835, align 4, !tbaa !12
  %inc873 = add i32 %817, 1
  store i32 %inc873, i32* %i835, align 4, !tbaa !12
  br label %for.cond836

for.end874:                                       ; preds = %for.cond.cleanup840
  %818 = bitcast i8** %dstRow828 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %818) #3
  %819 = bitcast i8** %srcRow821 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %819) #3
  br label %for.inc875

for.inc875:                                       ; preds = %for.end874
  %820 = load i32, i32* %j814, align 4, !tbaa !12
  %inc876 = add i32 %820, 1
  store i32 %inc876, i32* %j814, align 4, !tbaa !12
  br label %for.cond815

for.end877:                                       ; preds = %for.cond.cleanup819
  br label %if.end943

if.else878:                                       ; preds = %if.then809
  %821 = bitcast i32* %j879 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %821) #3
  store i32 0, i32* %j879, align 4, !tbaa !12
  br label %for.cond880

for.cond880:                                      ; preds = %for.inc940, %if.else878
  %822 = load i32, i32* %j879, align 4, !tbaa !12
  %823 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height881 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %823, i32 0, i32 1
  %824 = load i32, i32* %height881, align 4, !tbaa !13
  %cmp882 = icmp ult i32 %822, %824
  br i1 %cmp882, label %for.body885, label %for.cond.cleanup884

for.cond.cleanup884:                              ; preds = %for.cond880
  store i32 92, i32* %cleanup.dest.slot, align 4
  %825 = bitcast i32* %j879 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %825) #3
  br label %for.end942

for.body885:                                      ; preds = %for.cond880
  %826 = bitcast i8** %srcRow886 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %826) #3
  %827 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane887 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %827, i32 0, i32 4
  %828 = load i8*, i8** %srcPlane887, align 4, !tbaa !24
  %829 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes888 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %829, i32 0, i32 6
  %830 = load i32, i32* %srcOffsetBytes888, align 4, !tbaa !25
  %831 = load i32, i32* %j879, align 4, !tbaa !12
  %832 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes889 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %832, i32 0, i32 5
  %833 = load i32, i32* %srcRowBytes889, align 4, !tbaa !26
  %mul890 = mul i32 %831, %833
  %add891 = add i32 %830, %mul890
  %arrayidx892 = getelementptr inbounds i8, i8* %828, i32 %add891
  store i8* %arrayidx892, i8** %srcRow886, align 4, !tbaa !2
  %834 = bitcast i8** %dstRow893 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %834) #3
  %835 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane894 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %835, i32 0, i32 10
  %836 = load i8*, i8** %dstPlane894, align 4, !tbaa !14
  %837 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes895 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %837, i32 0, i32 12
  %838 = load i32, i32* %dstOffsetBytes895, align 4, !tbaa !15
  %839 = load i32, i32* %j879, align 4, !tbaa !12
  %840 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes896 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %840, i32 0, i32 11
  %841 = load i32, i32* %dstRowBytes896, align 4, !tbaa !16
  %mul897 = mul i32 %839, %841
  %add898 = add i32 %838, %mul897
  %arrayidx899 = getelementptr inbounds i8, i8* %836, i32 %add898
  store i8* %arrayidx899, i8** %dstRow893, align 4, !tbaa !2
  %842 = bitcast i32* %i900 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %842) #3
  store i32 0, i32* %i900, align 4, !tbaa !12
  br label %for.cond901

for.cond901:                                      ; preds = %for.inc937, %for.body885
  %843 = load i32, i32* %i900, align 4, !tbaa !12
  %844 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width902 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %844, i32 0, i32 0
  %845 = load i32, i32* %width902, align 4, !tbaa !17
  %cmp903 = icmp ult i32 %843, %845
  br i1 %cmp903, label %for.body906, label %for.cond.cleanup905

for.cond.cleanup905:                              ; preds = %for.cond901
  store i32 95, i32* %cleanup.dest.slot, align 4
  %846 = bitcast i32* %i900 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %846) #3
  br label %for.end939

for.body906:                                      ; preds = %for.cond901
  %847 = bitcast i32* %srcAlpha907 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %847) #3
  %848 = load i8*, i8** %srcRow886, align 4, !tbaa !2
  %849 = load i32, i32* %i900, align 4, !tbaa !12
  %850 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes908 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %850, i32 0, i32 7
  %851 = load i32, i32* %srcPixelBytes908, align 4, !tbaa !27
  %mul909 = mul i32 %849, %851
  %arrayidx910 = getelementptr inbounds i8, i8* %848, i32 %mul909
  %852 = bitcast i8* %arrayidx910 to i16*
  %853 = load i16, i16* %852, align 2, !tbaa !10
  %conv911 = zext i16 %853 to i32
  store i32 %conv911, i32* %srcAlpha907, align 4, !tbaa !12
  %854 = bitcast float* %alphaF912 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %854) #3
  %855 = load i32, i32* %srcAlpha907, align 4, !tbaa !12
  %conv913 = sitofp i32 %855 to float
  %856 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div914 = fdiv float %conv913, %856
  store float %div914, float* %alphaF912, align 4, !tbaa !21
  %857 = bitcast i32* %dstAlpha915 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %857) #3
  %858 = load float, float* %alphaF912, align 4, !tbaa !21
  %859 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul916 = fmul float %858, %859
  %add917 = fadd float 5.000000e-01, %mul916
  %conv918 = fptosi float %add917 to i32
  store i32 %conv918, i32* %dstAlpha915, align 4, !tbaa !12
  %860 = load i32, i32* %dstAlpha915, align 4, !tbaa !12
  %cmp919 = icmp slt i32 %860, 0
  br i1 %cmp919, label %cond.true921, label %cond.false922

cond.true921:                                     ; preds = %for.body906
  br label %cond.end929

cond.false922:                                    ; preds = %for.body906
  %861 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %862 = load i32, i32* %dstAlpha915, align 4, !tbaa !12
  %cmp923 = icmp slt i32 %861, %862
  br i1 %cmp923, label %cond.true925, label %cond.false926

cond.true925:                                     ; preds = %cond.false922
  %863 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end927

cond.false926:                                    ; preds = %cond.false922
  %864 = load i32, i32* %dstAlpha915, align 4, !tbaa !12
  br label %cond.end927

cond.end927:                                      ; preds = %cond.false926, %cond.true925
  %cond928 = phi i32 [ %863, %cond.true925 ], [ %864, %cond.false926 ]
  br label %cond.end929

cond.end929:                                      ; preds = %cond.end927, %cond.true921
  %cond930 = phi i32 [ 0, %cond.true921 ], [ %cond928, %cond.end927 ]
  store i32 %cond930, i32* %dstAlpha915, align 4, !tbaa !12
  %865 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth931 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %865, i32 0, i32 8
  %866 = load i32, i32* %dstDepth931, align 4, !tbaa !6
  %867 = load i32, i32* %dstAlpha915, align 4, !tbaa !12
  %call932 = call i32 @avifFullToLimitedY(i32 %866, i32 %867)
  store i32 %call932, i32* %dstAlpha915, align 4, !tbaa !12
  %868 = load i32, i32* %dstAlpha915, align 4, !tbaa !12
  %conv933 = trunc i32 %868 to i8
  %869 = load i8*, i8** %dstRow893, align 4, !tbaa !2
  %870 = load i32, i32* %i900, align 4, !tbaa !12
  %871 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes934 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %871, i32 0, i32 13
  %872 = load i32, i32* %dstPixelBytes934, align 4, !tbaa !18
  %mul935 = mul i32 %870, %872
  %arrayidx936 = getelementptr inbounds i8, i8* %869, i32 %mul935
  store i8 %conv933, i8* %arrayidx936, align 1, !tbaa !19
  %873 = bitcast i32* %dstAlpha915 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %873) #3
  %874 = bitcast float* %alphaF912 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %874) #3
  %875 = bitcast i32* %srcAlpha907 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %875) #3
  br label %for.inc937

for.inc937:                                       ; preds = %cond.end929
  %876 = load i32, i32* %i900, align 4, !tbaa !12
  %inc938 = add i32 %876, 1
  store i32 %inc938, i32* %i900, align 4, !tbaa !12
  br label %for.cond901

for.end939:                                       ; preds = %for.cond.cleanup905
  %877 = bitcast i8** %dstRow893 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %877) #3
  %878 = bitcast i8** %srcRow886 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %878) #3
  br label %for.inc940

for.inc940:                                       ; preds = %for.end939
  %879 = load i32, i32* %j879, align 4, !tbaa !12
  %inc941 = add i32 %879, 1
  store i32 %inc941, i32* %j879, align 4, !tbaa !12
  br label %for.cond880

for.end942:                                       ; preds = %for.cond.cleanup884
  br label %if.end943

if.end943:                                        ; preds = %for.end942, %for.end877
  br label %if.end1009

if.else944:                                       ; preds = %if.then805
  %880 = bitcast i32* %j945 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %880) #3
  store i32 0, i32* %j945, align 4, !tbaa !12
  br label %for.cond946

for.cond946:                                      ; preds = %for.inc1006, %if.else944
  %881 = load i32, i32* %j945, align 4, !tbaa !12
  %882 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height947 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %882, i32 0, i32 1
  %883 = load i32, i32* %height947, align 4, !tbaa !13
  %cmp948 = icmp ult i32 %881, %883
  br i1 %cmp948, label %for.body951, label %for.cond.cleanup950

for.cond.cleanup950:                              ; preds = %for.cond946
  store i32 98, i32* %cleanup.dest.slot, align 4
  %884 = bitcast i32* %j945 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %884) #3
  br label %for.end1008

for.body951:                                      ; preds = %for.cond946
  %885 = bitcast i8** %srcRow952 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %885) #3
  %886 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane953 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %886, i32 0, i32 4
  %887 = load i8*, i8** %srcPlane953, align 4, !tbaa !24
  %888 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes954 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %888, i32 0, i32 6
  %889 = load i32, i32* %srcOffsetBytes954, align 4, !tbaa !25
  %890 = load i32, i32* %j945, align 4, !tbaa !12
  %891 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes955 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %891, i32 0, i32 5
  %892 = load i32, i32* %srcRowBytes955, align 4, !tbaa !26
  %mul956 = mul i32 %890, %892
  %add957 = add i32 %889, %mul956
  %arrayidx958 = getelementptr inbounds i8, i8* %887, i32 %add957
  store i8* %arrayidx958, i8** %srcRow952, align 4, !tbaa !2
  %893 = bitcast i8** %dstRow959 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %893) #3
  %894 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane960 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %894, i32 0, i32 10
  %895 = load i8*, i8** %dstPlane960, align 4, !tbaa !14
  %896 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes961 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %896, i32 0, i32 12
  %897 = load i32, i32* %dstOffsetBytes961, align 4, !tbaa !15
  %898 = load i32, i32* %j945, align 4, !tbaa !12
  %899 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes962 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %899, i32 0, i32 11
  %900 = load i32, i32* %dstRowBytes962, align 4, !tbaa !16
  %mul963 = mul i32 %898, %900
  %add964 = add i32 %897, %mul963
  %arrayidx965 = getelementptr inbounds i8, i8* %895, i32 %add964
  store i8* %arrayidx965, i8** %dstRow959, align 4, !tbaa !2
  %901 = bitcast i32* %i966 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %901) #3
  store i32 0, i32* %i966, align 4, !tbaa !12
  br label %for.cond967

for.cond967:                                      ; preds = %for.inc1003, %for.body951
  %902 = load i32, i32* %i966, align 4, !tbaa !12
  %903 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width968 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %903, i32 0, i32 0
  %904 = load i32, i32* %width968, align 4, !tbaa !17
  %cmp969 = icmp ult i32 %902, %904
  br i1 %cmp969, label %for.body972, label %for.cond.cleanup971

for.cond.cleanup971:                              ; preds = %for.cond967
  store i32 101, i32* %cleanup.dest.slot, align 4
  %905 = bitcast i32* %i966 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %905) #3
  br label %for.end1005

for.body972:                                      ; preds = %for.cond967
  %906 = bitcast i32* %srcAlpha973 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %906) #3
  %907 = load i8*, i8** %srcRow952, align 4, !tbaa !2
  %908 = load i32, i32* %i966, align 4, !tbaa !12
  %909 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes974 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %909, i32 0, i32 7
  %910 = load i32, i32* %srcPixelBytes974, align 4, !tbaa !27
  %mul975 = mul i32 %908, %910
  %arrayidx976 = getelementptr inbounds i8, i8* %907, i32 %mul975
  %911 = load i8, i8* %arrayidx976, align 1, !tbaa !19
  %conv977 = zext i8 %911 to i32
  store i32 %conv977, i32* %srcAlpha973, align 4, !tbaa !12
  %912 = bitcast float* %alphaF978 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %912) #3
  %913 = load i32, i32* %srcAlpha973, align 4, !tbaa !12
  %conv979 = sitofp i32 %913 to float
  %914 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div980 = fdiv float %conv979, %914
  store float %div980, float* %alphaF978, align 4, !tbaa !21
  %915 = bitcast i32* %dstAlpha981 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %915) #3
  %916 = load float, float* %alphaF978, align 4, !tbaa !21
  %917 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul982 = fmul float %916, %917
  %add983 = fadd float 5.000000e-01, %mul982
  %conv984 = fptosi float %add983 to i32
  store i32 %conv984, i32* %dstAlpha981, align 4, !tbaa !12
  %918 = load i32, i32* %dstAlpha981, align 4, !tbaa !12
  %cmp985 = icmp slt i32 %918, 0
  br i1 %cmp985, label %cond.true987, label %cond.false988

cond.true987:                                     ; preds = %for.body972
  br label %cond.end995

cond.false988:                                    ; preds = %for.body972
  %919 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %920 = load i32, i32* %dstAlpha981, align 4, !tbaa !12
  %cmp989 = icmp slt i32 %919, %920
  br i1 %cmp989, label %cond.true991, label %cond.false992

cond.true991:                                     ; preds = %cond.false988
  %921 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end993

cond.false992:                                    ; preds = %cond.false988
  %922 = load i32, i32* %dstAlpha981, align 4, !tbaa !12
  br label %cond.end993

cond.end993:                                      ; preds = %cond.false992, %cond.true991
  %cond994 = phi i32 [ %921, %cond.true991 ], [ %922, %cond.false992 ]
  br label %cond.end995

cond.end995:                                      ; preds = %cond.end993, %cond.true987
  %cond996 = phi i32 [ 0, %cond.true987 ], [ %cond994, %cond.end993 ]
  store i32 %cond996, i32* %dstAlpha981, align 4, !tbaa !12
  %923 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth997 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %923, i32 0, i32 8
  %924 = load i32, i32* %dstDepth997, align 4, !tbaa !6
  %925 = load i32, i32* %dstAlpha981, align 4, !tbaa !12
  %call998 = call i32 @avifFullToLimitedY(i32 %924, i32 %925)
  store i32 %call998, i32* %dstAlpha981, align 4, !tbaa !12
  %926 = load i32, i32* %dstAlpha981, align 4, !tbaa !12
  %conv999 = trunc i32 %926 to i16
  %927 = load i8*, i8** %dstRow959, align 4, !tbaa !2
  %928 = load i32, i32* %i966, align 4, !tbaa !12
  %929 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes1000 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %929, i32 0, i32 13
  %930 = load i32, i32* %dstPixelBytes1000, align 4, !tbaa !18
  %mul1001 = mul i32 %928, %930
  %arrayidx1002 = getelementptr inbounds i8, i8* %927, i32 %mul1001
  %931 = bitcast i8* %arrayidx1002 to i16*
  store i16 %conv999, i16* %931, align 2, !tbaa !10
  %932 = bitcast i32* %dstAlpha981 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %932) #3
  %933 = bitcast float* %alphaF978 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %933) #3
  %934 = bitcast i32* %srcAlpha973 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %934) #3
  br label %for.inc1003

for.inc1003:                                      ; preds = %cond.end995
  %935 = load i32, i32* %i966, align 4, !tbaa !12
  %inc1004 = add i32 %935, 1
  store i32 %inc1004, i32* %i966, align 4, !tbaa !12
  br label %for.cond967

for.end1005:                                      ; preds = %for.cond.cleanup971
  %936 = bitcast i8** %dstRow959 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %936) #3
  %937 = bitcast i8** %srcRow952 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %937) #3
  br label %for.inc1006

for.inc1006:                                      ; preds = %for.end1005
  %938 = load i32, i32* %j945, align 4, !tbaa !12
  %inc1007 = add i32 %938, 1
  store i32 %inc1007, i32* %j945, align 4, !tbaa !12
  br label %for.cond946

for.end1008:                                      ; preds = %for.cond.cleanup950
  br label %if.end1009

if.end1009:                                       ; preds = %for.end1008, %if.end943
  br label %if.end1230

if.else1010:                                      ; preds = %land.lhs.true801, %if.else797
  %939 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRange1011 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %939, i32 0, i32 3
  %940 = load i32, i32* %srcRange1011, align 4, !tbaa !23
  %cmp1012 = icmp eq i32 %940, 0
  br i1 %cmp1012, label %land.lhs.true1014, label %if.end1229

land.lhs.true1014:                                ; preds = %if.else1010
  %941 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRange1015 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %941, i32 0, i32 9
  %942 = load i32, i32* %dstRange1015, align 4, !tbaa !9
  %cmp1016 = icmp eq i32 %942, 0
  br i1 %cmp1016, label %if.then1018, label %if.end1229

if.then1018:                                      ; preds = %land.lhs.true1014
  %943 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth1019 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %943, i32 0, i32 2
  %944 = load i32, i32* %srcDepth1019, align 4, !tbaa !20
  %cmp1020 = icmp ugt i32 %944, 8
  br i1 %cmp1020, label %if.then1022, label %if.else1161

if.then1022:                                      ; preds = %if.then1018
  %945 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth1023 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %945, i32 0, i32 8
  %946 = load i32, i32* %dstDepth1023, align 4, !tbaa !6
  %cmp1024 = icmp ugt i32 %946, 8
  br i1 %cmp1024, label %if.then1026, label %if.else1093

if.then1026:                                      ; preds = %if.then1022
  %947 = bitcast i32* %j1027 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %947) #3
  store i32 0, i32* %j1027, align 4, !tbaa !12
  br label %for.cond1028

for.cond1028:                                     ; preds = %for.inc1090, %if.then1026
  %948 = load i32, i32* %j1027, align 4, !tbaa !12
  %949 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height1029 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %949, i32 0, i32 1
  %950 = load i32, i32* %height1029, align 4, !tbaa !13
  %cmp1030 = icmp ult i32 %948, %950
  br i1 %cmp1030, label %for.body1033, label %for.cond.cleanup1032

for.cond.cleanup1032:                             ; preds = %for.cond1028
  store i32 104, i32* %cleanup.dest.slot, align 4
  %951 = bitcast i32* %j1027 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %951) #3
  br label %for.end1092

for.body1033:                                     ; preds = %for.cond1028
  %952 = bitcast i8** %srcRow1034 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %952) #3
  %953 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane1035 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %953, i32 0, i32 4
  %954 = load i8*, i8** %srcPlane1035, align 4, !tbaa !24
  %955 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes1036 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %955, i32 0, i32 6
  %956 = load i32, i32* %srcOffsetBytes1036, align 4, !tbaa !25
  %957 = load i32, i32* %j1027, align 4, !tbaa !12
  %958 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes1037 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %958, i32 0, i32 5
  %959 = load i32, i32* %srcRowBytes1037, align 4, !tbaa !26
  %mul1038 = mul i32 %957, %959
  %add1039 = add i32 %956, %mul1038
  %arrayidx1040 = getelementptr inbounds i8, i8* %954, i32 %add1039
  store i8* %arrayidx1040, i8** %srcRow1034, align 4, !tbaa !2
  %960 = bitcast i8** %dstRow1041 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %960) #3
  %961 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane1042 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %961, i32 0, i32 10
  %962 = load i8*, i8** %dstPlane1042, align 4, !tbaa !14
  %963 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes1043 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %963, i32 0, i32 12
  %964 = load i32, i32* %dstOffsetBytes1043, align 4, !tbaa !15
  %965 = load i32, i32* %j1027, align 4, !tbaa !12
  %966 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes1044 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %966, i32 0, i32 11
  %967 = load i32, i32* %dstRowBytes1044, align 4, !tbaa !16
  %mul1045 = mul i32 %965, %967
  %add1046 = add i32 %964, %mul1045
  %arrayidx1047 = getelementptr inbounds i8, i8* %962, i32 %add1046
  store i8* %arrayidx1047, i8** %dstRow1041, align 4, !tbaa !2
  %968 = bitcast i32* %i1048 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %968) #3
  store i32 0, i32* %i1048, align 4, !tbaa !12
  br label %for.cond1049

for.cond1049:                                     ; preds = %for.inc1087, %for.body1033
  %969 = load i32, i32* %i1048, align 4, !tbaa !12
  %970 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width1050 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %970, i32 0, i32 0
  %971 = load i32, i32* %width1050, align 4, !tbaa !17
  %cmp1051 = icmp ult i32 %969, %971
  br i1 %cmp1051, label %for.body1054, label %for.cond.cleanup1053

for.cond.cleanup1053:                             ; preds = %for.cond1049
  store i32 107, i32* %cleanup.dest.slot, align 4
  %972 = bitcast i32* %i1048 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %972) #3
  br label %for.end1089

for.body1054:                                     ; preds = %for.cond1049
  %973 = bitcast i32* %srcAlpha1055 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %973) #3
  %974 = load i8*, i8** %srcRow1034, align 4, !tbaa !2
  %975 = load i32, i32* %i1048, align 4, !tbaa !12
  %976 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes1056 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %976, i32 0, i32 7
  %977 = load i32, i32* %srcPixelBytes1056, align 4, !tbaa !27
  %mul1057 = mul i32 %975, %977
  %arrayidx1058 = getelementptr inbounds i8, i8* %974, i32 %mul1057
  %978 = bitcast i8* %arrayidx1058 to i16*
  %979 = load i16, i16* %978, align 2, !tbaa !10
  %conv1059 = zext i16 %979 to i32
  store i32 %conv1059, i32* %srcAlpha1055, align 4, !tbaa !12
  %980 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth1060 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %980, i32 0, i32 2
  %981 = load i32, i32* %srcDepth1060, align 4, !tbaa !20
  %982 = load i32, i32* %srcAlpha1055, align 4, !tbaa !12
  %call1061 = call i32 @avifLimitedToFullY(i32 %981, i32 %982)
  store i32 %call1061, i32* %srcAlpha1055, align 4, !tbaa !12
  %983 = bitcast float* %alphaF1062 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %983) #3
  %984 = load i32, i32* %srcAlpha1055, align 4, !tbaa !12
  %conv1063 = sitofp i32 %984 to float
  %985 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div1064 = fdiv float %conv1063, %985
  store float %div1064, float* %alphaF1062, align 4, !tbaa !21
  %986 = bitcast i32* %dstAlpha1065 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %986) #3
  %987 = load float, float* %alphaF1062, align 4, !tbaa !21
  %988 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul1066 = fmul float %987, %988
  %add1067 = fadd float 5.000000e-01, %mul1066
  %conv1068 = fptosi float %add1067 to i32
  store i32 %conv1068, i32* %dstAlpha1065, align 4, !tbaa !12
  %989 = load i32, i32* %dstAlpha1065, align 4, !tbaa !12
  %cmp1069 = icmp slt i32 %989, 0
  br i1 %cmp1069, label %cond.true1071, label %cond.false1072

cond.true1071:                                    ; preds = %for.body1054
  br label %cond.end1079

cond.false1072:                                   ; preds = %for.body1054
  %990 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %991 = load i32, i32* %dstAlpha1065, align 4, !tbaa !12
  %cmp1073 = icmp slt i32 %990, %991
  br i1 %cmp1073, label %cond.true1075, label %cond.false1076

cond.true1075:                                    ; preds = %cond.false1072
  %992 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end1077

cond.false1076:                                   ; preds = %cond.false1072
  %993 = load i32, i32* %dstAlpha1065, align 4, !tbaa !12
  br label %cond.end1077

cond.end1077:                                     ; preds = %cond.false1076, %cond.true1075
  %cond1078 = phi i32 [ %992, %cond.true1075 ], [ %993, %cond.false1076 ]
  br label %cond.end1079

cond.end1079:                                     ; preds = %cond.end1077, %cond.true1071
  %cond1080 = phi i32 [ 0, %cond.true1071 ], [ %cond1078, %cond.end1077 ]
  store i32 %cond1080, i32* %dstAlpha1065, align 4, !tbaa !12
  %994 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth1081 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %994, i32 0, i32 8
  %995 = load i32, i32* %dstDepth1081, align 4, !tbaa !6
  %996 = load i32, i32* %dstAlpha1065, align 4, !tbaa !12
  %call1082 = call i32 @avifFullToLimitedY(i32 %995, i32 %996)
  store i32 %call1082, i32* %dstAlpha1065, align 4, !tbaa !12
  %997 = load i32, i32* %dstAlpha1065, align 4, !tbaa !12
  %conv1083 = trunc i32 %997 to i16
  %998 = load i8*, i8** %dstRow1041, align 4, !tbaa !2
  %999 = load i32, i32* %i1048, align 4, !tbaa !12
  %1000 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes1084 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1000, i32 0, i32 13
  %1001 = load i32, i32* %dstPixelBytes1084, align 4, !tbaa !18
  %mul1085 = mul i32 %999, %1001
  %arrayidx1086 = getelementptr inbounds i8, i8* %998, i32 %mul1085
  %1002 = bitcast i8* %arrayidx1086 to i16*
  store i16 %conv1083, i16* %1002, align 2, !tbaa !10
  %1003 = bitcast i32* %dstAlpha1065 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1003) #3
  %1004 = bitcast float* %alphaF1062 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1004) #3
  %1005 = bitcast i32* %srcAlpha1055 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1005) #3
  br label %for.inc1087

for.inc1087:                                      ; preds = %cond.end1079
  %1006 = load i32, i32* %i1048, align 4, !tbaa !12
  %inc1088 = add i32 %1006, 1
  store i32 %inc1088, i32* %i1048, align 4, !tbaa !12
  br label %for.cond1049

for.end1089:                                      ; preds = %for.cond.cleanup1053
  %1007 = bitcast i8** %dstRow1041 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1007) #3
  %1008 = bitcast i8** %srcRow1034 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1008) #3
  br label %for.inc1090

for.inc1090:                                      ; preds = %for.end1089
  %1009 = load i32, i32* %j1027, align 4, !tbaa !12
  %inc1091 = add i32 %1009, 1
  store i32 %inc1091, i32* %j1027, align 4, !tbaa !12
  br label %for.cond1028

for.end1092:                                      ; preds = %for.cond.cleanup1032
  br label %if.end1160

if.else1093:                                      ; preds = %if.then1022
  %1010 = bitcast i32* %j1094 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1010) #3
  store i32 0, i32* %j1094, align 4, !tbaa !12
  br label %for.cond1095

for.cond1095:                                     ; preds = %for.inc1157, %if.else1093
  %1011 = load i32, i32* %j1094, align 4, !tbaa !12
  %1012 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height1096 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1012, i32 0, i32 1
  %1013 = load i32, i32* %height1096, align 4, !tbaa !13
  %cmp1097 = icmp ult i32 %1011, %1013
  br i1 %cmp1097, label %for.body1100, label %for.cond.cleanup1099

for.cond.cleanup1099:                             ; preds = %for.cond1095
  store i32 110, i32* %cleanup.dest.slot, align 4
  %1014 = bitcast i32* %j1094 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1014) #3
  br label %for.end1159

for.body1100:                                     ; preds = %for.cond1095
  %1015 = bitcast i8** %srcRow1101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1015) #3
  %1016 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane1102 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1016, i32 0, i32 4
  %1017 = load i8*, i8** %srcPlane1102, align 4, !tbaa !24
  %1018 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes1103 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1018, i32 0, i32 6
  %1019 = load i32, i32* %srcOffsetBytes1103, align 4, !tbaa !25
  %1020 = load i32, i32* %j1094, align 4, !tbaa !12
  %1021 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes1104 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1021, i32 0, i32 5
  %1022 = load i32, i32* %srcRowBytes1104, align 4, !tbaa !26
  %mul1105 = mul i32 %1020, %1022
  %add1106 = add i32 %1019, %mul1105
  %arrayidx1107 = getelementptr inbounds i8, i8* %1017, i32 %add1106
  store i8* %arrayidx1107, i8** %srcRow1101, align 4, !tbaa !2
  %1023 = bitcast i8** %dstRow1108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1023) #3
  %1024 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane1109 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1024, i32 0, i32 10
  %1025 = load i8*, i8** %dstPlane1109, align 4, !tbaa !14
  %1026 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes1110 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1026, i32 0, i32 12
  %1027 = load i32, i32* %dstOffsetBytes1110, align 4, !tbaa !15
  %1028 = load i32, i32* %j1094, align 4, !tbaa !12
  %1029 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes1111 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1029, i32 0, i32 11
  %1030 = load i32, i32* %dstRowBytes1111, align 4, !tbaa !16
  %mul1112 = mul i32 %1028, %1030
  %add1113 = add i32 %1027, %mul1112
  %arrayidx1114 = getelementptr inbounds i8, i8* %1025, i32 %add1113
  store i8* %arrayidx1114, i8** %dstRow1108, align 4, !tbaa !2
  %1031 = bitcast i32* %i1115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1031) #3
  store i32 0, i32* %i1115, align 4, !tbaa !12
  br label %for.cond1116

for.cond1116:                                     ; preds = %for.inc1154, %for.body1100
  %1032 = load i32, i32* %i1115, align 4, !tbaa !12
  %1033 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width1117 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1033, i32 0, i32 0
  %1034 = load i32, i32* %width1117, align 4, !tbaa !17
  %cmp1118 = icmp ult i32 %1032, %1034
  br i1 %cmp1118, label %for.body1121, label %for.cond.cleanup1120

for.cond.cleanup1120:                             ; preds = %for.cond1116
  store i32 113, i32* %cleanup.dest.slot, align 4
  %1035 = bitcast i32* %i1115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1035) #3
  br label %for.end1156

for.body1121:                                     ; preds = %for.cond1116
  %1036 = bitcast i32* %srcAlpha1122 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1036) #3
  %1037 = load i8*, i8** %srcRow1101, align 4, !tbaa !2
  %1038 = load i32, i32* %i1115, align 4, !tbaa !12
  %1039 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes1123 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1039, i32 0, i32 7
  %1040 = load i32, i32* %srcPixelBytes1123, align 4, !tbaa !27
  %mul1124 = mul i32 %1038, %1040
  %arrayidx1125 = getelementptr inbounds i8, i8* %1037, i32 %mul1124
  %1041 = bitcast i8* %arrayidx1125 to i16*
  %1042 = load i16, i16* %1041, align 2, !tbaa !10
  %conv1126 = zext i16 %1042 to i32
  store i32 %conv1126, i32* %srcAlpha1122, align 4, !tbaa !12
  %1043 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth1127 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1043, i32 0, i32 2
  %1044 = load i32, i32* %srcDepth1127, align 4, !tbaa !20
  %1045 = load i32, i32* %srcAlpha1122, align 4, !tbaa !12
  %call1128 = call i32 @avifLimitedToFullY(i32 %1044, i32 %1045)
  store i32 %call1128, i32* %srcAlpha1122, align 4, !tbaa !12
  %1046 = bitcast float* %alphaF1129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1046) #3
  %1047 = load i32, i32* %srcAlpha1122, align 4, !tbaa !12
  %conv1130 = sitofp i32 %1047 to float
  %1048 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div1131 = fdiv float %conv1130, %1048
  store float %div1131, float* %alphaF1129, align 4, !tbaa !21
  %1049 = bitcast i32* %dstAlpha1132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1049) #3
  %1050 = load float, float* %alphaF1129, align 4, !tbaa !21
  %1051 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul1133 = fmul float %1050, %1051
  %add1134 = fadd float 5.000000e-01, %mul1133
  %conv1135 = fptosi float %add1134 to i32
  store i32 %conv1135, i32* %dstAlpha1132, align 4, !tbaa !12
  %1052 = load i32, i32* %dstAlpha1132, align 4, !tbaa !12
  %cmp1136 = icmp slt i32 %1052, 0
  br i1 %cmp1136, label %cond.true1138, label %cond.false1139

cond.true1138:                                    ; preds = %for.body1121
  br label %cond.end1146

cond.false1139:                                   ; preds = %for.body1121
  %1053 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %1054 = load i32, i32* %dstAlpha1132, align 4, !tbaa !12
  %cmp1140 = icmp slt i32 %1053, %1054
  br i1 %cmp1140, label %cond.true1142, label %cond.false1143

cond.true1142:                                    ; preds = %cond.false1139
  %1055 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end1144

cond.false1143:                                   ; preds = %cond.false1139
  %1056 = load i32, i32* %dstAlpha1132, align 4, !tbaa !12
  br label %cond.end1144

cond.end1144:                                     ; preds = %cond.false1143, %cond.true1142
  %cond1145 = phi i32 [ %1055, %cond.true1142 ], [ %1056, %cond.false1143 ]
  br label %cond.end1146

cond.end1146:                                     ; preds = %cond.end1144, %cond.true1138
  %cond1147 = phi i32 [ 0, %cond.true1138 ], [ %cond1145, %cond.end1144 ]
  store i32 %cond1147, i32* %dstAlpha1132, align 4, !tbaa !12
  %1057 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth1148 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1057, i32 0, i32 8
  %1058 = load i32, i32* %dstDepth1148, align 4, !tbaa !6
  %1059 = load i32, i32* %dstAlpha1132, align 4, !tbaa !12
  %call1149 = call i32 @avifFullToLimitedY(i32 %1058, i32 %1059)
  store i32 %call1149, i32* %dstAlpha1132, align 4, !tbaa !12
  %1060 = load i32, i32* %dstAlpha1132, align 4, !tbaa !12
  %conv1150 = trunc i32 %1060 to i8
  %1061 = load i8*, i8** %dstRow1108, align 4, !tbaa !2
  %1062 = load i32, i32* %i1115, align 4, !tbaa !12
  %1063 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes1151 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1063, i32 0, i32 13
  %1064 = load i32, i32* %dstPixelBytes1151, align 4, !tbaa !18
  %mul1152 = mul i32 %1062, %1064
  %arrayidx1153 = getelementptr inbounds i8, i8* %1061, i32 %mul1152
  store i8 %conv1150, i8* %arrayidx1153, align 1, !tbaa !19
  %1065 = bitcast i32* %dstAlpha1132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1065) #3
  %1066 = bitcast float* %alphaF1129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1066) #3
  %1067 = bitcast i32* %srcAlpha1122 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1067) #3
  br label %for.inc1154

for.inc1154:                                      ; preds = %cond.end1146
  %1068 = load i32, i32* %i1115, align 4, !tbaa !12
  %inc1155 = add i32 %1068, 1
  store i32 %inc1155, i32* %i1115, align 4, !tbaa !12
  br label %for.cond1116

for.end1156:                                      ; preds = %for.cond.cleanup1120
  %1069 = bitcast i8** %dstRow1108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1069) #3
  %1070 = bitcast i8** %srcRow1101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1070) #3
  br label %for.inc1157

for.inc1157:                                      ; preds = %for.end1156
  %1071 = load i32, i32* %j1094, align 4, !tbaa !12
  %inc1158 = add i32 %1071, 1
  store i32 %inc1158, i32* %j1094, align 4, !tbaa !12
  br label %for.cond1095

for.end1159:                                      ; preds = %for.cond.cleanup1099
  br label %if.end1160

if.end1160:                                       ; preds = %for.end1159, %for.end1092
  br label %if.end1228

if.else1161:                                      ; preds = %if.then1018
  %1072 = bitcast i32* %j1162 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1072) #3
  store i32 0, i32* %j1162, align 4, !tbaa !12
  br label %for.cond1163

for.cond1163:                                     ; preds = %for.inc1225, %if.else1161
  %1073 = load i32, i32* %j1162, align 4, !tbaa !12
  %1074 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %height1164 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1074, i32 0, i32 1
  %1075 = load i32, i32* %height1164, align 4, !tbaa !13
  %cmp1165 = icmp ult i32 %1073, %1075
  br i1 %cmp1165, label %for.body1168, label %for.cond.cleanup1167

for.cond.cleanup1167:                             ; preds = %for.cond1163
  store i32 116, i32* %cleanup.dest.slot, align 4
  %1076 = bitcast i32* %j1162 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1076) #3
  br label %for.end1227

for.body1168:                                     ; preds = %for.cond1163
  %1077 = bitcast i8** %srcRow1169 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1077) #3
  %1078 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPlane1170 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1078, i32 0, i32 4
  %1079 = load i8*, i8** %srcPlane1170, align 4, !tbaa !24
  %1080 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcOffsetBytes1171 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1080, i32 0, i32 6
  %1081 = load i32, i32* %srcOffsetBytes1171, align 4, !tbaa !25
  %1082 = load i32, i32* %j1162, align 4, !tbaa !12
  %1083 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcRowBytes1172 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1083, i32 0, i32 5
  %1084 = load i32, i32* %srcRowBytes1172, align 4, !tbaa !26
  %mul1173 = mul i32 %1082, %1084
  %add1174 = add i32 %1081, %mul1173
  %arrayidx1175 = getelementptr inbounds i8, i8* %1079, i32 %add1174
  store i8* %arrayidx1175, i8** %srcRow1169, align 4, !tbaa !2
  %1085 = bitcast i8** %dstRow1176 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1085) #3
  %1086 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPlane1177 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1086, i32 0, i32 10
  %1087 = load i8*, i8** %dstPlane1177, align 4, !tbaa !14
  %1088 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstOffsetBytes1178 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1088, i32 0, i32 12
  %1089 = load i32, i32* %dstOffsetBytes1178, align 4, !tbaa !15
  %1090 = load i32, i32* %j1162, align 4, !tbaa !12
  %1091 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstRowBytes1179 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1091, i32 0, i32 11
  %1092 = load i32, i32* %dstRowBytes1179, align 4, !tbaa !16
  %mul1180 = mul i32 %1090, %1092
  %add1181 = add i32 %1089, %mul1180
  %arrayidx1182 = getelementptr inbounds i8, i8* %1087, i32 %add1181
  store i8* %arrayidx1182, i8** %dstRow1176, align 4, !tbaa !2
  %1093 = bitcast i32* %i1183 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1093) #3
  store i32 0, i32* %i1183, align 4, !tbaa !12
  br label %for.cond1184

for.cond1184:                                     ; preds = %for.inc1222, %for.body1168
  %1094 = load i32, i32* %i1183, align 4, !tbaa !12
  %1095 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %width1185 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1095, i32 0, i32 0
  %1096 = load i32, i32* %width1185, align 4, !tbaa !17
  %cmp1186 = icmp ult i32 %1094, %1096
  br i1 %cmp1186, label %for.body1189, label %for.cond.cleanup1188

for.cond.cleanup1188:                             ; preds = %for.cond1184
  store i32 119, i32* %cleanup.dest.slot, align 4
  %1097 = bitcast i32* %i1183 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1097) #3
  br label %for.end1224

for.body1189:                                     ; preds = %for.cond1184
  %1098 = bitcast i32* %srcAlpha1190 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1098) #3
  %1099 = load i8*, i8** %srcRow1169, align 4, !tbaa !2
  %1100 = load i32, i32* %i1183, align 4, !tbaa !12
  %1101 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcPixelBytes1191 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1101, i32 0, i32 7
  %1102 = load i32, i32* %srcPixelBytes1191, align 4, !tbaa !27
  %mul1192 = mul i32 %1100, %1102
  %arrayidx1193 = getelementptr inbounds i8, i8* %1099, i32 %mul1192
  %1103 = load i8, i8* %arrayidx1193, align 1, !tbaa !19
  %conv1194 = zext i8 %1103 to i32
  store i32 %conv1194, i32* %srcAlpha1190, align 4, !tbaa !12
  %1104 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %srcDepth1195 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1104, i32 0, i32 2
  %1105 = load i32, i32* %srcDepth1195, align 4, !tbaa !20
  %1106 = load i32, i32* %srcAlpha1190, align 4, !tbaa !12
  %call1196 = call i32 @avifLimitedToFullY(i32 %1105, i32 %1106)
  store i32 %call1196, i32* %srcAlpha1190, align 4, !tbaa !12
  %1107 = bitcast float* %alphaF1197 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1107) #3
  %1108 = load i32, i32* %srcAlpha1190, align 4, !tbaa !12
  %conv1198 = sitofp i32 %1108 to float
  %1109 = load float, float* %srcMaxChannelF, align 4, !tbaa !21
  %div1199 = fdiv float %conv1198, %1109
  store float %div1199, float* %alphaF1197, align 4, !tbaa !21
  %1110 = bitcast i32* %dstAlpha1200 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1110) #3
  %1111 = load float, float* %alphaF1197, align 4, !tbaa !21
  %1112 = load float, float* %dstMaxChannelF, align 4, !tbaa !21
  %mul1201 = fmul float %1111, %1112
  %add1202 = fadd float 5.000000e-01, %mul1201
  %conv1203 = fptosi float %add1202 to i32
  store i32 %conv1203, i32* %dstAlpha1200, align 4, !tbaa !12
  %1113 = load i32, i32* %dstAlpha1200, align 4, !tbaa !12
  %cmp1204 = icmp slt i32 %1113, 0
  br i1 %cmp1204, label %cond.true1206, label %cond.false1207

cond.true1206:                                    ; preds = %for.body1189
  br label %cond.end1214

cond.false1207:                                   ; preds = %for.body1189
  %1114 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  %1115 = load i32, i32* %dstAlpha1200, align 4, !tbaa !12
  %cmp1208 = icmp slt i32 %1114, %1115
  br i1 %cmp1208, label %cond.true1210, label %cond.false1211

cond.true1210:                                    ; preds = %cond.false1207
  %1116 = load i32, i32* %dstMaxChannel, align 4, !tbaa !12
  br label %cond.end1212

cond.false1211:                                   ; preds = %cond.false1207
  %1117 = load i32, i32* %dstAlpha1200, align 4, !tbaa !12
  br label %cond.end1212

cond.end1212:                                     ; preds = %cond.false1211, %cond.true1210
  %cond1213 = phi i32 [ %1116, %cond.true1210 ], [ %1117, %cond.false1211 ]
  br label %cond.end1214

cond.end1214:                                     ; preds = %cond.end1212, %cond.true1206
  %cond1215 = phi i32 [ 0, %cond.true1206 ], [ %cond1213, %cond.end1212 ]
  store i32 %cond1215, i32* %dstAlpha1200, align 4, !tbaa !12
  %1118 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstDepth1216 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1118, i32 0, i32 8
  %1119 = load i32, i32* %dstDepth1216, align 4, !tbaa !6
  %1120 = load i32, i32* %dstAlpha1200, align 4, !tbaa !12
  %call1217 = call i32 @avifFullToLimitedY(i32 %1119, i32 %1120)
  store i32 %call1217, i32* %dstAlpha1200, align 4, !tbaa !12
  %1121 = load i32, i32* %dstAlpha1200, align 4, !tbaa !12
  %conv1218 = trunc i32 %1121 to i16
  %1122 = load i8*, i8** %dstRow1176, align 4, !tbaa !2
  %1123 = load i32, i32* %i1183, align 4, !tbaa !12
  %1124 = load %struct.avifAlphaParams*, %struct.avifAlphaParams** %params.addr, align 4, !tbaa !2
  %dstPixelBytes1219 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %1124, i32 0, i32 13
  %1125 = load i32, i32* %dstPixelBytes1219, align 4, !tbaa !18
  %mul1220 = mul i32 %1123, %1125
  %arrayidx1221 = getelementptr inbounds i8, i8* %1122, i32 %mul1220
  %1126 = bitcast i8* %arrayidx1221 to i16*
  store i16 %conv1218, i16* %1126, align 2, !tbaa !10
  %1127 = bitcast i32* %dstAlpha1200 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1127) #3
  %1128 = bitcast float* %alphaF1197 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1128) #3
  %1129 = bitcast i32* %srcAlpha1190 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1129) #3
  br label %for.inc1222

for.inc1222:                                      ; preds = %cond.end1214
  %1130 = load i32, i32* %i1183, align 4, !tbaa !12
  %inc1223 = add i32 %1130, 1
  store i32 %inc1223, i32* %i1183, align 4, !tbaa !12
  br label %for.cond1184

for.end1224:                                      ; preds = %for.cond.cleanup1188
  %1131 = bitcast i8** %dstRow1176 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1131) #3
  %1132 = bitcast i8** %srcRow1169 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1132) #3
  br label %for.inc1225

for.inc1225:                                      ; preds = %for.end1224
  %1133 = load i32, i32* %j1162, align 4, !tbaa !12
  %inc1226 = add i32 %1133, 1
  store i32 %inc1226, i32* %j1162, align 4, !tbaa !12
  br label %for.cond1163

for.end1227:                                      ; preds = %for.cond.cleanup1167
  br label %if.end1228

if.end1228:                                       ; preds = %for.end1227, %if.end1160
  br label %if.end1229

if.end1229:                                       ; preds = %if.end1228, %land.lhs.true1014, %if.else1010
  br label %if.end1230

if.end1230:                                       ; preds = %if.end1229, %if.end1009
  br label %if.end1231

if.end1231:                                       ; preds = %if.end1230, %if.end796
  br label %if.end1232

if.end1232:                                       ; preds = %if.end1231, %if.end583
  br label %if.end1233

if.end1233:                                       ; preds = %if.end1232, %if.end382
  store i32 1, i32* %cleanup.dest.slot, align 4
  %1134 = bitcast float* %dstMaxChannelF to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1134) #3
  %1135 = bitcast float* %srcMaxChannelF to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1135) #3
  %1136 = bitcast i32* %dstMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1136) #3
  %1137 = bitcast i32* %srcMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1137) #3
  ret i32 1
}

declare i32 @avifLimitedToFullY(i32, i32) #2

declare i32 @avifFullToLimitedY(i32, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 32}
!7 = !{!"avifAlphaParams", !8, i64 0, !8, i64 4, !8, i64 8, !4, i64 12, !3, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !4, i64 36, !3, i64 40, !8, i64 44, !8, i64 48, !8, i64 52}
!8 = !{!"int", !4, i64 0}
!9 = !{!7, !4, i64 36}
!10 = !{!11, !11, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!8, !8, i64 0}
!13 = !{!7, !8, i64 4}
!14 = !{!7, !3, i64 40}
!15 = !{!7, !8, i64 48}
!16 = !{!7, !8, i64 44}
!17 = !{!7, !8, i64 0}
!18 = !{!7, !8, i64 52}
!19 = !{!4, !4, i64 0}
!20 = !{!7, !8, i64 8}
!21 = !{!22, !22, i64 0}
!22 = !{!"float", !4, i64 0}
!23 = !{!7, !4, i64 12}
!24 = !{!7, !3, i64 16}
!25 = !{!7, !8, i64 24}
!26 = !{!7, !8, i64 20}
!27 = !{!7, !8, i64 28}
