; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/scale.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/scale.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.mv32 = type { i32, i32 }
%struct.mv = type { i16, i16 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }

; Function Attrs: nounwind
define hidden void @av1_scale_mv(%struct.mv32* noalias sret align 4 %agg.result, %struct.mv* %mvq4, i32 %x, i32 %y, %struct.scale_factors* %sf) #0 {
entry:
  %mvq4.addr = alloca %struct.mv*, align 4
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %x_off_q4 = alloca i32, align 4
  %y_off_q4 = alloca i32, align 4
  store %struct.mv* %mvq4, %struct.mv** %mvq4.addr, align 4, !tbaa !2
  store i32 %x, i32* %x.addr, align 4, !tbaa !6
  store i32 %y, i32* %y.addr, align 4, !tbaa !6
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %x_off_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %x.addr, align 4, !tbaa !6
  %shl = shl i32 %1, 4
  %2 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call = call i32 @scaled_x(i32 %shl, %struct.scale_factors* %2)
  store i32 %call, i32* %x_off_q4, align 4, !tbaa !6
  %3 = bitcast i32* %y_off_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %y.addr, align 4, !tbaa !6
  %shl1 = shl i32 %4, 4
  %5 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call2 = call i32 @scaled_y(i32 %shl1, %struct.scale_factors* %5)
  store i32 %call2, i32* %y_off_q4, align 4, !tbaa !6
  %row = getelementptr inbounds %struct.mv32, %struct.mv32* %agg.result, i32 0, i32 0
  %6 = load i32, i32* %y.addr, align 4, !tbaa !6
  %shl3 = shl i32 %6, 4
  %7 = load %struct.mv*, %struct.mv** %mvq4.addr, align 4, !tbaa !2
  %row4 = getelementptr inbounds %struct.mv, %struct.mv* %7, i32 0, i32 0
  %8 = load i16, i16* %row4, align 2, !tbaa !8
  %conv = sext i16 %8 to i32
  %add = add nsw i32 %shl3, %conv
  %9 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call5 = call i32 @scaled_y(i32 %add, %struct.scale_factors* %9)
  %10 = load i32, i32* %y_off_q4, align 4, !tbaa !6
  %sub = sub nsw i32 %call5, %10
  store i32 %sub, i32* %row, align 4, !tbaa !11
  %col = getelementptr inbounds %struct.mv32, %struct.mv32* %agg.result, i32 0, i32 1
  %11 = load i32, i32* %x.addr, align 4, !tbaa !6
  %shl6 = shl i32 %11, 4
  %12 = load %struct.mv*, %struct.mv** %mvq4.addr, align 4, !tbaa !2
  %col7 = getelementptr inbounds %struct.mv, %struct.mv* %12, i32 0, i32 1
  %13 = load i16, i16* %col7, align 2, !tbaa !13
  %conv8 = sext i16 %13 to i32
  %add9 = add nsw i32 %shl6, %conv8
  %14 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call10 = call i32 @scaled_x(i32 %add9, %struct.scale_factors* %14)
  %15 = load i32, i32* %x_off_q4, align 4, !tbaa !6
  %sub11 = sub nsw i32 %call10, %15
  store i32 %sub11, i32* %col, align 4, !tbaa !14
  %16 = bitcast i32* %y_off_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i32* %x_off_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @scaled_x(i32 %val, %struct.scale_factors* %sf) #2 {
entry:
  %val.addr = alloca i32, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %off = alloca i32, align 4
  %tval = alloca i64, align 8
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %1, i32 0, i32 0
  %2 = load i32, i32* %x_scale_fp, align 4, !tbaa !15
  %sub = sub nsw i32 %2, 16384
  %mul = mul nsw i32 %sub, 8
  store i32 %mul, i32* %off, align 4, !tbaa !6
  %3 = bitcast i64* %tval to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #4
  %4 = load i32, i32* %val.addr, align 4, !tbaa !6
  %conv = sext i32 %4 to i64
  %5 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_scale_fp1 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %5, i32 0, i32 0
  %6 = load i32, i32* %x_scale_fp1, align 4, !tbaa !15
  %conv2 = sext i32 %6 to i64
  %mul3 = mul nsw i64 %conv, %conv2
  %7 = load i32, i32* %off, align 4, !tbaa !6
  %conv4 = sext i32 %7 to i64
  %add = add nsw i64 %mul3, %conv4
  store i64 %add, i64* %tval, align 8, !tbaa !17
  %8 = load i64, i64* %tval, align 8, !tbaa !17
  %cmp = icmp slt i64 %8, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %9 = load i64, i64* %tval, align 8, !tbaa !17
  %sub6 = sub nsw i64 0, %9
  %add7 = add nsw i64 %sub6, 128
  %shr = ashr i64 %add7, 8
  %sub8 = sub nsw i64 0, %shr
  br label %cond.end

cond.false:                                       ; preds = %entry
  %10 = load i64, i64* %tval, align 8, !tbaa !17
  %add9 = add nsw i64 %10, 128
  %shr10 = ashr i64 %add9, 8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i64 [ %sub8, %cond.true ], [ %shr10, %cond.false ]
  %conv11 = trunc i64 %cond to i32
  %11 = bitcast i64* %tval to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #4
  %12 = bitcast i32* %off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  ret i32 %conv11
}

; Function Attrs: inlinehint nounwind
define internal i32 @scaled_y(i32 %val, %struct.scale_factors* %sf) #2 {
entry:
  %val.addr = alloca i32, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %off = alloca i32, align 4
  %tval = alloca i64, align 8
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %1, i32 0, i32 1
  %2 = load i32, i32* %y_scale_fp, align 4, !tbaa !19
  %sub = sub nsw i32 %2, 16384
  %mul = mul nsw i32 %sub, 8
  store i32 %mul, i32* %off, align 4, !tbaa !6
  %3 = bitcast i64* %tval to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #4
  %4 = load i32, i32* %val.addr, align 4, !tbaa !6
  %conv = sext i32 %4 to i64
  %5 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_scale_fp1 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %5, i32 0, i32 1
  %6 = load i32, i32* %y_scale_fp1, align 4, !tbaa !19
  %conv2 = sext i32 %6 to i64
  %mul3 = mul nsw i64 %conv, %conv2
  %7 = load i32, i32* %off, align 4, !tbaa !6
  %conv4 = sext i32 %7 to i64
  %add = add nsw i64 %mul3, %conv4
  store i64 %add, i64* %tval, align 8, !tbaa !17
  %8 = load i64, i64* %tval, align 8, !tbaa !17
  %cmp = icmp slt i64 %8, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %9 = load i64, i64* %tval, align 8, !tbaa !17
  %sub6 = sub nsw i64 0, %9
  %add7 = add nsw i64 %sub6, 128
  %shr = ashr i64 %add7, 8
  %sub8 = sub nsw i64 0, %shr
  br label %cond.end

cond.false:                                       ; preds = %entry
  %10 = load i64, i64* %tval, align 8, !tbaa !17
  %add9 = add nsw i64 %10, 128
  %shr10 = ashr i64 %add9, 8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i64 [ %sub8, %cond.true ], [ %shr10, %cond.false ]
  %conv11 = trunc i64 %cond to i32
  %11 = bitcast i64* %tval to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #4
  %12 = bitcast i32* %off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  ret i32 %conv11
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_setup_scale_factors_for_frame(%struct.scale_factors* %sf, i32 %other_w, i32 %other_h, i32 %this_w, i32 %this_h) #0 {
entry:
  %sf.addr = alloca %struct.scale_factors*, align 4
  %other_w.addr = alloca i32, align 4
  %other_h.addr = alloca i32, align 4
  %this_w.addr = alloca i32, align 4
  %this_h.addr = alloca i32, align 4
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  store i32 %other_w, i32* %other_w.addr, align 4, !tbaa !6
  store i32 %other_h, i32* %other_h.addr, align 4, !tbaa !6
  store i32 %this_w, i32* %this_w.addr, align 4, !tbaa !6
  store i32 %this_h, i32* %this_h.addr, align 4, !tbaa !6
  %0 = load i32, i32* %other_w.addr, align 4, !tbaa !6
  %1 = load i32, i32* %other_h.addr, align 4, !tbaa !6
  %2 = load i32, i32* %this_w.addr, align 4, !tbaa !6
  %3 = load i32, i32* %this_h.addr, align 4, !tbaa !6
  %call = call i32 @valid_ref_frame_size(i32 %0, i32 %1, i32 %2, i32 %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %4 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %4, i32 0, i32 0
  store i32 -1, i32* %x_scale_fp, align 4, !tbaa !15
  %5 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %5, i32 0, i32 1
  store i32 -1, i32* %y_scale_fp, align 4, !tbaa !19
  br label %return

if.end:                                           ; preds = %entry
  %6 = load i32, i32* %other_w.addr, align 4, !tbaa !6
  %7 = load i32, i32* %this_w.addr, align 4, !tbaa !6
  %call1 = call i32 @get_fixed_point_scale_factor(i32 %6, i32 %7)
  %8 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_scale_fp2 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %8, i32 0, i32 0
  store i32 %call1, i32* %x_scale_fp2, align 4, !tbaa !15
  %9 = load i32, i32* %other_h.addr, align 4, !tbaa !6
  %10 = load i32, i32* %this_h.addr, align 4, !tbaa !6
  %call3 = call i32 @get_fixed_point_scale_factor(i32 %9, i32 %10)
  %11 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_scale_fp4 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %11, i32 0, i32 1
  store i32 %call3, i32* %y_scale_fp4, align 4, !tbaa !19
  %12 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_scale_fp5 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %12, i32 0, i32 0
  %13 = load i32, i32* %x_scale_fp5, align 4, !tbaa !15
  %call6 = call i32 @fixed_point_scale_to_coarse_point_scale(i32 %13)
  %14 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_step_q4 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %14, i32 0, i32 2
  store i32 %call6, i32* %x_step_q4, align 4, !tbaa !20
  %15 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_scale_fp7 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %15, i32 0, i32 1
  %16 = load i32, i32* %y_scale_fp7, align 4, !tbaa !19
  %call8 = call i32 @fixed_point_scale_to_coarse_point_scale(i32 %16)
  %17 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_step_q4 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %17, i32 0, i32 3
  store i32 %call8, i32* %y_step_q4, align 4, !tbaa !21
  %18 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call9 = call i32 @av1_is_scaled(%struct.scale_factors* %18)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.end
  %19 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %scale_value_x = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %19, i32 0, i32 4
  store i32 (i32, %struct.scale_factors*)* @scaled_x, i32 (i32, %struct.scale_factors*)** %scale_value_x, align 4, !tbaa !22
  %20 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %scale_value_y = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %20, i32 0, i32 5
  store i32 (i32, %struct.scale_factors*)* @scaled_y, i32 (i32, %struct.scale_factors*)** %scale_value_y, align 4, !tbaa !23
  br label %if.end14

if.else:                                          ; preds = %if.end
  %21 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %scale_value_x12 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %21, i32 0, i32 4
  store i32 (i32, %struct.scale_factors*)* @unscaled_value, i32 (i32, %struct.scale_factors*)** %scale_value_x12, align 4, !tbaa !22
  %22 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %scale_value_y13 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %22, i32 0, i32 5
  store i32 (i32, %struct.scale_factors*)* @unscaled_value, i32 (i32, %struct.scale_factors*)** %scale_value_y13, align 4, !tbaa !23
  br label %if.end14

if.end14:                                         ; preds = %if.else, %if.then11
  %23 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %convolve = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %23, i32 0, i32 6
  %arrayidx = getelementptr inbounds [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]]* %convolve, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]], [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]* %arrayidx, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*], [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]* %arrayidx15, i32 0, i32 0
  store void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)* @av1_convolve_2d_copy_sr_c, void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)** %arrayidx16, align 4, !tbaa !2
  %24 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %convolve17 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %24, i32 0, i32 6
  %arrayidx18 = getelementptr inbounds [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]]* %convolve17, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]], [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]* %arrayidx18, i32 0, i32 1
  %arrayidx20 = getelementptr inbounds [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*], [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]* %arrayidx19, i32 0, i32 0
  store void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)* @av1_convolve_y_sr_c, void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)** %arrayidx20, align 4, !tbaa !2
  %25 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %convolve21 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %25, i32 0, i32 6
  %arrayidx22 = getelementptr inbounds [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]]* %convolve21, i32 0, i32 1
  %arrayidx23 = getelementptr inbounds [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]], [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]* %arrayidx22, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*], [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]* %arrayidx23, i32 0, i32 0
  store void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)* @av1_convolve_x_sr_c, void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)** %arrayidx24, align 4, !tbaa !2
  %26 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %convolve25 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %26, i32 0, i32 6
  %arrayidx26 = getelementptr inbounds [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]]* %convolve25, i32 0, i32 1
  %arrayidx27 = getelementptr inbounds [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]], [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]* %arrayidx26, i32 0, i32 1
  %arrayidx28 = getelementptr inbounds [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*], [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]* %arrayidx27, i32 0, i32 0
  store void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)* @av1_convolve_2d_sr_c, void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)** %arrayidx28, align 4, !tbaa !2
  %27 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %convolve29 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %27, i32 0, i32 6
  %arrayidx30 = getelementptr inbounds [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]]* %convolve29, i32 0, i32 0
  %arrayidx31 = getelementptr inbounds [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]], [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]* %arrayidx30, i32 0, i32 0
  %arrayidx32 = getelementptr inbounds [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*], [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]* %arrayidx31, i32 0, i32 1
  store void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)* @av1_dist_wtd_convolve_2d_copy_c, void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)** %arrayidx32, align 4, !tbaa !2
  %28 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %convolve33 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %28, i32 0, i32 6
  %arrayidx34 = getelementptr inbounds [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]]* %convolve33, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]], [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]* %arrayidx34, i32 0, i32 1
  %arrayidx36 = getelementptr inbounds [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*], [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]* %arrayidx35, i32 0, i32 1
  store void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)* @av1_dist_wtd_convolve_y_c, void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)** %arrayidx36, align 4, !tbaa !2
  %29 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %convolve37 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %29, i32 0, i32 6
  %arrayidx38 = getelementptr inbounds [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]]* %convolve37, i32 0, i32 1
  %arrayidx39 = getelementptr inbounds [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]], [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]* %arrayidx38, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*], [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]* %arrayidx39, i32 0, i32 1
  store void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)* @av1_dist_wtd_convolve_x_c, void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)** %arrayidx40, align 4, !tbaa !2
  %30 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %convolve41 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %30, i32 0, i32 6
  %arrayidx42 = getelementptr inbounds [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]]* %convolve41, i32 0, i32 1
  %arrayidx43 = getelementptr inbounds [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]], [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]* %arrayidx42, i32 0, i32 1
  %arrayidx44 = getelementptr inbounds [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*], [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]* %arrayidx43, i32 0, i32 1
  store void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)* @av1_dist_wtd_convolve_2d_c, void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)** %arrayidx44, align 4, !tbaa !2
  %31 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %highbd_convolve = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %31, i32 0, i32 7
  %arrayidx45 = getelementptr inbounds [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]]* %highbd_convolve, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]], [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]* %arrayidx45, i32 0, i32 0
  %arrayidx47 = getelementptr inbounds [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*], [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]* %arrayidx46, i32 0, i32 0
  store void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)* @av1_highbd_convolve_2d_copy_sr_c, void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)** %arrayidx47, align 4, !tbaa !2
  %32 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %highbd_convolve48 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %32, i32 0, i32 7
  %arrayidx49 = getelementptr inbounds [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]]* %highbd_convolve48, i32 0, i32 0
  %arrayidx50 = getelementptr inbounds [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]], [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]* %arrayidx49, i32 0, i32 1
  %arrayidx51 = getelementptr inbounds [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*], [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]* %arrayidx50, i32 0, i32 0
  store void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)* @av1_highbd_convolve_y_sr_c, void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)** %arrayidx51, align 4, !tbaa !2
  %33 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %highbd_convolve52 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %33, i32 0, i32 7
  %arrayidx53 = getelementptr inbounds [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]]* %highbd_convolve52, i32 0, i32 1
  %arrayidx54 = getelementptr inbounds [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]], [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]* %arrayidx53, i32 0, i32 0
  %arrayidx55 = getelementptr inbounds [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*], [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]* %arrayidx54, i32 0, i32 0
  store void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)* @av1_highbd_convolve_x_sr_c, void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)** %arrayidx55, align 4, !tbaa !2
  %34 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %highbd_convolve56 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %34, i32 0, i32 7
  %arrayidx57 = getelementptr inbounds [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]]* %highbd_convolve56, i32 0, i32 1
  %arrayidx58 = getelementptr inbounds [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]], [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]* %arrayidx57, i32 0, i32 1
  %arrayidx59 = getelementptr inbounds [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*], [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]* %arrayidx58, i32 0, i32 0
  store void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)* @av1_highbd_convolve_2d_sr_c, void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)** %arrayidx59, align 4, !tbaa !2
  %35 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %highbd_convolve60 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %35, i32 0, i32 7
  %arrayidx61 = getelementptr inbounds [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]]* %highbd_convolve60, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]], [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]* %arrayidx61, i32 0, i32 0
  %arrayidx63 = getelementptr inbounds [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*], [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]* %arrayidx62, i32 0, i32 1
  store void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)* @av1_highbd_dist_wtd_convolve_2d_copy_c, void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)** %arrayidx63, align 4, !tbaa !2
  %36 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %highbd_convolve64 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %36, i32 0, i32 7
  %arrayidx65 = getelementptr inbounds [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]]* %highbd_convolve64, i32 0, i32 0
  %arrayidx66 = getelementptr inbounds [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]], [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]* %arrayidx65, i32 0, i32 1
  %arrayidx67 = getelementptr inbounds [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*], [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]* %arrayidx66, i32 0, i32 1
  store void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)* @av1_highbd_dist_wtd_convolve_y_c, void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)** %arrayidx67, align 4, !tbaa !2
  %37 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %highbd_convolve68 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %37, i32 0, i32 7
  %arrayidx69 = getelementptr inbounds [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]]* %highbd_convolve68, i32 0, i32 1
  %arrayidx70 = getelementptr inbounds [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]], [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]* %arrayidx69, i32 0, i32 0
  %arrayidx71 = getelementptr inbounds [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*], [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]* %arrayidx70, i32 0, i32 1
  store void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)* @av1_highbd_dist_wtd_convolve_x_c, void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)** %arrayidx71, align 4, !tbaa !2
  %38 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %highbd_convolve72 = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %38, i32 0, i32 7
  %arrayidx73 = getelementptr inbounds [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]]* %highbd_convolve72, i32 0, i32 1
  %arrayidx74 = getelementptr inbounds [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]], [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]* %arrayidx73, i32 0, i32 1
  %arrayidx75 = getelementptr inbounds [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*], [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]* %arrayidx74, i32 0, i32 1
  store void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)* @av1_highbd_dist_wtd_convolve_2d_c, void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)** %arrayidx75, align 4, !tbaa !2
  br label %return

return:                                           ; preds = %if.end14, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @valid_ref_frame_size(i32 %ref_width, i32 %ref_height, i32 %this_width, i32 %this_height) #2 {
entry:
  %ref_width.addr = alloca i32, align 4
  %ref_height.addr = alloca i32, align 4
  %this_width.addr = alloca i32, align 4
  %this_height.addr = alloca i32, align 4
  store i32 %ref_width, i32* %ref_width.addr, align 4, !tbaa !6
  store i32 %ref_height, i32* %ref_height.addr, align 4, !tbaa !6
  store i32 %this_width, i32* %this_width.addr, align 4, !tbaa !6
  store i32 %this_height, i32* %this_height.addr, align 4, !tbaa !6
  %0 = load i32, i32* %this_width.addr, align 4, !tbaa !6
  %mul = mul nsw i32 2, %0
  %1 = load i32, i32* %ref_width.addr, align 4, !tbaa !6
  %cmp = icmp sge i32 %mul, %1
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %2 = load i32, i32* %this_height.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 2, %2
  %3 = load i32, i32* %ref_height.addr, align 4, !tbaa !6
  %cmp2 = icmp sge i32 %mul1, %3
  br i1 %cmp2, label %land.lhs.true3, label %land.end

land.lhs.true3:                                   ; preds = %land.lhs.true
  %4 = load i32, i32* %this_width.addr, align 4, !tbaa !6
  %5 = load i32, i32* %ref_width.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 16, %5
  %cmp5 = icmp sle i32 %4, %mul4
  br i1 %cmp5, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true3
  %6 = load i32, i32* %this_height.addr, align 4, !tbaa !6
  %7 = load i32, i32* %ref_height.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 16, %7
  %cmp7 = icmp sle i32 %6, %mul6
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true3, %land.lhs.true, %entry
  %8 = phi i1 [ false, %land.lhs.true3 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp7, %land.rhs ]
  %land.ext = zext i1 %8 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define internal i32 @get_fixed_point_scale_factor(i32 %other_size, i32 %this_size) #0 {
entry:
  %other_size.addr = alloca i32, align 4
  %this_size.addr = alloca i32, align 4
  store i32 %other_size, i32* %other_size.addr, align 4, !tbaa !6
  store i32 %this_size, i32* %this_size.addr, align 4, !tbaa !6
  %0 = load i32, i32* %other_size.addr, align 4, !tbaa !6
  %shl = shl i32 %0, 14
  %1 = load i32, i32* %this_size.addr, align 4, !tbaa !6
  %div = sdiv i32 %1, 2
  %add = add nsw i32 %shl, %div
  %2 = load i32, i32* %this_size.addr, align 4, !tbaa !6
  %div1 = sdiv i32 %add, %2
  ret i32 %div1
}

; Function Attrs: nounwind
define internal i32 @fixed_point_scale_to_coarse_point_scale(i32 %scale_fp) #0 {
entry:
  %scale_fp.addr = alloca i32, align 4
  store i32 %scale_fp, i32* %scale_fp.addr, align 4, !tbaa !6
  %0 = load i32, i32* %scale_fp.addr, align 4, !tbaa !6
  %add = add nsw i32 %0, 8
  %shr = ashr i32 %add, 4
  ret i32 %shr
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_scaled(%struct.scale_factors* %sf) #2 {
entry:
  %sf.addr = alloca %struct.scale_factors*, align 4
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call = call i32 @av1_is_valid_scale(%struct.scale_factors* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %1, i32 0, i32 0
  %2 = load i32, i32* %x_scale_fp, align 4, !tbaa !15
  %cmp = icmp ne i32 %2, 16384
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.rhs
  %3 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %3, i32 0, i32 1
  %4 = load i32, i32* %y_scale_fp, align 4, !tbaa !19
  %cmp1 = icmp ne i32 %4, 16384
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.rhs
  %5 = phi i1 [ true, %land.rhs ], [ %cmp1, %lor.rhs ]
  br label %land.end

land.end:                                         ; preds = %lor.end, %entry
  %6 = phi i1 [ false, %entry ], [ %5, %lor.end ]
  %land.ext = zext i1 %6 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define internal i32 @unscaled_value(i32 %val, %struct.scale_factors* %sf) #0 {
entry:
  %val.addr = alloca i32, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %1 = load i32, i32* %val.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 64
  ret i32 %mul
}

declare void @av1_convolve_2d_copy_sr_c(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*) #3

declare void @av1_convolve_y_sr_c(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*) #3

declare void @av1_convolve_x_sr_c(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*) #3

declare void @av1_convolve_2d_sr_c(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*) #3

declare void @av1_dist_wtd_convolve_2d_copy_c(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*) #3

declare void @av1_dist_wtd_convolve_y_c(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*) #3

declare void @av1_dist_wtd_convolve_x_c(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*) #3

declare void @av1_dist_wtd_convolve_2d_c(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*) #3

declare void @av1_highbd_convolve_2d_copy_sr_c(i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32) #3

declare void @av1_highbd_convolve_y_sr_c(i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32) #3

declare void @av1_highbd_convolve_x_sr_c(i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32) #3

declare void @av1_highbd_convolve_2d_sr_c(i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32) #3

declare void @av1_highbd_dist_wtd_convolve_2d_copy_c(i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32) #3

declare void @av1_highbd_dist_wtd_convolve_y_c(i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32) #3

declare void @av1_highbd_dist_wtd_convolve_x_c(i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32) #3

declare void @av1_highbd_dist_wtd_convolve_2d_c(i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32) #3

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_valid_scale(%struct.scale_factors* %sf) #2 {
entry:
  %sf.addr = alloca %struct.scale_factors*, align 4
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %0, i32 0, i32 0
  %1 = load i32, i32* %x_scale_fp, align 4, !tbaa !15
  %cmp = icmp ne i32 %1, -1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %2, i32 0, i32 1
  %3 = load i32, i32* %y_scale_fp, align 4, !tbaa !19
  %cmp1 = icmp ne i32 %3, -1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %4 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  %land.ext = zext i1 %4 to i32
  ret i32 %land.ext
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !10, i64 0}
!9 = !{!"mv", !10, i64 0, !10, i64 2}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !7, i64 0}
!12 = !{!"mv32", !7, i64 0, !7, i64 4}
!13 = !{!9, !10, i64 2}
!14 = !{!12, !7, i64 4}
!15 = !{!16, !7, i64 0}
!16 = !{!"scale_factors", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!17 = !{!18, !18, i64 0}
!18 = !{!"long long", !4, i64 0}
!19 = !{!16, !7, i64 4}
!20 = !{!16, !7, i64 8}
!21 = !{!16, !7, i64 12}
!22 = !{!16, !3, i64 16}
!23 = !{!16, !3, i64 20}
