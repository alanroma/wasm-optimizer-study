; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/alloccommon.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/alloccommon.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }

@.str = private unnamed_addr constant [34 x i8] c"Failed to allocate cm->rst_tmpbuf\00", align 1
@.str.1 = private unnamed_addr constant [28 x i8] c"Failed to allocate cm->rlbs\00", align 1
@.str.2 = private unnamed_addr constant [53 x i8] c"Failed to allocate boundaries->stripe_boundary_above\00", align 1
@.str.3 = private unnamed_addr constant [53 x i8] c"Failed to allocate boundaries->stripe_boundary_below\00", align 1
@mi_size_wide = internal constant [22 x i8] c"\01\01\02\02\02\04\04\04\08\08\08\10\10\10  \01\04\02\08\04\10", align 16

; Function Attrs: nounwind
define hidden i32 @av1_get_MBs(i32 %width, i32 %height) #0 {
entry:
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %aligned_width = alloca i32, align 4
  %aligned_height = alloca i32, align 4
  %mi_cols = alloca i32, align 4
  %mi_rows = alloca i32, align 4
  %mb_cols = alloca i32, align 4
  %mb_rows = alloca i32, align 4
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  %0 = bitcast i32* %aligned_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %width.addr, align 4, !tbaa !2
  %add = add nsw i32 %1, 7
  %and = and i32 %add, -8
  store i32 %and, i32* %aligned_width, align 4, !tbaa !2
  %2 = bitcast i32* %aligned_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i32, i32* %height.addr, align 4, !tbaa !2
  %add1 = add nsw i32 %3, 7
  %and2 = and i32 %add1, -8
  store i32 %and2, i32* %aligned_height, align 4, !tbaa !2
  %4 = bitcast i32* %mi_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i32, i32* %aligned_width, align 4, !tbaa !2
  %shr = ashr i32 %5, 2
  store i32 %shr, i32* %mi_cols, align 4, !tbaa !2
  %6 = bitcast i32* %mi_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i32, i32* %aligned_height, align 4, !tbaa !2
  %shr3 = ashr i32 %7, 2
  store i32 %shr3, i32* %mi_rows, align 4, !tbaa !2
  %8 = bitcast i32* %mb_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i32, i32* %mi_cols, align 4, !tbaa !2
  %add4 = add nsw i32 %9, 2
  %shr5 = ashr i32 %add4, 2
  store i32 %shr5, i32* %mb_cols, align 4, !tbaa !2
  %10 = bitcast i32* %mb_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load i32, i32* %mi_rows, align 4, !tbaa !2
  %add6 = add nsw i32 %11, 2
  %shr7 = ashr i32 %add6, 2
  store i32 %shr7, i32* %mb_rows, align 4, !tbaa !2
  %12 = load i32, i32* %mb_rows, align 4, !tbaa !2
  %13 = load i32, i32* %mb_cols, align 4, !tbaa !2
  %mul = mul nsw i32 %12, %13
  %14 = bitcast i32* %mb_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %mb_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %mi_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i32* %mi_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  %18 = bitcast i32* %aligned_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  %19 = bitcast i32* %aligned_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  ret i32 %mul
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_free_ref_frame_buffers(%struct.BufferPool* %pool) #0 {
entry:
  %pool.addr = alloca %struct.BufferPool*, align 4
  %i = alloca i32, align 4
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !2
  %cmp = icmp slt i32 %1, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %2, i32 0, i32 3
  %3 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs, i32 0, i32 %3
  %ref_count = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx, i32 0, i32 0
  %4 = load i32, i32* %ref_count, align 4, !tbaa !8
  %cmp1 = icmp sgt i32 %4, 0
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %5 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs2 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %5, i32 0, i32 3
  %6 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs2, i32 0, i32 %6
  %raw_frame_buffer = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx3, i32 0, i32 16
  %data = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer, i32 0, i32 0
  %7 = load i8*, i8** %data, align 4, !tbaa !19
  %cmp4 = icmp ne i8* %7, null
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %8 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %release_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %8, i32 0, i32 2
  %9 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb, align 4, !tbaa !20
  %10 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %cb_priv = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %10, i32 0, i32 0
  %11 = load i8*, i8** %cb_priv, align 4, !tbaa !23
  %12 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs5 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %12, i32 0, i32 3
  %13 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs5, i32 0, i32 %13
  %raw_frame_buffer7 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx6, i32 0, i32 16
  %call = call i32 %9(i8* %11, %struct.aom_codec_frame_buffer* %raw_frame_buffer7)
  %14 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs8 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %14, i32 0, i32 3
  %15 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs8, i32 0, i32 %15
  %raw_frame_buffer10 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx9, i32 0, i32 16
  %data11 = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer10, i32 0, i32 0
  store i8* null, i8** %data11, align 4, !tbaa !19
  %16 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs12 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %16, i32 0, i32 3
  %17 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs12, i32 0, i32 %17
  %raw_frame_buffer14 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx13, i32 0, i32 16
  %size = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer14, i32 0, i32 1
  store i32 0, i32* %size, align 4, !tbaa !24
  %18 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs15 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %18, i32 0, i32 3
  %19 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs15, i32 0, i32 %19
  %raw_frame_buffer17 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx16, i32 0, i32 16
  %priv = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer17, i32 0, i32 2
  store i8* null, i8** %priv, align 4, !tbaa !25
  %20 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs18 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %20, i32 0, i32 3
  %21 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs18, i32 0, i32 %21
  %ref_count20 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx19, i32 0, i32 0
  store i32 0, i32* %ref_count20, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  %22 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs21 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %22, i32 0, i32 3
  %23 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs21, i32 0, i32 %23
  %mvs = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx22, i32 0, i32 5
  %24 = load %struct.MV_REF*, %struct.MV_REF** %mvs, align 4, !tbaa !26
  %25 = bitcast %struct.MV_REF* %24 to i8*
  call void @aom_free(i8* %25)
  %26 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs23 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %26, i32 0, i32 3
  %27 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs23, i32 0, i32 %27
  %mvs25 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx24, i32 0, i32 5
  store %struct.MV_REF* null, %struct.MV_REF** %mvs25, align 4, !tbaa !26
  %28 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs26 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %28, i32 0, i32 3
  %29 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs26, i32 0, i32 %29
  %seg_map = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx27, i32 0, i32 6
  %30 = load i8*, i8** %seg_map, align 4, !tbaa !27
  call void @aom_free(i8* %30)
  %31 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs28 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %31, i32 0, i32 3
  %32 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs28, i32 0, i32 %32
  %seg_map30 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx29, i32 0, i32 6
  store i8* null, i8** %seg_map30, align 4, !tbaa !27
  %33 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !6
  %frame_bufs31 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %33, i32 0, i32 3
  %34 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds [16 x %struct.RefCntBuffer], [16 x %struct.RefCntBuffer]* %frame_bufs31, i32 0, i32 %34
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %arrayidx32, i32 0, i32 17
  %call33 = call i32 @aom_free_frame_buffer(%struct.yv12_buffer_config* %buf)
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %35 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  ret void
}

declare void @aom_free(i8*) #2

declare i32 @aom_free_frame_buffer(%struct.yv12_buffer_config*) #2

; Function Attrs: nounwind
define hidden void @av1_alloc_restoration_buffers(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %num_planes = alloca i32, align 4
  %p = alloca i32, align 4
  %num_stripes = alloca i32, align 4
  %i = alloca i32, align 4
  %tile_info = alloca %struct.TileInfo, align 4
  %mi_h = alloca i32, align 4
  %ext_h = alloca i32, align 4
  %tile_stripes = alloca i32, align 4
  %frame_w = alloca i32, align 4
  %use_highbd = alloca i32, align 4
  %p34 = alloca i32, align 4
  %is_uv = alloca i32, align 4
  %ss_x = alloca i32, align 4
  %plane_w = alloca i32, align 4
  %stride = alloca i32, align 4
  %buf_size = alloca i32, align 4
  %boundaries = alloca %struct.RestorationStripeBoundaries*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %call = call i32 @av1_num_planes(%struct.AV1Common* %1)
  store i32 %call, i32* %num_planes, align 4, !tbaa !2
  %2 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %p, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %p, align 4, !tbaa !2
  %4 = load i32, i32* %num_planes, align 4, !tbaa !2
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %7, i32 0, i32 29
  %8 = load i32, i32* %p, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %8
  %9 = load i32, i32* %p, align 4, !tbaa !2
  %cmp1 = icmp sgt i32 %9, 0
  %conv = zext i1 %cmp1 to i32
  call void @av1_alloc_restoration_struct(%struct.AV1Common* %6, %struct.RestorationInfo* %arrayidx, i32 %conv)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %p, align 4, !tbaa !2
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %p, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_tmpbuf = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %11, i32 0, i32 30
  %12 = load i32*, i32** %rst_tmpbuf, align 8, !tbaa !28
  %cmp2 = icmp eq i32* %12, null
  br i1 %cmp2, label %if.then, label %if.end8

if.then:                                          ; preds = %for.end
  br label %do.body

do.body:                                          ; preds = %if.then
  %call4 = call i8* @aom_memalign(i32 16, i32 1292704)
  %13 = bitcast i8* %call4 to i32*
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_tmpbuf5 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 30
  store i32* %13, i32** %rst_tmpbuf5, align 8, !tbaa !28
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_tmpbuf6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 30
  %16 = load i32*, i32** %rst_tmpbuf6, align 8, !tbaa !28
  %tobool = icmp ne i32* %16, null
  br i1 %tobool, label %if.end, label %if.then7

if.then7:                                         ; preds = %do.body
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 2, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then7, %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end8

if.end8:                                          ; preds = %do.end, %for.end
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rlbs = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 31
  %19 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs, align 4, !tbaa !47
  %cmp9 = icmp eq %struct.RestorationLineBuffers* %19, null
  br i1 %cmp9, label %if.then11, label %if.end22

if.then11:                                        ; preds = %if.end8
  br label %do.body12

do.body12:                                        ; preds = %if.then11
  %call13 = call i8* @aom_malloc(i32 4704)
  %20 = bitcast i8* %call13 to %struct.RestorationLineBuffers*
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rlbs14 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 31
  store %struct.RestorationLineBuffers* %20, %struct.RestorationLineBuffers** %rlbs14, align 4, !tbaa !47
  %22 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rlbs15 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %22, i32 0, i32 31
  %23 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs15, align 4, !tbaa !47
  %tobool16 = icmp ne %struct.RestorationLineBuffers* %23, null
  br i1 %tobool16, label %if.end19, label %if.then17

if.then17:                                        ; preds = %do.body12
  %24 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %error18 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %24, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error18, i32 2, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end19

if.end19:                                         ; preds = %if.then17, %do.body12
  br label %do.cond20

do.cond20:                                        ; preds = %if.end19
  br label %do.end21

do.end21:                                         ; preds = %do.cond20
  br label %if.end22

if.end22:                                         ; preds = %do.end21, %if.end8
  %25 = bitcast i32* %num_stripes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  store i32 0, i32* %num_stripes, align 4, !tbaa !2
  %26 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc30, %if.end22
  %27 = load i32, i32* %i, align 4, !tbaa !2
  %28 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %tiles = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %28, i32 0, i32 40
  %rows = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles, i32 0, i32 1
  %29 = load i32, i32* %rows, align 4, !tbaa !48
  %cmp24 = icmp slt i32 %27, %29
  br i1 %cmp24, label %for.body27, label %for.cond.cleanup26

for.cond.cleanup26:                               ; preds = %for.cond23
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  br label %for.end32

for.body27:                                       ; preds = %for.cond23
  %31 = bitcast %struct.TileInfo* %tile_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %31) #4
  %32 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %33 = load i32, i32* %i, align 4, !tbaa !2
  call void @av1_tile_set_row(%struct.TileInfo* %tile_info, %struct.AV1Common* %32, i32 %33)
  %34 = bitcast i32* %mi_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %mi_row_end = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile_info, i32 0, i32 1
  %35 = load i32, i32* %mi_row_end, align 4, !tbaa !49
  %mi_row_start = getelementptr inbounds %struct.TileInfo, %struct.TileInfo* %tile_info, i32 0, i32 0
  %36 = load i32, i32* %mi_row_start, align 4, !tbaa !51
  %sub = sub nsw i32 %35, %36
  store i32 %sub, i32* %mi_h, align 4, !tbaa !2
  %37 = bitcast i32* %ext_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  %38 = load i32, i32* %mi_h, align 4, !tbaa !2
  %shl = shl i32 %38, 2
  %add = add nsw i32 8, %shl
  store i32 %add, i32* %ext_h, align 4, !tbaa !2
  %39 = bitcast i32* %tile_stripes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #4
  %40 = load i32, i32* %ext_h, align 4, !tbaa !2
  %add28 = add nsw i32 %40, 63
  %div = sdiv i32 %add28, 64
  store i32 %div, i32* %tile_stripes, align 4, !tbaa !2
  %41 = load i32, i32* %tile_stripes, align 4, !tbaa !2
  %42 = load i32, i32* %num_stripes, align 4, !tbaa !2
  %add29 = add nsw i32 %42, %41
  store i32 %add29, i32* %num_stripes, align 4, !tbaa !2
  %43 = bitcast i32* %tile_stripes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i32* %ext_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i32* %mi_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast %struct.TileInfo* %tile_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %46) #4
  br label %for.inc30

for.inc30:                                        ; preds = %for.body27
  %47 = load i32, i32* %i, align 4, !tbaa !2
  %inc31 = add nsw i32 %47, 1
  store i32 %inc31, i32* %i, align 4, !tbaa !2
  br label %for.cond23

for.end32:                                        ; preds = %for.cond.cleanup26
  %48 = bitcast i32* %frame_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #4
  %49 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %superres_upscaled_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %49, i32 0, i32 6
  %50 = load i32, i32* %superres_upscaled_width, align 16, !tbaa !52
  store i32 %50, i32* %frame_w, align 4, !tbaa !2
  %51 = bitcast i32* %use_highbd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #4
  %52 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %52, i32 0, i32 37
  %use_highbitdepth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 26
  %53 = load i8, i8* %use_highbitdepth, align 4, !tbaa !53
  %conv33 = zext i8 %53 to i32
  store i32 %conv33, i32* %use_highbd, align 4, !tbaa !2
  %54 = bitcast i32* %p34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #4
  store i32 0, i32* %p34, align 4, !tbaa !2
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc85, %for.end32
  %55 = load i32, i32* %p34, align 4, !tbaa !2
  %56 = load i32, i32* %num_planes, align 4, !tbaa !2
  %cmp36 = icmp slt i32 %55, %56
  br i1 %cmp36, label %for.body39, label %for.cond.cleanup38

for.cond.cleanup38:                               ; preds = %for.cond35
  %57 = bitcast i32* %p34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  br label %for.end87

for.body39:                                       ; preds = %for.cond35
  %58 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #4
  %59 = load i32, i32* %p34, align 4, !tbaa !2
  %cmp40 = icmp sgt i32 %59, 0
  %conv41 = zext i1 %cmp40 to i32
  store i32 %conv41, i32* %is_uv, align 4, !tbaa !2
  %60 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #4
  %61 = load i32, i32* %is_uv, align 4, !tbaa !2
  %tobool42 = icmp ne i32 %61, 0
  br i1 %tobool42, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.body39
  %62 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params43 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %62, i32 0, i32 37
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params43, i32 0, i32 32
  %63 = load i32, i32* %subsampling_x, align 16, !tbaa !54
  %tobool44 = icmp ne i32 %63, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.body39
  %64 = phi i1 [ false, %for.body39 ], [ %tobool44, %land.rhs ]
  %land.ext = zext i1 %64 to i32
  store i32 %land.ext, i32* %ss_x, align 4, !tbaa !2
  %65 = bitcast i32* %plane_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #4
  %66 = load i32, i32* %frame_w, align 4, !tbaa !2
  %67 = load i32, i32* %ss_x, align 4, !tbaa !2
  %add45 = add nsw i32 %66, %67
  %68 = load i32, i32* %ss_x, align 4, !tbaa !2
  %shr = ashr i32 %add45, %68
  %add46 = add nsw i32 %shr, 8
  store i32 %add46, i32* %plane_w, align 4, !tbaa !2
  %69 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %70 = load i32, i32* %plane_w, align 4, !tbaa !2
  %add47 = add nsw i32 %70, 31
  %and = and i32 %add47, -32
  store i32 %and, i32* %stride, align 4, !tbaa !2
  %71 = bitcast i32* %buf_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #4
  %72 = load i32, i32* %num_stripes, align 4, !tbaa !2
  %73 = load i32, i32* %stride, align 4, !tbaa !2
  %mul = mul nsw i32 %72, %73
  %mul48 = mul nsw i32 %mul, 2
  %74 = load i32, i32* %use_highbd, align 4, !tbaa !2
  %shl49 = shl i32 %mul48, %74
  store i32 %shl49, i32* %buf_size, align 4, !tbaa !2
  %75 = bitcast %struct.RestorationStripeBoundaries** %boundaries to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #4
  %76 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_info50 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %76, i32 0, i32 29
  %77 = load i32, i32* %p34, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info50, i32 0, i32 %77
  %boundaries52 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx51, i32 0, i32 6
  store %struct.RestorationStripeBoundaries* %boundaries52, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %78 = load i32, i32* %buf_size, align 4, !tbaa !2
  %79 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_size = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %79, i32 0, i32 3
  %80 = load i32, i32* %stripe_boundary_size, align 4, !tbaa !55
  %cmp53 = icmp ne i32 %78, %80
  br i1 %cmp53, label %if.then60, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.end
  %81 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_above = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %81, i32 0, i32 0
  %82 = load i8*, i8** %stripe_boundary_above, align 4, !tbaa !57
  %cmp55 = icmp eq i8* %82, null
  br i1 %cmp55, label %if.then60, label %lor.lhs.false57

lor.lhs.false57:                                  ; preds = %lor.lhs.false
  %83 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_below = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %83, i32 0, i32 1
  %84 = load i8*, i8** %stripe_boundary_below, align 4, !tbaa !58
  %cmp58 = icmp eq i8* %84, null
  br i1 %cmp58, label %if.then60, label %if.end84

if.then60:                                        ; preds = %lor.lhs.false57, %lor.lhs.false, %land.end
  %85 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_above61 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %85, i32 0, i32 0
  %86 = load i8*, i8** %stripe_boundary_above61, align 4, !tbaa !57
  call void @aom_free(i8* %86)
  %87 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_below62 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %87, i32 0, i32 1
  %88 = load i8*, i8** %stripe_boundary_below62, align 4, !tbaa !58
  call void @aom_free(i8* %88)
  br label %do.body63

do.body63:                                        ; preds = %if.then60
  %89 = load i32, i32* %buf_size, align 4, !tbaa !2
  %call64 = call i8* @aom_memalign(i32 32, i32 %89)
  %90 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_above65 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %90, i32 0, i32 0
  store i8* %call64, i8** %stripe_boundary_above65, align 4, !tbaa !57
  %91 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_above66 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %91, i32 0, i32 0
  %92 = load i8*, i8** %stripe_boundary_above66, align 4, !tbaa !57
  %tobool67 = icmp ne i8* %92, null
  br i1 %tobool67, label %if.end70, label %if.then68

if.then68:                                        ; preds = %do.body63
  %93 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %error69 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %93, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error69, i32 2, i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end70

if.end70:                                         ; preds = %if.then68, %do.body63
  br label %do.cond71

do.cond71:                                        ; preds = %if.end70
  br label %do.end72

do.end72:                                         ; preds = %do.cond71
  br label %do.body73

do.body73:                                        ; preds = %do.end72
  %94 = load i32, i32* %buf_size, align 4, !tbaa !2
  %call74 = call i8* @aom_memalign(i32 32, i32 %94)
  %95 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_below75 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %95, i32 0, i32 1
  store i8* %call74, i8** %stripe_boundary_below75, align 4, !tbaa !58
  %96 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_below76 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %96, i32 0, i32 1
  %97 = load i8*, i8** %stripe_boundary_below76, align 4, !tbaa !58
  %tobool77 = icmp ne i8* %97, null
  br i1 %tobool77, label %if.end80, label %if.then78

if.then78:                                        ; preds = %do.body73
  %98 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %error79 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %98, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error79, i32 2, i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.3, i32 0, i32 0))
  br label %if.end80

if.end80:                                         ; preds = %if.then78, %do.body73
  br label %do.cond81

do.cond81:                                        ; preds = %if.end80
  br label %do.end82

do.end82:                                         ; preds = %do.cond81
  %99 = load i32, i32* %buf_size, align 4, !tbaa !2
  %100 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_size83 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %100, i32 0, i32 3
  store i32 %99, i32* %stripe_boundary_size83, align 4, !tbaa !55
  br label %if.end84

if.end84:                                         ; preds = %do.end82, %lor.lhs.false57
  %101 = load i32, i32* %stride, align 4, !tbaa !2
  %102 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_stride = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %102, i32 0, i32 2
  store i32 %101, i32* %stripe_boundary_stride, align 4, !tbaa !59
  %103 = bitcast %struct.RestorationStripeBoundaries** %boundaries to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #4
  %104 = bitcast i32* %buf_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #4
  %105 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #4
  %106 = bitcast i32* %plane_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #4
  %107 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  %108 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #4
  br label %for.inc85

for.inc85:                                        ; preds = %if.end84
  %109 = load i32, i32* %p34, align 4, !tbaa !2
  %inc86 = add nsw i32 %109, 1
  store i32 %inc86, i32* %p34, align 4, !tbaa !2
  br label %for.cond35

for.end87:                                        ; preds = %for.cond.cleanup38
  %110 = bitcast i32* %use_highbd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast i32* %frame_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast i32* %num_stripes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_num_planes(%struct.AV1Common* %cm) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %1 = load i8, i8* %monochrome, align 1, !tbaa !60
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 3
  ret i32 %cond
}

declare void @av1_alloc_restoration_struct(%struct.AV1Common*, %struct.RestorationInfo*, i32) #2

declare i8* @aom_memalign(i32, i32) #2

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #2

declare i8* @aom_malloc(i32) #2

declare void @av1_tile_set_row(%struct.TileInfo*, %struct.AV1Common*, i32) #2

; Function Attrs: nounwind
define hidden void @av1_free_restoration_buffers(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %p = alloca i32, align 4
  %boundaries = alloca %struct.RestorationStripeBoundaries*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %p, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %p, align 4, !tbaa !2
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_info = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 29
  %3 = load i32, i32* %p, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info, i32 0, i32 %3
  call void @av1_free_restoration_struct(%struct.RestorationInfo* %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %p, align 4, !tbaa !2
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %p, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_tmpbuf = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 30
  %6 = load i32*, i32** %rst_tmpbuf, align 8, !tbaa !28
  %7 = bitcast i32* %6 to i8*
  call void @aom_free(i8* %7)
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_tmpbuf1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 30
  store i32* null, i32** %rst_tmpbuf1, align 8, !tbaa !28
  %9 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rlbs = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %9, i32 0, i32 31
  %10 = load %struct.RestorationLineBuffers*, %struct.RestorationLineBuffers** %rlbs, align 4, !tbaa !47
  %11 = bitcast %struct.RestorationLineBuffers* %10 to i8*
  call void @aom_free(i8* %11)
  %12 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rlbs2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %12, i32 0, i32 31
  store %struct.RestorationLineBuffers* null, %struct.RestorationLineBuffers** %rlbs2, align 4, !tbaa !47
  store i32 0, i32* %p, align 4, !tbaa !2
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc11, %for.end
  %13 = load i32, i32* %p, align 4, !tbaa !2
  %cmp4 = icmp slt i32 %13, 3
  br i1 %cmp4, label %for.body5, label %for.end13

for.body5:                                        ; preds = %for.cond3
  %14 = bitcast %struct.RestorationStripeBoundaries** %boundaries to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_info6 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 29
  %16 = load i32, i32* %p, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds [3 x %struct.RestorationInfo], [3 x %struct.RestorationInfo]* %rst_info6, i32 0, i32 %16
  %boundaries8 = getelementptr inbounds %struct.RestorationInfo, %struct.RestorationInfo* %arrayidx7, i32 0, i32 6
  store %struct.RestorationStripeBoundaries* %boundaries8, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %17 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_above = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %17, i32 0, i32 0
  %18 = load i8*, i8** %stripe_boundary_above, align 4, !tbaa !57
  call void @aom_free(i8* %18)
  %19 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_below = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %19, i32 0, i32 1
  %20 = load i8*, i8** %stripe_boundary_below, align 4, !tbaa !58
  call void @aom_free(i8* %20)
  %21 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_above9 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %21, i32 0, i32 0
  store i8* null, i8** %stripe_boundary_above9, align 4, !tbaa !57
  %22 = load %struct.RestorationStripeBoundaries*, %struct.RestorationStripeBoundaries** %boundaries, align 4, !tbaa !6
  %stripe_boundary_below10 = getelementptr inbounds %struct.RestorationStripeBoundaries, %struct.RestorationStripeBoundaries* %22, i32 0, i32 1
  store i8* null, i8** %stripe_boundary_below10, align 4, !tbaa !58
  %23 = bitcast %struct.RestorationStripeBoundaries** %boundaries to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body5
  %24 = load i32, i32* %p, align 4, !tbaa !2
  %inc12 = add nsw i32 %24, 1
  store i32 %inc12, i32* %p, align 4, !tbaa !2
  br label %for.cond3

for.end13:                                        ; preds = %for.cond3
  %25 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %rst_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %25, i32 0, i32 32
  %call = call i32 @aom_free_frame_buffer(%struct.yv12_buffer_config* %rst_frame)
  %26 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  ret void
}

declare void @av1_free_restoration_struct(%struct.RestorationInfo*) #2

; Function Attrs: nounwind
define hidden void @av1_free_above_context_buffers(%struct.CommonContexts* %above_contexts) #0 {
entry:
  %above_contexts.addr = alloca %struct.CommonContexts*, align 4
  %i = alloca i32, align 4
  %num_planes = alloca i32, align 4
  %tile_row = alloca i32, align 4
  store %struct.CommonContexts* %above_contexts, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %num_planes1 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %2, i32 0, i32 3
  %3 = load i32, i32* %num_planes1, align 4, !tbaa !61
  store i32 %3, i32* %num_planes, align 4, !tbaa !2
  %4 = bitcast i32* %tile_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %tile_row, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %5 = load i32, i32* %tile_row, align 4, !tbaa !2
  %6 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %num_tile_rows = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %6, i32 0, i32 4
  %7 = load i32, i32* %num_tile_rows, align 4, !tbaa !62
  %cmp = icmp slt i32 %5, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %8 = bitcast i32* %tile_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  br label %for.end17

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !2
  %10 = load i32, i32* %num_planes, align 4, !tbaa !2
  %cmp3 = icmp slt i32 %9, %10
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %11 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %entropy = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %11, i32 0, i32 1
  %12 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x i8**], [3 x i8**]* %entropy, i32 0, i32 %12
  %13 = load i8**, i8*** %arrayidx, align 4, !tbaa !6
  %14 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8*, i8** %13, i32 %14
  %15 = load i8*, i8** %arrayidx5, align 4, !tbaa !6
  call void @aom_free(i8* %15)
  %16 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %entropy6 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %16, i32 0, i32 1
  %17 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds [3 x i8**], [3 x i8**]* %entropy6, i32 0, i32 %17
  %18 = load i8**, i8*** %arrayidx7, align 4, !tbaa !6
  %19 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8*, i8** %18, i32 %19
  store i8* null, i8** %arrayidx8, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %20 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  %21 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %partition = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %21, i32 0, i32 0
  %22 = load i8**, i8*** %partition, align 4, !tbaa !63
  %23 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx9, align 4, !tbaa !6
  call void @aom_free(i8* %24)
  %25 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %partition10 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %25, i32 0, i32 0
  %26 = load i8**, i8*** %partition10, align 4, !tbaa !63
  %27 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %26, i32 %27
  store i8* null, i8** %arrayidx11, align 4, !tbaa !6
  %28 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %txfm = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %28, i32 0, i32 2
  %29 = load i8**, i8*** %txfm, align 4, !tbaa !64
  %30 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8*, i8** %29, i32 %30
  %31 = load i8*, i8** %arrayidx12, align 4, !tbaa !6
  call void @aom_free(i8* %31)
  %32 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %txfm13 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %32, i32 0, i32 2
  %33 = load i8**, i8*** %txfm13, align 4, !tbaa !64
  %34 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8*, i8** %33, i32 %34
  store i8* null, i8** %arrayidx14, align 4, !tbaa !6
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %35 = load i32, i32* %tile_row, align 4, !tbaa !2
  %inc16 = add nsw i32 %35, 1
  store i32 %inc16, i32* %tile_row, align 4, !tbaa !2
  br label %for.cond

for.end17:                                        ; preds = %for.cond.cleanup
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc25, %for.end17
  %36 = load i32, i32* %i, align 4, !tbaa !2
  %37 = load i32, i32* %num_planes, align 4, !tbaa !2
  %cmp19 = icmp slt i32 %36, %37
  br i1 %cmp19, label %for.body20, label %for.end27

for.body20:                                       ; preds = %for.cond18
  %38 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %entropy21 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %38, i32 0, i32 1
  %39 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds [3 x i8**], [3 x i8**]* %entropy21, i32 0, i32 %39
  %40 = load i8**, i8*** %arrayidx22, align 4, !tbaa !6
  %41 = bitcast i8** %40 to i8*
  call void @aom_free(i8* %41)
  %42 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %entropy23 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %42, i32 0, i32 1
  %43 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds [3 x i8**], [3 x i8**]* %entropy23, i32 0, i32 %43
  store i8** null, i8*** %arrayidx24, align 4, !tbaa !6
  br label %for.inc25

for.inc25:                                        ; preds = %for.body20
  %44 = load i32, i32* %i, align 4, !tbaa !2
  %inc26 = add nsw i32 %44, 1
  store i32 %inc26, i32* %i, align 4, !tbaa !2
  br label %for.cond18

for.end27:                                        ; preds = %for.cond18
  %45 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %partition28 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %45, i32 0, i32 0
  %46 = load i8**, i8*** %partition28, align 4, !tbaa !63
  %47 = bitcast i8** %46 to i8*
  call void @aom_free(i8* %47)
  %48 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %partition29 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %48, i32 0, i32 0
  store i8** null, i8*** %partition29, align 4, !tbaa !63
  %49 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %txfm30 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %49, i32 0, i32 2
  %50 = load i8**, i8*** %txfm30, align 4, !tbaa !64
  %51 = bitcast i8** %50 to i8*
  call void @aom_free(i8* %51)
  %52 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %txfm31 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %52, i32 0, i32 2
  store i8** null, i8*** %txfm31, align 4, !tbaa !64
  %53 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %num_tile_rows32 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %53, i32 0, i32 4
  store i32 0, i32* %num_tile_rows32, align 4, !tbaa !62
  %54 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %num_mi_cols = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %54, i32 0, i32 5
  store i32 0, i32* %num_mi_cols, align 4, !tbaa !65
  %55 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %num_planes33 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %55, i32 0, i32 3
  store i32 0, i32* %num_planes33, align 4, !tbaa !61
  %56 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %57 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_free_context_buffers(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 22
  %free_mi = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 13
  %1 = load void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)** %free_mi, align 4, !tbaa !66
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 22
  call void %1(%struct.CommonModeInfoParams* %mi_params1)
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %above_contexts = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 42
  call void @av1_free_above_context_buffers(%struct.CommonContexts* %above_contexts)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_alloc_above_context_buffers(%struct.CommonContexts* %above_contexts, i32 %num_tile_rows, i32 %num_mi_cols, i32 %num_planes) #0 {
entry:
  %retval = alloca i32, align 4
  %above_contexts.addr = alloca %struct.CommonContexts*, align 4
  %num_tile_rows.addr = alloca i32, align 4
  %num_mi_cols.addr = alloca i32, align 4
  %num_planes.addr = alloca i32, align 4
  %aligned_mi_cols = alloca i32, align 4
  %plane_idx = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tile_row = alloca i32, align 4
  %plane_idx20 = alloca i32, align 4
  store %struct.CommonContexts* %above_contexts, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  store i32 %num_tile_rows, i32* %num_tile_rows.addr, align 4, !tbaa !2
  store i32 %num_mi_cols, i32* %num_mi_cols.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !2
  %0 = bitcast i32* %aligned_mi_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %num_mi_cols.addr, align 4, !tbaa !2
  %add = add nsw i32 %1, 31
  %and = and i32 %add, -32
  store i32 %and, i32* %aligned_mi_cols, align 4, !tbaa !2
  %2 = load i32, i32* %num_tile_rows.addr, align 4, !tbaa !2
  %3 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %num_tile_rows1 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %3, i32 0, i32 4
  store i32 %2, i32* %num_tile_rows1, align 4, !tbaa !62
  %4 = load i32, i32* %aligned_mi_cols, align 4, !tbaa !2
  %5 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %num_mi_cols2 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %5, i32 0, i32 5
  store i32 %4, i32* %num_mi_cols2, align 4, !tbaa !65
  %6 = load i32, i32* %num_planes.addr, align 4, !tbaa !2
  %7 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %num_planes3 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %7, i32 0, i32 3
  store i32 %6, i32* %num_planes3, align 4, !tbaa !61
  %8 = bitcast i32* %plane_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store i32 0, i32* %plane_idx, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %plane_idx, align 4, !tbaa !2
  %10 = load i32, i32* %num_planes.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %num_tile_rows.addr, align 4, !tbaa !2
  %call = call i8* @aom_calloc(i32 %11, i32 4)
  %12 = bitcast i8* %call to i8**
  %13 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %entropy = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %13, i32 0, i32 1
  %14 = load i32, i32* %plane_idx, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [3 x i8**], [3 x i8**]* %entropy, i32 0, i32 %14
  store i8** %12, i8*** %arrayidx, align 4, !tbaa !6
  %15 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %entropy4 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %15, i32 0, i32 1
  %16 = load i32, i32* %plane_idx, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds [3 x i8**], [3 x i8**]* %entropy4, i32 0, i32 %16
  %17 = load i8**, i8*** %arrayidx5, align 4, !tbaa !6
  %tobool = icmp ne i8** %17, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %18 = load i32, i32* %plane_idx, align 4, !tbaa !2
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %plane_idx, align 4, !tbaa !2
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %19 = bitcast i32* %plane_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup61 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  %20 = load i32, i32* %num_tile_rows.addr, align 4, !tbaa !2
  %call6 = call i8* @aom_calloc(i32 %20, i32 4)
  %21 = bitcast i8* %call6 to i8**
  %22 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %partition = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %22, i32 0, i32 0
  store i8** %21, i8*** %partition, align 4, !tbaa !63
  %23 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %partition7 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %23, i32 0, i32 0
  %24 = load i8**, i8*** %partition7, align 4, !tbaa !63
  %tobool8 = icmp ne i8** %24, null
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %for.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end10:                                         ; preds = %for.end
  %25 = load i32, i32* %num_tile_rows.addr, align 4, !tbaa !2
  %call11 = call i8* @aom_calloc(i32 %25, i32 4)
  %26 = bitcast i8* %call11 to i8**
  %27 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %txfm = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %27, i32 0, i32 2
  store i8** %26, i8*** %txfm, align 4, !tbaa !64
  %28 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %txfm12 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %28, i32 0, i32 2
  %29 = load i8**, i8*** %txfm12, align 4, !tbaa !64
  %tobool13 = icmp ne i8** %29, null
  br i1 %tobool13, label %if.end15, label %if.then14

if.then14:                                        ; preds = %if.end10
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end15:                                         ; preds = %if.end10
  %30 = bitcast i32* %tile_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  store i32 0, i32* %tile_row, align 4, !tbaa !2
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc56, %if.end15
  %31 = load i32, i32* %tile_row, align 4, !tbaa !2
  %32 = load i32, i32* %num_tile_rows.addr, align 4, !tbaa !2
  %cmp17 = icmp slt i32 %31, %32
  br i1 %cmp17, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond16
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup58

for.body19:                                       ; preds = %for.cond16
  %33 = bitcast i32* %plane_idx20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  store i32 0, i32* %plane_idx20, align 4, !tbaa !2
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc35, %for.body19
  %34 = load i32, i32* %plane_idx20, align 4, !tbaa !2
  %35 = load i32, i32* %num_planes.addr, align 4, !tbaa !2
  %cmp22 = icmp slt i32 %34, %35
  br i1 %cmp22, label %for.body24, label %for.cond.cleanup23

for.cond.cleanup23:                               ; preds = %for.cond21
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup37

for.body24:                                       ; preds = %for.cond21
  %36 = load i32, i32* %aligned_mi_cols, align 4, !tbaa !2
  %call25 = call i8* @aom_calloc(i32 %36, i32 1)
  %37 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %entropy26 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %37, i32 0, i32 1
  %38 = load i32, i32* %plane_idx20, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds [3 x i8**], [3 x i8**]* %entropy26, i32 0, i32 %38
  %39 = load i8**, i8*** %arrayidx27, align 4, !tbaa !6
  %40 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i8*, i8** %39, i32 %40
  store i8* %call25, i8** %arrayidx28, align 4, !tbaa !6
  %41 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %entropy29 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %41, i32 0, i32 1
  %42 = load i32, i32* %plane_idx20, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds [3 x i8**], [3 x i8**]* %entropy29, i32 0, i32 %42
  %43 = load i8**, i8*** %arrayidx30, align 4, !tbaa !6
  %44 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8*, i8** %43, i32 %44
  %45 = load i8*, i8** %arrayidx31, align 4, !tbaa !6
  %tobool32 = icmp ne i8* %45, null
  br i1 %tobool32, label %if.end34, label %if.then33

if.then33:                                        ; preds = %for.body24
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup37

if.end34:                                         ; preds = %for.body24
  br label %for.inc35

for.inc35:                                        ; preds = %if.end34
  %46 = load i32, i32* %plane_idx20, align 4, !tbaa !2
  %inc36 = add nsw i32 %46, 1
  store i32 %inc36, i32* %plane_idx20, align 4, !tbaa !2
  br label %for.cond21

cleanup37:                                        ; preds = %if.then33, %for.cond.cleanup23
  %47 = bitcast i32* %plane_idx20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %cleanup.dest38 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest38, label %cleanup58 [
    i32 8, label %for.end39
  ]

for.end39:                                        ; preds = %cleanup37
  %48 = load i32, i32* %aligned_mi_cols, align 4, !tbaa !2
  %call40 = call i8* @aom_calloc(i32 %48, i32 1)
  %49 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %partition41 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %49, i32 0, i32 0
  %50 = load i8**, i8*** %partition41, align 4, !tbaa !63
  %51 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i8*, i8** %50, i32 %51
  store i8* %call40, i8** %arrayidx42, align 4, !tbaa !6
  %52 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %partition43 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %52, i32 0, i32 0
  %53 = load i8**, i8*** %partition43, align 4, !tbaa !63
  %54 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i8*, i8** %53, i32 %54
  %55 = load i8*, i8** %arrayidx44, align 4, !tbaa !6
  %tobool45 = icmp ne i8* %55, null
  br i1 %tobool45, label %if.end47, label %if.then46

if.then46:                                        ; preds = %for.end39
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup58

if.end47:                                         ; preds = %for.end39
  %56 = load i32, i32* %aligned_mi_cols, align 4, !tbaa !2
  %call48 = call i8* @aom_calloc(i32 %56, i32 1)
  %57 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %txfm49 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %57, i32 0, i32 2
  %58 = load i8**, i8*** %txfm49, align 4, !tbaa !64
  %59 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i8*, i8** %58, i32 %59
  store i8* %call48, i8** %arrayidx50, align 4, !tbaa !6
  %60 = load %struct.CommonContexts*, %struct.CommonContexts** %above_contexts.addr, align 4, !tbaa !6
  %txfm51 = getelementptr inbounds %struct.CommonContexts, %struct.CommonContexts* %60, i32 0, i32 2
  %61 = load i8**, i8*** %txfm51, align 4, !tbaa !64
  %62 = load i32, i32* %tile_row, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8*, i8** %61, i32 %62
  %63 = load i8*, i8** %arrayidx52, align 4, !tbaa !6
  %tobool53 = icmp ne i8* %63, null
  br i1 %tobool53, label %if.end55, label %if.then54

if.then54:                                        ; preds = %if.end47
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup58

if.end55:                                         ; preds = %if.end47
  br label %for.inc56

for.inc56:                                        ; preds = %if.end55
  %64 = load i32, i32* %tile_row, align 4, !tbaa !2
  %inc57 = add nsw i32 %64, 1
  store i32 %inc57, i32* %tile_row, align 4, !tbaa !2
  br label %for.cond16

cleanup58:                                        ; preds = %if.then54, %if.then46, %cleanup37, %for.cond.cleanup18
  %65 = bitcast i32* %tile_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  %cleanup.dest59 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest59, label %cleanup61 [
    i32 5, label %for.end60
  ]

for.end60:                                        ; preds = %cleanup58
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

cleanup61:                                        ; preds = %for.end60, %cleanup58, %if.then14, %if.then9, %cleanup
  %66 = bitcast i32* %aligned_mi_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #4
  %67 = load i32, i32* %retval, align 4
  ret i32 %67
}

declare i8* @aom_calloc(i32, i32) #2

; Function Attrs: nounwind
define hidden i32 @av1_alloc_context_buffers(%struct.AV1Common* %cm, i32 %width, i32 %height) #0 {
entry:
  %retval = alloca i32, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %mi_params = alloca %struct.CommonModeInfoParams*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !2
  store i32 %height, i32* %height.addr, align 4, !tbaa !2
  %0 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %mi_params1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 22
  store %struct.CommonModeInfoParams* %mi_params1, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %2 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %set_mb_mi = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %2, i32 0, i32 15
  %3 = load void (%struct.CommonModeInfoParams*, i32, i32)*, void (%struct.CommonModeInfoParams*, i32, i32)** %set_mb_mi, align 4, !tbaa !67
  %4 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %5 = load i32, i32* %width.addr, align 4, !tbaa !2
  %6 = load i32, i32* %height.addr, align 4, !tbaa !2
  call void %3(%struct.CommonModeInfoParams* %4, i32 %5, i32 %6)
  %7 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %call = call i32 @alloc_mi(%struct.CommonModeInfoParams* %7)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %fail

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

fail:                                             ; preds = %if.then
  %8 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  %set_mb_mi2 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %8, i32 0, i32 15
  %9 = load void (%struct.CommonModeInfoParams*, i32, i32)*, void (%struct.CommonModeInfoParams*, i32, i32)** %set_mb_mi2, align 4, !tbaa !67
  %10 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params, align 4, !tbaa !6
  call void %9(%struct.CommonModeInfoParams* %10, i32 0, i32 0)
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  call void @av1_free_context_buffers(%struct.AV1Common* %11)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %fail, %if.end
  %12 = bitcast %struct.CommonModeInfoParams** %mi_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

; Function Attrs: nounwind
define internal i32 @alloc_mi(%struct.CommonModeInfoParams* %mi_params) #0 {
entry:
  %retval = alloca i32, align 4
  %mi_params.addr = alloca %struct.CommonModeInfoParams*, align 4
  %aligned_mi_rows = alloca i32, align 4
  %mi_grid_size = alloca i32, align 4
  %alloc_size_1d = alloca i32, align 4
  %alloc_mi_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.CommonModeInfoParams* %mi_params, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %0 = bitcast i32* %aligned_mi_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %1, i32 0, i32 3
  %2 = load i32, i32* %mi_rows, align 4, !tbaa !68
  %call = call i32 @calc_mi_size(i32 %2)
  store i32 %call, i32* %aligned_mi_rows, align 4, !tbaa !2
  %3 = bitcast i32* %mi_grid_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %4, i32 0, i32 11
  %5 = load i32, i32* %mi_stride, align 4, !tbaa !69
  %6 = load i32, i32* %aligned_mi_rows, align 4, !tbaa !2
  %mul = mul nsw i32 %5, %6
  store i32 %mul, i32* %mi_grid_size, align 4, !tbaa !2
  %7 = bitcast i32* %alloc_size_1d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_alloc_bsize = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %8, i32 0, i32 8
  %9 = load i8, i8* %mi_alloc_bsize, align 4, !tbaa !70
  %idxprom = zext i8 %9 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !71
  %conv = zext i8 %10 to i32
  store i32 %conv, i32* %alloc_size_1d, align 4, !tbaa !2
  %11 = bitcast i32* %alloc_mi_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_alloc_stride = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %12, i32 0, i32 7
  %13 = load i32, i32* %mi_alloc_stride, align 4, !tbaa !72
  %14 = load i32, i32* %aligned_mi_rows, align 4, !tbaa !2
  %15 = load i32, i32* %alloc_size_1d, align 4, !tbaa !2
  %div = sdiv i32 %14, %15
  %mul1 = mul nsw i32 %13, %div
  store i32 %mul1, i32* %alloc_mi_size, align 4, !tbaa !2
  %16 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_alloc_size = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %16, i32 0, i32 6
  %17 = load i32, i32* %mi_alloc_size, align 4, !tbaa !73
  %18 = load i32, i32* %alloc_mi_size, align 4, !tbaa !2
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %19 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_grid_size3 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %19, i32 0, i32 10
  %20 = load i32, i32* %mi_grid_size3, align 4, !tbaa !74
  %21 = load i32, i32* %mi_grid_size, align 4, !tbaa !2
  %cmp4 = icmp slt i32 %20, %21
  br i1 %cmp4, label %if.then, label %if.end21

if.then:                                          ; preds = %lor.lhs.false, %entry
  %22 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %free_mi = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %22, i32 0, i32 13
  %23 = load void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)** %free_mi, align 4, !tbaa !75
  %24 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  call void %23(%struct.CommonModeInfoParams* %24)
  %25 = load i32, i32* %alloc_mi_size, align 4, !tbaa !2
  %call6 = call i8* @aom_calloc(i32 %25, i32 164)
  %26 = bitcast i8* %call6 to %struct.MB_MODE_INFO*
  %27 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_alloc = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %27, i32 0, i32 5
  store %struct.MB_MODE_INFO* %26, %struct.MB_MODE_INFO** %mi_alloc, align 4, !tbaa !76
  %28 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_alloc7 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %28, i32 0, i32 5
  %29 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi_alloc7, align 4, !tbaa !76
  %tobool = icmp ne %struct.MB_MODE_INFO* %29, null
  br i1 %tobool, label %if.end, label %if.then8

if.then8:                                         ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %30 = load i32, i32* %alloc_mi_size, align 4, !tbaa !2
  %31 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_alloc_size9 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %31, i32 0, i32 6
  store i32 %30, i32* %mi_alloc_size9, align 4, !tbaa !73
  %32 = load i32, i32* %mi_grid_size, align 4, !tbaa !2
  %call10 = call i8* @aom_calloc(i32 %32, i32 4)
  %33 = bitcast i8* %call10 to %struct.MB_MODE_INFO**
  %34 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_grid_base = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %34, i32 0, i32 9
  store %struct.MB_MODE_INFO** %33, %struct.MB_MODE_INFO*** %mi_grid_base, align 4, !tbaa !77
  %35 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_grid_base11 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %35, i32 0, i32 9
  %36 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi_grid_base11, align 4, !tbaa !77
  %tobool12 = icmp ne %struct.MB_MODE_INFO** %36, null
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.end
  %37 = load i32, i32* %mi_grid_size, align 4, !tbaa !2
  %38 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %mi_grid_size15 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %38, i32 0, i32 10
  store i32 %37, i32* %mi_grid_size15, align 4, !tbaa !74
  %39 = load i32, i32* %mi_grid_size, align 4, !tbaa !2
  %call16 = call i8* @aom_calloc(i32 %39, i32 1)
  %40 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %tx_type_map = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %40, i32 0, i32 12
  store i8* %call16, i8** %tx_type_map, align 4, !tbaa !78
  %41 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %tx_type_map17 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %41, i32 0, i32 12
  %42 = load i8*, i8** %tx_type_map17, align 4, !tbaa !78
  %tobool18 = icmp ne i8* %42, null
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %if.end14
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %if.end14
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %lor.lhs.false
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end21, %if.then19, %if.then13, %if.then8
  %43 = bitcast i32* %alloc_mi_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i32* %alloc_size_1d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i32* %mi_grid_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %aligned_mi_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = load i32, i32* %retval, align 4
  ret i32 %47
}

; Function Attrs: nounwind
define hidden void @av1_remove_common(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  call void @av1_free_context_buffers(%struct.AV1Common* %0)
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %fc = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %1, i32 0, i32 38
  %2 = load %struct.frame_contexts*, %struct.frame_contexts** %fc, align 16, !tbaa !79
  %3 = bitcast %struct.frame_contexts* %2 to i8*
  call void @aom_free(i8* %3)
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %fc1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 38
  store %struct.frame_contexts* null, %struct.frame_contexts** %fc1, align 16, !tbaa !79
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %default_frame_context = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 39
  %6 = load %struct.frame_contexts*, %struct.frame_contexts** %default_frame_context, align 4, !tbaa !80
  %7 = bitcast %struct.frame_contexts* %6 to i8*
  call void @aom_free(i8* %7)
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !6
  %default_frame_context2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %8, i32 0, i32 39
  store %struct.frame_contexts* null, %struct.frame_contexts** %default_frame_context2, align 4, !tbaa !80
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_init_mi_buffers(%struct.CommonModeInfoParams* %mi_params) #0 {
entry:
  %mi_params.addr = alloca %struct.CommonModeInfoParams*, align 4
  store %struct.CommonModeInfoParams* %mi_params, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %0 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  %setup_mi = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %0, i32 0, i32 14
  %1 = load void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)** %setup_mi, align 4, !tbaa !81
  %2 = load %struct.CommonModeInfoParams*, %struct.CommonModeInfoParams** %mi_params.addr, align 4, !tbaa !6
  call void %1(%struct.CommonModeInfoParams* %2)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @calc_mi_size(i32 %len) #3 {
entry:
  %len.addr = alloca i32, align 4
  store i32 %len, i32* %len.addr, align 4, !tbaa !2
  %0 = load i32, i32* %len.addr, align 4, !tbaa !2
  %add = add nsw i32 %0, 31
  %and = and i32 %add, -32
  ret i32 %and
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"any pointer", !4, i64 0}
!8 = !{!9, !3, i64 0}
!9 = !{!"RefCntBuffer", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 36, !4, i64 40, !7, i64 68, !7, i64 72, !10, i64 76, !3, i64 248, !3, i64 252, !3, i64 256, !3, i64 260, !4, i64 264, !3, i64 616, !4, i64 620, !11, i64 624, !13, i64 1272, !15, i64 1284, !4, i64 1432, !4, i64 1436, !4, i64 1452, !4, i64 1460, !16, i64 1464}
!10 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !3, i64 164, !4, i64 168}
!11 = !{!"", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 120, !4, i64 124, !3, i64 204, !4, i64 208, !3, i64 288, !3, i64 292, !3, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !3, i64 596, !3, i64 600, !3, i64 604, !3, i64 608, !3, i64 612, !3, i64 616, !3, i64 620, !3, i64 624, !3, i64 628, !3, i64 632, !3, i64 636, !3, i64 640, !12, i64 644}
!12 = !{!"short", !4, i64 0}
!13 = !{!"aom_codec_frame_buffer", !7, i64 0, !14, i64 4, !7, i64 8}
!14 = !{!"long", !4, i64 0}
!15 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !3, i64 52, !4, i64 56, !7, i64 68, !3, i64 72, !7, i64 76, !14, i64 80, !3, i64 84, !14, i64 88, !3, i64 92, !3, i64 96, !3, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !3, i64 128, !3, i64 132, !3, i64 136, !3, i64 140, !7, i64 144}
!16 = !{!"frame_contexts", !4, i64 0, !4, i64 390, !4, i64 930, !4, i64 966, !4, i64 1014, !4, i64 1070, !4, i64 1134, !4, i64 1206, !4, i64 1286, !4, i64 1374, !4, i64 1470, !4, i64 1790, !4, i64 5990, !4, i64 8090, !4, i64 8126, !4, i64 8138, !4, i64 8174, !4, i64 8192, !4, i64 8336, !4, i64 8468, !4, i64 9216, !4, i64 9240, !4, i64 9372, !4, i64 9412, !4, i64 9588, !4, i64 9720, !4, i64 9832, !4, i64 9944, !4, i64 10574, !4, i64 11204, !4, i64 11330, !4, i64 11342, !4, i64 11372, !4, i64 11480, !4, i64 11510, !4, i64 11564, !4, i64 11618, !4, i64 11654, !4, i64 11780, !4, i64 11816, !4, i64 11852, !4, i64 11870, !4, i64 11888, !17, i64 11912, !17, i64 12198, !4, i64 12484, !18, i64 12490, !4, i64 12580, !4, i64 12712, !4, i64 12724, !4, i64 12732, !4, i64 12738, !4, i64 12744, !4, i64 12856, !4, i64 13636, !4, i64 14076, !4, i64 14204, !4, i64 14904, !4, i64 15032, !4, i64 15128, !4, i64 15138, !4, i64 15178, !4, i64 15188, !4, i64 20492, !4, i64 21036, !4, i64 21054, !3, i64 21260}
!17 = !{!"", !4, i64 0, !4, i64 10}
!18 = !{!"segmentation_probs", !4, i64 0, !4, i64 18, !4, i64 36}
!19 = !{!9, !7, i64 1272}
!20 = !{!21, !7, i64 8}
!21 = !{!"BufferPool", !7, i64 0, !7, i64 4, !7, i64 8, !4, i64 12, !22, i64 363660}
!22 = !{!"InternalFrameBufferList", !3, i64 0, !7, i64 4}
!23 = !{!21, !7, i64 0}
!24 = !{!9, !14, i64 1276}
!25 = !{!9, !7, i64 1280}
!26 = !{!9, !7, i64 68}
!27 = !{!9, !7, i64 72}
!28 = !{!29, !7, i64 14872}
!29 = !{!"AV1Common", !30, i64 0, !32, i64 40, !3, i64 288, !3, i64 292, !3, i64 296, !3, i64 300, !3, i64 304, !3, i64 308, !4, i64 312, !33, i64 313, !4, i64 316, !3, i64 448, !7, i64 452, !7, i64 456, !4, i64 460, !34, i64 492, !4, i64 580, !4, i64 1284, !3, i64 1316, !3, i64 1320, !3, i64 1324, !35, i64 1328, !36, i64 1356, !37, i64 1420, !10, i64 10676, !7, i64 10848, !38, i64 10864, !39, i64 14704, !4, i64 14740, !7, i64 14872, !7, i64 14876, !15, i64 14880, !40, i64 15028, !11, i64 15168, !41, i64 15816, !4, i64 15836, !42, i64 16192, !7, i64 18128, !7, i64 18132, !45, i64 18136, !7, i64 18724, !46, i64 18728, !3, i64 18760, !4, i64 18764, !7, i64 18796, !3, i64 18800, !4, i64 18804, !4, i64 18836, !3, i64 18844, !3, i64 18848, !3, i64 18852, !3, i64 18856}
!30 = !{!"", !4, i64 0, !4, i64 1, !3, i64 4, !3, i64 8, !3, i64 12, !31, i64 16, !3, i64 32, !3, i64 36}
!31 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!32 = !{!"aom_internal_error_info", !4, i64 0, !3, i64 4, !4, i64 8, !3, i64 88, !4, i64 92}
!33 = !{!"_Bool", !4, i64 0}
!34 = !{!"scale_factors", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56}
!35 = !{!"", !33, i64 0, !33, i64 1, !33, i64 2, !33, i64 3, !33, i64 4, !33, i64 5, !33, i64 6, !33, i64 7, !33, i64 8, !33, i64 9, !33, i64 10, !33, i64 11, !4, i64 12, !4, i64 13, !3, i64 16, !3, i64 20, !4, i64 24}
!36 = !{!"CommonModeInfoParams", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !3, i64 24, !3, i64 28, !4, i64 32, !7, i64 36, !3, i64 40, !3, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60}
!37 = !{!"CommonQuantParams", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !33, i64 9240, !3, i64 9244, !3, i64 9248, !3, i64 9252}
!38 = !{!"", !4, i64 0, !4, i64 3072}
!39 = !{!"loopfilter", !4, i64 0, !3, i64 8, !3, i64 12, !3, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !3, i64 32}
!40 = !{!"", !3, i64 0, !3, i64 4, !4, i64 8, !4, i64 72, !3, i64 136}
!41 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!42 = !{!"SequenceHeader", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !4, i64 16, !3, i64 20, !3, i64 24, !4, i64 28, !3, i64 32, !3, i64 36, !31, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !3, i64 92, !3, i64 96, !3, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !3, i64 112, !4, i64 116, !3, i64 244, !43, i64 248, !4, i64 264, !44, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!43 = !{!"aom_timing", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!44 = !{!"aom_dec_model_info", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!45 = !{!"CommonTileParams", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !4, i64 60, !4, i64 320, !3, i64 580, !3, i64 584}
!46 = !{!"CommonContexts", !7, i64 0, !4, i64 4, !7, i64 16, !3, i64 20, !3, i64 24, !3, i64 28}
!47 = !{!29, !7, i64 14876}
!48 = !{!29, !3, i64 18140}
!49 = !{!50, !3, i64 4}
!50 = !{!"TileInfo", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20}
!51 = !{!50, !3, i64 0}
!52 = !{!29, !3, i64 304}
!53 = !{!29, !4, i64 16268}
!54 = !{!29, !3, i64 16288}
!55 = !{!56, !3, i64 12}
!56 = !{!"", !7, i64 0, !7, i64 4, !3, i64 8, !3, i64 12}
!57 = !{!56, !7, i64 0}
!58 = !{!56, !7, i64 4}
!59 = !{!56, !3, i64 8}
!60 = !{!29, !4, i64 16269}
!61 = !{!46, !3, i64 20}
!62 = !{!46, !3, i64 24}
!63 = !{!46, !7, i64 0}
!64 = !{!46, !7, i64 16}
!65 = !{!46, !3, i64 28}
!66 = !{!29, !7, i64 1408}
!67 = !{!36, !7, i64 60}
!68 = !{!36, !3, i64 12}
!69 = !{!36, !3, i64 44}
!70 = !{!36, !4, i64 32}
!71 = !{!4, !4, i64 0}
!72 = !{!36, !3, i64 28}
!73 = !{!36, !3, i64 24}
!74 = !{!36, !3, i64 40}
!75 = !{!36, !7, i64 52}
!76 = !{!36, !7, i64 20}
!77 = !{!36, !7, i64 36}
!78 = !{!36, !7, i64 48}
!79 = !{!29, !7, i64 18128}
!80 = !{!29, !7, i64 18132}
!81 = !{!36, !7, i64 56}
