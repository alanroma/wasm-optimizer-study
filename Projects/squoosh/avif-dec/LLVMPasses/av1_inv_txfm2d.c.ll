; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/av1_inv_txfm2d.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/av1_inv_txfm2d.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.TXFM_2D_FLIP_CFG = type { i8, i32, i32, i8*, i8, i8, [12 x i8], [12 x i8], i8, i8, i32, i32 }

@inv_shift_4x4 = internal constant [2 x i8] c"\00\FC", align 1
@inv_shift_8x8 = internal constant [2 x i8] c"\FF\FC", align 1
@inv_shift_16x16 = internal constant [2 x i8] c"\FE\FC", align 1
@inv_shift_32x32 = internal constant [2 x i8] c"\FE\FC", align 1
@inv_shift_64x64 = internal constant [2 x i8] c"\FE\FC", align 1
@inv_shift_4x8 = internal constant [2 x i8] c"\00\FC", align 1
@inv_shift_8x4 = internal constant [2 x i8] c"\00\FC", align 1
@inv_shift_8x16 = internal constant [2 x i8] c"\FF\FC", align 1
@inv_shift_16x8 = internal constant [2 x i8] c"\FF\FC", align 1
@inv_shift_16x32 = internal constant [2 x i8] c"\FF\FC", align 1
@inv_shift_32x16 = internal constant [2 x i8] c"\FF\FC", align 1
@inv_shift_32x64 = internal constant [2 x i8] c"\FF\FC", align 1
@inv_shift_64x32 = internal constant [2 x i8] c"\FF\FC", align 1
@inv_shift_4x16 = internal constant [2 x i8] c"\FF\FC", align 1
@inv_shift_16x4 = internal constant [2 x i8] c"\FF\FC", align 1
@inv_shift_8x32 = internal constant [2 x i8] c"\FE\FC", align 1
@inv_shift_32x8 = internal constant [2 x i8] c"\FE\FC", align 1
@inv_shift_16x64 = internal constant [2 x i8] c"\FE\FC", align 1
@inv_shift_64x16 = internal constant [2 x i8] c"\FE\FC", align 1
@av1_inv_txfm_shift_ls = hidden global [19 x i8*] [i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_4x4, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_8x8, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_16x16, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_32x32, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_64x64, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_4x8, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_8x4, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_8x16, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_16x8, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_16x32, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_32x16, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_32x64, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_64x32, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_4x16, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_16x4, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_8x32, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_32x8, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_16x64, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @inv_shift_64x16, i32 0, i32 0)], align 16
@av1_inv_cos_bit_col = hidden constant [5 x [5 x i8]] [[5 x i8] c"\0C\0C\0C\00\00", [5 x i8] c"\0C\0C\0C\0C\00", [5 x i8] c"\0C\0C\0C\0C\0C", [5 x i8] c"\00\0C\0C\0C\0C", [5 x i8] c"\00\00\0C\0C\0C"], align 16
@av1_inv_cos_bit_row = hidden constant [5 x [5 x i8]] [[5 x i8] c"\0C\0C\0C\00\00", [5 x i8] c"\0C\0C\0C\0C\00", [5 x i8] c"\0C\0C\0C\0C\0C", [5 x i8] c"\00\0C\0C\0C\0C", [5 x i8] c"\00\00\0C\0C\0C"], align 16
@vtx_tab = internal constant [16 x i8] c"\00\01\00\01\02\00\02\01\02\03\00\03\01\03\02\03", align 16
@htx_tab = internal constant [16 x i8] c"\00\00\01\01\00\02\02\02\01\03\03\00\03\01\03\02", align 16
@av1_txfm_type_ls = external constant [5 x [4 x i8]], align 16
@iadst4_range = internal constant [7 x i8] c"\00\01\00\00\00\00\00", align 1
@av1_txfm_stage_num_list = external constant [12 x i8], align 1
@inv_start_range = internal constant [19 x i8] c"\05\06\07\07\07\05\05\06\06\06\06\06\06\06\06\07\07\07\07", align 16
@tx_size_wide_log2 = internal constant [19 x i32] [i32 2, i32 3, i32 4, i32 5, i32 6, i32 2, i32 3, i32 3, i32 4, i32 4, i32 5, i32 5, i32 6, i32 2, i32 4, i32 3, i32 5, i32 4, i32 6], align 16
@tx_size_high_log2 = internal constant [19 x i32] [i32 2, i32 3, i32 4, i32 5, i32 6, i32 3, i32 2, i32 4, i32 3, i32 5, i32 4, i32 6, i32 5, i32 4, i32 2, i32 5, i32 3, i32 6, i32 4], align 16
@tx_size_wide = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 4, i32 8, i32 8, i32 16, i32 16, i32 32, i32 32, i32 64, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64], align 16
@tx_size_high = internal constant [19 x i32] [i32 4, i32 8, i32 16, i32 32, i32 64, i32 8, i32 4, i32 16, i32 8, i32 32, i32 16, i32 64, i32 32, i32 16, i32 4, i32 32, i32 8, i32 64, i32 16], align 16

; Function Attrs: nounwind
define hidden void @av1_highbd_iwht4x4_16_add_c(i32* %input, i8* %dest8, i32 %stride, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %dest8.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %output = alloca [16 x i32], align 16
  %a1 = alloca i32, align 4
  %b1 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %d1 = alloca i32, align 4
  %e1 = alloca i32, align 4
  %ip = alloca i32*, align 4
  %op = alloca i32*, align 4
  %dest = alloca i16*, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i8* %dest8, i8** %dest8.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast [16 x i32]* %output to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %1) #6
  %2 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = bitcast i32* %d1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = bitcast i32** %ip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i32*, i32** %input.addr, align 4, !tbaa !2
  store i32* %8, i32** %ip, align 4, !tbaa !2
  %9 = bitcast i32** %op to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %arraydecay = getelementptr inbounds [16 x i32], [16 x i32]* %output, i32 0, i32 0
  store i32* %arraydecay, i32** %op, align 4, !tbaa !2
  %10 = bitcast i16** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load i8*, i8** %dest8.addr, align 4, !tbaa !2
  %12 = ptrtoint i8* %11 to i32
  %shl = shl i32 %12, 1
  %13 = inttoptr i32 %shl to i16*
  store i16* %13, i16** %dest, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %14, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 0
  %16 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %shr = ashr i32 %16, 2
  store i32 %shr, i32* %a1, align 4, !tbaa !6
  %17 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %17, i32 1
  %18 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %shr2 = ashr i32 %18, 2
  store i32 %shr2, i32* %c1, align 4, !tbaa !6
  %19 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %19, i32 2
  %20 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %shr4 = ashr i32 %20, 2
  store i32 %shr4, i32* %d1, align 4, !tbaa !6
  %21 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %21, i32 3
  %22 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %shr6 = ashr i32 %22, 2
  store i32 %shr6, i32* %b1, align 4, !tbaa !6
  %23 = load i32, i32* %c1, align 4, !tbaa !6
  %24 = load i32, i32* %a1, align 4, !tbaa !6
  %add = add nsw i32 %24, %23
  store i32 %add, i32* %a1, align 4, !tbaa !6
  %25 = load i32, i32* %b1, align 4, !tbaa !6
  %26 = load i32, i32* %d1, align 4, !tbaa !6
  %sub = sub nsw i32 %26, %25
  store i32 %sub, i32* %d1, align 4, !tbaa !6
  %27 = load i32, i32* %a1, align 4, !tbaa !6
  %28 = load i32, i32* %d1, align 4, !tbaa !6
  %sub7 = sub nsw i32 %27, %28
  %shr8 = ashr i32 %sub7, 1
  store i32 %shr8, i32* %e1, align 4, !tbaa !6
  %29 = load i32, i32* %e1, align 4, !tbaa !6
  %30 = load i32, i32* %b1, align 4, !tbaa !6
  %sub9 = sub nsw i32 %29, %30
  store i32 %sub9, i32* %b1, align 4, !tbaa !6
  %31 = load i32, i32* %e1, align 4, !tbaa !6
  %32 = load i32, i32* %c1, align 4, !tbaa !6
  %sub10 = sub nsw i32 %31, %32
  store i32 %sub10, i32* %c1, align 4, !tbaa !6
  %33 = load i32, i32* %b1, align 4, !tbaa !6
  %34 = load i32, i32* %a1, align 4, !tbaa !6
  %sub11 = sub nsw i32 %34, %33
  store i32 %sub11, i32* %a1, align 4, !tbaa !6
  %35 = load i32, i32* %c1, align 4, !tbaa !6
  %36 = load i32, i32* %d1, align 4, !tbaa !6
  %add12 = add nsw i32 %36, %35
  store i32 %add12, i32* %d1, align 4, !tbaa !6
  %37 = load i32, i32* %a1, align 4, !tbaa !6
  %38 = load i32*, i32** %op, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %38, i32 0
  store i32 %37, i32* %arrayidx13, align 4, !tbaa !6
  %39 = load i32, i32* %b1, align 4, !tbaa !6
  %40 = load i32*, i32** %op, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i32, i32* %40, i32 1
  store i32 %39, i32* %arrayidx14, align 4, !tbaa !6
  %41 = load i32, i32* %c1, align 4, !tbaa !6
  %42 = load i32*, i32** %op, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i32, i32* %42, i32 2
  store i32 %41, i32* %arrayidx15, align 4, !tbaa !6
  %43 = load i32, i32* %d1, align 4, !tbaa !6
  %44 = load i32*, i32** %op, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %44, i32 3
  store i32 %43, i32* %arrayidx16, align 4, !tbaa !6
  %45 = load i32*, i32** %ip, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %45, i32 4
  store i32* %add.ptr, i32** %ip, align 4, !tbaa !2
  %46 = load i32*, i32** %op, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds i32, i32* %46, i32 4
  store i32* %add.ptr17, i32** %op, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay18 = getelementptr inbounds [16 x i32], [16 x i32]* %output, i32 0, i32 0
  store i32* %arraydecay18, i32** %ip, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc68, %for.end
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %cmp20 = icmp slt i32 %48, 4
  br i1 %cmp20, label %for.body21, label %for.end70

for.body21:                                       ; preds = %for.cond19
  %49 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i32, i32* %49, i32 0
  %50 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  store i32 %50, i32* %a1, align 4, !tbaa !6
  %51 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %51, i32 4
  %52 = load i32, i32* %arrayidx23, align 4, !tbaa !6
  store i32 %52, i32* %c1, align 4, !tbaa !6
  %53 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %53, i32 8
  %54 = load i32, i32* %arrayidx24, align 4, !tbaa !6
  store i32 %54, i32* %d1, align 4, !tbaa !6
  %55 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %55, i32 12
  %56 = load i32, i32* %arrayidx25, align 4, !tbaa !6
  store i32 %56, i32* %b1, align 4, !tbaa !6
  %57 = load i32, i32* %c1, align 4, !tbaa !6
  %58 = load i32, i32* %a1, align 4, !tbaa !6
  %add26 = add nsw i32 %58, %57
  store i32 %add26, i32* %a1, align 4, !tbaa !6
  %59 = load i32, i32* %b1, align 4, !tbaa !6
  %60 = load i32, i32* %d1, align 4, !tbaa !6
  %sub27 = sub nsw i32 %60, %59
  store i32 %sub27, i32* %d1, align 4, !tbaa !6
  %61 = load i32, i32* %a1, align 4, !tbaa !6
  %62 = load i32, i32* %d1, align 4, !tbaa !6
  %sub28 = sub nsw i32 %61, %62
  %shr29 = ashr i32 %sub28, 1
  store i32 %shr29, i32* %e1, align 4, !tbaa !6
  %63 = load i32, i32* %e1, align 4, !tbaa !6
  %64 = load i32, i32* %b1, align 4, !tbaa !6
  %sub30 = sub nsw i32 %63, %64
  store i32 %sub30, i32* %b1, align 4, !tbaa !6
  %65 = load i32, i32* %e1, align 4, !tbaa !6
  %66 = load i32, i32* %c1, align 4, !tbaa !6
  %sub31 = sub nsw i32 %65, %66
  store i32 %sub31, i32* %c1, align 4, !tbaa !6
  %67 = load i32, i32* %b1, align 4, !tbaa !6
  %68 = load i32, i32* %a1, align 4, !tbaa !6
  %sub32 = sub nsw i32 %68, %67
  store i32 %sub32, i32* %a1, align 4, !tbaa !6
  %69 = load i32, i32* %c1, align 4, !tbaa !6
  %70 = load i32, i32* %d1, align 4, !tbaa !6
  %add33 = add nsw i32 %70, %69
  store i32 %add33, i32* %d1, align 4, !tbaa !6
  %71 = load i32, i32* %a1, align 4, !tbaa !6
  %72 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add34 = add nsw i32 %72, 1
  %conv = trunc i32 %add34 to i8
  %call = call i32 @range_check_value(i32 %71, i8 signext %conv)
  %73 = load i32, i32* %b1, align 4, !tbaa !6
  %74 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add35 = add nsw i32 %74, 1
  %conv36 = trunc i32 %add35 to i8
  %call37 = call i32 @range_check_value(i32 %73, i8 signext %conv36)
  %75 = load i32, i32* %c1, align 4, !tbaa !6
  %76 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add38 = add nsw i32 %76, 1
  %conv39 = trunc i32 %add38 to i8
  %call40 = call i32 @range_check_value(i32 %75, i8 signext %conv39)
  %77 = load i32, i32* %d1, align 4, !tbaa !6
  %78 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add41 = add nsw i32 %78, 1
  %conv42 = trunc i32 %add41 to i8
  %call43 = call i32 @range_check_value(i32 %77, i8 signext %conv42)
  %79 = load i16*, i16** %dest, align 4, !tbaa !2
  %80 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %80, 0
  %arrayidx44 = getelementptr inbounds i16, i16* %79, i32 %mul
  %81 = load i16, i16* %arrayidx44, align 2, !tbaa !8
  %82 = load i32, i32* %a1, align 4, !tbaa !6
  %conv45 = sext i32 %82 to i64
  %83 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call46 = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %81, i64 %conv45, i32 %83)
  %84 = load i16*, i16** %dest, align 4, !tbaa !2
  %85 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul47 = mul nsw i32 %85, 0
  %arrayidx48 = getelementptr inbounds i16, i16* %84, i32 %mul47
  store i16 %call46, i16* %arrayidx48, align 2, !tbaa !8
  %86 = load i16*, i16** %dest, align 4, !tbaa !2
  %87 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul49 = mul nsw i32 %87, 1
  %arrayidx50 = getelementptr inbounds i16, i16* %86, i32 %mul49
  %88 = load i16, i16* %arrayidx50, align 2, !tbaa !8
  %89 = load i32, i32* %b1, align 4, !tbaa !6
  %conv51 = sext i32 %89 to i64
  %90 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call52 = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %88, i64 %conv51, i32 %90)
  %91 = load i16*, i16** %dest, align 4, !tbaa !2
  %92 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul53 = mul nsw i32 %92, 1
  %arrayidx54 = getelementptr inbounds i16, i16* %91, i32 %mul53
  store i16 %call52, i16* %arrayidx54, align 2, !tbaa !8
  %93 = load i16*, i16** %dest, align 4, !tbaa !2
  %94 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul55 = mul nsw i32 %94, 2
  %arrayidx56 = getelementptr inbounds i16, i16* %93, i32 %mul55
  %95 = load i16, i16* %arrayidx56, align 2, !tbaa !8
  %96 = load i32, i32* %c1, align 4, !tbaa !6
  %conv57 = sext i32 %96 to i64
  %97 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call58 = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %95, i64 %conv57, i32 %97)
  %98 = load i16*, i16** %dest, align 4, !tbaa !2
  %99 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul59 = mul nsw i32 %99, 2
  %arrayidx60 = getelementptr inbounds i16, i16* %98, i32 %mul59
  store i16 %call58, i16* %arrayidx60, align 2, !tbaa !8
  %100 = load i16*, i16** %dest, align 4, !tbaa !2
  %101 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul61 = mul nsw i32 %101, 3
  %arrayidx62 = getelementptr inbounds i16, i16* %100, i32 %mul61
  %102 = load i16, i16* %arrayidx62, align 2, !tbaa !8
  %103 = load i32, i32* %d1, align 4, !tbaa !6
  %conv63 = sext i32 %103 to i64
  %104 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call64 = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %102, i64 %conv63, i32 %104)
  %105 = load i16*, i16** %dest, align 4, !tbaa !2
  %106 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul65 = mul nsw i32 %106, 3
  %arrayidx66 = getelementptr inbounds i16, i16* %105, i32 %mul65
  store i16 %call64, i16* %arrayidx66, align 2, !tbaa !8
  %107 = load i32*, i32** %ip, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %107, i32 1
  store i32* %incdec.ptr, i32** %ip, align 4, !tbaa !2
  %108 = load i16*, i16** %dest, align 4, !tbaa !2
  %incdec.ptr67 = getelementptr inbounds i16, i16* %108, i32 1
  store i16* %incdec.ptr67, i16** %dest, align 4, !tbaa !2
  br label %for.inc68

for.inc68:                                        ; preds = %for.body21
  %109 = load i32, i32* %i, align 4, !tbaa !6
  %inc69 = add nsw i32 %109, 1
  store i32 %inc69, i32* %i, align 4, !tbaa !6
  br label %for.cond19

for.end70:                                        ; preds = %for.cond19
  %110 = bitcast i16** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast i32** %op to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  %112 = bitcast i32** %ip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #6
  %113 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #6
  %114 = bitcast i32* %d1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #6
  %115 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #6
  %116 = bitcast i32* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #6
  %117 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #6
  %118 = bitcast [16 x i32]* %output to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %118) #6
  %119 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @range_check_value(i32 %value, i8 signext %bit) #2 {
entry:
  %value.addr = alloca i32, align 4
  %bit.addr = alloca i8, align 1
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i8 %bit, i8* %bit.addr, align 1, !tbaa !10
  %0 = load i8, i8* %bit.addr, align 1, !tbaa !10
  %1 = load i32, i32* %value.addr, align 4, !tbaa !6
  ret i32 %1
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @highbd_clip_pixel_add(i16 zeroext %dest, i64 %trans, i32 %bd) #2 {
entry:
  %dest.addr = alloca i16, align 2
  %trans.addr = alloca i64, align 8
  %bd.addr = alloca i32, align 4
  store i16 %dest, i16* %dest.addr, align 2, !tbaa !8
  store i64 %trans, i64* %trans.addr, align 8, !tbaa !11
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16, i16* %dest.addr, align 2, !tbaa !8
  %conv = zext i16 %0 to i32
  %1 = load i64, i64* %trans.addr, align 8, !tbaa !11
  %conv1 = trunc i64 %1 to i32
  %add = add nsw i32 %conv, %conv1
  %2 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call zeroext i16 @clip_pixel_highbd(i32 %add, i32 %2)
  ret i16 %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_highbd_iwht4x4_1_add_c(i32* %in, i8* %dest8, i32 %dest_stride, i32 %bd) #0 {
entry:
  %in.addr = alloca i32*, align 4
  %dest8.addr = alloca i8*, align 4
  %dest_stride.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %a1 = alloca i32, align 4
  %e1 = alloca i32, align 4
  %tmp = alloca [4 x i32], align 16
  %ip = alloca i32*, align 4
  %op = alloca i32*, align 4
  %dest = alloca i16*, align 4
  store i32* %in, i32** %in.addr, align 4, !tbaa !2
  store i8* %dest8, i8** %dest8.addr, align 4, !tbaa !2
  store i32 %dest_stride, i32* %dest_stride.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = bitcast [4 x i32]* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = bitcast i32** %ip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load i32*, i32** %in.addr, align 4, !tbaa !2
  store i32* %5, i32** %ip, align 4, !tbaa !2
  %6 = bitcast i32** %op to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %tmp, i32 0, i32 0
  store i32* %arraydecay, i32** %op, align 4, !tbaa !2
  %7 = bitcast i16** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i8*, i8** %dest8.addr, align 4, !tbaa !2
  %9 = ptrtoint i8* %8 to i32
  %shl = shl i32 %9, 1
  %10 = inttoptr i32 %shl to i16*
  store i16* %10, i16** %dest, align 4, !tbaa !2
  %11 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %12 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %12, i32 0
  %13 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %shr = ashr i32 %13, 2
  store i32 %shr, i32* %a1, align 4, !tbaa !6
  %14 = load i32, i32* %a1, align 4, !tbaa !6
  %shr1 = ashr i32 %14, 1
  store i32 %shr1, i32* %e1, align 4, !tbaa !6
  %15 = load i32, i32* %e1, align 4, !tbaa !6
  %16 = load i32, i32* %a1, align 4, !tbaa !6
  %sub = sub nsw i32 %16, %15
  store i32 %sub, i32* %a1, align 4, !tbaa !6
  %17 = load i32, i32* %a1, align 4, !tbaa !6
  %18 = load i32*, i32** %op, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %18, i32 0
  store i32 %17, i32* %arrayidx2, align 4, !tbaa !6
  %19 = load i32, i32* %e1, align 4, !tbaa !6
  %20 = load i32*, i32** %op, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %20, i32 3
  store i32 %19, i32* %arrayidx3, align 4, !tbaa !6
  %21 = load i32*, i32** %op, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %21, i32 2
  store i32 %19, i32* %arrayidx4, align 4, !tbaa !6
  %22 = load i32*, i32** %op, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %22, i32 1
  store i32 %19, i32* %arrayidx5, align 4, !tbaa !6
  %arraydecay6 = getelementptr inbounds [4 x i32], [4 x i32]* %tmp, i32 0, i32 0
  store i32* %arraydecay6, i32** %ip, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %23, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %24, i32 0
  %25 = load i32, i32* %arrayidx7, align 4, !tbaa !6
  %shr8 = ashr i32 %25, 1
  store i32 %shr8, i32* %e1, align 4, !tbaa !6
  %26 = load i32*, i32** %ip, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %26, i32 0
  %27 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %28 = load i32, i32* %e1, align 4, !tbaa !6
  %sub10 = sub nsw i32 %27, %28
  store i32 %sub10, i32* %a1, align 4, !tbaa !6
  %29 = load i16*, i16** %dest, align 4, !tbaa !2
  %30 = load i32, i32* %dest_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %30, 0
  %arrayidx11 = getelementptr inbounds i16, i16* %29, i32 %mul
  %31 = load i16, i16* %arrayidx11, align 2, !tbaa !8
  %32 = load i32, i32* %a1, align 4, !tbaa !6
  %conv = sext i32 %32 to i64
  %33 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %31, i64 %conv, i32 %33)
  %34 = load i16*, i16** %dest, align 4, !tbaa !2
  %35 = load i32, i32* %dest_stride.addr, align 4, !tbaa !6
  %mul12 = mul nsw i32 %35, 0
  %arrayidx13 = getelementptr inbounds i16, i16* %34, i32 %mul12
  store i16 %call, i16* %arrayidx13, align 2, !tbaa !8
  %36 = load i16*, i16** %dest, align 4, !tbaa !2
  %37 = load i32, i32* %dest_stride.addr, align 4, !tbaa !6
  %mul14 = mul nsw i32 %37, 1
  %arrayidx15 = getelementptr inbounds i16, i16* %36, i32 %mul14
  %38 = load i16, i16* %arrayidx15, align 2, !tbaa !8
  %39 = load i32, i32* %e1, align 4, !tbaa !6
  %conv16 = sext i32 %39 to i64
  %40 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call17 = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %38, i64 %conv16, i32 %40)
  %41 = load i16*, i16** %dest, align 4, !tbaa !2
  %42 = load i32, i32* %dest_stride.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 %42, 1
  %arrayidx19 = getelementptr inbounds i16, i16* %41, i32 %mul18
  store i16 %call17, i16* %arrayidx19, align 2, !tbaa !8
  %43 = load i16*, i16** %dest, align 4, !tbaa !2
  %44 = load i32, i32* %dest_stride.addr, align 4, !tbaa !6
  %mul20 = mul nsw i32 %44, 2
  %arrayidx21 = getelementptr inbounds i16, i16* %43, i32 %mul20
  %45 = load i16, i16* %arrayidx21, align 2, !tbaa !8
  %46 = load i32, i32* %e1, align 4, !tbaa !6
  %conv22 = sext i32 %46 to i64
  %47 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call23 = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %45, i64 %conv22, i32 %47)
  %48 = load i16*, i16** %dest, align 4, !tbaa !2
  %49 = load i32, i32* %dest_stride.addr, align 4, !tbaa !6
  %mul24 = mul nsw i32 %49, 2
  %arrayidx25 = getelementptr inbounds i16, i16* %48, i32 %mul24
  store i16 %call23, i16* %arrayidx25, align 2, !tbaa !8
  %50 = load i16*, i16** %dest, align 4, !tbaa !2
  %51 = load i32, i32* %dest_stride.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %51, 3
  %arrayidx27 = getelementptr inbounds i16, i16* %50, i32 %mul26
  %52 = load i16, i16* %arrayidx27, align 2, !tbaa !8
  %53 = load i32, i32* %e1, align 4, !tbaa !6
  %conv28 = sext i32 %53 to i64
  %54 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call29 = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %52, i64 %conv28, i32 %54)
  %55 = load i16*, i16** %dest, align 4, !tbaa !2
  %56 = load i32, i32* %dest_stride.addr, align 4, !tbaa !6
  %mul30 = mul nsw i32 %56, 3
  %arrayidx31 = getelementptr inbounds i16, i16* %55, i32 %mul30
  store i16 %call29, i16* %arrayidx31, align 2, !tbaa !8
  %57 = load i32*, i32** %ip, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %57, i32 1
  store i32* %incdec.ptr, i32** %ip, align 4, !tbaa !2
  %58 = load i16*, i16** %dest, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds i16, i16* %58, i32 1
  store i16* %incdec.ptr32, i16** %dest, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %59, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %60 = bitcast i16** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32** %op to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i32** %ip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast [4 x i32]* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %63) #6
  %64 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  %66 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_get_inv_txfm_cfg(i8 zeroext %tx_type, i8 zeroext %tx_size, %struct.TXFM_2D_FLIP_CFG* %cfg) #0 {
entry:
  %tx_type.addr = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %cfg.addr = alloca %struct.TXFM_2D_FLIP_CFG*, align 4
  %tx_type_1d_col = alloca i8, align 1
  %tx_type_1d_row = alloca i8, align 1
  %txw_idx = alloca i32, align 4
  %txh_idx = alloca i32, align 4
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store %struct.TXFM_2D_FLIP_CFG* %cfg, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %1 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %tx_size1 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %1, i32 0, i32 0
  store i8 %0, i8* %tx_size1, align 4, !tbaa !13
  %2 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_range_col = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %2, i32 0, i32 6
  %3 = bitcast [12 x i8]* %stage_range_col to i8*
  call void @llvm.memset.p0i8.i32(i8* align 2 %3, i8 0, i32 12, i1 false)
  %4 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_range_row = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %4, i32 0, i32 7
  %5 = bitcast [12 x i8]* %stage_range_row to i8*
  call void @llvm.memset.p0i8.i32(i8* align 2 %5, i8 0, i32 12, i1 false)
  %6 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %7 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  call void @set_flip_cfg(i8 zeroext %6, %struct.TXFM_2D_FLIP_CFG* %7)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type_1d_col) #6
  %8 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %idxprom = zext i8 %8 to i32
  %arrayidx = getelementptr inbounds [16 x i8], [16 x i8]* @vtx_tab, i32 0, i32 %idxprom
  %9 = load i8, i8* %arrayidx, align 1, !tbaa !10
  store i8 %9, i8* %tx_type_1d_col, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tx_type_1d_row) #6
  %10 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %idxprom2 = zext i8 %10 to i32
  %arrayidx3 = getelementptr inbounds [16 x i8], [16 x i8]* @htx_tab, i32 0, i32 %idxprom2
  %11 = load i8, i8* %arrayidx3, align 1, !tbaa !10
  store i8 %11, i8* %tx_type_1d_row, align 1, !tbaa !10
  %12 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom4 = zext i8 %12 to i32
  %arrayidx5 = getelementptr inbounds [19 x i8*], [19 x i8*]* @av1_inv_txfm_shift_ls, i32 0, i32 %idxprom4
  %13 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  %14 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %shift = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %14, i32 0, i32 3
  store i8* %13, i8** %shift, align 4, !tbaa !15
  %15 = bitcast i32* %txw_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %call = call i32 @get_txw_idx(i8 zeroext %16)
  store i32 %call, i32* %txw_idx, align 4, !tbaa !6
  %17 = bitcast i32* %txh_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %18 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %call6 = call i32 @get_txh_idx(i8 zeroext %18)
  store i32 %call6, i32* %txh_idx, align 4, !tbaa !6
  %19 = load i32, i32* %txw_idx, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [5 x [5 x i8]], [5 x [5 x i8]]* @av1_inv_cos_bit_col, i32 0, i32 %19
  %20 = load i32, i32* %txh_idx, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds [5 x i8], [5 x i8]* %arrayidx7, i32 0, i32 %20
  %21 = load i8, i8* %arrayidx8, align 1, !tbaa !10
  %22 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %cos_bit_col = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %22, i32 0, i32 4
  store i8 %21, i8* %cos_bit_col, align 4, !tbaa !16
  %23 = load i32, i32* %txw_idx, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds [5 x [5 x i8]], [5 x [5 x i8]]* @av1_inv_cos_bit_row, i32 0, i32 %23
  %24 = load i32, i32* %txh_idx, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [5 x i8], [5 x i8]* %arrayidx9, i32 0, i32 %24
  %25 = load i8, i8* %arrayidx10, align 1, !tbaa !10
  %26 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %cos_bit_row = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %26, i32 0, i32 5
  store i8 %25, i8* %cos_bit_row, align 1, !tbaa !17
  %27 = load i32, i32* %txh_idx, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [5 x [4 x i8]], [5 x [4 x i8]]* @av1_txfm_type_ls, i32 0, i32 %27
  %28 = load i8, i8* %tx_type_1d_col, align 1, !tbaa !10
  %idxprom12 = zext i8 %28 to i32
  %arrayidx13 = getelementptr inbounds [4 x i8], [4 x i8]* %arrayidx11, i32 0, i32 %idxprom12
  %29 = load i8, i8* %arrayidx13, align 1, !tbaa !10
  %30 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_col = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %30, i32 0, i32 8
  store i8 %29, i8* %txfm_type_col, align 2, !tbaa !18
  %31 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_col14 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %31, i32 0, i32 8
  %32 = load i8, i8* %txfm_type_col14, align 2, !tbaa !18
  %conv = zext i8 %32 to i32
  %cmp = icmp eq i32 %conv, 5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %33 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_range_col16 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %33, i32 0, i32 6
  %arraydecay = getelementptr inbounds [12 x i8], [12 x i8]* %stage_range_col16, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %arraydecay, i8* align 1 getelementptr inbounds ([7 x i8], [7 x i8]* @iadst4_range, i32 0, i32 0), i32 7, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %34 = load i32, i32* %txw_idx, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [5 x [4 x i8]], [5 x [4 x i8]]* @av1_txfm_type_ls, i32 0, i32 %34
  %35 = load i8, i8* %tx_type_1d_row, align 1, !tbaa !10
  %idxprom18 = zext i8 %35 to i32
  %arrayidx19 = getelementptr inbounds [4 x i8], [4 x i8]* %arrayidx17, i32 0, i32 %idxprom18
  %36 = load i8, i8* %arrayidx19, align 1, !tbaa !10
  %37 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_row = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %37, i32 0, i32 9
  store i8 %36, i8* %txfm_type_row, align 1, !tbaa !19
  %38 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_row20 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %38, i32 0, i32 9
  %39 = load i8, i8* %txfm_type_row20, align 1, !tbaa !19
  %conv21 = zext i8 %39 to i32
  %cmp22 = icmp eq i32 %conv21, 5
  br i1 %cmp22, label %if.then24, label %if.end27

if.then24:                                        ; preds = %if.end
  %40 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_range_row25 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %40, i32 0, i32 7
  %arraydecay26 = getelementptr inbounds [12 x i8], [12 x i8]* %stage_range_row25, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %arraydecay26, i8* align 1 getelementptr inbounds ([7 x i8], [7 x i8]* @iadst4_range, i32 0, i32 0), i32 7, i1 false)
  br label %if.end27

if.end27:                                         ; preds = %if.then24, %if.end
  %41 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_col28 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %41, i32 0, i32 8
  %42 = load i8, i8* %txfm_type_col28, align 2, !tbaa !18
  %idxprom29 = zext i8 %42 to i32
  %arrayidx30 = getelementptr inbounds [12 x i8], [12 x i8]* @av1_txfm_stage_num_list, i32 0, i32 %idxprom29
  %43 = load i8, i8* %arrayidx30, align 1, !tbaa !10
  %conv31 = sext i8 %43 to i32
  %44 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_num_col = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %44, i32 0, i32 10
  store i32 %conv31, i32* %stage_num_col, align 4, !tbaa !20
  %45 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_row32 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %45, i32 0, i32 9
  %46 = load i8, i8* %txfm_type_row32, align 1, !tbaa !19
  %idxprom33 = zext i8 %46 to i32
  %arrayidx34 = getelementptr inbounds [12 x i8], [12 x i8]* @av1_txfm_stage_num_list, i32 0, i32 %idxprom33
  %47 = load i8, i8* %arrayidx34, align 1, !tbaa !10
  %conv35 = sext i8 %47 to i32
  %48 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_num_row = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %48, i32 0, i32 11
  store i32 %conv35, i32* %stage_num_row, align 4, !tbaa !21
  %49 = bitcast i32* %txh_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  %50 = bitcast i32* %txw_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type_1d_row) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tx_type_1d_col) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: inlinehint nounwind
define internal void @set_flip_cfg(i8 zeroext %tx_type, %struct.TXFM_2D_FLIP_CFG* %cfg) #2 {
entry:
  %tx_type.addr = alloca i8, align 1
  %cfg.addr = alloca %struct.TXFM_2D_FLIP_CFG*, align 4
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store %struct.TXFM_2D_FLIP_CFG* %cfg, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %0 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %1 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %ud_flip = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %1, i32 0, i32 1
  %2 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %lr_flip = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %2, i32 0, i32 2
  call void @get_flip_cfg(i8 zeroext %0, i32* %ud_flip, i32* %lr_flip)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_txw_idx(i8 zeroext %tx_size) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_log2, i32 0, i32 %idxprom
  %1 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %2 = load i32, i32* getelementptr inbounds ([19 x i32], [19 x i32]* @tx_size_wide_log2, i32 0, i32 0), align 16, !tbaa !6
  %sub = sub nsw i32 %1, %2
  ret i32 %sub
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_txh_idx(i8 zeroext %tx_size) #2 {
entry:
  %tx_size.addr = alloca i8, align 1
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  %0 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_log2, i32 0, i32 %idxprom
  %1 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %2 = load i32, i32* getelementptr inbounds ([19 x i32], [19 x i32]* @tx_size_high_log2, i32 0, i32 0), align 16, !tbaa !6
  %sub = sub nsw i32 %1, %2
  ret i32 %sub
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden void @av1_gen_inv_stage_range(i8* %stage_range_col, i8* %stage_range_row, %struct.TXFM_2D_FLIP_CFG* %cfg, i8 zeroext %tx_size, i32 %bd) #0 {
entry:
  %stage_range_col.addr = alloca i8*, align 4
  %stage_range_row.addr = alloca i8*, align 4
  %cfg.addr = alloca %struct.TXFM_2D_FLIP_CFG*, align 4
  %tx_size.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %fwd_shift = alloca i32, align 4
  %shift = alloca i8*, align 4
  %opt_range_row = alloca i8, align 1
  %opt_range_col = alloca i8, align 1
  %i = alloca i32, align 4
  %real_range_row = alloca i32, align 4
  %i27 = alloca i32, align 4
  %real_range_col = alloca i32, align 4
  store i8* %stage_range_col, i8** %stage_range_col.addr, align 4, !tbaa !2
  store i8* %stage_range_row, i8** %stage_range_row.addr, align 4, !tbaa !2
  store %struct.TXFM_2D_FLIP_CFG* %cfg, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %fwd_shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [19 x i8], [19 x i8]* @inv_start_range, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = sext i8 %2 to i32
  store i32 %conv, i32* %fwd_shift, align 4, !tbaa !6
  %3 = bitcast i8** %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %shift1 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %4, i32 0, i32 3
  %5 = load i8*, i8** %shift1, align 4, !tbaa !15
  store i8* %5, i8** %shift, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %opt_range_row) #6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %opt_range_col) #6
  %6 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %6, 8
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i8 16, i8* %opt_range_row, align 1, !tbaa !10
  store i8 16, i8* %opt_range_col, align 1, !tbaa !10
  br label %if.end7

if.else:                                          ; preds = %entry
  %7 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %cmp3 = icmp eq i32 %7, 10
  br i1 %cmp3, label %if.then5, label %if.else6

if.then5:                                         ; preds = %if.else
  store i8 18, i8* %opt_range_row, align 1, !tbaa !10
  store i8 16, i8* %opt_range_col, align 1, !tbaa !10
  br label %if.end

if.else6:                                         ; preds = %if.else
  store i8 20, i8* %opt_range_row, align 1, !tbaa !10
  store i8 18, i8* %opt_range_col, align 1, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.else6, %if.then5
  br label %if.end7

if.end7:                                          ; preds = %if.end, %if.then
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end7
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_num_row = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %10, i32 0, i32 11
  %11 = load i32, i32* %stage_num_row, align 4, !tbaa !21
  %cmp8 = icmp slt i32 %9, %11
  br i1 %cmp8, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %12, 12
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %13 = phi i1 [ false, %for.cond ], [ %cmp10, %land.rhs ]
  br i1 %13, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %land.end
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  br label %for.end

for.body:                                         ; preds = %land.end
  %15 = bitcast i32* %real_range_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_range_row12 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %16, i32 0, i32 7
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds [12 x i8], [12 x i8]* %stage_range_row12, i32 0, i32 %17
  %18 = load i8, i8* %arrayidx13, align 1, !tbaa !10
  %conv14 = sext i8 %18 to i32
  %19 = load i32, i32* %fwd_shift, align 4, !tbaa !6
  %add = add nsw i32 %conv14, %19
  %20 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add15 = add nsw i32 %add, %20
  %add16 = add nsw i32 %add15, 1
  store i32 %add16, i32* %real_range_row, align 4, !tbaa !6
  %21 = load i32, i32* %real_range_row, align 4, !tbaa !6
  %22 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_row = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %22, i32 0, i32 9
  %23 = load i8, i8* %txfm_type_row, align 1, !tbaa !19
  %conv17 = zext i8 %23 to i32
  %cmp18 = icmp eq i32 %conv17, 5
  br i1 %cmp18, label %land.lhs.true, label %if.else24

land.lhs.true:                                    ; preds = %for.body
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %cmp20 = icmp eq i32 %24, 1
  br i1 %cmp20, label %if.then22, label %if.else24

if.then22:                                        ; preds = %land.lhs.true
  %25 = load i8, i8* %opt_range_row, align 1, !tbaa !10
  %26 = load i8*, i8** %stage_range_row.addr, align 4, !tbaa !2
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds i8, i8* %26, i32 %27
  store i8 %25, i8* %arrayidx23, align 1, !tbaa !10
  br label %if.end26

if.else24:                                        ; preds = %land.lhs.true, %for.body
  %28 = load i8, i8* %opt_range_row, align 1, !tbaa !10
  %29 = load i8*, i8** %stage_range_row.addr, align 4, !tbaa !2
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds i8, i8* %29, i32 %30
  store i8 %28, i8* %arrayidx25, align 1, !tbaa !10
  br label %if.end26

if.end26:                                         ; preds = %if.else24, %if.then22
  %31 = bitcast i32* %real_range_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end26
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %33 = bitcast i32* %i27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  store i32 0, i32* %i27, align 4, !tbaa !6
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc57, %for.end
  %34 = load i32, i32* %i27, align 4, !tbaa !6
  %35 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_num_col = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %35, i32 0, i32 10
  %36 = load i32, i32* %stage_num_col, align 4, !tbaa !20
  %cmp29 = icmp slt i32 %34, %36
  br i1 %cmp29, label %land.rhs31, label %land.end34

land.rhs31:                                       ; preds = %for.cond28
  %37 = load i32, i32* %i27, align 4, !tbaa !6
  %cmp32 = icmp slt i32 %37, 12
  br label %land.end34

land.end34:                                       ; preds = %land.rhs31, %for.cond28
  %38 = phi i1 [ false, %for.cond28 ], [ %cmp32, %land.rhs31 ]
  br i1 %38, label %for.body36, label %for.cond.cleanup35

for.cond.cleanup35:                               ; preds = %land.end34
  %39 = bitcast i32* %i27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #6
  br label %for.end59

for.body36:                                       ; preds = %land.end34
  %40 = bitcast i32* %real_range_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  %41 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %stage_range_col37 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %41, i32 0, i32 6
  %42 = load i32, i32* %i27, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds [12 x i8], [12 x i8]* %stage_range_col37, i32 0, i32 %42
  %43 = load i8, i8* %arrayidx38, align 1, !tbaa !10
  %conv39 = sext i8 %43 to i32
  %44 = load i32, i32* %fwd_shift, align 4, !tbaa !6
  %add40 = add nsw i32 %conv39, %44
  %45 = load i8*, i8** %shift, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i8, i8* %45, i32 0
  %46 = load i8, i8* %arrayidx41, align 1, !tbaa !10
  %conv42 = sext i8 %46 to i32
  %add43 = add nsw i32 %add40, %conv42
  %47 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add44 = add nsw i32 %add43, %47
  %add45 = add nsw i32 %add44, 1
  store i32 %add45, i32* %real_range_col, align 4, !tbaa !6
  %48 = load i32, i32* %real_range_col, align 4, !tbaa !6
  %49 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_col = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %49, i32 0, i32 8
  %50 = load i8, i8* %txfm_type_col, align 2, !tbaa !18
  %conv46 = zext i8 %50 to i32
  %cmp47 = icmp eq i32 %conv46, 5
  br i1 %cmp47, label %land.lhs.true49, label %if.else54

land.lhs.true49:                                  ; preds = %for.body36
  %51 = load i32, i32* %i27, align 4, !tbaa !6
  %cmp50 = icmp eq i32 %51, 1
  br i1 %cmp50, label %if.then52, label %if.else54

if.then52:                                        ; preds = %land.lhs.true49
  %52 = load i8, i8* %opt_range_col, align 1, !tbaa !10
  %53 = load i8*, i8** %stage_range_col.addr, align 4, !tbaa !2
  %54 = load i32, i32* %i27, align 4, !tbaa !6
  %arrayidx53 = getelementptr inbounds i8, i8* %53, i32 %54
  store i8 %52, i8* %arrayidx53, align 1, !tbaa !10
  br label %if.end56

if.else54:                                        ; preds = %land.lhs.true49, %for.body36
  %55 = load i8, i8* %opt_range_col, align 1, !tbaa !10
  %56 = load i8*, i8** %stage_range_col.addr, align 4, !tbaa !2
  %57 = load i32, i32* %i27, align 4, !tbaa !6
  %arrayidx55 = getelementptr inbounds i8, i8* %56, i32 %57
  store i8 %55, i8* %arrayidx55, align 1, !tbaa !10
  br label %if.end56

if.end56:                                         ; preds = %if.else54, %if.then52
  %58 = bitcast i32* %real_range_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %59 = load i32, i32* %i27, align 4, !tbaa !6
  %inc58 = add nsw i32 %59, 1
  store i32 %inc58, i32* %i27, align 4, !tbaa !6
  br label %for.cond28

for.end59:                                        ; preds = %for.cond.cleanup35
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %opt_range_col) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %opt_range_row) #6
  %60 = bitcast i8** %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i32* %fwd_shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_4x8_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [48 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [48 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 192, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [48 x i32], [48 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 5, i32 %5)
  %6 = bitcast [48 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 192, i8* %6) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @inv_txfm2d_add_facade(i32* %input, i16* %output, i32 %stride, i32* %txfm_buf, i8 zeroext %tx_type, i8 zeroext %tx_size, i32 %bd) #2 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %txfm_buf.addr = alloca i32*, align 4
  %tx_type.addr = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %cfg = alloca %struct.TXFM_2D_FLIP_CFG, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32* %txfm_buf, i32** %txfm_buf.addr, align 4, !tbaa !2
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.TXFM_2D_FLIP_CFG* %cfg to i8*
  call void @llvm.lifetime.start.p0i8(i64 52, i8* %0) #6
  %1 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %2 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  call void @av1_get_inv_txfm_cfg(i8 zeroext %1, i8 zeroext %2, %struct.TXFM_2D_FLIP_CFG* %cfg)
  %3 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %4 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %5 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %6 = load i32*, i32** %txfm_buf.addr, align 4, !tbaa !2
  %7 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %8 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_c(i32* %3, i16* %4, i32 %5, %struct.TXFM_2D_FLIP_CFG* %cfg, i32* %6, i8 zeroext %7, i32 %8)
  %9 = bitcast %struct.TXFM_2D_FLIP_CFG* %cfg to i8*
  call void @llvm.lifetime.end.p0i8(i64 52, i8* %9) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_8x4_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [48 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [48 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 192, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [48 x i32], [48 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 6, i32 %5)
  %6 = bitcast [48 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 192, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_8x16_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [160 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [160 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 640, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [160 x i32], [160 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 7, i32 %5)
  %6 = bitcast [160 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 640, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_16x8_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [160 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [160 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 640, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [160 x i32], [160 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 8, i32 %5)
  %6 = bitcast [160 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 640, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_16x32_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [576 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [576 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 2304, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [576 x i32], [576 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 9, i32 %5)
  %6 = bitcast [576 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 2304, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_32x16_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [576 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [576 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 2304, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [576 x i32], [576 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 10, i32 %5)
  %6 = bitcast [576 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 2304, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_4x4_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [24 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [24 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 96, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [24 x i32], [24 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 0, i32 %5)
  %6 = bitcast [24 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 96, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_8x8_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [80 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [80 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 320, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [80 x i32], [80 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 1, i32 %5)
  %6 = bitcast [80 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 320, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_16x16_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [288 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [288 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 1152, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [288 x i32], [288 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 2, i32 %5)
  %6 = bitcast [288 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 1152, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_32x32_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [1088 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [1088 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4352, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [1088 x i32], [1088 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 3, i32 %5)
  %6 = bitcast [1088 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4352, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_64x64_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %mod_input = alloca [4096 x i32], align 16
  %row = alloca i32, align 4
  %txfm_buf = alloca [4224 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [4096 x i32]* %mod_input to i8*
  call void @llvm.lifetime.start.p0i8(i64 16384, i8* %0) #6
  %1 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %row, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 32
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %arraydecay = getelementptr inbounds [4096 x i32], [4096 x i32]* %mod_input, i32 0, i32 0
  %4 = load i32, i32* %row, align 4, !tbaa !6
  %mul = mul nsw i32 %4, 64
  %add.ptr = getelementptr inbounds i32, i32* %arraydecay, i32 %mul
  %5 = bitcast i32* %add.ptr to i8*
  %6 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %7 = load i32, i32* %row, align 4, !tbaa !6
  %mul1 = mul nsw i32 %7, 32
  %add.ptr2 = getelementptr inbounds i32, i32* %6, i32 %mul1
  %8 = bitcast i32* %add.ptr2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %8, i32 128, i1 false)
  %arraydecay3 = getelementptr inbounds [4096 x i32], [4096 x i32]* %mod_input, i32 0, i32 0
  %9 = load i32, i32* %row, align 4, !tbaa !6
  %mul4 = mul nsw i32 %9, 64
  %add.ptr5 = getelementptr inbounds i32, i32* %arraydecay3, i32 %mul4
  %add.ptr6 = getelementptr inbounds i32, i32* %add.ptr5, i32 32
  %10 = bitcast i32* %add.ptr6 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %10, i8 0, i32 128, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %arraydecay7 = getelementptr inbounds [4096 x i32], [4096 x i32]* %mod_input, i32 0, i32 0
  %add.ptr8 = getelementptr inbounds i32, i32* %arraydecay7, i32 2048
  %12 = bitcast i32* %add.ptr8 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %12, i8 0, i32 8192, i1 false)
  %13 = bitcast [4224 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 16896, i8* %13) #6
  %arraydecay9 = getelementptr inbounds [4096 x i32], [4096 x i32]* %mod_input, i32 0, i32 0
  %14 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %15 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay10 = getelementptr inbounds [4224 x i32], [4224 x i32]* %txfm_buf, i32 0, i32 0
  %16 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %17 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %arraydecay9, i16* %14, i32 %15, i32* %arraydecay10, i8 zeroext %16, i8 zeroext 4, i32 %17)
  %18 = bitcast [4224 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 16896, i8* %18) #6
  %19 = bitcast [4096 x i32]* %mod_input to i8*
  call void @llvm.lifetime.end.p0i8(i64 16384, i8* %19) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_64x32_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %mod_input = alloca [2048 x i32], align 16
  %row = alloca i32, align 4
  %txfm_buf = alloca [2176 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [2048 x i32]* %mod_input to i8*
  call void @llvm.lifetime.start.p0i8(i64 8192, i8* %0) #6
  %1 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %row, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 32
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %arraydecay = getelementptr inbounds [2048 x i32], [2048 x i32]* %mod_input, i32 0, i32 0
  %4 = load i32, i32* %row, align 4, !tbaa !6
  %mul = mul nsw i32 %4, 64
  %add.ptr = getelementptr inbounds i32, i32* %arraydecay, i32 %mul
  %5 = bitcast i32* %add.ptr to i8*
  %6 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %7 = load i32, i32* %row, align 4, !tbaa !6
  %mul1 = mul nsw i32 %7, 32
  %add.ptr2 = getelementptr inbounds i32, i32* %6, i32 %mul1
  %8 = bitcast i32* %add.ptr2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %8, i32 128, i1 false)
  %arraydecay3 = getelementptr inbounds [2048 x i32], [2048 x i32]* %mod_input, i32 0, i32 0
  %9 = load i32, i32* %row, align 4, !tbaa !6
  %mul4 = mul nsw i32 %9, 64
  %add.ptr5 = getelementptr inbounds i32, i32* %arraydecay3, i32 %mul4
  %add.ptr6 = getelementptr inbounds i32, i32* %add.ptr5, i32 32
  %10 = bitcast i32* %add.ptr6 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %10, i8 0, i32 128, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %12 = bitcast [2176 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 8704, i8* %12) #6
  %arraydecay7 = getelementptr inbounds [2048 x i32], [2048 x i32]* %mod_input, i32 0, i32 0
  %13 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %14 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay8 = getelementptr inbounds [2176 x i32], [2176 x i32]* %txfm_buf, i32 0, i32 0
  %15 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %16 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %arraydecay7, i16* %13, i32 %14, i32* %arraydecay8, i8 zeroext %15, i8 zeroext 12, i32 %16)
  %17 = bitcast [2176 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 8704, i8* %17) #6
  %18 = bitcast [2048 x i32]* %mod_input to i8*
  call void @llvm.lifetime.end.p0i8(i64 8192, i8* %18) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_32x64_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %mod_input = alloca [2048 x i32], align 16
  %txfm_buf = alloca [2176 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [2048 x i32]* %mod_input to i8*
  call void @llvm.lifetime.start.p0i8(i64 8192, i8* %0) #6
  %arraydecay = getelementptr inbounds [2048 x i32], [2048 x i32]* %mod_input, i32 0, i32 0
  %1 = bitcast i32* %arraydecay to i8*
  %2 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %3 = bitcast i32* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %1, i8* align 4 %3, i32 4096, i1 false)
  %arraydecay1 = getelementptr inbounds [2048 x i32], [2048 x i32]* %mod_input, i32 0, i32 0
  %add.ptr = getelementptr inbounds i32, i32* %arraydecay1, i32 1024
  %4 = bitcast i32* %add.ptr to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %4, i8 0, i32 4096, i1 false)
  %5 = bitcast [2176 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 8704, i8* %5) #6
  %arraydecay2 = getelementptr inbounds [2048 x i32], [2048 x i32]* %mod_input, i32 0, i32 0
  %6 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay3 = getelementptr inbounds [2176 x i32], [2176 x i32]* %txfm_buf, i32 0, i32 0
  %8 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %9 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %arraydecay2, i16* %6, i32 %7, i32* %arraydecay3, i8 zeroext %8, i8 zeroext 11, i32 %9)
  %10 = bitcast [2176 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 8704, i8* %10) #6
  %11 = bitcast [2048 x i32]* %mod_input to i8*
  call void @llvm.lifetime.end.p0i8(i64 8192, i8* %11) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_16x64_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %mod_input = alloca [1024 x i32], align 16
  %txfm_buf = alloca [1152 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [1024 x i32]* %mod_input to i8*
  call void @llvm.lifetime.start.p0i8(i64 4096, i8* %0) #6
  %arraydecay = getelementptr inbounds [1024 x i32], [1024 x i32]* %mod_input, i32 0, i32 0
  %1 = bitcast i32* %arraydecay to i8*
  %2 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %3 = bitcast i32* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %1, i8* align 4 %3, i32 2048, i1 false)
  %arraydecay1 = getelementptr inbounds [1024 x i32], [1024 x i32]* %mod_input, i32 0, i32 0
  %add.ptr = getelementptr inbounds i32, i32* %arraydecay1, i32 512
  %4 = bitcast i32* %add.ptr to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %4, i8 0, i32 2048, i1 false)
  %5 = bitcast [1152 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4608, i8* %5) #6
  %arraydecay2 = getelementptr inbounds [1024 x i32], [1024 x i32]* %mod_input, i32 0, i32 0
  %6 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay3 = getelementptr inbounds [1152 x i32], [1152 x i32]* %txfm_buf, i32 0, i32 0
  %8 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %9 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %arraydecay2, i16* %6, i32 %7, i32* %arraydecay3, i8 zeroext %8, i8 zeroext 17, i32 %9)
  %10 = bitcast [1152 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4608, i8* %10) #6
  %11 = bitcast [1024 x i32]* %mod_input to i8*
  call void @llvm.lifetime.end.p0i8(i64 4096, i8* %11) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_64x16_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %mod_input = alloca [1024 x i32], align 16
  %row = alloca i32, align 4
  %txfm_buf = alloca [1152 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [1024 x i32]* %mod_input to i8*
  call void @llvm.lifetime.start.p0i8(i64 4096, i8* %0) #6
  %1 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %row, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 16
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %arraydecay = getelementptr inbounds [1024 x i32], [1024 x i32]* %mod_input, i32 0, i32 0
  %4 = load i32, i32* %row, align 4, !tbaa !6
  %mul = mul nsw i32 %4, 64
  %add.ptr = getelementptr inbounds i32, i32* %arraydecay, i32 %mul
  %5 = bitcast i32* %add.ptr to i8*
  %6 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %7 = load i32, i32* %row, align 4, !tbaa !6
  %mul1 = mul nsw i32 %7, 32
  %add.ptr2 = getelementptr inbounds i32, i32* %6, i32 %mul1
  %8 = bitcast i32* %add.ptr2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %8, i32 128, i1 false)
  %arraydecay3 = getelementptr inbounds [1024 x i32], [1024 x i32]* %mod_input, i32 0, i32 0
  %9 = load i32, i32* %row, align 4, !tbaa !6
  %mul4 = mul nsw i32 %9, 64
  %add.ptr5 = getelementptr inbounds i32, i32* %arraydecay3, i32 %mul4
  %add.ptr6 = getelementptr inbounds i32, i32* %add.ptr5, i32 32
  %10 = bitcast i32* %add.ptr6 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %10, i8 0, i32 128, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %12 = bitcast [1152 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4608, i8* %12) #6
  %arraydecay7 = getelementptr inbounds [1024 x i32], [1024 x i32]* %mod_input, i32 0, i32 0
  %13 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %14 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay8 = getelementptr inbounds [1152 x i32], [1152 x i32]* %txfm_buf, i32 0, i32 0
  %15 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %16 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %arraydecay7, i16* %13, i32 %14, i32* %arraydecay8, i8 zeroext %15, i8 zeroext 18, i32 %16)
  %17 = bitcast [1152 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4608, i8* %17) #6
  %18 = bitcast [1024 x i32]* %mod_input to i8*
  call void @llvm.lifetime.end.p0i8(i64 4096, i8* %18) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_4x16_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [96 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [96 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 384, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [96 x i32], [96 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 13, i32 %5)
  %6 = bitcast [96 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 384, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_16x4_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [96 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [96 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 384, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [96 x i32], [96 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 14, i32 %5)
  %6 = bitcast [96 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 384, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_8x32_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [320 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [320 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 1280, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [320 x i32], [320 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 15, i32 %5)
  %6 = bitcast [320 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 1280, i8* %6) #6
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_inv_txfm2d_add_32x8_c(i32* %input, i16* %output, i32 %stride, i8 zeroext %tx_type, i32 %bd) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %tx_type.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_buf = alloca [320 x i32], align 32
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [320 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 1280, i8* %0) #6
  %1 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [320 x i32], [320 x i32]* %txfm_buf, i32 0, i32 0
  %4 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @inv_txfm2d_add_facade(i32* %1, i16* %2, i32 %3, i32* %arraydecay, i8 zeroext %4, i8 zeroext 16, i32 %5)
  %6 = bitcast [320 x i32]* %txfm_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 1280, i8* %6) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @clip_pixel_highbd(i32 %val, i32 %bd) #2 {
entry:
  %retval = alloca i16, align 2
  %val.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 8, label %sw.bb
    i32 10, label %sw.bb1
    i32 12, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb
  %1 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call = call i32 @clamp(i32 %1, i32 0, i32 255)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %retval, align 2
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call2 = call i32 @clamp(i32 %2, i32 0, i32 1023)
  %conv3 = trunc i32 %call2 to i16
  store i16 %conv3, i16* %retval, align 2
  br label %return

sw.bb4:                                           ; preds = %entry
  %3 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call5 = call i32 @clamp(i32 %3, i32 0, i32 4095)
  %conv6 = trunc i32 %call5 to i16
  store i16 %conv6, i16* %retval, align 2
  br label %return

return:                                           ; preds = %sw.bb4, %sw.bb1, %sw.default
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal void @get_flip_cfg(i8 zeroext %tx_type, i32* %ud_flip, i32* %lr_flip) #2 {
entry:
  %tx_type.addr = alloca i8, align 1
  %ud_flip.addr = alloca i32*, align 4
  %lr_flip.addr = alloca i32*, align 4
  store i8 %tx_type, i8* %tx_type.addr, align 1, !tbaa !10
  store i32* %ud_flip, i32** %ud_flip.addr, align 4, !tbaa !2
  store i32* %lr_flip, i32** %lr_flip.addr, align 4, !tbaa !2
  %0 = load i8, i8* %tx_type.addr, align 1, !tbaa !10
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb
    i32 9, label %sw.bb1
    i32 10, label %sw.bb1
    i32 11, label %sw.bb1
    i32 12, label %sw.bb1
    i32 13, label %sw.bb1
    i32 4, label %sw.bb2
    i32 8, label %sw.bb2
    i32 14, label %sw.bb2
    i32 5, label %sw.bb3
    i32 7, label %sw.bb3
    i32 15, label %sw.bb3
    i32 6, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry, %entry
  %1 = load i32*, i32** %ud_flip.addr, align 4, !tbaa !2
  store i32 0, i32* %1, align 4, !tbaa !6
  %2 = load i32*, i32** %lr_flip.addr, align 4, !tbaa !2
  store i32 0, i32* %2, align 4, !tbaa !6
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry, %entry, %entry, %entry
  %3 = load i32*, i32** %ud_flip.addr, align 4, !tbaa !2
  store i32 0, i32* %3, align 4, !tbaa !6
  %4 = load i32*, i32** %lr_flip.addr, align 4, !tbaa !2
  store i32 0, i32* %4, align 4, !tbaa !6
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry, %entry, %entry
  %5 = load i32*, i32** %ud_flip.addr, align 4, !tbaa !2
  store i32 1, i32* %5, align 4, !tbaa !6
  %6 = load i32*, i32** %lr_flip.addr, align 4, !tbaa !2
  store i32 0, i32* %6, align 4, !tbaa !6
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry, %entry
  %7 = load i32*, i32** %ud_flip.addr, align 4, !tbaa !2
  store i32 0, i32* %7, align 4, !tbaa !6
  %8 = load i32*, i32** %lr_flip.addr, align 4, !tbaa !2
  store i32 1, i32* %8, align 4, !tbaa !6
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry
  %9 = load i32*, i32** %ud_flip.addr, align 4, !tbaa !2
  store i32 1, i32* %9, align 4, !tbaa !6
  %10 = load i32*, i32** %lr_flip.addr, align 4, !tbaa !2
  store i32 1, i32* %10, align 4, !tbaa !6
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %11 = load i32*, i32** %ud_flip.addr, align 4, !tbaa !2
  store i32 0, i32* %11, align 4, !tbaa !6
  %12 = load i32*, i32** %lr_flip.addr, align 4, !tbaa !2
  store i32 0, i32* %12, align 4, !tbaa !6
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @inv_txfm2d_add_c(i32* %input, i16* %output, i32 %stride, %struct.TXFM_2D_FLIP_CFG* %cfg, i32* %txfm_buf, i8 zeroext %tx_size, i32 %bd) #2 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i16*, align 4
  %stride.addr = alloca i32, align 4
  %cfg.addr = alloca %struct.TXFM_2D_FLIP_CFG*, align 4
  %txfm_buf.addr = alloca i32*, align 4
  %tx_size.addr = alloca i8, align 1
  %bd.addr = alloca i32, align 4
  %txfm_size_col = alloca i32, align 4
  %txfm_size_row = alloca i32, align 4
  %shift = alloca i8*, align 4
  %rect_type = alloca i32, align 4
  %stage_range_row = alloca [12 x i8], align 1
  %stage_range_col = alloca [12 x i8], align 1
  %cos_bit_col = alloca i8, align 1
  %cos_bit_row = alloca i8, align 1
  %txfm_func_col = alloca void (i32*, i32*, i8, i8*)*, align 4
  %txfm_func_row = alloca void (i32*, i32*, i8, i8*)*, align 4
  %buf_offset = alloca i32, align 4
  %temp_in = alloca i32*, align 4
  %temp_out = alloca i32*, align 4
  %buf = alloca i32*, align 4
  %buf_ptr = alloca i32*, align 4
  %c = alloca i32, align 4
  %r = alloca i32, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i16* %output, i16** %output.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store %struct.TXFM_2D_FLIP_CFG* %cfg, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  store i32* %txfm_buf, i32** %txfm_buf.addr, align 4, !tbaa !2
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !10
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %txfm_size_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %tx_size1 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %1, i32 0, i32 0
  %2 = load i8, i8* %tx_size1, align 4, !tbaa !13
  %idxprom = zext i8 %2 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide, i32 0, i32 %idxprom
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %3, i32* %txfm_size_col, align 4, !tbaa !6
  %4 = bitcast i32* %txfm_size_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %tx_size2 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %5, i32 0, i32 0
  %6 = load i8, i8* %tx_size2, align 4, !tbaa !13
  %idxprom3 = zext i8 %6 to i32
  %arrayidx4 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high, i32 0, i32 %idxprom3
  %7 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  store i32 %7, i32* %txfm_size_row, align 4, !tbaa !6
  %8 = bitcast i8** %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %shift5 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %9, i32 0, i32 3
  %10 = load i8*, i8** %shift5, align 4, !tbaa !15
  store i8* %10, i8** %shift, align 4, !tbaa !2
  %11 = bitcast i32* %rect_type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %13 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %call = call i32 @get_rect_tx_log_ratio(i32 %12, i32 %13)
  store i32 %call, i32* %rect_type, align 4, !tbaa !6
  %14 = bitcast [12 x i8]* %stage_range_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %14) #6
  %15 = bitcast [12 x i8]* %stage_range_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %15) #6
  %arraydecay = getelementptr inbounds [12 x i8], [12 x i8]* %stage_range_col, i32 0, i32 0
  %arraydecay6 = getelementptr inbounds [12 x i8], [12 x i8]* %stage_range_row, i32 0, i32 0
  %16 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %17 = load i8, i8* %tx_size.addr, align 1, !tbaa !10
  %18 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @av1_gen_inv_stage_range(i8* %arraydecay, i8* %arraydecay6, %struct.TXFM_2D_FLIP_CFG* %16, i8 zeroext %17, i32 %18)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %cos_bit_col) #6
  %19 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %cos_bit_col7 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %19, i32 0, i32 4
  %20 = load i8, i8* %cos_bit_col7, align 4, !tbaa !16
  store i8 %20, i8* %cos_bit_col, align 1, !tbaa !10
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %cos_bit_row) #6
  %21 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %cos_bit_row8 = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %21, i32 0, i32 5
  %22 = load i8, i8* %cos_bit_row8, align 1, !tbaa !17
  store i8 %22, i8* %cos_bit_row, align 1, !tbaa !10
  %23 = bitcast void (i32*, i32*, i8, i8*)** %txfm_func_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_col = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %24, i32 0, i32 8
  %25 = load i8, i8* %txfm_type_col, align 2, !tbaa !18
  %call9 = call void (i32*, i32*, i8, i8*)* @inv_txfm_type_to_func(i8 zeroext %25)
  store void (i32*, i32*, i8, i8*)* %call9, void (i32*, i32*, i8, i8*)** %txfm_func_col, align 4, !tbaa !2
  %26 = bitcast void (i32*, i32*, i8, i8*)** %txfm_func_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %27 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %txfm_type_row = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %27, i32 0, i32 9
  %28 = load i8, i8* %txfm_type_row, align 1, !tbaa !19
  %call10 = call void (i32*, i32*, i8, i8*)* @inv_txfm_type_to_func(i8 zeroext %28)
  store void (i32*, i32*, i8, i8*)* %call10, void (i32*, i32*, i8, i8*)** %txfm_func_row, align 4, !tbaa !2
  %29 = bitcast i32* %buf_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %31 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %cmp = icmp sgt i32 %30, %31
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %32 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %33 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %32, %cond.true ], [ %33, %cond.false ]
  store i32 %cond, i32* %buf_offset, align 4, !tbaa !6
  %34 = bitcast i32** %temp_in to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  %35 = load i32*, i32** %txfm_buf.addr, align 4, !tbaa !2
  store i32* %35, i32** %temp_in, align 4, !tbaa !2
  %36 = bitcast i32** %temp_out to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  %37 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %38 = load i32, i32* %buf_offset, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i32, i32* %37, i32 %38
  store i32* %add.ptr, i32** %temp_out, align 4, !tbaa !2
  %39 = bitcast i32** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  %40 = load i32*, i32** %temp_out, align 4, !tbaa !2
  %41 = load i32, i32* %buf_offset, align 4, !tbaa !6
  %add.ptr11 = getelementptr inbounds i32, i32* %40, i32 %41
  store i32* %add.ptr11, i32** %buf, align 4, !tbaa !2
  %42 = bitcast i32** %buf_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #6
  %43 = load i32*, i32** %buf, align 4, !tbaa !2
  store i32* %43, i32** %buf_ptr, align 4, !tbaa !2
  %44 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #6
  %45 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #6
  store i32 0, i32* %r, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc39, %cond.end
  %46 = load i32, i32* %r, align 4, !tbaa !6
  %47 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %cmp12 = icmp slt i32 %46, %47
  br i1 %cmp12, label %for.body, label %for.end41

for.body:                                         ; preds = %for.cond
  %48 = load i32, i32* %rect_type, align 4, !tbaa !6
  %call13 = call i32 @abs(i32 %48) #7
  %cmp14 = icmp eq i32 %call13, 1
  br i1 %cmp14, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc, %if.then
  %49 = load i32, i32* %c, align 4, !tbaa !6
  %50 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %cmp16 = icmp slt i32 %49, %50
  br i1 %cmp16, label %for.body17, label %for.end

for.body17:                                       ; preds = %for.cond15
  %51 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %52 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds i32, i32* %51, i32 %52
  %53 = load i32, i32* %arrayidx18, align 4, !tbaa !6
  %conv = sext i32 %53 to i64
  %mul = mul nsw i64 %conv, 2896
  %call19 = call i32 @round_shift(i64 %mul, i32 12)
  %54 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %55 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds i32, i32* %54, i32 %55
  store i32 %call19, i32* %arrayidx20, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body17
  %56 = load i32, i32* %c, align 4, !tbaa !6
  %inc = add nsw i32 %56, 1
  store i32 %inc, i32* %c, align 4, !tbaa !6
  br label %for.cond15

for.end:                                          ; preds = %for.cond15
  %57 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %58 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %59 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add = add nsw i32 %59, 8
  %conv21 = trunc i32 %add to i8
  call void @clamp_buf(i32* %57, i32 %58, i8 signext %conv21)
  %60 = load void (i32*, i32*, i8, i8*)*, void (i32*, i32*, i8, i8*)** %txfm_func_row, align 4, !tbaa !2
  %61 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %62 = load i32*, i32** %buf_ptr, align 4, !tbaa !2
  %63 = load i8, i8* %cos_bit_row, align 1, !tbaa !10
  %arraydecay22 = getelementptr inbounds [12 x i8], [12 x i8]* %stage_range_row, i32 0, i32 0
  call void %60(i32* %61, i32* %62, i8 signext %63, i8* %arraydecay22)
  br label %if.end

if.else:                                          ; preds = %for.body
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc29, %if.else
  %64 = load i32, i32* %c, align 4, !tbaa !6
  %65 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %cmp24 = icmp slt i32 %64, %65
  br i1 %cmp24, label %for.body26, label %for.end31

for.body26:                                       ; preds = %for.cond23
  %66 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %67 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx27, align 4, !tbaa !6
  %69 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %70 = load i32, i32* %c, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds i32, i32* %69, i32 %70
  store i32 %68, i32* %arrayidx28, align 4, !tbaa !6
  br label %for.inc29

for.inc29:                                        ; preds = %for.body26
  %71 = load i32, i32* %c, align 4, !tbaa !6
  %inc30 = add nsw i32 %71, 1
  store i32 %inc30, i32* %c, align 4, !tbaa !6
  br label %for.cond23

for.end31:                                        ; preds = %for.cond23
  %72 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %73 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %74 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add32 = add nsw i32 %74, 8
  %conv33 = trunc i32 %add32 to i8
  call void @clamp_buf(i32* %72, i32 %73, i8 signext %conv33)
  %75 = load void (i32*, i32*, i8, i8*)*, void (i32*, i32*, i8, i8*)** %txfm_func_row, align 4, !tbaa !2
  %76 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %77 = load i32*, i32** %buf_ptr, align 4, !tbaa !2
  %78 = load i8, i8* %cos_bit_row, align 1, !tbaa !10
  %arraydecay34 = getelementptr inbounds [12 x i8], [12 x i8]* %stage_range_row, i32 0, i32 0
  call void %75(i32* %76, i32* %77, i8 signext %78, i8* %arraydecay34)
  br label %if.end

if.end:                                           ; preds = %for.end31, %for.end
  %79 = load i32*, i32** %buf_ptr, align 4, !tbaa !2
  %80 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %81 = load i8*, i8** %shift, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i8, i8* %81, i32 0
  %82 = load i8, i8* %arrayidx35, align 1, !tbaa !10
  %conv36 = sext i8 %82 to i32
  %sub = sub nsw i32 0, %conv36
  call void @av1_round_shift_array_c(i32* %79, i32 %80, i32 %sub)
  %83 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %84 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %add.ptr37 = getelementptr inbounds i32, i32* %84, i32 %83
  store i32* %add.ptr37, i32** %input.addr, align 4, !tbaa !2
  %85 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %86 = load i32*, i32** %buf_ptr, align 4, !tbaa !2
  %add.ptr38 = getelementptr inbounds i32, i32* %86, i32 %85
  store i32* %add.ptr38, i32** %buf_ptr, align 4, !tbaa !2
  br label %for.inc39

for.inc39:                                        ; preds = %if.end
  %87 = load i32, i32* %r, align 4, !tbaa !6
  %inc40 = add nsw i32 %87, 1
  store i32 %inc40, i32* %r, align 4, !tbaa !6
  br label %for.cond

for.end41:                                        ; preds = %for.cond
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc127, %for.end41
  %88 = load i32, i32* %c, align 4, !tbaa !6
  %89 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %cmp43 = icmp slt i32 %88, %89
  br i1 %cmp43, label %for.body45, label %for.end129

for.body45:                                       ; preds = %for.cond42
  %90 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %lr_flip = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %90, i32 0, i32 2
  %91 = load i32, i32* %lr_flip, align 4, !tbaa !22
  %cmp46 = icmp eq i32 %91, 0
  br i1 %cmp46, label %if.then48, label %if.else60

if.then48:                                        ; preds = %for.body45
  store i32 0, i32* %r, align 4, !tbaa !6
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc57, %if.then48
  %92 = load i32, i32* %r, align 4, !tbaa !6
  %93 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %cmp50 = icmp slt i32 %92, %93
  br i1 %cmp50, label %for.body52, label %for.end59

for.body52:                                       ; preds = %for.cond49
  %94 = load i32*, i32** %buf, align 4, !tbaa !2
  %95 = load i32, i32* %r, align 4, !tbaa !6
  %96 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %mul53 = mul nsw i32 %95, %96
  %97 = load i32, i32* %c, align 4, !tbaa !6
  %add54 = add nsw i32 %mul53, %97
  %arrayidx55 = getelementptr inbounds i32, i32* %94, i32 %add54
  %98 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %99 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %100 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds i32, i32* %99, i32 %100
  store i32 %98, i32* %arrayidx56, align 4, !tbaa !6
  br label %for.inc57

for.inc57:                                        ; preds = %for.body52
  %101 = load i32, i32* %r, align 4, !tbaa !6
  %inc58 = add nsw i32 %101, 1
  store i32 %inc58, i32* %r, align 4, !tbaa !6
  br label %for.cond49

for.end59:                                        ; preds = %for.cond49
  br label %if.end74

if.else60:                                        ; preds = %for.body45
  store i32 0, i32* %r, align 4, !tbaa !6
  br label %for.cond61

for.cond61:                                       ; preds = %for.inc71, %if.else60
  %102 = load i32, i32* %r, align 4, !tbaa !6
  %103 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %cmp62 = icmp slt i32 %102, %103
  br i1 %cmp62, label %for.body64, label %for.end73

for.body64:                                       ; preds = %for.cond61
  %104 = load i32*, i32** %buf, align 4, !tbaa !2
  %105 = load i32, i32* %r, align 4, !tbaa !6
  %106 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %mul65 = mul nsw i32 %105, %106
  %107 = load i32, i32* %txfm_size_col, align 4, !tbaa !6
  %108 = load i32, i32* %c, align 4, !tbaa !6
  %sub66 = sub nsw i32 %107, %108
  %sub67 = sub nsw i32 %sub66, 1
  %add68 = add nsw i32 %mul65, %sub67
  %arrayidx69 = getelementptr inbounds i32, i32* %104, i32 %add68
  %109 = load i32, i32* %arrayidx69, align 4, !tbaa !6
  %110 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %111 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx70 = getelementptr inbounds i32, i32* %110, i32 %111
  store i32 %109, i32* %arrayidx70, align 4, !tbaa !6
  br label %for.inc71

for.inc71:                                        ; preds = %for.body64
  %112 = load i32, i32* %r, align 4, !tbaa !6
  %inc72 = add nsw i32 %112, 1
  store i32 %inc72, i32* %r, align 4, !tbaa !6
  br label %for.cond61

for.end73:                                        ; preds = %for.cond61
  br label %if.end74

if.end74:                                         ; preds = %for.end73, %for.end59
  %113 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %114 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %115 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add75 = add nsw i32 %115, 6
  %cmp76 = icmp sgt i32 %add75, 16
  br i1 %cmp76, label %cond.true78, label %cond.false80

cond.true78:                                      ; preds = %if.end74
  %116 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add79 = add nsw i32 %116, 6
  br label %cond.end81

cond.false80:                                     ; preds = %if.end74
  br label %cond.end81

cond.end81:                                       ; preds = %cond.false80, %cond.true78
  %cond82 = phi i32 [ %add79, %cond.true78 ], [ 16, %cond.false80 ]
  %conv83 = trunc i32 %cond82 to i8
  call void @clamp_buf(i32* %113, i32 %114, i8 signext %conv83)
  %117 = load void (i32*, i32*, i8, i8*)*, void (i32*, i32*, i8, i8*)** %txfm_func_col, align 4, !tbaa !2
  %118 = load i32*, i32** %temp_in, align 4, !tbaa !2
  %119 = load i32*, i32** %temp_out, align 4, !tbaa !2
  %120 = load i8, i8* %cos_bit_col, align 1, !tbaa !10
  %arraydecay84 = getelementptr inbounds [12 x i8], [12 x i8]* %stage_range_col, i32 0, i32 0
  call void %117(i32* %118, i32* %119, i8 signext %120, i8* %arraydecay84)
  %121 = load i32*, i32** %temp_out, align 4, !tbaa !2
  %122 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %123 = load i8*, i8** %shift, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i8, i8* %123, i32 1
  %124 = load i8, i8* %arrayidx85, align 1, !tbaa !10
  %conv86 = sext i8 %124 to i32
  %sub87 = sub nsw i32 0, %conv86
  call void @av1_round_shift_array_c(i32* %121, i32 %122, i32 %sub87)
  %125 = load %struct.TXFM_2D_FLIP_CFG*, %struct.TXFM_2D_FLIP_CFG** %cfg.addr, align 4, !tbaa !2
  %ud_flip = getelementptr inbounds %struct.TXFM_2D_FLIP_CFG, %struct.TXFM_2D_FLIP_CFG* %125, i32 0, i32 1
  %126 = load i32, i32* %ud_flip, align 4, !tbaa !23
  %cmp88 = icmp eq i32 %126, 0
  br i1 %cmp88, label %if.then90, label %if.else107

if.then90:                                        ; preds = %cond.end81
  store i32 0, i32* %r, align 4, !tbaa !6
  br label %for.cond91

for.cond91:                                       ; preds = %for.inc104, %if.then90
  %127 = load i32, i32* %r, align 4, !tbaa !6
  %128 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %cmp92 = icmp slt i32 %127, %128
  br i1 %cmp92, label %for.body94, label %for.end106

for.body94:                                       ; preds = %for.cond91
  %129 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %130 = load i32, i32* %r, align 4, !tbaa !6
  %131 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul95 = mul nsw i32 %130, %131
  %132 = load i32, i32* %c, align 4, !tbaa !6
  %add96 = add nsw i32 %mul95, %132
  %arrayidx97 = getelementptr inbounds i16, i16* %129, i32 %add96
  %133 = load i16, i16* %arrayidx97, align 2, !tbaa !8
  %134 = load i32*, i32** %temp_out, align 4, !tbaa !2
  %135 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds i32, i32* %134, i32 %135
  %136 = load i32, i32* %arrayidx98, align 4, !tbaa !6
  %conv99 = sext i32 %136 to i64
  %137 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call100 = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %133, i64 %conv99, i32 %137)
  %138 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %139 = load i32, i32* %r, align 4, !tbaa !6
  %140 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul101 = mul nsw i32 %139, %140
  %141 = load i32, i32* %c, align 4, !tbaa !6
  %add102 = add nsw i32 %mul101, %141
  %arrayidx103 = getelementptr inbounds i16, i16* %138, i32 %add102
  store i16 %call100, i16* %arrayidx103, align 2, !tbaa !8
  br label %for.inc104

for.inc104:                                       ; preds = %for.body94
  %142 = load i32, i32* %r, align 4, !tbaa !6
  %inc105 = add nsw i32 %142, 1
  store i32 %inc105, i32* %r, align 4, !tbaa !6
  br label %for.cond91

for.end106:                                       ; preds = %for.cond91
  br label %if.end126

if.else107:                                       ; preds = %cond.end81
  store i32 0, i32* %r, align 4, !tbaa !6
  br label %for.cond108

for.cond108:                                      ; preds = %for.inc123, %if.else107
  %143 = load i32, i32* %r, align 4, !tbaa !6
  %144 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %cmp109 = icmp slt i32 %143, %144
  br i1 %cmp109, label %for.body111, label %for.end125

for.body111:                                      ; preds = %for.cond108
  %145 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %146 = load i32, i32* %r, align 4, !tbaa !6
  %147 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul112 = mul nsw i32 %146, %147
  %148 = load i32, i32* %c, align 4, !tbaa !6
  %add113 = add nsw i32 %mul112, %148
  %arrayidx114 = getelementptr inbounds i16, i16* %145, i32 %add113
  %149 = load i16, i16* %arrayidx114, align 2, !tbaa !8
  %150 = load i32*, i32** %temp_out, align 4, !tbaa !2
  %151 = load i32, i32* %txfm_size_row, align 4, !tbaa !6
  %152 = load i32, i32* %r, align 4, !tbaa !6
  %sub115 = sub nsw i32 %151, %152
  %sub116 = sub nsw i32 %sub115, 1
  %arrayidx117 = getelementptr inbounds i32, i32* %150, i32 %sub116
  %153 = load i32, i32* %arrayidx117, align 4, !tbaa !6
  %conv118 = sext i32 %153 to i64
  %154 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call119 = call zeroext i16 @highbd_clip_pixel_add(i16 zeroext %149, i64 %conv118, i32 %154)
  %155 = load i16*, i16** %output.addr, align 4, !tbaa !2
  %156 = load i32, i32* %r, align 4, !tbaa !6
  %157 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %mul120 = mul nsw i32 %156, %157
  %158 = load i32, i32* %c, align 4, !tbaa !6
  %add121 = add nsw i32 %mul120, %158
  %arrayidx122 = getelementptr inbounds i16, i16* %155, i32 %add121
  store i16 %call119, i16* %arrayidx122, align 2, !tbaa !8
  br label %for.inc123

for.inc123:                                       ; preds = %for.body111
  %159 = load i32, i32* %r, align 4, !tbaa !6
  %inc124 = add nsw i32 %159, 1
  store i32 %inc124, i32* %r, align 4, !tbaa !6
  br label %for.cond108

for.end125:                                       ; preds = %for.cond108
  br label %if.end126

if.end126:                                        ; preds = %for.end125, %for.end106
  br label %for.inc127

for.inc127:                                       ; preds = %if.end126
  %160 = load i32, i32* %c, align 4, !tbaa !6
  %inc128 = add nsw i32 %160, 1
  store i32 %inc128, i32* %c, align 4, !tbaa !6
  br label %for.cond42

for.end129:                                       ; preds = %for.cond42
  %161 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #6
  %162 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #6
  %163 = bitcast i32** %buf_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #6
  %164 = bitcast i32** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #6
  %165 = bitcast i32** %temp_out to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #6
  %166 = bitcast i32** %temp_in to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #6
  %167 = bitcast i32* %buf_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #6
  %168 = bitcast void (i32*, i32*, i8, i8*)** %txfm_func_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #6
  %169 = bitcast void (i32*, i32*, i8, i8*)** %txfm_func_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %cos_bit_row) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %cos_bit_col) #6
  %170 = bitcast [12 x i8]* %stage_range_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %170) #6
  %171 = bitcast [12 x i8]* %stage_range_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %171) #6
  %172 = bitcast i32* %rect_type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #6
  %173 = bitcast i8** %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #6
  %174 = bitcast i32* %txfm_size_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #6
  %175 = bitcast i32* %txfm_size_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_rect_tx_log_ratio(i32 %col, i32 %row) #2 {
entry:
  %retval = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  store i32 %col, i32* %col.addr, align 4, !tbaa !6
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  %0 = load i32, i32* %col.addr, align 4, !tbaa !6
  %1 = load i32, i32* %row.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %col.addr, align 4, !tbaa !6
  %3 = load i32, i32* %row.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %2, %3
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %4 = load i32, i32* %col.addr, align 4, !tbaa !6
  %5 = load i32, i32* %row.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %5, 2
  %cmp3 = icmp eq i32 %4, %mul
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.then2
  store i32 1, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.then2
  %6 = load i32, i32* %col.addr, align 4, !tbaa !6
  %7 = load i32, i32* %row.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 %7, 4
  %cmp7 = icmp eq i32 %6, %mul6
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end5
  store i32 2, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end5
  br label %if.end18

if.else:                                          ; preds = %if.end
  %8 = load i32, i32* %row.addr, align 4, !tbaa !6
  %9 = load i32, i32* %col.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 %9, 2
  %cmp11 = icmp eq i32 %8, %mul10
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.else
  store i32 -1, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %if.else
  %10 = load i32, i32* %row.addr, align 4, !tbaa !6
  %11 = load i32, i32* %col.addr, align 4, !tbaa !6
  %mul14 = mul nsw i32 %11, 4
  %cmp15 = icmp eq i32 %10, %mul14
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end13
  store i32 -2, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %if.end13
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.end9
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end18, %if.then16, %if.then12, %if.then8, %if.then4, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: inlinehint nounwind
define internal void (i32*, i32*, i8, i8*)* @inv_txfm_type_to_func(i8 zeroext %txfm_type) #2 {
entry:
  %retval = alloca void (i32*, i32*, i8, i8*)*, align 4
  %txfm_type.addr = alloca i8, align 1
  store i8 %txfm_type, i8* %txfm_type.addr, align 1, !tbaa !10
  %0 = load i8, i8* %txfm_type.addr, align 1, !tbaa !10
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
    i32 2, label %sw.bb2
    i32 3, label %sw.bb3
    i32 4, label %sw.bb4
    i32 5, label %sw.bb5
    i32 6, label %sw.bb6
    i32 7, label %sw.bb7
    i32 8, label %sw.bb8
    i32 9, label %sw.bb9
    i32 10, label %sw.bb10
    i32 11, label %sw.bb11
  ]

sw.bb:                                            ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_idct4, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_idct8, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_idct16, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_idct32, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_idct64, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_iadst4, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_iadst8, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb7:                                           ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_iadst16, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb8:                                           ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_iidentity4_c, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb9:                                           ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_iidentity8_c, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb10:                                          ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_iidentity16_c, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.bb11:                                          ; preds = %entry
  store void (i32*, i32*, i8, i8*)* @av1_iidentity32_c, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  store void (i32*, i32*, i8, i8*)* null, void (i32*, i32*, i8, i8*)** %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb11, %sw.bb10, %sw.bb9, %sw.bb8, %sw.bb7, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load void (i32*, i32*, i8, i8*)*, void (i32*, i32*, i8, i8*)** %retval, align 4
  ret void (i32*, i32*, i8, i8*)* %1
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #4

; Function Attrs: inlinehint nounwind
define internal i32 @round_shift(i64 %value, i32 %bit) #2 {
entry:
  %value.addr = alloca i64, align 8
  %bit.addr = alloca i32, align 4
  store i64 %value, i64* %value.addr, align 8, !tbaa !11
  store i32 %bit, i32* %bit.addr, align 4, !tbaa !6
  %0 = load i64, i64* %value.addr, align 8, !tbaa !11
  %1 = load i32, i32* %bit.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %1, 1
  %sh_prom = zext i32 %sub to i64
  %shl = shl i64 1, %sh_prom
  %add = add nsw i64 %0, %shl
  %2 = load i32, i32* %bit.addr, align 4, !tbaa !6
  %sh_prom1 = zext i32 %2 to i64
  %shr = ashr i64 %add, %sh_prom1
  %conv = trunc i64 %shr to i32
  ret i32 %conv
}

; Function Attrs: inlinehint nounwind
define internal void @clamp_buf(i32* %buf, i32 %size, i8 signext %bit) #2 {
entry:
  %buf.addr = alloca i32*, align 4
  %size.addr = alloca i32, align 4
  %bit.addr = alloca i8, align 1
  %i = alloca i32, align 4
  store i32* %buf, i32** %buf.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  store i8 %bit, i8* %bit.addr, align 1, !tbaa !10
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %size.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %buf.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %7 = load i8, i8* %bit.addr, align 1, !tbaa !10
  %call = call i32 @clamp_value(i32 %6, i8 signext %7)
  %8 = load i32*, i32** %buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds i32, i32* %8, i32 %9
  store i32 %call, i32* %arrayidx1, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @av1_round_shift_array_c(i32*, i32, i32) #5

declare void @av1_idct4(i32*, i32*, i8 signext, i8*) #5

declare void @av1_idct8(i32*, i32*, i8 signext, i8*) #5

declare void @av1_idct16(i32*, i32*, i8 signext, i8*) #5

declare void @av1_idct32(i32*, i32*, i8 signext, i8*) #5

declare void @av1_idct64(i32*, i32*, i8 signext, i8*) #5

declare void @av1_iadst4(i32*, i32*, i8 signext, i8*) #5

declare void @av1_iadst8(i32*, i32*, i8 signext, i8*) #5

declare void @av1_iadst16(i32*, i32*, i8 signext, i8*) #5

declare void @av1_iidentity4_c(i32*, i32*, i8 signext, i8*) #5

declare void @av1_iidentity8_c(i32*, i32*, i8 signext, i8*) #5

declare void @av1_iidentity16_c(i32*, i32*, i8 signext, i8*) #5

declare void @av1_iidentity32_c(i32*, i32*, i8 signext, i8*) #5

; Function Attrs: inlinehint nounwind
define internal i32 @clamp_value(i32 %value, i8 signext %bit) #2 {
entry:
  %retval = alloca i32, align 4
  %value.addr = alloca i32, align 4
  %bit.addr = alloca i8, align 1
  %max_value = alloca i64, align 8
  %min_value = alloca i64, align 8
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i8 %bit, i8* %bit.addr, align 1, !tbaa !10
  %0 = load i8, i8* %bit.addr, align 1, !tbaa !10
  %conv = sext i8 %0 to i32
  %cmp = icmp sle i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %value.addr, align 4, !tbaa !6
  store i32 %1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i64* %max_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #6
  %3 = load i8, i8* %bit.addr, align 1, !tbaa !10
  %conv2 = sext i8 %3 to i32
  %sub = sub nsw i32 %conv2, 1
  %sh_prom = zext i32 %sub to i64
  %shl = shl i64 1, %sh_prom
  %sub3 = sub nsw i64 %shl, 1
  store i64 %sub3, i64* %max_value, align 8, !tbaa !11
  %4 = bitcast i64* %min_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #6
  %5 = load i8, i8* %bit.addr, align 1, !tbaa !10
  %conv4 = sext i8 %5 to i32
  %sub5 = sub nsw i32 %conv4, 1
  %sh_prom6 = zext i32 %sub5 to i64
  %shl7 = shl i64 1, %sh_prom6
  %sub8 = sub nsw i64 0, %shl7
  store i64 %sub8, i64* %min_value, align 8, !tbaa !11
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  %conv9 = sext i32 %6 to i64
  %7 = load i64, i64* %min_value, align 8, !tbaa !11
  %8 = load i64, i64* %max_value, align 8, !tbaa !11
  %call = call i64 @clamp64(i64 %conv9, i64 %7, i64 %8)
  %conv10 = trunc i64 %call to i32
  store i32 %conv10, i32* %retval, align 4
  %9 = bitcast i64* %min_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %9) #6
  %10 = bitcast i64* %max_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #6
  br label %return

return:                                           ; preds = %if.end, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: inlinehint nounwind
define internal i64 @clamp64(i64 %value, i64 %low, i64 %high) #2 {
entry:
  %value.addr = alloca i64, align 8
  %low.addr = alloca i64, align 8
  %high.addr = alloca i64, align 8
  store i64 %value, i64* %value.addr, align 8, !tbaa !11
  store i64 %low, i64* %low.addr, align 8, !tbaa !11
  store i64 %high, i64* %high.addr, align 8, !tbaa !11
  %0 = load i64, i64* %value.addr, align 8, !tbaa !11
  %1 = load i64, i64* %low.addr, align 8, !tbaa !11
  %cmp = icmp slt i64 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i64, i64* %low.addr, align 8, !tbaa !11
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i64, i64* %value.addr, align 8, !tbaa !11
  %4 = load i64, i64* %high.addr, align 8, !tbaa !11
  %cmp1 = icmp sgt i64 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i64, i64* %high.addr, align 8, !tbaa !11
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i64, i64* %value.addr, align 8, !tbaa !11
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i64 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i64 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i64 %cond5
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"short", !4, i64 0}
!10 = !{!4, !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"long long", !4, i64 0}
!13 = !{!14, !4, i64 0}
!14 = !{!"TXFM_2D_FLIP_CFG", !4, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !4, i64 16, !4, i64 17, !4, i64 18, !4, i64 30, !4, i64 42, !4, i64 43, !7, i64 44, !7, i64 48}
!15 = !{!14, !3, i64 12}
!16 = !{!14, !4, i64 16}
!17 = !{!14, !4, i64 17}
!18 = !{!14, !4, i64 42}
!19 = !{!14, !4, i64 43}
!20 = !{!14, !7, i64 44}
!21 = !{!14, !7, i64 48}
!22 = !{!14, !7, i64 8}
!23 = !{!14, !7, i64 4}
