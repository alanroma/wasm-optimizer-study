; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/subtract.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/subtract.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @aom_subtract_block_c(i32 %rows, i32 %cols, i16* %diff, i32 %diff_stride, i8* %src, i32 %src_stride, i8* %pred, i32 %pred_stride) #0 {
entry:
  %rows.addr = alloca i32, align 4
  %cols.addr = alloca i32, align 4
  %diff.addr = alloca i16*, align 4
  %diff_stride.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %pred.addr = alloca i8*, align 4
  %pred_stride.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !2
  store i32 %cols, i32* %cols.addr, align 4, !tbaa !2
  store i16* %diff, i16** %diff.addr, align 4, !tbaa !6
  store i32 %diff_stride, i32* %diff_stride.addr, align 4, !tbaa !8
  store i8* %src, i8** %src.addr, align 4, !tbaa !6
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !8
  store i8* %pred, i8** %pred.addr, align 4, !tbaa !6
  store i32 %pred_stride, i32* %pred_stride.addr, align 4, !tbaa !8
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  store i32 0, i32* %r, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc10, %entry
  %2 = load i32, i32* %r, align 4, !tbaa !2
  %3 = load i32, i32* %rows.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end12

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %c, align 4, !tbaa !2
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %c, align 4, !tbaa !2
  %5 = load i32, i32* %cols.addr, align 4, !tbaa !2
  %cmp2 = icmp slt i32 %4, %5
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %6 = load i8*, i8** %src.addr, align 4, !tbaa !6
  %7 = load i32, i32* %c, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx, align 1, !tbaa !10
  %conv = zext i8 %8 to i32
  %9 = load i8*, i8** %pred.addr, align 4, !tbaa !6
  %10 = load i32, i32* %c, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx4, align 1, !tbaa !10
  %conv5 = zext i8 %11 to i32
  %sub = sub nsw i32 %conv, %conv5
  %conv6 = trunc i32 %sub to i16
  %12 = load i16*, i16** %diff.addr, align 4, !tbaa !6
  %13 = load i32, i32* %c, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i16, i16* %12, i32 %13
  store i16 %conv6, i16* %arrayidx7, align 2, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %14 = load i32, i32* %c, align 4, !tbaa !2
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %c, align 4, !tbaa !2
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %15 = load i32, i32* %diff_stride.addr, align 4, !tbaa !8
  %16 = load i16*, i16** %diff.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i16, i16* %16, i32 %15
  store i16* %add.ptr, i16** %diff.addr, align 4, !tbaa !6
  %17 = load i32, i32* %pred_stride.addr, align 4, !tbaa !8
  %18 = load i8*, i8** %pred.addr, align 4, !tbaa !6
  %add.ptr8 = getelementptr inbounds i8, i8* %18, i32 %17
  store i8* %add.ptr8, i8** %pred.addr, align 4, !tbaa !6
  %19 = load i32, i32* %src_stride.addr, align 4, !tbaa !8
  %20 = load i8*, i8** %src.addr, align 4, !tbaa !6
  %add.ptr9 = getelementptr inbounds i8, i8* %20, i32 %19
  store i8* %add.ptr9, i8** %src.addr, align 4, !tbaa !6
  br label %for.inc10

for.inc10:                                        ; preds = %for.end
  %21 = load i32, i32* %r, align 4, !tbaa !2
  %inc11 = add nsw i32 %21, 1
  store i32 %inc11, i32* %r, align 4, !tbaa !2
  br label %for.cond

for.end12:                                        ; preds = %for.cond
  %22 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #2
  %23 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_highbd_subtract_block_c(i32 %rows, i32 %cols, i16* %diff, i32 %diff_stride, i8* %src8, i32 %src_stride, i8* %pred8, i32 %pred_stride, i32 %bd) #0 {
entry:
  %rows.addr = alloca i32, align 4
  %cols.addr = alloca i32, align 4
  %diff.addr = alloca i16*, align 4
  %diff_stride.addr = alloca i32, align 4
  %src8.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %pred8.addr = alloca i8*, align 4
  %pred_stride.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %src = alloca i16*, align 4
  %pred = alloca i16*, align 4
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !2
  store i32 %cols, i32* %cols.addr, align 4, !tbaa !2
  store i16* %diff, i16** %diff.addr, align 4, !tbaa !6
  store i32 %diff_stride, i32* %diff_stride.addr, align 4, !tbaa !8
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !6
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !8
  store i8* %pred8, i8** %pred8.addr, align 4, !tbaa !6
  store i32 %pred_stride, i32* %pred_stride.addr, align 4, !tbaa !8
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !2
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i16** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = load i8*, i8** %src8.addr, align 4, !tbaa !6
  %4 = ptrtoint i8* %3 to i32
  %shl = shl i32 %4, 1
  %5 = inttoptr i32 %shl to i16*
  store i16* %5, i16** %src, align 4, !tbaa !6
  %6 = bitcast i16** %pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = load i8*, i8** %pred8.addr, align 4, !tbaa !6
  %8 = ptrtoint i8* %7 to i32
  %shl1 = shl i32 %8, 1
  %9 = inttoptr i32 %shl1 to i16*
  store i16* %9, i16** %pred, align 4, !tbaa !6
  %10 = load i32, i32* %bd.addr, align 4, !tbaa !2
  store i32 0, i32* %r, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc11, %entry
  %11 = load i32, i32* %r, align 4, !tbaa !2
  %12 = load i32, i32* %rows.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.end13

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %c, align 4, !tbaa !2
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %13 = load i32, i32* %c, align 4, !tbaa !2
  %14 = load i32, i32* %cols.addr, align 4, !tbaa !2
  %cmp3 = icmp slt i32 %13, %14
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %15 = load i16*, i16** %src, align 4, !tbaa !6
  %16 = load i32, i32* %c, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %15, i32 %16
  %17 = load i16, i16* %arrayidx, align 2, !tbaa !11
  %conv = zext i16 %17 to i32
  %18 = load i16*, i16** %pred, align 4, !tbaa !6
  %19 = load i32, i32* %c, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i16, i16* %18, i32 %19
  %20 = load i16, i16* %arrayidx5, align 2, !tbaa !11
  %conv6 = zext i16 %20 to i32
  %sub = sub nsw i32 %conv, %conv6
  %conv7 = trunc i32 %sub to i16
  %21 = load i16*, i16** %diff.addr, align 4, !tbaa !6
  %22 = load i32, i32* %c, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i16, i16* %21, i32 %22
  store i16 %conv7, i16* %arrayidx8, align 2, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %23 = load i32, i32* %c, align 4, !tbaa !2
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %c, align 4, !tbaa !2
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  %24 = load i32, i32* %diff_stride.addr, align 4, !tbaa !8
  %25 = load i16*, i16** %diff.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i16, i16* %25, i32 %24
  store i16* %add.ptr, i16** %diff.addr, align 4, !tbaa !6
  %26 = load i32, i32* %pred_stride.addr, align 4, !tbaa !8
  %27 = load i16*, i16** %pred, align 4, !tbaa !6
  %add.ptr9 = getelementptr inbounds i16, i16* %27, i32 %26
  store i16* %add.ptr9, i16** %pred, align 4, !tbaa !6
  %28 = load i32, i32* %src_stride.addr, align 4, !tbaa !8
  %29 = load i16*, i16** %src, align 4, !tbaa !6
  %add.ptr10 = getelementptr inbounds i16, i16* %29, i32 %28
  store i16* %add.ptr10, i16** %src, align 4, !tbaa !6
  br label %for.inc11

for.inc11:                                        ; preds = %for.end
  %30 = load i32, i32* %r, align 4, !tbaa !2
  %inc12 = add nsw i32 %30, 1
  store i32 %inc12, i32* %r, align 4, !tbaa !2
  br label %for.cond

for.end13:                                        ; preds = %for.cond
  %31 = bitcast i16** %pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #2
  %32 = bitcast i16** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #2
  %33 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #2
  %34 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #2
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"any pointer", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"long", !4, i64 0}
!10 = !{!4, !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"short", !4, i64 0}
