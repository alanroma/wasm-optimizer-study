; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-dec/src/rawdata.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-dec/src/rawdata.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifRWData = type { i8*, i32 }

; Function Attrs: nounwind
define hidden void @avifRWDataRealloc(%struct.avifRWData* %raw, i32 %newSize) #0 {
entry:
  %raw.addr = alloca %struct.avifRWData*, align 4
  %newSize.addr = alloca i32, align 4
  %old = alloca i8*, align 4
  %oldSize = alloca i32, align 4
  %bytesToCopy = alloca i32, align 4
  store %struct.avifRWData* %raw, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  store i32 %newSize, i32* %newSize.addr, align 4, !tbaa !6
  %0 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %0, i32 0, i32 1
  %1 = load i32, i32* %size, align 4, !tbaa !8
  %2 = load i32, i32* %newSize.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %1, %2
  br i1 %cmp, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %3 = bitcast i8** %old to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %4, i32 0, i32 0
  %5 = load i8*, i8** %data, align 4, !tbaa !10
  store i8* %5, i8** %old, align 4, !tbaa !2
  %6 = bitcast i32* %oldSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %size1 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %7, i32 0, i32 1
  %8 = load i32, i32* %size1, align 4, !tbaa !8
  store i32 %8, i32* %oldSize, align 4, !tbaa !6
  %9 = load i32, i32* %newSize.addr, align 4, !tbaa !6
  %call = call i8* @avifAlloc(i32 %9)
  %10 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %data2 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %10, i32 0, i32 0
  store i8* %call, i8** %data2, align 4, !tbaa !10
  %11 = load i32, i32* %newSize.addr, align 4, !tbaa !6
  %12 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %size3 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %12, i32 0, i32 1
  store i32 %11, i32* %size3, align 4, !tbaa !8
  %13 = load i32, i32* %oldSize, align 4, !tbaa !6
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %14 = bitcast i32* %bytesToCopy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load i32, i32* %oldSize, align 4, !tbaa !6
  %16 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %size5 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %16, i32 0, i32 1
  %17 = load i32, i32* %size5, align 4, !tbaa !8
  %cmp6 = icmp ult i32 %15, %17
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then4
  %18 = load i32, i32* %oldSize, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.then4
  %19 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %size7 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %19, i32 0, i32 1
  %20 = load i32, i32* %size7, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %18, %cond.true ], [ %20, %cond.false ]
  store i32 %cond, i32* %bytesToCopy, align 4, !tbaa !6
  %21 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %data8 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %21, i32 0, i32 0
  %22 = load i8*, i8** %data8, align 4, !tbaa !10
  %23 = load i8*, i8** %old, align 4, !tbaa !2
  %24 = load i32, i32* %bytesToCopy, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %22, i8* align 1 %23, i32 %24, i1 false)
  %25 = load i8*, i8** %old, align 4, !tbaa !2
  call void @avifFree(i8* %25)
  %26 = bitcast i32* %bytesToCopy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %27 = bitcast i32* %oldSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  %28 = bitcast i8** %old to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @avifAlloc(i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare void @avifFree(i8*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @avifRWDataSet(%struct.avifRWData* %raw, i8* %data, i32 %len) #0 {
entry:
  %raw.addr = alloca %struct.avifRWData*, align 4
  %data.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  store %struct.avifRWData* %raw, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !6
  %0 = load i32, i32* %len.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %2 = load i32, i32* %len.addr, align 4, !tbaa !6
  call void @avifRWDataRealloc(%struct.avifRWData* %1, i32 %2)
  %3 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %3, i32 0, i32 0
  %4 = load i8*, i8** %data1, align 4, !tbaa !10
  %5 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %6 = load i32, i32* %len.addr, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %4, i8* align 1 %5, i32 %6, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  call void @avifRWDataFree(%struct.avifRWData* %7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @avifRWDataFree(%struct.avifRWData* %raw) #0 {
entry:
  %raw.addr = alloca %struct.avifRWData*, align 4
  store %struct.avifRWData* %raw, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %0 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %0, i32 0, i32 0
  %1 = load i8*, i8** %data, align 4, !tbaa !10
  call void @avifFree(i8* %1)
  %2 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %2, i32 0, i32 0
  store i8* null, i8** %data1, align 4, !tbaa !10
  %3 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %3, i32 0, i32 1
  store i32 0, i32* %size, align 4, !tbaa !8
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !7, i64 4}
!9 = !{!"avifRWData", !3, i64 0, !7, i64 4}
!10 = !{!9, !3, i64 0}
