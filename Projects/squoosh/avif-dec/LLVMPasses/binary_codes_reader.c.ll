; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/binary_codes_reader.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/binary_codes_reader.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_reader = type { i8*, i8*, %struct.od_ec_dec, %struct.Accounting*, i8 }
%struct.od_ec_dec = type { i8*, i32, i8*, i8*, i32, i16, i16 }
%struct.Accounting = type { %struct.AccountingSymbols, i32, [1021 x i16], %struct.AccountingSymbolContext, i32 }
%struct.AccountingSymbols = type { %struct.AccountingSymbol*, i32, i32, i32, %struct.AccountingDictionary }
%struct.AccountingSymbol = type { %struct.AccountingSymbolContext, i32, i32, i32 }
%struct.AccountingDictionary = type { [256 x i8*], i32 }
%struct.AccountingSymbolContext = type { i16, i16 }

; Function Attrs: nounwind
define hidden zeroext i16 @aom_read_primitive_quniform_(%struct.aom_reader* %r, i16 zeroext %n, i8* %acct_str) #0 {
entry:
  %retval = alloca i16, align 2
  %r.addr = alloca %struct.aom_reader*, align 4
  %n.addr = alloca i16, align 2
  %acct_str.addr = alloca i8*, align 4
  %l = alloca i32, align 4
  %m = alloca i32, align 4
  %v = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %cmp = icmp sle i32 %conv, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i16 0, i16* %retval, align 2
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv2 = zext i16 %2 to i32
  %call = call i32 @get_msb(i32 %conv2)
  %add = add nsw i32 %call, 1
  store i32 %add, i32* %l, align 4, !tbaa !8
  %3 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i32, i32* %l, align 4, !tbaa !8
  %shl = shl i32 1, %4
  %5 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv3 = zext i16 %5 to i32
  %sub = sub nsw i32 %shl, %conv3
  store i32 %sub, i32* %m, align 4, !tbaa !8
  %6 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %8 = load i32, i32* %l, align 4, !tbaa !8
  %sub4 = sub nsw i32 %8, 1
  %9 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %call5 = call i32 @aom_read_literal_(%struct.aom_reader* %7, i32 %sub4, i8* %9)
  store i32 %call5, i32* %v, align 4, !tbaa !8
  %10 = load i32, i32* %v, align 4, !tbaa !8
  %11 = load i32, i32* %m, align 4, !tbaa !8
  %cmp6 = icmp slt i32 %10, %11
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %12 = load i32, i32* %v, align 4, !tbaa !8
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %13 = load i32, i32* %v, align 4, !tbaa !8
  %shl8 = shl i32 %13, 1
  %14 = load i32, i32* %m, align 4, !tbaa !8
  %sub9 = sub nsw i32 %shl8, %14
  %15 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %16 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %call10 = call i32 @aom_read_bit_(%struct.aom_reader* %15, i8* %16)
  %add11 = add nsw i32 %sub9, %call10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %12, %cond.true ], [ %add11, %cond.false ]
  %conv12 = trunc i32 %cond to i16
  store i16 %conv12, i16* %retval, align 2
  %17 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %20 = load i16, i16* %retval, align 2
  ret i16 %20
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #2 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_literal_(%struct.aom_reader* %r, i32 %bits, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %bits.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %literal = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !8
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %literal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %literal, align 4, !tbaa !8
  %1 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i32, i32* %bits.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %2, 1
  store i32 %sub, i32* %bit, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %bit, align 4, !tbaa !8
  %cmp = icmp sge i32 %3, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_read_bit_(%struct.aom_reader* %4, i8* null)
  %5 = load i32, i32* %bit, align 4, !tbaa !8
  %shl = shl i32 %call, %5
  %6 = load i32, i32* %literal, align 4, !tbaa !8
  %or = or i32 %6, %shl
  store i32 %or, i32* %literal, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %bit, align 4, !tbaa !8
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* %bit, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %8, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %9 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %9, i8* %10)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %11 = load i32, i32* %literal, align 4, !tbaa !8
  %12 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  %13 = bitcast i32* %literal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  ret i32 %11
}

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_bit_(%struct.aom_reader* %r, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %acct_str.addr = alloca i8*, align 4
  %ret = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_read_(%struct.aom_reader* %1, i32 128, i8* null)
  store i32 %call, i32* %ret, align 4, !tbaa !8
  %2 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %3, i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i32, i32* %ret, align 4, !tbaa !8
  %6 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #5
  ret i32 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden zeroext i16 @aom_read_primitive_subexpfin_(%struct.aom_reader* %r, i16 zeroext %n, i16 zeroext %k, i8* %acct_str) #0 {
entry:
  %retval = alloca i16, align 2
  %r.addr = alloca %struct.aom_reader*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %acct_str.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %mk = alloca i32, align 4
  %b = alloca i32, align 4
  %a = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %k, i16* %k.addr, align 2, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !8
  %1 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %mk, align 4, !tbaa !8
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  br label %while.body

while.body:                                       ; preds = %while.cond
  %2 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %i, align 4, !tbaa !8
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %4 = load i16, i16* %k.addr, align 2, !tbaa !6
  %conv = zext i16 %4 to i32
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %add = add nsw i32 %conv, %5
  %sub = sub nsw i32 %add, 1
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %6 = load i16, i16* %k.addr, align 2, !tbaa !6
  %conv1 = zext i16 %6 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ %conv1, %cond.false ]
  store i32 %cond, i32* %b, align 4, !tbaa !8
  %7 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %b, align 4, !tbaa !8
  %shl = shl i32 1, %8
  store i32 %shl, i32* %a, align 4, !tbaa !8
  %9 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv2 = zext i16 %9 to i32
  %10 = load i32, i32* %mk, align 4, !tbaa !8
  %11 = load i32, i32* %a, align 4, !tbaa !8
  %mul = mul nsw i32 3, %11
  %add3 = add nsw i32 %10, %mul
  %cmp = icmp sle i32 %conv2, %add3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %12 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %13 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv5 = zext i16 %13 to i32
  %14 = load i32, i32* %mk, align 4, !tbaa !8
  %sub6 = sub nsw i32 %conv5, %14
  %conv7 = trunc i32 %sub6 to i16
  %15 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %call = call zeroext i16 @aom_read_primitive_quniform_(%struct.aom_reader* %12, i16 zeroext %conv7, i8* %15)
  %conv8 = zext i16 %call to i32
  %16 = load i32, i32* %mk, align 4, !tbaa !8
  %add9 = add nsw i32 %conv8, %16
  %conv10 = trunc i32 %add9 to i16
  store i16 %conv10, i16* %retval, align 2
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %cond.end
  %17 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %call11 = call i32 @aom_read_bit_(%struct.aom_reader* %17, i8* %18)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end17, label %if.then13

if.then13:                                        ; preds = %if.end
  %19 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %20 = load i32, i32* %b, align 4, !tbaa !8
  %21 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %call14 = call i32 @aom_read_literal_(%struct.aom_reader* %19, i32 %20, i8* %21)
  %22 = load i32, i32* %mk, align 4, !tbaa !8
  %add15 = add nsw i32 %call14, %22
  %conv16 = trunc i32 %add15 to i16
  store i16 %conv16, i16* %retval, align 2
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %if.end
  %23 = load i32, i32* %i, align 4, !tbaa !8
  %add18 = add nsw i32 %23, 1
  store i32 %add18, i32* %i, align 4, !tbaa !8
  %24 = load i32, i32* %a, align 4, !tbaa !8
  %25 = load i32, i32* %mk, align 4, !tbaa !8
  %add19 = add nsw i32 %25, %24
  store i32 %add19, i32* %mk, align 4, !tbaa !8
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end17, %if.then13, %if.then
  %26 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup21 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

cleanup21:                                        ; preds = %cleanup
  %28 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  %30 = load i16, i16* %retval, align 2
  ret i16 %30
}

; Function Attrs: nounwind
define hidden zeroext i16 @aom_read_primitive_refsubexpfin_(%struct.aom_reader* %r, i16 zeroext %n, i16 zeroext %k, i16 zeroext %ref, i8* %acct_str) #0 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %ref.addr = alloca i16, align 2
  %acct_str.addr = alloca i8*, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %k, i16* %k.addr, align 2, !tbaa !6
  store i16 %ref, i16* %ref.addr, align 2, !tbaa !6
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = load i16, i16* %n.addr, align 2, !tbaa !6
  %1 = load i16, i16* %ref.addr, align 2, !tbaa !6
  %2 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %3 = load i16, i16* %n.addr, align 2, !tbaa !6
  %4 = load i16, i16* %k.addr, align 2, !tbaa !6
  %5 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %call = call zeroext i16 @aom_read_primitive_subexpfin_(%struct.aom_reader* %2, i16 zeroext %3, i16 zeroext %4, i8* %5)
  %call1 = call zeroext i16 @inv_recenter_finite_nonneg(i16 zeroext %0, i16 zeroext %1, i16 zeroext %call)
  ret i16 %call1
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @inv_recenter_finite_nonneg(i16 zeroext %n, i16 zeroext %r, i16 zeroext %v) #2 {
entry:
  %retval = alloca i16, align 2
  %n.addr = alloca i16, align 2
  %r.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store i16 %n, i16* %n.addr, align 2, !tbaa !6
  store i16 %r, i16* %r.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %shl = shl i32 %conv, 1
  %1 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv1 = zext i16 %1 to i32
  %cmp = icmp sle i32 %shl, %conv1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i16, i16* %r.addr, align 2, !tbaa !6
  %3 = load i16, i16* %v.addr, align 2, !tbaa !6
  %call = call zeroext i16 @inv_recenter_nonneg(i16 zeroext %2, i16 zeroext %3)
  store i16 %call, i16* %retval, align 2
  br label %return

if.else:                                          ; preds = %entry
  %4 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv3 = zext i16 %4 to i32
  %sub = sub nsw i32 %conv3, 1
  %5 = load i16, i16* %n.addr, align 2, !tbaa !6
  %conv4 = zext i16 %5 to i32
  %sub5 = sub nsw i32 %conv4, 1
  %6 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv6 = zext i16 %6 to i32
  %sub7 = sub nsw i32 %sub5, %conv6
  %conv8 = trunc i32 %sub7 to i16
  %7 = load i16, i16* %v.addr, align 2, !tbaa !6
  %call9 = call zeroext i16 @inv_recenter_nonneg(i16 zeroext %conv8, i16 zeroext %7)
  %conv10 = zext i16 %call9 to i32
  %sub11 = sub nsw i32 %sub, %conv10
  %conv12 = trunc i32 %sub11 to i16
  store i16 %conv12, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.else, %if.then
  %8 = load i16, i16* %retval, align 2
  ret i16 %8
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #3

; Function Attrs: inlinehint nounwind
define internal void @aom_process_accounting(%struct.aom_reader* %r, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %acct_str.addr = alloca i8*, align 4
  %tell_frac = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting, align 4, !tbaa !10
  %cmp = icmp ne %struct.Accounting* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %tell_frac to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_reader_tell_frac(%struct.aom_reader* %3)
  store i32 %call, i32* %tell_frac, align 4, !tbaa !8
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting1 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %4, i32 0, i32 3
  %5 = load %struct.Accounting*, %struct.Accounting** %accounting1, align 4, !tbaa !10
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %7 = load i32, i32* %tell_frac, align 4, !tbaa !8
  %8 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting2 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %8, i32 0, i32 3
  %9 = load %struct.Accounting*, %struct.Accounting** %accounting2, align 4, !tbaa !10
  %last_tell_frac = getelementptr inbounds %struct.Accounting, %struct.Accounting* %9, i32 0, i32 4
  %10 = load i32, i32* %last_tell_frac, align 4, !tbaa !13
  %sub = sub i32 %7, %10
  call void @aom_accounting_record(%struct.Accounting* %5, i8* %6, i32 %sub)
  %11 = load i32, i32* %tell_frac, align 4, !tbaa !8
  %12 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting3 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %12, i32 0, i32 3
  %13 = load %struct.Accounting*, %struct.Accounting** %accounting3, align 4, !tbaa !10
  %last_tell_frac4 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %13, i32 0, i32 4
  store i32 %11, i32* %last_tell_frac4, align 4, !tbaa !13
  %14 = bitcast i32* %tell_frac to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare i32 @aom_reader_tell_frac(%struct.aom_reader*) #4

declare void @aom_accounting_record(%struct.Accounting*, i8*, i32) #4

; Function Attrs: inlinehint nounwind
define internal i32 @aom_read_(%struct.aom_reader* %r, i32 %prob, i8* %acct_str) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %prob.addr = alloca i32, align 4
  %acct_str.addr = alloca i8*, align 4
  %p = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %prob, i32* %prob.addr, align 4, !tbaa !8
  store i8* %acct_str, i8** %acct_str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %prob.addr, align 4, !tbaa !8
  %shl = shl i32 %1, 15
  %sub = sub nsw i32 8388607, %shl
  %2 = load i32, i32* %prob.addr, align 4, !tbaa !8
  %add = add nsw i32 %sub, %2
  %shr = ashr i32 %add, 8
  store i32 %shr, i32* %p, align 4, !tbaa !8
  %3 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %4, i32 0, i32 2
  %5 = load i32, i32* %p, align 4, !tbaa !8
  %call = call i32 @od_ec_decode_bool_q15(%struct.od_ec_dec* %ec, i32 %5)
  store i32 %call, i32* %bit, align 4, !tbaa !8
  %6 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %8 = load i8*, i8** %acct_str.addr, align 4, !tbaa !2
  call void @aom_process_accounting(%struct.aom_reader* %7, i8* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  call void @aom_update_symb_counts(%struct.aom_reader* %9, i32 1)
  %10 = load i32, i32* %bit, align 4, !tbaa !8
  %11 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #5
  %12 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  ret i32 %10
}

declare i32 @od_ec_decode_bool_q15(%struct.od_ec_dec*, i32) #4

; Function Attrs: inlinehint nounwind
define internal void @aom_update_symb_counts(%struct.aom_reader* %r, i32 %is_binary) #2 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %is_binary.addr = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i32 %is_binary, i32* %is_binary.addr, align 4, !tbaa !8
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting, align 4, !tbaa !10
  %cmp = icmp ne %struct.Accounting* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %is_binary.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %2, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %3 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting1 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %3, i32 0, i32 3
  %4 = load %struct.Accounting*, %struct.Accounting** %accounting1, align 4, !tbaa !10
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %4, i32 0, i32 0
  %num_multi_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 2
  %5 = load i32, i32* %num_multi_syms, align 4, !tbaa !18
  %add = add nsw i32 %5, %lnot.ext
  store i32 %add, i32* %num_multi_syms, align 4, !tbaa !18
  %6 = load i32, i32* %is_binary.addr, align 4, !tbaa !8
  %tobool2 = icmp ne i32 %6, 0
  %lnot3 = xor i1 %tobool2, true
  %lnot5 = xor i1 %lnot3, true
  %lnot.ext6 = zext i1 %lnot5 to i32
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting7 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %7, i32 0, i32 3
  %8 = load %struct.Accounting*, %struct.Accounting** %accounting7, align 4, !tbaa !10
  %syms8 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %8, i32 0, i32 0
  %num_binary_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms8, i32 0, i32 3
  %9 = load i32, i32* %num_binary_syms, align 4, !tbaa !19
  %add9 = add nsw i32 %9, %lnot.ext6
  store i32 %add9, i32* %num_binary_syms, align 4, !tbaa !19
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @inv_recenter_nonneg(i16 zeroext %r, i16 zeroext %v) #2 {
entry:
  %retval = alloca i16, align 2
  %r.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store i16 %r, i16* %r.addr, align 2, !tbaa !6
  store i16 %v, i16* %v.addr, align 2, !tbaa !6
  %0 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %1 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv1 = zext i16 %1 to i32
  %shl = shl i32 %conv1, 1
  %cmp = icmp sgt i32 %conv, %shl
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i16, i16* %v.addr, align 2, !tbaa !6
  store i16 %2, i16* %retval, align 2
  br label %return

if.else:                                          ; preds = %entry
  %3 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv3 = zext i16 %3 to i32
  %and = and i32 %conv3, 1
  %cmp4 = icmp eq i32 %and, 0
  br i1 %cmp4, label %if.then6, label %if.else10

if.then6:                                         ; preds = %if.else
  %4 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv7 = zext i16 %4 to i32
  %shr = ashr i32 %conv7, 1
  %5 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv8 = zext i16 %5 to i32
  %add = add nsw i32 %shr, %conv8
  %conv9 = trunc i32 %add to i16
  store i16 %conv9, i16* %retval, align 2
  br label %return

if.else10:                                        ; preds = %if.else
  %6 = load i16, i16* %r.addr, align 2, !tbaa !6
  %conv11 = zext i16 %6 to i32
  %7 = load i16, i16* %v.addr, align 2, !tbaa !6
  %conv12 = zext i16 %7 to i32
  %add13 = add nsw i32 %conv12, 1
  %shr14 = ashr i32 %add13, 1
  %sub = sub nsw i32 %conv11, %shr14
  %conv15 = trunc i32 %sub to i16
  store i16 %conv15, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.else10, %if.then6, %if.then
  %8 = load i16, i16* %retval, align 2
  ret i16 %8
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone speculatable willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"short", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!11, !3, i64 32}
!11 = !{!"aom_reader", !3, i64 0, !3, i64 4, !12, i64 8, !3, i64 32, !4, i64 36}
!12 = !{!"od_ec_dec", !3, i64 0, !9, i64 4, !3, i64 8, !3, i64 12, !9, i64 16, !7, i64 20, !7, i64 22}
!13 = !{!14, !9, i64 3096}
!14 = !{!"Accounting", !15, i64 0, !9, i64 1044, !4, i64 1048, !17, i64 3090, !9, i64 3096}
!15 = !{!"", !3, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !16, i64 16}
!16 = !{!"", !4, i64 0, !9, i64 1024}
!17 = !{!"", !7, i64 0, !7, i64 2}
!18 = !{!14, !9, i64 8}
!19 = !{!14, !9, i64 12}
