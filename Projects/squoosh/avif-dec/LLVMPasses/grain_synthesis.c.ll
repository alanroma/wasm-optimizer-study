; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/grain_synthesis.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/grain_synthesis.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.aom_image = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.aom_metadata_array*, i8* }
%struct.aom_metadata_array = type opaque

@stderr = external constant %struct._IO_FILE*, align 4
@.str = private unnamed_addr constant [49 x i8] c"Film grain error: input format is not supported!\00", align 1
@random_register = internal global i16 0, align 2
@luma_subblock_size_y = internal global i32 32, align 4
@luma_subblock_size_x = internal global i32 32, align 4
@chroma_subblock_size_y = internal global i32 16, align 4
@chroma_subblock_size_x = internal global i32 16, align 4
@grain_min = internal global i32 0, align 4
@grain_max = internal global i32 0, align 4
@scaling_lut_y = internal global [256 x i32] zeroinitializer, align 16
@scaling_lut_cb = internal global [256 x i32] zeroinitializer, align 16
@scaling_lut_cr = internal global [256 x i32] zeroinitializer, align 16
@gaussian_sequence = internal constant [2048 x i32] [i32 56, i32 568, i32 -180, i32 172, i32 124, i32 -84, i32 172, i32 -64, i32 -900, i32 24, i32 820, i32 224, i32 1248, i32 996, i32 272, i32 -8, i32 -916, i32 -388, i32 -732, i32 -104, i32 -188, i32 800, i32 112, i32 -652, i32 -320, i32 -376, i32 140, i32 -252, i32 492, i32 -168, i32 44, i32 -788, i32 588, i32 -584, i32 500, i32 -228, i32 12, i32 680, i32 272, i32 -476, i32 972, i32 -100, i32 652, i32 368, i32 432, i32 -196, i32 -720, i32 -192, i32 1000, i32 -332, i32 652, i32 -136, i32 -552, i32 -604, i32 -4, i32 192, i32 -220, i32 -136, i32 1000, i32 -52, i32 372, i32 -96, i32 -624, i32 124, i32 -24, i32 396, i32 540, i32 -12, i32 -104, i32 640, i32 464, i32 244, i32 -208, i32 -84, i32 368, i32 -528, i32 -740, i32 248, i32 -968, i32 -848, i32 608, i32 376, i32 -60, i32 -292, i32 -40, i32 -156, i32 252, i32 -292, i32 248, i32 224, i32 -280, i32 400, i32 -244, i32 244, i32 -60, i32 76, i32 -80, i32 212, i32 532, i32 340, i32 128, i32 -36, i32 824, i32 -352, i32 -60, i32 -264, i32 -96, i32 -612, i32 416, i32 -704, i32 220, i32 -204, i32 640, i32 -160, i32 1220, i32 -408, i32 900, i32 336, i32 20, i32 -336, i32 -96, i32 -792, i32 304, i32 48, i32 -28, i32 -1232, i32 -1172, i32 -448, i32 104, i32 -292, i32 -520, i32 244, i32 60, i32 -948, i32 0, i32 -708, i32 268, i32 108, i32 356, i32 -548, i32 488, i32 -344, i32 -136, i32 488, i32 -196, i32 -224, i32 656, i32 -236, i32 -1128, i32 60, i32 4, i32 140, i32 276, i32 -676, i32 -376, i32 168, i32 -108, i32 464, i32 8, i32 564, i32 64, i32 240, i32 308, i32 -300, i32 -400, i32 -456, i32 -136, i32 56, i32 120, i32 -408, i32 -116, i32 436, i32 504, i32 -232, i32 328, i32 844, i32 -164, i32 -84, i32 784, i32 -168, i32 232, i32 -224, i32 348, i32 -376, i32 128, i32 568, i32 96, i32 -1244, i32 -288, i32 276, i32 848, i32 832, i32 -360, i32 656, i32 464, i32 -384, i32 -332, i32 -356, i32 728, i32 -388, i32 160, i32 -192, i32 468, i32 296, i32 224, i32 140, i32 -776, i32 -100, i32 280, i32 4, i32 196, i32 44, i32 -36, i32 -648, i32 932, i32 16, i32 1428, i32 28, i32 528, i32 808, i32 772, i32 20, i32 268, i32 88, i32 -332, i32 -284, i32 124, i32 -384, i32 -448, i32 208, i32 -228, i32 -1044, i32 -328, i32 660, i32 380, i32 -148, i32 -300, i32 588, i32 240, i32 540, i32 28, i32 136, i32 -88, i32 -436, i32 256, i32 296, i32 -1000, i32 1400, i32 0, i32 -48, i32 1056, i32 -136, i32 264, i32 -528, i32 -1108, i32 632, i32 -484, i32 -592, i32 -344, i32 796, i32 124, i32 -668, i32 -768, i32 388, i32 1296, i32 -232, i32 -188, i32 -200, i32 -288, i32 -4, i32 308, i32 100, i32 -168, i32 256, i32 -500, i32 204, i32 -508, i32 648, i32 -136, i32 372, i32 -272, i32 -120, i32 -1004, i32 -552, i32 -548, i32 -384, i32 548, i32 -296, i32 428, i32 -108, i32 -8, i32 -912, i32 -324, i32 -224, i32 -88, i32 -112, i32 -220, i32 -100, i32 996, i32 -796, i32 548, i32 360, i32 -216, i32 180, i32 428, i32 -200, i32 -212, i32 148, i32 96, i32 148, i32 284, i32 216, i32 -412, i32 -320, i32 120, i32 -300, i32 -384, i32 -604, i32 -572, i32 -332, i32 -8, i32 -180, i32 -176, i32 696, i32 116, i32 -88, i32 628, i32 76, i32 44, i32 -516, i32 240, i32 -208, i32 -40, i32 100, i32 -592, i32 344, i32 -308, i32 -452, i32 -228, i32 20, i32 916, i32 -1752, i32 -136, i32 -340, i32 -804, i32 140, i32 40, i32 512, i32 340, i32 248, i32 184, i32 -492, i32 896, i32 -156, i32 932, i32 -628, i32 328, i32 -688, i32 -448, i32 -616, i32 -752, i32 -100, i32 560, i32 -1020, i32 180, i32 -800, i32 -64, i32 76, i32 576, i32 1068, i32 396, i32 660, i32 552, i32 -108, i32 -28, i32 320, i32 -628, i32 312, i32 -92, i32 -92, i32 -472, i32 268, i32 16, i32 560, i32 516, i32 -672, i32 -52, i32 492, i32 -100, i32 260, i32 384, i32 284, i32 292, i32 304, i32 -148, i32 88, i32 -152, i32 1012, i32 1064, i32 -228, i32 164, i32 -376, i32 -684, i32 592, i32 -392, i32 156, i32 196, i32 -524, i32 -64, i32 -884, i32 160, i32 -176, i32 636, i32 648, i32 404, i32 -396, i32 -436, i32 864, i32 424, i32 -728, i32 988, i32 -604, i32 904, i32 -592, i32 296, i32 -224, i32 536, i32 -176, i32 -920, i32 436, i32 -48, i32 1176, i32 -884, i32 416, i32 -776, i32 -824, i32 -884, i32 524, i32 -548, i32 -564, i32 -68, i32 -164, i32 -96, i32 692, i32 364, i32 -692, i32 -1012, i32 -68, i32 260, i32 -480, i32 876, i32 -1116, i32 452, i32 -332, i32 -352, i32 892, i32 -1088, i32 1220, i32 -676, i32 12, i32 -292, i32 244, i32 496, i32 372, i32 -32, i32 280, i32 200, i32 112, i32 -440, i32 -96, i32 24, i32 -644, i32 -184, i32 56, i32 -432, i32 224, i32 -980, i32 272, i32 -260, i32 144, i32 -436, i32 420, i32 356, i32 364, i32 -528, i32 76, i32 172, i32 -744, i32 -368, i32 404, i32 -752, i32 -416, i32 684, i32 -688, i32 72, i32 540, i32 416, i32 92, i32 444, i32 480, i32 -72, i32 -1416, i32 164, i32 -1172, i32 -68, i32 24, i32 424, i32 264, i32 1040, i32 128, i32 -912, i32 -524, i32 -356, i32 64, i32 876, i32 -12, i32 4, i32 -88, i32 532, i32 272, i32 -524, i32 320, i32 276, i32 -508, i32 940, i32 24, i32 -400, i32 -120, i32 756, i32 60, i32 236, i32 -412, i32 100, i32 376, i32 -484, i32 400, i32 -100, i32 -740, i32 -108, i32 -260, i32 328, i32 -268, i32 224, i32 -200, i32 -416, i32 184, i32 -604, i32 -564, i32 -20, i32 296, i32 60, i32 892, i32 -888, i32 60, i32 164, i32 68, i32 -760, i32 216, i32 -296, i32 904, i32 -336, i32 -28, i32 404, i32 -356, i32 -568, i32 -208, i32 -1480, i32 -512, i32 296, i32 328, i32 -360, i32 -164, i32 -1560, i32 -776, i32 1156, i32 -428, i32 164, i32 -504, i32 -112, i32 120, i32 -216, i32 -148, i32 -264, i32 308, i32 32, i32 64, i32 -72, i32 72, i32 116, i32 176, i32 -64, i32 -272, i32 460, i32 -536, i32 -784, i32 -280, i32 348, i32 108, i32 -752, i32 -132, i32 524, i32 -540, i32 -776, i32 116, i32 -296, i32 -1196, i32 -288, i32 -560, i32 1040, i32 -472, i32 116, i32 -848, i32 -1116, i32 116, i32 636, i32 696, i32 284, i32 -176, i32 1016, i32 204, i32 -864, i32 -648, i32 -248, i32 356, i32 972, i32 -584, i32 -204, i32 264, i32 880, i32 528, i32 -24, i32 -184, i32 116, i32 448, i32 -144, i32 828, i32 524, i32 212, i32 -212, i32 52, i32 12, i32 200, i32 268, i32 -488, i32 -404, i32 -880, i32 824, i32 -672, i32 -40, i32 908, i32 -248, i32 500, i32 716, i32 -576, i32 492, i32 -576, i32 16, i32 720, i32 -108, i32 384, i32 124, i32 344, i32 280, i32 576, i32 -500, i32 252, i32 104, i32 -308, i32 196, i32 -188, i32 -8, i32 1268, i32 296, i32 1032, i32 -1196, i32 436, i32 316, i32 372, i32 -432, i32 -200, i32 -660, i32 704, i32 -224, i32 596, i32 -132, i32 268, i32 32, i32 -452, i32 884, i32 104, i32 -1008, i32 424, i32 -1348, i32 -280, i32 4, i32 -1168, i32 368, i32 476, i32 696, i32 300, i32 -8, i32 24, i32 180, i32 -592, i32 -196, i32 388, i32 304, i32 500, i32 724, i32 -160, i32 244, i32 -84, i32 272, i32 -256, i32 -420, i32 320, i32 208, i32 -144, i32 -156, i32 156, i32 364, i32 452, i32 28, i32 540, i32 316, i32 220, i32 -644, i32 -248, i32 464, i32 72, i32 360, i32 32, i32 -388, i32 496, i32 -680, i32 -48, i32 208, i32 -116, i32 -408, i32 60, i32 -604, i32 -392, i32 548, i32 -840, i32 784, i32 -460, i32 656, i32 -544, i32 -388, i32 -264, i32 908, i32 -800, i32 -628, i32 -612, i32 -568, i32 572, i32 -220, i32 164, i32 288, i32 -16, i32 -308, i32 308, i32 -112, i32 -636, i32 -760, i32 280, i32 -668, i32 432, i32 364, i32 240, i32 -196, i32 604, i32 340, i32 384, i32 196, i32 592, i32 -44, i32 -500, i32 432, i32 -580, i32 -132, i32 636, i32 -76, i32 392, i32 4, i32 -412, i32 540, i32 508, i32 328, i32 -356, i32 -36, i32 16, i32 -220, i32 -64, i32 -248, i32 -60, i32 24, i32 -192, i32 368, i32 1040, i32 92, i32 -24, i32 -1044, i32 -32, i32 40, i32 104, i32 148, i32 192, i32 -136, i32 -520, i32 56, i32 -816, i32 -224, i32 732, i32 392, i32 356, i32 212, i32 -80, i32 -424, i32 -1008, i32 -324, i32 588, i32 -1496, i32 576, i32 460, i32 -816, i32 -848, i32 56, i32 -580, i32 -92, i32 -1372, i32 -112, i32 -496, i32 200, i32 364, i32 52, i32 -140, i32 48, i32 -48, i32 -60, i32 84, i32 72, i32 40, i32 132, i32 -356, i32 -268, i32 -104, i32 -284, i32 -404, i32 732, i32 -520, i32 164, i32 -304, i32 -540, i32 120, i32 328, i32 -76, i32 -460, i32 756, i32 388, i32 588, i32 236, i32 -436, i32 -72, i32 -176, i32 -404, i32 -316, i32 -148, i32 716, i32 -604, i32 404, i32 -72, i32 -88, i32 -888, i32 -68, i32 944, i32 88, i32 -220, i32 -344, i32 960, i32 472, i32 460, i32 -232, i32 704, i32 120, i32 832, i32 -228, i32 692, i32 -508, i32 132, i32 -476, i32 844, i32 -748, i32 -364, i32 -44, i32 1116, i32 -1104, i32 -1056, i32 76, i32 428, i32 552, i32 -692, i32 60, i32 356, i32 96, i32 -384, i32 -188, i32 -612, i32 -576, i32 736, i32 508, i32 892, i32 352, i32 -1132, i32 504, i32 -24, i32 -352, i32 324, i32 332, i32 -600, i32 -312, i32 292, i32 508, i32 -144, i32 -8, i32 484, i32 48, i32 284, i32 -260, i32 -240, i32 256, i32 -100, i32 -292, i32 -204, i32 -44, i32 472, i32 -204, i32 908, i32 -188, i32 -1000, i32 -256, i32 92, i32 1164, i32 -392, i32 564, i32 356, i32 652, i32 -28, i32 -884, i32 256, i32 484, i32 -192, i32 760, i32 -176, i32 376, i32 -524, i32 -452, i32 -436, i32 860, i32 -736, i32 212, i32 124, i32 504, i32 -476, i32 468, i32 76, i32 -472, i32 552, i32 -692, i32 -944, i32 -620, i32 740, i32 -240, i32 400, i32 132, i32 20, i32 192, i32 -196, i32 264, i32 -668, i32 -1012, i32 -60, i32 296, i32 -316, i32 -828, i32 76, i32 -156, i32 284, i32 -768, i32 -448, i32 -832, i32 148, i32 248, i32 652, i32 616, i32 1236, i32 288, i32 -328, i32 -400, i32 -124, i32 588, i32 220, i32 520, i32 -696, i32 1032, i32 768, i32 -740, i32 -92, i32 -272, i32 296, i32 448, i32 -464, i32 412, i32 -200, i32 392, i32 440, i32 -200, i32 264, i32 -152, i32 -260, i32 320, i32 1032, i32 216, i32 320, i32 -8, i32 -64, i32 156, i32 -1016, i32 1084, i32 1172, i32 536, i32 484, i32 -432, i32 132, i32 372, i32 -52, i32 -256, i32 84, i32 116, i32 -352, i32 48, i32 116, i32 304, i32 -384, i32 412, i32 924, i32 -300, i32 528, i32 628, i32 180, i32 648, i32 44, i32 -980, i32 -220, i32 1320, i32 48, i32 332, i32 748, i32 524, i32 -268, i32 -720, i32 540, i32 -276, i32 564, i32 -344, i32 -208, i32 -196, i32 436, i32 896, i32 88, i32 -392, i32 132, i32 80, i32 -964, i32 -288, i32 568, i32 56, i32 -48, i32 -456, i32 888, i32 8, i32 552, i32 -156, i32 -292, i32 948, i32 288, i32 128, i32 -716, i32 -292, i32 1192, i32 -152, i32 876, i32 352, i32 -600, i32 -260, i32 -812, i32 -468, i32 -28, i32 -120, i32 -32, i32 -44, i32 1284, i32 496, i32 192, i32 464, i32 312, i32 -76, i32 -516, i32 -380, i32 -456, i32 -1012, i32 -48, i32 308, i32 -156, i32 36, i32 492, i32 -156, i32 -808, i32 188, i32 1652, i32 68, i32 -120, i32 -116, i32 316, i32 160, i32 -140, i32 352, i32 808, i32 -416, i32 592, i32 316, i32 -480, i32 56, i32 528, i32 -204, i32 -568, i32 372, i32 -232, i32 752, i32 -344, i32 744, i32 -4, i32 324, i32 -416, i32 -600, i32 768, i32 268, i32 -248, i32 -88, i32 -132, i32 -420, i32 -432, i32 80, i32 -288, i32 404, i32 -316, i32 -1216, i32 -588, i32 520, i32 -108, i32 92, i32 -320, i32 368, i32 -480, i32 -216, i32 -92, i32 1688, i32 -300, i32 180, i32 1020, i32 -176, i32 820, i32 -68, i32 -228, i32 -260, i32 436, i32 -904, i32 20, i32 40, i32 -508, i32 440, i32 -736, i32 312, i32 332, i32 204, i32 760, i32 -372, i32 728, i32 96, i32 -20, i32 -632, i32 -520, i32 -560, i32 336, i32 1076, i32 -64, i32 -532, i32 776, i32 584, i32 192, i32 396, i32 -728, i32 -520, i32 276, i32 -188, i32 80, i32 -52, i32 -612, i32 -252, i32 -48, i32 648, i32 212, i32 -688, i32 228, i32 -52, i32 -260, i32 428, i32 -412, i32 -272, i32 -404, i32 180, i32 816, i32 -796, i32 48, i32 152, i32 484, i32 -88, i32 -216, i32 988, i32 696, i32 188, i32 -528, i32 648, i32 -116, i32 -180, i32 316, i32 476, i32 12, i32 -564, i32 96, i32 476, i32 -252, i32 -364, i32 -376, i32 -392, i32 556, i32 -256, i32 -576, i32 260, i32 -352, i32 120, i32 -16, i32 -136, i32 -260, i32 -492, i32 72, i32 556, i32 660, i32 580, i32 616, i32 772, i32 436, i32 424, i32 -32, i32 -324, i32 -1268, i32 416, i32 -324, i32 -80, i32 920, i32 160, i32 228, i32 724, i32 32, i32 -516, i32 64, i32 384, i32 68, i32 -128, i32 136, i32 240, i32 248, i32 -204, i32 -68, i32 252, i32 -932, i32 -120, i32 -480, i32 -628, i32 -84, i32 192, i32 852, i32 -404, i32 -288, i32 -132, i32 204, i32 100, i32 168, i32 -68, i32 -196, i32 -868, i32 460, i32 1080, i32 380, i32 -80, i32 244, i32 0, i32 484, i32 -888, i32 64, i32 184, i32 352, i32 600, i32 460, i32 164, i32 604, i32 -196, i32 320, i32 -64, i32 588, i32 -184, i32 228, i32 12, i32 372, i32 48, i32 -848, i32 -344, i32 224, i32 208, i32 -200, i32 484, i32 128, i32 -20, i32 272, i32 -468, i32 -840, i32 384, i32 256, i32 -720, i32 -520, i32 -464, i32 -580, i32 112, i32 -120, i32 644, i32 -356, i32 -208, i32 -608, i32 -528, i32 704, i32 560, i32 -424, i32 392, i32 828, i32 40, i32 84, i32 200, i32 -152, i32 0, i32 -144, i32 584, i32 280, i32 -120, i32 80, i32 -556, i32 -972, i32 -196, i32 -472, i32 724, i32 80, i32 168, i32 -32, i32 88, i32 160, i32 -688, i32 0, i32 160, i32 356, i32 372, i32 -776, i32 740, i32 -128, i32 676, i32 -248, i32 -480, i32 4, i32 -364, i32 96, i32 544, i32 232, i32 -1032, i32 956, i32 236, i32 356, i32 20, i32 -40, i32 300, i32 24, i32 -676, i32 -596, i32 132, i32 1120, i32 -104, i32 532, i32 -1096, i32 568, i32 648, i32 444, i32 508, i32 380, i32 188, i32 -376, i32 -604, i32 1488, i32 424, i32 24, i32 756, i32 -220, i32 -192, i32 716, i32 120, i32 920, i32 688, i32 168, i32 44, i32 -460, i32 568, i32 284, i32 1144, i32 1160, i32 600, i32 424, i32 888, i32 656, i32 -356, i32 -320, i32 220, i32 316, i32 -176, i32 -724, i32 -188, i32 -816, i32 -628, i32 -348, i32 -228, i32 -380, i32 1012, i32 -452, i32 -660, i32 736, i32 928, i32 404, i32 -696, i32 -72, i32 -268, i32 -892, i32 128, i32 184, i32 -344, i32 -780, i32 360, i32 336, i32 400, i32 344, i32 428, i32 548, i32 -112, i32 136, i32 -228, i32 -216, i32 -820, i32 -516, i32 340, i32 92, i32 -136, i32 116, i32 -300, i32 376, i32 -244, i32 100, i32 -316, i32 -520, i32 -284, i32 -12, i32 824, i32 164, i32 -548, i32 -180, i32 -128, i32 116, i32 -924, i32 -828, i32 268, i32 -368, i32 -580, i32 620, i32 192, i32 160, i32 0, i32 -1676, i32 1068, i32 424, i32 -56, i32 -360, i32 468, i32 -156, i32 720, i32 288, i32 -528, i32 556, i32 -364, i32 548, i32 -148, i32 504, i32 316, i32 152, i32 -648, i32 -620, i32 -684, i32 -24, i32 -376, i32 -384, i32 -108, i32 -920, i32 -1032, i32 768, i32 180, i32 -264, i32 -508, i32 -1268, i32 -260, i32 -60, i32 300, i32 -240, i32 988, i32 724, i32 -376, i32 -576, i32 -212, i32 -736, i32 556, i32 192, i32 1092, i32 -620, i32 -880, i32 376, i32 -56, i32 -4, i32 -216, i32 -32, i32 836, i32 268, i32 396, i32 1332, i32 864, i32 -600, i32 100, i32 56, i32 -412, i32 -92, i32 356, i32 180, i32 884, i32 -468, i32 -436, i32 292, i32 -388, i32 -804, i32 -704, i32 -840, i32 368, i32 -348, i32 140, i32 -724, i32 1536, i32 940, i32 372, i32 112, i32 -372, i32 436, i32 -480, i32 1136, i32 296, i32 -32, i32 -228, i32 132, i32 -48, i32 -220, i32 868, i32 -1016, i32 -60, i32 -1044, i32 -464, i32 328, i32 916, i32 244, i32 12, i32 -736, i32 -296, i32 360, i32 468, i32 -376, i32 -108, i32 -92, i32 788, i32 368, i32 -56, i32 544, i32 400, i32 -672, i32 -420, i32 728, i32 16, i32 320, i32 44, i32 -284, i32 -380, i32 -796, i32 488, i32 132, i32 204, i32 -596, i32 -372, i32 88, i32 -152, i32 -908, i32 -636, i32 -572, i32 -624, i32 -116, i32 -692, i32 -200, i32 -56, i32 276, i32 -88, i32 484, i32 -324, i32 948, i32 864, i32 1000, i32 -456, i32 -184, i32 -276, i32 292, i32 -296, i32 156, i32 676, i32 320, i32 160, i32 908, i32 -84, i32 -1236, i32 -288, i32 -116, i32 260, i32 -372, i32 -644, i32 732, i32 -756, i32 -96, i32 84, i32 344, i32 -520, i32 348, i32 -688, i32 240, i32 -84, i32 216, i32 -1044, i32 -136, i32 -676, i32 -396, i32 -1500, i32 960, i32 -40, i32 176, i32 168, i32 1516, i32 420, i32 -504, i32 -344, i32 -364, i32 -360, i32 1216, i32 -940, i32 -380, i32 -212, i32 252, i32 -660, i32 -708, i32 484, i32 -444, i32 -152, i32 928, i32 -120, i32 1112, i32 476, i32 -260, i32 560, i32 -148, i32 -344, i32 108, i32 -196, i32 228, i32 -288, i32 504, i32 560, i32 -328, i32 -88, i32 288, i32 -1008, i32 460, i32 -228, i32 468, i32 -836, i32 -196, i32 76, i32 388, i32 232, i32 412, i32 -1168, i32 -716, i32 -644, i32 756, i32 -172, i32 -356, i32 -504, i32 116, i32 432, i32 528, i32 48, i32 476, i32 -168, i32 -608, i32 448, i32 160, i32 -532, i32 -272, i32 28, i32 -676, i32 -12, i32 828, i32 980, i32 456, i32 520, i32 104, i32 -104, i32 256, i32 -344, i32 -4, i32 -28, i32 -368, i32 -52, i32 -524, i32 -572, i32 -556, i32 -200, i32 768, i32 1124, i32 -208, i32 -512, i32 176, i32 232, i32 248, i32 -148, i32 -888, i32 604, i32 -600, i32 -304, i32 804, i32 -156, i32 -212, i32 488, i32 -192, i32 -804, i32 -256, i32 368, i32 -360, i32 -916, i32 -328, i32 228, i32 -240, i32 -448, i32 -472, i32 856, i32 -556, i32 -364, i32 572, i32 -12, i32 -156, i32 -368, i32 -340, i32 432, i32 252, i32 -752, i32 -152, i32 288, i32 268, i32 -580, i32 -848, i32 -592, i32 108, i32 -76, i32 244, i32 312, i32 -716, i32 592, i32 -80, i32 436, i32 360, i32 4, i32 -248, i32 160, i32 516, i32 584, i32 732, i32 44, i32 -468, i32 -280, i32 -292, i32 -156, i32 -588, i32 28, i32 308, i32 912, i32 24, i32 124, i32 156, i32 180, i32 -252, i32 944, i32 -924, i32 -772, i32 -520, i32 -428, i32 -624, i32 300, i32 -212, i32 -1144, i32 32, i32 -724, i32 800, i32 -1128, i32 -212, i32 -1288, i32 -848, i32 180, i32 -416, i32 440, i32 192, i32 -576, i32 -792, i32 -76, i32 -1080, i32 80, i32 -532, i32 -352, i32 -132, i32 380, i32 -820, i32 148, i32 1112, i32 128, i32 164, i32 456, i32 700, i32 -924, i32 144, i32 -668, i32 -384, i32 648, i32 -832, i32 508, i32 552, i32 -52, i32 -100, i32 -656, i32 208, i32 -568, i32 748, i32 -88, i32 680, i32 232, i32 300, i32 192, i32 -408, i32 -1012, i32 -152, i32 -252, i32 -268, i32 272, i32 -876, i32 -664, i32 -648, i32 -332, i32 -136, i32 16, i32 12, i32 1152, i32 -28, i32 332, i32 -536, i32 320, i32 -672, i32 -460, i32 -316, i32 532, i32 -260, i32 228, i32 -40, i32 1052, i32 -816, i32 180, i32 88, i32 -496, i32 -556, i32 -672, i32 -368, i32 428, i32 92, i32 356, i32 404, i32 -408, i32 252, i32 196, i32 -176, i32 -556, i32 792, i32 268, i32 32, i32 372, i32 40, i32 96, i32 -332, i32 328, i32 120, i32 372, i32 -900, i32 -40, i32 472, i32 -264, i32 -592, i32 952, i32 128, i32 656, i32 112, i32 664, i32 -232, i32 420, i32 4, i32 -344, i32 -464, i32 556, i32 244, i32 -416, i32 -32, i32 252, i32 0, i32 -412, i32 188, i32 -696, i32 508, i32 -476, i32 324, i32 -1096, i32 656, i32 -312, i32 560, i32 264, i32 -136, i32 304, i32 160, i32 -64, i32 -580, i32 248, i32 336, i32 -720, i32 560, i32 -348, i32 -288, i32 -276, i32 -196, i32 -500, i32 852, i32 -544, i32 -236, i32 -1128, i32 -992, i32 -776, i32 116, i32 56, i32 52, i32 860, i32 884, i32 212, i32 -12, i32 168, i32 1020, i32 512, i32 -552, i32 924, i32 -148, i32 716, i32 188, i32 164, i32 -340, i32 -520, i32 -184, i32 880, i32 -152, i32 -680, i32 -208, i32 -1156, i32 -300, i32 -528, i32 -472, i32 364, i32 100, i32 -744, i32 -1056, i32 -32, i32 540, i32 280, i32 144, i32 -676, i32 -32, i32 -232, i32 -280, i32 -224, i32 96, i32 568, i32 -76, i32 172, i32 148, i32 148, i32 104, i32 32, i32 -296, i32 -32, i32 788, i32 -80, i32 32, i32 -16, i32 280, i32 288, i32 944, i32 428, i32 -484], align 16
@.str.1 = private unnamed_addr constant [76 x i8] c"Grain synthesis: prediction between two chroma components is not supported!\00", align 1

; Function Attrs: nounwind
define hidden i32 @av1_add_film_grain(%struct.aom_film_grain_t* %params, %struct.aom_image* %src, %struct.aom_image* %dst) #0 {
entry:
  %retval = alloca i32, align 4
  %params.addr = alloca %struct.aom_film_grain_t*, align 4
  %src.addr = alloca %struct.aom_image*, align 4
  %dst.addr = alloca %struct.aom_image*, align 4
  %luma = alloca i8*, align 4
  %cb = alloca i8*, align 4
  %cr = alloca i8*, align 4
  %height = alloca i32, align 4
  %width = alloca i32, align 4
  %luma_stride = alloca i32, align 4
  %chroma_stride = alloca i32, align 4
  %use_high_bit_depth = alloca i32, align 4
  %chroma_subsamp_x = alloca i32, align 4
  %chroma_subsamp_y = alloca i32, align 4
  %mc_identity = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  store %struct.aom_image* %src, %struct.aom_image** %src.addr, align 4, !tbaa !2
  store %struct.aom_image* %dst, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %0 = bitcast i8** %luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i8** %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i8** %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = bitcast i32* %luma_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i32* %chroma_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = bitcast i32* %use_high_bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  store i32 0, i32* %use_high_bit_depth, align 4, !tbaa !6
  %8 = bitcast i32* %chroma_subsamp_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  store i32 0, i32* %chroma_subsamp_x, align 4, !tbaa !6
  %9 = bitcast i32* %chroma_subsamp_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  store i32 0, i32* %chroma_subsamp_y, align 4, !tbaa !6
  %10 = bitcast i32* %mc_identity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %mc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %11, i32 0, i32 3
  %12 = load i32, i32* %mc, align 4, !tbaa !8
  %cmp = icmp eq i32 %12, 0
  %13 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 0
  store i32 %cond, i32* %mc_identity, align 4, !tbaa !6
  %14 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %fmt = getelementptr inbounds %struct.aom_image, %struct.aom_image* %14, i32 0, i32 0
  %15 = load i32, i32* %fmt, align 4, !tbaa !11
  switch i32 %15, label %sw.default [
    i32 260, label %sw.bb
    i32 258, label %sw.bb
    i32 2306, label %sw.bb1
    i32 262, label %sw.bb2
    i32 2310, label %sw.bb3
    i32 261, label %sw.bb4
    i32 2309, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry, %entry
  store i32 0, i32* %use_high_bit_depth, align 4, !tbaa !6
  store i32 1, i32* %chroma_subsamp_x, align 4, !tbaa !6
  store i32 1, i32* %chroma_subsamp_y, align 4, !tbaa !6
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  store i32 1, i32* %use_high_bit_depth, align 4, !tbaa !6
  store i32 1, i32* %chroma_subsamp_x, align 4, !tbaa !6
  store i32 1, i32* %chroma_subsamp_y, align 4, !tbaa !6
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  store i32 0, i32* %use_high_bit_depth, align 4, !tbaa !6
  store i32 0, i32* %chroma_subsamp_x, align 4, !tbaa !6
  store i32 0, i32* %chroma_subsamp_y, align 4, !tbaa !6
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry
  store i32 1, i32* %use_high_bit_depth, align 4, !tbaa !6
  store i32 0, i32* %chroma_subsamp_x, align 4, !tbaa !6
  store i32 0, i32* %chroma_subsamp_y, align 4, !tbaa !6
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry
  store i32 0, i32* %use_high_bit_depth, align 4, !tbaa !6
  store i32 1, i32* %chroma_subsamp_x, align 4, !tbaa !6
  store i32 0, i32* %chroma_subsamp_y, align 4, !tbaa !6
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry
  store i32 1, i32* %use_high_bit_depth, align 4, !tbaa !6
  store i32 1, i32* %chroma_subsamp_x, align 4, !tbaa !6
  store i32 0, i32* %chroma_subsamp_y, align 4, !tbaa !6
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str, i32 0, i32 0))
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.epilog:                                        ; preds = %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %17 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %fmt6 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %17, i32 0, i32 0
  %18 = load i32, i32* %fmt6, align 4, !tbaa !11
  %19 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %fmt7 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %19, i32 0, i32 0
  store i32 %18, i32* %fmt7, align 4, !tbaa !11
  %20 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %bit_depth = getelementptr inbounds %struct.aom_image, %struct.aom_image* %20, i32 0, i32 9
  %21 = load i32, i32* %bit_depth, align 4, !tbaa !12
  %22 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %bit_depth8 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %22, i32 0, i32 9
  store i32 %21, i32* %bit_depth8, align 4, !tbaa !12
  %23 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %r_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %23, i32 0, i32 12
  %24 = load i32, i32* %r_w, align 4, !tbaa !13
  %25 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %r_w9 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %25, i32 0, i32 12
  store i32 %24, i32* %r_w9, align 4, !tbaa !13
  %26 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %r_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %26, i32 0, i32 13
  %27 = load i32, i32* %r_h, align 4, !tbaa !14
  %28 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %r_h10 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %28, i32 0, i32 13
  store i32 %27, i32* %r_h10, align 4, !tbaa !14
  %29 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %29, i32 0, i32 10
  %30 = load i32, i32* %d_w, align 4, !tbaa !15
  %31 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %d_w11 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %31, i32 0, i32 10
  store i32 %30, i32* %d_w11, align 4, !tbaa !15
  %32 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %32, i32 0, i32 11
  %33 = load i32, i32* %d_h, align 4, !tbaa !16
  %34 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %d_h12 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %34, i32 0, i32 11
  store i32 %33, i32* %d_h12, align 4, !tbaa !16
  %35 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %cp = getelementptr inbounds %struct.aom_image, %struct.aom_image* %35, i32 0, i32 1
  %36 = load i32, i32* %cp, align 4, !tbaa !17
  %37 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %cp13 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %37, i32 0, i32 1
  store i32 %36, i32* %cp13, align 4, !tbaa !17
  %38 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %tc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %38, i32 0, i32 2
  %39 = load i32, i32* %tc, align 4, !tbaa !18
  %40 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %tc14 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %40, i32 0, i32 2
  store i32 %39, i32* %tc14, align 4, !tbaa !18
  %41 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %mc15 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %41, i32 0, i32 3
  %42 = load i32, i32* %mc15, align 4, !tbaa !8
  %43 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %mc16 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %43, i32 0, i32 3
  store i32 %42, i32* %mc16, align 4, !tbaa !8
  %44 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %monochrome = getelementptr inbounds %struct.aom_image, %struct.aom_image* %44, i32 0, i32 4
  %45 = load i32, i32* %monochrome, align 4, !tbaa !19
  %46 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %monochrome17 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %46, i32 0, i32 4
  store i32 %45, i32* %monochrome17, align 4, !tbaa !19
  %47 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %csp = getelementptr inbounds %struct.aom_image, %struct.aom_image* %47, i32 0, i32 5
  %48 = load i32, i32* %csp, align 4, !tbaa !20
  %49 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %csp18 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %49, i32 0, i32 5
  store i32 %48, i32* %csp18, align 4, !tbaa !20
  %50 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %range = getelementptr inbounds %struct.aom_image, %struct.aom_image* %50, i32 0, i32 6
  %51 = load i32, i32* %range, align 4, !tbaa !21
  %52 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %range19 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %52, i32 0, i32 6
  store i32 %51, i32* %range19, align 4, !tbaa !21
  %53 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %x_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %53, i32 0, i32 14
  %54 = load i32, i32* %x_chroma_shift, align 4, !tbaa !22
  %55 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %x_chroma_shift20 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %55, i32 0, i32 14
  store i32 %54, i32* %x_chroma_shift20, align 4, !tbaa !22
  %56 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %y_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %56, i32 0, i32 15
  %57 = load i32, i32* %y_chroma_shift, align 4, !tbaa !23
  %58 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %y_chroma_shift21 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %58, i32 0, i32 15
  store i32 %57, i32* %y_chroma_shift21, align 4, !tbaa !23
  %59 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %temporal_id = getelementptr inbounds %struct.aom_image, %struct.aom_image* %59, i32 0, i32 20
  %60 = load i32, i32* %temporal_id, align 4, !tbaa !24
  %61 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %temporal_id22 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %61, i32 0, i32 20
  store i32 %60, i32* %temporal_id22, align 4, !tbaa !24
  %62 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %spatial_id = getelementptr inbounds %struct.aom_image, %struct.aom_image* %62, i32 0, i32 21
  %63 = load i32, i32* %spatial_id, align 4, !tbaa !25
  %64 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %spatial_id23 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %64, i32 0, i32 21
  store i32 %63, i32* %spatial_id23, align 4, !tbaa !25
  %65 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_w24 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %65, i32 0, i32 10
  %66 = load i32, i32* %d_w24, align 4, !tbaa !15
  %rem = urem i32 %66, 2
  %tobool = icmp ne i32 %rem, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.epilog
  %67 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_w25 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %67, i32 0, i32 10
  %68 = load i32, i32* %d_w25, align 4, !tbaa !15
  %add = add i32 %68, 1
  br label %cond.end

cond.false:                                       ; preds = %sw.epilog
  %69 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_w26 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %69, i32 0, i32 10
  %70 = load i32, i32* %d_w26, align 4, !tbaa !15
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond27 = phi i32 [ %add, %cond.true ], [ %70, %cond.false ]
  store i32 %cond27, i32* %width, align 4, !tbaa !6
  %71 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_h28 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %71, i32 0, i32 11
  %72 = load i32, i32* %d_h28, align 4, !tbaa !16
  %rem29 = urem i32 %72, 2
  %tobool30 = icmp ne i32 %rem29, 0
  br i1 %tobool30, label %cond.true31, label %cond.false34

cond.true31:                                      ; preds = %cond.end
  %73 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_h32 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %73, i32 0, i32 11
  %74 = load i32, i32* %d_h32, align 4, !tbaa !16
  %add33 = add i32 %74, 1
  br label %cond.end36

cond.false34:                                     ; preds = %cond.end
  %75 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_h35 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %75, i32 0, i32 11
  %76 = load i32, i32* %d_h35, align 4, !tbaa !16
  br label %cond.end36

cond.end36:                                       ; preds = %cond.false34, %cond.true31
  %cond37 = phi i32 [ %add33, %cond.true31 ], [ %76, %cond.false34 ]
  store i32 %cond37, i32* %height, align 4, !tbaa !6
  %77 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %planes = getelementptr inbounds %struct.aom_image, %struct.aom_image* %77, i32 0, i32 16
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes, i32 0, i32 0
  %78 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %79 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.aom_image, %struct.aom_image* %79, i32 0, i32 17
  %arrayidx38 = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 0
  %80 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %81 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %planes39 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %81, i32 0, i32 16
  %arrayidx40 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes39, i32 0, i32 0
  %82 = load i8*, i8** %arrayidx40, align 4, !tbaa !2
  %83 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %stride41 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %83, i32 0, i32 17
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* %stride41, i32 0, i32 0
  %84 = load i32, i32* %arrayidx42, align 4, !tbaa !6
  %85 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_w43 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %85, i32 0, i32 10
  %86 = load i32, i32* %d_w43, align 4, !tbaa !15
  %87 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_h44 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %87, i32 0, i32 11
  %88 = load i32, i32* %d_h44, align 4, !tbaa !16
  %89 = load i32, i32* %use_high_bit_depth, align 4, !tbaa !6
  call void @copy_rect(i8* %78, i32 %80, i8* %82, i32 %84, i32 %86, i32 %88, i32 %89)
  %90 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %planes45 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %90, i32 0, i32 16
  %arrayidx46 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes45, i32 0, i32 0
  %91 = load i8*, i8** %arrayidx46, align 4, !tbaa !2
  %92 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %stride47 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %92, i32 0, i32 17
  %arrayidx48 = getelementptr inbounds [3 x i32], [3 x i32]* %stride47, i32 0, i32 0
  %93 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %94 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_w49 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %94, i32 0, i32 10
  %95 = load i32, i32* %d_w49, align 4, !tbaa !15
  %96 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %d_h50 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %96, i32 0, i32 11
  %97 = load i32, i32* %d_h50, align 4, !tbaa !16
  %98 = load i32, i32* %use_high_bit_depth, align 4, !tbaa !6
  call void @extend_even(i8* %91, i32 %93, i32 %95, i32 %97, i32 %98)
  %99 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %monochrome51 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %99, i32 0, i32 4
  %100 = load i32, i32* %monochrome51, align 4, !tbaa !19
  %tobool52 = icmp ne i32 %100, 0
  br i1 %tobool52, label %if.end, label %if.then

if.then:                                          ; preds = %cond.end36
  %101 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %planes53 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %101, i32 0, i32 16
  %arrayidx54 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes53, i32 0, i32 1
  %102 = load i8*, i8** %arrayidx54, align 4, !tbaa !2
  %103 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %stride55 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %103, i32 0, i32 17
  %arrayidx56 = getelementptr inbounds [3 x i32], [3 x i32]* %stride55, i32 0, i32 1
  %104 = load i32, i32* %arrayidx56, align 4, !tbaa !6
  %105 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %planes57 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %105, i32 0, i32 16
  %arrayidx58 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes57, i32 0, i32 1
  %106 = load i8*, i8** %arrayidx58, align 4, !tbaa !2
  %107 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %stride59 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %107, i32 0, i32 17
  %arrayidx60 = getelementptr inbounds [3 x i32], [3 x i32]* %stride59, i32 0, i32 1
  %108 = load i32, i32* %arrayidx60, align 4, !tbaa !6
  %109 = load i32, i32* %width, align 4, !tbaa !6
  %110 = load i32, i32* %chroma_subsamp_x, align 4, !tbaa !6
  %shr = ashr i32 %109, %110
  %111 = load i32, i32* %height, align 4, !tbaa !6
  %112 = load i32, i32* %chroma_subsamp_y, align 4, !tbaa !6
  %shr61 = ashr i32 %111, %112
  %113 = load i32, i32* %use_high_bit_depth, align 4, !tbaa !6
  call void @copy_rect(i8* %102, i32 %104, i8* %106, i32 %108, i32 %shr, i32 %shr61, i32 %113)
  %114 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %planes62 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %114, i32 0, i32 16
  %arrayidx63 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes62, i32 0, i32 2
  %115 = load i8*, i8** %arrayidx63, align 4, !tbaa !2
  %116 = load %struct.aom_image*, %struct.aom_image** %src.addr, align 4, !tbaa !2
  %stride64 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %116, i32 0, i32 17
  %arrayidx65 = getelementptr inbounds [3 x i32], [3 x i32]* %stride64, i32 0, i32 2
  %117 = load i32, i32* %arrayidx65, align 4, !tbaa !6
  %118 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %planes66 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %118, i32 0, i32 16
  %arrayidx67 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes66, i32 0, i32 2
  %119 = load i8*, i8** %arrayidx67, align 4, !tbaa !2
  %120 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %stride68 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %120, i32 0, i32 17
  %arrayidx69 = getelementptr inbounds [3 x i32], [3 x i32]* %stride68, i32 0, i32 2
  %121 = load i32, i32* %arrayidx69, align 4, !tbaa !6
  %122 = load i32, i32* %width, align 4, !tbaa !6
  %123 = load i32, i32* %chroma_subsamp_x, align 4, !tbaa !6
  %shr70 = ashr i32 %122, %123
  %124 = load i32, i32* %height, align 4, !tbaa !6
  %125 = load i32, i32* %chroma_subsamp_y, align 4, !tbaa !6
  %shr71 = ashr i32 %124, %125
  %126 = load i32, i32* %use_high_bit_depth, align 4, !tbaa !6
  call void @copy_rect(i8* %115, i32 %117, i8* %119, i32 %121, i32 %shr70, i32 %shr71, i32 %126)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end36
  %127 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %planes72 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %127, i32 0, i32 16
  %arrayidx73 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes72, i32 0, i32 0
  %128 = load i8*, i8** %arrayidx73, align 4, !tbaa !2
  store i8* %128, i8** %luma, align 4, !tbaa !2
  %129 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %planes74 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %129, i32 0, i32 16
  %arrayidx75 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes74, i32 0, i32 1
  %130 = load i8*, i8** %arrayidx75, align 4, !tbaa !2
  store i8* %130, i8** %cb, align 4, !tbaa !2
  %131 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %planes76 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %131, i32 0, i32 16
  %arrayidx77 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes76, i32 0, i32 2
  %132 = load i8*, i8** %arrayidx77, align 4, !tbaa !2
  store i8* %132, i8** %cr, align 4, !tbaa !2
  %133 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %stride78 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %133, i32 0, i32 17
  %arrayidx79 = getelementptr inbounds [3 x i32], [3 x i32]* %stride78, i32 0, i32 0
  %134 = load i32, i32* %arrayidx79, align 4, !tbaa !6
  %135 = load i32, i32* %use_high_bit_depth, align 4, !tbaa !6
  %shr80 = ashr i32 %134, %135
  store i32 %shr80, i32* %luma_stride, align 4, !tbaa !6
  %136 = load %struct.aom_image*, %struct.aom_image** %dst.addr, align 4, !tbaa !2
  %stride81 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %136, i32 0, i32 17
  %arrayidx82 = getelementptr inbounds [3 x i32], [3 x i32]* %stride81, i32 0, i32 1
  %137 = load i32, i32* %arrayidx82, align 4, !tbaa !6
  %138 = load i32, i32* %use_high_bit_depth, align 4, !tbaa !6
  %shr83 = ashr i32 %137, %138
  store i32 %shr83, i32* %chroma_stride, align 4, !tbaa !6
  %139 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %140 = load i8*, i8** %luma, align 4, !tbaa !2
  %141 = load i8*, i8** %cb, align 4, !tbaa !2
  %142 = load i8*, i8** %cr, align 4, !tbaa !2
  %143 = load i32, i32* %height, align 4, !tbaa !6
  %144 = load i32, i32* %width, align 4, !tbaa !6
  %145 = load i32, i32* %luma_stride, align 4, !tbaa !6
  %146 = load i32, i32* %chroma_stride, align 4, !tbaa !6
  %147 = load i32, i32* %use_high_bit_depth, align 4, !tbaa !6
  %148 = load i32, i32* %chroma_subsamp_y, align 4, !tbaa !6
  %149 = load i32, i32* %chroma_subsamp_x, align 4, !tbaa !6
  %150 = load i32, i32* %mc_identity, align 4, !tbaa !6
  %call84 = call i32 @av1_add_film_grain_run(%struct.aom_film_grain_t* %139, i8* %140, i8* %141, i8* %142, i32 %143, i32 %144, i32 %145, i32 %146, i32 %147, i32 %148, i32 %149, i32 %150)
  store i32 %call84, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %sw.default
  %151 = bitcast i32* %mc_identity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #5
  %152 = bitcast i32* %chroma_subsamp_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #5
  %153 = bitcast i32* %chroma_subsamp_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #5
  %154 = bitcast i32* %use_high_bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #5
  %155 = bitcast i32* %chroma_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #5
  %156 = bitcast i32* %luma_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #5
  %157 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #5
  %158 = bitcast i32* %height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #5
  %159 = bitcast i8** %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #5
  %160 = bitcast i8** %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #5
  %161 = bitcast i8** %luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #5
  %162 = load i32, i32* %retval, align 4
  ret i32 %162
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: nounwind
define internal void @copy_rect(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %width, i32 %height, i32 %use_high_bit_depth) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %use_high_bit_depth.addr = alloca i32, align 4
  %hbd_coeff = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %use_high_bit_depth, i32* %use_high_bit_depth.addr, align 4, !tbaa !6
  %0 = bitcast i32* %hbd_coeff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %use_high_bit_depth.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %1, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 2, i32 1
  store i32 %cond, i32* %hbd_coeff, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i32, i32* %height.addr, align 4, !tbaa !6
  %tobool1 = icmp ne i32 %3, 0
  br i1 %tobool1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %6 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul = mul i32 %6, 1
  %7 = load i32, i32* %hbd_coeff, align 4, !tbaa !6
  %mul2 = mul i32 %mul, %7
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %4, i8* align 1 %5, i32 %mul2, i1 false)
  %8 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %8
  store i8* %add.ptr, i8** %src.addr, align 4, !tbaa !2
  %10 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %11 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr3 = getelementptr inbounds i8, i8* %11, i32 %10
  store i8* %add.ptr3, i8** %dst.addr, align 4, !tbaa !2
  %12 = load i32, i32* %height.addr, align 4, !tbaa !6
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %height.addr, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %13 = bitcast i32* %hbd_coeff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  ret void
}

; Function Attrs: nounwind
define internal void @extend_even(i8* %dst, i32 %dst_stride, i32 %width, i32 %height, i32 %use_high_bit_depth) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %use_high_bit_depth.addr = alloca i32, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %i = alloca i32, align 4
  %i27 = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %use_high_bit_depth, i32* %use_high_bit_depth.addr, align 4, !tbaa !6
  %0 = load i32, i32* %width.addr, align 4, !tbaa !6
  %and = and i32 %0, 1
  %cmp = icmp eq i32 %and, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %height.addr, align 4, !tbaa !6
  %and1 = and i32 %1, 1
  %cmp2 = icmp eq i32 %and1, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %if.end55

if.end:                                           ; preds = %land.lhs.true, %entry
  %2 = load i32, i32* %use_high_bit_depth.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %3 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %5 = bitcast i8* %4 to i16*
  store i16* %5, i16** %dst16, align 4, !tbaa !2
  %6 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %div = sdiv i32 %7, 2
  store i32 %div, i32* %dst16_stride, align 4, !tbaa !6
  %8 = load i32, i32* %width.addr, align 4, !tbaa !6
  %and4 = and i32 %8, 1
  %tobool5 = icmp ne i32 %and4, 0
  br i1 %tobool5, label %if.then6, label %if.end11

if.then6:                                         ; preds = %if.then3
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then6
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %10, %11
  br i1 %cmp7, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load i16*, i16** %dst16, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul = mul nsw i32 %14, %15
  %16 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add = add nsw i32 %mul, %16
  %sub = sub nsw i32 %add, 1
  %arrayidx = getelementptr inbounds i16, i16* %13, i32 %sub
  %17 = load i16, i16* %arrayidx, align 2, !tbaa !26
  %18 = load i16*, i16** %dst16, align 4, !tbaa !2
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %20 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul8 = mul nsw i32 %19, %20
  %21 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add9 = add nsw i32 %mul8, %21
  %arrayidx10 = getelementptr inbounds i16, i16* %18, i32 %add9
  store i16 %17, i16* %arrayidx10, align 2, !tbaa !26
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end11

if.end11:                                         ; preds = %for.end, %if.then3
  %23 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add12 = add nsw i32 %23, 1
  %and13 = and i32 %add12, -2
  store i32 %and13, i32* %width.addr, align 4, !tbaa !6
  %24 = load i32, i32* %height.addr, align 4, !tbaa !6
  %and14 = and i32 %24, 1
  %tobool15 = icmp ne i32 %and14, 0
  br i1 %tobool15, label %if.then16, label %if.end23

if.then16:                                        ; preds = %if.end11
  %25 = load i16*, i16** %dst16, align 4, !tbaa !2
  %26 = load i32, i32* %height.addr, align 4, !tbaa !6
  %27 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul17 = mul nsw i32 %26, %27
  %arrayidx18 = getelementptr inbounds i16, i16* %25, i32 %mul17
  %28 = bitcast i16* %arrayidx18 to i8*
  %29 = load i16*, i16** %dst16, align 4, !tbaa !2
  %30 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub19 = sub nsw i32 %30, 1
  %31 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul20 = mul nsw i32 %sub19, %31
  %arrayidx21 = getelementptr inbounds i16, i16* %29, i32 %mul20
  %32 = bitcast i16* %arrayidx21 to i8*
  %33 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul22 = mul i32 2, %33
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %28, i8* align 2 %32, i32 %mul22, i1 false)
  br label %if.end23

if.end23:                                         ; preds = %if.then16, %if.end11
  %34 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  %35 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  br label %if.end55

if.else:                                          ; preds = %if.end
  %36 = load i32, i32* %width.addr, align 4, !tbaa !6
  %and24 = and i32 %36, 1
  %tobool25 = icmp ne i32 %and24, 0
  br i1 %tobool25, label %if.then26, label %if.end42

if.then26:                                        ; preds = %if.else
  %37 = bitcast i32* %i27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #5
  store i32 0, i32* %i27, align 4, !tbaa !6
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc39, %if.then26
  %38 = load i32, i32* %i27, align 4, !tbaa !6
  %39 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp29 = icmp slt i32 %38, %39
  br i1 %cmp29, label %for.body31, label %for.cond.cleanup30

for.cond.cleanup30:                               ; preds = %for.cond28
  %40 = bitcast i32* %i27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #5
  br label %for.end41

for.body31:                                       ; preds = %for.cond28
  %41 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %42 = load i32, i32* %i27, align 4, !tbaa !6
  %43 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul32 = mul nsw i32 %42, %43
  %44 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add33 = add nsw i32 %mul32, %44
  %sub34 = sub nsw i32 %add33, 1
  %arrayidx35 = getelementptr inbounds i8, i8* %41, i32 %sub34
  %45 = load i8, i8* %arrayidx35, align 1, !tbaa !28
  %46 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %47 = load i32, i32* %i27, align 4, !tbaa !6
  %48 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul36 = mul nsw i32 %47, %48
  %49 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add37 = add nsw i32 %mul36, %49
  %arrayidx38 = getelementptr inbounds i8, i8* %46, i32 %add37
  store i8 %45, i8* %arrayidx38, align 1, !tbaa !28
  br label %for.inc39

for.inc39:                                        ; preds = %for.body31
  %50 = load i32, i32* %i27, align 4, !tbaa !6
  %inc40 = add nsw i32 %50, 1
  store i32 %inc40, i32* %i27, align 4, !tbaa !6
  br label %for.cond28

for.end41:                                        ; preds = %for.cond.cleanup30
  br label %if.end42

if.end42:                                         ; preds = %for.end41, %if.else
  %51 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add43 = add nsw i32 %51, 1
  %and44 = and i32 %add43, -2
  store i32 %and44, i32* %width.addr, align 4, !tbaa !6
  %52 = load i32, i32* %height.addr, align 4, !tbaa !6
  %and45 = and i32 %52, 1
  %tobool46 = icmp ne i32 %and45, 0
  br i1 %tobool46, label %if.then47, label %if.end54

if.then47:                                        ; preds = %if.end42
  %53 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %54 = load i32, i32* %height.addr, align 4, !tbaa !6
  %55 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul48 = mul nsw i32 %54, %55
  %arrayidx49 = getelementptr inbounds i8, i8* %53, i32 %mul48
  %56 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %57 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub50 = sub nsw i32 %57, 1
  %58 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul51 = mul nsw i32 %sub50, %58
  %arrayidx52 = getelementptr inbounds i8, i8* %56, i32 %mul51
  %59 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul53 = mul i32 1, %59
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arrayidx49, i8* align 1 %arrayidx52, i32 %mul53, i1 false)
  br label %if.end54

if.end54:                                         ; preds = %if.then47, %if.end42
  br label %if.end55

if.end55:                                         ; preds = %if.then, %if.end54, %if.end23
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_add_film_grain_run(%struct.aom_film_grain_t* %params, i8* %luma, i8* %cb, i8* %cr, i32 %height, i32 %width, i32 %luma_stride, i32 %chroma_stride, i32 %use_high_bit_depth, i32 %chroma_subsamp_y, i32 %chroma_subsamp_x, i32 %mc_identity) #0 {
entry:
  %retval = alloca i32, align 4
  %params.addr = alloca %struct.aom_film_grain_t*, align 4
  %luma.addr = alloca i8*, align 4
  %cb.addr = alloca i8*, align 4
  %cr.addr = alloca i8*, align 4
  %height.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %luma_stride.addr = alloca i32, align 4
  %chroma_stride.addr = alloca i32, align 4
  %use_high_bit_depth.addr = alloca i32, align 4
  %chroma_subsamp_y.addr = alloca i32, align 4
  %chroma_subsamp_x.addr = alloca i32, align 4
  %mc_identity.addr = alloca i32, align 4
  %pred_pos_luma = alloca i32**, align 4
  %pred_pos_chroma = alloca i32**, align 4
  %luma_grain_block = alloca i32*, align 4
  %cb_grain_block = alloca i32*, align 4
  %cr_grain_block = alloca i32*, align 4
  %y_line_buf = alloca i32*, align 4
  %cb_line_buf = alloca i32*, align 4
  %cr_line_buf = alloca i32*, align 4
  %y_col_buf = alloca i32*, align 4
  %cb_col_buf = alloca i32*, align 4
  %cr_col_buf = alloca i32*, align 4
  %left_pad = alloca i32, align 4
  %right_pad = alloca i32, align 4
  %top_pad = alloca i32, align 4
  %bottom_pad = alloca i32, align 4
  %ar_padding = alloca i32, align 4
  %luma_block_size_y = alloca i32, align 4
  %luma_block_size_x = alloca i32, align 4
  %chroma_block_size_y = alloca i32, align 4
  %chroma_block_size_x = alloca i32, align 4
  %luma_grain_stride = alloca i32, align 4
  %chroma_grain_stride = alloca i32, align 4
  %overlap = alloca i32, align 4
  %bit_depth = alloca i32, align 4
  %grain_center = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %y = alloca i32, align 4
  %x = alloca i32, align 4
  %offset_y = alloca i32, align 4
  %offset_x = alloca i32, align 4
  %luma_offset_y = alloca i32, align 4
  %luma_offset_x = alloca i32, align 4
  %chroma_offset_y = alloca i32, align 4
  %chroma_offset_x = alloca i32, align 4
  %i = alloca i32, align 4
  %i507 = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  store i8* %luma, i8** %luma.addr, align 4, !tbaa !2
  store i8* %cb, i8** %cb.addr, align 4, !tbaa !2
  store i8* %cr, i8** %cr.addr, align 4, !tbaa !2
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %luma_stride, i32* %luma_stride.addr, align 4, !tbaa !6
  store i32 %chroma_stride, i32* %chroma_stride.addr, align 4, !tbaa !6
  store i32 %use_high_bit_depth, i32* %use_high_bit_depth.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_y, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_x, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  store i32 %mc_identity, i32* %mc_identity.addr, align 4, !tbaa !6
  %0 = bitcast i32*** %pred_pos_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32*** %pred_pos_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32** %luma_grain_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32** %cb_grain_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32** %cr_grain_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = bitcast i32** %y_line_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i32** %cb_line_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = bitcast i32** %cr_line_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = bitcast i32** %y_col_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = bitcast i32** %cb_col_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = bitcast i32** %cr_col_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %random_seed = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %11, i32 0, i32 25
  %12 = load i16, i16* %random_seed, align 4, !tbaa !29
  store i16 %12, i16* @random_register, align 2, !tbaa !26
  %13 = bitcast i32* %left_pad to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  store i32 3, i32* %left_pad, align 4, !tbaa !6
  %14 = bitcast i32* %right_pad to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  store i32 3, i32* %right_pad, align 4, !tbaa !6
  %15 = bitcast i32* %top_pad to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  store i32 3, i32* %top_pad, align 4, !tbaa !6
  %16 = bitcast i32* %bottom_pad to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  store i32 0, i32* %bottom_pad, align 4, !tbaa !6
  %17 = bitcast i32* %ar_padding to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  store i32 3, i32* %ar_padding, align 4, !tbaa !6
  store i32 32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  store i32 32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %18 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %19 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr = ashr i32 %18, %19
  store i32 %shr, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %20 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %21 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr1 = ashr i32 %20, %21
  store i32 %shr1, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %22 = bitcast i32* %luma_block_size_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = load i32, i32* %top_pad, align 4, !tbaa !6
  %24 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul = mul nsw i32 2, %24
  %add = add nsw i32 %23, %mul
  %25 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %mul2 = mul nsw i32 %25, 2
  %add3 = add nsw i32 %add, %mul2
  %26 = load i32, i32* %bottom_pad, align 4, !tbaa !6
  %add4 = add nsw i32 %add3, %26
  store i32 %add4, i32* %luma_block_size_y, align 4, !tbaa !6
  %27 = bitcast i32* %luma_block_size_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  %28 = load i32, i32* %left_pad, align 4, !tbaa !6
  %29 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul5 = mul nsw i32 2, %29
  %add6 = add nsw i32 %28, %mul5
  %30 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %mul7 = mul nsw i32 %30, 2
  %add8 = add nsw i32 %add6, %mul7
  %31 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul9 = mul nsw i32 2, %31
  %add10 = add nsw i32 %add8, %mul9
  %32 = load i32, i32* %right_pad, align 4, !tbaa !6
  %add11 = add nsw i32 %add10, %32
  store i32 %add11, i32* %luma_block_size_x, align 4, !tbaa !6
  %33 = bitcast i32* %chroma_block_size_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #5
  %34 = load i32, i32* %top_pad, align 4, !tbaa !6
  %35 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr12 = ashr i32 2, %35
  %36 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul13 = mul nsw i32 %shr12, %36
  %add14 = add nsw i32 %34, %mul13
  %37 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %mul15 = mul nsw i32 %37, 2
  %add16 = add nsw i32 %add14, %mul15
  %38 = load i32, i32* %bottom_pad, align 4, !tbaa !6
  %add17 = add nsw i32 %add16, %38
  store i32 %add17, i32* %chroma_block_size_y, align 4, !tbaa !6
  %39 = bitcast i32* %chroma_block_size_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #5
  %40 = load i32, i32* %left_pad, align 4, !tbaa !6
  %41 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr18 = ashr i32 2, %41
  %42 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul19 = mul nsw i32 %shr18, %42
  %add20 = add nsw i32 %40, %mul19
  %43 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %mul21 = mul nsw i32 %43, 2
  %add22 = add nsw i32 %add20, %mul21
  %44 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr23 = ashr i32 2, %44
  %45 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul24 = mul nsw i32 %shr23, %45
  %add25 = add nsw i32 %add22, %mul24
  %46 = load i32, i32* %right_pad, align 4, !tbaa !6
  %add26 = add nsw i32 %add25, %46
  store i32 %add26, i32* %chroma_block_size_x, align 4, !tbaa !6
  %47 = bitcast i32* %luma_grain_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  %48 = load i32, i32* %luma_block_size_x, align 4, !tbaa !6
  store i32 %48, i32* %luma_grain_stride, align 4, !tbaa !6
  %49 = bitcast i32* %chroma_grain_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #5
  %50 = load i32, i32* %chroma_block_size_x, align 4, !tbaa !6
  store i32 %50, i32* %chroma_grain_stride, align 4, !tbaa !6
  %51 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #5
  %52 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %overlap_flag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %52, i32 0, i32 20
  %53 = load i32, i32* %overlap_flag, align 4, !tbaa !31
  store i32 %53, i32* %overlap, align 4, !tbaa !6
  %54 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #5
  %55 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %bit_depth27 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %55, i32 0, i32 22
  %56 = load i32, i32* %bit_depth27, align 4, !tbaa !32
  store i32 %56, i32* %bit_depth, align 4, !tbaa !6
  %57 = bitcast i32* %grain_center to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #5
  %58 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %sub = sub nsw i32 %58, 8
  %shl = shl i32 128, %sub
  store i32 %shl, i32* %grain_center, align 4, !tbaa !6
  %59 = load i32, i32* %grain_center, align 4, !tbaa !6
  %sub28 = sub nsw i32 0, %59
  store i32 %sub28, i32* @grain_min, align 4, !tbaa !6
  %60 = load i32, i32* %grain_center, align 4, !tbaa !6
  %sub29 = sub nsw i32 %60, 1
  store i32 %sub29, i32* @grain_max, align 4, !tbaa !6
  %61 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %62 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %63 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %64 = load i32, i32* %luma_block_size_y, align 4, !tbaa !6
  %65 = load i32, i32* %luma_block_size_x, align 4, !tbaa !6
  %mul30 = mul nsw i32 %64, %65
  %66 = load i32, i32* %chroma_block_size_y, align 4, !tbaa !6
  %67 = load i32, i32* %chroma_block_size_x, align 4, !tbaa !6
  %mul31 = mul nsw i32 %66, %67
  %68 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %69 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  call void @init_arrays(%struct.aom_film_grain_t* %61, i32 %62, i32 %63, i32*** %pred_pos_luma, i32*** %pred_pos_chroma, i32** %luma_grain_block, i32** %cb_grain_block, i32** %cr_grain_block, i32** %y_line_buf, i32** %cb_line_buf, i32** %cr_line_buf, i32** %y_col_buf, i32** %cb_col_buf, i32** %cr_col_buf, i32 %mul30, i32 %mul31, i32 %68, i32 %69)
  %70 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %71 = load i32**, i32*** %pred_pos_luma, align 4, !tbaa !2
  %72 = load i32*, i32** %luma_grain_block, align 4, !tbaa !2
  %73 = load i32, i32* %luma_block_size_y, align 4, !tbaa !6
  %74 = load i32, i32* %luma_block_size_x, align 4, !tbaa !6
  %75 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %76 = load i32, i32* %left_pad, align 4, !tbaa !6
  %77 = load i32, i32* %top_pad, align 4, !tbaa !6
  %78 = load i32, i32* %right_pad, align 4, !tbaa !6
  %79 = load i32, i32* %bottom_pad, align 4, !tbaa !6
  %call = call i32 @generate_luma_grain_block(%struct.aom_film_grain_t* %70, i32** %71, i32* %72, i32 %73, i32 %74, i32 %75, i32 %76, i32 %77, i32 %78, i32 %79)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %80 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %81 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %82 = load i32*, i32** %luma_grain_block, align 4, !tbaa !2
  %83 = load i32*, i32** %cb_grain_block, align 4, !tbaa !2
  %84 = load i32*, i32** %cr_grain_block, align 4, !tbaa !2
  %85 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %86 = load i32, i32* %chroma_block_size_y, align 4, !tbaa !6
  %87 = load i32, i32* %chroma_block_size_x, align 4, !tbaa !6
  %88 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %89 = load i32, i32* %left_pad, align 4, !tbaa !6
  %90 = load i32, i32* %top_pad, align 4, !tbaa !6
  %91 = load i32, i32* %right_pad, align 4, !tbaa !6
  %92 = load i32, i32* %bottom_pad, align 4, !tbaa !6
  %93 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %94 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %call32 = call i32 @generate_chroma_grain_blocks(%struct.aom_film_grain_t* %80, i32** %81, i32* %82, i32* %83, i32* %84, i32 %85, i32 %86, i32 %87, i32 %88, i32 %89, i32 %90, i32 %91, i32 %92, i32 %93, i32 %94)
  %tobool33 = icmp ne i32 %call32, 0
  br i1 %tobool33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end35:                                         ; preds = %if.end
  %95 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_points_y = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %95, i32 0, i32 2
  %arraydecay = getelementptr inbounds [14 x [2 x i32]], [14 x [2 x i32]]* %scaling_points_y, i32 0, i32 0
  %96 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %96, i32 0, i32 3
  %97 = load i32, i32* %num_y_points, align 4, !tbaa !33
  call void @init_scaling_function([2 x i32]* %arraydecay, i32 %97, i32* getelementptr inbounds ([256 x i32], [256 x i32]* @scaling_lut_y, i32 0, i32 0))
  %98 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %98, i32 0, i32 23
  %99 = load i32, i32* %chroma_scaling_from_luma, align 4, !tbaa !34
  %tobool36 = icmp ne i32 %99, 0
  br i1 %tobool36, label %if.then37, label %if.else

if.then37:                                        ; preds = %if.end35
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 bitcast ([256 x i32]* @scaling_lut_cb to i8*), i8* align 16 bitcast ([256 x i32]* @scaling_lut_y to i8*), i32 1024, i1 false)
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 bitcast ([256 x i32]* @scaling_lut_cr to i8*), i8* align 16 bitcast ([256 x i32]* @scaling_lut_y to i8*), i32 1024, i1 false)
  br label %if.end40

if.else:                                          ; preds = %if.end35
  %100 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_points_cb = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %100, i32 0, i32 4
  %arraydecay38 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cb, i32 0, i32 0
  %101 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cb_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %101, i32 0, i32 5
  %102 = load i32, i32* %num_cb_points, align 4, !tbaa !35
  call void @init_scaling_function([2 x i32]* %arraydecay38, i32 %102, i32* getelementptr inbounds ([256 x i32], [256 x i32]* @scaling_lut_cb, i32 0, i32 0))
  %103 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_points_cr = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %103, i32 0, i32 6
  %arraydecay39 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cr, i32 0, i32 0
  %104 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cr_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %104, i32 0, i32 7
  %105 = load i32, i32* %num_cr_points, align 4, !tbaa !36
  call void @init_scaling_function([2 x i32]* %arraydecay39, i32 %105, i32* getelementptr inbounds ([256 x i32], [256 x i32]* @scaling_lut_cr, i32 0, i32 0))
  br label %if.end40

if.end40:                                         ; preds = %if.else, %if.then37
  %106 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #5
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc860, %if.end40
  %107 = load i32, i32* %y, align 4, !tbaa !6
  %108 = load i32, i32* %height.addr, align 4, !tbaa !6
  %div = sdiv i32 %108, 2
  %cmp = icmp slt i32 %107, %div
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %109 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #5
  br label %for.end863

for.body:                                         ; preds = %for.cond
  %110 = load i32, i32* %y, align 4, !tbaa !6
  %mul41 = mul nsw i32 %110, 2
  %111 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %random_seed42 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %111, i32 0, i32 25
  %112 = load i16, i16* %random_seed42, align 4, !tbaa !29
  call void @init_random_generator(i32 %mul41, i16 zeroext %112)
  %113 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #5
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc, %for.body
  %114 = load i32, i32* %x, align 4, !tbaa !6
  %115 = load i32, i32* %width.addr, align 4, !tbaa !6
  %div44 = sdiv i32 %115, 2
  %cmp45 = icmp slt i32 %114, %div44
  br i1 %cmp45, label %for.body47, label %for.cond.cleanup46

for.cond.cleanup46:                               ; preds = %for.cond43
  store i32 5, i32* %cleanup.dest.slot, align 4
  %116 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #5
  br label %for.end

for.body47:                                       ; preds = %for.cond43
  %117 = bitcast i32* %offset_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #5
  %call48 = call i32 @get_random_number(i32 8)
  store i32 %call48, i32* %offset_y, align 4, !tbaa !6
  %118 = bitcast i32* %offset_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #5
  %119 = load i32, i32* %offset_y, align 4, !tbaa !6
  %shr49 = ashr i32 %119, 4
  %and = and i32 %shr49, 15
  store i32 %and, i32* %offset_x, align 4, !tbaa !6
  %120 = load i32, i32* %offset_y, align 4, !tbaa !6
  %and50 = and i32 %120, 15
  store i32 %and50, i32* %offset_y, align 4, !tbaa !6
  %121 = bitcast i32* %luma_offset_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #5
  %122 = load i32, i32* %left_pad, align 4, !tbaa !6
  %123 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul51 = mul nsw i32 2, %123
  %add52 = add nsw i32 %122, %mul51
  %124 = load i32, i32* %offset_y, align 4, !tbaa !6
  %shl53 = shl i32 %124, 1
  %add54 = add nsw i32 %add52, %shl53
  store i32 %add54, i32* %luma_offset_y, align 4, !tbaa !6
  %125 = bitcast i32* %luma_offset_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #5
  %126 = load i32, i32* %top_pad, align 4, !tbaa !6
  %127 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul55 = mul nsw i32 2, %127
  %add56 = add nsw i32 %126, %mul55
  %128 = load i32, i32* %offset_x, align 4, !tbaa !6
  %shl57 = shl i32 %128, 1
  %add58 = add nsw i32 %add56, %shl57
  store i32 %add58, i32* %luma_offset_x, align 4, !tbaa !6
  %129 = bitcast i32* %chroma_offset_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #5
  %130 = load i32, i32* %top_pad, align 4, !tbaa !6
  %131 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr59 = ashr i32 2, %131
  %132 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul60 = mul nsw i32 %shr59, %132
  %add61 = add nsw i32 %130, %mul60
  %133 = load i32, i32* %offset_y, align 4, !tbaa !6
  %134 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr62 = ashr i32 2, %134
  %mul63 = mul nsw i32 %133, %shr62
  %add64 = add nsw i32 %add61, %mul63
  store i32 %add64, i32* %chroma_offset_y, align 4, !tbaa !6
  %135 = bitcast i32* %chroma_offset_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #5
  %136 = load i32, i32* %left_pad, align 4, !tbaa !6
  %137 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr65 = ashr i32 2, %137
  %138 = load i32, i32* %ar_padding, align 4, !tbaa !6
  %mul66 = mul nsw i32 %shr65, %138
  %add67 = add nsw i32 %136, %mul66
  %139 = load i32, i32* %offset_x, align 4, !tbaa !6
  %140 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr68 = ashr i32 2, %140
  %mul69 = mul nsw i32 %139, %shr68
  %add70 = add nsw i32 %add67, %mul69
  store i32 %add70, i32* %chroma_offset_x, align 4, !tbaa !6
  %141 = load i32, i32* %overlap, align 4, !tbaa !6
  %tobool71 = icmp ne i32 %141, 0
  br i1 %tobool71, label %land.lhs.true, label %if.end225

land.lhs.true:                                    ; preds = %for.body47
  %142 = load i32, i32* %x, align 4, !tbaa !6
  %tobool72 = icmp ne i32 %142, 0
  br i1 %tobool72, label %if.then73, label %if.end225

if.then73:                                        ; preds = %land.lhs.true
  %143 = load i32*, i32** %y_col_buf, align 4, !tbaa !2
  %144 = load i32*, i32** %luma_grain_block, align 4, !tbaa !2
  %145 = load i32, i32* %luma_offset_y, align 4, !tbaa !6
  %146 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %mul74 = mul nsw i32 %145, %146
  %add.ptr = getelementptr inbounds i32, i32* %144, i32 %mul74
  %147 = load i32, i32* %luma_offset_x, align 4, !tbaa !6
  %add.ptr75 = getelementptr inbounds i32, i32* %add.ptr, i32 %147
  %148 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %149 = load i32*, i32** %y_col_buf, align 4, !tbaa !2
  %150 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %add76 = add nsw i32 %150, 2
  %151 = load i32, i32* %height.addr, align 4, !tbaa !6
  %152 = load i32, i32* %y, align 4, !tbaa !6
  %shl77 = shl i32 %152, 1
  %sub78 = sub nsw i32 %151, %shl77
  %cmp79 = icmp slt i32 %add76, %sub78
  br i1 %cmp79, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then73
  %153 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %add80 = add nsw i32 %153, 2
  br label %cond.end

cond.false:                                       ; preds = %if.then73
  %154 = load i32, i32* %height.addr, align 4, !tbaa !6
  %155 = load i32, i32* %y, align 4, !tbaa !6
  %shl81 = shl i32 %155, 1
  %sub82 = sub nsw i32 %154, %shl81
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add80, %cond.true ], [ %sub82, %cond.false ]
  call void @ver_boundary_overlap(i32* %143, i32 2, i32* %add.ptr75, i32 %148, i32* %149, i32 2, i32 2, i32 %cond)
  %156 = load i32*, i32** %cb_col_buf, align 4, !tbaa !2
  %157 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr83 = ashr i32 2, %157
  %158 = load i32*, i32** %cb_grain_block, align 4, !tbaa !2
  %159 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %160 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul84 = mul nsw i32 %159, %160
  %add.ptr85 = getelementptr inbounds i32, i32* %158, i32 %mul84
  %161 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr86 = getelementptr inbounds i32, i32* %add.ptr85, i32 %161
  %162 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %163 = load i32*, i32** %cb_col_buf, align 4, !tbaa !2
  %164 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr87 = ashr i32 2, %164
  %165 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr88 = ashr i32 2, %165
  %166 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %167 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr89 = ashr i32 2, %167
  %add90 = add nsw i32 %166, %shr89
  %168 = load i32, i32* %height.addr, align 4, !tbaa !6
  %169 = load i32, i32* %y, align 4, !tbaa !6
  %shl91 = shl i32 %169, 1
  %sub92 = sub nsw i32 %168, %shl91
  %170 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr93 = ashr i32 %sub92, %170
  %cmp94 = icmp slt i32 %add90, %shr93
  br i1 %cmp94, label %cond.true95, label %cond.false98

cond.true95:                                      ; preds = %cond.end
  %171 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %172 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr96 = ashr i32 2, %172
  %add97 = add nsw i32 %171, %shr96
  br label %cond.end102

cond.false98:                                     ; preds = %cond.end
  %173 = load i32, i32* %height.addr, align 4, !tbaa !6
  %174 = load i32, i32* %y, align 4, !tbaa !6
  %shl99 = shl i32 %174, 1
  %sub100 = sub nsw i32 %173, %shl99
  %175 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr101 = ashr i32 %sub100, %175
  br label %cond.end102

cond.end102:                                      ; preds = %cond.false98, %cond.true95
  %cond103 = phi i32 [ %add97, %cond.true95 ], [ %shr101, %cond.false98 ]
  call void @ver_boundary_overlap(i32* %156, i32 %shr83, i32* %add.ptr86, i32 %162, i32* %163, i32 %shr87, i32 %shr88, i32 %cond103)
  %176 = load i32*, i32** %cr_col_buf, align 4, !tbaa !2
  %177 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr104 = ashr i32 2, %177
  %178 = load i32*, i32** %cr_grain_block, align 4, !tbaa !2
  %179 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %180 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul105 = mul nsw i32 %179, %180
  %add.ptr106 = getelementptr inbounds i32, i32* %178, i32 %mul105
  %181 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr107 = getelementptr inbounds i32, i32* %add.ptr106, i32 %181
  %182 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %183 = load i32*, i32** %cr_col_buf, align 4, !tbaa !2
  %184 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr108 = ashr i32 2, %184
  %185 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr109 = ashr i32 2, %185
  %186 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %187 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr110 = ashr i32 2, %187
  %add111 = add nsw i32 %186, %shr110
  %188 = load i32, i32* %height.addr, align 4, !tbaa !6
  %189 = load i32, i32* %y, align 4, !tbaa !6
  %shl112 = shl i32 %189, 1
  %sub113 = sub nsw i32 %188, %shl112
  %190 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr114 = ashr i32 %sub113, %190
  %cmp115 = icmp slt i32 %add111, %shr114
  br i1 %cmp115, label %cond.true116, label %cond.false119

cond.true116:                                     ; preds = %cond.end102
  %191 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %192 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr117 = ashr i32 2, %192
  %add118 = add nsw i32 %191, %shr117
  br label %cond.end123

cond.false119:                                    ; preds = %cond.end102
  %193 = load i32, i32* %height.addr, align 4, !tbaa !6
  %194 = load i32, i32* %y, align 4, !tbaa !6
  %shl120 = shl i32 %194, 1
  %sub121 = sub nsw i32 %193, %shl120
  %195 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr122 = ashr i32 %sub121, %195
  br label %cond.end123

cond.end123:                                      ; preds = %cond.false119, %cond.true116
  %cond124 = phi i32 [ %add118, %cond.true116 ], [ %shr122, %cond.false119 ]
  call void @ver_boundary_overlap(i32* %176, i32 %shr104, i32* %add.ptr107, i32 %182, i32* %183, i32 %shr108, i32 %shr109, i32 %cond124)
  %196 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %196) #5
  %197 = load i32, i32* %y, align 4, !tbaa !6
  %tobool125 = icmp ne i32 %197, 0
  %198 = zext i1 %tobool125 to i64
  %cond126 = select i1 %tobool125, i32 1, i32 0
  store i32 %cond126, i32* %i, align 4, !tbaa !6
  %199 = load i32, i32* %use_high_bit_depth.addr, align 4, !tbaa !6
  %tobool127 = icmp ne i32 %199, 0
  br i1 %tobool127, label %if.then128, label %if.else176

if.then128:                                       ; preds = %cond.end123
  %200 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %201 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %202 = bitcast i8* %201 to i16*
  %203 = load i32, i32* %y, align 4, !tbaa !6
  %204 = load i32, i32* %i, align 4, !tbaa !6
  %add129 = add nsw i32 %203, %204
  %shl130 = shl i32 %add129, 1
  %205 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul131 = mul nsw i32 %shl130, %205
  %add.ptr132 = getelementptr inbounds i16, i16* %202, i32 %mul131
  %206 = load i32, i32* %x, align 4, !tbaa !6
  %shl133 = shl i32 %206, 1
  %add.ptr134 = getelementptr inbounds i16, i16* %add.ptr132, i32 %shl133
  %207 = load i8*, i8** %cb.addr, align 4, !tbaa !2
  %208 = bitcast i8* %207 to i16*
  %209 = load i32, i32* %y, align 4, !tbaa !6
  %210 = load i32, i32* %i, align 4, !tbaa !6
  %add135 = add nsw i32 %209, %210
  %211 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub136 = sub nsw i32 1, %211
  %shl137 = shl i32 %add135, %sub136
  %212 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul138 = mul nsw i32 %shl137, %212
  %add.ptr139 = getelementptr inbounds i16, i16* %208, i32 %mul138
  %213 = load i32, i32* %x, align 4, !tbaa !6
  %214 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub140 = sub nsw i32 1, %214
  %shl141 = shl i32 %213, %sub140
  %add.ptr142 = getelementptr inbounds i16, i16* %add.ptr139, i32 %shl141
  %215 = load i8*, i8** %cr.addr, align 4, !tbaa !2
  %216 = bitcast i8* %215 to i16*
  %217 = load i32, i32* %y, align 4, !tbaa !6
  %218 = load i32, i32* %i, align 4, !tbaa !6
  %add143 = add nsw i32 %217, %218
  %219 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub144 = sub nsw i32 1, %219
  %shl145 = shl i32 %add143, %sub144
  %220 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul146 = mul nsw i32 %shl145, %220
  %add.ptr147 = getelementptr inbounds i16, i16* %216, i32 %mul146
  %221 = load i32, i32* %x, align 4, !tbaa !6
  %222 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub148 = sub nsw i32 1, %222
  %shl149 = shl i32 %221, %sub148
  %add.ptr150 = getelementptr inbounds i16, i16* %add.ptr147, i32 %shl149
  %223 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %224 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %225 = load i32*, i32** %y_col_buf, align 4, !tbaa !2
  %226 = load i32, i32* %i, align 4, !tbaa !6
  %mul151 = mul nsw i32 %226, 4
  %add.ptr152 = getelementptr inbounds i32, i32* %225, i32 %mul151
  %227 = load i32*, i32** %cb_col_buf, align 4, !tbaa !2
  %228 = load i32, i32* %i, align 4, !tbaa !6
  %229 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub153 = sub nsw i32 2, %229
  %mul154 = mul nsw i32 %228, %sub153
  %230 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub155 = sub nsw i32 2, %230
  %mul156 = mul nsw i32 %mul154, %sub155
  %add.ptr157 = getelementptr inbounds i32, i32* %227, i32 %mul156
  %231 = load i32*, i32** %cr_col_buf, align 4, !tbaa !2
  %232 = load i32, i32* %i, align 4, !tbaa !6
  %233 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub158 = sub nsw i32 2, %233
  %mul159 = mul nsw i32 %232, %sub158
  %234 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub160 = sub nsw i32 2, %234
  %mul161 = mul nsw i32 %mul159, %sub160
  %add.ptr162 = getelementptr inbounds i32, i32* %231, i32 %mul161
  %235 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub163 = sub nsw i32 2, %235
  %236 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shr164 = ashr i32 %236, 1
  %237 = load i32, i32* %height.addr, align 4, !tbaa !6
  %div165 = sdiv i32 %237, 2
  %238 = load i32, i32* %y, align 4, !tbaa !6
  %sub166 = sub nsw i32 %div165, %238
  %cmp167 = icmp slt i32 %shr164, %sub166
  br i1 %cmp167, label %cond.true168, label %cond.false170

cond.true168:                                     ; preds = %if.then128
  %239 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shr169 = ashr i32 %239, 1
  br label %cond.end173

cond.false170:                                    ; preds = %if.then128
  %240 = load i32, i32* %height.addr, align 4, !tbaa !6
  %div171 = sdiv i32 %240, 2
  %241 = load i32, i32* %y, align 4, !tbaa !6
  %sub172 = sub nsw i32 %div171, %241
  br label %cond.end173

cond.end173:                                      ; preds = %cond.false170, %cond.true168
  %cond174 = phi i32 [ %shr169, %cond.true168 ], [ %sub172, %cond.false170 ]
  %242 = load i32, i32* %i, align 4, !tbaa !6
  %sub175 = sub nsw i32 %cond174, %242
  %243 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %244 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %245 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %246 = load i32, i32* %mc_identity.addr, align 4, !tbaa !6
  call void @add_noise_to_block_hbd(%struct.aom_film_grain_t* %200, i16* %add.ptr134, i16* %add.ptr142, i16* %add.ptr150, i32 %223, i32 %224, i32* %add.ptr152, i32* %add.ptr157, i32* %add.ptr162, i32 2, i32 %sub163, i32 %sub175, i32 1, i32 %243, i32 %244, i32 %245, i32 %246)
  br label %if.end224

if.else176:                                       ; preds = %cond.end123
  %247 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %248 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %249 = load i32, i32* %y, align 4, !tbaa !6
  %250 = load i32, i32* %i, align 4, !tbaa !6
  %add177 = add nsw i32 %249, %250
  %shl178 = shl i32 %add177, 1
  %251 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul179 = mul nsw i32 %shl178, %251
  %add.ptr180 = getelementptr inbounds i8, i8* %248, i32 %mul179
  %252 = load i32, i32* %x, align 4, !tbaa !6
  %shl181 = shl i32 %252, 1
  %add.ptr182 = getelementptr inbounds i8, i8* %add.ptr180, i32 %shl181
  %253 = load i8*, i8** %cb.addr, align 4, !tbaa !2
  %254 = load i32, i32* %y, align 4, !tbaa !6
  %255 = load i32, i32* %i, align 4, !tbaa !6
  %add183 = add nsw i32 %254, %255
  %256 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub184 = sub nsw i32 1, %256
  %shl185 = shl i32 %add183, %sub184
  %257 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul186 = mul nsw i32 %shl185, %257
  %add.ptr187 = getelementptr inbounds i8, i8* %253, i32 %mul186
  %258 = load i32, i32* %x, align 4, !tbaa !6
  %259 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub188 = sub nsw i32 1, %259
  %shl189 = shl i32 %258, %sub188
  %add.ptr190 = getelementptr inbounds i8, i8* %add.ptr187, i32 %shl189
  %260 = load i8*, i8** %cr.addr, align 4, !tbaa !2
  %261 = load i32, i32* %y, align 4, !tbaa !6
  %262 = load i32, i32* %i, align 4, !tbaa !6
  %add191 = add nsw i32 %261, %262
  %263 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub192 = sub nsw i32 1, %263
  %shl193 = shl i32 %add191, %sub192
  %264 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul194 = mul nsw i32 %shl193, %264
  %add.ptr195 = getelementptr inbounds i8, i8* %260, i32 %mul194
  %265 = load i32, i32* %x, align 4, !tbaa !6
  %266 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub196 = sub nsw i32 1, %266
  %shl197 = shl i32 %265, %sub196
  %add.ptr198 = getelementptr inbounds i8, i8* %add.ptr195, i32 %shl197
  %267 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %268 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %269 = load i32*, i32** %y_col_buf, align 4, !tbaa !2
  %270 = load i32, i32* %i, align 4, !tbaa !6
  %mul199 = mul nsw i32 %270, 4
  %add.ptr200 = getelementptr inbounds i32, i32* %269, i32 %mul199
  %271 = load i32*, i32** %cb_col_buf, align 4, !tbaa !2
  %272 = load i32, i32* %i, align 4, !tbaa !6
  %273 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub201 = sub nsw i32 2, %273
  %mul202 = mul nsw i32 %272, %sub201
  %274 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub203 = sub nsw i32 2, %274
  %mul204 = mul nsw i32 %mul202, %sub203
  %add.ptr205 = getelementptr inbounds i32, i32* %271, i32 %mul204
  %275 = load i32*, i32** %cr_col_buf, align 4, !tbaa !2
  %276 = load i32, i32* %i, align 4, !tbaa !6
  %277 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub206 = sub nsw i32 2, %277
  %mul207 = mul nsw i32 %276, %sub206
  %278 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub208 = sub nsw i32 2, %278
  %mul209 = mul nsw i32 %mul207, %sub208
  %add.ptr210 = getelementptr inbounds i32, i32* %275, i32 %mul209
  %279 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub211 = sub nsw i32 2, %279
  %280 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shr212 = ashr i32 %280, 1
  %281 = load i32, i32* %height.addr, align 4, !tbaa !6
  %div213 = sdiv i32 %281, 2
  %282 = load i32, i32* %y, align 4, !tbaa !6
  %sub214 = sub nsw i32 %div213, %282
  %cmp215 = icmp slt i32 %shr212, %sub214
  br i1 %cmp215, label %cond.true216, label %cond.false218

cond.true216:                                     ; preds = %if.else176
  %283 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shr217 = ashr i32 %283, 1
  br label %cond.end221

cond.false218:                                    ; preds = %if.else176
  %284 = load i32, i32* %height.addr, align 4, !tbaa !6
  %div219 = sdiv i32 %284, 2
  %285 = load i32, i32* %y, align 4, !tbaa !6
  %sub220 = sub nsw i32 %div219, %285
  br label %cond.end221

cond.end221:                                      ; preds = %cond.false218, %cond.true216
  %cond222 = phi i32 [ %shr217, %cond.true216 ], [ %sub220, %cond.false218 ]
  %286 = load i32, i32* %i, align 4, !tbaa !6
  %sub223 = sub nsw i32 %cond222, %286
  %287 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %288 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %289 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %290 = load i32, i32* %mc_identity.addr, align 4, !tbaa !6
  call void @add_noise_to_block(%struct.aom_film_grain_t* %247, i8* %add.ptr182, i8* %add.ptr190, i8* %add.ptr198, i32 %267, i32 %268, i32* %add.ptr200, i32* %add.ptr205, i32* %add.ptr210, i32 2, i32 %sub211, i32 %sub223, i32 1, i32 %287, i32 %288, i32 %289, i32 %290)
  br label %if.end224

if.end224:                                        ; preds = %cond.end221, %cond.end173
  %291 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %291) #5
  br label %if.end225

if.end225:                                        ; preds = %if.end224, %land.lhs.true, %for.body47
  %292 = load i32, i32* %overlap, align 4, !tbaa !6
  %tobool226 = icmp ne i32 %292, 0
  br i1 %tobool226, label %land.lhs.true227, label %if.end506

land.lhs.true227:                                 ; preds = %if.end225
  %293 = load i32, i32* %y, align 4, !tbaa !6
  %tobool228 = icmp ne i32 %293, 0
  br i1 %tobool228, label %if.then229, label %if.end506

if.then229:                                       ; preds = %land.lhs.true227
  %294 = load i32, i32* %x, align 4, !tbaa !6
  %tobool230 = icmp ne i32 %294, 0
  br i1 %tobool230, label %if.then231, label %if.end254

if.then231:                                       ; preds = %if.then229
  %295 = load i32*, i32** %y_line_buf, align 4, !tbaa !2
  %296 = load i32, i32* %x, align 4, !tbaa !6
  %shl232 = shl i32 %296, 1
  %add.ptr233 = getelementptr inbounds i32, i32* %295, i32 %shl232
  %297 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %298 = load i32*, i32** %y_col_buf, align 4, !tbaa !2
  %299 = load i32*, i32** %y_line_buf, align 4, !tbaa !2
  %300 = load i32, i32* %x, align 4, !tbaa !6
  %shl234 = shl i32 %300, 1
  %add.ptr235 = getelementptr inbounds i32, i32* %299, i32 %shl234
  %301 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  call void @hor_boundary_overlap(i32* %add.ptr233, i32 %297, i32* %298, i32 2, i32* %add.ptr235, i32 %301, i32 2, i32 2)
  %302 = load i32*, i32** %cb_line_buf, align 4, !tbaa !2
  %303 = load i32, i32* %x, align 4, !tbaa !6
  %304 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr236 = ashr i32 2, %304
  %mul237 = mul nsw i32 %303, %shr236
  %add.ptr238 = getelementptr inbounds i32, i32* %302, i32 %mul237
  %305 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %306 = load i32*, i32** %cb_col_buf, align 4, !tbaa !2
  %307 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr239 = ashr i32 2, %307
  %308 = load i32*, i32** %cb_line_buf, align 4, !tbaa !2
  %309 = load i32, i32* %x, align 4, !tbaa !6
  %310 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr240 = ashr i32 2, %310
  %mul241 = mul nsw i32 %309, %shr240
  %add.ptr242 = getelementptr inbounds i32, i32* %308, i32 %mul241
  %311 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %312 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr243 = ashr i32 2, %312
  %313 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr244 = ashr i32 2, %313
  call void @hor_boundary_overlap(i32* %add.ptr238, i32 %305, i32* %306, i32 %shr239, i32* %add.ptr242, i32 %311, i32 %shr243, i32 %shr244)
  %314 = load i32*, i32** %cr_line_buf, align 4, !tbaa !2
  %315 = load i32, i32* %x, align 4, !tbaa !6
  %316 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr245 = ashr i32 2, %316
  %mul246 = mul nsw i32 %315, %shr245
  %add.ptr247 = getelementptr inbounds i32, i32* %314, i32 %mul246
  %317 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %318 = load i32*, i32** %cr_col_buf, align 4, !tbaa !2
  %319 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr248 = ashr i32 2, %319
  %320 = load i32*, i32** %cr_line_buf, align 4, !tbaa !2
  %321 = load i32, i32* %x, align 4, !tbaa !6
  %322 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr249 = ashr i32 2, %322
  %mul250 = mul nsw i32 %321, %shr249
  %add.ptr251 = getelementptr inbounds i32, i32* %320, i32 %mul250
  %323 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %324 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr252 = ashr i32 2, %324
  %325 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr253 = ashr i32 2, %325
  call void @hor_boundary_overlap(i32* %add.ptr247, i32 %317, i32* %318, i32 %shr248, i32* %add.ptr251, i32 %323, i32 %shr252, i32 %shr253)
  br label %if.end254

if.end254:                                        ; preds = %if.then231, %if.then229
  %326 = load i32*, i32** %y_line_buf, align 4, !tbaa !2
  %327 = load i32, i32* %x, align 4, !tbaa !6
  %tobool255 = icmp ne i32 %327, 0
  br i1 %tobool255, label %cond.true256, label %cond.false258

cond.true256:                                     ; preds = %if.end254
  %328 = load i32, i32* %x, align 4, !tbaa !6
  %add257 = add nsw i32 %328, 1
  br label %cond.end259

cond.false258:                                    ; preds = %if.end254
  br label %cond.end259

cond.end259:                                      ; preds = %cond.false258, %cond.true256
  %cond260 = phi i32 [ %add257, %cond.true256 ], [ 0, %cond.false258 ]
  %shl261 = shl i32 %cond260, 1
  %add.ptr262 = getelementptr inbounds i32, i32* %326, i32 %shl261
  %329 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %330 = load i32*, i32** %luma_grain_block, align 4, !tbaa !2
  %331 = load i32, i32* %luma_offset_y, align 4, !tbaa !6
  %332 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %mul263 = mul nsw i32 %331, %332
  %add.ptr264 = getelementptr inbounds i32, i32* %330, i32 %mul263
  %333 = load i32, i32* %luma_offset_x, align 4, !tbaa !6
  %add.ptr265 = getelementptr inbounds i32, i32* %add.ptr264, i32 %333
  %334 = load i32, i32* %x, align 4, !tbaa !6
  %tobool266 = icmp ne i32 %334, 0
  %335 = zext i1 %tobool266 to i64
  %cond267 = select i1 %tobool266, i32 2, i32 0
  %add.ptr268 = getelementptr inbounds i32, i32* %add.ptr265, i32 %cond267
  %336 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %337 = load i32*, i32** %y_line_buf, align 4, !tbaa !2
  %338 = load i32, i32* %x, align 4, !tbaa !6
  %tobool269 = icmp ne i32 %338, 0
  br i1 %tobool269, label %cond.true270, label %cond.false272

cond.true270:                                     ; preds = %cond.end259
  %339 = load i32, i32* %x, align 4, !tbaa !6
  %add271 = add nsw i32 %339, 1
  br label %cond.end273

cond.false272:                                    ; preds = %cond.end259
  br label %cond.end273

cond.end273:                                      ; preds = %cond.false272, %cond.true270
  %cond274 = phi i32 [ %add271, %cond.true270 ], [ 0, %cond.false272 ]
  %shl275 = shl i32 %cond274, 1
  %add.ptr276 = getelementptr inbounds i32, i32* %337, i32 %shl275
  %340 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %341 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %342 = load i32, i32* %x, align 4, !tbaa !6
  %tobool277 = icmp ne i32 %342, 0
  %343 = zext i1 %tobool277 to i64
  %cond278 = select i1 %tobool277, i32 1, i32 0
  %shl279 = shl i32 %cond278, 1
  %sub280 = sub nsw i32 %341, %shl279
  %344 = load i32, i32* %width.addr, align 4, !tbaa !6
  %345 = load i32, i32* %x, align 4, !tbaa !6
  %tobool281 = icmp ne i32 %345, 0
  br i1 %tobool281, label %cond.true282, label %cond.false284

cond.true282:                                     ; preds = %cond.end273
  %346 = load i32, i32* %x, align 4, !tbaa !6
  %add283 = add nsw i32 %346, 1
  br label %cond.end285

cond.false284:                                    ; preds = %cond.end273
  br label %cond.end285

cond.end285:                                      ; preds = %cond.false284, %cond.true282
  %cond286 = phi i32 [ %add283, %cond.true282 ], [ 0, %cond.false284 ]
  %shl287 = shl i32 %cond286, 1
  %sub288 = sub nsw i32 %344, %shl287
  %cmp289 = icmp slt i32 %sub280, %sub288
  br i1 %cmp289, label %cond.true290, label %cond.false295

cond.true290:                                     ; preds = %cond.end285
  %347 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %348 = load i32, i32* %x, align 4, !tbaa !6
  %tobool291 = icmp ne i32 %348, 0
  %349 = zext i1 %tobool291 to i64
  %cond292 = select i1 %tobool291, i32 1, i32 0
  %shl293 = shl i32 %cond292, 1
  %sub294 = sub nsw i32 %347, %shl293
  br label %cond.end304

cond.false295:                                    ; preds = %cond.end285
  %350 = load i32, i32* %width.addr, align 4, !tbaa !6
  %351 = load i32, i32* %x, align 4, !tbaa !6
  %tobool296 = icmp ne i32 %351, 0
  br i1 %tobool296, label %cond.true297, label %cond.false299

cond.true297:                                     ; preds = %cond.false295
  %352 = load i32, i32* %x, align 4, !tbaa !6
  %add298 = add nsw i32 %352, 1
  br label %cond.end300

cond.false299:                                    ; preds = %cond.false295
  br label %cond.end300

cond.end300:                                      ; preds = %cond.false299, %cond.true297
  %cond301 = phi i32 [ %add298, %cond.true297 ], [ 0, %cond.false299 ]
  %shl302 = shl i32 %cond301, 1
  %sub303 = sub nsw i32 %350, %shl302
  br label %cond.end304

cond.end304:                                      ; preds = %cond.end300, %cond.true290
  %cond305 = phi i32 [ %sub294, %cond.true290 ], [ %sub303, %cond.end300 ]
  call void @hor_boundary_overlap(i32* %add.ptr262, i32 %329, i32* %add.ptr268, i32 %336, i32* %add.ptr276, i32 %340, i32 %cond305, i32 2)
  %353 = load i32*, i32** %cb_line_buf, align 4, !tbaa !2
  %354 = load i32, i32* %x, align 4, !tbaa !6
  %tobool306 = icmp ne i32 %354, 0
  br i1 %tobool306, label %cond.true307, label %cond.false309

cond.true307:                                     ; preds = %cond.end304
  %355 = load i32, i32* %x, align 4, !tbaa !6
  %add308 = add nsw i32 %355, 1
  br label %cond.end310

cond.false309:                                    ; preds = %cond.end304
  br label %cond.end310

cond.end310:                                      ; preds = %cond.false309, %cond.true307
  %cond311 = phi i32 [ %add308, %cond.true307 ], [ 0, %cond.false309 ]
  %356 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub312 = sub nsw i32 1, %356
  %shl313 = shl i32 %cond311, %sub312
  %add.ptr314 = getelementptr inbounds i32, i32* %353, i32 %shl313
  %357 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %358 = load i32*, i32** %cb_grain_block, align 4, !tbaa !2
  %359 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %360 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul315 = mul nsw i32 %359, %360
  %add.ptr316 = getelementptr inbounds i32, i32* %358, i32 %mul315
  %361 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr317 = getelementptr inbounds i32, i32* %add.ptr316, i32 %361
  %362 = load i32, i32* %x, align 4, !tbaa !6
  %tobool318 = icmp ne i32 %362, 0
  %363 = zext i1 %tobool318 to i64
  %cond319 = select i1 %tobool318, i32 1, i32 0
  %364 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub320 = sub nsw i32 1, %364
  %shl321 = shl i32 %cond319, %sub320
  %add.ptr322 = getelementptr inbounds i32, i32* %add.ptr317, i32 %shl321
  %365 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %366 = load i32*, i32** %cb_line_buf, align 4, !tbaa !2
  %367 = load i32, i32* %x, align 4, !tbaa !6
  %tobool323 = icmp ne i32 %367, 0
  br i1 %tobool323, label %cond.true324, label %cond.false326

cond.true324:                                     ; preds = %cond.end310
  %368 = load i32, i32* %x, align 4, !tbaa !6
  %add325 = add nsw i32 %368, 1
  br label %cond.end327

cond.false326:                                    ; preds = %cond.end310
  br label %cond.end327

cond.end327:                                      ; preds = %cond.false326, %cond.true324
  %cond328 = phi i32 [ %add325, %cond.true324 ], [ 0, %cond.false326 ]
  %369 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub329 = sub nsw i32 1, %369
  %shl330 = shl i32 %cond328, %sub329
  %add.ptr331 = getelementptr inbounds i32, i32* %366, i32 %shl330
  %370 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %371 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %372 = load i32, i32* %x, align 4, !tbaa !6
  %tobool332 = icmp ne i32 %372, 0
  %373 = zext i1 %tobool332 to i64
  %cond333 = select i1 %tobool332, i32 1, i32 0
  %374 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub334 = sub nsw i32 1, %374
  %shl335 = shl i32 %cond333, %sub334
  %sub336 = sub nsw i32 %371, %shl335
  %375 = load i32, i32* %width.addr, align 4, !tbaa !6
  %376 = load i32, i32* %x, align 4, !tbaa !6
  %tobool337 = icmp ne i32 %376, 0
  br i1 %tobool337, label %cond.true338, label %cond.false340

cond.true338:                                     ; preds = %cond.end327
  %377 = load i32, i32* %x, align 4, !tbaa !6
  %add339 = add nsw i32 %377, 1
  br label %cond.end341

cond.false340:                                    ; preds = %cond.end327
  br label %cond.end341

cond.end341:                                      ; preds = %cond.false340, %cond.true338
  %cond342 = phi i32 [ %add339, %cond.true338 ], [ 0, %cond.false340 ]
  %shl343 = shl i32 %cond342, 1
  %sub344 = sub nsw i32 %375, %shl343
  %378 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr345 = ashr i32 %sub344, %378
  %cmp346 = icmp slt i32 %sub336, %shr345
  br i1 %cmp346, label %cond.true347, label %cond.false353

cond.true347:                                     ; preds = %cond.end341
  %379 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %380 = load i32, i32* %x, align 4, !tbaa !6
  %tobool348 = icmp ne i32 %380, 0
  %381 = zext i1 %tobool348 to i64
  %cond349 = select i1 %tobool348, i32 1, i32 0
  %382 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub350 = sub nsw i32 1, %382
  %shl351 = shl i32 %cond349, %sub350
  %sub352 = sub nsw i32 %379, %shl351
  br label %cond.end363

cond.false353:                                    ; preds = %cond.end341
  %383 = load i32, i32* %width.addr, align 4, !tbaa !6
  %384 = load i32, i32* %x, align 4, !tbaa !6
  %tobool354 = icmp ne i32 %384, 0
  br i1 %tobool354, label %cond.true355, label %cond.false357

cond.true355:                                     ; preds = %cond.false353
  %385 = load i32, i32* %x, align 4, !tbaa !6
  %add356 = add nsw i32 %385, 1
  br label %cond.end358

cond.false357:                                    ; preds = %cond.false353
  br label %cond.end358

cond.end358:                                      ; preds = %cond.false357, %cond.true355
  %cond359 = phi i32 [ %add356, %cond.true355 ], [ 0, %cond.false357 ]
  %shl360 = shl i32 %cond359, 1
  %sub361 = sub nsw i32 %383, %shl360
  %386 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr362 = ashr i32 %sub361, %386
  br label %cond.end363

cond.end363:                                      ; preds = %cond.end358, %cond.true347
  %cond364 = phi i32 [ %sub352, %cond.true347 ], [ %shr362, %cond.end358 ]
  %387 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr365 = ashr i32 2, %387
  call void @hor_boundary_overlap(i32* %add.ptr314, i32 %357, i32* %add.ptr322, i32 %365, i32* %add.ptr331, i32 %370, i32 %cond364, i32 %shr365)
  %388 = load i32*, i32** %cr_line_buf, align 4, !tbaa !2
  %389 = load i32, i32* %x, align 4, !tbaa !6
  %tobool366 = icmp ne i32 %389, 0
  br i1 %tobool366, label %cond.true367, label %cond.false369

cond.true367:                                     ; preds = %cond.end363
  %390 = load i32, i32* %x, align 4, !tbaa !6
  %add368 = add nsw i32 %390, 1
  br label %cond.end370

cond.false369:                                    ; preds = %cond.end363
  br label %cond.end370

cond.end370:                                      ; preds = %cond.false369, %cond.true367
  %cond371 = phi i32 [ %add368, %cond.true367 ], [ 0, %cond.false369 ]
  %391 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub372 = sub nsw i32 1, %391
  %shl373 = shl i32 %cond371, %sub372
  %add.ptr374 = getelementptr inbounds i32, i32* %388, i32 %shl373
  %392 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %393 = load i32*, i32** %cr_grain_block, align 4, !tbaa !2
  %394 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %395 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul375 = mul nsw i32 %394, %395
  %add.ptr376 = getelementptr inbounds i32, i32* %393, i32 %mul375
  %396 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr377 = getelementptr inbounds i32, i32* %add.ptr376, i32 %396
  %397 = load i32, i32* %x, align 4, !tbaa !6
  %tobool378 = icmp ne i32 %397, 0
  %398 = zext i1 %tobool378 to i64
  %cond379 = select i1 %tobool378, i32 1, i32 0
  %399 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub380 = sub nsw i32 1, %399
  %shl381 = shl i32 %cond379, %sub380
  %add.ptr382 = getelementptr inbounds i32, i32* %add.ptr377, i32 %shl381
  %400 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %401 = load i32*, i32** %cr_line_buf, align 4, !tbaa !2
  %402 = load i32, i32* %x, align 4, !tbaa !6
  %tobool383 = icmp ne i32 %402, 0
  br i1 %tobool383, label %cond.true384, label %cond.false386

cond.true384:                                     ; preds = %cond.end370
  %403 = load i32, i32* %x, align 4, !tbaa !6
  %add385 = add nsw i32 %403, 1
  br label %cond.end387

cond.false386:                                    ; preds = %cond.end370
  br label %cond.end387

cond.end387:                                      ; preds = %cond.false386, %cond.true384
  %cond388 = phi i32 [ %add385, %cond.true384 ], [ 0, %cond.false386 ]
  %404 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub389 = sub nsw i32 1, %404
  %shl390 = shl i32 %cond388, %sub389
  %add.ptr391 = getelementptr inbounds i32, i32* %401, i32 %shl390
  %405 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %406 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %407 = load i32, i32* %x, align 4, !tbaa !6
  %tobool392 = icmp ne i32 %407, 0
  %408 = zext i1 %tobool392 to i64
  %cond393 = select i1 %tobool392, i32 1, i32 0
  %409 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub394 = sub nsw i32 1, %409
  %shl395 = shl i32 %cond393, %sub394
  %sub396 = sub nsw i32 %406, %shl395
  %410 = load i32, i32* %width.addr, align 4, !tbaa !6
  %411 = load i32, i32* %x, align 4, !tbaa !6
  %tobool397 = icmp ne i32 %411, 0
  br i1 %tobool397, label %cond.true398, label %cond.false400

cond.true398:                                     ; preds = %cond.end387
  %412 = load i32, i32* %x, align 4, !tbaa !6
  %add399 = add nsw i32 %412, 1
  br label %cond.end401

cond.false400:                                    ; preds = %cond.end387
  br label %cond.end401

cond.end401:                                      ; preds = %cond.false400, %cond.true398
  %cond402 = phi i32 [ %add399, %cond.true398 ], [ 0, %cond.false400 ]
  %shl403 = shl i32 %cond402, 1
  %sub404 = sub nsw i32 %410, %shl403
  %413 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr405 = ashr i32 %sub404, %413
  %cmp406 = icmp slt i32 %sub396, %shr405
  br i1 %cmp406, label %cond.true407, label %cond.false413

cond.true407:                                     ; preds = %cond.end401
  %414 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %415 = load i32, i32* %x, align 4, !tbaa !6
  %tobool408 = icmp ne i32 %415, 0
  %416 = zext i1 %tobool408 to i64
  %cond409 = select i1 %tobool408, i32 1, i32 0
  %417 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub410 = sub nsw i32 1, %417
  %shl411 = shl i32 %cond409, %sub410
  %sub412 = sub nsw i32 %414, %shl411
  br label %cond.end423

cond.false413:                                    ; preds = %cond.end401
  %418 = load i32, i32* %width.addr, align 4, !tbaa !6
  %419 = load i32, i32* %x, align 4, !tbaa !6
  %tobool414 = icmp ne i32 %419, 0
  br i1 %tobool414, label %cond.true415, label %cond.false417

cond.true415:                                     ; preds = %cond.false413
  %420 = load i32, i32* %x, align 4, !tbaa !6
  %add416 = add nsw i32 %420, 1
  br label %cond.end418

cond.false417:                                    ; preds = %cond.false413
  br label %cond.end418

cond.end418:                                      ; preds = %cond.false417, %cond.true415
  %cond419 = phi i32 [ %add416, %cond.true415 ], [ 0, %cond.false417 ]
  %shl420 = shl i32 %cond419, 1
  %sub421 = sub nsw i32 %418, %shl420
  %421 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr422 = ashr i32 %sub421, %421
  br label %cond.end423

cond.end423:                                      ; preds = %cond.end418, %cond.true407
  %cond424 = phi i32 [ %sub412, %cond.true407 ], [ %shr422, %cond.end418 ]
  %422 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr425 = ashr i32 2, %422
  call void @hor_boundary_overlap(i32* %add.ptr374, i32 %392, i32* %add.ptr382, i32 %400, i32* %add.ptr391, i32 %405, i32 %cond424, i32 %shr425)
  %423 = load i32, i32* %use_high_bit_depth.addr, align 4, !tbaa !6
  %tobool426 = icmp ne i32 %423, 0
  br i1 %tobool426, label %if.then427, label %if.else466

if.then427:                                       ; preds = %cond.end423
  %424 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %425 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %426 = bitcast i8* %425 to i16*
  %427 = load i32, i32* %y, align 4, !tbaa !6
  %shl428 = shl i32 %427, 1
  %428 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul429 = mul nsw i32 %shl428, %428
  %add.ptr430 = getelementptr inbounds i16, i16* %426, i32 %mul429
  %429 = load i32, i32* %x, align 4, !tbaa !6
  %shl431 = shl i32 %429, 1
  %add.ptr432 = getelementptr inbounds i16, i16* %add.ptr430, i32 %shl431
  %430 = load i8*, i8** %cb.addr, align 4, !tbaa !2
  %431 = bitcast i8* %430 to i16*
  %432 = load i32, i32* %y, align 4, !tbaa !6
  %433 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub433 = sub nsw i32 1, %433
  %shl434 = shl i32 %432, %sub433
  %434 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul435 = mul nsw i32 %shl434, %434
  %add.ptr436 = getelementptr inbounds i16, i16* %431, i32 %mul435
  %435 = load i32, i32* %x, align 4, !tbaa !6
  %436 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub437 = sub nsw i32 1, %436
  %shl438 = shl i32 %435, %sub437
  %add.ptr439 = getelementptr inbounds i16, i16* %add.ptr436, i32 %shl438
  %437 = load i8*, i8** %cr.addr, align 4, !tbaa !2
  %438 = bitcast i8* %437 to i16*
  %439 = load i32, i32* %y, align 4, !tbaa !6
  %440 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub440 = sub nsw i32 1, %440
  %shl441 = shl i32 %439, %sub440
  %441 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul442 = mul nsw i32 %shl441, %441
  %add.ptr443 = getelementptr inbounds i16, i16* %438, i32 %mul442
  %442 = load i32, i32* %x, align 4, !tbaa !6
  %443 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub444 = sub nsw i32 1, %443
  %shl445 = shl i32 %442, %sub444
  %add.ptr446 = getelementptr inbounds i16, i16* %add.ptr443, i32 %shl445
  %444 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %445 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %446 = load i32*, i32** %y_line_buf, align 4, !tbaa !2
  %447 = load i32, i32* %x, align 4, !tbaa !6
  %shl447 = shl i32 %447, 1
  %add.ptr448 = getelementptr inbounds i32, i32* %446, i32 %shl447
  %448 = load i32*, i32** %cb_line_buf, align 4, !tbaa !2
  %449 = load i32, i32* %x, align 4, !tbaa !6
  %450 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub449 = sub nsw i32 1, %450
  %shl450 = shl i32 %449, %sub449
  %add.ptr451 = getelementptr inbounds i32, i32* %448, i32 %shl450
  %451 = load i32*, i32** %cr_line_buf, align 4, !tbaa !2
  %452 = load i32, i32* %x, align 4, !tbaa !6
  %453 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub452 = sub nsw i32 1, %453
  %shl453 = shl i32 %452, %sub452
  %add.ptr454 = getelementptr inbounds i32, i32* %451, i32 %shl453
  %454 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %455 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %456 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %shr455 = ashr i32 %456, 1
  %457 = load i32, i32* %width.addr, align 4, !tbaa !6
  %div456 = sdiv i32 %457, 2
  %458 = load i32, i32* %x, align 4, !tbaa !6
  %sub457 = sub nsw i32 %div456, %458
  %cmp458 = icmp slt i32 %shr455, %sub457
  br i1 %cmp458, label %cond.true459, label %cond.false461

cond.true459:                                     ; preds = %if.then427
  %459 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %shr460 = ashr i32 %459, 1
  br label %cond.end464

cond.false461:                                    ; preds = %if.then427
  %460 = load i32, i32* %width.addr, align 4, !tbaa !6
  %div462 = sdiv i32 %460, 2
  %461 = load i32, i32* %x, align 4, !tbaa !6
  %sub463 = sub nsw i32 %div462, %461
  br label %cond.end464

cond.end464:                                      ; preds = %cond.false461, %cond.true459
  %cond465 = phi i32 [ %shr460, %cond.true459 ], [ %sub463, %cond.false461 ]
  %462 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %463 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %464 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %465 = load i32, i32* %mc_identity.addr, align 4, !tbaa !6
  call void @add_noise_to_block_hbd(%struct.aom_film_grain_t* %424, i16* %add.ptr432, i16* %add.ptr439, i16* %add.ptr446, i32 %444, i32 %445, i32* %add.ptr448, i32* %add.ptr451, i32* %add.ptr454, i32 %454, i32 %455, i32 1, i32 %cond465, i32 %462, i32 %463, i32 %464, i32 %465)
  br label %if.end505

if.else466:                                       ; preds = %cond.end423
  %466 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %467 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %468 = load i32, i32* %y, align 4, !tbaa !6
  %shl467 = shl i32 %468, 1
  %469 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul468 = mul nsw i32 %shl467, %469
  %add.ptr469 = getelementptr inbounds i8, i8* %467, i32 %mul468
  %470 = load i32, i32* %x, align 4, !tbaa !6
  %shl470 = shl i32 %470, 1
  %add.ptr471 = getelementptr inbounds i8, i8* %add.ptr469, i32 %shl470
  %471 = load i8*, i8** %cb.addr, align 4, !tbaa !2
  %472 = load i32, i32* %y, align 4, !tbaa !6
  %473 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub472 = sub nsw i32 1, %473
  %shl473 = shl i32 %472, %sub472
  %474 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul474 = mul nsw i32 %shl473, %474
  %add.ptr475 = getelementptr inbounds i8, i8* %471, i32 %mul474
  %475 = load i32, i32* %x, align 4, !tbaa !6
  %476 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub476 = sub nsw i32 1, %476
  %shl477 = shl i32 %475, %sub476
  %add.ptr478 = getelementptr inbounds i8, i8* %add.ptr475, i32 %shl477
  %477 = load i8*, i8** %cr.addr, align 4, !tbaa !2
  %478 = load i32, i32* %y, align 4, !tbaa !6
  %479 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub479 = sub nsw i32 1, %479
  %shl480 = shl i32 %478, %sub479
  %480 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul481 = mul nsw i32 %shl480, %480
  %add.ptr482 = getelementptr inbounds i8, i8* %477, i32 %mul481
  %481 = load i32, i32* %x, align 4, !tbaa !6
  %482 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub483 = sub nsw i32 1, %482
  %shl484 = shl i32 %481, %sub483
  %add.ptr485 = getelementptr inbounds i8, i8* %add.ptr482, i32 %shl484
  %483 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %484 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %485 = load i32*, i32** %y_line_buf, align 4, !tbaa !2
  %486 = load i32, i32* %x, align 4, !tbaa !6
  %shl486 = shl i32 %486, 1
  %add.ptr487 = getelementptr inbounds i32, i32* %485, i32 %shl486
  %487 = load i32*, i32** %cb_line_buf, align 4, !tbaa !2
  %488 = load i32, i32* %x, align 4, !tbaa !6
  %489 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub488 = sub nsw i32 1, %489
  %shl489 = shl i32 %488, %sub488
  %add.ptr490 = getelementptr inbounds i32, i32* %487, i32 %shl489
  %490 = load i32*, i32** %cr_line_buf, align 4, !tbaa !2
  %491 = load i32, i32* %x, align 4, !tbaa !6
  %492 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub491 = sub nsw i32 1, %492
  %shl492 = shl i32 %491, %sub491
  %add.ptr493 = getelementptr inbounds i32, i32* %490, i32 %shl492
  %493 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %494 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %495 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %shr494 = ashr i32 %495, 1
  %496 = load i32, i32* %width.addr, align 4, !tbaa !6
  %div495 = sdiv i32 %496, 2
  %497 = load i32, i32* %x, align 4, !tbaa !6
  %sub496 = sub nsw i32 %div495, %497
  %cmp497 = icmp slt i32 %shr494, %sub496
  br i1 %cmp497, label %cond.true498, label %cond.false500

cond.true498:                                     ; preds = %if.else466
  %498 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %shr499 = ashr i32 %498, 1
  br label %cond.end503

cond.false500:                                    ; preds = %if.else466
  %499 = load i32, i32* %width.addr, align 4, !tbaa !6
  %div501 = sdiv i32 %499, 2
  %500 = load i32, i32* %x, align 4, !tbaa !6
  %sub502 = sub nsw i32 %div501, %500
  br label %cond.end503

cond.end503:                                      ; preds = %cond.false500, %cond.true498
  %cond504 = phi i32 [ %shr499, %cond.true498 ], [ %sub502, %cond.false500 ]
  %501 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %502 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %503 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %504 = load i32, i32* %mc_identity.addr, align 4, !tbaa !6
  call void @add_noise_to_block(%struct.aom_film_grain_t* %466, i8* %add.ptr471, i8* %add.ptr478, i8* %add.ptr485, i32 %483, i32 %484, i32* %add.ptr487, i32* %add.ptr490, i32* %add.ptr493, i32 %493, i32 %494, i32 1, i32 %cond504, i32 %501, i32 %502, i32 %503, i32 %504)
  br label %if.end505

if.end505:                                        ; preds = %cond.end503, %cond.end464
  br label %if.end506

if.end506:                                        ; preds = %if.end505, %land.lhs.true227, %if.end225
  %505 = bitcast i32* %i507 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %505) #5
  %506 = load i32, i32* %overlap, align 4, !tbaa !6
  %tobool508 = icmp ne i32 %506, 0
  br i1 %tobool508, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end506
  %507 = load i32, i32* %y, align 4, !tbaa !6
  %tobool509 = icmp ne i32 %507, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end506
  %508 = phi i1 [ false, %if.end506 ], [ %tobool509, %land.rhs ]
  %509 = zext i1 %508 to i64
  %cond510 = select i1 %508, i32 1, i32 0
  store i32 %cond510, i32* %i507, align 4, !tbaa !6
  %510 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %510) #5
  %511 = load i32, i32* %overlap, align 4, !tbaa !6
  %tobool511 = icmp ne i32 %511, 0
  br i1 %tobool511, label %land.rhs512, label %land.end514

land.rhs512:                                      ; preds = %land.end
  %512 = load i32, i32* %x, align 4, !tbaa !6
  %tobool513 = icmp ne i32 %512, 0
  br label %land.end514

land.end514:                                      ; preds = %land.rhs512, %land.end
  %513 = phi i1 [ false, %land.end ], [ %tobool513, %land.rhs512 ]
  %514 = zext i1 %513 to i64
  %cond515 = select i1 %513, i32 1, i32 0
  store i32 %cond515, i32* %j, align 4, !tbaa !6
  %515 = load i32, i32* %use_high_bit_depth.addr, align 4, !tbaa !6
  %tobool516 = icmp ne i32 %515, 0
  br i1 %tobool516, label %if.then517, label %if.else592

if.then517:                                       ; preds = %land.end514
  %516 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %517 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %518 = bitcast i8* %517 to i16*
  %519 = load i32, i32* %y, align 4, !tbaa !6
  %520 = load i32, i32* %i507, align 4, !tbaa !6
  %add518 = add nsw i32 %519, %520
  %shl519 = shl i32 %add518, 1
  %521 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul520 = mul nsw i32 %shl519, %521
  %add.ptr521 = getelementptr inbounds i16, i16* %518, i32 %mul520
  %522 = load i32, i32* %x, align 4, !tbaa !6
  %523 = load i32, i32* %j, align 4, !tbaa !6
  %add522 = add nsw i32 %522, %523
  %shl523 = shl i32 %add522, 1
  %add.ptr524 = getelementptr inbounds i16, i16* %add.ptr521, i32 %shl523
  %524 = load i8*, i8** %cb.addr, align 4, !tbaa !2
  %525 = bitcast i8* %524 to i16*
  %526 = load i32, i32* %y, align 4, !tbaa !6
  %527 = load i32, i32* %i507, align 4, !tbaa !6
  %add525 = add nsw i32 %526, %527
  %528 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub526 = sub nsw i32 1, %528
  %shl527 = shl i32 %add525, %sub526
  %529 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul528 = mul nsw i32 %shl527, %529
  %add.ptr529 = getelementptr inbounds i16, i16* %525, i32 %mul528
  %530 = load i32, i32* %x, align 4, !tbaa !6
  %531 = load i32, i32* %j, align 4, !tbaa !6
  %add530 = add nsw i32 %530, %531
  %532 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub531 = sub nsw i32 1, %532
  %shl532 = shl i32 %add530, %sub531
  %add.ptr533 = getelementptr inbounds i16, i16* %add.ptr529, i32 %shl532
  %533 = load i8*, i8** %cr.addr, align 4, !tbaa !2
  %534 = bitcast i8* %533 to i16*
  %535 = load i32, i32* %y, align 4, !tbaa !6
  %536 = load i32, i32* %i507, align 4, !tbaa !6
  %add534 = add nsw i32 %535, %536
  %537 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub535 = sub nsw i32 1, %537
  %shl536 = shl i32 %add534, %sub535
  %538 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul537 = mul nsw i32 %shl536, %538
  %add.ptr538 = getelementptr inbounds i16, i16* %534, i32 %mul537
  %539 = load i32, i32* %x, align 4, !tbaa !6
  %540 = load i32, i32* %j, align 4, !tbaa !6
  %add539 = add nsw i32 %539, %540
  %541 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub540 = sub nsw i32 1, %541
  %shl541 = shl i32 %add539, %sub540
  %add.ptr542 = getelementptr inbounds i16, i16* %add.ptr538, i32 %shl541
  %542 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %543 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %544 = load i32*, i32** %luma_grain_block, align 4, !tbaa !2
  %545 = load i32, i32* %luma_offset_y, align 4, !tbaa !6
  %546 = load i32, i32* %i507, align 4, !tbaa !6
  %shl543 = shl i32 %546, 1
  %add544 = add nsw i32 %545, %shl543
  %547 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %mul545 = mul nsw i32 %add544, %547
  %add.ptr546 = getelementptr inbounds i32, i32* %544, i32 %mul545
  %548 = load i32, i32* %luma_offset_x, align 4, !tbaa !6
  %add.ptr547 = getelementptr inbounds i32, i32* %add.ptr546, i32 %548
  %549 = load i32, i32* %j, align 4, !tbaa !6
  %shl548 = shl i32 %549, 1
  %add.ptr549 = getelementptr inbounds i32, i32* %add.ptr547, i32 %shl548
  %550 = load i32*, i32** %cb_grain_block, align 4, !tbaa !2
  %551 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %552 = load i32, i32* %i507, align 4, !tbaa !6
  %553 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub550 = sub nsw i32 1, %553
  %shl551 = shl i32 %552, %sub550
  %add552 = add nsw i32 %551, %shl551
  %554 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul553 = mul nsw i32 %add552, %554
  %add.ptr554 = getelementptr inbounds i32, i32* %550, i32 %mul553
  %555 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr555 = getelementptr inbounds i32, i32* %add.ptr554, i32 %555
  %556 = load i32, i32* %j, align 4, !tbaa !6
  %557 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub556 = sub nsw i32 1, %557
  %shl557 = shl i32 %556, %sub556
  %add.ptr558 = getelementptr inbounds i32, i32* %add.ptr555, i32 %shl557
  %558 = load i32*, i32** %cr_grain_block, align 4, !tbaa !2
  %559 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %560 = load i32, i32* %i507, align 4, !tbaa !6
  %561 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub559 = sub nsw i32 1, %561
  %shl560 = shl i32 %560, %sub559
  %add561 = add nsw i32 %559, %shl560
  %562 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul562 = mul nsw i32 %add561, %562
  %add.ptr563 = getelementptr inbounds i32, i32* %558, i32 %mul562
  %563 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr564 = getelementptr inbounds i32, i32* %add.ptr563, i32 %563
  %564 = load i32, i32* %j, align 4, !tbaa !6
  %565 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub565 = sub nsw i32 1, %565
  %shl566 = shl i32 %564, %sub565
  %add.ptr567 = getelementptr inbounds i32, i32* %add.ptr564, i32 %shl566
  %566 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %567 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %568 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shr568 = ashr i32 %568, 1
  %569 = load i32, i32* %height.addr, align 4, !tbaa !6
  %div569 = sdiv i32 %569, 2
  %570 = load i32, i32* %y, align 4, !tbaa !6
  %sub570 = sub nsw i32 %div569, %570
  %cmp571 = icmp slt i32 %shr568, %sub570
  br i1 %cmp571, label %cond.true572, label %cond.false574

cond.true572:                                     ; preds = %if.then517
  %571 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shr573 = ashr i32 %571, 1
  br label %cond.end577

cond.false574:                                    ; preds = %if.then517
  %572 = load i32, i32* %height.addr, align 4, !tbaa !6
  %div575 = sdiv i32 %572, 2
  %573 = load i32, i32* %y, align 4, !tbaa !6
  %sub576 = sub nsw i32 %div575, %573
  br label %cond.end577

cond.end577:                                      ; preds = %cond.false574, %cond.true572
  %cond578 = phi i32 [ %shr573, %cond.true572 ], [ %sub576, %cond.false574 ]
  %574 = load i32, i32* %i507, align 4, !tbaa !6
  %sub579 = sub nsw i32 %cond578, %574
  %575 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %shr580 = ashr i32 %575, 1
  %576 = load i32, i32* %width.addr, align 4, !tbaa !6
  %div581 = sdiv i32 %576, 2
  %577 = load i32, i32* %x, align 4, !tbaa !6
  %sub582 = sub nsw i32 %div581, %577
  %cmp583 = icmp slt i32 %shr580, %sub582
  br i1 %cmp583, label %cond.true584, label %cond.false586

cond.true584:                                     ; preds = %cond.end577
  %578 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %shr585 = ashr i32 %578, 1
  br label %cond.end589

cond.false586:                                    ; preds = %cond.end577
  %579 = load i32, i32* %width.addr, align 4, !tbaa !6
  %div587 = sdiv i32 %579, 2
  %580 = load i32, i32* %x, align 4, !tbaa !6
  %sub588 = sub nsw i32 %div587, %580
  br label %cond.end589

cond.end589:                                      ; preds = %cond.false586, %cond.true584
  %cond590 = phi i32 [ %shr585, %cond.true584 ], [ %sub588, %cond.false586 ]
  %581 = load i32, i32* %j, align 4, !tbaa !6
  %sub591 = sub nsw i32 %cond590, %581
  %582 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %583 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %584 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %585 = load i32, i32* %mc_identity.addr, align 4, !tbaa !6
  call void @add_noise_to_block_hbd(%struct.aom_film_grain_t* %516, i16* %add.ptr524, i16* %add.ptr533, i16* %add.ptr542, i32 %542, i32 %543, i32* %add.ptr549, i32* %add.ptr558, i32* %add.ptr567, i32 %566, i32 %567, i32 %sub579, i32 %sub591, i32 %582, i32 %583, i32 %584, i32 %585)
  br label %if.end667

if.else592:                                       ; preds = %land.end514
  %586 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %587 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %588 = load i32, i32* %y, align 4, !tbaa !6
  %589 = load i32, i32* %i507, align 4, !tbaa !6
  %add593 = add nsw i32 %588, %589
  %shl594 = shl i32 %add593, 1
  %590 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul595 = mul nsw i32 %shl594, %590
  %add.ptr596 = getelementptr inbounds i8, i8* %587, i32 %mul595
  %591 = load i32, i32* %x, align 4, !tbaa !6
  %592 = load i32, i32* %j, align 4, !tbaa !6
  %add597 = add nsw i32 %591, %592
  %shl598 = shl i32 %add597, 1
  %add.ptr599 = getelementptr inbounds i8, i8* %add.ptr596, i32 %shl598
  %593 = load i8*, i8** %cb.addr, align 4, !tbaa !2
  %594 = load i32, i32* %y, align 4, !tbaa !6
  %595 = load i32, i32* %i507, align 4, !tbaa !6
  %add600 = add nsw i32 %594, %595
  %596 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub601 = sub nsw i32 1, %596
  %shl602 = shl i32 %add600, %sub601
  %597 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul603 = mul nsw i32 %shl602, %597
  %add.ptr604 = getelementptr inbounds i8, i8* %593, i32 %mul603
  %598 = load i32, i32* %x, align 4, !tbaa !6
  %599 = load i32, i32* %j, align 4, !tbaa !6
  %add605 = add nsw i32 %598, %599
  %600 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub606 = sub nsw i32 1, %600
  %shl607 = shl i32 %add605, %sub606
  %add.ptr608 = getelementptr inbounds i8, i8* %add.ptr604, i32 %shl607
  %601 = load i8*, i8** %cr.addr, align 4, !tbaa !2
  %602 = load i32, i32* %y, align 4, !tbaa !6
  %603 = load i32, i32* %i507, align 4, !tbaa !6
  %add609 = add nsw i32 %602, %603
  %604 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub610 = sub nsw i32 1, %604
  %shl611 = shl i32 %add609, %sub610
  %605 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul612 = mul nsw i32 %shl611, %605
  %add.ptr613 = getelementptr inbounds i8, i8* %601, i32 %mul612
  %606 = load i32, i32* %x, align 4, !tbaa !6
  %607 = load i32, i32* %j, align 4, !tbaa !6
  %add614 = add nsw i32 %606, %607
  %608 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub615 = sub nsw i32 1, %608
  %shl616 = shl i32 %add614, %sub615
  %add.ptr617 = getelementptr inbounds i8, i8* %add.ptr613, i32 %shl616
  %609 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %610 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %611 = load i32*, i32** %luma_grain_block, align 4, !tbaa !2
  %612 = load i32, i32* %luma_offset_y, align 4, !tbaa !6
  %613 = load i32, i32* %i507, align 4, !tbaa !6
  %shl618 = shl i32 %613, 1
  %add619 = add nsw i32 %612, %shl618
  %614 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %mul620 = mul nsw i32 %add619, %614
  %add.ptr621 = getelementptr inbounds i32, i32* %611, i32 %mul620
  %615 = load i32, i32* %luma_offset_x, align 4, !tbaa !6
  %add.ptr622 = getelementptr inbounds i32, i32* %add.ptr621, i32 %615
  %616 = load i32, i32* %j, align 4, !tbaa !6
  %shl623 = shl i32 %616, 1
  %add.ptr624 = getelementptr inbounds i32, i32* %add.ptr622, i32 %shl623
  %617 = load i32*, i32** %cb_grain_block, align 4, !tbaa !2
  %618 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %619 = load i32, i32* %i507, align 4, !tbaa !6
  %620 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub625 = sub nsw i32 1, %620
  %shl626 = shl i32 %619, %sub625
  %add627 = add nsw i32 %618, %shl626
  %621 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul628 = mul nsw i32 %add627, %621
  %add.ptr629 = getelementptr inbounds i32, i32* %617, i32 %mul628
  %622 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr630 = getelementptr inbounds i32, i32* %add.ptr629, i32 %622
  %623 = load i32, i32* %j, align 4, !tbaa !6
  %624 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub631 = sub nsw i32 1, %624
  %shl632 = shl i32 %623, %sub631
  %add.ptr633 = getelementptr inbounds i32, i32* %add.ptr630, i32 %shl632
  %625 = load i32*, i32** %cr_grain_block, align 4, !tbaa !2
  %626 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %627 = load i32, i32* %i507, align 4, !tbaa !6
  %628 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub634 = sub nsw i32 1, %628
  %shl635 = shl i32 %627, %sub634
  %add636 = add nsw i32 %626, %shl635
  %629 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul637 = mul nsw i32 %add636, %629
  %add.ptr638 = getelementptr inbounds i32, i32* %625, i32 %mul637
  %630 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr639 = getelementptr inbounds i32, i32* %add.ptr638, i32 %630
  %631 = load i32, i32* %j, align 4, !tbaa !6
  %632 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub640 = sub nsw i32 1, %632
  %shl641 = shl i32 %631, %sub640
  %add.ptr642 = getelementptr inbounds i32, i32* %add.ptr639, i32 %shl641
  %633 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %634 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %635 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shr643 = ashr i32 %635, 1
  %636 = load i32, i32* %height.addr, align 4, !tbaa !6
  %div644 = sdiv i32 %636, 2
  %637 = load i32, i32* %y, align 4, !tbaa !6
  %sub645 = sub nsw i32 %div644, %637
  %cmp646 = icmp slt i32 %shr643, %sub645
  br i1 %cmp646, label %cond.true647, label %cond.false649

cond.true647:                                     ; preds = %if.else592
  %638 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shr648 = ashr i32 %638, 1
  br label %cond.end652

cond.false649:                                    ; preds = %if.else592
  %639 = load i32, i32* %height.addr, align 4, !tbaa !6
  %div650 = sdiv i32 %639, 2
  %640 = load i32, i32* %y, align 4, !tbaa !6
  %sub651 = sub nsw i32 %div650, %640
  br label %cond.end652

cond.end652:                                      ; preds = %cond.false649, %cond.true647
  %cond653 = phi i32 [ %shr648, %cond.true647 ], [ %sub651, %cond.false649 ]
  %641 = load i32, i32* %i507, align 4, !tbaa !6
  %sub654 = sub nsw i32 %cond653, %641
  %642 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %shr655 = ashr i32 %642, 1
  %643 = load i32, i32* %width.addr, align 4, !tbaa !6
  %div656 = sdiv i32 %643, 2
  %644 = load i32, i32* %x, align 4, !tbaa !6
  %sub657 = sub nsw i32 %div656, %644
  %cmp658 = icmp slt i32 %shr655, %sub657
  br i1 %cmp658, label %cond.true659, label %cond.false661

cond.true659:                                     ; preds = %cond.end652
  %645 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %shr660 = ashr i32 %645, 1
  br label %cond.end664

cond.false661:                                    ; preds = %cond.end652
  %646 = load i32, i32* %width.addr, align 4, !tbaa !6
  %div662 = sdiv i32 %646, 2
  %647 = load i32, i32* %x, align 4, !tbaa !6
  %sub663 = sub nsw i32 %div662, %647
  br label %cond.end664

cond.end664:                                      ; preds = %cond.false661, %cond.true659
  %cond665 = phi i32 [ %shr660, %cond.true659 ], [ %sub663, %cond.false661 ]
  %648 = load i32, i32* %j, align 4, !tbaa !6
  %sub666 = sub nsw i32 %cond665, %648
  %649 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %650 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %651 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %652 = load i32, i32* %mc_identity.addr, align 4, !tbaa !6
  call void @add_noise_to_block(%struct.aom_film_grain_t* %586, i8* %add.ptr599, i8* %add.ptr608, i8* %add.ptr617, i32 %609, i32 %610, i32* %add.ptr624, i32* %add.ptr633, i32* %add.ptr642, i32 %633, i32 %634, i32 %sub654, i32 %sub666, i32 %649, i32 %650, i32 %651, i32 %652)
  br label %if.end667

if.end667:                                        ; preds = %cond.end664, %cond.end589
  %653 = load i32, i32* %overlap, align 4, !tbaa !6
  %tobool668 = icmp ne i32 %653, 0
  br i1 %tobool668, label %if.then669, label %if.end857

if.then669:                                       ; preds = %if.end667
  %654 = load i32, i32* %x, align 4, !tbaa !6
  %tobool670 = icmp ne i32 %654, 0
  br i1 %tobool670, label %if.then671, label %if.end694

if.then671:                                       ; preds = %if.then669
  %655 = load i32*, i32** %y_col_buf, align 4, !tbaa !2
  %656 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shl672 = shl i32 %656, 1
  %add.ptr673 = getelementptr inbounds i32, i32* %655, i32 %shl672
  %657 = load i32*, i32** %y_line_buf, align 4, !tbaa !2
  %658 = load i32, i32* %x, align 4, !tbaa !6
  %shl674 = shl i32 %658, 1
  %add.ptr675 = getelementptr inbounds i32, i32* %657, i32 %shl674
  %659 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  call void @copy_area(i32* %add.ptr673, i32 2, i32* %add.ptr675, i32 %659, i32 2, i32 2)
  %660 = load i32*, i32** %cb_col_buf, align 4, !tbaa !2
  %661 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %662 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub676 = sub nsw i32 1, %662
  %shl677 = shl i32 %661, %sub676
  %add.ptr678 = getelementptr inbounds i32, i32* %660, i32 %shl677
  %663 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr679 = ashr i32 2, %663
  %664 = load i32*, i32** %cb_line_buf, align 4, !tbaa !2
  %665 = load i32, i32* %x, align 4, !tbaa !6
  %666 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub680 = sub nsw i32 1, %666
  %shl681 = shl i32 %665, %sub680
  %add.ptr682 = getelementptr inbounds i32, i32* %664, i32 %shl681
  %667 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %668 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr683 = ashr i32 2, %668
  %669 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr684 = ashr i32 2, %669
  call void @copy_area(i32* %add.ptr678, i32 %shr679, i32* %add.ptr682, i32 %667, i32 %shr683, i32 %shr684)
  %670 = load i32*, i32** %cr_col_buf, align 4, !tbaa !2
  %671 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %672 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub685 = sub nsw i32 1, %672
  %shl686 = shl i32 %671, %sub685
  %add.ptr687 = getelementptr inbounds i32, i32* %670, i32 %shl686
  %673 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr688 = ashr i32 2, %673
  %674 = load i32*, i32** %cr_line_buf, align 4, !tbaa !2
  %675 = load i32, i32* %x, align 4, !tbaa !6
  %676 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub689 = sub nsw i32 1, %676
  %shl690 = shl i32 %675, %sub689
  %add.ptr691 = getelementptr inbounds i32, i32* %674, i32 %shl690
  %677 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %678 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr692 = ashr i32 2, %678
  %679 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr693 = ashr i32 2, %679
  call void @copy_area(i32* %add.ptr687, i32 %shr688, i32* %add.ptr691, i32 %677, i32 %shr692, i32 %shr693)
  br label %if.end694

if.end694:                                        ; preds = %if.then671, %if.then669
  %680 = load i32*, i32** %luma_grain_block, align 4, !tbaa !2
  %681 = load i32, i32* %luma_offset_y, align 4, !tbaa !6
  %682 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %add695 = add nsw i32 %681, %682
  %683 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %mul696 = mul nsw i32 %add695, %683
  %add.ptr697 = getelementptr inbounds i32, i32* %680, i32 %mul696
  %684 = load i32, i32* %luma_offset_x, align 4, !tbaa !6
  %add.ptr698 = getelementptr inbounds i32, i32* %add.ptr697, i32 %684
  %685 = load i32, i32* %x, align 4, !tbaa !6
  %tobool699 = icmp ne i32 %685, 0
  %686 = zext i1 %tobool699 to i64
  %cond700 = select i1 %tobool699, i32 2, i32 0
  %add.ptr701 = getelementptr inbounds i32, i32* %add.ptr698, i32 %cond700
  %687 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %688 = load i32*, i32** %y_line_buf, align 4, !tbaa !2
  %689 = load i32, i32* %x, align 4, !tbaa !6
  %tobool702 = icmp ne i32 %689, 0
  br i1 %tobool702, label %cond.true703, label %cond.false705

cond.true703:                                     ; preds = %if.end694
  %690 = load i32, i32* %x, align 4, !tbaa !6
  %add704 = add nsw i32 %690, 1
  br label %cond.end706

cond.false705:                                    ; preds = %if.end694
  br label %cond.end706

cond.end706:                                      ; preds = %cond.false705, %cond.true703
  %cond707 = phi i32 [ %add704, %cond.true703 ], [ 0, %cond.false705 ]
  %shl708 = shl i32 %cond707, 1
  %add.ptr709 = getelementptr inbounds i32, i32* %688, i32 %shl708
  %691 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %692 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %693 = load i32, i32* %width.addr, align 4, !tbaa !6
  %694 = load i32, i32* %x, align 4, !tbaa !6
  %shl710 = shl i32 %694, 1
  %sub711 = sub nsw i32 %693, %shl710
  %cmp712 = icmp slt i32 %692, %sub711
  br i1 %cmp712, label %cond.true713, label %cond.false714

cond.true713:                                     ; preds = %cond.end706
  %695 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  br label %cond.end717

cond.false714:                                    ; preds = %cond.end706
  %696 = load i32, i32* %width.addr, align 4, !tbaa !6
  %697 = load i32, i32* %x, align 4, !tbaa !6
  %shl715 = shl i32 %697, 1
  %sub716 = sub nsw i32 %696, %shl715
  br label %cond.end717

cond.end717:                                      ; preds = %cond.false714, %cond.true713
  %cond718 = phi i32 [ %695, %cond.true713 ], [ %sub716, %cond.false714 ]
  %698 = load i32, i32* %x, align 4, !tbaa !6
  %tobool719 = icmp ne i32 %698, 0
  %699 = zext i1 %tobool719 to i64
  %cond720 = select i1 %tobool719, i32 2, i32 0
  %sub721 = sub nsw i32 %cond718, %cond720
  call void @copy_area(i32* %add.ptr701, i32 %687, i32* %add.ptr709, i32 %691, i32 %sub721, i32 2)
  %700 = load i32*, i32** %cb_grain_block, align 4, !tbaa !2
  %701 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %702 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %add722 = add nsw i32 %701, %702
  %703 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul723 = mul nsw i32 %add722, %703
  %add.ptr724 = getelementptr inbounds i32, i32* %700, i32 %mul723
  %704 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr725 = getelementptr inbounds i32, i32* %add.ptr724, i32 %704
  %705 = load i32, i32* %x, align 4, !tbaa !6
  %tobool726 = icmp ne i32 %705, 0
  br i1 %tobool726, label %cond.true727, label %cond.false729

cond.true727:                                     ; preds = %cond.end717
  %706 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr728 = ashr i32 2, %706
  br label %cond.end730

cond.false729:                                    ; preds = %cond.end717
  br label %cond.end730

cond.end730:                                      ; preds = %cond.false729, %cond.true727
  %cond731 = phi i32 [ %shr728, %cond.true727 ], [ 0, %cond.false729 ]
  %add.ptr732 = getelementptr inbounds i32, i32* %add.ptr725, i32 %cond731
  %707 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %708 = load i32*, i32** %cb_line_buf, align 4, !tbaa !2
  %709 = load i32, i32* %x, align 4, !tbaa !6
  %tobool733 = icmp ne i32 %709, 0
  br i1 %tobool733, label %cond.true734, label %cond.false736

cond.true734:                                     ; preds = %cond.end730
  %710 = load i32, i32* %x, align 4, !tbaa !6
  %add735 = add nsw i32 %710, 1
  br label %cond.end737

cond.false736:                                    ; preds = %cond.end730
  br label %cond.end737

cond.end737:                                      ; preds = %cond.false736, %cond.true734
  %cond738 = phi i32 [ %add735, %cond.true734 ], [ 0, %cond.false736 ]
  %711 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub739 = sub nsw i32 1, %711
  %shl740 = shl i32 %cond738, %sub739
  %add.ptr741 = getelementptr inbounds i32, i32* %708, i32 %shl740
  %712 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %713 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %714 = load i32, i32* %width.addr, align 4, !tbaa !6
  %715 = load i32, i32* %x, align 4, !tbaa !6
  %shl742 = shl i32 %715, 1
  %sub743 = sub nsw i32 %714, %shl742
  %716 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr744 = ashr i32 %sub743, %716
  %cmp745 = icmp slt i32 %713, %shr744
  br i1 %cmp745, label %cond.true746, label %cond.false747

cond.true746:                                     ; preds = %cond.end737
  %717 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  br label %cond.end751

cond.false747:                                    ; preds = %cond.end737
  %718 = load i32, i32* %width.addr, align 4, !tbaa !6
  %719 = load i32, i32* %x, align 4, !tbaa !6
  %shl748 = shl i32 %719, 1
  %sub749 = sub nsw i32 %718, %shl748
  %720 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr750 = ashr i32 %sub749, %720
  br label %cond.end751

cond.end751:                                      ; preds = %cond.false747, %cond.true746
  %cond752 = phi i32 [ %717, %cond.true746 ], [ %shr750, %cond.false747 ]
  %721 = load i32, i32* %x, align 4, !tbaa !6
  %tobool753 = icmp ne i32 %721, 0
  br i1 %tobool753, label %cond.true754, label %cond.false756

cond.true754:                                     ; preds = %cond.end751
  %722 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr755 = ashr i32 2, %722
  br label %cond.end757

cond.false756:                                    ; preds = %cond.end751
  br label %cond.end757

cond.end757:                                      ; preds = %cond.false756, %cond.true754
  %cond758 = phi i32 [ %shr755, %cond.true754 ], [ 0, %cond.false756 ]
  %sub759 = sub nsw i32 %cond752, %cond758
  %723 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr760 = ashr i32 2, %723
  call void @copy_area(i32* %add.ptr732, i32 %707, i32* %add.ptr741, i32 %712, i32 %sub759, i32 %shr760)
  %724 = load i32*, i32** %cr_grain_block, align 4, !tbaa !2
  %725 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %726 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %add761 = add nsw i32 %725, %726
  %727 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul762 = mul nsw i32 %add761, %727
  %add.ptr763 = getelementptr inbounds i32, i32* %724, i32 %mul762
  %728 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr764 = getelementptr inbounds i32, i32* %add.ptr763, i32 %728
  %729 = load i32, i32* %x, align 4, !tbaa !6
  %tobool765 = icmp ne i32 %729, 0
  br i1 %tobool765, label %cond.true766, label %cond.false768

cond.true766:                                     ; preds = %cond.end757
  %730 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr767 = ashr i32 2, %730
  br label %cond.end769

cond.false768:                                    ; preds = %cond.end757
  br label %cond.end769

cond.end769:                                      ; preds = %cond.false768, %cond.true766
  %cond770 = phi i32 [ %shr767, %cond.true766 ], [ 0, %cond.false768 ]
  %add.ptr771 = getelementptr inbounds i32, i32* %add.ptr764, i32 %cond770
  %731 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %732 = load i32*, i32** %cr_line_buf, align 4, !tbaa !2
  %733 = load i32, i32* %x, align 4, !tbaa !6
  %tobool772 = icmp ne i32 %733, 0
  br i1 %tobool772, label %cond.true773, label %cond.false775

cond.true773:                                     ; preds = %cond.end769
  %734 = load i32, i32* %x, align 4, !tbaa !6
  %add774 = add nsw i32 %734, 1
  br label %cond.end776

cond.false775:                                    ; preds = %cond.end769
  br label %cond.end776

cond.end776:                                      ; preds = %cond.false775, %cond.true773
  %cond777 = phi i32 [ %add774, %cond.true773 ], [ 0, %cond.false775 ]
  %735 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub778 = sub nsw i32 1, %735
  %shl779 = shl i32 %cond777, %sub778
  %add.ptr780 = getelementptr inbounds i32, i32* %732, i32 %shl779
  %736 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %737 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %738 = load i32, i32* %width.addr, align 4, !tbaa !6
  %739 = load i32, i32* %x, align 4, !tbaa !6
  %shl781 = shl i32 %739, 1
  %sub782 = sub nsw i32 %738, %shl781
  %740 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr783 = ashr i32 %sub782, %740
  %cmp784 = icmp slt i32 %737, %shr783
  br i1 %cmp784, label %cond.true785, label %cond.false786

cond.true785:                                     ; preds = %cond.end776
  %741 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  br label %cond.end790

cond.false786:                                    ; preds = %cond.end776
  %742 = load i32, i32* %width.addr, align 4, !tbaa !6
  %743 = load i32, i32* %x, align 4, !tbaa !6
  %shl787 = shl i32 %743, 1
  %sub788 = sub nsw i32 %742, %shl787
  %744 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr789 = ashr i32 %sub788, %744
  br label %cond.end790

cond.end790:                                      ; preds = %cond.false786, %cond.true785
  %cond791 = phi i32 [ %741, %cond.true785 ], [ %shr789, %cond.false786 ]
  %745 = load i32, i32* %x, align 4, !tbaa !6
  %tobool792 = icmp ne i32 %745, 0
  br i1 %tobool792, label %cond.true793, label %cond.false795

cond.true793:                                     ; preds = %cond.end790
  %746 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr794 = ashr i32 2, %746
  br label %cond.end796

cond.false795:                                    ; preds = %cond.end790
  br label %cond.end796

cond.end796:                                      ; preds = %cond.false795, %cond.true793
  %cond797 = phi i32 [ %shr794, %cond.true793 ], [ 0, %cond.false795 ]
  %sub798 = sub nsw i32 %cond791, %cond797
  %747 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr799 = ashr i32 2, %747
  call void @copy_area(i32* %add.ptr771, i32 %731, i32* %add.ptr780, i32 %736, i32 %sub798, i32 %shr799)
  %748 = load i32*, i32** %luma_grain_block, align 4, !tbaa !2
  %749 = load i32, i32* %luma_offset_y, align 4, !tbaa !6
  %750 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %mul800 = mul nsw i32 %749, %750
  %add.ptr801 = getelementptr inbounds i32, i32* %748, i32 %mul800
  %751 = load i32, i32* %luma_offset_x, align 4, !tbaa !6
  %add.ptr802 = getelementptr inbounds i32, i32* %add.ptr801, i32 %751
  %752 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %add.ptr803 = getelementptr inbounds i32, i32* %add.ptr802, i32 %752
  %753 = load i32, i32* %luma_grain_stride, align 4, !tbaa !6
  %754 = load i32*, i32** %y_col_buf, align 4, !tbaa !2
  %755 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %add804 = add nsw i32 %755, 2
  %756 = load i32, i32* %height.addr, align 4, !tbaa !6
  %757 = load i32, i32* %y, align 4, !tbaa !6
  %shl805 = shl i32 %757, 1
  %sub806 = sub nsw i32 %756, %shl805
  %cmp807 = icmp slt i32 %add804, %sub806
  br i1 %cmp807, label %cond.true808, label %cond.false810

cond.true808:                                     ; preds = %cond.end796
  %758 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %add809 = add nsw i32 %758, 2
  br label %cond.end813

cond.false810:                                    ; preds = %cond.end796
  %759 = load i32, i32* %height.addr, align 4, !tbaa !6
  %760 = load i32, i32* %y, align 4, !tbaa !6
  %shl811 = shl i32 %760, 1
  %sub812 = sub nsw i32 %759, %shl811
  br label %cond.end813

cond.end813:                                      ; preds = %cond.false810, %cond.true808
  %cond814 = phi i32 [ %add809, %cond.true808 ], [ %sub812, %cond.false810 ]
  call void @copy_area(i32* %add.ptr803, i32 %753, i32* %754, i32 2, i32 2, i32 %cond814)
  %761 = load i32*, i32** %cb_grain_block, align 4, !tbaa !2
  %762 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %763 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul815 = mul nsw i32 %762, %763
  %add.ptr816 = getelementptr inbounds i32, i32* %761, i32 %mul815
  %764 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr817 = getelementptr inbounds i32, i32* %add.ptr816, i32 %764
  %765 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %add.ptr818 = getelementptr inbounds i32, i32* %add.ptr817, i32 %765
  %766 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %767 = load i32*, i32** %cb_col_buf, align 4, !tbaa !2
  %768 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr819 = ashr i32 2, %768
  %769 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr820 = ashr i32 2, %769
  %770 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %771 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr821 = ashr i32 2, %771
  %add822 = add nsw i32 %770, %shr821
  %772 = load i32, i32* %height.addr, align 4, !tbaa !6
  %773 = load i32, i32* %y, align 4, !tbaa !6
  %shl823 = shl i32 %773, 1
  %sub824 = sub nsw i32 %772, %shl823
  %774 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr825 = ashr i32 %sub824, %774
  %cmp826 = icmp slt i32 %add822, %shr825
  br i1 %cmp826, label %cond.true827, label %cond.false830

cond.true827:                                     ; preds = %cond.end813
  %775 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %776 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr828 = ashr i32 2, %776
  %add829 = add nsw i32 %775, %shr828
  br label %cond.end834

cond.false830:                                    ; preds = %cond.end813
  %777 = load i32, i32* %height.addr, align 4, !tbaa !6
  %778 = load i32, i32* %y, align 4, !tbaa !6
  %shl831 = shl i32 %778, 1
  %sub832 = sub nsw i32 %777, %shl831
  %779 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr833 = ashr i32 %sub832, %779
  br label %cond.end834

cond.end834:                                      ; preds = %cond.false830, %cond.true827
  %cond835 = phi i32 [ %add829, %cond.true827 ], [ %shr833, %cond.false830 ]
  call void @copy_area(i32* %add.ptr818, i32 %766, i32* %767, i32 %shr819, i32 %shr820, i32 %cond835)
  %780 = load i32*, i32** %cr_grain_block, align 4, !tbaa !2
  %781 = load i32, i32* %chroma_offset_y, align 4, !tbaa !6
  %782 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %mul836 = mul nsw i32 %781, %782
  %add.ptr837 = getelementptr inbounds i32, i32* %780, i32 %mul836
  %783 = load i32, i32* %chroma_offset_x, align 4, !tbaa !6
  %add.ptr838 = getelementptr inbounds i32, i32* %add.ptr837, i32 %783
  %784 = load i32, i32* @chroma_subblock_size_x, align 4, !tbaa !6
  %add.ptr839 = getelementptr inbounds i32, i32* %add.ptr838, i32 %784
  %785 = load i32, i32* %chroma_grain_stride, align 4, !tbaa !6
  %786 = load i32*, i32** %cr_col_buf, align 4, !tbaa !2
  %787 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr840 = ashr i32 2, %787
  %788 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr841 = ashr i32 2, %788
  %789 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %790 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr842 = ashr i32 2, %790
  %add843 = add nsw i32 %789, %shr842
  %791 = load i32, i32* %height.addr, align 4, !tbaa !6
  %792 = load i32, i32* %y, align 4, !tbaa !6
  %shl844 = shl i32 %792, 1
  %sub845 = sub nsw i32 %791, %shl844
  %793 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr846 = ashr i32 %sub845, %793
  %cmp847 = icmp slt i32 %add843, %shr846
  br i1 %cmp847, label %cond.true848, label %cond.false851

cond.true848:                                     ; preds = %cond.end834
  %794 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %795 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr849 = ashr i32 2, %795
  %add850 = add nsw i32 %794, %shr849
  br label %cond.end855

cond.false851:                                    ; preds = %cond.end834
  %796 = load i32, i32* %height.addr, align 4, !tbaa !6
  %797 = load i32, i32* %y, align 4, !tbaa !6
  %shl852 = shl i32 %797, 1
  %sub853 = sub nsw i32 %796, %shl852
  %798 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr854 = ashr i32 %sub853, %798
  br label %cond.end855

cond.end855:                                      ; preds = %cond.false851, %cond.true848
  %cond856 = phi i32 [ %add850, %cond.true848 ], [ %shr854, %cond.false851 ]
  call void @copy_area(i32* %add.ptr839, i32 %785, i32* %786, i32 %shr840, i32 %shr841, i32 %cond856)
  br label %if.end857

if.end857:                                        ; preds = %cond.end855, %if.end667
  %799 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %799) #5
  %800 = bitcast i32* %i507 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %800) #5
  %801 = bitcast i32* %chroma_offset_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %801) #5
  %802 = bitcast i32* %chroma_offset_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %802) #5
  %803 = bitcast i32* %luma_offset_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %803) #5
  %804 = bitcast i32* %luma_offset_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %804) #5
  %805 = bitcast i32* %offset_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %805) #5
  %806 = bitcast i32* %offset_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %806) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end857
  %807 = load i32, i32* @luma_subblock_size_x, align 4, !tbaa !6
  %shr858 = ashr i32 %807, 1
  %808 = load i32, i32* %x, align 4, !tbaa !6
  %add859 = add nsw i32 %808, %shr858
  store i32 %add859, i32* %x, align 4, !tbaa !6
  br label %for.cond43

for.end:                                          ; preds = %for.cond.cleanup46
  br label %for.inc860

for.inc860:                                       ; preds = %for.end
  %809 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %shr861 = ashr i32 %809, 1
  %810 = load i32, i32* %y, align 4, !tbaa !6
  %add862 = add nsw i32 %810, %shr861
  store i32 %add862, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end863:                                       ; preds = %for.cond.cleanup
  %811 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  call void @dealloc_arrays(%struct.aom_film_grain_t* %811, i32*** %pred_pos_luma, i32*** %pred_pos_chroma, i32** %luma_grain_block, i32** %cb_grain_block, i32** %cr_grain_block, i32** %y_line_buf, i32** %cb_line_buf, i32** %cr_line_buf, i32** %y_col_buf, i32** %cb_col_buf, i32** %cr_col_buf)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end863, %if.then34, %if.then
  %812 = bitcast i32* %grain_center to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %812) #5
  %813 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %813) #5
  %814 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %814) #5
  %815 = bitcast i32* %chroma_grain_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %815) #5
  %816 = bitcast i32* %luma_grain_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %816) #5
  %817 = bitcast i32* %chroma_block_size_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %817) #5
  %818 = bitcast i32* %chroma_block_size_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %818) #5
  %819 = bitcast i32* %luma_block_size_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %819) #5
  %820 = bitcast i32* %luma_block_size_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %820) #5
  %821 = bitcast i32* %ar_padding to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %821) #5
  %822 = bitcast i32* %bottom_pad to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %822) #5
  %823 = bitcast i32* %top_pad to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %823) #5
  %824 = bitcast i32* %right_pad to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %824) #5
  %825 = bitcast i32* %left_pad to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %825) #5
  %826 = bitcast i32** %cr_col_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %826) #5
  %827 = bitcast i32** %cb_col_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %827) #5
  %828 = bitcast i32** %y_col_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %828) #5
  %829 = bitcast i32** %cr_line_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %829) #5
  %830 = bitcast i32** %cb_line_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %830) #5
  %831 = bitcast i32** %y_line_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %831) #5
  %832 = bitcast i32** %cr_grain_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %832) #5
  %833 = bitcast i32** %cb_grain_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %833) #5
  %834 = bitcast i32** %luma_grain_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %834) #5
  %835 = bitcast i32*** %pred_pos_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %835) #5
  %836 = bitcast i32*** %pred_pos_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %836) #5
  %837 = load i32, i32* %retval, align 4
  ret i32 %837
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @init_arrays(%struct.aom_film_grain_t* %params, i32 %luma_stride, i32 %chroma_stride, i32*** %pred_pos_luma_p, i32*** %pred_pos_chroma_p, i32** %luma_grain_block, i32** %cb_grain_block, i32** %cr_grain_block, i32** %y_line_buf, i32** %cb_line_buf, i32** %cr_line_buf, i32** %y_col_buf, i32** %cb_col_buf, i32** %cr_col_buf, i32 %luma_grain_samples, i32 %chroma_grain_samples, i32 %chroma_subsamp_y, i32 %chroma_subsamp_x) #0 {
entry:
  %params.addr = alloca %struct.aom_film_grain_t*, align 4
  %luma_stride.addr = alloca i32, align 4
  %chroma_stride.addr = alloca i32, align 4
  %pred_pos_luma_p.addr = alloca i32***, align 4
  %pred_pos_chroma_p.addr = alloca i32***, align 4
  %luma_grain_block.addr = alloca i32**, align 4
  %cb_grain_block.addr = alloca i32**, align 4
  %cr_grain_block.addr = alloca i32**, align 4
  %y_line_buf.addr = alloca i32**, align 4
  %cb_line_buf.addr = alloca i32**, align 4
  %cr_line_buf.addr = alloca i32**, align 4
  %y_col_buf.addr = alloca i32**, align 4
  %cb_col_buf.addr = alloca i32**, align 4
  %cr_col_buf.addr = alloca i32**, align 4
  %luma_grain_samples.addr = alloca i32, align 4
  %chroma_grain_samples.addr = alloca i32, align 4
  %chroma_subsamp_y.addr = alloca i32, align 4
  %chroma_subsamp_x.addr = alloca i32, align 4
  %num_pos_luma = alloca i32, align 4
  %num_pos_chroma = alloca i32, align 4
  %pred_pos_luma = alloca i32**, align 4
  %pred_pos_chroma = alloca i32**, align 4
  %row = alloca i32, align 4
  %row9 = alloca i32, align 4
  %pos_ar_index = alloca i32, align 4
  %row19 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %col = alloca i32, align 4
  %col52 = alloca i32, align 4
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  store i32 %luma_stride, i32* %luma_stride.addr, align 4, !tbaa !6
  store i32 %chroma_stride, i32* %chroma_stride.addr, align 4, !tbaa !6
  store i32*** %pred_pos_luma_p, i32**** %pred_pos_luma_p.addr, align 4, !tbaa !2
  store i32*** %pred_pos_chroma_p, i32**** %pred_pos_chroma_p.addr, align 4, !tbaa !2
  store i32** %luma_grain_block, i32*** %luma_grain_block.addr, align 4, !tbaa !2
  store i32** %cb_grain_block, i32*** %cb_grain_block.addr, align 4, !tbaa !2
  store i32** %cr_grain_block, i32*** %cr_grain_block.addr, align 4, !tbaa !2
  store i32** %y_line_buf, i32*** %y_line_buf.addr, align 4, !tbaa !2
  store i32** %cb_line_buf, i32*** %cb_line_buf.addr, align 4, !tbaa !2
  store i32** %cr_line_buf, i32*** %cr_line_buf.addr, align 4, !tbaa !2
  store i32** %y_col_buf, i32*** %y_col_buf.addr, align 4, !tbaa !2
  store i32** %cb_col_buf, i32*** %cb_col_buf.addr, align 4, !tbaa !2
  store i32** %cr_col_buf, i32*** %cr_col_buf.addr, align 4, !tbaa !2
  store i32 %luma_grain_samples, i32* %luma_grain_samples.addr, align 4, !tbaa !6
  store i32 %chroma_grain_samples, i32* %chroma_grain_samples.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_y, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_x, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 16 bitcast ([256 x i32]* @scaling_lut_y to i8*), i8 0, i32 1024, i1 false)
  call void @llvm.memset.p0i8.i32(i8* align 16 bitcast ([256 x i32]* @scaling_lut_cb to i8*), i8 0, i32 1024, i1 false)
  call void @llvm.memset.p0i8.i32(i8* align 16 bitcast ([256 x i32]* @scaling_lut_cr to i8*), i8 0, i32 1024, i1 false)
  %0 = bitcast i32* %num_pos_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %1, i32 0, i32 9
  %2 = load i32, i32* %ar_coeff_lag, align 4, !tbaa !37
  %mul = mul nsw i32 2, %2
  %3 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag1 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %3, i32 0, i32 9
  %4 = load i32, i32* %ar_coeff_lag1, align 4, !tbaa !37
  %add = add nsw i32 %4, 1
  %mul2 = mul nsw i32 %mul, %add
  store i32 %mul2, i32* %num_pos_luma, align 4, !tbaa !6
  %5 = bitcast i32* %num_pos_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %num_pos_luma, align 4, !tbaa !6
  store i32 %6, i32* %num_pos_chroma, align 4, !tbaa !6
  %7 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %7, i32 0, i32 3
  %8 = load i32, i32* %num_y_points, align 4, !tbaa !33
  %cmp = icmp sgt i32 %8, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %9 = load i32, i32* %num_pos_chroma, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %num_pos_chroma, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = bitcast i32*** %pred_pos_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = bitcast i32*** %pred_pos_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %num_pos_luma, align 4, !tbaa !6
  %mul3 = mul i32 4, %12
  %call = call i8* @aom_malloc(i32 %mul3)
  %13 = bitcast i8* %call to i32**
  store i32** %13, i32*** %pred_pos_luma, align 4, !tbaa !2
  %14 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %15 = load i32, i32* %row, align 4, !tbaa !6
  %16 = load i32, i32* %num_pos_luma, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %15, %16
  br i1 %cmp4, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %17 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %call5 = call i8* @aom_malloc(i32 12)
  %18 = bitcast i8* %call5 to i32*
  %19 = load i32**, i32*** %pred_pos_luma, align 4, !tbaa !2
  %20 = load i32, i32* %row, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32*, i32** %19, i32 %20
  store i32* %18, i32** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %row, align 4, !tbaa !6
  %inc6 = add nsw i32 %21, 1
  store i32 %inc6, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %22 = load i32, i32* %num_pos_chroma, align 4, !tbaa !6
  %mul7 = mul i32 4, %22
  %call8 = call i8* @aom_malloc(i32 %mul7)
  %23 = bitcast i8* %call8 to i32**
  store i32** %23, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %24 = bitcast i32* %row9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  store i32 0, i32* %row9, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc16, %for.end
  %25 = load i32, i32* %row9, align 4, !tbaa !6
  %26 = load i32, i32* %num_pos_chroma, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %25, %26
  br i1 %cmp11, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond10
  %27 = bitcast i32* %row9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  br label %for.end18

for.body13:                                       ; preds = %for.cond10
  %call14 = call i8* @aom_malloc(i32 12)
  %28 = bitcast i8* %call14 to i32*
  %29 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %30 = load i32, i32* %row9, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds i32*, i32** %29, i32 %30
  store i32* %28, i32** %arrayidx15, align 4, !tbaa !2
  br label %for.inc16

for.inc16:                                        ; preds = %for.body13
  %31 = load i32, i32* %row9, align 4, !tbaa !6
  %inc17 = add nsw i32 %31, 1
  store i32 %inc17, i32* %row9, align 4, !tbaa !6
  br label %for.cond10

for.end18:                                        ; preds = %for.cond.cleanup12
  %32 = bitcast i32* %pos_ar_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  store i32 0, i32* %pos_ar_index, align 4, !tbaa !6
  %33 = bitcast i32* %row19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #5
  %34 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag20 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %34, i32 0, i32 9
  %35 = load i32, i32* %ar_coeff_lag20, align 4, !tbaa !37
  %sub = sub nsw i32 0, %35
  store i32 %sub, i32* %row19, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc49, %for.end18
  %36 = load i32, i32* %row19, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %36, 0
  br i1 %cmp22, label %for.body24, label %for.cond.cleanup23

for.cond.cleanup23:                               ; preds = %for.cond21
  store i32 8, i32* %cleanup.dest.slot, align 4
  %37 = bitcast i32* %row19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  br label %for.end51

for.body24:                                       ; preds = %for.cond21
  %38 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #5
  %39 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag25 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %39, i32 0, i32 9
  %40 = load i32, i32* %ar_coeff_lag25, align 4, !tbaa !37
  %sub26 = sub nsw i32 0, %40
  store i32 %sub26, i32* %col, align 4, !tbaa !6
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc46, %for.body24
  %41 = load i32, i32* %col, align 4, !tbaa !6
  %42 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag28 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %42, i32 0, i32 9
  %43 = load i32, i32* %ar_coeff_lag28, align 4, !tbaa !37
  %add29 = add nsw i32 %43, 1
  %cmp30 = icmp slt i32 %41, %add29
  br i1 %cmp30, label %for.body32, label %for.cond.cleanup31

for.cond.cleanup31:                               ; preds = %for.cond27
  store i32 11, i32* %cleanup.dest.slot, align 4
  %44 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #5
  br label %for.end48

for.body32:                                       ; preds = %for.cond27
  %45 = load i32, i32* %row19, align 4, !tbaa !6
  %46 = load i32**, i32*** %pred_pos_luma, align 4, !tbaa !2
  %47 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds i32*, i32** %46, i32 %47
  %48 = load i32*, i32** %arrayidx33, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %48, i32 0
  store i32 %45, i32* %arrayidx34, align 4, !tbaa !6
  %49 = load i32, i32* %col, align 4, !tbaa !6
  %50 = load i32**, i32*** %pred_pos_luma, align 4, !tbaa !2
  %51 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds i32*, i32** %50, i32 %51
  %52 = load i32*, i32** %arrayidx35, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %52, i32 1
  store i32 %49, i32* %arrayidx36, align 4, !tbaa !6
  %53 = load i32**, i32*** %pred_pos_luma, align 4, !tbaa !2
  %54 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds i32*, i32** %53, i32 %54
  %55 = load i32*, i32** %arrayidx37, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i32, i32* %55, i32 2
  store i32 0, i32* %arrayidx38, align 4, !tbaa !6
  %56 = load i32, i32* %row19, align 4, !tbaa !6
  %57 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %58 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds i32*, i32** %57, i32 %58
  %59 = load i32*, i32** %arrayidx39, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %59, i32 0
  store i32 %56, i32* %arrayidx40, align 4, !tbaa !6
  %60 = load i32, i32* %col, align 4, !tbaa !6
  %61 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %62 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds i32*, i32** %61, i32 %62
  %63 = load i32*, i32** %arrayidx41, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %63, i32 1
  store i32 %60, i32* %arrayidx42, align 4, !tbaa !6
  %64 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %65 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds i32*, i32** %64, i32 %65
  %66 = load i32*, i32** %arrayidx43, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %66, i32 2
  store i32 0, i32* %arrayidx44, align 4, !tbaa !6
  %67 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %inc45 = add nsw i32 %67, 1
  store i32 %inc45, i32* %pos_ar_index, align 4, !tbaa !6
  br label %for.inc46

for.inc46:                                        ; preds = %for.body32
  %68 = load i32, i32* %col, align 4, !tbaa !6
  %inc47 = add nsw i32 %68, 1
  store i32 %inc47, i32* %col, align 4, !tbaa !6
  br label %for.cond27

for.end48:                                        ; preds = %for.cond.cleanup31
  br label %for.inc49

for.inc49:                                        ; preds = %for.end48
  %69 = load i32, i32* %row19, align 4, !tbaa !6
  %inc50 = add nsw i32 %69, 1
  store i32 %inc50, i32* %row19, align 4, !tbaa !6
  br label %for.cond21

for.end51:                                        ; preds = %for.cond.cleanup23
  %70 = bitcast i32* %col52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #5
  %71 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag53 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %71, i32 0, i32 9
  %72 = load i32, i32* %ar_coeff_lag53, align 4, !tbaa !37
  %sub54 = sub nsw i32 0, %72
  store i32 %sub54, i32* %col52, align 4, !tbaa !6
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc72, %for.end51
  %73 = load i32, i32* %col52, align 4, !tbaa !6
  %cmp56 = icmp slt i32 %73, 0
  br i1 %cmp56, label %for.body58, label %for.cond.cleanup57

for.cond.cleanup57:                               ; preds = %for.cond55
  store i32 14, i32* %cleanup.dest.slot, align 4
  %74 = bitcast i32* %col52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  br label %for.end74

for.body58:                                       ; preds = %for.cond55
  %75 = load i32**, i32*** %pred_pos_luma, align 4, !tbaa !2
  %76 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds i32*, i32** %75, i32 %76
  %77 = load i32*, i32** %arrayidx59, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %77, i32 0
  store i32 0, i32* %arrayidx60, align 4, !tbaa !6
  %78 = load i32, i32* %col52, align 4, !tbaa !6
  %79 = load i32**, i32*** %pred_pos_luma, align 4, !tbaa !2
  %80 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds i32*, i32** %79, i32 %80
  %81 = load i32*, i32** %arrayidx61, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i32, i32* %81, i32 1
  store i32 %78, i32* %arrayidx62, align 4, !tbaa !6
  %82 = load i32**, i32*** %pred_pos_luma, align 4, !tbaa !2
  %83 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds i32*, i32** %82, i32 %83
  %84 = load i32*, i32** %arrayidx63, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %84, i32 2
  store i32 0, i32* %arrayidx64, align 4, !tbaa !6
  %85 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %86 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds i32*, i32** %85, i32 %86
  %87 = load i32*, i32** %arrayidx65, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i32, i32* %87, i32 0
  store i32 0, i32* %arrayidx66, align 4, !tbaa !6
  %88 = load i32, i32* %col52, align 4, !tbaa !6
  %89 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %90 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds i32*, i32** %89, i32 %90
  %91 = load i32*, i32** %arrayidx67, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i32, i32* %91, i32 1
  store i32 %88, i32* %arrayidx68, align 4, !tbaa !6
  %92 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %93 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx69 = getelementptr inbounds i32*, i32** %92, i32 %93
  %94 = load i32*, i32** %arrayidx69, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds i32, i32* %94, i32 2
  store i32 0, i32* %arrayidx70, align 4, !tbaa !6
  %95 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %inc71 = add nsw i32 %95, 1
  store i32 %inc71, i32* %pos_ar_index, align 4, !tbaa !6
  br label %for.inc72

for.inc72:                                        ; preds = %for.body58
  %96 = load i32, i32* %col52, align 4, !tbaa !6
  %inc73 = add nsw i32 %96, 1
  store i32 %inc73, i32* %col52, align 4, !tbaa !6
  br label %for.cond55

for.end74:                                        ; preds = %for.cond.cleanup57
  %97 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_y_points75 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %97, i32 0, i32 3
  %98 = load i32, i32* %num_y_points75, align 4, !tbaa !33
  %cmp76 = icmp sgt i32 %98, 0
  br i1 %cmp76, label %if.then77, label %if.end84

if.then77:                                        ; preds = %for.end74
  %99 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %100 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx78 = getelementptr inbounds i32*, i32** %99, i32 %100
  %101 = load i32*, i32** %arrayidx78, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %101, i32 0
  store i32 0, i32* %arrayidx79, align 4, !tbaa !6
  %102 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %103 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx80 = getelementptr inbounds i32*, i32** %102, i32 %103
  %104 = load i32*, i32** %arrayidx80, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i32, i32* %104, i32 1
  store i32 0, i32* %arrayidx81, align 4, !tbaa !6
  %105 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %106 = load i32, i32* %pos_ar_index, align 4, !tbaa !6
  %arrayidx82 = getelementptr inbounds i32*, i32** %105, i32 %106
  %107 = load i32*, i32** %arrayidx82, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %107, i32 2
  store i32 1, i32* %arrayidx83, align 4, !tbaa !6
  br label %if.end84

if.end84:                                         ; preds = %if.then77, %for.end74
  %108 = load i32**, i32*** %pred_pos_luma, align 4, !tbaa !2
  %109 = load i32***, i32**** %pred_pos_luma_p.addr, align 4, !tbaa !2
  store i32** %108, i32*** %109, align 4, !tbaa !2
  %110 = load i32**, i32*** %pred_pos_chroma, align 4, !tbaa !2
  %111 = load i32***, i32**** %pred_pos_chroma_p.addr, align 4, !tbaa !2
  store i32** %110, i32*** %111, align 4, !tbaa !2
  %112 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul85 = mul i32 4, %112
  %mul86 = mul i32 %mul85, 2
  %call87 = call i8* @aom_malloc(i32 %mul86)
  %113 = bitcast i8* %call87 to i32*
  %114 = load i32**, i32*** %y_line_buf.addr, align 4, !tbaa !2
  store i32* %113, i32** %114, align 4, !tbaa !2
  %115 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul88 = mul i32 4, %115
  %116 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr = ashr i32 2, %116
  %mul89 = mul i32 %mul88, %shr
  %call90 = call i8* @aom_malloc(i32 %mul89)
  %117 = bitcast i8* %call90 to i32*
  %118 = load i32**, i32*** %cb_line_buf.addr, align 4, !tbaa !2
  store i32* %117, i32** %118, align 4, !tbaa !2
  %119 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul91 = mul i32 4, %119
  %120 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr92 = ashr i32 2, %120
  %mul93 = mul i32 %mul91, %shr92
  %call94 = call i8* @aom_malloc(i32 %mul93)
  %121 = bitcast i8* %call94 to i32*
  %122 = load i32**, i32*** %cr_line_buf.addr, align 4, !tbaa !2
  store i32* %121, i32** %122, align 4, !tbaa !2
  %123 = load i32, i32* @luma_subblock_size_y, align 4, !tbaa !6
  %add95 = add nsw i32 %123, 2
  %mul96 = mul i32 4, %add95
  %mul97 = mul i32 %mul96, 2
  %call98 = call i8* @aom_malloc(i32 %mul97)
  %124 = bitcast i8* %call98 to i32*
  %125 = load i32**, i32*** %y_col_buf.addr, align 4, !tbaa !2
  store i32* %124, i32** %125, align 4, !tbaa !2
  %126 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %127 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr99 = ashr i32 2, %127
  %add100 = add nsw i32 %126, %shr99
  %mul101 = mul i32 4, %add100
  %128 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr102 = ashr i32 2, %128
  %mul103 = mul i32 %mul101, %shr102
  %call104 = call i8* @aom_malloc(i32 %mul103)
  %129 = bitcast i8* %call104 to i32*
  %130 = load i32**, i32*** %cb_col_buf.addr, align 4, !tbaa !2
  store i32* %129, i32** %130, align 4, !tbaa !2
  %131 = load i32, i32* @chroma_subblock_size_y, align 4, !tbaa !6
  %132 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shr105 = ashr i32 2, %132
  %add106 = add nsw i32 %131, %shr105
  %mul107 = mul i32 4, %add106
  %133 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shr108 = ashr i32 2, %133
  %mul109 = mul i32 %mul107, %shr108
  %call110 = call i8* @aom_malloc(i32 %mul109)
  %134 = bitcast i8* %call110 to i32*
  %135 = load i32**, i32*** %cr_col_buf.addr, align 4, !tbaa !2
  store i32* %134, i32** %135, align 4, !tbaa !2
  %136 = load i32, i32* %luma_grain_samples.addr, align 4, !tbaa !6
  %mul111 = mul i32 4, %136
  %call112 = call i8* @aom_malloc(i32 %mul111)
  %137 = bitcast i8* %call112 to i32*
  %138 = load i32**, i32*** %luma_grain_block.addr, align 4, !tbaa !2
  store i32* %137, i32** %138, align 4, !tbaa !2
  %139 = load i32, i32* %chroma_grain_samples.addr, align 4, !tbaa !6
  %mul113 = mul i32 4, %139
  %call114 = call i8* @aom_malloc(i32 %mul113)
  %140 = bitcast i8* %call114 to i32*
  %141 = load i32**, i32*** %cb_grain_block.addr, align 4, !tbaa !2
  store i32* %140, i32** %141, align 4, !tbaa !2
  %142 = load i32, i32* %chroma_grain_samples.addr, align 4, !tbaa !6
  %mul115 = mul i32 4, %142
  %call116 = call i8* @aom_malloc(i32 %mul115)
  %143 = bitcast i8* %call116 to i32*
  %144 = load i32**, i32*** %cr_grain_block.addr, align 4, !tbaa !2
  store i32* %143, i32** %144, align 4, !tbaa !2
  %145 = bitcast i32* %pos_ar_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #5
  %146 = bitcast i32*** %pred_pos_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #5
  %147 = bitcast i32*** %pred_pos_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #5
  %148 = bitcast i32* %num_pos_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #5
  %149 = bitcast i32* %num_pos_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #5
  ret void
}

; Function Attrs: nounwind
define internal i32 @generate_luma_grain_block(%struct.aom_film_grain_t* %params, i32** %pred_pos_luma, i32* %luma_grain_block, i32 %luma_block_size_y, i32 %luma_block_size_x, i32 %luma_grain_stride, i32 %left_pad, i32 %top_pad, i32 %right_pad, i32 %bottom_pad) #0 {
entry:
  %retval = alloca i32, align 4
  %params.addr = alloca %struct.aom_film_grain_t*, align 4
  %pred_pos_luma.addr = alloca i32**, align 4
  %luma_grain_block.addr = alloca i32*, align 4
  %luma_block_size_y.addr = alloca i32, align 4
  %luma_block_size_x.addr = alloca i32, align 4
  %luma_grain_stride.addr = alloca i32, align 4
  %left_pad.addr = alloca i32, align 4
  %top_pad.addr = alloca i32, align 4
  %right_pad.addr = alloca i32, align 4
  %bottom_pad.addr = alloca i32, align 4
  %bit_depth = alloca i32, align 4
  %gauss_sec_shift = alloca i32, align 4
  %num_pos_luma = alloca i32, align 4
  %rounding_offset = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %i22 = alloca i32, align 4
  %j28 = alloca i32, align 4
  %wsum = alloca i32, align 4
  %pos = alloca i32, align 4
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  store i32** %pred_pos_luma, i32*** %pred_pos_luma.addr, align 4, !tbaa !2
  store i32* %luma_grain_block, i32** %luma_grain_block.addr, align 4, !tbaa !2
  store i32 %luma_block_size_y, i32* %luma_block_size_y.addr, align 4, !tbaa !6
  store i32 %luma_block_size_x, i32* %luma_block_size_x.addr, align 4, !tbaa !6
  store i32 %luma_grain_stride, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  store i32 %left_pad, i32* %left_pad.addr, align 4, !tbaa !6
  store i32 %top_pad, i32* %top_pad.addr, align 4, !tbaa !6
  store i32 %right_pad, i32* %right_pad.addr, align 4, !tbaa !6
  store i32 %bottom_pad, i32* %bottom_pad.addr, align 4, !tbaa !6
  %0 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %0, i32 0, i32 3
  %1 = load i32, i32* %num_y_points, align 4, !tbaa !33
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32*, i32** %luma_grain_block.addr, align 4, !tbaa !2
  %3 = bitcast i32* %2 to i8*
  %4 = load i32, i32* %luma_block_size_y.addr, align 4, !tbaa !6
  %mul = mul i32 4, %4
  %5 = load i32, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  %mul1 = mul i32 %mul, %5
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 %mul1, i1 false)
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %6 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %bit_depth2 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %7, i32 0, i32 22
  %8 = load i32, i32* %bit_depth2, align 4, !tbaa !32
  store i32 %8, i32* %bit_depth, align 4, !tbaa !6
  %9 = bitcast i32* %gauss_sec_shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %sub = sub nsw i32 12, %10
  %11 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %grain_scale_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %11, i32 0, i32 24
  %12 = load i32, i32* %grain_scale_shift, align 4, !tbaa !38
  %add = add nsw i32 %sub, %12
  store i32 %add, i32* %gauss_sec_shift, align 4, !tbaa !6
  %13 = bitcast i32* %num_pos_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %14, i32 0, i32 9
  %15 = load i32, i32* %ar_coeff_lag, align 4, !tbaa !37
  %mul3 = mul nsw i32 2, %15
  %16 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag4 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %16, i32 0, i32 9
  %17 = load i32, i32* %ar_coeff_lag4, align 4, !tbaa !37
  %add5 = add nsw i32 %17, 1
  %mul6 = mul nsw i32 %mul3, %add5
  store i32 %mul6, i32* %num_pos_luma, align 4, !tbaa !6
  %18 = bitcast i32* %rounding_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %19, i32 0, i32 13
  %20 = load i32, i32* %ar_coeff_shift, align 4, !tbaa !39
  %sub7 = sub nsw i32 %20, 1
  %shl = shl i32 1, %sub7
  store i32 %shl, i32* %rounding_offset, align 4, !tbaa !6
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc19, %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %23 = load i32, i32* %luma_block_size_y.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %22, %23
  br i1 %cmp8, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  br label %for.end21

for.body:                                         ; preds = %for.cond
  %25 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %for.body
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %27 = load i32, i32* %luma_block_size_x.addr, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %26, %27
  br i1 %cmp10, label %for.body12, label %for.cond.cleanup11

for.cond.cleanup11:                               ; preds = %for.cond9
  store i32 5, i32* %cleanup.dest.slot, align 4
  %28 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  br label %for.end

for.body12:                                       ; preds = %for.cond9
  %call = call i32 @get_random_number(i32 11)
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @gaussian_sequence, i32 0, i32 %call
  %29 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %30 = load i32, i32* %gauss_sec_shift, align 4, !tbaa !6
  %shl13 = shl i32 1, %30
  %shr = ashr i32 %shl13, 1
  %add14 = add nsw i32 %29, %shr
  %31 = load i32, i32* %gauss_sec_shift, align 4, !tbaa !6
  %shr15 = ashr i32 %add14, %31
  %32 = load i32*, i32** %luma_grain_block.addr, align 4, !tbaa !2
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %34 = load i32, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  %mul16 = mul nsw i32 %33, %34
  %35 = load i32, i32* %j, align 4, !tbaa !6
  %add17 = add nsw i32 %mul16, %35
  %arrayidx18 = getelementptr inbounds i32, i32* %32, i32 %add17
  store i32 %shr15, i32* %arrayidx18, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body12
  %36 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond9

for.end:                                          ; preds = %for.cond.cleanup11
  br label %for.inc19

for.inc19:                                        ; preds = %for.end
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %inc20 = add nsw i32 %37, 1
  store i32 %inc20, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end21:                                        ; preds = %for.cond.cleanup
  %38 = bitcast i32* %i22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #5
  %39 = load i32, i32* %top_pad.addr, align 4, !tbaa !6
  store i32 %39, i32* %i22, align 4, !tbaa !6
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc67, %for.end21
  %40 = load i32, i32* %i22, align 4, !tbaa !6
  %41 = load i32, i32* %luma_block_size_y.addr, align 4, !tbaa !6
  %42 = load i32, i32* %bottom_pad.addr, align 4, !tbaa !6
  %sub24 = sub nsw i32 %41, %42
  %cmp25 = icmp slt i32 %40, %sub24
  br i1 %cmp25, label %for.body27, label %for.cond.cleanup26

for.cond.cleanup26:                               ; preds = %for.cond23
  store i32 8, i32* %cleanup.dest.slot, align 4
  %43 = bitcast i32* %i22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #5
  br label %for.end69

for.body27:                                       ; preds = %for.cond23
  %44 = bitcast i32* %j28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  %45 = load i32, i32* %left_pad.addr, align 4, !tbaa !6
  store i32 %45, i32* %j28, align 4, !tbaa !6
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc64, %for.body27
  %46 = load i32, i32* %j28, align 4, !tbaa !6
  %47 = load i32, i32* %luma_block_size_x.addr, align 4, !tbaa !6
  %48 = load i32, i32* %right_pad.addr, align 4, !tbaa !6
  %sub30 = sub nsw i32 %47, %48
  %cmp31 = icmp slt i32 %46, %sub30
  br i1 %cmp31, label %for.body33, label %for.cond.cleanup32

for.cond.cleanup32:                               ; preds = %for.cond29
  store i32 11, i32* %cleanup.dest.slot, align 4
  %49 = bitcast i32* %j28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #5
  br label %for.end66

for.body33:                                       ; preds = %for.cond29
  %50 = bitcast i32* %wsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #5
  store i32 0, i32* %wsum, align 4, !tbaa !6
  %51 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #5
  store i32 0, i32* %pos, align 4, !tbaa !6
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc50, %for.body33
  %52 = load i32, i32* %pos, align 4, !tbaa !6
  %53 = load i32, i32* %num_pos_luma, align 4, !tbaa !6
  %cmp35 = icmp slt i32 %52, %53
  br i1 %cmp35, label %for.body37, label %for.cond.cleanup36

for.cond.cleanup36:                               ; preds = %for.cond34
  store i32 14, i32* %cleanup.dest.slot, align 4
  %54 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #5
  br label %for.end52

for.body37:                                       ; preds = %for.cond34
  %55 = load i32, i32* %wsum, align 4, !tbaa !6
  %56 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeffs_y = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %56, i32 0, i32 10
  %57 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds [24 x i32], [24 x i32]* %ar_coeffs_y, i32 0, i32 %57
  %58 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %59 = load i32*, i32** %luma_grain_block.addr, align 4, !tbaa !2
  %60 = load i32, i32* %i22, align 4, !tbaa !6
  %61 = load i32**, i32*** %pred_pos_luma.addr, align 4, !tbaa !2
  %62 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds i32*, i32** %61, i32 %62
  %63 = load i32*, i32** %arrayidx39, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %63, i32 0
  %64 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %add41 = add nsw i32 %60, %64
  %65 = load i32, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  %mul42 = mul nsw i32 %add41, %65
  %66 = load i32, i32* %j28, align 4, !tbaa !6
  %add43 = add nsw i32 %mul42, %66
  %67 = load i32**, i32*** %pred_pos_luma.addr, align 4, !tbaa !2
  %68 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds i32*, i32** %67, i32 %68
  %69 = load i32*, i32** %arrayidx44, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %69, i32 1
  %70 = load i32, i32* %arrayidx45, align 4, !tbaa !6
  %add46 = add nsw i32 %add43, %70
  %arrayidx47 = getelementptr inbounds i32, i32* %59, i32 %add46
  %71 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %mul48 = mul nsw i32 %58, %71
  %add49 = add nsw i32 %55, %mul48
  store i32 %add49, i32* %wsum, align 4, !tbaa !6
  br label %for.inc50

for.inc50:                                        ; preds = %for.body37
  %72 = load i32, i32* %pos, align 4, !tbaa !6
  %inc51 = add nsw i32 %72, 1
  store i32 %inc51, i32* %pos, align 4, !tbaa !6
  br label %for.cond34

for.end52:                                        ; preds = %for.cond.cleanup36
  %73 = load i32*, i32** %luma_grain_block.addr, align 4, !tbaa !2
  %74 = load i32, i32* %i22, align 4, !tbaa !6
  %75 = load i32, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  %mul53 = mul nsw i32 %74, %75
  %76 = load i32, i32* %j28, align 4, !tbaa !6
  %add54 = add nsw i32 %mul53, %76
  %arrayidx55 = getelementptr inbounds i32, i32* %73, i32 %add54
  %77 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %78 = load i32, i32* %wsum, align 4, !tbaa !6
  %79 = load i32, i32* %rounding_offset, align 4, !tbaa !6
  %add56 = add nsw i32 %78, %79
  %80 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_shift57 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %80, i32 0, i32 13
  %81 = load i32, i32* %ar_coeff_shift57, align 4, !tbaa !39
  %shr58 = ashr i32 %add56, %81
  %add59 = add nsw i32 %77, %shr58
  %82 = load i32, i32* @grain_min, align 4, !tbaa !6
  %83 = load i32, i32* @grain_max, align 4, !tbaa !6
  %call60 = call i32 @clamp(i32 %add59, i32 %82, i32 %83)
  %84 = load i32*, i32** %luma_grain_block.addr, align 4, !tbaa !2
  %85 = load i32, i32* %i22, align 4, !tbaa !6
  %86 = load i32, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  %mul61 = mul nsw i32 %85, %86
  %87 = load i32, i32* %j28, align 4, !tbaa !6
  %add62 = add nsw i32 %mul61, %87
  %arrayidx63 = getelementptr inbounds i32, i32* %84, i32 %add62
  store i32 %call60, i32* %arrayidx63, align 4, !tbaa !6
  %88 = bitcast i32* %wsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #5
  br label %for.inc64

for.inc64:                                        ; preds = %for.end52
  %89 = load i32, i32* %j28, align 4, !tbaa !6
  %inc65 = add nsw i32 %89, 1
  store i32 %inc65, i32* %j28, align 4, !tbaa !6
  br label %for.cond29

for.end66:                                        ; preds = %for.cond.cleanup32
  br label %for.inc67

for.inc67:                                        ; preds = %for.end66
  %90 = load i32, i32* %i22, align 4, !tbaa !6
  %inc68 = add nsw i32 %90, 1
  store i32 %inc68, i32* %i22, align 4, !tbaa !6
  br label %for.cond23

for.end69:                                        ; preds = %for.cond.cleanup26
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %91 = bitcast i32* %rounding_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #5
  %92 = bitcast i32* %num_pos_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #5
  %93 = bitcast i32* %gauss_sec_shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #5
  %94 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  br label %return

return:                                           ; preds = %for.end69, %if.then
  %95 = load i32, i32* %retval, align 4
  ret i32 %95
}

; Function Attrs: nounwind
define internal i32 @generate_chroma_grain_blocks(%struct.aom_film_grain_t* %params, i32** %pred_pos_chroma, i32* %luma_grain_block, i32* %cb_grain_block, i32* %cr_grain_block, i32 %luma_grain_stride, i32 %chroma_block_size_y, i32 %chroma_block_size_x, i32 %chroma_grain_stride, i32 %left_pad, i32 %top_pad, i32 %right_pad, i32 %bottom_pad, i32 %chroma_subsamp_y, i32 %chroma_subsamp_x) #0 {
entry:
  %retval = alloca i32, align 4
  %params.addr = alloca %struct.aom_film_grain_t*, align 4
  %pred_pos_chroma.addr = alloca i32**, align 4
  %luma_grain_block.addr = alloca i32*, align 4
  %cb_grain_block.addr = alloca i32*, align 4
  %cr_grain_block.addr = alloca i32*, align 4
  %luma_grain_stride.addr = alloca i32, align 4
  %chroma_block_size_y.addr = alloca i32, align 4
  %chroma_block_size_x.addr = alloca i32, align 4
  %chroma_grain_stride.addr = alloca i32, align 4
  %left_pad.addr = alloca i32, align 4
  %top_pad.addr = alloca i32, align 4
  %right_pad.addr = alloca i32, align 4
  %bottom_pad.addr = alloca i32, align 4
  %chroma_subsamp_y.addr = alloca i32, align 4
  %chroma_subsamp_x.addr = alloca i32, align 4
  %bit_depth = alloca i32, align 4
  %gauss_sec_shift = alloca i32, align 4
  %num_pos_chroma = alloca i32, align 4
  %rounding_offset = alloca i32, align 4
  %chroma_grain_block_size = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %i32 = alloca i32, align 4
  %j37 = alloca i32, align 4
  %i60 = alloca i32, align 4
  %j66 = alloca i32, align 4
  %wsum_cb = alloca i32, align 4
  %wsum_cr = alloca i32, align 4
  %pos = alloca i32, align 4
  %av_luma = alloca i32, align 4
  %luma_coord_y = alloca i32, align 4
  %luma_coord_x = alloca i32, align 4
  %k = alloca i32, align 4
  %l = alloca i32, align 4
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  store i32** %pred_pos_chroma, i32*** %pred_pos_chroma.addr, align 4, !tbaa !2
  store i32* %luma_grain_block, i32** %luma_grain_block.addr, align 4, !tbaa !2
  store i32* %cb_grain_block, i32** %cb_grain_block.addr, align 4, !tbaa !2
  store i32* %cr_grain_block, i32** %cr_grain_block.addr, align 4, !tbaa !2
  store i32 %luma_grain_stride, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  store i32 %chroma_block_size_y, i32* %chroma_block_size_y.addr, align 4, !tbaa !6
  store i32 %chroma_block_size_x, i32* %chroma_block_size_x.addr, align 4, !tbaa !6
  store i32 %chroma_grain_stride, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  store i32 %left_pad, i32* %left_pad.addr, align 4, !tbaa !6
  store i32 %top_pad, i32* %top_pad.addr, align 4, !tbaa !6
  store i32 %right_pad, i32* %right_pad.addr, align 4, !tbaa !6
  store i32 %bottom_pad, i32* %bottom_pad.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_y, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_x, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %0 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %bit_depth1 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %1, i32 0, i32 22
  %2 = load i32, i32* %bit_depth1, align 4, !tbaa !32
  store i32 %2, i32* %bit_depth, align 4, !tbaa !6
  %3 = bitcast i32* %gauss_sec_shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i32, i32* %bit_depth, align 4, !tbaa !6
  %sub = sub nsw i32 12, %4
  %5 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %grain_scale_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %5, i32 0, i32 24
  %6 = load i32, i32* %grain_scale_shift, align 4, !tbaa !38
  %add = add nsw i32 %sub, %6
  store i32 %add, i32* %gauss_sec_shift, align 4, !tbaa !6
  %7 = bitcast i32* %num_pos_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %8, i32 0, i32 9
  %9 = load i32, i32* %ar_coeff_lag, align 4, !tbaa !37
  %mul = mul nsw i32 2, %9
  %10 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag2 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %10, i32 0, i32 9
  %11 = load i32, i32* %ar_coeff_lag2, align 4, !tbaa !37
  %add3 = add nsw i32 %11, 1
  %mul4 = mul nsw i32 %mul, %add3
  store i32 %mul4, i32* %num_pos_chroma, align 4, !tbaa !6
  %12 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %12, i32 0, i32 3
  %13 = load i32, i32* %num_y_points, align 4, !tbaa !33
  %cmp = icmp sgt i32 %13, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %14 = load i32, i32* %num_pos_chroma, align 4, !tbaa !6
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %num_pos_chroma, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = bitcast i32* %rounding_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %16, i32 0, i32 13
  %17 = load i32, i32* %ar_coeff_shift, align 4, !tbaa !39
  %sub5 = sub nsw i32 %17, 1
  %shl = shl i32 1, %sub5
  store i32 %shl, i32* %rounding_offset, align 4, !tbaa !6
  %18 = bitcast i32* %chroma_grain_block_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load i32, i32* %chroma_block_size_y.addr, align 4, !tbaa !6
  %20 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul6 = mul nsw i32 %19, %20
  store i32 %mul6, i32* %chroma_grain_block_size, align 4, !tbaa !6
  %21 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cb_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %21, i32 0, i32 5
  %22 = load i32, i32* %num_cb_points, align 4, !tbaa !35
  %tobool = icmp ne i32 %22, 0
  br i1 %tobool, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %23 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %23, i32 0, i32 23
  %24 = load i32, i32* %chroma_scaling_from_luma, align 4, !tbaa !34
  %tobool7 = icmp ne i32 %24, 0
  br i1 %tobool7, label %if.then8, label %if.else

if.then8:                                         ; preds = %lor.lhs.false, %if.end
  %25 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %random_seed = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %25, i32 0, i32 25
  %26 = load i16, i16* %random_seed, align 4, !tbaa !29
  call void @init_random_generator(i32 224, i16 zeroext %26)
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %if.then8
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %29 = load i32, i32* %chroma_block_size_y.addr, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %28, %29
  br i1 %cmp9, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  br label %for.end23

for.body:                                         ; preds = %for.cond
  %31 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body
  %32 = load i32, i32* %j, align 4, !tbaa !6
  %33 = load i32, i32* %chroma_block_size_x.addr, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %32, %33
  br i1 %cmp11, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond10
  store i32 5, i32* %cleanup.dest.slot, align 4
  %34 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  br label %for.end

for.body13:                                       ; preds = %for.cond10
  %call = call i32 @get_random_number(i32 11)
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @gaussian_sequence, i32 0, i32 %call
  %35 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %36 = load i32, i32* %gauss_sec_shift, align 4, !tbaa !6
  %shl14 = shl i32 1, %36
  %shr = ashr i32 %shl14, 1
  %add15 = add nsw i32 %35, %shr
  %37 = load i32, i32* %gauss_sec_shift, align 4, !tbaa !6
  %shr16 = ashr i32 %add15, %37
  %38 = load i32*, i32** %cb_grain_block.addr, align 4, !tbaa !2
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %40 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 %39, %40
  %41 = load i32, i32* %j, align 4, !tbaa !6
  %add18 = add nsw i32 %mul17, %41
  %arrayidx19 = getelementptr inbounds i32, i32* %38, i32 %add18
  store i32 %shr16, i32* %arrayidx19, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %42 = load i32, i32* %j, align 4, !tbaa !6
  %inc20 = add nsw i32 %42, 1
  store i32 %inc20, i32* %j, align 4, !tbaa !6
  br label %for.cond10

for.end:                                          ; preds = %for.cond.cleanup12
  br label %for.inc21

for.inc21:                                        ; preds = %for.end
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %inc22 = add nsw i32 %43, 1
  store i32 %inc22, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end23:                                        ; preds = %for.cond.cleanup
  br label %if.end25

if.else:                                          ; preds = %lor.lhs.false
  %44 = load i32*, i32** %cb_grain_block.addr, align 4, !tbaa !2
  %45 = bitcast i32* %44 to i8*
  %46 = load i32, i32* %chroma_grain_block_size, align 4, !tbaa !6
  %mul24 = mul i32 4, %46
  call void @llvm.memset.p0i8.i32(i8* align 4 %45, i8 0, i32 %mul24, i1 false)
  br label %if.end25

if.end25:                                         ; preds = %if.else, %for.end23
  %47 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cr_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %47, i32 0, i32 7
  %48 = load i32, i32* %num_cr_points, align 4, !tbaa !36
  %tobool26 = icmp ne i32 %48, 0
  br i1 %tobool26, label %if.then30, label %lor.lhs.false27

lor.lhs.false27:                                  ; preds = %if.end25
  %49 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma28 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %49, i32 0, i32 23
  %50 = load i32, i32* %chroma_scaling_from_luma28, align 4, !tbaa !34
  %tobool29 = icmp ne i32 %50, 0
  br i1 %tobool29, label %if.then30, label %if.else57

if.then30:                                        ; preds = %lor.lhs.false27, %if.end25
  %51 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %random_seed31 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %51, i32 0, i32 25
  %52 = load i16, i16* %random_seed31, align 4, !tbaa !29
  call void @init_random_generator(i32 352, i16 zeroext %52)
  %53 = bitcast i32* %i32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #5
  store i32 0, i32* %i32, align 4, !tbaa !6
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc54, %if.then30
  %54 = load i32, i32* %i32, align 4, !tbaa !6
  %55 = load i32, i32* %chroma_block_size_y.addr, align 4, !tbaa !6
  %cmp34 = icmp slt i32 %54, %55
  br i1 %cmp34, label %for.body36, label %for.cond.cleanup35

for.cond.cleanup35:                               ; preds = %for.cond33
  store i32 8, i32* %cleanup.dest.slot, align 4
  %56 = bitcast i32* %i32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #5
  br label %for.end56

for.body36:                                       ; preds = %for.cond33
  %57 = bitcast i32* %j37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #5
  store i32 0, i32* %j37, align 4, !tbaa !6
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc51, %for.body36
  %58 = load i32, i32* %j37, align 4, !tbaa !6
  %59 = load i32, i32* %chroma_block_size_x.addr, align 4, !tbaa !6
  %cmp39 = icmp slt i32 %58, %59
  br i1 %cmp39, label %for.body41, label %for.cond.cleanup40

for.cond.cleanup40:                               ; preds = %for.cond38
  store i32 11, i32* %cleanup.dest.slot, align 4
  %60 = bitcast i32* %j37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #5
  br label %for.end53

for.body41:                                       ; preds = %for.cond38
  %call42 = call i32 @get_random_number(i32 11)
  %arrayidx43 = getelementptr inbounds [2048 x i32], [2048 x i32]* @gaussian_sequence, i32 0, i32 %call42
  %61 = load i32, i32* %arrayidx43, align 4, !tbaa !6
  %62 = load i32, i32* %gauss_sec_shift, align 4, !tbaa !6
  %shl44 = shl i32 1, %62
  %shr45 = ashr i32 %shl44, 1
  %add46 = add nsw i32 %61, %shr45
  %63 = load i32, i32* %gauss_sec_shift, align 4, !tbaa !6
  %shr47 = ashr i32 %add46, %63
  %64 = load i32*, i32** %cr_grain_block.addr, align 4, !tbaa !2
  %65 = load i32, i32* %i32, align 4, !tbaa !6
  %66 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul48 = mul nsw i32 %65, %66
  %67 = load i32, i32* %j37, align 4, !tbaa !6
  %add49 = add nsw i32 %mul48, %67
  %arrayidx50 = getelementptr inbounds i32, i32* %64, i32 %add49
  store i32 %shr47, i32* %arrayidx50, align 4, !tbaa !6
  br label %for.inc51

for.inc51:                                        ; preds = %for.body41
  %68 = load i32, i32* %j37, align 4, !tbaa !6
  %inc52 = add nsw i32 %68, 1
  store i32 %inc52, i32* %j37, align 4, !tbaa !6
  br label %for.cond38

for.end53:                                        ; preds = %for.cond.cleanup40
  br label %for.inc54

for.inc54:                                        ; preds = %for.end53
  %69 = load i32, i32* %i32, align 4, !tbaa !6
  %inc55 = add nsw i32 %69, 1
  store i32 %inc55, i32* %i32, align 4, !tbaa !6
  br label %for.cond33

for.end56:                                        ; preds = %for.cond.cleanup35
  br label %if.end59

if.else57:                                        ; preds = %lor.lhs.false27
  %70 = load i32*, i32** %cr_grain_block.addr, align 4, !tbaa !2
  %71 = bitcast i32* %70 to i8*
  %72 = load i32, i32* %chroma_grain_block_size, align 4, !tbaa !6
  %mul58 = mul i32 4, %72
  call void @llvm.memset.p0i8.i32(i8* align 4 %71, i8 0, i32 %mul58, i1 false)
  br label %if.end59

if.end59:                                         ; preds = %if.else57, %for.end56
  %73 = bitcast i32* %i60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #5
  %74 = load i32, i32* %top_pad.addr, align 4, !tbaa !6
  store i32 %74, i32* %i60, align 4, !tbaa !6
  br label %for.cond61

for.cond61:                                       ; preds = %for.inc202, %if.end59
  %75 = load i32, i32* %i60, align 4, !tbaa !6
  %76 = load i32, i32* %chroma_block_size_y.addr, align 4, !tbaa !6
  %77 = load i32, i32* %bottom_pad.addr, align 4, !tbaa !6
  %sub62 = sub nsw i32 %76, %77
  %cmp63 = icmp slt i32 %75, %sub62
  br i1 %cmp63, label %for.body65, label %for.cond.cleanup64

for.cond.cleanup64:                               ; preds = %for.cond61
  store i32 14, i32* %cleanup.dest.slot, align 4
  br label %cleanup204

for.body65:                                       ; preds = %for.cond61
  %78 = bitcast i32* %j66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #5
  %79 = load i32, i32* %left_pad.addr, align 4, !tbaa !6
  store i32 %79, i32* %j66, align 4, !tbaa !6
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc197, %for.body65
  %80 = load i32, i32* %j66, align 4, !tbaa !6
  %81 = load i32, i32* %chroma_block_size_x.addr, align 4, !tbaa !6
  %82 = load i32, i32* %right_pad.addr, align 4, !tbaa !6
  %sub68 = sub nsw i32 %81, %82
  %cmp69 = icmp slt i32 %80, %sub68
  br i1 %cmp69, label %for.body71, label %for.cond.cleanup70

for.cond.cleanup70:                               ; preds = %for.cond67
  store i32 17, i32* %cleanup.dest.slot, align 4
  br label %cleanup199

for.body71:                                       ; preds = %for.cond67
  %83 = bitcast i32* %wsum_cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #5
  store i32 0, i32* %wsum_cb, align 4, !tbaa !6
  %84 = bitcast i32* %wsum_cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #5
  store i32 0, i32* %wsum_cr, align 4, !tbaa !6
  %85 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #5
  store i32 0, i32* %pos, align 4, !tbaa !6
  br label %for.cond72

for.cond72:                                       ; preds = %for.inc155, %for.body71
  %86 = load i32, i32* %pos, align 4, !tbaa !6
  %87 = load i32, i32* %num_pos_chroma, align 4, !tbaa !6
  %cmp73 = icmp slt i32 %86, %87
  br i1 %cmp73, label %for.body75, label %for.cond.cleanup74

for.cond.cleanup74:                               ; preds = %for.cond72
  store i32 20, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body75:                                       ; preds = %for.cond72
  %88 = load i32**, i32*** %pred_pos_chroma.addr, align 4, !tbaa !2
  %89 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx76 = getelementptr inbounds i32*, i32** %88, i32 %89
  %90 = load i32*, i32** %arrayidx76, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i32, i32* %90, i32 2
  %91 = load i32, i32* %arrayidx77, align 4, !tbaa !6
  %cmp78 = icmp eq i32 %91, 0
  br i1 %cmp78, label %if.then79, label %if.else104

if.then79:                                        ; preds = %for.body75
  %92 = load i32, i32* %wsum_cb, align 4, !tbaa !6
  %93 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeffs_cb = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %93, i32 0, i32 11
  %94 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx80 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cb, i32 0, i32 %94
  %95 = load i32, i32* %arrayidx80, align 4, !tbaa !6
  %96 = load i32*, i32** %cb_grain_block.addr, align 4, !tbaa !2
  %97 = load i32, i32* %i60, align 4, !tbaa !6
  %98 = load i32**, i32*** %pred_pos_chroma.addr, align 4, !tbaa !2
  %99 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx81 = getelementptr inbounds i32*, i32** %98, i32 %99
  %100 = load i32*, i32** %arrayidx81, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %100, i32 0
  %101 = load i32, i32* %arrayidx82, align 4, !tbaa !6
  %add83 = add nsw i32 %97, %101
  %102 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul84 = mul nsw i32 %add83, %102
  %103 = load i32, i32* %j66, align 4, !tbaa !6
  %add85 = add nsw i32 %mul84, %103
  %104 = load i32**, i32*** %pred_pos_chroma.addr, align 4, !tbaa !2
  %105 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds i32*, i32** %104, i32 %105
  %106 = load i32*, i32** %arrayidx86, align 4, !tbaa !2
  %arrayidx87 = getelementptr inbounds i32, i32* %106, i32 1
  %107 = load i32, i32* %arrayidx87, align 4, !tbaa !6
  %add88 = add nsw i32 %add85, %107
  %arrayidx89 = getelementptr inbounds i32, i32* %96, i32 %add88
  %108 = load i32, i32* %arrayidx89, align 4, !tbaa !6
  %mul90 = mul nsw i32 %95, %108
  %add91 = add nsw i32 %92, %mul90
  store i32 %add91, i32* %wsum_cb, align 4, !tbaa !6
  %109 = load i32, i32* %wsum_cr, align 4, !tbaa !6
  %110 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeffs_cr = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %110, i32 0, i32 12
  %111 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx92 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cr, i32 0, i32 %111
  %112 = load i32, i32* %arrayidx92, align 4, !tbaa !6
  %113 = load i32*, i32** %cr_grain_block.addr, align 4, !tbaa !2
  %114 = load i32, i32* %i60, align 4, !tbaa !6
  %115 = load i32**, i32*** %pred_pos_chroma.addr, align 4, !tbaa !2
  %116 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx93 = getelementptr inbounds i32*, i32** %115, i32 %116
  %117 = load i32*, i32** %arrayidx93, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i32, i32* %117, i32 0
  %118 = load i32, i32* %arrayidx94, align 4, !tbaa !6
  %add95 = add nsw i32 %114, %118
  %119 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul96 = mul nsw i32 %add95, %119
  %120 = load i32, i32* %j66, align 4, !tbaa !6
  %add97 = add nsw i32 %mul96, %120
  %121 = load i32**, i32*** %pred_pos_chroma.addr, align 4, !tbaa !2
  %122 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds i32*, i32** %121, i32 %122
  %123 = load i32*, i32** %arrayidx98, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i32, i32* %123, i32 1
  %124 = load i32, i32* %arrayidx99, align 4, !tbaa !6
  %add100 = add nsw i32 %add97, %124
  %arrayidx101 = getelementptr inbounds i32, i32* %113, i32 %add100
  %125 = load i32, i32* %arrayidx101, align 4, !tbaa !6
  %mul102 = mul nsw i32 %112, %125
  %add103 = add nsw i32 %109, %mul102
  store i32 %add103, i32* %wsum_cr, align 4, !tbaa !6
  br label %if.end154

if.else104:                                       ; preds = %for.body75
  %126 = load i32**, i32*** %pred_pos_chroma.addr, align 4, !tbaa !2
  %127 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx105 = getelementptr inbounds i32*, i32** %126, i32 %127
  %128 = load i32*, i32** %arrayidx105, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i32, i32* %128, i32 2
  %129 = load i32, i32* %arrayidx106, align 4, !tbaa !6
  %cmp107 = icmp eq i32 %129, 1
  br i1 %cmp107, label %if.then108, label %if.else151

if.then108:                                       ; preds = %if.else104
  %130 = bitcast i32* %av_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %130) #5
  store i32 0, i32* %av_luma, align 4, !tbaa !6
  %131 = bitcast i32* %luma_coord_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #5
  %132 = load i32, i32* %i60, align 4, !tbaa !6
  %133 = load i32, i32* %top_pad.addr, align 4, !tbaa !6
  %sub109 = sub nsw i32 %132, %133
  %134 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shl110 = shl i32 %sub109, %134
  %135 = load i32, i32* %top_pad.addr, align 4, !tbaa !6
  %add111 = add nsw i32 %shl110, %135
  store i32 %add111, i32* %luma_coord_y, align 4, !tbaa !6
  %136 = bitcast i32* %luma_coord_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #5
  %137 = load i32, i32* %j66, align 4, !tbaa !6
  %138 = load i32, i32* %left_pad.addr, align 4, !tbaa !6
  %sub112 = sub nsw i32 %137, %138
  %139 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shl113 = shl i32 %sub112, %139
  %140 = load i32, i32* %left_pad.addr, align 4, !tbaa !6
  %add114 = add nsw i32 %shl113, %140
  store i32 %add114, i32* %luma_coord_x, align 4, !tbaa !6
  %141 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %141) #5
  %142 = load i32, i32* %luma_coord_y, align 4, !tbaa !6
  store i32 %142, i32* %k, align 4, !tbaa !6
  br label %for.cond115

for.cond115:                                      ; preds = %for.inc134, %if.then108
  %143 = load i32, i32* %k, align 4, !tbaa !6
  %144 = load i32, i32* %luma_coord_y, align 4, !tbaa !6
  %145 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %add116 = add nsw i32 %144, %145
  %add117 = add nsw i32 %add116, 1
  %cmp118 = icmp slt i32 %143, %add117
  br i1 %cmp118, label %for.body120, label %for.cond.cleanup119

for.cond.cleanup119:                              ; preds = %for.cond115
  store i32 23, i32* %cleanup.dest.slot, align 4
  %146 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #5
  br label %for.end136

for.body120:                                      ; preds = %for.cond115
  %147 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %147) #5
  %148 = load i32, i32* %luma_coord_x, align 4, !tbaa !6
  store i32 %148, i32* %l, align 4, !tbaa !6
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc131, %for.body120
  %149 = load i32, i32* %l, align 4, !tbaa !6
  %150 = load i32, i32* %luma_coord_x, align 4, !tbaa !6
  %151 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %add122 = add nsw i32 %150, %151
  %add123 = add nsw i32 %add122, 1
  %cmp124 = icmp slt i32 %149, %add123
  br i1 %cmp124, label %for.body126, label %for.cond.cleanup125

for.cond.cleanup125:                              ; preds = %for.cond121
  store i32 26, i32* %cleanup.dest.slot, align 4
  %152 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #5
  br label %for.end133

for.body126:                                      ; preds = %for.cond121
  %153 = load i32*, i32** %luma_grain_block.addr, align 4, !tbaa !2
  %154 = load i32, i32* %k, align 4, !tbaa !6
  %155 = load i32, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  %mul127 = mul nsw i32 %154, %155
  %156 = load i32, i32* %l, align 4, !tbaa !6
  %add128 = add nsw i32 %mul127, %156
  %arrayidx129 = getelementptr inbounds i32, i32* %153, i32 %add128
  %157 = load i32, i32* %arrayidx129, align 4, !tbaa !6
  %158 = load i32, i32* %av_luma, align 4, !tbaa !6
  %add130 = add nsw i32 %158, %157
  store i32 %add130, i32* %av_luma, align 4, !tbaa !6
  br label %for.inc131

for.inc131:                                       ; preds = %for.body126
  %159 = load i32, i32* %l, align 4, !tbaa !6
  %inc132 = add nsw i32 %159, 1
  store i32 %inc132, i32* %l, align 4, !tbaa !6
  br label %for.cond121

for.end133:                                       ; preds = %for.cond.cleanup125
  br label %for.inc134

for.inc134:                                       ; preds = %for.end133
  %160 = load i32, i32* %k, align 4, !tbaa !6
  %inc135 = add nsw i32 %160, 1
  store i32 %inc135, i32* %k, align 4, !tbaa !6
  br label %for.cond115

for.end136:                                       ; preds = %for.cond.cleanup119
  %161 = load i32, i32* %av_luma, align 4, !tbaa !6
  %162 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %163 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %add137 = add nsw i32 %162, %163
  %shl138 = shl i32 1, %add137
  %shr139 = ashr i32 %shl138, 1
  %add140 = add nsw i32 %161, %shr139
  %164 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %165 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %add141 = add nsw i32 %164, %165
  %shr142 = ashr i32 %add140, %add141
  store i32 %shr142, i32* %av_luma, align 4, !tbaa !6
  %166 = load i32, i32* %wsum_cb, align 4, !tbaa !6
  %167 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeffs_cb143 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %167, i32 0, i32 11
  %168 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx144 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cb143, i32 0, i32 %168
  %169 = load i32, i32* %arrayidx144, align 4, !tbaa !6
  %170 = load i32, i32* %av_luma, align 4, !tbaa !6
  %mul145 = mul nsw i32 %169, %170
  %add146 = add nsw i32 %166, %mul145
  store i32 %add146, i32* %wsum_cb, align 4, !tbaa !6
  %171 = load i32, i32* %wsum_cr, align 4, !tbaa !6
  %172 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeffs_cr147 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %172, i32 0, i32 12
  %173 = load i32, i32* %pos, align 4, !tbaa !6
  %arrayidx148 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cr147, i32 0, i32 %173
  %174 = load i32, i32* %arrayidx148, align 4, !tbaa !6
  %175 = load i32, i32* %av_luma, align 4, !tbaa !6
  %mul149 = mul nsw i32 %174, %175
  %add150 = add nsw i32 %171, %mul149
  store i32 %add150, i32* %wsum_cr, align 4, !tbaa !6
  %176 = bitcast i32* %luma_coord_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #5
  %177 = bitcast i32* %luma_coord_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #5
  %178 = bitcast i32* %av_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #5
  br label %if.end153

if.else151:                                       ; preds = %if.else104
  %179 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call152 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %179, i8* getelementptr inbounds ([76 x i8], [76 x i8]* @.str.1, i32 0, i32 0))
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end153:                                        ; preds = %for.end136
  br label %if.end154

if.end154:                                        ; preds = %if.end153, %if.then79
  br label %for.inc155

for.inc155:                                       ; preds = %if.end154
  %180 = load i32, i32* %pos, align 4, !tbaa !6
  %inc156 = add nsw i32 %180, 1
  store i32 %inc156, i32* %pos, align 4, !tbaa !6
  br label %for.cond72

cleanup:                                          ; preds = %if.else151, %for.cond.cleanup74
  %181 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup194 [
    i32 20, label %for.end157
  ]

for.end157:                                       ; preds = %cleanup
  %182 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cb_points158 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %182, i32 0, i32 5
  %183 = load i32, i32* %num_cb_points158, align 4, !tbaa !35
  %tobool159 = icmp ne i32 %183, 0
  br i1 %tobool159, label %if.then163, label %lor.lhs.false160

lor.lhs.false160:                                 ; preds = %for.end157
  %184 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma161 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %184, i32 0, i32 23
  %185 = load i32, i32* %chroma_scaling_from_luma161, align 4, !tbaa !34
  %tobool162 = icmp ne i32 %185, 0
  br i1 %tobool162, label %if.then163, label %if.end175

if.then163:                                       ; preds = %lor.lhs.false160, %for.end157
  %186 = load i32*, i32** %cb_grain_block.addr, align 4, !tbaa !2
  %187 = load i32, i32* %i60, align 4, !tbaa !6
  %188 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul164 = mul nsw i32 %187, %188
  %189 = load i32, i32* %j66, align 4, !tbaa !6
  %add165 = add nsw i32 %mul164, %189
  %arrayidx166 = getelementptr inbounds i32, i32* %186, i32 %add165
  %190 = load i32, i32* %arrayidx166, align 4, !tbaa !6
  %191 = load i32, i32* %wsum_cb, align 4, !tbaa !6
  %192 = load i32, i32* %rounding_offset, align 4, !tbaa !6
  %add167 = add nsw i32 %191, %192
  %193 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_shift168 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %193, i32 0, i32 13
  %194 = load i32, i32* %ar_coeff_shift168, align 4, !tbaa !39
  %shr169 = ashr i32 %add167, %194
  %add170 = add nsw i32 %190, %shr169
  %195 = load i32, i32* @grain_min, align 4, !tbaa !6
  %196 = load i32, i32* @grain_max, align 4, !tbaa !6
  %call171 = call i32 @clamp(i32 %add170, i32 %195, i32 %196)
  %197 = load i32*, i32** %cb_grain_block.addr, align 4, !tbaa !2
  %198 = load i32, i32* %i60, align 4, !tbaa !6
  %199 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul172 = mul nsw i32 %198, %199
  %200 = load i32, i32* %j66, align 4, !tbaa !6
  %add173 = add nsw i32 %mul172, %200
  %arrayidx174 = getelementptr inbounds i32, i32* %197, i32 %add173
  store i32 %call171, i32* %arrayidx174, align 4, !tbaa !6
  br label %if.end175

if.end175:                                        ; preds = %if.then163, %lor.lhs.false160
  %201 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cr_points176 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %201, i32 0, i32 7
  %202 = load i32, i32* %num_cr_points176, align 4, !tbaa !36
  %tobool177 = icmp ne i32 %202, 0
  br i1 %tobool177, label %if.then181, label %lor.lhs.false178

lor.lhs.false178:                                 ; preds = %if.end175
  %203 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma179 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %203, i32 0, i32 23
  %204 = load i32, i32* %chroma_scaling_from_luma179, align 4, !tbaa !34
  %tobool180 = icmp ne i32 %204, 0
  br i1 %tobool180, label %if.then181, label %if.end193

if.then181:                                       ; preds = %lor.lhs.false178, %if.end175
  %205 = load i32*, i32** %cr_grain_block.addr, align 4, !tbaa !2
  %206 = load i32, i32* %i60, align 4, !tbaa !6
  %207 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul182 = mul nsw i32 %206, %207
  %208 = load i32, i32* %j66, align 4, !tbaa !6
  %add183 = add nsw i32 %mul182, %208
  %arrayidx184 = getelementptr inbounds i32, i32* %205, i32 %add183
  %209 = load i32, i32* %arrayidx184, align 4, !tbaa !6
  %210 = load i32, i32* %wsum_cr, align 4, !tbaa !6
  %211 = load i32, i32* %rounding_offset, align 4, !tbaa !6
  %add185 = add nsw i32 %210, %211
  %212 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_shift186 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %212, i32 0, i32 13
  %213 = load i32, i32* %ar_coeff_shift186, align 4, !tbaa !39
  %shr187 = ashr i32 %add185, %213
  %add188 = add nsw i32 %209, %shr187
  %214 = load i32, i32* @grain_min, align 4, !tbaa !6
  %215 = load i32, i32* @grain_max, align 4, !tbaa !6
  %call189 = call i32 @clamp(i32 %add188, i32 %214, i32 %215)
  %216 = load i32*, i32** %cr_grain_block.addr, align 4, !tbaa !2
  %217 = load i32, i32* %i60, align 4, !tbaa !6
  %218 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul190 = mul nsw i32 %217, %218
  %219 = load i32, i32* %j66, align 4, !tbaa !6
  %add191 = add nsw i32 %mul190, %219
  %arrayidx192 = getelementptr inbounds i32, i32* %216, i32 %add191
  store i32 %call189, i32* %arrayidx192, align 4, !tbaa !6
  br label %if.end193

if.end193:                                        ; preds = %if.then181, %lor.lhs.false178
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup194

cleanup194:                                       ; preds = %if.end193, %cleanup
  %220 = bitcast i32* %wsum_cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #5
  %221 = bitcast i32* %wsum_cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #5
  %cleanup.dest196 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest196, label %cleanup199 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup194
  br label %for.inc197

for.inc197:                                       ; preds = %cleanup.cont
  %222 = load i32, i32* %j66, align 4, !tbaa !6
  %inc198 = add nsw i32 %222, 1
  store i32 %inc198, i32* %j66, align 4, !tbaa !6
  br label %for.cond67

cleanup199:                                       ; preds = %cleanup194, %for.cond.cleanup70
  %223 = bitcast i32* %j66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #5
  %cleanup.dest200 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest200, label %cleanup204 [
    i32 17, label %for.end201
  ]

for.end201:                                       ; preds = %cleanup199
  br label %for.inc202

for.inc202:                                       ; preds = %for.end201
  %224 = load i32, i32* %i60, align 4, !tbaa !6
  %inc203 = add nsw i32 %224, 1
  store i32 %inc203, i32* %i60, align 4, !tbaa !6
  br label %for.cond61

cleanup204:                                       ; preds = %cleanup199, %for.cond.cleanup64
  %225 = bitcast i32* %i60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #5
  %cleanup.dest205 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest205, label %cleanup207 [
    i32 14, label %for.end206
  ]

for.end206:                                       ; preds = %cleanup204
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup207

cleanup207:                                       ; preds = %for.end206, %cleanup204
  %226 = bitcast i32* %chroma_grain_block_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #5
  %227 = bitcast i32* %rounding_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #5
  %228 = bitcast i32* %num_pos_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #5
  %229 = bitcast i32* %gauss_sec_shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #5
  %230 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #5
  %231 = load i32, i32* %retval, align 4
  ret i32 %231
}

; Function Attrs: nounwind
define internal void @init_scaling_function([2 x i32]* %scaling_points, i32 %num_points, i32* %scaling_lut) #0 {
entry:
  %scaling_points.addr = alloca [2 x i32]*, align 4
  %num_points.addr = alloca i32, align 4
  %scaling_lut.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %point = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %delta_y = alloca i32, align 4
  %delta_x = alloca i32, align 4
  %delta = alloca i64, align 8
  %x = alloca i32, align 4
  %i45 = alloca i32, align 4
  store [2 x i32]* %scaling_points, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  store i32 %num_points, i32* %num_points.addr, align 4, !tbaa !6
  store i32* %scaling_lut, i32** %scaling_lut.addr, align 4, !tbaa !2
  %0 = load i32, i32* %num_points.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end60

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %3, i32 0
  %arrayidx1 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 0
  %4 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %2, %4
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds [2 x i32], [2 x i32]* %6, i32 0
  %arrayidx4 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx3, i32 0, i32 1
  %7 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %8 = load i32*, i32** %scaling_lut.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i32, i32* %8, i32 %9
  store i32 %7, i32* %arrayidx5, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %11 = bitcast i32* %point to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  store i32 0, i32* %point, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc42, %for.end
  %12 = load i32, i32* %point, align 4, !tbaa !6
  %13 = load i32, i32* %num_points.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %13, 1
  %cmp7 = icmp slt i32 %12, %sub
  br i1 %cmp7, label %for.body9, label %for.cond.cleanup8

for.cond.cleanup8:                                ; preds = %for.cond6
  store i32 5, i32* %cleanup.dest.slot, align 4
  %14 = bitcast i32* %point to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  br label %for.end44

for.body9:                                        ; preds = %for.cond6
  %15 = bitcast i32* %delta_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %17 = load i32, i32* %point, align 4, !tbaa !6
  %add = add nsw i32 %17, 1
  %arrayidx10 = getelementptr inbounds [2 x i32], [2 x i32]* %16, i32 %add
  %arrayidx11 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx10, i32 0, i32 1
  %18 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %19 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %20 = load i32, i32* %point, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [2 x i32], [2 x i32]* %19, i32 %20
  %arrayidx13 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx12, i32 0, i32 1
  %21 = load i32, i32* %arrayidx13, align 4, !tbaa !6
  %sub14 = sub nsw i32 %18, %21
  store i32 %sub14, i32* %delta_y, align 4, !tbaa !6
  %22 = bitcast i32* %delta_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %24 = load i32, i32* %point, align 4, !tbaa !6
  %add15 = add nsw i32 %24, 1
  %arrayidx16 = getelementptr inbounds [2 x i32], [2 x i32]* %23, i32 %add15
  %arrayidx17 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx16, i32 0, i32 0
  %25 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %26 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %27 = load i32, i32* %point, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds [2 x i32], [2 x i32]* %26, i32 %27
  %arrayidx19 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx18, i32 0, i32 0
  %28 = load i32, i32* %arrayidx19, align 4, !tbaa !6
  %sub20 = sub nsw i32 %25, %28
  store i32 %sub20, i32* %delta_x, align 4, !tbaa !6
  %29 = bitcast i64* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %29) #5
  %30 = load i32, i32* %delta_y, align 4, !tbaa !6
  %31 = load i32, i32* %delta_x, align 4, !tbaa !6
  %shr = ashr i32 %31, 1
  %add21 = add nsw i32 65536, %shr
  %32 = load i32, i32* %delta_x, align 4, !tbaa !6
  %div = sdiv i32 %add21, %32
  %mul = mul nsw i32 %30, %div
  %conv = sext i32 %mul to i64
  store i64 %conv, i64* %delta, align 8, !tbaa !40
  %33 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #5
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc39, %for.body9
  %34 = load i32, i32* %x, align 4, !tbaa !6
  %35 = load i32, i32* %delta_x, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %34, %35
  br i1 %cmp23, label %for.body26, label %for.cond.cleanup25

for.cond.cleanup25:                               ; preds = %for.cond22
  store i32 8, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  br label %for.end41

for.body26:                                       ; preds = %for.cond22
  %37 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %38 = load i32, i32* %point, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds [2 x i32], [2 x i32]* %37, i32 %38
  %arrayidx28 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx27, i32 0, i32 1
  %39 = load i32, i32* %arrayidx28, align 4, !tbaa !6
  %40 = load i32, i32* %x, align 4, !tbaa !6
  %conv29 = sext i32 %40 to i64
  %41 = load i64, i64* %delta, align 8, !tbaa !40
  %mul30 = mul nsw i64 %conv29, %41
  %add31 = add nsw i64 %mul30, 32768
  %shr32 = ashr i64 %add31, 16
  %conv33 = trunc i64 %shr32 to i32
  %add34 = add nsw i32 %39, %conv33
  %42 = load i32*, i32** %scaling_lut.addr, align 4, !tbaa !2
  %43 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %44 = load i32, i32* %point, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds [2 x i32], [2 x i32]* %43, i32 %44
  %arrayidx36 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx35, i32 0, i32 0
  %45 = load i32, i32* %arrayidx36, align 4, !tbaa !6
  %46 = load i32, i32* %x, align 4, !tbaa !6
  %add37 = add nsw i32 %45, %46
  %arrayidx38 = getelementptr inbounds i32, i32* %42, i32 %add37
  store i32 %add34, i32* %arrayidx38, align 4, !tbaa !6
  br label %for.inc39

for.inc39:                                        ; preds = %for.body26
  %47 = load i32, i32* %x, align 4, !tbaa !6
  %inc40 = add nsw i32 %47, 1
  store i32 %inc40, i32* %x, align 4, !tbaa !6
  br label %for.cond22

for.end41:                                        ; preds = %for.cond.cleanup25
  %48 = bitcast i64* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %48) #5
  %49 = bitcast i32* %delta_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #5
  %50 = bitcast i32* %delta_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  br label %for.inc42

for.inc42:                                        ; preds = %for.end41
  %51 = load i32, i32* %point, align 4, !tbaa !6
  %inc43 = add nsw i32 %51, 1
  store i32 %inc43, i32* %point, align 4, !tbaa !6
  br label %for.cond6

for.end44:                                        ; preds = %for.cond.cleanup8
  %52 = bitcast i32* %i45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #5
  %53 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %54 = load i32, i32* %num_points.addr, align 4, !tbaa !6
  %sub46 = sub nsw i32 %54, 1
  %arrayidx47 = getelementptr inbounds [2 x i32], [2 x i32]* %53, i32 %sub46
  %arrayidx48 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx47, i32 0, i32 0
  %55 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  store i32 %55, i32* %i45, align 4, !tbaa !6
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc58, %for.end44
  %56 = load i32, i32* %i45, align 4, !tbaa !6
  %cmp50 = icmp slt i32 %56, 256
  br i1 %cmp50, label %for.body53, label %for.cond.cleanup52

for.cond.cleanup52:                               ; preds = %for.cond49
  store i32 11, i32* %cleanup.dest.slot, align 4
  %57 = bitcast i32* %i45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #5
  br label %for.end60

for.body53:                                       ; preds = %for.cond49
  %58 = load [2 x i32]*, [2 x i32]** %scaling_points.addr, align 4, !tbaa !2
  %59 = load i32, i32* %num_points.addr, align 4, !tbaa !6
  %sub54 = sub nsw i32 %59, 1
  %arrayidx55 = getelementptr inbounds [2 x i32], [2 x i32]* %58, i32 %sub54
  %arrayidx56 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx55, i32 0, i32 1
  %60 = load i32, i32* %arrayidx56, align 4, !tbaa !6
  %61 = load i32*, i32** %scaling_lut.addr, align 4, !tbaa !2
  %62 = load i32, i32* %i45, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds i32, i32* %61, i32 %62
  store i32 %60, i32* %arrayidx57, align 4, !tbaa !6
  br label %for.inc58

for.inc58:                                        ; preds = %for.body53
  %63 = load i32, i32* %i45, align 4, !tbaa !6
  %inc59 = add nsw i32 %63, 1
  store i32 %inc59, i32* %i45, align 4, !tbaa !6
  br label %for.cond49

for.end60:                                        ; preds = %if.then, %for.cond.cleanup52
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal void @init_random_generator(i32 %luma_line, i16 zeroext %seed) #0 {
entry:
  %luma_line.addr = alloca i32, align 4
  %seed.addr = alloca i16, align 2
  %msb = alloca i16, align 2
  %lsb = alloca i16, align 2
  %luma_num = alloca i32, align 4
  store i32 %luma_line, i32* %luma_line.addr, align 4, !tbaa !6
  store i16 %seed, i16* %seed.addr, align 2, !tbaa !26
  %0 = bitcast i16* %msb to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #5
  %1 = load i16, i16* %seed.addr, align 2, !tbaa !26
  %conv = zext i16 %1 to i32
  %shr = ashr i32 %conv, 8
  %and = and i32 %shr, 255
  %conv1 = trunc i32 %and to i16
  store i16 %conv1, i16* %msb, align 2, !tbaa !26
  %2 = bitcast i16* %lsb to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %2) #5
  %3 = load i16, i16* %seed.addr, align 2, !tbaa !26
  %conv2 = zext i16 %3 to i32
  %and3 = and i32 %conv2, 255
  %conv4 = trunc i32 %and3 to i16
  store i16 %conv4, i16* %lsb, align 2, !tbaa !26
  %4 = load i16, i16* %msb, align 2, !tbaa !26
  %conv5 = zext i16 %4 to i32
  %shl = shl i32 %conv5, 8
  %5 = load i16, i16* %lsb, align 2, !tbaa !26
  %conv6 = zext i16 %5 to i32
  %add = add nsw i32 %shl, %conv6
  %conv7 = trunc i32 %add to i16
  store i16 %conv7, i16* @random_register, align 2, !tbaa !26
  %6 = bitcast i32* %luma_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %luma_line.addr, align 4, !tbaa !6
  %shr8 = ashr i32 %7, 5
  store i32 %shr8, i32* %luma_num, align 4, !tbaa !6
  %8 = load i32, i32* %luma_num, align 4, !tbaa !6
  %mul = mul nsw i32 %8, 37
  %add9 = add nsw i32 %mul, 178
  %and10 = and i32 %add9, 255
  %shl11 = shl i32 %and10, 8
  %9 = load i16, i16* @random_register, align 2, !tbaa !26
  %conv12 = zext i16 %9 to i32
  %xor = xor i32 %conv12, %shl11
  %conv13 = trunc i32 %xor to i16
  store i16 %conv13, i16* @random_register, align 2, !tbaa !26
  %10 = load i32, i32* %luma_num, align 4, !tbaa !6
  %mul14 = mul nsw i32 %10, 173
  %add15 = add nsw i32 %mul14, 105
  %and16 = and i32 %add15, 255
  %11 = load i16, i16* @random_register, align 2, !tbaa !26
  %conv17 = zext i16 %11 to i32
  %xor18 = xor i32 %conv17, %and16
  %conv19 = trunc i32 %xor18 to i16
  store i16 %conv19, i16* @random_register, align 2, !tbaa !26
  %12 = bitcast i32* %luma_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  %13 = bitcast i16* %lsb to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %13) #5
  %14 = bitcast i16* %msb to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %14) #5
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_random_number(i32 %bits) #3 {
entry:
  %bits.addr = alloca i32, align 4
  %bit = alloca i16, align 2
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !6
  %0 = bitcast i16* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #5
  %1 = load i16, i16* @random_register, align 2, !tbaa !26
  %conv = zext i16 %1 to i32
  %shr = ashr i32 %conv, 0
  %2 = load i16, i16* @random_register, align 2, !tbaa !26
  %conv1 = zext i16 %2 to i32
  %shr2 = ashr i32 %conv1, 1
  %xor = xor i32 %shr, %shr2
  %3 = load i16, i16* @random_register, align 2, !tbaa !26
  %conv3 = zext i16 %3 to i32
  %shr4 = ashr i32 %conv3, 3
  %xor5 = xor i32 %xor, %shr4
  %4 = load i16, i16* @random_register, align 2, !tbaa !26
  %conv6 = zext i16 %4 to i32
  %shr7 = ashr i32 %conv6, 12
  %xor8 = xor i32 %xor5, %shr7
  %and = and i32 %xor8, 1
  %conv9 = trunc i32 %and to i16
  store i16 %conv9, i16* %bit, align 2, !tbaa !26
  %5 = load i16, i16* @random_register, align 2, !tbaa !26
  %conv10 = zext i16 %5 to i32
  %shr11 = ashr i32 %conv10, 1
  %6 = load i16, i16* %bit, align 2, !tbaa !26
  %conv12 = zext i16 %6 to i32
  %shl = shl i32 %conv12, 15
  %or = or i32 %shr11, %shl
  %conv13 = trunc i32 %or to i16
  store i16 %conv13, i16* @random_register, align 2, !tbaa !26
  %7 = load i16, i16* @random_register, align 2, !tbaa !26
  %conv14 = zext i16 %7 to i32
  %8 = load i32, i32* %bits.addr, align 4, !tbaa !6
  %sub = sub nsw i32 16, %8
  %shr15 = ashr i32 %conv14, %sub
  %9 = load i32, i32* %bits.addr, align 4, !tbaa !6
  %shl16 = shl i32 1, %9
  %sub17 = sub nsw i32 %shl16, 1
  %and18 = and i32 %shr15, %sub17
  %10 = bitcast i16* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %10) #5
  ret i32 %and18
}

; Function Attrs: nounwind
define internal void @ver_boundary_overlap(i32* %left_block, i32 %left_stride, i32* %right_block, i32 %right_stride, i32* %dst_block, i32 %dst_stride, i32 %width, i32 %height) #0 {
entry:
  %left_block.addr = alloca i32*, align 4
  %left_stride.addr = alloca i32, align 4
  %right_block.addr = alloca i32*, align 4
  %right_stride.addr = alloca i32, align 4
  %dst_block.addr = alloca i32*, align 4
  %dst_stride.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  store i32* %left_block, i32** %left_block.addr, align 4, !tbaa !2
  store i32 %left_stride, i32* %left_stride.addr, align 4, !tbaa !6
  store i32* %right_block, i32** %right_block.addr, align 4, !tbaa !2
  store i32 %right_stride, i32* %right_stride.addr, align 4, !tbaa !6
  store i32* %dst_block, i32** %dst_block.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  %0 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %1 = load i32, i32* %height.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32*, i32** %left_block.addr, align 4, !tbaa !2
  %3 = load i32, i32* %2, align 4, !tbaa !6
  %mul = mul nsw i32 %3, 23
  %4 = load i32*, i32** %right_block.addr, align 4, !tbaa !2
  %5 = load i32, i32* %4, align 4, !tbaa !6
  %mul1 = mul nsw i32 %5, 22
  %add = add nsw i32 %mul, %mul1
  %add2 = add nsw i32 %add, 16
  %shr = ashr i32 %add2, 5
  %6 = load i32, i32* @grain_min, align 4, !tbaa !6
  %7 = load i32, i32* @grain_max, align 4, !tbaa !6
  %call = call i32 @clamp(i32 %shr, i32 %6, i32 %7)
  %8 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  store i32 %call, i32* %8, align 4, !tbaa !6
  %9 = load i32, i32* %left_stride.addr, align 4, !tbaa !6
  %10 = load i32*, i32** %left_block.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %10, i32 %9
  store i32* %add.ptr, i32** %left_block.addr, align 4, !tbaa !2
  %11 = load i32, i32* %right_stride.addr, align 4, !tbaa !6
  %12 = load i32*, i32** %right_block.addr, align 4, !tbaa !2
  %add.ptr3 = getelementptr inbounds i32, i32* %12, i32 %11
  store i32* %add.ptr3, i32** %right_block.addr, align 4, !tbaa !2
  %13 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %14 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  %add.ptr4 = getelementptr inbounds i32, i32* %14, i32 %13
  store i32* %add.ptr4, i32** %dst_block.addr, align 4, !tbaa !2
  %15 = load i32, i32* %height.addr, align 4, !tbaa !6
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %height.addr, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end32

if.else:                                          ; preds = %entry
  %16 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp5 = icmp eq i32 %16, 2
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.else
  br label %while.cond7

while.cond7:                                      ; preds = %while.body9, %if.then6
  %17 = load i32, i32* %height.addr, align 4, !tbaa !6
  %tobool8 = icmp ne i32 %17, 0
  br i1 %tobool8, label %while.body9, label %while.end31

while.body9:                                      ; preds = %while.cond7
  %18 = load i32*, i32** %left_block.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %18, i32 0
  %19 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %mul10 = mul nsw i32 27, %19
  %20 = load i32*, i32** %right_block.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %20, i32 0
  %21 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %mul12 = mul nsw i32 17, %21
  %add13 = add nsw i32 %mul10, %mul12
  %add14 = add nsw i32 %add13, 16
  %shr15 = ashr i32 %add14, 5
  %22 = load i32, i32* @grain_min, align 4, !tbaa !6
  %23 = load i32, i32* @grain_max, align 4, !tbaa !6
  %call16 = call i32 @clamp(i32 %shr15, i32 %22, i32 %23)
  %24 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %24, i32 0
  store i32 %call16, i32* %arrayidx17, align 4, !tbaa !6
  %25 = load i32*, i32** %left_block.addr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i32, i32* %25, i32 1
  %26 = load i32, i32* %arrayidx18, align 4, !tbaa !6
  %mul19 = mul nsw i32 17, %26
  %27 = load i32*, i32** %right_block.addr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %27, i32 1
  %28 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %mul21 = mul nsw i32 27, %28
  %add22 = add nsw i32 %mul19, %mul21
  %add23 = add nsw i32 %add22, 16
  %shr24 = ashr i32 %add23, 5
  %29 = load i32, i32* @grain_min, align 4, !tbaa !6
  %30 = load i32, i32* @grain_max, align 4, !tbaa !6
  %call25 = call i32 @clamp(i32 %shr24, i32 %29, i32 %30)
  %31 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i32, i32* %31, i32 1
  store i32 %call25, i32* %arrayidx26, align 4, !tbaa !6
  %32 = load i32, i32* %left_stride.addr, align 4, !tbaa !6
  %33 = load i32*, i32** %left_block.addr, align 4, !tbaa !2
  %add.ptr27 = getelementptr inbounds i32, i32* %33, i32 %32
  store i32* %add.ptr27, i32** %left_block.addr, align 4, !tbaa !2
  %34 = load i32, i32* %right_stride.addr, align 4, !tbaa !6
  %35 = load i32*, i32** %right_block.addr, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds i32, i32* %35, i32 %34
  store i32* %add.ptr28, i32** %right_block.addr, align 4, !tbaa !2
  %36 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %37 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  %add.ptr29 = getelementptr inbounds i32, i32* %37, i32 %36
  store i32* %add.ptr29, i32** %dst_block.addr, align 4, !tbaa !2
  %38 = load i32, i32* %height.addr, align 4, !tbaa !6
  %dec30 = add nsw i32 %38, -1
  store i32 %dec30, i32* %height.addr, align 4, !tbaa !6
  br label %while.cond7

while.end31:                                      ; preds = %while.cond7
  br label %if.end32

if.end:                                           ; preds = %if.else
  br label %if.end32

if.end32:                                         ; preds = %while.end, %while.end31, %if.end
  ret void
}

; Function Attrs: nounwind
define internal void @add_noise_to_block_hbd(%struct.aom_film_grain_t* %params, i16* %luma, i16* %cb, i16* %cr, i32 %luma_stride, i32 %chroma_stride, i32* %luma_grain, i32* %cb_grain, i32* %cr_grain, i32 %luma_grain_stride, i32 %chroma_grain_stride, i32 %half_luma_height, i32 %half_luma_width, i32 %bit_depth, i32 %chroma_subsamp_y, i32 %chroma_subsamp_x, i32 %mc_identity) #0 {
entry:
  %params.addr = alloca %struct.aom_film_grain_t*, align 4
  %luma.addr = alloca i16*, align 4
  %cb.addr = alloca i16*, align 4
  %cr.addr = alloca i16*, align 4
  %luma_stride.addr = alloca i32, align 4
  %chroma_stride.addr = alloca i32, align 4
  %luma_grain.addr = alloca i32*, align 4
  %cb_grain.addr = alloca i32*, align 4
  %cr_grain.addr = alloca i32*, align 4
  %luma_grain_stride.addr = alloca i32, align 4
  %chroma_grain_stride.addr = alloca i32, align 4
  %half_luma_height.addr = alloca i32, align 4
  %half_luma_width.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %chroma_subsamp_y.addr = alloca i32, align 4
  %chroma_subsamp_x.addr = alloca i32, align 4
  %mc_identity.addr = alloca i32, align 4
  %cb_mult = alloca i32, align 4
  %cb_luma_mult = alloca i32, align 4
  %cb_offset = alloca i32, align 4
  %cr_mult = alloca i32, align 4
  %cr_luma_mult = alloca i32, align 4
  %cr_offset = alloca i32, align 4
  %rounding_offset = alloca i32, align 4
  %apply_y = alloca i32, align 4
  %apply_cb = alloca i32, align 4
  %apply_cr = alloca i32, align 4
  %min_luma = alloca i32, align 4
  %max_luma = alloca i32, align 4
  %min_chroma = alloca i32, align 4
  %max_chroma = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %average_luma = alloca i32, align 4
  %i155 = alloca i32, align 4
  %j162 = alloca i32, align 4
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  store i16* %luma, i16** %luma.addr, align 4, !tbaa !2
  store i16* %cb, i16** %cb.addr, align 4, !tbaa !2
  store i16* %cr, i16** %cr.addr, align 4, !tbaa !2
  store i32 %luma_stride, i32* %luma_stride.addr, align 4, !tbaa !6
  store i32 %chroma_stride, i32* %chroma_stride.addr, align 4, !tbaa !6
  store i32* %luma_grain, i32** %luma_grain.addr, align 4, !tbaa !2
  store i32* %cb_grain, i32** %cb_grain.addr, align 4, !tbaa !2
  store i32* %cr_grain, i32** %cr_grain.addr, align 4, !tbaa !2
  store i32 %luma_grain_stride, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  store i32 %chroma_grain_stride, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  store i32 %half_luma_height, i32* %half_luma_height.addr, align 4, !tbaa !6
  store i32 %half_luma_width, i32* %half_luma_width.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_y, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_x, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  store i32 %mc_identity, i32* %mc_identity.addr, align 4, !tbaa !6
  %0 = bitcast i32* %cb_mult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cb_mult1 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %1, i32 0, i32 14
  %2 = load i32, i32* %cb_mult1, align 4, !tbaa !42
  %sub = sub nsw i32 %2, 128
  store i32 %sub, i32* %cb_mult, align 4, !tbaa !6
  %3 = bitcast i32* %cb_luma_mult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cb_luma_mult2 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %4, i32 0, i32 15
  %5 = load i32, i32* %cb_luma_mult2, align 4, !tbaa !43
  %sub3 = sub nsw i32 %5, 128
  store i32 %sub3, i32* %cb_luma_mult, align 4, !tbaa !6
  %6 = bitcast i32* %cb_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cb_offset4 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %7, i32 0, i32 16
  %8 = load i32, i32* %cb_offset4, align 4, !tbaa !44
  %9 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub5 = sub nsw i32 %9, 8
  %shl = shl i32 %8, %sub5
  %10 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %shl6 = shl i32 1, %10
  %sub7 = sub nsw i32 %shl, %shl6
  store i32 %sub7, i32* %cb_offset, align 4, !tbaa !6
  %11 = bitcast i32* %cr_mult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cr_mult8 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %12, i32 0, i32 17
  %13 = load i32, i32* %cr_mult8, align 4, !tbaa !45
  %sub9 = sub nsw i32 %13, 128
  store i32 %sub9, i32* %cr_mult, align 4, !tbaa !6
  %14 = bitcast i32* %cr_luma_mult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cr_luma_mult10 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %15, i32 0, i32 18
  %16 = load i32, i32* %cr_luma_mult10, align 4, !tbaa !46
  %sub11 = sub nsw i32 %16, 128
  store i32 %sub11, i32* %cr_luma_mult, align 4, !tbaa !6
  %17 = bitcast i32* %cr_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cr_offset12 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %18, i32 0, i32 19
  %19 = load i32, i32* %cr_offset12, align 4, !tbaa !47
  %20 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub13 = sub nsw i32 %20, 8
  %shl14 = shl i32 %19, %sub13
  %21 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %shl15 = shl i32 1, %21
  %sub16 = sub nsw i32 %shl14, %shl15
  store i32 %sub16, i32* %cr_offset, align 4, !tbaa !6
  %22 = bitcast i32* %rounding_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %23, i32 0, i32 8
  %24 = load i32, i32* %scaling_shift, align 4, !tbaa !48
  %sub17 = sub nsw i32 %24, 1
  %shl18 = shl i32 1, %sub17
  store i32 %shl18, i32* %rounding_offset, align 4, !tbaa !6
  %25 = bitcast i32* %apply_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  %26 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %26, i32 0, i32 3
  %27 = load i32, i32* %num_y_points, align 4, !tbaa !33
  %cmp = icmp sgt i32 %27, 0
  %28 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 0
  store i32 %cond, i32* %apply_y, align 4, !tbaa !6
  %29 = bitcast i32* %apply_cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #5
  %30 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cb_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %30, i32 0, i32 5
  %31 = load i32, i32* %num_cb_points, align 4, !tbaa !35
  %cmp19 = icmp sgt i32 %31, 0
  br i1 %cmp19, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %32 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %32, i32 0, i32 23
  %33 = load i32, i32* %chroma_scaling_from_luma, align 4, !tbaa !34
  %tobool = icmp ne i32 %33, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %34 = phi i1 [ true, %entry ], [ %tobool, %lor.rhs ]
  %lor.ext = zext i1 %34 to i32
  %cmp20 = icmp sgt i32 %lor.ext, 0
  %35 = zext i1 %cmp20 to i64
  %cond21 = select i1 %cmp20, i32 1, i32 0
  store i32 %cond21, i32* %apply_cb, align 4, !tbaa !6
  %36 = bitcast i32* %apply_cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #5
  %37 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cr_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %37, i32 0, i32 7
  %38 = load i32, i32* %num_cr_points, align 4, !tbaa !36
  %cmp22 = icmp sgt i32 %38, 0
  br i1 %cmp22, label %lor.end26, label %lor.rhs23

lor.rhs23:                                        ; preds = %lor.end
  %39 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma24 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %39, i32 0, i32 23
  %40 = load i32, i32* %chroma_scaling_from_luma24, align 4, !tbaa !34
  %tobool25 = icmp ne i32 %40, 0
  br label %lor.end26

lor.end26:                                        ; preds = %lor.rhs23, %lor.end
  %41 = phi i1 [ true, %lor.end ], [ %tobool25, %lor.rhs23 ]
  %lor.ext27 = zext i1 %41 to i32
  %cmp28 = icmp sgt i32 %lor.ext27, 0
  %42 = zext i1 %cmp28 to i64
  %cond29 = select i1 %cmp28, i32 1, i32 0
  store i32 %cond29, i32* %apply_cr, align 4, !tbaa !6
  %43 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma30 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %43, i32 0, i32 23
  %44 = load i32, i32* %chroma_scaling_from_luma30, align 4, !tbaa !34
  %tobool31 = icmp ne i32 %44, 0
  br i1 %tobool31, label %if.then, label %if.end

if.then:                                          ; preds = %lor.end26
  store i32 0, i32* %cb_mult, align 4, !tbaa !6
  store i32 64, i32* %cb_luma_mult, align 4, !tbaa !6
  store i32 0, i32* %cb_offset, align 4, !tbaa !6
  store i32 0, i32* %cr_mult, align 4, !tbaa !6
  store i32 64, i32* %cr_luma_mult, align 4, !tbaa !6
  store i32 0, i32* %cr_offset, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.end26
  %45 = bitcast i32* %min_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #5
  %46 = bitcast i32* %max_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #5
  %47 = bitcast i32* %min_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  %48 = bitcast i32* %max_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #5
  %49 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %clip_to_restricted_range = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %49, i32 0, i32 21
  %50 = load i32, i32* %clip_to_restricted_range, align 4, !tbaa !49
  %tobool32 = icmp ne i32 %50, 0
  br i1 %tobool32, label %if.then33, label %if.else49

if.then33:                                        ; preds = %if.end
  %51 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub34 = sub nsw i32 %51, 8
  %shl35 = shl i32 16, %sub34
  store i32 %shl35, i32* %min_luma, align 4, !tbaa !6
  %52 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub36 = sub nsw i32 %52, 8
  %shl37 = shl i32 235, %sub36
  store i32 %shl37, i32* %max_luma, align 4, !tbaa !6
  %53 = load i32, i32* %mc_identity.addr, align 4, !tbaa !6
  %tobool38 = icmp ne i32 %53, 0
  br i1 %tobool38, label %if.then39, label %if.else

if.then39:                                        ; preds = %if.then33
  %54 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub40 = sub nsw i32 %54, 8
  %shl41 = shl i32 16, %sub40
  store i32 %shl41, i32* %min_chroma, align 4, !tbaa !6
  %55 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub42 = sub nsw i32 %55, 8
  %shl43 = shl i32 235, %sub42
  store i32 %shl43, i32* %max_chroma, align 4, !tbaa !6
  br label %if.end48

if.else:                                          ; preds = %if.then33
  %56 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub44 = sub nsw i32 %56, 8
  %shl45 = shl i32 16, %sub44
  store i32 %shl45, i32* %min_chroma, align 4, !tbaa !6
  %57 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub46 = sub nsw i32 %57, 8
  %shl47 = shl i32 240, %sub46
  store i32 %shl47, i32* %max_chroma, align 4, !tbaa !6
  br label %if.end48

if.end48:                                         ; preds = %if.else, %if.then39
  br label %if.end53

if.else49:                                        ; preds = %if.end
  store i32 0, i32* %min_chroma, align 4, !tbaa !6
  store i32 0, i32* %min_luma, align 4, !tbaa !6
  %58 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub50 = sub nsw i32 %58, 8
  %shl51 = shl i32 256, %sub50
  %sub52 = sub nsw i32 %shl51, 1
  store i32 %sub52, i32* %max_chroma, align 4, !tbaa !6
  store i32 %sub52, i32* %max_luma, align 4, !tbaa !6
  br label %if.end53

if.end53:                                         ; preds = %if.else49, %if.end48
  %59 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc150, %if.end53
  %60 = load i32, i32* %i, align 4, !tbaa !6
  %61 = load i32, i32* %half_luma_height.addr, align 4, !tbaa !6
  %62 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub54 = sub nsw i32 1, %62
  %shl55 = shl i32 %61, %sub54
  %cmp56 = icmp slt i32 %60, %shl55
  br i1 %cmp56, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %63 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  br label %for.end152

for.body:                                         ; preds = %for.cond
  %64 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc, %for.body
  %65 = load i32, i32* %j, align 4, !tbaa !6
  %66 = load i32, i32* %half_luma_width.addr, align 4, !tbaa !6
  %67 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub58 = sub nsw i32 1, %67
  %shl59 = shl i32 %66, %sub58
  %cmp60 = icmp slt i32 %65, %shl59
  br i1 %cmp60, label %for.body62, label %for.cond.cleanup61

for.cond.cleanup61:                               ; preds = %for.cond57
  store i32 5, i32* %cleanup.dest.slot, align 4
  %68 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  br label %for.end

for.body62:                                       ; preds = %for.cond57
  %69 = bitcast i32* %average_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #5
  store i32 0, i32* %average_luma, align 4, !tbaa !6
  %70 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %tobool63 = icmp ne i32 %70, 0
  br i1 %tobool63, label %if.then64, label %if.else76

if.then64:                                        ; preds = %for.body62
  %71 = load i16*, i16** %luma.addr, align 4, !tbaa !2
  %72 = load i32, i32* %i, align 4, !tbaa !6
  %73 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shl65 = shl i32 %72, %73
  %74 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %shl65, %74
  %75 = load i32, i32* %j, align 4, !tbaa !6
  %76 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shl66 = shl i32 %75, %76
  %add = add nsw i32 %mul, %shl66
  %arrayidx = getelementptr inbounds i16, i16* %71, i32 %add
  %77 = load i16, i16* %arrayidx, align 2, !tbaa !26
  %conv = zext i16 %77 to i32
  %78 = load i16*, i16** %luma.addr, align 4, !tbaa !2
  %79 = load i32, i32* %i, align 4, !tbaa !6
  %80 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shl67 = shl i32 %79, %80
  %81 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul68 = mul nsw i32 %shl67, %81
  %82 = load i32, i32* %j, align 4, !tbaa !6
  %83 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shl69 = shl i32 %82, %83
  %add70 = add nsw i32 %mul68, %shl69
  %add71 = add nsw i32 %add70, 1
  %arrayidx72 = getelementptr inbounds i16, i16* %78, i32 %add71
  %84 = load i16, i16* %arrayidx72, align 2, !tbaa !26
  %conv73 = zext i16 %84 to i32
  %add74 = add nsw i32 %conv, %conv73
  %add75 = add nsw i32 %add74, 1
  %shr = ashr i32 %add75, 1
  store i32 %shr, i32* %average_luma, align 4, !tbaa !6
  br label %if.end82

if.else76:                                        ; preds = %for.body62
  %85 = load i16*, i16** %luma.addr, align 4, !tbaa !2
  %86 = load i32, i32* %i, align 4, !tbaa !6
  %87 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shl77 = shl i32 %86, %87
  %88 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul78 = mul nsw i32 %shl77, %88
  %89 = load i32, i32* %j, align 4, !tbaa !6
  %add79 = add nsw i32 %mul78, %89
  %arrayidx80 = getelementptr inbounds i16, i16* %85, i32 %add79
  %90 = load i16, i16* %arrayidx80, align 2, !tbaa !26
  %conv81 = zext i16 %90 to i32
  store i32 %conv81, i32* %average_luma, align 4, !tbaa !6
  br label %if.end82

if.end82:                                         ; preds = %if.else76, %if.then64
  %91 = load i32, i32* %apply_cb, align 4, !tbaa !6
  %tobool83 = icmp ne i32 %91, 0
  br i1 %tobool83, label %if.then84, label %if.end115

if.then84:                                        ; preds = %if.end82
  %92 = load i16*, i16** %cb.addr, align 4, !tbaa !2
  %93 = load i32, i32* %i, align 4, !tbaa !6
  %94 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul85 = mul nsw i32 %93, %94
  %95 = load i32, i32* %j, align 4, !tbaa !6
  %add86 = add nsw i32 %mul85, %95
  %arrayidx87 = getelementptr inbounds i16, i16* %92, i32 %add86
  %96 = load i16, i16* %arrayidx87, align 2, !tbaa !26
  %conv88 = zext i16 %96 to i32
  %97 = load i32, i32* %average_luma, align 4, !tbaa !6
  %98 = load i32, i32* %cb_luma_mult, align 4, !tbaa !6
  %mul89 = mul nsw i32 %97, %98
  %99 = load i32, i32* %cb_mult, align 4, !tbaa !6
  %100 = load i16*, i16** %cb.addr, align 4, !tbaa !2
  %101 = load i32, i32* %i, align 4, !tbaa !6
  %102 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul90 = mul nsw i32 %101, %102
  %103 = load i32, i32* %j, align 4, !tbaa !6
  %add91 = add nsw i32 %mul90, %103
  %arrayidx92 = getelementptr inbounds i16, i16* %100, i32 %add91
  %104 = load i16, i16* %arrayidx92, align 2, !tbaa !26
  %conv93 = zext i16 %104 to i32
  %mul94 = mul nsw i32 %99, %conv93
  %add95 = add nsw i32 %mul89, %mul94
  %shr96 = ashr i32 %add95, 6
  %105 = load i32, i32* %cb_offset, align 4, !tbaa !6
  %add97 = add nsw i32 %shr96, %105
  %106 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub98 = sub nsw i32 %106, 8
  %shl99 = shl i32 256, %sub98
  %sub100 = sub nsw i32 %shl99, 1
  %call = call i32 @clamp(i32 %add97, i32 0, i32 %sub100)
  %107 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %call101 = call i32 @scale_LUT(i32* getelementptr inbounds ([256 x i32], [256 x i32]* @scaling_lut_cb, i32 0, i32 0), i32 %call, i32 %107)
  %108 = load i32*, i32** %cb_grain.addr, align 4, !tbaa !2
  %109 = load i32, i32* %i, align 4, !tbaa !6
  %110 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul102 = mul nsw i32 %109, %110
  %111 = load i32, i32* %j, align 4, !tbaa !6
  %add103 = add nsw i32 %mul102, %111
  %arrayidx104 = getelementptr inbounds i32, i32* %108, i32 %add103
  %112 = load i32, i32* %arrayidx104, align 4, !tbaa !6
  %mul105 = mul nsw i32 %call101, %112
  %113 = load i32, i32* %rounding_offset, align 4, !tbaa !6
  %add106 = add nsw i32 %mul105, %113
  %114 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_shift107 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %114, i32 0, i32 8
  %115 = load i32, i32* %scaling_shift107, align 4, !tbaa !48
  %shr108 = ashr i32 %add106, %115
  %add109 = add nsw i32 %conv88, %shr108
  %116 = load i32, i32* %min_chroma, align 4, !tbaa !6
  %117 = load i32, i32* %max_chroma, align 4, !tbaa !6
  %call110 = call i32 @clamp(i32 %add109, i32 %116, i32 %117)
  %conv111 = trunc i32 %call110 to i16
  %118 = load i16*, i16** %cb.addr, align 4, !tbaa !2
  %119 = load i32, i32* %i, align 4, !tbaa !6
  %120 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul112 = mul nsw i32 %119, %120
  %121 = load i32, i32* %j, align 4, !tbaa !6
  %add113 = add nsw i32 %mul112, %121
  %arrayidx114 = getelementptr inbounds i16, i16* %118, i32 %add113
  store i16 %conv111, i16* %arrayidx114, align 2, !tbaa !26
  br label %if.end115

if.end115:                                        ; preds = %if.then84, %if.end82
  %122 = load i32, i32* %apply_cr, align 4, !tbaa !6
  %tobool116 = icmp ne i32 %122, 0
  br i1 %tobool116, label %if.then117, label %if.end149

if.then117:                                       ; preds = %if.end115
  %123 = load i16*, i16** %cr.addr, align 4, !tbaa !2
  %124 = load i32, i32* %i, align 4, !tbaa !6
  %125 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul118 = mul nsw i32 %124, %125
  %126 = load i32, i32* %j, align 4, !tbaa !6
  %add119 = add nsw i32 %mul118, %126
  %arrayidx120 = getelementptr inbounds i16, i16* %123, i32 %add119
  %127 = load i16, i16* %arrayidx120, align 2, !tbaa !26
  %conv121 = zext i16 %127 to i32
  %128 = load i32, i32* %average_luma, align 4, !tbaa !6
  %129 = load i32, i32* %cr_luma_mult, align 4, !tbaa !6
  %mul122 = mul nsw i32 %128, %129
  %130 = load i32, i32* %cr_mult, align 4, !tbaa !6
  %131 = load i16*, i16** %cr.addr, align 4, !tbaa !2
  %132 = load i32, i32* %i, align 4, !tbaa !6
  %133 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul123 = mul nsw i32 %132, %133
  %134 = load i32, i32* %j, align 4, !tbaa !6
  %add124 = add nsw i32 %mul123, %134
  %arrayidx125 = getelementptr inbounds i16, i16* %131, i32 %add124
  %135 = load i16, i16* %arrayidx125, align 2, !tbaa !26
  %conv126 = zext i16 %135 to i32
  %mul127 = mul nsw i32 %130, %conv126
  %add128 = add nsw i32 %mul122, %mul127
  %shr129 = ashr i32 %add128, 6
  %136 = load i32, i32* %cr_offset, align 4, !tbaa !6
  %add130 = add nsw i32 %shr129, %136
  %137 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub131 = sub nsw i32 %137, 8
  %shl132 = shl i32 256, %sub131
  %sub133 = sub nsw i32 %shl132, 1
  %call134 = call i32 @clamp(i32 %add130, i32 0, i32 %sub133)
  %138 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %call135 = call i32 @scale_LUT(i32* getelementptr inbounds ([256 x i32], [256 x i32]* @scaling_lut_cr, i32 0, i32 0), i32 %call134, i32 %138)
  %139 = load i32*, i32** %cr_grain.addr, align 4, !tbaa !2
  %140 = load i32, i32* %i, align 4, !tbaa !6
  %141 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul136 = mul nsw i32 %140, %141
  %142 = load i32, i32* %j, align 4, !tbaa !6
  %add137 = add nsw i32 %mul136, %142
  %arrayidx138 = getelementptr inbounds i32, i32* %139, i32 %add137
  %143 = load i32, i32* %arrayidx138, align 4, !tbaa !6
  %mul139 = mul nsw i32 %call135, %143
  %144 = load i32, i32* %rounding_offset, align 4, !tbaa !6
  %add140 = add nsw i32 %mul139, %144
  %145 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_shift141 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %145, i32 0, i32 8
  %146 = load i32, i32* %scaling_shift141, align 4, !tbaa !48
  %shr142 = ashr i32 %add140, %146
  %add143 = add nsw i32 %conv121, %shr142
  %147 = load i32, i32* %min_chroma, align 4, !tbaa !6
  %148 = load i32, i32* %max_chroma, align 4, !tbaa !6
  %call144 = call i32 @clamp(i32 %add143, i32 %147, i32 %148)
  %conv145 = trunc i32 %call144 to i16
  %149 = load i16*, i16** %cr.addr, align 4, !tbaa !2
  %150 = load i32, i32* %i, align 4, !tbaa !6
  %151 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul146 = mul nsw i32 %150, %151
  %152 = load i32, i32* %j, align 4, !tbaa !6
  %add147 = add nsw i32 %mul146, %152
  %arrayidx148 = getelementptr inbounds i16, i16* %149, i32 %add147
  store i16 %conv145, i16* %arrayidx148, align 2, !tbaa !26
  br label %if.end149

if.end149:                                        ; preds = %if.then117, %if.end115
  %153 = bitcast i32* %average_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end149
  %154 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %154, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond57

for.end:                                          ; preds = %for.cond.cleanup61
  br label %for.inc150

for.inc150:                                       ; preds = %for.end
  %155 = load i32, i32* %i, align 4, !tbaa !6
  %inc151 = add nsw i32 %155, 1
  store i32 %inc151, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end152:                                       ; preds = %for.cond.cleanup
  %156 = load i32, i32* %apply_y, align 4, !tbaa !6
  %tobool153 = icmp ne i32 %156, 0
  br i1 %tobool153, label %if.then154, label %if.end197

if.then154:                                       ; preds = %for.end152
  %157 = bitcast i32* %i155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #5
  store i32 0, i32* %i155, align 4, !tbaa !6
  br label %for.cond156

for.cond156:                                      ; preds = %for.inc194, %if.then154
  %158 = load i32, i32* %i155, align 4, !tbaa !6
  %159 = load i32, i32* %half_luma_height.addr, align 4, !tbaa !6
  %shl157 = shl i32 %159, 1
  %cmp158 = icmp slt i32 %158, %shl157
  br i1 %cmp158, label %for.body161, label %for.cond.cleanup160

for.cond.cleanup160:                              ; preds = %for.cond156
  store i32 8, i32* %cleanup.dest.slot, align 4
  %160 = bitcast i32* %i155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #5
  br label %for.end196

for.body161:                                      ; preds = %for.cond156
  %161 = bitcast i32* %j162 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %161) #5
  store i32 0, i32* %j162, align 4, !tbaa !6
  br label %for.cond163

for.cond163:                                      ; preds = %for.inc191, %for.body161
  %162 = load i32, i32* %j162, align 4, !tbaa !6
  %163 = load i32, i32* %half_luma_width.addr, align 4, !tbaa !6
  %shl164 = shl i32 %163, 1
  %cmp165 = icmp slt i32 %162, %shl164
  br i1 %cmp165, label %for.body168, label %for.cond.cleanup167

for.cond.cleanup167:                              ; preds = %for.cond163
  store i32 11, i32* %cleanup.dest.slot, align 4
  %164 = bitcast i32* %j162 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #5
  br label %for.end193

for.body168:                                      ; preds = %for.cond163
  %165 = load i16*, i16** %luma.addr, align 4, !tbaa !2
  %166 = load i32, i32* %i155, align 4, !tbaa !6
  %167 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul169 = mul nsw i32 %166, %167
  %168 = load i32, i32* %j162, align 4, !tbaa !6
  %add170 = add nsw i32 %mul169, %168
  %arrayidx171 = getelementptr inbounds i16, i16* %165, i32 %add170
  %169 = load i16, i16* %arrayidx171, align 2, !tbaa !26
  %conv172 = zext i16 %169 to i32
  %170 = load i16*, i16** %luma.addr, align 4, !tbaa !2
  %171 = load i32, i32* %i155, align 4, !tbaa !6
  %172 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul173 = mul nsw i32 %171, %172
  %173 = load i32, i32* %j162, align 4, !tbaa !6
  %add174 = add nsw i32 %mul173, %173
  %arrayidx175 = getelementptr inbounds i16, i16* %170, i32 %add174
  %174 = load i16, i16* %arrayidx175, align 2, !tbaa !26
  %conv176 = zext i16 %174 to i32
  %175 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %call177 = call i32 @scale_LUT(i32* getelementptr inbounds ([256 x i32], [256 x i32]* @scaling_lut_y, i32 0, i32 0), i32 %conv176, i32 %175)
  %176 = load i32*, i32** %luma_grain.addr, align 4, !tbaa !2
  %177 = load i32, i32* %i155, align 4, !tbaa !6
  %178 = load i32, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  %mul178 = mul nsw i32 %177, %178
  %179 = load i32, i32* %j162, align 4, !tbaa !6
  %add179 = add nsw i32 %mul178, %179
  %arrayidx180 = getelementptr inbounds i32, i32* %176, i32 %add179
  %180 = load i32, i32* %arrayidx180, align 4, !tbaa !6
  %mul181 = mul nsw i32 %call177, %180
  %181 = load i32, i32* %rounding_offset, align 4, !tbaa !6
  %add182 = add nsw i32 %mul181, %181
  %182 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_shift183 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %182, i32 0, i32 8
  %183 = load i32, i32* %scaling_shift183, align 4, !tbaa !48
  %shr184 = ashr i32 %add182, %183
  %add185 = add nsw i32 %conv172, %shr184
  %184 = load i32, i32* %min_luma, align 4, !tbaa !6
  %185 = load i32, i32* %max_luma, align 4, !tbaa !6
  %call186 = call i32 @clamp(i32 %add185, i32 %184, i32 %185)
  %conv187 = trunc i32 %call186 to i16
  %186 = load i16*, i16** %luma.addr, align 4, !tbaa !2
  %187 = load i32, i32* %i155, align 4, !tbaa !6
  %188 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul188 = mul nsw i32 %187, %188
  %189 = load i32, i32* %j162, align 4, !tbaa !6
  %add189 = add nsw i32 %mul188, %189
  %arrayidx190 = getelementptr inbounds i16, i16* %186, i32 %add189
  store i16 %conv187, i16* %arrayidx190, align 2, !tbaa !26
  br label %for.inc191

for.inc191:                                       ; preds = %for.body168
  %190 = load i32, i32* %j162, align 4, !tbaa !6
  %inc192 = add nsw i32 %190, 1
  store i32 %inc192, i32* %j162, align 4, !tbaa !6
  br label %for.cond163

for.end193:                                       ; preds = %for.cond.cleanup167
  br label %for.inc194

for.inc194:                                       ; preds = %for.end193
  %191 = load i32, i32* %i155, align 4, !tbaa !6
  %inc195 = add nsw i32 %191, 1
  store i32 %inc195, i32* %i155, align 4, !tbaa !6
  br label %for.cond156

for.end196:                                       ; preds = %for.cond.cleanup160
  br label %if.end197

if.end197:                                        ; preds = %for.end196, %for.end152
  %192 = bitcast i32* %max_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #5
  %193 = bitcast i32* %min_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #5
  %194 = bitcast i32* %max_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #5
  %195 = bitcast i32* %min_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #5
  %196 = bitcast i32* %apply_cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #5
  %197 = bitcast i32* %apply_cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #5
  %198 = bitcast i32* %apply_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #5
  %199 = bitcast i32* %rounding_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #5
  %200 = bitcast i32* %cr_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #5
  %201 = bitcast i32* %cr_luma_mult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #5
  %202 = bitcast i32* %cr_mult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #5
  %203 = bitcast i32* %cb_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #5
  %204 = bitcast i32* %cb_luma_mult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #5
  %205 = bitcast i32* %cb_mult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #5
  ret void
}

; Function Attrs: nounwind
define internal void @add_noise_to_block(%struct.aom_film_grain_t* %params, i8* %luma, i8* %cb, i8* %cr, i32 %luma_stride, i32 %chroma_stride, i32* %luma_grain, i32* %cb_grain, i32* %cr_grain, i32 %luma_grain_stride, i32 %chroma_grain_stride, i32 %half_luma_height, i32 %half_luma_width, i32 %bit_depth, i32 %chroma_subsamp_y, i32 %chroma_subsamp_x, i32 %mc_identity) #0 {
entry:
  %params.addr = alloca %struct.aom_film_grain_t*, align 4
  %luma.addr = alloca i8*, align 4
  %cb.addr = alloca i8*, align 4
  %cr.addr = alloca i8*, align 4
  %luma_stride.addr = alloca i32, align 4
  %chroma_stride.addr = alloca i32, align 4
  %luma_grain.addr = alloca i32*, align 4
  %cb_grain.addr = alloca i32*, align 4
  %cr_grain.addr = alloca i32*, align 4
  %luma_grain_stride.addr = alloca i32, align 4
  %chroma_grain_stride.addr = alloca i32, align 4
  %half_luma_height.addr = alloca i32, align 4
  %half_luma_width.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %chroma_subsamp_y.addr = alloca i32, align 4
  %chroma_subsamp_x.addr = alloca i32, align 4
  %mc_identity.addr = alloca i32, align 4
  %cb_mult = alloca i32, align 4
  %cb_luma_mult = alloca i32, align 4
  %cb_offset = alloca i32, align 4
  %cr_mult = alloca i32, align 4
  %cr_luma_mult = alloca i32, align 4
  %cr_offset = alloca i32, align 4
  %rounding_offset = alloca i32, align 4
  %apply_y = alloca i32, align 4
  %apply_cb = alloca i32, align 4
  %apply_cr = alloca i32, align 4
  %min_luma = alloca i32, align 4
  %max_luma = alloca i32, align 4
  %min_chroma = alloca i32, align 4
  %max_chroma = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %average_luma = alloca i32, align 4
  %i131 = alloca i32, align 4
  %j138 = alloca i32, align 4
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  store i8* %luma, i8** %luma.addr, align 4, !tbaa !2
  store i8* %cb, i8** %cb.addr, align 4, !tbaa !2
  store i8* %cr, i8** %cr.addr, align 4, !tbaa !2
  store i32 %luma_stride, i32* %luma_stride.addr, align 4, !tbaa !6
  store i32 %chroma_stride, i32* %chroma_stride.addr, align 4, !tbaa !6
  store i32* %luma_grain, i32** %luma_grain.addr, align 4, !tbaa !2
  store i32* %cb_grain, i32** %cb_grain.addr, align 4, !tbaa !2
  store i32* %cr_grain, i32** %cr_grain.addr, align 4, !tbaa !2
  store i32 %luma_grain_stride, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  store i32 %chroma_grain_stride, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  store i32 %half_luma_height, i32* %half_luma_height.addr, align 4, !tbaa !6
  store i32 %half_luma_width, i32* %half_luma_width.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_y, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  store i32 %chroma_subsamp_x, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  store i32 %mc_identity, i32* %mc_identity.addr, align 4, !tbaa !6
  %0 = bitcast i32* %cb_mult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cb_mult1 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %1, i32 0, i32 14
  %2 = load i32, i32* %cb_mult1, align 4, !tbaa !42
  %sub = sub nsw i32 %2, 128
  store i32 %sub, i32* %cb_mult, align 4, !tbaa !6
  %3 = bitcast i32* %cb_luma_mult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cb_luma_mult2 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %4, i32 0, i32 15
  %5 = load i32, i32* %cb_luma_mult2, align 4, !tbaa !43
  %sub3 = sub nsw i32 %5, 128
  store i32 %sub3, i32* %cb_luma_mult, align 4, !tbaa !6
  %6 = bitcast i32* %cb_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cb_offset4 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %7, i32 0, i32 16
  %8 = load i32, i32* %cb_offset4, align 4, !tbaa !44
  %sub5 = sub nsw i32 %8, 256
  store i32 %sub5, i32* %cb_offset, align 4, !tbaa !6
  %9 = bitcast i32* %cr_mult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cr_mult6 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %10, i32 0, i32 17
  %11 = load i32, i32* %cr_mult6, align 4, !tbaa !45
  %sub7 = sub nsw i32 %11, 128
  store i32 %sub7, i32* %cr_mult, align 4, !tbaa !6
  %12 = bitcast i32* %cr_luma_mult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cr_luma_mult8 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %13, i32 0, i32 18
  %14 = load i32, i32* %cr_luma_mult8, align 4, !tbaa !46
  %sub9 = sub nsw i32 %14, 128
  store i32 %sub9, i32* %cr_luma_mult, align 4, !tbaa !6
  %15 = bitcast i32* %cr_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %cr_offset10 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %16, i32 0, i32 19
  %17 = load i32, i32* %cr_offset10, align 4, !tbaa !47
  %sub11 = sub nsw i32 %17, 256
  store i32 %sub11, i32* %cr_offset, align 4, !tbaa !6
  %18 = bitcast i32* %rounding_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %19, i32 0, i32 8
  %20 = load i32, i32* %scaling_shift, align 4, !tbaa !48
  %sub12 = sub nsw i32 %20, 1
  %shl = shl i32 1, %sub12
  store i32 %shl, i32* %rounding_offset, align 4, !tbaa !6
  %21 = bitcast i32* %apply_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #5
  %22 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %22, i32 0, i32 3
  %23 = load i32, i32* %num_y_points, align 4, !tbaa !33
  %cmp = icmp sgt i32 %23, 0
  %24 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 0
  store i32 %cond, i32* %apply_y, align 4, !tbaa !6
  %25 = bitcast i32* %apply_cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  %26 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cb_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %26, i32 0, i32 5
  %27 = load i32, i32* %num_cb_points, align 4, !tbaa !35
  %cmp13 = icmp sgt i32 %27, 0
  br i1 %cmp13, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %28 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %28, i32 0, i32 23
  %29 = load i32, i32* %chroma_scaling_from_luma, align 4, !tbaa !34
  %tobool = icmp ne i32 %29, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %30 = phi i1 [ true, %entry ], [ %tobool, %lor.rhs ]
  %31 = zext i1 %30 to i64
  %cond14 = select i1 %30, i32 1, i32 0
  store i32 %cond14, i32* %apply_cb, align 4, !tbaa !6
  %32 = bitcast i32* %apply_cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  %33 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_cr_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %33, i32 0, i32 7
  %34 = load i32, i32* %num_cr_points, align 4, !tbaa !36
  %cmp15 = icmp sgt i32 %34, 0
  br i1 %cmp15, label %lor.end19, label %lor.rhs16

lor.rhs16:                                        ; preds = %lor.end
  %35 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma17 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %35, i32 0, i32 23
  %36 = load i32, i32* %chroma_scaling_from_luma17, align 4, !tbaa !34
  %tobool18 = icmp ne i32 %36, 0
  br label %lor.end19

lor.end19:                                        ; preds = %lor.rhs16, %lor.end
  %37 = phi i1 [ true, %lor.end ], [ %tobool18, %lor.rhs16 ]
  %38 = zext i1 %37 to i64
  %cond20 = select i1 %37, i32 1, i32 0
  store i32 %cond20, i32* %apply_cr, align 4, !tbaa !6
  %39 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %chroma_scaling_from_luma21 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %39, i32 0, i32 23
  %40 = load i32, i32* %chroma_scaling_from_luma21, align 4, !tbaa !34
  %tobool22 = icmp ne i32 %40, 0
  br i1 %tobool22, label %if.then, label %if.end

if.then:                                          ; preds = %lor.end19
  store i32 0, i32* %cb_mult, align 4, !tbaa !6
  store i32 64, i32* %cb_luma_mult, align 4, !tbaa !6
  store i32 0, i32* %cb_offset, align 4, !tbaa !6
  store i32 0, i32* %cr_mult, align 4, !tbaa !6
  store i32 64, i32* %cr_luma_mult, align 4, !tbaa !6
  store i32 0, i32* %cr_offset, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.end19
  %41 = bitcast i32* %min_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #5
  %42 = bitcast i32* %max_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #5
  %43 = bitcast i32* %min_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #5
  %44 = bitcast i32* %max_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  %45 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %clip_to_restricted_range = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %45, i32 0, i32 21
  %46 = load i32, i32* %clip_to_restricted_range, align 4, !tbaa !49
  %tobool23 = icmp ne i32 %46, 0
  br i1 %tobool23, label %if.then24, label %if.else28

if.then24:                                        ; preds = %if.end
  store i32 16, i32* %min_luma, align 4, !tbaa !6
  store i32 235, i32* %max_luma, align 4, !tbaa !6
  %47 = load i32, i32* %mc_identity.addr, align 4, !tbaa !6
  %tobool25 = icmp ne i32 %47, 0
  br i1 %tobool25, label %if.then26, label %if.else

if.then26:                                        ; preds = %if.then24
  store i32 16, i32* %min_chroma, align 4, !tbaa !6
  store i32 235, i32* %max_chroma, align 4, !tbaa !6
  br label %if.end27

if.else:                                          ; preds = %if.then24
  store i32 16, i32* %min_chroma, align 4, !tbaa !6
  store i32 240, i32* %max_chroma, align 4, !tbaa !6
  br label %if.end27

if.end27:                                         ; preds = %if.else, %if.then26
  br label %if.end29

if.else28:                                        ; preds = %if.end
  store i32 0, i32* %min_chroma, align 4, !tbaa !6
  store i32 0, i32* %min_luma, align 4, !tbaa !6
  store i32 255, i32* %max_chroma, align 4, !tbaa !6
  store i32 255, i32* %max_luma, align 4, !tbaa !6
  br label %if.end29

if.end29:                                         ; preds = %if.else28, %if.end27
  %48 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc126, %if.end29
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %50 = load i32, i32* %half_luma_height.addr, align 4, !tbaa !6
  %51 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %sub30 = sub nsw i32 1, %51
  %shl31 = shl i32 %50, %sub30
  %cmp32 = icmp slt i32 %49, %shl31
  br i1 %cmp32, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %52 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #5
  br label %for.end128

for.body:                                         ; preds = %for.cond
  %53 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #5
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc, %for.body
  %54 = load i32, i32* %j, align 4, !tbaa !6
  %55 = load i32, i32* %half_luma_width.addr, align 4, !tbaa !6
  %56 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %sub34 = sub nsw i32 1, %56
  %shl35 = shl i32 %55, %sub34
  %cmp36 = icmp slt i32 %54, %shl35
  br i1 %cmp36, label %for.body38, label %for.cond.cleanup37

for.cond.cleanup37:                               ; preds = %for.cond33
  store i32 5, i32* %cleanup.dest.slot, align 4
  %57 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #5
  br label %for.end

for.body38:                                       ; preds = %for.cond33
  %58 = bitcast i32* %average_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #5
  store i32 0, i32* %average_luma, align 4, !tbaa !6
  %59 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %tobool39 = icmp ne i32 %59, 0
  br i1 %tobool39, label %if.then40, label %if.else52

if.then40:                                        ; preds = %for.body38
  %60 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %61 = load i32, i32* %i, align 4, !tbaa !6
  %62 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shl41 = shl i32 %61, %62
  %63 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %shl41, %63
  %64 = load i32, i32* %j, align 4, !tbaa !6
  %65 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shl42 = shl i32 %64, %65
  %add = add nsw i32 %mul, %shl42
  %arrayidx = getelementptr inbounds i8, i8* %60, i32 %add
  %66 = load i8, i8* %arrayidx, align 1, !tbaa !28
  %conv = zext i8 %66 to i32
  %67 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %68 = load i32, i32* %i, align 4, !tbaa !6
  %69 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shl43 = shl i32 %68, %69
  %70 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul44 = mul nsw i32 %shl43, %70
  %71 = load i32, i32* %j, align 4, !tbaa !6
  %72 = load i32, i32* %chroma_subsamp_x.addr, align 4, !tbaa !6
  %shl45 = shl i32 %71, %72
  %add46 = add nsw i32 %mul44, %shl45
  %add47 = add nsw i32 %add46, 1
  %arrayidx48 = getelementptr inbounds i8, i8* %67, i32 %add47
  %73 = load i8, i8* %arrayidx48, align 1, !tbaa !28
  %conv49 = zext i8 %73 to i32
  %add50 = add nsw i32 %conv, %conv49
  %add51 = add nsw i32 %add50, 1
  %shr = ashr i32 %add51, 1
  store i32 %shr, i32* %average_luma, align 4, !tbaa !6
  br label %if.end58

if.else52:                                        ; preds = %for.body38
  %74 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %75 = load i32, i32* %i, align 4, !tbaa !6
  %76 = load i32, i32* %chroma_subsamp_y.addr, align 4, !tbaa !6
  %shl53 = shl i32 %75, %76
  %77 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul54 = mul nsw i32 %shl53, %77
  %78 = load i32, i32* %j, align 4, !tbaa !6
  %add55 = add nsw i32 %mul54, %78
  %arrayidx56 = getelementptr inbounds i8, i8* %74, i32 %add55
  %79 = load i8, i8* %arrayidx56, align 1, !tbaa !28
  %conv57 = zext i8 %79 to i32
  store i32 %conv57, i32* %average_luma, align 4, !tbaa !6
  br label %if.end58

if.end58:                                         ; preds = %if.else52, %if.then40
  %80 = load i32, i32* %apply_cb, align 4, !tbaa !6
  %tobool59 = icmp ne i32 %80, 0
  br i1 %tobool59, label %if.then60, label %if.end91

if.then60:                                        ; preds = %if.end58
  %81 = load i8*, i8** %cb.addr, align 4, !tbaa !2
  %82 = load i32, i32* %i, align 4, !tbaa !6
  %83 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul61 = mul nsw i32 %82, %83
  %84 = load i32, i32* %j, align 4, !tbaa !6
  %add62 = add nsw i32 %mul61, %84
  %arrayidx63 = getelementptr inbounds i8, i8* %81, i32 %add62
  %85 = load i8, i8* %arrayidx63, align 1, !tbaa !28
  %conv64 = zext i8 %85 to i32
  %86 = load i32, i32* %average_luma, align 4, !tbaa !6
  %87 = load i32, i32* %cb_luma_mult, align 4, !tbaa !6
  %mul65 = mul nsw i32 %86, %87
  %88 = load i32, i32* %cb_mult, align 4, !tbaa !6
  %89 = load i8*, i8** %cb.addr, align 4, !tbaa !2
  %90 = load i32, i32* %i, align 4, !tbaa !6
  %91 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul66 = mul nsw i32 %90, %91
  %92 = load i32, i32* %j, align 4, !tbaa !6
  %add67 = add nsw i32 %mul66, %92
  %arrayidx68 = getelementptr inbounds i8, i8* %89, i32 %add67
  %93 = load i8, i8* %arrayidx68, align 1, !tbaa !28
  %conv69 = zext i8 %93 to i32
  %mul70 = mul nsw i32 %88, %conv69
  %add71 = add nsw i32 %mul65, %mul70
  %shr72 = ashr i32 %add71, 6
  %94 = load i32, i32* %cb_offset, align 4, !tbaa !6
  %add73 = add nsw i32 %shr72, %94
  %95 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub74 = sub nsw i32 %95, 8
  %shl75 = shl i32 256, %sub74
  %sub76 = sub nsw i32 %shl75, 1
  %call = call i32 @clamp(i32 %add73, i32 0, i32 %sub76)
  %call77 = call i32 @scale_LUT(i32* getelementptr inbounds ([256 x i32], [256 x i32]* @scaling_lut_cb, i32 0, i32 0), i32 %call, i32 8)
  %96 = load i32*, i32** %cb_grain.addr, align 4, !tbaa !2
  %97 = load i32, i32* %i, align 4, !tbaa !6
  %98 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul78 = mul nsw i32 %97, %98
  %99 = load i32, i32* %j, align 4, !tbaa !6
  %add79 = add nsw i32 %mul78, %99
  %arrayidx80 = getelementptr inbounds i32, i32* %96, i32 %add79
  %100 = load i32, i32* %arrayidx80, align 4, !tbaa !6
  %mul81 = mul nsw i32 %call77, %100
  %101 = load i32, i32* %rounding_offset, align 4, !tbaa !6
  %add82 = add nsw i32 %mul81, %101
  %102 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_shift83 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %102, i32 0, i32 8
  %103 = load i32, i32* %scaling_shift83, align 4, !tbaa !48
  %shr84 = ashr i32 %add82, %103
  %add85 = add nsw i32 %conv64, %shr84
  %104 = load i32, i32* %min_chroma, align 4, !tbaa !6
  %105 = load i32, i32* %max_chroma, align 4, !tbaa !6
  %call86 = call i32 @clamp(i32 %add85, i32 %104, i32 %105)
  %conv87 = trunc i32 %call86 to i8
  %106 = load i8*, i8** %cb.addr, align 4, !tbaa !2
  %107 = load i32, i32* %i, align 4, !tbaa !6
  %108 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul88 = mul nsw i32 %107, %108
  %109 = load i32, i32* %j, align 4, !tbaa !6
  %add89 = add nsw i32 %mul88, %109
  %arrayidx90 = getelementptr inbounds i8, i8* %106, i32 %add89
  store i8 %conv87, i8* %arrayidx90, align 1, !tbaa !28
  br label %if.end91

if.end91:                                         ; preds = %if.then60, %if.end58
  %110 = load i32, i32* %apply_cr, align 4, !tbaa !6
  %tobool92 = icmp ne i32 %110, 0
  br i1 %tobool92, label %if.then93, label %if.end125

if.then93:                                        ; preds = %if.end91
  %111 = load i8*, i8** %cr.addr, align 4, !tbaa !2
  %112 = load i32, i32* %i, align 4, !tbaa !6
  %113 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul94 = mul nsw i32 %112, %113
  %114 = load i32, i32* %j, align 4, !tbaa !6
  %add95 = add nsw i32 %mul94, %114
  %arrayidx96 = getelementptr inbounds i8, i8* %111, i32 %add95
  %115 = load i8, i8* %arrayidx96, align 1, !tbaa !28
  %conv97 = zext i8 %115 to i32
  %116 = load i32, i32* %average_luma, align 4, !tbaa !6
  %117 = load i32, i32* %cr_luma_mult, align 4, !tbaa !6
  %mul98 = mul nsw i32 %116, %117
  %118 = load i32, i32* %cr_mult, align 4, !tbaa !6
  %119 = load i8*, i8** %cr.addr, align 4, !tbaa !2
  %120 = load i32, i32* %i, align 4, !tbaa !6
  %121 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul99 = mul nsw i32 %120, %121
  %122 = load i32, i32* %j, align 4, !tbaa !6
  %add100 = add nsw i32 %mul99, %122
  %arrayidx101 = getelementptr inbounds i8, i8* %119, i32 %add100
  %123 = load i8, i8* %arrayidx101, align 1, !tbaa !28
  %conv102 = zext i8 %123 to i32
  %mul103 = mul nsw i32 %118, %conv102
  %add104 = add nsw i32 %mul98, %mul103
  %shr105 = ashr i32 %add104, 6
  %124 = load i32, i32* %cr_offset, align 4, !tbaa !6
  %add106 = add nsw i32 %shr105, %124
  %125 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub107 = sub nsw i32 %125, 8
  %shl108 = shl i32 256, %sub107
  %sub109 = sub nsw i32 %shl108, 1
  %call110 = call i32 @clamp(i32 %add106, i32 0, i32 %sub109)
  %call111 = call i32 @scale_LUT(i32* getelementptr inbounds ([256 x i32], [256 x i32]* @scaling_lut_cr, i32 0, i32 0), i32 %call110, i32 8)
  %126 = load i32*, i32** %cr_grain.addr, align 4, !tbaa !2
  %127 = load i32, i32* %i, align 4, !tbaa !6
  %128 = load i32, i32* %chroma_grain_stride.addr, align 4, !tbaa !6
  %mul112 = mul nsw i32 %127, %128
  %129 = load i32, i32* %j, align 4, !tbaa !6
  %add113 = add nsw i32 %mul112, %129
  %arrayidx114 = getelementptr inbounds i32, i32* %126, i32 %add113
  %130 = load i32, i32* %arrayidx114, align 4, !tbaa !6
  %mul115 = mul nsw i32 %call111, %130
  %131 = load i32, i32* %rounding_offset, align 4, !tbaa !6
  %add116 = add nsw i32 %mul115, %131
  %132 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_shift117 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %132, i32 0, i32 8
  %133 = load i32, i32* %scaling_shift117, align 4, !tbaa !48
  %shr118 = ashr i32 %add116, %133
  %add119 = add nsw i32 %conv97, %shr118
  %134 = load i32, i32* %min_chroma, align 4, !tbaa !6
  %135 = load i32, i32* %max_chroma, align 4, !tbaa !6
  %call120 = call i32 @clamp(i32 %add119, i32 %134, i32 %135)
  %conv121 = trunc i32 %call120 to i8
  %136 = load i8*, i8** %cr.addr, align 4, !tbaa !2
  %137 = load i32, i32* %i, align 4, !tbaa !6
  %138 = load i32, i32* %chroma_stride.addr, align 4, !tbaa !6
  %mul122 = mul nsw i32 %137, %138
  %139 = load i32, i32* %j, align 4, !tbaa !6
  %add123 = add nsw i32 %mul122, %139
  %arrayidx124 = getelementptr inbounds i8, i8* %136, i32 %add123
  store i8 %conv121, i8* %arrayidx124, align 1, !tbaa !28
  br label %if.end125

if.end125:                                        ; preds = %if.then93, %if.end91
  %140 = bitcast i32* %average_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end125
  %141 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %141, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond33

for.end:                                          ; preds = %for.cond.cleanup37
  br label %for.inc126

for.inc126:                                       ; preds = %for.end
  %142 = load i32, i32* %i, align 4, !tbaa !6
  %inc127 = add nsw i32 %142, 1
  store i32 %inc127, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end128:                                       ; preds = %for.cond.cleanup
  %143 = load i32, i32* %apply_y, align 4, !tbaa !6
  %tobool129 = icmp ne i32 %143, 0
  br i1 %tobool129, label %if.then130, label %if.end173

if.then130:                                       ; preds = %for.end128
  %144 = bitcast i32* %i131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #5
  store i32 0, i32* %i131, align 4, !tbaa !6
  br label %for.cond132

for.cond132:                                      ; preds = %for.inc170, %if.then130
  %145 = load i32, i32* %i131, align 4, !tbaa !6
  %146 = load i32, i32* %half_luma_height.addr, align 4, !tbaa !6
  %shl133 = shl i32 %146, 1
  %cmp134 = icmp slt i32 %145, %shl133
  br i1 %cmp134, label %for.body137, label %for.cond.cleanup136

for.cond.cleanup136:                              ; preds = %for.cond132
  store i32 8, i32* %cleanup.dest.slot, align 4
  %147 = bitcast i32* %i131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #5
  br label %for.end172

for.body137:                                      ; preds = %for.cond132
  %148 = bitcast i32* %j138 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #5
  store i32 0, i32* %j138, align 4, !tbaa !6
  br label %for.cond139

for.cond139:                                      ; preds = %for.inc167, %for.body137
  %149 = load i32, i32* %j138, align 4, !tbaa !6
  %150 = load i32, i32* %half_luma_width.addr, align 4, !tbaa !6
  %shl140 = shl i32 %150, 1
  %cmp141 = icmp slt i32 %149, %shl140
  br i1 %cmp141, label %for.body144, label %for.cond.cleanup143

for.cond.cleanup143:                              ; preds = %for.cond139
  store i32 11, i32* %cleanup.dest.slot, align 4
  %151 = bitcast i32* %j138 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #5
  br label %for.end169

for.body144:                                      ; preds = %for.cond139
  %152 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %153 = load i32, i32* %i131, align 4, !tbaa !6
  %154 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul145 = mul nsw i32 %153, %154
  %155 = load i32, i32* %j138, align 4, !tbaa !6
  %add146 = add nsw i32 %mul145, %155
  %arrayidx147 = getelementptr inbounds i8, i8* %152, i32 %add146
  %156 = load i8, i8* %arrayidx147, align 1, !tbaa !28
  %conv148 = zext i8 %156 to i32
  %157 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %158 = load i32, i32* %i131, align 4, !tbaa !6
  %159 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul149 = mul nsw i32 %158, %159
  %160 = load i32, i32* %j138, align 4, !tbaa !6
  %add150 = add nsw i32 %mul149, %160
  %arrayidx151 = getelementptr inbounds i8, i8* %157, i32 %add150
  %161 = load i8, i8* %arrayidx151, align 1, !tbaa !28
  %conv152 = zext i8 %161 to i32
  %call153 = call i32 @scale_LUT(i32* getelementptr inbounds ([256 x i32], [256 x i32]* @scaling_lut_y, i32 0, i32 0), i32 %conv152, i32 8)
  %162 = load i32*, i32** %luma_grain.addr, align 4, !tbaa !2
  %163 = load i32, i32* %i131, align 4, !tbaa !6
  %164 = load i32, i32* %luma_grain_stride.addr, align 4, !tbaa !6
  %mul154 = mul nsw i32 %163, %164
  %165 = load i32, i32* %j138, align 4, !tbaa !6
  %add155 = add nsw i32 %mul154, %165
  %arrayidx156 = getelementptr inbounds i32, i32* %162, i32 %add155
  %166 = load i32, i32* %arrayidx156, align 4, !tbaa !6
  %mul157 = mul nsw i32 %call153, %166
  %167 = load i32, i32* %rounding_offset, align 4, !tbaa !6
  %add158 = add nsw i32 %mul157, %167
  %168 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %scaling_shift159 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %168, i32 0, i32 8
  %169 = load i32, i32* %scaling_shift159, align 4, !tbaa !48
  %shr160 = ashr i32 %add158, %169
  %add161 = add nsw i32 %conv148, %shr160
  %170 = load i32, i32* %min_luma, align 4, !tbaa !6
  %171 = load i32, i32* %max_luma, align 4, !tbaa !6
  %call162 = call i32 @clamp(i32 %add161, i32 %170, i32 %171)
  %conv163 = trunc i32 %call162 to i8
  %172 = load i8*, i8** %luma.addr, align 4, !tbaa !2
  %173 = load i32, i32* %i131, align 4, !tbaa !6
  %174 = load i32, i32* %luma_stride.addr, align 4, !tbaa !6
  %mul164 = mul nsw i32 %173, %174
  %175 = load i32, i32* %j138, align 4, !tbaa !6
  %add165 = add nsw i32 %mul164, %175
  %arrayidx166 = getelementptr inbounds i8, i8* %172, i32 %add165
  store i8 %conv163, i8* %arrayidx166, align 1, !tbaa !28
  br label %for.inc167

for.inc167:                                       ; preds = %for.body144
  %176 = load i32, i32* %j138, align 4, !tbaa !6
  %inc168 = add nsw i32 %176, 1
  store i32 %inc168, i32* %j138, align 4, !tbaa !6
  br label %for.cond139

for.end169:                                       ; preds = %for.cond.cleanup143
  br label %for.inc170

for.inc170:                                       ; preds = %for.end169
  %177 = load i32, i32* %i131, align 4, !tbaa !6
  %inc171 = add nsw i32 %177, 1
  store i32 %inc171, i32* %i131, align 4, !tbaa !6
  br label %for.cond132

for.end172:                                       ; preds = %for.cond.cleanup136
  br label %if.end173

if.end173:                                        ; preds = %for.end172, %for.end128
  %178 = bitcast i32* %max_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #5
  %179 = bitcast i32* %min_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #5
  %180 = bitcast i32* %max_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #5
  %181 = bitcast i32* %min_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #5
  %182 = bitcast i32* %apply_cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #5
  %183 = bitcast i32* %apply_cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #5
  %184 = bitcast i32* %apply_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #5
  %185 = bitcast i32* %rounding_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #5
  %186 = bitcast i32* %cr_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #5
  %187 = bitcast i32* %cr_luma_mult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #5
  %188 = bitcast i32* %cr_mult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #5
  %189 = bitcast i32* %cb_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #5
  %190 = bitcast i32* %cb_luma_mult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #5
  %191 = bitcast i32* %cb_mult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #5
  ret void
}

; Function Attrs: nounwind
define internal void @hor_boundary_overlap(i32* %top_block, i32 %top_stride, i32* %bottom_block, i32 %bottom_stride, i32* %dst_block, i32 %dst_stride, i32 %width, i32 %height) #0 {
entry:
  %top_block.addr = alloca i32*, align 4
  %top_stride.addr = alloca i32, align 4
  %bottom_block.addr = alloca i32*, align 4
  %bottom_stride.addr = alloca i32, align 4
  %dst_block.addr = alloca i32*, align 4
  %dst_stride.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  store i32* %top_block, i32** %top_block.addr, align 4, !tbaa !2
  store i32 %top_stride, i32* %top_stride.addr, align 4, !tbaa !6
  store i32* %bottom_block, i32** %bottom_block.addr, align 4, !tbaa !2
  store i32 %bottom_stride, i32* %bottom_stride.addr, align 4, !tbaa !6
  store i32* %dst_block, i32** %dst_block.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  %0 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %1 = load i32, i32* %width.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32*, i32** %top_block.addr, align 4, !tbaa !2
  %3 = load i32, i32* %2, align 4, !tbaa !6
  %mul = mul nsw i32 %3, 23
  %4 = load i32*, i32** %bottom_block.addr, align 4, !tbaa !2
  %5 = load i32, i32* %4, align 4, !tbaa !6
  %mul1 = mul nsw i32 %5, 22
  %add = add nsw i32 %mul, %mul1
  %add2 = add nsw i32 %add, 16
  %shr = ashr i32 %add2, 5
  %6 = load i32, i32* @grain_min, align 4, !tbaa !6
  %7 = load i32, i32* @grain_max, align 4, !tbaa !6
  %call = call i32 @clamp(i32 %shr, i32 %6, i32 %7)
  %8 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  store i32 %call, i32* %8, align 4, !tbaa !6
  %9 = load i32*, i32** %top_block.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %9, i32 1
  store i32* %incdec.ptr, i32** %top_block.addr, align 4, !tbaa !2
  %10 = load i32*, i32** %bottom_block.addr, align 4, !tbaa !2
  %incdec.ptr3 = getelementptr inbounds i32, i32* %10, i32 1
  store i32* %incdec.ptr3, i32** %bottom_block.addr, align 4, !tbaa !2
  %11 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  %incdec.ptr4 = getelementptr inbounds i32, i32* %11, i32 1
  store i32* %incdec.ptr4, i32** %dst_block.addr, align 4, !tbaa !2
  %12 = load i32, i32* %width.addr, align 4, !tbaa !6
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %width.addr, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end32

if.else:                                          ; preds = %entry
  %13 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp5 = icmp eq i32 %13, 2
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.else
  br label %while.cond7

while.cond7:                                      ; preds = %while.body9, %if.then6
  %14 = load i32, i32* %width.addr, align 4, !tbaa !6
  %tobool8 = icmp ne i32 %14, 0
  br i1 %tobool8, label %while.body9, label %while.end31

while.body9:                                      ; preds = %while.cond7
  %15 = load i32*, i32** %top_block.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 0
  %16 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %mul10 = mul nsw i32 27, %16
  %17 = load i32*, i32** %bottom_block.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %17, i32 0
  %18 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %mul12 = mul nsw i32 17, %18
  %add13 = add nsw i32 %mul10, %mul12
  %add14 = add nsw i32 %add13, 16
  %shr15 = ashr i32 %add14, 5
  %19 = load i32, i32* @grain_min, align 4, !tbaa !6
  %20 = load i32, i32* @grain_max, align 4, !tbaa !6
  %call16 = call i32 @clamp(i32 %shr15, i32 %19, i32 %20)
  %21 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %21, i32 0
  store i32 %call16, i32* %arrayidx17, align 4, !tbaa !6
  %22 = load i32*, i32** %top_block.addr, align 4, !tbaa !2
  %23 = load i32, i32* %top_stride.addr, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds i32, i32* %22, i32 %23
  %24 = load i32, i32* %arrayidx18, align 4, !tbaa !6
  %mul19 = mul nsw i32 17, %24
  %25 = load i32*, i32** %bottom_block.addr, align 4, !tbaa !2
  %26 = load i32, i32* %bottom_stride.addr, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds i32, i32* %25, i32 %26
  %27 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %mul21 = mul nsw i32 27, %27
  %add22 = add nsw i32 %mul19, %mul21
  %add23 = add nsw i32 %add22, 16
  %shr24 = ashr i32 %add23, 5
  %28 = load i32, i32* @grain_min, align 4, !tbaa !6
  %29 = load i32, i32* @grain_max, align 4, !tbaa !6
  %call25 = call i32 @clamp(i32 %shr24, i32 %28, i32 %29)
  %30 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  %31 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds i32, i32* %30, i32 %31
  store i32 %call25, i32* %arrayidx26, align 4, !tbaa !6
  %32 = load i32*, i32** %top_block.addr, align 4, !tbaa !2
  %incdec.ptr27 = getelementptr inbounds i32, i32* %32, i32 1
  store i32* %incdec.ptr27, i32** %top_block.addr, align 4, !tbaa !2
  %33 = load i32*, i32** %bottom_block.addr, align 4, !tbaa !2
  %incdec.ptr28 = getelementptr inbounds i32, i32* %33, i32 1
  store i32* %incdec.ptr28, i32** %bottom_block.addr, align 4, !tbaa !2
  %34 = load i32*, i32** %dst_block.addr, align 4, !tbaa !2
  %incdec.ptr29 = getelementptr inbounds i32, i32* %34, i32 1
  store i32* %incdec.ptr29, i32** %dst_block.addr, align 4, !tbaa !2
  %35 = load i32, i32* %width.addr, align 4, !tbaa !6
  %dec30 = add nsw i32 %35, -1
  store i32 %dec30, i32* %width.addr, align 4, !tbaa !6
  br label %while.cond7

while.end31:                                      ; preds = %while.cond7
  br label %if.end32

if.end:                                           ; preds = %if.else
  br label %if.end32

if.end32:                                         ; preds = %while.end, %while.end31, %if.end
  ret void
}

; Function Attrs: nounwind
define internal void @copy_area(i32* %src, i32 %src_stride, i32* %dst, i32 %dst_stride, i32 %width, i32 %height) #0 {
entry:
  %src.addr = alloca i32*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i32*, align 4
  %dst_stride.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  store i32* %src, i32** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i32* %dst, i32** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %height.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %2 = bitcast i32* %1 to i8*
  %3 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %4 = bitcast i32* %3 to i8*
  %5 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul = mul i32 %5, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %4, i32 %mul, i1 false)
  %6 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %7 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %7, i32 %6
  store i32* %add.ptr, i32** %src.addr, align 4, !tbaa !2
  %8 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %9 = load i32*, i32** %dst.addr, align 4, !tbaa !2
  %add.ptr1 = getelementptr inbounds i32, i32* %9, i32 %8
  store i32* %add.ptr1, i32** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %height.addr, align 4, !tbaa !6
  %dec = add nsw i32 %10, -1
  store i32 %dec, i32* %height.addr, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: nounwind
define internal void @dealloc_arrays(%struct.aom_film_grain_t* %params, i32*** %pred_pos_luma, i32*** %pred_pos_chroma, i32** %luma_grain_block, i32** %cb_grain_block, i32** %cr_grain_block, i32** %y_line_buf, i32** %cb_line_buf, i32** %cr_line_buf, i32** %y_col_buf, i32** %cb_col_buf, i32** %cr_col_buf) #0 {
entry:
  %params.addr = alloca %struct.aom_film_grain_t*, align 4
  %pred_pos_luma.addr = alloca i32***, align 4
  %pred_pos_chroma.addr = alloca i32***, align 4
  %luma_grain_block.addr = alloca i32**, align 4
  %cb_grain_block.addr = alloca i32**, align 4
  %cr_grain_block.addr = alloca i32**, align 4
  %y_line_buf.addr = alloca i32**, align 4
  %cb_line_buf.addr = alloca i32**, align 4
  %cr_line_buf.addr = alloca i32**, align 4
  %y_col_buf.addr = alloca i32**, align 4
  %cb_col_buf.addr = alloca i32**, align 4
  %cr_col_buf.addr = alloca i32**, align 4
  %num_pos_luma = alloca i32, align 4
  %num_pos_chroma = alloca i32, align 4
  %row = alloca i32, align 4
  %row5 = alloca i32, align 4
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  store i32*** %pred_pos_luma, i32**** %pred_pos_luma.addr, align 4, !tbaa !2
  store i32*** %pred_pos_chroma, i32**** %pred_pos_chroma.addr, align 4, !tbaa !2
  store i32** %luma_grain_block, i32*** %luma_grain_block.addr, align 4, !tbaa !2
  store i32** %cb_grain_block, i32*** %cb_grain_block.addr, align 4, !tbaa !2
  store i32** %cr_grain_block, i32*** %cr_grain_block.addr, align 4, !tbaa !2
  store i32** %y_line_buf, i32*** %y_line_buf.addr, align 4, !tbaa !2
  store i32** %cb_line_buf, i32*** %cb_line_buf.addr, align 4, !tbaa !2
  store i32** %cr_line_buf, i32*** %cr_line_buf.addr, align 4, !tbaa !2
  store i32** %y_col_buf, i32*** %y_col_buf.addr, align 4, !tbaa !2
  store i32** %cb_col_buf, i32*** %cb_col_buf.addr, align 4, !tbaa !2
  store i32** %cr_col_buf, i32*** %cr_col_buf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %num_pos_luma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %1, i32 0, i32 9
  %2 = load i32, i32* %ar_coeff_lag, align 4, !tbaa !37
  %mul = mul nsw i32 2, %2
  %3 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %ar_coeff_lag1 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %3, i32 0, i32 9
  %4 = load i32, i32* %ar_coeff_lag1, align 4, !tbaa !37
  %add = add nsw i32 %4, 1
  %mul2 = mul nsw i32 %mul, %add
  store i32 %mul2, i32* %num_pos_luma, align 4, !tbaa !6
  %5 = bitcast i32* %num_pos_chroma to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %num_pos_luma, align 4, !tbaa !6
  store i32 %6, i32* %num_pos_chroma, align 4, !tbaa !6
  %7 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %params.addr, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %7, i32 0, i32 3
  %8 = load i32, i32* %num_y_points, align 4, !tbaa !33
  %cmp = icmp sgt i32 %8, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %9 = load i32, i32* %num_pos_chroma, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %num_pos_chroma, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %11 = load i32, i32* %row, align 4, !tbaa !6
  %12 = load i32, i32* %num_pos_luma, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %11, %12
  br i1 %cmp3, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i32***, i32**** %pred_pos_luma.addr, align 4, !tbaa !2
  %15 = load i32**, i32*** %14, align 4, !tbaa !2
  %16 = load i32, i32* %row, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32*, i32** %15, i32 %16
  %17 = load i32*, i32** %arrayidx, align 4, !tbaa !2
  %18 = bitcast i32* %17 to i8*
  call void @aom_free(i8* %18)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %row, align 4, !tbaa !6
  %inc4 = add nsw i32 %19, 1
  store i32 %inc4, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %20 = load i32***, i32**** %pred_pos_luma.addr, align 4, !tbaa !2
  %21 = load i32**, i32*** %20, align 4, !tbaa !2
  %22 = bitcast i32** %21 to i8*
  call void @aom_free(i8* %22)
  %23 = bitcast i32* %row5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  store i32 0, i32* %row5, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %for.end
  %24 = load i32, i32* %row5, align 4, !tbaa !6
  %25 = load i32, i32* %num_pos_chroma, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %24, %25
  br i1 %cmp7, label %for.body9, label %for.cond.cleanup8

for.cond.cleanup8:                                ; preds = %for.cond6
  %26 = bitcast i32* %row5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  br label %for.end13

for.body9:                                        ; preds = %for.cond6
  %27 = load i32***, i32**** %pred_pos_chroma.addr, align 4, !tbaa !2
  %28 = load i32**, i32*** %27, align 4, !tbaa !2
  %29 = load i32, i32* %row5, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds i32*, i32** %28, i32 %29
  %30 = load i32*, i32** %arrayidx10, align 4, !tbaa !2
  %31 = bitcast i32* %30 to i8*
  call void @aom_free(i8* %31)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body9
  %32 = load i32, i32* %row5, align 4, !tbaa !6
  %inc12 = add nsw i32 %32, 1
  store i32 %inc12, i32* %row5, align 4, !tbaa !6
  br label %for.cond6

for.end13:                                        ; preds = %for.cond.cleanup8
  %33 = load i32***, i32**** %pred_pos_chroma.addr, align 4, !tbaa !2
  %34 = load i32**, i32*** %33, align 4, !tbaa !2
  %35 = bitcast i32** %34 to i8*
  call void @aom_free(i8* %35)
  %36 = load i32**, i32*** %y_line_buf.addr, align 4, !tbaa !2
  %37 = load i32*, i32** %36, align 4, !tbaa !2
  %38 = bitcast i32* %37 to i8*
  call void @aom_free(i8* %38)
  %39 = load i32**, i32*** %cb_line_buf.addr, align 4, !tbaa !2
  %40 = load i32*, i32** %39, align 4, !tbaa !2
  %41 = bitcast i32* %40 to i8*
  call void @aom_free(i8* %41)
  %42 = load i32**, i32*** %cr_line_buf.addr, align 4, !tbaa !2
  %43 = load i32*, i32** %42, align 4, !tbaa !2
  %44 = bitcast i32* %43 to i8*
  call void @aom_free(i8* %44)
  %45 = load i32**, i32*** %y_col_buf.addr, align 4, !tbaa !2
  %46 = load i32*, i32** %45, align 4, !tbaa !2
  %47 = bitcast i32* %46 to i8*
  call void @aom_free(i8* %47)
  %48 = load i32**, i32*** %cb_col_buf.addr, align 4, !tbaa !2
  %49 = load i32*, i32** %48, align 4, !tbaa !2
  %50 = bitcast i32* %49 to i8*
  call void @aom_free(i8* %50)
  %51 = load i32**, i32*** %cr_col_buf.addr, align 4, !tbaa !2
  %52 = load i32*, i32** %51, align 4, !tbaa !2
  %53 = bitcast i32* %52 to i8*
  call void @aom_free(i8* %53)
  %54 = load i32**, i32*** %luma_grain_block.addr, align 4, !tbaa !2
  %55 = load i32*, i32** %54, align 4, !tbaa !2
  %56 = bitcast i32* %55 to i8*
  call void @aom_free(i8* %56)
  %57 = load i32**, i32*** %cb_grain_block.addr, align 4, !tbaa !2
  %58 = load i32*, i32** %57, align 4, !tbaa !2
  %59 = bitcast i32* %58 to i8*
  call void @aom_free(i8* %59)
  %60 = load i32**, i32*** %cr_grain_block.addr, align 4, !tbaa !2
  %61 = load i32*, i32** %60, align 4, !tbaa !2
  %62 = bitcast i32* %61 to i8*
  call void @aom_free(i8* %62)
  %63 = bitcast i32* %num_pos_chroma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  %64 = bitcast i32* %num_pos_luma to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

declare i8* @aom_malloc(i32) #2

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #3 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: nounwind
define internal i32 @scale_LUT(i32* %scaling_lut, i32 %index, i32 %bit_depth) #0 {
entry:
  %retval = alloca i32, align 4
  %scaling_lut.addr = alloca i32*, align 4
  %index.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %x = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32* %scaling_lut, i32** %scaling_lut.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  %0 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %index.addr, align 4, !tbaa !6
  %2 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 8
  %shr = ashr i32 %1, %sub
  store i32 %shr, i32* %x, align 4, !tbaa !6
  %3 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 %3, 8
  %tobool = icmp ne i32 %sub1, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %4 = load i32, i32* %x, align 4, !tbaa !6
  %cmp = icmp eq i32 %4, 255
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %5 = load i32*, i32** %scaling_lut.addr, align 4, !tbaa !2
  %6 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  %7 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %lor.lhs.false
  %8 = load i32*, i32** %scaling_lut.addr, align 4, !tbaa !2
  %9 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  %11 = load i32*, i32** %scaling_lut.addr, align 4, !tbaa !2
  %12 = load i32, i32* %x, align 4, !tbaa !6
  %add = add nsw i32 %12, 1
  %arrayidx3 = getelementptr inbounds i32, i32* %11, i32 %add
  %13 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %14 = load i32*, i32** %scaling_lut.addr, align 4, !tbaa !2
  %15 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %sub5 = sub nsw i32 %13, %16
  %17 = load i32, i32* %index.addr, align 4, !tbaa !6
  %18 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub6 = sub nsw i32 %18, 8
  %shl = shl i32 1, %sub6
  %sub7 = sub nsw i32 %shl, 1
  %and = and i32 %17, %sub7
  %mul = mul nsw i32 %sub5, %and
  %19 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub8 = sub nsw i32 %19, 9
  %shl9 = shl i32 1, %sub8
  %add10 = add nsw i32 %mul, %shl9
  %20 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %sub11 = sub nsw i32 %20, 8
  %shr12 = ashr i32 %add10, %sub11
  %add13 = add nsw i32 %10, %shr12
  store i32 %add13, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %21 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

declare void @aom_free(i8*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !4, i64 12}
!9 = !{!"aom_image", !4, i64 0, !4, i64 4, !4, i64 8, !4, i64 12, !7, i64 16, !4, i64 20, !4, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !4, i64 64, !4, i64 76, !10, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !3, i64 104, !3, i64 108, !7, i64 112, !7, i64 116, !3, i64 120, !3, i64 124}
!10 = !{!"long", !4, i64 0}
!11 = !{!9, !4, i64 0}
!12 = !{!9, !7, i64 36}
!13 = !{!9, !7, i64 48}
!14 = !{!9, !7, i64 52}
!15 = !{!9, !7, i64 40}
!16 = !{!9, !7, i64 44}
!17 = !{!9, !4, i64 4}
!18 = !{!9, !4, i64 8}
!19 = !{!9, !7, i64 16}
!20 = !{!9, !4, i64 20}
!21 = !{!9, !4, i64 24}
!22 = !{!9, !7, i64 56}
!23 = !{!9, !7, i64 60}
!24 = !{!9, !7, i64 96}
!25 = !{!9, !7, i64 100}
!26 = !{!27, !27, i64 0}
!27 = !{!"short", !4, i64 0}
!28 = !{!4, !4, i64 0}
!29 = !{!30, !27, i64 644}
!30 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !27, i64 644}
!31 = !{!30, !7, i64 624}
!32 = !{!30, !7, i64 632}
!33 = !{!30, !7, i64 120}
!34 = !{!30, !7, i64 636}
!35 = !{!30, !7, i64 204}
!36 = !{!30, !7, i64 288}
!37 = !{!30, !7, i64 296}
!38 = !{!30, !7, i64 640}
!39 = !{!30, !7, i64 596}
!40 = !{!41, !41, i64 0}
!41 = !{!"long long", !4, i64 0}
!42 = !{!30, !7, i64 600}
!43 = !{!30, !7, i64 604}
!44 = !{!30, !7, i64 608}
!45 = !{!30, !7, i64 612}
!46 = !{!30, !7, i64 616}
!47 = !{!30, !7, i64 620}
!48 = !{!30, !7, i64 292}
!49 = !{!30, !7, i64 628}
