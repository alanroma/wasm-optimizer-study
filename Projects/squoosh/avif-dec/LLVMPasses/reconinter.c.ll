; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/reconinter.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/reconinter.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.wedge_code_type = type { i8, i32, i32 }
%struct.wedge_params_type = type { i32, %struct.wedge_code_type*, i8*, [16 x i8*]* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%union.int_mv = type { i32 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.WarpTypesAllowed = type { i32, i32 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.InterPredParams = type { i32, i32, %struct.WarpedMotionParams, %struct.ConvolveParams, [2 x %struct.InterpFilterParams*], i32, i32, i32, i32, %struct.buf_2d, i32, i32, %struct.scale_factors*, i32, i32, %struct.INTERINTER_COMPOUND_DATA, i8, i32 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.InterpFilters = type { i16, i16 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.SubpelParams = type { i32, i32, i32, i32 }
%struct.mv = type { i16, i16 }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.obmc_inter_pred_ctxt = type { i8**, i32* }
%struct.build_prediction_ctxt = type { %struct.AV1Common*, i8**, i32*, i32*, i32*, i32 }
%struct.BUFFER_SET = type { [3 x i8*], [3 x i32] }

@default_warp_params = internal constant %struct.WarpedMotionParams { [8 x i32] [i32 0, i32 0, i32 65536, i32 0, i32 0, i32 65536, i32 0, i32 0], i16 0, i16 0, i16 0, i16 0, i8 0, i8 0 }, align 4
@av1_intrabc_filter_params = internal constant %struct.InterpFilterParams { i16* getelementptr inbounds ([32 x i16], [32 x i16]* bitcast (<{ [18 x i16], [14 x i16] }>* @av1_intrabc_bilinear_filter to [32 x i16]*), i32 0, i32 0), i16 2, i16 0, i8 3 }, align 4
@wedge_codebook_16_heqw = internal constant [16 x %struct.wedge_code_type] [%struct.wedge_code_type { i8 2, i32 4, i32 4 }, %struct.wedge_code_type { i8 3, i32 4, i32 4 }, %struct.wedge_code_type { i8 4, i32 4, i32 4 }, %struct.wedge_code_type { i8 5, i32 4, i32 4 }, %struct.wedge_code_type { i8 0, i32 4, i32 2 }, %struct.wedge_code_type { i8 0, i32 4, i32 6 }, %struct.wedge_code_type { i8 1, i32 2, i32 4 }, %struct.wedge_code_type { i8 1, i32 6, i32 4 }, %struct.wedge_code_type { i8 2, i32 4, i32 2 }, %struct.wedge_code_type { i8 2, i32 4, i32 6 }, %struct.wedge_code_type { i8 5, i32 4, i32 2 }, %struct.wedge_code_type { i8 5, i32 4, i32 6 }, %struct.wedge_code_type { i8 3, i32 2, i32 4 }, %struct.wedge_code_type { i8 3, i32 6, i32 4 }, %struct.wedge_code_type { i8 4, i32 2, i32 4 }, %struct.wedge_code_type { i8 4, i32 6, i32 4 }], align 16
@wedge_signflip_lookup = internal global [22 x [16 x i8]] [[16 x i8] zeroinitializer, [16 x i8] zeroinitializer, [16 x i8] zeroinitializer, [16 x i8] c"\01\01\01\01\01\01\01\01\01\01\00\01\01\01\00\01", [16 x i8] c"\01\01\01\01\00\01\01\01\01\01\00\01\01\01\00\01", [16 x i8] c"\01\01\01\01\00\01\01\01\01\01\00\01\01\01\00\01", [16 x i8] c"\01\01\01\01\01\01\01\01\01\01\00\01\01\01\00\01", [16 x i8] c"\01\01\01\01\00\01\01\01\01\01\00\01\01\01\00\01", [16 x i8] c"\01\01\01\01\00\01\01\01\01\01\00\01\01\01\00\01", [16 x i8] c"\01\01\01\01\01\01\01\01\01\01\00\01\01\01\00\01", [16 x i8] zeroinitializer, [16 x i8] zeroinitializer, [16 x i8] zeroinitializer, [16 x i8] zeroinitializer, [16 x i8] zeroinitializer, [16 x i8] zeroinitializer, [16 x i8] zeroinitializer, [16 x i8] zeroinitializer, [16 x i8] c"\01\01\01\01\00\01\01\01\00\01\00\01\01\01\00\01", [16 x i8] c"\01\01\01\01\00\01\01\01\01\01\00\01\00\01\00\01", [16 x i8] zeroinitializer, [16 x i8] zeroinitializer], align 16
@wedge_masks = internal global [22 x [2 x [16 x i8*]]] zeroinitializer, align 16
@wedge_codebook_16_hgtw = internal constant [16 x %struct.wedge_code_type] [%struct.wedge_code_type { i8 2, i32 4, i32 4 }, %struct.wedge_code_type { i8 3, i32 4, i32 4 }, %struct.wedge_code_type { i8 4, i32 4, i32 4 }, %struct.wedge_code_type { i8 5, i32 4, i32 4 }, %struct.wedge_code_type { i8 0, i32 4, i32 2 }, %struct.wedge_code_type { i8 0, i32 4, i32 4 }, %struct.wedge_code_type { i8 0, i32 4, i32 6 }, %struct.wedge_code_type { i8 1, i32 4, i32 4 }, %struct.wedge_code_type { i8 2, i32 4, i32 2 }, %struct.wedge_code_type { i8 2, i32 4, i32 6 }, %struct.wedge_code_type { i8 5, i32 4, i32 2 }, %struct.wedge_code_type { i8 5, i32 4, i32 6 }, %struct.wedge_code_type { i8 3, i32 2, i32 4 }, %struct.wedge_code_type { i8 3, i32 6, i32 4 }, %struct.wedge_code_type { i8 4, i32 2, i32 4 }, %struct.wedge_code_type { i8 4, i32 6, i32 4 }], align 16
@wedge_codebook_16_hltw = internal constant [16 x %struct.wedge_code_type] [%struct.wedge_code_type { i8 2, i32 4, i32 4 }, %struct.wedge_code_type { i8 3, i32 4, i32 4 }, %struct.wedge_code_type { i8 4, i32 4, i32 4 }, %struct.wedge_code_type { i8 5, i32 4, i32 4 }, %struct.wedge_code_type { i8 1, i32 2, i32 4 }, %struct.wedge_code_type { i8 1, i32 4, i32 4 }, %struct.wedge_code_type { i8 1, i32 6, i32 4 }, %struct.wedge_code_type { i8 0, i32 4, i32 4 }, %struct.wedge_code_type { i8 2, i32 4, i32 2 }, %struct.wedge_code_type { i8 2, i32 4, i32 6 }, %struct.wedge_code_type { i8 5, i32 4, i32 2 }, %struct.wedge_code_type { i8 5, i32 4, i32 6 }, %struct.wedge_code_type { i8 3, i32 2, i32 4 }, %struct.wedge_code_type { i8 3, i32 6, i32 4 }, %struct.wedge_code_type { i8 4, i32 2, i32 4 }, %struct.wedge_code_type { i8 4, i32 6, i32 4 }], align 16
@av1_wedge_params_lookup = hidden constant [22 x %struct.wedge_params_type] [%struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type { i32 16, %struct.wedge_code_type* getelementptr inbounds ([16 x %struct.wedge_code_type], [16 x %struct.wedge_code_type]* @wedge_codebook_16_heqw, i32 0, i32 0), i8* getelementptr (i8, i8* getelementptr inbounds ([22 x [16 x i8]], [22 x [16 x i8]]* @wedge_signflip_lookup, i32 0, i32 0, i32 0), i64 48), [16 x i8*]* bitcast (i8* getelementptr (i8, i8* bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i64 384) to [16 x i8*]*) }, %struct.wedge_params_type { i32 16, %struct.wedge_code_type* getelementptr inbounds ([16 x %struct.wedge_code_type], [16 x %struct.wedge_code_type]* @wedge_codebook_16_hgtw, i32 0, i32 0), i8* getelementptr (i8, i8* getelementptr inbounds ([22 x [16 x i8]], [22 x [16 x i8]]* @wedge_signflip_lookup, i32 0, i32 0, i32 0), i64 64), [16 x i8*]* bitcast (i8* getelementptr (i8, i8* bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i64 512) to [16 x i8*]*) }, %struct.wedge_params_type { i32 16, %struct.wedge_code_type* getelementptr inbounds ([16 x %struct.wedge_code_type], [16 x %struct.wedge_code_type]* @wedge_codebook_16_hltw, i32 0, i32 0), i8* getelementptr (i8, i8* getelementptr inbounds ([22 x [16 x i8]], [22 x [16 x i8]]* @wedge_signflip_lookup, i32 0, i32 0, i32 0), i64 80), [16 x i8*]* bitcast (i8* getelementptr (i8, i8* bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i64 640) to [16 x i8*]*) }, %struct.wedge_params_type { i32 16, %struct.wedge_code_type* getelementptr inbounds ([16 x %struct.wedge_code_type], [16 x %struct.wedge_code_type]* @wedge_codebook_16_heqw, i32 0, i32 0), i8* getelementptr (i8, i8* getelementptr inbounds ([22 x [16 x i8]], [22 x [16 x i8]]* @wedge_signflip_lookup, i32 0, i32 0, i32 0), i64 96), [16 x i8*]* bitcast (i8* getelementptr (i8, i8* bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i64 768) to [16 x i8*]*) }, %struct.wedge_params_type { i32 16, %struct.wedge_code_type* getelementptr inbounds ([16 x %struct.wedge_code_type], [16 x %struct.wedge_code_type]* @wedge_codebook_16_hgtw, i32 0, i32 0), i8* getelementptr (i8, i8* getelementptr inbounds ([22 x [16 x i8]], [22 x [16 x i8]]* @wedge_signflip_lookup, i32 0, i32 0, i32 0), i64 112), [16 x i8*]* bitcast (i8* getelementptr (i8, i8* bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i64 896) to [16 x i8*]*) }, %struct.wedge_params_type { i32 16, %struct.wedge_code_type* getelementptr inbounds ([16 x %struct.wedge_code_type], [16 x %struct.wedge_code_type]* @wedge_codebook_16_hltw, i32 0, i32 0), i8* getelementptr (i8, i8* getelementptr inbounds ([22 x [16 x i8]], [22 x [16 x i8]]* @wedge_signflip_lookup, i32 0, i32 0, i32 0), i64 128), [16 x i8*]* bitcast (i8* getelementptr (i8, i8* bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i64 1024) to [16 x i8*]*) }, %struct.wedge_params_type { i32 16, %struct.wedge_code_type* getelementptr inbounds ([16 x %struct.wedge_code_type], [16 x %struct.wedge_code_type]* @wedge_codebook_16_heqw, i32 0, i32 0), i8* getelementptr (i8, i8* getelementptr inbounds ([22 x [16 x i8]], [22 x [16 x i8]]* @wedge_signflip_lookup, i32 0, i32 0, i32 0), i64 144), [16 x i8*]* bitcast (i8* getelementptr (i8, i8* bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i64 1152) to [16 x i8*]*) }, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type { i32 16, %struct.wedge_code_type* getelementptr inbounds ([16 x %struct.wedge_code_type], [16 x %struct.wedge_code_type]* @wedge_codebook_16_hgtw, i32 0, i32 0), i8* getelementptr (i8, i8* getelementptr inbounds ([22 x [16 x i8]], [22 x [16 x i8]]* @wedge_signflip_lookup, i32 0, i32 0, i32 0), i64 288), [16 x i8*]* bitcast (i8* getelementptr (i8, i8* bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i64 2304) to [16 x i8*]*) }, %struct.wedge_params_type { i32 16, %struct.wedge_code_type* getelementptr inbounds ([16 x %struct.wedge_code_type], [16 x %struct.wedge_code_type]* @wedge_codebook_16_hltw, i32 0, i32 0), i8* getelementptr (i8, i8* getelementptr inbounds ([22 x [16 x i8]], [22 x [16 x i8]]* @wedge_signflip_lookup, i32 0, i32 0, i32 0), i64 304), [16 x i8*]* bitcast (i8* getelementptr (i8, i8* bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i64 2432) to [16 x i8*]*) }, %struct.wedge_params_type zeroinitializer, %struct.wedge_params_type zeroinitializer], align 16
@quant_dist_lookup_table = internal constant [2 x [4 x [2 x i32]]] [[4 x [2 x i32]] [[2 x i32] [i32 9, i32 7], [2 x i32] [i32 11, i32 5], [2 x i32] [i32 12, i32 4], [2 x i32] [i32 13, i32 3]], [4 x [2 x i32]] [[2 x i32] [i32 7, i32 9], [2 x i32] [i32 5, i32 11], [2 x i32] [i32 4, i32 12], [2 x i32] [i32 3, i32 13]]], align 16
@quant_dist_weight = internal constant [4 x [2 x i32]] [[2 x i32] [i32 2, i32 3], [2 x i32] [i32 2, i32 5], [2 x i32] [i32 2, i32 7], [2 x i32] [i32 1, i32 31]], align 16
@obmc_mask_1 = internal constant [1 x i8] c"@", align 1
@obmc_mask_2 = internal constant [2 x i8] c"-@", align 2
@obmc_mask_4 = internal constant [4 x i8] c"'2;@", align 4
@obmc_mask_8 = internal constant [8 x i8] c"$*059=@@", align 1
@obmc_mask_16 = internal constant [16 x i8] c"\22%(+.1468:<=@@@@", align 16
@obmc_mask_32 = internal constant [32 x i8] c"!#$&()+,-/02345789:;<<=>@@@@@@@@", align 16
@obmc_mask_64 = internal constant [64 x i8] c"!\22##$%&'(()*+,,,-.//0123334456788899::;<<<<<=>>>>>????@@@@@@@@@@", align 16
@max_neighbor_obmc = internal constant [6 x i32] [i32 0, i32 1, i32 2, i32 3, i32 4, i32 4], align 16
@mi_size_wide_log2 = internal constant [22 x i8] c"\00\00\01\01\01\02\02\02\03\03\03\04\04\04\05\05\00\02\01\03\02\04", align 16
@mi_size_high_log2 = internal constant [22 x i8] c"\00\01\00\01\02\01\02\03\02\03\04\03\04\05\04\05\02\00\03\01\04\02", align 16
@.str = private unnamed_addr constant [39 x i8] c"Reference frame has invalid dimensions\00", align 1
@interintra_to_intra_mode = internal constant [4 x i8] c"\00\01\02\09", align 1
@max_txsize_rect_lookup = internal constant [22 x i8] c"\00\05\06\01\07\08\02\09\0A\03\0B\0C\04\04\04\04\0D\0E\0F\10\11\12", align 16
@av1_intrabc_bilinear_filter = internal constant <{ [18 x i16], [14 x i16] }> <{ [18 x i16] [i16 128, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 0, i16 64, i16 64], [14 x i16] zeroinitializer }>, align 256
@av1_interp_4tap = internal constant [4 x %struct.InterpFilterParams] [%struct.InterpFilterParams { i16* getelementptr inbounds ([16 x [8 x i16]], [16 x [8 x i16]]* @av1_sub_pel_filters_4, i32 0, i32 0, i32 0), i16 8, i16 16, i8 0 }, %struct.InterpFilterParams { i16* getelementptr inbounds ([16 x [8 x i16]], [16 x [8 x i16]]* @av1_sub_pel_filters_4smooth, i32 0, i32 0, i32 0), i16 8, i16 16, i8 1 }, %struct.InterpFilterParams { i16* getelementptr inbounds ([16 x [8 x i16]], [16 x [8 x i16]]* @av1_sub_pel_filters_4, i32 0, i32 0, i32 0), i16 8, i16 16, i8 0 }, %struct.InterpFilterParams { i16* getelementptr inbounds ([16 x [8 x i16]], [16 x [8 x i16]]* @av1_bilinear_filters, i32 0, i32 0, i32 0), i16 8, i16 16, i8 3 }], align 16
@av1_interp_filter_params_list = internal constant [4 x %struct.InterpFilterParams] [%struct.InterpFilterParams { i16* getelementptr inbounds ([16 x [8 x i16]], [16 x [8 x i16]]* @av1_sub_pel_filters_8, i32 0, i32 0, i32 0), i16 8, i16 16, i8 0 }, %struct.InterpFilterParams { i16* getelementptr inbounds ([16 x [8 x i16]], [16 x [8 x i16]]* @av1_sub_pel_filters_8smooth, i32 0, i32 0, i32 0), i16 8, i16 16, i8 1 }, %struct.InterpFilterParams { i16* getelementptr inbounds ([16 x [8 x i16]], [16 x [8 x i16]]* @av1_sub_pel_filters_8sharp, i32 0, i32 0, i32 0), i16 8, i16 16, i8 2 }, %struct.InterpFilterParams { i16* getelementptr inbounds ([16 x [8 x i16]], [16 x [8 x i16]]* @av1_bilinear_filters, i32 0, i32 0, i32 0), i16 8, i16 16, i8 3 }], align 16
@av1_sub_pel_filters_4 = internal constant [16 x [8 x i16]] [[8 x i16] [i16 0, i16 0, i16 0, i16 128, i16 0, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -4, i16 126, i16 8, i16 -2, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -8, i16 122, i16 18, i16 -4, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -10, i16 116, i16 28, i16 -6, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -12, i16 110, i16 38, i16 -8, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -12, i16 102, i16 48, i16 -10, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -14, i16 94, i16 58, i16 -10, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -12, i16 84, i16 66, i16 -10, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -12, i16 76, i16 76, i16 -12, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -10, i16 66, i16 84, i16 -12, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -10, i16 58, i16 94, i16 -14, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -10, i16 48, i16 102, i16 -12, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -8, i16 38, i16 110, i16 -12, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -6, i16 28, i16 116, i16 -10, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -4, i16 18, i16 122, i16 -8, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 -2, i16 8, i16 126, i16 -4, i16 0, i16 0]], align 256
@av1_sub_pel_filters_4smooth = internal constant [16 x [8 x i16]] [[8 x i16] [i16 0, i16 0, i16 0, i16 128, i16 0, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 30, i16 62, i16 34, i16 2, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 26, i16 62, i16 36, i16 4, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 22, i16 62, i16 40, i16 4, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 20, i16 60, i16 42, i16 6, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 18, i16 58, i16 44, i16 8, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 16, i16 56, i16 46, i16 10, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 14, i16 54, i16 48, i16 12, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 12, i16 52, i16 52, i16 12, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 12, i16 48, i16 54, i16 14, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 10, i16 46, i16 56, i16 16, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 8, i16 44, i16 58, i16 18, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 6, i16 42, i16 60, i16 20, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 4, i16 40, i16 62, i16 22, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 4, i16 36, i16 62, i16 26, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 2, i16 34, i16 62, i16 30, i16 0, i16 0]], align 256
@av1_bilinear_filters = internal constant [16 x [8 x i16]] [[8 x i16] [i16 0, i16 0, i16 0, i16 128, i16 0, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 120, i16 8, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 112, i16 16, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 104, i16 24, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 96, i16 32, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 88, i16 40, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 80, i16 48, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 72, i16 56, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 64, i16 64, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 56, i16 72, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 48, i16 80, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 40, i16 88, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 32, i16 96, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 24, i16 104, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 16, i16 112, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 0, i16 8, i16 120, i16 0, i16 0, i16 0]], align 256
@av1_sub_pel_filters_8 = internal constant [16 x [8 x i16]] [[8 x i16] [i16 0, i16 0, i16 0, i16 128, i16 0, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 2, i16 -6, i16 126, i16 8, i16 -2, i16 0, i16 0], [8 x i16] [i16 0, i16 2, i16 -10, i16 122, i16 18, i16 -4, i16 0, i16 0], [8 x i16] [i16 0, i16 2, i16 -12, i16 116, i16 28, i16 -8, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -14, i16 110, i16 38, i16 -10, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -14, i16 102, i16 48, i16 -12, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -16, i16 94, i16 58, i16 -12, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -14, i16 84, i16 66, i16 -12, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -14, i16 76, i16 76, i16 -14, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -12, i16 66, i16 84, i16 -14, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -12, i16 58, i16 94, i16 -16, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -12, i16 48, i16 102, i16 -14, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -10, i16 38, i16 110, i16 -14, i16 2, i16 0], [8 x i16] [i16 0, i16 2, i16 -8, i16 28, i16 116, i16 -12, i16 2, i16 0], [8 x i16] [i16 0, i16 0, i16 -4, i16 18, i16 122, i16 -10, i16 2, i16 0], [8 x i16] [i16 0, i16 0, i16 -2, i16 8, i16 126, i16 -6, i16 2, i16 0]], align 256
@av1_sub_pel_filters_8smooth = internal constant [16 x [8 x i16]] [[8 x i16] [i16 0, i16 0, i16 0, i16 128, i16 0, i16 0, i16 0, i16 0], [8 x i16] [i16 0, i16 2, i16 28, i16 62, i16 34, i16 2, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 26, i16 62, i16 36, i16 4, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 22, i16 62, i16 40, i16 4, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 20, i16 60, i16 42, i16 6, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 18, i16 58, i16 44, i16 8, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 16, i16 56, i16 46, i16 10, i16 0, i16 0], [8 x i16] [i16 0, i16 -2, i16 16, i16 54, i16 48, i16 12, i16 0, i16 0], [8 x i16] [i16 0, i16 -2, i16 14, i16 52, i16 52, i16 14, i16 -2, i16 0], [8 x i16] [i16 0, i16 0, i16 12, i16 48, i16 54, i16 16, i16 -2, i16 0], [8 x i16] [i16 0, i16 0, i16 10, i16 46, i16 56, i16 16, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 8, i16 44, i16 58, i16 18, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 6, i16 42, i16 60, i16 20, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 4, i16 40, i16 62, i16 22, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 4, i16 36, i16 62, i16 26, i16 0, i16 0], [8 x i16] [i16 0, i16 0, i16 2, i16 34, i16 62, i16 28, i16 2, i16 0]], align 256
@av1_sub_pel_filters_8sharp = internal constant [16 x [8 x i16]] [[8 x i16] [i16 0, i16 0, i16 0, i16 128, i16 0, i16 0, i16 0, i16 0], [8 x i16] [i16 -2, i16 2, i16 -6, i16 126, i16 8, i16 -2, i16 2, i16 0], [8 x i16] [i16 -2, i16 6, i16 -12, i16 124, i16 16, i16 -6, i16 4, i16 -2], [8 x i16] [i16 -2, i16 8, i16 -18, i16 120, i16 26, i16 -10, i16 6, i16 -2], [8 x i16] [i16 -4, i16 10, i16 -22, i16 116, i16 38, i16 -14, i16 6, i16 -2], [8 x i16] [i16 -4, i16 10, i16 -22, i16 108, i16 48, i16 -18, i16 8, i16 -2], [8 x i16] [i16 -4, i16 10, i16 -24, i16 100, i16 60, i16 -20, i16 8, i16 -2], [8 x i16] [i16 -4, i16 10, i16 -24, i16 90, i16 70, i16 -22, i16 10, i16 -2], [8 x i16] [i16 -4, i16 12, i16 -24, i16 80, i16 80, i16 -24, i16 12, i16 -4], [8 x i16] [i16 -2, i16 10, i16 -22, i16 70, i16 90, i16 -24, i16 10, i16 -4], [8 x i16] [i16 -2, i16 8, i16 -20, i16 60, i16 100, i16 -24, i16 10, i16 -4], [8 x i16] [i16 -2, i16 8, i16 -18, i16 48, i16 108, i16 -22, i16 10, i16 -4], [8 x i16] [i16 -2, i16 6, i16 -14, i16 38, i16 116, i16 -22, i16 10, i16 -4], [8 x i16] [i16 -2, i16 6, i16 -10, i16 26, i16 120, i16 -18, i16 8, i16 -2], [8 x i16] [i16 -2, i16 4, i16 -6, i16 16, i16 124, i16 -12, i16 6, i16 -2], [8 x i16] [i16 0, i16 2, i16 -2, i16 8, i16 126, i16 -6, i16 2, i16 -2]], align 256
@wedge_master_oblique_even = internal constant [64 x i8] c"\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\01\04\0B\1B.:>?@@@@@@@@@@@@@@@@@@@@@@@@@@@@", align 16
@wedge_mask_obl = internal global [2 x [6 x [4096 x i8]]] zeroinitializer, align 16
@wedge_master_oblique_odd = internal constant [64 x i8] c"\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\01\02\06\12%5<?@@@@@@@@@@@@@@@@@@@@@@@@@@@@", align 16
@wedge_master_vertical = internal constant [64 x i8] c"\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\02\07\15+9>@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", align 16
@wedge_mask_buf = internal global [131072 x i8] zeroinitializer, align 16
@block_size_wide = internal constant [22 x i8] c"\04\04\08\08\08\10\10\10   @@@\80\80\04\10\08 \10@", align 16
@block_size_high = internal constant [22 x i8] c"\04\08\04\08\10\08\10 \10 @ @\80@\80\10\04 \08@\10", align 16
@smooth_interintra_mask_buf = internal global [4 x [22 x [1024 x i8]]] zeroinitializer, align 16
@ii_size_scales = internal global [22 x i8] c" \10\10\10\08\08\08\04\04\04\02\02\02\01\01\01\08\08\04\04\02\02", align 16
@ii_weights1d = internal constant [128 x i8] c"<:86420/-,*)'&%#\22! \1F\1E\1D\1C\1B\1A\19\18\17\16\16\15\14\13\13\12\12\11\10\10\0F\0F\0E\0E\0D\0D\0C\0C\0C\0B\0B\0A\0A\0A\09\09\09\08\08\08\08\07\07\07\07\06\06\06\06\06\05\05\05\05\05\04\04\04\04\04\04\04\04\03\03\03\03\03\03\03\03\03\02\02\02\02\02\02\02\02\02\02\02\02\02\02\02\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01", align 16
@mi_size_high = internal constant [22 x i8] c"\01\02\01\02\04\02\04\08\04\08\10\08\10 \10 \04\01\08\02\10\04", align 16
@mi_size_wide = internal constant [22 x i8] c"\01\01\02\02\02\04\04\04\08\08\08\10\10\10  \01\04\02\08\04\10", align 16
@ss_size_lookup = internal constant [22 x [2 x [2 x i8]]] [[2 x [2 x i8]] zeroinitializer, [2 x [2 x i8]] [[2 x i8] c"\01\00", [2 x i8] c"\FF\00"], [2 x [2 x i8]] [[2 x i8] c"\02\FF", [2 x i8] zeroinitializer], [2 x [2 x i8]] [[2 x i8] c"\03\02", [2 x i8] c"\01\00"], [2 x [2 x i8]] [[2 x i8] c"\04\03", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\05\FF", [2 x i8] c"\03\02"], [2 x [2 x i8]] [[2 x i8] c"\06\05", [2 x i8] c"\04\03"], [2 x [2 x i8]] [[2 x i8] c"\07\06", [2 x i8] c"\FF\04"], [2 x [2 x i8]] [[2 x i8] c"\08\FF", [2 x i8] c"\06\05"], [2 x [2 x i8]] [[2 x i8] c"\09\08", [2 x i8] c"\07\06"], [2 x [2 x i8]] [[2 x i8] c"\0A\09", [2 x i8] c"\FF\07"], [2 x [2 x i8]] [[2 x i8] c"\0B\FF", [2 x i8] c"\09\08"], [2 x [2 x i8]] [[2 x i8] c"\0C\0B", [2 x i8] c"\0A\09"], [2 x [2 x i8]] [[2 x i8] c"\0D\0C", [2 x i8] c"\FF\0A"], [2 x [2 x i8]] [[2 x i8] c"\0E\FF", [2 x i8] c"\0C\0B"], [2 x [2 x i8]] [[2 x i8] c"\0F\0E", [2 x i8] c"\0D\0C"], [2 x [2 x i8]] [[2 x i8] c"\10\01", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\11\FF", [2 x i8] c"\02\02"], [2 x [2 x i8]] [[2 x i8] c"\12\04", [2 x i8] c"\FF\10"], [2 x [2 x i8]] [[2 x i8] c"\13\FF", [2 x i8] c"\05\11"], [2 x [2 x i8]] [[2 x i8] c"\14\07", [2 x i8] c"\FF\12"], [2 x [2 x i8]] [[2 x i8] c"\15\FF", [2 x i8] c"\08\13"]], align 16

; Function Attrs: nounwind
define hidden i32 @av1_allow_warp(%struct.MB_MODE_INFO* %mbmi, %struct.WarpTypesAllowed* %warp_types, %struct.WarpedMotionParams* %gm_params, i32 %build_for_obmc, %struct.scale_factors* %sf, %struct.WarpedMotionParams* %final_warp_params) #0 {
entry:
  %retval = alloca i32, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %warp_types.addr = alloca %struct.WarpTypesAllowed*, align 4
  %gm_params.addr = alloca %struct.WarpedMotionParams*, align 4
  %build_for_obmc.addr = alloca i32, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %final_warp_params.addr = alloca %struct.WarpedMotionParams*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  store %struct.WarpTypesAllowed* %warp_types, %struct.WarpTypesAllowed** %warp_types.addr, align 4, !tbaa !2
  store %struct.WarpedMotionParams* %gm_params, %struct.WarpedMotionParams** %gm_params.addr, align 4, !tbaa !2
  store i32 %build_for_obmc, i32* %build_for_obmc.addr, align 4, !tbaa !6
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  store %struct.WarpedMotionParams* %final_warp_params, %struct.WarpedMotionParams** %final_warp_params.addr, align 4, !tbaa !2
  %0 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call = call i32 @av1_is_scaled(%struct.scale_factors* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %final_warp_params.addr, align 4, !tbaa !2
  %cmp = icmp ne %struct.WarpedMotionParams* %1, null
  br i1 %cmp, label %if.then1, label %if.end2

if.then1:                                         ; preds = %if.end
  %2 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %final_warp_params.addr, align 4, !tbaa !2
  %3 = bitcast %struct.WarpedMotionParams* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 bitcast (%struct.WarpedMotionParams* @default_warp_params to i8*), i32 44, i1 false), !tbaa.struct !8
  br label %if.end2

if.end2:                                          ; preds = %if.then1, %if.end
  %4 = load i32, i32* %build_for_obmc.addr, align 4, !tbaa !6
  %tobool3 = icmp ne i32 %4, 0
  br i1 %tobool3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end2
  store i32 0, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end2
  %5 = load %struct.WarpTypesAllowed*, %struct.WarpTypesAllowed** %warp_types.addr, align 4, !tbaa !2
  %local_warp_allowed = getelementptr inbounds %struct.WarpTypesAllowed, %struct.WarpTypesAllowed* %5, i32 0, i32 1
  %6 = load i32, i32* %local_warp_allowed, align 4, !tbaa !12
  %tobool6 = icmp ne i32 %6, 0
  br i1 %tobool6, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.end5
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %wm_params = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 1
  %invalid = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %wm_params, i32 0, i32 6
  %8 = load i8, i8* %invalid, align 1, !tbaa !14
  %tobool7 = icmp ne i8 %8, 0
  br i1 %tobool7, label %if.else, label %if.then8

if.then8:                                         ; preds = %land.lhs.true
  %9 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %final_warp_params.addr, align 4, !tbaa !2
  %cmp9 = icmp ne %struct.WarpedMotionParams* %9, null
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.then8
  %10 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %final_warp_params.addr, align 4, !tbaa !2
  %11 = bitcast %struct.WarpedMotionParams* %10 to i8*
  %12 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %wm_params11 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %12, i32 0, i32 1
  %13 = bitcast %struct.WarpedMotionParams* %wm_params11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %13, i32 44, i1 false)
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.then8
  store i32 1, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %land.lhs.true, %if.end5
  %14 = load %struct.WarpTypesAllowed*, %struct.WarpTypesAllowed** %warp_types.addr, align 4, !tbaa !2
  %global_warp_allowed = getelementptr inbounds %struct.WarpTypesAllowed, %struct.WarpTypesAllowed* %14, i32 0, i32 0
  %15 = load i32, i32* %global_warp_allowed, align 4, !tbaa !20
  %tobool13 = icmp ne i32 %15, 0
  br i1 %tobool13, label %land.lhs.true14, label %if.end21

land.lhs.true14:                                  ; preds = %if.else
  %16 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm_params.addr, align 4, !tbaa !2
  %invalid15 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %16, i32 0, i32 6
  %17 = load i8, i8* %invalid15, align 1, !tbaa !21
  %tobool16 = icmp ne i8 %17, 0
  br i1 %tobool16, label %if.end21, label %if.then17

if.then17:                                        ; preds = %land.lhs.true14
  %18 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %final_warp_params.addr, align 4, !tbaa !2
  %cmp18 = icmp ne %struct.WarpedMotionParams* %18, null
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.then17
  %19 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %final_warp_params.addr, align 4, !tbaa !2
  %20 = bitcast %struct.WarpedMotionParams* %19 to i8*
  %21 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %gm_params.addr, align 4, !tbaa !2
  %22 = bitcast %struct.WarpedMotionParams* %21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %22, i32 44, i1 false)
  br label %if.end20

if.end20:                                         ; preds = %if.then19, %if.then17
  store i32 1, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %land.lhs.true14, %if.else
  br label %if.end22

if.end22:                                         ; preds = %if.end21
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %if.end20, %if.end12, %if.then4, %if.then
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_scaled(%struct.scale_factors* %sf) #1 {
entry:
  %sf.addr = alloca %struct.scale_factors*, align 4
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call = call i32 @av1_is_valid_scale(%struct.scale_factors* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %1, i32 0, i32 0
  %2 = load i32, i32* %x_scale_fp, align 4, !tbaa !22
  %cmp = icmp ne i32 %2, 16384
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.rhs
  %3 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %3, i32 0, i32 1
  %4 = load i32, i32* %y_scale_fp, align 4, !tbaa !24
  %cmp1 = icmp ne i32 %4, 16384
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.rhs
  %5 = phi i1 [ true, %land.rhs ], [ %cmp1, %lor.rhs ]
  br label %land.end

land.end:                                         ; preds = %lor.end, %entry
  %6 = phi i1 [ false, %entry ], [ %5, %lor.end ]
  %land.ext = zext i1 %6 to i32
  ret i32 %land.ext
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define hidden void @av1_init_inter_params(%struct.InterPredParams* %inter_pred_params, i32 %block_width, i32 %block_height, i32 %pix_row, i32 %pix_col, i32 %subsampling_x, i32 %subsampling_y, i32 %bit_depth, i32 %use_hbd_buf, i32 %is_intrabc, %struct.scale_factors* %sf, %struct.buf_2d* %ref_buf, %union.int_interpfilters* byval(%union.int_interpfilters) align 4 %interp_filters) #0 {
entry:
  %inter_pred_params.addr = alloca %struct.InterPredParams*, align 4
  %block_width.addr = alloca i32, align 4
  %block_height.addr = alloca i32, align 4
  %pix_row.addr = alloca i32, align 4
  %pix_col.addr = alloca i32, align 4
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %bit_depth.addr = alloca i32, align 4
  %use_hbd_buf.addr = alloca i32, align 4
  %is_intrabc.addr = alloca i32, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %ref_buf.addr = alloca %struct.buf_2d*, align 4
  store %struct.InterPredParams* %inter_pred_params, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  store i32 %block_width, i32* %block_width.addr, align 4, !tbaa !6
  store i32 %block_height, i32* %block_height.addr, align 4, !tbaa !6
  store i32 %pix_row, i32* %pix_row.addr, align 4, !tbaa !6
  store i32 %pix_col, i32* %pix_col.addr, align 4, !tbaa !6
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  store i32 %bit_depth, i32* %bit_depth.addr, align 4, !tbaa !6
  store i32 %use_hbd_buf, i32* %use_hbd_buf.addr, align 4, !tbaa !6
  store i32 %is_intrabc, i32* %is_intrabc.addr, align 4, !tbaa !6
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  store %struct.buf_2d* %ref_buf, %struct.buf_2d** %ref_buf.addr, align 4, !tbaa !2
  %0 = load i32, i32* %block_width.addr, align 4, !tbaa !6
  %1 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_width1 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %1, i32 0, i32 5
  store i32 %0, i32* %block_width1, align 4, !tbaa !25
  %2 = load i32, i32* %block_height.addr, align 4, !tbaa !6
  %3 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_height2 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %3, i32 0, i32 6
  store i32 %2, i32* %block_height2, align 4, !tbaa !29
  %4 = load i32, i32* %pix_row.addr, align 4, !tbaa !6
  %5 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %pix_row3 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %5, i32 0, i32 7
  store i32 %4, i32* %pix_row3, align 4, !tbaa !30
  %6 = load i32, i32* %pix_col.addr, align 4, !tbaa !6
  %7 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %pix_col4 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %7, i32 0, i32 8
  store i32 %6, i32* %pix_col4, align 4, !tbaa !31
  %8 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %9 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %subsampling_x5 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %9, i32 0, i32 10
  store i32 %8, i32* %subsampling_x5, align 4, !tbaa !32
  %10 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %11 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %subsampling_y6 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %11, i32 0, i32 11
  store i32 %10, i32* %subsampling_y6, align 4, !tbaa !33
  %12 = load i32, i32* %bit_depth.addr, align 4, !tbaa !6
  %13 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %bit_depth7 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %13, i32 0, i32 13
  store i32 %12, i32* %bit_depth7, align 4, !tbaa !34
  %14 = load i32, i32* %use_hbd_buf.addr, align 4, !tbaa !6
  %15 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %use_hbd_buf8 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %15, i32 0, i32 14
  store i32 %14, i32* %use_hbd_buf8, align 4, !tbaa !35
  %16 = load i32, i32* %is_intrabc.addr, align 4, !tbaa !6
  %17 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %is_intrabc9 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %17, i32 0, i32 17
  store i32 %16, i32* %is_intrabc9, align 4, !tbaa !36
  %18 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %19 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %scale_factors = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %19, i32 0, i32 12
  store %struct.scale_factors* %18, %struct.scale_factors** %scale_factors, align 4, !tbaa !37
  %20 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %ref_frame_buf = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %20, i32 0, i32 9
  %21 = load %struct.buf_2d*, %struct.buf_2d** %ref_buf.addr, align 4, !tbaa !2
  %22 = bitcast %struct.buf_2d* %ref_frame_buf to i8*
  %23 = bitcast %struct.buf_2d* %21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 20, i1 false), !tbaa.struct !38
  %24 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %mode = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %24, i32 0, i32 0
  store i32 0, i32* %mode, align 4, !tbaa !39
  %25 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %comp_mode = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %25, i32 0, i32 1
  store i32 0, i32* %comp_mode, align 4, !tbaa !40
  %26 = load i32, i32* %is_intrabc.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %26, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %27 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %interp_filter_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %27, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x %struct.InterpFilterParams*], [2 x %struct.InterpFilterParams*]* %interp_filter_params, i32 0, i32 0
  store %struct.InterpFilterParams* @av1_intrabc_filter_params, %struct.InterpFilterParams** %arrayidx, align 4, !tbaa !2
  %28 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %interp_filter_params10 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %28, i32 0, i32 4
  %arrayidx11 = getelementptr inbounds [2 x %struct.InterpFilterParams*], [2 x %struct.InterpFilterParams*]* %interp_filter_params10, i32 0, i32 1
  store %struct.InterpFilterParams* @av1_intrabc_filter_params, %struct.InterpFilterParams** %arrayidx11, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %entry
  %as_filters = bitcast %union.int_interpfilters* %interp_filters to %struct.InterpFilters*
  %x_filter = getelementptr inbounds %struct.InterpFilters, %struct.InterpFilters* %as_filters, i32 0, i32 1
  %29 = load i16, i16* %x_filter, align 2, !tbaa !9
  %conv = trunc i16 %29 to i8
  %30 = load i32, i32* %block_width.addr, align 4, !tbaa !6
  %call = call %struct.InterpFilterParams* @av1_get_interp_filter_params_with_block_size(i8 zeroext %conv, i32 %30)
  %31 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %interp_filter_params12 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %31, i32 0, i32 4
  %arrayidx13 = getelementptr inbounds [2 x %struct.InterpFilterParams*], [2 x %struct.InterpFilterParams*]* %interp_filter_params12, i32 0, i32 0
  store %struct.InterpFilterParams* %call, %struct.InterpFilterParams** %arrayidx13, align 4, !tbaa !2
  %as_filters14 = bitcast %union.int_interpfilters* %interp_filters to %struct.InterpFilters*
  %y_filter = getelementptr inbounds %struct.InterpFilters, %struct.InterpFilters* %as_filters14, i32 0, i32 0
  %32 = load i16, i16* %y_filter, align 4, !tbaa !9
  %conv15 = trunc i16 %32 to i8
  %33 = load i32, i32* %block_height.addr, align 4, !tbaa !6
  %call16 = call %struct.InterpFilterParams* @av1_get_interp_filter_params_with_block_size(i8 zeroext %conv15, i32 %33)
  %34 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %interp_filter_params17 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %34, i32 0, i32 4
  %arrayidx18 = getelementptr inbounds [2 x %struct.InterpFilterParams*], [2 x %struct.InterpFilterParams*]* %interp_filter_params17, i32 0, i32 1
  store %struct.InterpFilterParams* %call16, %struct.InterpFilterParams** %arrayidx18, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal %struct.InterpFilterParams* @av1_get_interp_filter_params_with_block_size(i8 zeroext %interp_filter, i32 %w) #1 {
entry:
  %retval = alloca %struct.InterpFilterParams*, align 4
  %interp_filter.addr = alloca i8, align 1
  %w.addr = alloca i32, align 4
  store i8 %interp_filter, i8* %interp_filter.addr, align 1, !tbaa !9
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  %0 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp = icmp sle i32 %0, 4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8, i8* %interp_filter.addr, align 1, !tbaa !9
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [4 x %struct.InterpFilterParams], [4 x %struct.InterpFilterParams]* @av1_interp_4tap, i32 0, i32 %idxprom
  store %struct.InterpFilterParams* %arrayidx, %struct.InterpFilterParams** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8, i8* %interp_filter.addr, align 1, !tbaa !9
  %idxprom1 = zext i8 %2 to i32
  %arrayidx2 = getelementptr inbounds [4 x %struct.InterpFilterParams], [4 x %struct.InterpFilterParams]* @av1_interp_filter_params_list, i32 0, i32 %idxprom1
  store %struct.InterpFilterParams* %arrayidx2, %struct.InterpFilterParams** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %retval, align 4
  ret %struct.InterpFilterParams* %3
}

; Function Attrs: nounwind
define hidden void @av1_init_comp_mode(%struct.InterPredParams* %inter_pred_params) #0 {
entry:
  %inter_pred_params.addr = alloca %struct.InterPredParams*, align 4
  store %struct.InterPredParams* %inter_pred_params, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %0 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %comp_mode = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %0, i32 0, i32 1
  store i32 1, i32* %comp_mode, align 4, !tbaa !40
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_init_warp_params(%struct.InterPredParams* %inter_pred_params, %struct.WarpTypesAllowed* %warp_types, i32 %ref, %struct.macroblockd* %xd, %struct.MB_MODE_INFO* %mi) #0 {
entry:
  %inter_pred_params.addr = alloca %struct.InterPredParams*, align 4
  %warp_types.addr = alloca %struct.WarpTypesAllowed*, align 4
  %ref.addr = alloca i32, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.InterPredParams* %inter_pred_params, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  store %struct.WarpTypesAllowed* %warp_types, %struct.WarpTypesAllowed** %warp_types.addr, align 4, !tbaa !2
  store i32 %ref, i32* %ref.addr, align 4, !tbaa !6
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %mi, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %0 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_height = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %0, i32 0, i32 6
  %1 = load i32, i32* %block_height, align 4, !tbaa !29
  %cmp = icmp slt i32 %1, 8
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_width = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %2, i32 0, i32 5
  %3 = load i32, i32* %block_width, align 4, !tbaa !25
  %cmp1 = icmp slt i32 %3, 8
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  br label %if.end7

if.end:                                           ; preds = %lor.lhs.false
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cur_frame_force_integer_mv = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 45
  %5 = load i32, i32* %cur_frame_force_integer_mv, align 4, !tbaa !41
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  br label %if.end7

if.end3:                                          ; preds = %if.end
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %7 = load %struct.WarpTypesAllowed*, %struct.WarpTypesAllowed** %warp_types.addr, align 4, !tbaa !2
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %global_motion = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 47
  %9 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %global_motion, align 4, !tbaa !47
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %10, i32 0, i32 12
  %11 = load i32, i32* %ref.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 %11
  %12 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %idxprom = sext i8 %12 to i32
  %arrayidx4 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %9, i32 %idxprom
  %13 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %scale_factors = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %13, i32 0, i32 12
  %14 = load %struct.scale_factors*, %struct.scale_factors** %scale_factors, align 4, !tbaa !37
  %15 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %warp_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %15, i32 0, i32 2
  %call = call i32 @av1_allow_warp(%struct.MB_MODE_INFO* %6, %struct.WarpTypesAllowed* %7, %struct.WarpedMotionParams* %arrayidx4, i32 0, %struct.scale_factors* %14, %struct.WarpedMotionParams* %warp_params)
  %tobool5 = icmp ne i32 %call, 0
  br i1 %tobool5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end3
  %16 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %mode = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %16, i32 0, i32 0
  store i32 1, i32* %mode, align 4, !tbaa !39
  br label %if.end7

if.end7:                                          ; preds = %if.then, %if.then2, %if.then6, %if.end3
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_init_mask_comp(%struct.InterPredParams* %inter_pred_params, i8 zeroext %bsize, %struct.INTERINTER_COMPOUND_DATA* %mask_comp) #0 {
entry:
  %inter_pred_params.addr = alloca %struct.InterPredParams*, align 4
  %bsize.addr = alloca i8, align 1
  %mask_comp.addr = alloca %struct.INTERINTER_COMPOUND_DATA*, align 4
  store %struct.InterPredParams* %inter_pred_params, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store %struct.INTERINTER_COMPOUND_DATA* %mask_comp, %struct.INTERINTER_COMPOUND_DATA** %mask_comp.addr, align 4, !tbaa !2
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %1 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %1, i32 0, i32 16
  store i8 %0, i8* %sb_type, align 4, !tbaa !48
  %2 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %mask_comp1 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %2, i32 0, i32 15
  %3 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %mask_comp.addr, align 4, !tbaa !2
  %4 = bitcast %struct.INTERINTER_COMPOUND_DATA* %mask_comp1 to i8*
  %5 = bitcast %struct.INTERINTER_COMPOUND_DATA* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 8, i1 false), !tbaa.struct !49
  %6 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %6, i32 0, i32 3
  %compound_index = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params, i32 0, i32 7
  %7 = load i32, i32* %compound_index, align 4, !tbaa !50
  %cmp = icmp eq i32 %7, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params2 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %8, i32 0, i32 3
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params2, i32 0, i32 0
  store i32 0, i32* %do_average, align 4, !tbaa !51
  %9 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %comp_mode = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %9, i32 0, i32 1
  store i32 2, i32* %comp_mode, align 4, !tbaa !40
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_make_inter_predictor(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, %struct.InterPredParams* %inter_pred_params, %struct.SubpelParams* %subpel_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %inter_pred_params.addr = alloca %struct.InterPredParams*, align 4
  %subpel_params.addr = alloca %struct.SubpelParams*, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store %struct.InterPredParams* %inter_pred_params, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  store %struct.SubpelParams* %subpel_params, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %0 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %mode = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %0, i32 0, i32 0
  %1 = load i32, i32* %mode, align 4, !tbaa !39
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %warp_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %2, i32 0, i32 2
  %3 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %use_hbd_buf = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %3, i32 0, i32 14
  %4 = load i32, i32* %use_hbd_buf, align 4, !tbaa !35
  %5 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %bit_depth = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %5, i32 0, i32 13
  %6 = load i32, i32* %bit_depth, align 4, !tbaa !34
  %7 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %ref_frame_buf = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %7, i32 0, i32 9
  %buf0 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %ref_frame_buf, i32 0, i32 1
  %8 = load i8*, i8** %buf0, align 4, !tbaa !52
  %9 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %ref_frame_buf1 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %9, i32 0, i32 9
  %width = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %ref_frame_buf1, i32 0, i32 2
  %10 = load i32, i32* %width, align 4, !tbaa !53
  %11 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %ref_frame_buf2 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %11, i32 0, i32 9
  %height = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %ref_frame_buf2, i32 0, i32 3
  %12 = load i32, i32* %height, align 4, !tbaa !54
  %13 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %ref_frame_buf3 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %13, i32 0, i32 9
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %ref_frame_buf3, i32 0, i32 4
  %14 = load i32, i32* %stride, align 4, !tbaa !55
  %15 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %16 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %pix_col = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %16, i32 0, i32 8
  %17 = load i32, i32* %pix_col, align 4, !tbaa !31
  %18 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %pix_row = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %18, i32 0, i32 7
  %19 = load i32, i32* %pix_row, align 4, !tbaa !30
  %20 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_width = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %20, i32 0, i32 5
  %21 = load i32, i32* %block_width, align 4, !tbaa !25
  %22 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_height = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %22, i32 0, i32 6
  %23 = load i32, i32* %block_height, align 4, !tbaa !29
  %24 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %25 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %25, i32 0, i32 10
  %26 = load i32, i32* %subsampling_x, align 4, !tbaa !32
  %27 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %27, i32 0, i32 11
  %28 = load i32, i32* %subsampling_y, align 4, !tbaa !33
  %29 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %29, i32 0, i32 3
  call void @av1_warp_plane(%struct.WarpedMotionParams* %warp_params, i32 %4, i32 %6, i8* %8, i32 %10, i32 %12, i32 %14, i8* %15, i32 %17, i32 %19, i32 %21, i32 %23, i32 %24, i32 %26, i32 %28, %struct.ConvolveParams* %conv_params)
  br label %if.end21

if.else:                                          ; preds = %entry
  %30 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %mode4 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %30, i32 0, i32 0
  %31 = load i32, i32* %mode4, align 4, !tbaa !39
  %cmp5 = icmp eq i32 %31, 0
  br i1 %cmp5, label %if.then6, label %if.end20

if.then6:                                         ; preds = %if.else
  %32 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %use_hbd_buf7 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %32, i32 0, i32 14
  %33 = load i32, i32* %use_hbd_buf7, align 4, !tbaa !35
  %tobool = icmp ne i32 %33, 0
  br i1 %tobool, label %if.then8, label %if.else13

if.then8:                                         ; preds = %if.then6
  %34 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %35 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %36 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %37 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %38 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %39 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %scale_factors = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %39, i32 0, i32 12
  %40 = load %struct.scale_factors*, %struct.scale_factors** %scale_factors, align 4, !tbaa !37
  %41 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_width9 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %41, i32 0, i32 5
  %42 = load i32, i32* %block_width9, align 4, !tbaa !25
  %43 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_height10 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %43, i32 0, i32 6
  %44 = load i32, i32* %block_height10, align 4, !tbaa !29
  %45 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params11 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %45, i32 0, i32 3
  %46 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %interp_filter_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %46, i32 0, i32 4
  %arraydecay = getelementptr inbounds [2 x %struct.InterpFilterParams*], [2 x %struct.InterpFilterParams*]* %interp_filter_params, i32 0, i32 0
  %47 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %bit_depth12 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %47, i32 0, i32 13
  %48 = load i32, i32* %bit_depth12, align 4, !tbaa !34
  call void @highbd_inter_predictor(i8* %34, i32 %35, i8* %36, i32 %37, %struct.SubpelParams* %38, %struct.scale_factors* %40, i32 %42, i32 %44, %struct.ConvolveParams* %conv_params11, %struct.InterpFilterParams** %arraydecay, i32 %48)
  br label %if.end

if.else13:                                        ; preds = %if.then6
  %49 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %50 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %51 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %52 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %53 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %54 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %scale_factors14 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %54, i32 0, i32 12
  %55 = load %struct.scale_factors*, %struct.scale_factors** %scale_factors14, align 4, !tbaa !37
  %56 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_width15 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %56, i32 0, i32 5
  %57 = load i32, i32* %block_width15, align 4, !tbaa !25
  %58 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_height16 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %58, i32 0, i32 6
  %59 = load i32, i32* %block_height16, align 4, !tbaa !29
  %60 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params17 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %60, i32 0, i32 3
  %61 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %interp_filter_params18 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %61, i32 0, i32 4
  %arraydecay19 = getelementptr inbounds [2 x %struct.InterpFilterParams*], [2 x %struct.InterpFilterParams*]* %interp_filter_params18, i32 0, i32 0
  call void @inter_predictor(i8* %49, i32 %50, i8* %51, i32 %52, %struct.SubpelParams* %53, %struct.scale_factors* %55, i32 %57, i32 %59, %struct.ConvolveParams* %conv_params17, %struct.InterpFilterParams** %arraydecay19)
  br label %if.end

if.end:                                           ; preds = %if.else13, %if.then8
  br label %if.end20

if.end20:                                         ; preds = %if.end, %if.else
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.then
  ret void
}

declare void @av1_warp_plane(%struct.WarpedMotionParams*, i32, i32, i8*, i32, i32, i32, i8*, i32, i32, i32, i32, i32, i32, i32, %struct.ConvolveParams*) #3

; Function Attrs: inlinehint nounwind
define internal void @highbd_inter_predictor(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, %struct.SubpelParams* %subpel_params, %struct.scale_factors* %sf, i32 %w, i32 %h, %struct.ConvolveParams* %conv_params, %struct.InterpFilterParams** %interp_filters, i32 %bd) #1 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %subpel_params.addr = alloca %struct.SubpelParams*, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %interp_filters.addr = alloca %struct.InterpFilterParams**, align 4
  %bd.addr = alloca i32, align 4
  %is_scaled = alloca i32, align 4
  %sp = alloca %struct.SubpelParams, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store %struct.SubpelParams* %subpel_params, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams** %interp_filters, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %is_scaled to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %xs = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %1, i32 0, i32 0
  %2 = load i32, i32* %xs, align 4, !tbaa !56
  %3 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %ys = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %3, i32 0, i32 1
  %4 = load i32, i32* %ys, align 4, !tbaa !58
  %call = call i32 @has_scale(i32 %2, i32 %4)
  store i32 %call, i32* %is_scaled, align 4, !tbaa !6
  %5 = load i32, i32* %is_scaled, align 4, !tbaa !6
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %7 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %9 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %10 = load i32, i32* %w.addr, align 4, !tbaa !6
  %11 = load i32, i32* %h.addr, align 4, !tbaa !6
  %12 = load %struct.InterpFilterParams**, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  %13 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %subpel_x = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %13, i32 0, i32 2
  %14 = load i32, i32* %subpel_x, align 4, !tbaa !59
  %15 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %xs1 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %15, i32 0, i32 0
  %16 = load i32, i32* %xs1, align 4, !tbaa !56
  %17 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %subpel_y = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %17, i32 0, i32 3
  %18 = load i32, i32* %subpel_y, align 4, !tbaa !60
  %19 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %ys2 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %19, i32 0, i32 1
  %20 = load i32, i32* %ys2, align 4, !tbaa !58
  %21 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %22 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %23 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @av1_highbd_convolve_2d_facade(i8* %6, i32 %7, i8* %8, i32 %9, i32 %10, i32 %11, %struct.InterpFilterParams** %12, i32 %14, i32 %16, i32 %18, i32 %20, i32 1, %struct.ConvolveParams* %21, %struct.scale_factors* %22, i32 %23)
  br label %if.end

if.else:                                          ; preds = %entry
  %24 = bitcast %struct.SubpelParams* %sp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #7
  %25 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %26 = bitcast %struct.SubpelParams* %sp to i8*
  %27 = bitcast %struct.SubpelParams* %25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false), !tbaa.struct !61
  call void @revert_scale_extra_bits(%struct.SubpelParams* %sp)
  %28 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %29 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %30 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %31 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %32 = load i32, i32* %w.addr, align 4, !tbaa !6
  %33 = load i32, i32* %h.addr, align 4, !tbaa !6
  %34 = load %struct.InterpFilterParams**, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  %subpel_x3 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %sp, i32 0, i32 2
  %35 = load i32, i32* %subpel_x3, align 4, !tbaa !59
  %xs4 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %sp, i32 0, i32 0
  %36 = load i32, i32* %xs4, align 4, !tbaa !56
  %subpel_y5 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %sp, i32 0, i32 3
  %37 = load i32, i32* %subpel_y5, align 4, !tbaa !60
  %ys6 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %sp, i32 0, i32 1
  %38 = load i32, i32* %ys6, align 4, !tbaa !58
  %39 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %40 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %41 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @av1_highbd_convolve_2d_facade(i8* %28, i32 %29, i8* %30, i32 %31, i32 %32, i32 %33, %struct.InterpFilterParams** %34, i32 %35, i32 %36, i32 %37, i32 %38, i32 0, %struct.ConvolveParams* %39, %struct.scale_factors* %40, i32 %41)
  %42 = bitcast %struct.SubpelParams* %sp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %43 = bitcast i32* %is_scaled to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @inter_predictor(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, %struct.SubpelParams* %subpel_params, %struct.scale_factors* %sf, i32 %w, i32 %h, %struct.ConvolveParams* %conv_params, %struct.InterpFilterParams** %interp_filters) #1 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %subpel_params.addr = alloca %struct.SubpelParams*, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %interp_filters.addr = alloca %struct.InterpFilterParams**, align 4
  %is_scaled = alloca i32, align 4
  %sp = alloca %struct.SubpelParams, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store %struct.SubpelParams* %subpel_params, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams** %interp_filters, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  %0 = bitcast i32* %is_scaled to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %xs = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %1, i32 0, i32 0
  %2 = load i32, i32* %xs, align 4, !tbaa !56
  %3 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %ys = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %3, i32 0, i32 1
  %4 = load i32, i32* %ys, align 4, !tbaa !58
  %call = call i32 @has_scale(i32 %2, i32 %4)
  store i32 %call, i32* %is_scaled, align 4, !tbaa !6
  %5 = load i32, i32* %is_scaled, align 4, !tbaa !6
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %7 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %9 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %10 = load i32, i32* %w.addr, align 4, !tbaa !6
  %11 = load i32, i32* %h.addr, align 4, !tbaa !6
  %12 = load %struct.InterpFilterParams**, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  %13 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %subpel_x = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %13, i32 0, i32 2
  %14 = load i32, i32* %subpel_x, align 4, !tbaa !59
  %15 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %xs1 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %15, i32 0, i32 0
  %16 = load i32, i32* %xs1, align 4, !tbaa !56
  %17 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %subpel_y = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %17, i32 0, i32 3
  %18 = load i32, i32* %subpel_y, align 4, !tbaa !60
  %19 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %ys2 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %19, i32 0, i32 1
  %20 = load i32, i32* %ys2, align 4, !tbaa !58
  %21 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %22 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  call void @av1_convolve_2d_facade(i8* %6, i32 %7, i8* %8, i32 %9, i32 %10, i32 %11, %struct.InterpFilterParams** %12, i32 %14, i32 %16, i32 %18, i32 %20, i32 1, %struct.ConvolveParams* %21, %struct.scale_factors* %22)
  br label %if.end

if.else:                                          ; preds = %entry
  %23 = bitcast %struct.SubpelParams* %sp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #7
  %24 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %25 = bitcast %struct.SubpelParams* %sp to i8*
  %26 = bitcast %struct.SubpelParams* %24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false), !tbaa.struct !61
  call void @revert_scale_extra_bits(%struct.SubpelParams* %sp)
  %27 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %28 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %29 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %30 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %31 = load i32, i32* %w.addr, align 4, !tbaa !6
  %32 = load i32, i32* %h.addr, align 4, !tbaa !6
  %33 = load %struct.InterpFilterParams**, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  %subpel_x3 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %sp, i32 0, i32 2
  %34 = load i32, i32* %subpel_x3, align 4, !tbaa !59
  %xs4 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %sp, i32 0, i32 0
  %35 = load i32, i32* %xs4, align 4, !tbaa !56
  %subpel_y5 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %sp, i32 0, i32 3
  %36 = load i32, i32* %subpel_y5, align 4, !tbaa !60
  %ys6 = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %sp, i32 0, i32 1
  %37 = load i32, i32* %ys6, align 4, !tbaa !58
  %38 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %39 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  call void @av1_convolve_2d_facade(i8* %27, i32 %28, i8* %29, i32 %30, i32 %31, i32 %32, %struct.InterpFilterParams** %33, i32 %34, i32 %35, i32 %36, i32 %37, i32 0, %struct.ConvolveParams* %38, %struct.scale_factors* %39)
  %40 = bitcast %struct.SubpelParams* %sp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %41 = bitcast i32* %is_scaled to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  ret void
}

; Function Attrs: nounwind
define hidden i8* @av1_get_compound_type_mask(%struct.INTERINTER_COMPOUND_DATA* %comp_data, i8 zeroext %sb_type) #0 {
entry:
  %retval = alloca i8*, align 4
  %comp_data.addr = alloca %struct.INTERINTER_COMPOUND_DATA*, align 4
  %sb_type.addr = alloca i8, align 1
  store %struct.INTERINTER_COMPOUND_DATA* %comp_data, %struct.INTERINTER_COMPOUND_DATA** %comp_data.addr, align 4, !tbaa !2
  store i8 %sb_type, i8* %sb_type.addr, align 1, !tbaa !9
  %0 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %1 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %comp_data.addr, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %1, i32 0, i32 4
  %2 = load i8, i8* %type, align 1, !tbaa !62
  %conv = zext i8 %2 to i32
  switch i32 %conv, label %sw.default [
    i32 2, label %sw.bb
    i32 3, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry
  %3 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %comp_data.addr, align 4, !tbaa !2
  %wedge_index = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %3, i32 0, i32 1
  %4 = load i8, i8* %wedge_index, align 4, !tbaa !63
  %5 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %comp_data.addr, align 4, !tbaa !2
  %wedge_sign = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %5, i32 0, i32 2
  %6 = load i8, i8* %wedge_sign, align 1, !tbaa !64
  %7 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %call = call i8* @av1_get_contiguous_soft_mask(i8 signext %4, i8 signext %6, i8 zeroext %7)
  store i8* %call, i8** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  %8 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %comp_data.addr, align 4, !tbaa !2
  %seg_mask = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %8, i32 0, i32 0
  %9 = load i8*, i8** %seg_mask, align 4, !tbaa !65
  store i8* %9, i8** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb1, %sw.bb
  %10 = load i8*, i8** %retval, align 4
  ret i8* %10
}

; Function Attrs: inlinehint nounwind
define internal i8* @av1_get_contiguous_soft_mask(i8 signext %wedge_index, i8 signext %wedge_sign, i8 zeroext %sb_type) #1 {
entry:
  %wedge_index.addr = alloca i8, align 1
  %wedge_sign.addr = alloca i8, align 1
  %sb_type.addr = alloca i8, align 1
  store i8 %wedge_index, i8* %wedge_index.addr, align 1, !tbaa !9
  store i8 %wedge_sign, i8* %wedge_sign.addr, align 1, !tbaa !9
  store i8 %sb_type, i8* %sb_type.addr, align 1, !tbaa !9
  %0 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x %struct.wedge_params_type], [22 x %struct.wedge_params_type]* @av1_wedge_params_lookup, i32 0, i32 %idxprom
  %masks = getelementptr inbounds %struct.wedge_params_type, %struct.wedge_params_type* %arrayidx, i32 0, i32 3
  %1 = load [16 x i8*]*, [16 x i8*]** %masks, align 4, !tbaa !66
  %2 = load i8, i8* %wedge_sign.addr, align 1, !tbaa !9
  %idxprom1 = sext i8 %2 to i32
  %arrayidx2 = getelementptr inbounds [16 x i8*], [16 x i8*]* %1, i32 %idxprom1
  %3 = load i8, i8* %wedge_index.addr, align 1, !tbaa !9
  %idxprom3 = sext i8 %3 to i32
  %arrayidx4 = getelementptr inbounds [16 x i8*], [16 x i8*]* %arrayidx2, i32 0, i32 %idxprom3
  %4 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  ret i8* %4
}

; Function Attrs: nounwind
define hidden void @av1_build_compound_diffwtd_mask_d16_c(i8* %mask, i8 zeroext %mask_type, i16* %src0, i32 %src0_stride, i16* %src1, i32 %src1_stride, i32 %h, i32 %w, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %mask.addr = alloca i8*, align 4
  %mask_type.addr = alloca i8, align 1
  %src0.addr = alloca i16*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i16*, align 4
  %src1_stride.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i8 %mask_type, i8* %mask_type.addr, align 1, !tbaa !9
  store i16* %src0, i16** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i16* %src1, i16** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i8, i8* %mask_type.addr, align 1, !tbaa !9
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %3 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %4 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %5 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %6 = load i32, i32* %h.addr, align 4, !tbaa !6
  %7 = load i32, i32* %w.addr, align 4, !tbaa !6
  %8 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %9 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @diffwtd_mask_d16(i8* %1, i32 0, i32 38, i16* %2, i32 %3, i16* %4, i32 %5, i32 %6, i32 %7, %struct.ConvolveParams* %8, i32 %9)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  %10 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %11 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %12 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %13 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %14 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %15 = load i32, i32* %h.addr, align 4, !tbaa !6
  %16 = load i32, i32* %w.addr, align 4, !tbaa !6
  %17 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %18 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @diffwtd_mask_d16(i8* %10, i32 1, i32 38, i16* %11, i32 %12, i16* %13, i32 %14, i32 %15, i32 %16, %struct.ConvolveParams* %17, i32 %18)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @diffwtd_mask_d16(i8* %mask, i32 %which_inverse, i32 %mask_base, i16* %src0, i32 %src0_stride, i16* %src1, i32 %src1_stride, i32 %h, i32 %w, %struct.ConvolveParams* %conv_params, i32 %bd) #1 {
entry:
  %mask.addr = alloca i8*, align 4
  %which_inverse.addr = alloca i32, align 4
  %mask_base.addr = alloca i32, align 4
  %src0.addr = alloca i16*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i16*, align 4
  %src1_stride.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %round = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %m = alloca i32, align 4
  %diff = alloca i32, align 4
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %which_inverse, i32* %which_inverse.addr, align 4, !tbaa !6
  store i32 %mask_base, i32* %mask_base.addr, align 4, !tbaa !6
  store i16* %src0, i16** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i16* %src1, i16** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %round to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %1, i32 0, i32 3
  %2 = load i32, i32* %round_0, align 4, !tbaa !68
  %sub = sub nsw i32 14, %2
  %3 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %3, i32 0, i32 4
  %4 = load i32, i32* %round_1, align 4, !tbaa !69
  %sub1 = sub nsw i32 %sub, %4
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub2 = sub nsw i32 %5, 8
  %add = add nsw i32 %sub1, %sub2
  store i32 %add, i32* %round, align 4, !tbaa !6
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.end23

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %13 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %12, %13
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond3
  %14 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %16 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %15, %16
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %add6 = add nsw i32 %mul, %17
  %arrayidx = getelementptr inbounds i16, i16* %14, i32 %add6
  %18 = load i16, i16* %arrayidx, align 2, !tbaa !10
  %conv = zext i16 %18 to i32
  %19 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul7 = mul nsw i32 %20, %21
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %add8 = add nsw i32 %mul7, %22
  %arrayidx9 = getelementptr inbounds i16, i16* %19, i32 %add8
  %23 = load i16, i16* %arrayidx9, align 2, !tbaa !10
  %conv10 = zext i16 %23 to i32
  %sub11 = sub nsw i32 %conv, %conv10
  %call = call i32 @abs(i32 %sub11) #8
  store i32 %call, i32* %diff, align 4, !tbaa !6
  %24 = load i32, i32* %diff, align 4, !tbaa !6
  %25 = load i32, i32* %round, align 4, !tbaa !6
  %shl = shl i32 1, %25
  %shr = ashr i32 %shl, 1
  %add12 = add nsw i32 %24, %shr
  %26 = load i32, i32* %round, align 4, !tbaa !6
  %shr13 = ashr i32 %add12, %26
  store i32 %shr13, i32* %diff, align 4, !tbaa !6
  %27 = load i32, i32* %mask_base.addr, align 4, !tbaa !6
  %28 = load i32, i32* %diff, align 4, !tbaa !6
  %div = sdiv i32 %28, 16
  %add14 = add nsw i32 %27, %div
  %call15 = call i32 @clamp(i32 %add14, i32 0, i32 64)
  store i32 %call15, i32* %m, align 4, !tbaa !6
  %29 = load i32, i32* %which_inverse.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %29, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body5
  %30 = load i32, i32* %m, align 4, !tbaa !6
  %sub16 = sub nsw i32 64, %30
  br label %cond.end

cond.false:                                       ; preds = %for.body5
  %31 = load i32, i32* %m, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub16, %cond.true ], [ %31, %cond.false ]
  %conv17 = trunc i32 %cond to i8
  %32 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %34 = load i32, i32* %w.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 %33, %34
  %35 = load i32, i32* %j, align 4, !tbaa !6
  %add19 = add nsw i32 %mul18, %35
  %arrayidx20 = getelementptr inbounds i8, i8* %32, i32 %add19
  store i8 %conv17, i8* %arrayidx20, align 1, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %36 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc21

for.inc21:                                        ; preds = %for.end
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %inc22 = add nsw i32 %37, 1
  store i32 %inc22, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end23:                                        ; preds = %for.cond
  %38 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  %40 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  %42 = bitcast i32* %round to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_build_compound_diffwtd_mask_c(i8* %mask, i8 zeroext %mask_type, i8* %src0, i32 %src0_stride, i8* %src1, i32 %src1_stride, i32 %h, i32 %w) #0 {
entry:
  %mask.addr = alloca i8*, align 4
  %mask_type.addr = alloca i8, align 1
  %src0.addr = alloca i8*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i8*, align 4
  %src1_stride.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i8 %mask_type, i8* %mask_type.addr, align 1, !tbaa !9
  store i8* %src0, i8** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i8* %src1, i8** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  %0 = load i8, i8* %mask_type.addr, align 1, !tbaa !9
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %2 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %3 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %4 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %5 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %6 = load i32, i32* %h.addr, align 4, !tbaa !6
  %7 = load i32, i32* %w.addr, align 4, !tbaa !6
  call void @diffwtd_mask(i8* %1, i32 0, i32 38, i8* %2, i32 %3, i8* %4, i32 %5, i32 %6, i32 %7)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  %8 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %10 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %11 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %12 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %13 = load i32, i32* %h.addr, align 4, !tbaa !6
  %14 = load i32, i32* %w.addr, align 4, !tbaa !6
  call void @diffwtd_mask(i8* %8, i32 1, i32 38, i8* %9, i32 %10, i8* %11, i32 %12, i32 %13, i32 %14)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @diffwtd_mask(i8* %mask, i32 %which_inverse, i32 %mask_base, i8* %src0, i32 %src0_stride, i8* %src1, i32 %src1_stride, i32 %h, i32 %w) #1 {
entry:
  %mask.addr = alloca i8*, align 4
  %which_inverse.addr = alloca i32, align 4
  %mask_base.addr = alloca i32, align 4
  %src0.addr = alloca i8*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i8*, align 4
  %src1_stride.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %m = alloca i32, align 4
  %diff = alloca i32, align 4
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %which_inverse, i32* %which_inverse.addr, align 4, !tbaa !6
  store i32 %mask_base, i32* %mask_base.addr, align 4, !tbaa !6
  store i8* %src0, i8** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i8* %src1, i8** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %5 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %7 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %6, %7
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %8 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %9, %10
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %mul, %11
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %add
  %12 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %12 to i32
  %13 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 %14, %15
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %add5 = add nsw i32 %mul4, %16
  %arrayidx6 = getelementptr inbounds i8, i8* %13, i32 %add5
  %17 = load i8, i8* %arrayidx6, align 1, !tbaa !9
  %conv7 = zext i8 %17 to i32
  %sub = sub nsw i32 %conv, %conv7
  %call = call i32 @abs(i32 %sub) #8
  store i32 %call, i32* %diff, align 4, !tbaa !6
  %18 = load i32, i32* %mask_base.addr, align 4, !tbaa !6
  %19 = load i32, i32* %diff, align 4, !tbaa !6
  %div = sdiv i32 %19, 16
  %add8 = add nsw i32 %18, %div
  %call9 = call i32 @clamp(i32 %add8, i32 0, i32 64)
  store i32 %call9, i32* %m, align 4, !tbaa !6
  %20 = load i32, i32* %which_inverse.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %20, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body3
  %21 = load i32, i32* %m, align 4, !tbaa !6
  %sub10 = sub nsw i32 64, %21
  br label %cond.end

cond.false:                                       ; preds = %for.body3
  %22 = load i32, i32* %m, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub10, %cond.true ], [ %22, %cond.false ]
  %conv11 = trunc i32 %cond to i8
  %23 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %25 = load i32, i32* %w.addr, align 4, !tbaa !6
  %mul12 = mul nsw i32 %24, %25
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %add13 = add nsw i32 %mul12, %26
  %arrayidx14 = getelementptr inbounds i8, i8* %23, i32 %add13
  store i8 %conv11, i8* %arrayidx14, align 1, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %inc16 = add nsw i32 %28, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  %29 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #7
  %30 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_build_compound_diffwtd_mask_highbd_c(i8* %mask, i8 zeroext %mask_type, i8* %src0, i32 %src0_stride, i8* %src1, i32 %src1_stride, i32 %h, i32 %w, i32 %bd) #0 {
entry:
  %mask.addr = alloca i8*, align 4
  %mask_type.addr = alloca i8, align 1
  %src0.addr = alloca i8*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i8*, align 4
  %src1_stride.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i8 %mask_type, i8* %mask_type.addr, align 1, !tbaa !9
  store i8* %src0, i8** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i8* %src1, i8** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i8, i8* %mask_type.addr, align 1, !tbaa !9
  %conv = zext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb2
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %2 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %3 = ptrtoint i8* %2 to i32
  %shl = shl i32 %3, 1
  %4 = inttoptr i32 %shl to i16*
  %5 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %6 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %7 = ptrtoint i8* %6 to i32
  %shl1 = shl i32 %7, 1
  %8 = inttoptr i32 %shl1 to i16*
  %9 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %10 = load i32, i32* %h.addr, align 4, !tbaa !6
  %11 = load i32, i32* %w.addr, align 4, !tbaa !6
  %12 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @diffwtd_mask_highbd(i8* %1, i32 0, i32 38, i16* %4, i32 %5, i16* %8, i32 %9, i32 %10, i32 %11, i32 %12)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %13 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %14 = load i8*, i8** %src0.addr, align 4, !tbaa !2
  %15 = ptrtoint i8* %14 to i32
  %shl3 = shl i32 %15, 1
  %16 = inttoptr i32 %shl3 to i16*
  %17 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %18 = load i8*, i8** %src1.addr, align 4, !tbaa !2
  %19 = ptrtoint i8* %18 to i32
  %shl4 = shl i32 %19, 1
  %20 = inttoptr i32 %shl4 to i16*
  %21 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %22 = load i32, i32* %h.addr, align 4, !tbaa !6
  %23 = load i32, i32* %w.addr, align 4, !tbaa !6
  %24 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @diffwtd_mask_highbd(i8* %13, i32 1, i32 38, i16* %16, i32 %17, i16* %20, i32 %21, i32 %22, i32 %23, i32 %24)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb2, %sw.bb
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @diffwtd_mask_highbd(i8* %mask, i32 %which_inverse, i32 %mask_base, i16* %src0, i32 %src0_stride, i16* %src1, i32 %src1_stride, i32 %h, i32 %w, i32 %bd) #4 {
entry:
  %mask.addr = alloca i8*, align 4
  %which_inverse.addr = alloca i32, align 4
  %mask_base.addr = alloca i32, align 4
  %src0.addr = alloca i16*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i16*, align 4
  %src1_stride.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %diff = alloca i32, align 4
  %m = alloca i32, align 4
  %i20 = alloca i32, align 4
  %j26 = alloca i32, align 4
  %diff32 = alloca i32, align 4
  %m40 = alloca i32, align 4
  %bd_shift = alloca i32, align 4
  %i64 = alloca i32, align 4
  %j70 = alloca i32, align 4
  %diff76 = alloca i32, align 4
  %m84 = alloca i32, align 4
  %i106 = alloca i32, align 4
  %j112 = alloca i32, align 4
  %diff118 = alloca i32, align 4
  %m127 = alloca i32, align 4
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %which_inverse, i32* %which_inverse.addr, align 4, !tbaa !6
  store i32 %mask_base, i32* %mask_base.addr, align 4, !tbaa !6
  store i16* %src0, i16** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i16* %src1, i16** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, 8
  br i1 %cmp, label %if.then, label %if.else60

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %which_inverse.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.then
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %if.then1
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  br label %for.end19

for.body:                                         ; preds = %for.cond
  %6 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %7 = load i32, i32* %j, align 4, !tbaa !6
  %8 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %7, %8
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  %9 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  br label %for.end

for.body6:                                        ; preds = %for.cond3
  %10 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %11, i32 %12
  %13 = load i16, i16* %arrayidx, align 2, !tbaa !10
  %conv = zext i16 %13 to i32
  %14 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds i16, i16* %14, i32 %15
  %16 = load i16, i16* %arrayidx7, align 2, !tbaa !10
  %conv8 = zext i16 %16 to i32
  %sub = sub nsw i32 %conv, %conv8
  %call = call i32 @abs(i32 %sub) #8
  %div = sdiv i32 %call, 16
  store i32 %div, i32* %diff, align 4, !tbaa !6
  %17 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = load i32, i32* %mask_base.addr, align 4, !tbaa !6
  %19 = load i32, i32* %diff, align 4, !tbaa !6
  %add = add nsw i32 %18, %19
  %call9 = call i32 @negative_to_zero(i32 %add)
  store i32 %call9, i32* %m, align 4, !tbaa !6
  %20 = load i32, i32* %m, align 4, !tbaa !6
  %cmp10 = icmp ult i32 %20, 64
  br i1 %cmp10, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body6
  %21 = load i32, i32* %m, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %21, %cond.true ], [ 64, %cond.false ]
  store i32 %cond, i32* %m, align 4, !tbaa !6
  %22 = load i32, i32* %m, align 4, !tbaa !6
  %sub12 = sub i32 64, %22
  %conv13 = trunc i32 %sub12 to i8
  %23 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %24 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds i8, i8* %23, i32 %24
  store i8 %conv13, i8* %arrayidx14, align 1, !tbaa !9
  %25 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  %26 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond3

for.end:                                          ; preds = %for.cond.cleanup5
  %28 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %29 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %29, i32 %28
  store i16* %add.ptr, i16** %src0.addr, align 4, !tbaa !2
  %30 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %31 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %add.ptr15 = getelementptr inbounds i16, i16* %31, i32 %30
  store i16* %add.ptr15, i16** %src1.addr, align 4, !tbaa !2
  %32 = load i32, i32* %w.addr, align 4, !tbaa !6
  %33 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr16 = getelementptr inbounds i8, i8* %33, i32 %32
  store i8* %add.ptr16, i8** %mask.addr, align 4, !tbaa !2
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %inc18 = add nsw i32 %34, 1
  store i32 %inc18, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end19:                                        ; preds = %for.cond.cleanup
  br label %if.end

if.else:                                          ; preds = %if.then
  %35 = bitcast i32* %i20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  store i32 0, i32* %i20, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc57, %if.else
  %36 = load i32, i32* %i20, align 4, !tbaa !6
  %37 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %36, %37
  br i1 %cmp22, label %for.body25, label %for.cond.cleanup24

for.cond.cleanup24:                               ; preds = %for.cond21
  store i32 8, i32* %cleanup.dest.slot, align 4
  %38 = bitcast i32* %i20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  br label %for.end59

for.body25:                                       ; preds = %for.cond21
  %39 = bitcast i32* %j26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #7
  store i32 0, i32* %j26, align 4, !tbaa !6
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc51, %for.body25
  %40 = load i32, i32* %j26, align 4, !tbaa !6
  %41 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp28 = icmp slt i32 %40, %41
  br i1 %cmp28, label %for.body31, label %for.cond.cleanup30

for.cond.cleanup30:                               ; preds = %for.cond27
  store i32 11, i32* %cleanup.dest.slot, align 4
  %42 = bitcast i32* %j26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  br label %for.end53

for.body31:                                       ; preds = %for.cond27
  %43 = bitcast i32* %diff32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  %44 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %45 = load i32, i32* %j26, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds i16, i16* %44, i32 %45
  %46 = load i16, i16* %arrayidx33, align 2, !tbaa !10
  %conv34 = zext i16 %46 to i32
  %47 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %48 = load i32, i32* %j26, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds i16, i16* %47, i32 %48
  %49 = load i16, i16* %arrayidx35, align 2, !tbaa !10
  %conv36 = zext i16 %49 to i32
  %sub37 = sub nsw i32 %conv34, %conv36
  %call38 = call i32 @abs(i32 %sub37) #8
  %div39 = sdiv i32 %call38, 16
  store i32 %div39, i32* %diff32, align 4, !tbaa !6
  %50 = bitcast i32* %m40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load i32, i32* %mask_base.addr, align 4, !tbaa !6
  %52 = load i32, i32* %diff32, align 4, !tbaa !6
  %add41 = add nsw i32 %51, %52
  %call42 = call i32 @negative_to_zero(i32 %add41)
  store i32 %call42, i32* %m40, align 4, !tbaa !6
  %53 = load i32, i32* %m40, align 4, !tbaa !6
  %cmp43 = icmp ult i32 %53, 64
  br i1 %cmp43, label %cond.true45, label %cond.false46

cond.true45:                                      ; preds = %for.body31
  %54 = load i32, i32* %m40, align 4, !tbaa !6
  br label %cond.end47

cond.false46:                                     ; preds = %for.body31
  br label %cond.end47

cond.end47:                                       ; preds = %cond.false46, %cond.true45
  %cond48 = phi i32 [ %54, %cond.true45 ], [ 64, %cond.false46 ]
  store i32 %cond48, i32* %m40, align 4, !tbaa !6
  %55 = load i32, i32* %m40, align 4, !tbaa !6
  %conv49 = trunc i32 %55 to i8
  %56 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %57 = load i32, i32* %j26, align 4, !tbaa !6
  %arrayidx50 = getelementptr inbounds i8, i8* %56, i32 %57
  store i8 %conv49, i8* %arrayidx50, align 1, !tbaa !9
  %58 = bitcast i32* %m40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #7
  %59 = bitcast i32* %diff32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #7
  br label %for.inc51

for.inc51:                                        ; preds = %cond.end47
  %60 = load i32, i32* %j26, align 4, !tbaa !6
  %inc52 = add nsw i32 %60, 1
  store i32 %inc52, i32* %j26, align 4, !tbaa !6
  br label %for.cond27

for.end53:                                        ; preds = %for.cond.cleanup30
  %61 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %62 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %add.ptr54 = getelementptr inbounds i16, i16* %62, i32 %61
  store i16* %add.ptr54, i16** %src0.addr, align 4, !tbaa !2
  %63 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %64 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %add.ptr55 = getelementptr inbounds i16, i16* %64, i32 %63
  store i16* %add.ptr55, i16** %src1.addr, align 4, !tbaa !2
  %65 = load i32, i32* %w.addr, align 4, !tbaa !6
  %66 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr56 = getelementptr inbounds i8, i8* %66, i32 %65
  store i8* %add.ptr56, i8** %mask.addr, align 4, !tbaa !2
  br label %for.inc57

for.inc57:                                        ; preds = %for.end53
  %67 = load i32, i32* %i20, align 4, !tbaa !6
  %inc58 = add nsw i32 %67, 1
  store i32 %inc58, i32* %i20, align 4, !tbaa !6
  br label %for.cond21

for.end59:                                        ; preds = %for.cond.cleanup24
  br label %if.end

if.end:                                           ; preds = %for.end59, %for.end19
  br label %if.end148

if.else60:                                        ; preds = %entry
  %68 = bitcast i32* %bd_shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #7
  %69 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %sub61 = sub i32 %69, 8
  store i32 %sub61, i32* %bd_shift, align 4, !tbaa !6
  %70 = load i32, i32* %which_inverse.addr, align 4, !tbaa !6
  %tobool62 = icmp ne i32 %70, 0
  br i1 %tobool62, label %if.then63, label %if.else105

if.then63:                                        ; preds = %if.else60
  %71 = bitcast i32* %i64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #7
  store i32 0, i32* %i64, align 4, !tbaa !6
  br label %for.cond65

for.cond65:                                       ; preds = %for.inc102, %if.then63
  %72 = load i32, i32* %i64, align 4, !tbaa !6
  %73 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp66 = icmp slt i32 %72, %73
  br i1 %cmp66, label %for.body69, label %for.cond.cleanup68

for.cond.cleanup68:                               ; preds = %for.cond65
  store i32 14, i32* %cleanup.dest.slot, align 4
  %74 = bitcast i32* %i64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #7
  br label %for.end104

for.body69:                                       ; preds = %for.cond65
  %75 = bitcast i32* %j70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #7
  store i32 0, i32* %j70, align 4, !tbaa !6
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc96, %for.body69
  %76 = load i32, i32* %j70, align 4, !tbaa !6
  %77 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp72 = icmp slt i32 %76, %77
  br i1 %cmp72, label %for.body75, label %for.cond.cleanup74

for.cond.cleanup74:                               ; preds = %for.cond71
  store i32 17, i32* %cleanup.dest.slot, align 4
  %78 = bitcast i32* %j70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #7
  br label %for.end98

for.body75:                                       ; preds = %for.cond71
  %79 = bitcast i32* %diff76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #7
  %80 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %81 = load i32, i32* %j70, align 4, !tbaa !6
  %arrayidx77 = getelementptr inbounds i16, i16* %80, i32 %81
  %82 = load i16, i16* %arrayidx77, align 2, !tbaa !10
  %conv78 = zext i16 %82 to i32
  %83 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %84 = load i32, i32* %j70, align 4, !tbaa !6
  %arrayidx79 = getelementptr inbounds i16, i16* %83, i32 %84
  %85 = load i16, i16* %arrayidx79, align 2, !tbaa !10
  %conv80 = zext i16 %85 to i32
  %sub81 = sub nsw i32 %conv78, %conv80
  %call82 = call i32 @abs(i32 %sub81) #8
  %86 = load i32, i32* %bd_shift, align 4, !tbaa !6
  %shr = ashr i32 %call82, %86
  %div83 = sdiv i32 %shr, 16
  store i32 %div83, i32* %diff76, align 4, !tbaa !6
  %87 = bitcast i32* %m84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #7
  %88 = load i32, i32* %mask_base.addr, align 4, !tbaa !6
  %89 = load i32, i32* %diff76, align 4, !tbaa !6
  %add85 = add nsw i32 %88, %89
  %call86 = call i32 @negative_to_zero(i32 %add85)
  store i32 %call86, i32* %m84, align 4, !tbaa !6
  %90 = load i32, i32* %m84, align 4, !tbaa !6
  %cmp87 = icmp ult i32 %90, 64
  br i1 %cmp87, label %cond.true89, label %cond.false90

cond.true89:                                      ; preds = %for.body75
  %91 = load i32, i32* %m84, align 4, !tbaa !6
  br label %cond.end91

cond.false90:                                     ; preds = %for.body75
  br label %cond.end91

cond.end91:                                       ; preds = %cond.false90, %cond.true89
  %cond92 = phi i32 [ %91, %cond.true89 ], [ 64, %cond.false90 ]
  store i32 %cond92, i32* %m84, align 4, !tbaa !6
  %92 = load i32, i32* %m84, align 4, !tbaa !6
  %sub93 = sub i32 64, %92
  %conv94 = trunc i32 %sub93 to i8
  %93 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %94 = load i32, i32* %j70, align 4, !tbaa !6
  %arrayidx95 = getelementptr inbounds i8, i8* %93, i32 %94
  store i8 %conv94, i8* %arrayidx95, align 1, !tbaa !9
  %95 = bitcast i32* %m84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #7
  %96 = bitcast i32* %diff76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #7
  br label %for.inc96

for.inc96:                                        ; preds = %cond.end91
  %97 = load i32, i32* %j70, align 4, !tbaa !6
  %inc97 = add nsw i32 %97, 1
  store i32 %inc97, i32* %j70, align 4, !tbaa !6
  br label %for.cond71

for.end98:                                        ; preds = %for.cond.cleanup74
  %98 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %99 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %add.ptr99 = getelementptr inbounds i16, i16* %99, i32 %98
  store i16* %add.ptr99, i16** %src0.addr, align 4, !tbaa !2
  %100 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %101 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %add.ptr100 = getelementptr inbounds i16, i16* %101, i32 %100
  store i16* %add.ptr100, i16** %src1.addr, align 4, !tbaa !2
  %102 = load i32, i32* %w.addr, align 4, !tbaa !6
  %103 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr101 = getelementptr inbounds i8, i8* %103, i32 %102
  store i8* %add.ptr101, i8** %mask.addr, align 4, !tbaa !2
  br label %for.inc102

for.inc102:                                       ; preds = %for.end98
  %104 = load i32, i32* %i64, align 4, !tbaa !6
  %inc103 = add nsw i32 %104, 1
  store i32 %inc103, i32* %i64, align 4, !tbaa !6
  br label %for.cond65

for.end104:                                       ; preds = %for.cond.cleanup68
  br label %if.end147

if.else105:                                       ; preds = %if.else60
  %105 = bitcast i32* %i106 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #7
  store i32 0, i32* %i106, align 4, !tbaa !6
  br label %for.cond107

for.cond107:                                      ; preds = %for.inc144, %if.else105
  %106 = load i32, i32* %i106, align 4, !tbaa !6
  %107 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp108 = icmp slt i32 %106, %107
  br i1 %cmp108, label %for.body111, label %for.cond.cleanup110

for.cond.cleanup110:                              ; preds = %for.cond107
  store i32 20, i32* %cleanup.dest.slot, align 4
  %108 = bitcast i32* %i106 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #7
  br label %for.end146

for.body111:                                      ; preds = %for.cond107
  %109 = bitcast i32* %j112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #7
  store i32 0, i32* %j112, align 4, !tbaa !6
  br label %for.cond113

for.cond113:                                      ; preds = %for.inc138, %for.body111
  %110 = load i32, i32* %j112, align 4, !tbaa !6
  %111 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp114 = icmp slt i32 %110, %111
  br i1 %cmp114, label %for.body117, label %for.cond.cleanup116

for.cond.cleanup116:                              ; preds = %for.cond113
  store i32 23, i32* %cleanup.dest.slot, align 4
  %112 = bitcast i32* %j112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #7
  br label %for.end140

for.body117:                                      ; preds = %for.cond113
  %113 = bitcast i32* %diff118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #7
  %114 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %115 = load i32, i32* %j112, align 4, !tbaa !6
  %arrayidx119 = getelementptr inbounds i16, i16* %114, i32 %115
  %116 = load i16, i16* %arrayidx119, align 2, !tbaa !10
  %conv120 = zext i16 %116 to i32
  %117 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %118 = load i32, i32* %j112, align 4, !tbaa !6
  %arrayidx121 = getelementptr inbounds i16, i16* %117, i32 %118
  %119 = load i16, i16* %arrayidx121, align 2, !tbaa !10
  %conv122 = zext i16 %119 to i32
  %sub123 = sub nsw i32 %conv120, %conv122
  %call124 = call i32 @abs(i32 %sub123) #8
  %120 = load i32, i32* %bd_shift, align 4, !tbaa !6
  %shr125 = ashr i32 %call124, %120
  %div126 = sdiv i32 %shr125, 16
  store i32 %div126, i32* %diff118, align 4, !tbaa !6
  %121 = bitcast i32* %m127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #7
  %122 = load i32, i32* %mask_base.addr, align 4, !tbaa !6
  %123 = load i32, i32* %diff118, align 4, !tbaa !6
  %add128 = add nsw i32 %122, %123
  %call129 = call i32 @negative_to_zero(i32 %add128)
  store i32 %call129, i32* %m127, align 4, !tbaa !6
  %124 = load i32, i32* %m127, align 4, !tbaa !6
  %cmp130 = icmp ult i32 %124, 64
  br i1 %cmp130, label %cond.true132, label %cond.false133

cond.true132:                                     ; preds = %for.body117
  %125 = load i32, i32* %m127, align 4, !tbaa !6
  br label %cond.end134

cond.false133:                                    ; preds = %for.body117
  br label %cond.end134

cond.end134:                                      ; preds = %cond.false133, %cond.true132
  %cond135 = phi i32 [ %125, %cond.true132 ], [ 64, %cond.false133 ]
  store i32 %cond135, i32* %m127, align 4, !tbaa !6
  %126 = load i32, i32* %m127, align 4, !tbaa !6
  %conv136 = trunc i32 %126 to i8
  %127 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %128 = load i32, i32* %j112, align 4, !tbaa !6
  %arrayidx137 = getelementptr inbounds i8, i8* %127, i32 %128
  store i8 %conv136, i8* %arrayidx137, align 1, !tbaa !9
  %129 = bitcast i32* %m127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #7
  %130 = bitcast i32* %diff118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #7
  br label %for.inc138

for.inc138:                                       ; preds = %cond.end134
  %131 = load i32, i32* %j112, align 4, !tbaa !6
  %inc139 = add nsw i32 %131, 1
  store i32 %inc139, i32* %j112, align 4, !tbaa !6
  br label %for.cond113

for.end140:                                       ; preds = %for.cond.cleanup116
  %132 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %133 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %add.ptr141 = getelementptr inbounds i16, i16* %133, i32 %132
  store i16* %add.ptr141, i16** %src0.addr, align 4, !tbaa !2
  %134 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %135 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %add.ptr142 = getelementptr inbounds i16, i16* %135, i32 %134
  store i16* %add.ptr142, i16** %src1.addr, align 4, !tbaa !2
  %136 = load i32, i32* %w.addr, align 4, !tbaa !6
  %137 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr143 = getelementptr inbounds i8, i8* %137, i32 %136
  store i8* %add.ptr143, i8** %mask.addr, align 4, !tbaa !2
  br label %for.inc144

for.inc144:                                       ; preds = %for.end140
  %138 = load i32, i32* %i106, align 4, !tbaa !6
  %inc145 = add nsw i32 %138, 1
  store i32 %inc145, i32* %i106, align 4, !tbaa !6
  br label %for.cond107

for.end146:                                       ; preds = %for.cond.cleanup110
  br label %if.end147

if.end147:                                        ; preds = %for.end146, %for.end104
  %139 = bitcast i32* %bd_shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #7
  br label %if.end148

if.end148:                                        ; preds = %if.end147, %if.end
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_init_wedge_masks() #0 {
entry:
  call void @init_wedge_master_masks()
  call void @init_wedge_masks()
  call void @init_smooth_interintra_masks()
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_make_masked_inter_predictor(i8* %pre, i32 %pre_stride, i8* %dst, i32 %dst_stride, %struct.InterPredParams* %inter_pred_params, %struct.SubpelParams* %subpel_params) #0 {
entry:
  %pre.addr = alloca i8*, align 4
  %pre_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %inter_pred_params.addr = alloca %struct.InterPredParams*, align 4
  %subpel_params.addr = alloca %struct.SubpelParams*, align 4
  %comp_data = alloca %struct.INTERINTER_COMPOUND_DATA*, align 4
  %sb_type = alloca i8, align 1
  %tmp_buf = alloca [32768 x i8], align 32
  %tmp_dst = alloca i8*, align 4
  %tmp_buf_stride = alloca i32, align 4
  %org_dst = alloca i16*, align 4
  %org_dst_stride = alloca i32, align 4
  %tmp_buf16 = alloca i16*, align 4
  store i8* %pre, i8** %pre.addr, align 4, !tbaa !2
  store i32 %pre_stride, i32* %pre_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store %struct.InterPredParams* %inter_pred_params, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  store %struct.SubpelParams* %subpel_params, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  %0 = bitcast %struct.INTERINTER_COMPOUND_DATA** %comp_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %mask_comp = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %1, i32 0, i32 15
  store %struct.INTERINTER_COMPOUND_DATA* %mask_comp, %struct.INTERINTER_COMPOUND_DATA** %comp_data, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %sb_type) #7
  %2 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %sb_type1 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %2, i32 0, i32 16
  %3 = load i8, i8* %sb_type1, align 4, !tbaa !48
  store i8 %3, i8* %sb_type, align 1, !tbaa !9
  %4 = bitcast [32768 x i8]* %tmp_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 32768, i8* %4) #7
  %5 = bitcast i8** %tmp_dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %use_hbd_buf = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %6, i32 0, i32 14
  %7 = load i32, i32* %use_hbd_buf, align 4, !tbaa !35
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %arraydecay = getelementptr inbounds [32768 x i8], [32768 x i8]* %tmp_buf, i32 0, i32 0
  %8 = ptrtoint i8* %arraydecay to i32
  %shr = lshr i32 %8, 1
  %9 = inttoptr i32 %shr to i8*
  br label %cond.end

cond.false:                                       ; preds = %entry
  %arraydecay2 = getelementptr inbounds [32768 x i8], [32768 x i8]* %tmp_buf, i32 0, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %9, %cond.true ], [ %arraydecay2, %cond.false ]
  store i8* %cond, i8** %tmp_dst, align 4, !tbaa !2
  %10 = bitcast i32* %tmp_buf_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  store i32 128, i32* %tmp_buf_stride, align 4, !tbaa !6
  %11 = bitcast i16** %org_dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %12, i32 0, i32 3
  %dst3 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params, i32 0, i32 1
  %13 = load i16*, i16** %dst3, align 4, !tbaa !70
  store i16* %13, i16** %org_dst, align 4, !tbaa !2
  %14 = bitcast i32* %org_dst_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params4 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %15, i32 0, i32 3
  %dst_stride5 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params4, i32 0, i32 2
  %16 = load i32, i32* %dst_stride5, align 4, !tbaa !71
  store i32 %16, i32* %org_dst_stride, align 4, !tbaa !6
  %17 = bitcast i16** %tmp_buf16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %arraydecay6 = getelementptr inbounds [32768 x i8], [32768 x i8]* %tmp_buf, i32 0, i32 0
  %18 = bitcast i8* %arraydecay6 to i16*
  store i16* %18, i16** %tmp_buf16, align 4, !tbaa !2
  %19 = load i16*, i16** %tmp_buf16, align 4, !tbaa !2
  %20 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params7 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %20, i32 0, i32 3
  %dst8 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params7, i32 0, i32 1
  store i16* %19, i16** %dst8, align 4, !tbaa !70
  %21 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params9 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %21, i32 0, i32 3
  %dst_stride10 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params9, i32 0, i32 2
  store i32 128, i32* %dst_stride10, align 4, !tbaa !71
  %22 = load i8*, i8** %pre.addr, align 4, !tbaa !2
  %23 = load i32, i32* %pre_stride.addr, align 4, !tbaa !6
  %24 = load i8*, i8** %tmp_dst, align 4, !tbaa !2
  %25 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %26 = load %struct.SubpelParams*, %struct.SubpelParams** %subpel_params.addr, align 4, !tbaa !2
  call void @av1_make_inter_predictor(i8* %22, i32 %23, i8* %24, i32 128, %struct.InterPredParams* %25, %struct.SubpelParams* %26)
  %27 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params11 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %27, i32 0, i32 3
  %plane = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params11, i32 0, i32 5
  %28 = load i32, i32* %plane, align 4, !tbaa !72
  %tobool12 = icmp ne i32 %28, 0
  br i1 %tobool12, label %if.end, label %land.lhs.true

land.lhs.true:                                    ; preds = %cond.end
  %29 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %comp_data, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %29, i32 0, i32 4
  %30 = load i8, i8* %type, align 1, !tbaa !62
  %conv = zext i8 %30 to i32
  %cmp = icmp eq i32 %conv, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %31 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %comp_data, align 4, !tbaa !2
  %seg_mask = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %31, i32 0, i32 0
  %32 = load i8*, i8** %seg_mask, align 4, !tbaa !65
  %33 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %comp_data, align 4, !tbaa !2
  %mask_type = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %33, i32 0, i32 3
  %34 = load i8, i8* %mask_type, align 2, !tbaa !73
  %35 = load i16*, i16** %org_dst, align 4, !tbaa !2
  %36 = load i32, i32* %org_dst_stride, align 4, !tbaa !6
  %37 = load i16*, i16** %tmp_buf16, align 4, !tbaa !2
  %38 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_height = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %38, i32 0, i32 6
  %39 = load i32, i32* %block_height, align 4, !tbaa !29
  %40 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_width = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %40, i32 0, i32 5
  %41 = load i32, i32* %block_width, align 4, !tbaa !25
  %42 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params14 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %42, i32 0, i32 3
  %43 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %bit_depth = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %43, i32 0, i32 13
  %44 = load i32, i32* %bit_depth, align 4, !tbaa !34
  call void @av1_build_compound_diffwtd_mask_d16_c(i8* %32, i8 zeroext %34, i16* %35, i32 %36, i16* %37, i32 128, i32 %39, i32 %41, %struct.ConvolveParams* %conv_params14, i32 %44)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %cond.end
  %45 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %46 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %47 = load i16*, i16** %org_dst, align 4, !tbaa !2
  %48 = load i32, i32* %org_dst_stride, align 4, !tbaa !6
  %49 = load i16*, i16** %tmp_buf16, align 4, !tbaa !2
  %50 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %comp_data, align 4, !tbaa !2
  %51 = load i8, i8* %sb_type, align 1, !tbaa !9
  %52 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_height15 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %52, i32 0, i32 6
  %53 = load i32, i32* %block_height15, align 4, !tbaa !29
  %54 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %block_width16 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %54, i32 0, i32 5
  %55 = load i32, i32* %block_width16, align 4, !tbaa !25
  %56 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  call void @build_masked_compound_no_round(i8* %45, i32 %46, i16* %47, i32 %48, i16* %49, i32 128, %struct.INTERINTER_COMPOUND_DATA* %50, i8 zeroext %51, i32 %53, i32 %55, %struct.InterPredParams* %56)
  %57 = bitcast i16** %tmp_buf16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %58 = bitcast i32* %org_dst_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #7
  %59 = bitcast i16** %org_dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #7
  %60 = bitcast i32* %tmp_buf_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #7
  %61 = bitcast i8** %tmp_dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #7
  %62 = bitcast [32768 x i8]* %tmp_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 32768, i8* %62) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %sb_type) #7
  %63 = bitcast %struct.INTERINTER_COMPOUND_DATA** %comp_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define internal void @build_masked_compound_no_round(i8* %dst, i32 %dst_stride, i16* %src0, i32 %src0_stride, i16* %src1, i32 %src1_stride, %struct.INTERINTER_COMPOUND_DATA* %comp_data, i8 zeroext %sb_type, i32 %h, i32 %w, %struct.InterPredParams* %inter_pred_params) #1 {
entry:
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src0.addr = alloca i16*, align 4
  %src0_stride.addr = alloca i32, align 4
  %src1.addr = alloca i16*, align 4
  %src1_stride.addr = alloca i32, align 4
  %comp_data.addr = alloca %struct.INTERINTER_COMPOUND_DATA*, align 4
  %sb_type.addr = alloca i8, align 1
  %h.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %inter_pred_params.addr = alloca %struct.InterPredParams*, align 4
  %ssy = alloca i32, align 4
  %ssx = alloca i32, align 4
  %mask = alloca i8*, align 4
  %mask_stride = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i16* %src0, i16** %src0.addr, align 4, !tbaa !2
  store i32 %src0_stride, i32* %src0_stride.addr, align 4, !tbaa !6
  store i16* %src1, i16** %src1.addr, align 4, !tbaa !2
  store i32 %src1_stride, i32* %src1_stride.addr, align 4, !tbaa !6
  store %struct.INTERINTER_COMPOUND_DATA* %comp_data, %struct.INTERINTER_COMPOUND_DATA** %comp_data.addr, align 4, !tbaa !2
  store i8 %sb_type, i8* %sb_type.addr, align 1, !tbaa !9
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store %struct.InterPredParams* %inter_pred_params, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %1, i32 0, i32 11
  %2 = load i32, i32* %subsampling_y, align 4, !tbaa !33
  store i32 %2, i32* %ssy, align 4, !tbaa !6
  %3 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %4, i32 0, i32 10
  %5 = load i32, i32* %subsampling_x, align 4, !tbaa !32
  store i32 %5, i32* %ssx, align 4, !tbaa !6
  %6 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.INTERINTER_COMPOUND_DATA*, %struct.INTERINTER_COMPOUND_DATA** %comp_data.addr, align 4, !tbaa !2
  %8 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %call = call i8* @av1_get_compound_type_mask(%struct.INTERINTER_COMPOUND_DATA* %7, i8 zeroext %8)
  store i8* %call, i8** %mask, align 4, !tbaa !2
  %9 = bitcast i32* %mask_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %idxprom = zext i8 %10 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %11 to i32
  store i32 %conv, i32* %mask_stride, align 4, !tbaa !6
  %12 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %use_hbd_buf = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %12, i32 0, i32 14
  %13 = load i32, i32* %use_hbd_buf, align 4, !tbaa !35
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %14 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %15 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %16 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %17 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %18 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %19 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %20 = load i8*, i8** %mask, align 4, !tbaa !2
  %21 = load i32, i32* %mask_stride, align 4, !tbaa !6
  %22 = load i32, i32* %w.addr, align 4, !tbaa !6
  %23 = load i32, i32* %h.addr, align 4, !tbaa !6
  %24 = load i32, i32* %ssx, align 4, !tbaa !6
  %25 = load i32, i32* %ssy, align 4, !tbaa !6
  %26 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %26, i32 0, i32 3
  %27 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %bit_depth = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %27, i32 0, i32 13
  %28 = load i32, i32* %bit_depth, align 4, !tbaa !34
  call void @aom_highbd_blend_a64_d16_mask_c(i8* %14, i32 %15, i16* %16, i32 %17, i16* %18, i32 %19, i8* %20, i32 %21, i32 %22, i32 %23, i32 %24, i32 %25, %struct.ConvolveParams* %conv_params, i32 %28)
  br label %if.end

if.else:                                          ; preds = %entry
  %29 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %30 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %31 = load i16*, i16** %src0.addr, align 4, !tbaa !2
  %32 = load i32, i32* %src0_stride.addr, align 4, !tbaa !6
  %33 = load i16*, i16** %src1.addr, align 4, !tbaa !2
  %34 = load i32, i32* %src1_stride.addr, align 4, !tbaa !6
  %35 = load i8*, i8** %mask, align 4, !tbaa !2
  %36 = load i32, i32* %mask_stride, align 4, !tbaa !6
  %37 = load i32, i32* %w.addr, align 4, !tbaa !6
  %38 = load i32, i32* %h.addr, align 4, !tbaa !6
  %39 = load i32, i32* %ssx, align 4, !tbaa !6
  %40 = load i32, i32* %ssy, align 4, !tbaa !6
  %41 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %conv_params1 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %41, i32 0, i32 3
  call void @aom_lowbd_blend_a64_d16_mask_c(i8* %29, i32 %30, i16* %31, i32 %32, i16* %33, i32 %34, i8* %35, i32 %36, i32 %37, i32 %38, i32 %39, i32 %40, %struct.ConvolveParams* %conv_params1)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %42 = bitcast i32* %mask_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  %43 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  %44 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #7
  %45 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #7
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden void @av1_build_one_inter_predictor(i8* %dst, i32 %dst_stride, %struct.mv* %src_mv, %struct.InterPredParams* %inter_pred_params, %struct.macroblockd* %xd, i32 %mi_x, i32 %mi_y, i32 %ref, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %calc_subpel_params_func) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %src_mv.addr = alloca %struct.mv*, align 4
  %inter_pred_params.addr = alloca %struct.InterPredParams*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi_x.addr = alloca i32, align 4
  %mi_y.addr = alloca i32, align 4
  %ref.addr = alloca i32, align 4
  %calc_subpel_params_func.addr = alloca void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)*, align 4
  %subpel_params = alloca %struct.SubpelParams, align 4
  %src = alloca i8*, align 4
  %src_stride = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store %struct.mv* %src_mv, %struct.mv** %src_mv.addr, align 4, !tbaa !2
  store %struct.InterPredParams* %inter_pred_params, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %mi_x, i32* %mi_x.addr, align 4, !tbaa !6
  store i32 %mi_y, i32* %mi_y.addr, align 4, !tbaa !6
  store i32 %ref, i32* %ref.addr, align 4, !tbaa !6
  store void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %calc_subpel_params_func, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)** %calc_subpel_params_func.addr, align 4, !tbaa !2
  %0 = bitcast %struct.SubpelParams* %subpel_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast i32* %src_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)*, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)** %calc_subpel_params_func.addr, align 4, !tbaa !2
  %4 = load %struct.mv*, %struct.mv** %src_mv.addr, align 4, !tbaa !2
  %5 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %7 = load i32, i32* %mi_x.addr, align 4, !tbaa !6
  %8 = load i32, i32* %mi_y.addr, align 4, !tbaa !6
  %9 = load i32, i32* %ref.addr, align 4, !tbaa !6
  call void %3(%struct.mv* %4, %struct.InterPredParams* %5, %struct.macroblockd* %6, i32 %7, i32 %8, i32 %9, i8** %src, %struct.SubpelParams* %subpel_params, i32* %src_stride)
  %10 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %comp_mode = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %10, i32 0, i32 1
  %11 = load i32, i32* %comp_mode, align 4, !tbaa !40
  %cmp = icmp eq i32 %11, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %12 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  %comp_mode1 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %12, i32 0, i32 1
  %13 = load i32, i32* %comp_mode1, align 4, !tbaa !40
  %cmp2 = icmp eq i32 %13, 1
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %14 = load i8*, i8** %src, align 4, !tbaa !2
  %15 = load i32, i32* %src_stride, align 4, !tbaa !6
  %16 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %17 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %18 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  call void @av1_make_inter_predictor(i8* %14, i32 %15, i8* %16, i32 %17, %struct.InterPredParams* %18, %struct.SubpelParams* %subpel_params)
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  %19 = load i8*, i8** %src, align 4, !tbaa !2
  %20 = load i32, i32* %src_stride, align 4, !tbaa !6
  %21 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %22 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %23 = load %struct.InterPredParams*, %struct.InterPredParams** %inter_pred_params.addr, align 4, !tbaa !2
  call void @av1_make_masked_inter_predictor(i8* %19, i32 %20, i8* %21, i32 %22, %struct.InterPredParams* %23, %struct.SubpelParams* %subpel_params)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %24 = bitcast i32* %src_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %25 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  %26 = bitcast %struct.SubpelParams* %subpel_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_build_inter_predictors(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane, %struct.MB_MODE_INFO* %mi, i32 %build_for_obmc, i32 %bw, i32 %bh, i32 %mi_x, i32 %mi_y, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %calc_subpel_params_func) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %build_for_obmc.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %mi_x.addr = alloca i32, align 4
  %mi_y.addr = alloca i32, align 4
  %calc_subpel_params_func.addr = alloca void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mi, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  store i32 %build_for_obmc, i32* %build_for_obmc.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !6
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !6
  store i32 %mi_x, i32* %mi_x.addr, align 4, !tbaa !6
  store i32 %mi_y, i32* %mi_y.addr, align 4, !tbaa !6
  store void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %calc_subpel_params_func, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)** %calc_subpel_params_func.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %1 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 6
  %3 = load i8, i8* %sb_type, align 2, !tbaa !74
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %4)
  %5 = load i32, i32* %build_for_obmc.addr, align 4, !tbaa !6
  %call1 = call zeroext i1 @is_sub8x8_inter(%struct.macroblockd* %0, i32 %1, i8 zeroext %3, i32 %call, i32 %5)
  br i1 %call1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %8 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %9 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %10 = load i32, i32* %bw.addr, align 4, !tbaa !6
  %11 = load i32, i32* %bh.addr, align 4, !tbaa !6
  %12 = load i32, i32* %mi_x.addr, align 4, !tbaa !6
  %13 = load i32, i32* %mi_y.addr, align 4, !tbaa !6
  %14 = load void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)*, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)** %calc_subpel_params_func.addr, align 4, !tbaa !2
  call void @build_inter_predictors_sub8x8(%struct.AV1Common* %6, %struct.macroblockd* %7, i32 %8, %struct.MB_MODE_INFO* %9, i32 %10, i32 %11, i32 %12, i32 %13, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %14)
  br label %if.end

if.else:                                          ; preds = %entry
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %17 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %18 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %19 = load i32, i32* %build_for_obmc.addr, align 4, !tbaa !6
  %20 = load i32, i32* %bw.addr, align 4, !tbaa !6
  %21 = load i32, i32* %bh.addr, align 4, !tbaa !6
  %22 = load i32, i32* %mi_x.addr, align 4, !tbaa !6
  %23 = load i32, i32* %mi_y.addr, align 4, !tbaa !6
  %24 = load void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)*, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)** %calc_subpel_params_func.addr, align 4, !tbaa !2
  call void @build_inter_predictors_8x8_and_bigger(%struct.AV1Common* %15, %struct.macroblockd* %16, i32 %17, %struct.MB_MODE_INFO* %18, i32 %19, i32 %20, i32 %21, i32 %22, i32 %23, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %24)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal zeroext i1 @is_sub8x8_inter(%struct.macroblockd* %xd, i32 %plane, i8 zeroext %bsize, i32 %is_intrabc, i32 %build_for_obmc) #0 {
entry:
  %retval = alloca i1, align 1
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %bsize.addr = alloca i8, align 1
  %is_intrabc.addr = alloca i32, align 4
  %build_for_obmc.addr = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %ss_x = alloca i32, align 4
  %ss_y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %row_start = alloca i32, align 4
  %col_start = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %this_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store i32 %is_intrabc, i32* %is_intrabc.addr, align 4, !tbaa !6
  store i32 %build_for_obmc, i32* %build_for_obmc.addr, align 4, !tbaa !6
  %0 = load i32, i32* %is_intrabc.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %build_for_obmc.addr, align 4, !tbaa !6
  %tobool1 = icmp ne i32 %1, 0
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 4
  %4 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane2, i32 0, i32 %4
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %5 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %6, i32 0, i32 4
  %7 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  store i32 %7, i32* %ss_x, align 4, !tbaa !6
  %8 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %9, i32 0, i32 5
  %10 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  store i32 %10, i32* %ss_y, align 4, !tbaa !6
  %11 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom = zext i8 %11 to i32
  %arrayidx3 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %12 = load i8, i8* %arrayidx3, align 1, !tbaa !9
  %conv = zext i8 %12 to i32
  %cmp = icmp sge i32 %conv, 8
  br i1 %cmp, label %land.lhs.true, label %lor.lhs.false5

lor.lhs.false5:                                   ; preds = %if.end
  %13 = load i32, i32* %ss_x, align 4, !tbaa !6
  %tobool6 = icmp ne i32 %13, 0
  br i1 %tobool6, label %if.end15, label %land.lhs.true

land.lhs.true:                                    ; preds = %lor.lhs.false5, %if.end
  %14 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom7 = zext i8 %14 to i32
  %arrayidx8 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom7
  %15 = load i8, i8* %arrayidx8, align 1, !tbaa !9
  %conv9 = zext i8 %15 to i32
  %cmp10 = icmp sge i32 %conv9, 8
  br i1 %cmp10, label %if.then14, label %lor.lhs.false12

lor.lhs.false12:                                  ; preds = %land.lhs.true
  %16 = load i32, i32* %ss_y, align 4, !tbaa !6
  %tobool13 = icmp ne i32 %16, 0
  br i1 %tobool13, label %if.end15, label %if.then14

if.then14:                                        ; preds = %lor.lhs.false12, %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup55

if.end15:                                         ; preds = %lor.lhs.false12, %lor.lhs.false5
  %17 = bitcast i32* %row_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom16 = zext i8 %18 to i32
  %arrayidx17 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom16
  %19 = load i8, i8* %arrayidx17, align 1, !tbaa !9
  %conv18 = zext i8 %19 to i32
  %cmp19 = icmp eq i32 %conv18, 4
  br i1 %cmp19, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end15
  %20 = load i32, i32* %ss_y, align 4, !tbaa !6
  %tobool21 = icmp ne i32 %20, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end15
  %21 = phi i1 [ false, %if.end15 ], [ %tobool21, %land.rhs ]
  %22 = zext i1 %21 to i64
  %cond = select i1 %21, i32 -1, i32 0
  store i32 %cond, i32* %row_start, align 4, !tbaa !6
  %23 = bitcast i32* %col_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  %24 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom22 = zext i8 %24 to i32
  %arrayidx23 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom22
  %25 = load i8, i8* %arrayidx23, align 1, !tbaa !9
  %conv24 = zext i8 %25 to i32
  %cmp25 = icmp eq i32 %conv24, 4
  br i1 %cmp25, label %land.rhs27, label %land.end29

land.rhs27:                                       ; preds = %land.end
  %26 = load i32, i32* %ss_x, align 4, !tbaa !6
  %tobool28 = icmp ne i32 %26, 0
  br label %land.end29

land.end29:                                       ; preds = %land.rhs27, %land.end
  %27 = phi i1 [ false, %land.end ], [ %tobool28, %land.rhs27 ]
  %28 = zext i1 %27 to i64
  %cond30 = select i1 %27, i32 -1, i32 0
  store i32 %cond30, i32* %col_start, align 4, !tbaa !6
  %29 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  %30 = load i32, i32* %row_start, align 4, !tbaa !6
  store i32 %30, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc48, %land.end29
  %31 = load i32, i32* %row, align 4, !tbaa !6
  %cmp31 = icmp sle i32 %31, 0
  br i1 %cmp31, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup50

for.body:                                         ; preds = %for.cond
  %32 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = load i32, i32* %col_start, align 4, !tbaa !6
  store i32 %33, i32* %col, align 4, !tbaa !6
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc, %for.body
  %34 = load i32, i32* %col, align 4, !tbaa !6
  %cmp34 = icmp sle i32 %34, 0
  br i1 %cmp34, label %for.body37, label %for.cond.cleanup36

for.cond.cleanup36:                               ; preds = %for.cond33
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup46

for.body37:                                       ; preds = %for.cond33
  %35 = bitcast %struct.MB_MODE_INFO** %this_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  %36 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %36, i32 0, i32 6
  %37 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %38 = load i32, i32* %row, align 4, !tbaa !6
  %39 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %39, i32 0, i32 2
  %40 = load i32, i32* %mi_stride, align 8, !tbaa !79
  %mul = mul nsw i32 %38, %40
  %41 = load i32, i32* %col, align 4, !tbaa !6
  %add = add nsw i32 %mul, %41
  %arrayidx38 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %37, i32 %add
  %42 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx38, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %42, %struct.MB_MODE_INFO** %this_mbmi, align 4, !tbaa !2
  %43 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %this_mbmi, align 4, !tbaa !2
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %43)
  %tobool39 = icmp ne i32 %call, 0
  br i1 %tobool39, label %if.end41, label %if.then40

if.then40:                                        ; preds = %for.body37
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end41:                                         ; preds = %for.body37
  %44 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %this_mbmi, align 4, !tbaa !2
  %call42 = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %44)
  %tobool43 = icmp ne i32 %call42, 0
  br i1 %tobool43, label %if.then44, label %if.end45

if.then44:                                        ; preds = %if.end41
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end45:                                         ; preds = %if.end41
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end45, %if.then44, %if.then40
  %45 = bitcast %struct.MB_MODE_INFO** %this_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup46 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %46 = load i32, i32* %col, align 4, !tbaa !6
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %col, align 4, !tbaa !6
  br label %for.cond33

cleanup46:                                        ; preds = %cleanup, %for.cond.cleanup36
  %47 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  %cleanup.dest47 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest47, label %cleanup50 [
    i32 5, label %for.end
  ]

for.end:                                          ; preds = %cleanup46
  br label %for.inc48

for.inc48:                                        ; preds = %for.end
  %48 = load i32, i32* %row, align 4, !tbaa !6
  %inc49 = add nsw i32 %48, 1
  store i32 %inc49, i32* %row, align 4, !tbaa !6
  br label %for.cond

cleanup50:                                        ; preds = %cleanup46, %for.cond.cleanup
  %49 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  %cleanup.dest51 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest51, label %cleanup53 [
    i32 2, label %for.end52
  ]

for.end52:                                        ; preds = %cleanup50
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

cleanup53:                                        ; preds = %for.end52, %cleanup50
  %50 = bitcast i32* %col_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  %51 = bitcast i32* %row_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  br label %cleanup55

cleanup55:                                        ; preds = %cleanup53, %if.then14
  %52 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %54 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  br label %return

return:                                           ; preds = %cleanup55, %if.then
  %55 = load i1, i1* %retval, align 1
  ret i1 %55
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_intrabc_block(%struct.MB_MODE_INFO* %mbmi) #1 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv = zext i8 %bf.clear to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define internal void @build_inter_predictors_sub8x8(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane, %struct.MB_MODE_INFO* %mi, i32 %bw, i32 %bh, i32 %mi_x, i32 %mi_y, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %calc_subpel_params_func) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %mi_x.addr = alloca i32, align 4
  %mi_y.addr = alloca i32, align 4
  %calc_subpel_params_func.addr = alloca void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)*, align 4
  %bsize = alloca i8, align 1
  %pd = alloca %struct.macroblockd_plane*, align 4
  %ss_x = alloca i8, align 1
  %ss_y = alloca i8, align 1
  %b4_w = alloca i32, align 4
  %b4_h = alloca i32, align 4
  %plane_bsize = alloca i8, align 1
  %b8_w = alloca i32, align 4
  %b8_h = alloca i32, align 4
  %is_compound = alloca i32, align 4
  %row_start = alloca i32, align 4
  %col_start = alloca i32, align 4
  %pre_x = alloca i32, align 4
  %pre_y = alloca i32, align 4
  %row = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %col = alloca i32, align 4
  %x = alloca i32, align 4
  %this_mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %tmp_dst_stride = alloca i32, align 4
  %dst_buf = alloca %struct.buf_2d*, align 4
  %dst59 = alloca i8*, align 4
  %ref = alloca i32, align 4
  %ref_buf = alloca %struct.RefCntBuffer*, align 4
  %ref_scale_factors = alloca %struct.scale_factors*, align 4
  %sf = alloca %struct.scale_factors*, align 4
  %pre_buf = alloca %struct.buf_2d, align 4
  %mv = alloca %struct.mv, align 2
  %inter_pred_params = alloca %struct.InterPredParams, align 4
  %tmp = alloca %struct.ConvolveParams, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mi, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !6
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !6
  store i32 %mi_x, i32* %mi_x.addr, align 4, !tbaa !6
  store i32 %mi_y, i32* %mi_y.addr, align 4, !tbaa !6
  store void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %calc_subpel_params_func, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)** %calc_subpel_params_func.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #7
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 6
  %1 = load i8, i8* %sb_type, align 2, !tbaa !74
  store i8 %1, i8* %bsize, align 1, !tbaa !9
  %2 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 4
  %4 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane1, i32 0, i32 %4
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ss_x) #7
  %5 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %5, i32 0, i32 4
  %6 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  %tobool = icmp ne i32 %6, 0
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %ss_x, align 1, !tbaa !80
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ss_y) #7
  %7 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %7, i32 0, i32 5
  %8 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  %tobool2 = icmp ne i32 %8, 0
  %frombool3 = zext i1 %tobool2 to i8
  store i8 %frombool3, i8* %ss_y, align 1, !tbaa !80
  %9 = bitcast i32* %b4_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom = zext i8 %10 to i32
  %arrayidx4 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %11 = load i8, i8* %arrayidx4, align 1, !tbaa !9
  %conv = zext i8 %11 to i32
  %12 = load i8, i8* %ss_x, align 1, !tbaa !80, !range !81
  %tobool5 = trunc i8 %12 to i1
  %conv6 = zext i1 %tobool5 to i32
  %shr = ashr i32 %conv, %conv6
  store i32 %shr, i32* %b4_w, align 4, !tbaa !6
  %13 = bitcast i32* %b4_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom7 = zext i8 %14 to i32
  %arrayidx8 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom7
  %15 = load i8, i8* %arrayidx8, align 1, !tbaa !9
  %conv9 = zext i8 %15 to i32
  %16 = load i8, i8* %ss_y, align 1, !tbaa !80, !range !81
  %tobool10 = trunc i8 %16 to i1
  %conv11 = zext i1 %tobool10 to i32
  %shr12 = ashr i32 %conv9, %conv11
  store i32 %shr12, i32* %b4_h, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %plane_bsize) #7
  %17 = load i8, i8* %bsize, align 1, !tbaa !9
  %18 = load i8, i8* %ss_x, align 1, !tbaa !80, !range !81
  %tobool13 = trunc i8 %18 to i1
  %conv14 = zext i1 %tobool13 to i32
  %19 = load i8, i8* %ss_y, align 1, !tbaa !80, !range !81
  %tobool15 = trunc i8 %19 to i1
  %conv16 = zext i1 %tobool15 to i32
  %call = call zeroext i8 @get_plane_block_size(i8 zeroext %17, i32 %conv14, i32 %conv16)
  store i8 %call, i8* %plane_bsize, align 1, !tbaa !9
  %20 = bitcast i32* %b8_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %21 = load i8, i8* %plane_bsize, align 1, !tbaa !9
  %idxprom17 = zext i8 %21 to i32
  %arrayidx18 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom17
  %22 = load i8, i8* %arrayidx18, align 1, !tbaa !9
  %conv19 = zext i8 %22 to i32
  store i32 %conv19, i32* %b8_w, align 4, !tbaa !6
  %23 = bitcast i32* %b8_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  %24 = load i8, i8* %plane_bsize, align 1, !tbaa !9
  %idxprom20 = zext i8 %24 to i32
  %arrayidx21 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom20
  %25 = load i8, i8* %arrayidx21, align 1, !tbaa !9
  %conv22 = zext i8 %25 to i32
  store i32 %conv22, i32* %b8_h, align 4, !tbaa !6
  %26 = bitcast i32* %is_compound to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %call23 = call i32 @has_second_ref(%struct.MB_MODE_INFO* %27)
  store i32 %call23, i32* %is_compound, align 4, !tbaa !6
  %28 = bitcast i32* %row_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom24 = zext i8 %29 to i32
  %arrayidx25 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom24
  %30 = load i8, i8* %arrayidx25, align 1, !tbaa !9
  %conv26 = zext i8 %30 to i32
  %cmp = icmp eq i32 %conv26, 4
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %31 = load i8, i8* %ss_y, align 1, !tbaa !80, !range !81
  %tobool28 = trunc i8 %31 to i1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %32 = phi i1 [ false, %entry ], [ %tobool28, %land.rhs ]
  %33 = zext i1 %32 to i64
  %cond = select i1 %32, i32 -1, i32 0
  store i32 %cond, i32* %row_start, align 4, !tbaa !6
  %34 = bitcast i32* %col_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  %35 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom30 = zext i8 %35 to i32
  %arrayidx31 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom30
  %36 = load i8, i8* %arrayidx31, align 1, !tbaa !9
  %conv32 = zext i8 %36 to i32
  %cmp33 = icmp eq i32 %conv32, 4
  br i1 %cmp33, label %land.rhs35, label %land.end38

land.rhs35:                                       ; preds = %land.end
  %37 = load i8, i8* %ss_x, align 1, !tbaa !80, !range !81
  %tobool36 = trunc i8 %37 to i1
  br label %land.end38

land.end38:                                       ; preds = %land.rhs35, %land.end
  %38 = phi i1 [ false, %land.end ], [ %tobool36, %land.rhs35 ]
  %39 = zext i1 %38 to i64
  %cond39 = select i1 %38, i32 -1, i32 0
  store i32 %cond39, i32* %col_start, align 4, !tbaa !6
  %40 = bitcast i32* %pre_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #7
  %41 = load i32, i32* %mi_x.addr, align 4, !tbaa !6
  %42 = load i32, i32* %col_start, align 4, !tbaa !6
  %mul = mul nsw i32 4, %42
  %add = add nsw i32 %41, %mul
  %43 = load i8, i8* %ss_x, align 1, !tbaa !80, !range !81
  %tobool40 = trunc i8 %43 to i1
  %conv41 = zext i1 %tobool40 to i32
  %shr42 = ashr i32 %add, %conv41
  store i32 %shr42, i32* %pre_x, align 4, !tbaa !6
  %44 = bitcast i32* %pre_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  %45 = load i32, i32* %mi_y.addr, align 4, !tbaa !6
  %46 = load i32, i32* %row_start, align 4, !tbaa !6
  %mul43 = mul nsw i32 4, %46
  %add44 = add nsw i32 %45, %mul43
  %47 = load i8, i8* %ss_y, align 1, !tbaa !80, !range !81
  %tobool45 = trunc i8 %47 to i1
  %conv46 = zext i1 %tobool45 to i32
  %shr47 = ashr i32 %add44, %conv46
  store i32 %shr47, i32* %pre_y, align 4, !tbaa !6
  %48 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #7
  %49 = load i32, i32* %row_start, align 4, !tbaa !6
  store i32 %49, i32* %row, align 4, !tbaa !6
  %50 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc92, %land.end38
  %51 = load i32, i32* %y, align 4, !tbaa !6
  %52 = load i32, i32* %b8_h, align 4, !tbaa !6
  %cmp48 = icmp slt i32 %51, %52
  br i1 %cmp48, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %53 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  br label %for.end94

for.body:                                         ; preds = %for.cond
  %54 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  %55 = load i32, i32* %col_start, align 4, !tbaa !6
  store i32 %55, i32* %col, align 4, !tbaa !6
  %56 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #7
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond50

for.cond50:                                       ; preds = %for.inc, %for.body
  %57 = load i32, i32* %x, align 4, !tbaa !6
  %58 = load i32, i32* %b8_w, align 4, !tbaa !6
  %cmp51 = icmp slt i32 %57, %58
  br i1 %cmp51, label %for.body54, label %for.cond.cleanup53

for.cond.cleanup53:                               ; preds = %for.cond50
  store i32 5, i32* %cleanup.dest.slot, align 4
  %59 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #7
  br label %for.end

for.body54:                                       ; preds = %for.cond50
  %60 = bitcast %struct.MB_MODE_INFO** %this_mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #7
  %61 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi55 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %61, i32 0, i32 6
  %62 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi55, align 4, !tbaa !78
  %63 = load i32, i32* %row, align 4, !tbaa !6
  %64 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %64, i32 0, i32 2
  %65 = load i32, i32* %mi_stride, align 8, !tbaa !79
  %mul56 = mul nsw i32 %63, %65
  %66 = load i32, i32* %col, align 4, !tbaa !6
  %add57 = add nsw i32 %mul56, %66
  %arrayidx58 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %62, i32 %add57
  %67 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx58, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %67, %struct.MB_MODE_INFO** %this_mbmi, align 4, !tbaa !2
  %68 = bitcast i32* %tmp_dst_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #7
  store i32 8, i32* %tmp_dst_stride, align 4, !tbaa !6
  %69 = load i32, i32* %bw.addr, align 4, !tbaa !6
  %70 = load i32, i32* %bh.addr, align 4, !tbaa !6
  %71 = bitcast %struct.buf_2d** %dst_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #7
  %72 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %72, i32 0, i32 6
  store %struct.buf_2d* %dst, %struct.buf_2d** %dst_buf, align 4, !tbaa !2
  %73 = bitcast i8** %dst59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #7
  %74 = load %struct.buf_2d*, %struct.buf_2d** %dst_buf, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %74, i32 0, i32 0
  %75 = load i8*, i8** %buf, align 4, !tbaa !82
  %76 = load %struct.buf_2d*, %struct.buf_2d** %dst_buf, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %76, i32 0, i32 4
  %77 = load i32, i32* %stride, align 4, !tbaa !83
  %78 = load i32, i32* %y, align 4, !tbaa !6
  %mul60 = mul nsw i32 %77, %78
  %add.ptr = getelementptr inbounds i8, i8* %75, i32 %mul60
  %79 = load i32, i32* %x, align 4, !tbaa !6
  %add.ptr61 = getelementptr inbounds i8, i8* %add.ptr, i32 %79
  store i8* %add.ptr61, i8** %dst59, align 4, !tbaa !2
  %80 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #7
  store i32 0, i32* %ref, align 4, !tbaa !6
  %81 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #7
  %82 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %83 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %this_mbmi, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %83, i32 0, i32 12
  %84 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 %84
  %85 = load i8, i8* %arrayidx62, align 1, !tbaa !9
  %call63 = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %82, i8 signext %85)
  store %struct.RefCntBuffer* %call63, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %86 = bitcast %struct.scale_factors** %ref_scale_factors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #7
  %87 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %88 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %this_mbmi, align 4, !tbaa !2
  %ref_frame64 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %88, i32 0, i32 12
  %89 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame64, i32 0, i32 %89
  %90 = load i8, i8* %arrayidx65, align 1, !tbaa !9
  %call66 = call %struct.scale_factors* @get_ref_scale_factors_const(%struct.AV1Common* %87, i8 signext %90)
  store %struct.scale_factors* %call66, %struct.scale_factors** %ref_scale_factors, align 4, !tbaa !2
  %91 = bitcast %struct.scale_factors** %sf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #7
  %92 = load %struct.scale_factors*, %struct.scale_factors** %ref_scale_factors, align 4, !tbaa !2
  store %struct.scale_factors* %92, %struct.scale_factors** %sf, align 4, !tbaa !2
  %93 = bitcast %struct.buf_2d* %pre_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %93) #7
  %buf67 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %pre_buf, i32 0, i32 0
  store i8* null, i8** %buf67, align 4, !tbaa !82
  %buf0 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %pre_buf, i32 0, i32 1
  %94 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %cmp68 = icmp eq i32 %94, 1
  br i1 %cmp68, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body54
  %95 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %buf70 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %95, i32 0, i32 17
  %96 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf70, i32 0, i32 5
  %97 = bitcast %union.anon.8* %96 to %struct.anon.9*
  %u_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %97, i32 0, i32 1
  %98 = load i8*, i8** %u_buffer, align 4, !tbaa !9
  br label %cond.end

cond.false:                                       ; preds = %for.body54
  %99 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %buf71 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %99, i32 0, i32 17
  %100 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf71, i32 0, i32 5
  %101 = bitcast %union.anon.8* %100 to %struct.anon.9*
  %v_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %101, i32 0, i32 2
  %102 = load i8*, i8** %v_buffer, align 4, !tbaa !9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond72 = phi i8* [ %98, %cond.true ], [ %102, %cond.false ]
  store i8* %cond72, i8** %buf0, align 4, !tbaa !84
  %width = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %pre_buf, i32 0, i32 2
  %103 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %buf73 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %103, i32 0, i32 17
  %104 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf73, i32 0, i32 2
  %105 = bitcast %union.anon.2* %104 to %struct.anon.3*
  %uv_crop_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %105, i32 0, i32 1
  %106 = load i32, i32* %uv_crop_width, align 4, !tbaa !9
  store i32 %106, i32* %width, align 4, !tbaa !85
  %height = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %pre_buf, i32 0, i32 3
  %107 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %buf74 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %107, i32 0, i32 17
  %108 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf74, i32 0, i32 3
  %109 = bitcast %union.anon.4* %108 to %struct.anon.5*
  %uv_crop_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %109, i32 0, i32 1
  %110 = load i32, i32* %uv_crop_height, align 4, !tbaa !9
  store i32 %110, i32* %height, align 4, !tbaa !86
  %stride75 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %pre_buf, i32 0, i32 4
  %111 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %buf76 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %111, i32 0, i32 17
  %112 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf76, i32 0, i32 4
  %113 = bitcast %union.anon.6* %112 to %struct.anon.7*
  %uv_stride = getelementptr inbounds %struct.anon.7, %struct.anon.7* %113, i32 0, i32 1
  %114 = load i32, i32* %uv_stride, align 4, !tbaa !9
  store i32 %114, i32* %stride75, align 4, !tbaa !83
  %115 = bitcast %struct.mv* %mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %115) #7
  %116 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %this_mbmi, align 4, !tbaa !2
  %mv77 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %116, i32 0, i32 2
  %117 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx78 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv77, i32 0, i32 %117
  %as_mv = bitcast %union.int_mv* %arrayidx78 to %struct.mv*
  %118 = bitcast %struct.mv* %mv to i8*
  %119 = bitcast %struct.mv* %as_mv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %118, i8* align 4 %119, i32 4, i1 false), !tbaa.struct !87
  %120 = bitcast %struct.InterPredParams* %inter_pred_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 176, i8* %120) #7
  %121 = load i32, i32* %b4_w, align 4, !tbaa !6
  %122 = load i32, i32* %b4_h, align 4, !tbaa !6
  %123 = load i32, i32* %pre_y, align 4, !tbaa !6
  %124 = load i32, i32* %y, align 4, !tbaa !6
  %add79 = add nsw i32 %123, %124
  %125 = load i32, i32* %pre_x, align 4, !tbaa !6
  %126 = load i32, i32* %x, align 4, !tbaa !6
  %add80 = add nsw i32 %125, %126
  %127 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x81 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %127, i32 0, i32 4
  %128 = load i32, i32* %subsampling_x81, align 4, !tbaa !75
  %129 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y82 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %129, i32 0, i32 5
  %130 = load i32, i32* %subsampling_y82, align 4, !tbaa !77
  %131 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %131, i32 0, i32 41
  %132 = load i32, i32* %bd, align 4, !tbaa !88
  %133 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call83 = call i32 @is_cur_buf_hbd(%struct.macroblockd* %133)
  %134 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %134, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv84 = zext i8 %bf.clear to i32
  %135 = load %struct.scale_factors*, %struct.scale_factors** %sf, align 4, !tbaa !2
  %136 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %this_mbmi, align 4, !tbaa !2
  %interp_filters = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %136, i32 0, i32 4
  call void @av1_init_inter_params(%struct.InterPredParams* %inter_pred_params, i32 %121, i32 %122, i32 %add79, i32 %add80, i32 %128, i32 %130, i32 %132, i32 %call83, i32 %conv84, %struct.scale_factors* %135, %struct.buf_2d* %pre_buf, %union.int_interpfilters* byval(%union.int_interpfilters) align 4 %interp_filters)
  %conv_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %inter_pred_params, i32 0, i32 3
  %137 = bitcast %struct.ConvolveParams* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 44, i8* %137) #7
  %138 = load i32, i32* %ref, align 4, !tbaa !6
  %139 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %140 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tmp_conv_dst = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %140, i32 0, i32 61
  %141 = load i16*, i16** %tmp_conv_dst, align 8, !tbaa !89
  %142 = load i32, i32* %tmp_dst_stride, align 4, !tbaa !6
  %143 = load i32, i32* %is_compound, align 4, !tbaa !6
  %144 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd85 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %144, i32 0, i32 41
  %145 = load i32, i32* %bd85, align 4, !tbaa !88
  call void @get_conv_params_no_round(%struct.ConvolveParams* sret align 4 %tmp, i32 %138, i32 %139, i16* %141, i32 %142, i32 %143, i32 %145)
  %146 = bitcast %struct.ConvolveParams* %conv_params to i8*
  %147 = bitcast %struct.ConvolveParams* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %146, i8* align 4 %147, i32 44, i1 false), !tbaa.struct !90
  %148 = bitcast %struct.ConvolveParams* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 44, i8* %148) #7
  %conv_params86 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %inter_pred_params, i32 0, i32 3
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params86, i32 0, i32 8
  store i32 0, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !91
  %149 = load i8*, i8** %dst59, align 4, !tbaa !2
  %150 = load %struct.buf_2d*, %struct.buf_2d** %dst_buf, align 4, !tbaa !2
  %stride87 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %150, i32 0, i32 4
  %151 = load i32, i32* %stride87, align 4, !tbaa !83
  %152 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %153 = load i32, i32* %mi_x.addr, align 4, !tbaa !6
  %154 = load i32, i32* %x, align 4, !tbaa !6
  %add88 = add nsw i32 %153, %154
  %155 = load i32, i32* %mi_y.addr, align 4, !tbaa !6
  %156 = load i32, i32* %y, align 4, !tbaa !6
  %add89 = add nsw i32 %155, %156
  %157 = load i32, i32* %ref, align 4, !tbaa !6
  %158 = load void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)*, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)** %calc_subpel_params_func.addr, align 4, !tbaa !2
  call void @av1_build_one_inter_predictor(i8* %149, i32 %151, %struct.mv* %mv, %struct.InterPredParams* %inter_pred_params, %struct.macroblockd* %152, i32 %add88, i32 %add89, i32 %157, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %158)
  %159 = load i32, i32* %col, align 4, !tbaa !6
  %inc = add nsw i32 %159, 1
  store i32 %inc, i32* %col, align 4, !tbaa !6
  %160 = bitcast %struct.InterPredParams* %inter_pred_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 176, i8* %160) #7
  %161 = bitcast %struct.mv* %mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #7
  %162 = bitcast %struct.buf_2d* %pre_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %162) #7
  %163 = bitcast %struct.scale_factors** %sf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #7
  %164 = bitcast %struct.scale_factors** %ref_scale_factors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #7
  %165 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #7
  %166 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #7
  %167 = bitcast i8** %dst59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #7
  %168 = bitcast %struct.buf_2d** %dst_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #7
  %169 = bitcast i32* %tmp_dst_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #7
  %170 = bitcast %struct.MB_MODE_INFO** %this_mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #7
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %171 = load i32, i32* %b4_w, align 4, !tbaa !6
  %172 = load i32, i32* %x, align 4, !tbaa !6
  %add90 = add nsw i32 %172, %171
  store i32 %add90, i32* %x, align 4, !tbaa !6
  br label %for.cond50

for.end:                                          ; preds = %for.cond.cleanup53
  %173 = load i32, i32* %row, align 4, !tbaa !6
  %inc91 = add nsw i32 %173, 1
  store i32 %inc91, i32* %row, align 4, !tbaa !6
  %174 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #7
  br label %for.inc92

for.inc92:                                        ; preds = %for.end
  %175 = load i32, i32* %b4_h, align 4, !tbaa !6
  %176 = load i32, i32* %y, align 4, !tbaa !6
  %add93 = add nsw i32 %176, %175
  store i32 %add93, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end94:                                        ; preds = %for.cond.cleanup
  %177 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #7
  %178 = bitcast i32* %pre_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #7
  %179 = bitcast i32* %pre_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #7
  %180 = bitcast i32* %col_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #7
  %181 = bitcast i32* %row_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #7
  %182 = bitcast i32* %is_compound to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #7
  %183 = bitcast i32* %b8_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #7
  %184 = bitcast i32* %b8_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %plane_bsize) #7
  %185 = bitcast i32* %b4_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #7
  %186 = bitcast i32* %b4_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ss_y) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ss_x) #7
  %187 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #7
  ret void
}

; Function Attrs: nounwind
define internal void @build_inter_predictors_8x8_and_bigger(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %plane, %struct.MB_MODE_INFO* %mi, i32 %build_for_obmc, i32 %bw, i32 %bh, i32 %mi_x, i32 %mi_y, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %calc_subpel_params_func) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %plane.addr = alloca i32, align 4
  %mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %build_for_obmc.addr = alloca i32, align 4
  %bw.addr = alloca i32, align 4
  %bh.addr = alloca i32, align 4
  %mi_x.addr = alloca i32, align 4
  %mi_y.addr = alloca i32, align 4
  %calc_subpel_params_func.addr = alloca void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)*, align 4
  %is_compound = alloca i32, align 4
  %is_intrabc = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %dst_buf = alloca %struct.buf_2d*, align 4
  %dst3 = alloca i8*, align 4
  %is_global = alloca [2 x i32], align 4
  %ref = alloca i32, align 4
  %wm = alloca %struct.WarpedMotionParams*, align 4
  %bsize = alloca i8, align 1
  %ss_x = alloca i32, align 4
  %ss_y = alloca i32, align 4
  %row_start = alloca i32, align 4
  %col_start = alloca i32, align 4
  %pre_x = alloca i32, align 4
  %pre_y = alloca i32, align 4
  %ref29 = alloca i32, align 4
  %sf = alloca %struct.scale_factors*, align 4
  %pre_buf = alloca %struct.buf_2d*, align 4
  %mv = alloca %struct.mv, align 2
  %warp_types = alloca %struct.WarpTypesAllowed, align 4
  %inter_pred_params = alloca %struct.InterPredParams, align 4
  %tmp = alloca %struct.ConvolveParams, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mi, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  store i32 %build_for_obmc, i32* %build_for_obmc.addr, align 4, !tbaa !6
  store i32 %bw, i32* %bw.addr, align 4, !tbaa !6
  store i32 %bh, i32* %bh.addr, align 4, !tbaa !6
  store i32 %mi_x, i32* %mi_x.addr, align 4, !tbaa !6
  store i32 %mi_y, i32* %mi_y.addr, align 4, !tbaa !6
  store void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %calc_subpel_params_func, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)** %calc_subpel_params_func.addr, align 4, !tbaa !2
  %0 = bitcast i32* %is_compound to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %call = call i32 @has_second_ref(%struct.MB_MODE_INFO* %1)
  store i32 %call, i32* %is_compound, align 4, !tbaa !6
  %2 = bitcast i32* %is_intrabc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %call1 = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %3)
  store i32 %call1, i32* %is_intrabc, align 4, !tbaa !6
  %4 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 4
  %6 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane2, i32 0, i32 %6
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %7 = bitcast %struct.buf_2d** %dst_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %8, i32 0, i32 6
  store %struct.buf_2d* %dst, %struct.buf_2d** %dst_buf, align 4, !tbaa !2
  %9 = bitcast i8** %dst3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load %struct.buf_2d*, %struct.buf_2d** %dst_buf, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %10, i32 0, i32 0
  %11 = load i8*, i8** %buf, align 4, !tbaa !82
  store i8* %11, i8** %dst3, align 4, !tbaa !2
  %12 = bitcast [2 x i32]* %is_global to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %12) #7
  %13 = bitcast [2 x i32]* %is_global to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %13, i8 0, i32 8, i1 false)
  %14 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  store i32 0, i32* %ref, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %ref, align 4, !tbaa !6
  %16 = load i32, i32* %is_compound, align 4, !tbaa !6
  %add = add nsw i32 1, %16
  %cmp = icmp slt i32 %15, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %17 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %18 = bitcast %struct.WarpedMotionParams** %wm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  %19 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %global_motion = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %19, i32 0, i32 47
  %20 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %global_motion, align 4, !tbaa !47
  %21 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %21, i32 0, i32 12
  %22 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 %22
  %23 = load i8, i8* %arrayidx4, align 1, !tbaa !9
  %idxprom = sext i8 %23 to i32
  %arrayidx5 = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %20, i32 %idxprom
  store %struct.WarpedMotionParams* %arrayidx5, %struct.WarpedMotionParams** %wm, align 4, !tbaa !2
  %24 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %25 = load %struct.WarpedMotionParams*, %struct.WarpedMotionParams** %wm, align 4, !tbaa !2
  %wmtype = getelementptr inbounds %struct.WarpedMotionParams, %struct.WarpedMotionParams* %25, i32 0, i32 5
  %26 = load i8, i8* %wmtype, align 4, !tbaa !92
  %call6 = call i32 @is_global_mv_block(%struct.MB_MODE_INFO* %24, i8 zeroext %26)
  %27 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [2 x i32], [2 x i32]* %is_global, i32 0, i32 %27
  store i32 %call6, i32* %arrayidx7, align 4, !tbaa !6
  %28 = bitcast %struct.WarpedMotionParams** %wm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %ref, align 4, !tbaa !6
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %ref, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #7
  %30 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %30, i32 0, i32 6
  %31 = load i8, i8* %sb_type, align 2, !tbaa !74
  store i8 %31, i8* %bsize, align 1, !tbaa !9
  %32 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %33, i32 0, i32 4
  %34 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  store i32 %34, i32* %ss_x, align 4, !tbaa !6
  %35 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  %36 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %36, i32 0, i32 5
  %37 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  store i32 %37, i32* %ss_y, align 4, !tbaa !6
  %38 = bitcast i32* %row_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  %39 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom8 = zext i8 %39 to i32
  %arrayidx9 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom8
  %40 = load i8, i8* %arrayidx9, align 1, !tbaa !9
  %conv = zext i8 %40 to i32
  %cmp10 = icmp eq i32 %conv, 4
  br i1 %cmp10, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %for.end
  %41 = load i32, i32* %ss_y, align 4, !tbaa !6
  %tobool = icmp ne i32 %41, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %42 = load i32, i32* %build_for_obmc.addr, align 4, !tbaa !6
  %tobool12 = icmp ne i32 %42, 0
  %lnot = xor i1 %tobool12, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %for.end
  %43 = phi i1 [ false, %land.lhs.true ], [ false, %for.end ], [ %lnot, %land.rhs ]
  %44 = zext i1 %43 to i64
  %cond = select i1 %43, i32 -1, i32 0
  store i32 %cond, i32* %row_start, align 4, !tbaa !6
  %45 = bitcast i32* %col_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #7
  %46 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom13 = zext i8 %46 to i32
  %arrayidx14 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom13
  %47 = load i8, i8* %arrayidx14, align 1, !tbaa !9
  %conv15 = zext i8 %47 to i32
  %cmp16 = icmp eq i32 %conv15, 4
  br i1 %cmp16, label %land.lhs.true18, label %land.end23

land.lhs.true18:                                  ; preds = %land.end
  %48 = load i32, i32* %ss_x, align 4, !tbaa !6
  %tobool19 = icmp ne i32 %48, 0
  br i1 %tobool19, label %land.rhs20, label %land.end23

land.rhs20:                                       ; preds = %land.lhs.true18
  %49 = load i32, i32* %build_for_obmc.addr, align 4, !tbaa !6
  %tobool21 = icmp ne i32 %49, 0
  %lnot22 = xor i1 %tobool21, true
  br label %land.end23

land.end23:                                       ; preds = %land.rhs20, %land.lhs.true18, %land.end
  %50 = phi i1 [ false, %land.lhs.true18 ], [ false, %land.end ], [ %lnot22, %land.rhs20 ]
  %51 = zext i1 %50 to i64
  %cond24 = select i1 %50, i32 -1, i32 0
  store i32 %cond24, i32* %col_start, align 4, !tbaa !6
  %52 = bitcast i32* %pre_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #7
  %53 = load i32, i32* %mi_x.addr, align 4, !tbaa !6
  %54 = load i32, i32* %col_start, align 4, !tbaa !6
  %mul = mul nsw i32 4, %54
  %add25 = add nsw i32 %53, %mul
  %55 = load i32, i32* %ss_x, align 4, !tbaa !6
  %shr = ashr i32 %add25, %55
  store i32 %shr, i32* %pre_x, align 4, !tbaa !6
  %56 = bitcast i32* %pre_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #7
  %57 = load i32, i32* %mi_y.addr, align 4, !tbaa !6
  %58 = load i32, i32* %row_start, align 4, !tbaa !6
  %mul26 = mul nsw i32 4, %58
  %add27 = add nsw i32 %57, %mul26
  %59 = load i32, i32* %ss_y, align 4, !tbaa !6
  %shr28 = ashr i32 %add27, %59
  store i32 %shr28, i32* %pre_y, align 4, !tbaa !6
  %60 = bitcast i32* %ref29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #7
  store i32 0, i32* %ref29, align 4, !tbaa !6
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc70, %land.end23
  %61 = load i32, i32* %ref29, align 4, !tbaa !6
  %62 = load i32, i32* %is_compound, align 4, !tbaa !6
  %add31 = add nsw i32 1, %62
  %cmp32 = icmp slt i32 %61, %add31
  br i1 %cmp32, label %for.body35, label %for.cond.cleanup34

for.cond.cleanup34:                               ; preds = %for.cond30
  %63 = bitcast i32* %ref29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  br label %for.end72

for.body35:                                       ; preds = %for.cond30
  %64 = bitcast %struct.scale_factors** %sf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #7
  %65 = load i32, i32* %is_intrabc, align 4, !tbaa !6
  %tobool36 = icmp ne i32 %65, 0
  br i1 %tobool36, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body35
  %66 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %sf_identity = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %66, i32 0, i32 15
  br label %cond.end

cond.false:                                       ; preds = %for.body35
  %67 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %block_ref_scale_factors = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %67, i32 0, i32 21
  %68 = load i32, i32* %ref29, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [2 x %struct.scale_factors*], [2 x %struct.scale_factors*]* %block_ref_scale_factors, i32 0, i32 %68
  %69 = load %struct.scale_factors*, %struct.scale_factors** %arrayidx37, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond38 = phi %struct.scale_factors* [ %sf_identity, %cond.true ], [ %69, %cond.false ]
  store %struct.scale_factors* %cond38, %struct.scale_factors** %sf, align 4, !tbaa !2
  %70 = bitcast %struct.buf_2d** %pre_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #7
  %71 = load i32, i32* %is_intrabc, align 4, !tbaa !6
  %tobool39 = icmp ne i32 %71, 0
  br i1 %tobool39, label %cond.true40, label %cond.false41

cond.true40:                                      ; preds = %cond.end
  %72 = load %struct.buf_2d*, %struct.buf_2d** %dst_buf, align 4, !tbaa !2
  br label %cond.end43

cond.false41:                                     ; preds = %cond.end
  %73 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %pre = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %73, i32 0, i32 7
  %74 = load i32, i32* %ref29, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds [2 x %struct.buf_2d], [2 x %struct.buf_2d]* %pre, i32 0, i32 %74
  br label %cond.end43

cond.end43:                                       ; preds = %cond.false41, %cond.true40
  %cond44 = phi %struct.buf_2d* [ %72, %cond.true40 ], [ %arrayidx42, %cond.false41 ]
  store %struct.buf_2d* %cond44, %struct.buf_2d** %pre_buf, align 4, !tbaa !2
  %75 = bitcast %struct.mv* %mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #7
  %76 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %mv45 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %76, i32 0, i32 2
  %77 = load i32, i32* %ref29, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds [2 x %union.int_mv], [2 x %union.int_mv]* %mv45, i32 0, i32 %77
  %as_mv = bitcast %union.int_mv* %arrayidx46 to %struct.mv*
  %78 = bitcast %struct.mv* %mv to i8*
  %79 = bitcast %struct.mv* %as_mv to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %78, i8* align 4 %79, i32 4, i1 false), !tbaa.struct !87
  %80 = bitcast %struct.WarpTypesAllowed* %warp_types to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %80) #7
  %global_warp_allowed = getelementptr inbounds %struct.WarpTypesAllowed, %struct.WarpTypesAllowed* %warp_types, i32 0, i32 0
  %81 = load i32, i32* %ref29, align 4, !tbaa !6
  %arrayidx47 = getelementptr inbounds [2 x i32], [2 x i32]* %is_global, i32 0, i32 %81
  %82 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  store i32 %82, i32* %global_warp_allowed, align 4, !tbaa !20
  %local_warp_allowed = getelementptr inbounds %struct.WarpTypesAllowed, %struct.WarpTypesAllowed* %warp_types, i32 0, i32 1
  %83 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %motion_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %83, i32 0, i32 10
  %84 = load i8, i8* %motion_mode, align 2, !tbaa !93
  %conv48 = zext i8 %84 to i32
  %cmp49 = icmp eq i32 %conv48, 2
  %conv50 = zext i1 %cmp49 to i32
  store i32 %conv50, i32* %local_warp_allowed, align 4, !tbaa !12
  %85 = bitcast %struct.InterPredParams* %inter_pred_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 176, i8* %85) #7
  %86 = load i32, i32* %bw.addr, align 4, !tbaa !6
  %87 = load i32, i32* %bh.addr, align 4, !tbaa !6
  %88 = load i32, i32* %pre_y, align 4, !tbaa !6
  %89 = load i32, i32* %pre_x, align 4, !tbaa !6
  %90 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x51 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %90, i32 0, i32 4
  %91 = load i32, i32* %subsampling_x51, align 4, !tbaa !75
  %92 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y52 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %92, i32 0, i32 5
  %93 = load i32, i32* %subsampling_y52, align 4, !tbaa !77
  %94 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %94, i32 0, i32 41
  %95 = load i32, i32* %bd, align 4, !tbaa !88
  %96 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call53 = call i32 @is_cur_buf_hbd(%struct.macroblockd* %96)
  %97 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %use_intrabc = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %97, i32 0, i32 26
  %bf.load = load i8, i8* %use_intrabc, align 4
  %bf.lshr = lshr i8 %bf.load, 6
  %bf.clear = and i8 %bf.lshr, 1
  %conv54 = zext i8 %bf.clear to i32
  %98 = load %struct.scale_factors*, %struct.scale_factors** %sf, align 4, !tbaa !2
  %99 = load %struct.buf_2d*, %struct.buf_2d** %pre_buf, align 4, !tbaa !2
  %100 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %interp_filters = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %100, i32 0, i32 4
  call void @av1_init_inter_params(%struct.InterPredParams* %inter_pred_params, i32 %86, i32 %87, i32 %88, i32 %89, i32 %91, i32 %93, i32 %95, i32 %call53, i32 %conv54, %struct.scale_factors* %98, %struct.buf_2d* %99, %union.int_interpfilters* byval(%union.int_interpfilters) align 4 %interp_filters)
  %101 = load i32, i32* %is_compound, align 4, !tbaa !6
  %tobool55 = icmp ne i32 %101, 0
  br i1 %tobool55, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end43
  call void @av1_init_comp_mode(%struct.InterPredParams* %inter_pred_params)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end43
  %conv_params = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %inter_pred_params, i32 0, i32 3
  %102 = bitcast %struct.ConvolveParams* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 44, i8* %102) #7
  %103 = load i32, i32* %ref29, align 4, !tbaa !6
  %104 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %105 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %tmp_conv_dst = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %105, i32 0, i32 61
  %106 = load i16*, i16** %tmp_conv_dst, align 8, !tbaa !89
  %107 = load i32, i32* %is_compound, align 4, !tbaa !6
  %108 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd56 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %108, i32 0, i32 41
  %109 = load i32, i32* %bd56, align 4, !tbaa !88
  call void @get_conv_params_no_round(%struct.ConvolveParams* sret align 4 %tmp, i32 %103, i32 %104, i16* %106, i32 128, i32 %107, i32 %109)
  %110 = bitcast %struct.ConvolveParams* %conv_params to i8*
  %111 = bitcast %struct.ConvolveParams* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %110, i8* align 4 %111, i32 44, i1 false), !tbaa.struct !90
  %112 = bitcast %struct.ConvolveParams* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 44, i8* %112) #7
  %113 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %114 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %conv_params57 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %inter_pred_params, i32 0, i32 3
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params57, i32 0, i32 9
  %conv_params58 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %inter_pred_params, i32 0, i32 3
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params58, i32 0, i32 10
  %conv_params59 = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %inter_pred_params, i32 0, i32 3
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %conv_params59, i32 0, i32 8
  %115 = load i32, i32* %is_compound, align 4, !tbaa !6
  call void @av1_dist_wtd_comp_weight_assign(%struct.AV1Common* %113, %struct.MB_MODE_INFO* %114, i32 0, i32* %fwd_offset, i32* %bck_offset, i32* %use_dist_wtd_comp_avg, i32 %115)
  %116 = load i32, i32* %build_for_obmc.addr, align 4, !tbaa !6
  %tobool60 = icmp ne i32 %116, 0
  br i1 %tobool60, label %if.end62, label %if.then61

if.then61:                                        ; preds = %if.end
  %117 = load i32, i32* %ref29, align 4, !tbaa !6
  %118 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %119 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  call void @av1_init_warp_params(%struct.InterPredParams* %inter_pred_params, %struct.WarpTypesAllowed* %warp_types, i32 %117, %struct.macroblockd* %118, %struct.MB_MODE_INFO* %119)
  br label %if.end62

if.end62:                                         ; preds = %if.then61, %if.end
  %120 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %interinter_comp = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %120, i32 0, i32 0
  %type = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp, i32 0, i32 4
  %121 = load i8, i8* %type, align 1, !tbaa !94
  %call63 = call i32 @is_masked_compound_type(i8 zeroext %121)
  %tobool64 = icmp ne i32 %call63, 0
  br i1 %tobool64, label %if.then65, label %if.end69

if.then65:                                        ; preds = %if.end62
  %122 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %sb_type66 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %122, i32 0, i32 6
  %123 = load i8, i8* %sb_type66, align 2, !tbaa !74
  %124 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %interinter_comp67 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %124, i32 0, i32 0
  call void @av1_init_mask_comp(%struct.InterPredParams* %inter_pred_params, i8 zeroext %123, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp67)
  %125 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %seg_mask = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %125, i32 0, i32 54
  %arraydecay = getelementptr inbounds [32768 x i8], [32768 x i8]* %seg_mask, i32 0, i32 0
  %mask_comp = getelementptr inbounds %struct.InterPredParams, %struct.InterPredParams* %inter_pred_params, i32 0, i32 15
  %seg_mask68 = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %mask_comp, i32 0, i32 0
  store i8* %arraydecay, i8** %seg_mask68, align 4, !tbaa !95
  br label %if.end69

if.end69:                                         ; preds = %if.then65, %if.end62
  %126 = load i8*, i8** %dst3, align 4, !tbaa !2
  %127 = load %struct.buf_2d*, %struct.buf_2d** %dst_buf, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %127, i32 0, i32 4
  %128 = load i32, i32* %stride, align 4, !tbaa !83
  %129 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %130 = load i32, i32* %mi_x.addr, align 4, !tbaa !6
  %131 = load i32, i32* %mi_y.addr, align 4, !tbaa !6
  %132 = load i32, i32* %ref29, align 4, !tbaa !6
  %133 = load void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)*, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)** %calc_subpel_params_func.addr, align 4, !tbaa !2
  call void @av1_build_one_inter_predictor(i8* %126, i32 %128, %struct.mv* %mv, %struct.InterPredParams* %inter_pred_params, %struct.macroblockd* %129, i32 %130, i32 %131, i32 %132, void (%struct.mv*, %struct.InterPredParams*, %struct.macroblockd*, i32, i32, i32, i8**, %struct.SubpelParams*, i32*)* %133)
  %134 = bitcast %struct.InterPredParams* %inter_pred_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 176, i8* %134) #7
  %135 = bitcast %struct.WarpTypesAllowed* %warp_types to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %135) #7
  %136 = bitcast %struct.mv* %mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #7
  %137 = bitcast %struct.buf_2d** %pre_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #7
  %138 = bitcast %struct.scale_factors** %sf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #7
  br label %for.inc70

for.inc70:                                        ; preds = %if.end69
  %139 = load i32, i32* %ref29, align 4, !tbaa !6
  %inc71 = add nsw i32 %139, 1
  store i32 %inc71, i32* %ref29, align 4, !tbaa !6
  br label %for.cond30

for.end72:                                        ; preds = %for.cond.cleanup34
  %140 = bitcast i32* %pre_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #7
  %141 = bitcast i32* %pre_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #7
  %142 = bitcast i32* %col_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #7
  %143 = bitcast i32* %row_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #7
  %144 = bitcast i32* %ss_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #7
  %145 = bitcast i32* %ss_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #7
  %146 = bitcast [2 x i32]* %is_global to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %146) #7
  %147 = bitcast i8** %dst3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #7
  %148 = bitcast %struct.buf_2d** %dst_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #7
  %149 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #7
  %150 = bitcast i32* %is_intrabc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #7
  %151 = bitcast i32* %is_compound to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_dist_wtd_comp_weight_assign(%struct.AV1Common* %cm, %struct.MB_MODE_INFO* %mbmi, i32 %order_idx, i32* %fwd_offset, i32* %bck_offset, i32* %use_dist_wtd_comp_avg, i32 %is_compound) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %order_idx.addr = alloca i32, align 4
  %fwd_offset.addr = alloca i32*, align 4
  %bck_offset.addr = alloca i32*, align 4
  %use_dist_wtd_comp_avg.addr = alloca i32*, align 4
  %is_compound.addr = alloca i32, align 4
  %bck_buf = alloca %struct.RefCntBuffer*, align 4
  %fwd_buf = alloca %struct.RefCntBuffer*, align 4
  %cur_frame_index = alloca i32, align 4
  %bck_frame_index = alloca i32, align 4
  %fwd_frame_index = alloca i32, align 4
  %d0 = alloca i32, align 4
  %d1 = alloca i32, align 4
  %order = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %c0 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %d0_c0 = alloca i32, align 4
  %d1_c1 = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  store i32 %order_idx, i32* %order_idx.addr, align 4, !tbaa !6
  store i32* %fwd_offset, i32** %fwd_offset.addr, align 4, !tbaa !2
  store i32* %bck_offset, i32** %bck_offset.addr, align 4, !tbaa !2
  store i32* %use_dist_wtd_comp_avg, i32** %use_dist_wtd_comp_avg.addr, align 4, !tbaa !2
  store i32 %is_compound, i32* %is_compound.addr, align 4, !tbaa !6
  %0 = load i32, i32* %is_compound.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %compound_idx = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 25
  %2 = load i8, i8* %compound_idx, align 1, !tbaa !96
  %conv = zext i8 %2 to i32
  %tobool1 = icmp ne i32 %conv, 0
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %3 = load i32*, i32** %use_dist_wtd_comp_avg.addr, align 4, !tbaa !2
  store i32 0, i32* %3, align 4, !tbaa !6
  br label %cleanup.cont76

if.end:                                           ; preds = %lor.lhs.false
  %4 = load i32*, i32** %use_dist_wtd_comp_avg.addr, align 4, !tbaa !2
  store i32 1, i32* %4, align 4, !tbaa !6
  %5 = bitcast %struct.RefCntBuffer** %bck_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %7 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %7, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %8 = load i8, i8* %arrayidx, align 4, !tbaa !9
  %call = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %6, i8 signext %8)
  store %struct.RefCntBuffer* %call, %struct.RefCntBuffer** %bck_buf, align 4, !tbaa !2
  %9 = bitcast %struct.RefCntBuffer** %fwd_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %11 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %11, i32 0, i32 12
  %arrayidx3 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame2, i32 0, i32 1
  %12 = load i8, i8* %arrayidx3, align 1, !tbaa !9
  %call4 = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %10, i8 signext %12)
  store %struct.RefCntBuffer* %call4, %struct.RefCntBuffer** %fwd_buf, align 4, !tbaa !2
  %13 = bitcast i32* %cur_frame_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %14, i32 0, i32 13
  %15 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !97
  %order_hint = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %15, i32 0, i32 1
  %16 = load i32, i32* %order_hint, align 4, !tbaa !118
  store i32 %16, i32* %cur_frame_index, align 4, !tbaa !6
  %17 = bitcast i32* %bck_frame_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  store i32 0, i32* %bck_frame_index, align 4, !tbaa !6
  %18 = bitcast i32* %fwd_frame_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  store i32 0, i32* %fwd_frame_index, align 4, !tbaa !6
  %19 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %bck_buf, align 4, !tbaa !2
  %cmp = icmp ne %struct.RefCntBuffer* %19, null
  br i1 %cmp, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.end
  %20 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %bck_buf, align 4, !tbaa !2
  %order_hint7 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %20, i32 0, i32 1
  %21 = load i32, i32* %order_hint7, align 4, !tbaa !118
  store i32 %21, i32* %bck_frame_index, align 4, !tbaa !6
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %if.end
  %22 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %fwd_buf, align 4, !tbaa !2
  %cmp9 = icmp ne %struct.RefCntBuffer* %22, null
  br i1 %cmp9, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end8
  %23 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %fwd_buf, align 4, !tbaa !2
  %order_hint12 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %23, i32 0, i32 1
  %24 = load i32, i32* %order_hint12, align 4, !tbaa !118
  store i32 %24, i32* %fwd_frame_index, align 4, !tbaa !6
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %if.end8
  %25 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %26, i32 0, i32 37
  %order_hint_info = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 10
  %27 = load i32, i32* %fwd_frame_index, align 4, !tbaa !6
  %28 = load i32, i32* %cur_frame_index, align 4, !tbaa !6
  %call14 = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info, i32 %27, i32 %28)
  %call15 = call i32 @abs(i32 %call14) #8
  %call16 = call i32 @clamp(i32 %call15, i32 0, i32 31)
  store i32 %call16, i32* %d0, align 4, !tbaa !6
  %29 = bitcast i32* %d1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  %30 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params17 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %30, i32 0, i32 37
  %order_hint_info18 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params17, i32 0, i32 10
  %31 = load i32, i32* %cur_frame_index, align 4, !tbaa !6
  %32 = load i32, i32* %bck_frame_index, align 4, !tbaa !6
  %call19 = call i32 @get_relative_dist(%struct.OrderHintInfo* %order_hint_info18, i32 %31, i32 %32)
  %call20 = call i32 @abs(i32 %call19) #8
  %call21 = call i32 @clamp(i32 %call20, i32 0, i32 31)
  store i32 %call21, i32* %d1, align 4, !tbaa !6
  %33 = bitcast i32* %order to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %34 = load i32, i32* %d0, align 4, !tbaa !6
  %35 = load i32, i32* %d1, align 4, !tbaa !6
  %cmp22 = icmp sle i32 %34, %35
  %conv23 = zext i1 %cmp22 to i32
  store i32 %conv23, i32* %order, align 4, !tbaa !6
  %36 = load i32, i32* %d0, align 4, !tbaa !6
  %cmp24 = icmp eq i32 %36, 0
  br i1 %cmp24, label %if.then29, label %lor.lhs.false26

lor.lhs.false26:                                  ; preds = %if.end13
  %37 = load i32, i32* %d1, align 4, !tbaa !6
  %cmp27 = icmp eq i32 %37, 0
  br i1 %cmp27, label %if.then29, label %if.end36

if.then29:                                        ; preds = %lor.lhs.false26, %if.end13
  %38 = load i32, i32* %order_idx.addr, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds [2 x [4 x [2 x i32]]], [2 x [4 x [2 x i32]]]* @quant_dist_lookup_table, i32 0, i32 %38
  %arrayidx31 = getelementptr inbounds [4 x [2 x i32]], [4 x [2 x i32]]* %arrayidx30, i32 0, i32 3
  %39 = load i32, i32* %order, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx31, i32 0, i32 %39
  %40 = load i32, i32* %arrayidx32, align 4, !tbaa !6
  %41 = load i32*, i32** %fwd_offset.addr, align 4, !tbaa !2
  store i32 %40, i32* %41, align 4, !tbaa !6
  %42 = load i32, i32* %order_idx.addr, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [2 x [4 x [2 x i32]]], [2 x [4 x [2 x i32]]]* @quant_dist_lookup_table, i32 0, i32 %42
  %arrayidx34 = getelementptr inbounds [4 x [2 x i32]], [4 x [2 x i32]]* %arrayidx33, i32 0, i32 3
  %43 = load i32, i32* %order, align 4, !tbaa !6
  %sub = sub nsw i32 1, %43
  %arrayidx35 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx34, i32 0, i32 %sub
  %44 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %45 = load i32*, i32** %bck_offset.addr, align 4, !tbaa !2
  store i32 %44, i32* %45, align 4, !tbaa !6
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup67

if.end36:                                         ; preds = %lor.lhs.false26
  %46 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end36
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %cmp37 = icmp slt i32 %47, 3
  br i1 %cmp37, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %48 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #7
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds [4 x [2 x i32]], [4 x [2 x i32]]* @quant_dist_weight, i32 0, i32 %49
  %50 = load i32, i32* %order, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx39, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  store i32 %51, i32* %c0, align 4, !tbaa !6
  %52 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #7
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds [4 x [2 x i32]], [4 x [2 x i32]]* @quant_dist_weight, i32 0, i32 %53
  %54 = load i32, i32* %order, align 4, !tbaa !6
  %tobool42 = icmp ne i32 %54, 0
  %lnot = xor i1 %tobool42, true
  %lnot.ext = zext i1 %lnot to i32
  %arrayidx43 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx41, i32 0, i32 %lnot.ext
  %55 = load i32, i32* %arrayidx43, align 4, !tbaa !6
  store i32 %55, i32* %c1, align 4, !tbaa !6
  %56 = bitcast i32* %d0_c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #7
  %57 = load i32, i32* %d0, align 4, !tbaa !6
  %58 = load i32, i32* %c0, align 4, !tbaa !6
  %mul = mul nsw i32 %57, %58
  store i32 %mul, i32* %d0_c0, align 4, !tbaa !6
  %59 = bitcast i32* %d1_c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #7
  %60 = load i32, i32* %d1, align 4, !tbaa !6
  %61 = load i32, i32* %c1, align 4, !tbaa !6
  %mul44 = mul nsw i32 %60, %61
  store i32 %mul44, i32* %d1_c1, align 4, !tbaa !6
  %62 = load i32, i32* %d0, align 4, !tbaa !6
  %63 = load i32, i32* %d1, align 4, !tbaa !6
  %cmp45 = icmp sgt i32 %62, %63
  br i1 %cmp45, label %land.lhs.true, label %lor.lhs.false49

land.lhs.true:                                    ; preds = %for.body
  %64 = load i32, i32* %d0_c0, align 4, !tbaa !6
  %65 = load i32, i32* %d1_c1, align 4, !tbaa !6
  %cmp47 = icmp slt i32 %64, %65
  br i1 %cmp47, label %if.then55, label %lor.lhs.false49

lor.lhs.false49:                                  ; preds = %land.lhs.true, %for.body
  %66 = load i32, i32* %d0, align 4, !tbaa !6
  %67 = load i32, i32* %d1, align 4, !tbaa !6
  %cmp50 = icmp sle i32 %66, %67
  br i1 %cmp50, label %land.lhs.true52, label %if.end56

land.lhs.true52:                                  ; preds = %lor.lhs.false49
  %68 = load i32, i32* %d0_c0, align 4, !tbaa !6
  %69 = load i32, i32* %d1_c1, align 4, !tbaa !6
  %cmp53 = icmp sgt i32 %68, %69
  br i1 %cmp53, label %if.then55, label %if.end56

if.then55:                                        ; preds = %land.lhs.true52, %land.lhs.true
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end56:                                         ; preds = %land.lhs.true52, %lor.lhs.false49
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end56, %if.then55
  %70 = bitcast i32* %d1_c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  %71 = bitcast i32* %d0_c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  %72 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  %73 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %74 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %74, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %cleanup, %for.cond
  %75 = load i32, i32* %order_idx.addr, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [2 x [4 x [2 x i32]]], [2 x [4 x [2 x i32]]]* @quant_dist_lookup_table, i32 0, i32 %75
  %76 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds [4 x [2 x i32]], [4 x [2 x i32]]* %arrayidx60, i32 0, i32 %76
  %77 = load i32, i32* %order, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx61, i32 0, i32 %77
  %78 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  %79 = load i32*, i32** %fwd_offset.addr, align 4, !tbaa !2
  store i32 %78, i32* %79, align 4, !tbaa !6
  %80 = load i32, i32* %order_idx.addr, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds [2 x [4 x [2 x i32]]], [2 x [4 x [2 x i32]]]* @quant_dist_lookup_table, i32 0, i32 %80
  %81 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds [4 x [2 x i32]], [4 x [2 x i32]]* %arrayidx63, i32 0, i32 %81
  %82 = load i32, i32* %order, align 4, !tbaa !6
  %sub65 = sub nsw i32 1, %82
  %arrayidx66 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx64, i32 0, i32 %sub65
  %83 = load i32, i32* %arrayidx66, align 4, !tbaa !6
  %84 = load i32*, i32** %bck_offset.addr, align 4, !tbaa !2
  store i32 %83, i32* %84, align 4, !tbaa !6
  %85 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup67

cleanup67:                                        ; preds = %for.end, %if.then29
  %86 = bitcast i32* %order to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #7
  %87 = bitcast i32* %d1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #7
  %88 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #7
  %89 = bitcast i32* %fwd_frame_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #7
  %90 = bitcast i32* %bck_frame_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #7
  %91 = bitcast i32* %cur_frame_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #7
  %92 = bitcast %struct.RefCntBuffer** %fwd_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #7
  %93 = bitcast %struct.RefCntBuffer** %bck_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #7
  %cleanup.dest75 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest75, label %unreachable [
    i32 0, label %cleanup.cont76
    i32 1, label %cleanup.cont76
  ]

cleanup.cont76:                                   ; preds = %if.then, %cleanup67, %cleanup67
  ret void

unreachable:                                      ; preds = %cleanup67, %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %cm, i8 signext %ref_frame) #1 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  %map_idx = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !9
  %0 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %2 = load i8, i8* %ref_frame.addr, align 1, !tbaa !9
  %call = call i32 @get_ref_frame_map_idx(%struct.AV1Common* %1, i8 signext %2)
  store i32 %call, i32* %map_idx, align 4, !tbaa !6
  %3 = load i32, i32* %map_idx, align 4, !tbaa !6
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 17
  %5 = load i32, i32* %map_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %5
  %6 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.RefCntBuffer* [ %6, %cond.true ], [ null, %cond.false ]
  %7 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret %struct.RefCntBuffer* %cond
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #1 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #5

; Function Attrs: inlinehint nounwind
define internal i32 @get_relative_dist(%struct.OrderHintInfo* %oh, i32 %a, i32 %b) #1 {
entry:
  %retval = alloca i32, align 4
  %oh.addr = alloca %struct.OrderHintInfo*, align 4
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  %bits = alloca i32, align 4
  %diff = alloca i32, align 4
  %m = alloca i32, align 4
  store %struct.OrderHintInfo* %oh, %struct.OrderHintInfo** %oh.addr, align 4, !tbaa !2
  store i32 %a, i32* %a.addr, align 4, !tbaa !6
  store i32 %b, i32* %b.addr, align 4, !tbaa !6
  %0 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %oh.addr, align 4, !tbaa !2
  %enable_order_hint = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %0, i32 0, i32 0
  %1 = load i32, i32* %enable_order_hint, align 4, !tbaa !124
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.OrderHintInfo*, %struct.OrderHintInfo** %oh.addr, align 4, !tbaa !2
  %order_hint_bits_minus_1 = getelementptr inbounds %struct.OrderHintInfo, %struct.OrderHintInfo* %3, i32 0, i32 1
  %4 = load i32, i32* %order_hint_bits_minus_1, align 4, !tbaa !125
  %add = add nsw i32 %4, 1
  store i32 %add, i32* %bits, align 4, !tbaa !6
  %5 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load i32, i32* %a.addr, align 4, !tbaa !6
  %7 = load i32, i32* %b.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %6, %7
  store i32 %sub, i32* %diff, align 4, !tbaa !6
  %8 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load i32, i32* %bits, align 4, !tbaa !6
  %sub1 = sub nsw i32 %9, 1
  %shl = shl i32 1, %sub1
  store i32 %shl, i32* %m, align 4, !tbaa !6
  %10 = load i32, i32* %diff, align 4, !tbaa !6
  %11 = load i32, i32* %m, align 4, !tbaa !6
  %sub2 = sub nsw i32 %11, 1
  %and = and i32 %10, %sub2
  %12 = load i32, i32* %diff, align 4, !tbaa !6
  %13 = load i32, i32* %m, align 4, !tbaa !6
  %and3 = and i32 %12, %13
  %sub4 = sub nsw i32 %and, %and3
  store i32 %sub4, i32* %diff, align 4, !tbaa !6
  %14 = load i32, i32* %diff, align 4, !tbaa !6
  store i32 %14, i32* %retval, align 4
  %15 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %return

return:                                           ; preds = %if.end, %if.then
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

; Function Attrs: nounwind
define hidden void @av1_setup_dst_planes(%struct.macroblockd_plane* %planes, i8 zeroext %bsize, %struct.yv12_buffer_config* %src, i32 %mi_row, i32 %mi_col, i32 %plane_start, i32 %plane_end) #0 {
entry:
  %planes.addr = alloca %struct.macroblockd_plane*, align 4
  %bsize.addr = alloca i8, align 1
  %src.addr = alloca %struct.yv12_buffer_config*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %plane_start.addr = alloca i32, align 4
  %plane_end.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %is_uv = alloca i32, align 4
  store %struct.macroblockd_plane* %planes, %struct.macroblockd_plane** %planes.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store %struct.yv12_buffer_config* %src, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store i32 %plane_start, i32* %plane_start.addr, align 4, !tbaa !6
  store i32 %plane_end, i32* %plane_end.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %plane_start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %plane_end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, 3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.cond
  %4 = load i32, i32* %plane_end.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.cond
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ 3, %cond.false ]
  %cmp1 = icmp slt i32 %2, %cond
  br i1 %cmp1, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %cond.end
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  br label %for.end

for.body:                                         ; preds = %cond.end
  %6 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %planes.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %7, i32 %8
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %9 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %cmp2 = icmp sgt i32 %10, 0
  %conv = zext i1 %cmp2 to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !6
  %11 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %11, i32 0, i32 6
  %12 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %13 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %14 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %13, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %14 to [3 x i8*]*
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %15
  %16 = load i8*, i8** %arrayidx3, align 4, !tbaa !9
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %18 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 2
  %crop_widths = bitcast %union.anon.2* %18 to [2 x i32]*
  %19 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths, i32 0, i32 %19
  %20 = load i32, i32* %arrayidx4, align 4, !tbaa !9
  %21 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %22 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %21, i32 0, i32 3
  %crop_heights = bitcast %union.anon.4* %22 to [2 x i32]*
  %23 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx5, align 4, !tbaa !9
  %25 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %26 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %25, i32 0, i32 4
  %strides = bitcast %union.anon.6* %26 to [2 x i32]*
  %27 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %27
  %28 = load i32, i32* %arrayidx6, align 4, !tbaa !9
  %29 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %30 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %31 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %31, i32 0, i32 4
  %32 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  %33 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %33, i32 0, i32 5
  %34 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  call void @setup_pred_plane(%struct.buf_2d* %dst, i8 zeroext %12, i8* %16, i32 %20, i32 %24, i32 %28, i32 %29, i32 %30, %struct.scale_factors* null, i32 %32, i32 %34)
  %35 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @setup_pred_plane(%struct.buf_2d* %dst, i8 zeroext %bsize, i8* %src, i32 %width, i32 %height, i32 %stride, i32 %mi_row, i32 %mi_col, %struct.scale_factors* %scale, i32 %subsampling_x, i32 %subsampling_y) #1 {
entry:
  %dst.addr = alloca %struct.buf_2d*, align 4
  %bsize.addr = alloca i8, align 1
  %src.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %scale.addr = alloca %struct.scale_factors*, align 4
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store %struct.buf_2d* %dst, %struct.buf_2d** %dst.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store %struct.scale_factors* %scale, %struct.scale_factors** %scale.addr, align 4, !tbaa !2
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  %0 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %and = and i32 %1, 1
  %tobool1 = icmp ne i32 %and, 0
  br i1 %tobool1, label %land.lhs.true2, label %if.end

land.lhs.true2:                                   ; preds = %land.lhs.true
  %2 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom = zext i8 %2 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %3 to i32
  %cmp = icmp eq i32 %conv, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true2
  %4 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %4, 1
  store i32 %sub, i32* %mi_row.addr, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true2, %land.lhs.true, %entry
  %5 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %tobool4 = icmp ne i32 %5, 0
  br i1 %tobool4, label %land.lhs.true5, label %if.end16

land.lhs.true5:                                   ; preds = %if.end
  %6 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %and6 = and i32 %6, 1
  %tobool7 = icmp ne i32 %and6, 0
  br i1 %tobool7, label %land.lhs.true8, label %if.end16

land.lhs.true8:                                   ; preds = %land.lhs.true5
  %7 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom9 = zext i8 %7 to i32
  %arrayidx10 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom9
  %8 = load i8, i8* %arrayidx10, align 1, !tbaa !9
  %conv11 = zext i8 %8 to i32
  %cmp12 = icmp eq i32 %conv11, 1
  br i1 %cmp12, label %if.then14, label %if.end16

if.then14:                                        ; preds = %land.lhs.true8
  %9 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %sub15 = sub nsw i32 %9, 1
  store i32 %sub15, i32* %mi_col.addr, align 4, !tbaa !6
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %land.lhs.true8, %land.lhs.true5, %if.end
  %10 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %mul = mul nsw i32 4, %11
  %12 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %shr = ashr i32 %mul, %12
  store i32 %shr, i32* %x, align 4, !tbaa !6
  %13 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %mul17 = mul nsw i32 4, %14
  %15 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %shr18 = ashr i32 %mul17, %15
  store i32 %shr18, i32* %y, align 4, !tbaa !6
  %16 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %17 = load i32, i32* %x, align 4, !tbaa !6
  %18 = load i32, i32* %y, align 4, !tbaa !6
  %19 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %20 = load %struct.scale_factors*, %struct.scale_factors** %scale.addr, align 4, !tbaa !2
  %call = call i64 @scaled_buffer_offset(i32 %17, i32 %18, i32 %19, %struct.scale_factors* %20)
  %idx.ext = trunc i64 %call to i32
  %add.ptr = getelementptr inbounds i8, i8* %16, i32 %idx.ext
  %21 = load %struct.buf_2d*, %struct.buf_2d** %dst.addr, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %21, i32 0, i32 0
  store i8* %add.ptr, i8** %buf, align 4, !tbaa !82
  %22 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %23 = load %struct.buf_2d*, %struct.buf_2d** %dst.addr, align 4, !tbaa !2
  %buf0 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %23, i32 0, i32 1
  store i8* %22, i8** %buf0, align 4, !tbaa !84
  %24 = load i32, i32* %width.addr, align 4, !tbaa !6
  %25 = load %struct.buf_2d*, %struct.buf_2d** %dst.addr, align 4, !tbaa !2
  %width19 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %25, i32 0, i32 2
  store i32 %24, i32* %width19, align 4, !tbaa !85
  %26 = load i32, i32* %height.addr, align 4, !tbaa !6
  %27 = load %struct.buf_2d*, %struct.buf_2d** %dst.addr, align 4, !tbaa !2
  %height20 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %27, i32 0, i32 3
  store i32 %26, i32* %height20, align 4, !tbaa !86
  %28 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %29 = load %struct.buf_2d*, %struct.buf_2d** %dst.addr, align 4, !tbaa !2
  %stride21 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %29, i32 0, i32 4
  store i32 %28, i32* %stride21, align 4, !tbaa !83
  %30 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_setup_pre_planes(%struct.macroblockd* %xd, i32 %idx, %struct.yv12_buffer_config* %src, i32 %mi_row, i32 %mi_col, %struct.scale_factors* %sf, i32 %num_planes) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %idx.addr = alloca i32, align 4
  %src.addr = alloca %struct.yv12_buffer_config*, align 4
  %mi_row.addr = alloca i32, align 4
  %mi_col.addr = alloca i32, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %num_planes.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %is_uv = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %idx, i32* %idx.addr, align 4, !tbaa !6
  store %struct.yv12_buffer_config* %src, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  store i32 %mi_row, i32* %mi_row.addr, align 4, !tbaa !6
  store i32 %mi_col, i32* %mi_col.addr, align 4, !tbaa !6
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %cmp = icmp ne %struct.yv12_buffer_config* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %3, 3
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.cond
  %4 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.cond
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ 3, %cond.false ]
  %cmp2 = icmp slt i32 %2, %cond
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %cond.end
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  br label %for.end

for.body:                                         ; preds = %cond.end
  %6 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %7, i32 0, i32 4
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 %8
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %9 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %cmp3 = icmp sgt i32 %10, 0
  %conv = zext i1 %cmp3 to i32
  store i32 %conv, i32* %is_uv, align 4, !tbaa !6
  %11 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %pre = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %11, i32 0, i32 7
  %12 = load i32, i32* %idx.addr, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [2 x %struct.buf_2d], [2 x %struct.buf_2d]* %pre, i32 0, i32 %12
  %13 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %13, i32 0, i32 6
  %14 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %arrayidx5 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %14, i32 0
  %15 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx5, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %15, i32 0, i32 6
  %16 = load i8, i8* %sb_type, align 2, !tbaa !74
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %18 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 5
  %buffers = bitcast %union.anon.8* %18 to [3 x i8*]*
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [3 x i8*], [3 x i8*]* %buffers, i32 0, i32 %19
  %20 = load i8*, i8** %arrayidx6, align 4, !tbaa !9
  %21 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %22 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %21, i32 0, i32 2
  %crop_widths = bitcast %union.anon.2* %22 to [2 x i32]*
  %23 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_widths, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx7, align 4, !tbaa !9
  %25 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %26 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %25, i32 0, i32 3
  %crop_heights = bitcast %union.anon.4* %26 to [2 x i32]*
  %27 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds [2 x i32], [2 x i32]* %crop_heights, i32 0, i32 %27
  %28 = load i32, i32* %arrayidx8, align 4, !tbaa !9
  %29 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %src.addr, align 4, !tbaa !2
  %30 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %29, i32 0, i32 4
  %strides = bitcast %union.anon.6* %30 to [2 x i32]*
  %31 = load i32, i32* %is_uv, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds [2 x i32], [2 x i32]* %strides, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx9, align 4, !tbaa !9
  %33 = load i32, i32* %mi_row.addr, align 4, !tbaa !6
  %34 = load i32, i32* %mi_col.addr, align 4, !tbaa !6
  %35 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %36 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %36, i32 0, i32 4
  %37 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  %38 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %38, i32 0, i32 5
  %39 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  call void @setup_pred_plane(%struct.buf_2d* %arrayidx4, i8 zeroext %16, i8* %20, i32 %24, i32 %28, i32 %32, i32 %33, i32 %34, %struct.scale_factors* %35, i32 %37, i32 %39)
  %40 = bitcast i32* %is_uv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %42, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: nounwind
define hidden i8* @av1_get_obmc_mask(i32 %length) #0 {
entry:
  %retval = alloca i8*, align 4
  %length.addr = alloca i32, align 4
  store i32 %length, i32* %length.addr, align 4, !tbaa !6
  %0 = load i32, i32* %length.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
    i32 4, label %sw.bb2
    i32 8, label %sw.bb3
    i32 16, label %sw.bb4
    i32 32, label %sw.bb5
    i32 64, label %sw.bb6
  ]

sw.bb:                                            ; preds = %entry
  store i8* getelementptr inbounds ([1 x i8], [1 x i8]* @obmc_mask_1, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  store i8* getelementptr inbounds ([2 x i8], [2 x i8]* @obmc_mask_2, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8* getelementptr inbounds ([4 x i8], [4 x i8]* @obmc_mask_4, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store i8* getelementptr inbounds ([8 x i8], [8 x i8]* @obmc_mask_8, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store i8* getelementptr inbounds ([16 x i8], [16 x i8]* @obmc_mask_16, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  store i8* getelementptr inbounds ([32 x i8], [32 x i8]* @obmc_mask_32, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry
  store i8* getelementptr inbounds ([64 x i8], [64 x i8]* @obmc_mask_64, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i8*, i8** %retval, align 4
  ret i8* %1
}

; Function Attrs: nounwind
define hidden void @av1_count_overlappable_neighbors(%struct.AV1Common* %cm, %struct.macroblockd* %xd) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mbmi = alloca %struct.MB_MODE_INFO*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 6
  %2 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %2, i32 0
  %3 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  store %struct.MB_MODE_INFO* %3, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %4 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %overlappable_neighbors = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %4, i32 0, i32 24
  %arrayidx1 = getelementptr inbounds [2 x i8], [2 x i8]* %overlappable_neighbors, i32 0, i32 0
  store i8 0, i8* %arrayidx1, align 1, !tbaa !9
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %overlappable_neighbors2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %5, i32 0, i32 24
  %arrayidx3 = getelementptr inbounds [2 x i8], [2 x i8]* %overlappable_neighbors2, i32 0, i32 1
  store i8 0, i8* %arrayidx3, align 1, !tbaa !9
  %6 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %6, i32 0, i32 6
  %7 = load i8, i8* %sb_type, align 2, !tbaa !74
  %call = call i32 @is_motion_variation_allowed_bsize(i8 zeroext %7)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %8 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %10 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %overlappable_neighbors4 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %10, i32 0, i32 24
  %arrayidx5 = getelementptr inbounds [2 x i8], [2 x i8]* %overlappable_neighbors4, i32 0, i32 0
  call void @foreach_overlappable_nb_above(%struct.AV1Common* %8, %struct.macroblockd* %9, i32 2147483647, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)* @increment_int_ptr, i8* %arrayidx5)
  %11 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %13 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi, align 4, !tbaa !2
  %overlappable_neighbors6 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %13, i32 0, i32 24
  %arrayidx7 = getelementptr inbounds [2 x i8], [2 x i8]* %overlappable_neighbors6, i32 0, i32 1
  call void @foreach_overlappable_nb_left(%struct.AV1Common* %11, %struct.macroblockd* %12, i32 2147483647, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)* @increment_int_ptr, i8* %arrayidx7)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %14 = bitcast %struct.MB_MODE_INFO** %mbmi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_motion_variation_allowed_bsize(i8 zeroext %bsize) #1 {
entry:
  %bsize.addr = alloca i8, align 1
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %1 to i32
  %2 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom1 = zext i8 %2 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom1
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !9
  %conv3 = zext i8 %3 to i32
  %cmp = icmp slt i32 %conv, %conv3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom5 = zext i8 %4 to i32
  %arrayidx6 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom5
  %5 = load i8, i8* %arrayidx6, align 1, !tbaa !9
  %conv7 = zext i8 %5 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom8 = zext i8 %6 to i32
  %arrayidx9 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom8
  %7 = load i8, i8* %arrayidx9, align 1, !tbaa !9
  %conv10 = zext i8 %7 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv7, %cond.true ], [ %conv10, %cond.false ]
  %cmp11 = icmp sge i32 %cond, 8
  %conv12 = zext i1 %cmp11 to i32
  ret i32 %conv12
}

; Function Attrs: inlinehint nounwind
define internal void @foreach_overlappable_nb_above(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %nb_max, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)* %fun, i8* %fun_ctxt) #1 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %nb_max.addr = alloca i32, align 4
  %fun.addr = alloca void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)*, align 4
  %fun_ctxt.addr = alloca i8*, align 4
  %num_planes = alloca i32, align 4
  %nb_count = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %prev_row_mi = alloca %struct.MB_MODE_INFO**, align 4
  %end_col = alloca i32, align 4
  %mi_step = alloca i8, align 1
  %above_mi_col = alloca i32, align 4
  %above_mi = alloca %struct.MB_MODE_INFO**, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %nb_max, i32* %nb_max.addr, align 4, !tbaa !6
  store void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)* %fun, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)** %fun.addr, align 4, !tbaa !2
  store i8* %fun_ctxt, i8** %fun_ctxt.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %up_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 7
  %1 = load i8, i8* %up_available, align 8, !tbaa !126, !range !81
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %3)
  store i32 %call, i32* %num_planes, align 4, !tbaa !6
  %4 = bitcast i32* %nb_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %nb_count, align 4, !tbaa !6
  %5 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_col1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 1
  %7 = load i32, i32* %mi_col1, align 4, !tbaa !127
  store i32 %7, i32* %mi_col, align 4, !tbaa !6
  %8 = bitcast %struct.MB_MODE_INFO*** %prev_row_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %9, i32 0, i32 6
  %10 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %11 = load i32, i32* %mi_col, align 4, !tbaa !6
  %idx.neg = sub i32 0, %11
  %add.ptr = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %10, i32 %idx.neg
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %12, i32 0, i32 2
  %13 = load i32, i32* %mi_stride, align 8, !tbaa !79
  %mul = mul nsw i32 1, %13
  %idx.neg2 = sub i32 0, %mul
  %add.ptr3 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %add.ptr, i32 %idx.neg2
  store %struct.MB_MODE_INFO** %add.ptr3, %struct.MB_MODE_INFO*** %prev_row_mi, align 4, !tbaa !2
  %14 = bitcast i32* %end_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load i32, i32* %mi_col, align 4, !tbaa !6
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %16, i32 0, i32 33
  %17 = load i8, i8* %width, align 4, !tbaa !128
  %conv = zext i8 %17 to i32
  %add = add nsw i32 %15, %conv
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 4
  %19 = load i32, i32* %mi_cols, align 4, !tbaa !129
  %cmp = icmp slt i32 %add, %19
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %20 = load i32, i32* %mi_col, align 4, !tbaa !6
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width5 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %21, i32 0, i32 33
  %22 = load i8, i8* %width5, align 4, !tbaa !128
  %conv6 = zext i8 %22 to i32
  %add7 = add nsw i32 %20, %conv6
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params8 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %23, i32 0, i32 22
  %mi_cols9 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params8, i32 0, i32 4
  %24 = load i32, i32* %mi_cols9, align 4, !tbaa !129
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add7, %cond.true ], [ %24, %cond.false ]
  store i32 %cond, i32* %end_col, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mi_step) #7
  %25 = bitcast i32* %above_mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load i32, i32* %mi_col, align 4, !tbaa !6
  store i32 %26, i32* %above_mi_col, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %27 = load i32, i32* %above_mi_col, align 4, !tbaa !6
  %28 = load i32, i32* %end_col, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %27, %28
  br i1 %cmp10, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %29 = load i32, i32* %nb_count, align 4, !tbaa !6
  %30 = load i32, i32* %nb_max.addr, align 4, !tbaa !6
  %cmp12 = icmp slt i32 %29, %30
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %31 = phi i1 [ false, %for.cond ], [ %cmp12, %land.rhs ]
  br i1 %31, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %land.end
  %32 = bitcast i32* %above_mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  br label %for.end

for.body:                                         ; preds = %land.end
  %33 = bitcast %struct.MB_MODE_INFO*** %above_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %34 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %prev_row_mi, align 4, !tbaa !2
  %35 = load i32, i32* %above_mi_col, align 4, !tbaa !6
  %add.ptr14 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %34, i32 %35
  store %struct.MB_MODE_INFO** %add.ptr14, %struct.MB_MODE_INFO*** %above_mi, align 4, !tbaa !2
  %36 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %above_mi, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %36, i32 0
  %37 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %37, i32 0, i32 6
  %38 = load i8, i8* %sb_type, align 2, !tbaa !74
  %idxprom = zext i8 %38 to i32
  %arrayidx15 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom
  %39 = load i8, i8* %arrayidx15, align 1, !tbaa !9
  %conv16 = zext i8 %39 to i32
  %40 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !9
  %conv17 = zext i8 %40 to i32
  %cmp18 = icmp slt i32 %conv16, %conv17
  br i1 %cmp18, label %cond.true20, label %cond.false26

cond.true20:                                      ; preds = %for.body
  %41 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %above_mi, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %41, i32 0
  %42 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx21, align 4, !tbaa !2
  %sb_type22 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %42, i32 0, i32 6
  %43 = load i8, i8* %sb_type22, align 2, !tbaa !74
  %idxprom23 = zext i8 %43 to i32
  %arrayidx24 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom23
  %44 = load i8, i8* %arrayidx24, align 1, !tbaa !9
  %conv25 = zext i8 %44 to i32
  br label %cond.end28

cond.false26:                                     ; preds = %for.body
  %45 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 12), align 4, !tbaa !9
  %conv27 = zext i8 %45 to i32
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true20
  %cond29 = phi i32 [ %conv25, %cond.true20 ], [ %conv27, %cond.false26 ]
  %conv30 = trunc i32 %cond29 to i8
  store i8 %conv30, i8* %mi_step, align 1, !tbaa !9
  %46 = load i8, i8* %mi_step, align 1, !tbaa !9
  %conv31 = zext i8 %46 to i32
  %cmp32 = icmp eq i32 %conv31, 1
  br i1 %cmp32, label %if.then34, label %if.end37

if.then34:                                        ; preds = %cond.end28
  %47 = load i32, i32* %above_mi_col, align 4, !tbaa !6
  %and = and i32 %47, -2
  store i32 %and, i32* %above_mi_col, align 4, !tbaa !6
  %48 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %prev_row_mi, align 4, !tbaa !2
  %49 = load i32, i32* %above_mi_col, align 4, !tbaa !6
  %add.ptr35 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %48, i32 %49
  %add.ptr36 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %add.ptr35, i32 1
  store %struct.MB_MODE_INFO** %add.ptr36, %struct.MB_MODE_INFO*** %above_mi, align 4, !tbaa !2
  store i8 2, i8* %mi_step, align 1, !tbaa !9
  br label %if.end37

if.end37:                                         ; preds = %if.then34, %cond.end28
  %50 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %above_mi, align 4, !tbaa !2
  %51 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %50, align 4, !tbaa !2
  %call38 = call i32 @is_neighbor_overlappable(%struct.MB_MODE_INFO* %51)
  %tobool39 = icmp ne i32 %call38, 0
  br i1 %tobool39, label %if.then40, label %if.end54

if.then40:                                        ; preds = %if.end37
  %52 = load i32, i32* %nb_count, align 4, !tbaa !6
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %nb_count, align 4, !tbaa !6
  %53 = load void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)*, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)** %fun.addr, align 4, !tbaa !2
  %54 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %55 = load i32, i32* %above_mi_col, align 4, !tbaa !6
  %56 = load i32, i32* %mi_col, align 4, !tbaa !6
  %sub = sub nsw i32 %55, %56
  %57 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width41 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %57, i32 0, i32 33
  %58 = load i8, i8* %width41, align 4, !tbaa !128
  %conv42 = zext i8 %58 to i32
  %59 = load i8, i8* %mi_step, align 1, !tbaa !9
  %conv43 = zext i8 %59 to i32
  %cmp44 = icmp slt i32 %conv42, %conv43
  br i1 %cmp44, label %cond.true46, label %cond.false49

cond.true46:                                      ; preds = %if.then40
  %60 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width47 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %60, i32 0, i32 33
  %61 = load i8, i8* %width47, align 4, !tbaa !128
  %conv48 = zext i8 %61 to i32
  br label %cond.end51

cond.false49:                                     ; preds = %if.then40
  %62 = load i8, i8* %mi_step, align 1, !tbaa !9
  %conv50 = zext i8 %62 to i32
  br label %cond.end51

cond.end51:                                       ; preds = %cond.false49, %cond.true46
  %cond52 = phi i32 [ %conv48, %cond.true46 ], [ %conv50, %cond.false49 ]
  %conv53 = trunc i32 %cond52 to i8
  %63 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %above_mi, align 4, !tbaa !2
  %64 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %63, align 4, !tbaa !2
  %65 = load i8*, i8** %fun_ctxt.addr, align 4, !tbaa !2
  %66 = load i32, i32* %num_planes, align 4, !tbaa !6
  call void %53(%struct.macroblockd* %54, i32 0, i32 %sub, i8 zeroext %conv53, i32 0, %struct.MB_MODE_INFO* %64, i8* %65, i32 %66)
  br label %if.end54

if.end54:                                         ; preds = %cond.end51, %if.end37
  %67 = bitcast %struct.MB_MODE_INFO*** %above_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #7
  br label %for.inc

for.inc:                                          ; preds = %if.end54
  %68 = load i8, i8* %mi_step, align 1, !tbaa !9
  %conv55 = zext i8 %68 to i32
  %69 = load i32, i32* %above_mi_col, align 4, !tbaa !6
  %add56 = add nsw i32 %69, %conv55
  store i32 %add56, i32* %above_mi_col, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mi_step) #7
  %70 = bitcast i32* %end_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  %71 = bitcast %struct.MB_MODE_INFO*** %prev_row_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  %72 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  %73 = bitcast i32* %nb_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  %74 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #7
  br label %return

return:                                           ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @increment_int_ptr(%struct.macroblockd* %xd, i32 %rel_mi_row, i32 %rel_mi_col, i8 zeroext %op_mi_size, i32 %dir, %struct.MB_MODE_INFO* %mi, i8* %fun_ctxt, i32 %num_planes) #1 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %rel_mi_row.addr = alloca i32, align 4
  %rel_mi_col.addr = alloca i32, align 4
  %op_mi_size.addr = alloca i8, align 1
  %dir.addr = alloca i32, align 4
  %mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %fun_ctxt.addr = alloca i8*, align 4
  %num_planes.addr = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %rel_mi_row, i32* %rel_mi_row.addr, align 4, !tbaa !6
  store i32 %rel_mi_col, i32* %rel_mi_col.addr, align 4, !tbaa !6
  store i8 %op_mi_size, i8* %op_mi_size.addr, align 1, !tbaa !9
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %mi, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  store i8* %fun_ctxt, i8** %fun_ctxt.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %1 = load i32, i32* %rel_mi_row.addr, align 4, !tbaa !6
  %2 = load i32, i32* %rel_mi_col.addr, align 4, !tbaa !6
  %3 = load i8, i8* %op_mi_size.addr, align 1, !tbaa !9
  %4 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %5 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mi.addr, align 4, !tbaa !2
  %6 = load i8*, i8** %fun_ctxt.addr, align 4, !tbaa !2
  %7 = bitcast i8* %6 to i32*
  %8 = load i32, i32* %7, align 4, !tbaa !6
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %7, align 4, !tbaa !6
  %9 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @foreach_overlappable_nb_left(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i32 %nb_max, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)* %fun, i8* %fun_ctxt) #1 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %nb_max.addr = alloca i32, align 4
  %fun.addr = alloca void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)*, align 4
  %fun_ctxt.addr = alloca i8*, align 4
  %num_planes = alloca i32, align 4
  %nb_count = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %prev_col_mi = alloca %struct.MB_MODE_INFO**, align 4
  %end_row = alloca i32, align 4
  %mi_step = alloca i8, align 1
  %left_mi_row = alloca i32, align 4
  %left_mi = alloca %struct.MB_MODE_INFO**, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %nb_max, i32* %nb_max.addr, align 4, !tbaa !6
  store void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)* %fun, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)** %fun.addr, align 4, !tbaa !2
  store i8* %fun_ctxt, i8** %fun_ctxt.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %left_available = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 8
  %1 = load i8, i8* %left_available, align 1, !tbaa !130, !range !81
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %call = call i32 @av1_num_planes(%struct.AV1Common* %3)
  store i32 %call, i32* %num_planes, align 4, !tbaa !6
  %4 = bitcast i32* %nb_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %nb_count, align 4, !tbaa !6
  %5 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_row1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 0
  %7 = load i32, i32* %mi_row1, align 16, !tbaa !131
  store i32 %7, i32* %mi_row, align 4, !tbaa !6
  %8 = bitcast %struct.MB_MODE_INFO*** %prev_col_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %9, i32 0, i32 6
  %10 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %add.ptr = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %10, i32 -1
  %11 = load i32, i32* %mi_row, align 4, !tbaa !6
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %12, i32 0, i32 2
  %13 = load i32, i32* %mi_stride, align 8, !tbaa !79
  %mul = mul nsw i32 %11, %13
  %idx.neg = sub i32 0, %mul
  %add.ptr2 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %add.ptr, i32 %idx.neg
  store %struct.MB_MODE_INFO** %add.ptr2, %struct.MB_MODE_INFO*** %prev_col_mi, align 4, !tbaa !2
  %14 = bitcast i32* %end_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load i32, i32* %mi_row, align 4, !tbaa !6
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %16, i32 0, i32 34
  %17 = load i8, i8* %height, align 1, !tbaa !132
  %conv = zext i8 %17 to i32
  %add = add nsw i32 %15, %conv
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %19 = load i32, i32* %mi_rows, align 4, !tbaa !133
  %cmp = icmp slt i32 %add, %19
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %20 = load i32, i32* %mi_row, align 4, !tbaa !6
  %21 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %21, i32 0, i32 34
  %22 = load i8, i8* %height4, align 1, !tbaa !132
  %conv5 = zext i8 %22 to i32
  %add6 = add nsw i32 %20, %conv5
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %23 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %mi_params7 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %23, i32 0, i32 22
  %mi_rows8 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params7, i32 0, i32 3
  %24 = load i32, i32* %mi_rows8, align 4, !tbaa !133
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add6, %cond.true ], [ %24, %cond.false ]
  store i32 %cond, i32* %end_row, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mi_step) #7
  %25 = bitcast i32* %left_mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load i32, i32* %mi_row, align 4, !tbaa !6
  store i32 %26, i32* %left_mi_row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %27 = load i32, i32* %left_mi_row, align 4, !tbaa !6
  %28 = load i32, i32* %end_row, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %27, %28
  br i1 %cmp9, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %29 = load i32, i32* %nb_count, align 4, !tbaa !6
  %30 = load i32, i32* %nb_max.addr, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %29, %30
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %31 = phi i1 [ false, %for.cond ], [ %cmp11, %land.rhs ]
  br i1 %31, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %land.end
  %32 = bitcast i32* %left_mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  br label %for.end

for.body:                                         ; preds = %land.end
  %33 = bitcast %struct.MB_MODE_INFO*** %left_mi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %34 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %prev_col_mi, align 4, !tbaa !2
  %35 = load i32, i32* %left_mi_row, align 4, !tbaa !6
  %36 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride13 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %36, i32 0, i32 2
  %37 = load i32, i32* %mi_stride13, align 8, !tbaa !79
  %mul14 = mul nsw i32 %35, %37
  %add.ptr15 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %34, i32 %mul14
  store %struct.MB_MODE_INFO** %add.ptr15, %struct.MB_MODE_INFO*** %left_mi, align 4, !tbaa !2
  %38 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %left_mi, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %38, i32 0
  %39 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %39, i32 0, i32 6
  %40 = load i8, i8* %sb_type, align 2, !tbaa !74
  %idxprom = zext i8 %40 to i32
  %arrayidx16 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom
  %41 = load i8, i8* %arrayidx16, align 1, !tbaa !9
  %conv17 = zext i8 %41 to i32
  %42 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !9
  %conv18 = zext i8 %42 to i32
  %cmp19 = icmp slt i32 %conv17, %conv18
  br i1 %cmp19, label %cond.true21, label %cond.false27

cond.true21:                                      ; preds = %for.body
  %43 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %left_mi, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %43, i32 0
  %44 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx22, align 4, !tbaa !2
  %sb_type23 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %44, i32 0, i32 6
  %45 = load i8, i8* %sb_type23, align 2, !tbaa !74
  %idxprom24 = zext i8 %45 to i32
  %arrayidx25 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom24
  %46 = load i8, i8* %arrayidx25, align 1, !tbaa !9
  %conv26 = zext i8 %46 to i32
  br label %cond.end29

cond.false27:                                     ; preds = %for.body
  %47 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 12), align 4, !tbaa !9
  %conv28 = zext i8 %47 to i32
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false27, %cond.true21
  %cond30 = phi i32 [ %conv26, %cond.true21 ], [ %conv28, %cond.false27 ]
  %conv31 = trunc i32 %cond30 to i8
  store i8 %conv31, i8* %mi_step, align 1, !tbaa !9
  %48 = load i8, i8* %mi_step, align 1, !tbaa !9
  %conv32 = zext i8 %48 to i32
  %cmp33 = icmp eq i32 %conv32, 1
  br i1 %cmp33, label %if.then35, label %if.end40

if.then35:                                        ; preds = %cond.end29
  %49 = load i32, i32* %left_mi_row, align 4, !tbaa !6
  %and = and i32 %49, -2
  store i32 %and, i32* %left_mi_row, align 4, !tbaa !6
  %50 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %prev_col_mi, align 4, !tbaa !2
  %51 = load i32, i32* %left_mi_row, align 4, !tbaa !6
  %add36 = add nsw i32 %51, 1
  %52 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_stride37 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %52, i32 0, i32 2
  %53 = load i32, i32* %mi_stride37, align 8, !tbaa !79
  %mul38 = mul nsw i32 %add36, %53
  %add.ptr39 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %50, i32 %mul38
  store %struct.MB_MODE_INFO** %add.ptr39, %struct.MB_MODE_INFO*** %left_mi, align 4, !tbaa !2
  store i8 2, i8* %mi_step, align 1, !tbaa !9
  br label %if.end40

if.end40:                                         ; preds = %if.then35, %cond.end29
  %54 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %left_mi, align 4, !tbaa !2
  %55 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %54, align 4, !tbaa !2
  %call41 = call i32 @is_neighbor_overlappable(%struct.MB_MODE_INFO* %55)
  %tobool42 = icmp ne i32 %call41, 0
  br i1 %tobool42, label %if.then43, label %if.end57

if.then43:                                        ; preds = %if.end40
  %56 = load i32, i32* %nb_count, align 4, !tbaa !6
  %inc = add nsw i32 %56, 1
  store i32 %inc, i32* %nb_count, align 4, !tbaa !6
  %57 = load void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)*, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)** %fun.addr, align 4, !tbaa !2
  %58 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %59 = load i32, i32* %left_mi_row, align 4, !tbaa !6
  %60 = load i32, i32* %mi_row, align 4, !tbaa !6
  %sub = sub nsw i32 %59, %60
  %61 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height44 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %61, i32 0, i32 34
  %62 = load i8, i8* %height44, align 1, !tbaa !132
  %conv45 = zext i8 %62 to i32
  %63 = load i8, i8* %mi_step, align 1, !tbaa !9
  %conv46 = zext i8 %63 to i32
  %cmp47 = icmp slt i32 %conv45, %conv46
  br i1 %cmp47, label %cond.true49, label %cond.false52

cond.true49:                                      ; preds = %if.then43
  %64 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height50 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %64, i32 0, i32 34
  %65 = load i8, i8* %height50, align 1, !tbaa !132
  %conv51 = zext i8 %65 to i32
  br label %cond.end54

cond.false52:                                     ; preds = %if.then43
  %66 = load i8, i8* %mi_step, align 1, !tbaa !9
  %conv53 = zext i8 %66 to i32
  br label %cond.end54

cond.end54:                                       ; preds = %cond.false52, %cond.true49
  %cond55 = phi i32 [ %conv51, %cond.true49 ], [ %conv53, %cond.false52 ]
  %conv56 = trunc i32 %cond55 to i8
  %67 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %left_mi, align 4, !tbaa !2
  %68 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %67, align 4, !tbaa !2
  %69 = load i8*, i8** %fun_ctxt.addr, align 4, !tbaa !2
  %70 = load i32, i32* %num_planes, align 4, !tbaa !6
  call void %57(%struct.macroblockd* %58, i32 %sub, i32 0, i8 zeroext %conv56, i32 1, %struct.MB_MODE_INFO* %68, i8* %69, i32 %70)
  br label %if.end57

if.end57:                                         ; preds = %cond.end54, %if.end40
  %71 = bitcast %struct.MB_MODE_INFO*** %left_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  br label %for.inc

for.inc:                                          ; preds = %if.end57
  %72 = load i8, i8* %mi_step, align 1, !tbaa !9
  %conv58 = zext i8 %72 to i32
  %73 = load i32, i32* %left_mi_row, align 4, !tbaa !6
  %add59 = add nsw i32 %73, %conv58
  store i32 %add59, i32* %left_mi_row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mi_step) #7
  %74 = bitcast i32* %end_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #7
  %75 = bitcast %struct.MB_MODE_INFO*** %prev_col_mi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #7
  %76 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #7
  %77 = bitcast i32* %nb_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  %78 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #7
  br label %return

return:                                           ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_skip_u4x4_pred_in_obmc(i8 zeroext %bsize, %struct.macroblockd_plane* %pd, i32 %dir) #0 {
entry:
  %retval = alloca i32, align 4
  %bsize.addr = alloca i8, align 1
  %pd.addr = alloca %struct.macroblockd_plane*, align 4
  %dir.addr = alloca i32, align 4
  %bsize_plane = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store %struct.macroblockd_plane* %pd, %struct.macroblockd_plane** %pd.addr, align 4, !tbaa !2
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize_plane) #7
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %1 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %1, i32 0, i32 4
  %2 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  %3 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %3, i32 0, i32 5
  %4 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  %call = call zeroext i8 @get_plane_block_size(i8 zeroext %0, i32 %2, i32 %4)
  store i8 %call, i8* %bsize_plane, align 1, !tbaa !9
  %5 = load i8, i8* %bsize_plane, align 1, !tbaa !9
  %conv = zext i8 %5 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 2, label %sw.bb
    i32 1, label %sw.bb
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry
  %6 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %6, 0
  %conv1 = zext i1 %cmp to i32
  store i32 %conv1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.default:                                       ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %sw.default, %sw.bb
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize_plane) #7
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_plane_block_size(i8 zeroext %bsize, i32 %subsampling_x, i32 %subsampling_y) #1 {
entry:
  %bsize.addr = alloca i8, align 1
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !6
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !6
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x [2 x [2 x i8]]], [22 x [2 x [2 x i8]]]* @ss_size_lookup, i32 0, i32 %idxprom
  %1 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds [2 x [2 x i8]], [2 x [2 x i8]]* %arrayidx, i32 0, i32 %1
  %2 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx1, i32 0, i32 %2
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !9
  ret i8 %3
}

; Function Attrs: nounwind
define hidden void @av1_modify_neighbor_predictor_for_obmc(%struct.MB_MODE_INFO* %mbmi) #0 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 1
  store i8 -1, i8* %arrayidx, align 1, !tbaa !9
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %interinter_comp = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 0
  %type = getelementptr inbounds %struct.INTERINTER_COMPOUND_DATA, %struct.INTERINTER_COMPOUND_DATA* %interinter_comp, i32 0, i32 4
  store i8 0, i8* %type, align 1, !tbaa !94
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_build_obmc_inter_prediction(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i8** %above, i32* %above_stride, i8** %left, i32* %left_stride) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %above.addr = alloca i8**, align 4
  %above_stride.addr = alloca i32*, align 4
  %left.addr = alloca i8**, align 4
  %left_stride.addr = alloca i32*, align 4
  %bsize = alloca i8, align 1
  %ctxt_above = alloca %struct.obmc_inter_pred_ctxt, align 4
  %ctxt_left = alloca %struct.obmc_inter_pred_ctxt, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8** %above, i8*** %above.addr, align 4, !tbaa !2
  store i32* %above_stride, i32** %above_stride.addr, align 4, !tbaa !2
  store i8** %left, i8*** %left.addr, align 4, !tbaa !2
  store i32* %left_stride, i32** %left_stride.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #7
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 6
  %1 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %1, i32 0
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 6
  %3 = load i8, i8* %sb_type, align 2, !tbaa !74
  store i8 %3, i8* %bsize, align 1, !tbaa !9
  %4 = bitcast %struct.obmc_inter_pred_ctxt* %ctxt_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #7
  %adjacent = getelementptr inbounds %struct.obmc_inter_pred_ctxt, %struct.obmc_inter_pred_ctxt* %ctxt_above, i32 0, i32 0
  %5 = load i8**, i8*** %above.addr, align 4, !tbaa !2
  store i8** %5, i8*** %adjacent, align 4, !tbaa !134
  %adjacent_stride = getelementptr inbounds %struct.obmc_inter_pred_ctxt, %struct.obmc_inter_pred_ctxt* %ctxt_above, i32 0, i32 1
  %6 = load i32*, i32** %above_stride.addr, align 4, !tbaa !2
  store i32* %6, i32** %adjacent_stride, align 4, !tbaa !136
  %7 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %9 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom = zext i8 %9 to i32
  %arrayidx1 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide_log2, i32 0, i32 %idxprom
  %10 = load i8, i8* %arrayidx1, align 1, !tbaa !9
  %idxprom2 = zext i8 %10 to i32
  %arrayidx3 = getelementptr inbounds [6 x i32], [6 x i32]* @max_neighbor_obmc, i32 0, i32 %idxprom2
  %11 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %12 = bitcast %struct.obmc_inter_pred_ctxt* %ctxt_above to i8*
  call void @foreach_overlappable_nb_above(%struct.AV1Common* %7, %struct.macroblockd* %8, i32 %11, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)* @build_obmc_inter_pred_above, i8* %12)
  %13 = bitcast %struct.obmc_inter_pred_ctxt* %ctxt_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #7
  %adjacent4 = getelementptr inbounds %struct.obmc_inter_pred_ctxt, %struct.obmc_inter_pred_ctxt* %ctxt_left, i32 0, i32 0
  %14 = load i8**, i8*** %left.addr, align 4, !tbaa !2
  store i8** %14, i8*** %adjacent4, align 4, !tbaa !134
  %adjacent_stride5 = getelementptr inbounds %struct.obmc_inter_pred_ctxt, %struct.obmc_inter_pred_ctxt* %ctxt_left, i32 0, i32 1
  %15 = load i32*, i32** %left_stride.addr, align 4, !tbaa !2
  store i32* %15, i32** %adjacent_stride5, align 4, !tbaa !136
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %18 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom6 = zext i8 %18 to i32
  %arrayidx7 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high_log2, i32 0, i32 %idxprom6
  %19 = load i8, i8* %arrayidx7, align 1, !tbaa !9
  %idxprom8 = zext i8 %19 to i32
  %arrayidx9 = getelementptr inbounds [6 x i32], [6 x i32]* @max_neighbor_obmc, i32 0, i32 %idxprom8
  %20 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %21 = bitcast %struct.obmc_inter_pred_ctxt* %ctxt_left to i8*
  call void @foreach_overlappable_nb_left(%struct.AV1Common* %16, %struct.macroblockd* %17, i32 %20, void (%struct.macroblockd*, i32, i32, i8, i32, %struct.MB_MODE_INFO*, i8*, i32)* @build_obmc_inter_pred_left, i8* %21)
  %22 = bitcast %struct.obmc_inter_pred_ctxt* %ctxt_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #7
  %23 = bitcast %struct.obmc_inter_pred_ctxt* %ctxt_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @build_obmc_inter_pred_above(%struct.macroblockd* %xd, i32 %rel_mi_row, i32 %rel_mi_col, i8 zeroext %op_mi_size, i32 %dir, %struct.MB_MODE_INFO* %above_mi, i8* %fun_ctxt, i32 %num_planes) #1 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %rel_mi_row.addr = alloca i32, align 4
  %rel_mi_col.addr = alloca i32, align 4
  %op_mi_size.addr = alloca i8, align 1
  %dir.addr = alloca i32, align 4
  %above_mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %fun_ctxt.addr = alloca i8*, align 4
  %num_planes.addr = alloca i32, align 4
  %ctxt = alloca %struct.obmc_inter_pred_ctxt*, align 4
  %bsize = alloca i8, align 1
  %overlap = alloca i32, align 4
  %plane = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %plane_col = alloca i32, align 4
  %dst_stride = alloca i32, align 4
  %dst18 = alloca i8*, align 4
  %tmp_stride = alloca i32, align 4
  %tmp = alloca i8*, align 4
  %mask = alloca i8*, align 4
  %is_hbd = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %rel_mi_row, i32* %rel_mi_row.addr, align 4, !tbaa !6
  store i32 %rel_mi_col, i32* %rel_mi_col.addr, align 4, !tbaa !6
  store i8 %op_mi_size, i8* %op_mi_size.addr, align 1, !tbaa !9
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %above_mi, %struct.MB_MODE_INFO** %above_mi.addr, align 4, !tbaa !2
  store i8* %fun_ctxt, i8** %fun_ctxt.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi.addr, align 4, !tbaa !2
  %1 = load i32, i32* %rel_mi_row.addr, align 4, !tbaa !6
  %2 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %3 = bitcast %struct.obmc_inter_pred_ctxt** %ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i8*, i8** %fun_ctxt.addr, align 4, !tbaa !2
  %5 = bitcast i8* %4 to %struct.obmc_inter_pred_ctxt*
  store %struct.obmc_inter_pred_ctxt* %5, %struct.obmc_inter_pred_ctxt** %ctxt, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #7
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 6
  %7 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %7, i32 0
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 6
  %9 = load i8, i8* %sb_type, align 2, !tbaa !74
  store i8 %9, i8* %bsize, align 1, !tbaa !9
  %10 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom = zext i8 %11 to i32
  %arrayidx1 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom
  %12 = load i8, i8* %arrayidx1, align 1, !tbaa !9
  %conv = zext i8 %12 to i32
  %13 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @block_size_high, i32 0, i32 12), align 4, !tbaa !9
  %conv2 = zext i8 %13 to i32
  %cmp = icmp slt i32 %conv, %conv2
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %14 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom4 = zext i8 %14 to i32
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom4
  %15 = load i8, i8* %arrayidx5, align 1, !tbaa !9
  %conv6 = zext i8 %15 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %16 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @block_size_high, i32 0, i32 12), align 4, !tbaa !9
  %conv7 = zext i8 %16 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv6, %cond.true ], [ %conv7, %cond.false ]
  %shr = ashr i32 %cond, 1
  store i32 %shr, i32* %overlap, align 4, !tbaa !6
  %17 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  store i32 0, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %18 = load i32, i32* %plane, align 4, !tbaa !6
  %19 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %18, %19
  br i1 %cmp8, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %20 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %21 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane10 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %22, i32 0, i32 4
  %23 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane10, i32 0, i32 %23
  store %struct.macroblockd_plane* %arrayidx11, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %24 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = load i8, i8* %op_mi_size.addr, align 1, !tbaa !9
  %conv12 = zext i8 %25 to i32
  %mul = mul nsw i32 %conv12, 4
  %26 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %26, i32 0, i32 4
  %27 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  %shr13 = ashr i32 %mul, %27
  store i32 %shr13, i32* %bw, align 4, !tbaa !6
  %28 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load i32, i32* %overlap, align 4, !tbaa !6
  %30 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %30, i32 0, i32 5
  %31 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  %shr14 = ashr i32 %29, %31
  store i32 %shr14, i32* %bh, align 4, !tbaa !6
  %32 = bitcast i32* %plane_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = load i32, i32* %rel_mi_col.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 %33, 4
  %34 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x16 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %34, i32 0, i32 4
  %35 = load i32, i32* %subsampling_x16, align 4, !tbaa !75
  %shr17 = ashr i32 %mul15, %35
  store i32 %shr17, i32* %plane_col, align 4, !tbaa !6
  %36 = load i8, i8* %bsize, align 1, !tbaa !9
  %37 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %call = call i32 @av1_skip_u4x4_pred_in_obmc(i8 zeroext %36, %struct.macroblockd_plane* %37, i32 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %38 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  %39 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %39, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 4
  %40 = load i32, i32* %stride, align 4, !tbaa !137
  store i32 %40, i32* %dst_stride, align 4, !tbaa !6
  %41 = bitcast i8** %dst18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  %42 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst19 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %42, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst19, i32 0, i32 0
  %43 = load i8*, i8** %buf, align 4, !tbaa !138
  %44 = load i32, i32* %plane_col, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds i8, i8* %43, i32 %44
  store i8* %arrayidx20, i8** %dst18, align 4, !tbaa !2
  %45 = bitcast i32* %tmp_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #7
  %46 = load %struct.obmc_inter_pred_ctxt*, %struct.obmc_inter_pred_ctxt** %ctxt, align 4, !tbaa !2
  %adjacent_stride = getelementptr inbounds %struct.obmc_inter_pred_ctxt, %struct.obmc_inter_pred_ctxt* %46, i32 0, i32 1
  %47 = load i32*, i32** %adjacent_stride, align 4, !tbaa !136
  %48 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds i32, i32* %47, i32 %48
  %49 = load i32, i32* %arrayidx21, align 4, !tbaa !6
  store i32 %49, i32* %tmp_stride, align 4, !tbaa !6
  %50 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load %struct.obmc_inter_pred_ctxt*, %struct.obmc_inter_pred_ctxt** %ctxt, align 4, !tbaa !2
  %adjacent = getelementptr inbounds %struct.obmc_inter_pred_ctxt, %struct.obmc_inter_pred_ctxt* %51, i32 0, i32 0
  %52 = load i8**, i8*** %adjacent, align 4, !tbaa !134
  %53 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i8*, i8** %52, i32 %53
  %54 = load i8*, i8** %arrayidx22, align 4, !tbaa !2
  %55 = load i32, i32* %plane_col, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds i8, i8* %54, i32 %55
  store i8* %arrayidx23, i8** %tmp, align 4, !tbaa !2
  %56 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #7
  %57 = load i32, i32* %bh, align 4, !tbaa !6
  %call24 = call i8* @av1_get_obmc_mask(i32 %57)
  store i8* %call24, i8** %mask, align 4, !tbaa !2
  %58 = bitcast i32* %is_hbd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #7
  %59 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call25 = call i32 @is_cur_buf_hbd(%struct.macroblockd* %59)
  store i32 %call25, i32* %is_hbd, align 4, !tbaa !6
  %60 = load i32, i32* %is_hbd, align 4, !tbaa !6
  %tobool26 = icmp ne i32 %60, 0
  br i1 %tobool26, label %if.then27, label %if.else

if.then27:                                        ; preds = %if.end
  %61 = load i8*, i8** %dst18, align 4, !tbaa !2
  %62 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %63 = load i8*, i8** %dst18, align 4, !tbaa !2
  %64 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %65 = load i8*, i8** %tmp, align 4, !tbaa !2
  %66 = load i32, i32* %tmp_stride, align 4, !tbaa !6
  %67 = load i8*, i8** %mask, align 4, !tbaa !2
  %68 = load i32, i32* %bw, align 4, !tbaa !6
  %69 = load i32, i32* %bh, align 4, !tbaa !6
  %70 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %70, i32 0, i32 41
  %71 = load i32, i32* %bd, align 4, !tbaa !88
  call void @aom_highbd_blend_a64_vmask_c(i8* %61, i32 %62, i8* %63, i32 %64, i8* %65, i32 %66, i8* %67, i32 %68, i32 %69, i32 %71)
  br label %if.end28

if.else:                                          ; preds = %if.end
  %72 = load i8*, i8** %dst18, align 4, !tbaa !2
  %73 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %74 = load i8*, i8** %dst18, align 4, !tbaa !2
  %75 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %76 = load i8*, i8** %tmp, align 4, !tbaa !2
  %77 = load i32, i32* %tmp_stride, align 4, !tbaa !6
  %78 = load i8*, i8** %mask, align 4, !tbaa !2
  %79 = load i32, i32* %bw, align 4, !tbaa !6
  %80 = load i32, i32* %bh, align 4, !tbaa !6
  call void @aom_blend_a64_vmask_c(i8* %72, i32 %73, i8* %74, i32 %75, i8* %76, i32 %77, i8* %78, i32 %79, i32 %80)
  br label %if.end28

if.end28:                                         ; preds = %if.else, %if.then27
  %81 = bitcast i32* %is_hbd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #7
  %82 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #7
  %83 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #7
  %84 = bitcast i32* %tmp_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #7
  %85 = bitcast i8** %dst18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #7
  %86 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end28, %if.then
  %87 = bitcast i32* %plane_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #7
  %88 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #7
  %89 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #7
  %90 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %91 = load i32, i32* %plane, align 4, !tbaa !6
  %inc = add nsw i32 %91, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %92 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #7
  %93 = bitcast %struct.obmc_inter_pred_ctxt** %ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #7
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal void @build_obmc_inter_pred_left(%struct.macroblockd* %xd, i32 %rel_mi_row, i32 %rel_mi_col, i8 zeroext %op_mi_size, i32 %dir, %struct.MB_MODE_INFO* %left_mi, i8* %fun_ctxt, i32 %num_planes) #1 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %rel_mi_row.addr = alloca i32, align 4
  %rel_mi_col.addr = alloca i32, align 4
  %op_mi_size.addr = alloca i8, align 1
  %dir.addr = alloca i32, align 4
  %left_mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %fun_ctxt.addr = alloca i8*, align 4
  %num_planes.addr = alloca i32, align 4
  %ctxt = alloca %struct.obmc_inter_pred_ctxt*, align 4
  %bsize = alloca i8, align 1
  %overlap = alloca i32, align 4
  %plane = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %plane_row = alloca i32, align 4
  %dst_stride = alloca i32, align 4
  %dst18 = alloca i8*, align 4
  %tmp_stride = alloca i32, align 4
  %tmp = alloca i8*, align 4
  %mask = alloca i8*, align 4
  %is_hbd = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %rel_mi_row, i32* %rel_mi_row.addr, align 4, !tbaa !6
  store i32 %rel_mi_col, i32* %rel_mi_col.addr, align 4, !tbaa !6
  store i8 %op_mi_size, i8* %op_mi_size.addr, align 1, !tbaa !9
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %left_mi, %struct.MB_MODE_INFO** %left_mi.addr, align 4, !tbaa !2
  store i8* %fun_ctxt, i8** %fun_ctxt.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi.addr, align 4, !tbaa !2
  %1 = load i32, i32* %rel_mi_col.addr, align 4, !tbaa !6
  %2 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %3 = bitcast %struct.obmc_inter_pred_ctxt** %ctxt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i8*, i8** %fun_ctxt.addr, align 4, !tbaa !2
  %5 = bitcast i8* %4 to %struct.obmc_inter_pred_ctxt*
  store %struct.obmc_inter_pred_ctxt* %5, %struct.obmc_inter_pred_ctxt** %ctxt, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #7
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 6
  %7 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %arrayidx = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %7, i32 0
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %8, i32 0, i32 6
  %9 = load i8, i8* %sb_type, align 2, !tbaa !74
  store i8 %9, i8* %bsize, align 1, !tbaa !9
  %10 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom = zext i8 %11 to i32
  %arrayidx1 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %12 = load i8, i8* %arrayidx1, align 1, !tbaa !9
  %conv = zext i8 %12 to i32
  %13 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 12), align 4, !tbaa !9
  %conv2 = zext i8 %13 to i32
  %cmp = icmp slt i32 %conv, %conv2
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %14 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom4 = zext i8 %14 to i32
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom4
  %15 = load i8, i8* %arrayidx5, align 1, !tbaa !9
  %conv6 = zext i8 %15 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %16 = load i8, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 12), align 4, !tbaa !9
  %conv7 = zext i8 %16 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv6, %cond.true ], [ %conv7, %cond.false ]
  %shr = ashr i32 %cond, 1
  store i32 %shr, i32* %overlap, align 4, !tbaa !6
  %17 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  store i32 0, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %18 = load i32, i32* %plane, align 4, !tbaa !6
  %19 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %18, %19
  br i1 %cmp8, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %20 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %21 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane10 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %22, i32 0, i32 4
  %23 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane10, i32 0, i32 %23
  store %struct.macroblockd_plane* %arrayidx11, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %24 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = load i32, i32* %overlap, align 4, !tbaa !6
  %26 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %26, i32 0, i32 4
  %27 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  %shr12 = ashr i32 %25, %27
  store i32 %shr12, i32* %bw, align 4, !tbaa !6
  %28 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load i8, i8* %op_mi_size.addr, align 1, !tbaa !9
  %conv13 = zext i8 %29 to i32
  %mul = mul nsw i32 %conv13, 4
  %30 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %30, i32 0, i32 5
  %31 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  %shr14 = ashr i32 %mul, %31
  store i32 %shr14, i32* %bh, align 4, !tbaa !6
  %32 = bitcast i32* %plane_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = load i32, i32* %rel_mi_row.addr, align 4, !tbaa !6
  %mul15 = mul nsw i32 %33, 4
  %34 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y16 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %34, i32 0, i32 5
  %35 = load i32, i32* %subsampling_y16, align 4, !tbaa !77
  %shr17 = ashr i32 %mul15, %35
  store i32 %shr17, i32* %plane_row, align 4, !tbaa !6
  %36 = load i8, i8* %bsize, align 1, !tbaa !9
  %37 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %call = call i32 @av1_skip_u4x4_pred_in_obmc(i8 zeroext %36, %struct.macroblockd_plane* %37, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %38 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  %39 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %39, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 4
  %40 = load i32, i32* %stride, align 4, !tbaa !137
  store i32 %40, i32* %dst_stride, align 4, !tbaa !6
  %41 = bitcast i8** %dst18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  %42 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst19 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %42, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst19, i32 0, i32 0
  %43 = load i8*, i8** %buf, align 4, !tbaa !138
  %44 = load i32, i32* %plane_row, align 4, !tbaa !6
  %45 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %mul20 = mul nsw i32 %44, %45
  %arrayidx21 = getelementptr inbounds i8, i8* %43, i32 %mul20
  store i8* %arrayidx21, i8** %dst18, align 4, !tbaa !2
  %46 = bitcast i32* %tmp_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #7
  %47 = load %struct.obmc_inter_pred_ctxt*, %struct.obmc_inter_pred_ctxt** %ctxt, align 4, !tbaa !2
  %adjacent_stride = getelementptr inbounds %struct.obmc_inter_pred_ctxt, %struct.obmc_inter_pred_ctxt* %47, i32 0, i32 1
  %48 = load i32*, i32** %adjacent_stride, align 4, !tbaa !136
  %49 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i32, i32* %48, i32 %49
  %50 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  store i32 %50, i32* %tmp_stride, align 4, !tbaa !6
  %51 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #7
  %52 = load %struct.obmc_inter_pred_ctxt*, %struct.obmc_inter_pred_ctxt** %ctxt, align 4, !tbaa !2
  %adjacent = getelementptr inbounds %struct.obmc_inter_pred_ctxt, %struct.obmc_inter_pred_ctxt* %52, i32 0, i32 0
  %53 = load i8**, i8*** %adjacent, align 4, !tbaa !134
  %54 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds i8*, i8** %53, i32 %54
  %55 = load i8*, i8** %arrayidx23, align 4, !tbaa !2
  %56 = load i32, i32* %plane_row, align 4, !tbaa !6
  %57 = load i32, i32* %tmp_stride, align 4, !tbaa !6
  %mul24 = mul nsw i32 %56, %57
  %arrayidx25 = getelementptr inbounds i8, i8* %55, i32 %mul24
  store i8* %arrayidx25, i8** %tmp, align 4, !tbaa !2
  %58 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #7
  %59 = load i32, i32* %bw, align 4, !tbaa !6
  %call26 = call i8* @av1_get_obmc_mask(i32 %59)
  store i8* %call26, i8** %mask, align 4, !tbaa !2
  %60 = bitcast i32* %is_hbd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #7
  %61 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call27 = call i32 @is_cur_buf_hbd(%struct.macroblockd* %61)
  store i32 %call27, i32* %is_hbd, align 4, !tbaa !6
  %62 = load i32, i32* %is_hbd, align 4, !tbaa !6
  %tobool28 = icmp ne i32 %62, 0
  br i1 %tobool28, label %if.then29, label %if.else

if.then29:                                        ; preds = %if.end
  %63 = load i8*, i8** %dst18, align 4, !tbaa !2
  %64 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %65 = load i8*, i8** %dst18, align 4, !tbaa !2
  %66 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %67 = load i8*, i8** %tmp, align 4, !tbaa !2
  %68 = load i32, i32* %tmp_stride, align 4, !tbaa !6
  %69 = load i8*, i8** %mask, align 4, !tbaa !2
  %70 = load i32, i32* %bw, align 4, !tbaa !6
  %71 = load i32, i32* %bh, align 4, !tbaa !6
  %72 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %72, i32 0, i32 41
  %73 = load i32, i32* %bd, align 4, !tbaa !88
  call void @aom_highbd_blend_a64_hmask_c(i8* %63, i32 %64, i8* %65, i32 %66, i8* %67, i32 %68, i8* %69, i32 %70, i32 %71, i32 %73)
  br label %if.end30

if.else:                                          ; preds = %if.end
  %74 = load i8*, i8** %dst18, align 4, !tbaa !2
  %75 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %76 = load i8*, i8** %dst18, align 4, !tbaa !2
  %77 = load i32, i32* %dst_stride, align 4, !tbaa !6
  %78 = load i8*, i8** %tmp, align 4, !tbaa !2
  %79 = load i32, i32* %tmp_stride, align 4, !tbaa !6
  %80 = load i8*, i8** %mask, align 4, !tbaa !2
  %81 = load i32, i32* %bw, align 4, !tbaa !6
  %82 = load i32, i32* %bh, align 4, !tbaa !6
  call void @aom_blend_a64_hmask_c(i8* %74, i32 %75, i8* %76, i32 %77, i8* %78, i32 %79, i8* %80, i32 %81, i32 %82)
  br label %if.end30

if.end30:                                         ; preds = %if.else, %if.then29
  %83 = bitcast i32* %is_hbd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #7
  %84 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #7
  %85 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #7
  %86 = bitcast i32* %tmp_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #7
  %87 = bitcast i8** %dst18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #7
  %88 = bitcast i32* %dst_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end30, %if.then
  %89 = bitcast i32* %plane_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #7
  %90 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #7
  %91 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #7
  %92 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %93 = load i32, i32* %plane, align 4, !tbaa !6
  %inc = add nsw i32 %93, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %94 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #7
  %95 = bitcast %struct.obmc_inter_pred_ctxt** %ctxt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #7
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @av1_setup_address_for_obmc(%struct.macroblockd* %xd, i32 %mi_row_offset, i32 %mi_col_offset, %struct.MB_MODE_INFO* %ref_mbmi, %struct.build_prediction_ctxt* %ctxt, i32 %num_planes) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %mi_row_offset.addr = alloca i32, align 4
  %mi_col_offset.addr = alloca i32, align 4
  %ref_mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %ctxt.addr = alloca %struct.build_prediction_ctxt*, align 4
  %num_planes.addr = alloca i32, align 4
  %ref_bsize = alloca i8, align 1
  %ref_mi_row = alloca i32, align 4
  %ref_mi_col = alloca i32, align 4
  %plane = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %frame = alloca i8, align 1
  %ref_buf = alloca %struct.RefCntBuffer*, align 4
  %sf = alloca %struct.scale_factors*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %mi_row_offset, i32* %mi_row_offset.addr, align 4, !tbaa !6
  store i32 %mi_col_offset, i32* %mi_col_offset.addr, align 4, !tbaa !6
  store %struct.MB_MODE_INFO* %ref_mbmi, %struct.MB_MODE_INFO** %ref_mbmi.addr, align 4, !tbaa !2
  store %struct.build_prediction_ctxt* %ctxt, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref_bsize) #7
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ref_mbmi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 6
  %1 = load i8, i8* %sb_type, align 2, !tbaa !74
  %conv = zext i8 %1 to i32
  %cmp = icmp sgt i32 3, %conv
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ref_mbmi.addr, align 4, !tbaa !2
  %sb_type2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 6
  %3 = load i8, i8* %sb_type2, align 2, !tbaa !74
  %conv3 = zext i8 %3 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 3, %cond.true ], [ %conv3, %cond.false ]
  %conv4 = trunc i32 %cond to i8
  store i8 %conv4, i8* %ref_bsize, align 1, !tbaa !9
  %4 = bitcast i32* %ref_mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 0
  %6 = load i32, i32* %mi_row, align 16, !tbaa !131
  %7 = load i32, i32* %mi_row_offset.addr, align 4, !tbaa !6
  %add = add nsw i32 %6, %7
  store i32 %add, i32* %ref_mi_row, align 4, !tbaa !6
  %8 = bitcast i32* %ref_mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %9, i32 0, i32 1
  %10 = load i32, i32* %mi_col, align 4, !tbaa !127
  %11 = load i32, i32* %mi_col_offset.addr, align 4, !tbaa !6
  %add5 = add nsw i32 %10, %11
  store i32 %add5, i32* %ref_mi_col, align 4, !tbaa !6
  %12 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  store i32 0, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %13 = load i32, i32* %plane, align 4, !tbaa !6
  %14 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp6 = icmp slt i32 %13, %14
  br i1 %cmp6, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %15 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %16 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %17, i32 0, i32 4
  %18 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane8, i32 0, i32 %18
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %19 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %19, i32 0, i32 6
  %20 = load i8, i8* %ref_bsize, align 1, !tbaa !9
  %21 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_buf = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %21, i32 0, i32 1
  %22 = load i8**, i8*** %tmp_buf, align 4, !tbaa !139
  %23 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  %25 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_width = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %25, i32 0, i32 2
  %26 = load i32*, i32** %tmp_width, align 4, !tbaa !141
  %27 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds i32, i32* %26, i32 %27
  %28 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %29 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_height = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %29, i32 0, i32 3
  %30 = load i32*, i32** %tmp_height, align 4, !tbaa !142
  %31 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds i32, i32* %30, i32 %31
  %32 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %33 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_stride = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %33, i32 0, i32 4
  %34 = load i32*, i32** %tmp_stride, align 4, !tbaa !143
  %35 = load i32, i32* %plane, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds i32, i32* %34, i32 %35
  %36 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %37 = load i32, i32* %mi_row_offset.addr, align 4, !tbaa !6
  %38 = load i32, i32* %mi_col_offset.addr, align 4, !tbaa !6
  %39 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %39, i32 0, i32 4
  %40 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  %41 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %41, i32 0, i32 5
  %42 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  call void @setup_pred_plane(%struct.buf_2d* %dst, i8 zeroext %20, i8* %24, i32 %28, i32 %32, i32 %36, i32 %37, i32 %38, %struct.scale_factors* null, i32 %40, i32 %42)
  %43 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %plane, align 4, !tbaa !6
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %frame) #7
  %45 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %ref_mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %45, i32 0, i32 12
  %arrayidx13 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %46 = load i8, i8* %arrayidx13, align 4, !tbaa !9
  store i8 %46, i8* %frame, align 1, !tbaa !9
  %47 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #7
  %48 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %cm = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %48, i32 0, i32 0
  %49 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !144
  %50 = load i8, i8* %frame, align 1, !tbaa !9
  %call = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %49, i8 signext %50)
  store %struct.RefCntBuffer* %call, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %51 = bitcast %struct.scale_factors** %sf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #7
  %52 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %cm14 = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %52, i32 0, i32 0
  %53 = load %struct.AV1Common*, %struct.AV1Common** %cm14, align 4, !tbaa !144
  %54 = load i8, i8* %frame, align 1, !tbaa !9
  %call15 = call %struct.scale_factors* @get_ref_scale_factors_const(%struct.AV1Common* %53, i8 signext %54)
  store %struct.scale_factors* %call15, %struct.scale_factors** %sf, align 4, !tbaa !2
  %55 = load %struct.scale_factors*, %struct.scale_factors** %sf, align 4, !tbaa !2
  %56 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %block_ref_scale_factors = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %56, i32 0, i32 21
  %arrayidx16 = getelementptr inbounds [2 x %struct.scale_factors*], [2 x %struct.scale_factors*]* %block_ref_scale_factors, i32 0, i32 0
  store %struct.scale_factors* %55, %struct.scale_factors** %arrayidx16, align 4, !tbaa !2
  %57 = load %struct.scale_factors*, %struct.scale_factors** %sf, align 4, !tbaa !2
  %call17 = call i32 @av1_is_valid_scale(%struct.scale_factors* %57)
  %tobool = icmp ne i32 %call17, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.end
  %58 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %error_info = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %58, i32 0, i32 46
  %59 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info, align 16, !tbaa !145
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %59, i32 5, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %60 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %61 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %61, i32 0, i32 17
  %62 = load i32, i32* %ref_mi_row, align 4, !tbaa !6
  %63 = load i32, i32* %ref_mi_col, align 4, !tbaa !6
  %64 = load %struct.scale_factors*, %struct.scale_factors** %sf, align 4, !tbaa !2
  %65 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  call void @av1_setup_pre_planes(%struct.macroblockd* %60, i32 0, %struct.yv12_buffer_config* %buf, i32 %62, i32 %63, %struct.scale_factors* %64, i32 %65)
  %66 = bitcast %struct.scale_factors** %sf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #7
  %67 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %frame) #7
  %68 = bitcast i32* %ref_mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #7
  %69 = bitcast i32* %ref_mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref_bsize) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define internal %struct.scale_factors* @get_ref_scale_factors_const(%struct.AV1Common* %cm, i8 signext %ref_frame) #1 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  %map_idx = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !9
  %0 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %2 = load i8, i8* %ref_frame.addr, align 1, !tbaa !9
  %call = call i32 @get_ref_frame_map_idx(%struct.AV1Common* %1, i8 signext %2)
  store i32 %call, i32* %map_idx, align 4, !tbaa !6
  %3 = load i32, i32* %map_idx, align 4, !tbaa !6
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_scale_factors = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %4, i32 0, i32 16
  %5 = load i32, i32* %map_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [8 x %struct.scale_factors], [8 x %struct.scale_factors]* %ref_scale_factors, i32 0, i32 %5
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.scale_factors* [ %arrayidx, %cond.true ], [ null, %cond.false ]
  %6 = bitcast i32* %map_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  ret %struct.scale_factors* %cond
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_valid_scale(%struct.scale_factors* %sf) #1 {
entry:
  %sf.addr = alloca %struct.scale_factors*, align 4
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %x_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %0, i32 0, i32 0
  %1 = load i32, i32* %x_scale_fp, align 4, !tbaa !22
  %cmp = icmp ne i32 %1, -1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %y_scale_fp = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %2, i32 0, i32 1
  %3 = load i32, i32* %y_scale_fp, align 4, !tbaa !24
  %cmp1 = icmp ne i32 %3, -1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %4 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  %land.ext = zext i1 %4 to i32
  ret i32 %land.ext
}

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #3

; Function Attrs: nounwind
define hidden void @av1_setup_build_prediction_by_above_pred(%struct.macroblockd* %xd, i32 %rel_mi_col, i8 zeroext %above_mi_width, %struct.MB_MODE_INFO* %above_mbmi, %struct.build_prediction_ctxt* %ctxt, i32 %num_planes) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %rel_mi_col.addr = alloca i32, align 4
  %above_mi_width.addr = alloca i8, align 1
  %above_mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %ctxt.addr = alloca %struct.build_prediction_ctxt*, align 4
  %num_planes.addr = alloca i32, align 4
  %a_bsize = alloca i8, align 1
  %above_mi_col = alloca i32, align 4
  %j = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %num_refs = alloca i32, align 4
  %ref = alloca i32, align 4
  %frame = alloca i8, align 1
  %ref_buf = alloca %struct.RefCntBuffer*, align 4
  %sf = alloca %struct.scale_factors*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %rel_mi_col, i32* %rel_mi_col.addr, align 4, !tbaa !6
  store i8 %above_mi_width, i8* %above_mi_width.addr, align 1, !tbaa !9
  store %struct.MB_MODE_INFO* %above_mbmi, %struct.MB_MODE_INFO** %above_mbmi.addr, align 4, !tbaa !2
  store %struct.build_prediction_ctxt* %ctxt, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %a_bsize) #7
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 6
  %1 = load i8, i8* %sb_type, align 2, !tbaa !74
  %conv = zext i8 %1 to i32
  %cmp = icmp sgt i32 3, %conv
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi.addr, align 4, !tbaa !2
  %sb_type2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 6
  %3 = load i8, i8* %sb_type2, align 2, !tbaa !74
  %conv3 = zext i8 %3 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 3, %cond.true ], [ %conv3, %cond.false ]
  %conv4 = trunc i32 %cond to i8
  store i8 %conv4, i8* %a_bsize, align 1, !tbaa !9
  %4 = bitcast i32* %above_mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 1
  %6 = load i32, i32* %mi_col, align 4, !tbaa !127
  %7 = load i32, i32* %rel_mi_col.addr, align 4, !tbaa !6
  %add = add nsw i32 %6, %7
  store i32 %add, i32* %above_mi_col, align 4, !tbaa !6
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi.addr, align 4, !tbaa !2
  call void @av1_modify_neighbor_predictor_for_obmc(%struct.MB_MODE_INFO* %8)
  %9 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %11 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %10, %11
  br i1 %cmp5, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %14, i32 0, i32 4
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 %15
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %16 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %16, i32 0, i32 6
  %17 = load i8, i8* %a_bsize, align 1, !tbaa !9
  %18 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_buf = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %18, i32 0, i32 1
  %19 = load i8**, i8*** %tmp_buf, align 4, !tbaa !139
  %20 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds i8*, i8** %19, i32 %20
  %21 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  %22 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_width = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %22, i32 0, i32 2
  %23 = load i32*, i32** %tmp_width, align 4, !tbaa !141
  %24 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds i32, i32* %23, i32 %24
  %25 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %26 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_height = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %26, i32 0, i32 3
  %27 = load i32*, i32** %tmp_height, align 4, !tbaa !142
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds i32, i32* %27, i32 %28
  %29 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %30 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_stride = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %30, i32 0, i32 4
  %31 = load i32*, i32** %tmp_stride, align 4, !tbaa !143
  %32 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds i32, i32* %31, i32 %32
  %33 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %34 = load i32, i32* %rel_mi_col.addr, align 4, !tbaa !6
  %35 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %35, i32 0, i32 4
  %36 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  %37 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %37, i32 0, i32 5
  %38 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  call void @setup_pred_plane(%struct.buf_2d* %dst, i8 zeroext %17, i8* %21, i32 %25, i32 %29, i32 %33, i32 0, i32 %34, %struct.scale_factors* null, i32 %36, i32 %38)
  %39 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %40 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %41 = bitcast i32* %num_refs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  %42 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi.addr, align 4, !tbaa !2
  %call = call i32 @has_second_ref(%struct.MB_MODE_INFO* %42)
  %add11 = add nsw i32 1, %call
  store i32 %add11, i32* %num_refs, align 4, !tbaa !6
  %43 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  store i32 0, i32* %ref, align 4, !tbaa !6
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc23, %for.end
  %44 = load i32, i32* %ref, align 4, !tbaa !6
  %45 = load i32, i32* %num_refs, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %44, %45
  br i1 %cmp13, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond12
  %46 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #7
  br label %for.end25

for.body16:                                       ; preds = %for.cond12
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %frame) #7
  %47 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %47, i32 0, i32 12
  %48 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 %48
  %49 = load i8, i8* %arrayidx17, align 1, !tbaa !9
  store i8 %49, i8* %frame, align 1, !tbaa !9
  %50 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %cm = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %51, i32 0, i32 0
  %52 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !144
  %53 = load i8, i8* %frame, align 1, !tbaa !9
  %call18 = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %52, i8 signext %53)
  store %struct.RefCntBuffer* %call18, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %54 = bitcast %struct.scale_factors** %sf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  %55 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %cm19 = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %55, i32 0, i32 0
  %56 = load %struct.AV1Common*, %struct.AV1Common** %cm19, align 4, !tbaa !144
  %57 = load i8, i8* %frame, align 1, !tbaa !9
  %call20 = call %struct.scale_factors* @get_ref_scale_factors_const(%struct.AV1Common* %56, i8 signext %57)
  store %struct.scale_factors* %call20, %struct.scale_factors** %sf, align 4, !tbaa !2
  %58 = load %struct.scale_factors*, %struct.scale_factors** %sf, align 4, !tbaa !2
  %59 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %block_ref_scale_factors = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %59, i32 0, i32 21
  %60 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [2 x %struct.scale_factors*], [2 x %struct.scale_factors*]* %block_ref_scale_factors, i32 0, i32 %60
  store %struct.scale_factors* %58, %struct.scale_factors** %arrayidx21, align 4, !tbaa !2
  %61 = load %struct.scale_factors*, %struct.scale_factors** %sf, align 4, !tbaa !2
  %call22 = call i32 @av1_is_valid_scale(%struct.scale_factors* %61)
  %tobool = icmp ne i32 %call22, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body16
  %62 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %error_info = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %62, i32 0, i32 46
  %63 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info, align 16, !tbaa !145
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %63, i32 5, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body16
  %64 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %65 = load i32, i32* %ref, align 4, !tbaa !6
  %66 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %66, i32 0, i32 17
  %67 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %67, i32 0, i32 0
  %68 = load i32, i32* %mi_row, align 16, !tbaa !131
  %69 = load i32, i32* %above_mi_col, align 4, !tbaa !6
  %70 = load %struct.scale_factors*, %struct.scale_factors** %sf, align 4, !tbaa !2
  %71 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  call void @av1_setup_pre_planes(%struct.macroblockd* %64, i32 %65, %struct.yv12_buffer_config* %buf, i32 %68, i32 %69, %struct.scale_factors* %70, i32 %71)
  %72 = bitcast %struct.scale_factors** %sf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  %73 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %frame) #7
  br label %for.inc23

for.inc23:                                        ; preds = %if.end
  %74 = load i32, i32* %ref, align 4, !tbaa !6
  %inc24 = add nsw i32 %74, 1
  store i32 %inc24, i32* %ref, align 4, !tbaa !6
  br label %for.cond12

for.end25:                                        ; preds = %for.cond.cleanup15
  %75 = load i32, i32* %above_mi_col, align 4, !tbaa !6
  %sub = sub nsw i32 0, %75
  %mul = mul nsw i32 32, %sub
  %76 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_left_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %76, i32 0, i32 17
  store i32 %mul, i32* %mb_to_left_edge, align 4, !tbaa !146
  %77 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %mb_to_far_edge = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %77, i32 0, i32 5
  %78 = load i32, i32* %mb_to_far_edge, align 4, !tbaa !147
  %79 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %79, i32 0, i32 33
  %80 = load i8, i8* %width, align 4, !tbaa !128
  %conv26 = zext i8 %80 to i32
  %81 = load i32, i32* %rel_mi_col.addr, align 4, !tbaa !6
  %sub27 = sub nsw i32 %conv26, %81
  %82 = load i8, i8* %above_mi_width.addr, align 1, !tbaa !9
  %conv28 = zext i8 %82 to i32
  %sub29 = sub nsw i32 %sub27, %conv28
  %mul30 = mul nsw i32 %sub29, 4
  %mul31 = mul nsw i32 %mul30, 8
  %add32 = add nsw i32 %78, %mul31
  %83 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %83, i32 0, i32 18
  store i32 %add32, i32* %mb_to_right_edge, align 8, !tbaa !148
  %84 = bitcast i32* %num_refs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #7
  %85 = bitcast i32* %above_mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %a_bsize) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @has_second_ref(%struct.MB_MODE_INFO* %mbmi) #1 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 1
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = sext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 0
  %conv1 = zext i1 %cmp to i32
  ret i32 %conv1
}

; Function Attrs: nounwind
define hidden void @av1_setup_build_prediction_by_left_pred(%struct.macroblockd* %xd, i32 %rel_mi_row, i8 zeroext %left_mi_height, %struct.MB_MODE_INFO* %left_mbmi, %struct.build_prediction_ctxt* %ctxt, i32 %num_planes) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %rel_mi_row.addr = alloca i32, align 4
  %left_mi_height.addr = alloca i8, align 1
  %left_mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %ctxt.addr = alloca %struct.build_prediction_ctxt*, align 4
  %num_planes.addr = alloca i32, align 4
  %l_bsize = alloca i8, align 1
  %left_mi_row = alloca i32, align 4
  %j = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %num_refs = alloca i32, align 4
  %ref = alloca i32, align 4
  %frame = alloca i8, align 1
  %ref_buf = alloca %struct.RefCntBuffer*, align 4
  %ref_scale_factors = alloca %struct.scale_factors*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %rel_mi_row, i32* %rel_mi_row.addr, align 4, !tbaa !6
  store i8 %left_mi_height, i8* %left_mi_height.addr, align 1, !tbaa !9
  store %struct.MB_MODE_INFO* %left_mbmi, %struct.MB_MODE_INFO** %left_mbmi.addr, align 4, !tbaa !2
  store %struct.build_prediction_ctxt* %ctxt, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %l_bsize) #7
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 6
  %1 = load i8, i8* %sb_type, align 2, !tbaa !74
  %conv = zext i8 %1 to i32
  %cmp = icmp sgt i32 3, %conv
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi.addr, align 4, !tbaa !2
  %sb_type2 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 6
  %3 = load i8, i8* %sb_type2, align 2, !tbaa !74
  %conv3 = zext i8 %3 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 3, %cond.true ], [ %conv3, %cond.false ]
  %conv4 = trunc i32 %cond to i8
  store i8 %conv4, i8* %l_bsize, align 1, !tbaa !9
  %4 = bitcast i32* %left_mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_row = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 0
  %6 = load i32, i32* %mi_row, align 16, !tbaa !131
  %7 = load i32, i32* %rel_mi_row.addr, align 4, !tbaa !6
  %add = add nsw i32 %6, %7
  store i32 %add, i32* %left_mi_row, align 4, !tbaa !6
  %8 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi.addr, align 4, !tbaa !2
  call void @av1_modify_neighbor_predictor_for_obmc(%struct.MB_MODE_INFO* %8)
  %9 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %11 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %10, %11
  br i1 %cmp5, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %14, i32 0, i32 4
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 %15
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %16 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %16, i32 0, i32 6
  %17 = load i8, i8* %l_bsize, align 1, !tbaa !9
  %18 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_buf = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %18, i32 0, i32 1
  %19 = load i8**, i8*** %tmp_buf, align 4, !tbaa !139
  %20 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds i8*, i8** %19, i32 %20
  %21 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  %22 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_width = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %22, i32 0, i32 2
  %23 = load i32*, i32** %tmp_width, align 4, !tbaa !141
  %24 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds i32, i32* %23, i32 %24
  %25 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %26 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_height = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %26, i32 0, i32 3
  %27 = load i32*, i32** %tmp_height, align 4, !tbaa !142
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds i32, i32* %27, i32 %28
  %29 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %30 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %tmp_stride = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %30, i32 0, i32 4
  %31 = load i32*, i32** %tmp_stride, align 4, !tbaa !143
  %32 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds i32, i32* %31, i32 %32
  %33 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %34 = load i32, i32* %rel_mi_row.addr, align 4, !tbaa !6
  %35 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %35, i32 0, i32 4
  %36 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  %37 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %37, i32 0, i32 5
  %38 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  call void @setup_pred_plane(%struct.buf_2d* %dst, i8 zeroext %17, i8* %21, i32 %25, i32 %29, i32 %33, i32 %34, i32 0, %struct.scale_factors* null, i32 %36, i32 %38)
  %39 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %40 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %41 = bitcast i32* %num_refs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #7
  %42 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi.addr, align 4, !tbaa !2
  %call = call i32 @has_second_ref(%struct.MB_MODE_INFO* %42)
  %add11 = add nsw i32 1, %call
  store i32 %add11, i32* %num_refs, align 4, !tbaa !6
  %43 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  store i32 0, i32* %ref, align 4, !tbaa !6
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc23, %for.end
  %44 = load i32, i32* %ref, align 4, !tbaa !6
  %45 = load i32, i32* %num_refs, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %44, %45
  br i1 %cmp13, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond12
  %46 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #7
  br label %for.end25

for.body16:                                       ; preds = %for.cond12
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %frame) #7
  %47 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %47, i32 0, i32 12
  %48 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 %48
  %49 = load i8, i8* %arrayidx17, align 1, !tbaa !9
  store i8 %49, i8* %frame, align 1, !tbaa !9
  %50 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %cm = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %51, i32 0, i32 0
  %52 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !144
  %53 = load i8, i8* %frame, align 1, !tbaa !9
  %call18 = call %struct.RefCntBuffer* @get_ref_frame_buf(%struct.AV1Common* %52, i8 signext %53)
  store %struct.RefCntBuffer* %call18, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %54 = bitcast %struct.scale_factors** %ref_scale_factors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  %55 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %cm19 = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %55, i32 0, i32 0
  %56 = load %struct.AV1Common*, %struct.AV1Common** %cm19, align 4, !tbaa !144
  %57 = load i8, i8* %frame, align 1, !tbaa !9
  %call20 = call %struct.scale_factors* @get_ref_scale_factors_const(%struct.AV1Common* %56, i8 signext %57)
  store %struct.scale_factors* %call20, %struct.scale_factors** %ref_scale_factors, align 4, !tbaa !2
  %58 = load %struct.scale_factors*, %struct.scale_factors** %ref_scale_factors, align 4, !tbaa !2
  %59 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %block_ref_scale_factors = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %59, i32 0, i32 21
  %60 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [2 x %struct.scale_factors*], [2 x %struct.scale_factors*]* %block_ref_scale_factors, i32 0, i32 %60
  store %struct.scale_factors* %58, %struct.scale_factors** %arrayidx21, align 4, !tbaa !2
  %61 = load %struct.scale_factors*, %struct.scale_factors** %ref_scale_factors, align 4, !tbaa !2
  %call22 = call i32 @av1_is_valid_scale(%struct.scale_factors* %61)
  %tobool = icmp ne i32 %call22, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body16
  %62 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %error_info = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %62, i32 0, i32 46
  %63 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info, align 16, !tbaa !145
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %63, i32 5, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body16
  %64 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %65 = load i32, i32* %ref, align 4, !tbaa !6
  %66 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %ref_buf, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %66, i32 0, i32 17
  %67 = load i32, i32* %left_mi_row, align 4, !tbaa !6
  %68 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi_col = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %68, i32 0, i32 1
  %69 = load i32, i32* %mi_col, align 4, !tbaa !127
  %70 = load %struct.scale_factors*, %struct.scale_factors** %ref_scale_factors, align 4, !tbaa !2
  %71 = load i32, i32* %num_planes.addr, align 4, !tbaa !6
  call void @av1_setup_pre_planes(%struct.macroblockd* %64, i32 %65, %struct.yv12_buffer_config* %buf, i32 %67, i32 %69, %struct.scale_factors* %70, i32 %71)
  %72 = bitcast %struct.scale_factors** %ref_scale_factors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  %73 = bitcast %struct.RefCntBuffer** %ref_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %frame) #7
  br label %for.inc23

for.inc23:                                        ; preds = %if.end
  %74 = load i32, i32* %ref, align 4, !tbaa !6
  %inc24 = add nsw i32 %74, 1
  store i32 %inc24, i32* %ref, align 4, !tbaa !6
  br label %for.cond12

for.end25:                                        ; preds = %for.cond.cleanup15
  %75 = load i32, i32* %left_mi_row, align 4, !tbaa !6
  %sub = sub nsw i32 0, %75
  %mul = mul nsw i32 4, %sub
  %mul26 = mul nsw i32 %mul, 8
  %76 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_top_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %76, i32 0, i32 19
  store i32 %mul26, i32* %mb_to_top_edge, align 4, !tbaa !149
  %77 = load %struct.build_prediction_ctxt*, %struct.build_prediction_ctxt** %ctxt.addr, align 4, !tbaa !2
  %mb_to_far_edge = getelementptr inbounds %struct.build_prediction_ctxt, %struct.build_prediction_ctxt* %77, i32 0, i32 5
  %78 = load i32, i32* %mb_to_far_edge, align 4, !tbaa !147
  %79 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %79, i32 0, i32 34
  %80 = load i8, i8* %height, align 1, !tbaa !132
  %conv27 = zext i8 %80 to i32
  %81 = load i32, i32* %rel_mi_row.addr, align 4, !tbaa !6
  %sub28 = sub nsw i32 %conv27, %81
  %82 = load i8, i8* %left_mi_height.addr, align 1, !tbaa !9
  %conv29 = zext i8 %82 to i32
  %sub30 = sub nsw i32 %sub28, %conv29
  %mul31 = mul nsw i32 %sub30, 4
  %mul32 = mul nsw i32 %mul31, 8
  %add33 = add nsw i32 %78, %mul32
  %83 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %83, i32 0, i32 20
  store i32 %add33, i32* %mb_to_bottom_edge, align 16, !tbaa !150
  %84 = bitcast i32* %num_refs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #7
  %85 = bitcast i32* %left_mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %l_bsize) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_build_intra_predictors_for_interintra(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i8 zeroext %bsize, i32 %plane, %struct.BUFFER_SET* %ctx, i8* %dst, i32 %dst_stride) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bsize.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %ctx.addr = alloca %struct.BUFFER_SET*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %ssx = alloca i32, align 4
  %ssy = alloca i32, align 4
  %plane_bsize = alloca i8, align 1
  %mode = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store %struct.BUFFER_SET* %ctx, %struct.BUFFER_SET** %ctx.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  %0 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 4
  %2 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane1, i32 0, i32 %2
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %3 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 4
  %5 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane2, i32 0, i32 %5
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx3, i32 0, i32 4
  %6 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  store i32 %6, i32* %ssx, align 4, !tbaa !6
  %7 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 4
  %9 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane4, i32 0, i32 %9
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx5, i32 0, i32 5
  %10 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  store i32 %10, i32* %ssy, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %plane_bsize) #7
  %11 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %12 = load i32, i32* %ssx, align 4, !tbaa !6
  %13 = load i32, i32* %ssy, align 4, !tbaa !6
  %call = call zeroext i8 @get_plane_block_size(i8 zeroext %11, i32 %12, i32 %13)
  store i8 %call, i8* %plane_bsize, align 1, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mode) #7
  %14 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %14, i32 0, i32 6
  %15 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %arrayidx6 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %15, i32 0
  %16 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx6, align 4, !tbaa !2
  %interintra_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %16, i32 0, i32 9
  %17 = load i8, i8* %interintra_mode, align 1, !tbaa !151
  %idxprom = zext i8 %17 to i32
  %arrayidx7 = getelementptr inbounds [4 x i8], [4 x i8]* @interintra_to_intra_mode, i32 0, i32 %idxprom
  %18 = load i8, i8* %arrayidx7, align 1, !tbaa !9
  store i8 %18, i8* %mode, align 1, !tbaa !9
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %20 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %21 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %21, i32 0, i32 12
  %22 = load i8, i8* %width, align 4, !tbaa !152
  %conv = zext i8 %22 to i32
  %23 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %23, i32 0, i32 13
  %24 = load i8, i8* %height, align 1, !tbaa !153
  %conv8 = zext i8 %24 to i32
  %25 = load i8, i8* %plane_bsize, align 1, !tbaa !9
  %idxprom9 = zext i8 %25 to i32
  %arrayidx10 = getelementptr inbounds [22 x i8], [22 x i8]* @max_txsize_rect_lookup, i32 0, i32 %idxprom9
  %26 = load i8, i8* %arrayidx10, align 1, !tbaa !9
  %27 = load i8, i8* %mode, align 1, !tbaa !9
  %28 = load %struct.BUFFER_SET*, %struct.BUFFER_SET** %ctx.addr, align 4, !tbaa !2
  %plane11 = getelementptr inbounds %struct.BUFFER_SET, %struct.BUFFER_SET* %28, i32 0, i32 0
  %29 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [3 x i8*], [3 x i8*]* %plane11, i32 0, i32 %29
  %30 = load i8*, i8** %arrayidx12, align 4, !tbaa !2
  %31 = load %struct.BUFFER_SET*, %struct.BUFFER_SET** %ctx.addr, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.BUFFER_SET, %struct.BUFFER_SET* %31, i32 0, i32 1
  %32 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 %32
  %33 = load i32, i32* %arrayidx13, align 4, !tbaa !6
  %34 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %35 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %36 = load i32, i32* %plane.addr, align 4, !tbaa !6
  call void @av1_predict_intra_block(%struct.AV1Common* %19, %struct.macroblockd* %20, i32 %conv, i32 %conv8, i8 zeroext %26, i8 zeroext %27, i32 0, i32 0, i8 zeroext 5, i8* %30, i32 %33, i8* %34, i32 %35, i32 0, i32 0, i32 %36)
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mode) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %plane_bsize) #7
  %37 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  ret void
}

declare void @av1_predict_intra_block(%struct.AV1Common*, %struct.macroblockd*, i32, i32, i8 zeroext, i8 zeroext, i32, i32, i8 zeroext, i8*, i32, i8*, i32, i32, i32, i32) #3

; Function Attrs: nounwind
define hidden void @av1_combine_interintra(%struct.macroblockd* %xd, i8 zeroext %bsize, i32 %plane, i8* %inter_pred, i32 %inter_stride, i8* %intra_pred, i32 %intra_stride) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bsize.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %inter_pred.addr = alloca i8*, align 4
  %inter_stride.addr = alloca i32, align 4
  %intra_pred.addr = alloca i8*, align 4
  %intra_stride.addr = alloca i32, align 4
  %ssx = alloca i32, align 4
  %ssy = alloca i32, align 4
  %plane_bsize = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i8* %inter_pred, i8** %inter_pred.addr, align 4, !tbaa !2
  store i32 %inter_stride, i32* %inter_stride.addr, align 4, !tbaa !6
  store i8* %intra_pred, i8** %intra_pred.addr, align 4, !tbaa !2
  store i32 %intra_stride, i32* %intra_stride.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %1, i32 0, i32 4
  %2 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane1, i32 0, i32 %2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx, i32 0, i32 4
  %3 = load i32, i32* %subsampling_x, align 4, !tbaa !75
  store i32 %3, i32* %ssx, align 4, !tbaa !6
  %4 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %5, i32 0, i32 4
  %6 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane2, i32 0, i32 %6
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx3, i32 0, i32 5
  %7 = load i32, i32* %subsampling_y, align 4, !tbaa !77
  store i32 %7, i32* %ssy, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %plane_bsize) #7
  %8 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %9 = load i32, i32* %ssx, align 4, !tbaa !6
  %10 = load i32, i32* %ssy, align 4, !tbaa !6
  %call = call zeroext i8 @get_plane_block_size(i8 zeroext %8, i32 %9, i32 %10)
  store i8 %call, i8* %plane_bsize, align 1, !tbaa !9
  %11 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call4 = call i32 @is_cur_buf_hbd(%struct.macroblockd* %11)
  %tobool = icmp ne i32 %call4, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %12, i32 0, i32 6
  %13 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi, align 4, !tbaa !78
  %arrayidx5 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %13, i32 0
  %14 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx5, align 4, !tbaa !2
  %interintra_mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %14, i32 0, i32 9
  %15 = load i8, i8* %interintra_mode, align 1, !tbaa !151
  %16 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi6 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %16, i32 0, i32 6
  %17 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi6, align 4, !tbaa !78
  %arrayidx7 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %17, i32 0
  %18 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx7, align 4, !tbaa !2
  %use_wedge_interintra = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %18, i32 0, i32 26
  %bf.load = load i8, i8* %use_wedge_interintra, align 4
  %bf.clear = and i8 %bf.load, 1
  %19 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %19, i32 0, i32 6
  %20 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi8, align 4, !tbaa !78
  %arrayidx9 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %20, i32 0
  %21 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx9, align 4, !tbaa !2
  %interintra_wedge_index = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %21, i32 0, i32 19
  %22 = load i8, i8* %interintra_wedge_index, align 1, !tbaa !154
  %23 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %24 = load i8, i8* %plane_bsize, align 1, !tbaa !9
  %25 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane10 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %25, i32 0, i32 4
  %26 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane10, i32 0, i32 %26
  %dst = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx11, i32 0, i32 6
  %buf = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst, i32 0, i32 0
  %27 = load i8*, i8** %buf, align 4, !tbaa !138
  %28 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane12 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %28, i32 0, i32 4
  %29 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane12, i32 0, i32 %29
  %dst14 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx13, i32 0, i32 6
  %stride = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst14, i32 0, i32 4
  %30 = load i32, i32* %stride, align 4, !tbaa !137
  %31 = load i8*, i8** %inter_pred.addr, align 4, !tbaa !2
  %32 = load i32, i32* %inter_stride.addr, align 4, !tbaa !6
  %33 = load i8*, i8** %intra_pred.addr, align 4, !tbaa !2
  %34 = load i32, i32* %intra_stride.addr, align 4, !tbaa !6
  %35 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %bd = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %35, i32 0, i32 41
  %36 = load i32, i32* %bd, align 4, !tbaa !88
  call void @combine_interintra_highbd(i8 zeroext %15, i8 signext %bf.clear, i8 signext %22, i8 signext 0, i8 zeroext %23, i8 zeroext %24, i8* %27, i32 %30, i8* %31, i32 %32, i8* %33, i32 %34, i32 %36)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %37 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi15 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %37, i32 0, i32 6
  %38 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi15, align 4, !tbaa !78
  %arrayidx16 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %38, i32 0
  %39 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx16, align 4, !tbaa !2
  %interintra_mode17 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %39, i32 0, i32 9
  %40 = load i8, i8* %interintra_mode17, align 1, !tbaa !151
  %41 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi18 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %41, i32 0, i32 6
  %42 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi18, align 4, !tbaa !78
  %arrayidx19 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %42, i32 0
  %43 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx19, align 4, !tbaa !2
  %use_wedge_interintra20 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %43, i32 0, i32 26
  %bf.load21 = load i8, i8* %use_wedge_interintra20, align 4
  %bf.clear22 = and i8 %bf.load21, 1
  %44 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mi23 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %44, i32 0, i32 6
  %45 = load %struct.MB_MODE_INFO**, %struct.MB_MODE_INFO*** %mi23, align 4, !tbaa !78
  %arrayidx24 = getelementptr inbounds %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %45, i32 0
  %46 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %arrayidx24, align 4, !tbaa !2
  %interintra_wedge_index25 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %46, i32 0, i32 19
  %47 = load i8, i8* %interintra_wedge_index25, align 1, !tbaa !154
  %48 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %49 = load i8, i8* %plane_bsize, align 1, !tbaa !9
  %50 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane26 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %50, i32 0, i32 4
  %51 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane26, i32 0, i32 %51
  %dst28 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx27, i32 0, i32 6
  %buf29 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst28, i32 0, i32 0
  %52 = load i8*, i8** %buf29, align 4, !tbaa !138
  %53 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane30 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %53, i32 0, i32 4
  %54 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane30, i32 0, i32 %54
  %dst32 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx31, i32 0, i32 6
  %stride33 = getelementptr inbounds %struct.buf_2d, %struct.buf_2d* %dst32, i32 0, i32 4
  %55 = load i32, i32* %stride33, align 4, !tbaa !137
  %56 = load i8*, i8** %inter_pred.addr, align 4, !tbaa !2
  %57 = load i32, i32* %inter_stride.addr, align 4, !tbaa !6
  %58 = load i8*, i8** %intra_pred.addr, align 4, !tbaa !2
  %59 = load i32, i32* %intra_stride.addr, align 4, !tbaa !6
  call void @combine_interintra(i8 zeroext %40, i8 signext %bf.clear22, i8 signext %47, i8 signext 0, i8 zeroext %48, i8 zeroext %49, i8* %52, i32 %55, i8* %56, i32 %57, i8* %58, i32 %59)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %plane_bsize) #7
  %60 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #7
  %61 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_cur_buf_hbd(%struct.macroblockd* %xd) #1 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %cur_buf = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 22
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %cur_buf, align 4, !tbaa !155
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 26
  %2 = load i32, i32* %flags, align 4, !tbaa !156
  %and = and i32 %2, 8
  %tobool = icmp ne i32 %and, 0
  %3 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal void @combine_interintra_highbd(i8 zeroext %mode, i8 signext %use_wedge_interintra, i8 signext %wedge_index, i8 signext %wedge_sign, i8 zeroext %bsize, i8 zeroext %plane_bsize, i8* %comppred8, i32 %compstride, i8* %interpred8, i32 %interstride, i8* %intrapred8, i32 %intrastride, i32 %bd) #1 {
entry:
  %mode.addr = alloca i8, align 1
  %use_wedge_interintra.addr = alloca i8, align 1
  %wedge_index.addr = alloca i8, align 1
  %wedge_sign.addr = alloca i8, align 1
  %bsize.addr = alloca i8, align 1
  %plane_bsize.addr = alloca i8, align 1
  %comppred8.addr = alloca i8*, align 4
  %compstride.addr = alloca i32, align 4
  %interpred8.addr = alloca i8*, align 4
  %interstride.addr = alloca i32, align 4
  %intrapred8.addr = alloca i8*, align 4
  %intrastride.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %mask = alloca i8*, align 4
  %subh = alloca i32, align 4
  %subw = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %mask21 = alloca [16384 x i8], align 16
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !9
  store i8 %use_wedge_interintra, i8* %use_wedge_interintra.addr, align 1, !tbaa !9
  store i8 %wedge_index, i8* %wedge_index.addr, align 1, !tbaa !9
  store i8 %wedge_sign, i8* %wedge_sign.addr, align 1, !tbaa !9
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store i8 %plane_bsize, i8* %plane_bsize.addr, align 1, !tbaa !9
  store i8* %comppred8, i8** %comppred8.addr, align 4, !tbaa !2
  store i32 %compstride, i32* %compstride.addr, align 4, !tbaa !6
  store i8* %interpred8, i8** %interpred8.addr, align 4, !tbaa !2
  store i32 %interstride, i32* %interstride.addr, align 4, !tbaa !6
  store i8* %intrapred8, i8** %intrapred8.addr, align 4, !tbaa !2
  store i32 %intrastride, i32* %intrastride.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !9
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !6
  %3 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !9
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom1
  %5 = load i8, i8* %arrayidx2, align 1, !tbaa !9
  %conv3 = zext i8 %5 to i32
  store i32 %conv3, i32* %bh, align 4, !tbaa !6
  %6 = load i8, i8* %use_wedge_interintra.addr, align 1, !tbaa !9
  %tobool = icmp ne i8 %6, 0
  br i1 %tobool, label %if.then, label %if.end20

if.then:                                          ; preds = %entry
  %7 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %call = call i32 @av1_is_wedge_used(i8 zeroext %7)
  %tobool4 = icmp ne i32 %call, 0
  br i1 %tobool4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  %8 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load i8, i8* %wedge_index.addr, align 1, !tbaa !9
  %10 = load i8, i8* %wedge_sign.addr, align 1, !tbaa !9
  %11 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %call6 = call i8* @av1_get_contiguous_soft_mask(i8 signext %9, i8 signext %10, i8 zeroext %11)
  store i8* %call6, i8** %mask, align 4, !tbaa !2
  %12 = bitcast i32* %subh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom7 = zext i8 %13 to i32
  %arrayidx8 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom7
  %14 = load i8, i8* %arrayidx8, align 1, !tbaa !9
  %conv9 = zext i8 %14 to i32
  %mul = mul nsw i32 2, %conv9
  %15 = load i32, i32* %bh, align 4, !tbaa !6
  %cmp = icmp eq i32 %mul, %15
  %conv10 = zext i1 %cmp to i32
  store i32 %conv10, i32* %subh, align 4, !tbaa !6
  %16 = bitcast i32* %subw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom11 = zext i8 %17 to i32
  %arrayidx12 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom11
  %18 = load i8, i8* %arrayidx12, align 1, !tbaa !9
  %conv13 = zext i8 %18 to i32
  %mul14 = mul nsw i32 2, %conv13
  %19 = load i32, i32* %bw, align 4, !tbaa !6
  %cmp15 = icmp eq i32 %mul14, %19
  %conv16 = zext i1 %cmp15 to i32
  store i32 %conv16, i32* %subw, align 4, !tbaa !6
  %20 = load i8*, i8** %comppred8.addr, align 4, !tbaa !2
  %21 = load i32, i32* %compstride.addr, align 4, !tbaa !6
  %22 = load i8*, i8** %intrapred8.addr, align 4, !tbaa !2
  %23 = load i32, i32* %intrastride.addr, align 4, !tbaa !6
  %24 = load i8*, i8** %interpred8.addr, align 4, !tbaa !2
  %25 = load i32, i32* %interstride.addr, align 4, !tbaa !6
  %26 = load i8*, i8** %mask, align 4, !tbaa !2
  %27 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom17 = zext i8 %27 to i32
  %arrayidx18 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom17
  %28 = load i8, i8* %arrayidx18, align 1, !tbaa !9
  %conv19 = zext i8 %28 to i32
  %29 = load i32, i32* %bw, align 4, !tbaa !6
  %30 = load i32, i32* %bh, align 4, !tbaa !6
  %31 = load i32, i32* %subw, align 4, !tbaa !6
  %32 = load i32, i32* %subh, align 4, !tbaa !6
  %33 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_blend_a64_mask_c(i8* %20, i32 %21, i8* %22, i32 %23, i8* %24, i32 %25, i8* %26, i32 %conv19, i32 %29, i32 %30, i32 %31, i32 %32, i32 %33)
  %34 = bitcast i32* %subw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast i32* %subh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.then
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %entry
  %37 = bitcast [16384 x i8]* %mask21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16384, i8* %37) #7
  %arraydecay = getelementptr inbounds [16384 x i8], [16384 x i8]* %mask21, i32 0, i32 0
  %38 = load i32, i32* %bw, align 4, !tbaa !6
  %39 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !9
  %40 = load i8, i8* %mode.addr, align 1, !tbaa !9
  call void @build_smooth_interintra_mask(i8* %arraydecay, i32 %38, i8 zeroext %39, i8 zeroext %40)
  %41 = load i8*, i8** %comppred8.addr, align 4, !tbaa !2
  %42 = load i32, i32* %compstride.addr, align 4, !tbaa !6
  %43 = load i8*, i8** %intrapred8.addr, align 4, !tbaa !2
  %44 = load i32, i32* %intrastride.addr, align 4, !tbaa !6
  %45 = load i8*, i8** %interpred8.addr, align 4, !tbaa !2
  %46 = load i32, i32* %interstride.addr, align 4, !tbaa !6
  %arraydecay22 = getelementptr inbounds [16384 x i8], [16384 x i8]* %mask21, i32 0, i32 0
  %47 = load i32, i32* %bw, align 4, !tbaa !6
  %48 = load i32, i32* %bw, align 4, !tbaa !6
  %49 = load i32, i32* %bh, align 4, !tbaa !6
  %50 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @aom_highbd_blend_a64_mask_c(i8* %41, i32 %42, i8* %43, i32 %44, i8* %45, i32 %46, i8* %arraydecay22, i32 %47, i32 %48, i32 %49, i32 0, i32 0, i32 %50)
  %51 = bitcast [16384 x i8]* %mask21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16384, i8* %51) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.end
  %52 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal void @combine_interintra(i8 zeroext %mode, i8 signext %use_wedge_interintra, i8 signext %wedge_index, i8 signext %wedge_sign, i8 zeroext %bsize, i8 zeroext %plane_bsize, i8* %comppred, i32 %compstride, i8* %interpred, i32 %interstride, i8* %intrapred, i32 %intrastride) #1 {
entry:
  %mode.addr = alloca i8, align 1
  %use_wedge_interintra.addr = alloca i8, align 1
  %wedge_index.addr = alloca i8, align 1
  %wedge_sign.addr = alloca i8, align 1
  %bsize.addr = alloca i8, align 1
  %plane_bsize.addr = alloca i8, align 1
  %comppred.addr = alloca i8*, align 4
  %compstride.addr = alloca i32, align 4
  %interpred.addr = alloca i8*, align 4
  %interstride.addr = alloca i32, align 4
  %intrapred.addr = alloca i8*, align 4
  %intrastride.addr = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %mask = alloca i8*, align 4
  %subw = alloca i32, align 4
  %subh = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %mask21 = alloca i8*, align 4
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !9
  store i8 %use_wedge_interintra, i8* %use_wedge_interintra.addr, align 1, !tbaa !9
  store i8 %wedge_index, i8* %wedge_index.addr, align 1, !tbaa !9
  store i8 %wedge_sign, i8* %wedge_sign.addr, align 1, !tbaa !9
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  store i8 %plane_bsize, i8* %plane_bsize.addr, align 1, !tbaa !9
  store i8* %comppred, i8** %comppred.addr, align 4, !tbaa !2
  store i32 %compstride, i32* %compstride.addr, align 4, !tbaa !6
  store i8* %interpred, i8** %interpred.addr, align 4, !tbaa !2
  store i32 %interstride, i32* %interstride.addr, align 4, !tbaa !6
  store i8* %intrapred, i8** %intrapred.addr, align 4, !tbaa !2
  store i32 %intrastride, i32* %intrastride.addr, align 4, !tbaa !6
  %0 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !9
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !6
  %3 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !9
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom1
  %5 = load i8, i8* %arrayidx2, align 1, !tbaa !9
  %conv3 = zext i8 %5 to i32
  store i32 %conv3, i32* %bh, align 4, !tbaa !6
  %6 = load i8, i8* %use_wedge_interintra.addr, align 1, !tbaa !9
  %tobool = icmp ne i8 %6, 0
  br i1 %tobool, label %if.then, label %if.end20

if.then:                                          ; preds = %entry
  %7 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %call = call i32 @av1_is_wedge_used(i8 zeroext %7)
  %tobool4 = icmp ne i32 %call, 0
  br i1 %tobool4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  %8 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load i8, i8* %wedge_index.addr, align 1, !tbaa !9
  %10 = load i8, i8* %wedge_sign.addr, align 1, !tbaa !9
  %11 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %call6 = call i8* @av1_get_contiguous_soft_mask(i8 signext %9, i8 signext %10, i8 zeroext %11)
  store i8* %call6, i8** %mask, align 4, !tbaa !2
  %12 = bitcast i32* %subw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom7 = zext i8 %13 to i32
  %arrayidx8 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom7
  %14 = load i8, i8* %arrayidx8, align 1, !tbaa !9
  %conv9 = zext i8 %14 to i32
  %mul = mul nsw i32 2, %conv9
  %15 = load i32, i32* %bw, align 4, !tbaa !6
  %cmp = icmp eq i32 %mul, %15
  %conv10 = zext i1 %cmp to i32
  store i32 %conv10, i32* %subw, align 4, !tbaa !6
  %16 = bitcast i32* %subh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom11 = zext i8 %17 to i32
  %arrayidx12 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom11
  %18 = load i8, i8* %arrayidx12, align 1, !tbaa !9
  %conv13 = zext i8 %18 to i32
  %mul14 = mul nsw i32 2, %conv13
  %19 = load i32, i32* %bh, align 4, !tbaa !6
  %cmp15 = icmp eq i32 %mul14, %19
  %conv16 = zext i1 %cmp15 to i32
  store i32 %conv16, i32* %subh, align 4, !tbaa !6
  %20 = load i8*, i8** %comppred.addr, align 4, !tbaa !2
  %21 = load i32, i32* %compstride.addr, align 4, !tbaa !6
  %22 = load i8*, i8** %intrapred.addr, align 4, !tbaa !2
  %23 = load i32, i32* %intrastride.addr, align 4, !tbaa !6
  %24 = load i8*, i8** %interpred.addr, align 4, !tbaa !2
  %25 = load i32, i32* %interstride.addr, align 4, !tbaa !6
  %26 = load i8*, i8** %mask, align 4, !tbaa !2
  %27 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %idxprom17 = zext i8 %27 to i32
  %arrayidx18 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom17
  %28 = load i8, i8* %arrayidx18, align 1, !tbaa !9
  %conv19 = zext i8 %28 to i32
  %29 = load i32, i32* %bw, align 4, !tbaa !6
  %30 = load i32, i32* %bh, align 4, !tbaa !6
  %31 = load i32, i32* %subw, align 4, !tbaa !6
  %32 = load i32, i32* %subh, align 4, !tbaa !6
  call void @aom_blend_a64_mask_c(i8* %20, i32 %21, i8* %22, i32 %23, i8* %24, i32 %25, i8* %26, i32 %conv19, i32 %29, i32 %30, i32 %31, i32 %32)
  %33 = bitcast i32* %subh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = bitcast i32* %subw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.then
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %entry
  %36 = bitcast i8** %mask21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  %37 = load i8, i8* %mode.addr, align 1, !tbaa !9
  %idxprom22 = zext i8 %37 to i32
  %arrayidx23 = getelementptr inbounds [4 x [22 x [1024 x i8]]], [4 x [22 x [1024 x i8]]]* @smooth_interintra_mask_buf, i32 0, i32 %idxprom22
  %38 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !9
  %idxprom24 = zext i8 %38 to i32
  %arrayidx25 = getelementptr inbounds [22 x [1024 x i8]], [22 x [1024 x i8]]* %arrayidx23, i32 0, i32 %idxprom24
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %arrayidx25, i32 0, i32 0
  store i8* %arraydecay, i8** %mask21, align 4, !tbaa !2
  %39 = load i8*, i8** %comppred.addr, align 4, !tbaa !2
  %40 = load i32, i32* %compstride.addr, align 4, !tbaa !6
  %41 = load i8*, i8** %intrapred.addr, align 4, !tbaa !2
  %42 = load i32, i32* %intrastride.addr, align 4, !tbaa !6
  %43 = load i8*, i8** %interpred.addr, align 4, !tbaa !2
  %44 = load i32, i32* %interstride.addr, align 4, !tbaa !6
  %45 = load i8*, i8** %mask21, align 4, !tbaa !2
  %46 = load i32, i32* %bw, align 4, !tbaa !6
  %47 = load i32, i32* %bw, align 4, !tbaa !6
  %48 = load i32, i32* %bh, align 4, !tbaa !6
  call void @aom_blend_a64_mask_c(i8* %39, i32 %40, i8* %41, i32 %42, i8* %43, i32 %44, i8* %45, i32 %46, i32 %47, i32 %48, i32 0, i32 0)
  %49 = bitcast i8** %mask21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.end
  %50 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  %51 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @av1_build_interintra_predictor(%struct.AV1Common* %cm, %struct.macroblockd* %xd, i8* %pred, i32 %stride, %struct.BUFFER_SET* %ctx, i32 %plane, i8 zeroext %bsize) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pred.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %ctx.addr = alloca %struct.BUFFER_SET*, align 4
  %plane.addr = alloca i32, align 4
  %bsize.addr = alloca i8, align 1
  %intrapredictor = alloca [16384 x i16], align 16
  %intrapredictor3 = alloca [16384 x i8], align 16
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8* %pred, i8** %pred.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store %struct.BUFFER_SET* %ctx, %struct.BUFFER_SET** %ctx.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !9
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %call = call i32 @is_cur_buf_hbd(%struct.macroblockd* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast [16384 x i16]* %intrapredictor to i8*
  call void @llvm.lifetime.start.p0i8(i64 32768, i8* %1) #7
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %4 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %5 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %6 = load %struct.BUFFER_SET*, %struct.BUFFER_SET** %ctx.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [16384 x i16], [16384 x i16]* %intrapredictor, i32 0, i32 0
  %7 = ptrtoint i16* %arraydecay to i32
  %shr = lshr i32 %7, 1
  %8 = inttoptr i32 %shr to i8*
  call void @av1_build_intra_predictors_for_interintra(%struct.AV1Common* %2, %struct.macroblockd* %3, i8 zeroext %4, i32 %5, %struct.BUFFER_SET* %6, i8* %8, i32 128)
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %10 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %11 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %12 = load i8*, i8** %pred.addr, align 4, !tbaa !2
  %13 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay1 = getelementptr inbounds [16384 x i16], [16384 x i16]* %intrapredictor, i32 0, i32 0
  %14 = ptrtoint i16* %arraydecay1 to i32
  %shr2 = lshr i32 %14, 1
  %15 = inttoptr i32 %shr2 to i8*
  call void @av1_combine_interintra(%struct.macroblockd* %9, i8 zeroext %10, i32 %11, i8* %12, i32 %13, i8* %15, i32 128)
  %16 = bitcast [16384 x i16]* %intrapredictor to i8*
  call void @llvm.lifetime.end.p0i8(i64 32768, i8* %16) #7
  br label %if.end

if.else:                                          ; preds = %entry
  %17 = bitcast [16384 x i8]* %intrapredictor3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16384, i8* %17) #7
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %19 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %20 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %21 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %22 = load %struct.BUFFER_SET*, %struct.BUFFER_SET** %ctx.addr, align 4, !tbaa !2
  %arraydecay4 = getelementptr inbounds [16384 x i8], [16384 x i8]* %intrapredictor3, i32 0, i32 0
  call void @av1_build_intra_predictors_for_interintra(%struct.AV1Common* %18, %struct.macroblockd* %19, i8 zeroext %20, i32 %21, %struct.BUFFER_SET* %22, i8* %arraydecay4, i32 128)
  %23 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %24 = load i8, i8* %bsize.addr, align 1, !tbaa !9
  %25 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %26 = load i8*, i8** %pred.addr, align 4, !tbaa !2
  %27 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %arraydecay5 = getelementptr inbounds [16384 x i8], [16384 x i8]* %intrapredictor3, i32 0, i32 0
  call void @av1_combine_interintra(%struct.macroblockd* %23, i8 zeroext %24, i32 %25, i8* %26, i32 %27, i8* %arraydecay5, i32 128)
  %28 = bitcast [16384 x i8]* %intrapredictor3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16384, i8* %28) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @has_scale(i32 %xs, i32 %ys) #1 {
entry:
  %xs.addr = alloca i32, align 4
  %ys.addr = alloca i32, align 4
  store i32 %xs, i32* %xs.addr, align 4, !tbaa !6
  store i32 %ys, i32* %ys.addr, align 4, !tbaa !6
  %0 = load i32, i32* %xs.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %0, 1024
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i32, i32* %ys.addr, align 4, !tbaa !6
  %cmp1 = icmp ne i32 %1, 1024
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp1, %lor.rhs ]
  %lor.ext = zext i1 %2 to i32
  ret i32 %lor.ext
}

declare void @av1_highbd_convolve_2d_facade(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams**, i32, i32, i32, i32, i32, %struct.ConvolveParams*, %struct.scale_factors*, i32) #3

; Function Attrs: inlinehint nounwind
define internal void @revert_scale_extra_bits(%struct.SubpelParams* %sp) #1 {
entry:
  %sp.addr = alloca %struct.SubpelParams*, align 4
  store %struct.SubpelParams* %sp, %struct.SubpelParams** %sp.addr, align 4, !tbaa !2
  %0 = load %struct.SubpelParams*, %struct.SubpelParams** %sp.addr, align 4, !tbaa !2
  %subpel_x = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %0, i32 0, i32 2
  %1 = load i32, i32* %subpel_x, align 4, !tbaa !59
  %shr = ashr i32 %1, 6
  store i32 %shr, i32* %subpel_x, align 4, !tbaa !59
  %2 = load %struct.SubpelParams*, %struct.SubpelParams** %sp.addr, align 4, !tbaa !2
  %subpel_y = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %2, i32 0, i32 3
  %3 = load i32, i32* %subpel_y, align 4, !tbaa !60
  %shr1 = ashr i32 %3, 6
  store i32 %shr1, i32* %subpel_y, align 4, !tbaa !60
  %4 = load %struct.SubpelParams*, %struct.SubpelParams** %sp.addr, align 4, !tbaa !2
  %xs = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %4, i32 0, i32 0
  %5 = load i32, i32* %xs, align 4, !tbaa !56
  %shr2 = ashr i32 %5, 6
  store i32 %shr2, i32* %xs, align 4, !tbaa !56
  %6 = load %struct.SubpelParams*, %struct.SubpelParams** %sp.addr, align 4, !tbaa !2
  %ys = getelementptr inbounds %struct.SubpelParams, %struct.SubpelParams* %6, i32 0, i32 1
  %7 = load i32, i32* %ys, align 4, !tbaa !58
  %shr3 = ashr i32 %7, 6
  store i32 %shr3, i32* %ys, align 4, !tbaa !58
  ret void
}

declare void @av1_convolve_2d_facade(i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams**, i32, i32, i32, i32, i32, %struct.ConvolveParams*, %struct.scale_factors*) #3

; Function Attrs: inlinehint nounwind
define internal i32 @negative_to_zero(i32 %value) #1 {
entry:
  %value.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %value.addr, align 4, !tbaa !6
  %shr = ashr i32 %1, 31
  %neg = xor i32 %shr, -1
  %and = and i32 %0, %neg
  ret i32 %and
}

; Function Attrs: inlinehint nounwind
define internal void @init_wedge_master_masks() #1 {
entry:
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %w = alloca i32, align 4
  %h = alloca i32, align 4
  %stride = alloca i32, align 4
  %shift = alloca i32, align 4
  %msk = alloca i32, align 4
  %mskx = alloca i32, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store i32 64, i32* %w, align 4, !tbaa !6
  %3 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store i32 64, i32* %h, align 4, !tbaa !6
  %4 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 64, i32* %stride, align 4, !tbaa !6
  %5 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  store i32 16, i32* %shift, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %6, 64
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul nsw i32 %7, 64
  %arrayidx = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 3), i32 0, i32 %mul
  %8 = load i32, i32* %shift, align 4, !tbaa !6
  call void @shift_copy(i8* getelementptr inbounds ([64 x i8], [64 x i8]* @wedge_master_oblique_even, i32 0, i32 0), i8* %arrayidx, i32 %8, i32 64)
  %9 = load i32, i32* %shift, align 4, !tbaa !6
  %dec = add nsw i32 %9, -1
  store i32 %dec, i32* %shift, align 4, !tbaa !6
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %10, 1
  %mul1 = mul nsw i32 %add, 64
  %arrayidx2 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 3), i32 0, i32 %mul1
  %11 = load i32, i32* %shift, align 4, !tbaa !6
  call void @shift_copy(i8* getelementptr inbounds ([64 x i8], [64 x i8]* @wedge_master_oblique_odd, i32 0, i32 0), i8* %arrayidx2, i32 %11, i32 64)
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %mul3 = mul nsw i32 %12, 64
  %arrayidx4 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 1), i32 0, i32 %mul3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arrayidx4, i8* align 16 getelementptr inbounds ([64 x i8], [64 x i8]* @wedge_master_vertical, i32 0, i32 0), i32 64, i1 false)
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %add5 = add nsw i32 %13, 1
  %mul6 = mul nsw i32 %add5, 64
  %arrayidx7 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 1), i32 0, i32 %mul6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arrayidx7, i8* align 16 getelementptr inbounds ([64 x i8], [64 x i8]* @wedge_master_vertical, i32 0, i32 0), i32 64, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %add8 = add nsw i32 %14, 2
  store i32 %add8, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc68, %for.end
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %15, 64
  br i1 %cmp10, label %for.body11, label %for.end70

for.body11:                                       ; preds = %for.cond9
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc66, %for.body11
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %16, 64
  br i1 %cmp13, label %for.body14, label %for.end67

for.body14:                                       ; preds = %for.cond12
  %17 = bitcast i32* %msk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %mul15 = mul nsw i32 %18, 64
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %add16 = add nsw i32 %mul15, %19
  %arrayidx17 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 3), i32 0, i32 %add16
  %20 = load i8, i8* %arrayidx17, align 1, !tbaa !9
  %conv = zext i8 %20 to i32
  store i32 %conv, i32* %msk, align 4, !tbaa !6
  %21 = load i32, i32* %msk, align 4, !tbaa !6
  %conv18 = trunc i32 %21 to i8
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %mul19 = mul nsw i32 %22, 64
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %add20 = add nsw i32 %mul19, %23
  %arrayidx21 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 2), i32 0, i32 %add20
  store i8 %conv18, i8* %arrayidx21, align 1, !tbaa !9
  %24 = load i32, i32* %msk, align 4, !tbaa !6
  %sub = sub nsw i32 64, %24
  %conv22 = trunc i32 %sub to i8
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %sub23 = sub nsw i32 63, %25
  %mul24 = mul nsw i32 %sub23, 64
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %add25 = add nsw i32 %mul24, %26
  %arrayidx26 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 5), i32 0, i32 %add25
  store i8 %conv22, i8* %arrayidx26, align 1, !tbaa !9
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %mul27 = mul nsw i32 %27, 64
  %add28 = add nsw i32 %mul27, 64
  %sub29 = sub nsw i32 %add28, 1
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %sub30 = sub nsw i32 %sub29, %28
  %arrayidx31 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 4), i32 0, i32 %sub30
  store i8 %conv22, i8* %arrayidx31, align 1, !tbaa !9
  %29 = load i32, i32* %msk, align 4, !tbaa !6
  %sub32 = sub nsw i32 64, %29
  %conv33 = trunc i32 %sub32 to i8
  %30 = load i32, i32* %j, align 4, !tbaa !6
  %mul34 = mul nsw i32 %30, 64
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %add35 = add nsw i32 %mul34, %31
  %arrayidx36 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 1, i32 2), i32 0, i32 %add35
  store i8 %conv33, i8* %arrayidx36, align 1, !tbaa !9
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %mul37 = mul nsw i32 %32, 64
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %add38 = add nsw i32 %mul37, %33
  %arrayidx39 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 1, i32 3), i32 0, i32 %add38
  store i8 %conv33, i8* %arrayidx39, align 1, !tbaa !9
  %34 = load i32, i32* %msk, align 4, !tbaa !6
  %conv40 = trunc i32 %34 to i8
  %35 = load i32, i32* %j, align 4, !tbaa !6
  %sub41 = sub nsw i32 63, %35
  %mul42 = mul nsw i32 %sub41, 64
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %add43 = add nsw i32 %mul42, %36
  %arrayidx44 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 1, i32 5), i32 0, i32 %add43
  store i8 %conv40, i8* %arrayidx44, align 1, !tbaa !9
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %mul45 = mul nsw i32 %37, 64
  %add46 = add nsw i32 %mul45, 64
  %sub47 = sub nsw i32 %add46, 1
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %sub48 = sub nsw i32 %sub47, %38
  %arrayidx49 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 1, i32 4), i32 0, i32 %sub48
  store i8 %conv40, i8* %arrayidx49, align 1, !tbaa !9
  %39 = bitcast i32* %mskx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #7
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %mul50 = mul nsw i32 %40, 64
  %41 = load i32, i32* %j, align 4, !tbaa !6
  %add51 = add nsw i32 %mul50, %41
  %arrayidx52 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 1), i32 0, i32 %add51
  %42 = load i8, i8* %arrayidx52, align 1, !tbaa !9
  %conv53 = zext i8 %42 to i32
  store i32 %conv53, i32* %mskx, align 4, !tbaa !6
  %43 = load i32, i32* %mskx, align 4, !tbaa !6
  %conv54 = trunc i32 %43 to i8
  %44 = load i32, i32* %j, align 4, !tbaa !6
  %mul55 = mul nsw i32 %44, 64
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %add56 = add nsw i32 %mul55, %45
  %arrayidx57 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 0, i32 0), i32 0, i32 %add56
  store i8 %conv54, i8* %arrayidx57, align 1, !tbaa !9
  %46 = load i32, i32* %mskx, align 4, !tbaa !6
  %sub58 = sub nsw i32 64, %46
  %conv59 = trunc i32 %sub58 to i8
  %47 = load i32, i32* %j, align 4, !tbaa !6
  %mul60 = mul nsw i32 %47, 64
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %add61 = add nsw i32 %mul60, %48
  %arrayidx62 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 1, i32 0), i32 0, i32 %add61
  store i8 %conv59, i8* %arrayidx62, align 1, !tbaa !9
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %mul63 = mul nsw i32 %49, 64
  %50 = load i32, i32* %j, align 4, !tbaa !6
  %add64 = add nsw i32 %mul63, %50
  %arrayidx65 = getelementptr inbounds [4096 x i8], [4096 x i8]* getelementptr inbounds ([2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 1, i32 1), i32 0, i32 %add64
  store i8 %conv59, i8* %arrayidx65, align 1, !tbaa !9
  %51 = bitcast i32* %mskx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  %52 = bitcast i32* %msk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  br label %for.inc66

for.inc66:                                        ; preds = %for.body14
  %53 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %53, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond12

for.end67:                                        ; preds = %for.cond12
  br label %for.inc68

for.inc68:                                        ; preds = %for.end67
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %inc69 = add nsw i32 %54, 1
  store i32 %inc69, i32* %i, align 4, !tbaa !6
  br label %for.cond9

for.end70:                                        ; preds = %for.cond9
  %55 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #7
  %56 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #7
  %57 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %58 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #7
  %59 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #7
  %60 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @shift_copy(i8* %src, i8* %dst, i32 %shift, i32 %width) #1 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %shift.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %shift, i32* %shift.addr, align 4, !tbaa !6
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  %0 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %2 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %2
  %3 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %4 = load i32, i32* %width.addr, align 4, !tbaa !6
  %5 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %4, %5
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %3, i32 %sub, i1 false)
  %6 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %7 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %7, i32 0
  %8 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %8 to i32
  %9 = trunc i32 %conv to i8
  %10 = load i32, i32* %shift.addr, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %6, i8 %9, i32 %10, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sub1 = sub nsw i32 0, %11
  store i32 %sub1, i32* %shift.addr, align 4, !tbaa !6
  %12 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %13 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %14 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %add.ptr2 = getelementptr inbounds i8, i8* %13, i32 %14
  %15 = load i32, i32* %width.addr, align 4, !tbaa !6
  %16 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %sub3 = sub nsw i32 %15, %16
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %12, i8* align 1 %add.ptr2, i32 %sub3, i1 false)
  %17 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %18 = load i32, i32* %width.addr, align 4, !tbaa !6
  %add.ptr4 = getelementptr inbounds i8, i8* %17, i32 %18
  %19 = load i32, i32* %shift.addr, align 4, !tbaa !6
  %idx.neg = sub i32 0, %19
  %add.ptr5 = getelementptr inbounds i8, i8* %add.ptr4, i32 %idx.neg
  %20 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %21 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub6 = sub nsw i32 %21, 1
  %arrayidx7 = getelementptr inbounds i8, i8* %20, i32 %sub6
  %22 = load i8, i8* %arrayidx7, align 1, !tbaa !9
  %conv8 = zext i8 %22 to i32
  %23 = trunc i32 %conv8 to i8
  %24 = load i32, i32* %shift.addr, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr5, i8 %23, i32 %24, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #6

; Function Attrs: inlinehint nounwind
define internal void @init_wedge_masks() #1 {
entry:
  %dst = alloca i8*, align 4
  %bsize = alloca i8, align 1
  %wedge_params = alloca %struct.wedge_params_type*, align 4
  %wtypes = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %mask = alloca i8*, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %w = alloca i32, align 4
  %0 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i8* getelementptr inbounds ([131072 x i8], [131072 x i8]* @wedge_mask_buf, i32 0, i32 0), i8** %dst, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #7
  call void @llvm.memset.p0i8.i32(i8* align 16 bitcast ([22 x [2 x [16 x i8*]]]* @wedge_masks to i8*), i8 0, i32 2816, i1 false)
  store i8 0, i8* %bsize, align 1, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc23, %entry
  %1 = load i8, i8* %bsize, align 1, !tbaa !9
  %conv = zext i8 %1 to i32
  %cmp = icmp slt i32 %conv, 22
  br i1 %cmp, label %for.body, label %for.end25

for.body:                                         ; preds = %for.cond
  %2 = bitcast %struct.wedge_params_type** %wedge_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [22 x %struct.wedge_params_type], [22 x %struct.wedge_params_type]* @av1_wedge_params_lookup, i32 0, i32 %idxprom
  store %struct.wedge_params_type* %arrayidx, %struct.wedge_params_type** %wedge_params, align 4, !tbaa !2
  %4 = bitcast i32* %wtypes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.wedge_params_type*, %struct.wedge_params_type** %wedge_params, align 4, !tbaa !2
  %wedge_types = getelementptr inbounds %struct.wedge_params_type, %struct.wedge_params_type* %5, i32 0, i32 0
  %6 = load i32, i32* %wedge_types, align 4, !tbaa !157
  store i32 %6, i32* %wtypes, align 4, !tbaa !6
  %7 = load i32, i32* %wtypes, align 4, !tbaa !6
  %cmp2 = icmp eq i32 %7, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %8 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom4 = zext i8 %10 to i32
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom4
  %11 = load i8, i8* %arrayidx5, align 1, !tbaa !9
  %conv6 = zext i8 %11 to i32
  store i32 %conv6, i32* %bw, align 4, !tbaa !6
  %12 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom7 = zext i8 %13 to i32
  %arrayidx8 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom7
  %14 = load i8, i8* %arrayidx8, align 1, !tbaa !9
  %conv9 = zext i8 %14 to i32
  store i32 %conv9, i32* %bh, align 4, !tbaa !6
  %15 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  store i32 0, i32* %w, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %if.end
  %16 = load i32, i32* %w, align 4, !tbaa !6
  %17 = load i32, i32* %wtypes, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %16, %17
  br i1 %cmp11, label %for.body13, label %for.end

for.body13:                                       ; preds = %for.cond10
  %18 = load i32, i32* %w, align 4, !tbaa !6
  %19 = load i8, i8* %bsize, align 1, !tbaa !9
  %call = call i8* @get_wedge_mask_inplace(i32 %18, i32 0, i8 zeroext %19)
  store i8* %call, i8** %mask, align 4, !tbaa !2
  %20 = load i8*, i8** %mask, align 4, !tbaa !2
  %21 = load i8*, i8** %dst, align 4, !tbaa !2
  %22 = load i32, i32* %bw, align 4, !tbaa !6
  %23 = load i32, i32* %bw, align 4, !tbaa !6
  %24 = load i32, i32* %bh, align 4, !tbaa !6
  call void @aom_convolve_copy_c(i8* %20, i32 64, i8* %21, i32 %22, i16* null, i32 0, i16* null, i32 0, i32 %23, i32 %24)
  %25 = load i8*, i8** %dst, align 4, !tbaa !2
  %26 = load %struct.wedge_params_type*, %struct.wedge_params_type** %wedge_params, align 4, !tbaa !2
  %masks = getelementptr inbounds %struct.wedge_params_type, %struct.wedge_params_type* %26, i32 0, i32 3
  %27 = load [16 x i8*]*, [16 x i8*]** %masks, align 4, !tbaa !66
  %arrayidx14 = getelementptr inbounds [16 x i8*], [16 x i8*]* %27, i32 0
  %28 = load i32, i32* %w, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [16 x i8*], [16 x i8*]* %arrayidx14, i32 0, i32 %28
  store i8* %25, i8** %arrayidx15, align 4, !tbaa !2
  %29 = load i32, i32* %bw, align 4, !tbaa !6
  %30 = load i32, i32* %bh, align 4, !tbaa !6
  %mul = mul nsw i32 %29, %30
  %31 = load i8*, i8** %dst, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %31, i32 %mul
  store i8* %add.ptr, i8** %dst, align 4, !tbaa !2
  %32 = load i32, i32* %w, align 4, !tbaa !6
  %33 = load i8, i8* %bsize, align 1, !tbaa !9
  %call16 = call i8* @get_wedge_mask_inplace(i32 %32, i32 1, i8 zeroext %33)
  store i8* %call16, i8** %mask, align 4, !tbaa !2
  %34 = load i8*, i8** %mask, align 4, !tbaa !2
  %35 = load i8*, i8** %dst, align 4, !tbaa !2
  %36 = load i32, i32* %bw, align 4, !tbaa !6
  %37 = load i32, i32* %bw, align 4, !tbaa !6
  %38 = load i32, i32* %bh, align 4, !tbaa !6
  call void @aom_convolve_copy_c(i8* %34, i32 64, i8* %35, i32 %36, i16* null, i32 0, i16* null, i32 0, i32 %37, i32 %38)
  %39 = load i8*, i8** %dst, align 4, !tbaa !2
  %40 = load %struct.wedge_params_type*, %struct.wedge_params_type** %wedge_params, align 4, !tbaa !2
  %masks17 = getelementptr inbounds %struct.wedge_params_type, %struct.wedge_params_type* %40, i32 0, i32 3
  %41 = load [16 x i8*]*, [16 x i8*]** %masks17, align 4, !tbaa !66
  %arrayidx18 = getelementptr inbounds [16 x i8*], [16 x i8*]* %41, i32 1
  %42 = load i32, i32* %w, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds [16 x i8*], [16 x i8*]* %arrayidx18, i32 0, i32 %42
  store i8* %39, i8** %arrayidx19, align 4, !tbaa !2
  %43 = load i32, i32* %bw, align 4, !tbaa !6
  %44 = load i32, i32* %bh, align 4, !tbaa !6
  %mul20 = mul nsw i32 %43, %44
  %45 = load i8*, i8** %dst, align 4, !tbaa !2
  %add.ptr21 = getelementptr inbounds i8, i8* %45, i32 %mul20
  store i8* %add.ptr21, i8** %dst, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %46 = load i32, i32* %w, align 4, !tbaa !6
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %w, align 4, !tbaa !6
  br label %for.cond10

for.end:                                          ; preds = %for.cond10
  %47 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  %48 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  %50 = bitcast i8** %mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then
  %51 = bitcast i32* %wtypes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #7
  %52 = bitcast %struct.wedge_params_type** %wedge_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc23
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc23

for.inc23:                                        ; preds = %cleanup.cont, %cleanup
  %53 = load i8, i8* %bsize, align 1, !tbaa !9
  %inc24 = add i8 %53, 1
  store i8 %inc24, i8* %bsize, align 1, !tbaa !9
  br label %for.cond

for.end25:                                        ; preds = %for.cond
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #7
  %54 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i8* @get_wedge_mask_inplace(i32 %wedge_index, i32 %neg, i8 zeroext %sb_type) #0 {
entry:
  %wedge_index.addr = alloca i32, align 4
  %neg.addr = alloca i32, align 4
  %sb_type.addr = alloca i8, align 1
  %master = alloca i8*, align 4
  %bh = alloca i32, align 4
  %bw = alloca i32, align 4
  %a = alloca %struct.wedge_code_type*, align 4
  %woff = alloca i32, align 4
  %hoff = alloca i32, align 4
  %wsignflip = alloca i8, align 1
  store i32 %wedge_index, i32* %wedge_index.addr, align 4, !tbaa !6
  store i32 %neg, i32* %neg.addr, align 4, !tbaa !6
  store i8 %sb_type, i8* %sb_type.addr, align 1, !tbaa !9
  %0 = bitcast i8** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %idxprom = zext i8 %2 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %3 to i32
  store i32 %conv, i32* %bh, align 4, !tbaa !6
  %4 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %idxprom1 = zext i8 %5 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom1
  %6 = load i8, i8* %arrayidx2, align 1, !tbaa !9
  %conv3 = zext i8 %6 to i32
  store i32 %conv3, i32* %bw, align 4, !tbaa !6
  %7 = bitcast %struct.wedge_code_type** %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %idxprom4 = zext i8 %8 to i32
  %arrayidx5 = getelementptr inbounds [22 x %struct.wedge_params_type], [22 x %struct.wedge_params_type]* @av1_wedge_params_lookup, i32 0, i32 %idxprom4
  %codebook = getelementptr inbounds %struct.wedge_params_type, %struct.wedge_params_type* %arrayidx5, i32 0, i32 1
  %9 = load %struct.wedge_code_type*, %struct.wedge_code_type** %codebook, align 4, !tbaa !158
  %10 = load i32, i32* %wedge_index.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds %struct.wedge_code_type, %struct.wedge_code_type* %9, i32 %10
  store %struct.wedge_code_type* %add.ptr, %struct.wedge_code_type** %a, align 4, !tbaa !2
  %11 = bitcast i32* %woff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = bitcast i32* %hoff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %wsignflip) #7
  %13 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %idxprom6 = zext i8 %13 to i32
  %arrayidx7 = getelementptr inbounds [22 x %struct.wedge_params_type], [22 x %struct.wedge_params_type]* @av1_wedge_params_lookup, i32 0, i32 %idxprom6
  %signflip = getelementptr inbounds %struct.wedge_params_type, %struct.wedge_params_type* %arrayidx7, i32 0, i32 2
  %14 = load i8*, i8** %signflip, align 8, !tbaa !159
  %15 = load i32, i32* %wedge_index.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load i8, i8* %arrayidx8, align 1, !tbaa !9
  store i8 %16, i8* %wsignflip, align 1, !tbaa !9
  %17 = load %struct.wedge_code_type*, %struct.wedge_code_type** %a, align 4, !tbaa !2
  %x_offset = getelementptr inbounds %struct.wedge_code_type, %struct.wedge_code_type* %17, i32 0, i32 1
  %18 = load i32, i32* %x_offset, align 4, !tbaa !160
  %19 = load i32, i32* %bw, align 4, !tbaa !6
  %mul = mul nsw i32 %18, %19
  %shr = ashr i32 %mul, 3
  store i32 %shr, i32* %woff, align 4, !tbaa !6
  %20 = load %struct.wedge_code_type*, %struct.wedge_code_type** %a, align 4, !tbaa !2
  %y_offset = getelementptr inbounds %struct.wedge_code_type, %struct.wedge_code_type* %20, i32 0, i32 2
  %21 = load i32, i32* %y_offset, align 4, !tbaa !162
  %22 = load i32, i32* %bh, align 4, !tbaa !6
  %mul9 = mul nsw i32 %21, %22
  %shr10 = ashr i32 %mul9, 3
  store i32 %shr10, i32* %hoff, align 4, !tbaa !6
  %23 = load i32, i32* %neg.addr, align 4, !tbaa !6
  %24 = load i8, i8* %wsignflip, align 1, !tbaa !9
  %conv11 = zext i8 %24 to i32
  %xor = xor i32 %23, %conv11
  %arrayidx12 = getelementptr inbounds [2 x [6 x [4096 x i8]]], [2 x [6 x [4096 x i8]]]* @wedge_mask_obl, i32 0, i32 %xor
  %25 = load %struct.wedge_code_type*, %struct.wedge_code_type** %a, align 4, !tbaa !2
  %direction = getelementptr inbounds %struct.wedge_code_type, %struct.wedge_code_type* %25, i32 0, i32 0
  %26 = load i8, i8* %direction, align 4, !tbaa !163
  %idxprom13 = zext i8 %26 to i32
  %arrayidx14 = getelementptr inbounds [6 x [4096 x i8]], [6 x [4096 x i8]]* %arrayidx12, i32 0, i32 %idxprom13
  %arraydecay = getelementptr inbounds [4096 x i8], [4096 x i8]* %arrayidx14, i32 0, i32 0
  %27 = load i32, i32* %hoff, align 4, !tbaa !6
  %sub = sub nsw i32 32, %27
  %mul15 = mul nsw i32 64, %sub
  %add.ptr16 = getelementptr inbounds i8, i8* %arraydecay, i32 %mul15
  %add.ptr17 = getelementptr inbounds i8, i8* %add.ptr16, i32 32
  %28 = load i32, i32* %woff, align 4, !tbaa !6
  %idx.neg = sub i32 0, %28
  %add.ptr18 = getelementptr inbounds i8, i8* %add.ptr17, i32 %idx.neg
  store i8* %add.ptr18, i8** %master, align 4, !tbaa !2
  %29 = load i8*, i8** %master, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %wsignflip) #7
  %30 = bitcast i32* %hoff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast i32* %woff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast %struct.wedge_code_type** %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast i8** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  ret i8* %29
}

declare void @aom_convolve_copy_c(i8*, i32, i8*, i32, i16*, i32, i16*, i32, i32, i32) #3

; Function Attrs: inlinehint nounwind
define internal void @init_smooth_interintra_masks() #1 {
entry:
  %m = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %bs = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %0 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %m, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %entry
  %1 = load i32, i32* %m, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %2 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #7
  br label %for.end20

for.body:                                         ; preds = %for.cond
  %3 = bitcast i32* %bs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store i32 0, i32* %bs, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %bs, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %4, 22
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %5 = bitcast i32* %bs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %6 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load i32, i32* %bs, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %7
  %8 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %8 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !6
  %9 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i32, i32* %bs, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %10
  %11 = load i8, i8* %arrayidx5, align 1, !tbaa !9
  %conv6 = zext i8 %11 to i32
  store i32 %conv6, i32* %bh, align 4, !tbaa !6
  %12 = load i32, i32* %bw, align 4, !tbaa !6
  %cmp7 = icmp sgt i32 %12, 32
  br i1 %cmp7, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body4
  %13 = load i32, i32* %bh, align 4, !tbaa !6
  %cmp9 = icmp sgt i32 %13, 32
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body4
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %14 = load i32, i32* %m, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [4 x [22 x [1024 x i8]]], [4 x [22 x [1024 x i8]]]* @smooth_interintra_mask_buf, i32 0, i32 %14
  %15 = load i32, i32* %bs, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [22 x [1024 x i8]], [22 x [1024 x i8]]* %arrayidx11, i32 0, i32 %15
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %arrayidx12, i32 0, i32 0
  %16 = load i32, i32* %bw, align 4, !tbaa !6
  %17 = load i32, i32* %bs, align 4, !tbaa !6
  %conv13 = trunc i32 %17 to i8
  %18 = load i32, i32* %m, align 4, !tbaa !6
  %conv14 = trunc i32 %18 to i8
  call void @build_smooth_interintra_mask(i8* %arraydecay, i32 %16, i8 zeroext %conv13, i8 zeroext %conv14)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %19 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  %20 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %21 = load i32, i32* %bs, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %bs, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %22 = load i32, i32* %m, align 4, !tbaa !6
  %inc18 = add nsw i32 %22, 1
  store i32 %inc18, i32* %m, align 4, !tbaa !6
  br label %for.cond

for.end20:                                        ; preds = %for.cond.cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal void @build_smooth_interintra_mask(i8* %mask, i32 %stride, i8 zeroext %plane_bsize, i8 zeroext %mode) #1 {
entry:
  %mask.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %plane_bsize.addr = alloca i8, align 1
  %mode.addr = alloca i8, align 1
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %bw = alloca i32, align 4
  %bh = alloca i32, align 4
  %size_scale = alloca i32, align 4
  store i8* %mask, i8** %mask.addr, align 4, !tbaa !2
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i8 %plane_bsize, i8* %plane_bsize.addr, align 1, !tbaa !9
  store i8 %mode, i8* %mode.addr, align 1, !tbaa !9
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !9
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %bw, align 4, !tbaa !6
  %5 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !9
  %idxprom1 = zext i8 %6 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom1
  %7 = load i8, i8* %arrayidx2, align 1, !tbaa !9
  %conv3 = zext i8 %7 to i32
  store i32 %conv3, i32* %bh, align 4, !tbaa !6
  %8 = bitcast i32* %size_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !9
  %idxprom4 = zext i8 %9 to i32
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @ii_size_scales, i32 0, i32 %idxprom4
  %10 = load i8, i8* %arrayidx5, align 1, !tbaa !9
  %conv6 = zext i8 %10 to i32
  store i32 %conv6, i32* %size_scale, align 4, !tbaa !6
  %11 = load i8, i8* %mode.addr, align 1, !tbaa !9
  %conv7 = zext i8 %11 to i32
  switch i32 %conv7, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb12
    i32 3, label %sw.bb31
    i32 0, label %sw.bb52
  ]

sw.bb:                                            ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.bb
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %13 = load i32, i32* %bh, align 4, !tbaa !6
  %cmp = icmp slt i32 %12, %13
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %16 = load i32, i32* %size_scale, align 4, !tbaa !6
  %mul = mul nsw i32 %15, %16
  %arrayidx9 = getelementptr inbounds [128 x i8], [128 x i8]* @ii_weights1d, i32 0, i32 %mul
  %17 = load i8, i8* %arrayidx9, align 1, !tbaa !9
  %conv10 = zext i8 %17 to i32
  %18 = trunc i32 %conv10 to i8
  %19 = load i32, i32* %bw, align 4, !tbaa !6
  %mul11 = mul i32 %19, 1
  call void @llvm.memset.p0i8.i32(i8* align 1 %14, i8 %18, i32 %mul11, i1 false)
  %20 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %21 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 %20
  store i8* %add.ptr, i8** %mask.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %sw.epilog

sw.bb12:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc28, %sw.bb12
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %24 = load i32, i32* %bh, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %23, %24
  br i1 %cmp14, label %for.body16, label %for.end30

for.body16:                                       ; preds = %for.cond13
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc24, %for.body16
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %26 = load i32, i32* %bw, align 4, !tbaa !6
  %cmp18 = icmp slt i32 %25, %26
  br i1 %cmp18, label %for.body20, label %for.end26

for.body20:                                       ; preds = %for.cond17
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %28 = load i32, i32* %size_scale, align 4, !tbaa !6
  %mul21 = mul nsw i32 %27, %28
  %arrayidx22 = getelementptr inbounds [128 x i8], [128 x i8]* @ii_weights1d, i32 0, i32 %mul21
  %29 = load i8, i8* %arrayidx22, align 1, !tbaa !9
  %30 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds i8, i8* %30, i32 %31
  store i8 %29, i8* %arrayidx23, align 1, !tbaa !9
  br label %for.inc24

for.inc24:                                        ; preds = %for.body20
  %32 = load i32, i32* %j, align 4, !tbaa !6
  %inc25 = add nsw i32 %32, 1
  store i32 %inc25, i32* %j, align 4, !tbaa !6
  br label %for.cond17

for.end26:                                        ; preds = %for.cond17
  %33 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %34 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr27 = getelementptr inbounds i8, i8* %34, i32 %33
  store i8* %add.ptr27, i8** %mask.addr, align 4, !tbaa !2
  br label %for.inc28

for.inc28:                                        ; preds = %for.end26
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %inc29 = add nsw i32 %35, 1
  store i32 %inc29, i32* %i, align 4, !tbaa !6
  br label %for.cond13

for.end30:                                        ; preds = %for.cond13
  br label %sw.epilog

sw.bb31:                                          ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc49, %sw.bb31
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %bh, align 4, !tbaa !6
  %cmp33 = icmp slt i32 %36, %37
  br i1 %cmp33, label %for.body35, label %for.end51

for.body35:                                       ; preds = %for.cond32
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc45, %for.body35
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %39 = load i32, i32* %bw, align 4, !tbaa !6
  %cmp37 = icmp slt i32 %38, %39
  br i1 %cmp37, label %for.body39, label %for.end47

for.body39:                                       ; preds = %for.cond36
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %j, align 4, !tbaa !6
  %cmp40 = icmp slt i32 %40, %41
  br i1 %cmp40, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body39
  %42 = load i32, i32* %i, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %for.body39
  %43 = load i32, i32* %j, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %42, %cond.true ], [ %43, %cond.false ]
  %44 = load i32, i32* %size_scale, align 4, !tbaa !6
  %mul42 = mul nsw i32 %cond, %44
  %arrayidx43 = getelementptr inbounds [128 x i8], [128 x i8]* @ii_weights1d, i32 0, i32 %mul42
  %45 = load i8, i8* %arrayidx43, align 1, !tbaa !9
  %46 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %47 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds i8, i8* %46, i32 %47
  store i8 %45, i8* %arrayidx44, align 1, !tbaa !9
  br label %for.inc45

for.inc45:                                        ; preds = %cond.end
  %48 = load i32, i32* %j, align 4, !tbaa !6
  %inc46 = add nsw i32 %48, 1
  store i32 %inc46, i32* %j, align 4, !tbaa !6
  br label %for.cond36

for.end47:                                        ; preds = %for.cond36
  %49 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %50 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr48 = getelementptr inbounds i8, i8* %50, i32 %49
  store i8* %add.ptr48, i8** %mask.addr, align 4, !tbaa !2
  br label %for.inc49

for.inc49:                                        ; preds = %for.end47
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %inc50 = add nsw i32 %51, 1
  store i32 %inc50, i32* %i, align 4, !tbaa !6
  br label %for.cond32

for.end51:                                        ; preds = %for.cond32
  br label %sw.epilog

sw.bb52:                                          ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb52
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond53

for.cond53:                                       ; preds = %for.inc59, %sw.default
  %52 = load i32, i32* %i, align 4, !tbaa !6
  %53 = load i32, i32* %bh, align 4, !tbaa !6
  %cmp54 = icmp slt i32 %52, %53
  br i1 %cmp54, label %for.body56, label %for.end61

for.body56:                                       ; preds = %for.cond53
  %54 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %55 = load i32, i32* %bw, align 4, !tbaa !6
  %mul57 = mul i32 %55, 1
  call void @llvm.memset.p0i8.i32(i8* align 1 %54, i8 32, i32 %mul57, i1 false)
  %56 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %57 = load i8*, i8** %mask.addr, align 4, !tbaa !2
  %add.ptr58 = getelementptr inbounds i8, i8* %57, i32 %56
  store i8* %add.ptr58, i8** %mask.addr, align 4, !tbaa !2
  br label %for.inc59

for.inc59:                                        ; preds = %for.body56
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %inc60 = add nsw i32 %58, 1
  store i32 %inc60, i32* %i, align 4, !tbaa !6
  br label %for.cond53

for.end61:                                        ; preds = %for.cond53
  br label %sw.epilog

sw.epilog:                                        ; preds = %for.end61, %for.end51, %for.end30, %for.end
  %59 = bitcast i32* %size_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #7
  %60 = bitcast i32* %bh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #7
  %61 = bitcast i32* %bw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #7
  %62 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #7
  %63 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  ret void
}

declare void @aom_highbd_blend_a64_d16_mask_c(i8*, i32, i16*, i32, i16*, i32, i8*, i32, i32, i32, i32, i32, %struct.ConvolveParams*, i32) #3

declare void @aom_lowbd_blend_a64_d16_mask_c(i8*, i32, i16*, i32, i16*, i32, i8*, i32, i32, i32, i32, i32, %struct.ConvolveParams*) #3

; Function Attrs: inlinehint nounwind
define internal i32 @is_inter_block(%struct.MB_MODE_INFO* %mbmi) #1 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @is_intrabc_block(%struct.MB_MODE_INFO* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %ref_frame = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 12
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %ref_frame, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !9
  %conv = sext i8 %2 to i32
  %cmp = icmp sgt i32 %conv, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal void @get_conv_params_no_round(%struct.ConvolveParams* noalias sret align 4 %agg.result, i32 %cmp_index, i32 %plane, i16* %dst, i32 %dst_stride, i32 %is_compound, i32 %bd) #1 {
entry:
  %cmp_index.addr = alloca i32, align 4
  %plane.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %is_compound.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %intbufrange = alloca i32, align 4
  store i32 %cmp_index, i32* %cmp_index.addr, align 4, !tbaa !6
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %is_compound, i32* %is_compound.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %cmp_index.addr, align 4, !tbaa !6
  %compound_index = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 7
  store i32 %0, i32* %compound_index, align 4, !tbaa !164
  %1 = load i32, i32* %is_compound.addr, align 4, !tbaa !6
  %is_compound1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 6
  store i32 %1, i32* %is_compound1, align 4, !tbaa !165
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 3
  store i32 3, i32* %round_0, align 4, !tbaa !68
  %2 = load i32, i32* %is_compound.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %round_02 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 3
  %3 = load i32, i32* %round_02, align 4, !tbaa !68
  %sub = sub nsw i32 14, %3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 7, %cond.true ], [ %sub, %cond.false ]
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 4
  store i32 %cond, i32* %round_1, align 4, !tbaa !69
  %4 = bitcast i32* %intbufrange to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add = add nsw i32 %5, 7
  %round_03 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 3
  %6 = load i32, i32* %round_03, align 4, !tbaa !68
  %sub4 = sub nsw i32 %add, %6
  %add5 = add nsw i32 %sub4, 2
  store i32 %add5, i32* %intbufrange, align 4, !tbaa !6
  %7 = load i32, i32* %intbufrange, align 4, !tbaa !6
  %cmp = icmp sgt i32 %7, 16
  br i1 %cmp, label %if.then, label %if.end14

if.then:                                          ; preds = %cond.end
  %8 = load i32, i32* %intbufrange, align 4, !tbaa !6
  %sub6 = sub nsw i32 %8, 16
  %round_07 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 3
  %9 = load i32, i32* %round_07, align 4, !tbaa !68
  %add8 = add nsw i32 %9, %sub6
  store i32 %add8, i32* %round_07, align 4, !tbaa !68
  %10 = load i32, i32* %is_compound.addr, align 4, !tbaa !6
  %tobool9 = icmp ne i32 %10, 0
  br i1 %tobool9, label %if.end, label %if.then10

if.then10:                                        ; preds = %if.then
  %11 = load i32, i32* %intbufrange, align 4, !tbaa !6
  %sub11 = sub nsw i32 %11, 16
  %round_112 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 4
  %12 = load i32, i32* %round_112, align 4, !tbaa !69
  %sub13 = sub nsw i32 %12, %sub11
  store i32 %sub13, i32* %round_112, align 4, !tbaa !69
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.then
  br label %if.end14

if.end14:                                         ; preds = %if.end, %cond.end
  %13 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %dst15 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 1
  store i16* %13, i16** %dst15, align 4, !tbaa !166
  %14 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %dst_stride16 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 2
  store i32 %14, i32* %dst_stride16, align 4, !tbaa !167
  %15 = load i32, i32* %plane.addr, align 4, !tbaa !6
  %plane17 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 5
  store i32 %15, i32* %plane17, align 4, !tbaa !168
  %16 = load i32, i32* %cmp_index.addr, align 4, !tbaa !6
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %agg.result, i32 0, i32 0
  store i32 %16, i32* %do_average, align 4, !tbaa !169
  %17 = bitcast i32* %intbufrange to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_global_mv_block(%struct.MB_MODE_INFO* %mbmi, i8 zeroext %type) #1 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  %type.addr = alloca i8, align 1
  %mode = alloca i8, align 1
  %bsize = alloca i8, align 1
  %block_size_allowed = alloca i32, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  store i8 %type, i8* %type.addr, align 1, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mode) #7
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %mode1 = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %0, i32 0, i32 7
  %1 = load i8, i8* %mode1, align 1, !tbaa !170
  store i8 %1, i8* %mode, align 1, !tbaa !9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bsize) #7
  %2 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %sb_type = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %2, i32 0, i32 6
  %3 = load i8, i8* %sb_type, align 2, !tbaa !74
  store i8 %3, i8* %bsize, align 1, !tbaa !9
  %4 = bitcast i32* %block_size_allowed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom = zext i8 %5 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %6 to i32
  %7 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom2 = zext i8 %7 to i32
  %arrayidx3 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom2
  %8 = load i8, i8* %arrayidx3, align 1, !tbaa !9
  %conv4 = zext i8 %8 to i32
  %cmp = icmp slt i32 %conv, %conv4
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %9 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom6 = zext i8 %9 to i32
  %arrayidx7 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom6
  %10 = load i8, i8* %arrayidx7, align 1, !tbaa !9
  %conv8 = zext i8 %10 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load i8, i8* %bsize, align 1, !tbaa !9
  %idxprom9 = zext i8 %11 to i32
  %arrayidx10 = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom9
  %12 = load i8, i8* %arrayidx10, align 1, !tbaa !9
  %conv11 = zext i8 %12 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv8, %cond.true ], [ %conv11, %cond.false ]
  %cmp12 = icmp sge i32 %cond, 8
  %conv13 = zext i1 %cmp12 to i32
  store i32 %conv13, i32* %block_size_allowed, align 4, !tbaa !6
  %13 = load i8, i8* %mode, align 1, !tbaa !9
  %conv14 = zext i8 %13 to i32
  %cmp15 = icmp eq i32 %conv14, 15
  br i1 %cmp15, label %land.lhs.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end
  %14 = load i8, i8* %mode, align 1, !tbaa !9
  %conv17 = zext i8 %14 to i32
  %cmp18 = icmp eq i32 %conv17, 23
  br i1 %cmp18, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %lor.lhs.false, %cond.end
  %15 = load i8, i8* %type.addr, align 1, !tbaa !9
  %conv20 = zext i8 %15 to i32
  %cmp21 = icmp sgt i32 %conv20, 1
  br i1 %cmp21, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %16 = load i32, i32* %block_size_allowed, align 4, !tbaa !6
  %tobool = icmp ne i32 %16, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %lor.lhs.false
  %17 = phi i1 [ false, %land.lhs.true ], [ false, %lor.lhs.false ], [ %tobool, %land.rhs ]
  %land.ext = zext i1 %17 to i32
  %18 = bitcast i32* %block_size_allowed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bsize) #7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mode) #7
  ret i32 %land.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_masked_compound_type(i8 zeroext %type) #1 {
entry:
  %type.addr = alloca i8, align 1
  store i8 %type, i8* %type.addr, align 1, !tbaa !9
  %0 = load i8, i8* %type.addr, align 1, !tbaa !9
  %conv = zext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 2
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i8, i8* %type.addr, align 1, !tbaa !9
  %conv2 = zext i8 %1 to i32
  %cmp3 = icmp eq i32 %conv2, 3
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp3, %lor.rhs ]
  %lor.ext = zext i1 %2 to i32
  ret i32 %lor.ext
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_ref_frame_map_idx(%struct.AV1Common* %cm, i8 signext %ref_frame) #1 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  %ref_frame.addr = alloca i8, align 1
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i8 %ref_frame, i8* %ref_frame.addr, align 1, !tbaa !9
  %0 = load i8, i8* %ref_frame.addr, align 1, !tbaa !9
  %conv = sext i8 %0 to i32
  %cmp = icmp sge i32 %conv, 1
  br i1 %cmp, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %entry
  %1 = load i8, i8* %ref_frame.addr, align 1, !tbaa !9
  %conv2 = sext i8 %1 to i32
  %cmp3 = icmp sle i32 %conv2, 8
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %remapped_ref_idx = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 14
  %3 = load i8, i8* %ref_frame.addr, align 1, !tbaa !9
  %conv5 = sext i8 %3 to i32
  %sub = sub nsw i32 %conv5, 1
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %remapped_ref_idx, i32 0, i32 %sub
  %4 = load i32, i32* %arrayidx, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ -1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal i64 @scaled_buffer_offset(i32 %x_offset, i32 %y_offset, i32 %stride, %struct.scale_factors* %sf) #1 {
entry:
  %x_offset.addr = alloca i32, align 4
  %y_offset.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store i32 %x_offset, i32* %x_offset.addr, align 4, !tbaa !6
  store i32 %y_offset, i32* %y_offset.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.scale_factors* %1, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %scale_value_x = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %2, i32 0, i32 4
  %3 = load i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)** %scale_value_x, align 4, !tbaa !171
  %4 = load i32, i32* %x_offset.addr, align 4, !tbaa !6
  %5 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call = call i32 %3(i32 %4, %struct.scale_factors* %5)
  %shr = ashr i32 %call, 6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i32, i32* %x_offset.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shr, %cond.true ], [ %6, %cond.false ]
  store i32 %cond, i32* %x, align 4, !tbaa !6
  %7 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.scale_factors* %8, null
  br i1 %tobool1, label %cond.true2, label %cond.false5

cond.true2:                                       ; preds = %cond.end
  %9 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %scale_value_y = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %9, i32 0, i32 5
  %10 = load i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)** %scale_value_y, align 4, !tbaa !172
  %11 = load i32, i32* %y_offset.addr, align 4, !tbaa !6
  %12 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %call3 = call i32 %10(i32 %11, %struct.scale_factors* %12)
  %shr4 = ashr i32 %call3, 6
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  %13 = load i32, i32* %y_offset.addr, align 4, !tbaa !6
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true2
  %cond7 = phi i32 [ %shr4, %cond.true2 ], [ %13, %cond.false5 ]
  store i32 %cond7, i32* %y, align 4, !tbaa !6
  %14 = load i32, i32* %y, align 4, !tbaa !6
  %conv = sext i32 %14 to i64
  %15 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %conv8 = sext i32 %15 to i64
  %mul = mul nsw i64 %conv, %conv8
  %16 = load i32, i32* %x, align 4, !tbaa !6
  %conv9 = sext i32 %16 to i64
  %add = add nsw i64 %mul, %conv9
  %17 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %18 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  ret i64 %add
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_num_planes(%struct.AV1Common* %cm) #1 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %1 = load i8, i8* %monochrome, align 1, !tbaa !173
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 3
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal i32 @is_neighbor_overlappable(%struct.MB_MODE_INFO* %mbmi) #1 {
entry:
  %mbmi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %mbmi, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %mbmi.addr, align 4, !tbaa !2
  %call = call i32 @is_inter_block(%struct.MB_MODE_INFO* %0)
  ret i32 %call
}

declare void @aom_highbd_blend_a64_vmask_c(i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32) #3

declare void @aom_blend_a64_vmask_c(i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32) #3

declare void @aom_highbd_blend_a64_hmask_c(i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32) #3

declare void @aom_blend_a64_hmask_c(i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32) #3

; Function Attrs: inlinehint nounwind
define internal i32 @av1_is_wedge_used(i8 zeroext %sb_type) #1 {
entry:
  %sb_type.addr = alloca i8, align 1
  store i8 %sb_type, i8* %sb_type.addr, align 1, !tbaa !9
  %0 = load i8, i8* %sb_type.addr, align 1, !tbaa !9
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x %struct.wedge_params_type], [22 x %struct.wedge_params_type]* @av1_wedge_params_lookup, i32 0, i32 %idxprom
  %wedge_types = getelementptr inbounds %struct.wedge_params_type, %struct.wedge_params_type* %arrayidx, i32 0, i32 0
  %1 = load i32, i32* %wedge_types, align 16, !tbaa !157
  %cmp = icmp sgt i32 %1, 0
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

declare void @aom_highbd_blend_a64_mask_c(i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32, i32, i32, i32) #3

declare void @aom_blend_a64_mask_c(i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32, i32, i32) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn writeonly }
attributes #7 = { nounwind }
attributes #8 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{i64 0, i64 32, !9, i64 32, i64 2, !10, i64 34, i64 2, !10, i64 36, i64 2, !10, i64 38, i64 2, !10, i64 40, i64 1, !9, i64 41, i64 1, !9}
!9 = !{!4, !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !7, i64 4}
!13 = !{!"", !7, i64 0, !7, i64 4}
!14 = !{!15, !4, i64 49}
!15 = !{!"MB_MODE_INFO", !16, i64 0, !17, i64 8, !4, i64 52, !7, i64 60, !4, i64 64, !18, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !19, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!16 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!17 = !{!"", !4, i64 0, !11, i64 32, !11, i64 34, !11, i64 36, !11, i64 38, !4, i64 40, !4, i64 41}
!18 = !{!"", !4, i64 0, !4, i64 48}
!19 = !{!"", !4, i64 0, !4, i64 1}
!20 = !{!13, !7, i64 0}
!21 = !{!17, !4, i64 41}
!22 = !{!23, !7, i64 0}
!23 = !{!"scale_factors", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!24 = !{!23, !7, i64 4}
!25 = !{!26, !7, i64 104}
!26 = !{!"InterPredParams", !4, i64 0, !4, i64 4, !17, i64 8, !27, i64 52, !4, i64 96, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !28, i64 120, !7, i64 140, !7, i64 144, !3, i64 148, !7, i64 152, !7, i64 156, !16, i64 160, !4, i64 168, !7, i64 172}
!27 = !{!"ConvolveParams", !7, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40}
!28 = !{!"buf_2d", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!29 = !{!26, !7, i64 108}
!30 = !{!26, !7, i64 112}
!31 = !{!26, !7, i64 116}
!32 = !{!26, !7, i64 140}
!33 = !{!26, !7, i64 144}
!34 = !{!26, !7, i64 152}
!35 = !{!26, !7, i64 156}
!36 = !{!26, !7, i64 172}
!37 = !{!26, !3, i64 148}
!38 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !6, i64 12, i64 4, !6, i64 16, i64 4, !6}
!39 = !{!26, !4, i64 0}
!40 = !{!26, !4, i64 4}
!41 = !{!42, !7, i64 6908}
!42 = !{!"macroblockd", !7, i64 0, !7, i64 4, !7, i64 8, !43, i64 12, !4, i64 16, !44, i64 4060, !3, i64 4084, !43, i64 4088, !43, i64 4089, !43, i64 4090, !43, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !7, i64 4112, !7, i64 4116, !7, i64 4120, !7, i64 4124, !7, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !7, i64 6836, !4, i64 6840, !4, i64 6872, !7, i64 6904, !7, i64 6908, !3, i64 6912, !3, i64 6916, !7, i64 6920, !7, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !45, i64 39720, !46, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!43 = !{!"_Bool", !4, i64 0}
!44 = !{!"TileInfo", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20}
!45 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !7, i64 4104, !4, i64 4108, !7, i64 4236, !7, i64 4240, !7, i64 4244, !7, i64 4248, !7, i64 4252, !7, i64 4256}
!46 = !{!"dist_wtd_comp_params", !7, i64 0, !7, i64 4, !7, i64 8}
!47 = !{!42, !3, i64 6916}
!48 = !{!26, !4, i64 168}
!49 = !{i64 0, i64 4, !2, i64 4, i64 1, !9, i64 5, i64 1, !9, i64 6, i64 1, !9, i64 7, i64 1, !9}
!50 = !{!26, !7, i64 80}
!51 = !{!26, !7, i64 52}
!52 = !{!26, !3, i64 124}
!53 = !{!26, !7, i64 128}
!54 = !{!26, !7, i64 132}
!55 = !{!26, !7, i64 136}
!56 = !{!57, !7, i64 0}
!57 = !{!"SubpelParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!58 = !{!57, !7, i64 4}
!59 = !{!57, !7, i64 8}
!60 = !{!57, !7, i64 12}
!61 = !{i64 0, i64 4, !6, i64 4, i64 4, !6, i64 8, i64 4, !6, i64 12, i64 4, !6}
!62 = !{!16, !4, i64 7}
!63 = !{!16, !4, i64 4}
!64 = !{!16, !4, i64 5}
!65 = !{!16, !3, i64 0}
!66 = !{!67, !3, i64 12}
!67 = !{!"", !7, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!68 = !{!27, !7, i64 12}
!69 = !{!27, !7, i64 16}
!70 = !{!26, !3, i64 56}
!71 = !{!26, !7, i64 60}
!72 = !{!26, !7, i64 72}
!73 = !{!16, !4, i64 6}
!74 = !{!15, !4, i64 118}
!75 = !{!76, !7, i64 16}
!76 = !{!"macroblockd_plane", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !7, i64 16, !7, i64 20, !28, i64 24, !4, i64 44, !3, i64 84, !3, i64 88, !4, i64 92, !3, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!77 = !{!76, !7, i64 20}
!78 = !{!42, !3, i64 4084}
!79 = !{!42, !7, i64 8}
!80 = !{!43, !43, i64 0}
!81 = !{i8 0, i8 2}
!82 = !{!28, !3, i64 0}
!83 = !{!28, !7, i64 16}
!84 = !{!28, !3, i64 4}
!85 = !{!28, !7, i64 8}
!86 = !{!28, !7, i64 12}
!87 = !{i64 0, i64 2, !10, i64 2, i64 2, !10}
!88 = !{!42, !7, i64 6836}
!89 = !{!42, !3, i64 44008}
!90 = !{i64 0, i64 4, !6, i64 4, i64 4, !2, i64 8, i64 4, !6, i64 12, i64 4, !6, i64 16, i64 4, !6, i64 20, i64 4, !6, i64 24, i64 4, !6, i64 28, i64 4, !6, i64 32, i64 4, !6, i64 36, i64 4, !6, i64 40, i64 4, !6}
!91 = !{!26, !7, i64 84}
!92 = !{!17, !4, i64 40}
!93 = !{!15, !4, i64 122}
!94 = !{!15, !4, i64 7}
!95 = !{!26, !3, i64 160}
!96 = !{!15, !4, i64 159}
!97 = !{!98, !3, i64 456}
!98 = !{!"AV1Common", !99, i64 0, !101, i64 40, !7, i64 288, !7, i64 292, !7, i64 296, !7, i64 300, !7, i64 304, !7, i64 308, !4, i64 312, !43, i64 313, !4, i64 316, !7, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !23, i64 492, !4, i64 580, !4, i64 1284, !7, i64 1316, !7, i64 1320, !7, i64 1324, !102, i64 1328, !103, i64 1356, !104, i64 1420, !105, i64 10676, !3, i64 10848, !106, i64 10864, !107, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !108, i64 14880, !110, i64 15028, !111, i64 15168, !112, i64 15816, !4, i64 15836, !113, i64 16192, !3, i64 18128, !3, i64 18132, !116, i64 18136, !3, i64 18724, !117, i64 18728, !7, i64 18760, !4, i64 18764, !3, i64 18796, !7, i64 18800, !4, i64 18804, !4, i64 18836, !7, i64 18844, !7, i64 18848, !7, i64 18852, !7, i64 18856}
!99 = !{!"", !4, i64 0, !4, i64 1, !7, i64 4, !7, i64 8, !7, i64 12, !100, i64 16, !7, i64 32, !7, i64 36}
!100 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!101 = !{!"aom_internal_error_info", !4, i64 0, !7, i64 4, !4, i64 8, !7, i64 88, !4, i64 92}
!102 = !{!"", !43, i64 0, !43, i64 1, !43, i64 2, !43, i64 3, !43, i64 4, !43, i64 5, !43, i64 6, !43, i64 7, !43, i64 8, !43, i64 9, !43, i64 10, !43, i64 11, !4, i64 12, !4, i64 13, !7, i64 16, !7, i64 20, !4, i64 24}
!103 = !{!"CommonModeInfoParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !7, i64 28, !4, i64 32, !3, i64 36, !7, i64 40, !7, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!104 = !{!"CommonQuantParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !43, i64 9240, !7, i64 9244, !7, i64 9248, !7, i64 9252}
!105 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !7, i64 164, !4, i64 168}
!106 = !{!"", !4, i64 0, !4, i64 3072}
!107 = !{!"loopfilter", !4, i64 0, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !7, i64 32}
!108 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !7, i64 52, !4, i64 56, !3, i64 68, !7, i64 72, !3, i64 76, !109, i64 80, !7, i64 84, !109, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !3, i64 144}
!109 = !{!"long", !4, i64 0}
!110 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !4, i64 72, !7, i64 136}
!111 = !{!"", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 120, !4, i64 124, !7, i64 204, !4, i64 208, !7, i64 288, !7, i64 292, !7, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !7, i64 596, !7, i64 600, !7, i64 604, !7, i64 608, !7, i64 612, !7, i64 616, !7, i64 620, !7, i64 624, !7, i64 628, !7, i64 632, !7, i64 636, !7, i64 640, !11, i64 644}
!112 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16}
!113 = !{!"SequenceHeader", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !4, i64 16, !7, i64 20, !7, i64 24, !4, i64 28, !7, i64 32, !7, i64 36, !100, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !7, i64 112, !4, i64 116, !7, i64 244, !114, i64 248, !4, i64 264, !115, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!114 = !{!"aom_timing", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!115 = !{!"aom_dec_model_info", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!116 = !{!"CommonTileParams", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !4, i64 60, !4, i64 320, !7, i64 580, !7, i64 584}
!117 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !7, i64 20, !7, i64 24, !7, i64 28}
!118 = !{!119, !7, i64 4}
!119 = !{!"RefCntBuffer", !7, i64 0, !7, i64 4, !4, i64 8, !7, i64 36, !4, i64 40, !3, i64 68, !3, i64 72, !105, i64 76, !7, i64 248, !7, i64 252, !7, i64 256, !7, i64 260, !4, i64 264, !7, i64 616, !4, i64 620, !111, i64 624, !120, i64 1272, !108, i64 1284, !4, i64 1432, !4, i64 1436, !4, i64 1452, !4, i64 1460, !121, i64 1464}
!120 = !{!"aom_codec_frame_buffer", !3, i64 0, !109, i64 4, !3, i64 8}
!121 = !{!"frame_contexts", !4, i64 0, !4, i64 390, !4, i64 930, !4, i64 966, !4, i64 1014, !4, i64 1070, !4, i64 1134, !4, i64 1206, !4, i64 1286, !4, i64 1374, !4, i64 1470, !4, i64 1790, !4, i64 5990, !4, i64 8090, !4, i64 8126, !4, i64 8138, !4, i64 8174, !4, i64 8192, !4, i64 8336, !4, i64 8468, !4, i64 9216, !4, i64 9240, !4, i64 9372, !4, i64 9412, !4, i64 9588, !4, i64 9720, !4, i64 9832, !4, i64 9944, !4, i64 10574, !4, i64 11204, !4, i64 11330, !4, i64 11342, !4, i64 11372, !4, i64 11480, !4, i64 11510, !4, i64 11564, !4, i64 11618, !4, i64 11654, !4, i64 11780, !4, i64 11816, !4, i64 11852, !4, i64 11870, !4, i64 11888, !122, i64 11912, !122, i64 12198, !4, i64 12484, !123, i64 12490, !4, i64 12580, !4, i64 12712, !4, i64 12724, !4, i64 12732, !4, i64 12738, !4, i64 12744, !4, i64 12856, !4, i64 13636, !4, i64 14076, !4, i64 14204, !4, i64 14904, !4, i64 15032, !4, i64 15128, !4, i64 15138, !4, i64 15178, !4, i64 15188, !4, i64 20492, !4, i64 21036, !4, i64 21054, !7, i64 21260}
!122 = !{!"", !4, i64 0, !4, i64 10}
!123 = !{!"segmentation_probs", !4, i64 0, !4, i64 18, !4, i64 36}
!124 = !{!100, !7, i64 0}
!125 = !{!100, !7, i64 4}
!126 = !{!42, !43, i64 4088}
!127 = !{!42, !7, i64 4}
!128 = !{!42, !4, i64 4468}
!129 = !{!98, !7, i64 1372}
!130 = !{!42, !43, i64 4089}
!131 = !{!42, !7, i64 0}
!132 = !{!42, !4, i64 4469}
!133 = !{!98, !7, i64 1368}
!134 = !{!135, !3, i64 0}
!135 = !{!"obmc_inter_pred_ctxt", !3, i64 0, !3, i64 4}
!136 = !{!135, !3, i64 4}
!137 = !{!76, !7, i64 40}
!138 = !{!76, !3, i64 24}
!139 = !{!140, !3, i64 4}
!140 = !{!"build_prediction_ctxt", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20}
!141 = !{!140, !3, i64 8}
!142 = !{!140, !3, i64 12}
!143 = !{!140, !3, i64 16}
!144 = !{!140, !3, i64 0}
!145 = !{!42, !3, i64 6912}
!146 = !{!42, !7, i64 4116}
!147 = !{!140, !7, i64 20}
!148 = !{!42, !7, i64 4120}
!149 = !{!42, !7, i64 4124}
!150 = !{!42, !7, i64 4128}
!151 = !{!15, !4, i64 121}
!152 = !{!76, !4, i64 128}
!153 = !{!76, !4, i64 129}
!154 = !{!15, !4, i64 151}
!155 = !{!42, !3, i64 4140}
!156 = !{!108, !7, i64 140}
!157 = !{!67, !7, i64 0}
!158 = !{!67, !3, i64 4}
!159 = !{!67, !3, i64 8}
!160 = !{!161, !7, i64 4}
!161 = !{!"", !4, i64 0, !7, i64 4, !7, i64 8}
!162 = !{!161, !7, i64 8}
!163 = !{!161, !4, i64 0}
!164 = !{!27, !7, i64 28}
!165 = !{!27, !7, i64 24}
!166 = !{!27, !3, i64 4}
!167 = !{!27, !7, i64 8}
!168 = !{!27, !7, i64 20}
!169 = !{!27, !7, i64 0}
!170 = !{!15, !4, i64 119}
!171 = !{!23, !3, i64 16}
!172 = !{!23, !3, i64 20}
!173 = !{!98, !4, i64 16269}
