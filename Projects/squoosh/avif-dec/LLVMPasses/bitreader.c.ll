; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/bitreader.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/bitreader.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_reader = type { i8*, i8*, %struct.od_ec_dec, %struct.Accounting*, i8 }
%struct.od_ec_dec = type { i8*, i32, i8*, i8*, i32, i16, i16 }
%struct.Accounting = type { %struct.AccountingSymbols, i32, [1021 x i16], %struct.AccountingSymbolContext, i32 }
%struct.AccountingSymbols = type { %struct.AccountingSymbol*, i32, i32, i32, %struct.AccountingDictionary }
%struct.AccountingSymbol = type { %struct.AccountingSymbolContext, i32, i32, i32 }
%struct.AccountingDictionary = type { [256 x i8*], i32 }
%struct.AccountingSymbolContext = type { i16, i16 }

; Function Attrs: nounwind
define hidden i32 @aom_reader_init(%struct.aom_reader* %r, i8* %buffer, i32 %size) #0 {
entry:
  %retval = alloca i32, align 4
  %r.addr = alloca %struct.aom_reader*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %tobool1 = icmp ne i8* %1, null
  br i1 %tobool1, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %2 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %3 = load i32, i32* %size.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %3
  %4 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %buffer_end = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %4, i32 0, i32 1
  store i8* %add.ptr, i8** %buffer_end, align 4, !tbaa !8
  %5 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %6 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %buffer2 = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %6, i32 0, i32 0
  store i8* %5, i8** %buffer2, align 4, !tbaa !13
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %7, i32 0, i32 2
  %8 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %9 = load i32, i32* %size.addr, align 4, !tbaa !6
  call void @od_ec_dec_init(%struct.od_ec_dec* %ec, i8* %8, i32 %9)
  %10 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %10, i32 0, i32 3
  store %struct.Accounting* null, %struct.Accounting** %accounting, align 4, !tbaa !14
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

declare void @od_ec_dec_init(%struct.od_ec_dec*, i8*, i32) #1

; Function Attrs: nounwind
define hidden i8* @aom_reader_find_begin(%struct.aom_reader* %r) #0 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 0
  %1 = load i8*, i8** %buffer, align 4, !tbaa !13
  ret i8* %1
}

; Function Attrs: nounwind
define hidden i8* @aom_reader_find_end(%struct.aom_reader* %r) #0 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %buffer_end = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 1
  %1 = load i8*, i8** %buffer_end, align 4, !tbaa !8
  ret i8* %1
}

; Function Attrs: nounwind
define hidden i32 @aom_reader_tell(%struct.aom_reader* %r) #0 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 2
  %call = call i32 @od_ec_dec_tell(%struct.od_ec_dec* %ec)
  ret i32 %call
}

declare i32 @od_ec_dec_tell(%struct.od_ec_dec*) #1

; Function Attrs: nounwind
define hidden i32 @aom_reader_tell_frac(%struct.aom_reader* %r) #0 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %0 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %ec = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %0, i32 0, i32 2
  %call = call i32 @od_ec_dec_tell_frac(%struct.od_ec_dec* %ec)
  ret i32 %call
}

declare i32 @od_ec_dec_tell_frac(%struct.od_ec_dec*) #1

; Function Attrs: nounwind
define hidden i32 @aom_reader_has_overflowed(%struct.aom_reader* %r) #0 {
entry:
  %r.addr = alloca %struct.aom_reader*, align 4
  %tell_bits = alloca i32, align 4
  %tell_bytes = alloca i32, align 4
  store %struct.aom_reader* %r, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %0 = bitcast i32* %tell_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %call = call i32 @aom_reader_tell(%struct.aom_reader* %1)
  store i32 %call, i32* %tell_bits, align 4, !tbaa !15
  %2 = bitcast i32* %tell_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i32, i32* %tell_bits, align 4, !tbaa !15
  %add = add i32 %3, 7
  %shr = lshr i32 %add, 3
  store i32 %shr, i32* %tell_bytes, align 4, !tbaa !15
  %4 = load i32, i32* %tell_bytes, align 4, !tbaa !15
  %5 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %buffer_end = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %5, i32 0, i32 1
  %6 = load i8*, i8** %buffer_end, align 4, !tbaa !8
  %7 = load %struct.aom_reader*, %struct.aom_reader** %r.addr, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.aom_reader, %struct.aom_reader* %7, i32 0, i32 0
  %8 = load i8*, i8** %buffer, align 4, !tbaa !13
  %sub.ptr.lhs.cast = ptrtoint i8* %6 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %8 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %cmp = icmp sgt i32 %4, %sub.ptr.sub
  %conv = zext i1 %cmp to i32
  %9 = bitcast i32* %tell_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %tell_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #3
  ret i32 %conv
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !3, i64 4}
!9 = !{!"aom_reader", !3, i64 0, !3, i64 4, !10, i64 8, !3, i64 32, !4, i64 36}
!10 = !{!"od_ec_dec", !3, i64 0, !11, i64 4, !3, i64 8, !3, i64 12, !11, i64 16, !12, i64 20, !12, i64 22}
!11 = !{!"int", !4, i64 0}
!12 = !{!"short", !4, i64 0}
!13 = !{!9, !3, i64 0}
!14 = !{!9, !3, i64 32}
!15 = !{!11, !11, i64 0}
