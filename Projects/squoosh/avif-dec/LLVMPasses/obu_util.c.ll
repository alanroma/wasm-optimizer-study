; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/obu_util.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/obu_util.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.ObuHeader = type { i32, i8, i32, i32, i32, i32 }
%struct.aom_read_bit_buffer = type { i8*, i8*, i32, i8*, void (i8*)* }

; Function Attrs: nounwind
define hidden i32 @aom_read_obu_header(i8* %buffer, i32 %buffer_length, i32* %consumed, %struct.ObuHeader* %header, i32 %is_annexb) #0 {
entry:
  %retval = alloca i32, align 4
  %buffer.addr = alloca i8*, align 4
  %buffer_length.addr = alloca i32, align 4
  %consumed.addr = alloca i32*, align 4
  %header.addr = alloca %struct.ObuHeader*, align 4
  %is_annexb.addr = alloca i32, align 4
  %rb = alloca %struct.aom_read_bit_buffer, align 4
  %parse_result = alloca i32, align 4
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i32 %buffer_length, i32* %buffer_length.addr, align 4, !tbaa !6
  store i32* %consumed, i32** %consumed.addr, align 4, !tbaa !2
  store %struct.ObuHeader* %header, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  store i32 %is_annexb, i32* %is_annexb.addr, align 4, !tbaa !8
  %0 = load i32, i32* %buffer_length.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %0, 1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32*, i32** %consumed.addr, align 4, !tbaa !2
  %tobool = icmp ne i32* %1, null
  br i1 %tobool, label %lor.lhs.false1, label %if.then

lor.lhs.false1:                                   ; preds = %lor.lhs.false
  %2 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %tobool2 = icmp ne %struct.ObuHeader* %2, null
  br i1 %tobool2, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false1, %lor.lhs.false, %entry
  store i32 8, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false1
  %3 = bitcast %struct.aom_read_bit_buffer* %rb to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %3) #3
  %bit_buffer = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 0
  %4 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  store i8* %4, i8** %bit_buffer, align 4, !tbaa !10
  %bit_buffer_end = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 1
  %5 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %6 = load i32, i32* %buffer_length.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %6
  store i8* %add.ptr, i8** %bit_buffer_end, align 4, !tbaa !12
  %bit_offset = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 2
  store i32 0, i32* %bit_offset, align 4, !tbaa !13
  %error_handler_data = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 3
  store i8* null, i8** %error_handler_data, align 4, !tbaa !14
  %error_handler = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 4
  store void (i8*)* null, void (i8*)** %error_handler, align 4, !tbaa !15
  %7 = bitcast i32* %parse_result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load i32, i32* %is_annexb.addr, align 4, !tbaa !8
  %9 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %call = call i32 @read_obu_header(%struct.aom_read_bit_buffer* %rb, i32 %8, %struct.ObuHeader* %9)
  store i32 %call, i32* %parse_result, align 4, !tbaa !16
  %10 = load i32, i32* %parse_result, align 4, !tbaa !16
  %cmp3 = icmp eq i32 %10, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %11 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %11, i32 0, i32 0
  %12 = load i32, i32* %size, align 4, !tbaa !17
  %13 = load i32*, i32** %consumed.addr, align 4, !tbaa !2
  store i32 %12, i32* %13, align 4, !tbaa !6
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.end
  %14 = load i32, i32* %parse_result, align 4, !tbaa !16
  store i32 %14, i32* %retval, align 4
  %15 = bitcast i32* %parse_result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %16 = bitcast %struct.aom_read_bit_buffer* %rb to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %16) #3
  br label %return

return:                                           ; preds = %if.end5, %if.then
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @read_obu_header(%struct.aom_read_bit_buffer* %rb, i32 %is_annexb, %struct.ObuHeader* %header) #0 {
entry:
  %retval = alloca i32, align 4
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %is_annexb.addr = alloca i32, align 4
  %header.addr = alloca %struct.ObuHeader*, align 4
  %bit_buffer_byte_length = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i32 %is_annexb, i32* %is_annexb.addr, align 4, !tbaa !8
  store %struct.ObuHeader* %header, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %0 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_read_bit_buffer* %0, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.ObuHeader* %1, null
  br i1 %tobool1, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 8, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = bitcast i32* %bit_buffer_byte_length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %bit_buffer_end = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %3, i32 0, i32 1
  %4 = load i8*, i8** %bit_buffer_end, align 4, !tbaa !12
  %5 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %bit_buffer = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %5, i32 0, i32 0
  %6 = load i8*, i8** %bit_buffer, align 4, !tbaa !10
  %sub.ptr.lhs.cast = ptrtoint i8* %4 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %6 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %bit_buffer_byte_length, align 4, !tbaa !6
  %7 = load i32, i32* %bit_buffer_byte_length, align 4, !tbaa !6
  %cmp = icmp slt i32 %7, 1
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %8 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %8, i32 0, i32 0
  store i32 1, i32* %size, align 4, !tbaa !17
  %9 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %9)
  %cmp4 = icmp ne i32 %call, 0
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.end3
  %10 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call7 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %10, i32 4)
  %conv = trunc i32 %call7 to i8
  %11 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %11, i32 0, i32 1
  store i8 %conv, i8* %type, align 4, !tbaa !19
  %12 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %type8 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %12, i32 0, i32 1
  %13 = load i8, i8* %type8, align 4, !tbaa !19
  %conv9 = zext i8 %13 to i32
  %call10 = call i32 @valid_obu_type(i32 %conv9)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %if.end6
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.end6
  %14 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call14 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %14)
  %15 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %has_extension = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %15, i32 0, i32 3
  store i32 %call14, i32* %has_extension, align 4, !tbaa !20
  %16 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call15 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %16)
  %17 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %has_size_field = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %17, i32 0, i32 2
  store i32 %call15, i32* %has_size_field, align 4, !tbaa !21
  %18 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %has_size_field16 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %18, i32 0, i32 2
  %19 = load i32, i32* %has_size_field16, align 4, !tbaa !21
  %tobool17 = icmp ne i32 %19, 0
  br i1 %tobool17, label %if.end20, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end13
  %20 = load i32, i32* %is_annexb.addr, align 4, !tbaa !8
  %tobool18 = icmp ne i32 %20, 0
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %land.lhs.true
  store i32 5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %land.lhs.true, %if.end13
  %21 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call21 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %21)
  %cmp22 = icmp ne i32 %call21, 0
  br i1 %cmp22, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.end20
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end25:                                         ; preds = %if.end20
  %22 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %has_extension26 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %22, i32 0, i32 3
  %23 = load i32, i32* %has_extension26, align 4, !tbaa !20
  %tobool27 = icmp ne i32 %23, 0
  br i1 %tobool27, label %if.then28, label %if.end41

if.then28:                                        ; preds = %if.end25
  %24 = load i32, i32* %bit_buffer_byte_length, align 4, !tbaa !6
  %cmp29 = icmp eq i32 %24, 1
  br i1 %cmp29, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.then28
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end32:                                         ; preds = %if.then28
  %25 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %size33 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %25, i32 0, i32 0
  %26 = load i32, i32* %size33, align 4, !tbaa !17
  %add = add i32 %26, 1
  store i32 %add, i32* %size33, align 4, !tbaa !17
  %27 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call34 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %27, i32 3)
  %28 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %temporal_layer_id = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %28, i32 0, i32 4
  store i32 %call34, i32* %temporal_layer_id, align 4, !tbaa !22
  %29 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call35 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %29, i32 2)
  %30 = load %struct.ObuHeader*, %struct.ObuHeader** %header.addr, align 4, !tbaa !2
  %spatial_layer_id = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %30, i32 0, i32 5
  store i32 %call35, i32* %spatial_layer_id, align 4, !tbaa !23
  %31 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call36 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %31, i32 3)
  %cmp37 = icmp ne i32 %call36, 0
  br i1 %cmp37, label %if.then39, label %if.end40

if.then39:                                        ; preds = %if.end32
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end40:                                         ; preds = %if.end32
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %if.end25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end41, %if.then39, %if.then31, %if.then24, %if.then19, %if.then12, %if.then5, %if.then2
  %32 = bitcast i32* %bit_buffer_byte_length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %33 = load i32, i32* %retval, align 4
  ret i32 %33
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @aom_read_obu_header_and_size(i8* %data, i32 %bytes_available, i32 %is_annexb, %struct.ObuHeader* %obu_header, i32* %payload_size, i32* %bytes_read) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  %bytes_available.addr = alloca i32, align 4
  %is_annexb.addr = alloca i32, align 4
  %obu_header.addr = alloca %struct.ObuHeader*, align 4
  %payload_size.addr = alloca i32*, align 4
  %bytes_read.addr = alloca i32*, align 4
  %length_field_size_obu = alloca i32, align 4
  %length_field_size_payload = alloca i32, align 4
  %obu_size = alloca i32, align 4
  %status = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %rb = alloca %struct.aom_read_bit_buffer, align 4
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %bytes_available, i32* %bytes_available.addr, align 4, !tbaa !6
  store i32 %is_annexb, i32* %is_annexb.addr, align 4, !tbaa !8
  store %struct.ObuHeader* %obu_header, %struct.ObuHeader** %obu_header.addr, align 4, !tbaa !2
  store i32* %payload_size, i32** %payload_size.addr, align 4, !tbaa !2
  store i32* %bytes_read, i32** %bytes_read.addr, align 4, !tbaa !2
  %0 = bitcast i32* %length_field_size_obu to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %length_field_size_obu, align 4, !tbaa !6
  %1 = bitcast i32* %length_field_size_payload to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %length_field_size_payload, align 4, !tbaa !6
  %2 = bitcast i32* %obu_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %obu_size, align 4, !tbaa !6
  %3 = bitcast i32* %status to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i32, i32* %is_annexb.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.end2

if.then:                                          ; preds = %entry
  %5 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %6 = load i32, i32* %bytes_available.addr, align 4, !tbaa !6
  %call = call i32 @read_obu_size(i8* %5, i32 %6, i32* %obu_size, i32* %length_field_size_obu)
  store i32 %call, i32* %status, align 4, !tbaa !16
  %7 = load i32, i32* %status, align 4, !tbaa !16
  %cmp = icmp ne i32 %7, 0
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %8 = load i32, i32* %status, align 4, !tbaa !16
  store i32 %8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup27

if.end:                                           ; preds = %if.then
  br label %if.end2

if.end2:                                          ; preds = %if.end, %entry
  %9 = bitcast %struct.aom_read_bit_buffer* %rb to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %9) #3
  %bit_buffer = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 0
  %10 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %11 = load i32, i32* %length_field_size_obu, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %11
  store i8* %add.ptr, i8** %bit_buffer, align 4, !tbaa !10
  %bit_buffer_end = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 1
  %12 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %13 = load i32, i32* %bytes_available.addr, align 4, !tbaa !6
  %add.ptr3 = getelementptr inbounds i8, i8* %12, i32 %13
  store i8* %add.ptr3, i8** %bit_buffer_end, align 4, !tbaa !12
  %bit_offset = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 2
  store i32 0, i32* %bit_offset, align 4, !tbaa !13
  %error_handler_data = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 3
  store i8* null, i8** %error_handler_data, align 4, !tbaa !14
  %error_handler = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 4
  store void (i8*)* null, void (i8*)** %error_handler, align 4, !tbaa !15
  %14 = load i32, i32* %is_annexb.addr, align 4, !tbaa !8
  %15 = load %struct.ObuHeader*, %struct.ObuHeader** %obu_header.addr, align 4, !tbaa !2
  %call4 = call i32 @read_obu_header(%struct.aom_read_bit_buffer* %rb, i32 %14, %struct.ObuHeader* %15)
  store i32 %call4, i32* %status, align 4, !tbaa !16
  %16 = load i32, i32* %status, align 4, !tbaa !16
  %cmp5 = icmp ne i32 %16, 0
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end2
  %17 = load i32, i32* %status, align 4, !tbaa !16
  store i32 %17, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end2
  %18 = load %struct.ObuHeader*, %struct.ObuHeader** %obu_header.addr, align 4, !tbaa !2
  %has_size_field = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %18, i32 0, i32 2
  %19 = load i32, i32* %has_size_field, align 4, !tbaa !21
  %tobool8 = icmp ne i32 %19, 0
  br i1 %tobool8, label %if.else, label %if.then9

if.then9:                                         ; preds = %if.end7
  %20 = load i32, i32* %obu_size, align 4, !tbaa !6
  %21 = load %struct.ObuHeader*, %struct.ObuHeader** %obu_header.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %21, i32 0, i32 0
  %22 = load i32, i32* %size, align 4, !tbaa !17
  %cmp10 = icmp ult i32 %20, %22
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.then9
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.then9
  %23 = load i32, i32* %obu_size, align 4, !tbaa !6
  %24 = load %struct.ObuHeader*, %struct.ObuHeader** %obu_header.addr, align 4, !tbaa !2
  %size13 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %24, i32 0, i32 0
  %25 = load i32, i32* %size13, align 4, !tbaa !17
  %sub = sub i32 %23, %25
  %26 = load i32*, i32** %payload_size.addr, align 4, !tbaa !2
  store i32 %sub, i32* %26, align 4, !tbaa !6
  br label %if.end24

if.else:                                          ; preds = %if.end7
  %27 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %28 = load i32, i32* %length_field_size_obu, align 4, !tbaa !6
  %add.ptr14 = getelementptr inbounds i8, i8* %27, i32 %28
  %29 = load %struct.ObuHeader*, %struct.ObuHeader** %obu_header.addr, align 4, !tbaa !2
  %size15 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %29, i32 0, i32 0
  %30 = load i32, i32* %size15, align 4, !tbaa !17
  %add.ptr16 = getelementptr inbounds i8, i8* %add.ptr14, i32 %30
  %31 = load i32, i32* %bytes_available.addr, align 4, !tbaa !6
  %32 = load i32, i32* %length_field_size_obu, align 4, !tbaa !6
  %sub17 = sub i32 %31, %32
  %33 = load %struct.ObuHeader*, %struct.ObuHeader** %obu_header.addr, align 4, !tbaa !2
  %size18 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %33, i32 0, i32 0
  %34 = load i32, i32* %size18, align 4, !tbaa !17
  %sub19 = sub i32 %sub17, %34
  %35 = load i32*, i32** %payload_size.addr, align 4, !tbaa !2
  %call20 = call i32 @read_obu_size(i8* %add.ptr16, i32 %sub19, i32* %35, i32* %length_field_size_payload)
  store i32 %call20, i32* %status, align 4, !tbaa !16
  %36 = load i32, i32* %status, align 4, !tbaa !16
  %cmp21 = icmp ne i32 %36, 0
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.else
  %37 = load i32, i32* %status, align 4, !tbaa !16
  store i32 %37, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end23:                                         ; preds = %if.else
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.end12
  %38 = load i32, i32* %length_field_size_obu, align 4, !tbaa !6
  %39 = load %struct.ObuHeader*, %struct.ObuHeader** %obu_header.addr, align 4, !tbaa !2
  %size25 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %39, i32 0, i32 0
  %40 = load i32, i32* %size25, align 4, !tbaa !17
  %add = add i32 %38, %40
  %41 = load i32, i32* %length_field_size_payload, align 4, !tbaa !6
  %add26 = add i32 %add, %41
  %42 = load i32*, i32** %bytes_read.addr, align 4, !tbaa !2
  store i32 %add26, i32* %42, align 4, !tbaa !6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end24, %if.then22, %if.then11, %if.then6
  %43 = bitcast %struct.aom_read_bit_buffer* %rb to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %43) #3
  br label %cleanup27

cleanup27:                                        ; preds = %cleanup, %if.then1
  %44 = bitcast i32* %status to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  %45 = bitcast i32* %obu_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  %46 = bitcast i32* %length_field_size_payload to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast i32* %length_field_size_obu to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = load i32, i32* %retval, align 4
  ret i32 %48
}

; Function Attrs: nounwind
define internal i32 @read_obu_size(i8* %data, i32 %bytes_available, i32* %obu_size, i32* %length_field_size) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  %bytes_available.addr = alloca i32, align 4
  %obu_size.addr = alloca i32*, align 4
  %length_field_size.addr = alloca i32*, align 4
  %u_obu_size = alloca i64, align 8
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %bytes_available, i32* %bytes_available.addr, align 4, !tbaa !6
  store i32* %obu_size, i32** %obu_size.addr, align 4, !tbaa !2
  store i32* %length_field_size, i32** %length_field_size.addr, align 4, !tbaa !2
  %0 = bitcast i64* %u_obu_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #3
  store i64 0, i64* %u_obu_size, align 8, !tbaa !24
  %1 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %2 = load i32, i32* %bytes_available.addr, align 4, !tbaa !6
  %3 = load i32*, i32** %length_field_size.addr, align 4, !tbaa !2
  %call = call i32 @aom_uleb_decode(i8* %1, i32 %2, i64* %u_obu_size, i32* %3)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load i64, i64* %u_obu_size, align 8, !tbaa !24
  %cmp1 = icmp ugt i64 %4, 4294967295
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %5 = load i64, i64* %u_obu_size, align 8, !tbaa !24
  %conv = trunc i64 %5 to i32
  %6 = load i32*, i32** %obu_size.addr, align 4, !tbaa !2
  store i32 %conv, i32* %6, align 4, !tbaa !6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end3, %if.then2, %if.then
  %7 = bitcast i64* %u_obu_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %7) #3
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

declare i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer*) #2

declare i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer*, i32) #2

; Function Attrs: nounwind
define internal i32 @valid_obu_type(i32 %obu_type) #0 {
entry:
  %obu_type.addr = alloca i32, align 4
  %valid_type = alloca i32, align 4
  store i32 %obu_type, i32* %obu_type.addr, align 4, !tbaa !8
  %0 = bitcast i32* %valid_type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %valid_type, align 4, !tbaa !8
  %1 = load i32, i32* %obu_type.addr, align 4, !tbaa !8
  switch i32 %1, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb
    i32 4, label %sw.bb
    i32 5, label %sw.bb
    i32 6, label %sw.bb
    i32 7, label %sw.bb
    i32 8, label %sw.bb
    i32 15, label %sw.bb
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry
  store i32 1, i32* %valid_type, align 4, !tbaa !8
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb
  %2 = load i32, i32* %valid_type, align 4, !tbaa !8
  %3 = bitcast i32* %valid_type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  ret i32 %2
}

declare i32 @aom_uleb_decode(i8*, i32, i64*, i32*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!11, !3, i64 0}
!11 = !{!"aom_read_bit_buffer", !3, i64 0, !3, i64 4, !9, i64 8, !3, i64 12, !3, i64 16}
!12 = !{!11, !3, i64 4}
!13 = !{!11, !9, i64 8}
!14 = !{!11, !3, i64 12}
!15 = !{!11, !3, i64 16}
!16 = !{!4, !4, i64 0}
!17 = !{!18, !7, i64 0}
!18 = !{!"", !7, i64 0, !4, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20}
!19 = !{!18, !4, i64 4}
!20 = !{!18, !9, i64 12}
!21 = !{!18, !9, i64 8}
!22 = !{!18, !9, i64 16}
!23 = !{!18, !9, i64 20}
!24 = !{!25, !25, i64 0}
!25 = !{!"long long", !4, i64 0}
